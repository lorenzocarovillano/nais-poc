package com.modernsystems.jdbc.mapper;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bphx.ctu.af.annotations.OdfType;

@OdfType
public class NamedRowMapper<T> extends PropertyRowMapper<T> {
	private static final Logger log = LoggerFactory.getLogger(NamedRowMapper.class);
	
	private String[] properties;
	private Method[] setters;
	private PropertyDescriptor[] propertyDescriptors;
	private Class<?>[] setterClasses;
	private Map<String, Method> getters;
	
	public NamedRowMapper(Class<T> clazz, String... properties) {
		super(clazz);
		this.properties = properties;
		initialize();
	}
	
	private void initialize() {
		this.setters = new Method[properties.length];
		this.setterClasses = new Class<?>[properties.length];
		this.getters = new HashMap<>(properties.length);
		this.propertyDescriptors = beanInfo().getPropertyDescriptors();
		for(int idx=0;idx<properties.length;idx++) {
			String prop = properties[idx];
			PropertyDescriptor pd = findPropertyDescriptor(prop, this.propertyDescriptors);
			if(pd==null) {
				log.error("Cannot map property {} to bean class {}", prop, getBeanClass().getName());
				throw new IllegalArgumentException("Cannot map property "+prop+" to bean class "+getBeanClass().getName());
			} else {
				this.setters[idx] = pd.getWriteMethod();
				this.getters.put(prop, pd.getReadMethod());
				this.setterClasses[idx] = setters[idx].getParameterTypes()[0]; 
			}
		}
	}
	
	private PropertyDescriptor findPropertyDescriptor(String prop, PropertyDescriptor[] pds) {
		for (PropertyDescriptor pd : pds) {
			if (pd.getName().equals(prop)) {
				return pd;
			}
		}
		return null;
	}

	@Override
	public T mapRow(T target, ResultSet rs) throws SQLException {
		for(int idx=0;idx<properties.length;idx++) {
			log.debug("setting property {}", properties[idx]);
			int column = idx + 1;
			try {
				setters[idx].invoke(target, valueAdapter.adapt(extractColumn(rs, column), setterClasses[idx]));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				log.error("Cannot map resultset to property "+properties[idx]+" at column #"+column, e);
			}
		}
		return target;
	}
	
	@Override
	public Object getPropertyValue(T target, String name){

		if(getters.containsKey(name)) {
			try {
				return getters.get(name).invoke(target);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				throw new SqlPropertyMapException(name, target.getClass().getSimpleName(),
						(e instanceof InvocationTargetException ? e.getCause() : e));
			}

			// LCAROVILLANO - Added else logic
		} else {

			PropertyDescriptor pd = findPropertyDescriptor(name, this.propertyDescriptors);
			if (pd == null) {
				throw new PropertyNotFoundException(name, target.getClass().getSimpleName());
			}

			try {
				return pd.getReadMethod().invoke(target);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				throw new SqlPropertyMapException(name, target.getClass().getSimpleName(),
						(e instanceof InvocationTargetException ? e.getCause() : e));
			}
		}
		// throw new PropertyNotFoundException(name, target.getClass().getSimpleName());
	}
}

package com.modernsystems.jdbc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javax.inject.Inject;

import com.bphx.ctu.af.annotations.OdfType;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.sql.util.Dates;
import com.modernsystems.ctu.core.InjectorProvider;
import com.modernsystems.jdbc.IValueAdapter;

@OdfType
public abstract class AbstractRowMapper<T> implements IRowMapper<T> {

	@Inject
	protected IValueAdapter valueAdapter;

	public AbstractRowMapper() {
        InjectorProvider.injectMembers(this);
	}

	protected Object extractColumn(ResultSet rs, int columnNo) throws SQLException {
		//TODO invoke proper getter
		// LCAROVILLANO - Declare return object for null check
		Object obj = rs.getObject(columnNo);
		if (obj != null) {
			int columnType = rs.getMetaData().getColumnType(columnNo);
			// XXX: Oracle timestamp with local time type
			if (columnType == Types.TIMESTAMP || columnType == -101) {
				return Dates.from(rs.getTimestamp(columnNo));
			}
			if (columnType == Types.DATE) {
				return Dates.from(rs.getDate(columnNo));
			}
			if (columnType == Types.TIME) {
				return Dates.from(rs.getTime(columnNo));
			}
			if (columnType == Types.VARCHAR) {

				String data = rs.getString(columnNo);
				byte stream[] = new byte[2];
				byte[] b = data.getBytes();
				stream[1] = (byte) b.length;
				int numByteL = b.length >> 8;
				stream[0] = (byte) numByteL;
				return MarshalByte.toString(stream) + data;
			}
		}
		return obj;
	}

}
/*******************************************************************************
 * Confidential Information - Limited distribution to authorized persons only.
 * This software is protected as an unpublished work under the U.S. Copyright
 * Act of 1976.
 *
 * Copyright (c) 2004-2018, ModSys International Ltd. All rights reserved.
 *******************************************************************************/

package com.modernsystems.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.Session;
import com.bphx.ctu.af.sp.Returnability;
import com.google.inject.Inject;
import com.mdsy.sql.SqlError;
import com.mdsy.sql.SqlException;
import com.modernsystems.jdbc.mapper.BeanPropertyMapper;
import com.modernsystems.jdbc.mapper.IRowMapper;
import com.modernsystems.jdbc.mapper.NamedRowMapper;
import com.modernsystems.jdbc.mapper.SingleColumnRowMapper;
import com.modernsystems.jdbc.parse.IParsedSqlProvider;
import com.modernsystems.jdbc.parse.ParsedSql;
import com.modernsystems.jdbc.utils.JdbcUtils;

public abstract class BaseSqlDao<T extends BaseSqlTo> extends CoreSqlDao<T> {
	@Inject
	public IQueryProvider qp;
	@Inject
	IPreparedStatementProvider psp;
	@Inject
	IParsedSqlProvider psqp;
	@Inject
	IParamBinder pb;
	@Inject
	IReturnableCursorConnectionCustomizer connectionCustomizer;
	// Change Cursor Manager to be injected when we have 'run unit' scope available
	@Inject
	IUpdateNotifier updn;

	public BaseSqlDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	public String getQuery(String fullname) {
		return qp.get(fullname);
	}

	private <O> O runQuery(QueryBuilder<T> qb, O old, IRowMapper<O> rowMapper) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			dbStatus.clear();
			String query = getQuery(qb.getQueryName());
			ParsedSql psql = getParsedSql(query);
			log.info("Query {}", query);
			Connection connection = getConnection();
			ps = psp.get(psql, connection, qb.getResultsetType(), qb.getUpdatability(), qb.getHoldability());
			bindParams(ps, psql, qb);
			rs = ps.executeQuery();
			if (!rs.next()) {
				// no rows selected
				dbStatus.collect(errs.noRecordsFound());
				return old;
			}
			// LCAROVILLANO - rs.getObject(1)
			if (rs.getObject(2) == null) {
				// no value found
				dbStatus.collect(errs.nullDbWithoutIndicators());
				return old;
			}
			old = rowMapper.mapRow(old, rs);
			// Original code used !rs.last() for this check. However, at least for
			// DB2/LUW on a single item select, this islast() call would produce an
			// exception
			// com.ibm.db2.jcc.am.SqlException: [jcc][t4][10179][10900][4.19.26] This method
			// should only be called on ResultSet objects that are scrollable (type
			// TYPE_SCROLL_SENSITIVE or TYPE_SCROLL_INSENSITIVE). ERRORCODE=-4476,
			// SQLSTATE=null
			// In general, isLast() is an 'optional' operation, and so cannot be reliably
			// used.
			// Since we are only interested in knowing if there are more results or not,
			// instead, we do a next() operation
			// and check if there was one
			if (rs.next()) {
				// more than one row
				dbStatus.collect(errs.moreThanOneRow());
			}
		} catch (SQLException e) {
			log.error("Could not setup PreparedStatement", e);
			dbStatus.collect(e);
		} finally {
			dbStatus.updateSqlca();
			JdbcUtils.closeResultSet(rs);
			JdbcUtils.closeStatement(ps);
		}
		return old;

	}

	public Connection getConnection() {
		return cp.getConnection();
	}

	public T querySingleResult(QueryBuilder<T> qb, T old) {
		return runQuery(qb, old, qb.getRowMapper());
	}

	public <O> O queryScalarResult(QueryBuilder<T> qb, Class<O> cls, O dft) {
		return runQuery(qb, dft, singleColumnRowMpper(cls));
	}

	private <O> IRowMapper<O> singleColumnRowMpper(Class<O> cls) {
		return new SingleColumnRowMapper<>(cls);
	}

	public Cursor queryCursor(QueryBuilder<T> qb) {
		Cursor c = null;
		Connection connection = null;
		try {
			dbStatus.clear();
			String query = getQuery(qb.getQueryName());
			ParsedSql psql = getParsedSql(query);
			log.info("Open cursor {}", query);
			connection = getConnection();
			if (qb.getReturnability() != Returnability.WITHOUT_RETURN) {
				connectionCustomizer.customize(connection);
			}
			PreparedStatement ps = psp.get(psql, connection, qb.getResultsetType(), qb.getUpdatability(),
					qb.getHoldability());
			bindParams(ps, psql, qb);
			c = new Cursor(ps.executeQuery(), ps);
			Session.current().getCursorManager().cursorOpened(c.getResultSet(), qb.getReturnability());
		} catch (SQLException e) {
			log.error("Could not setup PreparedStatement", e);
			dbStatus.collect(e);
		} finally {
			if (connection != null && qb.getReturnability() != Returnability.WITHOUT_RETURN) {
				connectionCustomizer.uncustomize(connection);
			}
			dbStatus.updateSqlca();
		}
		return c;
	}

	private void bindParams(PreparedStatement ps, ParsedSql psql, QueryBuilder<T> qb) throws SQLException {
		StringBuilder sb = null;
		SqlError paramErr = qb.getParameterError();
		if (Objects.nonNull(paramErr)) {
			throw new SqlException(paramErr);
		}
		ParameterValueResolver<T> pvr = qb.getParams();
		IRowMapper<T> rowMapper = qb.getRowMapper();
		List<String> reqParams = psql.getParameterNames();
		for (int i = 0; i < reqParams.size(); i++) {
			String paramName = reqParams.get(i);
			Object value = pvr.get(paramName, rowMapper);
			if (log.isInfoEnabled()) {
				if (sb == null) {
					sb = new StringBuilder();
				} else {
					sb.append('\'');
				}
				sb.append(paramName);
				sb.append('=');
				sb.append(value == null ? "<null>" : value.toString());
			}
			pb.bind(ps, i + 1, value);
		}
		if (log.isInfoEnabled() && sb != null) {
			log.info("Query parameters: {}", sb);
		}
	}

	private ParsedSql getParsedSql(String query) {
		return psqp.get(query);
	}

	public T fetch(Cursor cursor, T to, IRowMapper<T> rowMapper) {
		ResultSet rs = cursor == null ? null : cursor.getResultSet();
		return fetch(rs, to, rowMapper);
	}

	// LCAROVILLANO - Fixing fecth STEP
	public T fetch(ResultSet rs, T to, IRowMapper<T> rowMapper) {
		try {

			log.debug("Fetching resultset {}", rs);

			dbStatus.clear();

			if (rs == null) {

				dbStatus.collect(errs.cursorNotOpen());

			} else {

				if (rs.isClosed()) { // NAIS fix temp

					dbStatus.collect(errs.noRecordsToFetch());

				} else {

					if (!rs.next()) {

						dbStatus.collect(errs.noRecordsToFetch());

					} else {

						T rowResult = rowMapper.mapRow(to, rs);

						if (rowResult != null) {

							to = rowResult;

						} else {

							dbStatus.collect(errs.nullDbWithoutIndicators());

						}

					}

				}

			}

		} catch (SQLException e) {

			log.error("Error mapping ResultSet to bean", e);

			dbStatus.collect(e);

		} finally {

			dbStatus.updateSqlca();

		}

		return to;

	}

	public T fetch(ResultSet rs, T to) {
		return fetch(rs, to, getBPM());
	}

	public T fetch(Cursor cursor, T to) {
		return fetch(cursor, to, getBPM());
	}

	BeanPropertyMapper<T> getBPM() {
		if (bpmCache == null) {
			bpmCache = new BeanPropertyMapper<>(getToClass());
		}
		return bpmCache;
	}

	protected DbAccessStatus closeCursor(Cursor cursor) {
		try {
			log.debug("Closing cursor {}", cursor);
			dbStatus.clear();
			if (cursor != null) {
				cursor.getStatement().close();
				cursor.getResultSet().close();
				Session.current().getCursorManager().cursorClosed(cursor.getResultSet());
				cursor = null;
			} else {
				dbStatus.collect(errs.cursorNotOpen());
			}
		} catch (SQLException e) {
			log.error("Could not close ResultSet", e);
			dbStatus.collect(e);
		} finally {
			dbStatus.updateSqlca();
		}
		return dbStatus;
	}

	public abstract Class<T> getToClass();

	public QueryBuilder<T> buildQuery(String queryName) {
		return QueryBuilder.get(this, queryName);
	}

	private int runUpdate(QueryBuilder<T> qb) throws SQLException {
		PreparedStatement ps = null;
		try {
			String query = getQuery(qb.getQueryName());
			ParsedSql psql = getParsedSql(query);
			log.info("Update {}", query);
			Connection connection = getConnection();
			ps = psp.get(psql, connection);
			bindParams(ps, psql, qb);
			return ps.executeUpdate();
		} finally {
			JdbcUtils.closeStatement(ps);
		}
	}

	public DbAccessStatus insert(QueryBuilder<T> qb) {
		try {
			updn.before();
			dbStatus.clear();
			int rows = runUpdate(qb);
			dbStatus.collectAffectedRecords(rows);
			updn.afterOk();
		} catch (SQLException e) {
			log.error("Error inserting row", e);
			dbStatus.collect(e);
			updn.afterError();
		} finally {
			dbStatus.updateSqlca();
		}
		return dbStatus;
	}

	public DbAccessStatus update(QueryBuilder<T> qb) {
		try {
			updn.before();
			dbStatus.clear();
			int rows = runUpdate(qb);
			dbStatus.collectAffectedRecords(rows);
			updn.afterOk();
		} catch (SQLException e) {
			log.error("Error deleting row", e);
			dbStatus.collect(e);
			updn.afterError();
		} finally {
			dbStatus.updateSqlca();
		}
		return dbStatus;
	}

	public IRowMapper<T> buildNamedRowMapper(Class<T> clazz, String... properties) {
		return new NamedRowMapper<>(clazz, properties);
	}
}

package it.accenture.jnais;

import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IRich;

/**Original name: RichIabs0900<br>*/
public class RichIabs09001 implements IRich {

    //==== PROPERTIES ====
    private Iabs0900 iabs0900;

    //==== CONSTRUCTORS ====
    public RichIabs09001(Iabs0900 iabs0900) {
        this.iabs0900 = iabs0900;
    }

    //==== METHODS ====
    @Override
    public int getCodCompAnia() {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public char getDsOperSql() {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public char getDsStatoElab() {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public long getDsTsCptz() {
        throw new FieldNotMappedException("dsTsCptz");
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        throw new FieldNotMappedException("dsTsCptz");
    }

    @Override
    public String getDsUtente() {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public void setDsUtente(String dsUtente) {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public int getDsVer() {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public void setDsVer(int dsVer) {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public String getDtEffDb() {
        throw new FieldNotMappedException("dtEffDb");
    }

    @Override
    public void setDtEffDb(String dtEffDb) {
        throw new FieldNotMappedException("dtEffDb");
    }

    @Override
    public String getDtEsecRichDb() {
        throw new FieldNotMappedException("dtEsecRichDb");
    }

    @Override
    public void setDtEsecRichDb(String dtEsecRichDb) {
        throw new FieldNotMappedException("dtEsecRichDb");
    }

    @Override
    public String getDtPervRichDb() {
        throw new FieldNotMappedException("dtPervRichDb");
    }

    @Override
    public void setDtPervRichDb(String dtPervRichDb) {
        throw new FieldNotMappedException("dtPervRichDb");
    }

    @Override
    public String getDtRgstrzRichDb() {
        throw new FieldNotMappedException("dtRgstrzRichDb");
    }

    @Override
    public void setDtRgstrzRichDb(String dtRgstrzRichDb) {
        throw new FieldNotMappedException("dtRgstrzRichDb");
    }

    @Override
    public char getFlSimulazione() {
        throw new FieldNotMappedException("flSimulazione");
    }

    @Override
    public void setFlSimulazione(char flSimulazione) {
        throw new FieldNotMappedException("flSimulazione");
    }

    @Override
    public Character getFlSimulazioneObj() {
        return ((Character)getFlSimulazione());
    }

    @Override
    public void setFlSimulazioneObj(Character flSimulazioneObj) {
        setFlSimulazione(((char)flSimulazioneObj));
    }

    @Override
    public char getIabv0002State01() {
        return iabs0900.getIabv0002().getIabv0002StateGlobal().getState01();
    }

    @Override
    public void setIabv0002State01(char iabv0002State01) {
        iabs0900.getIabv0002().getIabv0002StateGlobal().setState01(iabv0002State01);
    }

    @Override
    public char getIabv0002State02() {
        return iabs0900.getIabv0002().getIabv0002StateGlobal().getState02();
    }

    @Override
    public void setIabv0002State02(char iabv0002State02) {
        iabs0900.getIabv0002().getIabv0002StateGlobal().setState02(iabv0002State02);
    }

    @Override
    public char getIabv0002State03() {
        return iabs0900.getIabv0002().getIabv0002StateGlobal().getState03();
    }

    @Override
    public void setIabv0002State03(char iabv0002State03) {
        iabs0900.getIabv0002().getIabv0002StateGlobal().setState03(iabv0002State03);
    }

    @Override
    public char getIabv0002State04() {
        return iabs0900.getIabv0002().getIabv0002StateGlobal().getState04();
    }

    @Override
    public void setIabv0002State04(char iabv0002State04) {
        iabs0900.getIabv0002().getIabv0002StateGlobal().setState04(iabv0002State04);
    }

    @Override
    public char getIabv0002State05() {
        return iabs0900.getIabv0002().getIabv0002StateGlobal().getState05();
    }

    @Override
    public void setIabv0002State05(char iabv0002State05) {
        iabs0900.getIabv0002().getIabv0002StateGlobal().setState05(iabv0002State05);
    }

    @Override
    public char getIabv0002State06() {
        return iabs0900.getIabv0002().getIabv0002StateGlobal().getState06();
    }

    @Override
    public void setIabv0002State06(char iabv0002State06) {
        iabs0900.getIabv0002().getIabv0002StateGlobal().setState06(iabv0002State06);
    }

    @Override
    public char getIabv0002State07() {
        return iabs0900.getIabv0002().getIabv0002StateGlobal().getState07();
    }

    @Override
    public void setIabv0002State07(char iabv0002State07) {
        iabs0900.getIabv0002().getIabv0002StateGlobal().setState07(iabv0002State07);
    }

    @Override
    public char getIabv0002State08() {
        return iabs0900.getIabv0002().getIabv0002StateGlobal().getState08();
    }

    @Override
    public void setIabv0002State08(char iabv0002State08) {
        iabs0900.getIabv0002().getIabv0002StateGlobal().setState08(iabv0002State08);
    }

    @Override
    public char getIabv0002State09() {
        return iabs0900.getIabv0002().getIabv0002StateGlobal().getState09();
    }

    @Override
    public void setIabv0002State09(char iabv0002State09) {
        iabs0900.getIabv0002().getIabv0002StateGlobal().setState09(iabv0002State09);
    }

    @Override
    public char getIabv0002State10() {
        return iabs0900.getIabv0002().getIabv0002StateGlobal().getState10();
    }

    @Override
    public void setIabv0002State10(char iabv0002State10) {
        iabs0900.getIabv0002().getIabv0002StateGlobal().setState10(iabv0002State10);
    }

    @Override
    public char getIabv0002StateCurrent() {
        return iabs0900.getIabv0002().getIabv0002StateCurrent();
    }

    @Override
    public void setIabv0002StateCurrent(char iabv0002StateCurrent) {
        iabs0900.getIabv0002().setIabv0002StateCurrent(iabv0002StateCurrent);
    }

    @Override
    public String getIbAdes() {
        throw new FieldNotMappedException("ibAdes");
    }

    @Override
    public void setIbAdes(String ibAdes) {
        throw new FieldNotMappedException("ibAdes");
    }

    @Override
    public String getIbAdesObj() {
        return getIbAdes();
    }

    @Override
    public void setIbAdesObj(String ibAdesObj) {
        setIbAdes(ibAdesObj);
    }

    @Override
    public String getIbGar() {
        throw new FieldNotMappedException("ibGar");
    }

    @Override
    public void setIbGar(String ibGar) {
        throw new FieldNotMappedException("ibGar");
    }

    @Override
    public String getIbGarObj() {
        return getIbGar();
    }

    @Override
    public void setIbGarObj(String ibGarObj) {
        setIbGar(ibGarObj);
    }

    @Override
    public String getIbPoli() {
        throw new FieldNotMappedException("ibPoli");
    }

    @Override
    public void setIbPoli(String ibPoli) {
        throw new FieldNotMappedException("ibPoli");
    }

    @Override
    public String getIbPoliObj() {
        return getIbPoli();
    }

    @Override
    public void setIbPoliObj(String ibPoliObj) {
        setIbPoli(ibPoliObj);
    }

    @Override
    public String getIbRich() {
        throw new FieldNotMappedException("ibRich");
    }

    @Override
    public void setIbRich(String ibRich) {
        throw new FieldNotMappedException("ibRich");
    }

    @Override
    public String getIbRichObj() {
        return getIbRich();
    }

    @Override
    public void setIbRichObj(String ibRichObj) {
        setIbRich(ibRichObj);
    }

    @Override
    public String getIbTrchDiGar() {
        throw new FieldNotMappedException("ibTrchDiGar");
    }

    @Override
    public void setIbTrchDiGar(String ibTrchDiGar) {
        throw new FieldNotMappedException("ibTrchDiGar");
    }

    @Override
    public String getIbTrchDiGarObj() {
        return getIbTrchDiGar();
    }

    @Override
    public void setIbTrchDiGarObj(String ibTrchDiGarObj) {
        setIbTrchDiGar(ibTrchDiGarObj);
    }

    @Override
    public int getIdJob() {
        throw new FieldNotMappedException("idJob");
    }

    @Override
    public void setIdJob(int idJob) {
        throw new FieldNotMappedException("idJob");
    }

    @Override
    public Integer getIdJobObj() {
        return ((Integer)getIdJob());
    }

    @Override
    public void setIdJobObj(Integer idJobObj) {
        setIdJob(((int)idJobObj));
    }

    @Override
    public int getIdOgg() {
        throw new FieldNotMappedException("idOgg");
    }

    @Override
    public void setIdOgg(int idOgg) {
        throw new FieldNotMappedException("idOgg");
    }

    @Override
    public Integer getIdOggObj() {
        return ((Integer)getIdOgg());
    }

    @Override
    public void setIdOggObj(Integer idOggObj) {
        setIdOgg(((int)idOggObj));
    }

    @Override
    public int getIdRichCollg() {
        throw new FieldNotMappedException("idRichCollg");
    }

    @Override
    public void setIdRichCollg(int idRichCollg) {
        throw new FieldNotMappedException("idRichCollg");
    }

    @Override
    public Integer getIdRichCollgObj() {
        return ((Integer)getIdRichCollg());
    }

    @Override
    public void setIdRichCollgObj(Integer idRichCollgObj) {
        setIdRichCollg(((int)idRichCollgObj));
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return iabs0900.getIdsv0003().getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        iabs0900.getIdsv0003().setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public String getKeyOrdinamentoVchar() {
        throw new FieldNotMappedException("keyOrdinamentoVchar");
    }

    @Override
    public void setKeyOrdinamentoVchar(String keyOrdinamentoVchar) {
        throw new FieldNotMappedException("keyOrdinamentoVchar");
    }

    @Override
    public String getKeyOrdinamentoVcharObj() {
        return getKeyOrdinamentoVchar();
    }

    @Override
    public void setKeyOrdinamentoVcharObj(String keyOrdinamentoVcharObj) {
        setKeyOrdinamentoVchar(keyOrdinamentoVcharObj);
    }

    @Override
    public char getLdbv4511StatoElab00() {
        throw new FieldNotMappedException("ldbv4511StatoElab00");
    }

    @Override
    public void setLdbv4511StatoElab00(char ldbv4511StatoElab00) {
        throw new FieldNotMappedException("ldbv4511StatoElab00");
    }

    @Override
    public char getLdbv4511StatoElab01() {
        throw new FieldNotMappedException("ldbv4511StatoElab01");
    }

    @Override
    public void setLdbv4511StatoElab01(char ldbv4511StatoElab01) {
        throw new FieldNotMappedException("ldbv4511StatoElab01");
    }

    @Override
    public char getLdbv4511StatoElab02() {
        throw new FieldNotMappedException("ldbv4511StatoElab02");
    }

    @Override
    public void setLdbv4511StatoElab02(char ldbv4511StatoElab02) {
        throw new FieldNotMappedException("ldbv4511StatoElab02");
    }

    @Override
    public char getLdbv4511StatoElab03() {
        throw new FieldNotMappedException("ldbv4511StatoElab03");
    }

    @Override
    public void setLdbv4511StatoElab03(char ldbv4511StatoElab03) {
        throw new FieldNotMappedException("ldbv4511StatoElab03");
    }

    @Override
    public char getLdbv4511StatoElab04() {
        throw new FieldNotMappedException("ldbv4511StatoElab04");
    }

    @Override
    public void setLdbv4511StatoElab04(char ldbv4511StatoElab04) {
        throw new FieldNotMappedException("ldbv4511StatoElab04");
    }

    @Override
    public char getLdbv4511StatoElab05() {
        throw new FieldNotMappedException("ldbv4511StatoElab05");
    }

    @Override
    public void setLdbv4511StatoElab05(char ldbv4511StatoElab05) {
        throw new FieldNotMappedException("ldbv4511StatoElab05");
    }

    @Override
    public char getLdbv4511StatoElab06() {
        throw new FieldNotMappedException("ldbv4511StatoElab06");
    }

    @Override
    public void setLdbv4511StatoElab06(char ldbv4511StatoElab06) {
        throw new FieldNotMappedException("ldbv4511StatoElab06");
    }

    @Override
    public char getLdbv4511StatoElab07() {
        throw new FieldNotMappedException("ldbv4511StatoElab07");
    }

    @Override
    public void setLdbv4511StatoElab07(char ldbv4511StatoElab07) {
        throw new FieldNotMappedException("ldbv4511StatoElab07");
    }

    @Override
    public char getLdbv4511StatoElab08() {
        throw new FieldNotMappedException("ldbv4511StatoElab08");
    }

    @Override
    public void setLdbv4511StatoElab08(char ldbv4511StatoElab08) {
        throw new FieldNotMappedException("ldbv4511StatoElab08");
    }

    @Override
    public char getLdbv4511StatoElab09() {
        throw new FieldNotMappedException("ldbv4511StatoElab09");
    }

    @Override
    public void setLdbv4511StatoElab09(char ldbv4511StatoElab09) {
        throw new FieldNotMappedException("ldbv4511StatoElab09");
    }

    @Override
    public String getLdbv4511TpRich00() {
        throw new FieldNotMappedException("ldbv4511TpRich00");
    }

    @Override
    public void setLdbv4511TpRich00(String ldbv4511TpRich00) {
        throw new FieldNotMappedException("ldbv4511TpRich00");
    }

    @Override
    public String getLdbv4511TpRich01() {
        throw new FieldNotMappedException("ldbv4511TpRich01");
    }

    @Override
    public void setLdbv4511TpRich01(String ldbv4511TpRich01) {
        throw new FieldNotMappedException("ldbv4511TpRich01");
    }

    @Override
    public String getLdbv4511TpRich02() {
        throw new FieldNotMappedException("ldbv4511TpRich02");
    }

    @Override
    public void setLdbv4511TpRich02(String ldbv4511TpRich02) {
        throw new FieldNotMappedException("ldbv4511TpRich02");
    }

    @Override
    public String getLdbv4511TpRich03() {
        throw new FieldNotMappedException("ldbv4511TpRich03");
    }

    @Override
    public void setLdbv4511TpRich03(String ldbv4511TpRich03) {
        throw new FieldNotMappedException("ldbv4511TpRich03");
    }

    @Override
    public String getLdbv4511TpRich04() {
        throw new FieldNotMappedException("ldbv4511TpRich04");
    }

    @Override
    public void setLdbv4511TpRich04(String ldbv4511TpRich04) {
        throw new FieldNotMappedException("ldbv4511TpRich04");
    }

    @Override
    public String getLdbv4511TpRich05() {
        throw new FieldNotMappedException("ldbv4511TpRich05");
    }

    @Override
    public void setLdbv4511TpRich05(String ldbv4511TpRich05) {
        throw new FieldNotMappedException("ldbv4511TpRich05");
    }

    @Override
    public String getLdbv4511TpRich06() {
        throw new FieldNotMappedException("ldbv4511TpRich06");
    }

    @Override
    public void setLdbv4511TpRich06(String ldbv4511TpRich06) {
        throw new FieldNotMappedException("ldbv4511TpRich06");
    }

    @Override
    public String getLdbv4511TpRich07() {
        throw new FieldNotMappedException("ldbv4511TpRich07");
    }

    @Override
    public void setLdbv4511TpRich07(String ldbv4511TpRich07) {
        throw new FieldNotMappedException("ldbv4511TpRich07");
    }

    @Override
    public String getLdbv4511TpRich08() {
        throw new FieldNotMappedException("ldbv4511TpRich08");
    }

    @Override
    public void setLdbv4511TpRich08(String ldbv4511TpRich08) {
        throw new FieldNotMappedException("ldbv4511TpRich08");
    }

    @Override
    public String getLdbv4511TpRich09() {
        throw new FieldNotMappedException("ldbv4511TpRich09");
    }

    @Override
    public void setLdbv4511TpRich09(String ldbv4511TpRich09) {
        throw new FieldNotMappedException("ldbv4511TpRich09");
    }

    @Override
    public String getPrenotazione() {
        return iabs0900.getWs().getPrenotazione();
    }

    @Override
    public void setPrenotazione(String prenotazione) {
        iabs0900.getWs().setPrenotazione(prenotazione);
    }

    @Override
    public String getRamoBila() {
        throw new FieldNotMappedException("ramoBila");
    }

    @Override
    public void setRamoBila(String ramoBila) {
        throw new FieldNotMappedException("ramoBila");
    }

    @Override
    public String getRamoBilaObj() {
        return getRamoBila();
    }

    @Override
    public void setRamoBilaObj(String ramoBilaObj) {
        setRamoBila(ramoBilaObj);
    }

    @Override
    public String getRicCodMacrofunct() {
        throw new FieldNotMappedException("ricCodMacrofunct");
    }

    @Override
    public void setRicCodMacrofunct(String ricCodMacrofunct) {
        throw new FieldNotMappedException("ricCodMacrofunct");
    }

    @Override
    public int getRicIdBatch() {
        return iabs0900.getRich().getRicIdBatchRP().getRicIdBatch();
    }

    @Override
    public void setRicIdBatch(int ricIdBatch) {
        iabs0900.getRich().getRicIdBatchRP().setRicIdBatch(ricIdBatch);
    }

    @Override
    public Integer getRicIdBatchObj() {
        return ((Integer)getRicIdBatch());
    }

    @Override
    public void setRicIdBatchObj(Integer ricIdBatchObj) {
        setRicIdBatch(((int)ricIdBatchObj));
    }

    @Override
    public int getRicIdRich() {
        throw new FieldNotMappedException("ricIdRich");
    }

    @Override
    public void setRicIdRich(int ricIdRich) {
        throw new FieldNotMappedException("ricIdRich");
    }

    @Override
    public int getRicTpMovi() {
        return iabs0900.getRich().getRicTpMovi();
    }

    @Override
    public void setRicTpMovi(int ricTpMovi) {
        iabs0900.getRich().setRicTpMovi(ricTpMovi);
    }

    @Override
    public String getTpCalcRis() {
        throw new FieldNotMappedException("tpCalcRis");
    }

    @Override
    public void setTpCalcRis(String tpCalcRis) {
        throw new FieldNotMappedException("tpCalcRis");
    }

    @Override
    public String getTpCalcRisObj() {
        return getTpCalcRis();
    }

    @Override
    public void setTpCalcRisObj(String tpCalcRisObj) {
        setTpCalcRis(tpCalcRisObj);
    }

    @Override
    public String getTpFrmAssva() {
        throw new FieldNotMappedException("tpFrmAssva");
    }

    @Override
    public void setTpFrmAssva(String tpFrmAssva) {
        throw new FieldNotMappedException("tpFrmAssva");
    }

    @Override
    public String getTpFrmAssvaObj() {
        return getTpFrmAssva();
    }

    @Override
    public void setTpFrmAssvaObj(String tpFrmAssvaObj) {
        setTpFrmAssva(tpFrmAssvaObj);
    }

    @Override
    public String getTpOgg() {
        throw new FieldNotMappedException("tpOgg");
    }

    @Override
    public void setTpOgg(String tpOgg) {
        throw new FieldNotMappedException("tpOgg");
    }

    @Override
    public String getTpOggObj() {
        return getTpOgg();
    }

    @Override
    public void setTpOggObj(String tpOggObj) {
        setTpOgg(tpOggObj);
    }

    @Override
    public String getTpRamoBila() {
        throw new FieldNotMappedException("tpRamoBila");
    }

    @Override
    public void setTpRamoBila(String tpRamoBila) {
        throw new FieldNotMappedException("tpRamoBila");
    }

    @Override
    public String getTpRamoBilaObj() {
        return getTpRamoBila();
    }

    @Override
    public void setTpRamoBilaObj(String tpRamoBilaObj) {
        setTpRamoBila(tpRamoBilaObj);
    }

    @Override
    public String getTpRich() {
        throw new FieldNotMappedException("tpRich");
    }

    @Override
    public void setTpRich(String tpRich) {
        throw new FieldNotMappedException("tpRich");
    }

    @Override
    public long getTsEffEsecRich() {
        throw new FieldNotMappedException("tsEffEsecRich");
    }

    @Override
    public void setTsEffEsecRich(long tsEffEsecRich) {
        throw new FieldNotMappedException("tsEffEsecRich");
    }

    @Override
    public Long getTsEffEsecRichObj() {
        return ((Long)getTsEffEsecRich());
    }

    @Override
    public void setTsEffEsecRichObj(Long tsEffEsecRichObj) {
        setTsEffEsecRich(((long)tsEffEsecRichObj));
    }
}

package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.DettRichDao;
import it.accenture.jnais.commons.data.dao.RichDao;
import it.accenture.jnais.commons.data.to.IDettRich;
import it.accenture.jnais.commons.data.to.IRich;
import it.accenture.jnais.copy.DettRich;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Iabv0002TipoOrderBy;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.enums.Idsv0003TipologiaOperazione;
import it.accenture.jnais.ws.enums.Lccc006TrattDati;
import it.accenture.jnais.ws.Iabs0900Data;
import it.accenture.jnais.ws.Iabv0002;
import it.accenture.jnais.ws.Iabv0003;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.RicIdBatch;
import it.accenture.jnais.ws.redefines.RicIdJob;
import it.accenture.jnais.ws.redefines.RicIdOgg;
import it.accenture.jnais.ws.redefines.RicIdRichCollg;
import it.accenture.jnais.ws.redefines.RicTsEffEsecRich;
import it.accenture.jnais.ws.RichIabs0900;

/**Original name: IABS0900<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : SERVIZIO ESTRAZIONE OCCORRENZE              *
 *                    DA TABELLE GUIDA RICHIAMATO DAL BATCH EXEC. *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Iabs0900 extends Program implements IRich, IDettRich {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private RichIabs09001 richIabs09001 = new RichIabs09001(this);
    private RichDao richDao = new RichDao(dbAccessStatus);
    private DettRichDao dettRichDao = new DettRichDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Iabs0900Data ws = new Iabs0900Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: IABV0002
    private Iabv0002 iabv0002;
    //Original name: RICH
    private RichIabs0900 rich;
    //Original name: IABV0003
    private Iabv0003 iabv0003;

    //==== METHODS ====
    /**Original name: PROGRAM_IABS0900_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Iabv0002 iabv0002, RichIabs0900 rich, Iabv0003 iabv0003) {
        this.idsv0003 = idsv0003;
        this.iabv0002 = iabv0002;
        this.rich = rich;
        this.iabv0003 = iabv0003;
        // COB_CODE: PERFORM A000-INIZIO                 THRU A000-EX.
        a000Inizio();
        // COB_CODE: PERFORM A060-CNTL-INPUT             THRU A060-EX.
        a060CntlInput();
        // COB_CODE: PERFORM A300-ELABORA                THRU A300-EX.
        a300Elabora();
        // COB_CODE: PERFORM A400-FINE                   THRU A400-EX.
        a400Fine();
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Iabs0900 getInstance() {
        return ((Iabs0900)Programs.getInstance(Iabs0900.class));
    }

    /**Original name: A000-INIZIO<br>
	 * <pre>*****************************************************************</pre>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IABS0900'              TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IABS0900");
        // COB_CODE: MOVE 'RICH'                  TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("RICH");
        // COB_CODE: MOVE '00'                    TO IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                  TO IDSV0003-SQLCODE
        //                                           IDSV0003-NUM-RIGHE-LETTE
        //                                           WK-ID-RICH.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        ws.setWkIdRich(0);
        // COB_CODE: MOVE SPACES                  TO IDSV0003-DESCRIZ-ERR-DB2
        //                                           IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: SET ACCESSO-STD              TO TRUE
        ws.getFlagAccesso().setStd();
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A060-CNTL-INPUT<br>
	 * <pre>*****************************************************************</pre>*/
    private void a060CntlInput() {
        // COB_CODE: IF IDSV0003-WHERE-CONDITION-02
        //              END-IF
        //           END-IF.
        if (idsv0003.getTipologiaOperazione().isIdsv0003WhereCondition02()) {
            // COB_CODE: IF RIC-COD-MACROFUNCT NOT = SPACES     AND
            //              RIC-COD-MACROFUNCT NOT = LOW-VALUE  AND
            //              RIC-COD-MACROFUNCT NOT = HIGH-VALUE
            //              SET ACCESSO-MCRFNCT          TO TRUE
            //           END-IF
            if (!Characters.EQ_SPACE.test(rich.getRicCodMacrofunct()) && !Characters.EQ_LOW.test(rich.getRicCodMacrofunctFormatted()) && !Characters.EQ_HIGH.test(rich.getRicCodMacrofunctFormatted())) {
                // COB_CODE: SET ACCESSO-MCRFNCT          TO TRUE
                ws.getFlagAccesso().setMcrfnct();
            }
            // COB_CODE: IF RIC-TP-MOVI IS NUMERIC AND
            //              RIC-TP-MOVI NOT = ZERO
            //              END-IF
            //           END-IF
            if (Functions.isNumber(rich.getRicTpMovi()) && rich.getRicTpMovi() != 0) {
                // COB_CODE: IF ACCESSO-MCRFNCT
                //              SET ACCESSO-MCRFNCT-TPMV  TO TRUE
                //           ELSE
                //              SET ACCESSO-TPMV          TO TRUE
                //           END-IF
                if (ws.getFlagAccesso().isMcrfnct()) {
                    // COB_CODE: SET ACCESSO-MCRFNCT-TPMV  TO TRUE
                    ws.getFlagAccesso().setMcrfnctTpmv();
                }
                else {
                    // COB_CODE: SET ACCESSO-TPMV          TO TRUE
                    ws.getFlagAccesso().setTpmv();
                }
            }
        }
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>
	 * <pre>*****************************************************************</pre>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-SELECT         OR
                //              IDSV0003-FETCH-FIRST    OR
                //              IDSV0003-FETCH-NEXT     OR
                //              IDSV0003-UPDATE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isUpdate()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A300-ELABORA<br>
	 * <pre>*****************************************************************</pre>*/
    private void a300Elabora() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-WHERE-CONDITION-01
        //                   PERFORM SC01-SELECTION-CURSOR-01 THRU SC01-EX
        //              WHEN IDSV0003-WHERE-CONDITION-02
        //                   PERFORM SC02-SELECTION-CURSOR-02 THRU SC02-EX
        //              WHEN OTHER
        //                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
        //           END-EVALUATE.
        switch (idsv0003.getTipologiaOperazione().getTipologiaOperazione()) {

            case Idsv0003TipologiaOperazione.CONDITION01:// COB_CODE: PERFORM SC01-SELECTION-CURSOR-01 THRU SC01-EX
                sc01SelectionCursor01();
                break;

            case Idsv0003TipologiaOperazione.CONDITION02:// COB_CODE: PERFORM SC02-SELECTION-CURSOR-02 THRU SC02-EX
                sc02SelectionCursor02();
                break;

            default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidLevelOper();
                break;
        }
    }

    /**Original name: A400-FINE<br>
	 * <pre>*****************************************************************</pre>*/
    private void a400Fine() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: D101-DCL-CUR-IDR-STD-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void d101DclCurIdrStdSc01() {
    // COB_CODE: EXEC SQL
    //              DECLARE CUR-RIC-IDR-STD CURSOR WITH HOLD FOR
    //              SELECT
    //                ID_RICH
    //                ,COD_COMP_ANIA
    //                ,TP_RICH
    //                ,COD_MACROFUNCT
    //                ,TP_MOVI
    //                ,IB_RICH
    //                ,DT_EFF
    //                ,DT_RGSTRZ_RICH
    //                ,DT_PERV_RICH
    //                ,DT_ESEC_RICH
    //                ,TS_EFF_ESEC_RICH
    //                ,ID_OGG
    //                ,TP_OGG
    //                ,IB_POLI
    //                ,IB_ADES
    //                ,IB_GAR
    //                ,IB_TRCH_DI_GAR
    //                ,ID_BATCH
    //                ,ID_JOB
    //                ,FL_SIMULAZIONE
    //                ,KEY_ORDINAMENTO
    //                ,ID_RICH_COLLG
    //                ,TP_RAMO_BILA
    //                ,TP_FRM_ASSVA
    //                ,TP_CALC_RIS
    //                ,DS_OPER_SQL
    //                ,DS_VER
    //                ,DS_TS_CPTZ
    //                ,DS_UTENTE
    //                ,DS_STATO_ELAB
    //                ,RAMO_BILA
    //              FROM RICH
    //              WHERE    TP_RICH       =  :PRENOTAZIONE
    //                   AND COD_COMP_ANIA =  :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                   AND ID_BATCH      =  :RIC-ID-BATCH
    //                   AND DT_ESEC_RICH <=  CURRENT DATE
    //                   AND DS_STATO_ELAB IN (
    //                                         :IABV0002-STATE-01,
    //                                         :IABV0002-STATE-02,
    //                                         :IABV0002-STATE-03,
    //                                         :IABV0002-STATE-04,
    //                                         :IABV0002-STATE-05,
    //                                         :IABV0002-STATE-06,
    //                                         :IABV0002-STATE-07,
    //                                         :IABV0002-STATE-08,
    //                                         :IABV0002-STATE-09,
    //                                         :IABV0002-STATE-10
    //                                         )
    //              ORDER BY ID_RICH ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: D102-DCL-CUR-IDJ-MCRFNCT-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void d102DclCurIdjMcrfnctSc01() {
    // COB_CODE: EXEC SQL
    //              DECLARE CUR-RIC-IDR-MF CURSOR WITH HOLD FOR
    //              SELECT
    //                ID_RICH
    //                ,COD_COMP_ANIA
    //                ,TP_RICH
    //                ,COD_MACROFUNCT
    //                ,TP_MOVI
    //                ,IB_RICH
    //                ,DT_EFF
    //                ,DT_RGSTRZ_RICH
    //                ,DT_PERV_RICH
    //                ,DT_ESEC_RICH
    //                ,TS_EFF_ESEC_RICH
    //                ,ID_OGG
    //                ,TP_OGG
    //                ,IB_POLI
    //                ,IB_ADES
    //                ,IB_GAR
    //                ,IB_TRCH_DI_GAR
    //                ,ID_BATCH
    //                ,ID_JOB
    //                ,FL_SIMULAZIONE
    //                ,KEY_ORDINAMENTO
    //                ,ID_RICH_COLLG
    //                ,TP_RAMO_BILA
    //                ,TP_FRM_ASSVA
    //                ,TP_CALC_RIS
    //                ,DS_OPER_SQL
    //                ,DS_VER
    //                ,DS_TS_CPTZ
    //                ,DS_UTENTE
    //                ,DS_STATO_ELAB
    //                ,RAMO_BILA
    //              FROM RICH
    //              WHERE    TP_RICH        =  :PRENOTAZIONE
    //                   AND COD_COMP_ANIA  =  :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                   AND ID_BATCH       =  :RIC-ID-BATCH
    //                   AND COD_MACROFUNCT =  :RIC-COD-MACROFUNCT
    //                   AND DT_ESEC_RICH <=  CURRENT DATE
    //                   AND DS_STATO_ELAB IN (
    //                                         :IABV0002-STATE-01,
    //                                         :IABV0002-STATE-02,
    //                                         :IABV0002-STATE-03,
    //                                         :IABV0002-STATE-04,
    //                                         :IABV0002-STATE-05,
    //                                         :IABV0002-STATE-06,
    //                                         :IABV0002-STATE-07,
    //                                         :IABV0002-STATE-08,
    //                                         :IABV0002-STATE-09,
    //                                         :IABV0002-STATE-10
    //                                         )
    //              ORDER BY ID_RICH ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: D103-DCL-CUR-IDR-TPMV-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void d103DclCurIdrTpmvSc01() {
    // COB_CODE: EXEC SQL
    //              DECLARE CUR-RIC-IDR-TM CURSOR WITH HOLD FOR
    //              SELECT
    //                ID_RICH
    //                ,COD_COMP_ANIA
    //                ,TP_RICH
    //                ,COD_MACROFUNCT
    //                ,TP_MOVI
    //                ,IB_RICH
    //                ,DT_EFF
    //                ,DT_RGSTRZ_RICH
    //                ,DT_PERV_RICH
    //                ,DT_ESEC_RICH
    //                ,TS_EFF_ESEC_RICH
    //                ,ID_OGG
    //                ,TP_OGG
    //                ,IB_POLI
    //                ,IB_ADES
    //                ,IB_GAR
    //                ,IB_TRCH_DI_GAR
    //                ,ID_BATCH
    //                ,ID_JOB
    //                ,FL_SIMULAZIONE
    //                ,KEY_ORDINAMENTO
    //                ,ID_RICH_COLLG
    //                ,TP_RAMO_BILA
    //                ,TP_FRM_ASSVA
    //                ,TP_CALC_RIS
    //                ,DS_OPER_SQL
    //                ,DS_VER
    //                ,DS_TS_CPTZ
    //                ,DS_UTENTE
    //                ,DS_STATO_ELAB
    //                ,RAMO_BILA
    //              FROM RICH
    //              WHERE    TP_RICH       =  :PRENOTAZIONE
    //                   AND COD_COMP_ANIA =  :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                   AND ID_BATCH      =  :RIC-ID-BATCH
    //                   AND TP_MOVI       =  :RIC-TP-MOVI
    //                   AND DT_ESEC_RICH <=  CURRENT DATE
    //                   AND DS_STATO_ELAB IN (
    //                                         :IABV0002-STATE-01,
    //                                         :IABV0002-STATE-02,
    //                                         :IABV0002-STATE-03,
    //                                         :IABV0002-STATE-04,
    //                                         :IABV0002-STATE-05,
    //                                         :IABV0002-STATE-06,
    //                                         :IABV0002-STATE-07,
    //                                         :IABV0002-STATE-08,
    //                                         :IABV0002-STATE-09,
    //                                         :IABV0002-STATE-10
    //                                         )
    //              ORDER BY ID_RICH ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: D104-DCL-CUR-IDJ-MCRFNCT-TPMV<br>
	 * <pre>*****************************************************************</pre>*/
    private void d104DclCurIdjMcrfnctTpmv() {
    // COB_CODE: EXEC SQL
    //              DECLARE CUR-RIC-IDR-MF-TM CURSOR WITH HOLD FOR
    //              SELECT
    //                ID_RICH
    //                ,COD_COMP_ANIA
    //                ,TP_RICH
    //                ,COD_MACROFUNCT
    //                ,TP_MOVI
    //                ,IB_RICH
    //                ,DT_EFF
    //                ,DT_RGSTRZ_RICH
    //                ,DT_PERV_RICH
    //                ,DT_ESEC_RICH
    //                ,TS_EFF_ESEC_RICH
    //                ,ID_OGG
    //                ,TP_OGG
    //                ,IB_POLI
    //                ,IB_ADES
    //                ,IB_GAR
    //                ,IB_TRCH_DI_GAR
    //                ,ID_BATCH
    //                ,ID_JOB
    //                ,FL_SIMULAZIONE
    //                ,KEY_ORDINAMENTO
    //                ,ID_RICH_COLLG
    //                ,TP_RAMO_BILA
    //                ,TP_FRM_ASSVA
    //                ,TP_CALC_RIS
    //                ,DS_OPER_SQL
    //                ,DS_VER
    //                ,DS_TS_CPTZ
    //                ,DS_UTENTE
    //                ,DS_STATO_ELAB
    //                ,RAMO_BILA
    //              FROM RICH
    //              WHERE    TP_RICH        =  :PRENOTAZIONE
    //                   AND COD_COMP_ANIA  =  :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                   AND ID_BATCH       =  :RIC-ID-BATCH
    //                   AND TP_MOVI        =  :RIC-TP-MOVI
    //                   AND COD_MACROFUNCT =  :RIC-COD-MACROFUNCT
    //                   AND DT_ESEC_RICH  <=  CURRENT DATE
    //                   AND DS_STATO_ELAB IN (
    //                                         :IABV0002-STATE-01,
    //                                         :IABV0002-STATE-02,
    //                                         :IABV0002-STATE-03,
    //                                         :IABV0002-STATE-04,
    //                                         :IABV0002-STATE-05,
    //                                         :IABV0002-STATE-06,
    //                                         :IABV0002-STATE-07,
    //                                         :IABV0002-STATE-08,
    //                                         :IABV0002-STATE-09,
    //                                         :IABV0002-STATE-10
    //                                         )
    //              ORDER BY ID_RICH ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A305-DECLARE-CURSOR-IDR-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a305DeclareCursorIdrSc01() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN ACCESSO-STD
        //                 END-EXEC
        //              WHEN ACCESSO-MCRFNCT
        //                 END-EXEC
        //              WHEN ACCESSO-TPMV
        //                 END-EXEC
        //              WHEN ACCESSO-MCRFNCT-TPMV
        //                 END-EXEC
        //           END-EVALUATE.
        switch (ws.getFlagAccesso().getFlagModCalcPreP()) {

            case Lccc006TrattDati.AMMESSO_MOD:// COB_CODE: PERFORM D101-DCL-CUR-IDR-STD-SC01    THRU D101-SC01-EX
                d101DclCurIdrStdSc01();
                // COB_CODE: EXEC SQL
                //                OPEN CUR-RIC-IDR-STD
                //           END-EXEC
                richDao.openCurRicIdrStd(this);
                break;

            case Lccc006TrattDati.AMMESSO_NMOD:// COB_CODE: PERFORM D102-DCL-CUR-IDJ-MCRFNCT-SC01 THRU D102-SC01-EX
                d102DclCurIdjMcrfnctSc01();
                // COB_CODE: EXEC SQL
                //                OPEN CUR-RIC-IDR-MF
                //           END-EXEC
                richDao.openCurRicIdrMf(this);
                break;

            case Lccc006TrattDati.NON_AMMESSO:// COB_CODE: PERFORM D103-DCL-CUR-IDR-TPMV-SC01  THRU D103-SC01-EX
                d103DclCurIdrTpmvSc01();
                // COB_CODE: EXEC SQL
                //                OPEN CUR-RIC-IDR-TM
                //           END-EXEC
                richDao.openCurRicIdrTm(this);
                break;

            case Lccc006TrattDati.OBBLIGATORIO:// COB_CODE: PERFORM D104-DCL-CUR-IDJ-MCRFNCT-TPMV THRU D104-SC01-EX
                d104DclCurIdjMcrfnctTpmv();
                // COB_CODE: EXEC SQL
                //                OPEN CUR-RIC-IDR-MF-TM
                //           END-EXEC
                richDao.openCurRicIdrMfTm(this);
                break;

            default:break;
        }
    }

    /**Original name: A310-SELECT-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a310SelectSc01() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN ACCESSO-STD
        //                 PERFORM A311-SELECT-STD-SC01     THRU A311-SC01-EX
        //              WHEN ACCESSO-MCRFNCT
        //                 PERFORM A312-SELECT-MCRFNCT-SC01 THRU A312-SC01-EX
        //              WHEN ACCESSO-TPMV
        //                 PERFORM A313-SELECT-TPMV-SC01    THRU A313-SC01-EX
        //              WHEN ACCESSO-MCRFNCT-TPMV
        //                                                  THRU A314-SC01-EX
        //           END-EVALUATE.
        switch (ws.getFlagAccesso().getFlagModCalcPreP()) {

            case Lccc006TrattDati.AMMESSO_MOD:// COB_CODE: PERFORM A311-SELECT-STD-SC01     THRU A311-SC01-EX
                a311SelectStdSc01();
                break;

            case Lccc006TrattDati.AMMESSO_NMOD:// COB_CODE: PERFORM A312-SELECT-MCRFNCT-SC01 THRU A312-SC01-EX
                a312SelectMcrfnctSc01();
                break;

            case Lccc006TrattDati.NON_AMMESSO:// COB_CODE: PERFORM A313-SELECT-TPMV-SC01    THRU A313-SC01-EX
                a313SelectTpmvSc01();
                break;

            case Lccc006TrattDati.OBBLIGATORIO:// COB_CODE: PERFORM A314-SELECT-MCRFNCT-TPMV-SC01
                //                                            THRU A314-SC01-EX
                a314SelectMcrfnctTpmvSc01();
                break;

            default:break;
        }
    }

    /**Original name: A311-SELECT-STD-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a311SelectStdSc01() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RICH
        //                ,COD_COMP_ANIA
        //                ,TP_RICH
        //                ,COD_MACROFUNCT
        //                ,TP_MOVI
        //                ,IB_RICH
        //                ,DT_EFF
        //                ,DT_RGSTRZ_RICH
        //                ,DT_PERV_RICH
        //                ,DT_ESEC_RICH
        //                ,TS_EFF_ESEC_RICH
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,IB_POLI
        //                ,IB_ADES
        //                ,IB_GAR
        //                ,IB_TRCH_DI_GAR
        //                ,ID_BATCH
        //                ,ID_JOB
        //                ,FL_SIMULAZIONE
        //                ,KEY_ORDINAMENTO
        //                ,ID_RICH_COLLG
        //                ,TP_RAMO_BILA
        //                ,TP_FRM_ASSVA
        //                ,TP_CALC_RIS
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,RAMO_BILA
        //             INTO
        //                :RIC-ID-RICH
        //               ,:RIC-COD-COMP-ANIA
        //               ,:RIC-TP-RICH
        //               ,:RIC-COD-MACROFUNCT
        //               ,:RIC-TP-MOVI
        //               ,:RIC-IB-RICH
        //                :IND-RIC-IB-RICH
        //               ,:RIC-DT-EFF-DB
        //               ,:RIC-DT-RGSTRZ-RICH-DB
        //               ,:RIC-DT-PERV-RICH-DB
        //               ,:RIC-DT-ESEC-RICH-DB
        //               ,:RIC-TS-EFF-ESEC-RICH
        //                :IND-RIC-TS-EFF-ESEC-RICH
        //               ,:RIC-ID-OGG
        //                :IND-RIC-ID-OGG
        //               ,:RIC-TP-OGG
        //                :IND-RIC-TP-OGG
        //               ,:RIC-IB-POLI
        //                :IND-RIC-IB-POLI
        //               ,:RIC-IB-ADES
        //                :IND-RIC-IB-ADES
        //               ,:RIC-IB-GAR
        //                :IND-RIC-IB-GAR
        //               ,:RIC-IB-TRCH-DI-GAR
        //                :IND-RIC-IB-TRCH-DI-GAR
        //               ,:RIC-ID-BATCH
        //                :IND-RIC-ID-BATCH
        //               ,:RIC-ID-JOB
        //                :IND-RIC-ID-JOB
        //               ,:RIC-FL-SIMULAZIONE
        //                :IND-RIC-FL-SIMULAZIONE
        //               ,:RIC-KEY-ORDINAMENTO-VCHAR
        //                :IND-RIC-KEY-ORDINAMENTO
        //               ,:RIC-ID-RICH-COLLG
        //                :IND-RIC-ID-RICH-COLLG
        //               ,:RIC-TP-RAMO-BILA
        //                :IND-RIC-TP-RAMO-BILA
        //               ,:RIC-TP-FRM-ASSVA
        //                :IND-RIC-TP-FRM-ASSVA
        //               ,:RIC-TP-CALC-RIS
        //                :IND-RIC-TP-CALC-RIS
        //               ,:RIC-DS-OPER-SQL
        //               ,:RIC-DS-VER
        //               ,:RIC-DS-TS-CPTZ
        //               ,:RIC-DS-UTENTE
        //               ,:RIC-DS-STATO-ELAB
        //               ,:RIC-RAMO-BILA
        //                :IND-RIC-RAMO-BILA
        //             FROM RICH
        //             WHERE  TP_RICH               = :PRENOTAZIONE
        //               AND  ID_BATCH              = :RIC-ID-BATCH
        //               AND  COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //               AND DT_ESEC_RICH          <=  CURRENT DATE
        //               AND  DS_STATO_ELAB IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                     )
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        richDao.selectRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A312-SELECT-MCRFNCT-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a312SelectMcrfnctSc01() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RICH
        //                ,COD_COMP_ANIA
        //                ,TP_RICH
        //                ,COD_MACROFUNCT
        //                ,TP_MOVI
        //                ,IB_RICH
        //                ,DT_EFF
        //                ,DT_RGSTRZ_RICH
        //                ,DT_PERV_RICH
        //                ,DT_ESEC_RICH
        //                ,TS_EFF_ESEC_RICH
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,IB_POLI
        //                ,IB_ADES
        //                ,IB_GAR
        //                ,IB_TRCH_DI_GAR
        //                ,ID_BATCH
        //                ,ID_JOB
        //                ,FL_SIMULAZIONE
        //                ,KEY_ORDINAMENTO
        //                ,ID_RICH_COLLG
        //                ,TP_RAMO_BILA
        //                ,TP_FRM_ASSVA
        //                ,TP_CALC_RIS
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,RAMO_BILA
        //             INTO
        //                :RIC-ID-RICH
        //               ,:RIC-COD-COMP-ANIA
        //               ,:RIC-TP-RICH
        //               ,:RIC-COD-MACROFUNCT
        //               ,:RIC-TP-MOVI
        //               ,:RIC-IB-RICH
        //                :IND-RIC-IB-RICH
        //               ,:RIC-DT-EFF-DB
        //               ,:RIC-DT-RGSTRZ-RICH-DB
        //               ,:RIC-DT-PERV-RICH-DB
        //               ,:RIC-DT-ESEC-RICH-DB
        //               ,:RIC-TS-EFF-ESEC-RICH
        //                :IND-RIC-TS-EFF-ESEC-RICH
        //               ,:RIC-ID-OGG
        //                :IND-RIC-ID-OGG
        //               ,:RIC-TP-OGG
        //                :IND-RIC-TP-OGG
        //               ,:RIC-IB-POLI
        //                :IND-RIC-IB-POLI
        //               ,:RIC-IB-ADES
        //                :IND-RIC-IB-ADES
        //               ,:RIC-IB-GAR
        //                :IND-RIC-IB-GAR
        //               ,:RIC-IB-TRCH-DI-GAR
        //                :IND-RIC-IB-TRCH-DI-GAR
        //               ,:RIC-ID-BATCH
        //                :IND-RIC-ID-BATCH
        //               ,:RIC-ID-JOB
        //                :IND-RIC-ID-JOB
        //               ,:RIC-FL-SIMULAZIONE
        //                :IND-RIC-FL-SIMULAZIONE
        //               ,:RIC-KEY-ORDINAMENTO-VCHAR
        //                :IND-RIC-KEY-ORDINAMENTO
        //               ,:RIC-ID-RICH-COLLG
        //                :IND-RIC-ID-RICH-COLLG
        //               ,:RIC-TP-RAMO-BILA
        //                :IND-RIC-TP-RAMO-BILA
        //               ,:RIC-TP-FRM-ASSVA
        //                :IND-RIC-TP-FRM-ASSVA
        //               ,:RIC-TP-CALC-RIS
        //                :IND-RIC-TP-CALC-RIS
        //               ,:RIC-DS-OPER-SQL
        //               ,:RIC-DS-VER
        //               ,:RIC-DS-TS-CPTZ
        //               ,:RIC-DS-UTENTE
        //               ,:RIC-DS-STATO-ELAB
        //               ,:RIC-RAMO-BILA
        //                :IND-RIC-RAMO-BILA
        //             FROM RICH
        //             WHERE  TP_RICH               = :PRENOTAZIONE
        //               AND  ID_BATCH              = :RIC-ID-BATCH
        //               AND  COD_MACROFUNCT        = :RIC-COD-MACROFUNCT
        //               AND  COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //               AND DT_ESEC_RICH          <=  CURRENT DATE
        //               AND  DS_STATO_ELAB IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                      )
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        richDao.selectRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A313-SELECT-TPMV-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a313SelectTpmvSc01() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RICH
        //                ,COD_COMP_ANIA
        //                ,TP_RICH
        //                ,COD_MACROFUNCT
        //                ,TP_MOVI
        //                ,IB_RICH
        //                ,DT_EFF
        //                ,DT_RGSTRZ_RICH
        //                ,DT_PERV_RICH
        //                ,DT_ESEC_RICH
        //                ,TS_EFF_ESEC_RICH
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,IB_POLI
        //                ,IB_ADES
        //                ,IB_GAR
        //                ,IB_TRCH_DI_GAR
        //                ,ID_BATCH
        //                ,ID_JOB
        //                ,FL_SIMULAZIONE
        //                ,KEY_ORDINAMENTO
        //                ,ID_RICH_COLLG
        //                ,TP_RAMO_BILA
        //                ,TP_FRM_ASSVA
        //                ,TP_CALC_RIS
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,RAMO_BILA
        //             INTO
        //                :RIC-ID-RICH
        //               ,:RIC-COD-COMP-ANIA
        //               ,:RIC-TP-RICH
        //               ,:RIC-COD-MACROFUNCT
        //               ,:RIC-TP-MOVI
        //               ,:RIC-IB-RICH
        //                :IND-RIC-IB-RICH
        //               ,:RIC-DT-EFF-DB
        //               ,:RIC-DT-RGSTRZ-RICH-DB
        //               ,:RIC-DT-PERV-RICH-DB
        //               ,:RIC-DT-ESEC-RICH-DB
        //               ,:RIC-TS-EFF-ESEC-RICH
        //                :IND-RIC-TS-EFF-ESEC-RICH
        //               ,:RIC-ID-OGG
        //                :IND-RIC-ID-OGG
        //               ,:RIC-TP-OGG
        //                :IND-RIC-TP-OGG
        //               ,:RIC-IB-POLI
        //                :IND-RIC-IB-POLI
        //               ,:RIC-IB-ADES
        //                :IND-RIC-IB-ADES
        //               ,:RIC-IB-GAR
        //                :IND-RIC-IB-GAR
        //               ,:RIC-IB-TRCH-DI-GAR
        //                :IND-RIC-IB-TRCH-DI-GAR
        //               ,:RIC-ID-BATCH
        //                :IND-RIC-ID-BATCH
        //               ,:RIC-ID-JOB
        //                :IND-RIC-ID-JOB
        //               ,:RIC-FL-SIMULAZIONE
        //                :IND-RIC-FL-SIMULAZIONE
        //               ,:RIC-KEY-ORDINAMENTO-VCHAR
        //                :IND-RIC-KEY-ORDINAMENTO
        //               ,:RIC-ID-RICH-COLLG
        //                :IND-RIC-ID-RICH-COLLG
        //               ,:RIC-TP-RAMO-BILA
        //                :IND-RIC-TP-RAMO-BILA
        //               ,:RIC-TP-FRM-ASSVA
        //                :IND-RIC-TP-FRM-ASSVA
        //               ,:RIC-TP-CALC-RIS
        //                :IND-RIC-TP-CALC-RIS
        //               ,:RIC-DS-OPER-SQL
        //               ,:RIC-DS-VER
        //               ,:RIC-DS-TS-CPTZ
        //               ,:RIC-DS-UTENTE
        //               ,:RIC-DS-STATO-ELAB
        //               ,:RIC-RAMO-BILA
        //                :IND-RIC-RAMO-BILA
        //             FROM RICH
        //             WHERE  TP_RICH               = :PRENOTAZIONE
        //               AND  ID_BATCH              = :RIC-ID-BATCH
        //               AND  TP_MOVI               = :RIC-TP-MOVI
        //               AND  COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //               AND DT_ESEC_RICH          <=  CURRENT DATE
        //               AND  DS_STATO_ELAB IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                      )
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        richDao.selectRec2(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A314-SELECT-MCRFNCT-TPMV-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a314SelectMcrfnctTpmvSc01() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RICH
        //                ,COD_COMP_ANIA
        //                ,TP_RICH
        //                ,COD_MACROFUNCT
        //                ,TP_MOVI
        //                ,IB_RICH
        //                ,DT_EFF
        //                ,DT_RGSTRZ_RICH
        //                ,DT_PERV_RICH
        //                ,DT_ESEC_RICH
        //                ,TS_EFF_ESEC_RICH
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,IB_POLI
        //                ,IB_ADES
        //                ,IB_GAR
        //                ,IB_TRCH_DI_GAR
        //                ,ID_BATCH
        //                ,ID_JOB
        //                ,FL_SIMULAZIONE
        //                ,KEY_ORDINAMENTO
        //                ,ID_RICH_COLLG
        //                ,TP_RAMO_BILA
        //                ,TP_FRM_ASSVA
        //                ,TP_CALC_RIS
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,RAMO_BILA
        //             INTO
        //                :RIC-ID-RICH
        //               ,:RIC-COD-COMP-ANIA
        //               ,:RIC-TP-RICH
        //               ,:RIC-COD-MACROFUNCT
        //               ,:RIC-TP-MOVI
        //               ,:RIC-IB-RICH
        //                :IND-RIC-IB-RICH
        //               ,:RIC-DT-EFF-DB
        //               ,:RIC-DT-RGSTRZ-RICH-DB
        //               ,:RIC-DT-PERV-RICH-DB
        //               ,:RIC-DT-ESEC-RICH-DB
        //               ,:RIC-TS-EFF-ESEC-RICH
        //                :IND-RIC-TS-EFF-ESEC-RICH
        //               ,:RIC-ID-OGG
        //                :IND-RIC-ID-OGG
        //               ,:RIC-TP-OGG
        //                :IND-RIC-TP-OGG
        //               ,:RIC-IB-POLI
        //                :IND-RIC-IB-POLI
        //               ,:RIC-IB-ADES
        //                :IND-RIC-IB-ADES
        //               ,:RIC-IB-GAR
        //                :IND-RIC-IB-GAR
        //               ,:RIC-IB-TRCH-DI-GAR
        //                :IND-RIC-IB-TRCH-DI-GAR
        //               ,:RIC-ID-BATCH
        //                :IND-RIC-ID-BATCH
        //               ,:RIC-ID-JOB
        //                :IND-RIC-ID-JOB
        //               ,:RIC-FL-SIMULAZIONE
        //                :IND-RIC-FL-SIMULAZIONE
        //               ,:RIC-KEY-ORDINAMENTO-VCHAR
        //                :IND-RIC-KEY-ORDINAMENTO
        //               ,:RIC-ID-RICH-COLLG
        //                :IND-RIC-ID-RICH-COLLG
        //               ,:RIC-TP-RAMO-BILA
        //                :IND-RIC-TP-RAMO-BILA
        //               ,:RIC-TP-FRM-ASSVA
        //                :IND-RIC-TP-FRM-ASSVA
        //               ,:RIC-TP-CALC-RIS
        //                :IND-RIC-TP-CALC-RIS
        //               ,:RIC-DS-OPER-SQL
        //               ,:RIC-DS-VER
        //               ,:RIC-DS-TS-CPTZ
        //               ,:RIC-DS-UTENTE
        //               ,:RIC-DS-STATO-ELAB
        //               ,:RIC-RAMO-BILA
        //                :IND-RIC-RAMO-BILA
        //             FROM RICH
        //             WHERE  TP_RICH               = :PRENOTAZIONE
        //               AND  ID_BATCH              = :RIC-ID-BATCH
        //               AND  TP_MOVI               = :RIC-TP-MOVI
        //               AND COD_MACROFUNCT         = :RIC-COD-MACROFUNCT
        //               AND  COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //               AND DT_ESEC_RICH          <=  CURRENT DATE
        //               AND  DS_STATO_ELAB IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                      )
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        richDao.selectRec3(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A310-SELECT-SC02<br>
	 * <pre>*****************************************************************</pre>*/
    private void a310SelectSc02() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RICH
        //                ,COD_COMP_ANIA
        //                ,TP_RICH
        //                ,COD_MACROFUNCT
        //                ,TP_MOVI
        //                ,IB_RICH
        //                ,DT_EFF
        //                ,DT_RGSTRZ_RICH
        //                ,DT_PERV_RICH
        //                ,DT_ESEC_RICH
        //                ,TS_EFF_ESEC_RICH
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,IB_POLI
        //                ,IB_ADES
        //                ,IB_GAR
        //                ,IB_TRCH_DI_GAR
        //                ,ID_BATCH
        //                ,ID_JOB
        //                ,FL_SIMULAZIONE
        //                ,KEY_ORDINAMENTO
        //                ,ID_RICH_COLLG
        //                ,TP_RAMO_BILA
        //                ,TP_FRM_ASSVA
        //                ,TP_CALC_RIS
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,RAMO_BILA
        //             INTO
        //                :RIC-ID-RICH
        //               ,:RIC-COD-COMP-ANIA
        //               ,:RIC-TP-RICH
        //               ,:RIC-COD-MACROFUNCT
        //               ,:RIC-TP-MOVI
        //               ,:RIC-IB-RICH
        //                :IND-RIC-IB-RICH
        //               ,:RIC-DT-EFF-DB
        //               ,:RIC-DT-RGSTRZ-RICH-DB
        //               ,:RIC-DT-PERV-RICH-DB
        //               ,:RIC-DT-ESEC-RICH-DB
        //               ,:RIC-TS-EFF-ESEC-RICH
        //                :IND-RIC-TS-EFF-ESEC-RICH
        //               ,:RIC-ID-OGG
        //                :IND-RIC-ID-OGG
        //               ,:RIC-TP-OGG
        //                :IND-RIC-TP-OGG
        //               ,:RIC-IB-POLI
        //                :IND-RIC-IB-POLI
        //               ,:RIC-IB-ADES
        //                :IND-RIC-IB-ADES
        //               ,:RIC-IB-GAR
        //                :IND-RIC-IB-GAR
        //               ,:RIC-IB-TRCH-DI-GAR
        //                :IND-RIC-IB-TRCH-DI-GAR
        //               ,:RIC-ID-BATCH
        //                :IND-RIC-ID-BATCH
        //               ,:RIC-ID-JOB
        //                :IND-RIC-ID-JOB
        //               ,:RIC-FL-SIMULAZIONE
        //                :IND-RIC-FL-SIMULAZIONE
        //               ,:RIC-KEY-ORDINAMENTO-VCHAR
        //                :IND-RIC-KEY-ORDINAMENTO
        //               ,:RIC-ID-RICH-COLLG
        //                :IND-RIC-ID-RICH-COLLG
        //               ,:RIC-TP-RAMO-BILA
        //                :IND-RIC-TP-RAMO-BILA
        //               ,:RIC-TP-FRM-ASSVA
        //                :IND-RIC-TP-FRM-ASSVA
        //               ,:RIC-TP-CALC-RIS
        //                :IND-RIC-TP-CALC-RIS
        //               ,:RIC-DS-OPER-SQL
        //               ,:RIC-DS-VER
        //               ,:RIC-DS-TS-CPTZ
        //               ,:RIC-DS-UTENTE
        //               ,:RIC-DS-STATO-ELAB
        //               ,:RIC-RAMO-BILA
        //                :IND-RIC-RAMO-BILA
        //             FROM RICH
        //             WHERE  ID_RICH        =  :RIC-ID-RICH AND
        //                    DT_ESEC_RICH  <=  CURRENT DATE
        //           END-EXEC.
        richDao.selectByRicIdRich(rich.getRicIdRich(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B001-SELECT-DER       THRU B001-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM B001-SELECT-DER       THRU B001-EX
            b001SelectDer();
        }
    }

    /**Original name: A320-UPDATE-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a320UpdateSc01() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-PRIMARY-KEY
        //                   PERFORM A330-UPDATE-PK-SC01      THRU A330-SC01-EX
        //              WHEN IDSV0003-FIRST-ACTION
        //                                                    THRU A340-SC01-EX
        //              WHEN IDSV0003-WHERE-CONDITION
        //                                                    THRU A350-SC01-EX
        //              WHEN OTHER
        //                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
        //           END-EVALUATE.
        switch (idsv0003.getLivelloOperazione().getLivelloOperazione()) {

            case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A330-UPDATE-PK-SC01      THRU A330-SC01-EX
                a330UpdatePkSc01();
                break;

            case Idsv0003LivelloOperazione.FIRST_ACTION:// COB_CODE: PERFORM A340-UPDATE-FIRST-ACTION-SC01
                //                                            THRU A340-SC01-EX
                a340UpdateFirstActionSc01();
                break;

            case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A350-UPDATE-WHERE-COND-SC01
                //                                            THRU A350-SC01-EX
                a350UpdateWhereCondSc01();
                break;

            default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidLevelOper();
                break;
        }
    }

    /**Original name: A330-UPDATE-PK-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a330UpdatePkSc01() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE RICH SET
        //                   TS_EFF_ESEC_RICH       =
        //                :RIC-TS-EFF-ESEC-RICH
        //                :IND-RIC-TS-EFF-ESEC-RICH
        //                  ,DS_OPER_SQL            =
        //                :RIC-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :RIC-DS-VER
        //                  ,DS_TS_CPTZ             =
        //                :RIC-DS-TS-CPTZ
        //                  ,DS_UTENTE              =
        //                :RIC-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :IABV0002-STATE-CURRENT
        //                WHERE   ID_RICH       =  :RIC-ID-RICH
        //                  AND   COD_COMP_ANIA =  :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           END-EXEC.
        richDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
            rich.setRicDsStatoElab(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A330-UPDATE-SC02<br>
	 * <pre>*****************************************************************</pre>*/
    private void a330UpdateSc02() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-FIRST-ACTION
        //                                                    THRU A331-SC02-EX
        //              WHEN IDSV0003-NONE-ACTION
        //                                                    THRU A332-SC02-EX
        //              WHEN IDSV0003-ID
        //                                                    THRU A333-SC02-EX
        //              WHEN OTHER
        //                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
        //           END-EVALUATE.
        switch (idsv0003.getLivelloOperazione().getLivelloOperazione()) {

            case Idsv0003LivelloOperazione.FIRST_ACTION:// COB_CODE: PERFORM A331-UPDATE-FIRST-ACTION-SC02
                //                                            THRU A331-SC02-EX
                a331UpdateFirstActionSc02();
                break;

            case Idsv0003LivelloOperazione.NONE_ACTION:// COB_CODE: PERFORM A332-UPDATE-NONE-ACTION-SC02
                //                                            THRU A332-SC02-EX
                a332UpdateNoneActionSc02();
                break;

            case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A333-UPDATE-ID-SC02
                //                                            THRU A333-SC02-EX
                a333UpdateIdSc02();
                break;

            default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidLevelOper();
                break;
        }
    }

    /**Original name: A331-UPDATE-FIRST-ACTION-SC02<br>
	 * <pre>*****************************************************************</pre>*/
    private void a331UpdateFirstActionSc02() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE RICH
        //                SET TS_EFF_ESEC_RICH = :RIC-TS-EFF-ESEC-RICH
        //                                       :IND-RIC-TS-EFF-ESEC-RICH
        //                WHERE   ID_RICH      = :RIC-ID-RICH
        //           END-EXEC.
        richDao.updateRec1(rich.getRicTsEffEsecRich().getRicTsEffEsecRich(), rich.getRicIdRich());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE       THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A332-UPDATE-NONE-ACTION-SC02<br>
	 * <pre>*****************************************************************</pre>*/
    private void a332UpdateNoneActionSc02() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE RICH
        //                SET DS_STATO_ELAB = :IABV0002-STATE-CURRENT
        //                WHERE   ID_RICH   = :RIC-ID-RICH
        //           END-EXEC.
        richDao.updateRec2(iabv0002.getIabv0002StateCurrent(), rich.getRicIdRich());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE       THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT       TO RIC-DS-STATO-ELAB
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT       TO RIC-DS-STATO-ELAB
            rich.setRicDsStatoElab(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A333-UPDATE-ID-SC02<br>
	 * <pre>*****************************************************************</pre>*/
    private void a333UpdateIdSc02() {
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR  TO RIC-DS-TS-CPTZ.
        rich.setRicDsTsCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE RICH
        //                SET TP_RICH   = :RIC-TP-RICH
        //                   ,TP_MOVI   = :RIC-TP-MOVI
        //                   ,IB_RICH   = :RIC-IB-RICH
        //                                :IND-RIC-IB-RICH
        //                   ,DT_RGSTRZ_RICH
        //                              = :RIC-DT-RGSTRZ-RICH-DB
        //                   ,DT_PERV_RICH
        //                              = :RIC-DT-PERV-RICH-DB
        //                   ,DT_EFF    = :RIC-DT-EFF-DB
        //                   ,DT_ESEC_RICH
        //                                  = :RIC-DT-ESEC-RICH-DB
        //                   ,DS_TS_CPTZ    = :RIC-DS-TS-CPTZ
        //                WHERE   ID_RICH   = :RIC-ID-RICH
        //                AND COD_COMP_ANIA  = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           END-EXEC.
        richDao.updateRec3(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE       THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A350-INSERT-SC02<br>
	 * <pre>*****************************************************************</pre>*/
    private void a350InsertSc02() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO RICH
            //                  (
            //                     ID_RICH
            //                    ,COD_COMP_ANIA
            //                    ,TP_RICH
            //                    ,COD_MACROFUNCT
            //                    ,TP_MOVI
            //                    ,IB_RICH
            //                    ,DT_EFF
            //                    ,DT_RGSTRZ_RICH
            //                    ,DT_PERV_RICH
            //                    ,DT_ESEC_RICH
            //                    ,TS_EFF_ESEC_RICH
            //                    ,ID_OGG
            //                    ,TP_OGG
            //                    ,IB_POLI
            //                    ,IB_ADES
            //                    ,IB_GAR
            //                    ,IB_TRCH_DI_GAR
            //                    ,ID_BATCH
            //                    ,ID_JOB
            //                    ,FL_SIMULAZIONE
            //                    ,KEY_ORDINAMENTO
            //                    ,ID_RICH_COLLG
            //                    ,TP_RAMO_BILA
            //                    ,TP_FRM_ASSVA
            //                    ,TP_CALC_RIS
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,RAMO_BILA
            //                  )
            //              VALUES
            //                  (
            //                    :RIC-ID-RICH
            //                    ,:RIC-COD-COMP-ANIA
            //                    ,:RIC-TP-RICH
            //                    ,:RIC-COD-MACROFUNCT
            //                    ,:RIC-TP-MOVI
            //                    ,:RIC-IB-RICH
            //                     :IND-RIC-IB-RICH
            //                    ,:RIC-DT-EFF-DB
            //                    ,:RIC-DT-RGSTRZ-RICH-DB
            //                    ,:RIC-DT-PERV-RICH-DB
            //                    ,:RIC-DT-ESEC-RICH-DB
            //                    ,:RIC-TS-EFF-ESEC-RICH
            //                     :IND-RIC-TS-EFF-ESEC-RICH
            //                    ,:RIC-ID-OGG
            //                     :IND-RIC-ID-OGG
            //                    ,:RIC-TP-OGG
            //                     :IND-RIC-TP-OGG
            //                    ,:RIC-IB-POLI
            //                     :IND-RIC-IB-POLI
            //                    ,:RIC-IB-ADES
            //                     :IND-RIC-IB-ADES
            //                    ,:RIC-IB-GAR
            //                     :IND-RIC-IB-GAR
            //                    ,:RIC-IB-TRCH-DI-GAR
            //                     :IND-RIC-IB-TRCH-DI-GAR
            //                    ,:RIC-ID-BATCH
            //                     :IND-RIC-ID-BATCH
            //                    ,:RIC-ID-JOB
            //                     :IND-RIC-ID-JOB
            //                    ,:RIC-FL-SIMULAZIONE
            //                     :IND-RIC-FL-SIMULAZIONE
            //                    ,:RIC-KEY-ORDINAMENTO-VCHAR
            //                     :IND-RIC-KEY-ORDINAMENTO
            //                    ,:RIC-ID-RICH-COLLG
            //                     :IND-RIC-ID-RICH-COLLG
            //                    ,:RIC-TP-RAMO-BILA
            //                     :IND-RIC-TP-RAMO-BILA
            //                    ,:RIC-TP-FRM-ASSVA
            //                     :IND-RIC-TP-FRM-ASSVA
            //                    ,:RIC-TP-CALC-RIS
            //                     :IND-RIC-TP-CALC-RIS
            //                    ,:RIC-DS-OPER-SQL
            //                    ,:RIC-DS-VER
            //                    ,:RIC-DS-TS-CPTZ
            //                    ,:RIC-DS-UTENTE
            //                    ,:RIC-DS-STATO-ELAB
            //                    ,:RIC-RAMO-BILA
            //                     :IND-RIC-RAMO-BILA
            //                  )
            //           END-EXEC
            richDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A340-UPDATE-FIRST-ACTION-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a340UpdateFirstActionSc01() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN ACCESSO-STD
        //                 PERFORM A341-UPD-F-ACT-STD-SC01   THRU A341-SC01-EX
        //              WHEN ACCESSO-MCRFNCT
        //                 PERFORM A342-UPD-F-ACT-MFNCT-SC01 THRU A342-SC01-EX
        //              WHEN ACCESSO-TPMV
        //                 PERFORM A343-UPD-F-ACT-TPMV-SC01  THRU A343-SC01-EX
        //              WHEN ACCESSO-MCRFNCT-TPMV
        //                                                   THRU A344-SC01-EX
        //           END-EVALUATE.
        switch (ws.getFlagAccesso().getFlagModCalcPreP()) {

            case Lccc006TrattDati.AMMESSO_MOD:// COB_CODE: PERFORM A341-UPD-F-ACT-STD-SC01   THRU A341-SC01-EX
                a341UpdFActStdSc01();
                break;

            case Lccc006TrattDati.AMMESSO_NMOD:// COB_CODE: PERFORM A342-UPD-F-ACT-MFNCT-SC01 THRU A342-SC01-EX
                a342UpdFActMfnctSc01();
                break;

            case Lccc006TrattDati.NON_AMMESSO:// COB_CODE: PERFORM A343-UPD-F-ACT-TPMV-SC01  THRU A343-SC01-EX
                a343UpdFActTpmvSc01();
                break;

            case Lccc006TrattDati.OBBLIGATORIO:// COB_CODE: PERFORM A344-UPD-F-ACT-MFNCT-TPMV-SC01
                //                                             THRU A344-SC01-EX
                a344UpdFActMfnctTpmvSc01();
                break;

            default:break;
        }
    }

    /**Original name: A341-UPD-F-ACT-STD-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a341UpdFActStdSc01() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE RICH SET
        //                DS_STATO_ELAB      = :IABV0002-STATE-CURRENT
        //              WHERE TP_RICH        = :PRENOTAZIONE
        //                AND ID_BATCH       = :RIC-ID-BATCH
        //                AND COD_COMP_ANIA  = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                AND DS_STATO_ELAB IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                     )
        //           END-EXEC.
        richDao.updateRec5(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
            rich.setRicDsStatoElab(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A342-UPD-F-ACT-MFNCT-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a342UpdFActMfnctSc01() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE RICH SET
        //                DS_STATO_ELAB      = :IABV0002-STATE-CURRENT
        //              WHERE TP_RICH        = :PRENOTAZIONE
        //                AND ID_BATCH       = :RIC-ID-BATCH
        //                AND COD_MACROFUNCT = :RIC-COD-MACROFUNCT
        //                AND COD_COMP_ANIA  = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                AND DS_STATO_ELAB IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                     )
        //           END-EXEC.
        richDao.updateRec6(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
            rich.setRicDsStatoElab(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A343-UPD-F-ACT-TPMV-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a343UpdFActTpmvSc01() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE RICH SET
        //                DS_STATO_ELAB      = :IABV0002-STATE-CURRENT
        //              WHERE TP_RICH        = :PRENOTAZIONE
        //                AND ID_BATCH       = :RIC-ID-BATCH
        //                AND TP_MOVI        = :RIC-TP-MOVI
        //                AND COD_COMP_ANIA  = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                AND DS_STATO_ELAB IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                     )
        //           END-EXEC.
        richDao.updateRec7(this.getRichIabs09001());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
            rich.setRicDsStatoElab(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A344-UPD-F-ACT-MFNCT-TPMV-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a344UpdFActMfnctTpmvSc01() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE RICH SET
        //                DS_STATO_ELAB      = :IABV0002-STATE-CURRENT
        //              WHERE TP_RICH        = :PRENOTAZIONE
        //                AND ID_BATCH       = :RIC-ID-BATCH
        //                AND TP_MOVI        = :RIC-TP-MOVI
        //                AND COD_MACROFUNCT = :RIC-COD-MACROFUNCT
        //                AND COD_COMP_ANIA  = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                AND DS_STATO_ELAB IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                     )
        //           END-EXEC.
        richDao.updateRec8(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
            rich.setRicDsStatoElab(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A350-UPDATE-WHERE-COND-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a350UpdateWhereCondSc01() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN ACCESSO-STD
        //                 PERFORM A351-UPD-WHC-STD-SC01   THRU A351-SC01-EX
        //              WHEN ACCESSO-MCRFNCT
        //                 PERFORM A352-UPD-WHC-MFNCT-SC01 THRU A352-SC01-EX
        //              WHEN ACCESSO-TPMV
        //                 PERFORM A353-UPD-WHC-TPMV-SC01  THRU A353-SC01-EX
        //              WHEN ACCESSO-MCRFNCT-TPMV
        //                                                 THRU A354-SC01-EX
        //           END-EVALUATE.
        switch (ws.getFlagAccesso().getFlagModCalcPreP()) {

            case Lccc006TrattDati.AMMESSO_MOD:// COB_CODE: PERFORM A351-UPD-WHC-STD-SC01   THRU A351-SC01-EX
                a351UpdWhcStdSc01();
                break;

            case Lccc006TrattDati.AMMESSO_NMOD:// COB_CODE: PERFORM A352-UPD-WHC-MFNCT-SC01 THRU A352-SC01-EX
                a352UpdWhcMfnctSc01();
                break;

            case Lccc006TrattDati.NON_AMMESSO:// COB_CODE: PERFORM A353-UPD-WHC-TPMV-SC01  THRU A353-SC01-EX
                a353UpdWhcTpmvSc01();
                break;

            case Lccc006TrattDati.OBBLIGATORIO:// COB_CODE: PERFORM A354-UPD-WHC-MFNCT-TPMV-SC01
                //                                           THRU A354-SC01-EX
                a354UpdWhcMfnctTpmvSc01();
                break;

            default:break;
        }
    }

    /**Original name: A351-UPD-WHC-STD-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a351UpdWhcStdSc01() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE RICH SET
        //                DS_STATO_ELAB      = :IABV0002-STATE-CURRENT
        //              WHERE TP_RICH        = :PRENOTAZIONE
        //                AND ID_BATCH       = :RIC-ID-BATCH
        //                AND COD_COMP_ANIA  = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                AND DS_STATO_ELAB IN (
        //                                        :IABV0002-STATE-01,
        //                                        :IABV0002-STATE-02,
        //                                        :IABV0002-STATE-03,
        //                                        :IABV0002-STATE-04,
        //                                        :IABV0002-STATE-05,
        //                                        :IABV0002-STATE-06,
        //                                        :IABV0002-STATE-07,
        //                                        :IABV0002-STATE-08,
        //                                        :IABV0002-STATE-09,
        //                                        :IABV0002-STATE-10
        //                                     )
        //           END-EXEC.
        richDao.updateRec5(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
            rich.setRicDsStatoElab(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A352-UPD-WHC-MFNCT-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a352UpdWhcMfnctSc01() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE RICH SET
        //                DS_STATO_ELAB      = :IABV0002-STATE-CURRENT
        //              WHERE TP_RICH        = :PRENOTAZIONE
        //                AND ID_BATCH       = :RIC-ID-BATCH
        //                AND COD_MACROFUNCT = :RIC-COD-MACROFUNCT
        //                AND COD_COMP_ANIA  = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                AND DS_STATO_ELAB IN (
        //                                        :IABV0002-STATE-01,
        //                                        :IABV0002-STATE-02,
        //                                        :IABV0002-STATE-03,
        //                                        :IABV0002-STATE-04,
        //                                        :IABV0002-STATE-05,
        //                                        :IABV0002-STATE-06,
        //                                        :IABV0002-STATE-07,
        //                                        :IABV0002-STATE-08,
        //                                        :IABV0002-STATE-09,
        //                                        :IABV0002-STATE-10
        //                                      )
        //           END-EXEC.
        richDao.updateRec6(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
            rich.setRicDsStatoElab(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A353-UPD-WHC-TPMV-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a353UpdWhcTpmvSc01() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE RICH SET
        //                DS_STATO_ELAB      = :IABV0002-STATE-CURRENT
        //              WHERE TP_RICH        = :PRENOTAZIONE
        //                AND ID_BATCH       = :RIC-ID-BATCH
        //                AND TP_MOVI        = :RIC-TP-MOVI
        //                AND COD_COMP_ANIA  = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                AND DS_STATO_ELAB IN (
        //                                        :IABV0002-STATE-01,
        //                                        :IABV0002-STATE-02,
        //                                        :IABV0002-STATE-03,
        //                                        :IABV0002-STATE-04,
        //                                        :IABV0002-STATE-05,
        //                                        :IABV0002-STATE-06,
        //                                        :IABV0002-STATE-07,
        //                                        :IABV0002-STATE-08,
        //                                        :IABV0002-STATE-09,
        //                                        :IABV0002-STATE-10
        //                                      )
        //           END-EXEC.
        richDao.updateRec7(this.getRichIabs09001());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
            rich.setRicDsStatoElab(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A354-UPD-WHC-MFNCT-TPMV-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a354UpdWhcMfnctTpmvSc01() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE RICH SET
        //                DS_STATO_ELAB      = :IABV0002-STATE-CURRENT
        //              WHERE TP_RICH        = :PRENOTAZIONE
        //                AND ID_BATCH       = :RIC-ID-BATCH
        //                AND TP_MOVI        = :RIC-TP-MOVI
        //                AND COD_MACROFUNCT = :RIC-COD-MACROFUNCT
        //                AND COD_COMP_ANIA  = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                AND DS_STATO_ELAB IN (
        //                                        :IABV0002-STATE-01,
        //                                        :IABV0002-STATE-02,
        //                                        :IABV0002-STATE-03,
        //                                        :IABV0002-STATE-04,
        //                                        :IABV0002-STATE-05,
        //                                        :IABV0002-STATE-06,
        //                                        :IABV0002-STATE-07,
        //                                        :IABV0002-STATE-08,
        //                                        :IABV0002-STATE-09,
        //                                        :IABV0002-STATE-10
        //                                      )
        //           END-EXEC.
        richDao.updateRec8(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
            rich.setRicDsStatoElab(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A360-OPEN-CURSOR-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a360OpenCursorSc01() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IABV0002-ORDER-BY-ID-JOB
        //                                                   THRU A305-SC01-EX
        //              WHEN OTHER
        //                   SET IDSV0003-INVALID-OPER       TO TRUE
        //           END-EVALUATE.
        switch (iabv0002.getIabv0002TipoOrderBy().getIabv0002TipoOrderBy()) {

            case Iabv0002TipoOrderBy.ID_JOB:// COB_CODE: PERFORM A305-DECLARE-CURSOR-IDR-SC01
                //                                           THRU A305-SC01-EX
                a305DeclareCursorIdrSc01();
                break;

            default:// COB_CODE: SET IDSV0003-INVALID-OPER       TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                break;
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A370-CLOSE-CURSOR-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a370CloseCursorSc01() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IABV0002-ORDER-BY-ID-JOB
        //                   PERFORM C100-CLOSE-IDR-SC01 THRU C100-SC01-EX
        //              WHEN OTHER
        //                   SET IDSV0003-INVALID-OPER       TO TRUE
        //           END-EVALUATE.
        switch (iabv0002.getIabv0002TipoOrderBy().getIabv0002TipoOrderBy()) {

            case Iabv0002TipoOrderBy.ID_JOB:// COB_CODE: PERFORM C100-CLOSE-IDR-SC01 THRU C100-SC01-EX
                c100CloseIdrSc01();
                break;

            default:// COB_CODE: SET IDSV0003-INVALID-OPER       TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                break;
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: C100-CLOSE-IDR-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void c100CloseIdrSc01() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN ACCESSO-STD
        //                 END-EXEC
        //              WHEN ACCESSO-MCRFNCT
        //                 END-EXEC
        //              WHEN ACCESSO-TPMV
        //                 END-EXEC
        //              WHEN ACCESSO-MCRFNCT-TPMV
        //                 END-EXEC
        //           END-EVALUATE.
        switch (ws.getFlagAccesso().getFlagModCalcPreP()) {

            case Lccc006TrattDati.AMMESSO_MOD:// COB_CODE: EXEC SQL
                //                CLOSE CUR-RIC-IDR-STD
                //           END-EXEC
                richDao.closeCurRicIdrStd();
                break;

            case Lccc006TrattDati.AMMESSO_NMOD:// COB_CODE: EXEC SQL
                //                CLOSE CUR-RIC-IDR-MF
                //           END-EXEC
                richDao.closeCurRicIdrMf();
                break;

            case Lccc006TrattDati.NON_AMMESSO:// COB_CODE: EXEC SQL
                //                CLOSE CUR-RIC-IDR-TM
                //           END-EXEC
                richDao.closeCurRicIdrTm();
                break;

            case Lccc006TrattDati.OBBLIGATORIO:// COB_CODE: EXEC SQL
                //                CLOSE CUR-RIC-IDR-MF-TM
                //           END-EXEC
                richDao.closeCurRicIdrMfTm();
                break;

            default:break;
        }
    }

    /**Original name: A380-FETCH-FIRST-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a380FetchFirstSc01() {
        // COB_CODE: PERFORM A360-OPEN-CURSOR-SC01   THRU A360-SC01-EX.
        a360OpenCursorSc01();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A390-FETCH-NEXT-SC01 THRU A390-SC01-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-SC01 THRU A390-SC01-EX
            a390FetchNextSc01();
        }
    }

    /**Original name: A390-FETCH-NEXT-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a390FetchNextSc01() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IABV0002-ORDER-BY-ID-JOB
        //                   PERFORM A391-FETCH-NEXT-IDR-SC01 THRU A391-SC01-EX
        //              WHEN OTHER
        //                   SET IDSV0003-INVALID-OPER       TO TRUE
        //           END-EVALUATE.
        switch (iabv0002.getIabv0002TipoOrderBy().getIabv0002TipoOrderBy()) {

            case Iabv0002TipoOrderBy.ID_JOB:// COB_CODE: PERFORM A391-FETCH-NEXT-IDR-SC01 THRU A391-SC01-EX
                a391FetchNextIdrSc01();
                break;

            default:// COB_CODE: SET IDSV0003-INVALID-OPER       TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                break;
        }
    }

    /**Original name: A391-FETCH-NEXT-IDR-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a391FetchNextIdrSc01() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN ACCESSO-STD
        //                 PERFORM F101-FET-NX-IDR-STD-SC01     THRU F101-SC01-EX
        //              WHEN ACCESSO-MCRFNCT
        //                 PERFORM F102-FET-NX-IDR-MFNCT-SC01   THRU F102-SC01-EX
        //              WHEN ACCESSO-TPMV
        //                 PERFORM F103-FET-NX-IDR-TPMV-SC01    THRU F103-SC01-EX
        //              WHEN ACCESSO-MCRFNCT-TPMV
        //                 PERFORM F104-FET-NX-IDR-MF-TPMV-SC01 THRU F104-SC01-EX
        //           END-EVALUATE.
        switch (ws.getFlagAccesso().getFlagModCalcPreP()) {

            case Lccc006TrattDati.AMMESSO_MOD:// COB_CODE: PERFORM F101-FET-NX-IDR-STD-SC01     THRU F101-SC01-EX
                f101FetNxIdrStdSc01();
                break;

            case Lccc006TrattDati.AMMESSO_NMOD:// COB_CODE: PERFORM F102-FET-NX-IDR-MFNCT-SC01   THRU F102-SC01-EX
                f102FetNxIdrMfnctSc01();
                break;

            case Lccc006TrattDati.NON_AMMESSO:// COB_CODE: PERFORM F103-FET-NX-IDR-TPMV-SC01    THRU F103-SC01-EX
                f103FetNxIdrTpmvSc01();
                break;

            case Lccc006TrattDati.OBBLIGATORIO:// COB_CODE: PERFORM F104-FET-NX-IDR-MF-TPMV-SC01 THRU F104-SC01-EX
                f104FetNxIdrMfTpmvSc01();
                break;

            default:break;
        }
    }

    /**Original name: F101-FET-NX-IDR-STD-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void f101FetNxIdrStdSc01() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-RIC-IDR-STD
        //           INTO
        //                :RIC-ID-RICH
        //               ,:RIC-COD-COMP-ANIA
        //               ,:RIC-TP-RICH
        //               ,:RIC-COD-MACROFUNCT
        //               ,:RIC-TP-MOVI
        //               ,:RIC-IB-RICH
        //                :IND-RIC-IB-RICH
        //               ,:RIC-DT-EFF-DB
        //               ,:RIC-DT-RGSTRZ-RICH-DB
        //               ,:RIC-DT-PERV-RICH-DB
        //               ,:RIC-DT-ESEC-RICH-DB
        //               ,:RIC-TS-EFF-ESEC-RICH
        //                :IND-RIC-TS-EFF-ESEC-RICH
        //               ,:RIC-ID-OGG
        //                :IND-RIC-ID-OGG
        //               ,:RIC-TP-OGG
        //                :IND-RIC-TP-OGG
        //               ,:RIC-IB-POLI
        //                :IND-RIC-IB-POLI
        //               ,:RIC-IB-ADES
        //                :IND-RIC-IB-ADES
        //               ,:RIC-IB-GAR
        //                :IND-RIC-IB-GAR
        //               ,:RIC-IB-TRCH-DI-GAR
        //                :IND-RIC-IB-TRCH-DI-GAR
        //               ,:RIC-ID-BATCH
        //                :IND-RIC-ID-BATCH
        //               ,:RIC-ID-JOB
        //                :IND-RIC-ID-JOB
        //               ,:RIC-FL-SIMULAZIONE
        //                :IND-RIC-FL-SIMULAZIONE
        //               ,:RIC-KEY-ORDINAMENTO-VCHAR
        //                :IND-RIC-KEY-ORDINAMENTO
        //               ,:RIC-ID-RICH-COLLG
        //                :IND-RIC-ID-RICH-COLLG
        //               ,:RIC-TP-RAMO-BILA
        //                :IND-RIC-TP-RAMO-BILA
        //               ,:RIC-TP-FRM-ASSVA
        //                :IND-RIC-TP-FRM-ASSVA
        //               ,:RIC-TP-CALC-RIS
        //                :IND-RIC-TP-CALC-RIS
        //               ,:RIC-DS-OPER-SQL
        //               ,:RIC-DS-VER
        //               ,:RIC-DS-TS-CPTZ
        //               ,:RIC-DS-UTENTE
        //               ,:RIC-DS-STATO-ELAB
        //               ,:RIC-RAMO-BILA
        //                :IND-RIC-RAMO-BILA
        //           END-EXEC.
        richDao.fetchCurRicIdrStd(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B000-FETCH-DER        THRU B000-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM B000-FETCH-DER        THRU B000-EX
            b000FetchDer();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC01 THRU A370-SC01-EX
            a370CloseCursorSc01();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: F102-FET-NX-IDR-MFNCT-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void f102FetNxIdrMfnctSc01() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-RIC-IDR-MF
        //           INTO
        //                :RIC-ID-RICH
        //               ,:RIC-COD-COMP-ANIA
        //               ,:RIC-TP-RICH
        //               ,:RIC-COD-MACROFUNCT
        //               ,:RIC-TP-MOVI
        //               ,:RIC-IB-RICH
        //                :IND-RIC-IB-RICH
        //               ,:RIC-DT-EFF-DB
        //               ,:RIC-DT-RGSTRZ-RICH-DB
        //               ,:RIC-DT-PERV-RICH-DB
        //               ,:RIC-DT-ESEC-RICH-DB
        //               ,:RIC-TS-EFF-ESEC-RICH
        //                :IND-RIC-TS-EFF-ESEC-RICH
        //               ,:RIC-ID-OGG
        //                :IND-RIC-ID-OGG
        //               ,:RIC-TP-OGG
        //                :IND-RIC-TP-OGG
        //               ,:RIC-IB-POLI
        //                :IND-RIC-IB-POLI
        //               ,:RIC-IB-ADES
        //                :IND-RIC-IB-ADES
        //               ,:RIC-IB-GAR
        //                :IND-RIC-IB-GAR
        //               ,:RIC-IB-TRCH-DI-GAR
        //                :IND-RIC-IB-TRCH-DI-GAR
        //               ,:RIC-ID-BATCH
        //                :IND-RIC-ID-BATCH
        //               ,:RIC-ID-JOB
        //                :IND-RIC-ID-JOB
        //               ,:RIC-FL-SIMULAZIONE
        //                :IND-RIC-FL-SIMULAZIONE
        //               ,:RIC-KEY-ORDINAMENTO-VCHAR
        //                :IND-RIC-KEY-ORDINAMENTO
        //               ,:RIC-ID-RICH-COLLG
        //                :IND-RIC-ID-RICH-COLLG
        //               ,:RIC-TP-RAMO-BILA
        //                :IND-RIC-TP-RAMO-BILA
        //               ,:RIC-TP-FRM-ASSVA
        //                :IND-RIC-TP-FRM-ASSVA
        //               ,:RIC-TP-CALC-RIS
        //                :IND-RIC-TP-CALC-RIS
        //               ,:RIC-DS-OPER-SQL
        //               ,:RIC-DS-VER
        //               ,:RIC-DS-TS-CPTZ
        //               ,:RIC-DS-UTENTE
        //               ,:RIC-DS-STATO-ELAB
        //               ,:RIC-RAMO-BILA
        //                :IND-RIC-RAMO-BILA
        //           END-EXEC.
        richDao.fetchCurRicIdrMf(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B000-FETCH-DER        THRU B000-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM B000-FETCH-DER        THRU B000-EX
            b000FetchDer();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC01 THRU A370-SC01-EX
            a370CloseCursorSc01();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: F103-FET-NX-IDR-TPMV-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void f103FetNxIdrTpmvSc01() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-RIC-IDR-TM
        //           INTO
        //                :RIC-ID-RICH
        //               ,:RIC-COD-COMP-ANIA
        //               ,:RIC-TP-RICH
        //               ,:RIC-COD-MACROFUNCT
        //               ,:RIC-TP-MOVI
        //               ,:RIC-IB-RICH
        //                :IND-RIC-IB-RICH
        //               ,:RIC-DT-EFF-DB
        //               ,:RIC-DT-RGSTRZ-RICH-DB
        //               ,:RIC-DT-PERV-RICH-DB
        //               ,:RIC-DT-ESEC-RICH-DB
        //               ,:RIC-TS-EFF-ESEC-RICH
        //                :IND-RIC-TS-EFF-ESEC-RICH
        //               ,:RIC-ID-OGG
        //                :IND-RIC-ID-OGG
        //               ,:RIC-TP-OGG
        //                :IND-RIC-TP-OGG
        //               ,:RIC-IB-POLI
        //                :IND-RIC-IB-POLI
        //               ,:RIC-IB-ADES
        //                :IND-RIC-IB-ADES
        //               ,:RIC-IB-GAR
        //                :IND-RIC-IB-GAR
        //               ,:RIC-IB-TRCH-DI-GAR
        //                :IND-RIC-IB-TRCH-DI-GAR
        //               ,:RIC-ID-BATCH
        //                :IND-RIC-ID-BATCH
        //               ,:RIC-ID-JOB
        //                :IND-RIC-ID-JOB
        //               ,:RIC-FL-SIMULAZIONE
        //                :IND-RIC-FL-SIMULAZIONE
        //               ,:RIC-KEY-ORDINAMENTO-VCHAR
        //                :IND-RIC-KEY-ORDINAMENTO
        //               ,:RIC-ID-RICH-COLLG
        //                :IND-RIC-ID-RICH-COLLG
        //               ,:RIC-TP-RAMO-BILA
        //                :IND-RIC-TP-RAMO-BILA
        //               ,:RIC-TP-FRM-ASSVA
        //                :IND-RIC-TP-FRM-ASSVA
        //               ,:RIC-TP-CALC-RIS
        //                :IND-RIC-TP-CALC-RIS
        //               ,:RIC-DS-OPER-SQL
        //               ,:RIC-DS-VER
        //               ,:RIC-DS-TS-CPTZ
        //               ,:RIC-DS-UTENTE
        //               ,:RIC-DS-STATO-ELAB
        //               ,:RIC-RAMO-BILA
        //                :IND-RIC-RAMO-BILA
        //           END-EXEC.
        richDao.fetchCurRicIdrTm(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B000-FETCH-DER        THRU B000-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM B000-FETCH-DER        THRU B000-EX
            b000FetchDer();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC01 THRU A370-SC01-EX
            a370CloseCursorSc01();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: F104-FET-NX-IDR-MF-TPMV-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void f104FetNxIdrMfTpmvSc01() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-RIC-IDR-MF-TM
        //           INTO
        //                :RIC-ID-RICH
        //               ,:RIC-COD-COMP-ANIA
        //               ,:RIC-TP-RICH
        //               ,:RIC-COD-MACROFUNCT
        //               ,:RIC-TP-MOVI
        //               ,:RIC-IB-RICH
        //                :IND-RIC-IB-RICH
        //               ,:RIC-DT-EFF-DB
        //               ,:RIC-DT-RGSTRZ-RICH-DB
        //               ,:RIC-DT-PERV-RICH-DB
        //               ,:RIC-DT-ESEC-RICH-DB
        //               ,:RIC-TS-EFF-ESEC-RICH
        //                :IND-RIC-TS-EFF-ESEC-RICH
        //               ,:RIC-ID-OGG
        //                :IND-RIC-ID-OGG
        //               ,:RIC-TP-OGG
        //                :IND-RIC-TP-OGG
        //               ,:RIC-IB-POLI
        //                :IND-RIC-IB-POLI
        //               ,:RIC-IB-ADES
        //                :IND-RIC-IB-ADES
        //               ,:RIC-IB-GAR
        //                :IND-RIC-IB-GAR
        //               ,:RIC-IB-TRCH-DI-GAR
        //                :IND-RIC-IB-TRCH-DI-GAR
        //               ,:RIC-ID-BATCH
        //                :IND-RIC-ID-BATCH
        //               ,:RIC-ID-JOB
        //                :IND-RIC-ID-JOB
        //               ,:RIC-FL-SIMULAZIONE
        //                :IND-RIC-FL-SIMULAZIONE
        //               ,:RIC-KEY-ORDINAMENTO-VCHAR
        //                :IND-RIC-KEY-ORDINAMENTO
        //               ,:RIC-ID-RICH-COLLG
        //                :IND-RIC-ID-RICH-COLLG
        //               ,:RIC-TP-RAMO-BILA
        //                :IND-RIC-TP-RAMO-BILA
        //               ,:RIC-TP-FRM-ASSVA
        //                :IND-RIC-TP-FRM-ASSVA
        //               ,:RIC-TP-CALC-RIS
        //                :IND-RIC-TP-CALC-RIS
        //               ,:RIC-DS-OPER-SQL
        //               ,:RIC-DS-VER
        //               ,:RIC-DS-TS-CPTZ
        //               ,:RIC-DS-UTENTE
        //               ,:RIC-DS-STATO-ELAB
        //               ,:RIC-RAMO-BILA
        //                :IND-RIC-RAMO-BILA
        //           END-EXEC.
        richDao.fetchCurRicIdrMfTm(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B000-FETCH-DER        THRU B000-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM B000-FETCH-DER        THRU B000-EX
            b000FetchDer();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC01 THRU A370-SC01-EX
            a370CloseCursorSc01();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B000-FETCH-DER<br>
	 * <pre>*****************************************************************</pre>*/
    private void b000FetchDer() {
        // COB_CODE: MOVE RIC-ID-RICH               TO DER-ID-RICH.
        ws.getDettRich().setIdRich(rich.getRicIdRich());
        // COB_CODE: MOVE ZERO                      TO IABV0003-ELE-MAX.
        iabv0003.setIabv0003EleMax(((short)0));
        // COB_CODE: PERFORM B030-FETCH-FIRST-DER   THRU B030-EX.
        b030FetchFirstDer();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //                   OR NOT IDSV0003-SUCCESSFUL-SQL
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B040-FETCH-NEXT-DER
            //              THRU B040-EX
            //             UNTIL IDSV0003-NOT-FOUND
            //                OR NOT IDSV0003-SUCCESSFUL-SQL
            while (!(idsv0003.getSqlcode().isNotFound() || !idsv0003.getSqlcode().isSuccessfulSql())) {
                b040FetchNextDer();
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
        }
    }

    /**Original name: B001-SELECT-DER<br>
	 * <pre>*******************************************************</pre>*/
    private void b001SelectDer() {
        // COB_CODE: MOVE RIC-ID-RICH               TO DER-ID-RICH.
        ws.getDettRich().setIdRich(rich.getRicIdRich());
        // COB_CODE: EXEC SQL
        //              SELECT TP_AREA_D_RICH
        //                      ,AREA_D_RICH
        //              INTO
        //                     :DER-TP-AREA-D-RICH
        //                     :IND-DER-TP-AREA-D-RICH
        //                    ,:DER-AREA-D-RICH-VCHAR
        //                     :IND-DER-AREA-D-RICH
        //              FROM  DETT_RICH
        //              WHERE ID_RICH       = :DER-ID-RICH
        //              ORDER BY PROG_DETT_RICH
        //              FETCH FIRST ROW ONLY
        //           END-EXEC
        dettRichDao.selectByDerIdRich(ws.getDettRich().getIdRich(), ws);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //                                  TO IABV0009-BLOB-DATA-REC
        //           END-IF
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF IND-DER-TP-AREA-D-RICH = -1
            //              MOVE HIGH-VALUES TO DER-TP-AREA-D-RICH-NULL
            //           END-IF
            if (ws.getIdbvder2().getDerTpAreaDRich() == -1) {
                // COB_CODE: MOVE HIGH-VALUES TO DER-TP-AREA-D-RICH-NULL
                ws.getDettRich().setTpAreaDRich(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettRich.Len.TP_AREA_D_RICH));
            }
            // COB_CODE: IF IND-DER-AREA-D-RICH = -1
            //              MOVE HIGH-VALUES TO DER-AREA-D-RICH
            //           END-IF
            if (ws.getIdbvder2().getDerAreaDRich() == -1) {
                // COB_CODE: MOVE HIGH-VALUES TO DER-AREA-D-RICH
                ws.getDettRich().setAreaDRich(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettRich.Len.AREA_D_RICH));
            }
            // COB_CODE: MOVE DER-TP-AREA-D-RICH
            //                               TO IABV0009-BLOB-DATA-TYPE-REC
            iabv0002.getIabv0009GestGuideService().setBlobDataTypeRec(ws.getDettRich().getTpAreaDRich());
            // COB_CODE: MOVE DER-AREA-D-RICH
            //                               TO IABV0009-BLOB-DATA-REC
            iabv0002.getIabv0009GestGuideService().setBlobDataRec(ws.getDettRich().getAreaDRich());
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              MOVE 'DETT_RICH' TO IDSV0003-NOME-TABELLA
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
            idsv0003.getReturnCode().setSqlError();
            // COB_CODE: MOVE DER-ID-RICH       TO WK-ID-RICH
            ws.setWkIdRich(TruncAbs.toInt(ws.getDettRich().getIdRich(), 9));
            // COB_CODE: MOVE SPACES TO IDSV0003-DESCRIZ-ERR-DB2
            //skipped translation for moving SPACES to IDSV0003-DESCRIZ-ERR-DB2; considered in STRING statement translation below
            // COB_CODE: STRING 'DETT_RICH NON TROVATA - ID-RICH :'
            //                  DELIMITED BY SIZE
            //                  WK-ID-RICH
            //                  DELIMITED BY SIZE
            //                  INTO
            //                  IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            idsv0003.getCampiEsito().setDescrizErrDb2("DETT_RICH NON TROVATA - ID-RICH :" + ws.getWkIdRichFormatted());
            // COB_CODE: MOVE 'IABS0900'  TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe("IABS0900");
            // COB_CODE: MOVE 'DETT_RICH' TO IDSV0003-NOME-TABELLA
            idsv0003.getCampiEsito().setNomeTabella("DETT_RICH");
        }
    }

    /**Original name: B010-DECLARE-CURSOR-DER<br>
	 * <pre>*******************************************************</pre>*/
    private void b010DeclareCursorDer() {
    // COB_CODE: EXEC SQL
    //             DECLARE C-DER CURSOR FOR
    //              SELECT TP_AREA_D_RICH
    //                      ,AREA_D_RICH
    //                FROM  DETT_RICH
    //               WHERE ID_RICH       = :DER-ID-RICH
    //                 AND COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
    //               ORDER BY PROG_DETT_RICH
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B020-OPEN-CURSOR-DER<br>
	 * <pre>*******************************************************</pre>*/
    private void b020OpenCursorDer() {
        // COB_CODE: PERFORM B010-DECLARE-CURSOR-DER     THRU B010-EX.
        b010DeclareCursorDer();
        // COB_CODE: EXEC SQL
        //                OPEN C-DER
        //           END-EXEC.
        dettRichDao.openCDer(ws.getDettRich().getIdRich(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B030-FETCH-FIRST-DER<br>
	 * <pre>*******************************************************</pre>*/
    private void b030FetchFirstDer() {
        // COB_CODE: PERFORM B020-OPEN-CURSOR-DER        THRU B020-EX.
        b020OpenCursorDer();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B040-FETCH-NEXT-DER      THRU B040-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B040-FETCH-NEXT-DER      THRU B040-EX
            b040FetchNextDer();
        }
    }

    /**Original name: B040-FETCH-NEXT-DER<br>
	 * <pre>*******************************************************</pre>*/
    private void b040FetchNextDer() {
        // COB_CODE: EXEC SQL
        //                FETCH C-DER
        //           INTO
        //                :DER-TP-AREA-D-RICH
        //                :IND-DER-TP-AREA-D-RICH
        //               ,:DER-AREA-D-RICH-VCHAR
        //                :IND-DER-AREA-D-RICH
        //           END-EXEC.
        dettRichDao.fetchCDer(ws);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //                TO IABV0003-BLOB-DATA-REC(IABV0003-ELE-MAX)
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF IND-DER-TP-AREA-D-RICH = -1
            //              MOVE HIGH-VALUES TO DER-TP-AREA-D-RICH-NULL
            //           END-IF
            if (ws.getIdbvder2().getDerTpAreaDRich() == -1) {
                // COB_CODE: MOVE HIGH-VALUES TO DER-TP-AREA-D-RICH-NULL
                ws.getDettRich().setTpAreaDRich(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettRich.Len.TP_AREA_D_RICH));
            }
            // COB_CODE: IF IND-DER-AREA-D-RICH = -1
            //              MOVE HIGH-VALUES TO DER-AREA-D-RICH
            //           END-IF
            if (ws.getIdbvder2().getDerAreaDRich() == -1) {
                // COB_CODE: MOVE HIGH-VALUES TO DER-AREA-D-RICH
                ws.getDettRich().setAreaDRich(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettRich.Len.AREA_D_RICH));
            }
            // COB_CODE: ADD  1
            //             TO IABV0003-ELE-MAX
            iabv0003.setIabv0003EleMax(Trunc.toShort(1 + iabv0003.getIabv0003EleMax(), 3));
            // COB_CODE: MOVE DER-TP-AREA-D-RICH
            //             TO IABV0003-BLOB-DATA-TYPE-REC(IABV0003-ELE-MAX)
            iabv0003.getIabv0003BlobDataArray().setDataTypeRec(iabv0003.getIabv0003EleMax(), ws.getDettRich().getTpAreaDRich());
            // COB_CODE: MOVE DER-AREA-D-RICH
            //             TO IABV0003-BLOB-DATA-REC(IABV0003-ELE-MAX)
            iabv0003.getIabv0003BlobDataArray().setDataRec(iabv0003.getIabv0003EleMax(), ws.getDettRich().getAreaDRich());
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B050-CLOSE-CURSOR-DER     THRU B050-EX
            b050CloseCursorDer();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B050-CLOSE-CURSOR-DER<br>
	 * <pre>*****************************************************************</pre>*/
    private void b050CloseCursorDer() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-DER
        //           END-EXEC.
        dettRichDao.closeCDer();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: SC01-SELECTION-CURSOR-01<br>
	 * <pre>*****************************************************************</pre>*/
    private void sc01SelectionCursor01() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-SC01            THRU A310-SC01-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A360-OPEN-CURSOR-SC01       THRU A360-SC01-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A370-CLOSE-CURSOR-SC01      THRU A370-SC01-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A380-FETCH-FIRST-SC01       THRU A380-SC01-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A390-FETCH-NEXT-SC01        THRU A390-SC01-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A320-UPDATE-SC01            THRU A320-SC01-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-SC01            THRU A310-SC01-EX
            a310SelectSc01();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A360-OPEN-CURSOR-SC01       THRU A360-SC01-EX
            a360OpenCursorSc01();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC01      THRU A370-SC01-EX
            a370CloseCursorSc01();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A380-FETCH-FIRST-SC01       THRU A380-SC01-EX
            a380FetchFirstSc01();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-SC01        THRU A390-SC01-EX
            a390FetchNextSc01();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A320-UPDATE-SC01            THRU A320-SC01-EX
            a320UpdateSc01();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: SC02-SELECTION-CURSOR-02<br>
	 * <pre>*****************************************************************</pre>*/
    private void sc02SelectionCursor02() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-SC02            THRU A310-SC02-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A330-UPDATE-SC02            THRU A330-SC02-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A350-INSERT-SC02            THRU A350-SC02-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-SC02            THRU A310-SC02-EX
            a310SelectSc02();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A330-UPDATE-SC02            THRU A330-SC02-EX
            a330UpdateSc02();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A350-INSERT-SC02            THRU A350-SC02-EX
            a350InsertSc02();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-RIC-IB-RICH = -1
        //              MOVE HIGH-VALUES TO RIC-IB-RICH-NULL
        //           END-IF
        if (ws.getIndRich().getCodStrDato2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-IB-RICH-NULL
            rich.setRicIbRich(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_IB_RICH));
        }
        // COB_CODE: IF IND-RIC-TS-EFF-ESEC-RICH = -1
        //              MOVE HIGH-VALUES TO RIC-TS-EFF-ESEC-RICH-NULL
        //           END-IF
        if (ws.getIndRich().getCodDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-TS-EFF-ESEC-RICH-NULL
            rich.getRicTsEffEsecRich().setRicTsEffEsecRichNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RicTsEffEsecRich.Len.RIC_TS_EFF_ESEC_RICH_NULL));
        }
        // COB_CODE: IF IND-RIC-ID-OGG = -1
        //              MOVE HIGH-VALUES TO RIC-ID-OGG-NULL
        //           END-IF
        if (ws.getIndRich().getFlagKey() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-ID-OGG-NULL
            rich.getRicIdOgg().setRicIdOggNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RicIdOgg.Len.RIC_ID_OGG_NULL));
        }
        // COB_CODE: IF IND-RIC-TP-OGG = -1
        //              MOVE HIGH-VALUES TO RIC-TP-OGG-NULL
        //           END-IF
        if (ws.getIndRich().getFlagReturnCode() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-TP-OGG-NULL
            rich.setRicTpOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_TP_OGG));
        }
        // COB_CODE: IF IND-RIC-IB-POLI = -1
        //              MOVE HIGH-VALUES TO RIC-IB-POLI-NULL
        //           END-IF
        if (ws.getIndRich().getFlagCallUsing() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-IB-POLI-NULL
            rich.setRicIbPoli(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_IB_POLI));
        }
        // COB_CODE: IF IND-RIC-IB-ADES = -1
        //              MOVE HIGH-VALUES TO RIC-IB-ADES-NULL
        //           END-IF
        if (ws.getIndRich().getFlagWhereCond() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-IB-ADES-NULL
            rich.setRicIbAdes(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_IB_ADES));
        }
        // COB_CODE: IF IND-RIC-IB-GAR = -1
        //              MOVE HIGH-VALUES TO RIC-IB-GAR-NULL
        //           END-IF
        if (ws.getIndRich().getFlagRedefines() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-IB-GAR-NULL
            rich.setRicIbGar(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_IB_GAR));
        }
        // COB_CODE: IF IND-RIC-IB-TRCH-DI-GAR = -1
        //              MOVE HIGH-VALUES TO RIC-IB-TRCH-DI-GAR-NULL
        //           END-IF
        if (ws.getIndRich().getTipoDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-IB-TRCH-DI-GAR-NULL
            rich.setRicIbTrchDiGar(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_IB_TRCH_DI_GAR));
        }
        // COB_CODE: IF IND-RIC-ID-BATCH = -1
        //              MOVE HIGH-VALUES TO RIC-ID-BATCH-NULL
        //           END-IF
        if (ws.getIndRich().getLunghezzaDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-ID-BATCH-NULL
            rich.getRicIdBatchRP().setRicIdBatchNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RicIdBatch.Len.RIC_ID_BATCH_NULL));
        }
        // COB_CODE: IF IND-RIC-ID-JOB = -1
        //              MOVE HIGH-VALUES TO RIC-ID-JOB-NULL
        //           END-IF
        if (ws.getIndRich().getPrecisioneDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-ID-JOB-NULL
            rich.getRicIdJob().setRicIdJobNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RicIdJob.Len.RIC_ID_JOB_NULL));
        }
        // COB_CODE: IF IND-RIC-FL-SIMULAZIONE = -1
        //              MOVE HIGH-VALUES TO RIC-FL-SIMULAZIONE-NULL
        //           END-IF
        if (ws.getIndRich().getCodDominio() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-FL-SIMULAZIONE-NULL
            rich.setRicFlSimulazione(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RIC-KEY-ORDINAMENTO = -1
        //              MOVE HIGH-VALUES TO RIC-KEY-ORDINAMENTO
        //           END-IF
        if (ws.getIndRich().getFormattazioneDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-KEY-ORDINAMENTO
            rich.setRicKeyOrdinamento(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_KEY_ORDINAMENTO));
        }
        // COB_CODE: IF IND-RIC-ID-RICH-COLLG = -1
        //              MOVE HIGH-VALUES TO RIC-ID-RICH-COLLG-NULL
        //           END-IF
        if (ws.getIndRich().getServizioConvers() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-ID-RICH-COLLG-NULL
            rich.getRicIdRichCollg().setRicIdRichCollgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RicIdRichCollg.Len.RIC_ID_RICH_COLLG_NULL));
        }
        // COB_CODE: IF IND-RIC-TP-RAMO-BILA = -1
        //              MOVE HIGH-VALUES TO RIC-TP-RAMO-BILA-NULL
        //           END-IF
        if (ws.getIndRich().getAreaConvStandard() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-TP-RAMO-BILA-NULL
            rich.setRicTpRamoBila(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_TP_RAMO_BILA));
        }
        // COB_CODE: IF IND-RIC-TP-FRM-ASSVA = -1
        //              MOVE HIGH-VALUES TO RIC-TP-FRM-ASSVA-NULL
        //           END-IF
        if (ws.getIndRich().getRicorrenza() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-TP-FRM-ASSVA-NULL
            rich.setRicTpFrmAssva(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_TP_FRM_ASSVA));
        }
        // COB_CODE: IF IND-RIC-TP-CALC-RIS = -1
        //              MOVE HIGH-VALUES TO RIC-TP-CALC-RIS-NULL
        //           END-IF
        if (ws.getIndRich().getObbligatorieta() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-TP-CALC-RIS-NULL
            rich.setRicTpCalcRis(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_TP_CALC_RIS));
        }
        // COB_CODE: IF IND-RIC-RAMO-BILA = -1
        //              MOVE HIGH-VALUES TO RIC-RAMO-BILA-NULL
        //           END-IF.
        if (ws.getIndRich().getValoreDefault() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-RAMO-BILA-NULL
            rich.setRicRamoBila(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_RAMO_BILA));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>
	 * <pre>*****************************************************************</pre>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE IDSV0003-OPERAZIONE       TO RIC-DS-OPER-SQL
        rich.setRicDsOperSqlFormatted(idsv0003.getOperazione().getOperazioneFormatted());
        // COB_CODE: MOVE 1                         TO RIC-DS-VER
        rich.setRicDsVer(1);
        // COB_CODE: MOVE IDSV0003-USER-NAME        TO RIC-DS-UTENTE
        rich.setRicDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR TO RIC-DS-TS-CPTZ.
        rich.setRicDsTsCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF RIC-IB-RICH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIC-IB-RICH
        //           ELSE
        //              MOVE 0 TO IND-RIC-IB-RICH
        //           END-IF
        if (Characters.EQ_HIGH.test(rich.getRicIbRich(), RichIabs0900.Len.RIC_IB_RICH)) {
            // COB_CODE: MOVE -1 TO IND-RIC-IB-RICH
            ws.getIndRich().setCodStrDato2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIC-IB-RICH
            ws.getIndRich().setCodStrDato2(((short)0));
        }
        // COB_CODE: IF RIC-TS-EFF-ESEC-RICH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIC-TS-EFF-ESEC-RICH
        //           ELSE
        //              MOVE 0 TO IND-RIC-TS-EFF-ESEC-RICH
        //           END-IF
        if (Characters.EQ_HIGH.test(rich.getRicTsEffEsecRich().getRicTsEffEsecRichNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIC-TS-EFF-ESEC-RICH
            ws.getIndRich().setCodDato(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIC-TS-EFF-ESEC-RICH
            ws.getIndRich().setCodDato(((short)0));
        }
        // COB_CODE: IF RIC-ID-OGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIC-ID-OGG
        //           ELSE
        //              MOVE 0 TO IND-RIC-ID-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(rich.getRicIdOgg().getRicIdOggNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIC-ID-OGG
            ws.getIndRich().setFlagKey(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIC-ID-OGG
            ws.getIndRich().setFlagKey(((short)0));
        }
        // COB_CODE: IF RIC-TP-OGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIC-TP-OGG
        //           ELSE
        //              MOVE 0 TO IND-RIC-TP-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(rich.getRicTpOggFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIC-TP-OGG
            ws.getIndRich().setFlagReturnCode(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIC-TP-OGG
            ws.getIndRich().setFlagReturnCode(((short)0));
        }
        // COB_CODE: IF RIC-IB-POLI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIC-IB-POLI
        //           ELSE
        //              MOVE 0 TO IND-RIC-IB-POLI
        //           END-IF
        if (Characters.EQ_HIGH.test(rich.getRicIbPoli(), RichIabs0900.Len.RIC_IB_POLI)) {
            // COB_CODE: MOVE -1 TO IND-RIC-IB-POLI
            ws.getIndRich().setFlagCallUsing(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIC-IB-POLI
            ws.getIndRich().setFlagCallUsing(((short)0));
        }
        // COB_CODE: IF RIC-IB-ADES-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIC-IB-ADES
        //           ELSE
        //              MOVE 0 TO IND-RIC-IB-ADES
        //           END-IF
        if (Characters.EQ_HIGH.test(rich.getRicIbAdes(), RichIabs0900.Len.RIC_IB_ADES)) {
            // COB_CODE: MOVE -1 TO IND-RIC-IB-ADES
            ws.getIndRich().setFlagWhereCond(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIC-IB-ADES
            ws.getIndRich().setFlagWhereCond(((short)0));
        }
        // COB_CODE: IF RIC-IB-GAR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIC-IB-GAR
        //           ELSE
        //              MOVE 0 TO IND-RIC-IB-GAR
        //           END-IF
        if (Characters.EQ_HIGH.test(rich.getRicIbGar(), RichIabs0900.Len.RIC_IB_GAR)) {
            // COB_CODE: MOVE -1 TO IND-RIC-IB-GAR
            ws.getIndRich().setFlagRedefines(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIC-IB-GAR
            ws.getIndRich().setFlagRedefines(((short)0));
        }
        // COB_CODE: IF RIC-IB-TRCH-DI-GAR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIC-IB-TRCH-DI-GAR
        //           ELSE
        //              MOVE 0 TO IND-RIC-IB-TRCH-DI-GAR
        //           END-IF
        if (Characters.EQ_HIGH.test(rich.getRicIbTrchDiGar(), RichIabs0900.Len.RIC_IB_TRCH_DI_GAR)) {
            // COB_CODE: MOVE -1 TO IND-RIC-IB-TRCH-DI-GAR
            ws.getIndRich().setTipoDato(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIC-IB-TRCH-DI-GAR
            ws.getIndRich().setTipoDato(((short)0));
        }
        // COB_CODE: IF RIC-ID-BATCH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIC-ID-BATCH
        //           ELSE
        //              MOVE 0 TO IND-RIC-ID-BATCH
        //           END-IF
        if (Characters.EQ_HIGH.test(rich.getRicIdBatchRP().getRicIdBatchNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIC-ID-BATCH
            ws.getIndRich().setLunghezzaDato(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIC-ID-BATCH
            ws.getIndRich().setLunghezzaDato(((short)0));
        }
        // COB_CODE: IF RIC-ID-JOB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIC-ID-JOB
        //           ELSE
        //              MOVE 0 TO IND-RIC-ID-JOB
        //           END-IF
        if (Characters.EQ_HIGH.test(rich.getRicIdJob().getRicIdJobNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIC-ID-JOB
            ws.getIndRich().setPrecisioneDato(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIC-ID-JOB
            ws.getIndRich().setPrecisioneDato(((short)0));
        }
        // COB_CODE: IF RIC-FL-SIMULAZIONE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIC-FL-SIMULAZIONE
        //           ELSE
        //              MOVE 0 TO IND-RIC-FL-SIMULAZIONE
        //           END-IF
        if (Conditions.eq(rich.getRicFlSimulazione(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-RIC-FL-SIMULAZIONE
            ws.getIndRich().setCodDominio(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIC-FL-SIMULAZIONE
            ws.getIndRich().setCodDominio(((short)0));
        }
        // COB_CODE: IF RIC-KEY-ORDINAMENTO = HIGH-VALUES
        //              MOVE -1 TO IND-RIC-KEY-ORDINAMENTO
        //           ELSE
        //              MOVE 0 TO IND-RIC-KEY-ORDINAMENTO
        //           END-IF
        if (Characters.EQ_HIGH.test(rich.getRicKeyOrdinamento(), RichIabs0900.Len.RIC_KEY_ORDINAMENTO)) {
            // COB_CODE: MOVE -1 TO IND-RIC-KEY-ORDINAMENTO
            ws.getIndRich().setFormattazioneDato(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIC-KEY-ORDINAMENTO
            ws.getIndRich().setFormattazioneDato(((short)0));
        }
        // COB_CODE: IF RIC-ID-RICH-COLLG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIC-ID-RICH-COLLG
        //           ELSE
        //              MOVE 0 TO IND-RIC-ID-RICH-COLLG
        //           END-IF
        if (Characters.EQ_HIGH.test(rich.getRicIdRichCollg().getRicIdRichCollgNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIC-ID-RICH-COLLG
            ws.getIndRich().setServizioConvers(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIC-ID-RICH-COLLG
            ws.getIndRich().setServizioConvers(((short)0));
        }
        // COB_CODE: IF RIC-TP-RAMO-BILA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIC-TP-RAMO-BILA
        //           ELSE
        //              MOVE 0 TO IND-RIC-TP-RAMO-BILA
        //           END-IF
        if (Characters.EQ_HIGH.test(rich.getRicTpRamoBilaFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIC-TP-RAMO-BILA
            ws.getIndRich().setAreaConvStandard(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIC-TP-RAMO-BILA
            ws.getIndRich().setAreaConvStandard(((short)0));
        }
        // COB_CODE: IF RIC-TP-FRM-ASSVA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIC-TP-FRM-ASSVA
        //           ELSE
        //              MOVE 0 TO IND-RIC-TP-FRM-ASSVA
        //           END-IF
        if (Characters.EQ_HIGH.test(rich.getRicTpFrmAssvaFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIC-TP-FRM-ASSVA
            ws.getIndRich().setRicorrenza(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIC-TP-FRM-ASSVA
            ws.getIndRich().setRicorrenza(((short)0));
        }
        // COB_CODE: IF RIC-TP-CALC-RIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIC-TP-CALC-RIS
        //           ELSE
        //              MOVE 0 TO IND-RIC-TP-CALC-RIS
        //           END-IF
        if (Characters.EQ_HIGH.test(rich.getRicTpCalcRisFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIC-TP-CALC-RIS
            ws.getIndRich().setObbligatorieta(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIC-TP-CALC-RIS
            ws.getIndRich().setObbligatorieta(((short)0));
        }
        // COB_CODE: IF RIC-RAMO-BILA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIC-RAMO-BILA
        //           ELSE
        //              MOVE 0 TO IND-RIC-RAMO-BILA
        //           END-IF.
        if (Characters.EQ_HIGH.test(rich.getRicRamoBilaFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIC-RAMO-BILA
            ws.getIndRich().setValoreDefault(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIC-RAMO-BILA
            ws.getIndRich().setValoreDefault(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>
	 * <pre>*****************************************************************</pre>*/
    private void z400SeqRiga() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>*****************************************************************
	 * ----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE RIC-DT-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(rich.getRicDtEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO RIC-DT-EFF-DB
        ws.getRichDb().setEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE RIC-DT-RGSTRZ-RICH TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(rich.getRicDtRgstrzRich(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO RIC-DT-RGSTRZ-RICH-DB
        ws.getRichDb().setRgstrzRichDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE RIC-DT-PERV-RICH TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(rich.getRicDtPervRich(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO RIC-DT-PERV-RICH-DB
        ws.getRichDb().setPervRichDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE RIC-DT-ESEC-RICH TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(rich.getRicDtEsecRich(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO RIC-DT-ESEC-RICH-DB.
        ws.getRichDb().setEsecRichDb(ws.getIdsv0010().getWsDateX());
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>*****************************************************************
	 * ----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE RIC-DT-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRichDb().getEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RIC-DT-EFF
        rich.setRicDtEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE RIC-DT-RGSTRZ-RICH-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRichDb().getRgstrzRichDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RIC-DT-RGSTRZ-RICH
        rich.setRicDtRgstrzRich(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE RIC-DT-PERV-RICH-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRichDb().getPervRichDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RIC-DT-PERV-RICH
        rich.setRicDtPervRich(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE RIC-DT-ESEC-RICH-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRichDb().getEsecRichDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RIC-DT-ESEC-RICH.
        rich.setRicDtEsecRich(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>*****************************************************************
	 * ----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF RIC-KEY-ORDINAMENTO
        //                       TO RIC-KEY-ORDINAMENTO-LEN.
        rich.setRicKeyOrdinamentoLen(((short)RichIabs0900.Len.RIC_KEY_ORDINAMENTO));
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public String getAreaDRichVchar() {
        throw new FieldNotMappedException("areaDRichVchar");
    }

    @Override
    public void setAreaDRichVchar(String areaDRichVchar) {
        throw new FieldNotMappedException("areaDRichVchar");
    }

    @Override
    public String getAreaDRichVcharObj() {
        return getAreaDRichVchar();
    }

    @Override
    public void setAreaDRichVcharObj(String areaDRichVcharObj) {
        setAreaDRichVchar(areaDRichVcharObj);
    }

    @Override
    public int getCodCompAnia() {
        return rich.getRicCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.rich.setRicCodCompAnia(codCompAnia);
    }

    @Override
    public char getDsOperSql() {
        return rich.getRicDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.rich.setRicDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return rich.getRicDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.rich.setRicDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsCptz() {
        return rich.getRicDsTsCptz();
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        this.rich.setRicDsTsCptz(dsTsCptz);
    }

    @Override
    public String getDsUtente() {
        return rich.getRicDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.rich.setRicDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return rich.getRicDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.rich.setRicDsVer(dsVer);
    }

    @Override
    public String getDtEffDb() {
        return ws.getRichDb().getEffDb();
    }

    @Override
    public void setDtEffDb(String dtEffDb) {
        this.ws.getRichDb().setEffDb(dtEffDb);
    }

    @Override
    public String getDtEsecRichDb() {
        return ws.getRichDb().getEsecRichDb();
    }

    @Override
    public void setDtEsecRichDb(String dtEsecRichDb) {
        this.ws.getRichDb().setEsecRichDb(dtEsecRichDb);
    }

    @Override
    public String getDtPervRichDb() {
        return ws.getRichDb().getPervRichDb();
    }

    @Override
    public void setDtPervRichDb(String dtPervRichDb) {
        this.ws.getRichDb().setPervRichDb(dtPervRichDb);
    }

    @Override
    public String getDtRgstrzRichDb() {
        return ws.getRichDb().getRgstrzRichDb();
    }

    @Override
    public void setDtRgstrzRichDb(String dtRgstrzRichDb) {
        this.ws.getRichDb().setRgstrzRichDb(dtRgstrzRichDb);
    }

    @Override
    public char getFlSimulazione() {
        return rich.getRicFlSimulazione();
    }

    @Override
    public void setFlSimulazione(char flSimulazione) {
        this.rich.setRicFlSimulazione(flSimulazione);
    }

    @Override
    public Character getFlSimulazioneObj() {
        if (ws.getIndRich().getCodDominio() >= 0) {
            return ((Character)getFlSimulazione());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlSimulazioneObj(Character flSimulazioneObj) {
        if (flSimulazioneObj != null) {
            setFlSimulazione(((char)flSimulazioneObj));
            ws.getIndRich().setCodDominio(((short)0));
        }
        else {
            ws.getIndRich().setCodDominio(((short)-1));
        }
    }

    public Iabv0002 getIabv0002() {
        return iabv0002;
    }

    @Override
    public char getIabv0002State01() {
        return iabv0002.getIabv0002StateGlobal().getState01();
    }

    @Override
    public void setIabv0002State01(char iabv0002State01) {
        this.iabv0002.getIabv0002StateGlobal().setState01(iabv0002State01);
    }

    @Override
    public char getIabv0002State02() {
        return iabv0002.getIabv0002StateGlobal().getState02();
    }

    @Override
    public void setIabv0002State02(char iabv0002State02) {
        this.iabv0002.getIabv0002StateGlobal().setState02(iabv0002State02);
    }

    @Override
    public char getIabv0002State03() {
        return iabv0002.getIabv0002StateGlobal().getState03();
    }

    @Override
    public void setIabv0002State03(char iabv0002State03) {
        this.iabv0002.getIabv0002StateGlobal().setState03(iabv0002State03);
    }

    @Override
    public char getIabv0002State04() {
        return iabv0002.getIabv0002StateGlobal().getState04();
    }

    @Override
    public void setIabv0002State04(char iabv0002State04) {
        this.iabv0002.getIabv0002StateGlobal().setState04(iabv0002State04);
    }

    @Override
    public char getIabv0002State05() {
        return iabv0002.getIabv0002StateGlobal().getState05();
    }

    @Override
    public void setIabv0002State05(char iabv0002State05) {
        this.iabv0002.getIabv0002StateGlobal().setState05(iabv0002State05);
    }

    @Override
    public char getIabv0002State06() {
        return iabv0002.getIabv0002StateGlobal().getState06();
    }

    @Override
    public void setIabv0002State06(char iabv0002State06) {
        this.iabv0002.getIabv0002StateGlobal().setState06(iabv0002State06);
    }

    @Override
    public char getIabv0002State07() {
        return iabv0002.getIabv0002StateGlobal().getState07();
    }

    @Override
    public void setIabv0002State07(char iabv0002State07) {
        this.iabv0002.getIabv0002StateGlobal().setState07(iabv0002State07);
    }

    @Override
    public char getIabv0002State08() {
        return iabv0002.getIabv0002StateGlobal().getState08();
    }

    @Override
    public void setIabv0002State08(char iabv0002State08) {
        this.iabv0002.getIabv0002StateGlobal().setState08(iabv0002State08);
    }

    @Override
    public char getIabv0002State09() {
        return iabv0002.getIabv0002StateGlobal().getState09();
    }

    @Override
    public void setIabv0002State09(char iabv0002State09) {
        this.iabv0002.getIabv0002StateGlobal().setState09(iabv0002State09);
    }

    @Override
    public char getIabv0002State10() {
        return iabv0002.getIabv0002StateGlobal().getState10();
    }

    @Override
    public void setIabv0002State10(char iabv0002State10) {
        this.iabv0002.getIabv0002StateGlobal().setState10(iabv0002State10);
    }

    @Override
    public char getIabv0002StateCurrent() {
        return iabv0002.getIabv0002StateCurrent();
    }

    @Override
    public void setIabv0002StateCurrent(char iabv0002StateCurrent) {
        this.iabv0002.setIabv0002StateCurrent(iabv0002StateCurrent);
    }

    @Override
    public String getIbAdes() {
        return rich.getRicIbAdes();
    }

    @Override
    public void setIbAdes(String ibAdes) {
        this.rich.setRicIbAdes(ibAdes);
    }

    @Override
    public String getIbAdesObj() {
        if (ws.getIndRich().getFlagWhereCond() >= 0) {
            return getIbAdes();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbAdesObj(String ibAdesObj) {
        if (ibAdesObj != null) {
            setIbAdes(ibAdesObj);
            ws.getIndRich().setFlagWhereCond(((short)0));
        }
        else {
            ws.getIndRich().setFlagWhereCond(((short)-1));
        }
    }

    @Override
    public String getIbGar() {
        return rich.getRicIbGar();
    }

    @Override
    public void setIbGar(String ibGar) {
        this.rich.setRicIbGar(ibGar);
    }

    @Override
    public String getIbGarObj() {
        if (ws.getIndRich().getFlagRedefines() >= 0) {
            return getIbGar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbGarObj(String ibGarObj) {
        if (ibGarObj != null) {
            setIbGar(ibGarObj);
            ws.getIndRich().setFlagRedefines(((short)0));
        }
        else {
            ws.getIndRich().setFlagRedefines(((short)-1));
        }
    }

    @Override
    public String getIbPoli() {
        return rich.getRicIbPoli();
    }

    @Override
    public void setIbPoli(String ibPoli) {
        this.rich.setRicIbPoli(ibPoli);
    }

    @Override
    public String getIbPoliObj() {
        if (ws.getIndRich().getFlagCallUsing() >= 0) {
            return getIbPoli();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbPoliObj(String ibPoliObj) {
        if (ibPoliObj != null) {
            setIbPoli(ibPoliObj);
            ws.getIndRich().setFlagCallUsing(((short)0));
        }
        else {
            ws.getIndRich().setFlagCallUsing(((short)-1));
        }
    }

    @Override
    public String getIbRich() {
        return rich.getRicIbRich();
    }

    @Override
    public void setIbRich(String ibRich) {
        this.rich.setRicIbRich(ibRich);
    }

    @Override
    public String getIbRichObj() {
        if (ws.getIndRich().getCodStrDato2() >= 0) {
            return getIbRich();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbRichObj(String ibRichObj) {
        if (ibRichObj != null) {
            setIbRich(ibRichObj);
            ws.getIndRich().setCodStrDato2(((short)0));
        }
        else {
            ws.getIndRich().setCodStrDato2(((short)-1));
        }
    }

    @Override
    public String getIbTrchDiGar() {
        return rich.getRicIbTrchDiGar();
    }

    @Override
    public void setIbTrchDiGar(String ibTrchDiGar) {
        this.rich.setRicIbTrchDiGar(ibTrchDiGar);
    }

    @Override
    public String getIbTrchDiGarObj() {
        if (ws.getIndRich().getTipoDato() >= 0) {
            return getIbTrchDiGar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbTrchDiGarObj(String ibTrchDiGarObj) {
        if (ibTrchDiGarObj != null) {
            setIbTrchDiGar(ibTrchDiGarObj);
            ws.getIndRich().setTipoDato(((short)0));
        }
        else {
            ws.getIndRich().setTipoDato(((short)-1));
        }
    }

    @Override
    public int getIdJob() {
        return rich.getRicIdJob().getRicIdJob();
    }

    @Override
    public void setIdJob(int idJob) {
        this.rich.getRicIdJob().setRicIdJob(idJob);
    }

    @Override
    public Integer getIdJobObj() {
        if (ws.getIndRich().getPrecisioneDato() >= 0) {
            return ((Integer)getIdJob());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdJobObj(Integer idJobObj) {
        if (idJobObj != null) {
            setIdJob(((int)idJobObj));
            ws.getIndRich().setPrecisioneDato(((short)0));
        }
        else {
            ws.getIndRich().setPrecisioneDato(((short)-1));
        }
    }

    @Override
    public int getIdOgg() {
        return rich.getRicIdOgg().getRicIdOgg();
    }

    @Override
    public void setIdOgg(int idOgg) {
        this.rich.getRicIdOgg().setRicIdOgg(idOgg);
    }

    @Override
    public Integer getIdOggObj() {
        if (ws.getIndRich().getFlagKey() >= 0) {
            return ((Integer)getIdOgg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdOggObj(Integer idOggObj) {
        if (idOggObj != null) {
            setIdOgg(((int)idOggObj));
            ws.getIndRich().setFlagKey(((short)0));
        }
        else {
            ws.getIndRich().setFlagKey(((short)-1));
        }
    }

    @Override
    public int getIdRichCollg() {
        return rich.getRicIdRichCollg().getRicIdRichCollg();
    }

    @Override
    public void setIdRichCollg(int idRichCollg) {
        this.rich.getRicIdRichCollg().setRicIdRichCollg(idRichCollg);
    }

    @Override
    public Integer getIdRichCollgObj() {
        if (ws.getIndRich().getServizioConvers() >= 0) {
            return ((Integer)getIdRichCollg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRichCollgObj(Integer idRichCollgObj) {
        if (idRichCollgObj != null) {
            setIdRichCollg(((int)idRichCollgObj));
            ws.getIndRich().setServizioConvers(((short)0));
        }
        else {
            ws.getIndRich().setServizioConvers(((short)-1));
        }
    }

    public Idsv0003 getIdsv0003() {
        return idsv0003;
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public String getKeyOrdinamentoVchar() {
        return rich.getRicKeyOrdinamentoVcharFormatted();
    }

    @Override
    public void setKeyOrdinamentoVchar(String keyOrdinamentoVchar) {
        this.rich.setRicKeyOrdinamentoVcharFormatted(keyOrdinamentoVchar);
    }

    @Override
    public String getKeyOrdinamentoVcharObj() {
        if (ws.getIndRich().getFormattazioneDato() >= 0) {
            return getKeyOrdinamentoVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setKeyOrdinamentoVcharObj(String keyOrdinamentoVcharObj) {
        if (keyOrdinamentoVcharObj != null) {
            setKeyOrdinamentoVchar(keyOrdinamentoVcharObj);
            ws.getIndRich().setFormattazioneDato(((short)0));
        }
        else {
            ws.getIndRich().setFormattazioneDato(((short)-1));
        }
    }

    @Override
    public char getLdbv4511StatoElab00() {
        throw new FieldNotMappedException("ldbv4511StatoElab00");
    }

    @Override
    public void setLdbv4511StatoElab00(char ldbv4511StatoElab00) {
        throw new FieldNotMappedException("ldbv4511StatoElab00");
    }

    @Override
    public char getLdbv4511StatoElab01() {
        throw new FieldNotMappedException("ldbv4511StatoElab01");
    }

    @Override
    public void setLdbv4511StatoElab01(char ldbv4511StatoElab01) {
        throw new FieldNotMappedException("ldbv4511StatoElab01");
    }

    @Override
    public char getLdbv4511StatoElab02() {
        throw new FieldNotMappedException("ldbv4511StatoElab02");
    }

    @Override
    public void setLdbv4511StatoElab02(char ldbv4511StatoElab02) {
        throw new FieldNotMappedException("ldbv4511StatoElab02");
    }

    @Override
    public char getLdbv4511StatoElab03() {
        throw new FieldNotMappedException("ldbv4511StatoElab03");
    }

    @Override
    public void setLdbv4511StatoElab03(char ldbv4511StatoElab03) {
        throw new FieldNotMappedException("ldbv4511StatoElab03");
    }

    @Override
    public char getLdbv4511StatoElab04() {
        throw new FieldNotMappedException("ldbv4511StatoElab04");
    }

    @Override
    public void setLdbv4511StatoElab04(char ldbv4511StatoElab04) {
        throw new FieldNotMappedException("ldbv4511StatoElab04");
    }

    @Override
    public char getLdbv4511StatoElab05() {
        throw new FieldNotMappedException("ldbv4511StatoElab05");
    }

    @Override
    public void setLdbv4511StatoElab05(char ldbv4511StatoElab05) {
        throw new FieldNotMappedException("ldbv4511StatoElab05");
    }

    @Override
    public char getLdbv4511StatoElab06() {
        throw new FieldNotMappedException("ldbv4511StatoElab06");
    }

    @Override
    public void setLdbv4511StatoElab06(char ldbv4511StatoElab06) {
        throw new FieldNotMappedException("ldbv4511StatoElab06");
    }

    @Override
    public char getLdbv4511StatoElab07() {
        throw new FieldNotMappedException("ldbv4511StatoElab07");
    }

    @Override
    public void setLdbv4511StatoElab07(char ldbv4511StatoElab07) {
        throw new FieldNotMappedException("ldbv4511StatoElab07");
    }

    @Override
    public char getLdbv4511StatoElab08() {
        throw new FieldNotMappedException("ldbv4511StatoElab08");
    }

    @Override
    public void setLdbv4511StatoElab08(char ldbv4511StatoElab08) {
        throw new FieldNotMappedException("ldbv4511StatoElab08");
    }

    @Override
    public char getLdbv4511StatoElab09() {
        throw new FieldNotMappedException("ldbv4511StatoElab09");
    }

    @Override
    public void setLdbv4511StatoElab09(char ldbv4511StatoElab09) {
        throw new FieldNotMappedException("ldbv4511StatoElab09");
    }

    @Override
    public String getLdbv4511TpRich00() {
        throw new FieldNotMappedException("ldbv4511TpRich00");
    }

    @Override
    public void setLdbv4511TpRich00(String ldbv4511TpRich00) {
        throw new FieldNotMappedException("ldbv4511TpRich00");
    }

    @Override
    public String getLdbv4511TpRich01() {
        throw new FieldNotMappedException("ldbv4511TpRich01");
    }

    @Override
    public void setLdbv4511TpRich01(String ldbv4511TpRich01) {
        throw new FieldNotMappedException("ldbv4511TpRich01");
    }

    @Override
    public String getLdbv4511TpRich02() {
        throw new FieldNotMappedException("ldbv4511TpRich02");
    }

    @Override
    public void setLdbv4511TpRich02(String ldbv4511TpRich02) {
        throw new FieldNotMappedException("ldbv4511TpRich02");
    }

    @Override
    public String getLdbv4511TpRich03() {
        throw new FieldNotMappedException("ldbv4511TpRich03");
    }

    @Override
    public void setLdbv4511TpRich03(String ldbv4511TpRich03) {
        throw new FieldNotMappedException("ldbv4511TpRich03");
    }

    @Override
    public String getLdbv4511TpRich04() {
        throw new FieldNotMappedException("ldbv4511TpRich04");
    }

    @Override
    public void setLdbv4511TpRich04(String ldbv4511TpRich04) {
        throw new FieldNotMappedException("ldbv4511TpRich04");
    }

    @Override
    public String getLdbv4511TpRich05() {
        throw new FieldNotMappedException("ldbv4511TpRich05");
    }

    @Override
    public void setLdbv4511TpRich05(String ldbv4511TpRich05) {
        throw new FieldNotMappedException("ldbv4511TpRich05");
    }

    @Override
    public String getLdbv4511TpRich06() {
        throw new FieldNotMappedException("ldbv4511TpRich06");
    }

    @Override
    public void setLdbv4511TpRich06(String ldbv4511TpRich06) {
        throw new FieldNotMappedException("ldbv4511TpRich06");
    }

    @Override
    public String getLdbv4511TpRich07() {
        throw new FieldNotMappedException("ldbv4511TpRich07");
    }

    @Override
    public void setLdbv4511TpRich07(String ldbv4511TpRich07) {
        throw new FieldNotMappedException("ldbv4511TpRich07");
    }

    @Override
    public String getLdbv4511TpRich08() {
        throw new FieldNotMappedException("ldbv4511TpRich08");
    }

    @Override
    public void setLdbv4511TpRich08(String ldbv4511TpRich08) {
        throw new FieldNotMappedException("ldbv4511TpRich08");
    }

    @Override
    public String getLdbv4511TpRich09() {
        throw new FieldNotMappedException("ldbv4511TpRich09");
    }

    @Override
    public void setLdbv4511TpRich09(String ldbv4511TpRich09) {
        throw new FieldNotMappedException("ldbv4511TpRich09");
    }

    @Override
    public String getPrenotazione() {
        return ws.getPrenotazione();
    }

    @Override
    public void setPrenotazione(String prenotazione) {
        this.ws.setPrenotazione(prenotazione);
    }

    @Override
    public String getRamoBila() {
        return rich.getRicRamoBila();
    }

    @Override
    public void setRamoBila(String ramoBila) {
        this.rich.setRicRamoBila(ramoBila);
    }

    @Override
    public String getRamoBilaObj() {
        if (ws.getIndRich().getValoreDefault() >= 0) {
            return getRamoBila();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRamoBilaObj(String ramoBilaObj) {
        if (ramoBilaObj != null) {
            setRamoBila(ramoBilaObj);
            ws.getIndRich().setValoreDefault(((short)0));
        }
        else {
            ws.getIndRich().setValoreDefault(((short)-1));
        }
    }

    @Override
    public String getRicCodMacrofunct() {
        return rich.getRicCodMacrofunct();
    }

    @Override
    public void setRicCodMacrofunct(String ricCodMacrofunct) {
        this.rich.setRicCodMacrofunct(ricCodMacrofunct);
    }

    @Override
    public int getRicIdBatch() {
        return rich.getRicIdBatchRP().getRicIdBatch();
    }

    @Override
    public void setRicIdBatch(int ricIdBatch) {
        this.rich.getRicIdBatchRP().setRicIdBatch(ricIdBatch);
    }

    @Override
    public Integer getRicIdBatchObj() {
        if (ws.getIndRich().getLunghezzaDato() >= 0) {
            return ((Integer)getRicIdBatch());
        }
        else {
            return null;
        }
    }

    @Override
    public void setRicIdBatchObj(Integer ricIdBatchObj) {
        if (ricIdBatchObj != null) {
            setRicIdBatch(((int)ricIdBatchObj));
            ws.getIndRich().setLunghezzaDato(((short)0));
        }
        else {
            ws.getIndRich().setLunghezzaDato(((short)-1));
        }
    }

    @Override
    public int getRicIdRich() {
        return rich.getRicIdRich();
    }

    @Override
    public void setRicIdRich(int ricIdRich) {
        this.rich.setRicIdRich(ricIdRich);
    }

    @Override
    public int getRicTpMovi() {
        return rich.getRicTpMovi();
    }

    @Override
    public void setRicTpMovi(int ricTpMovi) {
        this.rich.setRicTpMovi(ricTpMovi);
    }

    public RichIabs0900 getRich() {
        return rich;
    }

    public RichIabs09001 getRichIabs09001() {
        return richIabs09001;
    }

    @Override
    public String getTpAreaDRich() {
        throw new FieldNotMappedException("tpAreaDRich");
    }

    @Override
    public void setTpAreaDRich(String tpAreaDRich) {
        throw new FieldNotMappedException("tpAreaDRich");
    }

    @Override
    public String getTpAreaDRichObj() {
        return getTpAreaDRich();
    }

    @Override
    public void setTpAreaDRichObj(String tpAreaDRichObj) {
        setTpAreaDRich(tpAreaDRichObj);
    }

    @Override
    public String getTpCalcRis() {
        return rich.getRicTpCalcRis();
    }

    @Override
    public void setTpCalcRis(String tpCalcRis) {
        this.rich.setRicTpCalcRis(tpCalcRis);
    }

    @Override
    public String getTpCalcRisObj() {
        if (ws.getIndRich().getObbligatorieta() >= 0) {
            return getTpCalcRis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCalcRisObj(String tpCalcRisObj) {
        if (tpCalcRisObj != null) {
            setTpCalcRis(tpCalcRisObj);
            ws.getIndRich().setObbligatorieta(((short)0));
        }
        else {
            ws.getIndRich().setObbligatorieta(((short)-1));
        }
    }

    @Override
    public String getTpFrmAssva() {
        return rich.getRicTpFrmAssva();
    }

    @Override
    public void setTpFrmAssva(String tpFrmAssva) {
        this.rich.setRicTpFrmAssva(tpFrmAssva);
    }

    @Override
    public String getTpFrmAssvaObj() {
        if (ws.getIndRich().getRicorrenza() >= 0) {
            return getTpFrmAssva();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpFrmAssvaObj(String tpFrmAssvaObj) {
        if (tpFrmAssvaObj != null) {
            setTpFrmAssva(tpFrmAssvaObj);
            ws.getIndRich().setRicorrenza(((short)0));
        }
        else {
            ws.getIndRich().setRicorrenza(((short)-1));
        }
    }

    @Override
    public String getTpOgg() {
        return rich.getRicTpOgg();
    }

    @Override
    public void setTpOgg(String tpOgg) {
        this.rich.setRicTpOgg(tpOgg);
    }

    @Override
    public String getTpOggObj() {
        if (ws.getIndRich().getFlagReturnCode() >= 0) {
            return getTpOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpOggObj(String tpOggObj) {
        if (tpOggObj != null) {
            setTpOgg(tpOggObj);
            ws.getIndRich().setFlagReturnCode(((short)0));
        }
        else {
            ws.getIndRich().setFlagReturnCode(((short)-1));
        }
    }

    @Override
    public String getTpRamoBila() {
        return rich.getRicTpRamoBila();
    }

    @Override
    public void setTpRamoBila(String tpRamoBila) {
        this.rich.setRicTpRamoBila(tpRamoBila);
    }

    @Override
    public String getTpRamoBilaObj() {
        if (ws.getIndRich().getAreaConvStandard() >= 0) {
            return getTpRamoBila();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRamoBilaObj(String tpRamoBilaObj) {
        if (tpRamoBilaObj != null) {
            setTpRamoBila(tpRamoBilaObj);
            ws.getIndRich().setAreaConvStandard(((short)0));
        }
        else {
            ws.getIndRich().setAreaConvStandard(((short)-1));
        }
    }

    @Override
    public String getTpRich() {
        return rich.getRicTpRich();
    }

    @Override
    public void setTpRich(String tpRich) {
        this.rich.setRicTpRich(tpRich);
    }

    @Override
    public long getTsEffEsecRich() {
        return rich.getRicTsEffEsecRich().getRicTsEffEsecRich();
    }

    @Override
    public void setTsEffEsecRich(long tsEffEsecRich) {
        this.rich.getRicTsEffEsecRich().setRicTsEffEsecRich(tsEffEsecRich);
    }

    @Override
    public Long getTsEffEsecRichObj() {
        if (ws.getIndRich().getCodDato() >= 0) {
            return ((Long)getTsEffEsecRich());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTsEffEsecRichObj(Long tsEffEsecRichObj) {
        if (tsEffEsecRichObj != null) {
            setTsEffEsecRich(((long)tsEffEsecRichObj));
            ws.getIndRich().setCodDato(((short)0));
        }
        else {
            ws.getIndRich().setCodDato(((short)-1));
        }
    }

    public Iabs0900Data getWs() {
        return ws;
    }
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.FieldNotMappedException;

import it.accenture.jnais.commons.data.to.IAdesPoliStatOggBusTrchDiGar;

/**Original name: AdesPoliStatOggBusTrchDiGarLdbm0250<br>*/
public class AdesPoliStatOggBusTrchDiGarLdbm02503 implements IAdesPoliStatOggBusTrchDiGar {

	//==== PROPERTIES ====
	private Ldbm0250 ldbm0250;

	//==== CONSTRUCTORS ====
	public AdesPoliStatOggBusTrchDiGarLdbm02503(Ldbm0250 ldbm0250) {
		this.ldbm0250 = ldbm0250;
	}

	//==== METHODS ====
	@Override
	public int getAdeCodCompAnia() {
		throw new FieldNotMappedException("adeCodCompAnia");
	}

	@Override
	public void setAdeCodCompAnia(int adeCodCompAnia) {
		throw new FieldNotMappedException("adeCodCompAnia");
	}

	@Override
	public char getAdeConcsPrest() {
		throw new FieldNotMappedException("adeConcsPrest");
	}

	@Override
	public void setAdeConcsPrest(char adeConcsPrest) {
		throw new FieldNotMappedException("adeConcsPrest");
	}

	@Override
	public Character getAdeConcsPrestObj() {
		return (getAdeConcsPrest());
	}

	@Override
	public void setAdeConcsPrestObj(Character adeConcsPrestObj) {
		setAdeConcsPrest((adeConcsPrestObj));
	}

	@Override
	public AfDecimal getAdeCumCnbtCap() {
		throw new FieldNotMappedException("adeCumCnbtCap");
	}

	@Override
	public void setAdeCumCnbtCap(AfDecimal adeCumCnbtCap) {
		throw new FieldNotMappedException("adeCumCnbtCap");
	}

	@Override
	public AfDecimal getAdeCumCnbtCapObj() {
		return getAdeCumCnbtCap();
	}

	@Override
	public void setAdeCumCnbtCapObj(AfDecimal adeCumCnbtCapObj) {
		setAdeCumCnbtCap(new AfDecimal(adeCumCnbtCapObj, 15, 3));
	}

	@Override
	public char getAdeDsOperSql() {
		throw new FieldNotMappedException("adeDsOperSql");
	}

	@Override
	public void setAdeDsOperSql(char adeDsOperSql) {
		throw new FieldNotMappedException("adeDsOperSql");
	}

	@Override
	public long getAdeDsRiga() {
		throw new FieldNotMappedException("adeDsRiga");
	}

	@Override
	public void setAdeDsRiga(long adeDsRiga) {
		throw new FieldNotMappedException("adeDsRiga");
	}

	@Override
	public char getAdeDsStatoElab() {
		throw new FieldNotMappedException("adeDsStatoElab");
	}

	@Override
	public void setAdeDsStatoElab(char adeDsStatoElab) {
		throw new FieldNotMappedException("adeDsStatoElab");
	}

	@Override
	public long getAdeDsTsEndCptz() {
		throw new FieldNotMappedException("adeDsTsEndCptz");
	}

	@Override
	public void setAdeDsTsEndCptz(long adeDsTsEndCptz) {
		throw new FieldNotMappedException("adeDsTsEndCptz");
	}

	@Override
	public long getAdeDsTsIniCptz() {
		throw new FieldNotMappedException("adeDsTsIniCptz");
	}

	@Override
	public void setAdeDsTsIniCptz(long adeDsTsIniCptz) {
		throw new FieldNotMappedException("adeDsTsIniCptz");
	}

	@Override
	public String getAdeDsUtente() {
		throw new FieldNotMappedException("adeDsUtente");
	}

	@Override
	public void setAdeDsUtente(String adeDsUtente) {
		throw new FieldNotMappedException("adeDsUtente");
	}

	@Override
	public int getAdeDsVer() {
		throw new FieldNotMappedException("adeDsVer");
	}

	@Override
	public void setAdeDsVer(int adeDsVer) {
		throw new FieldNotMappedException("adeDsVer");
	}

	@Override
	public String getAdeDtDecorDb() {
		throw new FieldNotMappedException("adeDtDecorDb");
	}

	@Override
	public void setAdeDtDecorDb(String adeDtDecorDb) {
		throw new FieldNotMappedException("adeDtDecorDb");
	}

	@Override
	public String getAdeDtDecorDbObj() {
		return getAdeDtDecorDb();
	}

	@Override
	public void setAdeDtDecorDbObj(String adeDtDecorDbObj) {
		setAdeDtDecorDb(adeDtDecorDbObj);
	}

	@Override
	public String getAdeDtDecorPrestBanDb() {
		throw new FieldNotMappedException("adeDtDecorPrestBanDb");
	}

	@Override
	public void setAdeDtDecorPrestBanDb(String adeDtDecorPrestBanDb) {
		throw new FieldNotMappedException("adeDtDecorPrestBanDb");
	}

	@Override
	public String getAdeDtDecorPrestBanDbObj() {
		return getAdeDtDecorPrestBanDb();
	}

	@Override
	public void setAdeDtDecorPrestBanDbObj(String adeDtDecorPrestBanDbObj) {
		setAdeDtDecorPrestBanDb(adeDtDecorPrestBanDbObj);
	}

	@Override
	public String getAdeDtEffVarzStatTDb() {
		throw new FieldNotMappedException("adeDtEffVarzStatTDb");
	}

	@Override
	public void setAdeDtEffVarzStatTDb(String adeDtEffVarzStatTDb) {
		throw new FieldNotMappedException("adeDtEffVarzStatTDb");
	}

	@Override
	public String getAdeDtEffVarzStatTDbObj() {
		return getAdeDtEffVarzStatTDb();
	}

	@Override
	public void setAdeDtEffVarzStatTDbObj(String adeDtEffVarzStatTDbObj) {
		setAdeDtEffVarzStatTDb(adeDtEffVarzStatTDbObj);
	}

	@Override
	public String getAdeDtEndEffDb() {
		throw new FieldNotMappedException("adeDtEndEffDb");
	}

	@Override
	public void setAdeDtEndEffDb(String adeDtEndEffDb) {
		throw new FieldNotMappedException("adeDtEndEffDb");
	}

	@Override
	public String getAdeDtIniEffDb() {
		throw new FieldNotMappedException("adeDtIniEffDb");
	}

	@Override
	public void setAdeDtIniEffDb(String adeDtIniEffDb) {
		throw new FieldNotMappedException("adeDtIniEffDb");
	}

	@Override
	public String getAdeDtNovaRgmFiscDb() {
		throw new FieldNotMappedException("adeDtNovaRgmFiscDb");
	}

	@Override
	public void setAdeDtNovaRgmFiscDb(String adeDtNovaRgmFiscDb) {
		throw new FieldNotMappedException("adeDtNovaRgmFiscDb");
	}

	@Override
	public String getAdeDtNovaRgmFiscDbObj() {
		return getAdeDtNovaRgmFiscDb();
	}

	@Override
	public void setAdeDtNovaRgmFiscDbObj(String adeDtNovaRgmFiscDbObj) {
		setAdeDtNovaRgmFiscDb(adeDtNovaRgmFiscDbObj);
	}

	@Override
	public String getAdeDtPrescDb() {
		throw new FieldNotMappedException("adeDtPrescDb");
	}

	@Override
	public void setAdeDtPrescDb(String adeDtPrescDb) {
		throw new FieldNotMappedException("adeDtPrescDb");
	}

	@Override
	public String getAdeDtPrescDbObj() {
		return getAdeDtPrescDb();
	}

	@Override
	public void setAdeDtPrescDbObj(String adeDtPrescDbObj) {
		setAdeDtPrescDb(adeDtPrescDbObj);
	}

	@Override
	public String getAdeDtScadDb() {
		throw new FieldNotMappedException("adeDtScadDb");
	}

	@Override
	public void setAdeDtScadDb(String adeDtScadDb) {
		throw new FieldNotMappedException("adeDtScadDb");
	}

	@Override
	public String getAdeDtScadDbObj() {
		return getAdeDtScadDb();
	}

	@Override
	public void setAdeDtScadDbObj(String adeDtScadDbObj) {
		setAdeDtScadDb(adeDtScadDbObj);
	}

	@Override
	public String getAdeDtUltConsCnbtDb() {
		throw new FieldNotMappedException("adeDtUltConsCnbtDb");
	}

	@Override
	public void setAdeDtUltConsCnbtDb(String adeDtUltConsCnbtDb) {
		throw new FieldNotMappedException("adeDtUltConsCnbtDb");
	}

	@Override
	public String getAdeDtUltConsCnbtDbObj() {
		return getAdeDtUltConsCnbtDb();
	}

	@Override
	public void setAdeDtUltConsCnbtDbObj(String adeDtUltConsCnbtDbObj) {
		setAdeDtUltConsCnbtDb(adeDtUltConsCnbtDbObj);
	}

	@Override
	public String getAdeDtVarzTpIasDb() {
		throw new FieldNotMappedException("adeDtVarzTpIasDb");
	}

	@Override
	public void setAdeDtVarzTpIasDb(String adeDtVarzTpIasDb) {
		throw new FieldNotMappedException("adeDtVarzTpIasDb");
	}

	@Override
	public String getAdeDtVarzTpIasDbObj() {
		return getAdeDtVarzTpIasDb();
	}

	@Override
	public void setAdeDtVarzTpIasDbObj(String adeDtVarzTpIasDbObj) {
		setAdeDtVarzTpIasDb(adeDtVarzTpIasDbObj);
	}

	@Override
	public int getAdeDurAa() {
		throw new FieldNotMappedException("adeDurAa");
	}

	@Override
	public void setAdeDurAa(int adeDurAa) {
		throw new FieldNotMappedException("adeDurAa");
	}

	@Override
	public Integer getAdeDurAaObj() {
		return (getAdeDurAa());
	}

	@Override
	public void setAdeDurAaObj(Integer adeDurAaObj) {
		setAdeDurAa((adeDurAaObj));
	}

	@Override
	public int getAdeDurGg() {
		throw new FieldNotMappedException("adeDurGg");
	}

	@Override
	public void setAdeDurGg(int adeDurGg) {
		throw new FieldNotMappedException("adeDurGg");
	}

	@Override
	public Integer getAdeDurGgObj() {
		return (getAdeDurGg());
	}

	@Override
	public void setAdeDurGgObj(Integer adeDurGgObj) {
		setAdeDurGg((adeDurGgObj));
	}

	@Override
	public int getAdeDurMm() {
		throw new FieldNotMappedException("adeDurMm");
	}

	@Override
	public void setAdeDurMm(int adeDurMm) {
		throw new FieldNotMappedException("adeDurMm");
	}

	@Override
	public Integer getAdeDurMmObj() {
		return (getAdeDurMm());
	}

	@Override
	public void setAdeDurMmObj(Integer adeDurMmObj) {
		setAdeDurMm((adeDurMmObj));
	}

	@Override
	public int getAdeEtaAScad() {
		throw new FieldNotMappedException("adeEtaAScad");
	}

	@Override
	public void setAdeEtaAScad(int adeEtaAScad) {
		throw new FieldNotMappedException("adeEtaAScad");
	}

	@Override
	public Integer getAdeEtaAScadObj() {
		return (getAdeEtaAScad());
	}

	@Override
	public void setAdeEtaAScadObj(Integer adeEtaAScadObj) {
		setAdeEtaAScad((adeEtaAScadObj));
	}

	@Override
	public char getAdeFlAttiv() {
		throw new FieldNotMappedException("adeFlAttiv");
	}

	@Override
	public void setAdeFlAttiv(char adeFlAttiv) {
		throw new FieldNotMappedException("adeFlAttiv");
	}

	@Override
	public Character getAdeFlAttivObj() {
		return (getAdeFlAttiv());
	}

	@Override
	public void setAdeFlAttivObj(Character adeFlAttivObj) {
		setAdeFlAttiv((adeFlAttivObj));
	}

	@Override
	public char getAdeFlCoincAssto() {
		throw new FieldNotMappedException("adeFlCoincAssto");
	}

	@Override
	public void setAdeFlCoincAssto(char adeFlCoincAssto) {
		throw new FieldNotMappedException("adeFlCoincAssto");
	}

	@Override
	public Character getAdeFlCoincAsstoObj() {
		return (getAdeFlCoincAssto());
	}

	@Override
	public void setAdeFlCoincAsstoObj(Character adeFlCoincAsstoObj) {
		setAdeFlCoincAssto((adeFlCoincAsstoObj));
	}

	@Override
	public char getAdeFlProvzaMigraz() {
		throw new FieldNotMappedException("adeFlProvzaMigraz");
	}

	@Override
	public void setAdeFlProvzaMigraz(char adeFlProvzaMigraz) {
		throw new FieldNotMappedException("adeFlProvzaMigraz");
	}

	@Override
	public Character getAdeFlProvzaMigrazObj() {
		return (getAdeFlProvzaMigraz());
	}

	@Override
	public void setAdeFlProvzaMigrazObj(Character adeFlProvzaMigrazObj) {
		setAdeFlProvzaMigraz((adeFlProvzaMigrazObj));
	}

	@Override
	public char getAdeFlVarzStatTbgc() {
		throw new FieldNotMappedException("adeFlVarzStatTbgc");
	}

	@Override
	public void setAdeFlVarzStatTbgc(char adeFlVarzStatTbgc) {
		throw new FieldNotMappedException("adeFlVarzStatTbgc");
	}

	@Override
	public Character getAdeFlVarzStatTbgcObj() {
		return (getAdeFlVarzStatTbgc());
	}

	@Override
	public void setAdeFlVarzStatTbgcObj(Character adeFlVarzStatTbgcObj) {
		setAdeFlVarzStatTbgc((adeFlVarzStatTbgcObj));
	}

	@Override
	public String getAdeIbDflt() {
		throw new FieldNotMappedException("adeIbDflt");
	}

	@Override
	public void setAdeIbDflt(String adeIbDflt) {
		throw new FieldNotMappedException("adeIbDflt");
	}

	@Override
	public String getAdeIbDfltObj() {
		return getAdeIbDflt();
	}

	@Override
	public void setAdeIbDfltObj(String adeIbDfltObj) {
		setAdeIbDflt(adeIbDfltObj);
	}

	@Override
	public String getAdeIbOgg() {
		throw new FieldNotMappedException("adeIbOgg");
	}

	@Override
	public void setAdeIbOgg(String adeIbOgg) {
		throw new FieldNotMappedException("adeIbOgg");
	}

	@Override
	public String getAdeIbOggObj() {
		return getAdeIbOgg();
	}

	@Override
	public void setAdeIbOggObj(String adeIbOggObj) {
		setAdeIbOgg(adeIbOggObj);
	}

	@Override
	public String getAdeIbPrev() {
		throw new FieldNotMappedException("adeIbPrev");
	}

	@Override
	public void setAdeIbPrev(String adeIbPrev) {
		throw new FieldNotMappedException("adeIbPrev");
	}

	@Override
	public String getAdeIbPrevObj() {
		return getAdeIbPrev();
	}

	@Override
	public void setAdeIbPrevObj(String adeIbPrevObj) {
		setAdeIbPrev(adeIbPrevObj);
	}

	@Override
	public int getAdeIdAdes() {
		throw new FieldNotMappedException("adeIdAdes");
	}

	@Override
	public void setAdeIdAdes(int adeIdAdes) {
		throw new FieldNotMappedException("adeIdAdes");
	}

	@Override
	public int getAdeIdMoviChiu() {
		throw new FieldNotMappedException("adeIdMoviChiu");
	}

	@Override
	public void setAdeIdMoviChiu(int adeIdMoviChiu) {
		throw new FieldNotMappedException("adeIdMoviChiu");
	}

	@Override
	public Integer getAdeIdMoviChiuObj() {
		return (getAdeIdMoviChiu());
	}

	@Override
	public void setAdeIdMoviChiuObj(Integer adeIdMoviChiuObj) {
		setAdeIdMoviChiu((adeIdMoviChiuObj));
	}

	@Override
	public int getAdeIdMoviCrz() {
		throw new FieldNotMappedException("adeIdMoviCrz");
	}

	@Override
	public void setAdeIdMoviCrz(int adeIdMoviCrz) {
		throw new FieldNotMappedException("adeIdMoviCrz");
	}

	@Override
	public int getAdeIdPoli() {
		throw new FieldNotMappedException("adeIdPoli");
	}

	@Override
	public void setAdeIdPoli(int adeIdPoli) {
		throw new FieldNotMappedException("adeIdPoli");
	}

	@Override
	public String getAdeIdenIscFnd() {
		throw new FieldNotMappedException("adeIdenIscFnd");
	}

	@Override
	public void setAdeIdenIscFnd(String adeIdenIscFnd) {
		throw new FieldNotMappedException("adeIdenIscFnd");
	}

	@Override
	public String getAdeIdenIscFndObj() {
		return getAdeIdenIscFnd();
	}

	@Override
	public void setAdeIdenIscFndObj(String adeIdenIscFndObj) {
		setAdeIdenIscFnd(adeIdenIscFndObj);
	}

	@Override
	public AfDecimal getAdeImpAder() {
		throw new FieldNotMappedException("adeImpAder");
	}

	@Override
	public void setAdeImpAder(AfDecimal adeImpAder) {
		throw new FieldNotMappedException("adeImpAder");
	}

	@Override
	public AfDecimal getAdeImpAderObj() {
		return getAdeImpAder();
	}

	@Override
	public void setAdeImpAderObj(AfDecimal adeImpAderObj) {
		setAdeImpAder(new AfDecimal(adeImpAderObj, 15, 3));
	}

	@Override
	public AfDecimal getAdeImpAz() {
		throw new FieldNotMappedException("adeImpAz");
	}

	@Override
	public void setAdeImpAz(AfDecimal adeImpAz) {
		throw new FieldNotMappedException("adeImpAz");
	}

	@Override
	public AfDecimal getAdeImpAzObj() {
		return getAdeImpAz();
	}

	@Override
	public void setAdeImpAzObj(AfDecimal adeImpAzObj) {
		setAdeImpAz(new AfDecimal(adeImpAzObj, 15, 3));
	}

	@Override
	public AfDecimal getAdeImpGarCnbt() {
		throw new FieldNotMappedException("adeImpGarCnbt");
	}

	@Override
	public void setAdeImpGarCnbt(AfDecimal adeImpGarCnbt) {
		throw new FieldNotMappedException("adeImpGarCnbt");
	}

	@Override
	public AfDecimal getAdeImpGarCnbtObj() {
		return getAdeImpGarCnbt();
	}

	@Override
	public void setAdeImpGarCnbtObj(AfDecimal adeImpGarCnbtObj) {
		setAdeImpGarCnbt(new AfDecimal(adeImpGarCnbtObj, 15, 3));
	}

	@Override
	public AfDecimal getAdeImpRecRitAcc() {
		throw new FieldNotMappedException("adeImpRecRitAcc");
	}

	@Override
	public void setAdeImpRecRitAcc(AfDecimal adeImpRecRitAcc) {
		throw new FieldNotMappedException("adeImpRecRitAcc");
	}

	@Override
	public AfDecimal getAdeImpRecRitAccObj() {
		return getAdeImpRecRitAcc();
	}

	@Override
	public void setAdeImpRecRitAccObj(AfDecimal adeImpRecRitAccObj) {
		setAdeImpRecRitAcc(new AfDecimal(adeImpRecRitAccObj, 15, 3));
	}

	@Override
	public AfDecimal getAdeImpRecRitVis() {
		throw new FieldNotMappedException("adeImpRecRitVis");
	}

	@Override
	public void setAdeImpRecRitVis(AfDecimal adeImpRecRitVis) {
		throw new FieldNotMappedException("adeImpRecRitVis");
	}

	@Override
	public AfDecimal getAdeImpRecRitVisObj() {
		return getAdeImpRecRitVis();
	}

	@Override
	public void setAdeImpRecRitVisObj(AfDecimal adeImpRecRitVisObj) {
		setAdeImpRecRitVis(new AfDecimal(adeImpRecRitVisObj, 15, 3));
	}

	@Override
	public AfDecimal getAdeImpTfr() {
		throw new FieldNotMappedException("adeImpTfr");
	}

	@Override
	public void setAdeImpTfr(AfDecimal adeImpTfr) {
		throw new FieldNotMappedException("adeImpTfr");
	}

	@Override
	public AfDecimal getAdeImpTfrObj() {
		return getAdeImpTfr();
	}

	@Override
	public void setAdeImpTfrObj(AfDecimal adeImpTfrObj) {
		setAdeImpTfr(new AfDecimal(adeImpTfrObj, 15, 3));
	}

	@Override
	public AfDecimal getAdeImpVolo() {
		throw new FieldNotMappedException("adeImpVolo");
	}

	@Override
	public void setAdeImpVolo(AfDecimal adeImpVolo) {
		throw new FieldNotMappedException("adeImpVolo");
	}

	@Override
	public AfDecimal getAdeImpVoloObj() {
		return getAdeImpVolo();
	}

	@Override
	public void setAdeImpVoloObj(AfDecimal adeImpVoloObj) {
		setAdeImpVolo(new AfDecimal(adeImpVoloObj, 15, 3));
	}

	@Override
	public AfDecimal getAdeImpbVisDaRec() {
		throw new FieldNotMappedException("adeImpbVisDaRec");
	}

	@Override
	public void setAdeImpbVisDaRec(AfDecimal adeImpbVisDaRec) {
		throw new FieldNotMappedException("adeImpbVisDaRec");
	}

	@Override
	public AfDecimal getAdeImpbVisDaRecObj() {
		return getAdeImpbVisDaRec();
	}

	@Override
	public void setAdeImpbVisDaRecObj(AfDecimal adeImpbVisDaRecObj) {
		setAdeImpbVisDaRec(new AfDecimal(adeImpbVisDaRecObj, 15, 3));
	}

	@Override
	public String getAdeModCalc() {
		throw new FieldNotMappedException("adeModCalc");
	}

	@Override
	public void setAdeModCalc(String adeModCalc) {
		throw new FieldNotMappedException("adeModCalc");
	}

	@Override
	public String getAdeModCalcObj() {
		return getAdeModCalc();
	}

	@Override
	public void setAdeModCalcObj(String adeModCalcObj) {
		setAdeModCalc(adeModCalcObj);
	}

	@Override
	public AfDecimal getAdeNumRatPian() {
		throw new FieldNotMappedException("adeNumRatPian");
	}

	@Override
	public void setAdeNumRatPian(AfDecimal adeNumRatPian) {
		throw new FieldNotMappedException("adeNumRatPian");
	}

	@Override
	public AfDecimal getAdeNumRatPianObj() {
		return getAdeNumRatPian();
	}

	@Override
	public void setAdeNumRatPianObj(AfDecimal adeNumRatPianObj) {
		setAdeNumRatPian(new AfDecimal(adeNumRatPianObj, 12, 5));
	}

	@Override
	public AfDecimal getAdePcAder() {
		throw new FieldNotMappedException("adePcAder");
	}

	@Override
	public void setAdePcAder(AfDecimal adePcAder) {
		throw new FieldNotMappedException("adePcAder");
	}

	@Override
	public AfDecimal getAdePcAderObj() {
		return getAdePcAder();
	}

	@Override
	public void setAdePcAderObj(AfDecimal adePcAderObj) {
		setAdePcAder(new AfDecimal(adePcAderObj, 6, 3));
	}

	@Override
	public AfDecimal getAdePcAz() {
		throw new FieldNotMappedException("adePcAz");
	}

	@Override
	public void setAdePcAz(AfDecimal adePcAz) {
		throw new FieldNotMappedException("adePcAz");
	}

	@Override
	public AfDecimal getAdePcAzObj() {
		return getAdePcAz();
	}

	@Override
	public void setAdePcAzObj(AfDecimal adePcAzObj) {
		setAdePcAz(new AfDecimal(adePcAzObj, 6, 3));
	}

	@Override
	public AfDecimal getAdePcTfr() {
		throw new FieldNotMappedException("adePcTfr");
	}

	@Override
	public void setAdePcTfr(AfDecimal adePcTfr) {
		throw new FieldNotMappedException("adePcTfr");
	}

	@Override
	public AfDecimal getAdePcTfrObj() {
		return getAdePcTfr();
	}

	@Override
	public void setAdePcTfrObj(AfDecimal adePcTfrObj) {
		setAdePcTfr(new AfDecimal(adePcTfrObj, 6, 3));
	}

	@Override
	public AfDecimal getAdePcVolo() {
		throw new FieldNotMappedException("adePcVolo");
	}

	@Override
	public void setAdePcVolo(AfDecimal adePcVolo) {
		throw new FieldNotMappedException("adePcVolo");
	}

	@Override
	public AfDecimal getAdePcVoloObj() {
		return getAdePcVolo();
	}

	@Override
	public void setAdePcVoloObj(AfDecimal adePcVoloObj) {
		setAdePcVolo(new AfDecimal(adePcVoloObj, 6, 3));
	}

	@Override
	public AfDecimal getAdePreLrdInd() {
		throw new FieldNotMappedException("adePreLrdInd");
	}

	@Override
	public void setAdePreLrdInd(AfDecimal adePreLrdInd) {
		throw new FieldNotMappedException("adePreLrdInd");
	}

	@Override
	public AfDecimal getAdePreLrdIndObj() {
		return getAdePreLrdInd();
	}

	@Override
	public void setAdePreLrdIndObj(AfDecimal adePreLrdIndObj) {
		setAdePreLrdInd(new AfDecimal(adePreLrdIndObj, 15, 3));
	}

	@Override
	public AfDecimal getAdePreNetInd() {
		throw new FieldNotMappedException("adePreNetInd");
	}

	@Override
	public void setAdePreNetInd(AfDecimal adePreNetInd) {
		throw new FieldNotMappedException("adePreNetInd");
	}

	@Override
	public AfDecimal getAdePreNetIndObj() {
		return getAdePreNetInd();
	}

	@Override
	public void setAdePreNetIndObj(AfDecimal adePreNetIndObj) {
		setAdePreNetInd(new AfDecimal(adePreNetIndObj, 15, 3));
	}

	@Override
	public AfDecimal getAdePrstzIniInd() {
		throw new FieldNotMappedException("adePrstzIniInd");
	}

	@Override
	public void setAdePrstzIniInd(AfDecimal adePrstzIniInd) {
		throw new FieldNotMappedException("adePrstzIniInd");
	}

	@Override
	public AfDecimal getAdePrstzIniIndObj() {
		return getAdePrstzIniInd();
	}

	@Override
	public void setAdePrstzIniIndObj(AfDecimal adePrstzIniIndObj) {
		setAdePrstzIniInd(new AfDecimal(adePrstzIniIndObj, 15, 3));
	}

	@Override
	public AfDecimal getAdeRatLrdInd() {
		throw new FieldNotMappedException("adeRatLrdInd");
	}

	@Override
	public void setAdeRatLrdInd(AfDecimal adeRatLrdInd) {
		throw new FieldNotMappedException("adeRatLrdInd");
	}

	@Override
	public AfDecimal getAdeRatLrdIndObj() {
		return getAdeRatLrdInd();
	}

	@Override
	public void setAdeRatLrdIndObj(AfDecimal adeRatLrdIndObj) {
		setAdeRatLrdInd(new AfDecimal(adeRatLrdIndObj, 15, 3));
	}

	@Override
	public String getAdeTpFntCnbtva() {
		throw new FieldNotMappedException("adeTpFntCnbtva");
	}

	@Override
	public void setAdeTpFntCnbtva(String adeTpFntCnbtva) {
		throw new FieldNotMappedException("adeTpFntCnbtva");
	}

	@Override
	public String getAdeTpFntCnbtvaObj() {
		return getAdeTpFntCnbtva();
	}

	@Override
	public void setAdeTpFntCnbtvaObj(String adeTpFntCnbtvaObj) {
		setAdeTpFntCnbtva(adeTpFntCnbtvaObj);
	}

	@Override
	public String getAdeTpIas() {
		throw new FieldNotMappedException("adeTpIas");
	}

	@Override
	public void setAdeTpIas(String adeTpIas) {
		throw new FieldNotMappedException("adeTpIas");
	}

	@Override
	public String getAdeTpIasObj() {
		return getAdeTpIas();
	}

	@Override
	public void setAdeTpIasObj(String adeTpIasObj) {
		setAdeTpIas(adeTpIasObj);
	}

	@Override
	public String getAdeTpModPagTit() {
		throw new FieldNotMappedException("adeTpModPagTit");
	}

	@Override
	public void setAdeTpModPagTit(String adeTpModPagTit) {
		throw new FieldNotMappedException("adeTpModPagTit");
	}

	@Override
	public String getAdeTpRgmFisc() {
		throw new FieldNotMappedException("adeTpRgmFisc");
	}

	@Override
	public void setAdeTpRgmFisc(String adeTpRgmFisc) {
		throw new FieldNotMappedException("adeTpRgmFisc");
	}

	@Override
	public String getAdeTpRiat() {
		throw new FieldNotMappedException("adeTpRiat");
	}

	@Override
	public void setAdeTpRiat(String adeTpRiat) {
		throw new FieldNotMappedException("adeTpRiat");
	}

	@Override
	public String getAdeTpRiatObj() {
		return getAdeTpRiat();
	}

	@Override
	public void setAdeTpRiatObj(String adeTpRiatObj) {
		setAdeTpRiat(adeTpRiatObj);
	}

	@Override
	public char getIabv0002State01() {
		return ldbm0250.getIabv0002().getIabv0002StateGlobal().getState01();
	}

	@Override
	public void setIabv0002State01(char iabv0002State01) {
		ldbm0250.getIabv0002().getIabv0002StateGlobal().setState01(iabv0002State01);
	}

	@Override
	public char getIabv0002State02() {
		return ldbm0250.getIabv0002().getIabv0002StateGlobal().getState02();
	}

	@Override
	public void setIabv0002State02(char iabv0002State02) {
		ldbm0250.getIabv0002().getIabv0002StateGlobal().setState02(iabv0002State02);
	}

	@Override
	public char getIabv0002State03() {
		return ldbm0250.getIabv0002().getIabv0002StateGlobal().getState03();
	}

	@Override
	public void setIabv0002State03(char iabv0002State03) {
		ldbm0250.getIabv0002().getIabv0002StateGlobal().setState03(iabv0002State03);
	}

	@Override
	public char getIabv0002State04() {
		return ldbm0250.getIabv0002().getIabv0002StateGlobal().getState04();
	}

	@Override
	public void setIabv0002State04(char iabv0002State04) {
		ldbm0250.getIabv0002().getIabv0002StateGlobal().setState04(iabv0002State04);
	}

	@Override
	public char getIabv0002State05() {
		return ldbm0250.getIabv0002().getIabv0002StateGlobal().getState05();
	}

	@Override
	public void setIabv0002State05(char iabv0002State05) {
		ldbm0250.getIabv0002().getIabv0002StateGlobal().setState05(iabv0002State05);
	}

	@Override
	public char getIabv0002State06() {
		return ldbm0250.getIabv0002().getIabv0002StateGlobal().getState06();
	}

	@Override
	public void setIabv0002State06(char iabv0002State06) {
		ldbm0250.getIabv0002().getIabv0002StateGlobal().setState06(iabv0002State06);
	}

	@Override
	public char getIabv0002State07() {
		return ldbm0250.getIabv0002().getIabv0002StateGlobal().getState07();
	}

	@Override
	public void setIabv0002State07(char iabv0002State07) {
		ldbm0250.getIabv0002().getIabv0002StateGlobal().setState07(iabv0002State07);
	}

	@Override
	public char getIabv0002State08() {
		return ldbm0250.getIabv0002().getIabv0002StateGlobal().getState08();
	}

	@Override
	public void setIabv0002State08(char iabv0002State08) {
		ldbm0250.getIabv0002().getIabv0002StateGlobal().setState08(iabv0002State08);
	}

	@Override
	public char getIabv0002State09() {
		return ldbm0250.getIabv0002().getIabv0002StateGlobal().getState09();
	}

	@Override
	public void setIabv0002State09(char iabv0002State09) {
		ldbm0250.getIabv0002().getIabv0002StateGlobal().setState09(iabv0002State09);
	}

	@Override
	public char getIabv0002State10() {
		return ldbm0250.getIabv0002().getIabv0002StateGlobal().getState10();
	}

	@Override
	public void setIabv0002State10(char iabv0002State10) {
		ldbm0250.getIabv0002().getIabv0002StateGlobal().setState10(iabv0002State10);
	}

	@Override
	public int getIabv0009IdOggA() {
		throw new FieldNotMappedException("iabv0009IdOggA");
	}

	@Override
	public void setIabv0009IdOggA(int iabv0009IdOggA) {
		throw new FieldNotMappedException("iabv0009IdOggA");
	}

	@Override
	public int getIabv0009IdOggDa() {
		throw new FieldNotMappedException("iabv0009IdOggDa");
	}

	@Override
	public void setIabv0009IdOggDa(int iabv0009IdOggDa) {
		throw new FieldNotMappedException("iabv0009IdOggDa");
	}

	@Override
	public int getIabv0009Versioning() {
		return ldbm0250.getIabv0002().getIabv0009GestGuideService().getVersioning();
	}

	@Override
	public void setIabv0009Versioning(int iabv0009Versioning) {
		ldbm0250.getIabv0002().getIabv0009GestGuideService().setVersioning(iabv0009Versioning);
	}

	@Override
	public int getIdsv0003CodiceCompagniaAnia() {
		return ldbm0250.getIdsv0003().getCodiceCompagniaAnia();
	}

	@Override
	public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
		ldbm0250.getIdsv0003().setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
	}

	@Override
	public int getPolAaDiffProrDflt() {
		throw new FieldNotMappedException("polAaDiffProrDflt");
	}

	@Override
	public void setPolAaDiffProrDflt(int polAaDiffProrDflt) {
		throw new FieldNotMappedException("polAaDiffProrDflt");
	}

	@Override
	public Integer getPolAaDiffProrDfltObj() {
		return (getPolAaDiffProrDflt());
	}

	@Override
	public void setPolAaDiffProrDfltObj(Integer polAaDiffProrDfltObj) {
		setPolAaDiffProrDflt((polAaDiffProrDfltObj));
	}

	@Override
	public int getPolCodCompAnia() {
		throw new FieldNotMappedException("polCodCompAnia");
	}

	@Override
	public void setPolCodCompAnia(int polCodCompAnia) {
		throw new FieldNotMappedException("polCodCompAnia");
	}

	@Override
	public String getPolCodConv() {
		throw new FieldNotMappedException("polCodConv");
	}

	@Override
	public String getPolCodConvAgg() {
		throw new FieldNotMappedException("polCodConvAgg");
	}

	@Override
	public void setPolCodConvAgg(String polCodConvAgg) {
		throw new FieldNotMappedException("polCodConvAgg");
	}

	@Override
	public String getPolCodConvAggObj() {
		return getPolCodConvAgg();
	}

	@Override
	public void setPolCodConvAggObj(String polCodConvAggObj) {
		setPolCodConvAgg(polCodConvAggObj);
	}

	@Override
	public void setPolCodConv(String polCodConv) {
		throw new FieldNotMappedException("polCodConv");
	}

	@Override
	public String getPolCodConvObj() {
		return getPolCodConv();
	}

	@Override
	public void setPolCodConvObj(String polCodConvObj) {
		setPolCodConv(polCodConvObj);
	}

	@Override
	public String getPolCodDvs() {
		throw new FieldNotMappedException("polCodDvs");
	}

	@Override
	public void setPolCodDvs(String polCodDvs) {
		throw new FieldNotMappedException("polCodDvs");
	}

	@Override
	public String getPolCodDvsObj() {
		return getPolCodDvs();
	}

	@Override
	public void setPolCodDvsObj(String polCodDvsObj) {
		setPolCodDvs(polCodDvsObj);
	}

	@Override
	public String getPolCodProd() {
		throw new FieldNotMappedException("polCodProd");
	}

	@Override
	public void setPolCodProd(String polCodProd) {
		throw new FieldNotMappedException("polCodProd");
	}

	@Override
	public String getPolCodRamo() {
		throw new FieldNotMappedException("polCodRamo");
	}

	@Override
	public void setPolCodRamo(String polCodRamo) {
		throw new FieldNotMappedException("polCodRamo");
	}

	@Override
	public String getPolCodRamoObj() {
		return getPolCodRamo();
	}

	@Override
	public void setPolCodRamoObj(String polCodRamoObj) {
		setPolCodRamo(polCodRamoObj);
	}

	@Override
	public String getPolCodTpa() {
		throw new FieldNotMappedException("polCodTpa");
	}

	@Override
	public void setPolCodTpa(String polCodTpa) {
		throw new FieldNotMappedException("polCodTpa");
	}

	@Override
	public String getPolCodTpaObj() {
		return getPolCodTpa();
	}

	@Override
	public void setPolCodTpaObj(String polCodTpaObj) {
		setPolCodTpa(polCodTpaObj);
	}

	@Override
	public String getPolConvGeco() {
		throw new FieldNotMappedException("polConvGeco");
	}

	@Override
	public void setPolConvGeco(String polConvGeco) {
		throw new FieldNotMappedException("polConvGeco");
	}

	@Override
	public String getPolConvGecoObj() {
		return getPolConvGeco();
	}

	@Override
	public void setPolConvGecoObj(String polConvGecoObj) {
		setPolConvGeco(polConvGecoObj);
	}

	@Override
	public AfDecimal getPolDir1oVers() {
		throw new FieldNotMappedException("polDir1oVers");
	}

	@Override
	public void setPolDir1oVers(AfDecimal polDir1oVers) {
		throw new FieldNotMappedException("polDir1oVers");
	}

	@Override
	public AfDecimal getPolDir1oVersObj() {
		return getPolDir1oVers();
	}

	@Override
	public void setPolDir1oVersObj(AfDecimal polDir1oVersObj) {
		setPolDir1oVers(new AfDecimal(polDir1oVersObj, 15, 3));
	}

	@Override
	public AfDecimal getPolDirEmis() {
		throw new FieldNotMappedException("polDirEmis");
	}

	@Override
	public void setPolDirEmis(AfDecimal polDirEmis) {
		throw new FieldNotMappedException("polDirEmis");
	}

	@Override
	public AfDecimal getPolDirEmisObj() {
		return getPolDirEmis();
	}

	@Override
	public void setPolDirEmisObj(AfDecimal polDirEmisObj) {
		setPolDirEmis(new AfDecimal(polDirEmisObj, 15, 3));
	}

	@Override
	public AfDecimal getPolDirQuiet() {
		throw new FieldNotMappedException("polDirQuiet");
	}

	@Override
	public void setPolDirQuiet(AfDecimal polDirQuiet) {
		throw new FieldNotMappedException("polDirQuiet");
	}

	@Override
	public AfDecimal getPolDirQuietObj() {
		return getPolDirQuiet();
	}

	@Override
	public void setPolDirQuietObj(AfDecimal polDirQuietObj) {
		setPolDirQuiet(new AfDecimal(polDirQuietObj, 15, 3));
	}

	@Override
	public AfDecimal getPolDirVersAgg() {
		throw new FieldNotMappedException("polDirVersAgg");
	}

	@Override
	public void setPolDirVersAgg(AfDecimal polDirVersAgg) {
		throw new FieldNotMappedException("polDirVersAgg");
	}

	@Override
	public AfDecimal getPolDirVersAggObj() {
		return getPolDirVersAgg();
	}

	@Override
	public void setPolDirVersAggObj(AfDecimal polDirVersAggObj) {
		setPolDirVersAgg(new AfDecimal(polDirVersAggObj, 15, 3));
	}

	@Override
	public char getPolDsOperSql() {
		throw new FieldNotMappedException("polDsOperSql");
	}

	@Override
	public void setPolDsOperSql(char polDsOperSql) {
		throw new FieldNotMappedException("polDsOperSql");
	}

	@Override
	public long getPolDsRiga() {
		throw new FieldNotMappedException("polDsRiga");
	}

	@Override
	public void setPolDsRiga(long polDsRiga) {
		throw new FieldNotMappedException("polDsRiga");
	}

	@Override
	public char getPolDsStatoElab() {
		throw new FieldNotMappedException("polDsStatoElab");
	}

	@Override
	public void setPolDsStatoElab(char polDsStatoElab) {
		throw new FieldNotMappedException("polDsStatoElab");
	}

	@Override
	public long getPolDsTsEndCptz() {
		throw new FieldNotMappedException("polDsTsEndCptz");
	}

	@Override
	public void setPolDsTsEndCptz(long polDsTsEndCptz) {
		throw new FieldNotMappedException("polDsTsEndCptz");
	}

	@Override
	public long getPolDsTsIniCptz() {
		throw new FieldNotMappedException("polDsTsIniCptz");
	}

	@Override
	public void setPolDsTsIniCptz(long polDsTsIniCptz) {
		throw new FieldNotMappedException("polDsTsIniCptz");
	}

	@Override
	public String getPolDsUtente() {
		throw new FieldNotMappedException("polDsUtente");
	}

	@Override
	public void setPolDsUtente(String polDsUtente) {
		throw new FieldNotMappedException("polDsUtente");
	}

	@Override
	public int getPolDsVer() {
		throw new FieldNotMappedException("polDsVer");
	}

	@Override
	public void setPolDsVer(int polDsVer) {
		throw new FieldNotMappedException("polDsVer");
	}

	@Override
	public String getPolDtApplzConvDb() {
		throw new FieldNotMappedException("polDtApplzConvDb");
	}

	@Override
	public void setPolDtApplzConvDb(String polDtApplzConvDb) {
		throw new FieldNotMappedException("polDtApplzConvDb");
	}

	@Override
	public String getPolDtApplzConvDbObj() {
		return getPolDtApplzConvDb();
	}

	@Override
	public void setPolDtApplzConvDbObj(String polDtApplzConvDbObj) {
		setPolDtApplzConvDb(polDtApplzConvDbObj);
	}

	@Override
	public String getPolDtDecorDb() {
		throw new FieldNotMappedException("polDtDecorDb");
	}

	@Override
	public void setPolDtDecorDb(String polDtDecorDb) {
		throw new FieldNotMappedException("polDtDecorDb");
	}

	@Override
	public String getPolDtEmisDb() {
		throw new FieldNotMappedException("polDtEmisDb");
	}

	@Override
	public void setPolDtEmisDb(String polDtEmisDb) {
		throw new FieldNotMappedException("polDtEmisDb");
	}

	@Override
	public String getPolDtEndEffDb() {
		throw new FieldNotMappedException("polDtEndEffDb");
	}

	@Override
	public void setPolDtEndEffDb(String polDtEndEffDb) {
		throw new FieldNotMappedException("polDtEndEffDb");
	}

	@Override
	public String getPolDtIniEffDb() {
		throw new FieldNotMappedException("polDtIniEffDb");
	}

	@Override
	public void setPolDtIniEffDb(String polDtIniEffDb) {
		throw new FieldNotMappedException("polDtIniEffDb");
	}

	@Override
	public String getPolDtIniVldtConvDb() {
		throw new FieldNotMappedException("polDtIniVldtConvDb");
	}

	@Override
	public void setPolDtIniVldtConvDb(String polDtIniVldtConvDb) {
		throw new FieldNotMappedException("polDtIniVldtConvDb");
	}

	@Override
	public String getPolDtIniVldtConvDbObj() {
		return getPolDtIniVldtConvDb();
	}

	@Override
	public void setPolDtIniVldtConvDbObj(String polDtIniVldtConvDbObj) {
		setPolDtIniVldtConvDb(polDtIniVldtConvDbObj);
	}

	@Override
	public String getPolDtIniVldtProdDb() {
		throw new FieldNotMappedException("polDtIniVldtProdDb");
	}

	@Override
	public void setPolDtIniVldtProdDb(String polDtIniVldtProdDb) {
		throw new FieldNotMappedException("polDtIniVldtProdDb");
	}

	@Override
	public String getPolDtPrescDb() {
		throw new FieldNotMappedException("polDtPrescDb");
	}

	@Override
	public void setPolDtPrescDb(String polDtPrescDb) {
		throw new FieldNotMappedException("polDtPrescDb");
	}

	@Override
	public String getPolDtPrescDbObj() {
		return getPolDtPrescDb();
	}

	@Override
	public void setPolDtPrescDbObj(String polDtPrescDbObj) {
		setPolDtPrescDb(polDtPrescDbObj);
	}

	@Override
	public String getPolDtPropDb() {
		throw new FieldNotMappedException("polDtPropDb");
	}

	@Override
	public void setPolDtPropDb(String polDtPropDb) {
		throw new FieldNotMappedException("polDtPropDb");
	}

	@Override
	public String getPolDtPropDbObj() {
		return getPolDtPropDb();
	}

	@Override
	public void setPolDtPropDbObj(String polDtPropDbObj) {
		setPolDtPropDb(polDtPropDbObj);
	}

	@Override
	public String getPolDtScadDb() {
		throw new FieldNotMappedException("polDtScadDb");
	}

	@Override
	public void setPolDtScadDb(String polDtScadDb) {
		throw new FieldNotMappedException("polDtScadDb");
	}

	@Override
	public String getPolDtScadDbObj() {
		return getPolDtScadDb();
	}

	@Override
	public void setPolDtScadDbObj(String polDtScadDbObj) {
		setPolDtScadDb(polDtScadDbObj);
	}

	@Override
	public int getPolDurAa() {
		throw new FieldNotMappedException("polDurAa");
	}

	@Override
	public void setPolDurAa(int polDurAa) {
		throw new FieldNotMappedException("polDurAa");
	}

	@Override
	public Integer getPolDurAaObj() {
		return (getPolDurAa());
	}

	@Override
	public void setPolDurAaObj(Integer polDurAaObj) {
		setPolDurAa((polDurAaObj));
	}

	@Override
	public int getPolDurGg() {
		throw new FieldNotMappedException("polDurGg");
	}

	@Override
	public void setPolDurGg(int polDurGg) {
		throw new FieldNotMappedException("polDurGg");
	}

	@Override
	public Integer getPolDurGgObj() {
		return (getPolDurGg());
	}

	@Override
	public void setPolDurGgObj(Integer polDurGgObj) {
		setPolDurGg((polDurGgObj));
	}

	@Override
	public int getPolDurMm() {
		throw new FieldNotMappedException("polDurMm");
	}

	@Override
	public void setPolDurMm(int polDurMm) {
		throw new FieldNotMappedException("polDurMm");
	}

	@Override
	public Integer getPolDurMmObj() {
		return (getPolDurMm());
	}

	@Override
	public void setPolDurMmObj(Integer polDurMmObj) {
		setPolDurMm((polDurMmObj));
	}

	@Override
	public char getPolFlAmmbMovi() {
		throw new FieldNotMappedException("polFlAmmbMovi");
	}

	@Override
	public void setPolFlAmmbMovi(char polFlAmmbMovi) {
		throw new FieldNotMappedException("polFlAmmbMovi");
	}

	@Override
	public Character getPolFlAmmbMoviObj() {
		return (getPolFlAmmbMovi());
	}

	@Override
	public void setPolFlAmmbMoviObj(Character polFlAmmbMoviObj) {
		setPolFlAmmbMovi((polFlAmmbMoviObj));
	}

	@Override
	public char getPolFlCopFinanz() {
		throw new FieldNotMappedException("polFlCopFinanz");
	}

	@Override
	public void setPolFlCopFinanz(char polFlCopFinanz) {
		throw new FieldNotMappedException("polFlCopFinanz");
	}

	@Override
	public Character getPolFlCopFinanzObj() {
		return (getPolFlCopFinanz());
	}

	@Override
	public void setPolFlCopFinanzObj(Character polFlCopFinanzObj) {
		setPolFlCopFinanz((polFlCopFinanzObj));
	}

	@Override
	public char getPolFlCumPreCntr() {
		throw new FieldNotMappedException("polFlCumPreCntr");
	}

	@Override
	public void setPolFlCumPreCntr(char polFlCumPreCntr) {
		throw new FieldNotMappedException("polFlCumPreCntr");
	}

	@Override
	public Character getPolFlCumPreCntrObj() {
		return (getPolFlCumPreCntr());
	}

	@Override
	public void setPolFlCumPreCntrObj(Character polFlCumPreCntrObj) {
		setPolFlCumPreCntr((polFlCumPreCntrObj));
	}

	@Override
	public char getPolFlEstas() {
		throw new FieldNotMappedException("polFlEstas");
	}

	@Override
	public void setPolFlEstas(char polFlEstas) {
		throw new FieldNotMappedException("polFlEstas");
	}

	@Override
	public Character getPolFlEstasObj() {
		return (getPolFlEstas());
	}

	@Override
	public void setPolFlEstasObj(Character polFlEstasObj) {
		setPolFlEstas((polFlEstasObj));
	}

	@Override
	public char getPolFlFntAder() {
		throw new FieldNotMappedException("polFlFntAder");
	}

	@Override
	public void setPolFlFntAder(char polFlFntAder) {
		throw new FieldNotMappedException("polFlFntAder");
	}

	@Override
	public Character getPolFlFntAderObj() {
		return (getPolFlFntAder());
	}

	@Override
	public void setPolFlFntAderObj(Character polFlFntAderObj) {
		setPolFlFntAder((polFlFntAderObj));
	}

	@Override
	public char getPolFlFntAz() {
		throw new FieldNotMappedException("polFlFntAz");
	}

	@Override
	public void setPolFlFntAz(char polFlFntAz) {
		throw new FieldNotMappedException("polFlFntAz");
	}

	@Override
	public Character getPolFlFntAzObj() {
		return (getPolFlFntAz());
	}

	@Override
	public void setPolFlFntAzObj(Character polFlFntAzObj) {
		setPolFlFntAz((polFlFntAzObj));
	}

	@Override
	public char getPolFlFntTfr() {
		throw new FieldNotMappedException("polFlFntTfr");
	}

	@Override
	public void setPolFlFntTfr(char polFlFntTfr) {
		throw new FieldNotMappedException("polFlFntTfr");
	}

	@Override
	public Character getPolFlFntTfrObj() {
		return (getPolFlFntTfr());
	}

	@Override
	public void setPolFlFntTfrObj(Character polFlFntTfrObj) {
		setPolFlFntTfr((polFlFntTfrObj));
	}

	@Override
	public char getPolFlFntVolo() {
		throw new FieldNotMappedException("polFlFntVolo");
	}

	@Override
	public void setPolFlFntVolo(char polFlFntVolo) {
		throw new FieldNotMappedException("polFlFntVolo");
	}

	@Override
	public Character getPolFlFntVoloObj() {
		return (getPolFlFntVolo());
	}

	@Override
	public void setPolFlFntVoloObj(Character polFlFntVoloObj) {
		setPolFlFntVolo((polFlFntVoloObj));
	}

	@Override
	public char getPolFlPoliBundling() {
		throw new FieldNotMappedException("polFlPoliBundling");
	}

	@Override
	public void setPolFlPoliBundling(char polFlPoliBundling) {
		throw new FieldNotMappedException("polFlPoliBundling");
	}

	@Override
	public Character getPolFlPoliBundlingObj() {
		return (getPolFlPoliBundling());
	}

	@Override
	public void setPolFlPoliBundlingObj(Character polFlPoliBundlingObj) {
		setPolFlPoliBundling((polFlPoliBundlingObj));
	}

	@Override
	public char getPolFlPoliCpiPr() {
		throw new FieldNotMappedException("polFlPoliCpiPr");
	}

	@Override
	public void setPolFlPoliCpiPr(char polFlPoliCpiPr) {
		throw new FieldNotMappedException("polFlPoliCpiPr");
	}

	@Override
	public Character getPolFlPoliCpiPrObj() {
		return (getPolFlPoliCpiPr());
	}

	@Override
	public void setPolFlPoliCpiPrObj(Character polFlPoliCpiPrObj) {
		setPolFlPoliCpiPr((polFlPoliCpiPrObj));
	}

	@Override
	public char getPolFlPoliIfp() {
		throw new FieldNotMappedException("polFlPoliIfp");
	}

	@Override
	public void setPolFlPoliIfp(char polFlPoliIfp) {
		throw new FieldNotMappedException("polFlPoliIfp");
	}

	@Override
	public Character getPolFlPoliIfpObj() {
		return (getPolFlPoliIfp());
	}

	@Override
	public void setPolFlPoliIfpObj(Character polFlPoliIfpObj) {
		setPolFlPoliIfp((polFlPoliIfpObj));
	}

	@Override
	public char getPolFlQuestAdegzAss() {
		throw new FieldNotMappedException("polFlQuestAdegzAss");
	}

	@Override
	public void setPolFlQuestAdegzAss(char polFlQuestAdegzAss) {
		throw new FieldNotMappedException("polFlQuestAdegzAss");
	}

	@Override
	public Character getPolFlQuestAdegzAssObj() {
		return (getPolFlQuestAdegzAss());
	}

	@Override
	public void setPolFlQuestAdegzAssObj(Character polFlQuestAdegzAssObj) {
		setPolFlQuestAdegzAss((polFlQuestAdegzAssObj));
	}

	@Override
	public char getPolFlRshComun() {
		throw new FieldNotMappedException("polFlRshComun");
	}

	@Override
	public void setPolFlRshComun(char polFlRshComun) {
		throw new FieldNotMappedException("polFlRshComun");
	}

	@Override
	public char getPolFlRshComunCond() {
		throw new FieldNotMappedException("polFlRshComunCond");
	}

	@Override
	public void setPolFlRshComunCond(char polFlRshComunCond) {
		throw new FieldNotMappedException("polFlRshComunCond");
	}

	@Override
	public Character getPolFlRshComunCondObj() {
		return (getPolFlRshComunCond());
	}

	@Override
	public void setPolFlRshComunCondObj(Character polFlRshComunCondObj) {
		setPolFlRshComunCond((polFlRshComunCondObj));
	}

	@Override
	public Character getPolFlRshComunObj() {
		return (getPolFlRshComun());
	}

	@Override
	public void setPolFlRshComunObj(Character polFlRshComunObj) {
		setPolFlRshComun((polFlRshComunObj));
	}

	@Override
	public char getPolFlScudoFisc() {
		throw new FieldNotMappedException("polFlScudoFisc");
	}

	@Override
	public void setPolFlScudoFisc(char polFlScudoFisc) {
		throw new FieldNotMappedException("polFlScudoFisc");
	}

	@Override
	public Character getPolFlScudoFiscObj() {
		return (getPolFlScudoFisc());
	}

	@Override
	public void setPolFlScudoFiscObj(Character polFlScudoFiscObj) {
		setPolFlScudoFisc((polFlScudoFiscObj));
	}

	@Override
	public char getPolFlTfrStrc() {
		throw new FieldNotMappedException("polFlTfrStrc");
	}

	@Override
	public void setPolFlTfrStrc(char polFlTfrStrc) {
		throw new FieldNotMappedException("polFlTfrStrc");
	}

	@Override
	public Character getPolFlTfrStrcObj() {
		return (getPolFlTfrStrc());
	}

	@Override
	public void setPolFlTfrStrcObj(Character polFlTfrStrcObj) {
		setPolFlTfrStrc((polFlTfrStrcObj));
	}

	@Override
	public char getPolFlTrasfe() {
		throw new FieldNotMappedException("polFlTrasfe");
	}

	@Override
	public void setPolFlTrasfe(char polFlTrasfe) {
		throw new FieldNotMappedException("polFlTrasfe");
	}

	@Override
	public Character getPolFlTrasfeObj() {
		return (getPolFlTrasfe());
	}

	@Override
	public void setPolFlTrasfeObj(Character polFlTrasfeObj) {
		setPolFlTrasfe((polFlTrasfeObj));
	}

	@Override
	public String getPolFlVerProd() {
		throw new FieldNotMappedException("polFlVerProd");
	}

	@Override
	public void setPolFlVerProd(String polFlVerProd) {
		throw new FieldNotMappedException("polFlVerProd");
	}

	@Override
	public String getPolFlVerProdObj() {
		return getPolFlVerProd();
	}

	@Override
	public void setPolFlVerProdObj(String polFlVerProdObj) {
		setPolFlVerProd(polFlVerProdObj);
	}

	@Override
	public char getPolFlVndBundle() {
		throw new FieldNotMappedException("polFlVndBundle");
	}

	@Override
	public void setPolFlVndBundle(char polFlVndBundle) {
		throw new FieldNotMappedException("polFlVndBundle");
	}

	@Override
	public Character getPolFlVndBundleObj() {
		return (getPolFlVndBundle());
	}

	@Override
	public void setPolFlVndBundleObj(Character polFlVndBundleObj) {
		setPolFlVndBundle((polFlVndBundleObj));
	}

	@Override
	public String getPolIbBs() {
		throw new FieldNotMappedException("polIbBs");
	}

	@Override
	public void setPolIbBs(String polIbBs) {
		throw new FieldNotMappedException("polIbBs");
	}

	@Override
	public String getPolIbBsObj() {
		return getPolIbBs();
	}

	@Override
	public void setPolIbBsObj(String polIbBsObj) {
		setPolIbBs(polIbBsObj);
	}

	@Override
	public String getPolIbOgg() {
		throw new FieldNotMappedException("polIbOgg");
	}

	@Override
	public void setPolIbOgg(String polIbOgg) {
		throw new FieldNotMappedException("polIbOgg");
	}

	@Override
	public String getPolIbOggObj() {
		return getPolIbOgg();
	}

	@Override
	public void setPolIbOggObj(String polIbOggObj) {
		setPolIbOgg(polIbOggObj);
	}

	@Override
	public String getPolIbProp() {
		throw new FieldNotMappedException("polIbProp");
	}

	@Override
	public void setPolIbProp(String polIbProp) {
		throw new FieldNotMappedException("polIbProp");
	}

	@Override
	public int getPolIdAccComm() {
		throw new FieldNotMappedException("polIdAccComm");
	}

	@Override
	public void setPolIdAccComm(int polIdAccComm) {
		throw new FieldNotMappedException("polIdAccComm");
	}

	@Override
	public Integer getPolIdAccCommObj() {
		return (getPolIdAccComm());
	}

	@Override
	public void setPolIdAccCommObj(Integer polIdAccCommObj) {
		setPolIdAccComm((polIdAccCommObj));
	}

	@Override
	public int getPolIdMoviChiu() {
		throw new FieldNotMappedException("polIdMoviChiu");
	}

	@Override
	public void setPolIdMoviChiu(int polIdMoviChiu) {
		throw new FieldNotMappedException("polIdMoviChiu");
	}

	@Override
	public Integer getPolIdMoviChiuObj() {
		return (getPolIdMoviChiu());
	}

	@Override
	public void setPolIdMoviChiuObj(Integer polIdMoviChiuObj) {
		setPolIdMoviChiu((polIdMoviChiuObj));
	}

	@Override
	public int getPolIdMoviCrz() {
		throw new FieldNotMappedException("polIdMoviCrz");
	}

	@Override
	public void setPolIdMoviCrz(int polIdMoviCrz) {
		throw new FieldNotMappedException("polIdMoviCrz");
	}

	@Override
	public int getPolIdPoli() {
		throw new FieldNotMappedException("polIdPoli");
	}

	@Override
	public void setPolIdPoli(int polIdPoli) {
		throw new FieldNotMappedException("polIdPoli");
	}

	@Override
	public char getPolIndPoliPrinColl() {
		throw new FieldNotMappedException("polIndPoliPrinColl");
	}

	@Override
	public void setPolIndPoliPrinColl(char polIndPoliPrinColl) {
		throw new FieldNotMappedException("polIndPoliPrinColl");
	}

	@Override
	public Character getPolIndPoliPrinCollObj() {
		return (getPolIndPoliPrinColl());
	}

	@Override
	public void setPolIndPoliPrinCollObj(Character polIndPoliPrinCollObj) {
		setPolIndPoliPrinColl((polIndPoliPrinCollObj));
	}

	@Override
	public AfDecimal getPolSpeMed() {
		throw new FieldNotMappedException("polSpeMed");
	}

	@Override
	public void setPolSpeMed(AfDecimal polSpeMed) {
		throw new FieldNotMappedException("polSpeMed");
	}

	@Override
	public AfDecimal getPolSpeMedObj() {
		return getPolSpeMed();
	}

	@Override
	public void setPolSpeMedObj(AfDecimal polSpeMedObj) {
		setPolSpeMed(new AfDecimal(polSpeMedObj, 15, 3));
	}

	@Override
	public String getPolSubcatProd() {
		throw new FieldNotMappedException("polSubcatProd");
	}

	@Override
	public void setPolSubcatProd(String polSubcatProd) {
		throw new FieldNotMappedException("polSubcatProd");
	}

	@Override
	public String getPolSubcatProdObj() {
		return getPolSubcatProd();
	}

	@Override
	public void setPolSubcatProdObj(String polSubcatProdObj) {
		setPolSubcatProd(polSubcatProdObj);
	}

	@Override
	public String getPolTpApplzDir() {
		throw new FieldNotMappedException("polTpApplzDir");
	}

	@Override
	public void setPolTpApplzDir(String polTpApplzDir) {
		throw new FieldNotMappedException("polTpApplzDir");
	}

	@Override
	public String getPolTpApplzDirObj() {
		return getPolTpApplzDir();
	}

	@Override
	public void setPolTpApplzDirObj(String polTpApplzDirObj) {
		setPolTpApplzDir(polTpApplzDirObj);
	}

	@Override
	public String getPolTpFrmAssva() {
		throw new FieldNotMappedException("polTpFrmAssva");
	}

	@Override
	public void setPolTpFrmAssva(String polTpFrmAssva) {
		throw new FieldNotMappedException("polTpFrmAssva");
	}

	@Override
	public String getPolTpLivGenzTit() {
		throw new FieldNotMappedException("polTpLivGenzTit");
	}

	@Override
	public void setPolTpLivGenzTit(String polTpLivGenzTit) {
		throw new FieldNotMappedException("polTpLivGenzTit");
	}

	@Override
	public String getPolTpOpzAScad() {
		throw new FieldNotMappedException("polTpOpzAScad");
	}

	@Override
	public void setPolTpOpzAScad(String polTpOpzAScad) {
		throw new FieldNotMappedException("polTpOpzAScad");
	}

	@Override
	public String getPolTpOpzAScadObj() {
		return getPolTpOpzAScad();
	}

	@Override
	public void setPolTpOpzAScadObj(String polTpOpzAScadObj) {
		setPolTpOpzAScad(polTpOpzAScadObj);
	}

	@Override
	public String getPolTpPoli() {
		throw new FieldNotMappedException("polTpPoli");
	}

	@Override
	public void setPolTpPoli(String polTpPoli) {
		throw new FieldNotMappedException("polTpPoli");
	}

	@Override
	public String getPolTpPtfEstno() {
		throw new FieldNotMappedException("polTpPtfEstno");
	}

	@Override
	public void setPolTpPtfEstno(String polTpPtfEstno) {
		throw new FieldNotMappedException("polTpPtfEstno");
	}

	@Override
	public String getPolTpPtfEstnoObj() {
		return getPolTpPtfEstno();
	}

	@Override
	public void setPolTpPtfEstnoObj(String polTpPtfEstnoObj) {
		setPolTpPtfEstno(polTpPtfEstnoObj);
	}

	@Override
	public String getPolTpRgmFisc() {
		throw new FieldNotMappedException("polTpRgmFisc");
	}

	@Override
	public void setPolTpRgmFisc(String polTpRgmFisc) {
		throw new FieldNotMappedException("polTpRgmFisc");
	}

	@Override
	public String getPolTpRgmFiscObj() {
		return getPolTpRgmFisc();
	}

	@Override
	public void setPolTpRgmFiscObj(String polTpRgmFiscObj) {
		setPolTpRgmFisc(polTpRgmFiscObj);
	}

	@Override
	public int getStbCodCompAnia() {
		throw new FieldNotMappedException("stbCodCompAnia");
	}

	@Override
	public void setStbCodCompAnia(int stbCodCompAnia) {
		throw new FieldNotMappedException("stbCodCompAnia");
	}

	@Override
	public char getStbDsOperSql() {
		throw new FieldNotMappedException("stbDsOperSql");
	}

	@Override
	public void setStbDsOperSql(char stbDsOperSql) {
		throw new FieldNotMappedException("stbDsOperSql");
	}

	@Override
	public long getStbDsRiga() {
		throw new FieldNotMappedException("stbDsRiga");
	}

	@Override
	public void setStbDsRiga(long stbDsRiga) {
		throw new FieldNotMappedException("stbDsRiga");
	}

	@Override
	public char getStbDsStatoElab() {
		throw new FieldNotMappedException("stbDsStatoElab");
	}

	@Override
	public void setStbDsStatoElab(char stbDsStatoElab) {
		throw new FieldNotMappedException("stbDsStatoElab");
	}

	@Override
	public long getStbDsTsEndCptz() {
		throw new FieldNotMappedException("stbDsTsEndCptz");
	}

	@Override
	public void setStbDsTsEndCptz(long stbDsTsEndCptz) {
		throw new FieldNotMappedException("stbDsTsEndCptz");
	}

	@Override
	public long getStbDsTsIniCptz() {
		throw new FieldNotMappedException("stbDsTsIniCptz");
	}

	@Override
	public void setStbDsTsIniCptz(long stbDsTsIniCptz) {
		throw new FieldNotMappedException("stbDsTsIniCptz");
	}

	@Override
	public String getStbDsUtente() {
		throw new FieldNotMappedException("stbDsUtente");
	}

	@Override
	public void setStbDsUtente(String stbDsUtente) {
		throw new FieldNotMappedException("stbDsUtente");
	}

	@Override
	public int getStbDsVer() {
		throw new FieldNotMappedException("stbDsVer");
	}

	@Override
	public void setStbDsVer(int stbDsVer) {
		throw new FieldNotMappedException("stbDsVer");
	}

	@Override
	public String getStbDtEndEffDb() {
		throw new FieldNotMappedException("stbDtEndEffDb");
	}

	@Override
	public void setStbDtEndEffDb(String stbDtEndEffDb) {
		throw new FieldNotMappedException("stbDtEndEffDb");
	}

	@Override
	public String getStbDtIniEffDb() {
		throw new FieldNotMappedException("stbDtIniEffDb");
	}

	@Override
	public void setStbDtIniEffDb(String stbDtIniEffDb) {
		throw new FieldNotMappedException("stbDtIniEffDb");
	}

	@Override
	public int getStbIdMoviChiu() {
		throw new FieldNotMappedException("stbIdMoviChiu");
	}

	@Override
	public void setStbIdMoviChiu(int stbIdMoviChiu) {
		throw new FieldNotMappedException("stbIdMoviChiu");
	}

	@Override
	public Integer getStbIdMoviChiuObj() {
		return (getStbIdMoviChiu());
	}

	@Override
	public void setStbIdMoviChiuObj(Integer stbIdMoviChiuObj) {
		setStbIdMoviChiu((stbIdMoviChiuObj));
	}

	@Override
	public int getStbIdMoviCrz() {
		throw new FieldNotMappedException("stbIdMoviCrz");
	}

	@Override
	public void setStbIdMoviCrz(int stbIdMoviCrz) {
		throw new FieldNotMappedException("stbIdMoviCrz");
	}

	@Override
	public int getStbIdOgg() {
		throw new FieldNotMappedException("stbIdOgg");
	}

	@Override
	public void setStbIdOgg(int stbIdOgg) {
		throw new FieldNotMappedException("stbIdOgg");
	}

	@Override
	public int getStbIdStatOggBus() {
		throw new FieldNotMappedException("stbIdStatOggBus");
	}

	@Override
	public void setStbIdStatOggBus(int stbIdStatOggBus) {
		throw new FieldNotMappedException("stbIdStatOggBus");
	}

	@Override
	public String getStbTpCaus() {
		throw new FieldNotMappedException("stbTpCaus");
	}

	@Override
	public void setStbTpCaus(String stbTpCaus) {
		throw new FieldNotMappedException("stbTpCaus");
	}

	@Override
	public String getStbTpOgg() {
		throw new FieldNotMappedException("stbTpOgg");
	}

	@Override
	public void setStbTpOgg(String stbTpOgg) {
		throw new FieldNotMappedException("stbTpOgg");
	}

	@Override
	public String getStbTpStatBus() {
		throw new FieldNotMappedException("stbTpStatBus");
	}

	@Override
	public void setStbTpStatBus(String stbTpStatBus) {
		throw new FieldNotMappedException("stbTpStatBus");
	}

	@Override
	public String getWlbCodProd() {
		throw new FieldNotMappedException("wlbCodProd");
	}

	@Override
	public void setWlbCodProd(String wlbCodProd) {
		throw new FieldNotMappedException("wlbCodProd");
	}

	@Override
	public String getWlbIbAdeFirst() {
		return ldbm0250.getWs().getWlbRecPren().getIbAdeFirst();
	}

	@Override
	public void setWlbIbAdeFirst(String wlbIbAdeFirst) {
		ldbm0250.getWs().getWlbRecPren().setIbAdeFirst(wlbIbAdeFirst);
	}

	@Override
	public String getWlbIbAdeLast() {
		return ldbm0250.getWs().getWlbRecPren().getIbAdeLast();
	}

	@Override
	public void setWlbIbAdeLast(String wlbIbAdeLast) {
		ldbm0250.getWs().getWlbRecPren().setIbAdeLast(wlbIbAdeLast);
	}

	@Override
	public String getWlbIbPoliFirst() {
		return ldbm0250.getWs().getWlbRecPren().getIbPoliFirst();
	}

	@Override
	public void setWlbIbPoliFirst(String wlbIbPoliFirst) {
		ldbm0250.getWs().getWlbRecPren().setIbPoliFirst(wlbIbPoliFirst);
	}

	@Override
	public String getWlbIbPoliLast() {
		return ldbm0250.getWs().getWlbRecPren().getIbPoliLast();
	}

	@Override
	public void setWlbIbPoliLast(String wlbIbPoliLast) {
		ldbm0250.getWs().getWlbRecPren().setIbPoliLast(wlbIbPoliLast);
	}

	@Override
	public String getWsDtPtfX() {
		return ldbm0250.getWs().getWsDtPtfX();
	}

	@Override
	public void setWsDtPtfX(String wsDtPtfX) {
		ldbm0250.getWs().setWsDtPtfX(wsDtPtfX);
	}

	@Override
	public long getWsDtTsPtf() {
		return ldbm0250.getWs().getWsDtTsPtf();
	}

	@Override
	public void setWsDtTsPtf(long wsDtTsPtf) {
		ldbm0250.getWs().setWsDtTsPtf(wsDtTsPtf);
	}

	@Override
	public long getWsDtTsPtfPre() {
		return ldbm0250.getWs().getWsDtTsPtfPre();
	}

	@Override
	public void setWsDtTsPtfPre(long wsDtTsPtfPre) {
		ldbm0250.getWs().setWsDtTsPtfPre(wsDtTsPtfPre);
	}

	@Override
	public String getWsTpFrmAssva1() {
		return ldbm0250.getWs().getWsTpFrmAssva1();
	}

	@Override
	public void setWsTpFrmAssva1(String wsTpFrmAssva1) {
		ldbm0250.getWs().setWsTpFrmAssva1(wsTpFrmAssva1);
	}

	@Override
	public String getWsTpFrmAssva2() {
		return ldbm0250.getWs().getWsTpFrmAssva2();
	}

	@Override
	public void setWsTpFrmAssva2(String wsTpFrmAssva2) {
		ldbm0250.getWs().setWsTpFrmAssva2(wsTpFrmAssva2);
	}
}

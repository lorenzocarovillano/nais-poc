/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

/**Original name: IVVC0221<br>
 * Variable: IVVC0221 from copybook IVVC0221<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ivvc0221 {

	//==== PROPERTIES ====
	//Original name: IVVC0221-DETT-QUEST-WC
	private Ivvc0221DettQuestWc ivvc0221DettQuestWc = new Ivvc0221DettQuestWc();

	//==== METHODS ====
	public Ivvc0221DettQuestWc getIvvc0221DettQuestWc() {
		return ivvc0221DettQuestWc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int IVVC0211_COD_DOMANDA = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

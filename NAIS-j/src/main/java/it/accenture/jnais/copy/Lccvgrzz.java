/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

/**Original name: LCCVGRZZ<br>
 * Copybook: LCCVGRZZ from copybook LCCVGRZZ<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Lccvgrzz {

	//==== PROPERTIES ====
	//Original name: WK-GRZ-MAX-B
	private short maxB = ((short) 20);

	//==== METHODS ====
	public short getMaxB() {
		return this.maxB;
	}
}

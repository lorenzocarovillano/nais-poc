/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

/**Original name: LCCVTITZ<br>
 * Copybook: LCCVTITZ from copybook LCCVTITZ<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Lccvtitz {

	//==== PROPERTIES ====
	//Original name: WK-TIT-MAX-A
	private short maxA = ((short) 200);

	//==== METHODS ====
	public short getMaxA() {
		return this.maxA;
	}
}

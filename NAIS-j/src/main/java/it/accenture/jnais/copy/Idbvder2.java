/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDBVDER2<br>
 * Copybook: IDBVDER2 from copybook IDBVDER2<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvder2 {

	//==== PROPERTIES ====
	//Original name: IND-DER-TP-AREA-D-RICH
	private short derTpAreaDRich = DefaultValues.BIN_SHORT_VAL;
	//Original name: IND-DER-AREA-D-RICH
	private short derAreaDRich = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	public void setDerTpAreaDRich(short derTpAreaDRich) {
		this.derTpAreaDRich = derTpAreaDRich;
	}

	public short getDerTpAreaDRich() {
		return this.derTpAreaDRich;
	}

	public void setDerAreaDRich(short derAreaDRich) {
		this.derAreaDRich = derAreaDRich;
	}

	public short getDerAreaDRich() {
		return this.derAreaDRich;
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

/**Original name: WK-ISPC-MAX<br>
 * Variable: WK-ISPC-MAX from copybook ISPC000Z<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkIspcMax {

	//==== PROPERTIES ====
	//Original name: WK-ISPC0211-NUM-COMPON-MAX-P
	private short ispc0211NumComponMaxP = ((short) 100);
	//Original name: WK-ISPC0211-NUM-COMPON-MAX-T
	private short ispc0211NumComponMaxT = ((short) 75);
	//Original name: WK-ISPC0140-NUM-COMPON-MAX-P
	private short ispc0140NumComponMaxP = ((short) 100);
	//Original name: WK-ISPC0140-NUM-COMPON-MAX-T
	private short ispc0140NumComponMaxT = ((short) 75);

	//==== METHODS ====
	public short getIspc0211NumComponMaxP() {
		return this.ispc0211NumComponMaxP;
	}

	public short getIspc0211NumComponMaxT() {
		return this.ispc0211NumComponMaxT;
	}

	public short getIspc0140NumComponMaxP() {
		return this.ispc0140NumComponMaxP;
	}

	public short getIspc0140NumComponMaxT() {
		return this.ispc0140NumComponMaxT;
	}
}

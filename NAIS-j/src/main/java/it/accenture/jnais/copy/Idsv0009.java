/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.notifier.IValueChangeListener;
import com.bphx.ctu.af.core.notifier.StringChangeNotifier;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

import it.accenture.jnais.ws.Idsv0003;

/**Original name: IDSV0009<br>
 * Variable: IDSV0009 from copybook IDSV0009<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Idsv0009 {

	//==== PROPERTIES ====
	public static final int FLAG_TRATTAMENTO_MAXOCCURS = 1000;
	private int flagTrattamentoListenerSize = FLAG_TRATTAMENTO_MAXOCCURS;
	private IValueChangeListener flagTrattamentoListener = new FlagTrattamentoListener();
	//Original name: WK-LIMITE-STR-DATO
	private StringChangeNotifier wkLimiteStrDato = new StringChangeNotifier(Len.WK_LIMITE_STR_DATO);
	//Original name: CONVERSION-VARIABLES
	private ConversionVariables conversionVariables = new ConversionVariables();
	//Original name: WK-CAMPO-ATTIVO
	private char wkCampoAttivo = 'S';
	//Original name: DESCRIZ-ERR-DB2
	private String descrizErrDb2 = "";
	//Original name: IND-SINONIM
	private String indSinonim = "0";
	//Original name: IDSV0003
	private Idsv0003 idsv0003 = new Idsv0003();

	//==== CONSTRUCTORS ====
	public Idsv0009() {
		init();
	}

	//==== METHODS ====
	public void init() {
		wkLimiteStrDato.setValue(1000);
	}

	public short getWkLimiteStrDato0() {
		return ((short) wkLimiteStrDato.getValue());
	}

	public char getWkCampoAttivo() {
		return this.wkCampoAttivo;
	}

	public String getDescrizErrDb2() {
		return this.descrizErrDb2;
	}

	public void setIndSinonim(short indSinonim) {
		this.indSinonim = NumericDisplay.asString(indSinonim, Len.IND_SINONIM);
	}

	public void setIndSinonimFormatted(String indSinonim) {
		this.indSinonim = Trunc.toUnsignedNumeric(indSinonim, Len.IND_SINONIM);
	}

	public short getIndSinonim() {
		return NumericDisplay.asShort(this.indSinonim);
	}

	public ConversionVariables getConversionVariables() {
		return conversionVariables;
	}

	public IValueChangeListener getFlagTrattamentoListener() {
		return flagTrattamentoListener;
	}

	public Idsv0003 getIdsv0003() {
		return idsv0003;
	}

	public StringChangeNotifier getWkLimiteStrDato() {
		return wkLimiteStrDato;
	}

	//==== INNER CLASSES ====
	/**Original name: FLAG-TRATTAMENTO<br>*/
	public class FlagTrattamentoListener implements IValueChangeListener {

		//==== METHODS ====
		@Override
		public void change() {
			flagTrattamentoListenerSize = FLAG_TRATTAMENTO_MAXOCCURS;
		}

		@Override
		public void change(int value) {
			flagTrattamentoListenerSize = value < 1 ? 0 : (value > FLAG_TRATTAMENTO_MAXOCCURS ? FLAG_TRATTAMENTO_MAXOCCURS : value);
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WK_LIMITE_STR_DATO = 4;
		public static final int WS_TIMESTAMP = 18;
		public static final int STR_WH_COD_STR_DATO = 30;
		public static final int IND_USING_AREA = 4;
		public static final int IND_CALL_USING = 4;
		public static final int AREA_LINK = 32000;
		public static final int COMODO_CODICE_STR_DATO = 30;
		public static final int COMODO_SERV_CONVERSIONE = 8;
		public static final int COMODO_TIPO_MOVIMENTO = 5;
		public static final int R999_CODICE_STR_DATO = 30;
		public static final int IND_SINONIM = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

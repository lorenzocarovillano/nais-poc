/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

import it.accenture.jnais.ws.occurs.WpreTabBatchPres;

/**Original name: LOAC0560<br>
 * Variable: LOAC0560 from copybook LOAC0560<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Loac0560 {

	//==== PROPERTIES ====
	public static final int TAB_BATCH_PRES_MAXOCCURS = 10;
	//Original name: WPRE-TAB-BATCH-PRES
	private WpreTabBatchPres[] tabBatchPres = new WpreTabBatchPres[TAB_BATCH_PRES_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public Loac0560() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int tabBatchPresIdx = 1; tabBatchPresIdx <= TAB_BATCH_PRES_MAXOCCURS; tabBatchPresIdx++) {
			tabBatchPres[tabBatchPresIdx - 1] = new WpreTabBatchPres();
		}
	}

	public WpreTabBatchPres getTabBatchPres(int idx) {
		return tabBatchPres[idx - 1];
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MOVI_BATCH_SEL = 5;
		public static final int DT_ELAB_DA_DEF = 8;
		public static final int DT_ELAB_A_DEF = 8;
		public static final int DT_COMPETENZA = 8;
		public static final int CANALE_VENDITA_SEL = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

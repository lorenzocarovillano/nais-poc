/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

/**Original name: WK-TGA-MAX<br>
 * Variable: WK-TGA-MAX from copybook LCCVTGAZ<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkTgaMax {

	//==== PROPERTIES ====
	//Original name: WK-TGA-MAX-B
	private short b = ((short) 20);
	//Original name: WK-TGA-MAX-C
	private short c = ((short) 1250);

	//==== METHODS ====
	public short getB() {
		return this.b;
	}

	public short getC() {
		return this.c;
	}
}

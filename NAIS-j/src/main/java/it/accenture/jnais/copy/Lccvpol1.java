/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVPOL1<br>
 * Variable: LCCVPOL1 from copybook LCCVPOL1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvpol1 {

	//==== PROPERTIES ====
	/**Original name: WPOL-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA POLI
	 *    ALIAS POL
	 *    ULTIMO AGG. 07 DIC 2017
	 * ------------------------------------------------------------</pre>*/
	private WpolStatus status = new WpolStatus();
	//Original name: WPOL-ID-PTF
	private int idPtf = DefaultValues.INT_VAL;
	//Original name: WPOL-DATI
	private WpolDati dati = new WpolDati();

	//==== METHODS ====
	public void setIdPtf(int idPtf) {
		this.idPtf = idPtf;
	}

	public int getIdPtf() {
		return this.idPtf;
	}

	public WpolDati getDati() {
		return dati;
	}

	public WpolStatus getStatus() {
		return status;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ID_PTF = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int ID_PTF = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

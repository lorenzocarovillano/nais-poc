/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

/**Original name: LCCVP851<br>
 * Copybook: LCCVP851 from copybook LCCVP851<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Lccvp851 {

	//==== PROPERTIES ====
	//Original name: WP85-DATI
	private Wp85Dati dati = new Wp85Dati();

	//==== METHODS ====
	public Wp85Dati getDati() {
		return dati;
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVB041<br>
 * Variable: LCCVB041 from copybook LCCVB041<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvb041 {

	//==== PROPERTIES ====
	/**Original name: WB04-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA BILA_VAR_CALC_P
	 *    ALIAS B04
	 *    ULTIMO AGG. 14 MAR 2011
	 * ------------------------------------------------------------</pre>*/
	private WpolStatus status = new WpolStatus();
	//Original name: WB04-ID-PTF
	private int idPtf = DefaultValues.INT_VAL;
	//Original name: WB04-DATI
	private Wb04Dati dati = new Wb04Dati();

	//==== METHODS ====
	public void setIdPtf(int idPtf) {
		this.idPtf = idPtf;
	}

	public int getIdPtf() {
		return this.idPtf;
	}

	public Wb04Dati getDati() {
		return dati;
	}

	public WpolStatus getStatus() {
		return status;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ID_PTF = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int ID_PTF = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

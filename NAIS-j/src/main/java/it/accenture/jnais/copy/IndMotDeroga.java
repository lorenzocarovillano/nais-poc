/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-MOT-DEROGA<br>
 * Variable: IND-MOT-DEROGA from copybook IDBVMDE2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndMotDeroga {

	//==== PROPERTIES ====
	//Original name: IND-MDE-ID-MOVI-CHIU
	private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
	//Original name: IND-MDE-DESC-ERR-BREVE
	private short descErrBreve = DefaultValues.BIN_SHORT_VAL;
	//Original name: IND-MDE-DESC-ERR-EST
	private short descErrEst = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	public void setIdMoviChiu(short idMoviChiu) {
		this.idMoviChiu = idMoviChiu;
	}

	public short getIdMoviChiu() {
		return this.idMoviChiu;
	}

	public void setDescErrBreve(short descErrBreve) {
		this.descErrBreve = descErrBreve;
	}

	public short getDescErrBreve() {
		return this.descErrBreve;
	}

	public void setDescErrEst(short descErrEst) {
		this.descErrEst = descErrEst;
	}

	public short getDescErrEst() {
		return this.descErrEst;
	}
}

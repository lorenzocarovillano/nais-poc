/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

import it.accenture.jnais.ws.enums.Idsv0012StatusSharedMemory;

/**Original name: IDSV0012<br>
 * Variable: IDSV0012 from copybook IDSV0012<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Idsv00122 {

	//==== PROPERTIES ====
	//Original name: IDSV0012-STATUS-SHARED-MEMORY
	private Idsv0012StatusSharedMemory statusSharedMemory = new Idsv0012StatusSharedMemory();

	//==== CONSTRUCTORS ====
	public Idsv00122() {
		init();
	}

	//==== METHODS ====
	public void init() {
	}

	public Idsv0012StatusSharedMemory getStatusSharedMemory() {
		return statusSharedMemory;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int AREA_I_O = 32000;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

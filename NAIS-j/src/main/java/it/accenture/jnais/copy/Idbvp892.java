/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDBVP892<br>
 * Copybook: IDBVP892 from copybook IDBVP892<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvp892 {

	//==== PROPERTIES ====
	//Original name: IND-P89-ID-MOVI-CHIU
	private short p89IdMoviChiu = DefaultValues.BIN_SHORT_VAL;
	//Original name: IND-P89-DT-INI-CNTRL-FND
	private short p89DtIniCntrlFnd = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	public void setP89IdMoviChiu(short p89IdMoviChiu) {
		this.p89IdMoviChiu = p89IdMoviChiu;
	}

	public short getP89IdMoviChiu() {
		return this.p89IdMoviChiu;
	}

	public void setP89DtIniCntrlFnd(short p89DtIniCntrlFnd) {
		this.p89DtIniCntrlFnd = p89DtIniCntrlFnd;
	}

	public short getP89DtIniCntrlFnd() {
		return this.p89DtIniCntrlFnd;
	}
}

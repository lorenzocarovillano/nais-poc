/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVDCO1<br>
 * Variable: LCCVDCO1 from copybook LCCVDCO1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvdco1 {

	//==== PROPERTIES ====
	/**Original name: WDCO-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA D_COLL
	 *    ALIAS DCO
	 *    ULTIMO AGG. 02 SET 2008
	 * ------------------------------------------------------------</pre>*/
	private WpolStatus status = new WpolStatus();
	//Original name: WDCO-ID-PTF
	private int idPtf = DefaultValues.INT_VAL;
	//Original name: WDCO-DATI
	private WdcoDati dati = new WdcoDati();

	//==== METHODS ====
	public void setIdPtf(int idPtf) {
		this.idPtf = idPtf;
	}

	public int getIdPtf() {
		return this.idPtf;
	}

	public WdcoDati getDati() {
		return dati;
	}

	public WpolStatus getStatus() {
		return status;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ID_PTF = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int ID_PTF = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

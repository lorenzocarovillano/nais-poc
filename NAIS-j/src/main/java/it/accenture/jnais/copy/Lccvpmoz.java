/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

/**Original name: LCCVPMOZ<br>
 * Copybook: LCCVPMOZ from copybook LCCVPMOZ<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Lccvpmoz {

	//==== PROPERTIES ====
	//Original name: WK-PMO-MAX-A
	private short maxA = ((short) 50);
	//Original name: WK-PMO-MAX-B
	private short maxB = ((short) 100);

	//==== METHODS ====
	public short getMaxA() {
		return this.maxA;
	}

	public short getMaxB() {
		return this.maxB;
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

import it.accenture.jnais.ws.enums.MsgErrore;

/**Original name: LRGC0661<br>
 * Copybook: LRGC0661 from copybook LRGC0661<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Lrgc0661 {

	//==== PROPERTIES ====
	/**Original name: MSG-ERRORE<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY GESTIONE ERRORI
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     ESTRATTO CONTO - FASE DIAGNOSTICO
	 *        - AREA ERRORI DI CONTROLLO FLUSSO
	 * ----------------------------------------------------------------*</pre>*/
	private MsgErrore msgErrore = new MsgErrore();
	//Original name: WS-DESC-TP-MOVI
	private WsDescTpMovi wsDescTpMovi = new WsDescTpMovi();

	//==== METHODS ====
	public MsgErrore getMsgErrore() {
		return msgErrore;
	}

	public WsDescTpMovi getWsDescTpMovi() {
		return wsDescTpMovi;
	}
}

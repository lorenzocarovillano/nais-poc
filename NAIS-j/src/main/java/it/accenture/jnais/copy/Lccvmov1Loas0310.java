/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

/**Original name: LCCVMOV1<br>
 * Variable: LCCVMOV1 from copybook LCCVMOV1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvmov1Loas0310 {

	//==== PROPERTIES ====
	//Original name: ZMOV-DATI
	private WmovDati dati = new WmovDati();

	//==== METHODS ====
	public WmovDati getDati() {
		return dati;
	}
}

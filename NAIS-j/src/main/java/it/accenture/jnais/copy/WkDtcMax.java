/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

/**Original name: WK-DTC-MAX<br>
 * Variable: WK-DTC-MAX from copybook LCCVDTCZ<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkDtcMax {

	//==== PROPERTIES ====
	//Original name: WK-DTC-MAX-A
	private short a = ((short) 100);

	//==== METHODS ====
	public short getA() {
		return this.a;
	}
}

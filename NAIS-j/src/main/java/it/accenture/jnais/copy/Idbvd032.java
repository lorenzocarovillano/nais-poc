/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDBVD032<br>
 * Copybook: IDBVD032 from copybook IDBVD032<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvd032 {

	//==== PROPERTIES ====
	//Original name: IND-D03-PROGR-INIZIALE
	private short d03ProgrIniziale = DefaultValues.BIN_SHORT_VAL;
	//Original name: IND-D03-PROGR-FINALE
	private short d03ProgrFinale = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	public void setD03ProgrIniziale(short d03ProgrIniziale) {
		this.d03ProgrIniziale = d03ProgrIniziale;
	}

	public short getD03ProgrIniziale() {
		return this.d03ProgrIniziale;
	}

	public void setD03ProgrFinale(short d03ProgrFinale) {
		this.d03ProgrFinale = d03ProgrFinale;
	}

	public short getD03ProgrFinale() {
		return this.d03ProgrFinale;
	}
}

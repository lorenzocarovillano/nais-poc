/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [DETT_RICH]
 * 
 */
public interface IDettRich extends BaseSqlTo {

	/**
	 * Host Variable DER-TP-AREA-D-RICH
	 * 
	 */
	String getTpAreaDRich();

	void setTpAreaDRich(String tpAreaDRich);

	/**
	 * Nullable property for DER-TP-AREA-D-RICH
	 * 
	 */
	String getTpAreaDRichObj();

	void setTpAreaDRichObj(String tpAreaDRichObj);

	/**
	 * Host Variable DER-AREA-D-RICH-VCHAR
	 * 
	 */
	String getAreaDRichVchar();

	void setAreaDRichVchar(String areaDRichVchar);

	/**
	 * Nullable property for DER-AREA-D-RICH-VCHAR
	 * 
	 */
	String getAreaDRichVcharObj();

	void setAreaDRichVcharObj(String areaDRichVcharObj);
};

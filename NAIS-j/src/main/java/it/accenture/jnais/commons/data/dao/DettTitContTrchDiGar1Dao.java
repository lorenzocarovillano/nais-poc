/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;

import it.accenture.jnais.commons.data.to.IDettTitContTrchDiGar1;

/**
 * Data Access Object(DAO) for tables [DETT_TIT_CONT, TIT_CONT, TRCH_DI_GAR]
 * 
 */
public class DettTitContTrchDiGar1Dao extends BaseSqlDao<IDettTitContTrchDiGar1> {

	private final IRowMapper<IDettTitContTrchDiGar1> selectRecRm = buildNamedRowMapper(IDettTitContTrchDiGar1.class, "ldbve091PreLrdObj");

	public DettTitContTrchDiGar1Dao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IDettTitContTrchDiGar1> getToClass() {
		return IDettTitContTrchDiGar1.class;
	}

	public IDettTitContTrchDiGar1 selectRec(IDettTitContTrchDiGar1 iDettTitContTrchDiGar1) {
		return buildQuery("selectRec").bind(iDettTitContTrchDiGar1).rowMapper(selectRecRm).singleResult(iDettTitContTrchDiGar1);
	}

	public IDettTitContTrchDiGar1 selectRec1(IDettTitContTrchDiGar1 iDettTitContTrchDiGar1) {
		return buildQuery("selectRec1").bind(iDettTitContTrchDiGar1).rowMapper(selectRecRm).singleResult(iDettTitContTrchDiGar1);
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;

import it.accenture.jnais.commons.data.to.IMatrValVar;

/**
 * Data Access Object(DAO) for table [MATR_VAL_VAR]
 * 
 */
public class MatrValVarDao extends BaseSqlDao<IMatrValVar> {

	private final IRowMapper<IMatrValVar> selectRecRm = buildNamedRowMapper(IMatrValVar.class, "idMatrValVar", "idpMatrValVarObj", "codCompagniaAnia",
			"tipoMovimentoObj", "codDatoExtObj", "obbligatorietaObj", "valoreDefaultObj", "codStrDatoPtfObj", "codDatoPtfObj", "codParametroObj",
			"operazioneObj", "livelloOperazioneObj", "tipoOggettoObj", "whereConditionVcharObj", "servizioLetturaObj", "moduloCalcoloObj",
			"stepValorizzatoreObj", "stepConversazioneObj", "operLogStatoBusObj", "arrayStatoBusObj", "operLogCausaleObj", "arrayCausaleObj");

	public MatrValVarDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IMatrValVar> getToClass() {
		return IMatrValVar.class;
	}

	public IMatrValVar selectRec(int codCompagniaAnia, int tipoMovimento, char stepValorizzatore, char stepConversazione, IMatrValVar iMatrValVar) {
		return buildQuery("selectRec").bind("codCompagniaAnia", codCompagniaAnia).bind("tipoMovimento", tipoMovimento)
				.bind("stepValorizzatore", String.valueOf(stepValorizzatore)).bind("stepConversazione", String.valueOf(stepConversazione))
				.rowMapper(selectRecRm).singleResult(iMatrValVar);
	}
}

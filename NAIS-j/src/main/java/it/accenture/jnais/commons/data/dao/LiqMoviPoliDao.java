/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;

import it.accenture.jnais.commons.data.to.ILiqMoviPoli;

/**
 * Data Access Object(DAO) for tables [LIQ, MOVI, POLI]
 * 
 */
public class LiqMoviPoliDao extends BaseSqlDao<ILiqMoviPoli> {

	public LiqMoviPoliDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<ILiqMoviPoli> getToClass() {
		return ILiqMoviPoli.class;
	}

	public String selectRec(ILiqMoviPoli iLiqMoviPoli, String dft) {
		return buildQuery("selectRec").bind(iLiqMoviPoli).scalarResultString(dft);
	}

	public String selectRec1(ILiqMoviPoli iLiqMoviPoli, String dft) {
		return buildQuery("selectRec1").bind(iLiqMoviPoli).scalarResultString(dft);
	}
}

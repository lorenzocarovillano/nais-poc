/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [PROGR_NUM_OGG]
 * 
 */
public interface IProgrNumOgg extends BaseSqlTo {

	/**
	 * Host Variable D03-COD-COMPAGNIA-ANIA
	 * 
	 */
	int getCodCompagniaAnia();

	void setCodCompagniaAnia(int codCompagniaAnia);

	/**
	 * Host Variable D03-FORMA-ASSICURATIVA
	 * 
	 */
	String getFormaAssicurativa();

	void setFormaAssicurativa(String formaAssicurativa);

	/**
	 * Host Variable D03-COD-OGGETTO
	 * 
	 */
	String getCodOggetto();

	void setCodOggetto(String codOggetto);

	/**
	 * Host Variable D03-KEY-BUSINESS
	 * 
	 */
	String getKeyBusiness();

	void setKeyBusiness(String keyBusiness);

	/**
	 * Host Variable D03-ULT-PROGR
	 * 
	 */
	long getUltProgr();

	void setUltProgr(long ultProgr);

	/**
	 * Host Variable D03-PROGR-INIZIALE
	 * 
	 */
	long getProgrIniziale();

	void setProgrIniziale(long progrIniziale);

	/**
	 * Nullable property for D03-PROGR-INIZIALE
	 * 
	 */
	Long getProgrInizialeObj();

	void setProgrInizialeObj(Long progrInizialeObj);

	/**
	 * Host Variable D03-PROGR-FINALE
	 * 
	 */
	long getProgrFinale();

	void setProgrFinale(long progrFinale);

	/**
	 * Nullable property for D03-PROGR-FINALE
	 * 
	 */
	Long getProgrFinaleObj();

	void setProgrFinaleObj(Long progrFinaleObj);
};

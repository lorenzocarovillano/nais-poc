/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

import it.accenture.jnais.commons.data.to.IAdesPoliStatOggBusTrchDiGar;

/**
 * Data Access Object(DAO) for tables [ADES, POLI, STAT_OGG_BUS, TRCH_DI_GAR]
 * 
 */
public class AdesPoliStatOggBusTrchDiGarDao extends BaseSqlDao<IAdesPoliStatOggBusTrchDiGar> {

	private Cursor curSc05Range;
	private Cursor curSc05;
	private Cursor curSc06;
	private Cursor curSc07;
	private Cursor curSc08;
	private final IRowMapper<IAdesPoliStatOggBusTrchDiGar> selectRecRm = buildNamedRowMapper(IAdesPoliStatOggBusTrchDiGar.class, "adeIdAdes",
			"adeIdPoli", "adeIdMoviCrz", "adeIdMoviChiuObj", "adeDtIniEffDb", "adeDtEndEffDb", "adeIbPrevObj", "adeIbOggObj", "adeCodCompAnia",
			"adeDtDecorDbObj", "adeDtScadDbObj", "adeEtaAScadObj", "adeDurAaObj", "adeDurMmObj", "adeDurGgObj", "adeTpRgmFisc", "adeTpRiatObj",
			"adeTpModPagTit", "adeTpIasObj", "adeDtVarzTpIasDbObj", "adePreNetIndObj", "adePreLrdIndObj", "adeRatLrdIndObj", "adePrstzIniIndObj",
			"adeFlCoincAsstoObj", "adeIbDfltObj", "adeModCalcObj", "adeTpFntCnbtvaObj", "adeImpAzObj", "adeImpAderObj", "adeImpTfrObj",
			"adeImpVoloObj", "adePcAzObj", "adePcAderObj", "adePcTfrObj", "adePcVoloObj", "adeDtNovaRgmFiscDbObj", "adeFlAttivObj",
			"adeImpRecRitVisObj", "adeImpRecRitAccObj", "adeFlVarzStatTbgcObj", "adeFlProvzaMigrazObj", "adeImpbVisDaRecObj",
			"adeDtDecorPrestBanDbObj", "adeDtEffVarzStatTDbObj", "adeDsRiga", "adeDsOperSql", "adeDsVer", "adeDsTsIniCptz", "adeDsTsEndCptz",
			"adeDsUtente", "adeDsStatoElab", "adeCumCnbtCapObj", "adeImpGarCnbtObj", "adeDtUltConsCnbtDbObj", "adeIdenIscFndObj", "adeNumRatPianObj",
			"adeDtPrescDbObj", "adeConcsPrestObj", "polIdPoli", "polIdMoviCrz", "polIdMoviChiuObj", "polIbOggObj", "polIbProp", "polDtPropDbObj",
			"polDtIniEffDb", "polDtEndEffDb", "polCodCompAnia", "polDtDecorDb", "polDtEmisDb", "polTpPoli", "polDurAaObj", "polDurMmObj",
			"polDtScadDbObj", "polCodProd", "polDtIniVldtProdDb", "polCodConvObj", "polCodRamoObj", "polDtIniVldtConvDbObj", "polDtApplzConvDbObj",
			"polTpFrmAssva", "polTpRgmFiscObj", "polFlEstasObj", "polFlRshComunObj", "polFlRshComunCondObj", "polTpLivGenzTit", "polFlCopFinanzObj",
			"polTpApplzDirObj", "polSpeMedObj", "polDirEmisObj", "polDir1oVersObj", "polDirVersAggObj", "polCodDvsObj", "polFlFntAzObj",
			"polFlFntAderObj", "polFlFntTfrObj", "polFlFntVoloObj", "polTpOpzAScadObj", "polAaDiffProrDfltObj", "polFlVerProdObj", "polDurGgObj",
			"polDirQuietObj", "polTpPtfEstnoObj", "polFlCumPreCntrObj", "polFlAmmbMoviObj", "polConvGecoObj", "polDsRiga", "polDsOperSql", "polDsVer",
			"polDsTsIniCptz", "polDsTsEndCptz", "polDsUtente", "polDsStatoElab", "polFlScudoFiscObj", "polFlTrasfeObj", "polFlTfrStrcObj",
			"polDtPrescDbObj", "polCodConvAggObj", "polSubcatProdObj", "polFlQuestAdegzAssObj", "polCodTpaObj", "polIdAccCommObj",
			"polFlPoliCpiPrObj", "polFlPoliBundlingObj", "polIndPoliPrinCollObj", "polFlVndBundleObj", "polIbBsObj", "polFlPoliIfpObj",
			"stbIdStatOggBus", "stbIdOgg", "stbTpOgg", "stbIdMoviCrz", "stbIdMoviChiuObj", "stbDtIniEffDb", "stbDtEndEffDb", "stbCodCompAnia",
			"stbTpStatBus", "stbTpCaus", "stbDsRiga", "stbDsOperSql", "stbDsVer", "stbDsTsIniCptz", "stbDsTsEndCptz", "stbDsUtente",
			"stbDsStatoElab");

	public AdesPoliStatOggBusTrchDiGarDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IAdesPoliStatOggBusTrchDiGar> getToClass() {
		return IAdesPoliStatOggBusTrchDiGar.class;
	}

	public IAdesPoliStatOggBusTrchDiGar selectRec(IAdesPoliStatOggBusTrchDiGar iAdesPoliStatOggBusTrchDiGar) {
		return buildQuery("selectRec").bind(iAdesPoliStatOggBusTrchDiGar).rowMapper(selectRecRm).singleResult(iAdesPoliStatOggBusTrchDiGar);
	}

	public IAdesPoliStatOggBusTrchDiGar selectRec1(IAdesPoliStatOggBusTrchDiGar iAdesPoliStatOggBusTrchDiGar) {
		return buildQuery("selectRec1").bind(iAdesPoliStatOggBusTrchDiGar).rowMapper(selectRecRm).singleResult(iAdesPoliStatOggBusTrchDiGar);
	}

	public DbAccessStatus openCurSc05Range(IAdesPoliStatOggBusTrchDiGar iAdesPoliStatOggBusTrchDiGar) {
		curSc05Range = buildQuery("openCurSc05Range").bind(iAdesPoliStatOggBusTrchDiGar).withHoldCursorsOverCommit().open();
		return dbStatus;
	}

	public DbAccessStatus openCurSc05(IAdesPoliStatOggBusTrchDiGar iAdesPoliStatOggBusTrchDiGar) {
		curSc05 = buildQuery("openCurSc05").bind(iAdesPoliStatOggBusTrchDiGar).withHoldCursorsOverCommit().open();
		return dbStatus;
	}

	public DbAccessStatus closeCurSc05Range() {
		return closeCursor(curSc05Range);
	}

	public DbAccessStatus closeCurSc05() {
		return closeCursor(curSc05);
	}

	public IAdesPoliStatOggBusTrchDiGar fetchCurSc05Range(IAdesPoliStatOggBusTrchDiGar iAdesPoliStatOggBusTrchDiGar) {
		return fetch(curSc05Range, iAdesPoliStatOggBusTrchDiGar, selectRecRm);
	}

	public IAdesPoliStatOggBusTrchDiGar fetchCurSc05(IAdesPoliStatOggBusTrchDiGar iAdesPoliStatOggBusTrchDiGar) {
		return fetch(curSc05, iAdesPoliStatOggBusTrchDiGar, selectRecRm);
	}

	public IAdesPoliStatOggBusTrchDiGar selectRec2(IAdesPoliStatOggBusTrchDiGar iAdesPoliStatOggBusTrchDiGar) {
		return buildQuery("selectRec2").bind(iAdesPoliStatOggBusTrchDiGar).rowMapper(selectRecRm).singleResult(iAdesPoliStatOggBusTrchDiGar);
	}

	public DbAccessStatus openCurSc06(IAdesPoliStatOggBusTrchDiGar iAdesPoliStatOggBusTrchDiGar) {
		curSc06 = buildQuery("openCurSc06").bind(iAdesPoliStatOggBusTrchDiGar).withHoldCursorsOverCommit().open();
		return dbStatus;
	}

	public DbAccessStatus closeCurSc06() {
		return closeCursor(curSc06);
	}

	public IAdesPoliStatOggBusTrchDiGar fetchCurSc06(IAdesPoliStatOggBusTrchDiGar iAdesPoliStatOggBusTrchDiGar) {
		return fetch(curSc06, iAdesPoliStatOggBusTrchDiGar, selectRecRm);
	}

	public IAdesPoliStatOggBusTrchDiGar selectRec3(IAdesPoliStatOggBusTrchDiGar iAdesPoliStatOggBusTrchDiGar) {
		return buildQuery("selectRec3").bind(iAdesPoliStatOggBusTrchDiGar).rowMapper(selectRecRm).singleResult(iAdesPoliStatOggBusTrchDiGar);
	}

	public DbAccessStatus openCurSc07(IAdesPoliStatOggBusTrchDiGar iAdesPoliStatOggBusTrchDiGar) {
		curSc07 = buildQuery("openCurSc07").bind(iAdesPoliStatOggBusTrchDiGar).withHoldCursorsOverCommit().open();
		return dbStatus;
	}

	public DbAccessStatus closeCurSc07() {
		return closeCursor(curSc07);
	}

	public IAdesPoliStatOggBusTrchDiGar fetchCurSc07(IAdesPoliStatOggBusTrchDiGar iAdesPoliStatOggBusTrchDiGar) {
		return fetch(curSc07, iAdesPoliStatOggBusTrchDiGar, selectRecRm);
	}

	public IAdesPoliStatOggBusTrchDiGar selectRec4(IAdesPoliStatOggBusTrchDiGar iAdesPoliStatOggBusTrchDiGar) {
		return buildQuery("selectRec4").bind(iAdesPoliStatOggBusTrchDiGar).rowMapper(selectRecRm).singleResult(iAdesPoliStatOggBusTrchDiGar);
	}

	public DbAccessStatus openCurSc08(IAdesPoliStatOggBusTrchDiGar iAdesPoliStatOggBusTrchDiGar) {
		curSc08 = buildQuery("openCurSc08").bind(iAdesPoliStatOggBusTrchDiGar).withHoldCursorsOverCommit().open();
		return dbStatus;
	}

	public DbAccessStatus closeCurSc08() {
		return closeCursor(curSc08);
	}

	public IAdesPoliStatOggBusTrchDiGar fetchCurSc08(IAdesPoliStatOggBusTrchDiGar iAdesPoliStatOggBusTrchDiGar) {
		return fetch(curSc08, iAdesPoliStatOggBusTrchDiGar, selectRecRm);
	}
}

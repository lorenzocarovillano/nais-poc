/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [DETT_TIT_CONT, TIT_CONT, TRCH_DI_GAR]
 * 
 */
public interface IDettTitContTrchDiGar1 extends BaseSqlTo {

	/**
	 * Host Variable LDBVE091-PRE-LRD
	 * 
	 */
	AfDecimal getLdbve091PreLrd();

	void setLdbve091PreLrd(AfDecimal ldbve091PreLrd);

	/**
	 * Nullable property for LDBVE091-PRE-LRD
	 * 
	 */
	AfDecimal getLdbve091PreLrdObj();

	void setLdbve091PreLrdObj(AfDecimal ldbve091PreLrdObj);

	/**
	 * Host Variable LDBVE091-ID-TGA
	 * 
	 */
	int getLdbve091IdTga();

	void setLdbve091IdTga(int ldbve091IdTga);

	/**
	 * Host Variable LDBVE091-DATA-INIZIO-DB
	 * 
	 */
	String getLdbve091DataInizioDb();

	void setLdbve091DataInizioDb(String ldbve091DataInizioDb);

	/**
	 * Host Variable LDBVE091-DATA-FINE-DB
	 * 
	 */
	String getLdbve091DataFineDb();

	void setLdbve091DataFineDb(String ldbve091DataFineDb);

	/**
	 * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
	 * 
	 */
	int getIdsv0003CodiceCompagniaAnia();

	void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

	/**
	 * Host Variable WS-DATA-INIZIO-EFFETTO-DB
	 * 
	 */
	String getWsDataInizioEffettoDb();

	void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

	/**
	 * Host Variable WS-TS-COMPETENZA
	 * 
	 */
	long getWsTsCompetenza();

	void setWsTsCompetenza(long wsTsCompetenza);
};

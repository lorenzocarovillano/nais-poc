/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;

import it.accenture.jnais.commons.data.to.ISinonimoStr;

/**
 * Data Access Object(DAO) for table [SINONIMO_STR]
 * 
 */
public class SinonimoStrDao extends BaseSqlDao<ISinonimoStr> {

	public SinonimoStrDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<ISinonimoStr> getToClass() {
		return ISinonimoStr.class;
	}

	public String selectRec(int codCompagniaAnia, String sstCodStrDato, String operazione, char flagModEsecutiva, String dft) {
		return buildQuery("selectRec").bind("codCompagniaAnia", codCompagniaAnia).bind("sstCodStrDato", sstCodStrDato).bind("operazione", operazione)
				.bind("flagModEsecutiva", String.valueOf(flagModEsecutiva)).scalarResultString(dft);
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;

import it.accenture.jnais.commons.data.to.IRidefDatoStr;

/**
 * Data Access Object(DAO) for table [RIDEF_DATO_STR]
 * 
 */
public class RidefDatoStrDao extends BaseSqlDao<IRidefDatoStr> {

	public RidefDatoStrDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IRidefDatoStr> getToClass() {
		return IRidefDatoStr.class;
	}

	public String selectRec(int codCompagniaAnia, int tipoMovimento, String rdsCodDato, String dft) {
		return buildQuery("selectRec").bind("codCompagniaAnia", codCompagniaAnia).bind("tipoMovimento", tipoMovimento).bind("rdsCodDato", rdsCodDato)
				.scalarResultString(dft);
	}
}

package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IAdesPoli;

/**
 * Data Access Object(DAO) for tables [ADES, POLI]
 * 
 */
public class AdesPoliDao extends BaseSqlDao<IAdesPoli> {

    private Cursor curSc01Range;
    private Cursor curSc01;
    private Cursor curSc02;
    private Cursor curSc03;
    private Cursor curSc04;
    private Cursor curSc05Range;
    private Cursor curSc05;
    private Cursor curSc06;
    private Cursor curSc07;
    private Cursor curSc08;
    private final IRowMapper<IAdesPoli> selectRecRm = buildNamedRowMapper(IAdesPoli.class, "adeIdAdes", "adeIdPoli", "adeIdMoviCrz", "adeIdMoviChiuObj", "adeDtIniEffDb", "adeDtEndEffDb", "adeIbPrevObj", "adeIbOggObj", "adeCodCompAnia", "adeDtDecorDbObj", "adeDtScadDbObj", "adeEtaAScadObj", "adeDurAaObj", "adeDurMmObj", "adeDurGgObj", "adeTpRgmFisc", "adeTpRiatObj", "adeTpModPagTit", "adeTpIasObj", "adeDtVarzTpIasDbObj", "adePreNetIndObj", "adePreLrdIndObj", "adeRatLrdIndObj", "adePrstzIniIndObj", "adeFlCoincAsstoObj", "adeIbDfltObj", "adeModCalcObj", "adeTpFntCnbtvaObj", "adeImpAzObj", "adeImpAderObj", "adeImpTfrObj", "adeImpVoloObj", "adePcAzObj", "adePcAderObj", "adePcTfrObj", "adePcVoloObj", "adeDtNovaRgmFiscDbObj", "adeFlAttivObj", "adeImpRecRitVisObj", "adeImpRecRitAccObj", "adeFlVarzStatTbgcObj", "adeFlProvzaMigrazObj", "adeImpbVisDaRecObj", "adeDtDecorPrestBanDbObj", "adeDtEffVarzStatTDbObj", "adeDsRiga", "adeDsOperSql", "adeDsVer", "adeDsTsIniCptz", "adeDsTsEndCptz", "adeDsUtente", "adeDsStatoElab", "adeCumCnbtCapObj", "adeImpGarCnbtObj", "adeDtUltConsCnbtDbObj", "adeIdenIscFndObj", "adeNumRatPianObj", "adeDtPrescDbObj", "adeConcsPrestObj", "polIdPoli", "polIdMoviCrz", "polIdMoviChiuObj", "polIbOggObj", "polIbProp", "polDtPropDbObj", "polDtIniEffDb", "polDtEndEffDb", "polCodCompAnia", "polDtDecorDb", "polDtEmisDb", "polTpPoli", "polDurAaObj", "polDurMmObj", "polDtScadDbObj", "polCodProd", "polDtIniVldtProdDb", "polCodConvObj", "polCodRamoObj", "polDtIniVldtConvDbObj", "polDtApplzConvDbObj", "polTpFrmAssva", "polTpRgmFiscObj", "polFlEstasObj", "polFlRshComunObj", "polFlRshComunCondObj", "polTpLivGenzTit", "polFlCopFinanzObj", "polTpApplzDirObj", "polSpeMedObj", "polDirEmisObj", "polDir1oVersObj", "polDirVersAggObj", "polCodDvsObj", "polFlFntAzObj", "polFlFntAderObj", "polFlFntTfrObj", "polFlFntVoloObj", "polTpOpzAScadObj", "polAaDiffProrDfltObj", "polFlVerProdObj", "polDurGgObj", "polDirQuietObj", "polTpPtfEstnoObj", "polFlCumPreCntrObj", "polFlAmmbMoviObj", "polConvGecoObj", "polDsRiga", "polDsOperSql", "polDsVer", "polDsTsIniCptz", "polDsTsEndCptz", "polDsUtente", "polDsStatoElab", "polFlScudoFiscObj", "polFlTrasfeObj", "polFlTfrStrcObj", "polDtPrescDbObj", "polCodConvAggObj", "polSubcatProdObj", "polFlQuestAdegzAssObj", "polCodTpaObj", "polIdAccCommObj", "polFlPoliCpiPrObj", "polFlPoliBundlingObj", "polIndPoliPrinCollObj", "polFlVndBundleObj", "polIbBsObj", "polFlPoliIfpObj", "stbIdStatOggBus", "stbIdOgg", "stbTpOgg", "stbIdMoviCrz", "stbIdMoviChiuObj", "stbDtIniEffDb", "stbDtEndEffDb", "stbCodCompAnia", "stbTpStatBus", "stbTpCaus", "stbDsRiga", "stbDsOperSql", "stbDsVer", "stbDsTsIniCptz", "stbDsTsEndCptz", "stbDsUtente", "stbDsStatoElab");

    public AdesPoliDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IAdesPoli> getToClass() {
        return IAdesPoli.class;
    }

    public IAdesPoli selectRec(IAdesPoli iAdesPoli) {
        return buildQuery("selectRec").bind(iAdesPoli).rowMapper(selectRecRm).singleResult(iAdesPoli);
    }

    public IAdesPoli selectRec1(IAdesPoli iAdesPoli) {
        return buildQuery("selectRec1").bind(iAdesPoli).rowMapper(selectRecRm).singleResult(iAdesPoli);
    }

    public DbAccessStatus openCurSc01Range(IAdesPoli iAdesPoli) {
        curSc01Range = buildQuery("openCurSc01Range").bind(iAdesPoli).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurSc01(IAdesPoli iAdesPoli) {
        curSc01 = buildQuery("openCurSc01").bind(iAdesPoli).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus closeCurSc01Range() {
        return closeCursor(curSc01Range);
    }

    public DbAccessStatus closeCurSc01() {
        return closeCursor(curSc01);
    }

    public IAdesPoli fetchCurSc01Range(IAdesPoli iAdesPoli) {
        return fetch(curSc01Range, iAdesPoli, selectRecRm);
    }

    public IAdesPoli fetchCurSc01(IAdesPoli iAdesPoli) {
        return fetch(curSc01, iAdesPoli, selectRecRm);
    }

    public IAdesPoli selectRec2(String wlbCodProd, int idsv0003CodiceCompagniaAnia, String wsDtPtfX, long wsDtTsPtf, IAdesPoli iAdesPoli) {
        return buildQuery("selectRec2").bind("wlbCodProd", wlbCodProd).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDtPtfX", wsDtPtfX).bind("wsDtTsPtf", wsDtTsPtf).rowMapper(selectRecRm).singleResult(iAdesPoli);
    }

    public DbAccessStatus openCurSc02(String wlbCodProd, int idsv0003CodiceCompagniaAnia, String wsDtPtfX, long wsDtTsPtf) {
        curSc02 = buildQuery("openCurSc02").bind("wlbCodProd", wlbCodProd).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDtPtfX", wsDtPtfX).bind("wsDtTsPtf", wsDtTsPtf).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus closeCurSc02() {
        return closeCursor(curSc02);
    }

    public IAdesPoli fetchCurSc02(IAdesPoli iAdesPoli) {
        return fetch(curSc02, iAdesPoli, selectRecRm);
    }

    public IAdesPoli selectRec3(IAdesPoli iAdesPoli) {
        return buildQuery("selectRec3").bind(iAdesPoli).rowMapper(selectRecRm).singleResult(iAdesPoli);
    }

    public DbAccessStatus openCurSc03(IAdesPoli iAdesPoli) {
        curSc03 = buildQuery("openCurSc03").bind(iAdesPoli).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus closeCurSc03() {
        return closeCursor(curSc03);
    }

    public IAdesPoli fetchCurSc03(IAdesPoli iAdesPoli) {
        return fetch(curSc03, iAdesPoli, selectRecRm);
    }

    public IAdesPoli selectRec4(IAdesPoli iAdesPoli) {
        return buildQuery("selectRec4").bind(iAdesPoli).rowMapper(selectRecRm).singleResult(iAdesPoli);
    }

    public DbAccessStatus openCurSc04(IAdesPoli iAdesPoli) {
        curSc04 = buildQuery("openCurSc04").bind(iAdesPoli).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus closeCurSc04() {
        return closeCursor(curSc04);
    }

    public IAdesPoli fetchCurSc04(IAdesPoli iAdesPoli) {
        return fetch(curSc04, iAdesPoli, selectRecRm);
    }

    public IAdesPoli selectRec5(IAdesPoli iAdesPoli) {
        return buildQuery("selectRec5").bind(iAdesPoli).rowMapper(selectRecRm).singleResult(iAdesPoli);
    }

    public IAdesPoli selectRec6(IAdesPoli iAdesPoli) {
        return buildQuery("selectRec6").bind(iAdesPoli).rowMapper(selectRecRm).singleResult(iAdesPoli);
    }

    public DbAccessStatus openCurSc05Range(IAdesPoli iAdesPoli) {
        curSc05Range = buildQuery("openCurSc05Range").bind(iAdesPoli).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurSc05(IAdesPoli iAdesPoli) {
        curSc05 = buildQuery("openCurSc05").bind(iAdesPoli).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus closeCurSc05Range() {
        return closeCursor(curSc05Range);
    }

    public DbAccessStatus closeCurSc05() {
        return closeCursor(curSc05);
    }

    public IAdesPoli fetchCurSc05Range(IAdesPoli iAdesPoli) {
        return fetch(curSc05Range, iAdesPoli, selectRecRm);
    }

    public IAdesPoli fetchCurSc05(IAdesPoli iAdesPoli) {
        return fetch(curSc05, iAdesPoli, selectRecRm);
    }

    public IAdesPoli selectRec7(IAdesPoli iAdesPoli) {
        return buildQuery("selectRec7").bind(iAdesPoli).rowMapper(selectRecRm).singleResult(iAdesPoli);
    }

    public DbAccessStatus openCurSc06(IAdesPoli iAdesPoli) {
        curSc06 = buildQuery("openCurSc06").bind(iAdesPoli).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus closeCurSc06() {
        return closeCursor(curSc06);
    }

    public IAdesPoli fetchCurSc06(IAdesPoli iAdesPoli) {
        return fetch(curSc06, iAdesPoli, selectRecRm);
    }

    public IAdesPoli selectRec8(IAdesPoli iAdesPoli) {
        return buildQuery("selectRec8").bind(iAdesPoli).rowMapper(selectRecRm).singleResult(iAdesPoli);
    }

    public DbAccessStatus openCurSc07(IAdesPoli iAdesPoli) {
        curSc07 = buildQuery("openCurSc07").bind(iAdesPoli).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus closeCurSc07() {
        return closeCursor(curSc07);
    }

    public IAdesPoli fetchCurSc07(IAdesPoli iAdesPoli) {
        return fetch(curSc07, iAdesPoli, selectRecRm);
    }

    public IAdesPoli selectRec9(IAdesPoli iAdesPoli) {
        return buildQuery("selectRec9").bind(iAdesPoli).rowMapper(selectRecRm).singleResult(iAdesPoli);
    }

    public DbAccessStatus openCurSc08(IAdesPoli iAdesPoli) {
        curSc08 = buildQuery("openCurSc08").bind(iAdesPoli).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus closeCurSc08() {
        return closeCursor(curSc08);
    }

    public IAdesPoli fetchCurSc08(IAdesPoli iAdesPoli) {
        return fetch(curSc08, iAdesPoli, selectRecRm);
    }
}

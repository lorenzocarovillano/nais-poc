/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [LINGUA_ERRORE]
 * 
 */
public interface ILinguaErrore extends BaseSqlTo {

	/**
	 * Host Variable LER-DESC-ERRORE-BREVE-VCHAR
	 * 
	 */
	String getBreveVchar();

	void setBreveVchar(String breveVchar);

	/**
	 * Host Variable LER-DESC-ERRORE-ESTESA-VCHAR
	 * 
	 */
	String getEstesaVchar();

	void setEstesaVchar(String estesaVchar);
};

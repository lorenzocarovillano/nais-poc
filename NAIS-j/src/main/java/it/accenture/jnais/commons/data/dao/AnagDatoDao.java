/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

import it.accenture.jnais.commons.data.to.IAnagDato;

/**
 * Data Access Object(DAO) for table [ANAG_DATO]
 * 
 */
public class AnagDatoDao extends BaseSqlDao<IAnagDato> {

	private Cursor curAda;
	private final IRowMapper<IAnagDato> selectRecRm = buildNamedRowMapper(IAnagDato.class, "codCompagniaAnia", "codDato", "descDatoVcharObj",
			"tipoDatoObj", "lunghezzaDatoObj", "precisioneDatoObj", "codDominioObj", "formattazioneDatoObj", "dsUtente");

	public AnagDatoDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IAnagDato> getToClass() {
		return IAnagDato.class;
	}

	public IAnagDato selectRec(int codCompagniaAnia, String codDato, IAnagDato iAnagDato) {
		return buildQuery("selectRec").bind("codCompagniaAnia", codCompagniaAnia).bind("codDato", codDato).rowMapper(selectRecRm)
				.singleResult(iAnagDato);
	}

	public DbAccessStatus openCurAda(int compagniaAnia, String dato) {
		curAda = buildQuery("openCurAda").bind("compagniaAnia", compagniaAnia).bind("dato", dato).withHoldCursorsOverCommit().open();
		return dbStatus;
	}

	public DbAccessStatus closeCurAda() {
		return closeCursor(curAda);
	}

	public IAnagDato fetchCurAda(IAnagDato iAnagDato) {
		return fetch(curAda, iAnagDato, selectRecRm);
	}
}

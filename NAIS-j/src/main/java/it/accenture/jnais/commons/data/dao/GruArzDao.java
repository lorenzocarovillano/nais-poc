/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;

import it.accenture.jnais.commons.data.to.IGruArz;

/**
 * Data Access Object(DAO) for table [GRU_ARZ]
 * 
 */
public class GruArzDao extends BaseSqlDao<IGruArz> {

	private final IRowMapper<IGruArz> selectByGruCodGruArzRm = buildNamedRowMapper(IGruArz.class, "gruCodGruArz", "codCompAnia", "codLivOgz",
			"dscGruArzVchar", "datIniGruArzDb", "datFineGruArzDb", "denRespGruArzVchar", "indTlmRespVchar", "codUteIns", "tmstInsRigDb", "codUteAgr",
			"tmstAgrRigDb");

	public GruArzDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IGruArz> getToClass() {
		return IGruArz.class;
	}

	public IGruArz selectByGruCodGruArz(long gruCodGruArz, IGruArz iGruArz) {
		return buildQuery("selectByGruCodGruArz").bind("gruCodGruArz", gruCodGruArz).rowMapper(selectByGruCodGruArzRm).singleResult(iGruArz);
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [LIQ, MOVI, POLI]
 * 
 */
public interface ILiqMoviPoli extends BaseSqlTo {

	/**
	 * Host Variable MOV-DT-EFF-DB
	 * 
	 */
	String getMovDtEffDb();

	void setMovDtEffDb(String movDtEffDb);

	/**
	 * Host Variable POL-DT-INI-EFF-DB
	 * 
	 */
	String getPolDtIniEffDb();

	void setPolDtIniEffDb(String polDtIniEffDb);

	/**
	 * Host Variable LDBV2921-ID-POL
	 * 
	 */
	int getLdbv2921IdPol();

	void setLdbv2921IdPol(int ldbv2921IdPol);

	/**
	 * Host Variable POL-DT-EMIS-DB
	 * 
	 */
	String getPolDtEmisDb();

	void setPolDtEmisDb(String polDtEmisDb);

	/**
	 * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
	 * 
	 */
	int getIdsv0003CodiceCompagniaAnia();

	void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

	/**
	 * Host Variable WS-DATA-INIZIO-EFFETTO-DB
	 * 
	 */
	String getWsDataInizioEffettoDb();

	void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

	/**
	 * Host Variable WS-TS-COMPETENZA
	 * 
	 */
	long getWsTsCompetenza();

	void setWsTsCompetenza(long wsTsCompetenza);
};

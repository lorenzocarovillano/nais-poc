/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;

import it.accenture.jnais.commons.data.to.IGravitaErrore;

/**
 * Data Access Object(DAO) for table [GRAVITA_ERRORE]
 * 
 */
public class GravitaErroreDao extends BaseSqlDao<IGravitaErrore> {

	public GravitaErroreDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IGravitaErrore> getToClass() {
		return IGravitaErrore.class;
	}

	public IGravitaErrore selectRec(int compagniaAnia, int errore, IGravitaErrore iGravitaErrore) {
		return buildQuery("selectRec").bind("compagniaAnia", compagniaAnia).bind("errore", errore).singleResult(iGravitaErrore);
	}
}

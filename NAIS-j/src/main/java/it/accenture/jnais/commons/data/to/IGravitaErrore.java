/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [GRAVITA_ERRORE]
 * 
 */
public interface IGravitaErrore extends BaseSqlTo {

	/**
	 * Host Variable GER-ID-GRAVITA-ERRORE
	 * 
	 */
	int getIdGravitaErrore();

	void setIdGravitaErrore(int idGravitaErrore);

	/**
	 * Host Variable GER-LIVELLO-GRAVITA
	 * 
	 */
	short getLivelloGravita();

	void setLivelloGravita(short livelloGravita);

	/**
	 * Host Variable GER-TIPO-TRATT-FE
	 * 
	 */
	char getTipoTrattFe();

	void setTipoTrattFe(char tipoTrattFe);
};

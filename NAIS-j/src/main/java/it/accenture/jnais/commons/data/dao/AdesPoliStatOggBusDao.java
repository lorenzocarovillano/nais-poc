/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

import it.accenture.jnais.commons.data.to.IAdesPoliStatOggBus;

/**
 * Data Access Object(DAO) for tables [ADES, POLI, STAT_OGG_BUS]
 * 
 */
public class AdesPoliStatOggBusDao extends BaseSqlDao<IAdesPoliStatOggBus> {

	private Cursor curSc01Range;
	private Cursor curSc01;
	private Cursor curSc02;
	private Cursor curSc03;
	private Cursor curSc04;
	private final IRowMapper<IAdesPoliStatOggBus> selectRecRm = buildNamedRowMapper(IAdesPoliStatOggBus.class, "adeIdAdes", "adeIdPoli",
			"adeIdMoviCrz", "adeIdMoviChiuObj", "adeDtIniEffDb", "adeDtEndEffDb", "adeIbPrevObj", "adeIbOggObj", "adeCodCompAnia", "adeDtDecorDbObj",
			"adeDtScadDbObj", "adeEtaAScadObj", "adeDurAaObj", "adeDurMmObj", "adeDurGgObj", "adeTpRgmFisc", "adeTpRiatObj", "adeTpModPagTit",
			"adeTpIasObj", "adeDtVarzTpIasDbObj", "adePreNetIndObj", "adePreLrdIndObj", "adeRatLrdIndObj", "adePrstzIniIndObj", "adeFlCoincAsstoObj",
			"adeIbDfltObj", "adeModCalcObj", "adeTpFntCnbtvaObj", "adeImpAzObj", "adeImpAderObj", "adeImpTfrObj", "adeImpVoloObj", "adePcAzObj",
			"adePcAderObj", "adePcTfrObj", "adePcVoloObj", "adeDtNovaRgmFiscDbObj", "adeFlAttivObj", "adeImpRecRitVisObj", "adeImpRecRitAccObj",
			"adeFlVarzStatTbgcObj", "adeFlProvzaMigrazObj", "adeImpbVisDaRecObj", "adeDtDecorPrestBanDbObj", "adeDtEffVarzStatTDbObj", "adeDsRiga",
			"adeDsOperSql", "adeDsVer", "adeDsTsIniCptz", "adeDsTsEndCptz", "adeDsUtente", "adeDsStatoElab", "adeCumCnbtCapObj", "adeImpGarCnbtObj",
			"adeDtUltConsCnbtDbObj", "adeIdenIscFndObj", "adeNumRatPianObj", "adeDtPrescDbObj", "adeConcsPrestObj", "polIdPoli", "polIdMoviCrz",
			"polIdMoviChiuObj", "polIbOggObj", "polIbProp", "polDtPropDbObj", "polDtIniEffDb", "polDtEndEffDb", "polCodCompAnia", "polDtDecorDb",
			"polDtEmisDb", "polTpPoli", "polDurAaObj", "polDurMmObj", "polDtScadDbObj", "polCodProd", "polDtIniVldtProdDb", "polCodConvObj",
			"polCodRamoObj", "polDtIniVldtConvDbObj", "polDtApplzConvDbObj", "polTpFrmAssva", "polTpRgmFiscObj", "polFlEstasObj", "polFlRshComunObj",
			"polFlRshComunCondObj", "polTpLivGenzTit", "polFlCopFinanzObj", "polTpApplzDirObj", "polSpeMedObj", "polDirEmisObj", "polDir1oVersObj",
			"polDirVersAggObj", "polCodDvsObj", "polFlFntAzObj", "polFlFntAderObj", "polFlFntTfrObj", "polFlFntVoloObj", "polTpOpzAScadObj",
			"polAaDiffProrDfltObj", "polFlVerProdObj", "polDurGgObj", "polDirQuietObj", "polTpPtfEstnoObj", "polFlCumPreCntrObj", "polFlAmmbMoviObj",
			"polConvGecoObj", "polDsRiga", "polDsOperSql", "polDsVer", "polDsTsIniCptz", "polDsTsEndCptz", "polDsUtente", "polDsStatoElab",
			"polFlScudoFiscObj", "polFlTrasfeObj", "polFlTfrStrcObj", "polDtPrescDbObj", "polCodConvAggObj", "polSubcatProdObj",
			"polFlQuestAdegzAssObj", "polCodTpaObj", "polIdAccCommObj", "polFlPoliCpiPrObj", "polFlPoliBundlingObj", "polIndPoliPrinCollObj",
			"polFlVndBundleObj", "polIbBsObj", "polFlPoliIfpObj", "stbIdStatOggBus", "stbIdOgg", "stbTpOgg", "stbIdMoviCrz", "stbIdMoviChiuObj",
			"stbDtIniEffDb", "stbDtEndEffDb", "stbCodCompAnia", "stbTpStatBus", "stbTpCaus", "stbDsRiga", "stbDsOperSql", "stbDsVer",
			"stbDsTsIniCptz", "stbDsTsEndCptz", "stbDsUtente", "stbDsStatoElab");

	public AdesPoliStatOggBusDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IAdesPoliStatOggBus> getToClass() {
		return IAdesPoliStatOggBus.class;
	}

	public IAdesPoliStatOggBus selectRec(IAdesPoliStatOggBus iAdesPoliStatOggBus) {
		return buildQuery("selectRec").bind(iAdesPoliStatOggBus).rowMapper(selectRecRm).singleResult(iAdesPoliStatOggBus);
	}

	public IAdesPoliStatOggBus selectRec1(IAdesPoliStatOggBus iAdesPoliStatOggBus) {
		return buildQuery("selectRec1").bind(iAdesPoliStatOggBus).rowMapper(selectRecRm).singleResult(iAdesPoliStatOggBus);
	}

	public DbAccessStatus openCurSc01Range(IAdesPoliStatOggBus iAdesPoliStatOggBus) {
		curSc01Range = buildQuery("openCurSc01Range").bind(iAdesPoliStatOggBus).withHoldCursorsOverCommit().open();
		return dbStatus;
	}

	public DbAccessStatus openCurSc01(IAdesPoliStatOggBus iAdesPoliStatOggBus) {
		curSc01 = buildQuery("openCurSc01").bind(iAdesPoliStatOggBus).withHoldCursorsOverCommit().open();
		return dbStatus;
	}

	public DbAccessStatus closeCurSc01Range() {
		return closeCursor(curSc01Range);
	}

	public DbAccessStatus closeCurSc01() {
		return closeCursor(curSc01);
	}

	public IAdesPoliStatOggBus fetchCurSc01Range(IAdesPoliStatOggBus iAdesPoliStatOggBus) {
		return fetch(curSc01Range, iAdesPoliStatOggBus, selectRecRm);
	}

	public IAdesPoliStatOggBus fetchCurSc01(IAdesPoliStatOggBus iAdesPoliStatOggBus) {
		return fetch(curSc01, iAdesPoliStatOggBus, selectRecRm);
	}

	public IAdesPoliStatOggBus selectRec2(String wlbCodProd, int idsv0003CodiceCompagniaAnia, String wsDtPtfX, long wsDtTsPtf,
			IAdesPoliStatOggBus iAdesPoliStatOggBus) {
		return buildQuery("selectRec2").bind("wlbCodProd", wlbCodProd).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia)
				.bind("wsDtPtfX", wsDtPtfX).bind("wsDtTsPtf", wsDtTsPtf).rowMapper(selectRecRm).singleResult(iAdesPoliStatOggBus);
	}

	public DbAccessStatus openCurSc02(String wlbCodProd, int idsv0003CodiceCompagniaAnia, String wsDtPtfX, long wsDtTsPtf) {
		curSc02 = buildQuery("openCurSc02").bind("wlbCodProd", wlbCodProd).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia)
				.bind("wsDtPtfX", wsDtPtfX).bind("wsDtTsPtf", wsDtTsPtf).withHoldCursorsOverCommit().open();
		return dbStatus;
	}

	public DbAccessStatus closeCurSc02() {
		return closeCursor(curSc02);
	}

	public IAdesPoliStatOggBus fetchCurSc02(IAdesPoliStatOggBus iAdesPoliStatOggBus) {
		return fetch(curSc02, iAdesPoliStatOggBus, selectRecRm);
	}

	public IAdesPoliStatOggBus selectRec3(IAdesPoliStatOggBus iAdesPoliStatOggBus) {
		return buildQuery("selectRec3").bind(iAdesPoliStatOggBus).rowMapper(selectRecRm).singleResult(iAdesPoliStatOggBus);
	}

	public DbAccessStatus openCurSc03(IAdesPoliStatOggBus iAdesPoliStatOggBus) {
		curSc03 = buildQuery("openCurSc03").bind(iAdesPoliStatOggBus).withHoldCursorsOverCommit().open();
		return dbStatus;
	}

	public DbAccessStatus closeCurSc03() {
		return closeCursor(curSc03);
	}

	public IAdesPoliStatOggBus fetchCurSc03(IAdesPoliStatOggBus iAdesPoliStatOggBus) {
		return fetch(curSc03, iAdesPoliStatOggBus, selectRecRm);
	}

	public IAdesPoliStatOggBus selectRec4(IAdesPoliStatOggBus iAdesPoliStatOggBus) {
		return buildQuery("selectRec4").bind(iAdesPoliStatOggBus).rowMapper(selectRecRm).singleResult(iAdesPoliStatOggBus);
	}

	public DbAccessStatus openCurSc04(IAdesPoliStatOggBus iAdesPoliStatOggBus) {
		curSc04 = buildQuery("openCurSc04").bind(iAdesPoliStatOggBus).withHoldCursorsOverCommit().open();
		return dbStatus;
	}

	public DbAccessStatus closeCurSc04() {
		return closeCursor(curSc04);
	}

	public IAdesPoliStatOggBus fetchCurSc04(IAdesPoliStatOggBus iAdesPoliStatOggBus) {
		return fetch(curSc04, iAdesPoliStatOggBus, selectRecRm);
	}

	public IAdesPoliStatOggBus selectRec5(IAdesPoliStatOggBus iAdesPoliStatOggBus) {
		return buildQuery("selectRec5").bind(iAdesPoliStatOggBus).rowMapper(selectRecRm).singleResult(iAdesPoliStatOggBus);
	}

	public IAdesPoliStatOggBus selectRec6(IAdesPoliStatOggBus iAdesPoliStatOggBus) {
		return buildQuery("selectRec6").bind(iAdesPoliStatOggBus).rowMapper(selectRecRm).singleResult(iAdesPoliStatOggBus);
	}
}

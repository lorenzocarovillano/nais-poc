/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WGRZ-ID-ADES<br>
 * Variable: WGRZ-ID-ADES from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzIdAdes extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WgrzIdAdes() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WGRZ_ID_ADES;
	}

	public void setWgrzIdAdes(int wgrzIdAdes) {
		writeIntAsPacked(Pos.WGRZ_ID_ADES, wgrzIdAdes, Len.Int.WGRZ_ID_ADES);
	}

	public void setWgrzIdAdesFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.WGRZ_ID_ADES, Pos.WGRZ_ID_ADES);
	}

	/**Original name: WGRZ-ID-ADES<br>*/
	public int getWgrzIdAdes() {
		return readPackedAsInt(Pos.WGRZ_ID_ADES, Len.Int.WGRZ_ID_ADES);
	}

	public byte[] getWgrzIdAdesAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WGRZ_ID_ADES, Pos.WGRZ_ID_ADES);
		return buffer;
	}

	public void initWgrzIdAdesSpaces() {
		fill(Pos.WGRZ_ID_ADES, Len.WGRZ_ID_ADES, Types.SPACE_CHAR);
	}

	public void setWgrzIdAdesNull(String wgrzIdAdesNull) {
		writeString(Pos.WGRZ_ID_ADES_NULL, wgrzIdAdesNull, Len.WGRZ_ID_ADES_NULL);
	}

	/**Original name: WGRZ-ID-ADES-NULL<br>*/
	public String getWgrzIdAdesNull() {
		return readString(Pos.WGRZ_ID_ADES_NULL, Len.WGRZ_ID_ADES_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WGRZ_ID_ADES = 1;
		public static final int WGRZ_ID_ADES_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WGRZ_ID_ADES = 5;
		public static final int WGRZ_ID_ADES_NULL = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WGRZ_ID_ADES = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

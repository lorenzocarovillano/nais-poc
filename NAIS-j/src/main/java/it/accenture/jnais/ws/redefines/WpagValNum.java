/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WPAG-VAL-NUM<br>
 * Variable: WPAG-VAL-NUM from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagValNum extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WpagValNum() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WPAG_VAL_NUM;
	}

	public void setWpagValNumFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.WPAG_VAL_NUM, Pos.WPAG_VAL_NUM);
	}

	public byte[] getWpagValNumAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WPAG_VAL_NUM, Pos.WPAG_VAL_NUM);
		return buffer;
	}

	public void initWpagValNumSpaces() {
		fill(Pos.WPAG_VAL_NUM, Len.WPAG_VAL_NUM, Types.SPACE_CHAR);
	}

	public void setWpagValNumNull(String wpagValNumNull) {
		writeString(Pos.WPAG_VAL_NUM_NULL, wpagValNumNull, Len.WPAG_VAL_NUM_NULL);
	}

	/**Original name: WPAG-VAL-NUM-NULL<br>*/
	public String getWpagValNumNull() {
		return readString(Pos.WPAG_VAL_NUM_NULL, Len.WPAG_VAL_NUM_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WPAG_VAL_NUM = 1;
		public static final int WPAG_VAL_NUM_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WPAG_VAL_NUM = 3;
		public static final int WPAG_VAL_NUM_NULL = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

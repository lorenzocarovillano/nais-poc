/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WBEL-ID-ASSTO<br>
 * Variable: WBEL-ID-ASSTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelIdAssto extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WbelIdAssto() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WBEL_ID_ASSTO;
	}

	public void setWbelIdAssto(int wbelIdAssto) {
		writeIntAsPacked(Pos.WBEL_ID_ASSTO, wbelIdAssto, Len.Int.WBEL_ID_ASSTO);
	}

	public void setWbelIdAsstoFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.WBEL_ID_ASSTO, Pos.WBEL_ID_ASSTO);
	}

	/**Original name: WBEL-ID-ASSTO<br>*/
	public int getWbelIdAssto() {
		return readPackedAsInt(Pos.WBEL_ID_ASSTO, Len.Int.WBEL_ID_ASSTO);
	}

	public byte[] getWbelIdAsstoAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WBEL_ID_ASSTO, Pos.WBEL_ID_ASSTO);
		return buffer;
	}

	public void initWbelIdAsstoSpaces() {
		fill(Pos.WBEL_ID_ASSTO, Len.WBEL_ID_ASSTO, Types.SPACE_CHAR);
	}

	public void setWbelIdAsstoNull(String wbelIdAsstoNull) {
		writeString(Pos.WBEL_ID_ASSTO_NULL, wbelIdAsstoNull, Len.WBEL_ID_ASSTO_NULL);
	}

	/**Original name: WBEL-ID-ASSTO-NULL<br>*/
	public String getWbelIdAsstoNull() {
		return readString(Pos.WBEL_ID_ASSTO_NULL, Len.WBEL_ID_ASSTO_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WBEL_ID_ASSTO = 1;
		public static final int WBEL_ID_ASSTO_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WBEL_ID_ASSTO = 5;
		public static final int WBEL_ID_ASSTO_NULL = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WBEL_ID_ASSTO = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-AREA-DISPLAY<br>
 * Variable: WK-AREA-DISPLAY from program IDSS8880<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class WkAreaDisplay extends BytesClass {

	//==== CONSTRUCTORS ====
	public WkAreaDisplay() {
	}

	public WkAreaDisplay(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WK_AREA_DISPLAY;
	}

	public void setWkAreaDisplay(String wkAreaDisplay) {
		writeString(Pos.WK_AREA_DISPLAY, wkAreaDisplay, Len.WK_AREA_DISPLAY);
	}

	public String getWkAreaDisplay() {
		return readString(Pos.WK_AREA_DISPLAY, Len.WK_AREA_DISPLAY);
	}

	public String getWkAreaDisplayFormatted() {
		return Functions.padBlanks(getWkAreaDisplay(), Len.WK_AREA_DISPLAY);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WK_AREA_DISPLAY = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WK_AREA_DISPLAY = 125;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

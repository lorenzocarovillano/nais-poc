/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.occurs;

/**Original name: IDSV0045-STR-INP-OUT<br>
 * Variables: IDSV0045-STR-INP-OUT from copybook IDSV0009<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Idsv0045StrInpOut {

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CODICE_STR_DATO = 30;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

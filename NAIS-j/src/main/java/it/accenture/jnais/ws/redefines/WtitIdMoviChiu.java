/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WTIT-ID-MOVI-CHIU<br>
 * Variable: WTIT-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitIdMoviChiu extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WtitIdMoviChiu() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WTIT_ID_MOVI_CHIU;
	}

	public void setWtitIdMoviChiu(int wtitIdMoviChiu) {
		writeIntAsPacked(Pos.WTIT_ID_MOVI_CHIU, wtitIdMoviChiu, Len.Int.WTIT_ID_MOVI_CHIU);
	}

	public void setWtitIdMoviChiuFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.WTIT_ID_MOVI_CHIU, Pos.WTIT_ID_MOVI_CHIU);
	}

	/**Original name: WTIT-ID-MOVI-CHIU<br>*/
	public int getWtitIdMoviChiu() {
		return readPackedAsInt(Pos.WTIT_ID_MOVI_CHIU, Len.Int.WTIT_ID_MOVI_CHIU);
	}

	public byte[] getWtitIdMoviChiuAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WTIT_ID_MOVI_CHIU, Pos.WTIT_ID_MOVI_CHIU);
		return buffer;
	}

	public void initWtitIdMoviChiuSpaces() {
		fill(Pos.WTIT_ID_MOVI_CHIU, Len.WTIT_ID_MOVI_CHIU, Types.SPACE_CHAR);
	}

	public void setWtitIdMoviChiuNull(String wtitIdMoviChiuNull) {
		writeString(Pos.WTIT_ID_MOVI_CHIU_NULL, wtitIdMoviChiuNull, Len.WTIT_ID_MOVI_CHIU_NULL);
	}

	/**Original name: WTIT-ID-MOVI-CHIU-NULL<br>*/
	public String getWtitIdMoviChiuNull() {
		return readString(Pos.WTIT_ID_MOVI_CHIU_NULL, Len.WTIT_ID_MOVI_CHIU_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WTIT_ID_MOVI_CHIU = 1;
		public static final int WTIT_ID_MOVI_CHIU_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WTIT_ID_MOVI_CHIU = 5;
		public static final int WTIT_ID_MOVI_CHIU_NULL = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WTIT_ID_MOVI_CHIU = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

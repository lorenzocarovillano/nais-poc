/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-ZERO-OPZIONI<br>
 * Variable: SW-ZERO-OPZIONI from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwZeroOpzioni {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char OK = 'S';
	public static final char KO = 'N';

	//==== METHODS ====
	public void setSwZeroOpzioni(char swZeroOpzioni) {
		this.value = swZeroOpzioni;
	}

	public char getSwZeroOpzioni() {
		return this.value;
	}

	public void setKo() {
		value = KO;
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws;

import it.accenture.jnais.copy.AreaLdbv1131;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndParamOgg;
import it.accenture.jnais.copy.ParamOggDb;
import it.accenture.jnais.ws.redefines.WsTimestamp;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS1130<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs1130Data {

	//==== PROPERTIES ====
	//Original name: DESCRIZ-ERR-DB2
	private String descrizErrDb2 = "";
	//Original name: WS-TIMESTAMP
	private WsTimestamp wsTimestamp = new WsTimestamp();
	//Original name: IDSV0010
	private Idsv0010 idsv0010 = new Idsv0010();
	//Original name: IND-PARAM-OGG
	private IndParamOgg indParamOgg = new IndParamOgg();
	//Original name: PARAM-OGG-DB
	private ParamOggDb paramOggDb = new ParamOggDb();
	//Original name: AREA-LDBV1131
	private AreaLdbv1131 areaLdbv1131 = new AreaLdbv1131();

	//==== METHODS ====
	public String getDescrizErrDb2() {
		return this.descrizErrDb2;
	}

	public AreaLdbv1131 getAreaLdbv1131() {
		return areaLdbv1131;
	}

	public Idsv0010 getIdsv0010() {
		return idsv0010;
	}

	public IndParamOgg getIndParamOgg() {
		return indParamOgg;
	}

	public ParamOggDb getParamOggDb() {
		return paramOggDb;
	}

	public WsTimestamp getWsTimestamp() {
		return wsTimestamp;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_ID_MOVI_CRZ = 9;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

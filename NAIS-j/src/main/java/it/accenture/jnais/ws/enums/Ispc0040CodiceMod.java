/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: ISPC0040-CODICE-MOD<br>
 * Variable: ISPC0040-CODICE-MOD from copybook ISPC0040<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispc0040CodiceMod {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char ETA_SCADENZA = '1';
	public static final char DATA_FISSA = '2';
	public static final char LIBERA = '3';

	//==== METHODS ====
	public void setCodiceMod(char codiceMod) {
		this.value = codiceMod;
	}

	public char getCodiceMod() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CODICE_MOD = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: MOVIMENTO-CEDOLA<br>
 * Variable: MOVIMENTO-CEDOLA from program LVES0270<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class MovimentoCedola {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NO = 'N';
	public static final char SI = 'S';

	//==== METHODS ====
	public void setMovimentoCedola(char movimentoCedola) {
		this.value = movimentoCedola;
	}

	public char getMovimentoCedola() {
		return this.value;
	}

	public void setNo() {
		value = NO;
	}

	public boolean isSi() {
		return value == SI;
	}

	public void setSi() {
		value = SI;
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WALL-ID-MOVI-CHIU<br>
 * Variable: WALL-ID-MOVI-CHIU from program LCCS1900<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WallIdMoviChiu extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WallIdMoviChiu() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WALL_ID_MOVI_CHIU;
	}

	public void setWallIdMoviChiu(int wallIdMoviChiu) {
		writeIntAsPacked(Pos.WALL_ID_MOVI_CHIU, wallIdMoviChiu, Len.Int.WALL_ID_MOVI_CHIU);
	}

	public void setWallIdMoviChiuFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.WALL_ID_MOVI_CHIU, Pos.WALL_ID_MOVI_CHIU);
	}

	/**Original name: WALL-ID-MOVI-CHIU<br>*/
	public int getWallIdMoviChiu() {
		return readPackedAsInt(Pos.WALL_ID_MOVI_CHIU, Len.Int.WALL_ID_MOVI_CHIU);
	}

	public byte[] getWallIdMoviChiuAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WALL_ID_MOVI_CHIU, Pos.WALL_ID_MOVI_CHIU);
		return buffer;
	}

	public void initWallIdMoviChiuSpaces() {
		fill(Pos.WALL_ID_MOVI_CHIU, Len.WALL_ID_MOVI_CHIU, Types.SPACE_CHAR);
	}

	public void setWallIdMoviChiuNull(String wallIdMoviChiuNull) {
		writeString(Pos.WALL_ID_MOVI_CHIU_NULL, wallIdMoviChiuNull, Len.WALL_ID_MOVI_CHIU_NULL);
	}

	/**Original name: WALL-ID-MOVI-CHIU-NULL<br>*/
	public String getWallIdMoviChiuNull() {
		return readString(Pos.WALL_ID_MOVI_CHIU_NULL, Len.WALL_ID_MOVI_CHIU_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WALL_ID_MOVI_CHIU = 1;
		public static final int WALL_ID_MOVI_CHIU_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WALL_ID_MOVI_CHIU = 5;
		public static final int WALL_ID_MOVI_CHIU_NULL = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WALL_ID_MOVI_CHIU = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WCOM-DER-FLAG-ST-FINALE<br>
 * Variable: WCOM-DER-FLAG-ST-FINALE from copybook LCCC0001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WcomDerFlagStFinale {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char FINALE_CON_EFF = '1';
	public static final char FINALE_SENZA_EFF = '2';
	public static final char NON_FINALE = '3';

	//==== METHODS ====
	public void setDerFlagStFinale(char derFlagStFinale) {
		this.value = derFlagStFinale;
	}

	public char getDerFlagStFinale() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DER_FLAG_ST_FINALE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMP-LRD-CALC<br>
 * Variable: DFL-IMP-LRD-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpLrdCalc extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public DflImpLrdCalc() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DFL_IMP_LRD_CALC;
	}

	public void setDflImpLrdCalc(AfDecimal dflImpLrdCalc) {
		writeDecimalAsPacked(Pos.DFL_IMP_LRD_CALC, dflImpLrdCalc.copy());
	}

	public void setDflImpLrdCalcFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.DFL_IMP_LRD_CALC, Pos.DFL_IMP_LRD_CALC);
	}

	/**Original name: DFL-IMP-LRD-CALC<br>*/
	public AfDecimal getDflImpLrdCalc() {
		return readPackedAsDecimal(Pos.DFL_IMP_LRD_CALC, Len.Int.DFL_IMP_LRD_CALC, Len.Fract.DFL_IMP_LRD_CALC);
	}

	public byte[] getDflImpLrdCalcAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.DFL_IMP_LRD_CALC, Pos.DFL_IMP_LRD_CALC);
		return buffer;
	}

	public void setDflImpLrdCalcNull(String dflImpLrdCalcNull) {
		writeString(Pos.DFL_IMP_LRD_CALC_NULL, dflImpLrdCalcNull, Len.DFL_IMP_LRD_CALC_NULL);
	}

	/**Original name: DFL-IMP-LRD-CALC-NULL<br>*/
	public String getDflImpLrdCalcNull() {
		return readString(Pos.DFL_IMP_LRD_CALC_NULL, Len.DFL_IMP_LRD_CALC_NULL);
	}

	public String getDflImpLrdCalcNullFormatted() {
		return Functions.padBlanks(getDflImpLrdCalcNull(), Len.DFL_IMP_LRD_CALC_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int DFL_IMP_LRD_CALC = 1;
		public static final int DFL_IMP_LRD_CALC_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int DFL_IMP_LRD_CALC = 8;
		public static final int DFL_IMP_LRD_CALC_NULL = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int DFL_IMP_LRD_CALC = 12;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int DFL_IMP_LRD_CALC = 3;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}

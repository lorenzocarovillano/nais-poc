/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: FLAG-ACCESSO-X-RANGE<br>
 * Variable: FLAG-ACCESSO-X-RANGE from program LDBM0170<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagAccessoXRangeLdbm0170 {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char SI = 'S';
	public static final char NO = 'N';

	//==== METHODS ====
	public void setFlagAccessoXRange(char flagAccessoXRange) {
		this.value = flagAccessoXRange;
	}

	public char getFlagAccessoXRange() {
		return this.value;
	}

	public boolean isSi() {
		return value == SI;
	}

	public void setSi() {
		value = SI;
	}

	public void setNo() {
		value = NO;
	}
}

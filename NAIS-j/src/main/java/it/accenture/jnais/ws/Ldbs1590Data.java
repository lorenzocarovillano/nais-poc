/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.ImpstSost;
import it.accenture.jnais.copy.IndImpstSost;
import it.accenture.jnais.copy.RichDb;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS1590<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs1590Data {

	//==== PROPERTIES ====
	//Original name: DESCRIZ-ERR-DB2
	private String descrizErrDb2 = "";
	//Original name: IDSV0010
	private Idsv0010 idsv0010 = new Idsv0010();
	//Original name: IND-IMPST-SOST
	private IndImpstSost indImpstSost = new IndImpstSost();
	//Original name: IMPST-SOST-DB
	private RichDb impstSostDb = new RichDb();
	//Original name: IMPST-SOST
	private ImpstSost impstSost = new ImpstSost();

	//==== METHODS ====
	public String getDescrizErrDb2() {
		return this.descrizErrDb2;
	}

	public Idsv0010 getIdsv0010() {
		return idsv0010;
	}

	public ImpstSost getImpstSost() {
		return impstSost;
	}

	public RichDb getImpstSostDb() {
		return impstSostDb;
	}

	public IndImpstSost getIndImpstSost() {
		return indImpstSost;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_ID_MOVI_CRZ = 9;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

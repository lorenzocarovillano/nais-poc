/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISPC0040-CODICE-ADESIONE<br>
 * Variable: ISPC0040-CODICE-ADESIONE from copybook ISPC0040<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispc0040CodiceAdesione {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.CODICE_ADESIONE);
	public static final String FACOLTIVA = "AF";
	public static final String OBBLIGATORIA = "AO";
	public static final String NON_AMMESSA = "NA";

	//==== METHODS ====
	public void setCodiceAdesione(String codiceAdesione) {
		this.value = Functions.subString(codiceAdesione, Len.CODICE_ADESIONE);
	}

	public String getCodiceAdesione() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CODICE_ADESIONE = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

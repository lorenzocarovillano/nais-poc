/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PRSTZ-AGG-INI<br>
 * Variable: B03-PRSTZ-AGG-INI from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03PrstzAggIni extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public B03PrstzAggIni() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.B03_PRSTZ_AGG_INI;
	}

	public void setB03PrstzAggIni(AfDecimal b03PrstzAggIni) {
		writeDecimalAsPacked(Pos.B03_PRSTZ_AGG_INI, b03PrstzAggIni.copy());
	}

	public void setB03PrstzAggIniFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.B03_PRSTZ_AGG_INI, Pos.B03_PRSTZ_AGG_INI);
	}

	/**Original name: B03-PRSTZ-AGG-INI<br>*/
	public AfDecimal getB03PrstzAggIni() {
		return readPackedAsDecimal(Pos.B03_PRSTZ_AGG_INI, Len.Int.B03_PRSTZ_AGG_INI, Len.Fract.B03_PRSTZ_AGG_INI);
	}

	public byte[] getB03PrstzAggIniAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.B03_PRSTZ_AGG_INI, Pos.B03_PRSTZ_AGG_INI);
		return buffer;
	}

	public void setB03PrstzAggIniNull(String b03PrstzAggIniNull) {
		writeString(Pos.B03_PRSTZ_AGG_INI_NULL, b03PrstzAggIniNull, Len.B03_PRSTZ_AGG_INI_NULL);
	}

	/**Original name: B03-PRSTZ-AGG-INI-NULL<br>*/
	public String getB03PrstzAggIniNull() {
		return readString(Pos.B03_PRSTZ_AGG_INI_NULL, Len.B03_PRSTZ_AGG_INI_NULL);
	}

	public String getB03PrstzAggIniNullFormatted() {
		return Functions.padBlanks(getB03PrstzAggIniNull(), Len.B03_PRSTZ_AGG_INI_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int B03_PRSTZ_AGG_INI = 1;
		public static final int B03_PRSTZ_AGG_INI_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int B03_PRSTZ_AGG_INI = 8;
		public static final int B03_PRSTZ_AGG_INI_NULL = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Fract {

			//==== PROPERTIES ====
			public static final int B03_PRSTZ_AGG_INI = 3;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}

		public static class Int {

			//==== PROPERTIES ====
			public static final int B03_PRSTZ_AGG_INI = 12;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

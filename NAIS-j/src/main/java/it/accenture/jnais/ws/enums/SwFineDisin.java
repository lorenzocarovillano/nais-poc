/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-FINE-DISIN<br>
 * Variable: SW-FINE-DISIN from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwFineDisin {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char DISIN = 'S';
	public static final char DISIN_NO = 'N';

	//==== METHODS ====
	public void setSwFineDisin(char swFineDisin) {
		this.value = swFineDisin;
	}

	public char getSwFineDisin() {
		return this.value;
	}

	public void setDisin() {
		value = DISIN;
	}

	public boolean isDisinNo() {
		return value == DISIN_NO;
	}

	public void setDisinNo() {
		value = DISIN_NO;
	}
}

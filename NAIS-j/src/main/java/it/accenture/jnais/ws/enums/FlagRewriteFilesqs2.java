/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-REWRITE-FILESQS2<br>
 * Variable: FLAG-REWRITE-FILESQS2 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagRewriteFilesqs2 {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char SI = 'S';
	public static final char NO = 'N';

	//==== METHODS ====
	public void setFlagRewriteFilesqs2(char flagRewriteFilesqs2) {
		this.value = flagRewriteFilesqs2;
	}

	public char getFlagRewriteFilesqs2() {
		return this.value;
	}

	public boolean isSi() {
		return value == SI;
	}

	public void setNo() {
		value = NO;
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-TRANCHE-DA-CARICARE<br>
 * Variable: WK-TRANCHE-DA-CARICARE from program LOAS0310<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkTrancheDaCaricare {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NON_CARICATA = 'N';
	public static final char CARICATA = 'S';

	//==== METHODS ====
	public void setWkTrancheDaCaricare(char wkTrancheDaCaricare) {
		this.value = wkTrancheDaCaricare;
	}

	public char getWkTrancheDaCaricare() {
		return this.value;
	}

	public void setNonCaricata() {
		value = NON_CARICATA;
	}

	public boolean isCaricata() {
		return value == CARICATA;
	}

	public void setCaricata() {
		value = CARICATA;
	}
}

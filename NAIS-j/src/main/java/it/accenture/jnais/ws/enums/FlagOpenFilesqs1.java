/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-OPEN-FILESQS1<br>
 * Variable: FLAG-OPEN-FILESQS1 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagOpenFilesqs1 {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char SI = 'S';
	public static final char NO = 'N';

	//==== METHODS ====
	public void setFlagOpenFilesqs1(char flagOpenFilesqs1) {
		this.value = flagOpenFilesqs1;
	}

	public char getFlagOpenFilesqs1() {
		return this.value;
	}

	public boolean isSi() {
		return value == SI;
	}

	public void setOpenFilesqs1Si() {
		value = SI;
	}

	public void setOpenFilesqs1No() {
		value = NO;
	}
}

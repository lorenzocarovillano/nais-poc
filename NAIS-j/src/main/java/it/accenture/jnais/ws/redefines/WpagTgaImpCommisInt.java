/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WPAG-TGA-IMP-COMMIS-INT<br>
 * Variable: WPAG-TGA-IMP-COMMIS-INT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagTgaImpCommisInt extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WpagTgaImpCommisInt() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WPAG_TGA_IMP_COMMIS_INT;
	}

	public void setWpagTgaImpCommisIntFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.WPAG_TGA_IMP_COMMIS_INT, Pos.WPAG_TGA_IMP_COMMIS_INT);
	}

	public byte[] getWpagTgaImpCommisIntAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WPAG_TGA_IMP_COMMIS_INT, Pos.WPAG_TGA_IMP_COMMIS_INT);
		return buffer;
	}

	public void initWpagTgaImpCommisIntSpaces() {
		fill(Pos.WPAG_TGA_IMP_COMMIS_INT, Len.WPAG_TGA_IMP_COMMIS_INT, Types.SPACE_CHAR);
	}

	public void setWpagTgaImpCommisIntNull(String wpagTgaImpCommisIntNull) {
		writeString(Pos.WPAG_TGA_IMP_COMMIS_INT_NULL, wpagTgaImpCommisIntNull, Len.WPAG_TGA_IMP_COMMIS_INT_NULL);
	}

	/**Original name: WPAG-TGA-IMP-COMMIS-INT-NULL<br>*/
	public String getWpagTgaImpCommisIntNull() {
		return readString(Pos.WPAG_TGA_IMP_COMMIS_INT_NULL, Len.WPAG_TGA_IMP_COMMIS_INT_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WPAG_TGA_IMP_COMMIS_INT = 1;
		public static final int WPAG_TGA_IMP_COMMIS_INT_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WPAG_TGA_IMP_COMMIS_INT = 8;
		public static final int WPAG_TGA_IMP_COMMIS_INT_NULL = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

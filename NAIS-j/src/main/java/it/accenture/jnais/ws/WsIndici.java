/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: WS-INDICI<br>
 * Variable: WS-INDICI from program IEAS9800<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsIndici {

	//==== PROPERTIES ====
	//Original name: WS-IND-PARAM
	private int indParam = DefaultValues.INT_VAL;
	//Original name: WS-INDICE
	private int indice = DefaultValues.INT_VAL;
	//Original name: WS-IND
	private int ind = DefaultValues.INT_VAL;
	//Original name: WS-IND-STRINGA
	private int indStringa = DefaultValues.INT_VAL;
	//Original name: WS-IND-POS
	private int indPos = DefaultValues.INT_VAL;
	//Original name: WS-IND-PARM
	private int indParm = DefaultValues.INT_VAL;
	//Original name: WS-POS-SUCC
	private int posSucc = DefaultValues.INT_VAL;
	//Original name: WS-POS-NEXT
	private int posNext = DefaultValues.INT_VAL;

	//==== METHODS ====
	public void setIndParam(int indParam) {
		this.indParam = indParam;
	}

	public int getIndParam() {
		return this.indParam;
	}

	public void setIndice(int indice) {
		this.indice = indice;
	}

	public int getIndice() {
		return this.indice;
	}

	public void setInd(int ind) {
		this.ind = ind;
	}

	public int getInd() {
		return this.ind;
	}

	public void setIndStringa(int indStringa) {
		this.indStringa = indStringa;
	}

	public int getIndStringa() {
		return this.indStringa;
	}

	public void setIndPos(int indPos) {
		this.indPos = indPos;
	}

	public int getIndPos() {
		return this.indPos;
	}

	public void setIndParm(int indParm) {
		this.indParm = indParm;
	}

	public void setIndParmFormatted(String indParm) {
		setIndParm(PicParser.display("9(9)").parseInt(indParm));
	}

	public int getIndParm() {
		return this.indParm;
	}

	public void setPosSucc(int posSucc) {
		this.posSucc = posSucc;
	}

	public int getPosSucc() {
		return this.posSucc;
	}

	public void setPosNext(int posNext) {
		this.posNext = posNext;
	}

	public int getPosNext() {
		return this.posNext;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int IND_STRINGA = 9;
		public static final int IND_POS = 9;
		public static final int POS_SUCC = 9;
		public static final int POS_NEXT = 9;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

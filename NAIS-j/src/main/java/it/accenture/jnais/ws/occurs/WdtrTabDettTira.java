/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.occurs;

import it.accenture.jnais.copy.Lccvdtr1;

/**Original name: WDTR-TAB-DETT-TIRA<br>
 * Variables: WDTR-TAB-DETT-TIRA from copybook LCCVDTRA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WdtrTabDettTira {

	//==== PROPERTIES ====
	//Original name: LCCVDTR1
	private Lccvdtr1 lccvdtr1 = new Lccvdtr1();

	//==== METHODS ====
	public Lccvdtr1 getLccvdtr1() {
		return lccvdtr1;
	}
}

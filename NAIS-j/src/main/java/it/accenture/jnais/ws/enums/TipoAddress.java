/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: TIPO-ADDRESS<br>
 * Variable: TIPO-ADDRESS from copybook IABC0010<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class TipoAddress {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char CACHE_VAL_VAR = 'A';
	private static final char[] CACHE_ACTUATOR = new char[] { 'B', 'E' };
	public static final char CACHE_ANAG = 'C';
	public static final char ANA_STR_DATO = 'D';
	public static final char VAR_AMBIENTE = 'P';
	public static final char AREA_MQ = 'Q';
	private static final char[] CACHE_VALIDO = new char[] { ' ', 'A', 'B', 'E', 'C', 'D', 'P', 'Q' };

	//==== METHODS ====
	public void setTipoAddress(char tipoAddress) {
		this.value = tipoAddress;
	}

	public char getTipoAddress() {
		return this.value;
	}

	public boolean isAddressCacheValVar() {
		return value == CACHE_VAL_VAR;
	}

	public void setCacheValVar() {
		value = CACHE_VAL_VAR;
	}

	public boolean isAddressCacheActuator() {
		return ArrayUtils.contains(CACHE_ACTUATOR, value);
	}

	public boolean isAddressVarAmbiente() {
		return value == VAR_AMBIENTE;
	}

	public boolean isAddressAreaMq() {
		return value == AREA_MQ;
	}

	public boolean isAddressCacheValido() {
		return ArrayUtils.contains(CACHE_VALIDO, value);
	}
}

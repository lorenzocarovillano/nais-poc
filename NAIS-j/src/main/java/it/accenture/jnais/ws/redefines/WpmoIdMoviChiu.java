/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-ID-MOVI-CHIU<br>
 * Variable: WPMO-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoIdMoviChiu extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WpmoIdMoviChiu() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WPMO_ID_MOVI_CHIU;
	}

	public void setWpmoIdMoviChiu(int wpmoIdMoviChiu) {
		writeIntAsPacked(Pos.WPMO_ID_MOVI_CHIU, wpmoIdMoviChiu, Len.Int.WPMO_ID_MOVI_CHIU);
	}

	public void setWpmoIdMoviChiuFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.WPMO_ID_MOVI_CHIU, Pos.WPMO_ID_MOVI_CHIU);
	}

	/**Original name: WPMO-ID-MOVI-CHIU<br>*/
	public int getWpmoIdMoviChiu() {
		return readPackedAsInt(Pos.WPMO_ID_MOVI_CHIU, Len.Int.WPMO_ID_MOVI_CHIU);
	}

	public byte[] getWpmoIdMoviChiuAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WPMO_ID_MOVI_CHIU, Pos.WPMO_ID_MOVI_CHIU);
		return buffer;
	}

	public void initWpmoIdMoviChiuSpaces() {
		fill(Pos.WPMO_ID_MOVI_CHIU, Len.WPMO_ID_MOVI_CHIU, Types.SPACE_CHAR);
	}

	public void setWpmoIdMoviChiuNull(String wpmoIdMoviChiuNull) {
		writeString(Pos.WPMO_ID_MOVI_CHIU_NULL, wpmoIdMoviChiuNull, Len.WPMO_ID_MOVI_CHIU_NULL);
	}

	/**Original name: WPMO-ID-MOVI-CHIU-NULL<br>*/
	public String getWpmoIdMoviChiuNull() {
		return readString(Pos.WPMO_ID_MOVI_CHIU_NULL, Len.WPMO_ID_MOVI_CHIU_NULL);
	}

	public String getWpmoIdMoviChiuNullFormatted() {
		return Functions.padBlanks(getWpmoIdMoviChiuNull(), Len.WPMO_ID_MOVI_CHIU_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WPMO_ID_MOVI_CHIU = 1;
		public static final int WPMO_ID_MOVI_CHIU_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WPMO_ID_MOVI_CHIU = 5;
		public static final int WPMO_ID_MOVI_CHIU_NULL = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WPMO_ID_MOVI_CHIU = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

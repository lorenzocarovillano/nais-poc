package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Session;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.AdesIvvs0216;
import it.accenture.jnais.copy.Iabv0007;
import it.accenture.jnais.copy.Iabv0008;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Ieav9904;
import it.accenture.jnais.copy.Ispv0000;
import it.accenture.jnais.copy.Lccvpmoz;
import it.accenture.jnais.copy.Loac0560;
import it.accenture.jnais.copy.Poli;
import it.accenture.jnais.ws.enums.WkAreaInizialize;
import it.accenture.jnais.ws.ptr.ActcacheAreaActuator;
import it.accenture.jnais.ws.ptr.AreaIdsv0102;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LOAM0170<br>
 * Generated as a class for rule WS.<br>*/
public class Loam0170Data {

    //==== PROPERTIES ====
    //Original name: WK-PGM
    private String wkPgm = "LOAM0170";
    //Original name: IDBSPOL0
    private String idbspol0 = "IDBSPOL0";
    //Original name: WK-PGM-SERV-ROTTURA
    private String wkPgmServRottura = "";
    //Original name: WK-PGM-SERV-SECON-BUS
    private String wkPgmServSeconBus = "LOAS0110";
    //Original name: WK-PGM-SERV-SECON-ROTT
    private String wkPgmServSeconRott = "";
    //Original name: WK-GESTIONE-SAVEPOINT
    private boolean wkGestioneSavepoint = false;
    //Original name: WK-OLD-ID-POL
    private int wkOldIdPol = DefaultValues.INT_VAL;
    //Original name: WK-OLD-ID-ADE
    private int wkOldIdAde = DefaultValues.INT_VAL;
    //Original name: WK-OLD-DT-RICOR
    private String wkOldDtRicor = DefaultValues.stringVal(Len.WK_OLD_DT_RICOR);
    //Original name: WK-NEW-ID-POL
    private int wkNewIdPol = DefaultValues.INT_VAL;
    //Original name: WK-NEW-ID-ADE
    private int wkNewIdAde = DefaultValues.INT_VAL;
    //Original name: WK-NEW-DT-RICOR
    private String wkNewDtRicor = DefaultValues.stringVal(Len.WK_NEW_DT_RICOR);
    //Original name: WK-VARIABILI
    private WkVariabiliLoam0170 wkVariabili = new WkVariabiliLoam0170();
    //Original name: LCCVPMOZ
    private Lccvpmoz lccvpmoz = new Lccvpmoz();
    //Original name: WCOM-INTERVALLO-ELAB
    private WcomIntervalloElab wcomIntervalloElab = new WcomIntervalloElab();
    /**Original name: AREA-IDSV0102<br>
	 * <pre>*****************************************************************
	 *  STRUTTURA PER GESTIONE CACHE COMP_STR_DATO / ANAG_DATO
	 * *****************************************************************</pre>*/
    private AreaIdsv0102 areaIdsv0102 = new AreaIdsv0102(new byte[400000]);
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: IABV0007
    private Iabv0007 iabv0007 = new Iabv0007();
    //Original name: IABV0008
    private Iabv0008 iabv0008 = new Iabv0008();
    //Original name: BTC-BATCH-STATE
    private BtcElabState btcBatchState = new BtcElabState();
    //Original name: BTC-BATCH-TYPE
    private BtcBatchType btcBatchType = new BtcBatchType();
    //Original name: BTC-EXE-MESSAGE
    private BtcExeMessage btcExeMessage = new BtcExeMessage();
    //Original name: BTC-ELAB-STATE
    private BtcElabState btcElabState = new BtcElabState();
    //Original name: BTC-JOB-EXECUTION
    private BtcJobExecution btcJobExecution = new BtcJobExecution();
    //Original name: BTC-JOB-SCHEDULE
    private BtcJobSchedule btcJobSchedule = new BtcJobSchedule();
    //Original name: BTC-REC-SCHEDULE
    private BtcRecScheduleLlbm0230 btcRecSchedule = new BtcRecScheduleLlbm0230();
    //Original name: BTC-BATCH
    private BtcBatch btcBatch = new BtcBatch();
    //Original name: LOG-ERRORE
    private LogErroreIabs0120 logErrore = new LogErroreIabs0120();
    //Original name: BTC-PARALLELISM
    private BtcParallelism btcParallelism = new BtcParallelism();
    //Original name: GRU-ARZ
    private GruArz gruArz = new GruArz();
    //Original name: RICH
    private RichIabs0900 rich = new RichIabs0900();
    //Original name: POLI
    private Poli poli = new Poli();
    //Original name: ADES
    private AdesIvvs0216 ades = new AdesIvvs0216();
    //Original name: IABV0006
    private Iabv0006 iabv0006 = new Iabv0006();
    //Original name: PARAM-MOVI
    private ParamMoviLdbs1470 paramMovi = new ParamMoviLdbs1470();
    //Original name: WCOM-IO-STATI
    private AreaPassaggio wcomIoStati = new AreaPassaggio();
    //Original name: BUFFER-WH-COD-RAMO
    private String bufferWhCodRamo = DefaultValues.stringVal(Len.BUFFER_WH_COD_RAMO);
    //Original name: BUFFER-WH-ANNO-ELAB
    private String bufferWhAnnoElab = DefaultValues.stringVal(Len.BUFFER_WH_ANNO_ELAB);
    //Original name: LOAC0560
    private Loac0560 loac0560 = new Loac0560();
    //Original name: ISPV0000
    private Ispv0000 ispv0000 = new Ispv0000();
    //Original name: WK-AREA-INIZIALIZE
    private WkAreaInizialize wkAreaInizialize = new WkAreaInizialize();
    //Original name: AREA-LOAS350
    private AreaLoas0350 areaLoas350 = new AreaLoas0350();
    //Original name: AREA-LOAS1050
    private AreaLoas1050 areaLoas1050 = new AreaLoas1050();
    //Original name: AREA-MAIN
    private AreaMainLoam0170 areaMain = new AreaMainLoam0170();
    //Original name: IEAV9904
    private Ieav9904 ieav9904 = new Ieav9904();
    /**Original name: ACTCACHE-AREA-ACTUATOR<br>
	 * <pre>*****************************************************************
	 *  STRUTTURA PER GESTIONE CACHE X ACTUATOR
	 * *****************************************************************
	 * *****************************************************************
	 *    area cache x ACTUATOR
	 * *****************************************************************</pre>*/
    private ActcacheAreaActuator actcacheAreaActuator = new ActcacheAreaActuator();

    //==== CONSTRUCTORS ====
    public Loam0170Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getWkPgmFormatted() {
        return Functions.padBlanks(getWkPgm(), Len.WK_PGM);
    }

    public String getIdbspol0() {
        return this.idbspol0;
    }

    public String getIdbspol0Formatted() {
        return Functions.padBlanks(getIdbspol0(), Len.IDBSPOL0);
    }

    public void setWkPgmServRottura(String wkPgmServRottura) {
        this.wkPgmServRottura = Functions.subString(wkPgmServRottura, Len.WK_PGM_SERV_ROTTURA);
    }

    public String getWkPgmServRottura() {
        return this.wkPgmServRottura;
    }

    public String getWkPgmServRotturaFormatted() {
        return Functions.padBlanks(getWkPgmServRottura(), Len.WK_PGM_SERV_ROTTURA);
    }

    public void setWkPgmServSeconBus(String wkPgmServSeconBus) {
        this.wkPgmServSeconBus = Functions.subString(wkPgmServSeconBus, Len.WK_PGM_SERV_SECON_BUS);
    }

    public String getWkPgmServSeconBus() {
        return this.wkPgmServSeconBus;
    }

    public String getWkPgmServSeconBusFormatted() {
        return Functions.padBlanks(getWkPgmServSeconBus(), Len.WK_PGM_SERV_SECON_BUS);
    }

    public void setWkPgmServSeconRott(String wkPgmServSeconRott) {
        this.wkPgmServSeconRott = Functions.subString(wkPgmServSeconRott, Len.WK_PGM_SERV_SECON_ROTT);
    }

    public String getWkPgmServSeconRott() {
        return this.wkPgmServSeconRott;
    }

    public String getWkPgmServSeconRottFormatted() {
        return Functions.padBlanks(getWkPgmServSeconRott(), Len.WK_PGM_SERV_SECON_ROTT);
    }

    public boolean isWkGestioneSavepoint() {
        return this.wkGestioneSavepoint;
    }

    public void setWkOldIdPol(int wkOldIdPol) {
        this.wkOldIdPol = wkOldIdPol;
    }

    public int getWkOldIdPol() {
        return this.wkOldIdPol;
    }

    public void setWkOldIdAde(int wkOldIdAde) {
        this.wkOldIdAde = wkOldIdAde;
    }

    public int getWkOldIdAde() {
        return this.wkOldIdAde;
    }

    public void setWkOldDtRicorFormatted(String wkOldDtRicor) {
        this.wkOldDtRicor = Trunc.toUnsignedNumeric(wkOldDtRicor, Len.WK_OLD_DT_RICOR);
    }

    public int getWkOldDtRicor() {
        return NumericDisplay.asInt(this.wkOldDtRicor);
    }

    public void setWkNewIdPol(int wkNewIdPol) {
        this.wkNewIdPol = wkNewIdPol;
    }

    public int getWkNewIdPol() {
        return this.wkNewIdPol;
    }

    public void setWkNewIdAde(int wkNewIdAde) {
        this.wkNewIdAde = wkNewIdAde;
    }

    public int getWkNewIdAde() {
        return this.wkNewIdAde;
    }

    public void setWkNewDtRicor(int wkNewDtRicor) {
        this.wkNewDtRicor = NumericDisplay.asString(wkNewDtRicor, Len.WK_NEW_DT_RICOR);
    }

    public int getWkNewDtRicor() {
        return NumericDisplay.asInt(this.wkNewDtRicor);
    }

    public String getWkNewDtRicorFormatted() {
        return this.wkNewDtRicor;
    }

    public void setBufferWhereConditionFormatted(String data) {
        byte[] buffer = new byte[Len.BUFFER_WHERE_CONDITION];
        MarshalByte.writeString(buffer, 1, data, Len.BUFFER_WHERE_CONDITION);
        setBufferWhereConditionBytes(buffer, 1);
    }

    public void setBufferWhereConditionBytes(byte[] buffer, int offset) {
        int position = offset;
        bufferWhCodRamo = MarshalByte.readString(buffer, position, Len.BUFFER_WH_COD_RAMO);
        position += Len.BUFFER_WH_COD_RAMO;
        bufferWhAnnoElab = MarshalByte.readString(buffer, position, Len.BUFFER_WH_ANNO_ELAB);
    }

    public void setBufferWhCodRamo(String bufferWhCodRamo) {
        this.bufferWhCodRamo = Functions.subString(bufferWhCodRamo, Len.BUFFER_WH_COD_RAMO);
    }

    public String getBufferWhCodRamo() {
        return this.bufferWhCodRamo;
    }

    public void setBufferWhAnnoElab(String bufferWhAnnoElab) {
        this.bufferWhAnnoElab = Functions.subString(bufferWhAnnoElab, Len.BUFFER_WH_ANNO_ELAB);
    }

    public String getBufferWhAnnoElab() {
        return this.bufferWhAnnoElab;
    }

    public String getBufferWhAnnoElabFormatted() {
        return Functions.padBlanks(getBufferWhAnnoElab(), Len.BUFFER_WH_ANNO_ELAB);
    }

    public ActcacheAreaActuator getActcacheAreaActuator() {
        return actcacheAreaActuator;
    }

    public AdesIvvs0216 getAdes() {
        return ades;
    }

    public AreaIdsv0102 getAreaIdsv0102() {
        return areaIdsv0102;
    }

    public AreaLoas1050 getAreaLoas1050() {
        return areaLoas1050;
    }

    public AreaLoas0350 getAreaLoas350() {
        return areaLoas350;
    }

    public AreaMainLoam0170 getAreaMain() {
        return areaMain;
    }

    public BtcBatch getBtcBatch() {
        return btcBatch;
    }

    public BtcElabState getBtcBatchState() {
        return btcBatchState;
    }

    public BtcBatchType getBtcBatchType() {
        return btcBatchType;
    }

    public BtcElabState getBtcElabState() {
        return btcElabState;
    }

    public BtcExeMessage getBtcExeMessage() {
        return btcExeMessage;
    }

    public BtcJobExecution getBtcJobExecution() {
        return btcJobExecution;
    }

    public BtcJobSchedule getBtcJobSchedule() {
        return btcJobSchedule;
    }

    public BtcParallelism getBtcParallelism() {
        return btcParallelism;
    }

    public BtcRecScheduleLlbm0230 getBtcRecSchedule() {
        return btcRecSchedule;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public GruArz getGruArz() {
        return gruArz;
    }

    public Iabv0006 getIabv0006() {
        return iabv0006;
    }

    public Iabv0007 getIabv0007() {
        return iabv0007;
    }

    public Iabv0008 getIabv0008() {
        return iabv0008;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Ieav9904 getIeav9904() {
        return ieav9904;
    }

    public Ispv0000 getIspv0000() {
        return ispv0000;
    }

    public Lccvpmoz getLccvpmoz() {
        return lccvpmoz;
    }

    public Loac0560 getLoac0560() {
        return loac0560;
    }

    public LogErroreIabs0120 getLogErrore() {
        return logErrore;
    }

    public ParamMoviLdbs1470 getParamMovi() {
        return paramMovi;
    }

    public Poli getPoli() {
        return poli;
    }

    public RichIabs0900 getRich() {
        return rich;
    }

    public WcomIntervalloElab getWcomIntervalloElab() {
        return wcomIntervalloElab;
    }

    public AreaPassaggio getWcomIoStati() {
        return wcomIoStati;
    }

    public WkAreaInizialize getWkAreaInizialize() {
        return wkAreaInizialize;
    }

    public WkVariabiliLoam0170 getWkVariabili() {
        return wkVariabili;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_OLD_DT_RICOR = 8;
        public static final int WK_NEW_DT_RICOR = 8;
        public static final int BUFFER_WH_COD_RAMO = 12;
        public static final int BUFFER_WH_ANNO_ELAB = 4;
        public static final int WK_PGM = 8;
        public static final int BUFFER_WHERE_CONDITION = BUFFER_WH_COD_RAMO + BUFFER_WH_ANNO_ELAB;
        public static final int IDBSPOL0 = 8;
        public static final int WK_PGM_SERV_ROTTURA = 8;
        public static final int WK_PGM_SERV_SECON_BUS = 8;
        public static final int WK_PGM_SERV_SECON_ROTT = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}

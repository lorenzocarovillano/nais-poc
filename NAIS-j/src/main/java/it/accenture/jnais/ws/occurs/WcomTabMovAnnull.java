/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: WCOM-TAB-MOV-ANNULL<br>
 * Variables: WCOM-TAB-MOV-ANNULL from copybook LCCC0001<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WcomTabMovAnnull {

	//==== PROPERTIES ====
	//Original name: WCOM-ID-MOVI-ANN
	private int wcomIdMoviAnn = DefaultValues.INT_VAL;

	//==== METHODS ====
	public void setTabMovAnnullBytes(byte[] buffer, int offset) {
		int position = offset;
		wcomIdMoviAnn = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WCOM_ID_MOVI_ANN, 0);
	}

	public byte[] getTabMovAnnullBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeIntAsPacked(buffer, position, wcomIdMoviAnn, Len.Int.WCOM_ID_MOVI_ANN, 0);
		return buffer;
	}

	public void initTabMovAnnullSpaces() {
		wcomIdMoviAnn = Types.INVALID_INT_VAL;
	}

	public void setWcomIdMoviAnn(int wcomIdMoviAnn) {
		this.wcomIdMoviAnn = wcomIdMoviAnn;
	}

	public int getWcomIdMoviAnn() {
		return this.wcomIdMoviAnn;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WCOM_ID_MOVI_ANN = 5;
		public static final int TAB_MOV_ANNULL = WCOM_ID_MOVI_ANN;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WCOM_ID_MOVI_ANN = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

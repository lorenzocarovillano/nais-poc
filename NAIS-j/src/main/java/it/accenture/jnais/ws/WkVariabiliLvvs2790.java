/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws;

import it.accenture.jnais.ws.enums.FlagFineLqu;

/**Original name: WK-VARIABILI<br>
 * Variable: WK-VARIABILI from program LVVS2790<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkVariabiliLvvs2790 {

	//==== PROPERTIES ====
	//Original name: FLAG-FINE-LQU
	private FlagFineLqu fineLqu = new FlagFineLqu();

	//==== METHODS ====
	public FlagFineLqu getFineLqu() {
		return fineLqu;
	}
}

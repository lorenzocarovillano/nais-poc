/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-BLC-TROVATO<br>
 * Variable: WK-BLC-TROVATO from program LOAS0310<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkBlcTrovato {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char SI = 'S';
	public static final char NO = 'N';

	//==== METHODS ====
	public void setWkBlcTrovato(char wkBlcTrovato) {
		this.value = wkBlcTrovato;
	}

	public char getWkBlcTrovato() {
		return this.value;
	}

	public boolean isSi() {
		return value == SI;
	}

	public void setSi() {
		value = SI;
	}

	public void setNo() {
		value = NO;
	}
}

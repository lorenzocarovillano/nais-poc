/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-CONTESTO-POG<br>
 * Variable: WK-CONTESTO-POG from program IVVS0216<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkContestoPog {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char SI_POG = 'S';
	public static final char NO_POG = 'N';

	//==== METHODS ====
	public void setWkContestoPog(char wkContestoPog) {
		this.value = wkContestoPog;
	}

	public char getWkContestoPog() {
		return this.value;
	}

	public boolean isSiPog() {
		return value == SI_POG;
	}

	public void setSiPog() {
		value = SI_POG;
	}

	public void setNoPog() {
		value = NO_POG;
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-COD-PRDT<br>
 * Variable: B03-COD-PRDT from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CodPrdt extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public B03CodPrdt() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.B03_COD_PRDT;
	}

	public void setB03CodPrdt(int b03CodPrdt) {
		writeIntAsPacked(Pos.B03_COD_PRDT, b03CodPrdt, Len.Int.B03_COD_PRDT);
	}

	public void setB03CodPrdtFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.B03_COD_PRDT, Pos.B03_COD_PRDT);
	}

	/**Original name: B03-COD-PRDT<br>*/
	public int getB03CodPrdt() {
		return readPackedAsInt(Pos.B03_COD_PRDT, Len.Int.B03_COD_PRDT);
	}

	public byte[] getB03CodPrdtAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.B03_COD_PRDT, Pos.B03_COD_PRDT);
		return buffer;
	}

	public void setB03CodPrdtNull(String b03CodPrdtNull) {
		writeString(Pos.B03_COD_PRDT_NULL, b03CodPrdtNull, Len.B03_COD_PRDT_NULL);
	}

	/**Original name: B03-COD-PRDT-NULL<br>*/
	public String getB03CodPrdtNull() {
		return readString(Pos.B03_COD_PRDT_NULL, Len.B03_COD_PRDT_NULL);
	}

	public String getB03CodPrdtNullFormatted() {
		return Functions.padBlanks(getB03CodPrdtNull(), Len.B03_COD_PRDT_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int B03_COD_PRDT = 1;
		public static final int B03_COD_PRDT_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int B03_COD_PRDT = 3;
		public static final int B03_COD_PRDT_NULL = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int B03_COD_PRDT = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

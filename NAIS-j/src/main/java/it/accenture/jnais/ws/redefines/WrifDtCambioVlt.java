/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WRIF-DT-CAMBIO-VLT<br>
 * Variable: WRIF-DT-CAMBIO-VLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrifDtCambioVlt extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WrifDtCambioVlt() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WRIF_DT_CAMBIO_VLT;
	}

	public void setWrifDtCambioVlt(int wrifDtCambioVlt) {
		writeIntAsPacked(Pos.WRIF_DT_CAMBIO_VLT, wrifDtCambioVlt, Len.Int.WRIF_DT_CAMBIO_VLT);
	}

	public void setWrifDtCambioVltFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.WRIF_DT_CAMBIO_VLT, Pos.WRIF_DT_CAMBIO_VLT);
	}

	/**Original name: WRIF-DT-CAMBIO-VLT<br>*/
	public int getWrifDtCambioVlt() {
		return readPackedAsInt(Pos.WRIF_DT_CAMBIO_VLT, Len.Int.WRIF_DT_CAMBIO_VLT);
	}

	public byte[] getWrifDtCambioVltAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WRIF_DT_CAMBIO_VLT, Pos.WRIF_DT_CAMBIO_VLT);
		return buffer;
	}

	public void initWrifDtCambioVltSpaces() {
		fill(Pos.WRIF_DT_CAMBIO_VLT, Len.WRIF_DT_CAMBIO_VLT, Types.SPACE_CHAR);
	}

	public void setWrifDtCambioVltNull(String wrifDtCambioVltNull) {
		writeString(Pos.WRIF_DT_CAMBIO_VLT_NULL, wrifDtCambioVltNull, Len.WRIF_DT_CAMBIO_VLT_NULL);
	}

	/**Original name: WRIF-DT-CAMBIO-VLT-NULL<br>*/
	public String getWrifDtCambioVltNull() {
		return readString(Pos.WRIF_DT_CAMBIO_VLT_NULL, Len.WRIF_DT_CAMBIO_VLT_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WRIF_DT_CAMBIO_VLT = 1;
		public static final int WRIF_DT_CAMBIO_VLT_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WRIF_DT_CAMBIO_VLT = 5;
		public static final int WRIF_DT_CAMBIO_VLT_NULL = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WRIF_DT_CAMBIO_VLT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WE12-ID-MOVI-CHIU<br>
 * Variable: WE12-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class We12IdMoviChiu extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public We12IdMoviChiu() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WE12_ID_MOVI_CHIU;
	}

	public void setWe12IdMoviChiu(int we12IdMoviChiu) {
		writeIntAsPacked(Pos.WE12_ID_MOVI_CHIU, we12IdMoviChiu, Len.Int.WE12_ID_MOVI_CHIU);
	}

	/**Original name: WE12-ID-MOVI-CHIU<br>*/
	public int getWe12IdMoviChiu() {
		return readPackedAsInt(Pos.WE12_ID_MOVI_CHIU, Len.Int.WE12_ID_MOVI_CHIU);
	}

	public byte[] getWe12IdMoviChiuAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WE12_ID_MOVI_CHIU, Pos.WE12_ID_MOVI_CHIU);
		return buffer;
	}

	public void setWe12IdMoviChiuNull(String we12IdMoviChiuNull) {
		writeString(Pos.WE12_ID_MOVI_CHIU_NULL, we12IdMoviChiuNull, Len.WE12_ID_MOVI_CHIU_NULL);
	}

	/**Original name: WE12-ID-MOVI-CHIU-NULL<br>*/
	public String getWe12IdMoviChiuNull() {
		return readString(Pos.WE12_ID_MOVI_CHIU_NULL, Len.WE12_ID_MOVI_CHIU_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WE12_ID_MOVI_CHIU = 1;
		public static final int WE12_ID_MOVI_CHIU_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WE12_ID_MOVI_CHIU = 5;
		public static final int WE12_ID_MOVI_CHIU_NULL = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WE12_ID_MOVI_CHIU = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

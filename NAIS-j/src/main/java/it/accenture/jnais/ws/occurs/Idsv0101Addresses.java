/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: IDSV0101-ADDRESSES<br>
 * Variables: IDSV0101-ADDRESSES from copybook IDSV0101<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Idsv0101Addresses {

	//==== PROPERTIES ====
	//Original name: IDSV0101-ADDRESS-TYPE
	private char type = DefaultValues.CHAR_VAL;
	//Original name: IDSV0101-ADDRESS
	private int s = DefaultValues.BIN_INT_VAL;

	//==== METHODS ====
	public void setAddressesBytes(byte[] buffer, int offset) {
		int position = offset;
		type = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		s = MarshalByte.readBinaryInt(buffer, position);
	}

	public byte[] getAddressesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, type);
		position += Types.CHAR_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, s);
		return buffer;
	}

	public void initAddressesSpaces() {
		type = Types.SPACE_CHAR;
		s = Types.INVALID_BINARY_INT_VAL;
	}

	public void setType(char type) {
		this.type = type;
	}

	public char getType() {
		return this.type;
	}

	public void setS(int s) {
		this.s = s;
	}

	public int getS() {
		return this.s;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TYPE = 1;
		public static final int S = 4;
		public static final int ADDRESSES = TYPE + S;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Session;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Iabcsq99;
import it.accenture.jnais.copy.Iabv0007;
import it.accenture.jnais.copy.Iabv0008;
import it.accenture.jnais.copy.Iabvsqg1;
import it.accenture.jnais.copy.Ieav9904;
import it.accenture.jnais.copy.Lrgc0041;
import it.accenture.jnais.copy.WrinRecOutput;
import it.accenture.jnais.ws.enums.FlagLancio;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LRGM0380<br>
 * Generated as a class for rule WS.<br>*/
public class Lrgm0380Data {

    //==== PROPERTIES ====
    //Original name: IABVSQG1
    private Iabvsqg1 iabvsqg1 = new Iabvsqg1();
    //Original name: WK-PGM
    private String wkPgm = "LRGM0380";
    //Original name: IX-IND
    private short ixInd = DefaultValues.SHORT_VAL;
    //Original name: IABCSQ99
    private Iabcsq99 iabcsq99 = new Iabcsq99();
    /**Original name: WK-PGM-SERV-ROTTURA<br>
	 * <pre>-- Inizio PGM-USED
	 * --
	 * --    valorizzare i campi PGM
	 * --    per un 'eventuale servizio in caso di rottura chiave
	 * --</pre>*/
    private String wkPgmServRottura = "";
    //Original name: WK-PGM-SERV-SECON-BUS
    private String wkPgmServSeconBus = "";
    //Original name: WK-PGM-SERV-SECON-ROTT
    private String wkPgmServSeconRott = "";
    //Original name: BUFFER-WHERE-CONDITION
    private BufferWhereCondition bufferWhereCondition = new BufferWhereCondition();
    //Original name: FLAG-LANCIO
    private FlagLancio flagLancio = new FlagLancio();
    //Original name: WK-APPO-DATA-JOB
    private WkAppoDataJob wkAppoDataJob = new WkAppoDataJob();
    //Original name: WCOM-IO-STATI
    private Lccc0001 lccc0001 = new Lccc0001();
    //Original name: IABV0007
    private Iabv0007 iabv0007 = new Iabv0007();
    //Original name: IABV0008
    private Iabv0008 iabv0008 = new Iabv0008();
    //Original name: BTC-BATCH-STATE
    private BtcElabState btcBatchState = new BtcElabState();
    //Original name: BTC-BATCH-TYPE
    private BtcBatchType btcBatchType = new BtcBatchType();
    //Original name: BTC-EXE-MESSAGE
    private BtcExeMessage btcExeMessage = new BtcExeMessage();
    //Original name: BTC-ELAB-STATE
    private BtcElabState btcElabState = new BtcElabState();
    //Original name: BTC-JOB-EXECUTION
    private BtcJobExecution btcJobExecution = new BtcJobExecution();
    //Original name: BTC-JOB-SCHEDULE
    private BtcJobSchedule btcJobSchedule = new BtcJobSchedule();
    //Original name: BTC-REC-SCHEDULE
    private BtcRecScheduleLlbm0230 btcRecSchedule = new BtcRecScheduleLlbm0230();
    //Original name: BTC-BATCH
    private BtcBatch btcBatch = new BtcBatch();
    //Original name: LOG-ERRORE
    private LogErroreIabs0120 logErrore = new LogErroreIabs0120();
    //Original name: GRU-ARZ
    private GruArz gruArz = new GruArz();
    //Original name: BTC-PARALLELISM
    private BtcParallelism btcParallelism = new BtcParallelism();
    //Original name: RICH
    private RichIabs0900 rich = new RichIabs0900();
    //Original name: IABV0006
    private Iabv0006 iabv0006 = new Iabv0006();
    //Original name: WRIN-REC-OUTPUT
    private WrinRecOutput wrinRecOutput = new WrinRecOutput();
    //Original name: LRGC0041
    private Lrgc0041 lrgc0041 = new Lrgc0041();
    //Original name: WK-AREA-CONTATORI
    private WkAreaContatori wkAreaContatori = new WkAreaContatori();
    //Original name: IEAV9904
    private Ieav9904 ieav9904 = new Ieav9904();

    //==== CONSTRUCTORS ====
    public Lrgm0380Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getWkPgmFormatted() {
        return Functions.padBlanks(getWkPgm(), Len.WK_PGM);
    }

    public void setIxInd(short ixInd) {
        this.ixInd = ixInd;
    }

    public short getIxInd() {
        return this.ixInd;
    }

    public void setWkPgmServRottura(String wkPgmServRottura) {
        this.wkPgmServRottura = Functions.subString(wkPgmServRottura, Len.WK_PGM_SERV_ROTTURA);
    }

    public String getWkPgmServRottura() {
        return this.wkPgmServRottura;
    }

    public String getWkPgmServRotturaFormatted() {
        return Functions.padBlanks(getWkPgmServRottura(), Len.WK_PGM_SERV_ROTTURA);
    }

    public void setWkPgmServSeconBus(String wkPgmServSeconBus) {
        this.wkPgmServSeconBus = Functions.subString(wkPgmServSeconBus, Len.WK_PGM_SERV_SECON_BUS);
    }

    public String getWkPgmServSeconBus() {
        return this.wkPgmServSeconBus;
    }

    public String getWkPgmServSeconBusFormatted() {
        return Functions.padBlanks(getWkPgmServSeconBus(), Len.WK_PGM_SERV_SECON_BUS);
    }

    public void setWkPgmServSeconRott(String wkPgmServSeconRott) {
        this.wkPgmServSeconRott = Functions.subString(wkPgmServSeconRott, Len.WK_PGM_SERV_SECON_ROTT);
    }

    public String getWkPgmServSeconRott() {
        return this.wkPgmServSeconRott;
    }

    public String getWkPgmServSeconRottFormatted() {
        return Functions.padBlanks(getWkPgmServSeconRott(), Len.WK_PGM_SERV_SECON_ROTT);
    }

    public void setWrinRecFileinFormatted(String data) {
        byte[] buffer = new byte[Len.WRIN_REC_FILEIN];
        MarshalByte.writeString(buffer, 1, data, Len.WRIN_REC_FILEIN);
        setWrinRecFileinBytes(buffer, 1);
    }

    public String getWrinRecFileinFormatted() {
        return wrinRecOutput.getWrinRecOutputFormatted();
    }

    public void setWrinRecFileinBytes(byte[] buffer, int offset) {
        int position = offset;
        wrinRecOutput.setWrinRecOutputBytes(buffer, position);
    }

    public BtcBatch getBtcBatch() {
        return btcBatch;
    }

    public BtcElabState getBtcBatchState() {
        return btcBatchState;
    }

    public BtcBatchType getBtcBatchType() {
        return btcBatchType;
    }

    public BtcElabState getBtcElabState() {
        return btcElabState;
    }

    public BtcExeMessage getBtcExeMessage() {
        return btcExeMessage;
    }

    public BtcJobExecution getBtcJobExecution() {
        return btcJobExecution;
    }

    public BtcJobSchedule getBtcJobSchedule() {
        return btcJobSchedule;
    }

    public BtcParallelism getBtcParallelism() {
        return btcParallelism;
    }

    public BtcRecScheduleLlbm0230 getBtcRecSchedule() {
        return btcRecSchedule;
    }

    public BufferWhereCondition getBufferWhereCondition() {
        return bufferWhereCondition;
    }

    public FlagLancio getFlagLancio() {
        return flagLancio;
    }

    public GruArz getGruArz() {
        return gruArz;
    }

    public Iabcsq99 getIabcsq99() {
        return iabcsq99;
    }

    public Iabv0006 getIabv0006() {
        return iabv0006;
    }

    public Iabv0007 getIabv0007() {
        return iabv0007;
    }

    public Iabv0008 getIabv0008() {
        return iabv0008;
    }

    public Iabvsqg1 getIabvsqg1() {
        return iabvsqg1;
    }

    public Ieav9904 getIeav9904() {
        return ieav9904;
    }

    public Lccc0001 getLccc0001() {
        return lccc0001;
    }

    public LogErroreIabs0120 getLogErrore() {
        return logErrore;
    }

    public Lrgc0041 getLrgc0041() {
        return lrgc0041;
    }

    public RichIabs0900 getRich() {
        return rich;
    }

    public WkAppoDataJob getWkAppoDataJob() {
        return wkAppoDataJob;
    }

    public WkAreaContatori getWkAreaContatori() {
        return wkAreaContatori;
    }

    public WrinRecOutput getWrinRecOutput() {
        return wrinRecOutput;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_PGM = 8;
        public static final int WRIN_REC_FILEIN = WrinRecOutput.Len.WRIN_REC_OUTPUT;
        public static final int WK_PGM_SERV_ROTTURA = 8;
        public static final int WK_PGM_SERV_SECON_BUS = 8;
        public static final int WK_PGM_SERV_SECON_ROTT = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}

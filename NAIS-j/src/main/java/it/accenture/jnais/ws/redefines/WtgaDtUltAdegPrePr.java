/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WTGA-DT-ULT-ADEG-PRE-PR<br>
 * Variable: WTGA-DT-ULT-ADEG-PRE-PR from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaDtUltAdegPrePr extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WtgaDtUltAdegPrePr() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WTGA_DT_ULT_ADEG_PRE_PR;
	}

	public void setWtgaDtUltAdegPrePr(int wtgaDtUltAdegPrePr) {
		writeIntAsPacked(Pos.WTGA_DT_ULT_ADEG_PRE_PR, wtgaDtUltAdegPrePr, Len.Int.WTGA_DT_ULT_ADEG_PRE_PR);
	}

	public void setWtgaDtUltAdegPrePrFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.WTGA_DT_ULT_ADEG_PRE_PR, Pos.WTGA_DT_ULT_ADEG_PRE_PR);
	}

	/**Original name: WTGA-DT-ULT-ADEG-PRE-PR<br>*/
	public int getWtgaDtUltAdegPrePr() {
		return readPackedAsInt(Pos.WTGA_DT_ULT_ADEG_PRE_PR, Len.Int.WTGA_DT_ULT_ADEG_PRE_PR);
	}

	public String getDtgaDtUltAdegPrePrFormatted() {
		return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).format(getWtgaDtUltAdegPrePr()).toString();
	}

	public byte[] getWtgaDtUltAdegPrePrAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WTGA_DT_ULT_ADEG_PRE_PR, Pos.WTGA_DT_ULT_ADEG_PRE_PR);
		return buffer;
	}

	public void initWtgaDtUltAdegPrePrSpaces() {
		fill(Pos.WTGA_DT_ULT_ADEG_PRE_PR, Len.WTGA_DT_ULT_ADEG_PRE_PR, Types.SPACE_CHAR);
	}

	public void setWtgaDtUltAdegPrePrNull(String wtgaDtUltAdegPrePrNull) {
		writeString(Pos.WTGA_DT_ULT_ADEG_PRE_PR_NULL, wtgaDtUltAdegPrePrNull, Len.WTGA_DT_ULT_ADEG_PRE_PR_NULL);
	}

	/**Original name: WTGA-DT-ULT-ADEG-PRE-PR-NULL<br>*/
	public String getWtgaDtUltAdegPrePrNull() {
		return readString(Pos.WTGA_DT_ULT_ADEG_PRE_PR_NULL, Len.WTGA_DT_ULT_ADEG_PRE_PR_NULL);
	}

	public String getWtgaDtUltAdegPrePrNullFormatted() {
		return Functions.padBlanks(getWtgaDtUltAdegPrePrNull(), Len.WTGA_DT_ULT_ADEG_PRE_PR_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WTGA_DT_ULT_ADEG_PRE_PR = 1;
		public static final int WTGA_DT_ULT_ADEG_PRE_PR_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WTGA_DT_ULT_ADEG_PRE_PR = 5;
		public static final int WTGA_DT_ULT_ADEG_PRE_PR_NULL = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WTGA_DT_ULT_ADEG_PRE_PR = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

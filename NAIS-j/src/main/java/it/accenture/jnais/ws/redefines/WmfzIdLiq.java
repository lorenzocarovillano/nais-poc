/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WMFZ-ID-LIQ<br>
 * Variable: WMFZ-ID-LIQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WmfzIdLiq extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WmfzIdLiq() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WMFZ_ID_LIQ;
	}

	public void setWmfzIdLiq(int wmfzIdLiq) {
		writeIntAsPacked(Pos.WMFZ_ID_LIQ, wmfzIdLiq, Len.Int.WMFZ_ID_LIQ);
	}

	public void setWmfzIdLiqFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.WMFZ_ID_LIQ, Pos.WMFZ_ID_LIQ);
	}

	/**Original name: WMFZ-ID-LIQ<br>*/
	public int getWmfzIdLiq() {
		return readPackedAsInt(Pos.WMFZ_ID_LIQ, Len.Int.WMFZ_ID_LIQ);
	}

	public byte[] getWmfzIdLiqAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WMFZ_ID_LIQ, Pos.WMFZ_ID_LIQ);
		return buffer;
	}

	public void initWmfzIdLiqSpaces() {
		fill(Pos.WMFZ_ID_LIQ, Len.WMFZ_ID_LIQ, Types.SPACE_CHAR);
	}

	public void setWmfzIdLiqNull(String wmfzIdLiqNull) {
		writeString(Pos.WMFZ_ID_LIQ_NULL, wmfzIdLiqNull, Len.WMFZ_ID_LIQ_NULL);
	}

	/**Original name: WMFZ-ID-LIQ-NULL<br>*/
	public String getWmfzIdLiqNull() {
		return readString(Pos.WMFZ_ID_LIQ_NULL, Len.WMFZ_ID_LIQ_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WMFZ_ID_LIQ = 1;
		public static final int WMFZ_ID_LIQ_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WMFZ_ID_LIQ = 5;
		public static final int WMFZ_ID_LIQ_NULL = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WMFZ_ID_LIQ = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

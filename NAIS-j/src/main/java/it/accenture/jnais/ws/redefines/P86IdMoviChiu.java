/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: P86-ID-MOVI-CHIU<br>
 * Variable: P86-ID-MOVI-CHIU from program IDBSP860<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P86IdMoviChiu extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public P86IdMoviChiu() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.P86_ID_MOVI_CHIU;
	}

	public void setP86IdMoviChiu(int p86IdMoviChiu) {
		writeIntAsPacked(Pos.P86_ID_MOVI_CHIU, p86IdMoviChiu, Len.Int.P86_ID_MOVI_CHIU);
	}

	public void setP86IdMoviChiuFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.P86_ID_MOVI_CHIU, Pos.P86_ID_MOVI_CHIU);
	}

	/**Original name: P86-ID-MOVI-CHIU<br>*/
	public int getP86IdMoviChiu() {
		return readPackedAsInt(Pos.P86_ID_MOVI_CHIU, Len.Int.P86_ID_MOVI_CHIU);
	}

	public byte[] getP86IdMoviChiuAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.P86_ID_MOVI_CHIU, Pos.P86_ID_MOVI_CHIU);
		return buffer;
	}

	public void setP86IdMoviChiuNull(String p86IdMoviChiuNull) {
		writeString(Pos.P86_ID_MOVI_CHIU_NULL, p86IdMoviChiuNull, Len.P86_ID_MOVI_CHIU_NULL);
	}

	/**Original name: P86-ID-MOVI-CHIU-NULL<br>*/
	public String getP86IdMoviChiuNull() {
		return readString(Pos.P86_ID_MOVI_CHIU_NULL, Len.P86_ID_MOVI_CHIU_NULL);
	}

	public String getP86IdMoviChiuNullFormatted() {
		return Functions.padBlanks(getP86IdMoviChiuNull(), Len.P86_ID_MOVI_CHIU_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int P86_ID_MOVI_CHIU = 1;
		public static final int P86_ID_MOVI_CHIU_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int P86_ID_MOVI_CHIU = 5;
		public static final int P86_ID_MOVI_CHIU_NULL = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int P86_ID_MOVI_CHIU = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

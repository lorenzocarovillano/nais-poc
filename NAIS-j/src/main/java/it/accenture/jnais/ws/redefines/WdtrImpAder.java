/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-IMP-ADER<br>
 * Variable: WDTR-IMP-ADER from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrImpAder extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WdtrImpAder() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WDTR_IMP_ADER;
	}

	public void setWdtrImpAder(AfDecimal wdtrImpAder) {
		writeDecimalAsPacked(Pos.WDTR_IMP_ADER, wdtrImpAder.copy());
	}

	/**Original name: WDTR-IMP-ADER<br>*/
	public AfDecimal getWdtrImpAder() {
		return readPackedAsDecimal(Pos.WDTR_IMP_ADER, Len.Int.WDTR_IMP_ADER, Len.Fract.WDTR_IMP_ADER);
	}

	public void setWdtrImpAderNull(String wdtrImpAderNull) {
		writeString(Pos.WDTR_IMP_ADER_NULL, wdtrImpAderNull, Len.WDTR_IMP_ADER_NULL);
	}

	/**Original name: WDTR-IMP-ADER-NULL<br>*/
	public String getWdtrImpAderNull() {
		return readString(Pos.WDTR_IMP_ADER_NULL, Len.WDTR_IMP_ADER_NULL);
	}

	public String getWdtrImpAderNullFormatted() {
		return Functions.padBlanks(getWdtrImpAderNull(), Len.WDTR_IMP_ADER_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WDTR_IMP_ADER = 1;
		public static final int WDTR_IMP_ADER_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WDTR_IMP_ADER = 8;
		public static final int WDTR_IMP_ADER_NULL = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Fract {

			//==== PROPERTIES ====
			public static final int WDTR_IMP_ADER = 3;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}

		public static class Int {

			//==== PROPERTIES ====
			public static final int WDTR_IMP_ADER = 12;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WCOM-TP-OGG-BLOCCO<br>
 * Variable: WCOM-TP-OGG-BLOCCO from copybook LCCC0261<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WcomTpOggBlocco {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.TP_OGG_BLOCCO);
	public static final String POLIZZA_L11 = "PO";
	public static final String ADESIONE_L11 = "AD";

	//==== METHODS ====
	public void setTpOggBlocco(String tpOggBlocco) {
		this.value = Functions.subString(tpOggBlocco, Len.TP_OGG_BLOCCO);
	}

	public String getTpOggBlocco() {
		return this.value;
	}

	public boolean isPolizzaL11() {
		return value.equals(POLIZZA_L11);
	}

	public void setWcomPolizzaL11() {
		value = POLIZZA_L11;
	}

	public void setWcomAdesioneL11() {
		value = ADESIONE_L11;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TP_OGG_BLOCCO = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

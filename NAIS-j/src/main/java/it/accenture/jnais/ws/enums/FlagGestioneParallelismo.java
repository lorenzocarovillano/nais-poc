/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-GESTIONE-PARALLELISMO<br>
 * Variable: FLAG-GESTIONE-PARALLELISMO from copybook IABV0007<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagGestioneParallelismo {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char YES = 'Y';
	public static final char NO = 'N';

	//==== METHODS ====
	public void setFlagGestioneParallelismo(char flagGestioneParallelismo) {
		this.value = flagGestioneParallelismo;
	}

	public char getFlagGestioneParallelismo() {
		return this.value;
	}

	public boolean isYes() {
		return value == YES;
	}

	public void setYes() {
		value = YES;
	}

	public void setNo() {
		value = NO;
	}
}

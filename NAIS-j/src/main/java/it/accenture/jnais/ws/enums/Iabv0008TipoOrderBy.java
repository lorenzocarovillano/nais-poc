/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

/**Original name: IABV0008-TIPO-ORDER-BY<br>
 * Variable: IABV0008-TIPO-ORDER-BY from copybook IABV0008<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabv0008TipoOrderBy {

	//==== PROPERTIES ====
	private String value = "JOB";
	public static final String IB_OGG = "IBO";
	public static final String ID_JOB = "JOB";

	//==== METHODS ====
	public String getTipoOrderBy() {
		return this.value;
	}
}

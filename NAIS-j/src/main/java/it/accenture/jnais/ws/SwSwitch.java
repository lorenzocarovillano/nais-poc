/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws;

import it.accenture.jnais.ws.enums.SwErroreTrovato;

/**Original name: SW-SWITCH<br>
 * Variable: SW-SWITCH from program IEAS9900<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SwSwitch {

	//==== PROPERTIES ====
	/**Original name: SW-CORRETTO<br>
	 * <pre>--SWITCH PER ITER ELABORAZIONE</pre>*/
	private boolean swCorretto = false;
	//Original name: SW-TROVATO
	private boolean swTrovato = false;
	//Original name: SW-ERRORE-TROVATO
	private SwErroreTrovato swErroreTrovato = new SwErroreTrovato();

	//==== METHODS ====
	public void setSwCorretto(boolean swCorretto) {
		this.swCorretto = swCorretto;
	}

	public boolean isSwCorretto() {
		return this.swCorretto;
	}

	public void setSwTrovato(boolean swTrovato) {
		this.swTrovato = swTrovato;
	}

	public boolean isSwTrovato() {
		return this.swTrovato;
	}

	public SwErroreTrovato getSwErroreTrovato() {
		return swErroreTrovato;
	}
}

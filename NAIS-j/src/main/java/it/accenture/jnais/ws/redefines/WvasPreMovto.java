/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WVAS-PRE-MOVTO<br>
 * Variable: WVAS-PRE-MOVTO from program LVVS0135<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WvasPreMovto extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WvasPreMovto() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WVAS_PRE_MOVTO;
	}

	public void setWvasPreMovto(AfDecimal wvasPreMovto) {
		writeDecimalAsPacked(Pos.WVAS_PRE_MOVTO, wvasPreMovto.copy());
	}

	/**Original name: WVAS-PRE-MOVTO<br>*/
	public AfDecimal getWvasPreMovto() {
		return readPackedAsDecimal(Pos.WVAS_PRE_MOVTO, Len.Int.WVAS_PRE_MOVTO, Len.Fract.WVAS_PRE_MOVTO);
	}

	public void setWvasPreMovtoNull(String wvasPreMovtoNull) {
		writeString(Pos.WVAS_PRE_MOVTO_NULL, wvasPreMovtoNull, Len.WVAS_PRE_MOVTO_NULL);
	}

	/**Original name: WVAS-PRE-MOVTO-NULL<br>*/
	public String getWvasPreMovtoNull() {
		return readString(Pos.WVAS_PRE_MOVTO_NULL, Len.WVAS_PRE_MOVTO_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WVAS_PRE_MOVTO = 1;
		public static final int WVAS_PRE_MOVTO_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WVAS_PRE_MOVTO = 8;
		public static final int WVAS_PRE_MOVTO_NULL = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Fract {

			//==== PROPERTIES ====
			public static final int WVAS_PRE_MOVTO = 3;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}

		public static class Int {

			//==== PROPERTIES ====
			public static final int WVAS_PRE_MOVTO = 12;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

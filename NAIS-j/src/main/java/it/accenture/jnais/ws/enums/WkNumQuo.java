/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-NUM-QUO<br>
 * Variable: WK-NUM-QUO from program LVVS0116<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkNumQuo {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.WK_NUM_QUO);
	public static final String OK = "OK";
	public static final String KO = "KO";

	//==== METHODS ====
	public void setWkNumQuo(String wkNumQuo) {
		this.value = Functions.subString(wkNumQuo, Len.WK_NUM_QUO);
	}

	public String getWkNumQuo() {
		return this.value;
	}

	public void setOk() {
		value = OK;
	}

	public boolean isKo() {
		return value.equals(KO);
	}

	public void setKo() {
		value = KO;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WK_NUM_QUO = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WBEL-DT-ULT-DOCTO<br>
 * Variable: WBEL-DT-ULT-DOCTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelDtUltDocto extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WbelDtUltDocto() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WBEL_DT_ULT_DOCTO;
	}

	public void setWbelDtUltDocto(int wbelDtUltDocto) {
		writeIntAsPacked(Pos.WBEL_DT_ULT_DOCTO, wbelDtUltDocto, Len.Int.WBEL_DT_ULT_DOCTO);
	}

	public void setWbelDtUltDoctoFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.WBEL_DT_ULT_DOCTO, Pos.WBEL_DT_ULT_DOCTO);
	}

	/**Original name: WBEL-DT-ULT-DOCTO<br>*/
	public int getWbelDtUltDocto() {
		return readPackedAsInt(Pos.WBEL_DT_ULT_DOCTO, Len.Int.WBEL_DT_ULT_DOCTO);
	}

	public byte[] getWbelDtUltDoctoAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WBEL_DT_ULT_DOCTO, Pos.WBEL_DT_ULT_DOCTO);
		return buffer;
	}

	public void initWbelDtUltDoctoSpaces() {
		fill(Pos.WBEL_DT_ULT_DOCTO, Len.WBEL_DT_ULT_DOCTO, Types.SPACE_CHAR);
	}

	public void setWbelDtUltDoctoNull(String wbelDtUltDoctoNull) {
		writeString(Pos.WBEL_DT_ULT_DOCTO_NULL, wbelDtUltDoctoNull, Len.WBEL_DT_ULT_DOCTO_NULL);
	}

	/**Original name: WBEL-DT-ULT-DOCTO-NULL<br>*/
	public String getWbelDtUltDoctoNull() {
		return readString(Pos.WBEL_DT_ULT_DOCTO_NULL, Len.WBEL_DT_ULT_DOCTO_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WBEL_DT_ULT_DOCTO = 1;
		public static final int WBEL_DT_ULT_DOCTO_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WBEL_DT_ULT_DOCTO = 5;
		public static final int WBEL_DT_ULT_DOCTO_NULL = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WBEL_DT_ULT_DOCTO = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

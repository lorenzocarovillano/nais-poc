/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.occurs;

/**Original name: AREA-CALL-I-O-TOT<br>
 * Variables: AREA-CALL-I-O-TOT from copybook IDSV0009<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class AreaCallIOTot {

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 2000;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

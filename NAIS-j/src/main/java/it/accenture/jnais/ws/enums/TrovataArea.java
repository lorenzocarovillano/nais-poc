/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: TROVATA-AREA<br>
 * Variable: TROVATA-AREA from program LVVS0039<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class TrovataArea {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char SI = 'S';
	public static final char NO = 'N';

	//==== METHODS ====
	public void setTrovataArea(char trovataArea) {
		this.value = trovataArea;
	}

	public char getTrovataArea() {
		return this.value;
	}

	public void setNo() {
		value = NO;
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WCOM-FL-RATE-ACCORPATE<br>
 * Variable: WCOM-FL-RATE-ACCORPATE from copybook LCCC0320<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WcomFlRateAccorpate {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.FL_RATE_ACCORPATE);
	public static final String SI = "SI";
	public static final String NO = "NO";

	//==== METHODS ====
	public void setFlRateAccorpate(String flRateAccorpate) {
		this.value = Functions.subString(flRateAccorpate, Len.FL_RATE_ACCORPATE);
	}

	public String getFlRateAccorpate() {
		return this.value;
	}

	public boolean isSi() {
		return value.equals(SI);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FL_RATE_ACCORPATE = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

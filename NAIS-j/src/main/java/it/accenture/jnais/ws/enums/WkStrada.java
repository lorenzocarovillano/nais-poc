/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

/**Original name: WK-STRADA<br>
 * Variable: WK-STRADA from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkStrada {

	//==== PROPERTIES ====
	private char value = 'T';
	public static final char SEQ = 'S';
	public static final char TAB = 'T';

	//==== METHODS ====
	public char getWkStrada() {
		return this.value;
	}

	public boolean isTab() {
		return value == TAB;
	}
}

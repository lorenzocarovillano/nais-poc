/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WK-APPO-DT<br>
 * Variable: WK-APPO-DT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkAppoDtLves0269 extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WkAppoDtLves0269() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WK_APPO_DT;
	}

	public void setWkAppoDt(String wkAppoDt) {
		writeString(Pos.WK_APPO_DT, wkAppoDt, Len.WK_APPO_DT);
	}

	/**Original name: WK-APPO-DT<br>*/
	public String getWkAppoDt() {
		return readString(Pos.WK_APPO_DT, Len.WK_APPO_DT);
	}

	/**Original name: WK-APPO-DT-RED<br>*/
	public int getWkAppoDtRed() {
		return readNumDispUnsignedInt(Pos.WK_APPO_DT_RED, Len.WK_APPO_DT_RED);
	}

	public String getWkAppoDtRedFormatted() {
		return readFixedString(Pos.WK_APPO_DT_RED, Len.WK_APPO_DT_RED);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WK_APPO_DT = 1;
		public static final int WK_APPO_DT_RED = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WK_APPO_DT = 8;
		public static final int WK_APPO_DT_RED = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

/**Original name: WCOM-FREQUENZA<br>
 * Variable: WCOM-FREQUENZA from copybook LCCC0320<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WcomFrequenza {

	//==== PROPERTIES ====
	public String value = "000000000000";
	public static final String NO_FREQUENZA = "000000000000";
	public static final String ANNUALE = "000000000001";
	public static final String SEMESTRALE = "000000000002";
	public static final String QUADRIMESTRALE = "000000000003";
	public static final String TRIMESTRALE = "000000000004";
	public static final String BIMESTRALE = "000000000006";
	public static final String MENSILE = "000000000012";

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FREQUENZA = 12;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

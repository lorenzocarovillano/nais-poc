/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-ST-GAR<br>
 * Variable: SW-ST-GAR from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwStGar {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char ST = 'S';
	public static final char NO_ST = 'N';

	//==== METHODS ====
	public void setSwStGar(char swStGar) {
		this.value = swStGar;
	}

	public char getSwStGar() {
		return this.value;
	}

	public void setSt() {
		value = ST;
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-SOPR-ALT<br>
 * Variable: DTC-SOPR-ALT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcSoprAlt extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public DtcSoprAlt() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DTC_SOPR_ALT;
	}

	public void setDtcSoprAlt(AfDecimal dtcSoprAlt) {
		writeDecimalAsPacked(Pos.DTC_SOPR_ALT, dtcSoprAlt.copy());
	}

	public void setDtcSoprAltFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.DTC_SOPR_ALT, Pos.DTC_SOPR_ALT);
	}

	/**Original name: DTC-SOPR-ALT<br>*/
	public AfDecimal getDtcSoprAlt() {
		return readPackedAsDecimal(Pos.DTC_SOPR_ALT, Len.Int.DTC_SOPR_ALT, Len.Fract.DTC_SOPR_ALT);
	}

	public byte[] getDtcSoprAltAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.DTC_SOPR_ALT, Pos.DTC_SOPR_ALT);
		return buffer;
	}

	public void setDtcSoprAltNull(String dtcSoprAltNull) {
		writeString(Pos.DTC_SOPR_ALT_NULL, dtcSoprAltNull, Len.DTC_SOPR_ALT_NULL);
	}

	/**Original name: DTC-SOPR-ALT-NULL<br>*/
	public String getDtcSoprAltNull() {
		return readString(Pos.DTC_SOPR_ALT_NULL, Len.DTC_SOPR_ALT_NULL);
	}

	public String getDtcSoprAltNullFormatted() {
		return Functions.padBlanks(getDtcSoprAltNull(), Len.DTC_SOPR_ALT_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int DTC_SOPR_ALT = 1;
		public static final int DTC_SOPR_ALT_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int DTC_SOPR_ALT = 8;
		public static final int DTC_SOPR_ALT_NULL = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int DTC_SOPR_ALT = 12;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int DTC_SOPR_ALT = 3;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}

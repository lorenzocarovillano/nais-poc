/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WKS-TROVATO-TABB<br>
 * Variable: WKS-TROVATO-TABB from program IVVS0216<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WksTrovatoTabbIvvs0216 {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.WKS_TROVATO_TABB);
	public static final String SI = "SI";
	public static final String NO = "NO";

	//==== METHODS ====
	public void setWksTrovatoTabb(String wksTrovatoTabb) {
		this.value = Functions.subString(wksTrovatoTabb, Len.WKS_TROVATO_TABB);
	}

	public String getWksTrovatoTabb() {
		return this.value;
	}

	public boolean isSi() {
		return value.equals(SI);
	}

	public void setSi() {
		value = SI;
	}

	public boolean isNo() {
		return value.equals(NO);
	}

	public void setNo() {
		value = NO;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WKS_TROVATO_TABB = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

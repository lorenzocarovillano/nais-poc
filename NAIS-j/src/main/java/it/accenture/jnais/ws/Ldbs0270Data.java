/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws;

import it.accenture.jnais.copy.IndAmmbFunzFunz;
import it.accenture.jnais.ws.redefines.WsTimestamp;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS0270<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs0270Data {

	//==== PROPERTIES ====
	//Original name: DESCRIZ-ERR-DB2
	private String descrizErrDb2 = "";
	//Original name: WS-TIMESTAMP
	private WsTimestamp wsTimestamp = new WsTimestamp();
	//Original name: IND-VAR-FUNZ-DI-CALC
	private IndAmmbFunzFunz indVarFunzDiCalc = new IndAmmbFunzFunz();

	//==== METHODS ====
	public String getDescrizErrDb2() {
		return this.descrizErrDb2;
	}

	public IndAmmbFunzFunz getIndVarFunzDiCalc() {
		return indVarFunzDiCalc;
	}

	public WsTimestamp getWsTimestamp() {
		return wsTimestamp;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_ID_MOVI_CRZ = 9;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-ID-LIVELLO-FLAG<br>
 * Variable: WK-ID-LIVELLO-FLAG from program IVVS0216<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkIdLivelloFlag {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char TROVATO = 'S';
	public static final char NON_TROVATO = 'N';

	//==== METHODS ====
	public void setWkIdLivelloFlag(char wkIdLivelloFlag) {
		this.value = wkIdLivelloFlag;
	}

	public char getWkIdLivelloFlag() {
		return this.value;
	}

	public boolean isTrovato() {
		return value == TROVATO;
	}

	public void setTrovato() {
		value = TROVATO;
	}

	public void setNonTrovato() {
		value = NON_TROVATO;
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: GARANZIA-BASE<br>
 * Variable: GARANZIA-BASE from program LCCS0490<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class GaranziaBase {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char OK = 'S';
	public static final char KO = 'N';

	//==== METHODS ====
	public void setGaranziaBase(char garanziaBase) {
		this.value = garanziaBase;
	}

	public char getGaranziaBase() {
		return this.value;
	}

	public boolean isOk() {
		return value == OK;
	}

	public void setOk() {
		value = OK;
	}

	public void setKo() {
		value = KO;
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: S089-DT-VLT<br>
 * Variable: S089-DT-VLT from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089DtVlt extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public S089DtVlt() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.S089_DT_VLT;
	}

	public void setWlquDtVlt(int wlquDtVlt) {
		writeIntAsPacked(Pos.S089_DT_VLT, wlquDtVlt, Len.Int.WLQU_DT_VLT);
	}

	public void setWlquDtVltFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.S089_DT_VLT, Pos.S089_DT_VLT);
	}

	/**Original name: WLQU-DT-VLT<br>*/
	public int getWlquDtVlt() {
		return readPackedAsInt(Pos.S089_DT_VLT, Len.Int.WLQU_DT_VLT);
	}

	public byte[] getWlquDtVltAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.S089_DT_VLT, Pos.S089_DT_VLT);
		return buffer;
	}

	public void initWlquDtVltSpaces() {
		fill(Pos.S089_DT_VLT, Len.S089_DT_VLT, Types.SPACE_CHAR);
	}

	public void setWlquDtVltNull(String wlquDtVltNull) {
		writeString(Pos.S089_DT_VLT_NULL, wlquDtVltNull, Len.WLQU_DT_VLT_NULL);
	}

	/**Original name: WLQU-DT-VLT-NULL<br>*/
	public String getWlquDtVltNull() {
		return readString(Pos.S089_DT_VLT_NULL, Len.WLQU_DT_VLT_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int S089_DT_VLT = 1;
		public static final int S089_DT_VLT_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int S089_DT_VLT = 5;
		public static final int WLQU_DT_VLT_NULL = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WLQU_DT_VLT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

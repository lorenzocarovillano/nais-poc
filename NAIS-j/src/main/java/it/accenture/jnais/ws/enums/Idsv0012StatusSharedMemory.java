/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0012-STATUS-SHARED-MEMORY<br>
 * Variable: IDSV0012-STATUS-SHARED-MEMORY from copybook IDSV0012<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0012StatusSharedMemory {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.STATUS_SHARED_MEMORY);
	public static final String OK = "OK";
	public static final String KO = "KO";

	//==== METHODS ====
	public void setStatusSharedMemory(String statusSharedMemory) {
		this.value = Functions.subString(statusSharedMemory, Len.STATUS_SHARED_MEMORY);
	}

	public String getStatusSharedMemory() {
		return this.value;
	}

	public void setKo() {
		value = KO;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int STATUS_SHARED_MEMORY = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

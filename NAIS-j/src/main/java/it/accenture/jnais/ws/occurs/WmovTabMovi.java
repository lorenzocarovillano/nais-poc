/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.occurs;

import it.accenture.jnais.copy.Lccvmov1;

/**Original name: WMOV-TAB-MOVI<br>
 * Variables: WMOV-TAB-MOVI from program LCCS0023<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WmovTabMovi {

	//==== PROPERTIES ====
	//Original name: LCCVMOV1
	private Lccvmov1 lccvmov1 = new Lccvmov1();

	//==== METHODS ====
	public Lccvmov1 getLccvmov1() {
		return lccvmov1;
	}
}

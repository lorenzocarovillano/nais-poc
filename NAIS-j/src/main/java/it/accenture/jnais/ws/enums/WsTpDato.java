/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TP-DATO<br>
 * Variable: WS-TP-DATO from copybook LCCVXDA0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTpDato {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char IMPORTO = 'N';
	public static final char PERC_MILLESIMI = 'M';
	public static final char TASSO = 'A';
	public static final char STRINGA = 'S';
	public static final char FLAG = 'F';
	public static final char NUMERICO = 'I';
	public static final char DATA2 = 'D';
	public static final char PERC_CENTESIMI = 'P';
	public static final char VAR_LISTA = 'X';

	//==== METHODS ====
	public void setWsTpDato(char wsTpDato) {
		this.value = wsTpDato;
	}

	public void setWsTpDatoFormatted(String wsTpDato) {
		setWsTpDato(Functions.charAt(wsTpDato, Types.CHAR_SIZE));
	}

	public char getWsTpDato() {
		return this.value;
	}
}

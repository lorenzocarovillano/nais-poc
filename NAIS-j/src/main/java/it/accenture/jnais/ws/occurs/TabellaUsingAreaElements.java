/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.occurs;

/**Original name: TABELLA-USING-AREA-ELEMENTS<br>
 * Variables: TABELLA-USING-AREA-ELEMENTS from copybook IDSV0009<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class TabellaUsingAreaElements {

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CODICE_STR_DATO_U_A = 30;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

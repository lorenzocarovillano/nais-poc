/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WPRE-DT-DECOR-PREST<br>
 * Variable: WPRE-DT-DECOR-PREST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpreDtDecorPrest extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WpreDtDecorPrest() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WPRE_DT_DECOR_PREST;
	}

	public void setWpreDtDecorPrest(int wpreDtDecorPrest) {
		writeIntAsPacked(Pos.WPRE_DT_DECOR_PREST, wpreDtDecorPrest, Len.Int.WPRE_DT_DECOR_PREST);
	}

	public void setWpreDtDecorPrestFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.WPRE_DT_DECOR_PREST, Pos.WPRE_DT_DECOR_PREST);
	}

	/**Original name: WPRE-DT-DECOR-PREST<br>*/
	public int getWpreDtDecorPrest() {
		return readPackedAsInt(Pos.WPRE_DT_DECOR_PREST, Len.Int.WPRE_DT_DECOR_PREST);
	}

	public byte[] getWpreDtDecorPrestAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WPRE_DT_DECOR_PREST, Pos.WPRE_DT_DECOR_PREST);
		return buffer;
	}

	public void initWpreDtDecorPrestSpaces() {
		fill(Pos.WPRE_DT_DECOR_PREST, Len.WPRE_DT_DECOR_PREST, Types.SPACE_CHAR);
	}

	public void setWpreDtDecorPrestNull(String wpreDtDecorPrestNull) {
		writeString(Pos.WPRE_DT_DECOR_PREST_NULL, wpreDtDecorPrestNull, Len.WPRE_DT_DECOR_PREST_NULL);
	}

	/**Original name: WPRE-DT-DECOR-PREST-NULL<br>*/
	public String getWpreDtDecorPrestNull() {
		return readString(Pos.WPRE_DT_DECOR_PREST_NULL, Len.WPRE_DT_DECOR_PREST_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WPRE_DT_DECOR_PREST = 1;
		public static final int WPRE_DT_DECOR_PREST_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WPRE_DT_DECOR_PREST = 5;
		public static final int WPRE_DT_DECOR_PREST_NULL = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WPRE_DT_DECOR_PREST = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: WODE-COD-LIV-AUT-APPRT<br>
 * Variable: WODE-COD-LIV-AUT-APPRT from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WodeCodLivAutApprt extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WodeCodLivAutApprt() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WODE_COD_LIV_AUT_APPRT;
	}

	public void setWodeCodLivAutApprt(int wodeCodLivAutApprt) {
		writeIntAsPacked(Pos.WODE_COD_LIV_AUT_APPRT, wodeCodLivAutApprt, Len.Int.WODE_COD_LIV_AUT_APPRT);
	}

	/**Original name: WODE-COD-LIV-AUT-APPRT<br>*/
	public int getWodeCodLivAutApprt() {
		return readPackedAsInt(Pos.WODE_COD_LIV_AUT_APPRT, Len.Int.WODE_COD_LIV_AUT_APPRT);
	}

	/**Original name: WODE-COD-LIV-AUT-APPRT-NULL<br>*/
	public String getWodeCodLivAutApprtNull() {
		return readString(Pos.WODE_COD_LIV_AUT_APPRT_NULL, Len.WODE_COD_LIV_AUT_APPRT_NULL);
	}

	public String getWodeCodLivAutApprtNullFormatted() {
		return Functions.padBlanks(getWodeCodLivAutApprtNull(), Len.WODE_COD_LIV_AUT_APPRT_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WODE_COD_LIV_AUT_APPRT = 1;
		public static final int WODE_COD_LIV_AUT_APPRT_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WODE_COD_LIV_AUT_APPRT = 3;
		public static final int WODE_COD_LIV_AUT_APPRT_NULL = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WODE_COD_LIV_AUT_APPRT = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

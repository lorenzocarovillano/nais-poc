/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCO-DT-ULT-PRE-PAG<br>
 * Variable: OCO-DT-ULT-PRE-PAG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcoDtUltPrePag extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public OcoDtUltPrePag() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.OCO_DT_ULT_PRE_PAG;
	}

	public void setOcoDtUltPrePag(int ocoDtUltPrePag) {
		writeIntAsPacked(Pos.OCO_DT_ULT_PRE_PAG, ocoDtUltPrePag, Len.Int.OCO_DT_ULT_PRE_PAG);
	}

	public void setOcoDtUltPrePagFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.OCO_DT_ULT_PRE_PAG, Pos.OCO_DT_ULT_PRE_PAG);
	}

	/**Original name: OCO-DT-ULT-PRE-PAG<br>*/
	public int getOcoDtUltPrePag() {
		return readPackedAsInt(Pos.OCO_DT_ULT_PRE_PAG, Len.Int.OCO_DT_ULT_PRE_PAG);
	}

	public byte[] getOcoDtUltPrePagAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.OCO_DT_ULT_PRE_PAG, Pos.OCO_DT_ULT_PRE_PAG);
		return buffer;
	}

	public void setOcoDtUltPrePagNull(String ocoDtUltPrePagNull) {
		writeString(Pos.OCO_DT_ULT_PRE_PAG_NULL, ocoDtUltPrePagNull, Len.OCO_DT_ULT_PRE_PAG_NULL);
	}

	/**Original name: OCO-DT-ULT-PRE-PAG-NULL<br>*/
	public String getOcoDtUltPrePagNull() {
		return readString(Pos.OCO_DT_ULT_PRE_PAG_NULL, Len.OCO_DT_ULT_PRE_PAG_NULL);
	}

	public String getOcoDtUltPrePagNullFormatted() {
		return Functions.padBlanks(getOcoDtUltPrePagNull(), Len.OCO_DT_ULT_PRE_PAG_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int OCO_DT_ULT_PRE_PAG = 1;
		public static final int OCO_DT_ULT_PRE_PAG_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int OCO_DT_ULT_PRE_PAG = 5;
		public static final int OCO_DT_ULT_PRE_PAG_NULL = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int OCO_DT_ULT_PRE_PAG = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

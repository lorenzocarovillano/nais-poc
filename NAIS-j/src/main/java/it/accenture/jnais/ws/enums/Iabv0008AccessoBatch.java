/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

/**Original name: IABV0008-ACCESSO-BATCH<br>
 * Variable: IABV0008-ACCESSO-BATCH from copybook IABV0008<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabv0008AccessoBatch {

	//==== PROPERTIES ====
	private String value = "MININS";
	public static final String ALL_BATCH = "ALLBTC";
	public static final String DT_EFF_MAX = "MAXEFF";
	public static final String DT_EFF_MIN = "MINEFF";
	public static final String DT_INS_MAX = "MAXINS";
	public static final String DT_INS_MIN = "MININS";

	//==== METHODS ====
	public String getAccessoBatch() {
		return this.value;
	}
}

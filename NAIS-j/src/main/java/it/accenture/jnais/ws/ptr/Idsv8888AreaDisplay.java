/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: IDSV8888-AREA-DISPLAY<br>
 * Variable: IDSV8888-AREA-DISPLAY from copybook IDSV8888<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class Idsv8888AreaDisplay extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public Idsv8888AreaDisplay() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.AREA_DISPLAY;
	}

	@Override
	public void init() {
		int position = 1;
		writeString(position, "", Len.AREA_DISPLAY);
	}

	public void setAreaDisplay(String areaDisplay) {
		writeString(Pos.AREA_DISPLAY, areaDisplay, Len.AREA_DISPLAY);
	}

	public String getAreaDisplay() {
		return readString(Pos.AREA_DISPLAY, Len.AREA_DISPLAY);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int AREA_DISPLAY = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int AREA_DISPLAY = 125;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

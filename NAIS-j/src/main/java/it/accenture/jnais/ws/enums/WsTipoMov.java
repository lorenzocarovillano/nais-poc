/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-TIPO-MOV<br>
 * Variable: WS-TIPO-MOV from program LVVS0089<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTipoMov {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char LIQUIDAZIONE = 'L';
	public static final char COMUNICAZIONE = 'C';

	//==== METHODS ====
	public void setWsTipoMov(char wsTipoMov) {
		this.value = wsTipoMov;
	}

	public char getWsTipoMov() {
		return this.value;
	}

	public boolean isLiquidazione() {
		return value == LIQUIDAZIONE;
	}

	public void setLiquidazione() {
		value = LIQUIDAZIONE;
	}

	public void setComunicazione() {
		value = COMUNICAZIONE;
	}
}

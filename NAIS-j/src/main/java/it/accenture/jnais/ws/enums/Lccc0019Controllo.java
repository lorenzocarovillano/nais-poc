/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCC0019-CONTROLLO<br>
 * Variable: LCCC0019-CONTROLLO from copybook LCCC0019<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccc0019Controllo {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.CONTROLLO);
	public static final String SI = "SI";
	public static final String NO = "NO";

	//==== METHODS ====
	public void setControllo(String controllo) {
		this.value = Functions.subString(controllo, Len.CONTROLLO);
	}

	public String getControllo() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CONTROLLO = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

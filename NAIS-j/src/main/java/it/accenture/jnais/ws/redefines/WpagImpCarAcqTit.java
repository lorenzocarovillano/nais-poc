/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPAG-IMP-CAR-ACQ-TIT<br>
 * Variable: WPAG-IMP-CAR-ACQ-TIT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpCarAcqTit extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WpagImpCarAcqTit() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WPAG_IMP_CAR_ACQ_TIT;
	}

	public void setWpagImpCarAcqTit(AfDecimal wpagImpCarAcqTit) {
		writeDecimalAsPacked(Pos.WPAG_IMP_CAR_ACQ_TIT, wpagImpCarAcqTit.copy());
	}

	public void setWpagImpCarAcqTitFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.WPAG_IMP_CAR_ACQ_TIT, Pos.WPAG_IMP_CAR_ACQ_TIT);
	}

	/**Original name: WPAG-IMP-CAR-ACQ-TIT<br>*/
	public AfDecimal getWpagImpCarAcqTit() {
		return readPackedAsDecimal(Pos.WPAG_IMP_CAR_ACQ_TIT, Len.Int.WPAG_IMP_CAR_ACQ_TIT, Len.Fract.WPAG_IMP_CAR_ACQ_TIT);
	}

	public byte[] getWpagImpCarAcqTitAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WPAG_IMP_CAR_ACQ_TIT, Pos.WPAG_IMP_CAR_ACQ_TIT);
		return buffer;
	}

	public void initWpagImpCarAcqTitSpaces() {
		fill(Pos.WPAG_IMP_CAR_ACQ_TIT, Len.WPAG_IMP_CAR_ACQ_TIT, Types.SPACE_CHAR);
	}

	public void setWpagImpCarAcqTitNull(String wpagImpCarAcqTitNull) {
		writeString(Pos.WPAG_IMP_CAR_ACQ_TIT_NULL, wpagImpCarAcqTitNull, Len.WPAG_IMP_CAR_ACQ_TIT_NULL);
	}

	/**Original name: WPAG-IMP-CAR-ACQ-TIT-NULL<br>*/
	public String getWpagImpCarAcqTitNull() {
		return readString(Pos.WPAG_IMP_CAR_ACQ_TIT_NULL, Len.WPAG_IMP_CAR_ACQ_TIT_NULL);
	}

	public String getWpagImpCarAcqTitNullFormatted() {
		return Functions.padBlanks(getWpagImpCarAcqTitNull(), Len.WPAG_IMP_CAR_ACQ_TIT_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WPAG_IMP_CAR_ACQ_TIT = 1;
		public static final int WPAG_IMP_CAR_ACQ_TIT_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WPAG_IMP_CAR_ACQ_TIT = 8;
		public static final int WPAG_IMP_CAR_ACQ_TIT_NULL = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Fract {

			//==== PROPERTIES ====
			public static final int WPAG_IMP_CAR_ACQ_TIT = 3;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}

		public static class Int {

			//==== PROPERTIES ====
			public static final int WPAG_IMP_CAR_ACQ_TIT = 12;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

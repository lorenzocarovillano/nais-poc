/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-ID-MOVI-CHIU<br>
 * Variable: DFA-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaIdMoviChiu extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public DfaIdMoviChiu() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DFA_ID_MOVI_CHIU;
	}

	public void setDfaIdMoviChiu(int dfaIdMoviChiu) {
		writeIntAsPacked(Pos.DFA_ID_MOVI_CHIU, dfaIdMoviChiu, Len.Int.DFA_ID_MOVI_CHIU);
	}

	public void setDfaIdMoviChiuFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.DFA_ID_MOVI_CHIU, Pos.DFA_ID_MOVI_CHIU);
	}

	/**Original name: DFA-ID-MOVI-CHIU<br>*/
	public int getDfaIdMoviChiu() {
		return readPackedAsInt(Pos.DFA_ID_MOVI_CHIU, Len.Int.DFA_ID_MOVI_CHIU);
	}

	public byte[] getDfaIdMoviChiuAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.DFA_ID_MOVI_CHIU, Pos.DFA_ID_MOVI_CHIU);
		return buffer;
	}

	public void setDfaIdMoviChiuNull(String dfaIdMoviChiuNull) {
		writeString(Pos.DFA_ID_MOVI_CHIU_NULL, dfaIdMoviChiuNull, Len.DFA_ID_MOVI_CHIU_NULL);
	}

	/**Original name: DFA-ID-MOVI-CHIU-NULL<br>*/
	public String getDfaIdMoviChiuNull() {
		return readString(Pos.DFA_ID_MOVI_CHIU_NULL, Len.DFA_ID_MOVI_CHIU_NULL);
	}

	public String getDfaIdMoviChiuNullFormatted() {
		return Functions.padBlanks(getDfaIdMoviChiuNull(), Len.DFA_ID_MOVI_CHIU_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int DFA_ID_MOVI_CHIU = 1;
		public static final int DFA_ID_MOVI_CHIU_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int DFA_ID_MOVI_CHIU = 5;
		public static final int DFA_ID_MOVI_CHIU_NULL = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int DFA_ID_MOVI_CHIU = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

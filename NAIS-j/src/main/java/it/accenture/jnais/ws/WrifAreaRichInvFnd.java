/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;

import it.accenture.jnais.ws.redefines.WrifTabella;

/**Original name: WRIF-AREA-RICH-INV-FND<br>
 * Variable: WRIF-AREA-RICH-INV-FND from program IVVS0211<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WrifAreaRichInvFnd {

	//==== PROPERTIES ====
	//Original name: WRIF-ELE-RIC-INV-MAX
	private short wrifEleRicInvMax = DefaultValues.BIN_SHORT_VAL;
	//Original name: WRIF-TABELLA
	private WrifTabella wrifTabella = new WrifTabella();

	//==== METHODS ====
	public void setWrifAreaRichInvFndFormatted(String data) {
		byte[] buffer = new byte[Len.WRIF_AREA_RICH_INV_FND];
		MarshalByte.writeString(buffer, 1, data, Len.WRIF_AREA_RICH_INV_FND);
		setWrifAreaRichInvFndBytes(buffer, 1);
	}

	public void setWrifAreaRichInvFndBytes(byte[] buffer, int offset) {
		int position = offset;
		wrifEleRicInvMax = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		wrifTabella.setWrifTabellaBytes(buffer, position);
	}

	public byte[] getWrifAreaRichInvFndBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, wrifEleRicInvMax);
		position += Types.SHORT_SIZE;
		wrifTabella.getWrifTabellaBytes(buffer, position);
		return buffer;
	}

	public void setWrifEleRicInvMax(short wrifEleRicInvMax) {
		this.wrifEleRicInvMax = wrifEleRicInvMax;
	}

	public short getWrifEleRicInvMax() {
		return this.wrifEleRicInvMax;
	}

	public WrifTabella getWrifTabella() {
		return wrifTabella;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WRIF_ELE_RIC_INV_MAX = 2;
		public static final int WRIF_AREA_RICH_INV_FND = WRIF_ELE_RIC_INV_MAX + WrifTabella.Len.WRIF_TABELLA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

/**Original name: WK-SIMULAZIONE<br>
 * Variable: WK-SIMULAZIONE from copybook IABV0007<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkSimulazione {

	//==== PROPERTIES ====
	private char value = 'N';
	public static final char INFR = 'I';
	public static final char FITTIZIA = 'S';
	public static final char APPL = 'A';
	public static final char NO = 'N';

	//==== METHODS ====
	public void setWkSimulazione(char wkSimulazione) {
		this.value = wkSimulazione;
	}

	public char getFlagSimulazione() {
		return this.value;
	}

	public boolean isInfr() {
		return value == INFR;
	}

	public boolean isFittizia() {
		return value == FITTIZIA;
	}

	public boolean isAppl() {
		return value == APPL;
	}

	public boolean isNo() {
		return value == NO;
	}
}

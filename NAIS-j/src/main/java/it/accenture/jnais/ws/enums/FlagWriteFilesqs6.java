/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-WRITE-FILESQS6<br>
 * Variable: FLAG-WRITE-FILESQS6 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagWriteFilesqs6 {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char SI = 'S';
	public static final char NO = 'N';

	//==== METHODS ====
	public void setFlagWriteFilesqs6(char flagWriteFilesqs6) {
		this.value = flagWriteFilesqs6;
	}

	public char getFlagWriteFilesqs6() {
		return this.value;
	}

	public boolean isWriteFilesqs6Si() {
		return value == SI;
	}

	public void setWriteFilesqs6Si() {
		value = SI;
	}

	public void setWriteFilesqs6No() {
		value = NO;
	}
}

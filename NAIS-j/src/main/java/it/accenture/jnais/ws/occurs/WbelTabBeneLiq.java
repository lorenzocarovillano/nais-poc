/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;

import it.accenture.jnais.copy.Lccvbel1;
import it.accenture.jnais.copy.WbelDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WBEL-TAB-BENE-LIQ<br>
 * Variables: WBEL-TAB-BENE-LIQ from copybook LCCVBELA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WbelTabBeneLiq {

	//==== PROPERTIES ====
	//Original name: LCCVBEL1
	private Lccvbel1 lccvbel1 = new Lccvbel1();

	//==== METHODS ====
	public void setWbelTabBeneLiqBytes(byte[] buffer, int offset) {
		int position = offset;
		lccvbel1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		lccvbel1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvbel1.Len.Int.ID_PTF, 0));
		position += Lccvbel1.Len.ID_PTF;
		lccvbel1.getDati().setDatiBytes(buffer, position);
	}

	public byte[] getWbelTabBeneLiqBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, lccvbel1.getStatus().getStatus());
		position += Types.CHAR_SIZE;
		MarshalByte.writeIntAsPacked(buffer, position, lccvbel1.getIdPtf(), Lccvbel1.Len.Int.ID_PTF, 0);
		position += Lccvbel1.Len.ID_PTF;
		lccvbel1.getDati().getDatiBytes(buffer, position);
		return buffer;
	}

	public void initWbelTabBeneLiqSpaces() {
		lccvbel1.initLccvbel1Spaces();
	}

	public Lccvbel1 getLccvbel1() {
		return lccvbel1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WBEL_TAB_BENE_LIQ = WpolStatus.Len.STATUS + Lccvbel1.Len.ID_PTF + WbelDati.Len.DATI;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

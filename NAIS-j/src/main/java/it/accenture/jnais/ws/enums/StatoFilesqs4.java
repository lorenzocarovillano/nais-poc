/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: STATO-FILESQS4<br>
 * Variable: STATO-FILESQS4 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class StatoFilesqs4 {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char APERTO = 'A';
	public static final char CHIUSO = 'C';

	//==== METHODS ====
	public void setStatoFilesqs4(char statoFilesqs4) {
		this.value = statoFilesqs4;
	}

	public char getStatoFilesqs4() {
		return this.value;
	}

	public boolean isAperto() {
		return value == APERTO;
	}

	public void setAperto() {
		value = APERTO;
	}

	public void setChiuso() {
		value = CHIUSO;
	}
}

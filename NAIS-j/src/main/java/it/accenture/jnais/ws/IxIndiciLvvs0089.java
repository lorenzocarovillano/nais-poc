/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program LVVS0089<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndiciLvvs0089 {

	//==== PROPERTIES ====
	//Original name: IX-DCLGEN
	private short dclgen = DefaultValues.BIN_SHORT_VAL;
	//Original name: IX-TAB-TGA
	private short tabTga = DefaultValues.BIN_SHORT_VAL;
	//Original name: IX-TAB
	private short tab = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	public void setDclgen(short dclgen) {
		this.dclgen = dclgen;
	}

	public short getDclgen() {
		return this.dclgen;
	}

	public void setTabTga(short tabTga) {
		this.tabTga = tabTga;
	}

	public short getTabTga() {
		return this.tabTga;
	}

	public void setTab(short tab) {
		this.tab = tab;
	}

	public short getTab() {
		return this.tab;
	}
}

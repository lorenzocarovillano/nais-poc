/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: P89-ID-MOVI-CHIU<br>
 * Variable: P89-ID-MOVI-CHIU from program IDBSP890<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P89IdMoviChiu extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public P89IdMoviChiu() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.P89_ID_MOVI_CHIU;
	}

	public void setP89IdMoviChiu(int p89IdMoviChiu) {
		writeIntAsPacked(Pos.P89_ID_MOVI_CHIU, p89IdMoviChiu, Len.Int.P89_ID_MOVI_CHIU);
	}

	public void setP89IdMoviChiuFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.P89_ID_MOVI_CHIU, Pos.P89_ID_MOVI_CHIU);
	}

	/**Original name: P89-ID-MOVI-CHIU<br>*/
	public int getP89IdMoviChiu() {
		return readPackedAsInt(Pos.P89_ID_MOVI_CHIU, Len.Int.P89_ID_MOVI_CHIU);
	}

	public byte[] getP89IdMoviChiuAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.P89_ID_MOVI_CHIU, Pos.P89_ID_MOVI_CHIU);
		return buffer;
	}

	public void setP89IdMoviChiuNull(String p89IdMoviChiuNull) {
		writeString(Pos.P89_ID_MOVI_CHIU_NULL, p89IdMoviChiuNull, Len.P89_ID_MOVI_CHIU_NULL);
	}

	/**Original name: P89-ID-MOVI-CHIU-NULL<br>*/
	public String getP89IdMoviChiuNull() {
		return readString(Pos.P89_ID_MOVI_CHIU_NULL, Len.P89_ID_MOVI_CHIU_NULL);
	}

	public String getP89IdMoviChiuNullFormatted() {
		return Functions.padBlanks(getP89IdMoviChiuNull(), Len.P89_ID_MOVI_CHIU_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int P89_ID_MOVI_CHIU = 1;
		public static final int P89_ID_MOVI_CHIU_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int P89_ID_MOVI_CHIU = 5;
		public static final int P89_ID_MOVI_CHIU_NULL = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int P89_ID_MOVI_CHIU = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

/**Original name: WCOM-FL-MOD-CALCOLO<br>
 * Variable: WCOM-FL-MOD-CALCOLO from copybook LCCC0320<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WcomFlModCalcolo {

	//==== PROPERTIES ====
	public String value = "0";
	public static final String NO_GESTIONE = "0";
	public static final String GESTIONE_ANNIVERSARIA = "1";
	public static final String GESTIONE_DATA_FISSA = "2";
	public static final String GESTIONE_DATA_SPECIFICA = "3";

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FL_MOD_CALCOLO = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.lang.ICopyable;

import it.accenture.jnais.copy.Lccvrif1;
import it.accenture.jnais.copy.WrifDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WRIF-TAB-RIC-INV<br>
 * Variables: WRIF-TAB-RIC-INV from copybook LCCVRIFA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WrifTabRicInv implements ICopyable<WrifTabRicInv> {

	//==== PROPERTIES ====
	//Original name: LCCVRIF1
	private Lccvrif1 lccvrif1 = new Lccvrif1();

	//==== CONSTRUCTORS ====
	public WrifTabRicInv() {
	}

	public WrifTabRicInv(WrifTabRicInv wrifTabRicInv) {
		this();
		this.lccvrif1 = (wrifTabRicInv.lccvrif1.copy());
	}

	//==== METHODS ====
	public void setWrifTabRicInvBytes(byte[] buffer, int offset) {
		int position = offset;
		lccvrif1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		lccvrif1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvrif1.Len.Int.ID_PTF, 0));
		position += Lccvrif1.Len.ID_PTF;
		lccvrif1.getDati().setDatiBytes(buffer, position);
	}

	public byte[] getWrifTabRicInvBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, lccvrif1.getStatus().getStatus());
		position += Types.CHAR_SIZE;
		MarshalByte.writeIntAsPacked(buffer, position, lccvrif1.getIdPtf(), Lccvrif1.Len.Int.ID_PTF, 0);
		position += Lccvrif1.Len.ID_PTF;
		lccvrif1.getDati().getDatiBytes(buffer, position);
		return buffer;
	}

	public WrifTabRicInv initWrifTabRicInvSpaces() {
		lccvrif1.initLccvrif1Spaces();
		return this;
	}

	public Lccvrif1 getLccvrif1() {
		return lccvrif1;
	}

	@Override
	public WrifTabRicInv copy() {
		return new WrifTabRicInv(this);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WRIF_TAB_RIC_INV = WpolStatus.Len.STATUS + Lccvrif1.Len.ID_PTF + WrifDati.Len.DATI;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

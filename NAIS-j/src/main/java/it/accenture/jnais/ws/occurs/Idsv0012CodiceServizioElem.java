/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.occurs;

/**Original name: IDSV0012-CODICE-SERVIZIO-ELEM<br>
 * Variables: IDSV0012-CODICE-SERVIZIO-ELEM from copybook IDSV0012<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Idsv0012CodiceServizioElem {

	//==== CONSTRUCTORS ====
	public Idsv0012CodiceServizioElem() {
		init();
	}

	//==== METHODS ====
	public void init() {
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CODICE_SERVIZIO = 8;
		public static final int TIPO_SERVIZIO = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

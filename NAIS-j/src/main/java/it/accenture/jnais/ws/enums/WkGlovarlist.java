/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-GLOVARLIST<br>
 * Variable: WK-GLOVARLIST from program IVVS0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkGlovarlist {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.WK_GLOVARLIST);
	public static final String VUOTA = "GV";
	public static final String PIENA = "GP";

	//==== METHODS ====
	public void setWkGlovarlist(String wkGlovarlist) {
		this.value = Functions.subString(wkGlovarlist, Len.WK_GLOVARLIST);
	}

	public String getWkGlovarlist() {
		return this.value;
	}

	public boolean isVuota() {
		return value.equals(VUOTA);
	}

	public void setVuota() {
		value = VUOTA;
	}

	public void setPiena() {
		value = PIENA;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WK_GLOVARLIST = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

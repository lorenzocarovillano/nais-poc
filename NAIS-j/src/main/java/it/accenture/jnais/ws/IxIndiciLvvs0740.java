/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program LVVS0740<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndiciLvvs0740 {

	//==== PROPERTIES ====
	//Original name: IX-DCLGEN
	private short dclgen = DefaultValues.BIN_SHORT_VAL;
	//Original name: IX-TAB-ADE
	private short tabAde = DefaultValues.BIN_SHORT_VAL;
	//Original name: IX-TAB-TIT
	private short tabTit = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	public void setDclgen(short dclgen) {
		this.dclgen = dclgen;
	}

	public short getDclgen() {
		return this.dclgen;
	}

	public void setTabAde(short tabAde) {
		this.tabAde = tabAde;
	}

	public short getTabAde() {
		return this.tabAde;
	}

	public void setTabTit(short tabTit) {
		this.tabTit = tabTit;
	}

	public short getTabTit() {
		return this.tabTit;
	}
}

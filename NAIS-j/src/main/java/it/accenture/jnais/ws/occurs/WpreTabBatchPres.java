/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.occurs;

import it.accenture.jnais.copy.WpreAreaTabBatchPres;

/**Original name: WPRE-TAB-BATCH-PRES<br>
 * Variables: WPRE-TAB-BATCH-PRES from copybook LOAC0560<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WpreTabBatchPres {

	//==== PROPERTIES ====
	//Original name: WPRE-AREA-TAB-BATCH-PRES
	private WpreAreaTabBatchPres areaTabBatchPres = new WpreAreaTabBatchPres();

	//==== METHODS ====
	public WpreAreaTabBatchPres getAreaTabBatchPres() {
		return areaTabBatchPres;
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WPOL-AA-DIFF-PROR-DFLT<br>
 * Variable: WPOL-AA-DIFF-PROR-DFLT from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpolAaDiffProrDflt extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WpolAaDiffProrDflt() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WPOL_AA_DIFF_PROR_DFLT;
	}

	public void setWpolAaDiffProrDflt(int wpolAaDiffProrDflt) {
		writeIntAsPacked(Pos.WPOL_AA_DIFF_PROR_DFLT, wpolAaDiffProrDflt, Len.Int.WPOL_AA_DIFF_PROR_DFLT);
	}

	public void setWpolAaDiffProrDfltFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.WPOL_AA_DIFF_PROR_DFLT, Pos.WPOL_AA_DIFF_PROR_DFLT);
	}

	/**Original name: WPOL-AA-DIFF-PROR-DFLT<br>*/
	public int getWpolAaDiffProrDflt() {
		return readPackedAsInt(Pos.WPOL_AA_DIFF_PROR_DFLT, Len.Int.WPOL_AA_DIFF_PROR_DFLT);
	}

	public byte[] getWpolAaDiffProrDfltAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WPOL_AA_DIFF_PROR_DFLT, Pos.WPOL_AA_DIFF_PROR_DFLT);
		return buffer;
	}

	public void setWpolAaDiffProrDfltNull(String wpolAaDiffProrDfltNull) {
		writeString(Pos.WPOL_AA_DIFF_PROR_DFLT_NULL, wpolAaDiffProrDfltNull, Len.WPOL_AA_DIFF_PROR_DFLT_NULL);
	}

	/**Original name: WPOL-AA-DIFF-PROR-DFLT-NULL<br>*/
	public String getWpolAaDiffProrDfltNull() {
		return readString(Pos.WPOL_AA_DIFF_PROR_DFLT_NULL, Len.WPOL_AA_DIFF_PROR_DFLT_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WPOL_AA_DIFF_PROR_DFLT = 1;
		public static final int WPOL_AA_DIFF_PROR_DFLT_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WPOL_AA_DIFF_PROR_DFLT = 3;
		public static final int WPOL_AA_DIFF_PROR_DFLT_NULL = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WPOL_AA_DIFF_PROR_DFLT = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

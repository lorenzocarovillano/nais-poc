/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WS-VARIABILI<br>
 * Variable: WS-VARIABILI from program LCCS0033<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsVariabiliLccs0033 {

	//==== PROPERTIES ====
	//Original name: WK-ID-OGG
	private int wkIdOgg = DefaultValues.INT_VAL;
	//Original name: WS-PREMIO-ANNUO-TGA
	private AfDecimal wsPremioAnnuoTga = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
	//Original name: WS-PREMIO-ANNUO-CALC
	private AfDecimal wsPremioAnnuoCalc = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

	//==== METHODS ====
	public void setWkIdOgg(int wkIdOgg) {
		this.wkIdOgg = wkIdOgg;
	}

	public int getWkIdOgg() {
		return this.wkIdOgg;
	}

	public void setWsPremioAnnuoTga(AfDecimal wsPremioAnnuoTga) {
		this.wsPremioAnnuoTga.assign(wsPremioAnnuoTga);
	}

	public AfDecimal getWsPremioAnnuoTga() {
		return this.wsPremioAnnuoTga.copy();
	}

	public void setWsPremioAnnuoCalc(AfDecimal wsPremioAnnuoCalc) {
		this.wsPremioAnnuoCalc.assign(wsPremioAnnuoCalc);
	}

	public AfDecimal getWsPremioAnnuoCalc() {
		return this.wsPremioAnnuoCalc.copy();
	}
}

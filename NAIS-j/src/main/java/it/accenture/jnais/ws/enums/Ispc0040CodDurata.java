/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISPC0040-COD-DURATA<br>
 * Variable: ISPC0040-COD-DURATA from copybook ISPC0040<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispc0040CodDurata {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.COD_DURATA);
	public static final String ANNI_INTERI = "AA";
	public static final String ANNI_MESI = "AM";
	public static final String GIORNI = "GG";

	//==== METHODS ====
	public void setCodDurata(String codDurata) {
		this.value = Functions.subString(codDurata, Len.COD_DURATA);
	}

	public String getCodDurata() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int COD_DURATA = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

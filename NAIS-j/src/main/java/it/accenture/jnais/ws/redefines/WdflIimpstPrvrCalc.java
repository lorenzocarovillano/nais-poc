/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IIMPST-PRVR-CALC<br>
 * Variable: WDFL-IIMPST-PRVR-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIimpstPrvrCalc extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WdflIimpstPrvrCalc() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WDFL_IIMPST_PRVR_CALC;
	}

	public void setWdflIimpstPrvrCalc(AfDecimal wdflIimpstPrvrCalc) {
		writeDecimalAsPacked(Pos.WDFL_IIMPST_PRVR_CALC, wdflIimpstPrvrCalc.copy());
	}

	public void setWdflIimpstPrvrCalcFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.WDFL_IIMPST_PRVR_CALC, Pos.WDFL_IIMPST_PRVR_CALC);
	}

	/**Original name: WDFL-IIMPST-PRVR-CALC<br>*/
	public AfDecimal getWdflIimpstPrvrCalc() {
		return readPackedAsDecimal(Pos.WDFL_IIMPST_PRVR_CALC, Len.Int.WDFL_IIMPST_PRVR_CALC, Len.Fract.WDFL_IIMPST_PRVR_CALC);
	}

	public byte[] getWdflIimpstPrvrCalcAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WDFL_IIMPST_PRVR_CALC, Pos.WDFL_IIMPST_PRVR_CALC);
		return buffer;
	}

	public void setWdflIimpstPrvrCalcNull(String wdflIimpstPrvrCalcNull) {
		writeString(Pos.WDFL_IIMPST_PRVR_CALC_NULL, wdflIimpstPrvrCalcNull, Len.WDFL_IIMPST_PRVR_CALC_NULL);
	}

	/**Original name: WDFL-IIMPST-PRVR-CALC-NULL<br>*/
	public String getWdflIimpstPrvrCalcNull() {
		return readString(Pos.WDFL_IIMPST_PRVR_CALC_NULL, Len.WDFL_IIMPST_PRVR_CALC_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WDFL_IIMPST_PRVR_CALC = 1;
		public static final int WDFL_IIMPST_PRVR_CALC_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WDFL_IIMPST_PRVR_CALC = 8;
		public static final int WDFL_IIMPST_PRVR_CALC_NULL = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Fract {

			//==== PROPERTIES ====
			public static final int WDFL_IIMPST_PRVR_CALC = 3;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}

		public static class Int {

			//==== PROPERTIES ====
			public static final int WDFL_IIMPST_PRVR_CALC = 12;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

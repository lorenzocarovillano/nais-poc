/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-ST-ADES<br>
 * Variable: SW-ST-ADES from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwStAdes {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char STORNATA = 'S';
	public static final char NO_STORNATA = 'N';

	//==== METHODS ====
	public void setSwStAdes(char swStAdes) {
		this.value = swStAdes;
	}

	public char getSwStAdes() {
		return this.value;
	}

	public boolean isNoStornata() {
		return value == NO_STORNATA;
	}

	public void setNoStornata() {
		value = NO_STORNATA;
	}
}

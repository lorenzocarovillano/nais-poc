/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

/**Original name: WK-TIPO-ELABORAZIONE<br>
 * Variable: WK-TIPO-ELABORAZIONE from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkTipoElaborazione {

	//==== PROPERTIES ====
	private String value = "2";
	public static final String RAMO_OPERAZ = "1";
	public static final String STANDARD = "2";

	//==== METHODS ====
	public String getWkTipoElaborazioneFormatted() {
		return this.value;
	}

	public boolean isRamoOperaz() {
		return getWkTipoElaborazioneFormatted().equals(RAMO_OPERAZ);
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: CSD-LUNGHEZZA-DATO<br>
 * Variable: CSD-LUNGHEZZA-DATO from program IDSS0020<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class CsdLunghezzaDato extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public CsdLunghezzaDato() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.CSD_LUNGHEZZA_DATO;
	}

	public void setCsdLunghezzaDato(int csdLunghezzaDato) {
		writeIntAsPacked(Pos.CSD_LUNGHEZZA_DATO, csdLunghezzaDato, Len.Int.CSD_LUNGHEZZA_DATO);
	}

	/**Original name: CSD-LUNGHEZZA-DATO<br>*/
	public int getCsdLunghezzaDato() {
		return readPackedAsInt(Pos.CSD_LUNGHEZZA_DATO, Len.Int.CSD_LUNGHEZZA_DATO);
	}

	public void initCsdLunghezzaDatoSpaces() {
		fill(Pos.CSD_LUNGHEZZA_DATO, Len.CSD_LUNGHEZZA_DATO, Types.SPACE_CHAR);
	}

	public void setCsdLunghezzaDatoNull(String csdLunghezzaDatoNull) {
		writeString(Pos.CSD_LUNGHEZZA_DATO_NULL, csdLunghezzaDatoNull, Len.CSD_LUNGHEZZA_DATO_NULL);
	}

	/**Original name: CSD-LUNGHEZZA-DATO-NULL<br>*/
	public String getCsdLunghezzaDatoNull() {
		return readString(Pos.CSD_LUNGHEZZA_DATO_NULL, Len.CSD_LUNGHEZZA_DATO_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int CSD_LUNGHEZZA_DATO = 1;
		public static final int CSD_LUNGHEZZA_DATO_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int CSD_LUNGHEZZA_DATO = 3;
		public static final int CSD_LUNGHEZZA_DATO_NULL = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int CSD_LUNGHEZZA_DATO = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: EOF-FILESQS5<br>
 * Variable: EOF-FILESQS5 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class EofFilesqs5 {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char SI = 'S';
	public static final char NO = 'N';

	//==== METHODS ====
	public void setEofFilesqs5(char eofFilesqs5) {
		this.value = eofFilesqs5;
	}

	public char getEofFilesqs5() {
		return this.value;
	}

	public boolean isFilesqs5EofSi() {
		return value == SI;
	}

	public void setSi() {
		value = SI;
	}

	public boolean isFilesqs5EofNo() {
		return value == NO;
	}

	public void setNo() {
		value = NO;
	}
}

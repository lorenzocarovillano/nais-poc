/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCC0019-RETURN-CODE<br>
 * Variable: LCCC0019-RETURN-CODE from copybook LCCC0019<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccc0019ReturnCode {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.RETURN_CODE);
	private static final String[] SUCCESSFUL_RC = new String[] { "00", "IN" };
	public static final String INFORMATION = "IN";
	public static final String SQL_ERROR = "D3";
	public static final String GENERIC_ERROR = "KO";

	//==== METHODS ====
	public void setReturnCode(String returnCode) {
		this.value = Functions.subString(returnCode, Len.RETURN_CODE);
	}

	public String getReturnCode() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RETURN_CODE = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

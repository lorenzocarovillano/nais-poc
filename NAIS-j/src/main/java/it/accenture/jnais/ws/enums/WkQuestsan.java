/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

/**Original name: WK-QUESTSAN<br>
 * Variable: WK-QUESTSAN from program LVES0269<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkQuestsan {

	//==== PROPERTIES ====
	private char value = 'N';
	public static final char SI = 'S';
	public static final char NO = 'N';
	public static final char SPECIALE = 'F';

	//==== METHODS ====
	public void setWkQuestsan(char wkQuestsan) {
		this.value = wkQuestsan;
	}

	public char getWkQuestsan() {
		return this.value;
	}

	public void setSi() {
		value = SI;
	}

	public void setSpeciale() {
		value = SPECIALE;
	}
}

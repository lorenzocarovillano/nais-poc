/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

/**Original name: IABV0008-TEMPORARY-TABLE<br>
 * Variable: IABV0008-TEMPORARY-TABLE from copybook IABV0008<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabv0008TemporaryTable {

	//==== PROPERTIES ====
	private String value = "NOTABLE";
	public static final String NO_TEMP_TABLE = "NOTABLE";
	public static final String STATIC_TEMP_TABLE = "STATIC";
	public static final String SESSION_TEMP_TABLE = "SESSION";

	//==== METHODS ====
	public String getTemporaryTable() {
		return this.value;
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws;

import it.accenture.jnais.ws.enums.SwFineElementi;
import it.accenture.jnais.ws.enums.WkVarlist;

/**Original name: SW-SWITCH<br>
 * Variable: SW-SWITCH from program IVVS0212<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SwSwitchIvvs0212 {

	//==== PROPERTIES ====
	//Original name: SW-TROVATO
	private boolean swTrovato = false;
	//Original name: WK-VARLIST
	private WkVarlist wkVarlist = new WkVarlist();
	//Original name: SW-FINE-ELEMENTI
	private SwFineElementi swFineElementi = new SwFineElementi();

	//==== METHODS ====
	public void setSwTrovato(boolean swTrovato) {
		this.swTrovato = swTrovato;
	}

	public boolean isSwTrovato() {
		return this.swTrovato;
	}

	public SwFineElementi getSwFineElementi() {
		return swFineElementi;
	}

	public WkVarlist getWkVarlist() {
		return wkVarlist;
	}
}

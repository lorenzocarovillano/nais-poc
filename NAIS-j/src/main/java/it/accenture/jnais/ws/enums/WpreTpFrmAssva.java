/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPRE-TP-FRM-ASSVA<br>
 * Variable: WPRE-TP-FRM-ASSVA from copybook LOAC0560<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WpreTpFrmAssva {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.TP_FRM_ASSVA);
	public static final String COLLETTIVA = "CO";
	public static final String INDIVIDUALI = "IN";
	public static final String FT_ENTRAMBE = "EN";

	//==== METHODS ====
	public void setTpFrmAssva(String tpFrmAssva) {
		this.value = Functions.subString(tpFrmAssva, Len.TP_FRM_ASSVA);
	}

	public String getTpFrmAssva() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TP_FRM_ASSVA = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}

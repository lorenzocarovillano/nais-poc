/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

/**Original name: IABV0008-GESTIONE-RIPARTENZA<br>
 * Variable: IABV0008-GESTIONE-RIPARTENZA from copybook IABV0008<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabv0008GestioneRipartenza {

	//==== PROPERTIES ====
	private String value = "SM";
	public static final String SENZA_RIPARTENZA = "SR";
	public static final String RIP_CON_MONITORING = "CM";
	public static final String RIP_SENZA_MONITORING = "SM";
	public static final String RIP_CON_VERSIONAMENTO = "CV";

	//==== METHODS ====
	public String getGestioneRipartenza() {
		return this.value;
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WDAD-ETA-SCAD-MASC-DFLT<br>
 * Variable: WDAD-ETA-SCAD-MASC-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdadEtaScadMascDflt extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WdadEtaScadMascDflt() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WDAD_ETA_SCAD_MASC_DFLT;
	}

	public void setWdadEtaScadMascDflt(int wdadEtaScadMascDflt) {
		writeIntAsPacked(Pos.WDAD_ETA_SCAD_MASC_DFLT, wdadEtaScadMascDflt, Len.Int.WDAD_ETA_SCAD_MASC_DFLT);
	}

	public void setWdadEtaScadMascDfltFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.WDAD_ETA_SCAD_MASC_DFLT, Pos.WDAD_ETA_SCAD_MASC_DFLT);
	}

	/**Original name: WDAD-ETA-SCAD-MASC-DFLT<br>*/
	public int getWdadEtaScadMascDflt() {
		return readPackedAsInt(Pos.WDAD_ETA_SCAD_MASC_DFLT, Len.Int.WDAD_ETA_SCAD_MASC_DFLT);
	}

	public byte[] getWdadEtaScadMascDfltAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WDAD_ETA_SCAD_MASC_DFLT, Pos.WDAD_ETA_SCAD_MASC_DFLT);
		return buffer;
	}

	public void setWdadEtaScadMascDfltNull(String wdadEtaScadMascDfltNull) {
		writeString(Pos.WDAD_ETA_SCAD_MASC_DFLT_NULL, wdadEtaScadMascDfltNull, Len.WDAD_ETA_SCAD_MASC_DFLT_NULL);
	}

	/**Original name: WDAD-ETA-SCAD-MASC-DFLT-NULL<br>*/
	public String getWdadEtaScadMascDfltNull() {
		return readString(Pos.WDAD_ETA_SCAD_MASC_DFLT_NULL, Len.WDAD_ETA_SCAD_MASC_DFLT_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WDAD_ETA_SCAD_MASC_DFLT = 1;
		public static final int WDAD_ETA_SCAD_MASC_DFLT_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WDAD_ETA_SCAD_MASC_DFLT = 3;
		public static final int WDAD_ETA_SCAD_MASC_DFLT_NULL = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WDAD_ETA_SCAD_MASC_DFLT = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

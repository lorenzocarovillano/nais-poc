/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-GRAV-FUNZ-BLOCCO<br>
 * Variable: WK-GRAV-FUNZ-BLOCCO from program LCCS0022<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkGravFunzBlocco {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char WARNING = 'W';
	public static final char BLOCCANTE = 'B';
	public static final char AMMESSO = 'A';

	//==== METHODS ====
	public void setWkGravFunzBlocco(char wkGravFunzBlocco) {
		this.value = wkGravFunzBlocco;
	}

	public char getWkGravFunzBlocco() {
		return this.value;
	}

	public boolean isBloccante() {
		return value == BLOCCANTE;
	}

	public boolean isAmmesso() {
		return value == AMMESSO;
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: POL-DT-PRESC<br>
 * Variable: POL-DT-PRESC from program LCCS0025<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PolDtPresc extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public PolDtPresc() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.POL_DT_PRESC;
	}

	public void setPolDtPresc(int polDtPresc) {
		writeIntAsPacked(Pos.POL_DT_PRESC, polDtPresc, Len.Int.POL_DT_PRESC);
	}

	public void setPolDtPrescFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.POL_DT_PRESC, Pos.POL_DT_PRESC);
	}

	/**Original name: POL-DT-PRESC<br>*/
	public int getPolDtPresc() {
		return readPackedAsInt(Pos.POL_DT_PRESC, Len.Int.POL_DT_PRESC);
	}

	public String getPolDtPrescFormatted() {
		return PicFormatter.display(new PicParams("S9(8)V").setUsage(PicUsage.PACKED)).format(getPolDtPresc()).toString();
	}

	public byte[] getPolDtPrescAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.POL_DT_PRESC, Pos.POL_DT_PRESC);
		return buffer;
	}

	public void setPolDtPrescNull(String polDtPrescNull) {
		writeString(Pos.POL_DT_PRESC_NULL, polDtPrescNull, Len.POL_DT_PRESC_NULL);
	}

	/**Original name: POL-DT-PRESC-NULL<br>*/
	public String getPolDtPrescNull() {
		return readString(Pos.POL_DT_PRESC_NULL, Len.POL_DT_PRESC_NULL);
	}

	public String getPolDtPrescNullFormatted() {
		return Functions.padBlanks(getPolDtPrescNull(), Len.POL_DT_PRESC_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int POL_DT_PRESC = 1;
		public static final int POL_DT_PRESC_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_DT_PRESC = 5;
		public static final int POL_DT_PRESC_NULL = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int POL_DT_PRESC = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

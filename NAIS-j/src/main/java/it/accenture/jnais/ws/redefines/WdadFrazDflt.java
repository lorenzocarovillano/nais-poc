/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WDAD-FRAZ-DFLT<br>
 * Variable: WDAD-FRAZ-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdadFrazDflt extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WdadFrazDflt() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WDAD_FRAZ_DFLT;
	}

	public void setWdadFrazDflt(int wdadFrazDflt) {
		writeIntAsPacked(Pos.WDAD_FRAZ_DFLT, wdadFrazDflt, Len.Int.WDAD_FRAZ_DFLT);
	}

	public void setWdadFrazDfltFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.WDAD_FRAZ_DFLT, Pos.WDAD_FRAZ_DFLT);
	}

	/**Original name: WDAD-FRAZ-DFLT<br>*/
	public int getWdadFrazDflt() {
		return readPackedAsInt(Pos.WDAD_FRAZ_DFLT, Len.Int.WDAD_FRAZ_DFLT);
	}

	public byte[] getWdadFrazDfltAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WDAD_FRAZ_DFLT, Pos.WDAD_FRAZ_DFLT);
		return buffer;
	}

	public void setWdadFrazDfltNull(String wdadFrazDfltNull) {
		writeString(Pos.WDAD_FRAZ_DFLT_NULL, wdadFrazDfltNull, Len.WDAD_FRAZ_DFLT_NULL);
	}

	/**Original name: WDAD-FRAZ-DFLT-NULL<br>*/
	public String getWdadFrazDfltNull() {
		return readString(Pos.WDAD_FRAZ_DFLT_NULL, Len.WDAD_FRAZ_DFLT_NULL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WDAD_FRAZ_DFLT = 1;
		public static final int WDAD_FRAZ_DFLT_NULL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WDAD_FRAZ_DFLT = 3;
		public static final int WDAD_FRAZ_DFLT_NULL = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WDAD_FRAZ_DFLT = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}

/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais.ws;

/**Original name: COSTANTI<br>
 * Variable: COSTANTI from program IEAS9900<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Costanti {

	//==== PROPERTIES ====
	//Original name: WN-ACCESSI
	private short wnAccessi = ((short) 13);

	//==== METHODS ====
	public short getWnAccessi() {
		return this.wnAccessi;
	}
}

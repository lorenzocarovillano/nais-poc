/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;

import it.accenture.jnais.commons.data.dao.AdesDao;
import it.accenture.jnais.commons.data.dao.AdesPoliStatOggBusDao;
import it.accenture.jnais.commons.data.dao.AdesPoliStatOggBusTrchDiGarDao;
import it.accenture.jnais.commons.data.to.IAdes;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.Ades;
import it.accenture.jnais.ws.Iabv0002;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbm0250Data;
import it.accenture.jnais.ws.Llbv0269;
import it.accenture.jnais.ws.PoliIdbspol0;
import it.accenture.jnais.ws.StatOggBusIdbsstb0;
import it.accenture.jnais.ws.enums.Idsv0003TipologiaOperazione;
import it.accenture.jnais.ws.redefines.AdeCumCnbtCap;
import it.accenture.jnais.ws.redefines.AdeDtDecor;
import it.accenture.jnais.ws.redefines.AdeDtDecorPrestBan;
import it.accenture.jnais.ws.redefines.AdeDtEffVarzStatT;
import it.accenture.jnais.ws.redefines.AdeDtNovaRgmFisc;
import it.accenture.jnais.ws.redefines.AdeDtPresc;
import it.accenture.jnais.ws.redefines.AdeDtScad;
import it.accenture.jnais.ws.redefines.AdeDtUltConsCnbt;
import it.accenture.jnais.ws.redefines.AdeDtVarzTpIas;
import it.accenture.jnais.ws.redefines.AdeDurAa;
import it.accenture.jnais.ws.redefines.AdeDurGg;
import it.accenture.jnais.ws.redefines.AdeDurMm;
import it.accenture.jnais.ws.redefines.AdeEtaAScad;
import it.accenture.jnais.ws.redefines.AdeIdMoviChiu;
import it.accenture.jnais.ws.redefines.AdeImpAder;
import it.accenture.jnais.ws.redefines.AdeImpAz;
import it.accenture.jnais.ws.redefines.AdeImpGarCnbt;
import it.accenture.jnais.ws.redefines.AdeImpRecRitAcc;
import it.accenture.jnais.ws.redefines.AdeImpRecRitVis;
import it.accenture.jnais.ws.redefines.AdeImpTfr;
import it.accenture.jnais.ws.redefines.AdeImpVolo;
import it.accenture.jnais.ws.redefines.AdeImpbVisDaRec;
import it.accenture.jnais.ws.redefines.AdeNumRatPian;
import it.accenture.jnais.ws.redefines.AdePcAder;
import it.accenture.jnais.ws.redefines.AdePcAz;
import it.accenture.jnais.ws.redefines.AdePcTfr;
import it.accenture.jnais.ws.redefines.AdePcVolo;
import it.accenture.jnais.ws.redefines.AdePreLrdInd;
import it.accenture.jnais.ws.redefines.AdePreNetInd;
import it.accenture.jnais.ws.redefines.AdePrstzIniInd;
import it.accenture.jnais.ws.redefines.AdeRatLrdInd;
import it.accenture.jnais.ws.redefines.PolAaDiffProrDflt;
import it.accenture.jnais.ws.redefines.PolDir1oVers;
import it.accenture.jnais.ws.redefines.PolDirEmis;
import it.accenture.jnais.ws.redefines.PolDirQuiet;
import it.accenture.jnais.ws.redefines.PolDirVersAgg;
import it.accenture.jnais.ws.redefines.PolDtApplzConv;
import it.accenture.jnais.ws.redefines.PolDtIniVldtConv;
import it.accenture.jnais.ws.redefines.PolDtPresc;
import it.accenture.jnais.ws.redefines.PolDtProp;
import it.accenture.jnais.ws.redefines.PolDtScad;
import it.accenture.jnais.ws.redefines.PolDurAa;
import it.accenture.jnais.ws.redefines.PolDurGg;
import it.accenture.jnais.ws.redefines.PolDurMm;
import it.accenture.jnais.ws.redefines.PolIdAccComm;
import it.accenture.jnais.ws.redefines.PolIdMoviChiu;
import it.accenture.jnais.ws.redefines.PolSpeMed;
import it.accenture.jnais.ws.redefines.StbIdMoviChiu;

/**Original name: LDBM0250<br>
 * <pre>AUTHOR.        A.I.I.S
 * DATE-WRITTEN.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : SERVIZIO ESTRAZIONE OCCORRENZE              *
 *                    DA TABELLE GUIDA RICHIAMATO DAL BATCH EXEC. *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbm0250 extends Program implements IAdes {

	//==== PROPERTIES ====
	//Original name: SQLCA
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private AdesPoliStatOggBusLdbm0250 adesPoliStatOggBusLdbm0250 = new AdesPoliStatOggBusLdbm0250(this);
	private AdesPoliStatOggBusLdbm02501 adesPoliStatOggBusLdbm02501 = new AdesPoliStatOggBusLdbm02501(this);
	private AdesPoliStatOggBusLdbm02502 adesPoliStatOggBusLdbm02502 = new AdesPoliStatOggBusLdbm02502(this);
	private AdesPoliStatOggBusTrchDiGarLdbm0250 adesPoliStatOggBusTrchDiGarLdbm0250 = new AdesPoliStatOggBusTrchDiGarLdbm0250(this);
	private AdesPoliStatOggBusTrchDiGarLdbm02501 adesPoliStatOggBusTrchDiGarLdbm02501 = new AdesPoliStatOggBusTrchDiGarLdbm02501(this);
	private AdesPoliStatOggBusTrchDiGarLdbm02502 adesPoliStatOggBusTrchDiGarLdbm02502 = new AdesPoliStatOggBusTrchDiGarLdbm02502(this);
	private AdesPoliStatOggBusTrchDiGarLdbm02503 adesPoliStatOggBusTrchDiGarLdbm02503 = new AdesPoliStatOggBusTrchDiGarLdbm02503(this);
	private AdesPoliStatOggBusDao adesPoliStatOggBusDao = new AdesPoliStatOggBusDao(dbAccessStatus);
	private AdesDao adesDao = new AdesDao(dbAccessStatus);
	private AdesPoliStatOggBusTrchDiGarDao adesPoliStatOggBusTrchDiGarDao = new AdesPoliStatOggBusTrchDiGarDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private Ldbm0250Data ws = new Ldbm0250Data();
	//Original name: IDSV0003
	private Idsv0003 idsv0003;
	//Original name: IABV0002
	private Iabv0002 iabv0002;
	//Original name: ADES
	private Ades ades;
	//Original name: POLI
	private PoliIdbspol0 poli;
	//Original name: STAT-OGG-BUS
	private StatOggBusIdbsstb0 statOggBus;
	//Original name: LLBV0269
	private Llbv0269 llbv0269;

	//==== METHODS ====
	/**Original name: PROGRAM_LDBM0250_FIRST_SENTENCES<br>*/
	public long execute(Idsv0003 idsv0003, Iabv0002 iabv0002, Ades ades, PoliIdbspol0 poli, StatOggBusIdbsstb0 statOggBus, Llbv0269 llbv0269) {
		this.idsv0003 = idsv0003;
		this.iabv0002 = iabv0002;
		this.ades = ades;
		this.poli = poli;
		this.statOggBus = statOggBus;
		this.llbv0269 = llbv0269;
		// COB_CODE: PERFORM A000-INIZIO                  THRU A000-EX.
		a000Inizio();
		// COB_CODE: PERFORM A025-CNTL-INPUT              THRU A025-EX.
		a025CntlInput();
		// COB_CODE: PERFORM A040-CARICA-WHERE-CONDITION  THRU A040-EX.
		a040CaricaWhereCondition();
		// COB_CODE: PERFORM A300-ELABORA                 THRU A300-EX
		a300Elabora();
		// COB_CODE: PERFORM A350-CTRL-COMMIT             THRU A350-EX.
		a350CtrlCommit();
		// COB_CODE: PERFORM A400-FINE                    THRU A400-EX.
		a400Fine();
		// COB_CODE: GOBACK.
		//last return statement was skipped
		return 0;
	}

	public static Ldbm0250 getInstance() {
		return (Programs.getInstance(Ldbm0250.class));
	}

	/**Original name: A000-INIZIO<br>
	 * <pre>*****************************************************************</pre>*/
	private void a000Inizio() {
		// COB_CODE: MOVE 'LDBM0250'              TO   IDSV0003-COD-SERVIZIO-BE.
		idsv0003.getCampiEsito().setCodServizioBe("LDBM0250");
		// COB_CODE: MOVE 'ADES'                  TO   IDSV0003-NOME-TABELLA.
		idsv0003.getCampiEsito().setNomeTabella("ADES");
		// COB_CODE: MOVE '00'                    TO   IDSV0003-RETURN-CODE.
		idsv0003.getReturnCode().setReturnCode("00");
		// COB_CODE: MOVE ZEROES                  TO   IDSV0003-SQLCODE
		//                                             IDSV0003-NUM-RIGHE-LETTE.
		idsv0003.getSqlcode().setSqlcode(0);
		idsv0003.getCampiEsito().setNumRigheLette(((short) 0));
		// COB_CODE: MOVE SPACES                  TO   IDSV0003-DESCRIZ-ERR-DB2
		//                                             IDSV0003-KEY-TABELLA.
		idsv0003.getCampiEsito().setDescrizErrDb2("");
		idsv0003.getCampiEsito().setKeyTabella("");
		// COB_CODE: SET ACCESSO-X-RANGE-NO       TO   TRUE.
		ws.getFlagAccessoXRange().setNo();
		// COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
		a001TrattaDateTimestamp();
	}

	/**Original name: A025-CNTL-INPUT<br>
	 * <pre>*****************************************************************</pre>*/
	private void a025CntlInput() {
		// COB_CODE: IF IABV0009-ID-OGG-DA IS NUMERIC AND
		//              IABV0009-ID-OGG-DA NOT = ZEROES
		//              END-IF
		//           END-IF.
		if (Functions.isNumber(iabv0002.getIabv0009GestGuideService().getIdOggDa()) && iabv0002.getIabv0009GestGuideService().getIdOggDa() != 0) {
			// COB_CODE: IF IABV0009-ID-OGG-A  IS NUMERIC AND
			//              IABV0009-ID-OGG-A  NOT = ZEROES
			//              SET ACCESSO-X-RANGE-SI    TO TRUE
			//           END-IF
			if (Functions.isNumber(iabv0002.getIabv0009GestGuideService().getIdOggA()) && iabv0002.getIabv0009GestGuideService().getIdOggA() != 0) {
				// COB_CODE: SET ACCESSO-X-RANGE-SI    TO TRUE
				ws.getFlagAccessoXRange().setSi();
			}
		}
	}

	/**Original name: A040-CARICA-WHERE-CONDITION<br>
	 * <pre>*****************************************************************</pre>*/
	private void a040CaricaWhereCondition() {
		// COB_CODE: MOVE IABV0009-BLOB-DATA-REC        TO WLB-REC-PREN
		ws.getWlbRecPren().setWlbRecPrenFormatted(iabv0002.getIabv0009GestGuideService().getIabv0009BlobDataRecFormatted());
		// COB_CODE: INITIALIZE WS-DT-PTF-X
		//                      WS-DT-TS-PTF
		//                      WS-DT-TS-PTF-PRE.
		ws.setWsDtPtfX("");
		ws.setWsDtTsPtf(0);
		ws.setWsDtTsPtfPre(0);
		//    LA DATA EFFETTO VIENE VALORIZZATA CON LA DATA PRODUZIONE
		// COB_CODE: MOVE WLB-DT-PRODUZIONE
		//             TO WS-DT-N.
		ws.getWsDtN().setWsDtNFormatted(ws.getWlbRecPren().getDtProduzioneFormatted());
		// COB_CODE: MOVE WS-DT-N-AA
		//             TO WS-DT-X-AA.
		ws.getWsDtX().setAaFormatted(ws.getWsDtN().getAaFormatted());
		// COB_CODE: MOVE WS-DT-N-MM
		//             TO WS-DT-X-MM.
		ws.getWsDtX().setMmFormatted(ws.getWsDtN().getMmFormatted());
		// COB_CODE: MOVE WS-DT-N-GG
		//             TO WS-DT-X-GG.
		ws.getWsDtX().setGgFormatted(ws.getWsDtN().getGgFormatted());
		// COB_CODE: MOVE WS-DT-X
		//             TO WS-DT-PTF-X.
		ws.setWsDtPtfX(ws.getWsDtX().getWsDtXFormatted());
		// COB_CODE: MOVE WLB-TS-COMPETENZA
		//             TO WS-DT-TS-PTF.
		ws.setWsDtTsPtf(ws.getWlbRecPren().getTsCompetenza());
		// COB_CODE: MOVE LLBV0269-TS-PRECED
		//             TO WS-DT-TS-PTF-PRE.
		ws.setWsDtTsPtfPre(llbv0269.getTsPreced());
		//    DISPLAY PER VERIFICA CORRETTEZZA DATE
		//    DISPLAY 'DATA RISERVA =                   ' WLB-DT-RISERVA
		//    DISPLAY 'DATA PRODUZIONE =                ' WLB-DT-PRODUZIONE
		//    DISPLAY 'DATA EFFETTO (DATA PRODUZIONE)=  ' WS-DT-PTF-X
		//    DISPLAY 'TIMESTAMP COMPETENZA =           ' WS-DT-TS-PTF
		//    DISPLAY 'TIMESTAMP COMPETENZA PRECEDENTE = '
		//                                                WS-DT-TS-PTF-PRE
		// COB_CODE: IF  WLB-TP-FRM-ASSVA EQUAL 'EN'
		//               MOVE 'CO'                      TO WS-TP-FRM-ASSVA2
		//           ELSE
		//               MOVE SPACES                    TO WS-TP-FRM-ASSVA2
		//           END-IF.
		if (Conditions.eq(ws.getWlbRecPren().getTpFrmAssva(), "EN")) {
			// COB_CODE: MOVE 'IN'                      TO WS-TP-FRM-ASSVA1
			ws.setWsTpFrmAssva1("IN");
			// COB_CODE: MOVE 'CO'                      TO WS-TP-FRM-ASSVA2
			ws.setWsTpFrmAssva2("CO");
		} else {
			// COB_CODE: MOVE WLB-TP-FRM-ASSVA          TO WS-TP-FRM-ASSVA1
			ws.setWsTpFrmAssva1(ws.getWlbRecPren().getTpFrmAssva());
			// COB_CODE: MOVE SPACES                    TO WS-TP-FRM-ASSVA2
			ws.setWsTpFrmAssva2("");
		}
	}

	/**Original name: A100-CHECK-RETURN-CODE<br>
	 * <pre>*****************************************************************</pre>*/
	private void a100CheckReturnCode() {
		// COB_CODE: IF IDSV0003-SUCCESSFUL-RC
		//              END-EVALUATE
		//           END-IF.
		if (idsv0003.getReturnCode().isSuccessfulRc()) {
			// COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
			idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
			// COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
			idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
			// COB_CODE: EVALUATE IDSV0003-SQLCODE
			//               WHEN ZERO
			//                             CONTINUE
			//               WHEN +100
			//                  END-IF
			//               WHEN OTHER
			//                             SET IDSV0003-SQL-ERROR TO TRUE
			//           END-EVALUATE
			if (idsv0003.getSqlcode().getSqlcode() == 0) {
				// COB_CODE: CONTINUE
				//continue
			} else if (idsv0003.getSqlcode().getSqlcode() == 100) {
				// COB_CODE: IF IDSV0003-SELECT                OR
				//              IDSV0003-FETCH-FIRST           OR
				//              IDSV0003-FETCH-NEXT
				//                      CONTINUE
				//           ELSE
				//                      SET IDSV0003-SQL-ERROR TO TRUE
				//           END-IF
				if (idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext()) {
					// COB_CODE: CONTINUE
					//continue
				} else {
					// COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
					idsv0003.getReturnCode().setSqlError();
				}
			} else {
				// COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
				idsv0003.getReturnCode().setSqlError();
			}
		}
	}

	/**Original name: A300-ELABORA<br>
	 * <pre>*****************************************************************</pre>*/
	private void a300Elabora() {
		// COB_CODE:      EVALUATE TRUE
		//                   WHEN IDSV0003-WHERE-CONDITION-01
		//           * Estrazione massiva (parametrizzata per tp_forma_ass)
		//                        PERFORM SC01-SELECTION-CURSOR-01 THRU SC01-EX
		//           * Estrazione per Prodotto
		//                   WHEN IDSV0003-WHERE-CONDITION-02
		//                        PERFORM SC02-SELECTION-CURSOR-02 THRU SC02-EX
		//           * Estrazione per Range di polizze
		//                   WHEN IDSV0003-WHERE-CONDITION-03
		//                        PERFORM SC03-SELECTION-CURSOR-03 THRU SC03-EX
		//           * Estrazione per collettiva\range di adesioni
		//                   WHEN IDSV0003-WHERE-CONDITION-04
		//                        PERFORM SC04-SELECTION-CURSOR-04 THRU SC04-EX
		//           * Aggiornamento estrazione massiva
		//                   WHEN IDSV0003-WHERE-CONDITION-05
		//                        PERFORM SC05-SELECTION-CURSOR-05 THRU SC05-EX
		//           * Aggiornamento estrazione massiva per Prodotto
		//                   WHEN IDSV0003-WHERE-CONDITION-06
		//                        PERFORM SC06-SELECTION-CURSOR-06 THRU SC06-EX
		//           * Aggiornamento estrazione massiva per Range di polizze
		//                   WHEN IDSV0003-WHERE-CONDITION-07
		//                        PERFORM SC07-SELECTION-CURSOR-07 THRU SC07-EX
		//           * Aggiornamento estrazione massiva per Range di polizze\adesioni
		//                   WHEN IDSV0003-WHERE-CONDITION-08
		//                        PERFORM SC08-SELECTION-CURSOR-08 THRU SC08-EX
		//           *
		//                   WHEN IDSV0003-WHERE-CONDITION-09
		//                        SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
		//           *
		//                   WHEN IDSV0003-WHERE-CONDITION-10
		//                        SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
		//                   WHEN OTHER
		//                        SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
		//                END-EVALUATE.
		switch (idsv0003.getTipologiaOperazione().getTipologiaOperazione()) {

		case Idsv0003TipologiaOperazione.CONDITION01:// Estrazione massiva (parametrizzata per tp_forma_ass)
			// COB_CODE: PERFORM SC01-SELECTION-CURSOR-01 THRU SC01-EX
			sc01SelectionCursor01();
			// Estrazione per Prodotto
			break;

		case Idsv0003TipologiaOperazione.CONDITION02:// COB_CODE: PERFORM SC02-SELECTION-CURSOR-02 THRU SC02-EX
			sc02SelectionCursor02();
			// Estrazione per Range di polizze
			break;

		case Idsv0003TipologiaOperazione.CONDITION03:// COB_CODE: PERFORM SC03-SELECTION-CURSOR-03 THRU SC03-EX
			sc03SelectionCursor03();
			// Estrazione per collettiva\\range di adesioni
			break;

		case Idsv0003TipologiaOperazione.CONDITION04:// COB_CODE: PERFORM SC04-SELECTION-CURSOR-04 THRU SC04-EX
			sc04SelectionCursor04();
			// Aggiornamento estrazione massiva
			break;

		case Idsv0003TipologiaOperazione.CONDITION05:// COB_CODE: PERFORM SC05-SELECTION-CURSOR-05 THRU SC05-EX
			sc05SelectionCursor05();
			// Aggiornamento estrazione massiva per Prodotto
			break;

		case Idsv0003TipologiaOperazione.CONDITION06:// COB_CODE: PERFORM SC06-SELECTION-CURSOR-06 THRU SC06-EX
			sc06SelectionCursor06();
			// Aggiornamento estrazione massiva per Range di polizze
			break;

		case Idsv0003TipologiaOperazione.CONDITION07:// COB_CODE: PERFORM SC07-SELECTION-CURSOR-07 THRU SC07-EX
			sc07SelectionCursor07();
			// Aggiornamento estrazione massiva per Range di polizze\\adesioni
			break;

		case Idsv0003TipologiaOperazione.CONDITION08:// COB_CODE: PERFORM SC08-SELECTION-CURSOR-08 THRU SC08-EX
			sc08SelectionCursor08();
			//
			break;

		case Idsv0003TipologiaOperazione.CONDITION09:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
			idsv0003.getReturnCode().setInvalidLevelOper();
			//
			break;

		case Idsv0003TipologiaOperazione.CONDITION10:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
			idsv0003.getReturnCode().setInvalidLevelOper();
			break;

		default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
			idsv0003.getReturnCode().setInvalidLevelOper();
			break;
		}
	}

	/**Original name: SC01-SELECTION-CURSOR-01<br>
	 * <pre>*****************************************************************</pre>*/
	private void sc01SelectionCursor01() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-SELECT
		//                 PERFORM A310-SELECT-SC01           THRU A310-SC01-EX
		//              WHEN IDSV0003-OPEN-CURSOR
		//                 PERFORM A360-OPEN-CURSOR-SC01      THRU A360-SC01-EX
		//              WHEN IDSV0003-CLOSE-CURSOR
		//                 PERFORM A370-CLOSE-CURSOR-SC01     THRU A370-SC01-EX
		//              WHEN IDSV0003-FETCH-FIRST
		//                 PERFORM A380-FETCH-FIRST-SC01      THRU A380-SC01-EX
		//              WHEN IDSV0003-FETCH-NEXT
		//                 PERFORM A390-FETCH-NEXT-SC01       THRU A390-SC01-EX
		//              WHEN IDSV0003-UPDATE
		//                 PERFORM A320-UPDATE-SC01           THRU A320-SC01-EX
		//              WHEN OTHER
		//                 SET IDSV0003-INVALID-OPER          TO TRUE
		//           END-EVALUATE.
		if (idsv0003.getOperazione().isSelect()) {
			// COB_CODE: PERFORM A310-SELECT-SC01           THRU A310-SC01-EX
			a310SelectSc01();
		} else if (idsv0003.getOperazione().isOpenCursor()) {
			// COB_CODE: PERFORM A360-OPEN-CURSOR-SC01      THRU A360-SC01-EX
			a360OpenCursorSc01();
		} else if (idsv0003.getOperazione().isCloseCursor()) {
			// COB_CODE: PERFORM A370-CLOSE-CURSOR-SC01     THRU A370-SC01-EX
			a370CloseCursorSc01();
		} else if (idsv0003.getOperazione().isFetchFirst()) {
			// COB_CODE: PERFORM A380-FETCH-FIRST-SC01      THRU A380-SC01-EX
			a380FetchFirstSc01();
		} else if (idsv0003.getOperazione().isFetchNext()) {
			// COB_CODE: PERFORM A390-FETCH-NEXT-SC01       THRU A390-SC01-EX
			a390FetchNextSc01();
		} else if (idsv0003.getOperazione().isUpdate()) {
			// COB_CODE: PERFORM A320-UPDATE-SC01           THRU A320-SC01-EX
			a320UpdateSc01();
		} else {
			// COB_CODE: SET IDSV0003-INVALID-OPER          TO TRUE
			idsv0003.getReturnCode().setInvalidOper();
		}
	}

	/**Original name: A305-DECLARE-CURSOR-SC01<br>
	 * <pre>*****************************************************************</pre>*/
	private void a305DeclareCursorSc01() {
		// COB_CODE: IF ACCESSO-X-RANGE-SI
		//              CONTINUE
		//           ELSE
		//              CONTINUE
		//           END-IF.
		if (ws.getFlagAccessoXRange().isSi()) {
			// COB_CODE:         EXEC SQL
			//                        DECLARE CUR-SC01-RANGE CURSOR WITH HOLD FOR
			//                     SELECT
			//                         A.ID_ADES
			//                        ,A.ID_POLI
			//                        ,A.ID_MOVI_CRZ
			//                        ,A.ID_MOVI_CHIU
			//                        ,A.DT_INI_EFF
			//                        ,A.DT_END_EFF
			//                        ,A.IB_PREV
			//                        ,A.IB_OGG
			//                        ,A.COD_COMP_ANIA
			//                        ,A.DT_DECOR
			//                        ,A.DT_SCAD
			//                        ,A.ETA_A_SCAD
			//                        ,A.DUR_AA
			//                        ,A.DUR_MM
			//                        ,A.DUR_GG
			//                        ,A.TP_RGM_FISC
			//                        ,A.TP_RIAT
			//                        ,A.TP_MOD_PAG_TIT
			//                        ,A.TP_IAS
			//                        ,A.DT_VARZ_TP_IAS
			//                        ,A.PRE_NET_IND
			//                        ,A.PRE_LRD_IND
			//                        ,A.RAT_LRD_IND
			//                        ,A.PRSTZ_INI_IND
			//                        ,A.FL_COINC_ASSTO
			//                        ,A.IB_DFLT
			//                        ,A.MOD_CALC
			//                        ,A.TP_FNT_CNBTVA
			//                        ,A.IMP_AZ
			//                        ,A.IMP_ADER
			//                        ,A.IMP_TFR
			//                        ,A.IMP_VOLO
			//                        ,A.PC_AZ
			//                        ,A.PC_ADER
			//                        ,A.PC_TFR
			//                        ,A.PC_VOLO
			//                        ,A.DT_NOVA_RGM_FISC
			//                        ,A.FL_ATTIV
			//                        ,A.IMP_REC_RIT_VIS
			//                        ,A.IMP_REC_RIT_ACC
			//                        ,A.FL_VARZ_STAT_TBGC
			//                        ,A.FL_PROVZA_MIGRAZ
			//                        ,A.IMPB_VIS_DA_REC
			//                        ,A.DT_DECOR_PREST_BAN
			//                        ,A.DT_EFF_VARZ_STAT_T
			//                        ,A.DS_RIGA
			//                        ,A.DS_OPER_SQL
			//                        ,A.DS_VER
			//                        ,A.DS_TS_INI_CPTZ
			//                        ,A.DS_TS_END_CPTZ
			//                        ,A.DS_UTENTE
			//                        ,A.DS_STATO_ELAB
			//                        ,A.CUM_CNBT_CAP
			//                        ,A.IMP_GAR_CNBT
			//                        ,A.DT_ULT_CONS_CNBT
			//                        ,A.IDEN_ISC_FND
			//                        ,A.NUM_RAT_PIAN
			//                        ,A.DT_PRESC
			//                        ,A.CONCS_PREST
			//                        ,B.ID_POLI
			//                        ,B.ID_MOVI_CRZ
			//                        ,B.ID_MOVI_CHIU
			//                        ,B.IB_OGG
			//                        ,B.IB_PROP
			//                        ,B.DT_PROP
			//                        ,B.DT_INI_EFF
			//                        ,B.DT_END_EFF
			//                        ,B.COD_COMP_ANIA
			//                        ,B.DT_DECOR
			//                        ,B.DT_EMIS
			//                        ,B.TP_POLI
			//                        ,B.DUR_AA
			//                        ,B.DUR_MM
			//                        ,B.DT_SCAD
			//                        ,B.COD_PROD
			//                        ,B.DT_INI_VLDT_PROD
			//                        ,B.COD_CONV
			//                        ,B.COD_RAMO
			//                        ,B.DT_INI_VLDT_CONV
			//                        ,B.DT_APPLZ_CONV
			//                        ,B.TP_FRM_ASSVA
			//                        ,B.TP_RGM_FISC
			//                        ,B.FL_ESTAS
			//                        ,B.FL_RSH_COMUN
			//                        ,B.FL_RSH_COMUN_COND
			//                        ,B.TP_LIV_GENZ_TIT
			//                        ,B.FL_COP_FINANZ
			//                        ,B.TP_APPLZ_DIR
			//                        ,B.SPE_MED
			//                        ,B.DIR_EMIS
			//                        ,B.DIR_1O_VERS
			//                        ,B.DIR_VERS_AGG
			//                        ,B.COD_DVS
			//                        ,B.FL_FNT_AZ
			//                        ,B.FL_FNT_ADER
			//                        ,B.FL_FNT_TFR
			//                        ,B.FL_FNT_VOLO
			//                        ,B.TP_OPZ_A_SCAD
			//                        ,B.AA_DIFF_PROR_DFLT
			//                        ,B.FL_VER_PROD
			//                        ,B.DUR_GG
			//                        ,B.DIR_QUIET
			//                        ,B.TP_PTF_ESTNO
			//                        ,B.FL_CUM_PRE_CNTR
			//                        ,B.FL_AMMB_MOVI
			//                        ,B.CONV_GECO
			//                        ,B.DS_RIGA
			//                        ,B.DS_OPER_SQL
			//                        ,B.DS_VER
			//                        ,B.DS_TS_INI_CPTZ
			//                        ,B.DS_TS_END_CPTZ
			//                        ,B.DS_UTENTE
			//                        ,B.DS_STATO_ELAB
			//                        ,B.FL_SCUDO_FISC
			//                        ,B.FL_TRASFE
			//                        ,B.FL_TFR_STRC
			//                        ,B.DT_PRESC
			//                        ,B.COD_CONV_AGG
			//                        ,B.SUBCAT_PROD
			//                        ,B.FL_QUEST_ADEGZ_ASS
			//                        ,B.COD_TPA
			//                        ,B.ID_ACC_COMM
			//                        ,B.FL_POLI_CPI_PR
			//                        ,B.FL_POLI_BUNDLING
			//                        ,B.IND_POLI_PRIN_COLL
			//                        ,B.FL_VND_BUNDLE
			//                        ,B.IB_BS
			//                        ,B.FL_POLI_IFP
			//                        ,C.ID_STAT_OGG_BUS
			//                        ,C.ID_OGG
			//                        ,C.TP_OGG
			//                        ,C.ID_MOVI_CRZ
			//                        ,C.ID_MOVI_CHIU
			//                        ,C.DT_INI_EFF
			//                        ,C.DT_END_EFF
			//                        ,C.COD_COMP_ANIA
			//                        ,C.TP_STAT_BUS
			//                        ,C.TP_CAUS
			//                        ,C.DS_RIGA
			//                        ,C.DS_OPER_SQL
			//                        ,C.DS_VER
			//                        ,C.DS_TS_INI_CPTZ
			//                        ,C.DS_TS_END_CPTZ
			//                        ,C.DS_UTENTE
			//                        ,C.DS_STATO_ELAB
			//                     FROM ADES         A,
			//                          POLI         B,
			//                          STAT_OGG_BUS C
			//                     WHERE B.TP_FRM_ASSVA   IN (:WS-TP-FRM-ASSVA1,
			//                                                :WS-TP-FRM-ASSVA2)
			//                       AND A.ID_POLI         =   B.ID_POLI
			//                       AND A.ID_ADES         =   C.ID_OGG
			//                       AND C.TP_OGG          =  'AD'
			//                       AND C.TP_STAT_BUS    =  'VI'
			//           *           OR
			//           *               (C.TP_STAT_BUS    =  'ST'  AND
			//           *               EXISTS (SELECT
			//           *                     D.ID_MOVI_FINRIO
			//           *
			//           *                  FROM  MOVI_FINRIO D
			//           *                  WHERE A.ID_ADES = D.ID_ADES
			//           *                    AND D.STAT_MOVI IN ('AL','IE')
			//           *                    AND D.TP_MOVI_FINRIO = 'DI'
			//           *                    AND D.COD_COMP_ANIA   =
			//           *                        :IDSV0003-CODICE-COMPAGNIA-ANIA
			//           *                    AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
			//           *                    AND D.DT_END_EFF     >   :WS-DT-PTF-X
			//           *                    AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
			//           *                    AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
			//                     AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
			//                     AND A.ID_POLI        BETWEEN   :IABV0009-ID-OGG-DA AND
			//                                                    :IABV0009-ID-OGG-A
			//                     AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
			//                     AND A.DT_END_EFF     >   :WS-DT-PTF-X
			//                     AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
			//                     AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
			//                     AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
			//                     AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
			//                     AND B.DT_END_EFF     >   :WS-DT-PTF-X
			//                     AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
			//                     AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
			//                     AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
			//                     AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
			//                     AND C.DT_END_EFF     >   :WS-DT-PTF-X
			//                     AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
			//                     AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
			//           *         AND A.DS_STATO_ELAB IN (
			//           *                                 :IABV0002-STATE-01,
			//           *                                 :IABV0002-STATE-02,
			//           *                                 :IABV0002-STATE-03,
			//           *                                 :IABV0002-STATE-04,
			//           *                                 :IABV0002-STATE-05,
			//           *                                 :IABV0002-STATE-06,
			//           *                                 :IABV0002-STATE-07,
			//           *                                 :IABV0002-STATE-08,
			//           *                                 :IABV0002-STATE-09,
			//           *                                 :IABV0002-STATE-10
			//           *                                     )
			//           *           AND NOT A.DS_VER     = :IABV0009-VERSIONING
			//                       ORDER BY A.ID_POLI, A.ID_ADES
			//                   END-EXEC
			// DECLARE CURSOR doesn't need a translation;
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE:         EXEC SQL
			//                        DECLARE CUR-SC01 CURSOR WITH HOLD FOR
			//                     SELECT
			//                         A.ID_ADES
			//                        ,A.ID_POLI
			//                        ,A.ID_MOVI_CRZ
			//                        ,A.ID_MOVI_CHIU
			//                        ,A.DT_INI_EFF
			//                        ,A.DT_END_EFF
			//                        ,A.IB_PREV
			//                        ,A.IB_OGG
			//                        ,A.COD_COMP_ANIA
			//                        ,A.DT_DECOR
			//                        ,A.DT_SCAD
			//                        ,A.ETA_A_SCAD
			//                        ,A.DUR_AA
			//                        ,A.DUR_MM
			//                        ,A.DUR_GG
			//                        ,A.TP_RGM_FISC
			//                        ,A.TP_RIAT
			//                        ,A.TP_MOD_PAG_TIT
			//                        ,A.TP_IAS
			//                        ,A.DT_VARZ_TP_IAS
			//                        ,A.PRE_NET_IND
			//                        ,A.PRE_LRD_IND
			//                        ,A.RAT_LRD_IND
			//                        ,A.PRSTZ_INI_IND
			//                        ,A.FL_COINC_ASSTO
			//                        ,A.IB_DFLT
			//                        ,A.MOD_CALC
			//                        ,A.TP_FNT_CNBTVA
			//                        ,A.IMP_AZ
			//                        ,A.IMP_ADER
			//                        ,A.IMP_TFR
			//                        ,A.IMP_VOLO
			//                        ,A.PC_AZ
			//                        ,A.PC_ADER
			//                        ,A.PC_TFR
			//                        ,A.PC_VOLO
			//                        ,A.DT_NOVA_RGM_FISC
			//                        ,A.FL_ATTIV
			//                        ,A.IMP_REC_RIT_VIS
			//                        ,A.IMP_REC_RIT_ACC
			//                        ,A.FL_VARZ_STAT_TBGC
			//                        ,A.FL_PROVZA_MIGRAZ
			//                        ,A.IMPB_VIS_DA_REC
			//                        ,A.DT_DECOR_PREST_BAN
			//                        ,A.DT_EFF_VARZ_STAT_T
			//                        ,A.DS_RIGA
			//                        ,A.DS_OPER_SQL
			//                        ,A.DS_VER
			//                        ,A.DS_TS_INI_CPTZ
			//                        ,A.DS_TS_END_CPTZ
			//                        ,A.DS_UTENTE
			//                        ,A.DS_STATO_ELAB
			//                        ,A.CUM_CNBT_CAP
			//                        ,A.IMP_GAR_CNBT
			//                        ,A.DT_ULT_CONS_CNBT
			//                        ,A.IDEN_ISC_FND
			//                        ,A.NUM_RAT_PIAN
			//                        ,A.DT_PRESC
			//                        ,A.CONCS_PREST
			//                        ,B.ID_POLI
			//                        ,B.ID_MOVI_CRZ
			//                        ,B.ID_MOVI_CHIU
			//                        ,B.IB_OGG
			//                        ,B.IB_PROP
			//                        ,B.DT_PROP
			//                        ,B.DT_INI_EFF
			//                        ,B.DT_END_EFF
			//                        ,B.COD_COMP_ANIA
			//                        ,B.DT_DECOR
			//                        ,B.DT_EMIS
			//                        ,B.TP_POLI
			//                        ,B.DUR_AA
			//                        ,B.DUR_MM
			//                        ,B.DT_SCAD
			//                        ,B.COD_PROD
			//                        ,B.DT_INI_VLDT_PROD
			//                        ,B.COD_CONV
			//                        ,B.COD_RAMO
			//                        ,B.DT_INI_VLDT_CONV
			//                        ,B.DT_APPLZ_CONV
			//                        ,B.TP_FRM_ASSVA
			//                        ,B.TP_RGM_FISC
			//                        ,B.FL_ESTAS
			//                        ,B.FL_RSH_COMUN
			//                        ,B.FL_RSH_COMUN_COND
			//                        ,B.TP_LIV_GENZ_TIT
			//                        ,B.FL_COP_FINANZ
			//                        ,B.TP_APPLZ_DIR
			//                        ,B.SPE_MED
			//                        ,B.DIR_EMIS
			//                        ,B.DIR_1O_VERS
			//                        ,B.DIR_VERS_AGG
			//                        ,B.COD_DVS
			//                        ,B.FL_FNT_AZ
			//                        ,B.FL_FNT_ADER
			//                        ,B.FL_FNT_TFR
			//                        ,B.FL_FNT_VOLO
			//                        ,B.TP_OPZ_A_SCAD
			//                        ,B.AA_DIFF_PROR_DFLT
			//                        ,B.FL_VER_PROD
			//                        ,B.DUR_GG
			//                        ,B.DIR_QUIET
			//                        ,B.TP_PTF_ESTNO
			//                        ,B.FL_CUM_PRE_CNTR
			//                        ,B.FL_AMMB_MOVI
			//                        ,B.CONV_GECO
			//                        ,B.DS_RIGA
			//                        ,B.DS_OPER_SQL
			//                        ,B.DS_VER
			//                        ,B.DS_TS_INI_CPTZ
			//                        ,B.DS_TS_END_CPTZ
			//                        ,B.DS_UTENTE
			//                        ,B.DS_STATO_ELAB
			//                        ,B.FL_SCUDO_FISC
			//                        ,B.FL_TRASFE
			//                        ,B.FL_TFR_STRC
			//                        ,B.DT_PRESC
			//                        ,B.COD_CONV_AGG
			//                        ,B.SUBCAT_PROD
			//                        ,B.FL_QUEST_ADEGZ_ASS
			//                        ,B.COD_TPA
			//                        ,B.ID_ACC_COMM
			//                        ,B.FL_POLI_CPI_PR
			//                        ,B.FL_POLI_BUNDLING
			//                        ,B.IND_POLI_PRIN_COLL
			//                        ,B.FL_VND_BUNDLE
			//                        ,B.IB_BS
			//                        ,B.FL_POLI_IFP
			//                        ,C.ID_STAT_OGG_BUS
			//                        ,C.ID_OGG
			//                        ,C.TP_OGG
			//                        ,C.ID_MOVI_CRZ
			//                        ,C.ID_MOVI_CHIU
			//                        ,C.DT_INI_EFF
			//                        ,C.DT_END_EFF
			//                        ,C.COD_COMP_ANIA
			//                        ,C.TP_STAT_BUS
			//                        ,C.TP_CAUS
			//                        ,C.DS_RIGA
			//                        ,C.DS_OPER_SQL
			//                        ,C.DS_VER
			//                        ,C.DS_TS_INI_CPTZ
			//                        ,C.DS_TS_END_CPTZ
			//                        ,C.DS_UTENTE
			//                        ,C.DS_STATO_ELAB
			//                     FROM ADES         A,
			//                          POLI         B,
			//                          STAT_OGG_BUS C
			//                     WHERE B.TP_FRM_ASSVA   IN (:WS-TP-FRM-ASSVA1,
			//                                                :WS-TP-FRM-ASSVA2)
			//                       AND A.ID_POLI         =   B.ID_POLI
			//                       AND A.ID_ADES         =   C.ID_OGG
			//                       AND C.TP_OGG          =  'AD'
			//                       AND C.TP_STAT_BUS    =  'VI'
			//           *           OR
			//           *               (C.TP_STAT_BUS    =  'ST'  AND
			//           *               EXISTS (SELECT
			//           *                     D.ID_MOVI_FINRIO
			//           *
			//           *                  FROM  MOVI_FINRIO D
			//           *                  WHERE A.ID_ADES = D.ID_ADES
			//           *                    AND D.STAT_MOVI IN ('AL','IE')
			//           *                    AND D.TP_MOVI_FINRIO = 'DI'
			//           *                    AND D.COD_COMP_ANIA   =
			//           *                        :IDSV0003-CODICE-COMPAGNIA-ANIA
			//           *                    AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
			//           *                    AND D.DT_END_EFF     >   :WS-DT-PTF-X
			//           *                    AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
			//           *                    AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
			//                     AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
			//                     AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
			//                     AND A.DT_END_EFF     >   :WS-DT-PTF-X
			//                     AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
			//                     AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
			//                     AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
			//                     AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
			//                     AND B.DT_END_EFF     >   :WS-DT-PTF-X
			//                     AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
			//                     AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
			//                     AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
			//                     AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
			//                     AND C.DT_END_EFF     >   :WS-DT-PTF-X
			//                     AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
			//                     AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
			//           *         AND A.DS_STATO_ELAB IN (
			//           *                                 :IABV0002-STATE-01,
			//           *                                 :IABV0002-STATE-02,
			//           *                                 :IABV0002-STATE-03,
			//           *                                 :IABV0002-STATE-04,
			//           *                                 :IABV0002-STATE-05,
			//           *                                 :IABV0002-STATE-06,
			//           *                                 :IABV0002-STATE-07,
			//           *                                 :IABV0002-STATE-08,
			//           *                                 :IABV0002-STATE-09,
			//           *                                 :IABV0002-STATE-10
			//           *                                     )
			//           *           AND NOT A.DS_VER     = :IABV0009-VERSIONING
			//                       ORDER BY A.ID_POLI, A.ID_ADES
			//                   END-EXEC
			// DECLARE CURSOR doesn't need a translation;
			// COB_CODE: CONTINUE
			//continue
		}
	}

	/**Original name: A310-SELECT-SC01<br>
	 * <pre>*****************************************************************</pre>*/
	private void a310SelectSc01() {
		// COB_CODE: IF ACCESSO-X-RANGE-SI
		//              END-EXEC
		//           ELSE
		//              END-EXEC
		//           END-IF.
		if (ws.getFlagAccessoXRange().isSi()) {
			//         AND A.DS_STATO_ELAB IN   (
			//                                   :IABV0002-STATE-01,
			//                                   :IABV0002-STATE-02,
			//                                   :IABV0002-STATE-03,
			//                                   :IABV0002-STATE-04,
			//                                   :IABV0002-STATE-05,
			//                                   :IABV0002-STATE-06,
			//                                   :IABV0002-STATE-07,
			//                                   :IABV0002-STATE-08,
			//                                   :IABV0002-STATE-09,
			//                                   :IABV0002-STATE-10
			//                                    )
			//           AND NOT A.DS_VER     = :IABV0009-VERSIONING
			// COB_CODE:         EXEC SQL
			//                     SELECT
			//                         A.ID_ADES
			//                        ,A.ID_POLI
			//                        ,A.ID_MOVI_CRZ
			//                        ,A.ID_MOVI_CHIU
			//                        ,A.DT_INI_EFF
			//                        ,A.DT_END_EFF
			//                        ,A.IB_PREV
			//                        ,A.IB_OGG
			//                        ,A.COD_COMP_ANIA
			//                        ,A.DT_DECOR
			//                        ,A.DT_SCAD
			//                        ,A.ETA_A_SCAD
			//                        ,A.DUR_AA
			//                        ,A.DUR_MM
			//                        ,A.DUR_GG
			//                        ,A.TP_RGM_FISC
			//                        ,A.TP_RIAT
			//                        ,A.TP_MOD_PAG_TIT
			//                        ,A.TP_IAS
			//                        ,A.DT_VARZ_TP_IAS
			//                        ,A.PRE_NET_IND
			//                        ,A.PRE_LRD_IND
			//                        ,A.RAT_LRD_IND
			//                        ,A.PRSTZ_INI_IND
			//                        ,A.FL_COINC_ASSTO
			//                        ,A.IB_DFLT
			//                        ,A.MOD_CALC
			//                        ,A.TP_FNT_CNBTVA
			//                        ,A.IMP_AZ
			//                        ,A.IMP_ADER
			//                        ,A.IMP_TFR
			//                        ,A.IMP_VOLO
			//                        ,A.PC_AZ
			//                        ,A.PC_ADER
			//                        ,A.PC_TFR
			//                        ,A.PC_VOLO
			//                        ,A.DT_NOVA_RGM_FISC
			//                        ,A.FL_ATTIV
			//                        ,A.IMP_REC_RIT_VIS
			//                        ,A.IMP_REC_RIT_ACC
			//                        ,A.FL_VARZ_STAT_TBGC
			//                        ,A.FL_PROVZA_MIGRAZ
			//                        ,A.IMPB_VIS_DA_REC
			//                        ,A.DT_DECOR_PREST_BAN
			//                        ,A.DT_EFF_VARZ_STAT_T
			//                        ,A.DS_RIGA
			//                        ,A.DS_OPER_SQL
			//                        ,A.DS_VER
			//                        ,A.DS_TS_INI_CPTZ
			//                        ,A.DS_TS_END_CPTZ
			//                        ,A.DS_UTENTE
			//                        ,A.DS_STATO_ELAB
			//                        ,A.CUM_CNBT_CAP
			//                        ,A.IMP_GAR_CNBT
			//                        ,A.DT_ULT_CONS_CNBT
			//                        ,A.IDEN_ISC_FND
			//                        ,A.NUM_RAT_PIAN
			//                        ,A.DT_PRESC
			//                        ,A.CONCS_PREST
			//                        ,B.ID_POLI
			//                        ,B.ID_MOVI_CRZ
			//                        ,B.ID_MOVI_CHIU
			//                        ,B.IB_OGG
			//                        ,B.IB_PROP
			//                        ,B.DT_PROP
			//                        ,B.DT_INI_EFF
			//                        ,B.DT_END_EFF
			//                        ,B.COD_COMP_ANIA
			//                        ,B.DT_DECOR
			//                        ,B.DT_EMIS
			//                        ,B.TP_POLI
			//                        ,B.DUR_AA
			//                        ,B.DUR_MM
			//                        ,B.DT_SCAD
			//                        ,B.COD_PROD
			//                        ,B.DT_INI_VLDT_PROD
			//                        ,B.COD_CONV
			//                        ,B.COD_RAMO
			//                        ,B.DT_INI_VLDT_CONV
			//                        ,B.DT_APPLZ_CONV
			//                        ,B.TP_FRM_ASSVA
			//                        ,B.TP_RGM_FISC
			//                        ,B.FL_ESTAS
			//                        ,B.FL_RSH_COMUN
			//                        ,B.FL_RSH_COMUN_COND
			//                        ,B.TP_LIV_GENZ_TIT
			//                        ,B.FL_COP_FINANZ
			//                        ,B.TP_APPLZ_DIR
			//                        ,B.SPE_MED
			//                        ,B.DIR_EMIS
			//                        ,B.DIR_1O_VERS
			//                        ,B.DIR_VERS_AGG
			//                        ,B.COD_DVS
			//                        ,B.FL_FNT_AZ
			//                        ,B.FL_FNT_ADER
			//                        ,B.FL_FNT_TFR
			//                        ,B.FL_FNT_VOLO
			//                        ,B.TP_OPZ_A_SCAD
			//                        ,B.AA_DIFF_PROR_DFLT
			//                        ,B.FL_VER_PROD
			//                        ,B.DUR_GG
			//                        ,B.DIR_QUIET
			//                        ,B.TP_PTF_ESTNO
			//                        ,B.FL_CUM_PRE_CNTR
			//                        ,B.FL_AMMB_MOVI
			//                        ,B.CONV_GECO
			//                        ,B.DS_RIGA
			//                        ,B.DS_OPER_SQL
			//                        ,B.DS_VER
			//                        ,B.DS_TS_INI_CPTZ
			//                        ,B.DS_TS_END_CPTZ
			//                        ,B.DS_UTENTE
			//                        ,B.DS_STATO_ELAB
			//                        ,B.FL_SCUDO_FISC
			//                        ,B.FL_TRASFE
			//                        ,B.FL_TFR_STRC
			//                        ,B.DT_PRESC
			//                        ,B.COD_CONV_AGG
			//                        ,B.SUBCAT_PROD
			//                        ,B.FL_QUEST_ADEGZ_ASS
			//                        ,B.COD_TPA
			//                        ,B.ID_ACC_COMM
			//                        ,B.FL_POLI_CPI_PR
			//                        ,B.FL_POLI_BUNDLING
			//                        ,B.IND_POLI_PRIN_COLL
			//                        ,B.FL_VND_BUNDLE
			//                        ,B.IB_BS
			//                        ,B.FL_POLI_IFP
			//                        ,C.ID_STAT_OGG_BUS
			//                        ,C.ID_OGG
			//                        ,C.TP_OGG
			//                        ,C.ID_MOVI_CRZ
			//                        ,C.ID_MOVI_CHIU
			//                        ,C.DT_INI_EFF
			//                        ,C.DT_END_EFF
			//                        ,C.COD_COMP_ANIA
			//                        ,C.TP_STAT_BUS
			//                        ,C.TP_CAUS
			//                        ,C.DS_RIGA
			//                        ,C.DS_OPER_SQL
			//                        ,C.DS_VER
			//                        ,C.DS_TS_INI_CPTZ
			//                        ,C.DS_TS_END_CPTZ
			//                        ,C.DS_UTENTE
			//                        ,C.DS_STATO_ELAB
			//                     INTO
			//                        :ADE-ID-ADES
			//                       ,:ADE-ID-POLI
			//                       ,:ADE-ID-MOVI-CRZ
			//                       ,:ADE-ID-MOVI-CHIU
			//                        :IND-ADE-ID-MOVI-CHIU
			//                       ,:ADE-DT-INI-EFF-DB
			//                       ,:ADE-DT-END-EFF-DB
			//                       ,:ADE-IB-PREV
			//                        :IND-ADE-IB-PREV
			//                       ,:ADE-IB-OGG
			//                        :IND-ADE-IB-OGG
			//                       ,:ADE-COD-COMP-ANIA
			//                       ,:ADE-DT-DECOR-DB
			//                        :IND-ADE-DT-DECOR
			//                       ,:ADE-DT-SCAD-DB
			//                        :IND-ADE-DT-SCAD
			//                       ,:ADE-ETA-A-SCAD
			//                        :IND-ADE-ETA-A-SCAD
			//                       ,:ADE-DUR-AA
			//                        :IND-ADE-DUR-AA
			//                       ,:ADE-DUR-MM
			//                        :IND-ADE-DUR-MM
			//                       ,:ADE-DUR-GG
			//                        :IND-ADE-DUR-GG
			//                       ,:ADE-TP-RGM-FISC
			//                       ,:ADE-TP-RIAT
			//                        :IND-ADE-TP-RIAT
			//                       ,:ADE-TP-MOD-PAG-TIT
			//                       ,:ADE-TP-IAS
			//                        :IND-ADE-TP-IAS
			//                       ,:ADE-DT-VARZ-TP-IAS-DB
			//                        :IND-ADE-DT-VARZ-TP-IAS
			//                       ,:ADE-PRE-NET-IND
			//                        :IND-ADE-PRE-NET-IND
			//                       ,:ADE-PRE-LRD-IND
			//                        :IND-ADE-PRE-LRD-IND
			//                       ,:ADE-RAT-LRD-IND
			//                        :IND-ADE-RAT-LRD-IND
			//                       ,:ADE-PRSTZ-INI-IND
			//                        :IND-ADE-PRSTZ-INI-IND
			//                       ,:ADE-FL-COINC-ASSTO
			//                        :IND-ADE-FL-COINC-ASSTO
			//                       ,:ADE-IB-DFLT
			//                        :IND-ADE-IB-DFLT
			//                       ,:ADE-MOD-CALC
			//                        :IND-ADE-MOD-CALC
			//                       ,:ADE-TP-FNT-CNBTVA
			//                        :IND-ADE-TP-FNT-CNBTVA
			//                       ,:ADE-IMP-AZ
			//                        :IND-ADE-IMP-AZ
			//                       ,:ADE-IMP-ADER
			//                        :IND-ADE-IMP-ADER
			//                       ,:ADE-IMP-TFR
			//                        :IND-ADE-IMP-TFR
			//                       ,:ADE-IMP-VOLO
			//                        :IND-ADE-IMP-VOLO
			//                       ,:ADE-PC-AZ
			//                        :IND-ADE-PC-AZ
			//                       ,:ADE-PC-ADER
			//                        :IND-ADE-PC-ADER
			//                       ,:ADE-PC-TFR
			//                        :IND-ADE-PC-TFR
			//                       ,:ADE-PC-VOLO
			//                        :IND-ADE-PC-VOLO
			//                       ,:ADE-DT-NOVA-RGM-FISC-DB
			//                        :IND-ADE-DT-NOVA-RGM-FISC
			//                       ,:ADE-FL-ATTIV
			//                        :IND-ADE-FL-ATTIV
			//                       ,:ADE-IMP-REC-RIT-VIS
			//                        :IND-ADE-IMP-REC-RIT-VIS
			//                       ,:ADE-IMP-REC-RIT-ACC
			//                        :IND-ADE-IMP-REC-RIT-ACC
			//                       ,:ADE-FL-VARZ-STAT-TBGC
			//                        :IND-ADE-FL-VARZ-STAT-TBGC
			//                       ,:ADE-FL-PROVZA-MIGRAZ
			//                        :IND-ADE-FL-PROVZA-MIGRAZ
			//                       ,:ADE-IMPB-VIS-DA-REC
			//                        :IND-ADE-IMPB-VIS-DA-REC
			//                       ,:ADE-DT-DECOR-PREST-BAN-DB
			//                        :IND-ADE-DT-DECOR-PREST-BAN
			//                       ,:ADE-DT-EFF-VARZ-STAT-T-DB
			//                        :IND-ADE-DT-EFF-VARZ-STAT-T
			//                       ,:ADE-DS-RIGA
			//                       ,:ADE-DS-OPER-SQL
			//                       ,:ADE-DS-VER
			//                       ,:ADE-DS-TS-INI-CPTZ
			//                       ,:ADE-DS-TS-END-CPTZ
			//                       ,:ADE-DS-UTENTE
			//                       ,:ADE-DS-STATO-ELAB
			//                       ,:ADE-CUM-CNBT-CAP
			//                        :IND-ADE-CUM-CNBT-CAP
			//                       ,:ADE-IMP-GAR-CNBT
			//                        :IND-ADE-IMP-GAR-CNBT
			//                       ,:ADE-DT-ULT-CONS-CNBT-DB
			//                        :IND-ADE-DT-ULT-CONS-CNBT
			//                       ,:ADE-IDEN-ISC-FND
			//                        :IND-ADE-IDEN-ISC-FND
			//                       ,:ADE-NUM-RAT-PIAN
			//                        :IND-ADE-NUM-RAT-PIAN
			//                       ,:ADE-DT-PRESC-DB
			//                        :IND-ADE-DT-PRESC
			//                       ,:ADE-CONCS-PREST
			//                        :IND-ADE-CONCS-PREST
			//                       ,:POL-ID-POLI
			//                       ,:POL-ID-MOVI-CRZ
			//                       ,:POL-ID-MOVI-CHIU
			//                        :IND-POL-ID-MOVI-CHIU
			//                       ,:POL-IB-OGG
			//                        :IND-POL-IB-OGG
			//                       ,:POL-IB-PROP
			//                       ,:POL-DT-PROP-DB
			//                        :IND-POL-DT-PROP
			//                       ,:POL-DT-INI-EFF-DB
			//                       ,:POL-DT-END-EFF-DB
			//                       ,:POL-COD-COMP-ANIA
			//                       ,:POL-DT-DECOR-DB
			//                       ,:POL-DT-EMIS-DB
			//                       ,:POL-TP-POLI
			//                       ,:POL-DUR-AA
			//                        :IND-POL-DUR-AA
			//                       ,:POL-DUR-MM
			//                        :IND-POL-DUR-MM
			//                       ,:POL-DT-SCAD-DB
			//                        :IND-POL-DT-SCAD
			//                       ,:POL-COD-PROD
			//                       ,:POL-DT-INI-VLDT-PROD-DB
			//                       ,:POL-COD-CONV
			//                        :IND-POL-COD-CONV
			//                       ,:POL-COD-RAMO
			//                        :IND-POL-COD-RAMO
			//                       ,:POL-DT-INI-VLDT-CONV-DB
			//                        :IND-POL-DT-INI-VLDT-CONV
			//                       ,:POL-DT-APPLZ-CONV-DB
			//                        :IND-POL-DT-APPLZ-CONV
			//                       ,:POL-TP-FRM-ASSVA
			//                       ,:POL-TP-RGM-FISC
			//                        :IND-POL-TP-RGM-FISC
			//                       ,:POL-FL-ESTAS
			//                        :IND-POL-FL-ESTAS
			//                       ,:POL-FL-RSH-COMUN
			//                        :IND-POL-FL-RSH-COMUN
			//                       ,:POL-FL-RSH-COMUN-COND
			//                        :IND-POL-FL-RSH-COMUN-COND
			//                       ,:POL-TP-LIV-GENZ-TIT
			//                       ,:POL-FL-COP-FINANZ
			//                        :IND-POL-FL-COP-FINANZ
			//                       ,:POL-TP-APPLZ-DIR
			//                        :IND-POL-TP-APPLZ-DIR
			//                       ,:POL-SPE-MED
			//                        :IND-POL-SPE-MED
			//                       ,:POL-DIR-EMIS
			//                        :IND-POL-DIR-EMIS
			//                       ,:POL-DIR-1O-VERS
			//                        :IND-POL-DIR-1O-VERS
			//                       ,:POL-DIR-VERS-AGG
			//                        :IND-POL-DIR-VERS-AGG
			//                       ,:POL-COD-DVS
			//                        :IND-POL-COD-DVS
			//                       ,:POL-FL-FNT-AZ
			//                        :IND-POL-FL-FNT-AZ
			//                       ,:POL-FL-FNT-ADER
			//                        :IND-POL-FL-FNT-ADER
			//                       ,:POL-FL-FNT-TFR
			//                        :IND-POL-FL-FNT-TFR
			//                       ,:POL-FL-FNT-VOLO
			//                        :IND-POL-FL-FNT-VOLO
			//                       ,:POL-TP-OPZ-A-SCAD
			//                        :IND-POL-TP-OPZ-A-SCAD
			//                       ,:POL-AA-DIFF-PROR-DFLT
			//                        :IND-POL-AA-DIFF-PROR-DFLT
			//                       ,:POL-FL-VER-PROD
			//                        :IND-POL-FL-VER-PROD
			//                       ,:POL-DUR-GG
			//                        :IND-POL-DUR-GG
			//                       ,:POL-DIR-QUIET
			//                        :IND-POL-DIR-QUIET
			//                       ,:POL-TP-PTF-ESTNO
			//                        :IND-POL-TP-PTF-ESTNO
			//                       ,:POL-FL-CUM-PRE-CNTR
			//                        :IND-POL-FL-CUM-PRE-CNTR
			//                       ,:POL-FL-AMMB-MOVI
			//                        :IND-POL-FL-AMMB-MOVI
			//                       ,:POL-CONV-GECO
			//                        :IND-POL-CONV-GECO
			//                       ,:POL-DS-RIGA
			//                       ,:POL-DS-OPER-SQL
			//                       ,:POL-DS-VER
			//                       ,:POL-DS-TS-INI-CPTZ
			//                       ,:POL-DS-TS-END-CPTZ
			//                       ,:POL-DS-UTENTE
			//                       ,:POL-DS-STATO-ELAB
			//                       ,:POL-FL-SCUDO-FISC
			//                        :IND-POL-FL-SCUDO-FISC
			//                       ,:POL-FL-TRASFE
			//                        :IND-POL-FL-TRASFE
			//                       ,:POL-FL-TFR-STRC
			//                        :IND-POL-FL-TFR-STRC
			//                       ,:POL-DT-PRESC-DB
			//                        :IND-POL-DT-PRESC
			//                       ,:POL-COD-CONV-AGG
			//                        :IND-POL-COD-CONV-AGG
			//                       ,:POL-SUBCAT-PROD
			//                        :IND-POL-SUBCAT-PROD
			//                       ,:POL-FL-QUEST-ADEGZ-ASS
			//                        :IND-POL-FL-QUEST-ADEGZ-ASS
			//                       ,:POL-COD-TPA
			//                        :IND-POL-COD-TPA
			//                       ,:POL-ID-ACC-COMM
			//                        :IND-POL-ID-ACC-COMM
			//                       ,:POL-FL-POLI-CPI-PR
			//                        :IND-POL-FL-POLI-CPI-PR
			//                       ,:POL-FL-POLI-BUNDLING
			//                        :IND-POL-FL-POLI-BUNDLING
			//                       ,:POL-IND-POLI-PRIN-COLL
			//                        :IND-POL-IND-POLI-PRIN-COLL
			//                       ,:POL-FL-VND-BUNDLE
			//                        :IND-POL-FL-VND-BUNDLE
			//                       ,:POL-IB-BS
			//                        :IND-POL-IB-BS
			//                       ,:POL-FL-POLI-IFP
			//                        :IND-POL-FL-POLI-IFP
			//                       ,:STB-ID-STAT-OGG-BUS
			//                       ,:STB-ID-OGG
			//                       ,:STB-TP-OGG
			//                       ,:STB-ID-MOVI-CRZ
			//                       ,:STB-ID-MOVI-CHIU
			//                        :IND-STB-ID-MOVI-CHIU
			//                       ,:STB-DT-INI-EFF-DB
			//                       ,:STB-DT-END-EFF-DB
			//                       ,:STB-COD-COMP-ANIA
			//                       ,:STB-TP-STAT-BUS
			//                       ,:STB-TP-CAUS
			//                       ,:STB-DS-RIGA
			//                       ,:STB-DS-OPER-SQL
			//                       ,:STB-DS-VER
			//                       ,:STB-DS-TS-INI-CPTZ
			//                       ,:STB-DS-TS-END-CPTZ
			//                       ,:STB-DS-UTENTE
			//                       ,:STB-DS-STATO-ELAB
			//                     FROM ADES         A,
			//                          POLI         B,
			//                          STAT_OGG_BUS C
			//                     WHERE B.TP_FRM_ASSVA   IN (:WS-TP-FRM-ASSVA1,
			//                                                :WS-TP-FRM-ASSVA2)
			//                       AND A.ID_POLI         =   B.ID_POLI
			//                       AND A.ID_ADES         =   C.ID_OGG
			//                       AND C.TP_OGG          =  'AD'
			//                       AND C.TP_STAT_BUS     =  'VI'
			//           *           OR
			//           *               (C.TP_STAT_BUS    =  'ST'  AND
			//           *               EXISTS(SELECT
			//           *                     D.ID_MOVI_FINRIO
			//           *
			//           *                  FROM  MOVI_FINRIO D
			//           *                  WHERE A.ID_ADES = D.ID_ADES
			//           *                    AND D.STAT_MOVI IN ('AL','IE')
			//           *                    AND D.TP_MOVI_FINRIO = 'DI'
			//           *                    AND D.COD_COMP_ANIA   =
			//           *                        :IDSV0003-CODICE-COMPAGNIA-ANIA
			//           *                    AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
			//           *                    AND D.DT_END_EFF     >   :WS-DT-PTF-X
			//           *                    AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
			//           *                    AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
			//                     AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
			//                     AND A.ID_POLI        BETWEEN   :IABV0009-ID-OGG-DA AND
			//                                                    :IABV0009-ID-OGG-A
			//                     AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
			//                     AND A.DT_END_EFF     >   :WS-DT-PTF-X
			//                     AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
			//                     AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
			//                     AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
			//                     AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
			//                     AND B.DT_END_EFF     >   :WS-DT-PTF-X
			//                     AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
			//                     AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
			//                     AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
			//                     AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
			//                     AND C.DT_END_EFF     >   :WS-DT-PTF-X
			//                     AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
			//                     AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
			//           *         AND A.DS_STATO_ELAB IN   (
			//           *                                   :IABV0002-STATE-01,
			//           *                                   :IABV0002-STATE-02,
			//           *                                   :IABV0002-STATE-03,
			//           *                                   :IABV0002-STATE-04,
			//           *                                   :IABV0002-STATE-05,
			//           *                                   :IABV0002-STATE-06,
			//           *                                   :IABV0002-STATE-07,
			//           *                                   :IABV0002-STATE-08,
			//           *                                   :IABV0002-STATE-09,
			//           *                                   :IABV0002-STATE-10
			//           *                                    )
			//           *           AND NOT A.DS_VER     = :IABV0009-VERSIONING
			//                      FETCH FIRST ROW ONLY
			//                   END-EXEC
			adesPoliStatOggBusDao.selectRec(this.getAdesPoliStatOggBusLdbm0250());
		} else {
			//         AND A.DS_STATO_ELAB IN   (
			//                                   :IABV0002-STATE-01,
			//                                   :IABV0002-STATE-02,
			//                                   :IABV0002-STATE-03,
			//                                   :IABV0002-STATE-04,
			//                                   :IABV0002-STATE-05,
			//                                   :IABV0002-STATE-06,
			//                                   :IABV0002-STATE-07,
			//                                   :IABV0002-STATE-08,
			//                                   :IABV0002-STATE-09,
			//                                   :IABV0002-STATE-10
			//                                    )
			//           AND NOT A.DS_VER     = :IABV0009-VERSIONING
			// COB_CODE:         EXEC SQL
			//                     SELECT
			//                         A.ID_ADES
			//                        ,A.ID_POLI
			//                        ,A.ID_MOVI_CRZ
			//                        ,A.ID_MOVI_CHIU
			//                        ,A.DT_INI_EFF
			//                        ,A.DT_END_EFF
			//                        ,A.IB_PREV
			//                        ,A.IB_OGG
			//                        ,A.COD_COMP_ANIA
			//                        ,A.DT_DECOR
			//                        ,A.DT_SCAD
			//                        ,A.ETA_A_SCAD
			//                        ,A.DUR_AA
			//                        ,A.DUR_MM
			//                        ,A.DUR_GG
			//                        ,A.TP_RGM_FISC
			//                        ,A.TP_RIAT
			//                        ,A.TP_MOD_PAG_TIT
			//                        ,A.TP_IAS
			//                        ,A.DT_VARZ_TP_IAS
			//                        ,A.PRE_NET_IND
			//                        ,A.PRE_LRD_IND
			//                        ,A.RAT_LRD_IND
			//                        ,A.PRSTZ_INI_IND
			//                        ,A.FL_COINC_ASSTO
			//                        ,A.IB_DFLT
			//                        ,A.MOD_CALC
			//                        ,A.TP_FNT_CNBTVA
			//                        ,A.IMP_AZ
			//                        ,A.IMP_ADER
			//                        ,A.IMP_TFR
			//                        ,A.IMP_VOLO
			//                        ,A.PC_AZ
			//                        ,A.PC_ADER
			//                        ,A.PC_TFR
			//                        ,A.PC_VOLO
			//                        ,A.DT_NOVA_RGM_FISC
			//                        ,A.FL_ATTIV
			//                        ,A.IMP_REC_RIT_VIS
			//                        ,A.IMP_REC_RIT_ACC
			//                        ,A.FL_VARZ_STAT_TBGC
			//                        ,A.FL_PROVZA_MIGRAZ
			//                        ,A.IMPB_VIS_DA_REC
			//                        ,A.DT_DECOR_PREST_BAN
			//                        ,A.DT_EFF_VARZ_STAT_T
			//                        ,A.DS_RIGA
			//                        ,A.DS_OPER_SQL
			//                        ,A.DS_VER
			//                        ,A.DS_TS_INI_CPTZ
			//                        ,A.DS_TS_END_CPTZ
			//                        ,A.DS_UTENTE
			//                        ,A.DS_STATO_ELAB
			//                        ,A.CUM_CNBT_CAP
			//                        ,A.IMP_GAR_CNBT
			//                        ,A.DT_ULT_CONS_CNBT
			//                        ,A.IDEN_ISC_FND
			//                        ,A.NUM_RAT_PIAN
			//                        ,A.DT_PRESC
			//                        ,A.CONCS_PREST
			//                        ,B.ID_POLI
			//                        ,B.ID_MOVI_CRZ
			//                        ,B.ID_MOVI_CHIU
			//                        ,B.IB_OGG
			//                        ,B.IB_PROP
			//                        ,B.DT_PROP
			//                        ,B.DT_INI_EFF
			//                        ,B.DT_END_EFF
			//                        ,B.COD_COMP_ANIA
			//                        ,B.DT_DECOR
			//                        ,B.DT_EMIS
			//                        ,B.TP_POLI
			//                        ,B.DUR_AA
			//                        ,B.DUR_MM
			//                        ,B.DT_SCAD
			//                        ,B.COD_PROD
			//                        ,B.DT_INI_VLDT_PROD
			//                        ,B.COD_CONV
			//                        ,B.COD_RAMO
			//                        ,B.DT_INI_VLDT_CONV
			//                        ,B.DT_APPLZ_CONV
			//                        ,B.TP_FRM_ASSVA
			//                        ,B.TP_RGM_FISC
			//                        ,B.FL_ESTAS
			//                        ,B.FL_RSH_COMUN
			//                        ,B.FL_RSH_COMUN_COND
			//                        ,B.TP_LIV_GENZ_TIT
			//                        ,B.FL_COP_FINANZ
			//                        ,B.TP_APPLZ_DIR
			//                        ,B.SPE_MED
			//                        ,B.DIR_EMIS
			//                        ,B.DIR_1O_VERS
			//                        ,B.DIR_VERS_AGG
			//                        ,B.COD_DVS
			//                        ,B.FL_FNT_AZ
			//                        ,B.FL_FNT_ADER
			//                        ,B.FL_FNT_TFR
			//                        ,B.FL_FNT_VOLO
			//                        ,B.TP_OPZ_A_SCAD
			//                        ,B.AA_DIFF_PROR_DFLT
			//                        ,B.FL_VER_PROD
			//                        ,B.DUR_GG
			//                        ,B.DIR_QUIET
			//                        ,B.TP_PTF_ESTNO
			//                        ,B.FL_CUM_PRE_CNTR
			//                        ,B.FL_AMMB_MOVI
			//                        ,B.CONV_GECO
			//                        ,B.DS_RIGA
			//                        ,B.DS_OPER_SQL
			//                        ,B.DS_VER
			//                        ,B.DS_TS_INI_CPTZ
			//                        ,B.DS_TS_END_CPTZ
			//                        ,B.DS_UTENTE
			//                        ,B.DS_STATO_ELAB
			//                        ,B.FL_SCUDO_FISC
			//                        ,B.FL_TRASFE
			//                        ,B.FL_TFR_STRC
			//                        ,B.DT_PRESC
			//                        ,B.COD_CONV_AGG
			//                        ,B.SUBCAT_PROD
			//                        ,B.FL_QUEST_ADEGZ_ASS
			//                        ,B.COD_TPA
			//                        ,B.ID_ACC_COMM
			//                        ,B.FL_POLI_CPI_PR
			//                        ,B.FL_POLI_BUNDLING
			//                        ,B.IND_POLI_PRIN_COLL
			//                        ,B.FL_VND_BUNDLE
			//                        ,B.IB_BS
			//                        ,B.FL_POLI_IFP
			//                        ,C.ID_STAT_OGG_BUS
			//                        ,C.ID_OGG
			//                        ,C.TP_OGG
			//                        ,C.ID_MOVI_CRZ
			//                        ,C.ID_MOVI_CHIU
			//                        ,C.DT_INI_EFF
			//                        ,C.DT_END_EFF
			//                        ,C.COD_COMP_ANIA
			//                        ,C.TP_STAT_BUS
			//                        ,C.TP_CAUS
			//                        ,C.DS_RIGA
			//                        ,C.DS_OPER_SQL
			//                        ,C.DS_VER
			//                        ,C.DS_TS_INI_CPTZ
			//                        ,C.DS_TS_END_CPTZ
			//                        ,C.DS_UTENTE
			//                        ,C.DS_STATO_ELAB
			//                     INTO
			//                        :ADE-ID-ADES
			//                       ,:ADE-ID-POLI
			//                       ,:ADE-ID-MOVI-CRZ
			//                       ,:ADE-ID-MOVI-CHIU
			//                        :IND-ADE-ID-MOVI-CHIU
			//                       ,:ADE-DT-INI-EFF-DB
			//                       ,:ADE-DT-END-EFF-DB
			//                       ,:ADE-IB-PREV
			//                        :IND-ADE-IB-PREV
			//                       ,:ADE-IB-OGG
			//                        :IND-ADE-IB-OGG
			//                       ,:ADE-COD-COMP-ANIA
			//                       ,:ADE-DT-DECOR-DB
			//                        :IND-ADE-DT-DECOR
			//                       ,:ADE-DT-SCAD-DB
			//                        :IND-ADE-DT-SCAD
			//                       ,:ADE-ETA-A-SCAD
			//                        :IND-ADE-ETA-A-SCAD
			//                       ,:ADE-DUR-AA
			//                        :IND-ADE-DUR-AA
			//                       ,:ADE-DUR-MM
			//                        :IND-ADE-DUR-MM
			//                       ,:ADE-DUR-GG
			//                        :IND-ADE-DUR-GG
			//                       ,:ADE-TP-RGM-FISC
			//                       ,:ADE-TP-RIAT
			//                        :IND-ADE-TP-RIAT
			//                       ,:ADE-TP-MOD-PAG-TIT
			//                       ,:ADE-TP-IAS
			//                        :IND-ADE-TP-IAS
			//                       ,:ADE-DT-VARZ-TP-IAS-DB
			//                        :IND-ADE-DT-VARZ-TP-IAS
			//                       ,:ADE-PRE-NET-IND
			//                        :IND-ADE-PRE-NET-IND
			//                       ,:ADE-PRE-LRD-IND
			//                        :IND-ADE-PRE-LRD-IND
			//                       ,:ADE-RAT-LRD-IND
			//                        :IND-ADE-RAT-LRD-IND
			//                       ,:ADE-PRSTZ-INI-IND
			//                        :IND-ADE-PRSTZ-INI-IND
			//                       ,:ADE-FL-COINC-ASSTO
			//                        :IND-ADE-FL-COINC-ASSTO
			//                       ,:ADE-IB-DFLT
			//                        :IND-ADE-IB-DFLT
			//                       ,:ADE-MOD-CALC
			//                        :IND-ADE-MOD-CALC
			//                       ,:ADE-TP-FNT-CNBTVA
			//                        :IND-ADE-TP-FNT-CNBTVA
			//                       ,:ADE-IMP-AZ
			//                        :IND-ADE-IMP-AZ
			//                       ,:ADE-IMP-ADER
			//                        :IND-ADE-IMP-ADER
			//                       ,:ADE-IMP-TFR
			//                        :IND-ADE-IMP-TFR
			//                       ,:ADE-IMP-VOLO
			//                        :IND-ADE-IMP-VOLO
			//                       ,:ADE-PC-AZ
			//                        :IND-ADE-PC-AZ
			//                       ,:ADE-PC-ADER
			//                        :IND-ADE-PC-ADER
			//                       ,:ADE-PC-TFR
			//                        :IND-ADE-PC-TFR
			//                       ,:ADE-PC-VOLO
			//                        :IND-ADE-PC-VOLO
			//                       ,:ADE-DT-NOVA-RGM-FISC-DB
			//                        :IND-ADE-DT-NOVA-RGM-FISC
			//                       ,:ADE-FL-ATTIV
			//                        :IND-ADE-FL-ATTIV
			//                       ,:ADE-IMP-REC-RIT-VIS
			//                        :IND-ADE-IMP-REC-RIT-VIS
			//                       ,:ADE-IMP-REC-RIT-ACC
			//                        :IND-ADE-IMP-REC-RIT-ACC
			//                       ,:ADE-FL-VARZ-STAT-TBGC
			//                        :IND-ADE-FL-VARZ-STAT-TBGC
			//                       ,:ADE-FL-PROVZA-MIGRAZ
			//                        :IND-ADE-FL-PROVZA-MIGRAZ
			//                       ,:ADE-IMPB-VIS-DA-REC
			//                        :IND-ADE-IMPB-VIS-DA-REC
			//                       ,:ADE-DT-DECOR-PREST-BAN-DB
			//                        :IND-ADE-DT-DECOR-PREST-BAN
			//                       ,:ADE-DT-EFF-VARZ-STAT-T-DB
			//                        :IND-ADE-DT-EFF-VARZ-STAT-T
			//                       ,:ADE-DS-RIGA
			//                       ,:ADE-DS-OPER-SQL
			//                       ,:ADE-DS-VER
			//                       ,:ADE-DS-TS-INI-CPTZ
			//                       ,:ADE-DS-TS-END-CPTZ
			//                       ,:ADE-DS-UTENTE
			//                       ,:ADE-DS-STATO-ELAB
			//                       ,:ADE-CUM-CNBT-CAP
			//                        :IND-ADE-CUM-CNBT-CAP
			//                       ,:ADE-IMP-GAR-CNBT
			//                        :IND-ADE-IMP-GAR-CNBT
			//                       ,:ADE-DT-ULT-CONS-CNBT-DB
			//                        :IND-ADE-DT-ULT-CONS-CNBT
			//                       ,:ADE-IDEN-ISC-FND
			//                        :IND-ADE-IDEN-ISC-FND
			//                       ,:ADE-NUM-RAT-PIAN
			//                        :IND-ADE-NUM-RAT-PIAN
			//                       ,:ADE-DT-PRESC-DB
			//                        :IND-ADE-DT-PRESC
			//                       ,:ADE-CONCS-PREST
			//                        :IND-ADE-CONCS-PREST
			//                       ,:POL-ID-POLI
			//                       ,:POL-ID-MOVI-CRZ
			//                       ,:POL-ID-MOVI-CHIU
			//                        :IND-POL-ID-MOVI-CHIU
			//                       ,:POL-IB-OGG
			//                        :IND-POL-IB-OGG
			//                       ,:POL-IB-PROP
			//                       ,:POL-DT-PROP-DB
			//                        :IND-POL-DT-PROP
			//                       ,:POL-DT-INI-EFF-DB
			//                       ,:POL-DT-END-EFF-DB
			//                       ,:POL-COD-COMP-ANIA
			//                       ,:POL-DT-DECOR-DB
			//                       ,:POL-DT-EMIS-DB
			//                       ,:POL-TP-POLI
			//                       ,:POL-DUR-AA
			//                        :IND-POL-DUR-AA
			//                       ,:POL-DUR-MM
			//                        :IND-POL-DUR-MM
			//                       ,:POL-DT-SCAD-DB
			//                        :IND-POL-DT-SCAD
			//                       ,:POL-COD-PROD
			//                       ,:POL-DT-INI-VLDT-PROD-DB
			//                       ,:POL-COD-CONV
			//                        :IND-POL-COD-CONV
			//                       ,:POL-COD-RAMO
			//                        :IND-POL-COD-RAMO
			//                       ,:POL-DT-INI-VLDT-CONV-DB
			//                        :IND-POL-DT-INI-VLDT-CONV
			//                       ,:POL-DT-APPLZ-CONV-DB
			//                        :IND-POL-DT-APPLZ-CONV
			//                       ,:POL-TP-FRM-ASSVA
			//                       ,:POL-TP-RGM-FISC
			//                        :IND-POL-TP-RGM-FISC
			//                       ,:POL-FL-ESTAS
			//                        :IND-POL-FL-ESTAS
			//                       ,:POL-FL-RSH-COMUN
			//                        :IND-POL-FL-RSH-COMUN
			//                       ,:POL-FL-RSH-COMUN-COND
			//                        :IND-POL-FL-RSH-COMUN-COND
			//                       ,:POL-TP-LIV-GENZ-TIT
			//                       ,:POL-FL-COP-FINANZ
			//                        :IND-POL-FL-COP-FINANZ
			//                       ,:POL-TP-APPLZ-DIR
			//                        :IND-POL-TP-APPLZ-DIR
			//                       ,:POL-SPE-MED
			//                        :IND-POL-SPE-MED
			//                       ,:POL-DIR-EMIS
			//                        :IND-POL-DIR-EMIS
			//                       ,:POL-DIR-1O-VERS
			//                        :IND-POL-DIR-1O-VERS
			//                       ,:POL-DIR-VERS-AGG
			//                        :IND-POL-DIR-VERS-AGG
			//                       ,:POL-COD-DVS
			//                        :IND-POL-COD-DVS
			//                       ,:POL-FL-FNT-AZ
			//                        :IND-POL-FL-FNT-AZ
			//                       ,:POL-FL-FNT-ADER
			//                        :IND-POL-FL-FNT-ADER
			//                       ,:POL-FL-FNT-TFR
			//                        :IND-POL-FL-FNT-TFR
			//                       ,:POL-FL-FNT-VOLO
			//                        :IND-POL-FL-FNT-VOLO
			//                       ,:POL-TP-OPZ-A-SCAD
			//                        :IND-POL-TP-OPZ-A-SCAD
			//                       ,:POL-AA-DIFF-PROR-DFLT
			//                        :IND-POL-AA-DIFF-PROR-DFLT
			//                       ,:POL-FL-VER-PROD
			//                        :IND-POL-FL-VER-PROD
			//                       ,:POL-DUR-GG
			//                        :IND-POL-DUR-GG
			//                       ,:POL-DIR-QUIET
			//                        :IND-POL-DIR-QUIET
			//                       ,:POL-TP-PTF-ESTNO
			//                        :IND-POL-TP-PTF-ESTNO
			//                       ,:POL-FL-CUM-PRE-CNTR
			//                        :IND-POL-FL-CUM-PRE-CNTR
			//                       ,:POL-FL-AMMB-MOVI
			//                        :IND-POL-FL-AMMB-MOVI
			//                       ,:POL-CONV-GECO
			//                        :IND-POL-CONV-GECO
			//                       ,:POL-DS-RIGA
			//                       ,:POL-DS-OPER-SQL
			//                       ,:POL-DS-VER
			//                       ,:POL-DS-TS-INI-CPTZ
			//                       ,:POL-DS-TS-END-CPTZ
			//                       ,:POL-DS-UTENTE
			//                       ,:POL-DS-STATO-ELAB
			//                       ,:POL-FL-SCUDO-FISC
			//                        :IND-POL-FL-SCUDO-FISC
			//                       ,:POL-FL-TRASFE
			//                        :IND-POL-FL-TRASFE
			//                       ,:POL-FL-TFR-STRC
			//                        :IND-POL-FL-TFR-STRC
			//                       ,:POL-DT-PRESC-DB
			//                        :IND-POL-DT-PRESC
			//                       ,:POL-COD-CONV-AGG
			//                        :IND-POL-COD-CONV-AGG
			//                       ,:POL-SUBCAT-PROD
			//                        :IND-POL-SUBCAT-PROD
			//                       ,:POL-FL-QUEST-ADEGZ-ASS
			//                        :IND-POL-FL-QUEST-ADEGZ-ASS
			//                       ,:POL-COD-TPA
			//                        :IND-POL-COD-TPA
			//                       ,:POL-ID-ACC-COMM
			//                        :IND-POL-ID-ACC-COMM
			//                       ,:POL-FL-POLI-CPI-PR
			//                        :IND-POL-FL-POLI-CPI-PR
			//                       ,:POL-FL-POLI-BUNDLING
			//                        :IND-POL-FL-POLI-BUNDLING
			//                       ,:POL-IND-POLI-PRIN-COLL
			//                        :IND-POL-IND-POLI-PRIN-COLL
			//                       ,:POL-FL-VND-BUNDLE
			//                        :IND-POL-FL-VND-BUNDLE
			//                       ,:POL-IB-BS
			//                        :IND-POL-IB-BS
			//                       ,:POL-FL-POLI-IFP
			//                        :IND-POL-FL-POLI-IFP
			//                       ,:STB-ID-STAT-OGG-BUS
			//                       ,:STB-ID-OGG
			//                       ,:STB-TP-OGG
			//                       ,:STB-ID-MOVI-CRZ
			//                       ,:STB-ID-MOVI-CHIU
			//                        :IND-STB-ID-MOVI-CHIU
			//                       ,:STB-DT-INI-EFF-DB
			//                       ,:STB-DT-END-EFF-DB
			//                       ,:STB-COD-COMP-ANIA
			//                       ,:STB-TP-STAT-BUS
			//                       ,:STB-TP-CAUS
			//                       ,:STB-DS-RIGA
			//                       ,:STB-DS-OPER-SQL
			//                       ,:STB-DS-VER
			//                       ,:STB-DS-TS-INI-CPTZ
			//                       ,:STB-DS-TS-END-CPTZ
			//                       ,:STB-DS-UTENTE
			//                       ,:STB-DS-STATO-ELAB
			//                     FROM ADES         A,
			//                          POLI         B,
			//                          STAT_OGG_BUS C
			//                     WHERE B.TP_FRM_ASSVA   IN (:WS-TP-FRM-ASSVA1,
			//                                                :WS-TP-FRM-ASSVA2)
			//                       AND A.ID_POLI         =   B.ID_POLI
			//                       AND A.ID_ADES         =   C.ID_OGG
			//                       AND C.TP_OGG          =  'AD'
			//                       AND C.TP_STAT_BUS     =  'VI'
			//           *           OR
			//           *               (C.TP_STAT_BUS    =  'ST'  AND
			//           *               EXISTS(SELECT
			//           *                     D.ID_MOVI_FINRIO
			//           *
			//           *                  FROM  MOVI_FINRIO D
			//           *                  WHERE A.ID_ADES = D.ID_ADES
			//           *                    AND D.STAT_MOVI IN ('AL','IE')
			//           *                    AND D.TP_MOVI_FINRIO = 'DI'
			//           *                    AND D.COD_COMP_ANIA   =
			//           *                        :IDSV0003-CODICE-COMPAGNIA-ANIA
			//           *                    AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
			//           *                    AND D.DT_END_EFF     >   :WS-DT-PTF-X
			//           *                    AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
			//           *                    AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
			//                     AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
			//                     AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
			//                     AND A.DT_END_EFF     >   :WS-DT-PTF-X
			//                     AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
			//                     AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
			//                     AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
			//                     AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
			//                     AND B.DT_END_EFF     >   :WS-DT-PTF-X
			//                     AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
			//                     AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
			//                     AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
			//                     AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
			//                     AND C.DT_END_EFF     >   :WS-DT-PTF-X
			//                     AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
			//                     AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
			//           *         AND A.DS_STATO_ELAB IN   (
			//           *                                   :IABV0002-STATE-01,
			//           *                                   :IABV0002-STATE-02,
			//           *                                   :IABV0002-STATE-03,
			//           *                                   :IABV0002-STATE-04,
			//           *                                   :IABV0002-STATE-05,
			//           *                                   :IABV0002-STATE-06,
			//           *                                   :IABV0002-STATE-07,
			//           *                                   :IABV0002-STATE-08,
			//           *                                   :IABV0002-STATE-09,
			//           *                                   :IABV0002-STATE-10
			//           *                                    )
			//           *           AND NOT A.DS_VER     = :IABV0009-VERSIONING
			//                      FETCH FIRST ROW ONLY
			//                   END-EXEC
			adesPoliStatOggBusDao.selectRec1(this.getAdesPoliStatOggBusLdbm0250());
		}
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			z100SetColonneNull();
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		}
	}

	/**Original name: A320-UPDATE-SC01<br>
	 * <pre>*****************************************************************</pre>*/
	private void a320UpdateSc01() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-PRIMARY-KEY
		//                   PERFORM A330-UPDATE-PK-SC01         THRU A330-SC01-EX
		//              WHEN IDSV0003-WHERE-CONDITION-01
		//                   PERFORM A340-UPDATE-WHERE-COND-SC01 THRU A340-SC01-EX
		//              WHEN IDSV0003-FIRST-ACTION
		//                                                       THRU A345-SC01-EX
		//              WHEN OTHER
		//                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
		//           END-EVALUATE.
		if (idsv0003.getLivelloOperazione().isIdsv0003PrimaryKey()) {
			// COB_CODE: PERFORM A330-UPDATE-PK-SC01         THRU A330-SC01-EX
			a330UpdatePkSc01();
		} else if (idsv0003.getTipologiaOperazione().isIdsv0003WhereCondition01()) {
			// COB_CODE: PERFORM A340-UPDATE-WHERE-COND-SC01 THRU A340-SC01-EX
			a340UpdateWhereCondSc01();
		} else if (idsv0003.getLivelloOperazione().isIdsv0003FirstAction()) {
			// COB_CODE: PERFORM A345-UPDATE-FIRST-ACTION-SC01
			//                                               THRU A345-SC01-EX
			a345UpdateFirstActionSc01();
		} else {
			// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
			idsv0003.getReturnCode().setInvalidLevelOper();
		}
	}

	/**Original name: A330-UPDATE-PK-SC01<br>
	 * <pre>*****************************************************************</pre>*/
	private void a330UpdatePkSc01() {
		// COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
		z150ValorizzaDataServices();
		// COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
		z200SetIndicatoriNull();
		// COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBM0250.cbl:line=1698, because the code is unreachable.
		// COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
		z960LengthVchar();
		//          ,DS_VER                 = :IABV0009-VERSIONING
		// COB_CODE:      EXEC SQL
		//                    UPDATE ADES
		//                    SET
		//                       DS_OPER_SQL            = :ADE-DS-OPER-SQL
		//           *          ,DS_VER                 = :IABV0009-VERSIONING
		//                      ,DS_UTENTE              = :ADE-DS-UTENTE
		//                      ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
		//                    WHERE             DS_RIGA = :ADE-DS-RIGA
		//                END-EXEC.
		adesDao.updateRec2(ades.getAdeDsOperSql(), ades.getAdeDsUtente(), iabv0002.getIabv0002StateCurrent(), ades.getAdeDsRiga());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A340-UPDATE-WHERE-COND-SC01<br>
	 * <pre>*****************************************************************</pre>*/
	private void a340UpdateWhereCondSc01() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A345-UPDATE-FIRST-ACTION-SC01<br>
	 * <pre>*****************************************************************</pre>*/
	private void a345UpdateFirstActionSc01() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A350-CTRL-COMMIT<br>
	 * <pre>*****************************************************************
	 * --
	 * --    si effettuano i controlli per comandare
	 * --    un'eventuale COMMIT al BATCH EXECUTOR
	 * --    tramite il settaggio del campo IABV0002-FLAG-COMMIT
	 * --</pre>*/
	private void a350CtrlCommit() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A360-OPEN-CURSOR-SC01<br>
	 * <pre>*****************************************************************</pre>*/
	private void a360OpenCursorSc01() {
		// COB_CODE: PERFORM A305-DECLARE-CURSOR-SC01 THRU A305-SC01-EX.
		a305DeclareCursorSc01();
		// COB_CODE: IF ACCESSO-X-RANGE-SI
		//              END-EXEC
		//           ELSE
		//              END-EXEC
		//           END-IF.
		if (ws.getFlagAccessoXRange().isSi()) {
			// COB_CODE: EXEC SQL
			//                OPEN CUR-SC01-RANGE
			//           END-EXEC
			adesPoliStatOggBusDao.openCurSc01Range(this.getAdesPoliStatOggBusLdbm0250());
		} else {
			// COB_CODE: EXEC SQL
			//                OPEN CUR-SC01
			//           END-EXEC
			adesPoliStatOggBusDao.openCurSc01(this.getAdesPoliStatOggBusLdbm0250());
		}
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A370-CLOSE-CURSOR-SC01<br>
	 * <pre>*****************************************************************</pre>*/
	private void a370CloseCursorSc01() {
		// COB_CODE: IF ACCESSO-X-RANGE-SI
		//              END-EXEC
		//           ELSE
		//              END-EXEC
		//           END-IF.
		if (ws.getFlagAccessoXRange().isSi()) {
			// COB_CODE: EXEC SQL
			//                CLOSE CUR-SC01-RANGE
			//           END-EXEC
			adesPoliStatOggBusDao.closeCurSc01Range();
		} else {
			// COB_CODE: EXEC SQL
			//                CLOSE CUR-SC01
			//           END-EXEC
			adesPoliStatOggBusDao.closeCurSc01();
		}
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A380-FETCH-FIRST-SC01<br>
	 * <pre>*****************************************************************</pre>*/
	private void a380FetchFirstSc01() {
		// COB_CODE: PERFORM A360-OPEN-CURSOR-SC01    THRU A360-SC01-EX.
		a360OpenCursorSc01();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM A390-FETCH-NEXT-SC01  THRU A390-SC01-EX
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM A390-FETCH-NEXT-SC01  THRU A390-SC01-EX
			a390FetchNextSc01();
		}
	}

	/**Original name: A390-FETCH-NEXT-SC01<br>
	 * <pre>*****************************************************************</pre>*/
	private void a390FetchNextSc01() {
		// COB_CODE: IF ACCESSO-X-RANGE-SI
		//              END-EXEC
		//           ELSE
		//              END-EXEC
		//           END-IF.
		if (ws.getFlagAccessoXRange().isSi()) {
			// COB_CODE: EXEC SQL
			//             FETCH CUR-SC01-RANGE
			//           INTO
			//             :ADE-ID-ADES
			//            ,:ADE-ID-POLI
			//            ,:ADE-ID-MOVI-CRZ
			//            ,:ADE-ID-MOVI-CHIU
			//             :IND-ADE-ID-MOVI-CHIU
			//            ,:ADE-DT-INI-EFF-DB
			//            ,:ADE-DT-END-EFF-DB
			//            ,:ADE-IB-PREV
			//             :IND-ADE-IB-PREV
			//            ,:ADE-IB-OGG
			//             :IND-ADE-IB-OGG
			//            ,:ADE-COD-COMP-ANIA
			//            ,:ADE-DT-DECOR-DB
			//             :IND-ADE-DT-DECOR
			//            ,:ADE-DT-SCAD-DB
			//             :IND-ADE-DT-SCAD
			//            ,:ADE-ETA-A-SCAD
			//             :IND-ADE-ETA-A-SCAD
			//            ,:ADE-DUR-AA
			//             :IND-ADE-DUR-AA
			//            ,:ADE-DUR-MM
			//             :IND-ADE-DUR-MM
			//            ,:ADE-DUR-GG
			//             :IND-ADE-DUR-GG
			//            ,:ADE-TP-RGM-FISC
			//            ,:ADE-TP-RIAT
			//             :IND-ADE-TP-RIAT
			//            ,:ADE-TP-MOD-PAG-TIT
			//            ,:ADE-TP-IAS
			//             :IND-ADE-TP-IAS
			//            ,:ADE-DT-VARZ-TP-IAS-DB
			//             :IND-ADE-DT-VARZ-TP-IAS
			//            ,:ADE-PRE-NET-IND
			//             :IND-ADE-PRE-NET-IND
			//            ,:ADE-PRE-LRD-IND
			//             :IND-ADE-PRE-LRD-IND
			//            ,:ADE-RAT-LRD-IND
			//             :IND-ADE-RAT-LRD-IND
			//            ,:ADE-PRSTZ-INI-IND
			//             :IND-ADE-PRSTZ-INI-IND
			//            ,:ADE-FL-COINC-ASSTO
			//             :IND-ADE-FL-COINC-ASSTO
			//            ,:ADE-IB-DFLT
			//             :IND-ADE-IB-DFLT
			//            ,:ADE-MOD-CALC
			//             :IND-ADE-MOD-CALC
			//            ,:ADE-TP-FNT-CNBTVA
			//             :IND-ADE-TP-FNT-CNBTVA
			//            ,:ADE-IMP-AZ
			//             :IND-ADE-IMP-AZ
			//            ,:ADE-IMP-ADER
			//             :IND-ADE-IMP-ADER
			//            ,:ADE-IMP-TFR
			//             :IND-ADE-IMP-TFR
			//            ,:ADE-IMP-VOLO
			//             :IND-ADE-IMP-VOLO
			//            ,:ADE-PC-AZ
			//             :IND-ADE-PC-AZ
			//            ,:ADE-PC-ADER
			//             :IND-ADE-PC-ADER
			//            ,:ADE-PC-TFR
			//             :IND-ADE-PC-TFR
			//            ,:ADE-PC-VOLO
			//             :IND-ADE-PC-VOLO
			//            ,:ADE-DT-NOVA-RGM-FISC-DB
			//             :IND-ADE-DT-NOVA-RGM-FISC
			//            ,:ADE-FL-ATTIV
			//             :IND-ADE-FL-ATTIV
			//            ,:ADE-IMP-REC-RIT-VIS
			//             :IND-ADE-IMP-REC-RIT-VIS
			//            ,:ADE-IMP-REC-RIT-ACC
			//             :IND-ADE-IMP-REC-RIT-ACC
			//            ,:ADE-FL-VARZ-STAT-TBGC
			//             :IND-ADE-FL-VARZ-STAT-TBGC
			//            ,:ADE-FL-PROVZA-MIGRAZ
			//             :IND-ADE-FL-PROVZA-MIGRAZ
			//            ,:ADE-IMPB-VIS-DA-REC
			//             :IND-ADE-IMPB-VIS-DA-REC
			//            ,:ADE-DT-DECOR-PREST-BAN-DB
			//             :IND-ADE-DT-DECOR-PREST-BAN
			//            ,:ADE-DT-EFF-VARZ-STAT-T-DB
			//             :IND-ADE-DT-EFF-VARZ-STAT-T
			//            ,:ADE-DS-RIGA
			//            ,:ADE-DS-OPER-SQL
			//            ,:ADE-DS-VER
			//            ,:ADE-DS-TS-INI-CPTZ
			//            ,:ADE-DS-TS-END-CPTZ
			//            ,:ADE-DS-UTENTE
			//            ,:ADE-DS-STATO-ELAB
			//            ,:ADE-CUM-CNBT-CAP
			//             :IND-ADE-CUM-CNBT-CAP
			//            ,:ADE-IMP-GAR-CNBT
			//             :IND-ADE-IMP-GAR-CNBT
			//            ,:ADE-DT-ULT-CONS-CNBT-DB
			//             :IND-ADE-DT-ULT-CONS-CNBT
			//            ,:ADE-IDEN-ISC-FND
			//             :IND-ADE-IDEN-ISC-FND
			//            ,:ADE-NUM-RAT-PIAN
			//             :IND-ADE-NUM-RAT-PIAN
			//            ,:ADE-DT-PRESC-DB
			//             :IND-ADE-DT-PRESC
			//            ,:ADE-CONCS-PREST
			//             :IND-ADE-CONCS-PREST
			//            ,:POL-ID-POLI
			//            ,:POL-ID-MOVI-CRZ
			//            ,:POL-ID-MOVI-CHIU
			//             :IND-POL-ID-MOVI-CHIU
			//            ,:POL-IB-OGG
			//             :IND-POL-IB-OGG
			//            ,:POL-IB-PROP
			//            ,:POL-DT-PROP-DB
			//             :IND-POL-DT-PROP
			//            ,:POL-DT-INI-EFF-DB
			//            ,:POL-DT-END-EFF-DB
			//            ,:POL-COD-COMP-ANIA
			//            ,:POL-DT-DECOR-DB
			//            ,:POL-DT-EMIS-DB
			//            ,:POL-TP-POLI
			//            ,:POL-DUR-AA
			//             :IND-POL-DUR-AA
			//            ,:POL-DUR-MM
			//             :IND-POL-DUR-MM
			//            ,:POL-DT-SCAD-DB
			//             :IND-POL-DT-SCAD
			//            ,:POL-COD-PROD
			//            ,:POL-DT-INI-VLDT-PROD-DB
			//            ,:POL-COD-CONV
			//             :IND-POL-COD-CONV
			//            ,:POL-COD-RAMO
			//             :IND-POL-COD-RAMO
			//            ,:POL-DT-INI-VLDT-CONV-DB
			//             :IND-POL-DT-INI-VLDT-CONV
			//            ,:POL-DT-APPLZ-CONV-DB
			//             :IND-POL-DT-APPLZ-CONV
			//            ,:POL-TP-FRM-ASSVA
			//            ,:POL-TP-RGM-FISC
			//             :IND-POL-TP-RGM-FISC
			//            ,:POL-FL-ESTAS
			//             :IND-POL-FL-ESTAS
			//            ,:POL-FL-RSH-COMUN
			//             :IND-POL-FL-RSH-COMUN
			//            ,:POL-FL-RSH-COMUN-COND
			//             :IND-POL-FL-RSH-COMUN-COND
			//            ,:POL-TP-LIV-GENZ-TIT
			//            ,:POL-FL-COP-FINANZ
			//             :IND-POL-FL-COP-FINANZ
			//            ,:POL-TP-APPLZ-DIR
			//             :IND-POL-TP-APPLZ-DIR
			//            ,:POL-SPE-MED
			//             :IND-POL-SPE-MED
			//            ,:POL-DIR-EMIS
			//             :IND-POL-DIR-EMIS
			//            ,:POL-DIR-1O-VERS
			//             :IND-POL-DIR-1O-VERS
			//            ,:POL-DIR-VERS-AGG
			//             :IND-POL-DIR-VERS-AGG
			//            ,:POL-COD-DVS
			//             :IND-POL-COD-DVS
			//            ,:POL-FL-FNT-AZ
			//             :IND-POL-FL-FNT-AZ
			//            ,:POL-FL-FNT-ADER
			//             :IND-POL-FL-FNT-ADER
			//            ,:POL-FL-FNT-TFR
			//             :IND-POL-FL-FNT-TFR
			//            ,:POL-FL-FNT-VOLO
			//             :IND-POL-FL-FNT-VOLO
			//            ,:POL-TP-OPZ-A-SCAD
			//             :IND-POL-TP-OPZ-A-SCAD
			//            ,:POL-AA-DIFF-PROR-DFLT
			//             :IND-POL-AA-DIFF-PROR-DFLT
			//            ,:POL-FL-VER-PROD
			//             :IND-POL-FL-VER-PROD
			//            ,:POL-DUR-GG
			//             :IND-POL-DUR-GG
			//            ,:POL-DIR-QUIET
			//             :IND-POL-DIR-QUIET
			//            ,:POL-TP-PTF-ESTNO
			//             :IND-POL-TP-PTF-ESTNO
			//            ,:POL-FL-CUM-PRE-CNTR
			//             :IND-POL-FL-CUM-PRE-CNTR
			//            ,:POL-FL-AMMB-MOVI
			//             :IND-POL-FL-AMMB-MOVI
			//            ,:POL-CONV-GECO
			//             :IND-POL-CONV-GECO
			//            ,:POL-DS-RIGA
			//            ,:POL-DS-OPER-SQL
			//            ,:POL-DS-VER
			//            ,:POL-DS-TS-INI-CPTZ
			//            ,:POL-DS-TS-END-CPTZ
			//            ,:POL-DS-UTENTE
			//            ,:POL-DS-STATO-ELAB
			//            ,:POL-FL-SCUDO-FISC
			//             :IND-POL-FL-SCUDO-FISC
			//            ,:POL-FL-TRASFE
			//             :IND-POL-FL-TRASFE
			//            ,:POL-FL-TFR-STRC
			//             :IND-POL-FL-TFR-STRC
			//            ,:POL-DT-PRESC-DB
			//             :IND-POL-DT-PRESC
			//            ,:POL-COD-CONV-AGG
			//             :IND-POL-COD-CONV-AGG
			//            ,:POL-SUBCAT-PROD
			//             :IND-POL-SUBCAT-PROD
			//            ,:POL-FL-QUEST-ADEGZ-ASS
			//             :IND-POL-FL-QUEST-ADEGZ-ASS
			//            ,:POL-COD-TPA
			//             :IND-POL-COD-TPA
			//            ,:POL-ID-ACC-COMM
			//             :IND-POL-ID-ACC-COMM
			//            ,:POL-FL-POLI-CPI-PR
			//             :IND-POL-FL-POLI-CPI-PR
			//            ,:POL-FL-POLI-BUNDLING
			//             :IND-POL-FL-POLI-BUNDLING
			//            ,:POL-IND-POLI-PRIN-COLL
			//             :IND-POL-IND-POLI-PRIN-COLL
			//            ,:POL-FL-VND-BUNDLE
			//             :IND-POL-FL-VND-BUNDLE
			//            ,:POL-IB-BS
			//             :IND-POL-IB-BS
			//            ,:POL-FL-POLI-IFP
			//             :IND-POL-FL-POLI-IFP
			//            ,:STB-ID-STAT-OGG-BUS
			//            ,:STB-ID-OGG
			//            ,:STB-TP-OGG
			//            ,:STB-ID-MOVI-CRZ
			//            ,:STB-ID-MOVI-CHIU
			//             :IND-STB-ID-MOVI-CHIU
			//            ,:STB-DT-INI-EFF-DB
			//            ,:STB-DT-END-EFF-DB
			//            ,:STB-COD-COMP-ANIA
			//            ,:STB-TP-STAT-BUS
			//            ,:STB-TP-CAUS
			//            ,:STB-DS-RIGA
			//            ,:STB-DS-OPER-SQL
			//            ,:STB-DS-VER
			//            ,:STB-DS-TS-INI-CPTZ
			//            ,:STB-DS-TS-END-CPTZ
			//            ,:STB-DS-UTENTE
			//            ,:STB-DS-STATO-ELAB
			//           END-EXEC
			adesPoliStatOggBusDao.fetchCurSc01Range(this.getAdesPoliStatOggBusLdbm0250());
		} else {
			// COB_CODE: EXEC SQL
			//             FETCH CUR-SC01
			//           INTO
			//                :ADE-ID-ADES
			//               ,:ADE-ID-POLI
			//               ,:ADE-ID-MOVI-CRZ
			//               ,:ADE-ID-MOVI-CHIU
			//                :IND-ADE-ID-MOVI-CHIU
			//               ,:ADE-DT-INI-EFF-DB
			//               ,:ADE-DT-END-EFF-DB
			//               ,:ADE-IB-PREV
			//                :IND-ADE-IB-PREV
			//               ,:ADE-IB-OGG
			//                :IND-ADE-IB-OGG
			//               ,:ADE-COD-COMP-ANIA
			//               ,:ADE-DT-DECOR-DB
			//                :IND-ADE-DT-DECOR
			//               ,:ADE-DT-SCAD-DB
			//                :IND-ADE-DT-SCAD
			//               ,:ADE-ETA-A-SCAD
			//                :IND-ADE-ETA-A-SCAD
			//               ,:ADE-DUR-AA
			//                :IND-ADE-DUR-AA
			//               ,:ADE-DUR-MM
			//                :IND-ADE-DUR-MM
			//               ,:ADE-DUR-GG
			//                :IND-ADE-DUR-GG
			//               ,:ADE-TP-RGM-FISC
			//               ,:ADE-TP-RIAT
			//                :IND-ADE-TP-RIAT
			//               ,:ADE-TP-MOD-PAG-TIT
			//               ,:ADE-TP-IAS
			//                :IND-ADE-TP-IAS
			//               ,:ADE-DT-VARZ-TP-IAS-DB
			//                :IND-ADE-DT-VARZ-TP-IAS
			//               ,:ADE-PRE-NET-IND
			//                :IND-ADE-PRE-NET-IND
			//               ,:ADE-PRE-LRD-IND
			//                :IND-ADE-PRE-LRD-IND
			//               ,:ADE-RAT-LRD-IND
			//                :IND-ADE-RAT-LRD-IND
			//               ,:ADE-PRSTZ-INI-IND
			//                :IND-ADE-PRSTZ-INI-IND
			//               ,:ADE-FL-COINC-ASSTO
			//                :IND-ADE-FL-COINC-ASSTO
			//               ,:ADE-IB-DFLT
			//                :IND-ADE-IB-DFLT
			//               ,:ADE-MOD-CALC
			//                :IND-ADE-MOD-CALC
			//               ,:ADE-TP-FNT-CNBTVA
			//                :IND-ADE-TP-FNT-CNBTVA
			//               ,:ADE-IMP-AZ
			//                :IND-ADE-IMP-AZ
			//               ,:ADE-IMP-ADER
			//                :IND-ADE-IMP-ADER
			//               ,:ADE-IMP-TFR
			//                :IND-ADE-IMP-TFR
			//               ,:ADE-IMP-VOLO
			//                :IND-ADE-IMP-VOLO
			//               ,:ADE-PC-AZ
			//                :IND-ADE-PC-AZ
			//               ,:ADE-PC-ADER
			//                :IND-ADE-PC-ADER
			//               ,:ADE-PC-TFR
			//                :IND-ADE-PC-TFR
			//               ,:ADE-PC-VOLO
			//                :IND-ADE-PC-VOLO
			//               ,:ADE-DT-NOVA-RGM-FISC-DB
			//                :IND-ADE-DT-NOVA-RGM-FISC
			//               ,:ADE-FL-ATTIV
			//                :IND-ADE-FL-ATTIV
			//               ,:ADE-IMP-REC-RIT-VIS
			//                :IND-ADE-IMP-REC-RIT-VIS
			//               ,:ADE-IMP-REC-RIT-ACC
			//                :IND-ADE-IMP-REC-RIT-ACC
			//               ,:ADE-FL-VARZ-STAT-TBGC
			//                :IND-ADE-FL-VARZ-STAT-TBGC
			//               ,:ADE-FL-PROVZA-MIGRAZ
			//                :IND-ADE-FL-PROVZA-MIGRAZ
			//               ,:ADE-IMPB-VIS-DA-REC
			//                :IND-ADE-IMPB-VIS-DA-REC
			//               ,:ADE-DT-DECOR-PREST-BAN-DB
			//                :IND-ADE-DT-DECOR-PREST-BAN
			//               ,:ADE-DT-EFF-VARZ-STAT-T-DB
			//                :IND-ADE-DT-EFF-VARZ-STAT-T
			//               ,:ADE-DS-RIGA
			//               ,:ADE-DS-OPER-SQL
			//               ,:ADE-DS-VER
			//               ,:ADE-DS-TS-INI-CPTZ
			//               ,:ADE-DS-TS-END-CPTZ
			//               ,:ADE-DS-UTENTE
			//               ,:ADE-DS-STATO-ELAB
			//               ,:ADE-CUM-CNBT-CAP
			//                :IND-ADE-CUM-CNBT-CAP
			//               ,:ADE-IMP-GAR-CNBT
			//                :IND-ADE-IMP-GAR-CNBT
			//               ,:ADE-DT-ULT-CONS-CNBT-DB
			//                :IND-ADE-DT-ULT-CONS-CNBT
			//               ,:ADE-IDEN-ISC-FND
			//                :IND-ADE-IDEN-ISC-FND
			//               ,:ADE-NUM-RAT-PIAN
			//                :IND-ADE-NUM-RAT-PIAN
			//               ,:ADE-DT-PRESC-DB
			//                :IND-ADE-DT-PRESC
			//               ,:ADE-CONCS-PREST
			//                :IND-ADE-CONCS-PREST
			//               ,:POL-ID-POLI
			//               ,:POL-ID-MOVI-CRZ
			//               ,:POL-ID-MOVI-CHIU
			//                :IND-POL-ID-MOVI-CHIU
			//               ,:POL-IB-OGG
			//                :IND-POL-IB-OGG
			//               ,:POL-IB-PROP
			//               ,:POL-DT-PROP-DB
			//                :IND-POL-DT-PROP
			//               ,:POL-DT-INI-EFF-DB
			//               ,:POL-DT-END-EFF-DB
			//               ,:POL-COD-COMP-ANIA
			//               ,:POL-DT-DECOR-DB
			//               ,:POL-DT-EMIS-DB
			//               ,:POL-TP-POLI
			//               ,:POL-DUR-AA
			//                :IND-POL-DUR-AA
			//               ,:POL-DUR-MM
			//                :IND-POL-DUR-MM
			//               ,:POL-DT-SCAD-DB
			//                :IND-POL-DT-SCAD
			//               ,:POL-COD-PROD
			//               ,:POL-DT-INI-VLDT-PROD-DB
			//               ,:POL-COD-CONV
			//                :IND-POL-COD-CONV
			//               ,:POL-COD-RAMO
			//                :IND-POL-COD-RAMO
			//               ,:POL-DT-INI-VLDT-CONV-DB
			//                :IND-POL-DT-INI-VLDT-CONV
			//               ,:POL-DT-APPLZ-CONV-DB
			//                :IND-POL-DT-APPLZ-CONV
			//               ,:POL-TP-FRM-ASSVA
			//               ,:POL-TP-RGM-FISC
			//                :IND-POL-TP-RGM-FISC
			//               ,:POL-FL-ESTAS
			//                :IND-POL-FL-ESTAS
			//               ,:POL-FL-RSH-COMUN
			//                :IND-POL-FL-RSH-COMUN
			//               ,:POL-FL-RSH-COMUN-COND
			//                :IND-POL-FL-RSH-COMUN-COND
			//               ,:POL-TP-LIV-GENZ-TIT
			//               ,:POL-FL-COP-FINANZ
			//                :IND-POL-FL-COP-FINANZ
			//               ,:POL-TP-APPLZ-DIR
			//                :IND-POL-TP-APPLZ-DIR
			//               ,:POL-SPE-MED
			//                :IND-POL-SPE-MED
			//               ,:POL-DIR-EMIS
			//                :IND-POL-DIR-EMIS
			//               ,:POL-DIR-1O-VERS
			//                :IND-POL-DIR-1O-VERS
			//               ,:POL-DIR-VERS-AGG
			//                :IND-POL-DIR-VERS-AGG
			//               ,:POL-COD-DVS
			//                :IND-POL-COD-DVS
			//               ,:POL-FL-FNT-AZ
			//                :IND-POL-FL-FNT-AZ
			//               ,:POL-FL-FNT-ADER
			//                :IND-POL-FL-FNT-ADER
			//               ,:POL-FL-FNT-TFR
			//                :IND-POL-FL-FNT-TFR
			//               ,:POL-FL-FNT-VOLO
			//                :IND-POL-FL-FNT-VOLO
			//               ,:POL-TP-OPZ-A-SCAD
			//                :IND-POL-TP-OPZ-A-SCAD
			//               ,:POL-AA-DIFF-PROR-DFLT
			//                :IND-POL-AA-DIFF-PROR-DFLT
			//               ,:POL-FL-VER-PROD
			//                :IND-POL-FL-VER-PROD
			//               ,:POL-DUR-GG
			//                :IND-POL-DUR-GG
			//               ,:POL-DIR-QUIET
			//                :IND-POL-DIR-QUIET
			//               ,:POL-TP-PTF-ESTNO
			//                :IND-POL-TP-PTF-ESTNO
			//               ,:POL-FL-CUM-PRE-CNTR
			//                :IND-POL-FL-CUM-PRE-CNTR
			//               ,:POL-FL-AMMB-MOVI
			//                :IND-POL-FL-AMMB-MOVI
			//               ,:POL-CONV-GECO
			//                :IND-POL-CONV-GECO
			//               ,:POL-DS-RIGA
			//               ,:POL-DS-OPER-SQL
			//               ,:POL-DS-VER
			//               ,:POL-DS-TS-INI-CPTZ
			//               ,:POL-DS-TS-END-CPTZ
			//               ,:POL-DS-UTENTE
			//               ,:POL-DS-STATO-ELAB
			//               ,:POL-FL-SCUDO-FISC
			//                :IND-POL-FL-SCUDO-FISC
			//               ,:POL-FL-TRASFE
			//                :IND-POL-FL-TRASFE
			//               ,:POL-FL-TFR-STRC
			//                :IND-POL-FL-TFR-STRC
			//               ,:POL-DT-PRESC-DB
			//                :IND-POL-DT-PRESC
			//               ,:POL-COD-CONV-AGG
			//                :IND-POL-COD-CONV-AGG
			//               ,:POL-SUBCAT-PROD
			//                :IND-POL-SUBCAT-PROD
			//               ,:POL-FL-QUEST-ADEGZ-ASS
			//                :IND-POL-FL-QUEST-ADEGZ-ASS
			//               ,:POL-COD-TPA
			//                :IND-POL-COD-TPA
			//               ,:POL-ID-ACC-COMM
			//                :IND-POL-ID-ACC-COMM
			//               ,:POL-FL-POLI-CPI-PR
			//                :IND-POL-FL-POLI-CPI-PR
			//               ,:POL-FL-POLI-BUNDLING
			//                :IND-POL-FL-POLI-BUNDLING
			//               ,:POL-IND-POLI-PRIN-COLL
			//                :IND-POL-IND-POLI-PRIN-COLL
			//               ,:POL-FL-VND-BUNDLE
			//                :IND-POL-FL-VND-BUNDLE
			//               ,:POL-IB-BS
			//                :IND-POL-IB-BS
			//               ,:POL-FL-POLI-IFP
			//                :IND-POL-FL-POLI-IFP
			//               ,:STB-ID-STAT-OGG-BUS
			//               ,:STB-ID-OGG
			//               ,:STB-TP-OGG
			//               ,:STB-ID-MOVI-CRZ
			//               ,:STB-ID-MOVI-CHIU
			//                :IND-STB-ID-MOVI-CHIU
			//               ,:STB-DT-INI-EFF-DB
			//               ,:STB-DT-END-EFF-DB
			//               ,:STB-COD-COMP-ANIA
			//               ,:STB-TP-STAT-BUS
			//               ,:STB-TP-CAUS
			//               ,:STB-DS-RIGA
			//               ,:STB-DS-OPER-SQL
			//               ,:STB-DS-VER
			//               ,:STB-DS-TS-INI-CPTZ
			//               ,:STB-DS-TS-END-CPTZ
			//               ,:STB-DS-UTENTE
			//               ,:STB-DS-STATO-ELAB
			//           END-EXEC
			adesPoliStatOggBusDao.fetchCurSc01(this.getAdesPoliStatOggBusLdbm0250());
		}
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           ELSE
		//             END-IF
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			z100SetColonneNull();
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		} else if (idsv0003.getSqlcode().isNotFound()) {
			// COB_CODE: IF IDSV0003-NOT-FOUND
			//              END-IF
			//           END-IF
			// COB_CODE: PERFORM A370-CLOSE-CURSOR-SC01 THRU A370-SC01-EX
			a370CloseCursorSc01();
			// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
			//              SET IDSV0003-NOT-FOUND TO TRUE
			//           END-IF
			if (idsv0003.getSqlcode().isSuccessfulSql()) {
				// COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
				idsv0003.getSqlcode().setNotFound();
			}
		}
	}

	/**Original name: A400-FINE<br>
	 * <pre>*****************************************************************</pre>*/
	private void a400Fine() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: SC02-SELECTION-CURSOR-02<br>
	 * <pre>*****************************************************************</pre>*/
	private void sc02SelectionCursor02() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-SELECT
		//                 PERFORM A310-SELECT-SC02           THRU A310-SC02-EX
		//              WHEN IDSV0003-OPEN-CURSOR
		//                 PERFORM A360-OPEN-CURSOR-SC02      THRU A360-SC02-EX
		//              WHEN IDSV0003-CLOSE-CURSOR
		//                 PERFORM A370-CLOSE-CURSOR-SC02     THRU A370-SC02-EX
		//              WHEN IDSV0003-FETCH-FIRST
		//                 PERFORM A380-FETCH-FIRST-SC02      THRU A380-SC02-EX
		//              WHEN IDSV0003-FETCH-NEXT
		//                 PERFORM A390-FETCH-NEXT-SC02       THRU A390-SC02-EX
		//              WHEN IDSV0003-UPDATE
		//                 PERFORM A320-UPDATE-SC02           THRU A320-SC02-EX
		//              WHEN OTHER
		//                 SET IDSV0003-INVALID-OPER          TO TRUE
		//           END-EVALUATE.
		if (idsv0003.getOperazione().isSelect()) {
			// COB_CODE: PERFORM A310-SELECT-SC02           THRU A310-SC02-EX
			a310SelectSc02();
		} else if (idsv0003.getOperazione().isOpenCursor()) {
			// COB_CODE: PERFORM A360-OPEN-CURSOR-SC02      THRU A360-SC02-EX
			a360OpenCursorSc02();
		} else if (idsv0003.getOperazione().isCloseCursor()) {
			// COB_CODE: PERFORM A370-CLOSE-CURSOR-SC02     THRU A370-SC02-EX
			a370CloseCursorSc02();
		} else if (idsv0003.getOperazione().isFetchFirst()) {
			// COB_CODE: PERFORM A380-FETCH-FIRST-SC02      THRU A380-SC02-EX
			a380FetchFirstSc02();
		} else if (idsv0003.getOperazione().isFetchNext()) {
			// COB_CODE: PERFORM A390-FETCH-NEXT-SC02       THRU A390-SC02-EX
			a390FetchNextSc02();
		} else if (idsv0003.getOperazione().isUpdate()) {
			// COB_CODE: PERFORM A320-UPDATE-SC02           THRU A320-SC02-EX
			a320UpdateSc02();
		} else {
			// COB_CODE: SET IDSV0003-INVALID-OPER          TO TRUE
			idsv0003.getReturnCode().setInvalidOper();
		}
	}

	/**Original name: A305-DECLARE-CURSOR-SC02<br>
	 * <pre>*****************************************************************</pre>*/
	private void a305DeclareCursorSc02() {
		// COB_CODE:      EXEC SQL
		//                     DECLARE CUR-SC02 CURSOR WITH HOLD FOR
		//                  SELECT
		//                      A.ID_ADES
		//                     ,A.ID_POLI
		//                     ,A.ID_MOVI_CRZ
		//                     ,A.ID_MOVI_CHIU
		//                     ,A.DT_INI_EFF
		//                     ,A.DT_END_EFF
		//                     ,A.IB_PREV
		//                     ,A.IB_OGG
		//                     ,A.COD_COMP_ANIA
		//                     ,A.DT_DECOR
		//                     ,A.DT_SCAD
		//                     ,A.ETA_A_SCAD
		//                     ,A.DUR_AA
		//                     ,A.DUR_MM
		//                     ,A.DUR_GG
		//                     ,A.TP_RGM_FISC
		//                     ,A.TP_RIAT
		//                     ,A.TP_MOD_PAG_TIT
		//                     ,A.TP_IAS
		//                     ,A.DT_VARZ_TP_IAS
		//                     ,A.PRE_NET_IND
		//                     ,A.PRE_LRD_IND
		//                     ,A.RAT_LRD_IND
		//                     ,A.PRSTZ_INI_IND
		//                     ,A.FL_COINC_ASSTO
		//                     ,A.IB_DFLT
		//                     ,A.MOD_CALC
		//                     ,A.TP_FNT_CNBTVA
		//                     ,A.IMP_AZ
		//                     ,A.IMP_ADER
		//                     ,A.IMP_TFR
		//                     ,A.IMP_VOLO
		//                     ,A.PC_AZ
		//                     ,A.PC_ADER
		//                     ,A.PC_TFR
		//                     ,A.PC_VOLO
		//                     ,A.DT_NOVA_RGM_FISC
		//                     ,A.FL_ATTIV
		//                     ,A.IMP_REC_RIT_VIS
		//                     ,A.IMP_REC_RIT_ACC
		//                     ,A.FL_VARZ_STAT_TBGC
		//                     ,A.FL_PROVZA_MIGRAZ
		//                     ,A.IMPB_VIS_DA_REC
		//                     ,A.DT_DECOR_PREST_BAN
		//                     ,A.DT_EFF_VARZ_STAT_T
		//                     ,A.DS_RIGA
		//                     ,A.DS_OPER_SQL
		//                     ,A.DS_VER
		//                     ,A.DS_TS_INI_CPTZ
		//                     ,A.DS_TS_END_CPTZ
		//                     ,A.DS_UTENTE
		//                     ,A.DS_STATO_ELAB
		//                     ,A.CUM_CNBT_CAP
		//                     ,A.IMP_GAR_CNBT
		//                     ,A.DT_ULT_CONS_CNBT
		//                     ,A.IDEN_ISC_FND
		//                     ,A.NUM_RAT_PIAN
		//                     ,A.DT_PRESC
		//                     ,A.CONCS_PREST
		//                     ,B.ID_POLI
		//                     ,B.ID_MOVI_CRZ
		//                     ,B.ID_MOVI_CHIU
		//                     ,B.IB_OGG
		//                     ,B.IB_PROP
		//                     ,B.DT_PROP
		//                     ,B.DT_INI_EFF
		//                     ,B.DT_END_EFF
		//                     ,B.COD_COMP_ANIA
		//                     ,B.DT_DECOR
		//                     ,B.DT_EMIS
		//                     ,B.TP_POLI
		//                     ,B.DUR_AA
		//                     ,B.DUR_MM
		//                     ,B.DT_SCAD
		//                     ,B.COD_PROD
		//                     ,B.DT_INI_VLDT_PROD
		//                     ,B.COD_CONV
		//                     ,B.COD_RAMO
		//                     ,B.DT_INI_VLDT_CONV
		//                     ,B.DT_APPLZ_CONV
		//                     ,B.TP_FRM_ASSVA
		//                     ,B.TP_RGM_FISC
		//                     ,B.FL_ESTAS
		//                     ,B.FL_RSH_COMUN
		//                     ,B.FL_RSH_COMUN_COND
		//                     ,B.TP_LIV_GENZ_TIT
		//                     ,B.FL_COP_FINANZ
		//                     ,B.TP_APPLZ_DIR
		//                     ,B.SPE_MED
		//                     ,B.DIR_EMIS
		//                     ,B.DIR_1O_VERS
		//                     ,B.DIR_VERS_AGG
		//                     ,B.COD_DVS
		//                     ,B.FL_FNT_AZ
		//                     ,B.FL_FNT_ADER
		//                     ,B.FL_FNT_TFR
		//                     ,B.FL_FNT_VOLO
		//                     ,B.TP_OPZ_A_SCAD
		//                     ,B.AA_DIFF_PROR_DFLT
		//                     ,B.FL_VER_PROD
		//                     ,B.DUR_GG
		//                     ,B.DIR_QUIET
		//                     ,B.TP_PTF_ESTNO
		//                     ,B.FL_CUM_PRE_CNTR
		//                     ,B.FL_AMMB_MOVI
		//                     ,B.CONV_GECO
		//                     ,B.DS_RIGA
		//                     ,B.DS_OPER_SQL
		//                     ,B.DS_VER
		//                     ,B.DS_TS_INI_CPTZ
		//                     ,B.DS_TS_END_CPTZ
		//                     ,B.DS_UTENTE
		//                     ,B.DS_STATO_ELAB
		//                     ,B.FL_SCUDO_FISC
		//                     ,B.FL_TRASFE
		//                     ,B.FL_TFR_STRC
		//                     ,B.DT_PRESC
		//                     ,B.COD_CONV_AGG
		//                     ,B.SUBCAT_PROD
		//                     ,B.FL_QUEST_ADEGZ_ASS
		//                     ,B.COD_TPA
		//                     ,B.ID_ACC_COMM
		//                     ,B.FL_POLI_CPI_PR
		//                     ,B.FL_POLI_BUNDLING
		//                     ,B.IND_POLI_PRIN_COLL
		//                     ,B.FL_VND_BUNDLE
		//                     ,B.IB_BS
		//                     ,B.FL_POLI_IFP
		//                     ,C.ID_STAT_OGG_BUS
		//                     ,C.ID_OGG
		//                     ,C.TP_OGG
		//                     ,C.ID_MOVI_CRZ
		//                     ,C.ID_MOVI_CHIU
		//                     ,C.DT_INI_EFF
		//                     ,C.DT_END_EFF
		//                     ,C.COD_COMP_ANIA
		//                     ,C.TP_STAT_BUS
		//                     ,C.TP_CAUS
		//                     ,C.DS_RIGA
		//                     ,C.DS_OPER_SQL
		//                     ,C.DS_VER
		//                     ,C.DS_TS_INI_CPTZ
		//                     ,C.DS_TS_END_CPTZ
		//                     ,C.DS_UTENTE
		//                     ,C.DS_STATO_ELAB
		//                  FROM ADES         A,
		//                       POLI         B,
		//                       STAT_OGG_BUS C
		//                  WHERE B.COD_PROD        =  :WLB-COD-PROD
		//                    AND A.ID_POLI         =   B.ID_POLI
		//                    AND A.ID_ADES         =   C.ID_OGG
		//                    AND C.TP_OGG          =  'AD'
		//                    AND C.TP_STAT_BUS     =  'VI'
		//           *        OR
		//           *            (C.TP_STAT_BUS    =  'ST'  AND
		//           *            EXISTS (SELECT
		//           *                  D.ID_MOVI_FINRIO
		//           *
		//           *               FROM  MOVI_FINRIO D
		//           *               WHERE A.ID_ADES = D.ID_ADES
		//           *                 AND D.STAT_MOVI IN ('AL','IE')
		//           *                 AND D.TP_MOVI_FINRIO = 'DI'
		//           *                 AND D.COD_COMP_ANIA   =
		//           *                     :IDSV0003-CODICE-COMPAGNIA-ANIA
		//           *                 AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
		//           *                 AND D.DT_END_EFF     >   :WS-DT-PTF-X
		//           *                 AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//           *                 AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
		//                  AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
		//                  AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
		//                  AND A.DT_END_EFF     >   :WS-DT-PTF-X
		//                  AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//                  AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
		//                  AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
		//                  AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
		//                  AND B.DT_END_EFF     >   :WS-DT-PTF-X
		//                  AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//                  AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
		//                  AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
		//                  AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
		//                  AND C.DT_END_EFF     >   :WS-DT-PTF-X
		//                  AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//                  AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
		//           *      AND A.DS_STATO_ELAB IN (
		//           *                              :IABV0002-STATE-01,
		//           *                              :IABV0002-STATE-02,
		//           *                              :IABV0002-STATE-03,
		//           *                              :IABV0002-STATE-04,
		//           *                              :IABV0002-STATE-05,
		//           *                              :IABV0002-STATE-06,
		//           *                              :IABV0002-STATE-07,
		//           *                              :IABV0002-STATE-08,
		//           *                              :IABV0002-STATE-09,
		//           *                              :IABV0002-STATE-10
		//           *                                  )
		//           *        AND NOT A.DS_VER     = :IABV0009-VERSIONING
		//                    ORDER BY A.ID_POLI, A.ID_ADES
		//                END-EXEC.
		// DECLARE CURSOR doesn't need a translation;
	}

	/**Original name: A310-SELECT-SC02<br>
	 * <pre>*****************************************************************
	 *       AND A.DS_STATO_ELAB IN (
	 *                               :IABV0002-STATE-01,
	 *                               :IABV0002-STATE-02,
	 *                               :IABV0002-STATE-03,
	 *                               :IABV0002-STATE-04,
	 *                               :IABV0002-STATE-05,
	 *                               :IABV0002-STATE-06,
	 *                               :IABV0002-STATE-07,
	 *                               :IABV0002-STATE-08,
	 *                               :IABV0002-STATE-09,
	 *                               :IABV0002-STATE-10
	 *                                   )
	 *         AND NOT A.DS_VER     = :IABV0009-VERSIONING</pre>*/
	private void a310SelectSc02() {
		// COB_CODE:      EXEC SQL
		//                  SELECT
		//                      A.ID_ADES
		//                     ,A.ID_POLI
		//                     ,A.ID_MOVI_CRZ
		//                     ,A.ID_MOVI_CHIU
		//                     ,A.DT_INI_EFF
		//                     ,A.DT_END_EFF
		//                     ,A.IB_PREV
		//                     ,A.IB_OGG
		//                     ,A.COD_COMP_ANIA
		//                     ,A.DT_DECOR
		//                     ,A.DT_SCAD
		//                     ,A.ETA_A_SCAD
		//                     ,A.DUR_AA
		//                     ,A.DUR_MM
		//                     ,A.DUR_GG
		//                     ,A.TP_RGM_FISC
		//                     ,A.TP_RIAT
		//                     ,A.TP_MOD_PAG_TIT
		//                     ,A.TP_IAS
		//                     ,A.DT_VARZ_TP_IAS
		//                     ,A.PRE_NET_IND
		//                     ,A.PRE_LRD_IND
		//                     ,A.RAT_LRD_IND
		//                     ,A.PRSTZ_INI_IND
		//                     ,A.FL_COINC_ASSTO
		//                     ,A.IB_DFLT
		//                     ,A.MOD_CALC
		//                     ,A.TP_FNT_CNBTVA
		//                     ,A.IMP_AZ
		//                     ,A.IMP_ADER
		//                     ,A.IMP_TFR
		//                     ,A.IMP_VOLO
		//                     ,A.PC_AZ
		//                     ,A.PC_ADER
		//                     ,A.PC_TFR
		//                     ,A.PC_VOLO
		//                     ,A.DT_NOVA_RGM_FISC
		//                     ,A.FL_ATTIV
		//                     ,A.IMP_REC_RIT_VIS
		//                     ,A.IMP_REC_RIT_ACC
		//                     ,A.FL_VARZ_STAT_TBGC
		//                     ,A.FL_PROVZA_MIGRAZ
		//                     ,A.IMPB_VIS_DA_REC
		//                     ,A.DT_DECOR_PREST_BAN
		//                     ,A.DT_EFF_VARZ_STAT_T
		//                     ,A.DS_RIGA
		//                     ,A.DS_OPER_SQL
		//                     ,A.DS_VER
		//                     ,A.DS_TS_INI_CPTZ
		//                     ,A.DS_TS_END_CPTZ
		//                     ,A.DS_UTENTE
		//                     ,A.DS_STATO_ELAB
		//                     ,A.CUM_CNBT_CAP
		//                     ,A.IMP_GAR_CNBT
		//                     ,A.DT_ULT_CONS_CNBT
		//                     ,A.IDEN_ISC_FND
		//                     ,A.NUM_RAT_PIAN
		//                     ,A.DT_PRESC
		//                     ,A.CONCS_PREST
		//                     ,B.ID_POLI
		//                     ,B.ID_MOVI_CRZ
		//                     ,B.ID_MOVI_CHIU
		//                     ,B.IB_OGG
		//                     ,B.IB_PROP
		//                     ,B.DT_PROP
		//                     ,B.DT_INI_EFF
		//                     ,B.DT_END_EFF
		//                     ,B.COD_COMP_ANIA
		//                     ,B.DT_DECOR
		//                     ,B.DT_EMIS
		//                     ,B.TP_POLI
		//                     ,B.DUR_AA
		//                     ,B.DUR_MM
		//                     ,B.DT_SCAD
		//                     ,B.COD_PROD
		//                     ,B.DT_INI_VLDT_PROD
		//                     ,B.COD_CONV
		//                     ,B.COD_RAMO
		//                     ,B.DT_INI_VLDT_CONV
		//                     ,B.DT_APPLZ_CONV
		//                     ,B.TP_FRM_ASSVA
		//                     ,B.TP_RGM_FISC
		//                     ,B.FL_ESTAS
		//                     ,B.FL_RSH_COMUN
		//                     ,B.FL_RSH_COMUN_COND
		//                     ,B.TP_LIV_GENZ_TIT
		//                     ,B.FL_COP_FINANZ
		//                     ,B.TP_APPLZ_DIR
		//                     ,B.SPE_MED
		//                     ,B.DIR_EMIS
		//                     ,B.DIR_1O_VERS
		//                     ,B.DIR_VERS_AGG
		//                     ,B.COD_DVS
		//                     ,B.FL_FNT_AZ
		//                     ,B.FL_FNT_ADER
		//                     ,B.FL_FNT_TFR
		//                     ,B.FL_FNT_VOLO
		//                     ,B.TP_OPZ_A_SCAD
		//                     ,B.AA_DIFF_PROR_DFLT
		//                     ,B.FL_VER_PROD
		//                     ,B.DUR_GG
		//                     ,B.DIR_QUIET
		//                     ,B.TP_PTF_ESTNO
		//                     ,B.FL_CUM_PRE_CNTR
		//                     ,B.FL_AMMB_MOVI
		//                     ,B.CONV_GECO
		//                     ,B.DS_RIGA
		//                     ,B.DS_OPER_SQL
		//                     ,B.DS_VER
		//                     ,B.DS_TS_INI_CPTZ
		//                     ,B.DS_TS_END_CPTZ
		//                     ,B.DS_UTENTE
		//                     ,B.DS_STATO_ELAB
		//                     ,B.FL_SCUDO_FISC
		//                     ,B.FL_TRASFE
		//                     ,B.FL_TFR_STRC
		//                     ,B.DT_PRESC
		//                     ,B.COD_CONV_AGG
		//                     ,B.SUBCAT_PROD
		//                     ,B.FL_QUEST_ADEGZ_ASS
		//                     ,B.COD_TPA
		//                     ,B.ID_ACC_COMM
		//                     ,B.FL_POLI_CPI_PR
		//                     ,B.FL_POLI_BUNDLING
		//                     ,B.IND_POLI_PRIN_COLL
		//                     ,B.FL_VND_BUNDLE
		//                     ,B.IB_BS
		//                     ,B.FL_POLI_IFP
		//                     ,C.ID_STAT_OGG_BUS
		//                     ,C.ID_OGG
		//                     ,C.TP_OGG
		//                     ,C.ID_MOVI_CRZ
		//                     ,C.ID_MOVI_CHIU
		//                     ,C.DT_INI_EFF
		//                     ,C.DT_END_EFF
		//                     ,C.COD_COMP_ANIA
		//                     ,C.TP_STAT_BUS
		//                     ,C.TP_CAUS
		//                     ,C.DS_RIGA
		//                     ,C.DS_OPER_SQL
		//                     ,C.DS_VER
		//                     ,C.DS_TS_INI_CPTZ
		//                     ,C.DS_TS_END_CPTZ
		//                     ,C.DS_UTENTE
		//                     ,C.DS_STATO_ELAB
		//                  INTO
		//                     :ADE-ID-ADES
		//                    ,:ADE-ID-POLI
		//                    ,:ADE-ID-MOVI-CRZ
		//                    ,:ADE-ID-MOVI-CHIU
		//                     :IND-ADE-ID-MOVI-CHIU
		//                    ,:ADE-DT-INI-EFF-DB
		//                    ,:ADE-DT-END-EFF-DB
		//                    ,:ADE-IB-PREV
		//                     :IND-ADE-IB-PREV
		//                    ,:ADE-IB-OGG
		//                     :IND-ADE-IB-OGG
		//                    ,:ADE-COD-COMP-ANIA
		//                    ,:ADE-DT-DECOR-DB
		//                     :IND-ADE-DT-DECOR
		//                    ,:ADE-DT-SCAD-DB
		//                     :IND-ADE-DT-SCAD
		//                    ,:ADE-ETA-A-SCAD
		//                     :IND-ADE-ETA-A-SCAD
		//                    ,:ADE-DUR-AA
		//                     :IND-ADE-DUR-AA
		//                    ,:ADE-DUR-MM
		//                     :IND-ADE-DUR-MM
		//                    ,:ADE-DUR-GG
		//                     :IND-ADE-DUR-GG
		//                    ,:ADE-TP-RGM-FISC
		//                    ,:ADE-TP-RIAT
		//                     :IND-ADE-TP-RIAT
		//                    ,:ADE-TP-MOD-PAG-TIT
		//                    ,:ADE-TP-IAS
		//                     :IND-ADE-TP-IAS
		//                    ,:ADE-DT-VARZ-TP-IAS-DB
		//                     :IND-ADE-DT-VARZ-TP-IAS
		//                    ,:ADE-PRE-NET-IND
		//                     :IND-ADE-PRE-NET-IND
		//                    ,:ADE-PRE-LRD-IND
		//                     :IND-ADE-PRE-LRD-IND
		//                    ,:ADE-RAT-LRD-IND
		//                     :IND-ADE-RAT-LRD-IND
		//                    ,:ADE-PRSTZ-INI-IND
		//                     :IND-ADE-PRSTZ-INI-IND
		//                    ,:ADE-FL-COINC-ASSTO
		//                     :IND-ADE-FL-COINC-ASSTO
		//                    ,:ADE-IB-DFLT
		//                     :IND-ADE-IB-DFLT
		//                    ,:ADE-MOD-CALC
		//                     :IND-ADE-MOD-CALC
		//                    ,:ADE-TP-FNT-CNBTVA
		//                     :IND-ADE-TP-FNT-CNBTVA
		//                    ,:ADE-IMP-AZ
		//                     :IND-ADE-IMP-AZ
		//                    ,:ADE-IMP-ADER
		//                     :IND-ADE-IMP-ADER
		//                    ,:ADE-IMP-TFR
		//                     :IND-ADE-IMP-TFR
		//                    ,:ADE-IMP-VOLO
		//                     :IND-ADE-IMP-VOLO
		//                    ,:ADE-PC-AZ
		//                     :IND-ADE-PC-AZ
		//                    ,:ADE-PC-ADER
		//                     :IND-ADE-PC-ADER
		//                    ,:ADE-PC-TFR
		//                     :IND-ADE-PC-TFR
		//                    ,:ADE-PC-VOLO
		//                     :IND-ADE-PC-VOLO
		//                    ,:ADE-DT-NOVA-RGM-FISC-DB
		//                     :IND-ADE-DT-NOVA-RGM-FISC
		//                    ,:ADE-FL-ATTIV
		//                     :IND-ADE-FL-ATTIV
		//                    ,:ADE-IMP-REC-RIT-VIS
		//                     :IND-ADE-IMP-REC-RIT-VIS
		//                    ,:ADE-IMP-REC-RIT-ACC
		//                     :IND-ADE-IMP-REC-RIT-ACC
		//                    ,:ADE-FL-VARZ-STAT-TBGC
		//                     :IND-ADE-FL-VARZ-STAT-TBGC
		//                    ,:ADE-FL-PROVZA-MIGRAZ
		//                     :IND-ADE-FL-PROVZA-MIGRAZ
		//                    ,:ADE-IMPB-VIS-DA-REC
		//                     :IND-ADE-IMPB-VIS-DA-REC
		//                    ,:ADE-DT-DECOR-PREST-BAN-DB
		//                     :IND-ADE-DT-DECOR-PREST-BAN
		//                    ,:ADE-DT-EFF-VARZ-STAT-T-DB
		//                     :IND-ADE-DT-EFF-VARZ-STAT-T
		//                    ,:ADE-DS-RIGA
		//                    ,:ADE-DS-OPER-SQL
		//                    ,:ADE-DS-VER
		//                    ,:ADE-DS-TS-INI-CPTZ
		//                    ,:ADE-DS-TS-END-CPTZ
		//                    ,:ADE-DS-UTENTE
		//                    ,:ADE-DS-STATO-ELAB
		//                    ,:ADE-CUM-CNBT-CAP
		//                     :IND-ADE-CUM-CNBT-CAP
		//                    ,:ADE-IMP-GAR-CNBT
		//                     :IND-ADE-IMP-GAR-CNBT
		//                    ,:ADE-DT-ULT-CONS-CNBT-DB
		//                     :IND-ADE-DT-ULT-CONS-CNBT
		//                    ,:ADE-IDEN-ISC-FND
		//                     :IND-ADE-IDEN-ISC-FND
		//                    ,:ADE-NUM-RAT-PIAN
		//                     :IND-ADE-NUM-RAT-PIAN
		//                    ,:ADE-DT-PRESC-DB
		//                     :IND-ADE-DT-PRESC
		//                    ,:ADE-CONCS-PREST
		//                     :IND-ADE-CONCS-PREST
		//                    ,:POL-ID-POLI
		//                    ,:POL-ID-MOVI-CRZ
		//                    ,:POL-ID-MOVI-CHIU
		//                     :IND-POL-ID-MOVI-CHIU
		//                    ,:POL-IB-OGG
		//                     :IND-POL-IB-OGG
		//                    ,:POL-IB-PROP
		//                    ,:POL-DT-PROP-DB
		//                     :IND-POL-DT-PROP
		//                    ,:POL-DT-INI-EFF-DB
		//                    ,:POL-DT-END-EFF-DB
		//                    ,:POL-COD-COMP-ANIA
		//                    ,:POL-DT-DECOR-DB
		//                    ,:POL-DT-EMIS-DB
		//                    ,:POL-TP-POLI
		//                    ,:POL-DUR-AA
		//                     :IND-POL-DUR-AA
		//                    ,:POL-DUR-MM
		//                     :IND-POL-DUR-MM
		//                    ,:POL-DT-SCAD-DB
		//                     :IND-POL-DT-SCAD
		//                    ,:POL-COD-PROD
		//                    ,:POL-DT-INI-VLDT-PROD-DB
		//                    ,:POL-COD-CONV
		//                     :IND-POL-COD-CONV
		//                    ,:POL-COD-RAMO
		//                     :IND-POL-COD-RAMO
		//                    ,:POL-DT-INI-VLDT-CONV-DB
		//                     :IND-POL-DT-INI-VLDT-CONV
		//                    ,:POL-DT-APPLZ-CONV-DB
		//                     :IND-POL-DT-APPLZ-CONV
		//                    ,:POL-TP-FRM-ASSVA
		//                    ,:POL-TP-RGM-FISC
		//                     :IND-POL-TP-RGM-FISC
		//                    ,:POL-FL-ESTAS
		//                     :IND-POL-FL-ESTAS
		//                    ,:POL-FL-RSH-COMUN
		//                     :IND-POL-FL-RSH-COMUN
		//                    ,:POL-FL-RSH-COMUN-COND
		//                     :IND-POL-FL-RSH-COMUN-COND
		//                    ,:POL-TP-LIV-GENZ-TIT
		//                    ,:POL-FL-COP-FINANZ
		//                     :IND-POL-FL-COP-FINANZ
		//                    ,:POL-TP-APPLZ-DIR
		//                     :IND-POL-TP-APPLZ-DIR
		//                    ,:POL-SPE-MED
		//                     :IND-POL-SPE-MED
		//                    ,:POL-DIR-EMIS
		//                     :IND-POL-DIR-EMIS
		//                    ,:POL-DIR-1O-VERS
		//                     :IND-POL-DIR-1O-VERS
		//                    ,:POL-DIR-VERS-AGG
		//                     :IND-POL-DIR-VERS-AGG
		//                    ,:POL-COD-DVS
		//                     :IND-POL-COD-DVS
		//                    ,:POL-FL-FNT-AZ
		//                     :IND-POL-FL-FNT-AZ
		//                    ,:POL-FL-FNT-ADER
		//                     :IND-POL-FL-FNT-ADER
		//                    ,:POL-FL-FNT-TFR
		//                     :IND-POL-FL-FNT-TFR
		//                    ,:POL-FL-FNT-VOLO
		//                     :IND-POL-FL-FNT-VOLO
		//                    ,:POL-TP-OPZ-A-SCAD
		//                     :IND-POL-TP-OPZ-A-SCAD
		//                    ,:POL-AA-DIFF-PROR-DFLT
		//                     :IND-POL-AA-DIFF-PROR-DFLT
		//                    ,:POL-FL-VER-PROD
		//                     :IND-POL-FL-VER-PROD
		//                    ,:POL-DUR-GG
		//                     :IND-POL-DUR-GG
		//                    ,:POL-DIR-QUIET
		//                     :IND-POL-DIR-QUIET
		//                    ,:POL-TP-PTF-ESTNO
		//                     :IND-POL-TP-PTF-ESTNO
		//                    ,:POL-FL-CUM-PRE-CNTR
		//                     :IND-POL-FL-CUM-PRE-CNTR
		//                    ,:POL-FL-AMMB-MOVI
		//                     :IND-POL-FL-AMMB-MOVI
		//                    ,:POL-CONV-GECO
		//                     :IND-POL-CONV-GECO
		//                    ,:POL-DS-RIGA
		//                    ,:POL-DS-OPER-SQL
		//                    ,:POL-DS-VER
		//                    ,:POL-DS-TS-INI-CPTZ
		//                    ,:POL-DS-TS-END-CPTZ
		//                    ,:POL-DS-UTENTE
		//                    ,:POL-DS-STATO-ELAB
		//                    ,:POL-FL-SCUDO-FISC
		//                     :IND-POL-FL-SCUDO-FISC
		//                    ,:POL-FL-TRASFE
		//                     :IND-POL-FL-TRASFE
		//                    ,:POL-FL-TFR-STRC
		//                     :IND-POL-FL-TFR-STRC
		//                    ,:POL-DT-PRESC-DB
		//                     :IND-POL-DT-PRESC
		//                    ,:POL-COD-CONV-AGG
		//                     :IND-POL-COD-CONV-AGG
		//                    ,:POL-SUBCAT-PROD
		//                     :IND-POL-SUBCAT-PROD
		//                    ,:POL-FL-QUEST-ADEGZ-ASS
		//                     :IND-POL-FL-QUEST-ADEGZ-ASS
		//                    ,:POL-COD-TPA
		//                     :IND-POL-COD-TPA
		//                    ,:POL-ID-ACC-COMM
		//                     :IND-POL-ID-ACC-COMM
		//                    ,:POL-FL-POLI-CPI-PR
		//                     :IND-POL-FL-POLI-CPI-PR
		//                    ,:POL-FL-POLI-BUNDLING
		//                     :IND-POL-FL-POLI-BUNDLING
		//                    ,:POL-IND-POLI-PRIN-COLL
		//                     :IND-POL-IND-POLI-PRIN-COLL
		//                    ,:POL-FL-VND-BUNDLE
		//                     :IND-POL-FL-VND-BUNDLE
		//                    ,:POL-IB-BS
		//                     :IND-POL-IB-BS
		//                    ,:POL-FL-POLI-IFP
		//                     :IND-POL-FL-POLI-IFP
		//                    ,:STB-ID-STAT-OGG-BUS
		//                    ,:STB-ID-OGG
		//                    ,:STB-TP-OGG
		//                    ,:STB-ID-MOVI-CRZ
		//                    ,:STB-ID-MOVI-CHIU
		//                     :IND-STB-ID-MOVI-CHIU
		//                    ,:STB-DT-INI-EFF-DB
		//                    ,:STB-DT-END-EFF-DB
		//                    ,:STB-COD-COMP-ANIA
		//                    ,:STB-TP-STAT-BUS
		//                    ,:STB-TP-CAUS
		//                    ,:STB-DS-RIGA
		//                    ,:STB-DS-OPER-SQL
		//                    ,:STB-DS-VER
		//                    ,:STB-DS-TS-INI-CPTZ
		//                    ,:STB-DS-TS-END-CPTZ
		//                    ,:STB-DS-UTENTE
		//                    ,:STB-DS-STATO-ELAB
		//                  FROM ADES         A,
		//                       POLI         B,
		//                       STAT_OGG_BUS C
		//                  WHERE B.COD_PROD        =  :WLB-COD-PROD
		//                    AND A.ID_POLI         =   B.ID_POLI
		//                    AND A.ID_ADES         =   C.ID_OGG
		//                    AND C.TP_OGG          =  'AD'
		//                    AND C.TP_STAT_BUS     =  'VI'
		//           *        OR
		//           *            (C.TP_STAT_BUS    =  'ST'  AND
		//           *            EXISTS (SELECT
		//           *                  D.ID_MOVI_FINRIO
		//           *
		//           *               FROM  MOVI_FINRIO D
		//           *               WHERE A.ID_ADES = D.ID_ADES
		//           *                 AND D.STAT_MOVI IN ('AL','IE')
		//           *                 AND D.TP_MOVI_FINRIO = 'DI'
		//           *                 AND D.COD_COMP_ANIA   =
		//           *                     :IDSV0003-CODICE-COMPAGNIA-ANIA
		//           *                 AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
		//           *                 AND D.DT_END_EFF     >   :WS-DT-PTF-X
		//           *                 AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//           *                 AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
		//                  AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
		//                  AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
		//                  AND A.DT_END_EFF     >   :WS-DT-PTF-X
		//                  AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//                  AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
		//                  AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
		//                  AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
		//                  AND B.DT_END_EFF     >   :WS-DT-PTF-X
		//                  AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//                  AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
		//                  AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
		//                  AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
		//                  AND C.DT_END_EFF     >   :WS-DT-PTF-X
		//                  AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//                  AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
		//           *      AND A.DS_STATO_ELAB IN (
		//           *                              :IABV0002-STATE-01,
		//           *                              :IABV0002-STATE-02,
		//           *                              :IABV0002-STATE-03,
		//           *                              :IABV0002-STATE-04,
		//           *                              :IABV0002-STATE-05,
		//           *                              :IABV0002-STATE-06,
		//           *                              :IABV0002-STATE-07,
		//           *                              :IABV0002-STATE-08,
		//           *                              :IABV0002-STATE-09,
		//           *                              :IABV0002-STATE-10
		//           *                                  )
		//           *        AND NOT A.DS_VER     = :IABV0009-VERSIONING
		//                    ORDER BY A.ID_POLI, A.ID_ADES
		//                   FETCH FIRST ROW ONLY
		//                END-EXEC.
		adesPoliStatOggBusDao.selectRec2(ws.getWlbRecPren().getCodProd(), idsv0003.getCodiceCompagniaAnia(), ws.getWsDtPtfX(), ws.getWsDtTsPtf(),
				this.getAdesPoliStatOggBusLdbm0250());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			z100SetColonneNull();
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		}
	}

	/**Original name: A320-UPDATE-SC02<br>
	 * <pre>*****************************************************************</pre>*/
	private void a320UpdateSc02() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-PRIMARY-KEY
		//                   PERFORM A330-UPDATE-PK-SC02         THRU A330-SC02-EX
		//              WHEN IDSV0003-WHERE-CONDITION-02
		//                   PERFORM A340-UPDATE-WHERE-COND-SC02 THRU A340-SC02-EX
		//              WHEN IDSV0003-FIRST-ACTION
		//                                                       THRU A345-SC02-EX
		//              WHEN OTHER
		//                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
		//           END-EVALUATE.
		if (idsv0003.getLivelloOperazione().isIdsv0003PrimaryKey()) {
			// COB_CODE: PERFORM A330-UPDATE-PK-SC02         THRU A330-SC02-EX
			a330UpdatePkSc02();
		} else if (idsv0003.getTipologiaOperazione().isIdsv0003WhereCondition02()) {
			// COB_CODE: PERFORM A340-UPDATE-WHERE-COND-SC02 THRU A340-SC02-EX
			a340UpdateWhereCondSc02();
		} else if (idsv0003.getLivelloOperazione().isIdsv0003FirstAction()) {
			// COB_CODE: PERFORM A345-UPDATE-FIRST-ACTION-SC02
			//                                               THRU A345-SC02-EX
			a345UpdateFirstActionSc02();
		} else {
			// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
			idsv0003.getReturnCode().setInvalidLevelOper();
		}
	}

	/**Original name: A330-UPDATE-PK-SC02<br>
	 * <pre>*****************************************************************</pre>*/
	private void a330UpdatePkSc02() {
		// COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
		z150ValorizzaDataServices();
		// COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
		z200SetIndicatoriNull();
		// COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBM0250.cbl:line=3039, because the code is unreachable.
		// COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
		z960LengthVchar();
		//           ,DS_VER                 = :IABV0009-VERSIONING
		// COB_CODE:      EXEC SQL
		//                     UPDATE ADES
		//                     SET
		//                        DS_OPER_SQL            = :ADE-DS-OPER-SQL
		//           *           ,DS_VER                 = :IABV0009-VERSIONING
		//                       ,DS_UTENTE              = :ADE-DS-UTENTE
		//                       ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
		//                     WHERE
		//                        DS_RIGA                = :ADE-DS-RIGA
		//                END-EXEC.
		adesDao.updateRec2(ades.getAdeDsOperSql(), ades.getAdeDsUtente(), iabv0002.getIabv0002StateCurrent(), ades.getAdeDsRiga());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A340-UPDATE-WHERE-COND-SC02<br>
	 * <pre>*****************************************************************</pre>*/
	private void a340UpdateWhereCondSc02() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A345-UPDATE-FIRST-ACTION-SC02<br>
	 * <pre>*****************************************************************</pre>*/
	private void a345UpdateFirstActionSc02() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A360-OPEN-CURSOR-SC02<br>
	 * <pre>*****************************************************************</pre>*/
	private void a360OpenCursorSc02() {
		// COB_CODE: PERFORM A305-DECLARE-CURSOR-SC02 THRU A305-SC02-EX.
		a305DeclareCursorSc02();
		// COB_CODE: EXEC SQL
		//                OPEN CUR-SC02
		//           END-EXEC.
		adesPoliStatOggBusDao.openCurSc02(ws.getWlbRecPren().getCodProd(), idsv0003.getCodiceCompagniaAnia(), ws.getWsDtPtfX(), ws.getWsDtTsPtf());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A370-CLOSE-CURSOR-SC02<br>
	 * <pre>*****************************************************************</pre>*/
	private void a370CloseCursorSc02() {
		// COB_CODE: EXEC SQL
		//                CLOSE CUR-SC02
		//           END-EXEC.
		adesPoliStatOggBusDao.closeCurSc02();
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A380-FETCH-FIRST-SC02<br>
	 * <pre>*****************************************************************</pre>*/
	private void a380FetchFirstSc02() {
		// COB_CODE: PERFORM A360-OPEN-CURSOR-SC02    THRU A360-SC02-EX.
		a360OpenCursorSc02();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM A390-FETCH-NEXT-SC02  THRU A390-SC02-EX
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM A390-FETCH-NEXT-SC02  THRU A390-SC02-EX
			a390FetchNextSc02();
		}
	}

	/**Original name: A390-FETCH-NEXT-SC02<br>
	 * <pre>*****************************************************************</pre>*/
	private void a390FetchNextSc02() {
		// COB_CODE: EXEC SQL
		//                FETCH CUR-SC02
		//           INTO
		//                :ADE-ID-ADES
		//               ,:ADE-ID-POLI
		//               ,:ADE-ID-MOVI-CRZ
		//               ,:ADE-ID-MOVI-CHIU
		//                :IND-ADE-ID-MOVI-CHIU
		//               ,:ADE-DT-INI-EFF-DB
		//               ,:ADE-DT-END-EFF-DB
		//               ,:ADE-IB-PREV
		//                :IND-ADE-IB-PREV
		//               ,:ADE-IB-OGG
		//                :IND-ADE-IB-OGG
		//               ,:ADE-COD-COMP-ANIA
		//               ,:ADE-DT-DECOR-DB
		//                :IND-ADE-DT-DECOR
		//               ,:ADE-DT-SCAD-DB
		//                :IND-ADE-DT-SCAD
		//               ,:ADE-ETA-A-SCAD
		//                :IND-ADE-ETA-A-SCAD
		//               ,:ADE-DUR-AA
		//                :IND-ADE-DUR-AA
		//               ,:ADE-DUR-MM
		//                :IND-ADE-DUR-MM
		//               ,:ADE-DUR-GG
		//                :IND-ADE-DUR-GG
		//               ,:ADE-TP-RGM-FISC
		//               ,:ADE-TP-RIAT
		//                :IND-ADE-TP-RIAT
		//               ,:ADE-TP-MOD-PAG-TIT
		//               ,:ADE-TP-IAS
		//                :IND-ADE-TP-IAS
		//               ,:ADE-DT-VARZ-TP-IAS-DB
		//                :IND-ADE-DT-VARZ-TP-IAS
		//               ,:ADE-PRE-NET-IND
		//                :IND-ADE-PRE-NET-IND
		//               ,:ADE-PRE-LRD-IND
		//                :IND-ADE-PRE-LRD-IND
		//               ,:ADE-RAT-LRD-IND
		//                :IND-ADE-RAT-LRD-IND
		//               ,:ADE-PRSTZ-INI-IND
		//                :IND-ADE-PRSTZ-INI-IND
		//               ,:ADE-FL-COINC-ASSTO
		//                :IND-ADE-FL-COINC-ASSTO
		//               ,:ADE-IB-DFLT
		//                :IND-ADE-IB-DFLT
		//               ,:ADE-MOD-CALC
		//                :IND-ADE-MOD-CALC
		//               ,:ADE-TP-FNT-CNBTVA
		//                :IND-ADE-TP-FNT-CNBTVA
		//               ,:ADE-IMP-AZ
		//                :IND-ADE-IMP-AZ
		//               ,:ADE-IMP-ADER
		//                :IND-ADE-IMP-ADER
		//               ,:ADE-IMP-TFR
		//                :IND-ADE-IMP-TFR
		//               ,:ADE-IMP-VOLO
		//                :IND-ADE-IMP-VOLO
		//               ,:ADE-PC-AZ
		//                :IND-ADE-PC-AZ
		//               ,:ADE-PC-ADER
		//                :IND-ADE-PC-ADER
		//               ,:ADE-PC-TFR
		//                :IND-ADE-PC-TFR
		//               ,:ADE-PC-VOLO
		//                :IND-ADE-PC-VOLO
		//               ,:ADE-DT-NOVA-RGM-FISC-DB
		//                :IND-ADE-DT-NOVA-RGM-FISC
		//               ,:ADE-FL-ATTIV
		//                :IND-ADE-FL-ATTIV
		//               ,:ADE-IMP-REC-RIT-VIS
		//                :IND-ADE-IMP-REC-RIT-VIS
		//               ,:ADE-IMP-REC-RIT-ACC
		//                :IND-ADE-IMP-REC-RIT-ACC
		//               ,:ADE-FL-VARZ-STAT-TBGC
		//                :IND-ADE-FL-VARZ-STAT-TBGC
		//               ,:ADE-FL-PROVZA-MIGRAZ
		//                :IND-ADE-FL-PROVZA-MIGRAZ
		//               ,:ADE-IMPB-VIS-DA-REC
		//                :IND-ADE-IMPB-VIS-DA-REC
		//               ,:ADE-DT-DECOR-PREST-BAN-DB
		//                :IND-ADE-DT-DECOR-PREST-BAN
		//               ,:ADE-DT-EFF-VARZ-STAT-T-DB
		//                :IND-ADE-DT-EFF-VARZ-STAT-T
		//               ,:ADE-DS-RIGA
		//               ,:ADE-DS-OPER-SQL
		//               ,:ADE-DS-VER
		//               ,:ADE-DS-TS-INI-CPTZ
		//               ,:ADE-DS-TS-END-CPTZ
		//               ,:ADE-DS-UTENTE
		//               ,:ADE-DS-STATO-ELAB
		//               ,:ADE-CUM-CNBT-CAP
		//                :IND-ADE-CUM-CNBT-CAP
		//               ,:ADE-IMP-GAR-CNBT
		//                :IND-ADE-IMP-GAR-CNBT
		//               ,:ADE-DT-ULT-CONS-CNBT-DB
		//                :IND-ADE-DT-ULT-CONS-CNBT
		//               ,:ADE-IDEN-ISC-FND
		//                :IND-ADE-IDEN-ISC-FND
		//               ,:ADE-NUM-RAT-PIAN
		//                :IND-ADE-NUM-RAT-PIAN
		//               ,:ADE-DT-PRESC-DB
		//                :IND-ADE-DT-PRESC
		//               ,:ADE-CONCS-PREST
		//                :IND-ADE-CONCS-PREST
		//               ,:POL-ID-POLI
		//               ,:POL-ID-MOVI-CRZ
		//               ,:POL-ID-MOVI-CHIU
		//                :IND-POL-ID-MOVI-CHIU
		//               ,:POL-IB-OGG
		//                :IND-POL-IB-OGG
		//               ,:POL-IB-PROP
		//               ,:POL-DT-PROP-DB
		//                :IND-POL-DT-PROP
		//               ,:POL-DT-INI-EFF-DB
		//               ,:POL-DT-END-EFF-DB
		//               ,:POL-COD-COMP-ANIA
		//               ,:POL-DT-DECOR-DB
		//               ,:POL-DT-EMIS-DB
		//               ,:POL-TP-POLI
		//               ,:POL-DUR-AA
		//                :IND-POL-DUR-AA
		//               ,:POL-DUR-MM
		//                :IND-POL-DUR-MM
		//               ,:POL-DT-SCAD-DB
		//                :IND-POL-DT-SCAD
		//               ,:POL-COD-PROD
		//               ,:POL-DT-INI-VLDT-PROD-DB
		//               ,:POL-COD-CONV
		//                :IND-POL-COD-CONV
		//               ,:POL-COD-RAMO
		//                :IND-POL-COD-RAMO
		//               ,:POL-DT-INI-VLDT-CONV-DB
		//                :IND-POL-DT-INI-VLDT-CONV
		//               ,:POL-DT-APPLZ-CONV-DB
		//                :IND-POL-DT-APPLZ-CONV
		//               ,:POL-TP-FRM-ASSVA
		//               ,:POL-TP-RGM-FISC
		//                :IND-POL-TP-RGM-FISC
		//               ,:POL-FL-ESTAS
		//                :IND-POL-FL-ESTAS
		//               ,:POL-FL-RSH-COMUN
		//                :IND-POL-FL-RSH-COMUN
		//               ,:POL-FL-RSH-COMUN-COND
		//                :IND-POL-FL-RSH-COMUN-COND
		//               ,:POL-TP-LIV-GENZ-TIT
		//               ,:POL-FL-COP-FINANZ
		//                :IND-POL-FL-COP-FINANZ
		//               ,:POL-TP-APPLZ-DIR
		//                :IND-POL-TP-APPLZ-DIR
		//               ,:POL-SPE-MED
		//                :IND-POL-SPE-MED
		//               ,:POL-DIR-EMIS
		//                :IND-POL-DIR-EMIS
		//               ,:POL-DIR-1O-VERS
		//                :IND-POL-DIR-1O-VERS
		//               ,:POL-DIR-VERS-AGG
		//                :IND-POL-DIR-VERS-AGG
		//               ,:POL-COD-DVS
		//                :IND-POL-COD-DVS
		//               ,:POL-FL-FNT-AZ
		//                :IND-POL-FL-FNT-AZ
		//               ,:POL-FL-FNT-ADER
		//                :IND-POL-FL-FNT-ADER
		//               ,:POL-FL-FNT-TFR
		//                :IND-POL-FL-FNT-TFR
		//               ,:POL-FL-FNT-VOLO
		//                :IND-POL-FL-FNT-VOLO
		//               ,:POL-TP-OPZ-A-SCAD
		//                :IND-POL-TP-OPZ-A-SCAD
		//               ,:POL-AA-DIFF-PROR-DFLT
		//                :IND-POL-AA-DIFF-PROR-DFLT
		//               ,:POL-FL-VER-PROD
		//                :IND-POL-FL-VER-PROD
		//               ,:POL-DUR-GG
		//                :IND-POL-DUR-GG
		//               ,:POL-DIR-QUIET
		//                :IND-POL-DIR-QUIET
		//               ,:POL-TP-PTF-ESTNO
		//                :IND-POL-TP-PTF-ESTNO
		//               ,:POL-FL-CUM-PRE-CNTR
		//                :IND-POL-FL-CUM-PRE-CNTR
		//               ,:POL-FL-AMMB-MOVI
		//                :IND-POL-FL-AMMB-MOVI
		//               ,:POL-CONV-GECO
		//                :IND-POL-CONV-GECO
		//               ,:POL-DS-RIGA
		//               ,:POL-DS-OPER-SQL
		//               ,:POL-DS-VER
		//               ,:POL-DS-TS-INI-CPTZ
		//               ,:POL-DS-TS-END-CPTZ
		//               ,:POL-DS-UTENTE
		//               ,:POL-DS-STATO-ELAB
		//               ,:POL-FL-SCUDO-FISC
		//                :IND-POL-FL-SCUDO-FISC
		//               ,:POL-FL-TRASFE
		//                :IND-POL-FL-TRASFE
		//               ,:POL-FL-TFR-STRC
		//                :IND-POL-FL-TFR-STRC
		//               ,:POL-DT-PRESC-DB
		//                :IND-POL-DT-PRESC
		//               ,:POL-COD-CONV-AGG
		//                :IND-POL-COD-CONV-AGG
		//               ,:POL-SUBCAT-PROD
		//                :IND-POL-SUBCAT-PROD
		//               ,:POL-FL-QUEST-ADEGZ-ASS
		//                :IND-POL-FL-QUEST-ADEGZ-ASS
		//               ,:POL-COD-TPA
		//                :IND-POL-COD-TPA
		//               ,:POL-ID-ACC-COMM
		//                :IND-POL-ID-ACC-COMM
		//               ,:POL-FL-POLI-CPI-PR
		//                :IND-POL-FL-POLI-CPI-PR
		//               ,:POL-FL-POLI-BUNDLING
		//                :IND-POL-FL-POLI-BUNDLING
		//               ,:POL-IND-POLI-PRIN-COLL
		//                :IND-POL-IND-POLI-PRIN-COLL
		//               ,:POL-FL-VND-BUNDLE
		//                :IND-POL-FL-VND-BUNDLE
		//               ,:POL-IB-BS
		//                :IND-POL-IB-BS
		//               ,:POL-FL-POLI-IFP
		//                :IND-POL-FL-POLI-IFP
		//               ,:STB-ID-STAT-OGG-BUS
		//               ,:STB-ID-OGG
		//               ,:STB-TP-OGG
		//               ,:STB-ID-MOVI-CRZ
		//               ,:STB-ID-MOVI-CHIU
		//                :IND-STB-ID-MOVI-CHIU
		//               ,:STB-DT-INI-EFF-DB
		//               ,:STB-DT-END-EFF-DB
		//               ,:STB-COD-COMP-ANIA
		//               ,:STB-TP-STAT-BUS
		//               ,:STB-TP-CAUS
		//               ,:STB-DS-RIGA
		//               ,:STB-DS-OPER-SQL
		//               ,:STB-DS-VER
		//               ,:STB-DS-TS-INI-CPTZ
		//               ,:STB-DS-TS-END-CPTZ
		//               ,:STB-DS-UTENTE
		//               ,:STB-DS-STATO-ELAB
		//           END-EXEC.
		adesPoliStatOggBusDao.fetchCurSc02(this.getAdesPoliStatOggBusLdbm0250());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           ELSE
		//             END-IF
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			z100SetColonneNull();
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		} else if (idsv0003.getSqlcode().isNotFound()) {
			// COB_CODE: IF IDSV0003-NOT-FOUND
			//              END-IF
			//           END-IF
			// COB_CODE: PERFORM A370-CLOSE-CURSOR-SC02 THRU A370-SC02-EX
			a370CloseCursorSc02();
			// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
			//              SET IDSV0003-NOT-FOUND TO TRUE
			//           END-IF
			if (idsv0003.getSqlcode().isSuccessfulSql()) {
				// COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
				idsv0003.getSqlcode().setNotFound();
			}
		}
	}

	/**Original name: SC03-SELECTION-CURSOR-03<br>
	 * <pre>*****************************************************************
	 * *****************************************************************</pre>*/
	private void sc03SelectionCursor03() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-SELECT
		//                 PERFORM A310-SELECT-SC03           THRU A310-SC03-EX
		//              WHEN IDSV0003-OPEN-CURSOR
		//                 PERFORM A360-OPEN-CURSOR-SC03      THRU A360-SC03-EX
		//              WHEN IDSV0003-CLOSE-CURSOR
		//                 PERFORM A370-CLOSE-CURSOR-SC03     THRU A370-SC03-EX
		//              WHEN IDSV0003-FETCH-FIRST
		//                 PERFORM A380-FETCH-FIRST-SC03      THRU A380-SC03-EX
		//              WHEN IDSV0003-FETCH-NEXT
		//                 PERFORM A390-FETCH-NEXT-SC03       THRU A390-SC03-EX
		//              WHEN IDSV0003-UPDATE
		//                 PERFORM A320-UPDATE-SC03           THRU A320-SC03-EX
		//              WHEN OTHER
		//                 SET IDSV0003-INVALID-OPER          TO TRUE
		//           END-EVALUATE.
		if (idsv0003.getOperazione().isSelect()) {
			// COB_CODE: PERFORM A310-SELECT-SC03           THRU A310-SC03-EX
			a310SelectSc03();
		} else if (idsv0003.getOperazione().isOpenCursor()) {
			// COB_CODE: PERFORM A360-OPEN-CURSOR-SC03      THRU A360-SC03-EX
			a360OpenCursorSc03();
		} else if (idsv0003.getOperazione().isCloseCursor()) {
			// COB_CODE: PERFORM A370-CLOSE-CURSOR-SC03     THRU A370-SC03-EX
			a370CloseCursorSc03();
		} else if (idsv0003.getOperazione().isFetchFirst()) {
			// COB_CODE: PERFORM A380-FETCH-FIRST-SC03      THRU A380-SC03-EX
			a380FetchFirstSc03();
		} else if (idsv0003.getOperazione().isFetchNext()) {
			// COB_CODE: PERFORM A390-FETCH-NEXT-SC03       THRU A390-SC03-EX
			a390FetchNextSc03();
		} else if (idsv0003.getOperazione().isUpdate()) {
			// COB_CODE: PERFORM A320-UPDATE-SC03           THRU A320-SC03-EX
			a320UpdateSc03();
		} else {
			// COB_CODE: SET IDSV0003-INVALID-OPER          TO TRUE
			idsv0003.getReturnCode().setInvalidOper();
		}
	}

	/**Original name: A305-DECLARE-CURSOR-SC03<br>
	 * <pre>*****************************************************************</pre>*/
	private void a305DeclareCursorSc03() {
		// COB_CODE:      EXEC SQL
		//                     DECLARE CUR-SC03 CURSOR WITH HOLD FOR
		//                     SELECT
		//                      A.ID_ADES
		//                     ,A.ID_POLI
		//                     ,A.ID_MOVI_CRZ
		//                     ,A.ID_MOVI_CHIU
		//                     ,A.DT_INI_EFF
		//                     ,A.DT_END_EFF
		//                     ,A.IB_PREV
		//                     ,A.IB_OGG
		//                     ,A.COD_COMP_ANIA
		//                     ,A.DT_DECOR
		//                     ,A.DT_SCAD
		//                     ,A.ETA_A_SCAD
		//                     ,A.DUR_AA
		//                     ,A.DUR_MM
		//                     ,A.DUR_GG
		//                     ,A.TP_RGM_FISC
		//                     ,A.TP_RIAT
		//                     ,A.TP_MOD_PAG_TIT
		//                     ,A.TP_IAS
		//                     ,A.DT_VARZ_TP_IAS
		//                     ,A.PRE_NET_IND
		//                     ,A.PRE_LRD_IND
		//                     ,A.RAT_LRD_IND
		//                     ,A.PRSTZ_INI_IND
		//                     ,A.FL_COINC_ASSTO
		//                     ,A.IB_DFLT
		//                     ,A.MOD_CALC
		//                     ,A.TP_FNT_CNBTVA
		//                     ,A.IMP_AZ
		//                     ,A.IMP_ADER
		//                     ,A.IMP_TFR
		//                     ,A.IMP_VOLO
		//                     ,A.PC_AZ
		//                     ,A.PC_ADER
		//                     ,A.PC_TFR
		//                     ,A.PC_VOLO
		//                     ,A.DT_NOVA_RGM_FISC
		//                     ,A.FL_ATTIV
		//                     ,A.IMP_REC_RIT_VIS
		//                     ,A.IMP_REC_RIT_ACC
		//                     ,A.FL_VARZ_STAT_TBGC
		//                     ,A.FL_PROVZA_MIGRAZ
		//                     ,A.IMPB_VIS_DA_REC
		//                     ,A.DT_DECOR_PREST_BAN
		//                     ,A.DT_EFF_VARZ_STAT_T
		//                     ,A.DS_RIGA
		//                     ,A.DS_OPER_SQL
		//                     ,A.DS_VER
		//                     ,A.DS_TS_INI_CPTZ
		//                     ,A.DS_TS_END_CPTZ
		//                     ,A.DS_UTENTE
		//                     ,A.DS_STATO_ELAB
		//                     ,A.CUM_CNBT_CAP
		//                     ,A.IMP_GAR_CNBT
		//                     ,A.DT_ULT_CONS_CNBT
		//                     ,A.IDEN_ISC_FND
		//                     ,A.NUM_RAT_PIAN
		//                     ,A.DT_PRESC
		//                     ,A.CONCS_PREST
		//                     ,B.ID_POLI
		//                     ,B.ID_MOVI_CRZ
		//                     ,B.ID_MOVI_CHIU
		//                     ,B.IB_OGG
		//                     ,B.IB_PROP
		//                     ,B.DT_PROP
		//                     ,B.DT_INI_EFF
		//                     ,B.DT_END_EFF
		//                     ,B.COD_COMP_ANIA
		//                     ,B.DT_DECOR
		//                     ,B.DT_EMIS
		//                     ,B.TP_POLI
		//                     ,B.DUR_AA
		//                     ,B.DUR_MM
		//                     ,B.DT_SCAD
		//                     ,B.COD_PROD
		//                     ,B.DT_INI_VLDT_PROD
		//                     ,B.COD_CONV
		//                     ,B.COD_RAMO
		//                     ,B.DT_INI_VLDT_CONV
		//                     ,B.DT_APPLZ_CONV
		//                     ,B.TP_FRM_ASSVA
		//                     ,B.TP_RGM_FISC
		//                     ,B.FL_ESTAS
		//                     ,B.FL_RSH_COMUN
		//                     ,B.FL_RSH_COMUN_COND
		//                     ,B.TP_LIV_GENZ_TIT
		//                     ,B.FL_COP_FINANZ
		//                     ,B.TP_APPLZ_DIR
		//                     ,B.SPE_MED
		//                     ,B.DIR_EMIS
		//                     ,B.DIR_1O_VERS
		//                     ,B.DIR_VERS_AGG
		//                     ,B.COD_DVS
		//                     ,B.FL_FNT_AZ
		//                     ,B.FL_FNT_ADER
		//                     ,B.FL_FNT_TFR
		//                     ,B.FL_FNT_VOLO
		//                     ,B.TP_OPZ_A_SCAD
		//                     ,B.AA_DIFF_PROR_DFLT
		//                     ,B.FL_VER_PROD
		//                     ,B.DUR_GG
		//                     ,B.DIR_QUIET
		//                     ,B.TP_PTF_ESTNO
		//                     ,B.FL_CUM_PRE_CNTR
		//                     ,B.FL_AMMB_MOVI
		//                     ,B.CONV_GECO
		//                     ,B.DS_RIGA
		//                     ,B.DS_OPER_SQL
		//                     ,B.DS_VER
		//                     ,B.DS_TS_INI_CPTZ
		//                     ,B.DS_TS_END_CPTZ
		//                     ,B.DS_UTENTE
		//                     ,B.DS_STATO_ELAB
		//                     ,B.FL_SCUDO_FISC
		//                     ,B.FL_TRASFE
		//                     ,B.FL_TFR_STRC
		//                     ,B.DT_PRESC
		//                     ,B.COD_CONV_AGG
		//                     ,B.SUBCAT_PROD
		//                     ,B.FL_QUEST_ADEGZ_ASS
		//                     ,B.COD_TPA
		//                     ,B.ID_ACC_COMM
		//                     ,B.FL_POLI_CPI_PR
		//                     ,B.FL_POLI_BUNDLING
		//                     ,B.IND_POLI_PRIN_COLL
		//                     ,B.FL_VND_BUNDLE
		//                     ,B.IB_BS
		//                     ,B.FL_POLI_IFP
		//                     ,C.ID_STAT_OGG_BUS
		//                     ,C.ID_OGG
		//                     ,C.TP_OGG
		//                     ,C.ID_MOVI_CRZ
		//                     ,C.ID_MOVI_CHIU
		//                     ,C.DT_INI_EFF
		//                     ,C.DT_END_EFF
		//                     ,C.COD_COMP_ANIA
		//                     ,C.TP_STAT_BUS
		//                     ,C.TP_CAUS
		//                     ,C.DS_RIGA
		//                     ,C.DS_OPER_SQL
		//                     ,C.DS_VER
		//                     ,C.DS_TS_INI_CPTZ
		//                     ,C.DS_TS_END_CPTZ
		//                     ,C.DS_UTENTE
		//                     ,C.DS_STATO_ELAB
		//                  FROM ADES         A,
		//                       POLI         B,
		//                       STAT_OGG_BUS C
		//                  WHERE B.IB_OGG   BETWEEN   :WLB-IB-POLI-FIRST AND
		//                                             :WLB-IB-POLI-LAST
		//                    AND A.ID_POLI         =   B.ID_POLI
		//                    AND A.ID_ADES         =   C.ID_OGG
		//                    AND C.TP_OGG          =  'AD'
		//                    AND C.TP_STAT_BUS     =  'VI'
		//           *        OR
		//           *            (C.TP_STAT_BUS    =  'ST'  AND
		//           *            EXISTS (SELECT
		//           *                  D.ID_MOVI_FINRIO
		//           *
		//           *               FROM  MOVI_FINRIO D
		//           *               WHERE A.ID_ADES = D.ID_ADES
		//           *                 AND D.STAT_MOVI IN ('AL','IE')
		//           *                 AND D.TP_MOVI_FINRIO = 'DI'
		//           *                 AND D.COD_COMP_ANIA   =
		//           *                     :IDSV0003-CODICE-COMPAGNIA-ANIA
		//           *                 AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
		//           *                 AND D.DT_END_EFF     >   :WS-DT-PTF-X
		//           *                 AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//           *                 AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
		//                  AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
		//                  AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
		//                  AND A.DT_END_EFF     >   :WS-DT-PTF-X
		//                  AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//                  AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
		//                  AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
		//                  AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
		//                  AND B.DT_END_EFF     >   :WS-DT-PTF-X
		//                  AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//                  AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
		//                  AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
		//                  AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
		//                  AND C.DT_END_EFF     >   :WS-DT-PTF-X
		//                  AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//                  AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
		//           *      AND A.DS_STATO_ELAB IN (
		//           *                              :IABV0002-STATE-01,
		//           *                              :IABV0002-STATE-02,
		//           *                              :IABV0002-STATE-03,
		//           *                              :IABV0002-STATE-04,
		//           *                              :IABV0002-STATE-05,
		//           *                              :IABV0002-STATE-06,
		//           *                              :IABV0002-STATE-07,
		//           *                              :IABV0002-STATE-08,
		//           *                              :IABV0002-STATE-09,
		//           *                              :IABV0002-STATE-10
		//           *                                  )
		//           *        AND NOT A.DS_VER     = :IABV0009-VERSIONING
		//                    ORDER BY A.ID_POLI, A.ID_ADES
		//                END-EXEC.
		// DECLARE CURSOR doesn't need a translation;
	}

	/**Original name: A310-SELECT-SC03<br>
	 * <pre>*****************************************************************
	 *       AND A.DS_STATO_ELAB IN (
	 *                               :IABV0002-STATE-01,
	 *                               :IABV0002-STATE-02,
	 *                               :IABV0002-STATE-03,
	 *                               :IABV0002-STATE-04,
	 *                               :IABV0002-STATE-05,
	 *                               :IABV0002-STATE-06,
	 *                               :IABV0002-STATE-07,
	 *                               :IABV0002-STATE-08,
	 *                               :IABV0002-STATE-09,
	 *                               :IABV0002-STATE-10
	 *                                   )
	 *         AND NOT A.DS_VER     = :IABV0009-VERSIONING</pre>*/
	private void a310SelectSc03() {
		// COB_CODE:      EXEC SQL
		//                  SELECT
		//                      A.ID_ADES
		//                     ,A.ID_POLI
		//                     ,A.ID_MOVI_CRZ
		//                     ,A.ID_MOVI_CHIU
		//                     ,A.DT_INI_EFF
		//                     ,A.DT_END_EFF
		//                     ,A.IB_PREV
		//                     ,A.IB_OGG
		//                     ,A.COD_COMP_ANIA
		//                     ,A.DT_DECOR
		//                     ,A.DT_SCAD
		//                     ,A.ETA_A_SCAD
		//                     ,A.DUR_AA
		//                     ,A.DUR_MM
		//                     ,A.DUR_GG
		//                     ,A.TP_RGM_FISC
		//                     ,A.TP_RIAT
		//                     ,A.TP_MOD_PAG_TIT
		//                     ,A.TP_IAS
		//                     ,A.DT_VARZ_TP_IAS
		//                     ,A.PRE_NET_IND
		//                     ,A.PRE_LRD_IND
		//                     ,A.RAT_LRD_IND
		//                     ,A.PRSTZ_INI_IND
		//                     ,A.FL_COINC_ASSTO
		//                     ,A.IB_DFLT
		//                     ,A.MOD_CALC
		//                     ,A.TP_FNT_CNBTVA
		//                     ,A.IMP_AZ
		//                     ,A.IMP_ADER
		//                     ,A.IMP_TFR
		//                     ,A.IMP_VOLO
		//                     ,A.PC_AZ
		//                     ,A.PC_ADER
		//                     ,A.PC_TFR
		//                     ,A.PC_VOLO
		//                     ,A.DT_NOVA_RGM_FISC
		//                     ,A.FL_ATTIV
		//                     ,A.IMP_REC_RIT_VIS
		//                     ,A.IMP_REC_RIT_ACC
		//                     ,A.FL_VARZ_STAT_TBGC
		//                     ,A.FL_PROVZA_MIGRAZ
		//                     ,A.IMPB_VIS_DA_REC
		//                     ,A.DT_DECOR_PREST_BAN
		//                     ,A.DT_EFF_VARZ_STAT_T
		//                     ,A.DS_RIGA
		//                     ,A.DS_OPER_SQL
		//                     ,A.DS_VER
		//                     ,A.DS_TS_INI_CPTZ
		//                     ,A.DS_TS_END_CPTZ
		//                     ,A.DS_UTENTE
		//                     ,A.DS_STATO_ELAB
		//                     ,A.CUM_CNBT_CAP
		//                     ,A.IMP_GAR_CNBT
		//                     ,A.DT_ULT_CONS_CNBT
		//                     ,A.IDEN_ISC_FND
		//                     ,A.NUM_RAT_PIAN
		//                     ,A.DT_PRESC
		//                     ,A.CONCS_PREST
		//                     ,B.ID_POLI
		//                     ,B.ID_MOVI_CRZ
		//                     ,B.ID_MOVI_CHIU
		//                     ,B.IB_OGG
		//                     ,B.IB_PROP
		//                     ,B.DT_PROP
		//                     ,B.DT_INI_EFF
		//                     ,B.DT_END_EFF
		//                     ,B.COD_COMP_ANIA
		//                     ,B.DT_DECOR
		//                     ,B.DT_EMIS
		//                     ,B.TP_POLI
		//                     ,B.DUR_AA
		//                     ,B.DUR_MM
		//                     ,B.DT_SCAD
		//                     ,B.COD_PROD
		//                     ,B.DT_INI_VLDT_PROD
		//                     ,B.COD_CONV
		//                     ,B.COD_RAMO
		//                     ,B.DT_INI_VLDT_CONV
		//                     ,B.DT_APPLZ_CONV
		//                     ,B.TP_FRM_ASSVA
		//                     ,B.TP_RGM_FISC
		//                     ,B.FL_ESTAS
		//                     ,B.FL_RSH_COMUN
		//                     ,B.FL_RSH_COMUN_COND
		//                     ,B.TP_LIV_GENZ_TIT
		//                     ,B.FL_COP_FINANZ
		//                     ,B.TP_APPLZ_DIR
		//                     ,B.SPE_MED
		//                     ,B.DIR_EMIS
		//                     ,B.DIR_1O_VERS
		//                     ,B.DIR_VERS_AGG
		//                     ,B.COD_DVS
		//                     ,B.FL_FNT_AZ
		//                     ,B.FL_FNT_ADER
		//                     ,B.FL_FNT_TFR
		//                     ,B.FL_FNT_VOLO
		//                     ,B.TP_OPZ_A_SCAD
		//                     ,B.AA_DIFF_PROR_DFLT
		//                     ,B.FL_VER_PROD
		//                     ,B.DUR_GG
		//                     ,B.DIR_QUIET
		//                     ,B.TP_PTF_ESTNO
		//                     ,B.FL_CUM_PRE_CNTR
		//                     ,B.FL_AMMB_MOVI
		//                     ,B.CONV_GECO
		//                     ,B.DS_RIGA
		//                     ,B.DS_OPER_SQL
		//                     ,B.DS_VER
		//                     ,B.DS_TS_INI_CPTZ
		//                     ,B.DS_TS_END_CPTZ
		//                     ,B.DS_UTENTE
		//                     ,B.DS_STATO_ELAB
		//                     ,B.FL_SCUDO_FISC
		//                     ,B.FL_TRASFE
		//                     ,B.FL_TFR_STRC
		//                     ,B.DT_PRESC
		//                     ,B.COD_CONV_AGG
		//                     ,B.SUBCAT_PROD
		//                     ,B.FL_QUEST_ADEGZ_ASS
		//                     ,B.COD_TPA
		//                     ,B.ID_ACC_COMM
		//                     ,B.FL_POLI_CPI_PR
		//                     ,B.FL_POLI_BUNDLING
		//                     ,B.IND_POLI_PRIN_COLL
		//                     ,B.FL_VND_BUNDLE
		//                     ,B.IB_BS
		//                     ,B.FL_POLI_IFP
		//                     ,C.ID_STAT_OGG_BUS
		//                     ,C.ID_OGG
		//                     ,C.TP_OGG
		//                     ,C.ID_MOVI_CRZ
		//                     ,C.ID_MOVI_CHIU
		//                     ,C.DT_INI_EFF
		//                     ,C.DT_END_EFF
		//                     ,C.COD_COMP_ANIA
		//                     ,C.TP_STAT_BUS
		//                     ,C.TP_CAUS
		//                     ,C.DS_RIGA
		//                     ,C.DS_OPER_SQL
		//                     ,C.DS_VER
		//                     ,C.DS_TS_INI_CPTZ
		//                     ,C.DS_TS_END_CPTZ
		//                     ,C.DS_UTENTE
		//                     ,C.DS_STATO_ELAB
		//                  INTO
		//                     :ADE-ID-ADES
		//                    ,:ADE-ID-POLI
		//                    ,:ADE-ID-MOVI-CRZ
		//                    ,:ADE-ID-MOVI-CHIU
		//                     :IND-ADE-ID-MOVI-CHIU
		//                    ,:ADE-DT-INI-EFF-DB
		//                    ,:ADE-DT-END-EFF-DB
		//                    ,:ADE-IB-PREV
		//                     :IND-ADE-IB-PREV
		//                    ,:ADE-IB-OGG
		//                     :IND-ADE-IB-OGG
		//                    ,:ADE-COD-COMP-ANIA
		//                    ,:ADE-DT-DECOR-DB
		//                     :IND-ADE-DT-DECOR
		//                    ,:ADE-DT-SCAD-DB
		//                     :IND-ADE-DT-SCAD
		//                    ,:ADE-ETA-A-SCAD
		//                     :IND-ADE-ETA-A-SCAD
		//                    ,:ADE-DUR-AA
		//                     :IND-ADE-DUR-AA
		//                    ,:ADE-DUR-MM
		//                     :IND-ADE-DUR-MM
		//                    ,:ADE-DUR-GG
		//                     :IND-ADE-DUR-GG
		//                    ,:ADE-TP-RGM-FISC
		//                    ,:ADE-TP-RIAT
		//                     :IND-ADE-TP-RIAT
		//                    ,:ADE-TP-MOD-PAG-TIT
		//                    ,:ADE-TP-IAS
		//                     :IND-ADE-TP-IAS
		//                    ,:ADE-DT-VARZ-TP-IAS-DB
		//                     :IND-ADE-DT-VARZ-TP-IAS
		//                    ,:ADE-PRE-NET-IND
		//                     :IND-ADE-PRE-NET-IND
		//                    ,:ADE-PRE-LRD-IND
		//                     :IND-ADE-PRE-LRD-IND
		//                    ,:ADE-RAT-LRD-IND
		//                     :IND-ADE-RAT-LRD-IND
		//                    ,:ADE-PRSTZ-INI-IND
		//                     :IND-ADE-PRSTZ-INI-IND
		//                    ,:ADE-FL-COINC-ASSTO
		//                     :IND-ADE-FL-COINC-ASSTO
		//                    ,:ADE-IB-DFLT
		//                     :IND-ADE-IB-DFLT
		//                    ,:ADE-MOD-CALC
		//                     :IND-ADE-MOD-CALC
		//                    ,:ADE-TP-FNT-CNBTVA
		//                     :IND-ADE-TP-FNT-CNBTVA
		//                    ,:ADE-IMP-AZ
		//                     :IND-ADE-IMP-AZ
		//                    ,:ADE-IMP-ADER
		//                     :IND-ADE-IMP-ADER
		//                    ,:ADE-IMP-TFR
		//                     :IND-ADE-IMP-TFR
		//                    ,:ADE-IMP-VOLO
		//                     :IND-ADE-IMP-VOLO
		//                    ,:ADE-PC-AZ
		//                     :IND-ADE-PC-AZ
		//                    ,:ADE-PC-ADER
		//                     :IND-ADE-PC-ADER
		//                    ,:ADE-PC-TFR
		//                     :IND-ADE-PC-TFR
		//                    ,:ADE-PC-VOLO
		//                     :IND-ADE-PC-VOLO
		//                    ,:ADE-DT-NOVA-RGM-FISC-DB
		//                     :IND-ADE-DT-NOVA-RGM-FISC
		//                    ,:ADE-FL-ATTIV
		//                     :IND-ADE-FL-ATTIV
		//                    ,:ADE-IMP-REC-RIT-VIS
		//                     :IND-ADE-IMP-REC-RIT-VIS
		//                    ,:ADE-IMP-REC-RIT-ACC
		//                     :IND-ADE-IMP-REC-RIT-ACC
		//                    ,:ADE-FL-VARZ-STAT-TBGC
		//                     :IND-ADE-FL-VARZ-STAT-TBGC
		//                    ,:ADE-FL-PROVZA-MIGRAZ
		//                     :IND-ADE-FL-PROVZA-MIGRAZ
		//                    ,:ADE-IMPB-VIS-DA-REC
		//                     :IND-ADE-IMPB-VIS-DA-REC
		//                    ,:ADE-DT-DECOR-PREST-BAN-DB
		//                     :IND-ADE-DT-DECOR-PREST-BAN
		//                    ,:ADE-DT-EFF-VARZ-STAT-T-DB
		//                     :IND-ADE-DT-EFF-VARZ-STAT-T
		//                    ,:ADE-DS-RIGA
		//                    ,:ADE-DS-OPER-SQL
		//                    ,:ADE-DS-VER
		//                    ,:ADE-DS-TS-INI-CPTZ
		//                    ,:ADE-DS-TS-END-CPTZ
		//                    ,:ADE-DS-UTENTE
		//                    ,:ADE-DS-STATO-ELAB
		//                    ,:ADE-CUM-CNBT-CAP
		//                     :IND-ADE-CUM-CNBT-CAP
		//                    ,:ADE-IMP-GAR-CNBT
		//                     :IND-ADE-IMP-GAR-CNBT
		//                    ,:ADE-DT-ULT-CONS-CNBT-DB
		//                     :IND-ADE-DT-ULT-CONS-CNBT
		//                    ,:ADE-IDEN-ISC-FND
		//                     :IND-ADE-IDEN-ISC-FND
		//                    ,:ADE-NUM-RAT-PIAN
		//                     :IND-ADE-NUM-RAT-PIAN
		//                    ,:ADE-DT-PRESC-DB
		//                     :IND-ADE-DT-PRESC
		//                    ,:ADE-CONCS-PREST
		//                     :IND-ADE-CONCS-PREST
		//                    ,:POL-ID-POLI
		//                    ,:POL-ID-MOVI-CRZ
		//                    ,:POL-ID-MOVI-CHIU
		//                     :IND-POL-ID-MOVI-CHIU
		//                    ,:POL-IB-OGG
		//                     :IND-POL-IB-OGG
		//                    ,:POL-IB-PROP
		//                    ,:POL-DT-PROP-DB
		//                     :IND-POL-DT-PROP
		//                    ,:POL-DT-INI-EFF-DB
		//                    ,:POL-DT-END-EFF-DB
		//                    ,:POL-COD-COMP-ANIA
		//                    ,:POL-DT-DECOR-DB
		//                    ,:POL-DT-EMIS-DB
		//                    ,:POL-TP-POLI
		//                    ,:POL-DUR-AA
		//                     :IND-POL-DUR-AA
		//                    ,:POL-DUR-MM
		//                     :IND-POL-DUR-MM
		//                    ,:POL-DT-SCAD-DB
		//                     :IND-POL-DT-SCAD
		//                    ,:POL-COD-PROD
		//                    ,:POL-DT-INI-VLDT-PROD-DB
		//                    ,:POL-COD-CONV
		//                     :IND-POL-COD-CONV
		//                    ,:POL-COD-RAMO
		//                     :IND-POL-COD-RAMO
		//                    ,:POL-DT-INI-VLDT-CONV-DB
		//                     :IND-POL-DT-INI-VLDT-CONV
		//                    ,:POL-DT-APPLZ-CONV-DB
		//                     :IND-POL-DT-APPLZ-CONV
		//                    ,:POL-TP-FRM-ASSVA
		//                    ,:POL-TP-RGM-FISC
		//                     :IND-POL-TP-RGM-FISC
		//                    ,:POL-FL-ESTAS
		//                     :IND-POL-FL-ESTAS
		//                    ,:POL-FL-RSH-COMUN
		//                     :IND-POL-FL-RSH-COMUN
		//                    ,:POL-FL-RSH-COMUN-COND
		//                     :IND-POL-FL-RSH-COMUN-COND
		//                    ,:POL-TP-LIV-GENZ-TIT
		//                    ,:POL-FL-COP-FINANZ
		//                     :IND-POL-FL-COP-FINANZ
		//                    ,:POL-TP-APPLZ-DIR
		//                     :IND-POL-TP-APPLZ-DIR
		//                    ,:POL-SPE-MED
		//                     :IND-POL-SPE-MED
		//                    ,:POL-DIR-EMIS
		//                     :IND-POL-DIR-EMIS
		//                    ,:POL-DIR-1O-VERS
		//                     :IND-POL-DIR-1O-VERS
		//                    ,:POL-DIR-VERS-AGG
		//                     :IND-POL-DIR-VERS-AGG
		//                    ,:POL-COD-DVS
		//                     :IND-POL-COD-DVS
		//                    ,:POL-FL-FNT-AZ
		//                     :IND-POL-FL-FNT-AZ
		//                    ,:POL-FL-FNT-ADER
		//                     :IND-POL-FL-FNT-ADER
		//                    ,:POL-FL-FNT-TFR
		//                     :IND-POL-FL-FNT-TFR
		//                    ,:POL-FL-FNT-VOLO
		//                     :IND-POL-FL-FNT-VOLO
		//                    ,:POL-TP-OPZ-A-SCAD
		//                     :IND-POL-TP-OPZ-A-SCAD
		//                    ,:POL-AA-DIFF-PROR-DFLT
		//                     :IND-POL-AA-DIFF-PROR-DFLT
		//                    ,:POL-FL-VER-PROD
		//                     :IND-POL-FL-VER-PROD
		//                    ,:POL-DUR-GG
		//                     :IND-POL-DUR-GG
		//                    ,:POL-DIR-QUIET
		//                     :IND-POL-DIR-QUIET
		//                    ,:POL-TP-PTF-ESTNO
		//                     :IND-POL-TP-PTF-ESTNO
		//                    ,:POL-FL-CUM-PRE-CNTR
		//                     :IND-POL-FL-CUM-PRE-CNTR
		//                    ,:POL-FL-AMMB-MOVI
		//                     :IND-POL-FL-AMMB-MOVI
		//                    ,:POL-CONV-GECO
		//                     :IND-POL-CONV-GECO
		//                    ,:POL-DS-RIGA
		//                    ,:POL-DS-OPER-SQL
		//                    ,:POL-DS-VER
		//                    ,:POL-DS-TS-INI-CPTZ
		//                    ,:POL-DS-TS-END-CPTZ
		//                    ,:POL-DS-UTENTE
		//                    ,:POL-DS-STATO-ELAB
		//                    ,:POL-FL-SCUDO-FISC
		//                     :IND-POL-FL-SCUDO-FISC
		//                    ,:POL-FL-TRASFE
		//                     :IND-POL-FL-TRASFE
		//                    ,:POL-FL-TFR-STRC
		//                     :IND-POL-FL-TFR-STRC
		//                    ,:POL-DT-PRESC-DB
		//                     :IND-POL-DT-PRESC
		//                    ,:POL-COD-CONV-AGG
		//                     :IND-POL-COD-CONV-AGG
		//                    ,:POL-SUBCAT-PROD
		//                     :IND-POL-SUBCAT-PROD
		//                    ,:POL-FL-QUEST-ADEGZ-ASS
		//                     :IND-POL-FL-QUEST-ADEGZ-ASS
		//                    ,:POL-COD-TPA
		//                     :IND-POL-COD-TPA
		//                    ,:POL-ID-ACC-COMM
		//                     :IND-POL-ID-ACC-COMM
		//                    ,:POL-FL-POLI-CPI-PR
		//                     :IND-POL-FL-POLI-CPI-PR
		//                    ,:POL-FL-POLI-BUNDLING
		//                     :IND-POL-FL-POLI-BUNDLING
		//                    ,:POL-IND-POLI-PRIN-COLL
		//                     :IND-POL-IND-POLI-PRIN-COLL
		//                    ,:POL-FL-VND-BUNDLE
		//                     :IND-POL-FL-VND-BUNDLE
		//                    ,:POL-IB-BS
		//                     :IND-POL-IB-BS
		//                    ,:POL-FL-POLI-IFP
		//                     :IND-POL-FL-POLI-IFP
		//                    ,:STB-ID-STAT-OGG-BUS
		//                    ,:STB-ID-OGG
		//                    ,:STB-TP-OGG
		//                    ,:STB-ID-MOVI-CRZ
		//                    ,:STB-ID-MOVI-CHIU
		//                     :IND-STB-ID-MOVI-CHIU
		//                    ,:STB-DT-INI-EFF-DB
		//                    ,:STB-DT-END-EFF-DB
		//                    ,:STB-COD-COMP-ANIA
		//                    ,:STB-TP-STAT-BUS
		//                    ,:STB-TP-CAUS
		//                    ,:STB-DS-RIGA
		//                    ,:STB-DS-OPER-SQL
		//                    ,:STB-DS-VER
		//                    ,:STB-DS-TS-INI-CPTZ
		//                    ,:STB-DS-TS-END-CPTZ
		//                    ,:STB-DS-UTENTE
		//                    ,:STB-DS-STATO-ELAB
		//                  FROM ADES         A,
		//                       POLI         B,
		//                       STAT_OGG_BUS C
		//                  WHERE B.IB_OGG   BETWEEN   :WLB-IB-POLI-FIRST AND
		//                                             :WLB-IB-POLI-LAST
		//                    AND A.ID_POLI         =   B.ID_POLI
		//                    AND A.ID_ADES         =   C.ID_OGG
		//                    AND C.TP_OGG          =  'AD'
		//                    AND C.TP_STAT_BUS     =  'VI'
		//           *        OR
		//           *            (C.TP_STAT_BUS    =  'ST'  AND
		//           *            EXISTS (SELECT
		//           *                  D.ID_MOVI_FINRIO
		//           *
		//           *               FROM  MOVI_FINRIO D
		//           *               WHERE A.ID_ADES = D.ID_ADES
		//           *                 AND D.STAT_MOVI IN ('AL','IE')
		//           *                 AND D.TP_MOVI_FINRIO = 'DI'
		//           *                 AND D.COD_COMP_ANIA   =
		//           *                     :IDSV0003-CODICE-COMPAGNIA-ANIA
		//           *                 AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
		//           *                 AND D.DT_END_EFF     >   :WS-DT-PTF-X
		//           *                 AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//           *                 AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
		//                  AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
		//                  AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
		//                  AND A.DT_END_EFF     >   :WS-DT-PTF-X
		//                  AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//                  AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
		//                  AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
		//                  AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
		//                  AND B.DT_END_EFF     >   :WS-DT-PTF-X
		//                  AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//                  AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
		//                  AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
		//                  AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
		//                  AND C.DT_END_EFF     >   :WS-DT-PTF-X
		//                  AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//                  AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
		//           *      AND A.DS_STATO_ELAB IN (
		//           *                              :IABV0002-STATE-01,
		//           *                              :IABV0002-STATE-02,
		//           *                              :IABV0002-STATE-03,
		//           *                              :IABV0002-STATE-04,
		//           *                              :IABV0002-STATE-05,
		//           *                              :IABV0002-STATE-06,
		//           *                              :IABV0002-STATE-07,
		//           *                              :IABV0002-STATE-08,
		//           *                              :IABV0002-STATE-09,
		//           *                              :IABV0002-STATE-10
		//           *                                  )
		//           *        AND NOT A.DS_VER     = :IABV0009-VERSIONING
		//                    ORDER BY A.ID_POLI, A.ID_ADES
		//                   FETCH FIRST ROW ONLY
		//                END-EXEC.
		adesPoliStatOggBusDao.selectRec3(this.getAdesPoliStatOggBusLdbm02501());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			z100SetColonneNull();
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		}
	}

	/**Original name: A320-UPDATE-SC03<br>
	 * <pre>*****************************************************************</pre>*/
	private void a320UpdateSc03() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-PRIMARY-KEY
		//                   PERFORM A330-UPDATE-PK-SC03         THRU A330-SC03-EX
		//              WHEN IDSV0003-WHERE-CONDITION-03
		//                   PERFORM A340-UPDATE-WHERE-COND-SC03 THRU A340-SC03-EX
		//              WHEN IDSV0003-FIRST-ACTION
		//                                                       THRU A345-SC03-EX
		//              WHEN OTHER
		//                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
		//           END-EVALUATE.
		if (idsv0003.getLivelloOperazione().isIdsv0003PrimaryKey()) {
			// COB_CODE: PERFORM A330-UPDATE-PK-SC03         THRU A330-SC03-EX
			a330UpdatePkSc03();
		} else if (idsv0003.getTipologiaOperazione().isIdsv0003WhereCondition03()) {
			// COB_CODE: PERFORM A340-UPDATE-WHERE-COND-SC03 THRU A340-SC03-EX
			a340UpdateWhereCondSc03();
		} else if (idsv0003.getLivelloOperazione().isIdsv0003FirstAction()) {
			// COB_CODE: PERFORM A345-UPDATE-FIRST-ACTION-SC03
			//                                               THRU A345-SC03-EX
			a345UpdateFirstActionSc03();
		} else {
			// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
			idsv0003.getReturnCode().setInvalidLevelOper();
		}
	}

	/**Original name: A330-UPDATE-PK-SC03<br>
	 * <pre>*****************************************************************</pre>*/
	private void a330UpdatePkSc03() {
		// COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
		z150ValorizzaDataServices();
		// COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
		z200SetIndicatoriNull();
		// COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBM0250.cbl:line=4105, because the code is unreachable.
		// COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
		z960LengthVchar();
		//           ,DS_VER                 = :IABV0009-VERSIONING
		// COB_CODE:      EXEC SQL
		//                     UPDATE ADES
		//                     SET
		//                        DS_OPER_SQL            = :ADE-DS-OPER-SQL
		//           *           ,DS_VER                 = :IABV0009-VERSIONING
		//                       ,DS_UTENTE              = :ADE-DS-UTENTE
		//                       ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
		//                     WHERE             DS_RIGA = :ADE-DS-RIGA
		//                END-EXEC.
		adesDao.updateRec2(ades.getAdeDsOperSql(), ades.getAdeDsUtente(), iabv0002.getIabv0002StateCurrent(), ades.getAdeDsRiga());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A340-UPDATE-WHERE-COND-SC03<br>
	 * <pre>*****************************************************************</pre>*/
	private void a340UpdateWhereCondSc03() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A345-UPDATE-FIRST-ACTION-SC03<br>
	 * <pre>*****************************************************************</pre>*/
	private void a345UpdateFirstActionSc03() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A360-OPEN-CURSOR-SC03<br>
	 * <pre>*****************************************************************</pre>*/
	private void a360OpenCursorSc03() {
		// COB_CODE: PERFORM A305-DECLARE-CURSOR-SC03 THRU A305-SC03-EX.
		a305DeclareCursorSc03();
		// COB_CODE: EXEC SQL
		//                OPEN CUR-SC03
		//           END-EXEC.
		adesPoliStatOggBusDao.openCurSc03(this.getAdesPoliStatOggBusLdbm02501());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A370-CLOSE-CURSOR-SC03<br>
	 * <pre>*****************************************************************</pre>*/
	private void a370CloseCursorSc03() {
		// COB_CODE: EXEC SQL
		//                CLOSE CUR-SC03
		//           END-EXEC.
		adesPoliStatOggBusDao.closeCurSc03();
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A380-FETCH-FIRST-SC03<br>
	 * <pre>*****************************************************************</pre>*/
	private void a380FetchFirstSc03() {
		// COB_CODE: PERFORM A360-OPEN-CURSOR-SC03    THRU A360-SC03-EX.
		a360OpenCursorSc03();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM A390-FETCH-NEXT-SC03  THRU A390-SC03-EX
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM A390-FETCH-NEXT-SC03  THRU A390-SC03-EX
			a390FetchNextSc03();
		}
	}

	/**Original name: A390-FETCH-NEXT-SC03<br>
	 * <pre>*****************************************************************</pre>*/
	private void a390FetchNextSc03() {
		// COB_CODE: EXEC SQL
		//                FETCH CUR-SC03
		//           INTO
		//                :ADE-ID-ADES
		//               ,:ADE-ID-POLI
		//               ,:ADE-ID-MOVI-CRZ
		//               ,:ADE-ID-MOVI-CHIU
		//                :IND-ADE-ID-MOVI-CHIU
		//               ,:ADE-DT-INI-EFF-DB
		//               ,:ADE-DT-END-EFF-DB
		//               ,:ADE-IB-PREV
		//                :IND-ADE-IB-PREV
		//               ,:ADE-IB-OGG
		//                :IND-ADE-IB-OGG
		//               ,:ADE-COD-COMP-ANIA
		//               ,:ADE-DT-DECOR-DB
		//                :IND-ADE-DT-DECOR
		//               ,:ADE-DT-SCAD-DB
		//                :IND-ADE-DT-SCAD
		//               ,:ADE-ETA-A-SCAD
		//                :IND-ADE-ETA-A-SCAD
		//               ,:ADE-DUR-AA
		//                :IND-ADE-DUR-AA
		//               ,:ADE-DUR-MM
		//                :IND-ADE-DUR-MM
		//               ,:ADE-DUR-GG
		//                :IND-ADE-DUR-GG
		//               ,:ADE-TP-RGM-FISC
		//               ,:ADE-TP-RIAT
		//                :IND-ADE-TP-RIAT
		//               ,:ADE-TP-MOD-PAG-TIT
		//               ,:ADE-TP-IAS
		//                :IND-ADE-TP-IAS
		//               ,:ADE-DT-VARZ-TP-IAS-DB
		//                :IND-ADE-DT-VARZ-TP-IAS
		//               ,:ADE-PRE-NET-IND
		//                :IND-ADE-PRE-NET-IND
		//               ,:ADE-PRE-LRD-IND
		//                :IND-ADE-PRE-LRD-IND
		//               ,:ADE-RAT-LRD-IND
		//                :IND-ADE-RAT-LRD-IND
		//               ,:ADE-PRSTZ-INI-IND
		//                :IND-ADE-PRSTZ-INI-IND
		//               ,:ADE-FL-COINC-ASSTO
		//                :IND-ADE-FL-COINC-ASSTO
		//               ,:ADE-IB-DFLT
		//                :IND-ADE-IB-DFLT
		//               ,:ADE-MOD-CALC
		//                :IND-ADE-MOD-CALC
		//               ,:ADE-TP-FNT-CNBTVA
		//                :IND-ADE-TP-FNT-CNBTVA
		//               ,:ADE-IMP-AZ
		//                :IND-ADE-IMP-AZ
		//               ,:ADE-IMP-ADER
		//                :IND-ADE-IMP-ADER
		//               ,:ADE-IMP-TFR
		//                :IND-ADE-IMP-TFR
		//               ,:ADE-IMP-VOLO
		//                :IND-ADE-IMP-VOLO
		//               ,:ADE-PC-AZ
		//                :IND-ADE-PC-AZ
		//               ,:ADE-PC-ADER
		//                :IND-ADE-PC-ADER
		//               ,:ADE-PC-TFR
		//                :IND-ADE-PC-TFR
		//               ,:ADE-PC-VOLO
		//                :IND-ADE-PC-VOLO
		//               ,:ADE-DT-NOVA-RGM-FISC-DB
		//                :IND-ADE-DT-NOVA-RGM-FISC
		//               ,:ADE-FL-ATTIV
		//                :IND-ADE-FL-ATTIV
		//               ,:ADE-IMP-REC-RIT-VIS
		//                :IND-ADE-IMP-REC-RIT-VIS
		//               ,:ADE-IMP-REC-RIT-ACC
		//                :IND-ADE-IMP-REC-RIT-ACC
		//               ,:ADE-FL-VARZ-STAT-TBGC
		//                :IND-ADE-FL-VARZ-STAT-TBGC
		//               ,:ADE-FL-PROVZA-MIGRAZ
		//                :IND-ADE-FL-PROVZA-MIGRAZ
		//               ,:ADE-IMPB-VIS-DA-REC
		//                :IND-ADE-IMPB-VIS-DA-REC
		//               ,:ADE-DT-DECOR-PREST-BAN-DB
		//                :IND-ADE-DT-DECOR-PREST-BAN
		//               ,:ADE-DT-EFF-VARZ-STAT-T-DB
		//                :IND-ADE-DT-EFF-VARZ-STAT-T
		//               ,:ADE-DS-RIGA
		//               ,:ADE-DS-OPER-SQL
		//               ,:ADE-DS-VER
		//               ,:ADE-DS-TS-INI-CPTZ
		//               ,:ADE-DS-TS-END-CPTZ
		//               ,:ADE-DS-UTENTE
		//               ,:ADE-DS-STATO-ELAB
		//               ,:ADE-CUM-CNBT-CAP
		//                :IND-ADE-CUM-CNBT-CAP
		//               ,:ADE-IMP-GAR-CNBT
		//                :IND-ADE-IMP-GAR-CNBT
		//               ,:ADE-DT-ULT-CONS-CNBT-DB
		//                :IND-ADE-DT-ULT-CONS-CNBT
		//               ,:ADE-IDEN-ISC-FND
		//                :IND-ADE-IDEN-ISC-FND
		//               ,:ADE-NUM-RAT-PIAN
		//                :IND-ADE-NUM-RAT-PIAN
		//               ,:ADE-DT-PRESC-DB
		//                :IND-ADE-DT-PRESC
		//               ,:ADE-CONCS-PREST
		//                :IND-ADE-CONCS-PREST
		//               ,:POL-ID-POLI
		//               ,:POL-ID-MOVI-CRZ
		//               ,:POL-ID-MOVI-CHIU
		//                :IND-POL-ID-MOVI-CHIU
		//               ,:POL-IB-OGG
		//                :IND-POL-IB-OGG
		//               ,:POL-IB-PROP
		//               ,:POL-DT-PROP-DB
		//                :IND-POL-DT-PROP
		//               ,:POL-DT-INI-EFF-DB
		//               ,:POL-DT-END-EFF-DB
		//               ,:POL-COD-COMP-ANIA
		//               ,:POL-DT-DECOR-DB
		//               ,:POL-DT-EMIS-DB
		//               ,:POL-TP-POLI
		//               ,:POL-DUR-AA
		//                :IND-POL-DUR-AA
		//               ,:POL-DUR-MM
		//                :IND-POL-DUR-MM
		//               ,:POL-DT-SCAD-DB
		//                :IND-POL-DT-SCAD
		//               ,:POL-COD-PROD
		//               ,:POL-DT-INI-VLDT-PROD-DB
		//               ,:POL-COD-CONV
		//                :IND-POL-COD-CONV
		//               ,:POL-COD-RAMO
		//                :IND-POL-COD-RAMO
		//               ,:POL-DT-INI-VLDT-CONV-DB
		//                :IND-POL-DT-INI-VLDT-CONV
		//               ,:POL-DT-APPLZ-CONV-DB
		//                :IND-POL-DT-APPLZ-CONV
		//               ,:POL-TP-FRM-ASSVA
		//               ,:POL-TP-RGM-FISC
		//                :IND-POL-TP-RGM-FISC
		//               ,:POL-FL-ESTAS
		//                :IND-POL-FL-ESTAS
		//               ,:POL-FL-RSH-COMUN
		//                :IND-POL-FL-RSH-COMUN
		//               ,:POL-FL-RSH-COMUN-COND
		//                :IND-POL-FL-RSH-COMUN-COND
		//               ,:POL-TP-LIV-GENZ-TIT
		//               ,:POL-FL-COP-FINANZ
		//                :IND-POL-FL-COP-FINANZ
		//               ,:POL-TP-APPLZ-DIR
		//                :IND-POL-TP-APPLZ-DIR
		//               ,:POL-SPE-MED
		//                :IND-POL-SPE-MED
		//               ,:POL-DIR-EMIS
		//                :IND-POL-DIR-EMIS
		//               ,:POL-DIR-1O-VERS
		//                :IND-POL-DIR-1O-VERS
		//               ,:POL-DIR-VERS-AGG
		//                :IND-POL-DIR-VERS-AGG
		//               ,:POL-COD-DVS
		//                :IND-POL-COD-DVS
		//               ,:POL-FL-FNT-AZ
		//                :IND-POL-FL-FNT-AZ
		//               ,:POL-FL-FNT-ADER
		//                :IND-POL-FL-FNT-ADER
		//               ,:POL-FL-FNT-TFR
		//                :IND-POL-FL-FNT-TFR
		//               ,:POL-FL-FNT-VOLO
		//                :IND-POL-FL-FNT-VOLO
		//               ,:POL-TP-OPZ-A-SCAD
		//                :IND-POL-TP-OPZ-A-SCAD
		//               ,:POL-AA-DIFF-PROR-DFLT
		//                :IND-POL-AA-DIFF-PROR-DFLT
		//               ,:POL-FL-VER-PROD
		//                :IND-POL-FL-VER-PROD
		//               ,:POL-DUR-GG
		//                :IND-POL-DUR-GG
		//               ,:POL-DIR-QUIET
		//                :IND-POL-DIR-QUIET
		//               ,:POL-TP-PTF-ESTNO
		//                :IND-POL-TP-PTF-ESTNO
		//               ,:POL-FL-CUM-PRE-CNTR
		//                :IND-POL-FL-CUM-PRE-CNTR
		//               ,:POL-FL-AMMB-MOVI
		//                :IND-POL-FL-AMMB-MOVI
		//               ,:POL-CONV-GECO
		//                :IND-POL-CONV-GECO
		//               ,:POL-DS-RIGA
		//               ,:POL-DS-OPER-SQL
		//               ,:POL-DS-VER
		//               ,:POL-DS-TS-INI-CPTZ
		//               ,:POL-DS-TS-END-CPTZ
		//               ,:POL-DS-UTENTE
		//               ,:POL-DS-STATO-ELAB
		//               ,:POL-FL-SCUDO-FISC
		//                :IND-POL-FL-SCUDO-FISC
		//               ,:POL-FL-TRASFE
		//                :IND-POL-FL-TRASFE
		//               ,:POL-FL-TFR-STRC
		//                :IND-POL-FL-TFR-STRC
		//               ,:POL-DT-PRESC-DB
		//                :IND-POL-DT-PRESC
		//               ,:POL-COD-CONV-AGG
		//                :IND-POL-COD-CONV-AGG
		//               ,:POL-SUBCAT-PROD
		//                :IND-POL-SUBCAT-PROD
		//               ,:POL-FL-QUEST-ADEGZ-ASS
		//                :IND-POL-FL-QUEST-ADEGZ-ASS
		//               ,:POL-COD-TPA
		//                :IND-POL-COD-TPA
		//               ,:POL-ID-ACC-COMM
		//                :IND-POL-ID-ACC-COMM
		//               ,:POL-FL-POLI-CPI-PR
		//                :IND-POL-FL-POLI-CPI-PR
		//               ,:POL-FL-POLI-BUNDLING
		//                :IND-POL-FL-POLI-BUNDLING
		//               ,:POL-IND-POLI-PRIN-COLL
		//                :IND-POL-IND-POLI-PRIN-COLL
		//               ,:POL-FL-VND-BUNDLE
		//                :IND-POL-FL-VND-BUNDLE
		//               ,:POL-IB-BS
		//                :IND-POL-IB-BS
		//               ,:POL-FL-POLI-IFP
		//                :IND-POL-FL-POLI-IFP
		//               ,:STB-ID-STAT-OGG-BUS
		//               ,:STB-ID-OGG
		//               ,:STB-TP-OGG
		//               ,:STB-ID-MOVI-CRZ
		//               ,:STB-ID-MOVI-CHIU
		//                :IND-STB-ID-MOVI-CHIU
		//               ,:STB-DT-INI-EFF-DB
		//               ,:STB-DT-END-EFF-DB
		//               ,:STB-COD-COMP-ANIA
		//               ,:STB-TP-STAT-BUS
		//               ,:STB-TP-CAUS
		//               ,:STB-DS-RIGA
		//               ,:STB-DS-OPER-SQL
		//               ,:STB-DS-VER
		//               ,:STB-DS-TS-INI-CPTZ
		//               ,:STB-DS-TS-END-CPTZ
		//               ,:STB-DS-UTENTE
		//               ,:STB-DS-STATO-ELAB
		//           END-EXEC.
		adesPoliStatOggBusDao.fetchCurSc03(this.getAdesPoliStatOggBusLdbm0250());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           ELSE
		//             END-IF
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			z100SetColonneNull();
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		} else if (idsv0003.getSqlcode().isNotFound()) {
			// COB_CODE: IF IDSV0003-NOT-FOUND
			//              END-IF
			//           END-IF
			// COB_CODE: PERFORM A370-CLOSE-CURSOR-SC03 THRU A370-SC03-EX
			a370CloseCursorSc03();
			// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
			//              SET IDSV0003-NOT-FOUND TO TRUE
			//           END-IF
			if (idsv0003.getSqlcode().isSuccessfulSql()) {
				// COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
				idsv0003.getSqlcode().setNotFound();
			}
		}
	}

	/**Original name: SC04-SELECTION-CURSOR-04<br>
	 * <pre>*****************************************************************
	 * *****************************************************************</pre>*/
	private void sc04SelectionCursor04() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-SELECT
		//                 PERFORM A310-SELECT-SC04           THRU A310-SC04-EX
		//              WHEN IDSV0003-OPEN-CURSOR
		//                 PERFORM A360-OPEN-CURSOR-SC04      THRU A360-SC04-EX
		//              WHEN IDSV0003-CLOSE-CURSOR
		//                 PERFORM A370-CLOSE-CURSOR-SC04     THRU A370-SC04-EX
		//              WHEN IDSV0003-FETCH-FIRST
		//                 PERFORM A380-FETCH-FIRST-SC04      THRU A380-SC04-EX
		//              WHEN IDSV0003-FETCH-NEXT
		//                 PERFORM A390-FETCH-NEXT-SC04       THRU A390-SC04-EX
		//              WHEN IDSV0003-UPDATE
		//                 PERFORM A320-UPDATE-SC04           THRU A320-SC04-EX
		//              WHEN OTHER
		//                 SET IDSV0003-INVALID-OPER          TO TRUE
		//           END-EVALUATE.
		if (idsv0003.getOperazione().isSelect()) {
			// COB_CODE: PERFORM A310-SELECT-SC04           THRU A310-SC04-EX
			a310SelectSc04();
		} else if (idsv0003.getOperazione().isOpenCursor()) {
			// COB_CODE: PERFORM A360-OPEN-CURSOR-SC04      THRU A360-SC04-EX
			a360OpenCursorSc04();
		} else if (idsv0003.getOperazione().isCloseCursor()) {
			// COB_CODE: PERFORM A370-CLOSE-CURSOR-SC04     THRU A370-SC04-EX
			a370CloseCursorSc04();
		} else if (idsv0003.getOperazione().isFetchFirst()) {
			// COB_CODE: PERFORM A380-FETCH-FIRST-SC04      THRU A380-SC04-EX
			a380FetchFirstSc04();
		} else if (idsv0003.getOperazione().isFetchNext()) {
			// COB_CODE: PERFORM A390-FETCH-NEXT-SC04       THRU A390-SC04-EX
			a390FetchNextSc04();
		} else if (idsv0003.getOperazione().isUpdate()) {
			// COB_CODE: PERFORM A320-UPDATE-SC04           THRU A320-SC04-EX
			a320UpdateSc04();
		} else {
			// COB_CODE: SET IDSV0003-INVALID-OPER          TO TRUE
			idsv0003.getReturnCode().setInvalidOper();
		}
	}

	/**Original name: A305-DECLARE-CURSOR-SC04<br>
	 * <pre>*****************************************************************</pre>*/
	private void a305DeclareCursorSc04() {
		// COB_CODE:      EXEC SQL
		//                     DECLARE CUR-SC04 CURSOR WITH HOLD FOR
		//                  SELECT
		//                      A.ID_ADES
		//                     ,A.ID_POLI
		//                     ,A.ID_MOVI_CRZ
		//                     ,A.ID_MOVI_CHIU
		//                     ,A.DT_INI_EFF
		//                     ,A.DT_END_EFF
		//                     ,A.IB_PREV
		//                     ,A.IB_OGG
		//                     ,A.COD_COMP_ANIA
		//                     ,A.DT_DECOR
		//                     ,A.DT_SCAD
		//                     ,A.ETA_A_SCAD
		//                     ,A.DUR_AA
		//                     ,A.DUR_MM
		//                     ,A.DUR_GG
		//                     ,A.TP_RGM_FISC
		//                     ,A.TP_RIAT
		//                     ,A.TP_MOD_PAG_TIT
		//                     ,A.TP_IAS
		//                     ,A.DT_VARZ_TP_IAS
		//                     ,A.PRE_NET_IND
		//                     ,A.PRE_LRD_IND
		//                     ,A.RAT_LRD_IND
		//                     ,A.PRSTZ_INI_IND
		//                     ,A.FL_COINC_ASSTO
		//                     ,A.IB_DFLT
		//                     ,A.MOD_CALC
		//                     ,A.TP_FNT_CNBTVA
		//                     ,A.IMP_AZ
		//                     ,A.IMP_ADER
		//                     ,A.IMP_TFR
		//                     ,A.IMP_VOLO
		//                     ,A.PC_AZ
		//                     ,A.PC_ADER
		//                     ,A.PC_TFR
		//                     ,A.PC_VOLO
		//                     ,A.DT_NOVA_RGM_FISC
		//                     ,A.FL_ATTIV
		//                     ,A.IMP_REC_RIT_VIS
		//                     ,A.IMP_REC_RIT_ACC
		//                     ,A.FL_VARZ_STAT_TBGC
		//                     ,A.FL_PROVZA_MIGRAZ
		//                     ,A.IMPB_VIS_DA_REC
		//                     ,A.DT_DECOR_PREST_BAN
		//                     ,A.DT_EFF_VARZ_STAT_T
		//                     ,A.DS_RIGA
		//                     ,A.DS_OPER_SQL
		//                     ,A.DS_VER
		//                     ,A.DS_TS_INI_CPTZ
		//                     ,A.DS_TS_END_CPTZ
		//                     ,A.DS_UTENTE
		//                     ,A.DS_STATO_ELAB
		//                     ,A.CUM_CNBT_CAP
		//                     ,A.IMP_GAR_CNBT
		//                     ,A.DT_ULT_CONS_CNBT
		//                     ,A.IDEN_ISC_FND
		//                     ,A.NUM_RAT_PIAN
		//                     ,A.DT_PRESC
		//                     ,A.CONCS_PREST
		//                     ,B.ID_POLI
		//                     ,B.ID_MOVI_CRZ
		//                     ,B.ID_MOVI_CHIU
		//                     ,B.IB_OGG
		//                     ,B.IB_PROP
		//                     ,B.DT_PROP
		//                     ,B.DT_INI_EFF
		//                     ,B.DT_END_EFF
		//                     ,B.COD_COMP_ANIA
		//                     ,B.DT_DECOR
		//                     ,B.DT_EMIS
		//                     ,B.TP_POLI
		//                     ,B.DUR_AA
		//                     ,B.DUR_MM
		//                     ,B.DT_SCAD
		//                     ,B.COD_PROD
		//                     ,B.DT_INI_VLDT_PROD
		//                     ,B.COD_CONV
		//                     ,B.COD_RAMO
		//                     ,B.DT_INI_VLDT_CONV
		//                     ,B.DT_APPLZ_CONV
		//                     ,B.TP_FRM_ASSVA
		//                     ,B.TP_RGM_FISC
		//                     ,B.FL_ESTAS
		//                     ,B.FL_RSH_COMUN
		//                     ,B.FL_RSH_COMUN_COND
		//                     ,B.TP_LIV_GENZ_TIT
		//                     ,B.FL_COP_FINANZ
		//                     ,B.TP_APPLZ_DIR
		//                     ,B.SPE_MED
		//                     ,B.DIR_EMIS
		//                     ,B.DIR_1O_VERS
		//                     ,B.DIR_VERS_AGG
		//                     ,B.COD_DVS
		//                     ,B.FL_FNT_AZ
		//                     ,B.FL_FNT_ADER
		//                     ,B.FL_FNT_TFR
		//                     ,B.FL_FNT_VOLO
		//                     ,B.TP_OPZ_A_SCAD
		//                     ,B.AA_DIFF_PROR_DFLT
		//                     ,B.FL_VER_PROD
		//                     ,B.DUR_GG
		//                     ,B.DIR_QUIET
		//                     ,B.TP_PTF_ESTNO
		//                     ,B.FL_CUM_PRE_CNTR
		//                     ,B.FL_AMMB_MOVI
		//                     ,B.CONV_GECO
		//                     ,B.DS_RIGA
		//                     ,B.DS_OPER_SQL
		//                     ,B.DS_VER
		//                     ,B.DS_TS_INI_CPTZ
		//                     ,B.DS_TS_END_CPTZ
		//                     ,B.DS_UTENTE
		//                     ,B.DS_STATO_ELAB
		//                     ,B.FL_SCUDO_FISC
		//                     ,B.FL_TRASFE
		//                     ,B.FL_TFR_STRC
		//                     ,B.DT_PRESC
		//                     ,B.COD_CONV_AGG
		//                     ,B.SUBCAT_PROD
		//                     ,B.FL_QUEST_ADEGZ_ASS
		//                     ,B.COD_TPA
		//                     ,B.ID_ACC_COMM
		//                     ,B.FL_POLI_CPI_PR
		//                     ,B.FL_POLI_BUNDLING
		//                     ,B.IND_POLI_PRIN_COLL
		//                     ,B.FL_VND_BUNDLE
		//                     ,B.IB_BS
		//                     ,B.FL_POLI_IFP
		//                     ,C.ID_STAT_OGG_BUS
		//                     ,C.ID_OGG
		//                     ,C.TP_OGG
		//                     ,C.ID_MOVI_CRZ
		//                     ,C.ID_MOVI_CHIU
		//                     ,C.DT_INI_EFF
		//                     ,C.DT_END_EFF
		//                     ,C.COD_COMP_ANIA
		//                     ,C.TP_STAT_BUS
		//                     ,C.TP_CAUS
		//                     ,C.DS_RIGA
		//                     ,C.DS_OPER_SQL
		//                     ,C.DS_VER
		//                     ,C.DS_TS_INI_CPTZ
		//                     ,C.DS_TS_END_CPTZ
		//                     ,C.DS_UTENTE
		//                     ,C.DS_STATO_ELAB
		//                  FROM ADES         A,
		//                       POLI         B,
		//                       STAT_OGG_BUS C
		//                  WHERE A.IB_OGG    BETWEEN  :WLB-IB-ADE-FIRST AND
		//                                             :WLB-IB-ADE-LAST
		//                    AND B.IB_OGG          =  :WLB-IB-POLI-FIRST
		//                    AND A.ID_POLI         =   B.ID_POLI
		//                    AND A.ID_ADES         =   C.ID_OGG
		//                    AND C.TP_OGG          =  'AD'
		//                    AND C.TP_STAT_BUS     =  'VI'
		//           *        OR
		//           *            (C.TP_STAT_BUS    =  'ST'  AND
		//           *            EXISTS (SELECT
		//           *                  D.ID_MOVI_FINRIO
		//           *
		//           *               FROM  MOVI_FINRIO D
		//           *               WHERE A.ID_ADES = D.ID_ADES
		//           *                 AND D.STAT_MOVI IN ('AL','IE')
		//           *                 AND D.TP_MOVI_FINRIO = 'DI'
		//           *                 AND D.COD_COMP_ANIA   =
		//           *                     :IDSV0003-CODICE-COMPAGNIA-ANIA
		//           *                 AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
		//           *                 AND D.DT_END_EFF     >   :WS-DT-PTF-X
		//           *                 AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//           *                 AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
		//                  AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
		//                  AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
		//                  AND A.DT_END_EFF     >   :WS-DT-PTF-X
		//                  AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//                  AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
		//                  AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
		//                  AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
		//                  AND B.DT_END_EFF     >   :WS-DT-PTF-X
		//                  AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//                  AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
		//                  AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
		//                  AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
		//                  AND C.DT_END_EFF     >   :WS-DT-PTF-X
		//                  AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//                  AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
		//           *      AND A.DS_STATO_ELAB IN (
		//           *                              :IABV0002-STATE-01,
		//           *                              :IABV0002-STATE-02,
		//           *                              :IABV0002-STATE-03,
		//           *                              :IABV0002-STATE-04,
		//           *                              :IABV0002-STATE-05,
		//           *                              :IABV0002-STATE-06,
		//           *                              :IABV0002-STATE-07,
		//           *                              :IABV0002-STATE-08,
		//           *                              :IABV0002-STATE-09,
		//           *                              :IABV0002-STATE-10
		//           *                                  )
		//           *        AND NOT A.DS_VER     = :IABV0009-VERSIONING
		//                    ORDER BY A.ID_POLI, A.ID_ADES
		//                END-EXEC.
		// DECLARE CURSOR doesn't need a translation;
	}

	/**Original name: A310-SELECT-SC04<br>
	 * <pre>*****************************************************************
	 *       AND A.DS_STATO_ELAB IN (
	 *                               :IABV0002-STATE-01,
	 *                               :IABV0002-STATE-02,
	 *                               :IABV0002-STATE-03,
	 *                               :IABV0002-STATE-04,
	 *                               :IABV0002-STATE-05,
	 *                               :IABV0002-STATE-06,
	 *                               :IABV0002-STATE-07,
	 *                               :IABV0002-STATE-08,
	 *                               :IABV0002-STATE-09,
	 *                               :IABV0002-STATE-10
	 *                                   )
	 *         AND NOT A.DS_VER     = :IABV0009-VERSIONING</pre>*/
	private void a310SelectSc04() {
		// COB_CODE:      EXEC SQL
		//                  SELECT
		//                      A.ID_ADES
		//                     ,A.ID_POLI
		//                     ,A.ID_MOVI_CRZ
		//                     ,A.ID_MOVI_CHIU
		//                     ,A.DT_INI_EFF
		//                     ,A.DT_END_EFF
		//                     ,A.IB_PREV
		//                     ,A.IB_OGG
		//                     ,A.COD_COMP_ANIA
		//                     ,A.DT_DECOR
		//                     ,A.DT_SCAD
		//                     ,A.ETA_A_SCAD
		//                     ,A.DUR_AA
		//                     ,A.DUR_MM
		//                     ,A.DUR_GG
		//                     ,A.TP_RGM_FISC
		//                     ,A.TP_RIAT
		//                     ,A.TP_MOD_PAG_TIT
		//                     ,A.TP_IAS
		//                     ,A.DT_VARZ_TP_IAS
		//                     ,A.PRE_NET_IND
		//                     ,A.PRE_LRD_IND
		//                     ,A.RAT_LRD_IND
		//                     ,A.PRSTZ_INI_IND
		//                     ,A.FL_COINC_ASSTO
		//                     ,A.IB_DFLT
		//                     ,A.MOD_CALC
		//                     ,A.TP_FNT_CNBTVA
		//                     ,A.IMP_AZ
		//                     ,A.IMP_ADER
		//                     ,A.IMP_TFR
		//                     ,A.IMP_VOLO
		//                     ,A.PC_AZ
		//                     ,A.PC_ADER
		//                     ,A.PC_TFR
		//                     ,A.PC_VOLO
		//                     ,A.DT_NOVA_RGM_FISC
		//                     ,A.FL_ATTIV
		//                     ,A.IMP_REC_RIT_VIS
		//                     ,A.IMP_REC_RIT_ACC
		//                     ,A.FL_VARZ_STAT_TBGC
		//                     ,A.FL_PROVZA_MIGRAZ
		//                     ,A.IMPB_VIS_DA_REC
		//                     ,A.DT_DECOR_PREST_BAN
		//                     ,A.DT_EFF_VARZ_STAT_T
		//                     ,A.DS_RIGA
		//                     ,A.DS_OPER_SQL
		//                     ,A.DS_VER
		//                     ,A.DS_TS_INI_CPTZ
		//                     ,A.DS_TS_END_CPTZ
		//                     ,A.DS_UTENTE
		//                     ,A.DS_STATO_ELAB
		//                     ,A.CUM_CNBT_CAP
		//                     ,A.IMP_GAR_CNBT
		//                     ,A.DT_ULT_CONS_CNBT
		//                     ,A.IDEN_ISC_FND
		//                     ,A.NUM_RAT_PIAN
		//                     ,A.DT_PRESC
		//                     ,A.CONCS_PREST
		//                     ,B.ID_POLI
		//                     ,B.ID_MOVI_CRZ
		//                     ,B.ID_MOVI_CHIU
		//                     ,B.IB_OGG
		//                     ,B.IB_PROP
		//                     ,B.DT_PROP
		//                     ,B.DT_INI_EFF
		//                     ,B.DT_END_EFF
		//                     ,B.COD_COMP_ANIA
		//                     ,B.DT_DECOR
		//                     ,B.DT_EMIS
		//                     ,B.TP_POLI
		//                     ,B.DUR_AA
		//                     ,B.DUR_MM
		//                     ,B.DT_SCAD
		//                     ,B.COD_PROD
		//                     ,B.DT_INI_VLDT_PROD
		//                     ,B.COD_CONV
		//                     ,B.COD_RAMO
		//                     ,B.DT_INI_VLDT_CONV
		//                     ,B.DT_APPLZ_CONV
		//                     ,B.TP_FRM_ASSVA
		//                     ,B.TP_RGM_FISC
		//                     ,B.FL_ESTAS
		//                     ,B.FL_RSH_COMUN
		//                     ,B.FL_RSH_COMUN_COND
		//                     ,B.TP_LIV_GENZ_TIT
		//                     ,B.FL_COP_FINANZ
		//                     ,B.TP_APPLZ_DIR
		//                     ,B.SPE_MED
		//                     ,B.DIR_EMIS
		//                     ,B.DIR_1O_VERS
		//                     ,B.DIR_VERS_AGG
		//                     ,B.COD_DVS
		//                     ,B.FL_FNT_AZ
		//                     ,B.FL_FNT_ADER
		//                     ,B.FL_FNT_TFR
		//                     ,B.FL_FNT_VOLO
		//                     ,B.TP_OPZ_A_SCAD
		//                     ,B.AA_DIFF_PROR_DFLT
		//                     ,B.FL_VER_PROD
		//                     ,B.DUR_GG
		//                     ,B.DIR_QUIET
		//                     ,B.TP_PTF_ESTNO
		//                     ,B.FL_CUM_PRE_CNTR
		//                     ,B.FL_AMMB_MOVI
		//                     ,B.CONV_GECO
		//                     ,B.DS_RIGA
		//                     ,B.DS_OPER_SQL
		//                     ,B.DS_VER
		//                     ,B.DS_TS_INI_CPTZ
		//                     ,B.DS_TS_END_CPTZ
		//                     ,B.DS_UTENTE
		//                     ,B.DS_STATO_ELAB
		//                     ,B.FL_SCUDO_FISC
		//                     ,B.FL_TRASFE
		//                     ,B.FL_TFR_STRC
		//                     ,B.DT_PRESC
		//                     ,B.COD_CONV_AGG
		//                     ,B.SUBCAT_PROD
		//                     ,B.FL_QUEST_ADEGZ_ASS
		//                     ,B.COD_TPA
		//                     ,B.ID_ACC_COMM
		//                     ,B.FL_POLI_CPI_PR
		//                     ,B.FL_POLI_BUNDLING
		//                     ,B.IND_POLI_PRIN_COLL
		//                     ,B.FL_VND_BUNDLE
		//                     ,B.IB_BS
		//                     ,B.FL_POLI_IFP
		//                     ,C.ID_STAT_OGG_BUS
		//                     ,C.ID_OGG
		//                     ,C.TP_OGG
		//                     ,C.ID_MOVI_CRZ
		//                     ,C.ID_MOVI_CHIU
		//                     ,C.DT_INI_EFF
		//                     ,C.DT_END_EFF
		//                     ,C.COD_COMP_ANIA
		//                     ,C.TP_STAT_BUS
		//                     ,C.TP_CAUS
		//                     ,C.DS_RIGA
		//                     ,C.DS_OPER_SQL
		//                     ,C.DS_VER
		//                     ,C.DS_TS_INI_CPTZ
		//                     ,C.DS_TS_END_CPTZ
		//                     ,C.DS_UTENTE
		//                     ,C.DS_STATO_ELAB
		//                  INTO
		//                     :ADE-ID-ADES
		//                    ,:ADE-ID-POLI
		//                    ,:ADE-ID-MOVI-CRZ
		//                    ,:ADE-ID-MOVI-CHIU
		//                     :IND-ADE-ID-MOVI-CHIU
		//                    ,:ADE-DT-INI-EFF-DB
		//                    ,:ADE-DT-END-EFF-DB
		//                    ,:ADE-IB-PREV
		//                     :IND-ADE-IB-PREV
		//                    ,:ADE-IB-OGG
		//                     :IND-ADE-IB-OGG
		//                    ,:ADE-COD-COMP-ANIA
		//                    ,:ADE-DT-DECOR-DB
		//                     :IND-ADE-DT-DECOR
		//                    ,:ADE-DT-SCAD-DB
		//                     :IND-ADE-DT-SCAD
		//                    ,:ADE-ETA-A-SCAD
		//                     :IND-ADE-ETA-A-SCAD
		//                    ,:ADE-DUR-AA
		//                     :IND-ADE-DUR-AA
		//                    ,:ADE-DUR-MM
		//                     :IND-ADE-DUR-MM
		//                    ,:ADE-DUR-GG
		//                     :IND-ADE-DUR-GG
		//                    ,:ADE-TP-RGM-FISC
		//                    ,:ADE-TP-RIAT
		//                     :IND-ADE-TP-RIAT
		//                    ,:ADE-TP-MOD-PAG-TIT
		//                    ,:ADE-TP-IAS
		//                     :IND-ADE-TP-IAS
		//                    ,:ADE-DT-VARZ-TP-IAS-DB
		//                     :IND-ADE-DT-VARZ-TP-IAS
		//                    ,:ADE-PRE-NET-IND
		//                     :IND-ADE-PRE-NET-IND
		//                    ,:ADE-PRE-LRD-IND
		//                     :IND-ADE-PRE-LRD-IND
		//                    ,:ADE-RAT-LRD-IND
		//                     :IND-ADE-RAT-LRD-IND
		//                    ,:ADE-PRSTZ-INI-IND
		//                     :IND-ADE-PRSTZ-INI-IND
		//                    ,:ADE-FL-COINC-ASSTO
		//                     :IND-ADE-FL-COINC-ASSTO
		//                    ,:ADE-IB-DFLT
		//                     :IND-ADE-IB-DFLT
		//                    ,:ADE-MOD-CALC
		//                     :IND-ADE-MOD-CALC
		//                    ,:ADE-TP-FNT-CNBTVA
		//                     :IND-ADE-TP-FNT-CNBTVA
		//                    ,:ADE-IMP-AZ
		//                     :IND-ADE-IMP-AZ
		//                    ,:ADE-IMP-ADER
		//                     :IND-ADE-IMP-ADER
		//                    ,:ADE-IMP-TFR
		//                     :IND-ADE-IMP-TFR
		//                    ,:ADE-IMP-VOLO
		//                     :IND-ADE-IMP-VOLO
		//                    ,:ADE-PC-AZ
		//                     :IND-ADE-PC-AZ
		//                    ,:ADE-PC-ADER
		//                     :IND-ADE-PC-ADER
		//                    ,:ADE-PC-TFR
		//                     :IND-ADE-PC-TFR
		//                    ,:ADE-PC-VOLO
		//                     :IND-ADE-PC-VOLO
		//                    ,:ADE-DT-NOVA-RGM-FISC-DB
		//                     :IND-ADE-DT-NOVA-RGM-FISC
		//                    ,:ADE-FL-ATTIV
		//                     :IND-ADE-FL-ATTIV
		//                    ,:ADE-IMP-REC-RIT-VIS
		//                     :IND-ADE-IMP-REC-RIT-VIS
		//                    ,:ADE-IMP-REC-RIT-ACC
		//                     :IND-ADE-IMP-REC-RIT-ACC
		//                    ,:ADE-FL-VARZ-STAT-TBGC
		//                     :IND-ADE-FL-VARZ-STAT-TBGC
		//                    ,:ADE-FL-PROVZA-MIGRAZ
		//                     :IND-ADE-FL-PROVZA-MIGRAZ
		//                    ,:ADE-IMPB-VIS-DA-REC
		//                     :IND-ADE-IMPB-VIS-DA-REC
		//                    ,:ADE-DT-DECOR-PREST-BAN-DB
		//                     :IND-ADE-DT-DECOR-PREST-BAN
		//                    ,:ADE-DT-EFF-VARZ-STAT-T-DB
		//                     :IND-ADE-DT-EFF-VARZ-STAT-T
		//                    ,:ADE-DS-RIGA
		//                    ,:ADE-DS-OPER-SQL
		//                    ,:ADE-DS-VER
		//                    ,:ADE-DS-TS-INI-CPTZ
		//                    ,:ADE-DS-TS-END-CPTZ
		//                    ,:ADE-DS-UTENTE
		//                    ,:ADE-DS-STATO-ELAB
		//                    ,:ADE-CUM-CNBT-CAP
		//                     :IND-ADE-CUM-CNBT-CAP
		//                    ,:ADE-IMP-GAR-CNBT
		//                     :IND-ADE-IMP-GAR-CNBT
		//                    ,:ADE-DT-ULT-CONS-CNBT-DB
		//                     :IND-ADE-DT-ULT-CONS-CNBT
		//                    ,:ADE-IDEN-ISC-FND
		//                     :IND-ADE-IDEN-ISC-FND
		//                    ,:ADE-NUM-RAT-PIAN
		//                     :IND-ADE-NUM-RAT-PIAN
		//                    ,:ADE-DT-PRESC-DB
		//                     :IND-ADE-DT-PRESC
		//                    ,:ADE-CONCS-PREST
		//                     :IND-ADE-CONCS-PREST
		//                    ,:POL-ID-POLI
		//                    ,:POL-ID-MOVI-CRZ
		//                    ,:POL-ID-MOVI-CHIU
		//                     :IND-POL-ID-MOVI-CHIU
		//                    ,:POL-IB-OGG
		//                     :IND-POL-IB-OGG
		//                    ,:POL-IB-PROP
		//                    ,:POL-DT-PROP-DB
		//                     :IND-POL-DT-PROP
		//                    ,:POL-DT-INI-EFF-DB
		//                    ,:POL-DT-END-EFF-DB
		//                    ,:POL-COD-COMP-ANIA
		//                    ,:POL-DT-DECOR-DB
		//                    ,:POL-DT-EMIS-DB
		//                    ,:POL-TP-POLI
		//                    ,:POL-DUR-AA
		//                     :IND-POL-DUR-AA
		//                    ,:POL-DUR-MM
		//                     :IND-POL-DUR-MM
		//                    ,:POL-DT-SCAD-DB
		//                     :IND-POL-DT-SCAD
		//                    ,:POL-COD-PROD
		//                    ,:POL-DT-INI-VLDT-PROD-DB
		//                    ,:POL-COD-CONV
		//                     :IND-POL-COD-CONV
		//                    ,:POL-COD-RAMO
		//                     :IND-POL-COD-RAMO
		//                    ,:POL-DT-INI-VLDT-CONV-DB
		//                     :IND-POL-DT-INI-VLDT-CONV
		//                    ,:POL-DT-APPLZ-CONV-DB
		//                     :IND-POL-DT-APPLZ-CONV
		//                    ,:POL-TP-FRM-ASSVA
		//                    ,:POL-TP-RGM-FISC
		//                     :IND-POL-TP-RGM-FISC
		//                    ,:POL-FL-ESTAS
		//                     :IND-POL-FL-ESTAS
		//                    ,:POL-FL-RSH-COMUN
		//                     :IND-POL-FL-RSH-COMUN
		//                    ,:POL-FL-RSH-COMUN-COND
		//                     :IND-POL-FL-RSH-COMUN-COND
		//                    ,:POL-TP-LIV-GENZ-TIT
		//                    ,:POL-FL-COP-FINANZ
		//                     :IND-POL-FL-COP-FINANZ
		//                    ,:POL-TP-APPLZ-DIR
		//                     :IND-POL-TP-APPLZ-DIR
		//                    ,:POL-SPE-MED
		//                     :IND-POL-SPE-MED
		//                    ,:POL-DIR-EMIS
		//                     :IND-POL-DIR-EMIS
		//                    ,:POL-DIR-1O-VERS
		//                     :IND-POL-DIR-1O-VERS
		//                    ,:POL-DIR-VERS-AGG
		//                     :IND-POL-DIR-VERS-AGG
		//                    ,:POL-COD-DVS
		//                     :IND-POL-COD-DVS
		//                    ,:POL-FL-FNT-AZ
		//                     :IND-POL-FL-FNT-AZ
		//                    ,:POL-FL-FNT-ADER
		//                     :IND-POL-FL-FNT-ADER
		//                    ,:POL-FL-FNT-TFR
		//                     :IND-POL-FL-FNT-TFR
		//                    ,:POL-FL-FNT-VOLO
		//                     :IND-POL-FL-FNT-VOLO
		//                    ,:POL-TP-OPZ-A-SCAD
		//                     :IND-POL-TP-OPZ-A-SCAD
		//                    ,:POL-AA-DIFF-PROR-DFLT
		//                     :IND-POL-AA-DIFF-PROR-DFLT
		//                    ,:POL-FL-VER-PROD
		//                     :IND-POL-FL-VER-PROD
		//                    ,:POL-DUR-GG
		//                     :IND-POL-DUR-GG
		//                    ,:POL-DIR-QUIET
		//                     :IND-POL-DIR-QUIET
		//                    ,:POL-TP-PTF-ESTNO
		//                     :IND-POL-TP-PTF-ESTNO
		//                    ,:POL-FL-CUM-PRE-CNTR
		//                     :IND-POL-FL-CUM-PRE-CNTR
		//                    ,:POL-FL-AMMB-MOVI
		//                     :IND-POL-FL-AMMB-MOVI
		//                    ,:POL-CONV-GECO
		//                     :IND-POL-CONV-GECO
		//                    ,:POL-DS-RIGA
		//                    ,:POL-DS-OPER-SQL
		//                    ,:POL-DS-VER
		//                    ,:POL-DS-TS-INI-CPTZ
		//                    ,:POL-DS-TS-END-CPTZ
		//                    ,:POL-DS-UTENTE
		//                    ,:POL-DS-STATO-ELAB
		//                    ,:POL-FL-SCUDO-FISC
		//                     :IND-POL-FL-SCUDO-FISC
		//                    ,:POL-FL-TRASFE
		//                     :IND-POL-FL-TRASFE
		//                    ,:POL-FL-TFR-STRC
		//                     :IND-POL-FL-TFR-STRC
		//                    ,:POL-DT-PRESC-DB
		//                     :IND-POL-DT-PRESC
		//                    ,:POL-COD-CONV-AGG
		//                     :IND-POL-COD-CONV-AGG
		//                    ,:POL-SUBCAT-PROD
		//                     :IND-POL-SUBCAT-PROD
		//                    ,:POL-FL-QUEST-ADEGZ-ASS
		//                     :IND-POL-FL-QUEST-ADEGZ-ASS
		//                    ,:POL-COD-TPA
		//                     :IND-POL-COD-TPA
		//                    ,:POL-ID-ACC-COMM
		//                     :IND-POL-ID-ACC-COMM
		//                    ,:POL-FL-POLI-CPI-PR
		//                     :IND-POL-FL-POLI-CPI-PR
		//                    ,:POL-FL-POLI-BUNDLING
		//                     :IND-POL-FL-POLI-BUNDLING
		//                    ,:POL-IND-POLI-PRIN-COLL
		//                     :IND-POL-IND-POLI-PRIN-COLL
		//                    ,:POL-FL-VND-BUNDLE
		//                     :IND-POL-FL-VND-BUNDLE
		//                    ,:POL-IB-BS
		//                     :IND-POL-IB-BS
		//                    ,:POL-FL-POLI-IFP
		//                     :IND-POL-FL-POLI-IFP
		//                    ,:STB-ID-STAT-OGG-BUS
		//                    ,:STB-ID-OGG
		//                    ,:STB-TP-OGG
		//                    ,:STB-ID-MOVI-CRZ
		//                    ,:STB-ID-MOVI-CHIU
		//                     :IND-STB-ID-MOVI-CHIU
		//                    ,:STB-DT-INI-EFF-DB
		//                    ,:STB-DT-END-EFF-DB
		//                    ,:STB-COD-COMP-ANIA
		//                    ,:STB-TP-STAT-BUS
		//                    ,:STB-TP-CAUS
		//                    ,:STB-DS-RIGA
		//                    ,:STB-DS-OPER-SQL
		//                    ,:STB-DS-VER
		//                    ,:STB-DS-TS-INI-CPTZ
		//                    ,:STB-DS-TS-END-CPTZ
		//                    ,:STB-DS-UTENTE
		//                    ,:STB-DS-STATO-ELAB
		//                  FROM ADES         A,
		//                       POLI         B,
		//                       STAT_OGG_BUS C
		//                  WHERE  A.IB_OGG    BETWEEN  :WLB-IB-ADE-FIRST AND
		//                                             :WLB-IB-ADE-LAST
		//                    AND B.IB_OGG          =  :WLB-IB-POLI-FIRST
		//                    AND A.ID_POLI         =   B.ID_POLI
		//                    AND A.ID_ADES         =   C.ID_OGG
		//                    AND C.TP_OGG          =  'AD'
		//                    AND C.TP_STAT_BUS     =  'VI'
		//           *        OR
		//           *            (C.TP_STAT_BUS    =  'ST'  AND
		//           *            EXISTS (SELECT
		//           *                  D.ID_MOVI_FINRIO
		//           *
		//           *               FROM  MOVI_FINRIO D
		//           *               WHERE A.ID_ADES = D.ID_ADES
		//           *                 AND D.STAT_MOVI IN ('AL','IE')
		//           *                 AND D.TP_MOVI_FINRIO = 'DI'
		//           *                 AND D.COD_COMP_ANIA   =
		//           *                     :IDSV0003-CODICE-COMPAGNIA-ANIA
		//           *                 AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
		//           *                 AND D.DT_END_EFF     >   :WS-DT-PTF-X
		//           *                 AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//           *                 AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
		//                  AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
		//                  AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
		//                  AND A.DT_END_EFF     >   :WS-DT-PTF-X
		//                  AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//                  AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
		//                  AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
		//                  AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
		//                  AND B.DT_END_EFF     >   :WS-DT-PTF-X
		//                  AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//                  AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
		//                  AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
		//                  AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
		//                  AND C.DT_END_EFF     >   :WS-DT-PTF-X
		//                  AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
		//                  AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
		//           *      AND A.DS_STATO_ELAB IN (
		//           *                              :IABV0002-STATE-01,
		//           *                              :IABV0002-STATE-02,
		//           *                              :IABV0002-STATE-03,
		//           *                              :IABV0002-STATE-04,
		//           *                              :IABV0002-STATE-05,
		//           *                              :IABV0002-STATE-06,
		//           *                              :IABV0002-STATE-07,
		//           *                              :IABV0002-STATE-08,
		//           *                              :IABV0002-STATE-09,
		//           *                              :IABV0002-STATE-10
		//           *                                  )
		//           *        AND NOT A.DS_VER     = :IABV0009-VERSIONING
		//                    ORDER BY A.ID_POLI, A.ID_ADES
		//                   FETCH FIRST ROW ONLY
		//                END-EXEC.
		adesPoliStatOggBusDao.selectRec4(this.getAdesPoliStatOggBusLdbm02502());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			z100SetColonneNull();
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		}
	}

	/**Original name: A320-UPDATE-SC04<br>
	 * <pre>*****************************************************************</pre>*/
	private void a320UpdateSc04() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-PRIMARY-KEY
		//                   PERFORM A330-UPDATE-PK-SC04         THRU A330-SC04-EX
		//              WHEN IDSV0003-WHERE-CONDITION-04
		//                   PERFORM A340-UPDATE-WHERE-COND-SC04 THRU A340-SC04-EX
		//              WHEN IDSV0003-FIRST-ACTION
		//                                                       THRU A345-SC04-EX
		//              WHEN OTHER
		//                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
		//           END-EVALUATE.
		if (idsv0003.getLivelloOperazione().isIdsv0003PrimaryKey()) {
			// COB_CODE: PERFORM A330-UPDATE-PK-SC04         THRU A330-SC04-EX
			a330UpdatePkSc04();
		} else if (idsv0003.getTipologiaOperazione().isIdsv0003WhereCondition04()) {
			// COB_CODE: PERFORM A340-UPDATE-WHERE-COND-SC04 THRU A340-SC04-EX
			a340UpdateWhereCondSc04();
		} else if (idsv0003.getLivelloOperazione().isIdsv0003FirstAction()) {
			// COB_CODE: PERFORM A345-UPDATE-FIRST-ACTION-SC04
			//                                               THRU A345-SC04-EX
			a345UpdateFirstActionSc04();
		} else {
			// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
			idsv0003.getReturnCode().setInvalidLevelOper();
		}
	}

	/**Original name: A330-UPDATE-PK-SC04<br>
	 * <pre>*****************************************************************</pre>*/
	private void a330UpdatePkSc04() {
		// COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
		z150ValorizzaDataServices();
		// COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
		z200SetIndicatoriNull();
		// COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBM0250.cbl:line=5171, because the code is unreachable.
		// COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
		z960LengthVchar();
		//           ,DS_VER                 = :IABV0009-VERSIONING
		// COB_CODE:      EXEC SQL
		//                     UPDATE ADES
		//                     SET
		//                        DS_OPER_SQL            = :ADE-DS-OPER-SQL
		//           *           ,DS_VER                 = :IABV0009-VERSIONING
		//                       ,DS_UTENTE              = :ADE-DS-UTENTE
		//                       ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
		//                     WHERE             DS_RIGA = :ADE-DS-RIGA
		//                END-EXEC.
		adesDao.updateRec2(ades.getAdeDsOperSql(), ades.getAdeDsUtente(), iabv0002.getIabv0002StateCurrent(), ades.getAdeDsRiga());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A340-UPDATE-WHERE-COND-SC04<br>
	 * <pre>*****************************************************************</pre>*/
	private void a340UpdateWhereCondSc04() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A345-UPDATE-FIRST-ACTION-SC04<br>
	 * <pre>*****************************************************************</pre>*/
	private void a345UpdateFirstActionSc04() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A360-OPEN-CURSOR-SC04<br>
	 * <pre>*****************************************************************</pre>*/
	private void a360OpenCursorSc04() {
		// COB_CODE: PERFORM A305-DECLARE-CURSOR-SC04 THRU A305-SC04-EX.
		a305DeclareCursorSc04();
		// COB_CODE: EXEC SQL
		//                OPEN CUR-SC04
		//           END-EXEC.
		adesPoliStatOggBusDao.openCurSc04(this.getAdesPoliStatOggBusLdbm02502());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A370-CLOSE-CURSOR-SC04<br>
	 * <pre>*****************************************************************</pre>*/
	private void a370CloseCursorSc04() {
		// COB_CODE: EXEC SQL
		//                CLOSE CUR-SC04
		//           END-EXEC.
		adesPoliStatOggBusDao.closeCurSc04();
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A380-FETCH-FIRST-SC04<br>
	 * <pre>*****************************************************************</pre>*/
	private void a380FetchFirstSc04() {
		// COB_CODE: PERFORM A360-OPEN-CURSOR-SC04    THRU A360-SC04-EX.
		a360OpenCursorSc04();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM A390-FETCH-NEXT-SC04  THRU A390-SC04-EX
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM A390-FETCH-NEXT-SC04  THRU A390-SC04-EX
			a390FetchNextSc04();
		}
	}

	/**Original name: A390-FETCH-NEXT-SC04<br>
	 * <pre>*****************************************************************</pre>*/
	private void a390FetchNextSc04() {
		// COB_CODE: EXEC SQL
		//                FETCH CUR-SC04
		//           INTO
		//                :ADE-ID-ADES
		//               ,:ADE-ID-POLI
		//               ,:ADE-ID-MOVI-CRZ
		//               ,:ADE-ID-MOVI-CHIU
		//                :IND-ADE-ID-MOVI-CHIU
		//               ,:ADE-DT-INI-EFF-DB
		//               ,:ADE-DT-END-EFF-DB
		//               ,:ADE-IB-PREV
		//                :IND-ADE-IB-PREV
		//               ,:ADE-IB-OGG
		//                :IND-ADE-IB-OGG
		//               ,:ADE-COD-COMP-ANIA
		//               ,:ADE-DT-DECOR-DB
		//                :IND-ADE-DT-DECOR
		//               ,:ADE-DT-SCAD-DB
		//                :IND-ADE-DT-SCAD
		//               ,:ADE-ETA-A-SCAD
		//                :IND-ADE-ETA-A-SCAD
		//               ,:ADE-DUR-AA
		//                :IND-ADE-DUR-AA
		//               ,:ADE-DUR-MM
		//                :IND-ADE-DUR-MM
		//               ,:ADE-DUR-GG
		//                :IND-ADE-DUR-GG
		//               ,:ADE-TP-RGM-FISC
		//               ,:ADE-TP-RIAT
		//                :IND-ADE-TP-RIAT
		//               ,:ADE-TP-MOD-PAG-TIT
		//               ,:ADE-TP-IAS
		//                :IND-ADE-TP-IAS
		//               ,:ADE-DT-VARZ-TP-IAS-DB
		//                :IND-ADE-DT-VARZ-TP-IAS
		//               ,:ADE-PRE-NET-IND
		//                :IND-ADE-PRE-NET-IND
		//               ,:ADE-PRE-LRD-IND
		//                :IND-ADE-PRE-LRD-IND
		//               ,:ADE-RAT-LRD-IND
		//                :IND-ADE-RAT-LRD-IND
		//               ,:ADE-PRSTZ-INI-IND
		//                :IND-ADE-PRSTZ-INI-IND
		//               ,:ADE-FL-COINC-ASSTO
		//                :IND-ADE-FL-COINC-ASSTO
		//               ,:ADE-IB-DFLT
		//                :IND-ADE-IB-DFLT
		//               ,:ADE-MOD-CALC
		//                :IND-ADE-MOD-CALC
		//               ,:ADE-TP-FNT-CNBTVA
		//                :IND-ADE-TP-FNT-CNBTVA
		//               ,:ADE-IMP-AZ
		//                :IND-ADE-IMP-AZ
		//               ,:ADE-IMP-ADER
		//                :IND-ADE-IMP-ADER
		//               ,:ADE-IMP-TFR
		//                :IND-ADE-IMP-TFR
		//               ,:ADE-IMP-VOLO
		//                :IND-ADE-IMP-VOLO
		//               ,:ADE-PC-AZ
		//                :IND-ADE-PC-AZ
		//               ,:ADE-PC-ADER
		//                :IND-ADE-PC-ADER
		//               ,:ADE-PC-TFR
		//                :IND-ADE-PC-TFR
		//               ,:ADE-PC-VOLO
		//                :IND-ADE-PC-VOLO
		//               ,:ADE-DT-NOVA-RGM-FISC-DB
		//                :IND-ADE-DT-NOVA-RGM-FISC
		//               ,:ADE-FL-ATTIV
		//                :IND-ADE-FL-ATTIV
		//               ,:ADE-IMP-REC-RIT-VIS
		//                :IND-ADE-IMP-REC-RIT-VIS
		//               ,:ADE-IMP-REC-RIT-ACC
		//                :IND-ADE-IMP-REC-RIT-ACC
		//               ,:ADE-FL-VARZ-STAT-TBGC
		//                :IND-ADE-FL-VARZ-STAT-TBGC
		//               ,:ADE-FL-PROVZA-MIGRAZ
		//                :IND-ADE-FL-PROVZA-MIGRAZ
		//               ,:ADE-IMPB-VIS-DA-REC
		//                :IND-ADE-IMPB-VIS-DA-REC
		//               ,:ADE-DT-DECOR-PREST-BAN-DB
		//                :IND-ADE-DT-DECOR-PREST-BAN
		//               ,:ADE-DT-EFF-VARZ-STAT-T-DB
		//                :IND-ADE-DT-EFF-VARZ-STAT-T
		//               ,:ADE-DS-RIGA
		//               ,:ADE-DS-OPER-SQL
		//               ,:ADE-DS-VER
		//               ,:ADE-DS-TS-INI-CPTZ
		//               ,:ADE-DS-TS-END-CPTZ
		//               ,:ADE-DS-UTENTE
		//               ,:ADE-DS-STATO-ELAB
		//               ,:ADE-CUM-CNBT-CAP
		//                :IND-ADE-CUM-CNBT-CAP
		//               ,:ADE-IMP-GAR-CNBT
		//                :IND-ADE-IMP-GAR-CNBT
		//               ,:ADE-DT-ULT-CONS-CNBT-DB
		//                :IND-ADE-DT-ULT-CONS-CNBT
		//               ,:ADE-IDEN-ISC-FND
		//                :IND-ADE-IDEN-ISC-FND
		//               ,:ADE-NUM-RAT-PIAN
		//                :IND-ADE-NUM-RAT-PIAN
		//               ,:ADE-DT-PRESC-DB
		//                :IND-ADE-DT-PRESC
		//               ,:ADE-CONCS-PREST
		//                :IND-ADE-CONCS-PREST
		//               ,:POL-ID-POLI
		//               ,:POL-ID-MOVI-CRZ
		//               ,:POL-ID-MOVI-CHIU
		//                :IND-POL-ID-MOVI-CHIU
		//               ,:POL-IB-OGG
		//                :IND-POL-IB-OGG
		//               ,:POL-IB-PROP
		//               ,:POL-DT-PROP-DB
		//                :IND-POL-DT-PROP
		//               ,:POL-DT-INI-EFF-DB
		//               ,:POL-DT-END-EFF-DB
		//               ,:POL-COD-COMP-ANIA
		//               ,:POL-DT-DECOR-DB
		//               ,:POL-DT-EMIS-DB
		//               ,:POL-TP-POLI
		//               ,:POL-DUR-AA
		//                :IND-POL-DUR-AA
		//               ,:POL-DUR-MM
		//                :IND-POL-DUR-MM
		//               ,:POL-DT-SCAD-DB
		//                :IND-POL-DT-SCAD
		//               ,:POL-COD-PROD
		//               ,:POL-DT-INI-VLDT-PROD-DB
		//               ,:POL-COD-CONV
		//                :IND-POL-COD-CONV
		//               ,:POL-COD-RAMO
		//                :IND-POL-COD-RAMO
		//               ,:POL-DT-INI-VLDT-CONV-DB
		//                :IND-POL-DT-INI-VLDT-CONV
		//               ,:POL-DT-APPLZ-CONV-DB
		//                :IND-POL-DT-APPLZ-CONV
		//               ,:POL-TP-FRM-ASSVA
		//               ,:POL-TP-RGM-FISC
		//                :IND-POL-TP-RGM-FISC
		//               ,:POL-FL-ESTAS
		//                :IND-POL-FL-ESTAS
		//               ,:POL-FL-RSH-COMUN
		//                :IND-POL-FL-RSH-COMUN
		//               ,:POL-FL-RSH-COMUN-COND
		//                :IND-POL-FL-RSH-COMUN-COND
		//               ,:POL-TP-LIV-GENZ-TIT
		//               ,:POL-FL-COP-FINANZ
		//                :IND-POL-FL-COP-FINANZ
		//               ,:POL-TP-APPLZ-DIR
		//                :IND-POL-TP-APPLZ-DIR
		//               ,:POL-SPE-MED
		//                :IND-POL-SPE-MED
		//               ,:POL-DIR-EMIS
		//                :IND-POL-DIR-EMIS
		//               ,:POL-DIR-1O-VERS
		//                :IND-POL-DIR-1O-VERS
		//               ,:POL-DIR-VERS-AGG
		//                :IND-POL-DIR-VERS-AGG
		//               ,:POL-COD-DVS
		//                :IND-POL-COD-DVS
		//               ,:POL-FL-FNT-AZ
		//                :IND-POL-FL-FNT-AZ
		//               ,:POL-FL-FNT-ADER
		//                :IND-POL-FL-FNT-ADER
		//               ,:POL-FL-FNT-TFR
		//                :IND-POL-FL-FNT-TFR
		//               ,:POL-FL-FNT-VOLO
		//                :IND-POL-FL-FNT-VOLO
		//               ,:POL-TP-OPZ-A-SCAD
		//                :IND-POL-TP-OPZ-A-SCAD
		//               ,:POL-AA-DIFF-PROR-DFLT
		//                :IND-POL-AA-DIFF-PROR-DFLT
		//               ,:POL-FL-VER-PROD
		//                :IND-POL-FL-VER-PROD
		//               ,:POL-DUR-GG
		//                :IND-POL-DUR-GG
		//               ,:POL-DIR-QUIET
		//                :IND-POL-DIR-QUIET
		//               ,:POL-TP-PTF-ESTNO
		//                :IND-POL-TP-PTF-ESTNO
		//               ,:POL-FL-CUM-PRE-CNTR
		//                :IND-POL-FL-CUM-PRE-CNTR
		//               ,:POL-FL-AMMB-MOVI
		//                :IND-POL-FL-AMMB-MOVI
		//               ,:POL-CONV-GECO
		//                :IND-POL-CONV-GECO
		//               ,:POL-DS-RIGA
		//               ,:POL-DS-OPER-SQL
		//               ,:POL-DS-VER
		//               ,:POL-DS-TS-INI-CPTZ
		//               ,:POL-DS-TS-END-CPTZ
		//               ,:POL-DS-UTENTE
		//               ,:POL-DS-STATO-ELAB
		//               ,:POL-FL-SCUDO-FISC
		//                :IND-POL-FL-SCUDO-FISC
		//               ,:POL-FL-TRASFE
		//                :IND-POL-FL-TRASFE
		//               ,:POL-FL-TFR-STRC
		//                :IND-POL-FL-TFR-STRC
		//               ,:POL-DT-PRESC-DB
		//                :IND-POL-DT-PRESC
		//               ,:POL-COD-CONV-AGG
		//                :IND-POL-COD-CONV-AGG
		//               ,:POL-SUBCAT-PROD
		//                :IND-POL-SUBCAT-PROD
		//               ,:POL-FL-QUEST-ADEGZ-ASS
		//                :IND-POL-FL-QUEST-ADEGZ-ASS
		//               ,:POL-COD-TPA
		//                :IND-POL-COD-TPA
		//               ,:POL-ID-ACC-COMM
		//                :IND-POL-ID-ACC-COMM
		//               ,:POL-FL-POLI-CPI-PR
		//                :IND-POL-FL-POLI-CPI-PR
		//               ,:POL-FL-POLI-BUNDLING
		//                :IND-POL-FL-POLI-BUNDLING
		//               ,:POL-IND-POLI-PRIN-COLL
		//                :IND-POL-IND-POLI-PRIN-COLL
		//               ,:POL-FL-VND-BUNDLE
		//                :IND-POL-FL-VND-BUNDLE
		//               ,:POL-IB-BS
		//                :IND-POL-IB-BS
		//               ,:POL-FL-POLI-IFP
		//                :IND-POL-FL-POLI-IFP
		//               ,:STB-ID-STAT-OGG-BUS
		//               ,:STB-ID-OGG
		//               ,:STB-TP-OGG
		//               ,:STB-ID-MOVI-CRZ
		//               ,:STB-ID-MOVI-CHIU
		//                :IND-STB-ID-MOVI-CHIU
		//               ,:STB-DT-INI-EFF-DB
		//               ,:STB-DT-END-EFF-DB
		//               ,:STB-COD-COMP-ANIA
		//               ,:STB-TP-STAT-BUS
		//               ,:STB-TP-CAUS
		//               ,:STB-DS-RIGA
		//               ,:STB-DS-OPER-SQL
		//               ,:STB-DS-VER
		//               ,:STB-DS-TS-INI-CPTZ
		//               ,:STB-DS-TS-END-CPTZ
		//               ,:STB-DS-UTENTE
		//               ,:STB-DS-STATO-ELAB
		//           END-EXEC.
		adesPoliStatOggBusDao.fetchCurSc04(this.getAdesPoliStatOggBusLdbm0250());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           ELSE
		//             END-IF
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			z100SetColonneNull();
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		} else if (idsv0003.getSqlcode().isNotFound()) {
			// COB_CODE: IF IDSV0003-NOT-FOUND
			//              END-IF
			//           END-IF
			// COB_CODE: PERFORM A370-CLOSE-CURSOR-SC04 THRU A370-SC04-EX
			a370CloseCursorSc04();
			// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
			//              SET IDSV0003-NOT-FOUND TO TRUE
			//           END-IF
			if (idsv0003.getSqlcode().isSuccessfulSql()) {
				// COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
				idsv0003.getSqlcode().setNotFound();
			}
		}
	}

	/**Original name: SC05-SELECTION-CURSOR-05<br>
	 * <pre>*****************************************************************
	 * *****************************************************************</pre>*/
	private void sc05SelectionCursor05() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-SELECT
		//                 PERFORM A310-SELECT-SC05           THRU A310-SC05-EX
		//              WHEN IDSV0003-OPEN-CURSOR
		//                 PERFORM A360-OPEN-CURSOR-SC05      THRU A360-SC05-EX
		//              WHEN IDSV0003-CLOSE-CURSOR
		//                 PERFORM A370-CLOSE-CURSOR-SC05     THRU A370-SC05-EX
		//              WHEN IDSV0003-FETCH-FIRST
		//                 PERFORM A380-FETCH-FIRST-SC05      THRU A380-SC05-EX
		//              WHEN IDSV0003-FETCH-NEXT
		//                 PERFORM A390-FETCH-NEXT-SC05       THRU A390-SC05-EX
		//              WHEN IDSV0003-UPDATE
		//                 PERFORM A320-UPDATE-SC05           THRU A320-SC05-EX
		//              WHEN OTHER
		//                 SET IDSV0003-INVALID-OPER          TO TRUE
		//           END-EVALUATE.
		if (idsv0003.getOperazione().isSelect()) {
			// COB_CODE: PERFORM A310-SELECT-SC05           THRU A310-SC05-EX
			a310SelectSc05();
		} else if (idsv0003.getOperazione().isOpenCursor()) {
			// COB_CODE: PERFORM A360-OPEN-CURSOR-SC05      THRU A360-SC05-EX
			a360OpenCursorSc05();
		} else if (idsv0003.getOperazione().isCloseCursor()) {
			// COB_CODE: PERFORM A370-CLOSE-CURSOR-SC05     THRU A370-SC05-EX
			a370CloseCursorSc05();
		} else if (idsv0003.getOperazione().isFetchFirst()) {
			// COB_CODE: PERFORM A380-FETCH-FIRST-SC05      THRU A380-SC05-EX
			a380FetchFirstSc05();
		} else if (idsv0003.getOperazione().isFetchNext()) {
			// COB_CODE: PERFORM A390-FETCH-NEXT-SC05       THRU A390-SC05-EX
			a390FetchNextSc05();
		} else if (idsv0003.getOperazione().isUpdate()) {
			// COB_CODE: PERFORM A320-UPDATE-SC05           THRU A320-SC05-EX
			a320UpdateSc05();
		} else {
			// COB_CODE: SET IDSV0003-INVALID-OPER          TO TRUE
			idsv0003.getReturnCode().setInvalidOper();
		}
	}

	/**Original name: A305-DECLARE-CURSOR-SC05<br>
	 * <pre>*****************************************************************</pre>*/
	private void a305DeclareCursorSc05() {
		// COB_CODE: IF ACCESSO-X-RANGE-SI
		//              CONTINUE
		//           ELSE
		//              CONTINUE
		//           END-IF.
		if (ws.getFlagAccessoXRange().isSi()) {
			// COB_CODE:         EXEC SQL
			//                        DECLARE CUR-SC05-RANGE CURSOR WITH HOLD FOR
			//                     SELECT
			//                            A.ID_ADES
			//                           ,A.ID_POLI
			//                           ,A.ID_MOVI_CRZ
			//                           ,A.ID_MOVI_CHIU
			//                           ,A.DT_INI_EFF
			//                           ,A.DT_END_EFF
			//                           ,A.IB_PREV
			//                           ,A.IB_OGG
			//                           ,A.COD_COMP_ANIA
			//                           ,A.DT_DECOR
			//                           ,A.DT_SCAD
			//                           ,A.ETA_A_SCAD
			//                           ,A.DUR_AA
			//                           ,A.DUR_MM
			//                           ,A.DUR_GG
			//                           ,A.TP_RGM_FISC
			//                           ,A.TP_RIAT
			//                           ,A.TP_MOD_PAG_TIT
			//                           ,A.TP_IAS
			//                           ,A.DT_VARZ_TP_IAS
			//                           ,A.PRE_NET_IND
			//                           ,A.PRE_LRD_IND
			//                           ,A.RAT_LRD_IND
			//                           ,A.PRSTZ_INI_IND
			//                           ,A.FL_COINC_ASSTO
			//                           ,A.IB_DFLT
			//                           ,A.MOD_CALC
			//                           ,A.TP_FNT_CNBTVA
			//                           ,A.IMP_AZ
			//                           ,A.IMP_ADER
			//                           ,A.IMP_TFR
			//                           ,A.IMP_VOLO
			//                           ,A.PC_AZ
			//                           ,A.PC_ADER
			//                           ,A.PC_TFR
			//                           ,A.PC_VOLO
			//                           ,A.DT_NOVA_RGM_FISC
			//                           ,A.FL_ATTIV
			//                           ,A.IMP_REC_RIT_VIS
			//                           ,A.IMP_REC_RIT_ACC
			//                           ,A.FL_VARZ_STAT_TBGC
			//                           ,A.FL_PROVZA_MIGRAZ
			//                           ,A.IMPB_VIS_DA_REC
			//                           ,A.DT_DECOR_PREST_BAN
			//                           ,A.DT_EFF_VARZ_STAT_T
			//                           ,A.DS_RIGA
			//                           ,A.DS_OPER_SQL
			//                           ,A.DS_VER
			//                           ,A.DS_TS_INI_CPTZ
			//                           ,A.DS_TS_END_CPTZ
			//                           ,A.DS_UTENTE
			//                           ,A.DS_STATO_ELAB
			//                           ,A.CUM_CNBT_CAP
			//                           ,A.IMP_GAR_CNBT
			//                           ,A.DT_ULT_CONS_CNBT
			//                           ,A.IDEN_ISC_FND
			//                           ,A.NUM_RAT_PIAN
			//                           ,A.DT_PRESC
			//                           ,A.CONCS_PREST
			//                           ,B.ID_POLI
			//                           ,B.ID_MOVI_CRZ
			//                           ,B.ID_MOVI_CHIU
			//                           ,B.IB_OGG
			//                           ,B.IB_PROP
			//                           ,B.DT_PROP
			//                           ,B.DT_INI_EFF
			//                           ,B.DT_END_EFF
			//                           ,B.COD_COMP_ANIA
			//                           ,B.DT_DECOR
			//                           ,B.DT_EMIS
			//                           ,B.TP_POLI
			//                           ,B.DUR_AA
			//                           ,B.DUR_MM
			//                           ,B.DT_SCAD
			//                           ,B.COD_PROD
			//                           ,B.DT_INI_VLDT_PROD
			//                           ,B.COD_CONV
			//                           ,B.COD_RAMO
			//                           ,B.DT_INI_VLDT_CONV
			//                           ,B.DT_APPLZ_CONV
			//                           ,B.TP_FRM_ASSVA
			//                           ,B.TP_RGM_FISC
			//                           ,B.FL_ESTAS
			//                           ,B.FL_RSH_COMUN
			//                           ,B.FL_RSH_COMUN_COND
			//                           ,B.TP_LIV_GENZ_TIT
			//                           ,B.FL_COP_FINANZ
			//                           ,B.TP_APPLZ_DIR
			//                           ,B.SPE_MED
			//                           ,B.DIR_EMIS
			//                           ,B.DIR_1O_VERS
			//                           ,B.DIR_VERS_AGG
			//                           ,B.COD_DVS
			//                           ,B.FL_FNT_AZ
			//                           ,B.FL_FNT_ADER
			//                           ,B.FL_FNT_TFR
			//                           ,B.FL_FNT_VOLO
			//                           ,B.TP_OPZ_A_SCAD
			//                           ,B.AA_DIFF_PROR_DFLT
			//                           ,B.FL_VER_PROD
			//                           ,B.DUR_GG
			//                           ,B.DIR_QUIET
			//                           ,B.TP_PTF_ESTNO
			//                           ,B.FL_CUM_PRE_CNTR
			//                           ,B.FL_AMMB_MOVI
			//                           ,B.CONV_GECO
			//                           ,B.DS_RIGA
			//                           ,B.DS_OPER_SQL
			//                           ,B.DS_VER
			//                           ,B.DS_TS_INI_CPTZ
			//                           ,B.DS_TS_END_CPTZ
			//                           ,B.DS_UTENTE
			//                           ,B.DS_STATO_ELAB
			//                           ,B.FL_SCUDO_FISC
			//                           ,B.FL_TRASFE
			//                           ,B.FL_TFR_STRC
			//                           ,B.DT_PRESC
			//                           ,B.COD_CONV_AGG
			//                           ,B.SUBCAT_PROD
			//                           ,B.FL_QUEST_ADEGZ_ASS
			//                           ,B.COD_TPA
			//                           ,B.ID_ACC_COMM
			//                           ,B.FL_POLI_CPI_PR
			//                           ,B.FL_POLI_BUNDLING
			//                           ,B.IND_POLI_PRIN_COLL
			//                           ,B.FL_VND_BUNDLE
			//                           ,B.IB_BS
			//                           ,B.FL_POLI_IFP
			//                           ,C.ID_STAT_OGG_BUS
			//                           ,C.ID_OGG
			//                           ,C.TP_OGG
			//                           ,C.ID_MOVI_CRZ
			//                           ,C.ID_MOVI_CHIU
			//                           ,C.DT_INI_EFF
			//                           ,C.DT_END_EFF
			//                           ,C.COD_COMP_ANIA
			//                           ,C.TP_STAT_BUS
			//                           ,C.TP_CAUS
			//                           ,C.DS_RIGA
			//                           ,C.DS_OPER_SQL
			//                           ,C.DS_VER
			//                           ,C.DS_TS_INI_CPTZ
			//                           ,C.DS_TS_END_CPTZ
			//                           ,C.DS_UTENTE
			//                           ,C.DS_STATO_ELAB
			//                     FROM ADES      A,
			//                          POLI      B,
			//                          STAT_OGG_BUS C
			//                     WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
			//                                                :WS-TP-FRM-ASSVA2 )
			//                       AND A.ID_POLI      =   B.ID_POLI
			//                       AND A.ID_ADES      =   C.ID_OGG
			//                       AND A.ID_POLI     BETWEEN   :IABV0009-ID-OGG-DA AND
			//                                                   :IABV0009-ID-OGG-A
			//                       AND C.TP_OGG       =  'AD'
			//                       AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
			//                       AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
			//                       AND A.DT_END_EFF  >   :WS-DT-PTF-X
			//                       AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                       AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                       AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
			//                       AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
			//                       AND B.DT_END_EFF  >   :WS-DT-PTF-X
			//                       AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                       AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                       AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
			//                       AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
			//                       AND C.DT_END_EFF  >   :WS-DT-PTF-X
			//                       AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                       AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                       AND (A.ID_ADES IN
			//                          (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D,
			//                                                           STAT_OGG_BUS E
			//                           WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
			//                           AND E.TP_OGG = 'TG'
			//           *                AND E.TP_STAT_BUS IN ('VI','ST')
			//                           AND E.TP_STAT_BUS  = 'VI'
			//                           AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
			//                           AND D.COD_COMP_ANIA =
			//                              :IDSV0003-CODICE-COMPAGNIA-ANIA
			//                           AND D.DT_INI_EFF <=   :WS-DT-PTF-X
			//                           AND D.DT_END_EFF >    :WS-DT-PTF-X
			//                           AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                           AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                           AND E.DT_INI_EFF <=   :WS-DT-PTF-X
			//                           AND E.DT_END_EFF >    :WS-DT-PTF-X
			//                           AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                           AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                           AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
			//                                OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
			//                           ))
			//           *               OR A.ID_ADES IN (
			//           *                 SELECT DISTINCT(D.ID_ADES)
			//           *                 FROM MOVI_FINRIO D
			//           *                 WHERE A.ID_ADES = D.ID_ADES
			//           *                 AND D.TP_MOVI_FINRIO = 'DI'
			//           *                 AND D.COD_COMP_ANIA =
			//           *                 :IDSV0003-CODICE-COMPAGNIA-ANIA
			//           *                 AND D.DT_INI_EFF  <=  :WS-DT-PTF-X
			//           *                 AND D.DT_END_EFF  >   :WS-DT-PTF-X
			//           *                 AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF-PRE
			//           *                 AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF-PRE
			//           *                 AND C.TP_STAT_BUS =  'ST'
			//           *               ))
			//           *           AND A.DS_STATO_ELAB IN (
			//           *                                   :IABV0002-STATE-01,
			//           *                                   :IABV0002-STATE-02,
			//           *                                   :IABV0002-STATE-03,
			//           *                                   :IABV0002-STATE-04,
			//           *                                   :IABV0002-STATE-05,
			//           *                                   :IABV0002-STATE-06,
			//           *                                   :IABV0002-STATE-07,
			//           *                                   :IABV0002-STATE-08,
			//           *                                   :IABV0002-STATE-09,
			//           *                                   :IABV0002-STATE-10
			//           *                                     )
			//           *           AND NOT A.DS_VER  = :IABV0009-VERSIONING
			//                       ORDER BY A.ID_POLI, A.ID_ADES
			//                   END-EXEC
			// DECLARE CURSOR doesn't need a translation;
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE:         EXEC SQL
			//                        DECLARE CUR-SC05 CURSOR WITH HOLD FOR
			//                     SELECT
			//                            A.ID_ADES
			//                           ,A.ID_POLI
			//                           ,A.ID_MOVI_CRZ
			//                           ,A.ID_MOVI_CHIU
			//                           ,A.DT_INI_EFF
			//                           ,A.DT_END_EFF
			//                           ,A.IB_PREV
			//                           ,A.IB_OGG
			//                           ,A.COD_COMP_ANIA
			//                           ,A.DT_DECOR
			//                           ,A.DT_SCAD
			//                           ,A.ETA_A_SCAD
			//                           ,A.DUR_AA
			//                           ,A.DUR_MM
			//                           ,A.DUR_GG
			//                           ,A.TP_RGM_FISC
			//                           ,A.TP_RIAT
			//                           ,A.TP_MOD_PAG_TIT
			//                           ,A.TP_IAS
			//                           ,A.DT_VARZ_TP_IAS
			//                           ,A.PRE_NET_IND
			//                           ,A.PRE_LRD_IND
			//                           ,A.RAT_LRD_IND
			//                           ,A.PRSTZ_INI_IND
			//                           ,A.FL_COINC_ASSTO
			//                           ,A.IB_DFLT
			//                           ,A.MOD_CALC
			//                           ,A.TP_FNT_CNBTVA
			//                           ,A.IMP_AZ
			//                           ,A.IMP_ADER
			//                           ,A.IMP_TFR
			//                           ,A.IMP_VOLO
			//                           ,A.PC_AZ
			//                           ,A.PC_ADER
			//                           ,A.PC_TFR
			//                           ,A.PC_VOLO
			//                           ,A.DT_NOVA_RGM_FISC
			//                           ,A.FL_ATTIV
			//                           ,A.IMP_REC_RIT_VIS
			//                           ,A.IMP_REC_RIT_ACC
			//                           ,A.FL_VARZ_STAT_TBGC
			//                           ,A.FL_PROVZA_MIGRAZ
			//                           ,A.IMPB_VIS_DA_REC
			//                           ,A.DT_DECOR_PREST_BAN
			//                           ,A.DT_EFF_VARZ_STAT_T
			//                           ,A.DS_RIGA
			//                           ,A.DS_OPER_SQL
			//                           ,A.DS_VER
			//                           ,A.DS_TS_INI_CPTZ
			//                           ,A.DS_TS_END_CPTZ
			//                           ,A.DS_UTENTE
			//                           ,A.DS_STATO_ELAB
			//                           ,A.CUM_CNBT_CAP
			//                           ,A.IMP_GAR_CNBT
			//                           ,A.DT_ULT_CONS_CNBT
			//                           ,A.IDEN_ISC_FND
			//                           ,A.NUM_RAT_PIAN
			//                           ,A.DT_PRESC
			//                           ,A.CONCS_PREST
			//                           ,B.ID_POLI
			//                           ,B.ID_MOVI_CRZ
			//                           ,B.ID_MOVI_CHIU
			//                           ,B.IB_OGG
			//                           ,B.IB_PROP
			//                           ,B.DT_PROP
			//                           ,B.DT_INI_EFF
			//                           ,B.DT_END_EFF
			//                           ,B.COD_COMP_ANIA
			//                           ,B.DT_DECOR
			//                           ,B.DT_EMIS
			//                           ,B.TP_POLI
			//                           ,B.DUR_AA
			//                           ,B.DUR_MM
			//                           ,B.DT_SCAD
			//                           ,B.COD_PROD
			//                           ,B.DT_INI_VLDT_PROD
			//                           ,B.COD_CONV
			//                           ,B.COD_RAMO
			//                           ,B.DT_INI_VLDT_CONV
			//                           ,B.DT_APPLZ_CONV
			//                           ,B.TP_FRM_ASSVA
			//                           ,B.TP_RGM_FISC
			//                           ,B.FL_ESTAS
			//                           ,B.FL_RSH_COMUN
			//                           ,B.FL_RSH_COMUN_COND
			//                           ,B.TP_LIV_GENZ_TIT
			//                           ,B.FL_COP_FINANZ
			//                           ,B.TP_APPLZ_DIR
			//                           ,B.SPE_MED
			//                           ,B.DIR_EMIS
			//                           ,B.DIR_1O_VERS
			//                           ,B.DIR_VERS_AGG
			//                           ,B.COD_DVS
			//                           ,B.FL_FNT_AZ
			//                           ,B.FL_FNT_ADER
			//                           ,B.FL_FNT_TFR
			//                           ,B.FL_FNT_VOLO
			//                           ,B.TP_OPZ_A_SCAD
			//                           ,B.AA_DIFF_PROR_DFLT
			//                           ,B.FL_VER_PROD
			//                           ,B.DUR_GG
			//                           ,B.DIR_QUIET
			//                           ,B.TP_PTF_ESTNO
			//                           ,B.FL_CUM_PRE_CNTR
			//                           ,B.FL_AMMB_MOVI
			//                           ,B.CONV_GECO
			//                           ,B.DS_RIGA
			//                           ,B.DS_OPER_SQL
			//                           ,B.DS_VER
			//                           ,B.DS_TS_INI_CPTZ
			//                           ,B.DS_TS_END_CPTZ
			//                           ,B.DS_UTENTE
			//                           ,B.DS_STATO_ELAB
			//                           ,B.FL_SCUDO_FISC
			//                           ,B.FL_TRASFE
			//                           ,B.FL_TFR_STRC
			//                           ,B.DT_PRESC
			//                           ,B.COD_CONV_AGG
			//                           ,B.SUBCAT_PROD
			//                           ,B.FL_QUEST_ADEGZ_ASS
			//                           ,B.COD_TPA
			//                           ,B.ID_ACC_COMM
			//                           ,B.FL_POLI_CPI_PR
			//                           ,B.FL_POLI_BUNDLING
			//                           ,B.IND_POLI_PRIN_COLL
			//                           ,B.FL_VND_BUNDLE
			//                           ,B.IB_BS
			//                           ,B.FL_POLI_IFP
			//                           ,C.ID_STAT_OGG_BUS
			//                           ,C.ID_OGG
			//                           ,C.TP_OGG
			//                           ,C.ID_MOVI_CRZ
			//                           ,C.ID_MOVI_CHIU
			//                           ,C.DT_INI_EFF
			//                           ,C.DT_END_EFF
			//                           ,C.COD_COMP_ANIA
			//                           ,C.TP_STAT_BUS
			//                           ,C.TP_CAUS
			//                           ,C.DS_RIGA
			//                           ,C.DS_OPER_SQL
			//                           ,C.DS_VER
			//                           ,C.DS_TS_INI_CPTZ
			//                           ,C.DS_TS_END_CPTZ
			//                           ,C.DS_UTENTE
			//                           ,C.DS_STATO_ELAB
			//                     FROM ADES      A,
			//                          POLI      B,
			//                          STAT_OGG_BUS C
			//                     WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
			//                                                :WS-TP-FRM-ASSVA2 )
			//                       AND A.ID_POLI      =   B.ID_POLI
			//                       AND A.ID_ADES      =   C.ID_OGG
			//                       AND C.TP_OGG       =  'AD'
			//                       AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
			//                       AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
			//                       AND A.DT_END_EFF  >   :WS-DT-PTF-X
			//                       AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                       AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                       AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
			//                       AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
			//                       AND B.DT_END_EFF  >   :WS-DT-PTF-X
			//                       AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                       AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                       AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
			//                       AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
			//                       AND C.DT_END_EFF  >   :WS-DT-PTF-X
			//                       AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                       AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                       AND (A.ID_ADES IN
			//                          (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D,
			//                                                           STAT_OGG_BUS E
			//                           WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
			//                           AND E.TP_OGG = 'TG'
			//           *                AND E.TP_STAT_BUS IN ('VI','ST')
			//                           AND E.TP_STAT_BUS = 'VI'
			//                           AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
			//                           AND D.COD_COMP_ANIA =
			//                               :IDSV0003-CODICE-COMPAGNIA-ANIA
			//                           AND D.DT_INI_EFF <=   :WS-DT-PTF-X
			//                           AND D.DT_END_EFF >    :WS-DT-PTF-X
			//                           AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                           AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                           AND E.DT_INI_EFF <=   :WS-DT-PTF-X
			//                           AND E.DT_END_EFF >    :WS-DT-PTF-X
			//                           AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                           AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                           AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
			//                                OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
			//                           ))
			//           *               OR A.ID_ADES IN (
			//           *                 SELECT DISTINCT(D.ID_ADES)
			//           *                 FROM MOVI_FINRIO D
			//           *                 WHERE A.ID_ADES = D.ID_ADES
			//           *                 AND D.TP_MOVI_FINRIO = 'DI'
			//           *                 AND D.COD_COMP_ANIA =
			//           *                 :IDSV0003-CODICE-COMPAGNIA-ANIA
			//           *                 AND D.DT_INI_EFF  <=  :WS-DT-PTF-X
			//           *                 AND D.DT_END_EFF  >   :WS-DT-PTF-X
			//           *                 AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF-PRE
			//           *                 AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF-PRE
			//           *                 AND C.TP_STAT_BUS =  'ST'
			//           *               ))
			//           *           AND A.DS_STATO_ELAB IN (
			//           *                                   :IABV0002-STATE-01,
			//           *                                   :IABV0002-STATE-02,
			//           *                                   :IABV0002-STATE-03,
			//           *                                   :IABV0002-STATE-04,
			//           *                                   :IABV0002-STATE-05,
			//           *                                   :IABV0002-STATE-06,
			//           *                                   :IABV0002-STATE-07,
			//           *                                   :IABV0002-STATE-08,
			//           *                                   :IABV0002-STATE-09,
			//           *                                   :IABV0002-STATE-10
			//           *                                     )
			//           *           AND NOT A.DS_VER  = :IABV0009-VERSIONING
			//                       ORDER BY A.ID_POLI, A.ID_ADES
			//                   END-EXEC
			// DECLARE CURSOR doesn't need a translation;
			// COB_CODE: CONTINUE
			//continue
		}
	}

	/**Original name: A310-SELECT-SC05<br>
	 * <pre>*****************************************************************</pre>*/
	private void a310SelectSc05() {
		// COB_CODE: IF ACCESSO-X-RANGE-SI
		//              END-EXEC
		//           ELSE
		//              END-EXEC
		//           END-IF.
		if (ws.getFlagAccessoXRange().isSi()) {
			//           AND A.DS_STATO_ELAB IN (
			//                                   :IABV0002-STATE-01,
			//                                   :IABV0002-STATE-02,
			//                                   :IABV0002-STATE-03,
			//                                   :IABV0002-STATE-04,
			//                                   :IABV0002-STATE-05,
			//                                   :IABV0002-STATE-06,
			//                                   :IABV0002-STATE-07,
			//                                   :IABV0002-STATE-08,
			//                                   :IABV0002-STATE-09,
			//                                   :IABV0002-STATE-10
			//                                     )
			//           AND NOT A.DS_VER  = :IABV0009-VERSIONING
			// COB_CODE:         EXEC SQL
			//                     SELECT
			//                            A.ID_ADES
			//                           ,A.ID_POLI
			//                           ,A.ID_MOVI_CRZ
			//                           ,A.ID_MOVI_CHIU
			//                           ,A.DT_INI_EFF
			//                           ,A.DT_END_EFF
			//                           ,A.IB_PREV
			//                           ,A.IB_OGG
			//                           ,A.COD_COMP_ANIA
			//                           ,A.DT_DECOR
			//                           ,A.DT_SCAD
			//                           ,A.ETA_A_SCAD
			//                           ,A.DUR_AA
			//                           ,A.DUR_MM
			//                           ,A.DUR_GG
			//                           ,A.TP_RGM_FISC
			//                           ,A.TP_RIAT
			//                           ,A.TP_MOD_PAG_TIT
			//                           ,A.TP_IAS
			//                           ,A.DT_VARZ_TP_IAS
			//                           ,A.PRE_NET_IND
			//                           ,A.PRE_LRD_IND
			//                           ,A.RAT_LRD_IND
			//                           ,A.PRSTZ_INI_IND
			//                           ,A.FL_COINC_ASSTO
			//                           ,A.IB_DFLT
			//                           ,A.MOD_CALC
			//                           ,A.TP_FNT_CNBTVA
			//                           ,A.IMP_AZ
			//                           ,A.IMP_ADER
			//                           ,A.IMP_TFR
			//                           ,A.IMP_VOLO
			//                           ,A.PC_AZ
			//                           ,A.PC_ADER
			//                           ,A.PC_TFR
			//                           ,A.PC_VOLO
			//                           ,A.DT_NOVA_RGM_FISC
			//                           ,A.FL_ATTIV
			//                           ,A.IMP_REC_RIT_VIS
			//                           ,A.IMP_REC_RIT_ACC
			//                           ,A.FL_VARZ_STAT_TBGC
			//                           ,A.FL_PROVZA_MIGRAZ
			//                           ,A.IMPB_VIS_DA_REC
			//                           ,A.DT_DECOR_PREST_BAN
			//                           ,A.DT_EFF_VARZ_STAT_T
			//                           ,A.DS_RIGA
			//                           ,A.DS_OPER_SQL
			//                           ,A.DS_VER
			//                           ,A.DS_TS_INI_CPTZ
			//                           ,A.DS_TS_END_CPTZ
			//                           ,A.DS_UTENTE
			//                           ,A.DS_STATO_ELAB
			//                           ,A.CUM_CNBT_CAP
			//                           ,A.IMP_GAR_CNBT
			//                           ,A.DT_ULT_CONS_CNBT
			//                           ,A.IDEN_ISC_FND
			//                           ,A.NUM_RAT_PIAN
			//                           ,A.DT_PRESC
			//                           ,A.CONCS_PREST
			//                           ,B.ID_POLI
			//                           ,B.ID_MOVI_CRZ
			//                           ,B.ID_MOVI_CHIU
			//                           ,B.IB_OGG
			//                           ,B.IB_PROP
			//                           ,B.DT_PROP
			//                           ,B.DT_INI_EFF
			//                           ,B.DT_END_EFF
			//                           ,B.COD_COMP_ANIA
			//                           ,B.DT_DECOR
			//                           ,B.DT_EMIS
			//                           ,B.TP_POLI
			//                           ,B.DUR_AA
			//                           ,B.DUR_MM
			//                           ,B.DT_SCAD
			//                           ,B.COD_PROD
			//                           ,B.DT_INI_VLDT_PROD
			//                           ,B.COD_CONV
			//                           ,B.COD_RAMO
			//                           ,B.DT_INI_VLDT_CONV
			//                           ,B.DT_APPLZ_CONV
			//                           ,B.TP_FRM_ASSVA
			//                           ,B.TP_RGM_FISC
			//                           ,B.FL_ESTAS
			//                           ,B.FL_RSH_COMUN
			//                           ,B.FL_RSH_COMUN_COND
			//                           ,B.TP_LIV_GENZ_TIT
			//                           ,B.FL_COP_FINANZ
			//                           ,B.TP_APPLZ_DIR
			//                           ,B.SPE_MED
			//                           ,B.DIR_EMIS
			//                           ,B.DIR_1O_VERS
			//                           ,B.DIR_VERS_AGG
			//                           ,B.COD_DVS
			//                           ,B.FL_FNT_AZ
			//                           ,B.FL_FNT_ADER
			//                           ,B.FL_FNT_TFR
			//                           ,B.FL_FNT_VOLO
			//                           ,B.TP_OPZ_A_SCAD
			//                           ,B.AA_DIFF_PROR_DFLT
			//                           ,B.FL_VER_PROD
			//                           ,B.DUR_GG
			//                           ,B.DIR_QUIET
			//                           ,B.TP_PTF_ESTNO
			//                           ,B.FL_CUM_PRE_CNTR
			//                           ,B.FL_AMMB_MOVI
			//                           ,B.CONV_GECO
			//                           ,B.DS_RIGA
			//                           ,B.DS_OPER_SQL
			//                           ,B.DS_VER
			//                           ,B.DS_TS_INI_CPTZ
			//                           ,B.DS_TS_END_CPTZ
			//                           ,B.DS_UTENTE
			//                           ,B.DS_STATO_ELAB
			//                           ,B.FL_SCUDO_FISC
			//                           ,B.FL_TRASFE
			//                           ,B.FL_TFR_STRC
			//                           ,B.DT_PRESC
			//                           ,B.COD_CONV_AGG
			//                           ,B.SUBCAT_PROD
			//                           ,B.FL_QUEST_ADEGZ_ASS
			//                           ,B.COD_TPA
			//                           ,B.ID_ACC_COMM
			//                           ,B.FL_POLI_CPI_PR
			//                           ,B.FL_POLI_BUNDLING
			//                           ,B.IND_POLI_PRIN_COLL
			//                           ,B.FL_VND_BUNDLE
			//                           ,B.IB_BS
			//                           ,B.FL_POLI_IFP
			//                           ,C.ID_STAT_OGG_BUS
			//                           ,C.ID_OGG
			//                           ,C.TP_OGG
			//                           ,C.ID_MOVI_CRZ
			//                           ,C.ID_MOVI_CHIU
			//                           ,C.DT_INI_EFF
			//                           ,C.DT_END_EFF
			//                           ,C.COD_COMP_ANIA
			//                           ,C.TP_STAT_BUS
			//                           ,C.TP_CAUS
			//                           ,C.DS_RIGA
			//                           ,C.DS_OPER_SQL
			//                           ,C.DS_VER
			//                           ,C.DS_TS_INI_CPTZ
			//                           ,C.DS_TS_END_CPTZ
			//                           ,C.DS_UTENTE
			//                           ,C.DS_STATO_ELAB
			//                     INTO
			//                           :ADE-ID-ADES
			//                          ,:ADE-ID-POLI
			//                          ,:ADE-ID-MOVI-CRZ
			//                          ,:ADE-ID-MOVI-CHIU
			//                           :IND-ADE-ID-MOVI-CHIU
			//                          ,:ADE-DT-INI-EFF-DB
			//                          ,:ADE-DT-END-EFF-DB
			//                          ,:ADE-IB-PREV
			//                           :IND-ADE-IB-PREV
			//                          ,:ADE-IB-OGG
			//                           :IND-ADE-IB-OGG
			//                          ,:ADE-COD-COMP-ANIA
			//                          ,:ADE-DT-DECOR-DB
			//                           :IND-ADE-DT-DECOR
			//                          ,:ADE-DT-SCAD-DB
			//                           :IND-ADE-DT-SCAD
			//                          ,:ADE-ETA-A-SCAD
			//                           :IND-ADE-ETA-A-SCAD
			//                          ,:ADE-DUR-AA
			//                           :IND-ADE-DUR-AA
			//                          ,:ADE-DUR-MM
			//                           :IND-ADE-DUR-MM
			//                          ,:ADE-DUR-GG
			//                           :IND-ADE-DUR-GG
			//                          ,:ADE-TP-RGM-FISC
			//                          ,:ADE-TP-RIAT
			//                           :IND-ADE-TP-RIAT
			//                          ,:ADE-TP-MOD-PAG-TIT
			//                          ,:ADE-TP-IAS
			//                           :IND-ADE-TP-IAS
			//                          ,:ADE-DT-VARZ-TP-IAS-DB
			//                           :IND-ADE-DT-VARZ-TP-IAS
			//                          ,:ADE-PRE-NET-IND
			//                           :IND-ADE-PRE-NET-IND
			//                          ,:ADE-PRE-LRD-IND
			//                           :IND-ADE-PRE-LRD-IND
			//                          ,:ADE-RAT-LRD-IND
			//                           :IND-ADE-RAT-LRD-IND
			//                          ,:ADE-PRSTZ-INI-IND
			//                           :IND-ADE-PRSTZ-INI-IND
			//                          ,:ADE-FL-COINC-ASSTO
			//                           :IND-ADE-FL-COINC-ASSTO
			//                          ,:ADE-IB-DFLT
			//                           :IND-ADE-IB-DFLT
			//                          ,:ADE-MOD-CALC
			//                           :IND-ADE-MOD-CALC
			//                          ,:ADE-TP-FNT-CNBTVA
			//                           :IND-ADE-TP-FNT-CNBTVA
			//                          ,:ADE-IMP-AZ
			//                           :IND-ADE-IMP-AZ
			//                          ,:ADE-IMP-ADER
			//                           :IND-ADE-IMP-ADER
			//                          ,:ADE-IMP-TFR
			//                           :IND-ADE-IMP-TFR
			//                          ,:ADE-IMP-VOLO
			//                           :IND-ADE-IMP-VOLO
			//                          ,:ADE-PC-AZ
			//                           :IND-ADE-PC-AZ
			//                          ,:ADE-PC-ADER
			//                           :IND-ADE-PC-ADER
			//                          ,:ADE-PC-TFR
			//                           :IND-ADE-PC-TFR
			//                          ,:ADE-PC-VOLO
			//                           :IND-ADE-PC-VOLO
			//                          ,:ADE-DT-NOVA-RGM-FISC-DB
			//                           :IND-ADE-DT-NOVA-RGM-FISC
			//                          ,:ADE-FL-ATTIV
			//                           :IND-ADE-FL-ATTIV
			//                          ,:ADE-IMP-REC-RIT-VIS
			//                           :IND-ADE-IMP-REC-RIT-VIS
			//                          ,:ADE-IMP-REC-RIT-ACC
			//                           :IND-ADE-IMP-REC-RIT-ACC
			//                          ,:ADE-FL-VARZ-STAT-TBGC
			//                           :IND-ADE-FL-VARZ-STAT-TBGC
			//                          ,:ADE-FL-PROVZA-MIGRAZ
			//                           :IND-ADE-FL-PROVZA-MIGRAZ
			//                          ,:ADE-IMPB-VIS-DA-REC
			//                           :IND-ADE-IMPB-VIS-DA-REC
			//                          ,:ADE-DT-DECOR-PREST-BAN-DB
			//                           :IND-ADE-DT-DECOR-PREST-BAN
			//                          ,:ADE-DT-EFF-VARZ-STAT-T-DB
			//                           :IND-ADE-DT-EFF-VARZ-STAT-T
			//                          ,:ADE-DS-RIGA
			//                          ,:ADE-DS-OPER-SQL
			//                          ,:ADE-DS-VER
			//                          ,:ADE-DS-TS-INI-CPTZ
			//                          ,:ADE-DS-TS-END-CPTZ
			//                          ,:ADE-DS-UTENTE
			//                          ,:ADE-DS-STATO-ELAB
			//                          ,:ADE-CUM-CNBT-CAP
			//                           :IND-ADE-CUM-CNBT-CAP
			//                          ,:ADE-IMP-GAR-CNBT
			//                           :IND-ADE-IMP-GAR-CNBT
			//                          ,:ADE-DT-ULT-CONS-CNBT-DB
			//                           :IND-ADE-DT-ULT-CONS-CNBT
			//                          ,:ADE-IDEN-ISC-FND
			//                           :IND-ADE-IDEN-ISC-FND
			//                          ,:ADE-NUM-RAT-PIAN
			//                           :IND-ADE-NUM-RAT-PIAN
			//                          ,:ADE-DT-PRESC-DB
			//                           :IND-ADE-DT-PRESC
			//                          ,:ADE-CONCS-PREST
			//                           :IND-ADE-CONCS-PREST
			//                          ,:POL-ID-POLI
			//                          ,:POL-ID-MOVI-CRZ
			//                          ,:POL-ID-MOVI-CHIU
			//                           :IND-POL-ID-MOVI-CHIU
			//                          ,:POL-IB-OGG
			//                           :IND-POL-IB-OGG
			//                          ,:POL-IB-PROP
			//                          ,:POL-DT-PROP-DB
			//                           :IND-POL-DT-PROP
			//                          ,:POL-DT-INI-EFF-DB
			//                          ,:POL-DT-END-EFF-DB
			//                          ,:POL-COD-COMP-ANIA
			//                          ,:POL-DT-DECOR-DB
			//                          ,:POL-DT-EMIS-DB
			//                          ,:POL-TP-POLI
			//                          ,:POL-DUR-AA
			//                           :IND-POL-DUR-AA
			//                          ,:POL-DUR-MM
			//                           :IND-POL-DUR-MM
			//                          ,:POL-DT-SCAD-DB
			//                           :IND-POL-DT-SCAD
			//                          ,:POL-COD-PROD
			//                          ,:POL-DT-INI-VLDT-PROD-DB
			//                          ,:POL-COD-CONV
			//                           :IND-POL-COD-CONV
			//                          ,:POL-COD-RAMO
			//                           :IND-POL-COD-RAMO
			//                          ,:POL-DT-INI-VLDT-CONV-DB
			//                           :IND-POL-DT-INI-VLDT-CONV
			//                          ,:POL-DT-APPLZ-CONV-DB
			//                           :IND-POL-DT-APPLZ-CONV
			//                          ,:POL-TP-FRM-ASSVA
			//                          ,:POL-TP-RGM-FISC
			//                           :IND-POL-TP-RGM-FISC
			//                          ,:POL-FL-ESTAS
			//                           :IND-POL-FL-ESTAS
			//                          ,:POL-FL-RSH-COMUN
			//                           :IND-POL-FL-RSH-COMUN
			//                          ,:POL-FL-RSH-COMUN-COND
			//                           :IND-POL-FL-RSH-COMUN-COND
			//                          ,:POL-TP-LIV-GENZ-TIT
			//                          ,:POL-FL-COP-FINANZ
			//                           :IND-POL-FL-COP-FINANZ
			//                          ,:POL-TP-APPLZ-DIR
			//                           :IND-POL-TP-APPLZ-DIR
			//                          ,:POL-SPE-MED
			//                           :IND-POL-SPE-MED
			//                          ,:POL-DIR-EMIS
			//                           :IND-POL-DIR-EMIS
			//                          ,:POL-DIR-1O-VERS
			//                           :IND-POL-DIR-1O-VERS
			//                          ,:POL-DIR-VERS-AGG
			//                           :IND-POL-DIR-VERS-AGG
			//                          ,:POL-COD-DVS
			//                           :IND-POL-COD-DVS
			//                          ,:POL-FL-FNT-AZ
			//                           :IND-POL-FL-FNT-AZ
			//                          ,:POL-FL-FNT-ADER
			//                           :IND-POL-FL-FNT-ADER
			//                          ,:POL-FL-FNT-TFR
			//                           :IND-POL-FL-FNT-TFR
			//                          ,:POL-FL-FNT-VOLO
			//                           :IND-POL-FL-FNT-VOLO
			//                          ,:POL-TP-OPZ-A-SCAD
			//                           :IND-POL-TP-OPZ-A-SCAD
			//                          ,:POL-AA-DIFF-PROR-DFLT
			//                           :IND-POL-AA-DIFF-PROR-DFLT
			//                          ,:POL-FL-VER-PROD
			//                           :IND-POL-FL-VER-PROD
			//                          ,:POL-DUR-GG
			//                           :IND-POL-DUR-GG
			//                          ,:POL-DIR-QUIET
			//                           :IND-POL-DIR-QUIET
			//                          ,:POL-TP-PTF-ESTNO
			//                           :IND-POL-TP-PTF-ESTNO
			//                          ,:POL-FL-CUM-PRE-CNTR
			//                           :IND-POL-FL-CUM-PRE-CNTR
			//                          ,:POL-FL-AMMB-MOVI
			//                           :IND-POL-FL-AMMB-MOVI
			//                          ,:POL-CONV-GECO
			//                           :IND-POL-CONV-GECO
			//                          ,:POL-DS-RIGA
			//                          ,:POL-DS-OPER-SQL
			//                          ,:POL-DS-VER
			//                          ,:POL-DS-TS-INI-CPTZ
			//                          ,:POL-DS-TS-END-CPTZ
			//                          ,:POL-DS-UTENTE
			//                          ,:POL-DS-STATO-ELAB
			//                          ,:POL-FL-SCUDO-FISC
			//                           :IND-POL-FL-SCUDO-FISC
			//                          ,:POL-FL-TRASFE
			//                           :IND-POL-FL-TRASFE
			//                          ,:POL-FL-TFR-STRC
			//                           :IND-POL-FL-TFR-STRC
			//                          ,:POL-DT-PRESC-DB
			//                           :IND-POL-DT-PRESC
			//                          ,:POL-COD-CONV-AGG
			//                           :IND-POL-COD-CONV-AGG
			//                          ,:POL-SUBCAT-PROD
			//                           :IND-POL-SUBCAT-PROD
			//                          ,:POL-FL-QUEST-ADEGZ-ASS
			//                           :IND-POL-FL-QUEST-ADEGZ-ASS
			//                          ,:POL-COD-TPA
			//                           :IND-POL-COD-TPA
			//                          ,:POL-ID-ACC-COMM
			//                           :IND-POL-ID-ACC-COMM
			//                          ,:POL-FL-POLI-CPI-PR
			//                           :IND-POL-FL-POLI-CPI-PR
			//                          ,:POL-FL-POLI-BUNDLING
			//                           :IND-POL-FL-POLI-BUNDLING
			//                          ,:POL-IND-POLI-PRIN-COLL
			//                           :IND-POL-IND-POLI-PRIN-COLL
			//                          ,:POL-FL-VND-BUNDLE
			//                           :IND-POL-FL-VND-BUNDLE
			//                          ,:POL-IB-BS
			//                           :IND-POL-IB-BS
			//                          ,:POL-FL-POLI-IFP
			//                           :IND-POL-FL-POLI-IFP
			//                          ,:STB-ID-STAT-OGG-BUS
			//                          ,:STB-ID-OGG
			//                          ,:STB-TP-OGG
			//                          ,:STB-ID-MOVI-CRZ
			//                          ,:STB-ID-MOVI-CHIU
			//                           :IND-STB-ID-MOVI-CHIU
			//                          ,:STB-DT-INI-EFF-DB
			//                          ,:STB-DT-END-EFF-DB
			//                          ,:STB-COD-COMP-ANIA
			//                          ,:STB-TP-STAT-BUS
			//                          ,:STB-TP-CAUS
			//                          ,:STB-DS-RIGA
			//                          ,:STB-DS-OPER-SQL
			//                          ,:STB-DS-VER
			//                          ,:STB-DS-TS-INI-CPTZ
			//                          ,:STB-DS-TS-END-CPTZ
			//                          ,:STB-DS-UTENTE
			//                          ,:STB-DS-STATO-ELAB
			//                     FROM ADES      A,
			//                          POLI      B,
			//                          STAT_OGG_BUS C
			//                     WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
			//                                                :WS-TP-FRM-ASSVA2 )
			//                       AND A.ID_POLI      =   B.ID_POLI
			//                       AND A.ID_ADES      =   C.ID_OGG
			//                       AND A.ID_POLI        BETWEEN   :IABV0009-ID-OGG-DA AND
			//                                                      :IABV0009-ID-OGG-A
			//                       AND C.TP_OGG       =  'AD'
			//           *--      AND C.TP_STAT_BUS     =  'VI'
			//                       AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
			//                       AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
			//                       AND A.DT_END_EFF  >   :WS-DT-PTF-X
			//                       AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                       AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                       AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
			//                       AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
			//                       AND B.DT_END_EFF  >   :WS-DT-PTF-X
			//                       AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                       AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                       AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
			//                       AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
			//                       AND C.DT_END_EFF  >   :WS-DT-PTF-X
			//                       AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                       AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                       AND A.ID_ADES IN
			//                          (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
			//                                                           STAT_OGG_BUS E
			//                           WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
			//                           AND E.TP_OGG = 'TG'
			//                           AND E.TP_STAT_BUS IN ('VI','ST')
			//                           AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
			//                           AND D.COD_COMP_ANIA =
			//                               :IDSV0003-CODICE-COMPAGNIA-ANIA
			//                           AND D.DT_INI_EFF <=   :WS-DT-PTF-X
			//                           AND D.DT_END_EFF >    :WS-DT-PTF-X
			//                           AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                           AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                           AND E.DT_INI_EFF <=   :WS-DT-PTF-X
			//                           AND E.DT_END_EFF >    :WS-DT-PTF-X
			//                           AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                           AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                           AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
			//                                OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
			//                           )
			//           *           AND A.DS_STATO_ELAB IN (
			//           *                                   :IABV0002-STATE-01,
			//           *                                   :IABV0002-STATE-02,
			//           *                                   :IABV0002-STATE-03,
			//           *                                   :IABV0002-STATE-04,
			//           *                                   :IABV0002-STATE-05,
			//           *                                   :IABV0002-STATE-06,
			//           *                                   :IABV0002-STATE-07,
			//           *                                   :IABV0002-STATE-08,
			//           *                                   :IABV0002-STATE-09,
			//           *                                   :IABV0002-STATE-10
			//           *                                     )
			//           *           AND NOT A.DS_VER  = :IABV0009-VERSIONING
			//                       ORDER BY A.ID_POLI, A.ID_ADES
			//                      FETCH FIRST ROW ONLY
			//                   END-EXEC
			adesPoliStatOggBusTrchDiGarDao.selectRec(this.getAdesPoliStatOggBusTrchDiGarLdbm0250());
		} else {
			//           AND A.DS_STATO_ELAB IN (
			//                                   :IABV0002-STATE-01,
			//                                   :IABV0002-STATE-02,
			//                                   :IABV0002-STATE-03,
			//                                   :IABV0002-STATE-04,
			//                                   :IABV0002-STATE-05,
			//                                   :IABV0002-STATE-06,
			//                                   :IABV0002-STATE-07,
			//                                   :IABV0002-STATE-08,
			//                                   :IABV0002-STATE-09,
			//                                   :IABV0002-STATE-10
			//                                     )
			//           AND NOT A.DS_VER  = :IABV0009-VERSIONING
			// COB_CODE:         EXEC SQL
			//                     SELECT
			//                            A.ID_ADES
			//                           ,A.ID_POLI
			//                           ,A.ID_MOVI_CRZ
			//                           ,A.ID_MOVI_CHIU
			//                           ,A.DT_INI_EFF
			//                           ,A.DT_END_EFF
			//                           ,A.IB_PREV
			//                           ,A.IB_OGG
			//                           ,A.COD_COMP_ANIA
			//                           ,A.DT_DECOR
			//                           ,A.DT_SCAD
			//                           ,A.ETA_A_SCAD
			//                           ,A.DUR_AA
			//                           ,A.DUR_MM
			//                           ,A.DUR_GG
			//                           ,A.TP_RGM_FISC
			//                           ,A.TP_RIAT
			//                           ,A.TP_MOD_PAG_TIT
			//                           ,A.TP_IAS
			//                           ,A.DT_VARZ_TP_IAS
			//                           ,A.PRE_NET_IND
			//                           ,A.PRE_LRD_IND
			//                           ,A.RAT_LRD_IND
			//                           ,A.PRSTZ_INI_IND
			//                           ,A.FL_COINC_ASSTO
			//                           ,A.IB_DFLT
			//                           ,A.MOD_CALC
			//                           ,A.TP_FNT_CNBTVA
			//                           ,A.IMP_AZ
			//                           ,A.IMP_ADER
			//                           ,A.IMP_TFR
			//                           ,A.IMP_VOLO
			//                           ,A.PC_AZ
			//                           ,A.PC_ADER
			//                           ,A.PC_TFR
			//                           ,A.PC_VOLO
			//                           ,A.DT_NOVA_RGM_FISC
			//                           ,A.FL_ATTIV
			//                           ,A.IMP_REC_RIT_VIS
			//                           ,A.IMP_REC_RIT_ACC
			//                           ,A.FL_VARZ_STAT_TBGC
			//                           ,A.FL_PROVZA_MIGRAZ
			//                           ,A.IMPB_VIS_DA_REC
			//                           ,A.DT_DECOR_PREST_BAN
			//                           ,A.DT_EFF_VARZ_STAT_T
			//                           ,A.DS_RIGA
			//                           ,A.DS_OPER_SQL
			//                           ,A.DS_VER
			//                           ,A.DS_TS_INI_CPTZ
			//                           ,A.DS_TS_END_CPTZ
			//                           ,A.DS_UTENTE
			//                           ,A.DS_STATO_ELAB
			//                           ,A.CUM_CNBT_CAP
			//                           ,A.IMP_GAR_CNBT
			//                           ,A.DT_ULT_CONS_CNBT
			//                           ,A.IDEN_ISC_FND
			//                           ,A.NUM_RAT_PIAN
			//                           ,A.DT_PRESC
			//                           ,A.CONCS_PREST
			//                           ,B.ID_POLI
			//                           ,B.ID_MOVI_CRZ
			//                           ,B.ID_MOVI_CHIU
			//                           ,B.IB_OGG
			//                           ,B.IB_PROP
			//                           ,B.DT_PROP
			//                           ,B.DT_INI_EFF
			//                           ,B.DT_END_EFF
			//                           ,B.COD_COMP_ANIA
			//                           ,B.DT_DECOR
			//                           ,B.DT_EMIS
			//                           ,B.TP_POLI
			//                           ,B.DUR_AA
			//                           ,B.DUR_MM
			//                           ,B.DT_SCAD
			//                           ,B.COD_PROD
			//                           ,B.DT_INI_VLDT_PROD
			//                           ,B.COD_CONV
			//                           ,B.COD_RAMO
			//                           ,B.DT_INI_VLDT_CONV
			//                           ,B.DT_APPLZ_CONV
			//                           ,B.TP_FRM_ASSVA
			//                           ,B.TP_RGM_FISC
			//                           ,B.FL_ESTAS
			//                           ,B.FL_RSH_COMUN
			//                           ,B.FL_RSH_COMUN_COND
			//                           ,B.TP_LIV_GENZ_TIT
			//                           ,B.FL_COP_FINANZ
			//                           ,B.TP_APPLZ_DIR
			//                           ,B.SPE_MED
			//                           ,B.DIR_EMIS
			//                           ,B.DIR_1O_VERS
			//                           ,B.DIR_VERS_AGG
			//                           ,B.COD_DVS
			//                           ,B.FL_FNT_AZ
			//                           ,B.FL_FNT_ADER
			//                           ,B.FL_FNT_TFR
			//                           ,B.FL_FNT_VOLO
			//                           ,B.TP_OPZ_A_SCAD
			//                           ,B.AA_DIFF_PROR_DFLT
			//                           ,B.FL_VER_PROD
			//                           ,B.DUR_GG
			//                           ,B.DIR_QUIET
			//                           ,B.TP_PTF_ESTNO
			//                           ,B.FL_CUM_PRE_CNTR
			//                           ,B.FL_AMMB_MOVI
			//                           ,B.CONV_GECO
			//                           ,B.DS_RIGA
			//                           ,B.DS_OPER_SQL
			//                           ,B.DS_VER
			//                           ,B.DS_TS_INI_CPTZ
			//                           ,B.DS_TS_END_CPTZ
			//                           ,B.DS_UTENTE
			//                           ,B.DS_STATO_ELAB
			//                           ,B.FL_SCUDO_FISC
			//                           ,B.FL_TRASFE
			//                           ,B.FL_TFR_STRC
			//                           ,B.DT_PRESC
			//                           ,B.COD_CONV_AGG
			//                           ,B.SUBCAT_PROD
			//                           ,B.FL_QUEST_ADEGZ_ASS
			//                           ,B.COD_TPA
			//                           ,B.ID_ACC_COMM
			//                           ,B.FL_POLI_CPI_PR
			//                           ,B.FL_POLI_BUNDLING
			//                           ,B.IND_POLI_PRIN_COLL
			//                           ,B.FL_VND_BUNDLE
			//                           ,B.IB_BS
			//                           ,B.FL_POLI_IFP
			//                           ,C.ID_STAT_OGG_BUS
			//                           ,C.ID_OGG
			//                           ,C.TP_OGG
			//                           ,C.ID_MOVI_CRZ
			//                           ,C.ID_MOVI_CHIU
			//                           ,C.DT_INI_EFF
			//                           ,C.DT_END_EFF
			//                           ,C.COD_COMP_ANIA
			//                           ,C.TP_STAT_BUS
			//                           ,C.TP_CAUS
			//                           ,C.DS_RIGA
			//                           ,C.DS_OPER_SQL
			//                           ,C.DS_VER
			//                           ,C.DS_TS_INI_CPTZ
			//                           ,C.DS_TS_END_CPTZ
			//                           ,C.DS_UTENTE
			//                           ,C.DS_STATO_ELAB
			//                     INTO
			//                           :ADE-ID-ADES
			//                          ,:ADE-ID-POLI
			//                          ,:ADE-ID-MOVI-CRZ
			//                          ,:ADE-ID-MOVI-CHIU
			//                           :IND-ADE-ID-MOVI-CHIU
			//                          ,:ADE-DT-INI-EFF-DB
			//                          ,:ADE-DT-END-EFF-DB
			//                          ,:ADE-IB-PREV
			//                           :IND-ADE-IB-PREV
			//                          ,:ADE-IB-OGG
			//                           :IND-ADE-IB-OGG
			//                          ,:ADE-COD-COMP-ANIA
			//                          ,:ADE-DT-DECOR-DB
			//                           :IND-ADE-DT-DECOR
			//                          ,:ADE-DT-SCAD-DB
			//                           :IND-ADE-DT-SCAD
			//                          ,:ADE-ETA-A-SCAD
			//                           :IND-ADE-ETA-A-SCAD
			//                          ,:ADE-DUR-AA
			//                           :IND-ADE-DUR-AA
			//                          ,:ADE-DUR-MM
			//                           :IND-ADE-DUR-MM
			//                          ,:ADE-DUR-GG
			//                           :IND-ADE-DUR-GG
			//                          ,:ADE-TP-RGM-FISC
			//                          ,:ADE-TP-RIAT
			//                           :IND-ADE-TP-RIAT
			//                          ,:ADE-TP-MOD-PAG-TIT
			//                          ,:ADE-TP-IAS
			//                           :IND-ADE-TP-IAS
			//                          ,:ADE-DT-VARZ-TP-IAS-DB
			//                           :IND-ADE-DT-VARZ-TP-IAS
			//                          ,:ADE-PRE-NET-IND
			//                           :IND-ADE-PRE-NET-IND
			//                          ,:ADE-PRE-LRD-IND
			//                           :IND-ADE-PRE-LRD-IND
			//                          ,:ADE-RAT-LRD-IND
			//                           :IND-ADE-RAT-LRD-IND
			//                          ,:ADE-PRSTZ-INI-IND
			//                           :IND-ADE-PRSTZ-INI-IND
			//                          ,:ADE-FL-COINC-ASSTO
			//                           :IND-ADE-FL-COINC-ASSTO
			//                          ,:ADE-IB-DFLT
			//                           :IND-ADE-IB-DFLT
			//                          ,:ADE-MOD-CALC
			//                           :IND-ADE-MOD-CALC
			//                          ,:ADE-TP-FNT-CNBTVA
			//                           :IND-ADE-TP-FNT-CNBTVA
			//                          ,:ADE-IMP-AZ
			//                           :IND-ADE-IMP-AZ
			//                          ,:ADE-IMP-ADER
			//                           :IND-ADE-IMP-ADER
			//                          ,:ADE-IMP-TFR
			//                           :IND-ADE-IMP-TFR
			//                          ,:ADE-IMP-VOLO
			//                           :IND-ADE-IMP-VOLO
			//                          ,:ADE-PC-AZ
			//                           :IND-ADE-PC-AZ
			//                          ,:ADE-PC-ADER
			//                           :IND-ADE-PC-ADER
			//                          ,:ADE-PC-TFR
			//                           :IND-ADE-PC-TFR
			//                          ,:ADE-PC-VOLO
			//                           :IND-ADE-PC-VOLO
			//                          ,:ADE-DT-NOVA-RGM-FISC-DB
			//                           :IND-ADE-DT-NOVA-RGM-FISC
			//                          ,:ADE-FL-ATTIV
			//                           :IND-ADE-FL-ATTIV
			//                          ,:ADE-IMP-REC-RIT-VIS
			//                           :IND-ADE-IMP-REC-RIT-VIS
			//                          ,:ADE-IMP-REC-RIT-ACC
			//                           :IND-ADE-IMP-REC-RIT-ACC
			//                          ,:ADE-FL-VARZ-STAT-TBGC
			//                           :IND-ADE-FL-VARZ-STAT-TBGC
			//                          ,:ADE-FL-PROVZA-MIGRAZ
			//                           :IND-ADE-FL-PROVZA-MIGRAZ
			//                          ,:ADE-IMPB-VIS-DA-REC
			//                           :IND-ADE-IMPB-VIS-DA-REC
			//                          ,:ADE-DT-DECOR-PREST-BAN-DB
			//                           :IND-ADE-DT-DECOR-PREST-BAN
			//                          ,:ADE-DT-EFF-VARZ-STAT-T-DB
			//                           :IND-ADE-DT-EFF-VARZ-STAT-T
			//                          ,:ADE-DS-RIGA
			//                          ,:ADE-DS-OPER-SQL
			//                          ,:ADE-DS-VER
			//                          ,:ADE-DS-TS-INI-CPTZ
			//                          ,:ADE-DS-TS-END-CPTZ
			//                          ,:ADE-DS-UTENTE
			//                          ,:ADE-DS-STATO-ELAB
			//                          ,:ADE-CUM-CNBT-CAP
			//                           :IND-ADE-CUM-CNBT-CAP
			//                          ,:ADE-IMP-GAR-CNBT
			//                           :IND-ADE-IMP-GAR-CNBT
			//                          ,:ADE-DT-ULT-CONS-CNBT-DB
			//                           :IND-ADE-DT-ULT-CONS-CNBT
			//                          ,:ADE-IDEN-ISC-FND
			//                           :IND-ADE-IDEN-ISC-FND
			//                          ,:ADE-NUM-RAT-PIAN
			//                           :IND-ADE-NUM-RAT-PIAN
			//                          ,:ADE-DT-PRESC-DB
			//                           :IND-ADE-DT-PRESC
			//                          ,:ADE-CONCS-PREST
			//                           :IND-ADE-CONCS-PREST
			//                          ,:POL-ID-POLI
			//                          ,:POL-ID-MOVI-CRZ
			//                          ,:POL-ID-MOVI-CHIU
			//                           :IND-POL-ID-MOVI-CHIU
			//                          ,:POL-IB-OGG
			//                           :IND-POL-IB-OGG
			//                          ,:POL-IB-PROP
			//                          ,:POL-DT-PROP-DB
			//                           :IND-POL-DT-PROP
			//                          ,:POL-DT-INI-EFF-DB
			//                          ,:POL-DT-END-EFF-DB
			//                          ,:POL-COD-COMP-ANIA
			//                          ,:POL-DT-DECOR-DB
			//                          ,:POL-DT-EMIS-DB
			//                          ,:POL-TP-POLI
			//                          ,:POL-DUR-AA
			//                           :IND-POL-DUR-AA
			//                          ,:POL-DUR-MM
			//                           :IND-POL-DUR-MM
			//                          ,:POL-DT-SCAD-DB
			//                           :IND-POL-DT-SCAD
			//                          ,:POL-COD-PROD
			//                          ,:POL-DT-INI-VLDT-PROD-DB
			//                          ,:POL-COD-CONV
			//                           :IND-POL-COD-CONV
			//                          ,:POL-COD-RAMO
			//                           :IND-POL-COD-RAMO
			//                          ,:POL-DT-INI-VLDT-CONV-DB
			//                           :IND-POL-DT-INI-VLDT-CONV
			//                          ,:POL-DT-APPLZ-CONV-DB
			//                           :IND-POL-DT-APPLZ-CONV
			//                          ,:POL-TP-FRM-ASSVA
			//                          ,:POL-TP-RGM-FISC
			//                           :IND-POL-TP-RGM-FISC
			//                          ,:POL-FL-ESTAS
			//                           :IND-POL-FL-ESTAS
			//                          ,:POL-FL-RSH-COMUN
			//                           :IND-POL-FL-RSH-COMUN
			//                          ,:POL-FL-RSH-COMUN-COND
			//                           :IND-POL-FL-RSH-COMUN-COND
			//                          ,:POL-TP-LIV-GENZ-TIT
			//                          ,:POL-FL-COP-FINANZ
			//                           :IND-POL-FL-COP-FINANZ
			//                          ,:POL-TP-APPLZ-DIR
			//                           :IND-POL-TP-APPLZ-DIR
			//                          ,:POL-SPE-MED
			//                           :IND-POL-SPE-MED
			//                          ,:POL-DIR-EMIS
			//                           :IND-POL-DIR-EMIS
			//                          ,:POL-DIR-1O-VERS
			//                           :IND-POL-DIR-1O-VERS
			//                          ,:POL-DIR-VERS-AGG
			//                           :IND-POL-DIR-VERS-AGG
			//                          ,:POL-COD-DVS
			//                           :IND-POL-COD-DVS
			//                          ,:POL-FL-FNT-AZ
			//                           :IND-POL-FL-FNT-AZ
			//                          ,:POL-FL-FNT-ADER
			//                           :IND-POL-FL-FNT-ADER
			//                          ,:POL-FL-FNT-TFR
			//                           :IND-POL-FL-FNT-TFR
			//                          ,:POL-FL-FNT-VOLO
			//                           :IND-POL-FL-FNT-VOLO
			//                          ,:POL-TP-OPZ-A-SCAD
			//                           :IND-POL-TP-OPZ-A-SCAD
			//                          ,:POL-AA-DIFF-PROR-DFLT
			//                           :IND-POL-AA-DIFF-PROR-DFLT
			//                          ,:POL-FL-VER-PROD
			//                           :IND-POL-FL-VER-PROD
			//                          ,:POL-DUR-GG
			//                           :IND-POL-DUR-GG
			//                          ,:POL-DIR-QUIET
			//                           :IND-POL-DIR-QUIET
			//                          ,:POL-TP-PTF-ESTNO
			//                           :IND-POL-TP-PTF-ESTNO
			//                          ,:POL-FL-CUM-PRE-CNTR
			//                           :IND-POL-FL-CUM-PRE-CNTR
			//                          ,:POL-FL-AMMB-MOVI
			//                           :IND-POL-FL-AMMB-MOVI
			//                          ,:POL-CONV-GECO
			//                           :IND-POL-CONV-GECO
			//                          ,:POL-DS-RIGA
			//                          ,:POL-DS-OPER-SQL
			//                          ,:POL-DS-VER
			//                          ,:POL-DS-TS-INI-CPTZ
			//                          ,:POL-DS-TS-END-CPTZ
			//                          ,:POL-DS-UTENTE
			//                          ,:POL-DS-STATO-ELAB
			//                          ,:POL-FL-SCUDO-FISC
			//                           :IND-POL-FL-SCUDO-FISC
			//                          ,:POL-FL-TRASFE
			//                           :IND-POL-FL-TRASFE
			//                          ,:POL-FL-TFR-STRC
			//                           :IND-POL-FL-TFR-STRC
			//                          ,:POL-DT-PRESC-DB
			//                           :IND-POL-DT-PRESC
			//                          ,:POL-COD-CONV-AGG
			//                           :IND-POL-COD-CONV-AGG
			//                          ,:POL-SUBCAT-PROD
			//                           :IND-POL-SUBCAT-PROD
			//                          ,:POL-FL-QUEST-ADEGZ-ASS
			//                           :IND-POL-FL-QUEST-ADEGZ-ASS
			//                          ,:POL-COD-TPA
			//                           :IND-POL-COD-TPA
			//                          ,:POL-ID-ACC-COMM
			//                           :IND-POL-ID-ACC-COMM
			//                          ,:POL-FL-POLI-CPI-PR
			//                           :IND-POL-FL-POLI-CPI-PR
			//                          ,:POL-FL-POLI-BUNDLING
			//                           :IND-POL-FL-POLI-BUNDLING
			//                          ,:POL-IND-POLI-PRIN-COLL
			//                           :IND-POL-IND-POLI-PRIN-COLL
			//                          ,:POL-FL-VND-BUNDLE
			//                           :IND-POL-FL-VND-BUNDLE
			//                          ,:POL-IB-BS
			//                           :IND-POL-IB-BS
			//                          ,:POL-FL-POLI-IFP
			//                           :IND-POL-FL-POLI-IFP
			//                          ,:STB-ID-STAT-OGG-BUS
			//                          ,:STB-ID-OGG
			//                          ,:STB-TP-OGG
			//                          ,:STB-ID-MOVI-CRZ
			//                          ,:STB-ID-MOVI-CHIU
			//                           :IND-STB-ID-MOVI-CHIU
			//                          ,:STB-DT-INI-EFF-DB
			//                          ,:STB-DT-END-EFF-DB
			//                          ,:STB-COD-COMP-ANIA
			//                          ,:STB-TP-STAT-BUS
			//                          ,:STB-TP-CAUS
			//                          ,:STB-DS-RIGA
			//                          ,:STB-DS-OPER-SQL
			//                          ,:STB-DS-VER
			//                          ,:STB-DS-TS-INI-CPTZ
			//                          ,:STB-DS-TS-END-CPTZ
			//                          ,:STB-DS-UTENTE
			//                          ,:STB-DS-STATO-ELAB
			//                     FROM ADES      A,
			//                          POLI      B,
			//                          STAT_OGG_BUS C
			//                     WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
			//                                                :WS-TP-FRM-ASSVA2 )
			//                       AND A.ID_POLI      =   B.ID_POLI
			//                       AND A.ID_ADES      =   C.ID_OGG
			//                       AND C.TP_OGG       =  'AD'
			//           *--      AND C.TP_STAT_BUS     =  'VI'
			//                       AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
			//                       AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
			//                       AND A.DT_END_EFF  >   :WS-DT-PTF-X
			//                       AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                       AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                       AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
			//                       AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
			//                       AND B.DT_END_EFF  >   :WS-DT-PTF-X
			//                       AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                       AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                       AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
			//                       AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
			//                       AND C.DT_END_EFF  >   :WS-DT-PTF-X
			//                       AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                       AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                       AND A.ID_ADES IN
			//                          (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
			//                                                           STAT_OGG_BUS E
			//                           WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
			//                           AND E.TP_OGG = 'TG'
			//                           AND E.TP_STAT_BUS IN ('VI','ST')
			//                           AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
			//                           AND D.COD_COMP_ANIA =
			//                               :IDSV0003-CODICE-COMPAGNIA-ANIA
			//                           AND D.DT_INI_EFF <=   :WS-DT-PTF-X
			//                           AND D.DT_END_EFF >    :WS-DT-PTF-X
			//                           AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                           AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                           AND E.DT_INI_EFF <=   :WS-DT-PTF-X
			//                           AND E.DT_END_EFF >    :WS-DT-PTF-X
			//                           AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
			//                           AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
			//                           AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
			//                                OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
			//                           )
			//           *           AND A.DS_STATO_ELAB IN (
			//           *                                   :IABV0002-STATE-01,
			//           *                                   :IABV0002-STATE-02,
			//           *                                   :IABV0002-STATE-03,
			//           *                                   :IABV0002-STATE-04,
			//           *                                   :IABV0002-STATE-05,
			//           *                                   :IABV0002-STATE-06,
			//           *                                   :IABV0002-STATE-07,
			//           *                                   :IABV0002-STATE-08,
			//           *                                   :IABV0002-STATE-09,
			//           *                                   :IABV0002-STATE-10
			//           *                                     )
			//           *           AND NOT A.DS_VER  = :IABV0009-VERSIONING
			//                       ORDER BY A.ID_POLI, A.ID_ADES
			//                      FETCH FIRST ROW ONLY
			//                   END-EXEC
			adesPoliStatOggBusTrchDiGarDao.selectRec1(this.getAdesPoliStatOggBusTrchDiGarLdbm0250());
		}
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			z100SetColonneNull();
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		}
	}

	/**Original name: A320-UPDATE-SC05<br>
	 * <pre>*****************************************************************</pre>*/
	private void a320UpdateSc05() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-PRIMARY-KEY
		//                   PERFORM A330-UPDATE-PK-SC05         THRU A330-SC05-EX
		//              WHEN IDSV0003-WHERE-CONDITION-05
		//                   PERFORM A340-UPDATE-WHERE-COND-SC05 THRU A340-SC05-EX
		//              WHEN IDSV0003-FIRST-ACTION
		//                                                       THRU A345-SC05-EX
		//              WHEN OTHER
		//                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
		//           END-EVALUATE.
		if (idsv0003.getLivelloOperazione().isIdsv0003PrimaryKey()) {
			// COB_CODE: PERFORM A330-UPDATE-PK-SC05         THRU A330-SC05-EX
			a330UpdatePkSc05();
		} else if (idsv0003.getTipologiaOperazione().isIdsv0003WhereCondition05()) {
			// COB_CODE: PERFORM A340-UPDATE-WHERE-COND-SC05 THRU A340-SC05-EX
			a340UpdateWhereCondSc05();
		} else if (idsv0003.getLivelloOperazione().isIdsv0003FirstAction()) {
			// COB_CODE: PERFORM A345-UPDATE-FIRST-ACTION-SC05
			//                                               THRU A345-SC05-EX
			a345UpdateFirstActionSc05();
		} else {
			// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
			idsv0003.getReturnCode().setInvalidLevelOper();
		}
	}

	/**Original name: A330-UPDATE-PK-SC05<br>
	 * <pre>*****************************************************************</pre>*/
	private void a330UpdatePkSc05() {
		// COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
		z150ValorizzaDataServices();
		// COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
		z200SetIndicatoriNull();
		// COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBM0250.cbl:line=6945, because the code is unreachable.
		// COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
		z960LengthVchar();
		//           ,DS_VER                 = :IABV0009-VERSIONING
		// COB_CODE:      EXEC SQL
		//                     UPDATE ADES
		//                     SET
		//                        DS_OPER_SQL            = :ADE-DS-OPER-SQL
		//           *           ,DS_VER                 = :IABV0009-VERSIONING
		//                       ,DS_UTENTE              = :ADE-DS-UTENTE
		//                       ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
		//                     WHERE             DS_RIGA = :ADE-DS-RIGA
		//                END-EXEC.
		adesDao.updateRec2(ades.getAdeDsOperSql(), ades.getAdeDsUtente(), iabv0002.getIabv0002StateCurrent(), ades.getAdeDsRiga());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A340-UPDATE-WHERE-COND-SC05<br>
	 * <pre>*****************************************************************</pre>*/
	private void a340UpdateWhereCondSc05() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A345-UPDATE-FIRST-ACTION-SC05<br>
	 * <pre>*****************************************************************</pre>*/
	private void a345UpdateFirstActionSc05() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A360-OPEN-CURSOR-SC05<br>
	 * <pre>*****************************************************************</pre>*/
	private void a360OpenCursorSc05() {
		// COB_CODE: PERFORM A305-DECLARE-CURSOR-SC05 THRU A305-SC05-EX.
		a305DeclareCursorSc05();
		// COB_CODE: IF ACCESSO-X-RANGE-SI
		//              END-EXEC
		//           ELSE
		//              END-EXEC
		//           END-IF.
		if (ws.getFlagAccessoXRange().isSi()) {
			// COB_CODE: EXEC SQL
			//                OPEN CUR-SC05-RANGE
			//           END-EXEC
			adesPoliStatOggBusTrchDiGarDao.openCurSc05Range(this.getAdesPoliStatOggBusTrchDiGarLdbm0250());
		} else {
			// COB_CODE: EXEC SQL
			//                OPEN CUR-SC05
			//           END-EXEC
			adesPoliStatOggBusTrchDiGarDao.openCurSc05(this.getAdesPoliStatOggBusTrchDiGarLdbm0250());
		}
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A370-CLOSE-CURSOR-SC05<br>
	 * <pre>*****************************************************************</pre>*/
	private void a370CloseCursorSc05() {
		// COB_CODE: IF ACCESSO-X-RANGE-SI
		//              END-EXEC
		//           ELSE
		//              END-EXEC
		//           END-IF.
		if (ws.getFlagAccessoXRange().isSi()) {
			// COB_CODE: EXEC SQL
			//                CLOSE CUR-SC05-RANGE
			//           END-EXEC
			adesPoliStatOggBusTrchDiGarDao.closeCurSc05Range();
		} else {
			// COB_CODE: EXEC SQL
			//                CLOSE CUR-SC05
			//           END-EXEC
			adesPoliStatOggBusTrchDiGarDao.closeCurSc05();
		}
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A380-FETCH-FIRST-SC05<br>
	 * <pre>*****************************************************************</pre>*/
	private void a380FetchFirstSc05() {
		// COB_CODE: PERFORM A360-OPEN-CURSOR-SC05    THRU A360-SC05-EX.
		a360OpenCursorSc05();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM A390-FETCH-NEXT-SC05  THRU A390-SC05-EX
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM A390-FETCH-NEXT-SC05  THRU A390-SC05-EX
			a390FetchNextSc05();
		}
	}

	/**Original name: A390-FETCH-NEXT-SC05<br>
	 * <pre>*****************************************************************</pre>*/
	private void a390FetchNextSc05() {
		// COB_CODE: IF ACCESSO-X-RANGE-SI
		//              END-EXEC
		//           ELSE
		//              END-EXEC
		//           END-IF.
		if (ws.getFlagAccessoXRange().isSi()) {
			// COB_CODE: EXEC SQL
			//                FETCH CUR-SC05-RANGE
			//           INTO
			//                   :ADE-ID-ADES
			//                  ,:ADE-ID-POLI
			//                  ,:ADE-ID-MOVI-CRZ
			//                  ,:ADE-ID-MOVI-CHIU
			//                   :IND-ADE-ID-MOVI-CHIU
			//                  ,:ADE-DT-INI-EFF-DB
			//                  ,:ADE-DT-END-EFF-DB
			//                  ,:ADE-IB-PREV
			//                   :IND-ADE-IB-PREV
			//                  ,:ADE-IB-OGG
			//                   :IND-ADE-IB-OGG
			//                  ,:ADE-COD-COMP-ANIA
			//                  ,:ADE-DT-DECOR-DB
			//                   :IND-ADE-DT-DECOR
			//                  ,:ADE-DT-SCAD-DB
			//                   :IND-ADE-DT-SCAD
			//                  ,:ADE-ETA-A-SCAD
			//                   :IND-ADE-ETA-A-SCAD
			//                  ,:ADE-DUR-AA
			//                   :IND-ADE-DUR-AA
			//                  ,:ADE-DUR-MM
			//                   :IND-ADE-DUR-MM
			//                  ,:ADE-DUR-GG
			//                   :IND-ADE-DUR-GG
			//                  ,:ADE-TP-RGM-FISC
			//                  ,:ADE-TP-RIAT
			//                   :IND-ADE-TP-RIAT
			//                  ,:ADE-TP-MOD-PAG-TIT
			//                  ,:ADE-TP-IAS
			//                   :IND-ADE-TP-IAS
			//                  ,:ADE-DT-VARZ-TP-IAS-DB
			//                   :IND-ADE-DT-VARZ-TP-IAS
			//                  ,:ADE-PRE-NET-IND
			//                   :IND-ADE-PRE-NET-IND
			//                  ,:ADE-PRE-LRD-IND
			//                   :IND-ADE-PRE-LRD-IND
			//                  ,:ADE-RAT-LRD-IND
			//                   :IND-ADE-RAT-LRD-IND
			//                  ,:ADE-PRSTZ-INI-IND
			//                   :IND-ADE-PRSTZ-INI-IND
			//                  ,:ADE-FL-COINC-ASSTO
			//                   :IND-ADE-FL-COINC-ASSTO
			//                  ,:ADE-IB-DFLT
			//                   :IND-ADE-IB-DFLT
			//                  ,:ADE-MOD-CALC
			//                   :IND-ADE-MOD-CALC
			//                  ,:ADE-TP-FNT-CNBTVA
			//                   :IND-ADE-TP-FNT-CNBTVA
			//                  ,:ADE-IMP-AZ
			//                   :IND-ADE-IMP-AZ
			//                  ,:ADE-IMP-ADER
			//                   :IND-ADE-IMP-ADER
			//                  ,:ADE-IMP-TFR
			//                   :IND-ADE-IMP-TFR
			//                  ,:ADE-IMP-VOLO
			//                   :IND-ADE-IMP-VOLO
			//                  ,:ADE-PC-AZ
			//                   :IND-ADE-PC-AZ
			//                  ,:ADE-PC-ADER
			//                   :IND-ADE-PC-ADER
			//                  ,:ADE-PC-TFR
			//                   :IND-ADE-PC-TFR
			//                  ,:ADE-PC-VOLO
			//                   :IND-ADE-PC-VOLO
			//                  ,:ADE-DT-NOVA-RGM-FISC-DB
			//                   :IND-ADE-DT-NOVA-RGM-FISC
			//                  ,:ADE-FL-ATTIV
			//                   :IND-ADE-FL-ATTIV
			//                  ,:ADE-IMP-REC-RIT-VIS
			//                   :IND-ADE-IMP-REC-RIT-VIS
			//                  ,:ADE-IMP-REC-RIT-ACC
			//                   :IND-ADE-IMP-REC-RIT-ACC
			//                  ,:ADE-FL-VARZ-STAT-TBGC
			//                   :IND-ADE-FL-VARZ-STAT-TBGC
			//                  ,:ADE-FL-PROVZA-MIGRAZ
			//                   :IND-ADE-FL-PROVZA-MIGRAZ
			//                  ,:ADE-IMPB-VIS-DA-REC
			//                   :IND-ADE-IMPB-VIS-DA-REC
			//                  ,:ADE-DT-DECOR-PREST-BAN-DB
			//                   :IND-ADE-DT-DECOR-PREST-BAN
			//                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
			//                   :IND-ADE-DT-EFF-VARZ-STAT-T
			//                  ,:ADE-DS-RIGA
			//                  ,:ADE-DS-OPER-SQL
			//                  ,:ADE-DS-VER
			//                  ,:ADE-DS-TS-INI-CPTZ
			//                  ,:ADE-DS-TS-END-CPTZ
			//                  ,:ADE-DS-UTENTE
			//                  ,:ADE-DS-STATO-ELAB
			//                  ,:ADE-CUM-CNBT-CAP
			//                   :IND-ADE-CUM-CNBT-CAP
			//                  ,:ADE-IMP-GAR-CNBT
			//                   :IND-ADE-IMP-GAR-CNBT
			//                  ,:ADE-DT-ULT-CONS-CNBT-DB
			//                   :IND-ADE-DT-ULT-CONS-CNBT
			//                  ,:ADE-IDEN-ISC-FND
			//                   :IND-ADE-IDEN-ISC-FND
			//                  ,:ADE-NUM-RAT-PIAN
			//                   :IND-ADE-NUM-RAT-PIAN
			//                  ,:ADE-DT-PRESC-DB
			//                   :IND-ADE-DT-PRESC
			//                  ,:ADE-CONCS-PREST
			//                   :IND-ADE-CONCS-PREST
			//                  ,:POL-ID-POLI
			//                  ,:POL-ID-MOVI-CRZ
			//                  ,:POL-ID-MOVI-CHIU
			//                   :IND-POL-ID-MOVI-CHIU
			//                  ,:POL-IB-OGG
			//                   :IND-POL-IB-OGG
			//                  ,:POL-IB-PROP
			//                  ,:POL-DT-PROP-DB
			//                   :IND-POL-DT-PROP
			//                  ,:POL-DT-INI-EFF-DB
			//                  ,:POL-DT-END-EFF-DB
			//                  ,:POL-COD-COMP-ANIA
			//                  ,:POL-DT-DECOR-DB
			//                  ,:POL-DT-EMIS-DB
			//                  ,:POL-TP-POLI
			//                  ,:POL-DUR-AA
			//                   :IND-POL-DUR-AA
			//                  ,:POL-DUR-MM
			//                   :IND-POL-DUR-MM
			//                  ,:POL-DT-SCAD-DB
			//                   :IND-POL-DT-SCAD
			//                  ,:POL-COD-PROD
			//                  ,:POL-DT-INI-VLDT-PROD-DB
			//                  ,:POL-COD-CONV
			//                   :IND-POL-COD-CONV
			//                  ,:POL-COD-RAMO
			//                   :IND-POL-COD-RAMO
			//                  ,:POL-DT-INI-VLDT-CONV-DB
			//                   :IND-POL-DT-INI-VLDT-CONV
			//                  ,:POL-DT-APPLZ-CONV-DB
			//                   :IND-POL-DT-APPLZ-CONV
			//                  ,:POL-TP-FRM-ASSVA
			//                  ,:POL-TP-RGM-FISC
			//                   :IND-POL-TP-RGM-FISC
			//                  ,:POL-FL-ESTAS
			//                   :IND-POL-FL-ESTAS
			//                  ,:POL-FL-RSH-COMUN
			//                   :IND-POL-FL-RSH-COMUN
			//                  ,:POL-FL-RSH-COMUN-COND
			//                   :IND-POL-FL-RSH-COMUN-COND
			//                  ,:POL-TP-LIV-GENZ-TIT
			//                  ,:POL-FL-COP-FINANZ
			//                   :IND-POL-FL-COP-FINANZ
			//                  ,:POL-TP-APPLZ-DIR
			//                   :IND-POL-TP-APPLZ-DIR
			//                  ,:POL-SPE-MED
			//                   :IND-POL-SPE-MED
			//                  ,:POL-DIR-EMIS
			//                   :IND-POL-DIR-EMIS
			//                  ,:POL-DIR-1O-VERS
			//                   :IND-POL-DIR-1O-VERS
			//                  ,:POL-DIR-VERS-AGG
			//                   :IND-POL-DIR-VERS-AGG
			//                  ,:POL-COD-DVS
			//                   :IND-POL-COD-DVS
			//                  ,:POL-FL-FNT-AZ
			//                   :IND-POL-FL-FNT-AZ
			//                  ,:POL-FL-FNT-ADER
			//                   :IND-POL-FL-FNT-ADER
			//                  ,:POL-FL-FNT-TFR
			//                   :IND-POL-FL-FNT-TFR
			//                  ,:POL-FL-FNT-VOLO
			//                   :IND-POL-FL-FNT-VOLO
			//                  ,:POL-TP-OPZ-A-SCAD
			//                   :IND-POL-TP-OPZ-A-SCAD
			//                  ,:POL-AA-DIFF-PROR-DFLT
			//                   :IND-POL-AA-DIFF-PROR-DFLT
			//                  ,:POL-FL-VER-PROD
			//                   :IND-POL-FL-VER-PROD
			//                  ,:POL-DUR-GG
			//                   :IND-POL-DUR-GG
			//                  ,:POL-DIR-QUIET
			//                   :IND-POL-DIR-QUIET
			//                  ,:POL-TP-PTF-ESTNO
			//                   :IND-POL-TP-PTF-ESTNO
			//                  ,:POL-FL-CUM-PRE-CNTR
			//                   :IND-POL-FL-CUM-PRE-CNTR
			//                  ,:POL-FL-AMMB-MOVI
			//                   :IND-POL-FL-AMMB-MOVI
			//                  ,:POL-CONV-GECO
			//                   :IND-POL-CONV-GECO
			//                  ,:POL-DS-RIGA
			//                  ,:POL-DS-OPER-SQL
			//                  ,:POL-DS-VER
			//                  ,:POL-DS-TS-INI-CPTZ
			//                  ,:POL-DS-TS-END-CPTZ
			//                  ,:POL-DS-UTENTE
			//                  ,:POL-DS-STATO-ELAB
			//                  ,:POL-FL-SCUDO-FISC
			//                   :IND-POL-FL-SCUDO-FISC
			//                  ,:POL-FL-TRASFE
			//                   :IND-POL-FL-TRASFE
			//                  ,:POL-FL-TFR-STRC
			//                   :IND-POL-FL-TFR-STRC
			//                  ,:POL-DT-PRESC-DB
			//                   :IND-POL-DT-PRESC
			//                  ,:POL-COD-CONV-AGG
			//                   :IND-POL-COD-CONV-AGG
			//                  ,:POL-SUBCAT-PROD
			//                   :IND-POL-SUBCAT-PROD
			//                  ,:POL-FL-QUEST-ADEGZ-ASS
			//                   :IND-POL-FL-QUEST-ADEGZ-ASS
			//                  ,:POL-COD-TPA
			//                   :IND-POL-COD-TPA
			//                  ,:POL-ID-ACC-COMM
			//                   :IND-POL-ID-ACC-COMM
			//                  ,:POL-FL-POLI-CPI-PR
			//                   :IND-POL-FL-POLI-CPI-PR
			//                  ,:POL-FL-POLI-BUNDLING
			//                   :IND-POL-FL-POLI-BUNDLING
			//                  ,:POL-IND-POLI-PRIN-COLL
			//                   :IND-POL-IND-POLI-PRIN-COLL
			//                  ,:POL-FL-VND-BUNDLE
			//                   :IND-POL-FL-VND-BUNDLE
			//                  ,:POL-IB-BS
			//                   :IND-POL-IB-BS
			//                  ,:POL-FL-POLI-IFP
			//                   :IND-POL-FL-POLI-IFP
			//                  ,:STB-ID-STAT-OGG-BUS
			//                  ,:STB-ID-OGG
			//                  ,:STB-TP-OGG
			//                  ,:STB-ID-MOVI-CRZ
			//                  ,:STB-ID-MOVI-CHIU
			//                   :IND-STB-ID-MOVI-CHIU
			//                  ,:STB-DT-INI-EFF-DB
			//                  ,:STB-DT-END-EFF-DB
			//                  ,:STB-COD-COMP-ANIA
			//                  ,:STB-TP-STAT-BUS
			//                  ,:STB-TP-CAUS
			//                  ,:STB-DS-RIGA
			//                  ,:STB-DS-OPER-SQL
			//                  ,:STB-DS-VER
			//                  ,:STB-DS-TS-INI-CPTZ
			//                  ,:STB-DS-TS-END-CPTZ
			//                  ,:STB-DS-UTENTE
			//                  ,:STB-DS-STATO-ELAB
			//           END-EXEC
			adesPoliStatOggBusTrchDiGarDao.fetchCurSc05Range(this.getAdesPoliStatOggBusTrchDiGarLdbm0250());
		} else {
			// COB_CODE: EXEC SQL
			//                FETCH CUR-SC05
			//           INTO
			//                   :ADE-ID-ADES
			//                  ,:ADE-ID-POLI
			//                  ,:ADE-ID-MOVI-CRZ
			//                  ,:ADE-ID-MOVI-CHIU
			//                   :IND-ADE-ID-MOVI-CHIU
			//                  ,:ADE-DT-INI-EFF-DB
			//                  ,:ADE-DT-END-EFF-DB
			//                  ,:ADE-IB-PREV
			//                   :IND-ADE-IB-PREV
			//                  ,:ADE-IB-OGG
			//                   :IND-ADE-IB-OGG
			//                  ,:ADE-COD-COMP-ANIA
			//                  ,:ADE-DT-DECOR-DB
			//                   :IND-ADE-DT-DECOR
			//                  ,:ADE-DT-SCAD-DB
			//                   :IND-ADE-DT-SCAD
			//                  ,:ADE-ETA-A-SCAD
			//                   :IND-ADE-ETA-A-SCAD
			//                  ,:ADE-DUR-AA
			//                   :IND-ADE-DUR-AA
			//                  ,:ADE-DUR-MM
			//                   :IND-ADE-DUR-MM
			//                  ,:ADE-DUR-GG
			//                   :IND-ADE-DUR-GG
			//                  ,:ADE-TP-RGM-FISC
			//                  ,:ADE-TP-RIAT
			//                   :IND-ADE-TP-RIAT
			//                  ,:ADE-TP-MOD-PAG-TIT
			//                  ,:ADE-TP-IAS
			//                   :IND-ADE-TP-IAS
			//                  ,:ADE-DT-VARZ-TP-IAS-DB
			//                   :IND-ADE-DT-VARZ-TP-IAS
			//                  ,:ADE-PRE-NET-IND
			//                   :IND-ADE-PRE-NET-IND
			//                  ,:ADE-PRE-LRD-IND
			//                   :IND-ADE-PRE-LRD-IND
			//                  ,:ADE-RAT-LRD-IND
			//                   :IND-ADE-RAT-LRD-IND
			//                  ,:ADE-PRSTZ-INI-IND
			//                   :IND-ADE-PRSTZ-INI-IND
			//                  ,:ADE-FL-COINC-ASSTO
			//                   :IND-ADE-FL-COINC-ASSTO
			//                  ,:ADE-IB-DFLT
			//                   :IND-ADE-IB-DFLT
			//                  ,:ADE-MOD-CALC
			//                   :IND-ADE-MOD-CALC
			//                  ,:ADE-TP-FNT-CNBTVA
			//                   :IND-ADE-TP-FNT-CNBTVA
			//                  ,:ADE-IMP-AZ
			//                   :IND-ADE-IMP-AZ
			//                  ,:ADE-IMP-ADER
			//                   :IND-ADE-IMP-ADER
			//                  ,:ADE-IMP-TFR
			//                   :IND-ADE-IMP-TFR
			//                  ,:ADE-IMP-VOLO
			//                   :IND-ADE-IMP-VOLO
			//                  ,:ADE-PC-AZ
			//                   :IND-ADE-PC-AZ
			//                  ,:ADE-PC-ADER
			//                   :IND-ADE-PC-ADER
			//                  ,:ADE-PC-TFR
			//                   :IND-ADE-PC-TFR
			//                  ,:ADE-PC-VOLO
			//                   :IND-ADE-PC-VOLO
			//                  ,:ADE-DT-NOVA-RGM-FISC-DB
			//                   :IND-ADE-DT-NOVA-RGM-FISC
			//                  ,:ADE-FL-ATTIV
			//                   :IND-ADE-FL-ATTIV
			//                  ,:ADE-IMP-REC-RIT-VIS
			//                   :IND-ADE-IMP-REC-RIT-VIS
			//                  ,:ADE-IMP-REC-RIT-ACC
			//                   :IND-ADE-IMP-REC-RIT-ACC
			//                  ,:ADE-FL-VARZ-STAT-TBGC
			//                   :IND-ADE-FL-VARZ-STAT-TBGC
			//                  ,:ADE-FL-PROVZA-MIGRAZ
			//                   :IND-ADE-FL-PROVZA-MIGRAZ
			//                  ,:ADE-IMPB-VIS-DA-REC
			//                   :IND-ADE-IMPB-VIS-DA-REC
			//                  ,:ADE-DT-DECOR-PREST-BAN-DB
			//                   :IND-ADE-DT-DECOR-PREST-BAN
			//                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
			//                   :IND-ADE-DT-EFF-VARZ-STAT-T
			//                  ,:ADE-DS-RIGA
			//                  ,:ADE-DS-OPER-SQL
			//                  ,:ADE-DS-VER
			//                  ,:ADE-DS-TS-INI-CPTZ
			//                  ,:ADE-DS-TS-END-CPTZ
			//                  ,:ADE-DS-UTENTE
			//                  ,:ADE-DS-STATO-ELAB
			//                  ,:ADE-CUM-CNBT-CAP
			//                   :IND-ADE-CUM-CNBT-CAP
			//                  ,:ADE-IMP-GAR-CNBT
			//                   :IND-ADE-IMP-GAR-CNBT
			//                  ,:ADE-DT-ULT-CONS-CNBT-DB
			//                   :IND-ADE-DT-ULT-CONS-CNBT
			//                  ,:ADE-IDEN-ISC-FND
			//                   :IND-ADE-IDEN-ISC-FND
			//                  ,:ADE-NUM-RAT-PIAN
			//                   :IND-ADE-NUM-RAT-PIAN
			//                  ,:ADE-DT-PRESC-DB
			//                   :IND-ADE-DT-PRESC
			//                  ,:ADE-CONCS-PREST
			//                   :IND-ADE-CONCS-PREST
			//                  ,:POL-ID-POLI
			//                  ,:POL-ID-MOVI-CRZ
			//                  ,:POL-ID-MOVI-CHIU
			//                   :IND-POL-ID-MOVI-CHIU
			//                  ,:POL-IB-OGG
			//                   :IND-POL-IB-OGG
			//                  ,:POL-IB-PROP
			//                  ,:POL-DT-PROP-DB
			//                   :IND-POL-DT-PROP
			//                  ,:POL-DT-INI-EFF-DB
			//                  ,:POL-DT-END-EFF-DB
			//                  ,:POL-COD-COMP-ANIA
			//                  ,:POL-DT-DECOR-DB
			//                  ,:POL-DT-EMIS-DB
			//                  ,:POL-TP-POLI
			//                  ,:POL-DUR-AA
			//                   :IND-POL-DUR-AA
			//                  ,:POL-DUR-MM
			//                   :IND-POL-DUR-MM
			//                  ,:POL-DT-SCAD-DB
			//                   :IND-POL-DT-SCAD
			//                  ,:POL-COD-PROD
			//                  ,:POL-DT-INI-VLDT-PROD-DB
			//                  ,:POL-COD-CONV
			//                   :IND-POL-COD-CONV
			//                  ,:POL-COD-RAMO
			//                   :IND-POL-COD-RAMO
			//                  ,:POL-DT-INI-VLDT-CONV-DB
			//                   :IND-POL-DT-INI-VLDT-CONV
			//                  ,:POL-DT-APPLZ-CONV-DB
			//                   :IND-POL-DT-APPLZ-CONV
			//                  ,:POL-TP-FRM-ASSVA
			//                  ,:POL-TP-RGM-FISC
			//                   :IND-POL-TP-RGM-FISC
			//                  ,:POL-FL-ESTAS
			//                   :IND-POL-FL-ESTAS
			//                  ,:POL-FL-RSH-COMUN
			//                   :IND-POL-FL-RSH-COMUN
			//                  ,:POL-FL-RSH-COMUN-COND
			//                   :IND-POL-FL-RSH-COMUN-COND
			//                  ,:POL-TP-LIV-GENZ-TIT
			//                  ,:POL-FL-COP-FINANZ
			//                   :IND-POL-FL-COP-FINANZ
			//                  ,:POL-TP-APPLZ-DIR
			//                   :IND-POL-TP-APPLZ-DIR
			//                  ,:POL-SPE-MED
			//                   :IND-POL-SPE-MED
			//                  ,:POL-DIR-EMIS
			//                   :IND-POL-DIR-EMIS
			//                  ,:POL-DIR-1O-VERS
			//                   :IND-POL-DIR-1O-VERS
			//                  ,:POL-DIR-VERS-AGG
			//                   :IND-POL-DIR-VERS-AGG
			//                  ,:POL-COD-DVS
			//                   :IND-POL-COD-DVS
			//                  ,:POL-FL-FNT-AZ
			//                   :IND-POL-FL-FNT-AZ
			//                  ,:POL-FL-FNT-ADER
			//                   :IND-POL-FL-FNT-ADER
			//                  ,:POL-FL-FNT-TFR
			//                   :IND-POL-FL-FNT-TFR
			//                  ,:POL-FL-FNT-VOLO
			//                   :IND-POL-FL-FNT-VOLO
			//                  ,:POL-TP-OPZ-A-SCAD
			//                   :IND-POL-TP-OPZ-A-SCAD
			//                  ,:POL-AA-DIFF-PROR-DFLT
			//                   :IND-POL-AA-DIFF-PROR-DFLT
			//                  ,:POL-FL-VER-PROD
			//                   :IND-POL-FL-VER-PROD
			//                  ,:POL-DUR-GG
			//                   :IND-POL-DUR-GG
			//                  ,:POL-DIR-QUIET
			//                   :IND-POL-DIR-QUIET
			//                  ,:POL-TP-PTF-ESTNO
			//                   :IND-POL-TP-PTF-ESTNO
			//                  ,:POL-FL-CUM-PRE-CNTR
			//                   :IND-POL-FL-CUM-PRE-CNTR
			//                  ,:POL-FL-AMMB-MOVI
			//                   :IND-POL-FL-AMMB-MOVI
			//                  ,:POL-CONV-GECO
			//                   :IND-POL-CONV-GECO
			//                  ,:POL-DS-RIGA
			//                  ,:POL-DS-OPER-SQL
			//                  ,:POL-DS-VER
			//                  ,:POL-DS-TS-INI-CPTZ
			//                  ,:POL-DS-TS-END-CPTZ
			//                  ,:POL-DS-UTENTE
			//                  ,:POL-DS-STATO-ELAB
			//                  ,:POL-FL-SCUDO-FISC
			//                   :IND-POL-FL-SCUDO-FISC
			//                  ,:POL-FL-TRASFE
			//                   :IND-POL-FL-TRASFE
			//                  ,:POL-FL-TFR-STRC
			//                   :IND-POL-FL-TFR-STRC
			//                  ,:POL-DT-PRESC-DB
			//                   :IND-POL-DT-PRESC
			//                  ,:POL-COD-CONV-AGG
			//                   :IND-POL-COD-CONV-AGG
			//                  ,:POL-SUBCAT-PROD
			//                   :IND-POL-SUBCAT-PROD
			//                  ,:POL-FL-QUEST-ADEGZ-ASS
			//                   :IND-POL-FL-QUEST-ADEGZ-ASS
			//                  ,:POL-COD-TPA
			//                   :IND-POL-COD-TPA
			//                  ,:POL-ID-ACC-COMM
			//                   :IND-POL-ID-ACC-COMM
			//                  ,:POL-FL-POLI-CPI-PR
			//                   :IND-POL-FL-POLI-CPI-PR
			//                  ,:POL-FL-POLI-BUNDLING
			//                   :IND-POL-FL-POLI-BUNDLING
			//                  ,:POL-IND-POLI-PRIN-COLL
			//                   :IND-POL-IND-POLI-PRIN-COLL
			//                  ,:POL-FL-VND-BUNDLE
			//                   :IND-POL-FL-VND-BUNDLE
			//                  ,:POL-IB-BS
			//                   :IND-POL-IB-BS
			//                  ,:POL-FL-POLI-IFP
			//                   :IND-POL-FL-POLI-IFP
			//                  ,:STB-ID-STAT-OGG-BUS
			//                  ,:STB-ID-OGG
			//                  ,:STB-TP-OGG
			//                  ,:STB-ID-MOVI-CRZ
			//                  ,:STB-ID-MOVI-CHIU
			//                   :IND-STB-ID-MOVI-CHIU
			//                  ,:STB-DT-INI-EFF-DB
			//                  ,:STB-DT-END-EFF-DB
			//                  ,:STB-COD-COMP-ANIA
			//                  ,:STB-TP-STAT-BUS
			//                  ,:STB-TP-CAUS
			//                  ,:STB-DS-RIGA
			//                  ,:STB-DS-OPER-SQL
			//                  ,:STB-DS-VER
			//                  ,:STB-DS-TS-INI-CPTZ
			//                  ,:STB-DS-TS-END-CPTZ
			//                  ,:STB-DS-UTENTE
			//                  ,:STB-DS-STATO-ELAB
			//           END-EXEC
			adesPoliStatOggBusTrchDiGarDao.fetchCurSc05(this.getAdesPoliStatOggBusTrchDiGarLdbm0250());
		}
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           ELSE
		//             END-IF
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			z100SetColonneNull();
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		} else if (idsv0003.getSqlcode().isNotFound()) {
			// COB_CODE: IF IDSV0003-NOT-FOUND
			//              END-IF
			//           END-IF
			// COB_CODE: PERFORM A370-CLOSE-CURSOR-SC05 THRU A370-SC05-EX
			a370CloseCursorSc05();
			// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
			//              SET IDSV0003-NOT-FOUND TO TRUE
			//           END-IF
			if (idsv0003.getSqlcode().isSuccessfulSql()) {
				// COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
				idsv0003.getSqlcode().setNotFound();
			}
		}
	}

	/**Original name: SC06-SELECTION-CURSOR-06<br>
	 * <pre>*****************************************************************
	 * *****************************************************************</pre>*/
	private void sc06SelectionCursor06() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-SELECT
		//                 PERFORM A310-SELECT-SC06           THRU A310-SC06-EX
		//              WHEN IDSV0003-OPEN-CURSOR
		//                 PERFORM A360-OPEN-CURSOR-SC06      THRU A360-SC06-EX
		//              WHEN IDSV0003-CLOSE-CURSOR
		//                 PERFORM A370-CLOSE-CURSOR-SC06     THRU A370-SC06-EX
		//              WHEN IDSV0003-FETCH-FIRST
		//                 PERFORM A380-FETCH-FIRST-SC06      THRU A380-SC06-EX
		//              WHEN IDSV0003-FETCH-NEXT
		//                 PERFORM A390-FETCH-NEXT-SC06       THRU A390-SC06-EX
		//              WHEN IDSV0003-UPDATE
		//                 PERFORM A320-UPDATE-SC06           THRU A320-SC06-EX
		//              WHEN OTHER
		//                 SET IDSV0003-INVALID-OPER          TO TRUE
		//           END-EVALUATE.
		if (idsv0003.getOperazione().isSelect()) {
			// COB_CODE: PERFORM A310-SELECT-SC06           THRU A310-SC06-EX
			a310SelectSc06();
		} else if (idsv0003.getOperazione().isOpenCursor()) {
			// COB_CODE: PERFORM A360-OPEN-CURSOR-SC06      THRU A360-SC06-EX
			a360OpenCursorSc06();
		} else if (idsv0003.getOperazione().isCloseCursor()) {
			// COB_CODE: PERFORM A370-CLOSE-CURSOR-SC06     THRU A370-SC06-EX
			a370CloseCursorSc06();
		} else if (idsv0003.getOperazione().isFetchFirst()) {
			// COB_CODE: PERFORM A380-FETCH-FIRST-SC06      THRU A380-SC06-EX
			a380FetchFirstSc06();
		} else if (idsv0003.getOperazione().isFetchNext()) {
			// COB_CODE: PERFORM A390-FETCH-NEXT-SC06       THRU A390-SC06-EX
			a390FetchNextSc06();
		} else if (idsv0003.getOperazione().isUpdate()) {
			// COB_CODE: PERFORM A320-UPDATE-SC06           THRU A320-SC06-EX
			a320UpdateSc06();
		} else {
			// COB_CODE: SET IDSV0003-INVALID-OPER          TO TRUE
			idsv0003.getReturnCode().setInvalidOper();
		}
	}

	/**Original name: A305-DECLARE-CURSOR-SC06<br>
	 * <pre>*****************************************************************</pre>*/
	private void a305DeclareCursorSc06() {
		// COB_CODE:      EXEC SQL
		//                     DECLARE CUR-SC06 CURSOR WITH HOLD FOR
		//                  SELECT
		//                         A.ID_ADES
		//                        ,A.ID_POLI
		//                        ,A.ID_MOVI_CRZ
		//                        ,A.ID_MOVI_CHIU
		//                        ,A.DT_INI_EFF
		//                        ,A.DT_END_EFF
		//                        ,A.IB_PREV
		//                        ,A.IB_OGG
		//                        ,A.COD_COMP_ANIA
		//                        ,A.DT_DECOR
		//                        ,A.DT_SCAD
		//                        ,A.ETA_A_SCAD
		//                        ,A.DUR_AA
		//                        ,A.DUR_MM
		//                        ,A.DUR_GG
		//                        ,A.TP_RGM_FISC
		//                        ,A.TP_RIAT
		//                        ,A.TP_MOD_PAG_TIT
		//                        ,A.TP_IAS
		//                        ,A.DT_VARZ_TP_IAS
		//                        ,A.PRE_NET_IND
		//                        ,A.PRE_LRD_IND
		//                        ,A.RAT_LRD_IND
		//                        ,A.PRSTZ_INI_IND
		//                        ,A.FL_COINC_ASSTO
		//                        ,A.IB_DFLT
		//                        ,A.MOD_CALC
		//                        ,A.TP_FNT_CNBTVA
		//                        ,A.IMP_AZ
		//                        ,A.IMP_ADER
		//                        ,A.IMP_TFR
		//                        ,A.IMP_VOLO
		//                        ,A.PC_AZ
		//                        ,A.PC_ADER
		//                        ,A.PC_TFR
		//                        ,A.PC_VOLO
		//                        ,A.DT_NOVA_RGM_FISC
		//                        ,A.FL_ATTIV
		//                        ,A.IMP_REC_RIT_VIS
		//                        ,A.IMP_REC_RIT_ACC
		//                        ,A.FL_VARZ_STAT_TBGC
		//                        ,A.FL_PROVZA_MIGRAZ
		//                        ,A.IMPB_VIS_DA_REC
		//                        ,A.DT_DECOR_PREST_BAN
		//                        ,A.DT_EFF_VARZ_STAT_T
		//                        ,A.DS_RIGA
		//                        ,A.DS_OPER_SQL
		//                        ,A.DS_VER
		//                        ,A.DS_TS_INI_CPTZ
		//                        ,A.DS_TS_END_CPTZ
		//                        ,A.DS_UTENTE
		//                        ,A.DS_STATO_ELAB
		//                        ,A.CUM_CNBT_CAP
		//                        ,A.IMP_GAR_CNBT
		//                        ,A.DT_ULT_CONS_CNBT
		//                        ,A.IDEN_ISC_FND
		//                        ,A.NUM_RAT_PIAN
		//                        ,A.DT_PRESC
		//                        ,A.CONCS_PREST
		//                        ,B.ID_POLI
		//                        ,B.ID_MOVI_CRZ
		//                        ,B.ID_MOVI_CHIU
		//                        ,B.IB_OGG
		//                        ,B.IB_PROP
		//                        ,B.DT_PROP
		//                        ,B.DT_INI_EFF
		//                        ,B.DT_END_EFF
		//                        ,B.COD_COMP_ANIA
		//                        ,B.DT_DECOR
		//                        ,B.DT_EMIS
		//                        ,B.TP_POLI
		//                        ,B.DUR_AA
		//                        ,B.DUR_MM
		//                        ,B.DT_SCAD
		//                        ,B.COD_PROD
		//                        ,B.DT_INI_VLDT_PROD
		//                        ,B.COD_CONV
		//                        ,B.COD_RAMO
		//                        ,B.DT_INI_VLDT_CONV
		//                        ,B.DT_APPLZ_CONV
		//                        ,B.TP_FRM_ASSVA
		//                        ,B.TP_RGM_FISC
		//                        ,B.FL_ESTAS
		//                        ,B.FL_RSH_COMUN
		//                        ,B.FL_RSH_COMUN_COND
		//                        ,B.TP_LIV_GENZ_TIT
		//                        ,B.FL_COP_FINANZ
		//                        ,B.TP_APPLZ_DIR
		//                        ,B.SPE_MED
		//                        ,B.DIR_EMIS
		//                        ,B.DIR_1O_VERS
		//                        ,B.DIR_VERS_AGG
		//                        ,B.COD_DVS
		//                        ,B.FL_FNT_AZ
		//                        ,B.FL_FNT_ADER
		//                        ,B.FL_FNT_TFR
		//                        ,B.FL_FNT_VOLO
		//                        ,B.TP_OPZ_A_SCAD
		//                        ,B.AA_DIFF_PROR_DFLT
		//                        ,B.FL_VER_PROD
		//                        ,B.DUR_GG
		//                        ,B.DIR_QUIET
		//                        ,B.TP_PTF_ESTNO
		//                        ,B.FL_CUM_PRE_CNTR
		//                        ,B.FL_AMMB_MOVI
		//                        ,B.CONV_GECO
		//                        ,B.DS_RIGA
		//                        ,B.DS_OPER_SQL
		//                        ,B.DS_VER
		//                        ,B.DS_TS_INI_CPTZ
		//                        ,B.DS_TS_END_CPTZ
		//                        ,B.DS_UTENTE
		//                        ,B.DS_STATO_ELAB
		//                        ,B.FL_SCUDO_FISC
		//                        ,B.FL_TRASFE
		//                        ,B.FL_TFR_STRC
		//                        ,B.DT_PRESC
		//                        ,B.COD_CONV_AGG
		//                        ,B.SUBCAT_PROD
		//                        ,B.FL_QUEST_ADEGZ_ASS
		//                        ,B.COD_TPA
		//                        ,B.ID_ACC_COMM
		//                        ,B.FL_POLI_CPI_PR
		//                        ,B.FL_POLI_BUNDLING
		//                        ,B.IND_POLI_PRIN_COLL
		//                        ,B.FL_VND_BUNDLE
		//                        ,B.IB_BS
		//                        ,B.FL_POLI_IFP
		//                        ,C.ID_STAT_OGG_BUS
		//                        ,C.ID_OGG
		//                        ,C.TP_OGG
		//                        ,C.ID_MOVI_CRZ
		//                        ,C.ID_MOVI_CHIU
		//                        ,C.DT_INI_EFF
		//                        ,C.DT_END_EFF
		//                        ,C.COD_COMP_ANIA
		//                        ,C.TP_STAT_BUS
		//                        ,C.TP_CAUS
		//                        ,C.DS_RIGA
		//                        ,C.DS_OPER_SQL
		//                        ,C.DS_VER
		//                        ,C.DS_TS_INI_CPTZ
		//                        ,C.DS_TS_END_CPTZ
		//                        ,C.DS_UTENTE
		//                        ,C.DS_STATO_ELAB
		//                  FROM ADES      A,
		//                       POLI      B,
		//                       STAT_OGG_BUS C
		//                  WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
		//                                             :WS-TP-FRM-ASSVA2 )
		//                    AND B.COD_PROD     =  :WLB-COD-PROD
		//                    AND A.ID_POLI      =   B.ID_POLI
		//                    AND A.ID_ADES      =   C.ID_OGG
		//                    AND C.TP_OGG       =  'AD'
		//           *--  D C.TP_STAT_BUS     =  'VI'
		//                    AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
		//                    AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
		//                    AND A.DT_END_EFF  >   :WS-DT-PTF-X
		//                    AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                    AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                    AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
		//                    AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
		//                    AND B.DT_END_EFF  >   :WS-DT-PTF-X
		//                    AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                    AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                    AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
		//                    AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
		//                    AND C.DT_END_EFF  >   :WS-DT-PTF-X
		//                    AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                    AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                    AND A.ID_ADES IN
		//                       (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
		//                                                        STAT_OGG_BUS E
		//                        WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
		//                        AND E.TP_OGG = 'TG'
		//                        AND E.TP_STAT_BUS IN ('VI','ST')
		//                        AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
		//                        AND D.COD_COMP_ANIA =
		//                            :IDSV0003-CODICE-COMPAGNIA-ANIA
		//                        AND D.DT_INI_EFF <=   :WS-DT-PTF-X
		//                        AND D.DT_END_EFF >    :WS-DT-PTF-X
		//                        AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                        AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                        AND E.DT_INI_EFF <=   :WS-DT-PTF-X
		//                        AND E.DT_END_EFF >    :WS-DT-PTF-X
		//                        AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                        AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                        AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
		//                             OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
		//                        )
		//                    AND A.DS_STATO_ELAB IN (
		//                                            :IABV0002-STATE-01,
		//                                            :IABV0002-STATE-02,
		//                                            :IABV0002-STATE-03,
		//                                            :IABV0002-STATE-04,
		//                                            :IABV0002-STATE-05,
		//                                            :IABV0002-STATE-06,
		//                                            :IABV0002-STATE-07,
		//                                            :IABV0002-STATE-08,
		//                                            :IABV0002-STATE-09,
		//                                            :IABV0002-STATE-10
		//                                              )
		//                    AND NOT A.DS_VER  = :IABV0009-VERSIONING
		//                    ORDER BY A.ID_POLI, A.ID_ADES
		//                END-EXEC.
		// DECLARE CURSOR doesn't need a translation;
	}

	/**Original name: A310-SELECT-SC06<br>
	 * <pre>*****************************************************************</pre>*/
	private void a310SelectSc06() {
		// COB_CODE:      EXEC SQL
		//                  SELECT
		//                         A.ID_ADES
		//                        ,A.ID_POLI
		//                        ,A.ID_MOVI_CRZ
		//                        ,A.ID_MOVI_CHIU
		//                        ,A.DT_INI_EFF
		//                        ,A.DT_END_EFF
		//                        ,A.IB_PREV
		//                        ,A.IB_OGG
		//                        ,A.COD_COMP_ANIA
		//                        ,A.DT_DECOR
		//                        ,A.DT_SCAD
		//                        ,A.ETA_A_SCAD
		//                        ,A.DUR_AA
		//                        ,A.DUR_MM
		//                        ,A.DUR_GG
		//                        ,A.TP_RGM_FISC
		//                        ,A.TP_RIAT
		//                        ,A.TP_MOD_PAG_TIT
		//                        ,A.TP_IAS
		//                        ,A.DT_VARZ_TP_IAS
		//                        ,A.PRE_NET_IND
		//                        ,A.PRE_LRD_IND
		//                        ,A.RAT_LRD_IND
		//                        ,A.PRSTZ_INI_IND
		//                        ,A.FL_COINC_ASSTO
		//                        ,A.IB_DFLT
		//                        ,A.MOD_CALC
		//                        ,A.TP_FNT_CNBTVA
		//                        ,A.IMP_AZ
		//                        ,A.IMP_ADER
		//                        ,A.IMP_TFR
		//                        ,A.IMP_VOLO
		//                        ,A.PC_AZ
		//                        ,A.PC_ADER
		//                        ,A.PC_TFR
		//                        ,A.PC_VOLO
		//                        ,A.DT_NOVA_RGM_FISC
		//                        ,A.FL_ATTIV
		//                        ,A.IMP_REC_RIT_VIS
		//                        ,A.IMP_REC_RIT_ACC
		//                        ,A.FL_VARZ_STAT_TBGC
		//                        ,A.FL_PROVZA_MIGRAZ
		//                        ,A.IMPB_VIS_DA_REC
		//                        ,A.DT_DECOR_PREST_BAN
		//                        ,A.DT_EFF_VARZ_STAT_T
		//                        ,A.DS_RIGA
		//                        ,A.DS_OPER_SQL
		//                        ,A.DS_VER
		//                        ,A.DS_TS_INI_CPTZ
		//                        ,A.DS_TS_END_CPTZ
		//                        ,A.DS_UTENTE
		//                        ,A.DS_STATO_ELAB
		//                        ,A.CUM_CNBT_CAP
		//                        ,A.IMP_GAR_CNBT
		//                        ,A.DT_ULT_CONS_CNBT
		//                        ,A.IDEN_ISC_FND
		//                        ,A.NUM_RAT_PIAN
		//                        ,A.DT_PRESC
		//                        ,A.CONCS_PREST
		//                        ,B.ID_POLI
		//                        ,B.ID_MOVI_CRZ
		//                        ,B.ID_MOVI_CHIU
		//                        ,B.IB_OGG
		//                        ,B.IB_PROP
		//                        ,B.DT_PROP
		//                        ,B.DT_INI_EFF
		//                        ,B.DT_END_EFF
		//                        ,B.COD_COMP_ANIA
		//                        ,B.DT_DECOR
		//                        ,B.DT_EMIS
		//                        ,B.TP_POLI
		//                        ,B.DUR_AA
		//                        ,B.DUR_MM
		//                        ,B.DT_SCAD
		//                        ,B.COD_PROD
		//                        ,B.DT_INI_VLDT_PROD
		//                        ,B.COD_CONV
		//                        ,B.COD_RAMO
		//                        ,B.DT_INI_VLDT_CONV
		//                        ,B.DT_APPLZ_CONV
		//                        ,B.TP_FRM_ASSVA
		//                        ,B.TP_RGM_FISC
		//                        ,B.FL_ESTAS
		//                        ,B.FL_RSH_COMUN
		//                        ,B.FL_RSH_COMUN_COND
		//                        ,B.TP_LIV_GENZ_TIT
		//                        ,B.FL_COP_FINANZ
		//                        ,B.TP_APPLZ_DIR
		//                        ,B.SPE_MED
		//                        ,B.DIR_EMIS
		//                        ,B.DIR_1O_VERS
		//                        ,B.DIR_VERS_AGG
		//                        ,B.COD_DVS
		//                        ,B.FL_FNT_AZ
		//                        ,B.FL_FNT_ADER
		//                        ,B.FL_FNT_TFR
		//                        ,B.FL_FNT_VOLO
		//                        ,B.TP_OPZ_A_SCAD
		//                        ,B.AA_DIFF_PROR_DFLT
		//                        ,B.FL_VER_PROD
		//                        ,B.DUR_GG
		//                        ,B.DIR_QUIET
		//                        ,B.TP_PTF_ESTNO
		//                        ,B.FL_CUM_PRE_CNTR
		//                        ,B.FL_AMMB_MOVI
		//                        ,B.CONV_GECO
		//                        ,B.DS_RIGA
		//                        ,B.DS_OPER_SQL
		//                        ,B.DS_VER
		//                        ,B.DS_TS_INI_CPTZ
		//                        ,B.DS_TS_END_CPTZ
		//                        ,B.DS_UTENTE
		//                        ,B.DS_STATO_ELAB
		//                        ,B.FL_SCUDO_FISC
		//                        ,B.FL_TRASFE
		//                        ,B.FL_TFR_STRC
		//                        ,B.DT_PRESC
		//                        ,B.COD_CONV_AGG
		//                        ,B.SUBCAT_PROD
		//                        ,B.FL_QUEST_ADEGZ_ASS
		//                        ,B.COD_TPA
		//                        ,B.ID_ACC_COMM
		//                        ,B.FL_POLI_CPI_PR
		//                        ,B.FL_POLI_BUNDLING
		//                        ,B.IND_POLI_PRIN_COLL
		//                        ,B.FL_VND_BUNDLE
		//                        ,B.IB_BS
		//                        ,B.FL_POLI_IFP
		//                        ,C.ID_STAT_OGG_BUS
		//                        ,C.ID_OGG
		//                        ,C.TP_OGG
		//                        ,C.ID_MOVI_CRZ
		//                        ,C.ID_MOVI_CHIU
		//                        ,C.DT_INI_EFF
		//                        ,C.DT_END_EFF
		//                        ,C.COD_COMP_ANIA
		//                        ,C.TP_STAT_BUS
		//                        ,C.TP_CAUS
		//                        ,C.DS_RIGA
		//                        ,C.DS_OPER_SQL
		//                        ,C.DS_VER
		//                        ,C.DS_TS_INI_CPTZ
		//                        ,C.DS_TS_END_CPTZ
		//                        ,C.DS_UTENTE
		//                        ,C.DS_STATO_ELAB
		//                  INTO
		//                        :ADE-ID-ADES
		//                       ,:ADE-ID-POLI
		//                       ,:ADE-ID-MOVI-CRZ
		//                       ,:ADE-ID-MOVI-CHIU
		//                        :IND-ADE-ID-MOVI-CHIU
		//                       ,:ADE-DT-INI-EFF-DB
		//                       ,:ADE-DT-END-EFF-DB
		//                       ,:ADE-IB-PREV
		//                        :IND-ADE-IB-PREV
		//                       ,:ADE-IB-OGG
		//                        :IND-ADE-IB-OGG
		//                       ,:ADE-COD-COMP-ANIA
		//                       ,:ADE-DT-DECOR-DB
		//                        :IND-ADE-DT-DECOR
		//                       ,:ADE-DT-SCAD-DB
		//                        :IND-ADE-DT-SCAD
		//                       ,:ADE-ETA-A-SCAD
		//                        :IND-ADE-ETA-A-SCAD
		//                       ,:ADE-DUR-AA
		//                        :IND-ADE-DUR-AA
		//                       ,:ADE-DUR-MM
		//                        :IND-ADE-DUR-MM
		//                       ,:ADE-DUR-GG
		//                        :IND-ADE-DUR-GG
		//                       ,:ADE-TP-RGM-FISC
		//                       ,:ADE-TP-RIAT
		//                        :IND-ADE-TP-RIAT
		//                       ,:ADE-TP-MOD-PAG-TIT
		//                       ,:ADE-TP-IAS
		//                        :IND-ADE-TP-IAS
		//                       ,:ADE-DT-VARZ-TP-IAS-DB
		//                        :IND-ADE-DT-VARZ-TP-IAS
		//                       ,:ADE-PRE-NET-IND
		//                        :IND-ADE-PRE-NET-IND
		//                       ,:ADE-PRE-LRD-IND
		//                        :IND-ADE-PRE-LRD-IND
		//                       ,:ADE-RAT-LRD-IND
		//                        :IND-ADE-RAT-LRD-IND
		//                       ,:ADE-PRSTZ-INI-IND
		//                        :IND-ADE-PRSTZ-INI-IND
		//                       ,:ADE-FL-COINC-ASSTO
		//                        :IND-ADE-FL-COINC-ASSTO
		//                       ,:ADE-IB-DFLT
		//                        :IND-ADE-IB-DFLT
		//                       ,:ADE-MOD-CALC
		//                        :IND-ADE-MOD-CALC
		//                       ,:ADE-TP-FNT-CNBTVA
		//                        :IND-ADE-TP-FNT-CNBTVA
		//                       ,:ADE-IMP-AZ
		//                        :IND-ADE-IMP-AZ
		//                       ,:ADE-IMP-ADER
		//                        :IND-ADE-IMP-ADER
		//                       ,:ADE-IMP-TFR
		//                        :IND-ADE-IMP-TFR
		//                       ,:ADE-IMP-VOLO
		//                        :IND-ADE-IMP-VOLO
		//                       ,:ADE-PC-AZ
		//                        :IND-ADE-PC-AZ
		//                       ,:ADE-PC-ADER
		//                        :IND-ADE-PC-ADER
		//                       ,:ADE-PC-TFR
		//                        :IND-ADE-PC-TFR
		//                       ,:ADE-PC-VOLO
		//                        :IND-ADE-PC-VOLO
		//                       ,:ADE-DT-NOVA-RGM-FISC-DB
		//                        :IND-ADE-DT-NOVA-RGM-FISC
		//                       ,:ADE-FL-ATTIV
		//                        :IND-ADE-FL-ATTIV
		//                       ,:ADE-IMP-REC-RIT-VIS
		//                        :IND-ADE-IMP-REC-RIT-VIS
		//                       ,:ADE-IMP-REC-RIT-ACC
		//                        :IND-ADE-IMP-REC-RIT-ACC
		//                       ,:ADE-FL-VARZ-STAT-TBGC
		//                        :IND-ADE-FL-VARZ-STAT-TBGC
		//                       ,:ADE-FL-PROVZA-MIGRAZ
		//                        :IND-ADE-FL-PROVZA-MIGRAZ
		//                       ,:ADE-IMPB-VIS-DA-REC
		//                        :IND-ADE-IMPB-VIS-DA-REC
		//                       ,:ADE-DT-DECOR-PREST-BAN-DB
		//                        :IND-ADE-DT-DECOR-PREST-BAN
		//                       ,:ADE-DT-EFF-VARZ-STAT-T-DB
		//                        :IND-ADE-DT-EFF-VARZ-STAT-T
		//                       ,:ADE-DS-RIGA
		//                       ,:ADE-DS-OPER-SQL
		//                       ,:ADE-DS-VER
		//                       ,:ADE-DS-TS-INI-CPTZ
		//                       ,:ADE-DS-TS-END-CPTZ
		//                       ,:ADE-DS-UTENTE
		//                       ,:ADE-DS-STATO-ELAB
		//                       ,:ADE-CUM-CNBT-CAP
		//                        :IND-ADE-CUM-CNBT-CAP
		//                       ,:ADE-IMP-GAR-CNBT
		//                        :IND-ADE-IMP-GAR-CNBT
		//                       ,:ADE-DT-ULT-CONS-CNBT-DB
		//                        :IND-ADE-DT-ULT-CONS-CNBT
		//                       ,:ADE-IDEN-ISC-FND
		//                        :IND-ADE-IDEN-ISC-FND
		//                       ,:ADE-NUM-RAT-PIAN
		//                        :IND-ADE-NUM-RAT-PIAN
		//                       ,:ADE-DT-PRESC-DB
		//                        :IND-ADE-DT-PRESC
		//                       ,:ADE-CONCS-PREST
		//                        :IND-ADE-CONCS-PREST
		//                       ,:POL-ID-POLI
		//                       ,:POL-ID-MOVI-CRZ
		//                       ,:POL-ID-MOVI-CHIU
		//                        :IND-POL-ID-MOVI-CHIU
		//                       ,:POL-IB-OGG
		//                        :IND-POL-IB-OGG
		//                       ,:POL-IB-PROP
		//                       ,:POL-DT-PROP-DB
		//                        :IND-POL-DT-PROP
		//                       ,:POL-DT-INI-EFF-DB
		//                       ,:POL-DT-END-EFF-DB
		//                       ,:POL-COD-COMP-ANIA
		//                       ,:POL-DT-DECOR-DB
		//                       ,:POL-DT-EMIS-DB
		//                       ,:POL-TP-POLI
		//                       ,:POL-DUR-AA
		//                        :IND-POL-DUR-AA
		//                       ,:POL-DUR-MM
		//                        :IND-POL-DUR-MM
		//                       ,:POL-DT-SCAD-DB
		//                        :IND-POL-DT-SCAD
		//                       ,:POL-COD-PROD
		//                       ,:POL-DT-INI-VLDT-PROD-DB
		//                       ,:POL-COD-CONV
		//                        :IND-POL-COD-CONV
		//                       ,:POL-COD-RAMO
		//                        :IND-POL-COD-RAMO
		//                       ,:POL-DT-INI-VLDT-CONV-DB
		//                        :IND-POL-DT-INI-VLDT-CONV
		//                       ,:POL-DT-APPLZ-CONV-DB
		//                        :IND-POL-DT-APPLZ-CONV
		//                       ,:POL-TP-FRM-ASSVA
		//                       ,:POL-TP-RGM-FISC
		//                        :IND-POL-TP-RGM-FISC
		//                       ,:POL-FL-ESTAS
		//                        :IND-POL-FL-ESTAS
		//                       ,:POL-FL-RSH-COMUN
		//                        :IND-POL-FL-RSH-COMUN
		//                       ,:POL-FL-RSH-COMUN-COND
		//                        :IND-POL-FL-RSH-COMUN-COND
		//                       ,:POL-TP-LIV-GENZ-TIT
		//                       ,:POL-FL-COP-FINANZ
		//                        :IND-POL-FL-COP-FINANZ
		//                       ,:POL-TP-APPLZ-DIR
		//                        :IND-POL-TP-APPLZ-DIR
		//                       ,:POL-SPE-MED
		//                        :IND-POL-SPE-MED
		//                       ,:POL-DIR-EMIS
		//                        :IND-POL-DIR-EMIS
		//                       ,:POL-DIR-1O-VERS
		//                        :IND-POL-DIR-1O-VERS
		//                       ,:POL-DIR-VERS-AGG
		//                        :IND-POL-DIR-VERS-AGG
		//                       ,:POL-COD-DVS
		//                        :IND-POL-COD-DVS
		//                       ,:POL-FL-FNT-AZ
		//                        :IND-POL-FL-FNT-AZ
		//                       ,:POL-FL-FNT-ADER
		//                        :IND-POL-FL-FNT-ADER
		//                       ,:POL-FL-FNT-TFR
		//                        :IND-POL-FL-FNT-TFR
		//                       ,:POL-FL-FNT-VOLO
		//                        :IND-POL-FL-FNT-VOLO
		//                       ,:POL-TP-OPZ-A-SCAD
		//                        :IND-POL-TP-OPZ-A-SCAD
		//                       ,:POL-AA-DIFF-PROR-DFLT
		//                        :IND-POL-AA-DIFF-PROR-DFLT
		//                       ,:POL-FL-VER-PROD
		//                        :IND-POL-FL-VER-PROD
		//                       ,:POL-DUR-GG
		//                        :IND-POL-DUR-GG
		//                       ,:POL-DIR-QUIET
		//                        :IND-POL-DIR-QUIET
		//                       ,:POL-TP-PTF-ESTNO
		//                        :IND-POL-TP-PTF-ESTNO
		//                       ,:POL-FL-CUM-PRE-CNTR
		//                        :IND-POL-FL-CUM-PRE-CNTR
		//                       ,:POL-FL-AMMB-MOVI
		//                        :IND-POL-FL-AMMB-MOVI
		//                       ,:POL-CONV-GECO
		//                        :IND-POL-CONV-GECO
		//                       ,:POL-DS-RIGA
		//                       ,:POL-DS-OPER-SQL
		//                       ,:POL-DS-VER
		//                       ,:POL-DS-TS-INI-CPTZ
		//                       ,:POL-DS-TS-END-CPTZ
		//                       ,:POL-DS-UTENTE
		//                       ,:POL-DS-STATO-ELAB
		//                       ,:POL-FL-SCUDO-FISC
		//                        :IND-POL-FL-SCUDO-FISC
		//                       ,:POL-FL-TRASFE
		//                        :IND-POL-FL-TRASFE
		//                       ,:POL-FL-TFR-STRC
		//                        :IND-POL-FL-TFR-STRC
		//                       ,:POL-DT-PRESC-DB
		//                        :IND-POL-DT-PRESC
		//                       ,:POL-COD-CONV-AGG
		//                        :IND-POL-COD-CONV-AGG
		//                       ,:POL-SUBCAT-PROD
		//                        :IND-POL-SUBCAT-PROD
		//                       ,:POL-FL-QUEST-ADEGZ-ASS
		//                        :IND-POL-FL-QUEST-ADEGZ-ASS
		//                       ,:POL-COD-TPA
		//                        :IND-POL-COD-TPA
		//                       ,:POL-ID-ACC-COMM
		//                        :IND-POL-ID-ACC-COMM
		//                       ,:POL-FL-POLI-CPI-PR
		//                        :IND-POL-FL-POLI-CPI-PR
		//                       ,:POL-FL-POLI-BUNDLING
		//                        :IND-POL-FL-POLI-BUNDLING
		//                       ,:POL-IND-POLI-PRIN-COLL
		//                        :IND-POL-IND-POLI-PRIN-COLL
		//                       ,:POL-FL-VND-BUNDLE
		//                        :IND-POL-FL-VND-BUNDLE
		//                       ,:POL-IB-BS
		//                        :IND-POL-IB-BS
		//                       ,:POL-FL-POLI-IFP
		//                        :IND-POL-FL-POLI-IFP
		//                       ,:STB-ID-STAT-OGG-BUS
		//                       ,:STB-ID-OGG
		//                       ,:STB-TP-OGG
		//                       ,:STB-ID-MOVI-CRZ
		//                       ,:STB-ID-MOVI-CHIU
		//                        :IND-STB-ID-MOVI-CHIU
		//                       ,:STB-DT-INI-EFF-DB
		//                       ,:STB-DT-END-EFF-DB
		//                       ,:STB-COD-COMP-ANIA
		//                       ,:STB-TP-STAT-BUS
		//                       ,:STB-TP-CAUS
		//                       ,:STB-DS-RIGA
		//                       ,:STB-DS-OPER-SQL
		//                       ,:STB-DS-VER
		//                       ,:STB-DS-TS-INI-CPTZ
		//                       ,:STB-DS-TS-END-CPTZ
		//                       ,:STB-DS-UTENTE
		//                       ,:STB-DS-STATO-ELAB
		//                  FROM ADES      A,
		//                       POLI      B,
		//                       STAT_OGG_BUS C
		//                  WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
		//                                             :WS-TP-FRM-ASSVA2 )
		//                    AND B.COD_PROD     =  :WLB-COD-PROD
		//                    AND A.ID_POLI      =   B.ID_POLI
		//                    AND A.ID_ADES      =   C.ID_OGG
		//                    AND C.TP_OGG       =  'AD'
		//           *--  D C.TP_STAT_BUS     =  'VI'
		//                    AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
		//                    AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
		//                    AND A.DT_END_EFF  >   :WS-DT-PTF-X
		//                    AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                    AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                    AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
		//                    AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
		//                    AND B.DT_END_EFF  >   :WS-DT-PTF-X
		//                    AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                    AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                    AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
		//                    AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
		//                    AND C.DT_END_EFF  >   :WS-DT-PTF-X
		//                    AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                    AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                    AND A.ID_ADES IN
		//                       (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
		//                                                        STAT_OGG_BUS E
		//                        WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
		//                        AND E.TP_OGG = 'TG'
		//                        AND E.TP_STAT_BUS IN ('VI','ST')
		//                        AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
		//                        AND D.COD_COMP_ANIA =
		//                            :IDSV0003-CODICE-COMPAGNIA-ANIA
		//                        AND D.DT_INI_EFF <=   :WS-DT-PTF-X
		//                        AND D.DT_END_EFF >    :WS-DT-PTF-X
		//                        AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                        AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                        AND E.DT_INI_EFF <=   :WS-DT-PTF-X
		//                        AND E.DT_END_EFF >    :WS-DT-PTF-X
		//                        AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                        AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                        AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
		//                             OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
		//                        )
		//                    AND A.DS_STATO_ELAB IN (
		//                                            :IABV0002-STATE-01,
		//                                            :IABV0002-STATE-02,
		//                                            :IABV0002-STATE-03,
		//                                            :IABV0002-STATE-04,
		//                                            :IABV0002-STATE-05,
		//                                            :IABV0002-STATE-06,
		//                                            :IABV0002-STATE-07,
		//                                            :IABV0002-STATE-08,
		//                                            :IABV0002-STATE-09,
		//                                            :IABV0002-STATE-10
		//                                              )
		//                    AND NOT A.DS_VER  = :IABV0009-VERSIONING
		//                    ORDER BY A.ID_POLI, A.ID_ADES
		//                   FETCH FIRST ROW ONLY
		//                END-EXEC.
		adesPoliStatOggBusTrchDiGarDao.selectRec2(this.getAdesPoliStatOggBusTrchDiGarLdbm02501());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			z100SetColonneNull();
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		}
	}

	/**Original name: A320-UPDATE-SC06<br>
	 * <pre>*****************************************************************</pre>*/
	private void a320UpdateSc06() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-PRIMARY-KEY
		//                   PERFORM A330-UPDATE-PK-SC06         THRU A330-SC06-EX
		//              WHEN IDSV0003-WHERE-CONDITION-06
		//                   PERFORM A340-UPDATE-WHERE-COND-SC06 THRU A340-SC06-EX
		//              WHEN IDSV0003-FIRST-ACTION
		//                                                       THRU A345-SC06-EX
		//              WHEN OTHER
		//                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
		//           END-EVALUATE.
		if (idsv0003.getLivelloOperazione().isIdsv0003PrimaryKey()) {
			// COB_CODE: PERFORM A330-UPDATE-PK-SC06         THRU A330-SC06-EX
			a330UpdatePkSc06();
		} else if (idsv0003.getTipologiaOperazione().isIdsv0003WhereCondition06()) {
			// COB_CODE: PERFORM A340-UPDATE-WHERE-COND-SC06 THRU A340-SC06-EX
			a340UpdateWhereCondSc06();
		} else if (idsv0003.getLivelloOperazione().isIdsv0003FirstAction()) {
			// COB_CODE: PERFORM A345-UPDATE-FIRST-ACTION-SC06
			//                                               THRU A345-SC06-EX
			a345UpdateFirstActionSc06();
		} else {
			// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
			idsv0003.getReturnCode().setInvalidLevelOper();
		}
	}

	/**Original name: A330-UPDATE-PK-SC06<br>
	 * <pre>*****************************************************************</pre>*/
	private void a330UpdatePkSc06() {
		// COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
		z150ValorizzaDataServices();
		// COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
		z200SetIndicatoriNull();
		// COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBM0250.cbl:line=8285, because the code is unreachable.
		// COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
		z960LengthVchar();
		// COB_CODE: EXEC SQL
		//                UPDATE ADES
		//                SET
		//                   DS_OPER_SQL            = :ADE-DS-OPER-SQL
		//                  ,DS_VER                 = :IABV0009-VERSIONING
		//                  ,DS_UTENTE              = :ADE-DS-UTENTE
		//                  ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
		//                WHERE             DS_RIGA = :ADE-DS-RIGA
		//           END-EXEC.
		adesDao.updateRec3(this);
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A340-UPDATE-WHERE-COND-SC06<br>
	 * <pre>*****************************************************************</pre>*/
	private void a340UpdateWhereCondSc06() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A345-UPDATE-FIRST-ACTION-SC06<br>
	 * <pre>*****************************************************************</pre>*/
	private void a345UpdateFirstActionSc06() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A360-OPEN-CURSOR-SC06<br>
	 * <pre>*****************************************************************</pre>*/
	private void a360OpenCursorSc06() {
		// COB_CODE: PERFORM A305-DECLARE-CURSOR-SC06 THRU A305-SC06-EX.
		a305DeclareCursorSc06();
		// COB_CODE: EXEC SQL
		//                OPEN CUR-SC06
		//           END-EXEC.
		adesPoliStatOggBusTrchDiGarDao.openCurSc06(this.getAdesPoliStatOggBusTrchDiGarLdbm02501());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A370-CLOSE-CURSOR-SC06<br>
	 * <pre>*****************************************************************</pre>*/
	private void a370CloseCursorSc06() {
		// COB_CODE: EXEC SQL
		//                CLOSE CUR-SC06
		//           END-EXEC.
		adesPoliStatOggBusTrchDiGarDao.closeCurSc06();
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A380-FETCH-FIRST-SC06<br>
	 * <pre>*****************************************************************</pre>*/
	private void a380FetchFirstSc06() {
		// COB_CODE: PERFORM A360-OPEN-CURSOR-SC06    THRU A360-SC06-EX.
		a360OpenCursorSc06();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM A390-FETCH-NEXT-SC06  THRU A390-SC06-EX
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM A390-FETCH-NEXT-SC06  THRU A390-SC06-EX
			a390FetchNextSc06();
		}
	}

	/**Original name: A390-FETCH-NEXT-SC06<br>
	 * <pre>*****************************************************************</pre>*/
	private void a390FetchNextSc06() {
		// COB_CODE: EXEC SQL
		//                FETCH CUR-SC06
		//           INTO
		//                   :ADE-ID-ADES
		//                  ,:ADE-ID-POLI
		//                  ,:ADE-ID-MOVI-CRZ
		//                  ,:ADE-ID-MOVI-CHIU
		//                   :IND-ADE-ID-MOVI-CHIU
		//                  ,:ADE-DT-INI-EFF-DB
		//                  ,:ADE-DT-END-EFF-DB
		//                  ,:ADE-IB-PREV
		//                   :IND-ADE-IB-PREV
		//                  ,:ADE-IB-OGG
		//                   :IND-ADE-IB-OGG
		//                  ,:ADE-COD-COMP-ANIA
		//                  ,:ADE-DT-DECOR-DB
		//                   :IND-ADE-DT-DECOR
		//                  ,:ADE-DT-SCAD-DB
		//                   :IND-ADE-DT-SCAD
		//                  ,:ADE-ETA-A-SCAD
		//                   :IND-ADE-ETA-A-SCAD
		//                  ,:ADE-DUR-AA
		//                   :IND-ADE-DUR-AA
		//                  ,:ADE-DUR-MM
		//                   :IND-ADE-DUR-MM
		//                  ,:ADE-DUR-GG
		//                   :IND-ADE-DUR-GG
		//                  ,:ADE-TP-RGM-FISC
		//                  ,:ADE-TP-RIAT
		//                   :IND-ADE-TP-RIAT
		//                  ,:ADE-TP-MOD-PAG-TIT
		//                  ,:ADE-TP-IAS
		//                   :IND-ADE-TP-IAS
		//                  ,:ADE-DT-VARZ-TP-IAS-DB
		//                   :IND-ADE-DT-VARZ-TP-IAS
		//                  ,:ADE-PRE-NET-IND
		//                   :IND-ADE-PRE-NET-IND
		//                  ,:ADE-PRE-LRD-IND
		//                   :IND-ADE-PRE-LRD-IND
		//                  ,:ADE-RAT-LRD-IND
		//                   :IND-ADE-RAT-LRD-IND
		//                  ,:ADE-PRSTZ-INI-IND
		//                   :IND-ADE-PRSTZ-INI-IND
		//                  ,:ADE-FL-COINC-ASSTO
		//                   :IND-ADE-FL-COINC-ASSTO
		//                  ,:ADE-IB-DFLT
		//                   :IND-ADE-IB-DFLT
		//                  ,:ADE-MOD-CALC
		//                   :IND-ADE-MOD-CALC
		//                  ,:ADE-TP-FNT-CNBTVA
		//                   :IND-ADE-TP-FNT-CNBTVA
		//                  ,:ADE-IMP-AZ
		//                   :IND-ADE-IMP-AZ
		//                  ,:ADE-IMP-ADER
		//                   :IND-ADE-IMP-ADER
		//                  ,:ADE-IMP-TFR
		//                   :IND-ADE-IMP-TFR
		//                  ,:ADE-IMP-VOLO
		//                   :IND-ADE-IMP-VOLO
		//                  ,:ADE-PC-AZ
		//                   :IND-ADE-PC-AZ
		//                  ,:ADE-PC-ADER
		//                   :IND-ADE-PC-ADER
		//                  ,:ADE-PC-TFR
		//                   :IND-ADE-PC-TFR
		//                  ,:ADE-PC-VOLO
		//                   :IND-ADE-PC-VOLO
		//                  ,:ADE-DT-NOVA-RGM-FISC-DB
		//                   :IND-ADE-DT-NOVA-RGM-FISC
		//                  ,:ADE-FL-ATTIV
		//                   :IND-ADE-FL-ATTIV
		//                  ,:ADE-IMP-REC-RIT-VIS
		//                   :IND-ADE-IMP-REC-RIT-VIS
		//                  ,:ADE-IMP-REC-RIT-ACC
		//                   :IND-ADE-IMP-REC-RIT-ACC
		//                  ,:ADE-FL-VARZ-STAT-TBGC
		//                   :IND-ADE-FL-VARZ-STAT-TBGC
		//                  ,:ADE-FL-PROVZA-MIGRAZ
		//                   :IND-ADE-FL-PROVZA-MIGRAZ
		//                  ,:ADE-IMPB-VIS-DA-REC
		//                   :IND-ADE-IMPB-VIS-DA-REC
		//                  ,:ADE-DT-DECOR-PREST-BAN-DB
		//                   :IND-ADE-DT-DECOR-PREST-BAN
		//                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
		//                   :IND-ADE-DT-EFF-VARZ-STAT-T
		//                  ,:ADE-DS-RIGA
		//                  ,:ADE-DS-OPER-SQL
		//                  ,:ADE-DS-VER
		//                  ,:ADE-DS-TS-INI-CPTZ
		//                  ,:ADE-DS-TS-END-CPTZ
		//                  ,:ADE-DS-UTENTE
		//                  ,:ADE-DS-STATO-ELAB
		//                  ,:ADE-CUM-CNBT-CAP
		//                   :IND-ADE-CUM-CNBT-CAP
		//                  ,:ADE-IMP-GAR-CNBT
		//                   :IND-ADE-IMP-GAR-CNBT
		//                  ,:ADE-DT-ULT-CONS-CNBT-DB
		//                   :IND-ADE-DT-ULT-CONS-CNBT
		//                  ,:ADE-IDEN-ISC-FND
		//                   :IND-ADE-IDEN-ISC-FND
		//                  ,:ADE-NUM-RAT-PIAN
		//                   :IND-ADE-NUM-RAT-PIAN
		//                  ,:ADE-DT-PRESC-DB
		//                   :IND-ADE-DT-PRESC
		//                  ,:ADE-CONCS-PREST
		//                   :IND-ADE-CONCS-PREST
		//                  ,:POL-ID-POLI
		//                  ,:POL-ID-MOVI-CRZ
		//                  ,:POL-ID-MOVI-CHIU
		//                   :IND-POL-ID-MOVI-CHIU
		//                  ,:POL-IB-OGG
		//                   :IND-POL-IB-OGG
		//                  ,:POL-IB-PROP
		//                  ,:POL-DT-PROP-DB
		//                   :IND-POL-DT-PROP
		//                  ,:POL-DT-INI-EFF-DB
		//                  ,:POL-DT-END-EFF-DB
		//                  ,:POL-COD-COMP-ANIA
		//                  ,:POL-DT-DECOR-DB
		//                  ,:POL-DT-EMIS-DB
		//                  ,:POL-TP-POLI
		//                  ,:POL-DUR-AA
		//                   :IND-POL-DUR-AA
		//                  ,:POL-DUR-MM
		//                   :IND-POL-DUR-MM
		//                  ,:POL-DT-SCAD-DB
		//                   :IND-POL-DT-SCAD
		//                  ,:POL-COD-PROD
		//                  ,:POL-DT-INI-VLDT-PROD-DB
		//                  ,:POL-COD-CONV
		//                   :IND-POL-COD-CONV
		//                  ,:POL-COD-RAMO
		//                   :IND-POL-COD-RAMO
		//                  ,:POL-DT-INI-VLDT-CONV-DB
		//                   :IND-POL-DT-INI-VLDT-CONV
		//                  ,:POL-DT-APPLZ-CONV-DB
		//                   :IND-POL-DT-APPLZ-CONV
		//                  ,:POL-TP-FRM-ASSVA
		//                  ,:POL-TP-RGM-FISC
		//                   :IND-POL-TP-RGM-FISC
		//                  ,:POL-FL-ESTAS
		//                   :IND-POL-FL-ESTAS
		//                  ,:POL-FL-RSH-COMUN
		//                   :IND-POL-FL-RSH-COMUN
		//                  ,:POL-FL-RSH-COMUN-COND
		//                   :IND-POL-FL-RSH-COMUN-COND
		//                  ,:POL-TP-LIV-GENZ-TIT
		//                  ,:POL-FL-COP-FINANZ
		//                   :IND-POL-FL-COP-FINANZ
		//                  ,:POL-TP-APPLZ-DIR
		//                   :IND-POL-TP-APPLZ-DIR
		//                  ,:POL-SPE-MED
		//                   :IND-POL-SPE-MED
		//                  ,:POL-DIR-EMIS
		//                   :IND-POL-DIR-EMIS
		//                  ,:POL-DIR-1O-VERS
		//                   :IND-POL-DIR-1O-VERS
		//                  ,:POL-DIR-VERS-AGG
		//                   :IND-POL-DIR-VERS-AGG
		//                  ,:POL-COD-DVS
		//                   :IND-POL-COD-DVS
		//                  ,:POL-FL-FNT-AZ
		//                   :IND-POL-FL-FNT-AZ
		//                  ,:POL-FL-FNT-ADER
		//                   :IND-POL-FL-FNT-ADER
		//                  ,:POL-FL-FNT-TFR
		//                   :IND-POL-FL-FNT-TFR
		//                  ,:POL-FL-FNT-VOLO
		//                   :IND-POL-FL-FNT-VOLO
		//                  ,:POL-TP-OPZ-A-SCAD
		//                   :IND-POL-TP-OPZ-A-SCAD
		//                  ,:POL-AA-DIFF-PROR-DFLT
		//                   :IND-POL-AA-DIFF-PROR-DFLT
		//                  ,:POL-FL-VER-PROD
		//                   :IND-POL-FL-VER-PROD
		//                  ,:POL-DUR-GG
		//                   :IND-POL-DUR-GG
		//                  ,:POL-DIR-QUIET
		//                   :IND-POL-DIR-QUIET
		//                  ,:POL-TP-PTF-ESTNO
		//                   :IND-POL-TP-PTF-ESTNO
		//                  ,:POL-FL-CUM-PRE-CNTR
		//                   :IND-POL-FL-CUM-PRE-CNTR
		//                  ,:POL-FL-AMMB-MOVI
		//                   :IND-POL-FL-AMMB-MOVI
		//                  ,:POL-CONV-GECO
		//                   :IND-POL-CONV-GECO
		//                  ,:POL-DS-RIGA
		//                  ,:POL-DS-OPER-SQL
		//                  ,:POL-DS-VER
		//                  ,:POL-DS-TS-INI-CPTZ
		//                  ,:POL-DS-TS-END-CPTZ
		//                  ,:POL-DS-UTENTE
		//                  ,:POL-DS-STATO-ELAB
		//                  ,:POL-FL-SCUDO-FISC
		//                   :IND-POL-FL-SCUDO-FISC
		//                  ,:POL-FL-TRASFE
		//                   :IND-POL-FL-TRASFE
		//                  ,:POL-FL-TFR-STRC
		//                   :IND-POL-FL-TFR-STRC
		//                  ,:POL-DT-PRESC-DB
		//                   :IND-POL-DT-PRESC
		//                  ,:POL-COD-CONV-AGG
		//                   :IND-POL-COD-CONV-AGG
		//                  ,:POL-SUBCAT-PROD
		//                   :IND-POL-SUBCAT-PROD
		//                  ,:POL-FL-QUEST-ADEGZ-ASS
		//                   :IND-POL-FL-QUEST-ADEGZ-ASS
		//                  ,:POL-COD-TPA
		//                   :IND-POL-COD-TPA
		//                  ,:POL-ID-ACC-COMM
		//                   :IND-POL-ID-ACC-COMM
		//                  ,:POL-FL-POLI-CPI-PR
		//                   :IND-POL-FL-POLI-CPI-PR
		//                  ,:POL-FL-POLI-BUNDLING
		//                   :IND-POL-FL-POLI-BUNDLING
		//                  ,:POL-IND-POLI-PRIN-COLL
		//                   :IND-POL-IND-POLI-PRIN-COLL
		//                  ,:POL-FL-VND-BUNDLE
		//                   :IND-POL-FL-VND-BUNDLE
		//                  ,:POL-IB-BS
		//                   :IND-POL-IB-BS
		//                  ,:POL-FL-POLI-IFP
		//                   :IND-POL-FL-POLI-IFP
		//                  ,:STB-ID-STAT-OGG-BUS
		//                  ,:STB-ID-OGG
		//                  ,:STB-TP-OGG
		//                  ,:STB-ID-MOVI-CRZ
		//                  ,:STB-ID-MOVI-CHIU
		//                   :IND-STB-ID-MOVI-CHIU
		//                  ,:STB-DT-INI-EFF-DB
		//                  ,:STB-DT-END-EFF-DB
		//                  ,:STB-COD-COMP-ANIA
		//                  ,:STB-TP-STAT-BUS
		//                  ,:STB-TP-CAUS
		//                  ,:STB-DS-RIGA
		//                  ,:STB-DS-OPER-SQL
		//                  ,:STB-DS-VER
		//                  ,:STB-DS-TS-INI-CPTZ
		//                  ,:STB-DS-TS-END-CPTZ
		//                  ,:STB-DS-UTENTE
		//                  ,:STB-DS-STATO-ELAB
		//           END-EXEC.
		adesPoliStatOggBusTrchDiGarDao.fetchCurSc06(this.getAdesPoliStatOggBusTrchDiGarLdbm0250());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           ELSE
		//             END-IF
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			z100SetColonneNull();
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		} else if (idsv0003.getSqlcode().isNotFound()) {
			// COB_CODE: IF IDSV0003-NOT-FOUND
			//              END-IF
			//           END-IF
			// COB_CODE: PERFORM A370-CLOSE-CURSOR-SC06 THRU A370-SC06-EX
			a370CloseCursorSc06();
			// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
			//              SET IDSV0003-NOT-FOUND TO TRUE
			//           END-IF
			if (idsv0003.getSqlcode().isSuccessfulSql()) {
				// COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
				idsv0003.getSqlcode().setNotFound();
			}
		}
	}

	/**Original name: SC07-SELECTION-CURSOR-07<br>
	 * <pre>*****************************************************************
	 * *****************************************************************</pre>*/
	private void sc07SelectionCursor07() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-SELECT
		//                 PERFORM A310-SELECT-SC07           THRU A310-SC07-EX
		//              WHEN IDSV0003-OPEN-CURSOR
		//                 PERFORM A360-OPEN-CURSOR-SC07      THRU A360-SC07-EX
		//              WHEN IDSV0003-CLOSE-CURSOR
		//                 PERFORM A370-CLOSE-CURSOR-SC07     THRU A370-SC07-EX
		//              WHEN IDSV0003-FETCH-FIRST
		//                 PERFORM A380-FETCH-FIRST-SC07      THRU A380-SC07-EX
		//              WHEN IDSV0003-FETCH-NEXT
		//                 PERFORM A390-FETCH-NEXT-SC07       THRU A390-SC07-EX
		//              WHEN IDSV0003-UPDATE
		//                 PERFORM A320-UPDATE-SC07           THRU A320-SC07-EX
		//              WHEN OTHER
		//                 SET IDSV0003-INVALID-OPER          TO TRUE
		//           END-EVALUATE.
		if (idsv0003.getOperazione().isSelect()) {
			// COB_CODE: PERFORM A310-SELECT-SC07           THRU A310-SC07-EX
			a310SelectSc07();
		} else if (idsv0003.getOperazione().isOpenCursor()) {
			// COB_CODE: PERFORM A360-OPEN-CURSOR-SC07      THRU A360-SC07-EX
			a360OpenCursorSc07();
		} else if (idsv0003.getOperazione().isCloseCursor()) {
			// COB_CODE: PERFORM A370-CLOSE-CURSOR-SC07     THRU A370-SC07-EX
			a370CloseCursorSc07();
		} else if (idsv0003.getOperazione().isFetchFirst()) {
			// COB_CODE: PERFORM A380-FETCH-FIRST-SC07      THRU A380-SC07-EX
			a380FetchFirstSc07();
		} else if (idsv0003.getOperazione().isFetchNext()) {
			// COB_CODE: PERFORM A390-FETCH-NEXT-SC07       THRU A390-SC07-EX
			a390FetchNextSc07();
		} else if (idsv0003.getOperazione().isUpdate()) {
			// COB_CODE: PERFORM A320-UPDATE-SC07           THRU A320-SC07-EX
			a320UpdateSc07();
		} else {
			// COB_CODE: SET IDSV0003-INVALID-OPER          TO TRUE
			idsv0003.getReturnCode().setInvalidOper();
		}
	}

	/**Original name: A305-DECLARE-CURSOR-SC07<br>
	 * <pre>*****************************************************************</pre>*/
	private void a305DeclareCursorSc07() {
		// COB_CODE:      EXEC SQL
		//                     DECLARE CUR-SC07 CURSOR WITH HOLD FOR
		//                  SELECT
		//                         A.ID_ADES
		//                        ,A.ID_POLI
		//                        ,A.ID_MOVI_CRZ
		//                        ,A.ID_MOVI_CHIU
		//                        ,A.DT_INI_EFF
		//                        ,A.DT_END_EFF
		//                        ,A.IB_PREV
		//                        ,A.IB_OGG
		//                        ,A.COD_COMP_ANIA
		//                        ,A.DT_DECOR
		//                        ,A.DT_SCAD
		//                        ,A.ETA_A_SCAD
		//                        ,A.DUR_AA
		//                        ,A.DUR_MM
		//                        ,A.DUR_GG
		//                        ,A.TP_RGM_FISC
		//                        ,A.TP_RIAT
		//                        ,A.TP_MOD_PAG_TIT
		//                        ,A.TP_IAS
		//                        ,A.DT_VARZ_TP_IAS
		//                        ,A.PRE_NET_IND
		//                        ,A.PRE_LRD_IND
		//                        ,A.RAT_LRD_IND
		//                        ,A.PRSTZ_INI_IND
		//                        ,A.FL_COINC_ASSTO
		//                        ,A.IB_DFLT
		//                        ,A.MOD_CALC
		//                        ,A.TP_FNT_CNBTVA
		//                        ,A.IMP_AZ
		//                        ,A.IMP_ADER
		//                        ,A.IMP_TFR
		//                        ,A.IMP_VOLO
		//                        ,A.PC_AZ
		//                        ,A.PC_ADER
		//                        ,A.PC_TFR
		//                        ,A.PC_VOLO
		//                        ,A.DT_NOVA_RGM_FISC
		//                        ,A.FL_ATTIV
		//                        ,A.IMP_REC_RIT_VIS
		//                        ,A.IMP_REC_RIT_ACC
		//                        ,A.FL_VARZ_STAT_TBGC
		//                        ,A.FL_PROVZA_MIGRAZ
		//                        ,A.IMPB_VIS_DA_REC
		//                        ,A.DT_DECOR_PREST_BAN
		//                        ,A.DT_EFF_VARZ_STAT_T
		//                        ,A.DS_RIGA
		//                        ,A.DS_OPER_SQL
		//                        ,A.DS_VER
		//                        ,A.DS_TS_INI_CPTZ
		//                        ,A.DS_TS_END_CPTZ
		//                        ,A.DS_UTENTE
		//                        ,A.DS_STATO_ELAB
		//                        ,A.CUM_CNBT_CAP
		//                        ,A.IMP_GAR_CNBT
		//                        ,A.DT_ULT_CONS_CNBT
		//                        ,A.IDEN_ISC_FND
		//                        ,A.NUM_RAT_PIAN
		//                        ,A.DT_PRESC
		//                        ,A.CONCS_PREST
		//                        ,B.ID_POLI
		//                        ,B.ID_MOVI_CRZ
		//                        ,B.ID_MOVI_CHIU
		//                        ,B.IB_OGG
		//                        ,B.IB_PROP
		//                        ,B.DT_PROP
		//                        ,B.DT_INI_EFF
		//                        ,B.DT_END_EFF
		//                        ,B.COD_COMP_ANIA
		//                        ,B.DT_DECOR
		//                        ,B.DT_EMIS
		//                        ,B.TP_POLI
		//                        ,B.DUR_AA
		//                        ,B.DUR_MM
		//                        ,B.DT_SCAD
		//                        ,B.COD_PROD
		//                        ,B.DT_INI_VLDT_PROD
		//                        ,B.COD_CONV
		//                        ,B.COD_RAMO
		//                        ,B.DT_INI_VLDT_CONV
		//                        ,B.DT_APPLZ_CONV
		//                        ,B.TP_FRM_ASSVA
		//                        ,B.TP_RGM_FISC
		//                        ,B.FL_ESTAS
		//                        ,B.FL_RSH_COMUN
		//                        ,B.FL_RSH_COMUN_COND
		//                        ,B.TP_LIV_GENZ_TIT
		//                        ,B.FL_COP_FINANZ
		//                        ,B.TP_APPLZ_DIR
		//                        ,B.SPE_MED
		//                        ,B.DIR_EMIS
		//                        ,B.DIR_1O_VERS
		//                        ,B.DIR_VERS_AGG
		//                        ,B.COD_DVS
		//                        ,B.FL_FNT_AZ
		//                        ,B.FL_FNT_ADER
		//                        ,B.FL_FNT_TFR
		//                        ,B.FL_FNT_VOLO
		//                        ,B.TP_OPZ_A_SCAD
		//                        ,B.AA_DIFF_PROR_DFLT
		//                        ,B.FL_VER_PROD
		//                        ,B.DUR_GG
		//                        ,B.DIR_QUIET
		//                        ,B.TP_PTF_ESTNO
		//                        ,B.FL_CUM_PRE_CNTR
		//                        ,B.FL_AMMB_MOVI
		//                        ,B.CONV_GECO
		//                        ,B.DS_RIGA
		//                        ,B.DS_OPER_SQL
		//                        ,B.DS_VER
		//                        ,B.DS_TS_INI_CPTZ
		//                        ,B.DS_TS_END_CPTZ
		//                        ,B.DS_UTENTE
		//                        ,B.DS_STATO_ELAB
		//                        ,B.FL_SCUDO_FISC
		//                        ,B.FL_TRASFE
		//                        ,B.FL_TFR_STRC
		//                        ,B.DT_PRESC
		//                        ,B.COD_CONV_AGG
		//                        ,B.SUBCAT_PROD
		//                        ,B.FL_QUEST_ADEGZ_ASS
		//                        ,B.COD_TPA
		//                        ,B.ID_ACC_COMM
		//                        ,B.FL_POLI_CPI_PR
		//                        ,B.FL_POLI_BUNDLING
		//                        ,B.IND_POLI_PRIN_COLL
		//                        ,B.FL_VND_BUNDLE
		//                        ,B.IB_BS
		//                        ,B.FL_POLI_IFP
		//                        ,C.ID_STAT_OGG_BUS
		//                        ,C.ID_OGG
		//                        ,C.TP_OGG
		//                        ,C.ID_MOVI_CRZ
		//                        ,C.ID_MOVI_CHIU
		//                        ,C.DT_INI_EFF
		//                        ,C.DT_END_EFF
		//                        ,C.COD_COMP_ANIA
		//                        ,C.TP_STAT_BUS
		//                        ,C.TP_CAUS
		//                        ,C.DS_RIGA
		//                        ,C.DS_OPER_SQL
		//                        ,C.DS_VER
		//                        ,C.DS_TS_INI_CPTZ
		//                        ,C.DS_TS_END_CPTZ
		//                        ,C.DS_UTENTE
		//                        ,C.DS_STATO_ELAB
		//                  FROM ADES      A,
		//                       POLI      B,
		//                       STAT_OGG_BUS C
		//                  WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
		//                                             :WS-TP-FRM-ASSVA2 )
		//                    AND B.IB_OGG BETWEEN  :WLB-IB-POLI-FIRST AND
		//                                             :WLB-IB-POLI-LAST
		//                    AND A.ID_POLI      =   B.ID_POLI
		//                    AND A.ID_ADES      =   C.ID_OGG
		//                    AND C.TP_OGG       =  'AD'
		//           *--  D C.TP_STAT_BUS     =  'VI'
		//                    AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
		//                    AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
		//                    AND A.DT_END_EFF  >   :WS-DT-PTF-X
		//                    AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                    AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                    AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
		//                    AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
		//                    AND B.DT_END_EFF  >   :WS-DT-PTF-X
		//                    AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                    AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                    AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
		//                    AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
		//                    AND C.DT_END_EFF  >   :WS-DT-PTF-X
		//                    AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                    AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                    AND A.ID_ADES IN
		//                       (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
		//                                                        STAT_OGG_BUS E
		//                        WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
		//                        AND E.TP_OGG = 'TG'
		//                        AND E.TP_STAT_BUS IN ('VI','ST')
		//                        AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
		//                        AND D.COD_COMP_ANIA =
		//                            :IDSV0003-CODICE-COMPAGNIA-ANIA
		//                        AND D.DT_INI_EFF <=   :WS-DT-PTF-X
		//                        AND D.DT_END_EFF >    :WS-DT-PTF-X
		//                        AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                        AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                        AND E.DT_INI_EFF <=   :WS-DT-PTF-X
		//                        AND E.DT_END_EFF >    :WS-DT-PTF-X
		//                        AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                        AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                        AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
		//                             OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
		//                        )
		//                    AND A.DS_STATO_ELAB IN (
		//                                            :IABV0002-STATE-01,
		//                                            :IABV0002-STATE-02,
		//                                            :IABV0002-STATE-03,
		//                                            :IABV0002-STATE-04,
		//                                            :IABV0002-STATE-05,
		//                                            :IABV0002-STATE-06,
		//                                            :IABV0002-STATE-07,
		//                                            :IABV0002-STATE-08,
		//                                            :IABV0002-STATE-09,
		//                                            :IABV0002-STATE-10
		//                                              )
		//                    AND NOT A.DS_VER  = :IABV0009-VERSIONING
		//                    ORDER BY A.ID_POLI, A.ID_ADES
		//                END-EXEC.
		// DECLARE CURSOR doesn't need a translation;
	}

	/**Original name: A310-SELECT-SC07<br>
	 * <pre>*****************************************************************</pre>*/
	private void a310SelectSc07() {
		// COB_CODE:      EXEC SQL
		//                  SELECT
		//                         A.ID_ADES
		//                        ,A.ID_POLI
		//                        ,A.ID_MOVI_CRZ
		//                        ,A.ID_MOVI_CHIU
		//                        ,A.DT_INI_EFF
		//                        ,A.DT_END_EFF
		//                        ,A.IB_PREV
		//                        ,A.IB_OGG
		//                        ,A.COD_COMP_ANIA
		//                        ,A.DT_DECOR
		//                        ,A.DT_SCAD
		//                        ,A.ETA_A_SCAD
		//                        ,A.DUR_AA
		//                        ,A.DUR_MM
		//                        ,A.DUR_GG
		//                        ,A.TP_RGM_FISC
		//                        ,A.TP_RIAT
		//                        ,A.TP_MOD_PAG_TIT
		//                        ,A.TP_IAS
		//                        ,A.DT_VARZ_TP_IAS
		//                        ,A.PRE_NET_IND
		//                        ,A.PRE_LRD_IND
		//                        ,A.RAT_LRD_IND
		//                        ,A.PRSTZ_INI_IND
		//                        ,A.FL_COINC_ASSTO
		//                        ,A.IB_DFLT
		//                        ,A.MOD_CALC
		//                        ,A.TP_FNT_CNBTVA
		//                        ,A.IMP_AZ
		//                        ,A.IMP_ADER
		//                        ,A.IMP_TFR
		//                        ,A.IMP_VOLO
		//                        ,A.PC_AZ
		//                        ,A.PC_ADER
		//                        ,A.PC_TFR
		//                        ,A.PC_VOLO
		//                        ,A.DT_NOVA_RGM_FISC
		//                        ,A.FL_ATTIV
		//                        ,A.IMP_REC_RIT_VIS
		//                        ,A.IMP_REC_RIT_ACC
		//                        ,A.FL_VARZ_STAT_TBGC
		//                        ,A.FL_PROVZA_MIGRAZ
		//                        ,A.IMPB_VIS_DA_REC
		//                        ,A.DT_DECOR_PREST_BAN
		//                        ,A.DT_EFF_VARZ_STAT_T
		//                        ,A.DS_RIGA
		//                        ,A.DS_OPER_SQL
		//                        ,A.DS_VER
		//                        ,A.DS_TS_INI_CPTZ
		//                        ,A.DS_TS_END_CPTZ
		//                        ,A.DS_UTENTE
		//                        ,A.DS_STATO_ELAB
		//                        ,A.CUM_CNBT_CAP
		//                        ,A.IMP_GAR_CNBT
		//                        ,A.DT_ULT_CONS_CNBT
		//                        ,A.IDEN_ISC_FND
		//                        ,A.NUM_RAT_PIAN
		//                        ,A.DT_PRESC
		//                        ,A.CONCS_PREST
		//                        ,B.ID_POLI
		//                        ,B.ID_MOVI_CRZ
		//                        ,B.ID_MOVI_CHIU
		//                        ,B.IB_OGG
		//                        ,B.IB_PROP
		//                        ,B.DT_PROP
		//                        ,B.DT_INI_EFF
		//                        ,B.DT_END_EFF
		//                        ,B.COD_COMP_ANIA
		//                        ,B.DT_DECOR
		//                        ,B.DT_EMIS
		//                        ,B.TP_POLI
		//                        ,B.DUR_AA
		//                        ,B.DUR_MM
		//                        ,B.DT_SCAD
		//                        ,B.COD_PROD
		//                        ,B.DT_INI_VLDT_PROD
		//                        ,B.COD_CONV
		//                        ,B.COD_RAMO
		//                        ,B.DT_INI_VLDT_CONV
		//                        ,B.DT_APPLZ_CONV
		//                        ,B.TP_FRM_ASSVA
		//                        ,B.TP_RGM_FISC
		//                        ,B.FL_ESTAS
		//                        ,B.FL_RSH_COMUN
		//                        ,B.FL_RSH_COMUN_COND
		//                        ,B.TP_LIV_GENZ_TIT
		//                        ,B.FL_COP_FINANZ
		//                        ,B.TP_APPLZ_DIR
		//                        ,B.SPE_MED
		//                        ,B.DIR_EMIS
		//                        ,B.DIR_1O_VERS
		//                        ,B.DIR_VERS_AGG
		//                        ,B.COD_DVS
		//                        ,B.FL_FNT_AZ
		//                        ,B.FL_FNT_ADER
		//                        ,B.FL_FNT_TFR
		//                        ,B.FL_FNT_VOLO
		//                        ,B.TP_OPZ_A_SCAD
		//                        ,B.AA_DIFF_PROR_DFLT
		//                        ,B.FL_VER_PROD
		//                        ,B.DUR_GG
		//                        ,B.DIR_QUIET
		//                        ,B.TP_PTF_ESTNO
		//                        ,B.FL_CUM_PRE_CNTR
		//                        ,B.FL_AMMB_MOVI
		//                        ,B.CONV_GECO
		//                        ,B.DS_RIGA
		//                        ,B.DS_OPER_SQL
		//                        ,B.DS_VER
		//                        ,B.DS_TS_INI_CPTZ
		//                        ,B.DS_TS_END_CPTZ
		//                        ,B.DS_UTENTE
		//                        ,B.DS_STATO_ELAB
		//                        ,B.FL_SCUDO_FISC
		//                        ,B.FL_TRASFE
		//                        ,B.FL_TFR_STRC
		//                        ,B.DT_PRESC
		//                        ,B.COD_CONV_AGG
		//                        ,B.SUBCAT_PROD
		//                        ,B.FL_QUEST_ADEGZ_ASS
		//                        ,B.COD_TPA
		//                        ,B.ID_ACC_COMM
		//                        ,B.FL_POLI_CPI_PR
		//                        ,B.FL_POLI_BUNDLING
		//                        ,B.IND_POLI_PRIN_COLL
		//                        ,B.FL_VND_BUNDLE
		//                        ,B.IB_BS
		//                        ,B.FL_POLI_IFP
		//                        ,C.ID_STAT_OGG_BUS
		//                        ,C.ID_OGG
		//                        ,C.TP_OGG
		//                        ,C.ID_MOVI_CRZ
		//                        ,C.ID_MOVI_CHIU
		//                        ,C.DT_INI_EFF
		//                        ,C.DT_END_EFF
		//                        ,C.COD_COMP_ANIA
		//                        ,C.TP_STAT_BUS
		//                        ,C.TP_CAUS
		//                        ,C.DS_RIGA
		//                        ,C.DS_OPER_SQL
		//                        ,C.DS_VER
		//                        ,C.DS_TS_INI_CPTZ
		//                        ,C.DS_TS_END_CPTZ
		//                        ,C.DS_UTENTE
		//                        ,C.DS_STATO_ELAB
		//                  INTO
		//                        :ADE-ID-ADES
		//                       ,:ADE-ID-POLI
		//                       ,:ADE-ID-MOVI-CRZ
		//                       ,:ADE-ID-MOVI-CHIU
		//                        :IND-ADE-ID-MOVI-CHIU
		//                       ,:ADE-DT-INI-EFF-DB
		//                       ,:ADE-DT-END-EFF-DB
		//                       ,:ADE-IB-PREV
		//                        :IND-ADE-IB-PREV
		//                       ,:ADE-IB-OGG
		//                        :IND-ADE-IB-OGG
		//                       ,:ADE-COD-COMP-ANIA
		//                       ,:ADE-DT-DECOR-DB
		//                        :IND-ADE-DT-DECOR
		//                       ,:ADE-DT-SCAD-DB
		//                        :IND-ADE-DT-SCAD
		//                       ,:ADE-ETA-A-SCAD
		//                        :IND-ADE-ETA-A-SCAD
		//                       ,:ADE-DUR-AA
		//                        :IND-ADE-DUR-AA
		//                       ,:ADE-DUR-MM
		//                        :IND-ADE-DUR-MM
		//                       ,:ADE-DUR-GG
		//                        :IND-ADE-DUR-GG
		//                       ,:ADE-TP-RGM-FISC
		//                       ,:ADE-TP-RIAT
		//                        :IND-ADE-TP-RIAT
		//                       ,:ADE-TP-MOD-PAG-TIT
		//                       ,:ADE-TP-IAS
		//                        :IND-ADE-TP-IAS
		//                       ,:ADE-DT-VARZ-TP-IAS-DB
		//                        :IND-ADE-DT-VARZ-TP-IAS
		//                       ,:ADE-PRE-NET-IND
		//                        :IND-ADE-PRE-NET-IND
		//                       ,:ADE-PRE-LRD-IND
		//                        :IND-ADE-PRE-LRD-IND
		//                       ,:ADE-RAT-LRD-IND
		//                        :IND-ADE-RAT-LRD-IND
		//                       ,:ADE-PRSTZ-INI-IND
		//                        :IND-ADE-PRSTZ-INI-IND
		//                       ,:ADE-FL-COINC-ASSTO
		//                        :IND-ADE-FL-COINC-ASSTO
		//                       ,:ADE-IB-DFLT
		//                        :IND-ADE-IB-DFLT
		//                       ,:ADE-MOD-CALC
		//                        :IND-ADE-MOD-CALC
		//                       ,:ADE-TP-FNT-CNBTVA
		//                        :IND-ADE-TP-FNT-CNBTVA
		//                       ,:ADE-IMP-AZ
		//                        :IND-ADE-IMP-AZ
		//                       ,:ADE-IMP-ADER
		//                        :IND-ADE-IMP-ADER
		//                       ,:ADE-IMP-TFR
		//                        :IND-ADE-IMP-TFR
		//                       ,:ADE-IMP-VOLO
		//                        :IND-ADE-IMP-VOLO
		//                       ,:ADE-PC-AZ
		//                        :IND-ADE-PC-AZ
		//                       ,:ADE-PC-ADER
		//                        :IND-ADE-PC-ADER
		//                       ,:ADE-PC-TFR
		//                        :IND-ADE-PC-TFR
		//                       ,:ADE-PC-VOLO
		//                        :IND-ADE-PC-VOLO
		//                       ,:ADE-DT-NOVA-RGM-FISC-DB
		//                        :IND-ADE-DT-NOVA-RGM-FISC
		//                       ,:ADE-FL-ATTIV
		//                        :IND-ADE-FL-ATTIV
		//                       ,:ADE-IMP-REC-RIT-VIS
		//                        :IND-ADE-IMP-REC-RIT-VIS
		//                       ,:ADE-IMP-REC-RIT-ACC
		//                        :IND-ADE-IMP-REC-RIT-ACC
		//                       ,:ADE-FL-VARZ-STAT-TBGC
		//                        :IND-ADE-FL-VARZ-STAT-TBGC
		//                       ,:ADE-FL-PROVZA-MIGRAZ
		//                        :IND-ADE-FL-PROVZA-MIGRAZ
		//                       ,:ADE-IMPB-VIS-DA-REC
		//                        :IND-ADE-IMPB-VIS-DA-REC
		//                       ,:ADE-DT-DECOR-PREST-BAN-DB
		//                        :IND-ADE-DT-DECOR-PREST-BAN
		//                       ,:ADE-DT-EFF-VARZ-STAT-T-DB
		//                        :IND-ADE-DT-EFF-VARZ-STAT-T
		//                       ,:ADE-DS-RIGA
		//                       ,:ADE-DS-OPER-SQL
		//                       ,:ADE-DS-VER
		//                       ,:ADE-DS-TS-INI-CPTZ
		//                       ,:ADE-DS-TS-END-CPTZ
		//                       ,:ADE-DS-UTENTE
		//                       ,:ADE-DS-STATO-ELAB
		//                       ,:ADE-CUM-CNBT-CAP
		//                        :IND-ADE-CUM-CNBT-CAP
		//                       ,:ADE-IMP-GAR-CNBT
		//                        :IND-ADE-IMP-GAR-CNBT
		//                       ,:ADE-DT-ULT-CONS-CNBT-DB
		//                        :IND-ADE-DT-ULT-CONS-CNBT
		//                       ,:ADE-IDEN-ISC-FND
		//                        :IND-ADE-IDEN-ISC-FND
		//                       ,:ADE-NUM-RAT-PIAN
		//                        :IND-ADE-NUM-RAT-PIAN
		//                       ,:ADE-DT-PRESC-DB
		//                        :IND-ADE-DT-PRESC
		//                       ,:ADE-CONCS-PREST
		//                        :IND-ADE-CONCS-PREST
		//                       ,:POL-ID-POLI
		//                       ,:POL-ID-MOVI-CRZ
		//                       ,:POL-ID-MOVI-CHIU
		//                        :IND-POL-ID-MOVI-CHIU
		//                       ,:POL-IB-OGG
		//                        :IND-POL-IB-OGG
		//                       ,:POL-IB-PROP
		//                       ,:POL-DT-PROP-DB
		//                        :IND-POL-DT-PROP
		//                       ,:POL-DT-INI-EFF-DB
		//                       ,:POL-DT-END-EFF-DB
		//                       ,:POL-COD-COMP-ANIA
		//                       ,:POL-DT-DECOR-DB
		//                       ,:POL-DT-EMIS-DB
		//                       ,:POL-TP-POLI
		//                       ,:POL-DUR-AA
		//                        :IND-POL-DUR-AA
		//                       ,:POL-DUR-MM
		//                        :IND-POL-DUR-MM
		//                       ,:POL-DT-SCAD-DB
		//                        :IND-POL-DT-SCAD
		//                       ,:POL-COD-PROD
		//                       ,:POL-DT-INI-VLDT-PROD-DB
		//                       ,:POL-COD-CONV
		//                        :IND-POL-COD-CONV
		//                       ,:POL-COD-RAMO
		//                        :IND-POL-COD-RAMO
		//                       ,:POL-DT-INI-VLDT-CONV-DB
		//                        :IND-POL-DT-INI-VLDT-CONV
		//                       ,:POL-DT-APPLZ-CONV-DB
		//                        :IND-POL-DT-APPLZ-CONV
		//                       ,:POL-TP-FRM-ASSVA
		//                       ,:POL-TP-RGM-FISC
		//                        :IND-POL-TP-RGM-FISC
		//                       ,:POL-FL-ESTAS
		//                        :IND-POL-FL-ESTAS
		//                       ,:POL-FL-RSH-COMUN
		//                        :IND-POL-FL-RSH-COMUN
		//                       ,:POL-FL-RSH-COMUN-COND
		//                        :IND-POL-FL-RSH-COMUN-COND
		//                       ,:POL-TP-LIV-GENZ-TIT
		//                       ,:POL-FL-COP-FINANZ
		//                        :IND-POL-FL-COP-FINANZ
		//                       ,:POL-TP-APPLZ-DIR
		//                        :IND-POL-TP-APPLZ-DIR
		//                       ,:POL-SPE-MED
		//                        :IND-POL-SPE-MED
		//                       ,:POL-DIR-EMIS
		//                        :IND-POL-DIR-EMIS
		//                       ,:POL-DIR-1O-VERS
		//                        :IND-POL-DIR-1O-VERS
		//                       ,:POL-DIR-VERS-AGG
		//                        :IND-POL-DIR-VERS-AGG
		//                       ,:POL-COD-DVS
		//                        :IND-POL-COD-DVS
		//                       ,:POL-FL-FNT-AZ
		//                        :IND-POL-FL-FNT-AZ
		//                       ,:POL-FL-FNT-ADER
		//                        :IND-POL-FL-FNT-ADER
		//                       ,:POL-FL-FNT-TFR
		//                        :IND-POL-FL-FNT-TFR
		//                       ,:POL-FL-FNT-VOLO
		//                        :IND-POL-FL-FNT-VOLO
		//                       ,:POL-TP-OPZ-A-SCAD
		//                        :IND-POL-TP-OPZ-A-SCAD
		//                       ,:POL-AA-DIFF-PROR-DFLT
		//                        :IND-POL-AA-DIFF-PROR-DFLT
		//                       ,:POL-FL-VER-PROD
		//                        :IND-POL-FL-VER-PROD
		//                       ,:POL-DUR-GG
		//                        :IND-POL-DUR-GG
		//                       ,:POL-DIR-QUIET
		//                        :IND-POL-DIR-QUIET
		//                       ,:POL-TP-PTF-ESTNO
		//                        :IND-POL-TP-PTF-ESTNO
		//                       ,:POL-FL-CUM-PRE-CNTR
		//                        :IND-POL-FL-CUM-PRE-CNTR
		//                       ,:POL-FL-AMMB-MOVI
		//                        :IND-POL-FL-AMMB-MOVI
		//                       ,:POL-CONV-GECO
		//                        :IND-POL-CONV-GECO
		//                       ,:POL-DS-RIGA
		//                       ,:POL-DS-OPER-SQL
		//                       ,:POL-DS-VER
		//                       ,:POL-DS-TS-INI-CPTZ
		//                       ,:POL-DS-TS-END-CPTZ
		//                       ,:POL-DS-UTENTE
		//                       ,:POL-DS-STATO-ELAB
		//                       ,:POL-FL-SCUDO-FISC
		//                        :IND-POL-FL-SCUDO-FISC
		//                       ,:POL-FL-TRASFE
		//                        :IND-POL-FL-TRASFE
		//                       ,:POL-FL-TFR-STRC
		//                        :IND-POL-FL-TFR-STRC
		//                       ,:POL-DT-PRESC-DB
		//                        :IND-POL-DT-PRESC
		//                       ,:POL-COD-CONV-AGG
		//                        :IND-POL-COD-CONV-AGG
		//                       ,:POL-SUBCAT-PROD
		//                        :IND-POL-SUBCAT-PROD
		//                       ,:POL-FL-QUEST-ADEGZ-ASS
		//                        :IND-POL-FL-QUEST-ADEGZ-ASS
		//                       ,:POL-COD-TPA
		//                        :IND-POL-COD-TPA
		//                       ,:POL-ID-ACC-COMM
		//                        :IND-POL-ID-ACC-COMM
		//                       ,:POL-FL-POLI-CPI-PR
		//                        :IND-POL-FL-POLI-CPI-PR
		//                       ,:POL-FL-POLI-BUNDLING
		//                        :IND-POL-FL-POLI-BUNDLING
		//                       ,:POL-IND-POLI-PRIN-COLL
		//                        :IND-POL-IND-POLI-PRIN-COLL
		//                       ,:POL-FL-VND-BUNDLE
		//                        :IND-POL-FL-VND-BUNDLE
		//                       ,:POL-IB-BS
		//                        :IND-POL-IB-BS
		//                       ,:POL-FL-POLI-IFP
		//                        :IND-POL-FL-POLI-IFP
		//                       ,:STB-ID-STAT-OGG-BUS
		//                       ,:STB-ID-OGG
		//                       ,:STB-TP-OGG
		//                       ,:STB-ID-MOVI-CRZ
		//                       ,:STB-ID-MOVI-CHIU
		//                        :IND-STB-ID-MOVI-CHIU
		//                       ,:STB-DT-INI-EFF-DB
		//                       ,:STB-DT-END-EFF-DB
		//                       ,:STB-COD-COMP-ANIA
		//                       ,:STB-TP-STAT-BUS
		//                       ,:STB-TP-CAUS
		//                       ,:STB-DS-RIGA
		//                       ,:STB-DS-OPER-SQL
		//                       ,:STB-DS-VER
		//                       ,:STB-DS-TS-INI-CPTZ
		//                       ,:STB-DS-TS-END-CPTZ
		//                       ,:STB-DS-UTENTE
		//                       ,:STB-DS-STATO-ELAB
		//                  FROM ADES      A,
		//                       POLI      B,
		//                       STAT_OGG_BUS C
		//                  WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
		//                                             :WS-TP-FRM-ASSVA2 )
		//                    AND B.IB_OGG BETWEEN  :WLB-IB-POLI-FIRST AND
		//                                             :WLB-IB-POLI-LAST
		//                    AND A.ID_POLI      =   B.ID_POLI
		//                    AND A.ID_ADES      =   C.ID_OGG
		//                    AND C.TP_OGG       =  'AD'
		//           *--  D C.TP_STAT_BUS     =  'VI'
		//                    AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
		//                    AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
		//                    AND A.DT_END_EFF  >   :WS-DT-PTF-X
		//                    AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                    AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                    AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
		//                    AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
		//                    AND B.DT_END_EFF  >   :WS-DT-PTF-X
		//                    AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                    AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                    AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
		//                    AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
		//                    AND C.DT_END_EFF  >   :WS-DT-PTF-X
		//                    AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                    AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                    AND A.ID_ADES IN
		//                       (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
		//                                                        STAT_OGG_BUS E
		//                        WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
		//                        AND E.TP_OGG = 'TG'
		//                        AND E.TP_STAT_BUS IN ('VI','ST')
		//                        AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
		//                        AND D.COD_COMP_ANIA =
		//                            :IDSV0003-CODICE-COMPAGNIA-ANIA
		//                        AND D.DT_INI_EFF <=   :WS-DT-PTF-X
		//                        AND D.DT_END_EFF >    :WS-DT-PTF-X
		//                        AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                        AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                        AND E.DT_INI_EFF <=   :WS-DT-PTF-X
		//                        AND E.DT_END_EFF >    :WS-DT-PTF-X
		//                        AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                        AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                        AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
		//                             OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
		//                        )
		//                    AND A.DS_STATO_ELAB IN (
		//                                            :IABV0002-STATE-01,
		//                                            :IABV0002-STATE-02,
		//                                            :IABV0002-STATE-03,
		//                                            :IABV0002-STATE-04,
		//                                            :IABV0002-STATE-05,
		//                                            :IABV0002-STATE-06,
		//                                            :IABV0002-STATE-07,
		//                                            :IABV0002-STATE-08,
		//                                            :IABV0002-STATE-09,
		//                                            :IABV0002-STATE-10
		//                                              )
		//                    AND NOT A.DS_VER  = :IABV0009-VERSIONING
		//                    ORDER BY A.ID_POLI, A.ID_ADES
		//                   FETCH FIRST ROW ONLY
		//                END-EXEC.
		adesPoliStatOggBusTrchDiGarDao.selectRec3(this.getAdesPoliStatOggBusTrchDiGarLdbm02502());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			z100SetColonneNull();
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		}
	}

	/**Original name: A320-UPDATE-SC07<br>
	 * <pre>*****************************************************************</pre>*/
	private void a320UpdateSc07() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-PRIMARY-KEY
		//                   PERFORM A330-UPDATE-PK-SC07         THRU A330-SC07-EX
		//              WHEN IDSV0003-WHERE-CONDITION-07
		//                   PERFORM A340-UPDATE-WHERE-COND-SC07 THRU A340-SC07-EX
		//              WHEN IDSV0003-FIRST-ACTION
		//                                                       THRU A345-SC07-EX
		//              WHEN OTHER
		//                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
		//           END-EVALUATE.
		if (idsv0003.getLivelloOperazione().isIdsv0003PrimaryKey()) {
			// COB_CODE: PERFORM A330-UPDATE-PK-SC07         THRU A330-SC07-EX
			a330UpdatePkSc07();
		} else if (idsv0003.getTipologiaOperazione().isIdsv0003WhereCondition07()) {
			// COB_CODE: PERFORM A340-UPDATE-WHERE-COND-SC07 THRU A340-SC07-EX
			a340UpdateWhereCondSc07();
		} else if (idsv0003.getLivelloOperazione().isIdsv0003FirstAction()) {
			// COB_CODE: PERFORM A345-UPDATE-FIRST-ACTION-SC07
			//                                               THRU A345-SC07-EX
			a345UpdateFirstActionSc07();
		} else {
			// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
			idsv0003.getReturnCode().setInvalidLevelOper();
		}
	}

	/**Original name: A330-UPDATE-PK-SC07<br>
	 * <pre>*****************************************************************</pre>*/
	private void a330UpdatePkSc07() {
		// COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
		z150ValorizzaDataServices();
		// COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
		z200SetIndicatoriNull();
		// COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBM0250.cbl:line=9369, because the code is unreachable.
		// COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
		z960LengthVchar();
		// COB_CODE: EXEC SQL
		//                UPDATE ADES
		//                SET
		//                   DS_OPER_SQL            = :ADE-DS-OPER-SQL
		//                  ,DS_VER                 = :IABV0009-VERSIONING
		//                  ,DS_UTENTE              = :ADE-DS-UTENTE
		//                  ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
		//                WHERE             DS_RIGA = :ADE-DS-RIGA
		//           END-EXEC.
		adesDao.updateRec3(this);
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A340-UPDATE-WHERE-COND-SC07<br>
	 * <pre>*****************************************************************</pre>*/
	private void a340UpdateWhereCondSc07() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A345-UPDATE-FIRST-ACTION-SC07<br>
	 * <pre>*****************************************************************</pre>*/
	private void a345UpdateFirstActionSc07() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A360-OPEN-CURSOR-SC07<br>
	 * <pre>*****************************************************************</pre>*/
	private void a360OpenCursorSc07() {
		// COB_CODE: PERFORM A305-DECLARE-CURSOR-SC07 THRU A305-SC07-EX.
		a305DeclareCursorSc07();
		// COB_CODE: EXEC SQL
		//               OPEN CUR-SC07
		//           END-EXEC.
		adesPoliStatOggBusTrchDiGarDao.openCurSc07(this.getAdesPoliStatOggBusTrchDiGarLdbm02502());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A370-CLOSE-CURSOR-SC07<br>
	 * <pre>*****************************************************************</pre>*/
	private void a370CloseCursorSc07() {
		// COB_CODE: EXEC SQL
		//                CLOSE CUR-SC07
		//           END-EXEC.
		adesPoliStatOggBusTrchDiGarDao.closeCurSc07();
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A380-FETCH-FIRST-SC07<br>
	 * <pre>*****************************************************************</pre>*/
	private void a380FetchFirstSc07() {
		// COB_CODE: PERFORM A360-OPEN-CURSOR-SC07    THRU A360-SC07-EX.
		a360OpenCursorSc07();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM A390-FETCH-NEXT-SC07  THRU A390-SC07-EX
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM A390-FETCH-NEXT-SC07  THRU A390-SC07-EX
			a390FetchNextSc07();
		}
	}

	/**Original name: A390-FETCH-NEXT-SC07<br>
	 * <pre>*****************************************************************</pre>*/
	private void a390FetchNextSc07() {
		// COB_CODE: EXEC SQL
		//                FETCH CUR-SC07
		//           INTO
		//                   :ADE-ID-ADES
		//                  ,:ADE-ID-POLI
		//                  ,:ADE-ID-MOVI-CRZ
		//                  ,:ADE-ID-MOVI-CHIU
		//                   :IND-ADE-ID-MOVI-CHIU
		//                  ,:ADE-DT-INI-EFF-DB
		//                  ,:ADE-DT-END-EFF-DB
		//                  ,:ADE-IB-PREV
		//                   :IND-ADE-IB-PREV
		//                  ,:ADE-IB-OGG
		//                   :IND-ADE-IB-OGG
		//                  ,:ADE-COD-COMP-ANIA
		//                  ,:ADE-DT-DECOR-DB
		//                   :IND-ADE-DT-DECOR
		//                  ,:ADE-DT-SCAD-DB
		//                   :IND-ADE-DT-SCAD
		//                  ,:ADE-ETA-A-SCAD
		//                   :IND-ADE-ETA-A-SCAD
		//                  ,:ADE-DUR-AA
		//                   :IND-ADE-DUR-AA
		//                  ,:ADE-DUR-MM
		//                   :IND-ADE-DUR-MM
		//                  ,:ADE-DUR-GG
		//                   :IND-ADE-DUR-GG
		//                  ,:ADE-TP-RGM-FISC
		//                  ,:ADE-TP-RIAT
		//                   :IND-ADE-TP-RIAT
		//                  ,:ADE-TP-MOD-PAG-TIT
		//                  ,:ADE-TP-IAS
		//                   :IND-ADE-TP-IAS
		//                  ,:ADE-DT-VARZ-TP-IAS-DB
		//                   :IND-ADE-DT-VARZ-TP-IAS
		//                  ,:ADE-PRE-NET-IND
		//                   :IND-ADE-PRE-NET-IND
		//                  ,:ADE-PRE-LRD-IND
		//                   :IND-ADE-PRE-LRD-IND
		//                  ,:ADE-RAT-LRD-IND
		//                   :IND-ADE-RAT-LRD-IND
		//                  ,:ADE-PRSTZ-INI-IND
		//                   :IND-ADE-PRSTZ-INI-IND
		//                  ,:ADE-FL-COINC-ASSTO
		//                   :IND-ADE-FL-COINC-ASSTO
		//                  ,:ADE-IB-DFLT
		//                   :IND-ADE-IB-DFLT
		//                  ,:ADE-MOD-CALC
		//                   :IND-ADE-MOD-CALC
		//                  ,:ADE-TP-FNT-CNBTVA
		//                   :IND-ADE-TP-FNT-CNBTVA
		//                  ,:ADE-IMP-AZ
		//                   :IND-ADE-IMP-AZ
		//                  ,:ADE-IMP-ADER
		//                   :IND-ADE-IMP-ADER
		//                  ,:ADE-IMP-TFR
		//                   :IND-ADE-IMP-TFR
		//                  ,:ADE-IMP-VOLO
		//                   :IND-ADE-IMP-VOLO
		//                  ,:ADE-PC-AZ
		//                   :IND-ADE-PC-AZ
		//                  ,:ADE-PC-ADER
		//                   :IND-ADE-PC-ADER
		//                  ,:ADE-PC-TFR
		//                   :IND-ADE-PC-TFR
		//                  ,:ADE-PC-VOLO
		//                   :IND-ADE-PC-VOLO
		//                  ,:ADE-DT-NOVA-RGM-FISC-DB
		//                   :IND-ADE-DT-NOVA-RGM-FISC
		//                  ,:ADE-FL-ATTIV
		//                   :IND-ADE-FL-ATTIV
		//                  ,:ADE-IMP-REC-RIT-VIS
		//                   :IND-ADE-IMP-REC-RIT-VIS
		//                  ,:ADE-IMP-REC-RIT-ACC
		//                   :IND-ADE-IMP-REC-RIT-ACC
		//                  ,:ADE-FL-VARZ-STAT-TBGC
		//                   :IND-ADE-FL-VARZ-STAT-TBGC
		//                  ,:ADE-FL-PROVZA-MIGRAZ
		//                   :IND-ADE-FL-PROVZA-MIGRAZ
		//                  ,:ADE-IMPB-VIS-DA-REC
		//                   :IND-ADE-IMPB-VIS-DA-REC
		//                  ,:ADE-DT-DECOR-PREST-BAN-DB
		//                   :IND-ADE-DT-DECOR-PREST-BAN
		//                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
		//                   :IND-ADE-DT-EFF-VARZ-STAT-T
		//                  ,:ADE-DS-RIGA
		//                  ,:ADE-DS-OPER-SQL
		//                  ,:ADE-DS-VER
		//                  ,:ADE-DS-TS-INI-CPTZ
		//                  ,:ADE-DS-TS-END-CPTZ
		//                  ,:ADE-DS-UTENTE
		//                  ,:ADE-DS-STATO-ELAB
		//                  ,:ADE-CUM-CNBT-CAP
		//                   :IND-ADE-CUM-CNBT-CAP
		//                  ,:ADE-IMP-GAR-CNBT
		//                   :IND-ADE-IMP-GAR-CNBT
		//                  ,:ADE-DT-ULT-CONS-CNBT-DB
		//                   :IND-ADE-DT-ULT-CONS-CNBT
		//                  ,:ADE-IDEN-ISC-FND
		//                   :IND-ADE-IDEN-ISC-FND
		//                  ,:ADE-NUM-RAT-PIAN
		//                   :IND-ADE-NUM-RAT-PIAN
		//                  ,:ADE-DT-PRESC-DB
		//                   :IND-ADE-DT-PRESC
		//                  ,:ADE-CONCS-PREST
		//                   :IND-ADE-CONCS-PREST
		//                  ,:POL-ID-POLI
		//                  ,:POL-ID-MOVI-CRZ
		//                  ,:POL-ID-MOVI-CHIU
		//                   :IND-POL-ID-MOVI-CHIU
		//                  ,:POL-IB-OGG
		//                   :IND-POL-IB-OGG
		//                  ,:POL-IB-PROP
		//                  ,:POL-DT-PROP-DB
		//                   :IND-POL-DT-PROP
		//                  ,:POL-DT-INI-EFF-DB
		//                  ,:POL-DT-END-EFF-DB
		//                  ,:POL-COD-COMP-ANIA
		//                  ,:POL-DT-DECOR-DB
		//                  ,:POL-DT-EMIS-DB
		//                  ,:POL-TP-POLI
		//                  ,:POL-DUR-AA
		//                   :IND-POL-DUR-AA
		//                  ,:POL-DUR-MM
		//                   :IND-POL-DUR-MM
		//                  ,:POL-DT-SCAD-DB
		//                   :IND-POL-DT-SCAD
		//                  ,:POL-COD-PROD
		//                  ,:POL-DT-INI-VLDT-PROD-DB
		//                  ,:POL-COD-CONV
		//                   :IND-POL-COD-CONV
		//                  ,:POL-COD-RAMO
		//                   :IND-POL-COD-RAMO
		//                  ,:POL-DT-INI-VLDT-CONV-DB
		//                   :IND-POL-DT-INI-VLDT-CONV
		//                  ,:POL-DT-APPLZ-CONV-DB
		//                   :IND-POL-DT-APPLZ-CONV
		//                  ,:POL-TP-FRM-ASSVA
		//                  ,:POL-TP-RGM-FISC
		//                   :IND-POL-TP-RGM-FISC
		//                  ,:POL-FL-ESTAS
		//                   :IND-POL-FL-ESTAS
		//                  ,:POL-FL-RSH-COMUN
		//                   :IND-POL-FL-RSH-COMUN
		//                  ,:POL-FL-RSH-COMUN-COND
		//                   :IND-POL-FL-RSH-COMUN-COND
		//                  ,:POL-TP-LIV-GENZ-TIT
		//                  ,:POL-FL-COP-FINANZ
		//                   :IND-POL-FL-COP-FINANZ
		//                  ,:POL-TP-APPLZ-DIR
		//                   :IND-POL-TP-APPLZ-DIR
		//                  ,:POL-SPE-MED
		//                   :IND-POL-SPE-MED
		//                  ,:POL-DIR-EMIS
		//                   :IND-POL-DIR-EMIS
		//                  ,:POL-DIR-1O-VERS
		//                   :IND-POL-DIR-1O-VERS
		//                  ,:POL-DIR-VERS-AGG
		//                   :IND-POL-DIR-VERS-AGG
		//                  ,:POL-COD-DVS
		//                   :IND-POL-COD-DVS
		//                  ,:POL-FL-FNT-AZ
		//                   :IND-POL-FL-FNT-AZ
		//                  ,:POL-FL-FNT-ADER
		//                   :IND-POL-FL-FNT-ADER
		//                  ,:POL-FL-FNT-TFR
		//                   :IND-POL-FL-FNT-TFR
		//                  ,:POL-FL-FNT-VOLO
		//                   :IND-POL-FL-FNT-VOLO
		//                  ,:POL-TP-OPZ-A-SCAD
		//                   :IND-POL-TP-OPZ-A-SCAD
		//                  ,:POL-AA-DIFF-PROR-DFLT
		//                   :IND-POL-AA-DIFF-PROR-DFLT
		//                  ,:POL-FL-VER-PROD
		//                   :IND-POL-FL-VER-PROD
		//                  ,:POL-DUR-GG
		//                   :IND-POL-DUR-GG
		//                  ,:POL-DIR-QUIET
		//                   :IND-POL-DIR-QUIET
		//                  ,:POL-TP-PTF-ESTNO
		//                   :IND-POL-TP-PTF-ESTNO
		//                  ,:POL-FL-CUM-PRE-CNTR
		//                   :IND-POL-FL-CUM-PRE-CNTR
		//                  ,:POL-FL-AMMB-MOVI
		//                   :IND-POL-FL-AMMB-MOVI
		//                  ,:POL-CONV-GECO
		//                   :IND-POL-CONV-GECO
		//                  ,:POL-DS-RIGA
		//                  ,:POL-DS-OPER-SQL
		//                  ,:POL-DS-VER
		//                  ,:POL-DS-TS-INI-CPTZ
		//                  ,:POL-DS-TS-END-CPTZ
		//                  ,:POL-DS-UTENTE
		//                  ,:POL-DS-STATO-ELAB
		//                  ,:POL-FL-SCUDO-FISC
		//                   :IND-POL-FL-SCUDO-FISC
		//                  ,:POL-FL-TRASFE
		//                   :IND-POL-FL-TRASFE
		//                  ,:POL-FL-TFR-STRC
		//                   :IND-POL-FL-TFR-STRC
		//                  ,:POL-DT-PRESC-DB
		//                   :IND-POL-DT-PRESC
		//                  ,:POL-COD-CONV-AGG
		//                   :IND-POL-COD-CONV-AGG
		//                  ,:POL-SUBCAT-PROD
		//                   :IND-POL-SUBCAT-PROD
		//                  ,:POL-FL-QUEST-ADEGZ-ASS
		//                   :IND-POL-FL-QUEST-ADEGZ-ASS
		//                  ,:POL-COD-TPA
		//                   :IND-POL-COD-TPA
		//                  ,:POL-ID-ACC-COMM
		//                   :IND-POL-ID-ACC-COMM
		//                  ,:POL-FL-POLI-CPI-PR
		//                   :IND-POL-FL-POLI-CPI-PR
		//                  ,:POL-FL-POLI-BUNDLING
		//                   :IND-POL-FL-POLI-BUNDLING
		//                  ,:POL-IND-POLI-PRIN-COLL
		//                   :IND-POL-IND-POLI-PRIN-COLL
		//                  ,:POL-FL-VND-BUNDLE
		//                   :IND-POL-FL-VND-BUNDLE
		//                  ,:POL-IB-BS
		//                   :IND-POL-IB-BS
		//                  ,:POL-FL-POLI-IFP
		//                   :IND-POL-FL-POLI-IFP
		//                  ,:STB-ID-STAT-OGG-BUS
		//                  ,:STB-ID-OGG
		//                  ,:STB-TP-OGG
		//                  ,:STB-ID-MOVI-CRZ
		//                  ,:STB-ID-MOVI-CHIU
		//                   :IND-STB-ID-MOVI-CHIU
		//                  ,:STB-DT-INI-EFF-DB
		//                  ,:STB-DT-END-EFF-DB
		//                  ,:STB-COD-COMP-ANIA
		//                  ,:STB-TP-STAT-BUS
		//                  ,:STB-TP-CAUS
		//                  ,:STB-DS-RIGA
		//                  ,:STB-DS-OPER-SQL
		//                  ,:STB-DS-VER
		//                  ,:STB-DS-TS-INI-CPTZ
		//                  ,:STB-DS-TS-END-CPTZ
		//                  ,:STB-DS-UTENTE
		//                  ,:STB-DS-STATO-ELAB
		//           END-EXEC.
		adesPoliStatOggBusTrchDiGarDao.fetchCurSc07(this.getAdesPoliStatOggBusTrchDiGarLdbm0250());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           ELSE
		//             END-IF
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			z100SetColonneNull();
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		} else if (idsv0003.getSqlcode().isNotFound()) {
			// COB_CODE: IF IDSV0003-NOT-FOUND
			//              END-IF
			//           END-IF
			// COB_CODE: PERFORM A370-CLOSE-CURSOR-SC07 THRU A370-SC07-EX
			a370CloseCursorSc07();
			// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
			//              SET IDSV0003-NOT-FOUND TO TRUE
			//           END-IF
			if (idsv0003.getSqlcode().isSuccessfulSql()) {
				// COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
				idsv0003.getSqlcode().setNotFound();
			}
		}
	}

	/**Original name: SC08-SELECTION-CURSOR-08<br>
	 * <pre>*****************************************************************
	 * *****************************************************************</pre>*/
	private void sc08SelectionCursor08() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-SELECT
		//                 PERFORM A310-SELECT-SC08           THRU A310-SC08-EX
		//              WHEN IDSV0003-OPEN-CURSOR
		//                 PERFORM A360-OPEN-CURSOR-SC08      THRU A360-SC08-EX
		//              WHEN IDSV0003-CLOSE-CURSOR
		//                 PERFORM A370-CLOSE-CURSOR-SC08     THRU A370-SC08-EX
		//              WHEN IDSV0003-FETCH-FIRST
		//                 PERFORM A380-FETCH-FIRST-SC08      THRU A380-SC08-EX
		//              WHEN IDSV0003-FETCH-NEXT
		//                 PERFORM A390-FETCH-NEXT-SC08       THRU A390-SC08-EX
		//              WHEN IDSV0003-UPDATE
		//                 PERFORM A320-UPDATE-SC08           THRU A320-SC08-EX
		//              WHEN OTHER
		//                 SET IDSV0003-INVALID-OPER          TO TRUE
		//           END-EVALUATE.
		if (idsv0003.getOperazione().isSelect()) {
			// COB_CODE: PERFORM A310-SELECT-SC08           THRU A310-SC08-EX
			a310SelectSc08();
		} else if (idsv0003.getOperazione().isOpenCursor()) {
			// COB_CODE: PERFORM A360-OPEN-CURSOR-SC08      THRU A360-SC08-EX
			a360OpenCursorSc08();
		} else if (idsv0003.getOperazione().isCloseCursor()) {
			// COB_CODE: PERFORM A370-CLOSE-CURSOR-SC08     THRU A370-SC08-EX
			a370CloseCursorSc08();
		} else if (idsv0003.getOperazione().isFetchFirst()) {
			// COB_CODE: PERFORM A380-FETCH-FIRST-SC08      THRU A380-SC08-EX
			a380FetchFirstSc08();
		} else if (idsv0003.getOperazione().isFetchNext()) {
			// COB_CODE: PERFORM A390-FETCH-NEXT-SC08       THRU A390-SC08-EX
			a390FetchNextSc08();
		} else if (idsv0003.getOperazione().isUpdate()) {
			// COB_CODE: PERFORM A320-UPDATE-SC08           THRU A320-SC08-EX
			a320UpdateSc08();
		} else {
			// COB_CODE: SET IDSV0003-INVALID-OPER          TO TRUE
			idsv0003.getReturnCode().setInvalidOper();
		}
	}

	/**Original name: A305-DECLARE-CURSOR-SC08<br>
	 * <pre>*****************************************************************</pre>*/
	private void a305DeclareCursorSc08() {
		// COB_CODE:      EXEC SQL
		//                     DECLARE CUR-SC08 CURSOR WITH HOLD FOR
		//                  SELECT
		//                         A.ID_ADES
		//                        ,A.ID_POLI
		//                        ,A.ID_MOVI_CRZ
		//                        ,A.ID_MOVI_CHIU
		//                        ,A.DT_INI_EFF
		//                        ,A.DT_END_EFF
		//                        ,A.IB_PREV
		//                        ,A.IB_OGG
		//                        ,A.COD_COMP_ANIA
		//                        ,A.DT_DECOR
		//                        ,A.DT_SCAD
		//                        ,A.ETA_A_SCAD
		//                        ,A.DUR_AA
		//                        ,A.DUR_MM
		//                        ,A.DUR_GG
		//                        ,A.TP_RGM_FISC
		//                        ,A.TP_RIAT
		//                        ,A.TP_MOD_PAG_TIT
		//                        ,A.TP_IAS
		//                        ,A.DT_VARZ_TP_IAS
		//                        ,A.PRE_NET_IND
		//                        ,A.PRE_LRD_IND
		//                        ,A.RAT_LRD_IND
		//                        ,A.PRSTZ_INI_IND
		//                        ,A.FL_COINC_ASSTO
		//                        ,A.IB_DFLT
		//                        ,A.MOD_CALC
		//                        ,A.TP_FNT_CNBTVA
		//                        ,A.IMP_AZ
		//                        ,A.IMP_ADER
		//                        ,A.IMP_TFR
		//                        ,A.IMP_VOLO
		//                        ,A.PC_AZ
		//                        ,A.PC_ADER
		//                        ,A.PC_TFR
		//                        ,A.PC_VOLO
		//                        ,A.DT_NOVA_RGM_FISC
		//                        ,A.FL_ATTIV
		//                        ,A.IMP_REC_RIT_VIS
		//                        ,A.IMP_REC_RIT_ACC
		//                        ,A.FL_VARZ_STAT_TBGC
		//                        ,A.FL_PROVZA_MIGRAZ
		//                        ,A.IMPB_VIS_DA_REC
		//                        ,A.DT_DECOR_PREST_BAN
		//                        ,A.DT_EFF_VARZ_STAT_T
		//                        ,A.DS_RIGA
		//                        ,A.DS_OPER_SQL
		//                        ,A.DS_VER
		//                        ,A.DS_TS_INI_CPTZ
		//                        ,A.DS_TS_END_CPTZ
		//                        ,A.DS_UTENTE
		//                        ,A.DS_STATO_ELAB
		//                        ,A.CUM_CNBT_CAP
		//                        ,A.IMP_GAR_CNBT
		//                        ,A.DT_ULT_CONS_CNBT
		//                        ,A.IDEN_ISC_FND
		//                        ,A.NUM_RAT_PIAN
		//                        ,A.DT_PRESC
		//                        ,A.CONCS_PREST
		//                        ,B.ID_POLI
		//                        ,B.ID_MOVI_CRZ
		//                        ,B.ID_MOVI_CHIU
		//                        ,B.IB_OGG
		//                        ,B.IB_PROP
		//                        ,B.DT_PROP
		//                        ,B.DT_INI_EFF
		//                        ,B.DT_END_EFF
		//                        ,B.COD_COMP_ANIA
		//                        ,B.DT_DECOR
		//                        ,B.DT_EMIS
		//                        ,B.TP_POLI
		//                        ,B.DUR_AA
		//                        ,B.DUR_MM
		//                        ,B.DT_SCAD
		//                        ,B.COD_PROD
		//                        ,B.DT_INI_VLDT_PROD
		//                        ,B.COD_CONV
		//                        ,B.COD_RAMO
		//                        ,B.DT_INI_VLDT_CONV
		//                        ,B.DT_APPLZ_CONV
		//                        ,B.TP_FRM_ASSVA
		//                        ,B.TP_RGM_FISC
		//                        ,B.FL_ESTAS
		//                        ,B.FL_RSH_COMUN
		//                        ,B.FL_RSH_COMUN_COND
		//                        ,B.TP_LIV_GENZ_TIT
		//                        ,B.FL_COP_FINANZ
		//                        ,B.TP_APPLZ_DIR
		//                        ,B.SPE_MED
		//                        ,B.DIR_EMIS
		//                        ,B.DIR_1O_VERS
		//                        ,B.DIR_VERS_AGG
		//                        ,B.COD_DVS
		//                        ,B.FL_FNT_AZ
		//                        ,B.FL_FNT_ADER
		//                        ,B.FL_FNT_TFR
		//                        ,B.FL_FNT_VOLO
		//                        ,B.TP_OPZ_A_SCAD
		//                        ,B.AA_DIFF_PROR_DFLT
		//                        ,B.FL_VER_PROD
		//                        ,B.DUR_GG
		//                        ,B.DIR_QUIET
		//                        ,B.TP_PTF_ESTNO
		//                        ,B.FL_CUM_PRE_CNTR
		//                        ,B.FL_AMMB_MOVI
		//                        ,B.CONV_GECO
		//                        ,B.DS_RIGA
		//                        ,B.DS_OPER_SQL
		//                        ,B.DS_VER
		//                        ,B.DS_TS_INI_CPTZ
		//                        ,B.DS_TS_END_CPTZ
		//                        ,B.DS_UTENTE
		//                        ,B.DS_STATO_ELAB
		//                        ,B.FL_SCUDO_FISC
		//                        ,B.FL_TRASFE
		//                        ,B.FL_TFR_STRC
		//                        ,B.DT_PRESC
		//                        ,B.COD_CONV_AGG
		//                        ,B.SUBCAT_PROD
		//                        ,B.FL_QUEST_ADEGZ_ASS
		//                        ,B.COD_TPA
		//                        ,B.ID_ACC_COMM
		//                        ,B.FL_POLI_CPI_PR
		//                        ,B.FL_POLI_BUNDLING
		//                        ,B.IND_POLI_PRIN_COLL
		//                        ,B.FL_VND_BUNDLE
		//                        ,B.IB_BS
		//                        ,B.FL_POLI_IFP
		//                        ,C.ID_STAT_OGG_BUS
		//                        ,C.ID_OGG
		//                        ,C.TP_OGG
		//                        ,C.ID_MOVI_CRZ
		//                        ,C.ID_MOVI_CHIU
		//                        ,C.DT_INI_EFF
		//                        ,C.DT_END_EFF
		//                        ,C.COD_COMP_ANIA
		//                        ,C.TP_STAT_BUS
		//                        ,C.TP_CAUS
		//                        ,C.DS_RIGA
		//                        ,C.DS_OPER_SQL
		//                        ,C.DS_VER
		//                        ,C.DS_TS_INI_CPTZ
		//                        ,C.DS_TS_END_CPTZ
		//                        ,C.DS_UTENTE
		//                        ,C.DS_STATO_ELAB
		//                  FROM ADES      A,
		//                       POLI      B,
		//                       STAT_OGG_BUS C
		//                  WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
		//                                             :WS-TP-FRM-ASSVA2 )
		//                    AND B.IB_OGG BETWEEN  :WLB-IB-POLI-FIRST AND
		//                                             :WLB-IB-POLI-LAST
		//                    AND A.IB_OGG BETWEEN  :WLB-IB-ADE-FIRST AND
		//                                             :WLB-IB-ADE-LAST
		//                    AND A.ID_POLI      =   B.ID_POLI
		//                    AND A.ID_ADES      =   C.ID_OGG
		//                    AND C.TP_OGG       =  'AD'
		//           *--  D C.TP_STAT_BUS     =  'VI'
		//                    AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
		//                    AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
		//                    AND A.DT_END_EFF  >   :WS-DT-PTF-X
		//                    AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                    AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                    AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
		//                    AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
		//                    AND B.DT_END_EFF  >   :WS-DT-PTF-X
		//                    AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                    AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                    AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
		//                    AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
		//                    AND C.DT_END_EFF  >   :WS-DT-PTF-X
		//                    AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                    AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                    AND A.ID_ADES IN
		//                       (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
		//                                                        STAT_OGG_BUS E
		//                        WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
		//                        AND E.TP_OGG = 'TG'
		//                        AND E.TP_STAT_BUS IN ('VI','ST')
		//                        AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
		//                        AND D.COD_COMP_ANIA =
		//                            :IDSV0003-CODICE-COMPAGNIA-ANIA
		//                        AND D.DT_INI_EFF <=   :WS-DT-PTF-X
		//                        AND D.DT_END_EFF >    :WS-DT-PTF-X
		//                        AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                        AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                        AND E.DT_INI_EFF <=   :WS-DT-PTF-X
		//                        AND E.DT_END_EFF >    :WS-DT-PTF-X
		//                        AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                        AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                        AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
		//                             OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
		//                        )
		//                    AND A.DS_STATO_ELAB IN (
		//                                            :IABV0002-STATE-01,
		//                                            :IABV0002-STATE-02,
		//                                            :IABV0002-STATE-03,
		//                                            :IABV0002-STATE-04,
		//                                            :IABV0002-STATE-05,
		//                                            :IABV0002-STATE-06,
		//                                            :IABV0002-STATE-07,
		//                                            :IABV0002-STATE-08,
		//                                            :IABV0002-STATE-09,
		//                                            :IABV0002-STATE-10
		//                                              )
		//                    AND NOT A.DS_VER  = :IABV0009-VERSIONING
		//                    ORDER BY A.ID_POLI, A.ID_ADES
		//                END-EXEC.
		// DECLARE CURSOR doesn't need a translation;
	}

	/**Original name: A310-SELECT-SC08<br>
	 * <pre>*****************************************************************</pre>*/
	private void a310SelectSc08() {
		// COB_CODE:      EXEC SQL
		//                  SELECT
		//                         A.ID_ADES
		//                        ,A.ID_POLI
		//                        ,A.ID_MOVI_CRZ
		//                        ,A.ID_MOVI_CHIU
		//                        ,A.DT_INI_EFF
		//                        ,A.DT_END_EFF
		//                        ,A.IB_PREV
		//                        ,A.IB_OGG
		//                        ,A.COD_COMP_ANIA
		//                        ,A.DT_DECOR
		//                        ,A.DT_SCAD
		//                        ,A.ETA_A_SCAD
		//                        ,A.DUR_AA
		//                        ,A.DUR_MM
		//                        ,A.DUR_GG
		//                        ,A.TP_RGM_FISC
		//                        ,A.TP_RIAT
		//                        ,A.TP_MOD_PAG_TIT
		//                        ,A.TP_IAS
		//                        ,A.DT_VARZ_TP_IAS
		//                        ,A.PRE_NET_IND
		//                        ,A.PRE_LRD_IND
		//                        ,A.RAT_LRD_IND
		//                        ,A.PRSTZ_INI_IND
		//                        ,A.FL_COINC_ASSTO
		//                        ,A.IB_DFLT
		//                        ,A.MOD_CALC
		//                        ,A.TP_FNT_CNBTVA
		//                        ,A.IMP_AZ
		//                        ,A.IMP_ADER
		//                        ,A.IMP_TFR
		//                        ,A.IMP_VOLO
		//                        ,A.PC_AZ
		//                        ,A.PC_ADER
		//                        ,A.PC_TFR
		//                        ,A.PC_VOLO
		//                        ,A.DT_NOVA_RGM_FISC
		//                        ,A.FL_ATTIV
		//                        ,A.IMP_REC_RIT_VIS
		//                        ,A.IMP_REC_RIT_ACC
		//                        ,A.FL_VARZ_STAT_TBGC
		//                        ,A.FL_PROVZA_MIGRAZ
		//                        ,A.IMPB_VIS_DA_REC
		//                        ,A.DT_DECOR_PREST_BAN
		//                        ,A.DT_EFF_VARZ_STAT_T
		//                        ,A.DS_RIGA
		//                        ,A.DS_OPER_SQL
		//                        ,A.DS_VER
		//                        ,A.DS_TS_INI_CPTZ
		//                        ,A.DS_TS_END_CPTZ
		//                        ,A.DS_UTENTE
		//                        ,A.DS_STATO_ELAB
		//                        ,A.CUM_CNBT_CAP
		//                        ,A.IMP_GAR_CNBT
		//                        ,A.DT_ULT_CONS_CNBT
		//                        ,A.IDEN_ISC_FND
		//                        ,A.NUM_RAT_PIAN
		//                        ,A.DT_PRESC
		//                        ,A.CONCS_PREST
		//                        ,B.ID_POLI
		//                        ,B.ID_MOVI_CRZ
		//                        ,B.ID_MOVI_CHIU
		//                        ,B.IB_OGG
		//                        ,B.IB_PROP
		//                        ,B.DT_PROP
		//                        ,B.DT_INI_EFF
		//                        ,B.DT_END_EFF
		//                        ,B.COD_COMP_ANIA
		//                        ,B.DT_DECOR
		//                        ,B.DT_EMIS
		//                        ,B.TP_POLI
		//                        ,B.DUR_AA
		//                        ,B.DUR_MM
		//                        ,B.DT_SCAD
		//                        ,B.COD_PROD
		//                        ,B.DT_INI_VLDT_PROD
		//                        ,B.COD_CONV
		//                        ,B.COD_RAMO
		//                        ,B.DT_INI_VLDT_CONV
		//                        ,B.DT_APPLZ_CONV
		//                        ,B.TP_FRM_ASSVA
		//                        ,B.TP_RGM_FISC
		//                        ,B.FL_ESTAS
		//                        ,B.FL_RSH_COMUN
		//                        ,B.FL_RSH_COMUN_COND
		//                        ,B.TP_LIV_GENZ_TIT
		//                        ,B.FL_COP_FINANZ
		//                        ,B.TP_APPLZ_DIR
		//                        ,B.SPE_MED
		//                        ,B.DIR_EMIS
		//                        ,B.DIR_1O_VERS
		//                        ,B.DIR_VERS_AGG
		//                        ,B.COD_DVS
		//                        ,B.FL_FNT_AZ
		//                        ,B.FL_FNT_ADER
		//                        ,B.FL_FNT_TFR
		//                        ,B.FL_FNT_VOLO
		//                        ,B.TP_OPZ_A_SCAD
		//                        ,B.AA_DIFF_PROR_DFLT
		//                        ,B.FL_VER_PROD
		//                        ,B.DUR_GG
		//                        ,B.DIR_QUIET
		//                        ,B.TP_PTF_ESTNO
		//                        ,B.FL_CUM_PRE_CNTR
		//                        ,B.FL_AMMB_MOVI
		//                        ,B.CONV_GECO
		//                        ,B.DS_RIGA
		//                        ,B.DS_OPER_SQL
		//                        ,B.DS_VER
		//                        ,B.DS_TS_INI_CPTZ
		//                        ,B.DS_TS_END_CPTZ
		//                        ,B.DS_UTENTE
		//                        ,B.DS_STATO_ELAB
		//                        ,B.FL_SCUDO_FISC
		//                        ,B.FL_TRASFE
		//                        ,B.FL_TFR_STRC
		//                        ,B.DT_PRESC
		//                        ,B.COD_CONV_AGG
		//                        ,B.SUBCAT_PROD
		//                        ,B.FL_QUEST_ADEGZ_ASS
		//                        ,B.COD_TPA
		//                        ,B.ID_ACC_COMM
		//                        ,B.FL_POLI_CPI_PR
		//                        ,B.FL_POLI_BUNDLING
		//                        ,B.IND_POLI_PRIN_COLL
		//                        ,B.FL_VND_BUNDLE
		//                        ,B.IB_BS
		//                        ,B.FL_POLI_IFP
		//                        ,C.ID_STAT_OGG_BUS
		//                        ,C.ID_OGG
		//                        ,C.TP_OGG
		//                        ,C.ID_MOVI_CRZ
		//                        ,C.ID_MOVI_CHIU
		//                        ,C.DT_INI_EFF
		//                        ,C.DT_END_EFF
		//                        ,C.COD_COMP_ANIA
		//                        ,C.TP_STAT_BUS
		//                        ,C.TP_CAUS
		//                        ,C.DS_RIGA
		//                        ,C.DS_OPER_SQL
		//                        ,C.DS_VER
		//                        ,C.DS_TS_INI_CPTZ
		//                        ,C.DS_TS_END_CPTZ
		//                        ,C.DS_UTENTE
		//                        ,C.DS_STATO_ELAB
		//                  INTO
		//                        :ADE-ID-ADES
		//                       ,:ADE-ID-POLI
		//                       ,:ADE-ID-MOVI-CRZ
		//                       ,:ADE-ID-MOVI-CHIU
		//                        :IND-ADE-ID-MOVI-CHIU
		//                       ,:ADE-DT-INI-EFF-DB
		//                       ,:ADE-DT-END-EFF-DB
		//                       ,:ADE-IB-PREV
		//                        :IND-ADE-IB-PREV
		//                       ,:ADE-IB-OGG
		//                        :IND-ADE-IB-OGG
		//                       ,:ADE-COD-COMP-ANIA
		//                       ,:ADE-DT-DECOR-DB
		//                        :IND-ADE-DT-DECOR
		//                       ,:ADE-DT-SCAD-DB
		//                        :IND-ADE-DT-SCAD
		//                       ,:ADE-ETA-A-SCAD
		//                        :IND-ADE-ETA-A-SCAD
		//                       ,:ADE-DUR-AA
		//                        :IND-ADE-DUR-AA
		//                       ,:ADE-DUR-MM
		//                        :IND-ADE-DUR-MM
		//                       ,:ADE-DUR-GG
		//                        :IND-ADE-DUR-GG
		//                       ,:ADE-TP-RGM-FISC
		//                       ,:ADE-TP-RIAT
		//                        :IND-ADE-TP-RIAT
		//                       ,:ADE-TP-MOD-PAG-TIT
		//                       ,:ADE-TP-IAS
		//                        :IND-ADE-TP-IAS
		//                       ,:ADE-DT-VARZ-TP-IAS-DB
		//                        :IND-ADE-DT-VARZ-TP-IAS
		//                       ,:ADE-PRE-NET-IND
		//                        :IND-ADE-PRE-NET-IND
		//                       ,:ADE-PRE-LRD-IND
		//                        :IND-ADE-PRE-LRD-IND
		//                       ,:ADE-RAT-LRD-IND
		//                        :IND-ADE-RAT-LRD-IND
		//                       ,:ADE-PRSTZ-INI-IND
		//                        :IND-ADE-PRSTZ-INI-IND
		//                       ,:ADE-FL-COINC-ASSTO
		//                        :IND-ADE-FL-COINC-ASSTO
		//                       ,:ADE-IB-DFLT
		//                        :IND-ADE-IB-DFLT
		//                       ,:ADE-MOD-CALC
		//                        :IND-ADE-MOD-CALC
		//                       ,:ADE-TP-FNT-CNBTVA
		//                        :IND-ADE-TP-FNT-CNBTVA
		//                       ,:ADE-IMP-AZ
		//                        :IND-ADE-IMP-AZ
		//                       ,:ADE-IMP-ADER
		//                        :IND-ADE-IMP-ADER
		//                       ,:ADE-IMP-TFR
		//                        :IND-ADE-IMP-TFR
		//                       ,:ADE-IMP-VOLO
		//                        :IND-ADE-IMP-VOLO
		//                       ,:ADE-PC-AZ
		//                        :IND-ADE-PC-AZ
		//                       ,:ADE-PC-ADER
		//                        :IND-ADE-PC-ADER
		//                       ,:ADE-PC-TFR
		//                        :IND-ADE-PC-TFR
		//                       ,:ADE-PC-VOLO
		//                        :IND-ADE-PC-VOLO
		//                       ,:ADE-DT-NOVA-RGM-FISC-DB
		//                        :IND-ADE-DT-NOVA-RGM-FISC
		//                       ,:ADE-FL-ATTIV
		//                        :IND-ADE-FL-ATTIV
		//                       ,:ADE-IMP-REC-RIT-VIS
		//                        :IND-ADE-IMP-REC-RIT-VIS
		//                       ,:ADE-IMP-REC-RIT-ACC
		//                        :IND-ADE-IMP-REC-RIT-ACC
		//                       ,:ADE-FL-VARZ-STAT-TBGC
		//                        :IND-ADE-FL-VARZ-STAT-TBGC
		//                       ,:ADE-FL-PROVZA-MIGRAZ
		//                        :IND-ADE-FL-PROVZA-MIGRAZ
		//                       ,:ADE-IMPB-VIS-DA-REC
		//                        :IND-ADE-IMPB-VIS-DA-REC
		//                       ,:ADE-DT-DECOR-PREST-BAN-DB
		//                        :IND-ADE-DT-DECOR-PREST-BAN
		//                       ,:ADE-DT-EFF-VARZ-STAT-T-DB
		//                        :IND-ADE-DT-EFF-VARZ-STAT-T
		//                       ,:ADE-DS-RIGA
		//                       ,:ADE-DS-OPER-SQL
		//                       ,:ADE-DS-VER
		//                       ,:ADE-DS-TS-INI-CPTZ
		//                       ,:ADE-DS-TS-END-CPTZ
		//                       ,:ADE-DS-UTENTE
		//                       ,:ADE-DS-STATO-ELAB
		//                       ,:ADE-CUM-CNBT-CAP
		//                        :IND-ADE-CUM-CNBT-CAP
		//                       ,:ADE-IMP-GAR-CNBT
		//                        :IND-ADE-IMP-GAR-CNBT
		//                       ,:ADE-DT-ULT-CONS-CNBT-DB
		//                        :IND-ADE-DT-ULT-CONS-CNBT
		//                       ,:ADE-IDEN-ISC-FND
		//                        :IND-ADE-IDEN-ISC-FND
		//                       ,:ADE-NUM-RAT-PIAN
		//                        :IND-ADE-NUM-RAT-PIAN
		//                       ,:ADE-DT-PRESC-DB
		//                        :IND-ADE-DT-PRESC
		//                       ,:ADE-CONCS-PREST
		//                        :IND-ADE-CONCS-PREST
		//                       ,:POL-ID-POLI
		//                       ,:POL-ID-MOVI-CRZ
		//                       ,:POL-ID-MOVI-CHIU
		//                        :IND-POL-ID-MOVI-CHIU
		//                       ,:POL-IB-OGG
		//                        :IND-POL-IB-OGG
		//                       ,:POL-IB-PROP
		//                       ,:POL-DT-PROP-DB
		//                        :IND-POL-DT-PROP
		//                       ,:POL-DT-INI-EFF-DB
		//                       ,:POL-DT-END-EFF-DB
		//                       ,:POL-COD-COMP-ANIA
		//                       ,:POL-DT-DECOR-DB
		//                       ,:POL-DT-EMIS-DB
		//                       ,:POL-TP-POLI
		//                       ,:POL-DUR-AA
		//                        :IND-POL-DUR-AA
		//                       ,:POL-DUR-MM
		//                        :IND-POL-DUR-MM
		//                       ,:POL-DT-SCAD-DB
		//                        :IND-POL-DT-SCAD
		//                       ,:POL-COD-PROD
		//                       ,:POL-DT-INI-VLDT-PROD-DB
		//                       ,:POL-COD-CONV
		//                        :IND-POL-COD-CONV
		//                       ,:POL-COD-RAMO
		//                        :IND-POL-COD-RAMO
		//                       ,:POL-DT-INI-VLDT-CONV-DB
		//                        :IND-POL-DT-INI-VLDT-CONV
		//                       ,:POL-DT-APPLZ-CONV-DB
		//                        :IND-POL-DT-APPLZ-CONV
		//                       ,:POL-TP-FRM-ASSVA
		//                       ,:POL-TP-RGM-FISC
		//                        :IND-POL-TP-RGM-FISC
		//                       ,:POL-FL-ESTAS
		//                        :IND-POL-FL-ESTAS
		//                       ,:POL-FL-RSH-COMUN
		//                        :IND-POL-FL-RSH-COMUN
		//                       ,:POL-FL-RSH-COMUN-COND
		//                        :IND-POL-FL-RSH-COMUN-COND
		//                       ,:POL-TP-LIV-GENZ-TIT
		//                       ,:POL-FL-COP-FINANZ
		//                        :IND-POL-FL-COP-FINANZ
		//                       ,:POL-TP-APPLZ-DIR
		//                        :IND-POL-TP-APPLZ-DIR
		//                       ,:POL-SPE-MED
		//                        :IND-POL-SPE-MED
		//                       ,:POL-DIR-EMIS
		//                        :IND-POL-DIR-EMIS
		//                       ,:POL-DIR-1O-VERS
		//                        :IND-POL-DIR-1O-VERS
		//                       ,:POL-DIR-VERS-AGG
		//                        :IND-POL-DIR-VERS-AGG
		//                       ,:POL-COD-DVS
		//                        :IND-POL-COD-DVS
		//                       ,:POL-FL-FNT-AZ
		//                        :IND-POL-FL-FNT-AZ
		//                       ,:POL-FL-FNT-ADER
		//                        :IND-POL-FL-FNT-ADER
		//                       ,:POL-FL-FNT-TFR
		//                        :IND-POL-FL-FNT-TFR
		//                       ,:POL-FL-FNT-VOLO
		//                        :IND-POL-FL-FNT-VOLO
		//                       ,:POL-TP-OPZ-A-SCAD
		//                        :IND-POL-TP-OPZ-A-SCAD
		//                       ,:POL-AA-DIFF-PROR-DFLT
		//                        :IND-POL-AA-DIFF-PROR-DFLT
		//                       ,:POL-FL-VER-PROD
		//                        :IND-POL-FL-VER-PROD
		//                       ,:POL-DUR-GG
		//                        :IND-POL-DUR-GG
		//                       ,:POL-DIR-QUIET
		//                        :IND-POL-DIR-QUIET
		//                       ,:POL-TP-PTF-ESTNO
		//                        :IND-POL-TP-PTF-ESTNO
		//                       ,:POL-FL-CUM-PRE-CNTR
		//                        :IND-POL-FL-CUM-PRE-CNTR
		//                       ,:POL-FL-AMMB-MOVI
		//                        :IND-POL-FL-AMMB-MOVI
		//                       ,:POL-CONV-GECO
		//                        :IND-POL-CONV-GECO
		//                       ,:POL-DS-RIGA
		//                       ,:POL-DS-OPER-SQL
		//                       ,:POL-DS-VER
		//                       ,:POL-DS-TS-INI-CPTZ
		//                       ,:POL-DS-TS-END-CPTZ
		//                       ,:POL-DS-UTENTE
		//                       ,:POL-DS-STATO-ELAB
		//                       ,:POL-FL-SCUDO-FISC
		//                        :IND-POL-FL-SCUDO-FISC
		//                       ,:POL-FL-TRASFE
		//                        :IND-POL-FL-TRASFE
		//                       ,:POL-FL-TFR-STRC
		//                        :IND-POL-FL-TFR-STRC
		//                       ,:POL-DT-PRESC-DB
		//                        :IND-POL-DT-PRESC
		//                       ,:POL-COD-CONV-AGG
		//                        :IND-POL-COD-CONV-AGG
		//                       ,:POL-SUBCAT-PROD
		//                        :IND-POL-SUBCAT-PROD
		//                       ,:POL-FL-QUEST-ADEGZ-ASS
		//                        :IND-POL-FL-QUEST-ADEGZ-ASS
		//                       ,:POL-COD-TPA
		//                        :IND-POL-COD-TPA
		//                       ,:POL-ID-ACC-COMM
		//                        :IND-POL-ID-ACC-COMM
		//                       ,:POL-FL-POLI-CPI-PR
		//                        :IND-POL-FL-POLI-CPI-PR
		//                       ,:POL-FL-POLI-BUNDLING
		//                        :IND-POL-FL-POLI-BUNDLING
		//                       ,:POL-IND-POLI-PRIN-COLL
		//                        :IND-POL-IND-POLI-PRIN-COLL
		//                       ,:POL-FL-VND-BUNDLE
		//                        :IND-POL-FL-VND-BUNDLE
		//                       ,:POL-IB-BS
		//                        :IND-POL-IB-BS
		//                       ,:POL-FL-POLI-IFP
		//                        :IND-POL-FL-POLI-IFP
		//                       ,:STB-ID-STAT-OGG-BUS
		//                       ,:STB-ID-OGG
		//                       ,:STB-TP-OGG
		//                       ,:STB-ID-MOVI-CRZ
		//                       ,:STB-ID-MOVI-CHIU
		//                        :IND-STB-ID-MOVI-CHIU
		//                       ,:STB-DT-INI-EFF-DB
		//                       ,:STB-DT-END-EFF-DB
		//                       ,:STB-COD-COMP-ANIA
		//                       ,:STB-TP-STAT-BUS
		//                       ,:STB-TP-CAUS
		//                       ,:STB-DS-RIGA
		//                       ,:STB-DS-OPER-SQL
		//                       ,:STB-DS-VER
		//                       ,:STB-DS-TS-INI-CPTZ
		//                       ,:STB-DS-TS-END-CPTZ
		//                       ,:STB-DS-UTENTE
		//                       ,:STB-DS-STATO-ELAB
		//                  FROM ADES      A,
		//                       POLI      B,
		//                       STAT_OGG_BUS C
		//                  WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
		//                                             :WS-TP-FRM-ASSVA2 )
		//                    AND B.IB_OGG BETWEEN  :WLB-IB-POLI-FIRST AND
		//                                             :WLB-IB-POLI-LAST
		//                    AND A.IB_OGG BETWEEN  :WLB-IB-ADE-FIRST AND
		//                                             :WLB-IB-ADE-LAST
		//                    AND A.ID_POLI      =   B.ID_POLI
		//                    AND A.ID_ADES      =   C.ID_OGG
		//                    AND C.TP_OGG       =  'AD'
		//           *--  D C.TP_STAT_BUS     =  'VI'
		//                    AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
		//                    AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
		//                    AND A.DT_END_EFF  >   :WS-DT-PTF-X
		//                    AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                    AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                    AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
		//                    AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
		//                    AND B.DT_END_EFF  >   :WS-DT-PTF-X
		//                    AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                    AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                    AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
		//                    AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
		//                    AND C.DT_END_EFF  >   :WS-DT-PTF-X
		//                    AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                    AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                    AND A.ID_ADES IN
		//                       (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
		//                                                        STAT_OGG_BUS E
		//                        WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
		//                        AND E.TP_OGG = 'TG'
		//                        AND E.TP_STAT_BUS IN ('VI','ST')
		//                        AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
		//                        AND D.COD_COMP_ANIA =
		//                            :IDSV0003-CODICE-COMPAGNIA-ANIA
		//                        AND D.DT_INI_EFF <=   :WS-DT-PTF-X
		//                        AND D.DT_END_EFF >    :WS-DT-PTF-X
		//                        AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                        AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                        AND E.DT_INI_EFF <=   :WS-DT-PTF-X
		//                        AND E.DT_END_EFF >    :WS-DT-PTF-X
		//                        AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
		//                        AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
		//                        AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
		//                             OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
		//                        )
		//                    AND A.DS_STATO_ELAB IN (
		//                                            :IABV0002-STATE-01,
		//                                            :IABV0002-STATE-02,
		//                                            :IABV0002-STATE-03,
		//                                            :IABV0002-STATE-04,
		//                                            :IABV0002-STATE-05,
		//                                            :IABV0002-STATE-06,
		//                                            :IABV0002-STATE-07,
		//                                            :IABV0002-STATE-08,
		//                                            :IABV0002-STATE-09,
		//                                            :IABV0002-STATE-10
		//                                              )
		//                    AND NOT A.DS_VER  = :IABV0009-VERSIONING
		//                    ORDER BY A.ID_POLI, A.ID_ADES
		//                   FETCH FIRST ROW ONLY
		//                END-EXEC.
		adesPoliStatOggBusTrchDiGarDao.selectRec4(this.getAdesPoliStatOggBusTrchDiGarLdbm02502());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			z100SetColonneNull();
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		}
	}

	/**Original name: A320-UPDATE-SC08<br>
	 * <pre>*****************************************************************</pre>*/
	private void a320UpdateSc08() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-PRIMARY-KEY
		//                   PERFORM A330-UPDATE-PK-SC08         THRU A330-SC08-EX
		//              WHEN IDSV0003-WHERE-CONDITION-08
		//                   PERFORM A340-UPDATE-WHERE-COND-SC08 THRU A340-SC08-EX
		//              WHEN IDSV0003-FIRST-ACTION
		//                                                       THRU A345-SC08-EX
		//              WHEN OTHER
		//                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
		//           END-EVALUATE.
		if (idsv0003.getLivelloOperazione().isIdsv0003PrimaryKey()) {
			// COB_CODE: PERFORM A330-UPDATE-PK-SC08         THRU A330-SC08-EX
			a330UpdatePkSc08();
		} else if (idsv0003.getTipologiaOperazione().isIdsv0003WhereCondition08()) {
			// COB_CODE: PERFORM A340-UPDATE-WHERE-COND-SC08 THRU A340-SC08-EX
			a340UpdateWhereCondSc08();
		} else if (idsv0003.getLivelloOperazione().isIdsv0003FirstAction()) {
			// COB_CODE: PERFORM A345-UPDATE-FIRST-ACTION-SC08
			//                                               THRU A345-SC08-EX
			a345UpdateFirstActionSc08();
		} else {
			// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
			idsv0003.getReturnCode().setInvalidLevelOper();
		}
	}

	/**Original name: A330-UPDATE-PK-SC08<br>
	 * <pre>*****************************************************************</pre>*/
	private void a330UpdatePkSc08() {
		// COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
		z150ValorizzaDataServices();
		// COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
		z200SetIndicatoriNull();
		// COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBM0250.cbl:line=10457, because the code is unreachable.
		// COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
		z960LengthVchar();
		// COB_CODE: EXEC SQL
		//                UPDATE ADES
		//                SET
		//                   DS_OPER_SQL            = :ADE-DS-OPER-SQL
		//                  ,DS_VER                 = :IABV0009-VERSIONING
		//                  ,DS_UTENTE              = :ADE-DS-UTENTE
		//                  ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
		//                WHERE             DS_RIGA = :ADE-DS-RIGA
		//           END-EXEC.
		adesDao.updateRec3(this);
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A340-UPDATE-WHERE-COND-SC08<br>
	 * <pre>*****************************************************************</pre>*/
	private void a340UpdateWhereCondSc08() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A345-UPDATE-FIRST-ACTION-SC08<br>
	 * <pre>*****************************************************************</pre>*/
	private void a345UpdateFirstActionSc08() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A360-OPEN-CURSOR-SC08<br>
	 * <pre>*****************************************************************</pre>*/
	private void a360OpenCursorSc08() {
		// COB_CODE: PERFORM A305-DECLARE-CURSOR-SC08 THRU A305-SC08-EX.
		a305DeclareCursorSc08();
		// COB_CODE: EXEC SQL
		//                OPEN CUR-SC08
		//           END-EXEC.
		adesPoliStatOggBusTrchDiGarDao.openCurSc08(this.getAdesPoliStatOggBusTrchDiGarLdbm02503());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A370-CLOSE-CURSOR-SC08<br>
	 * <pre>*****************************************************************</pre>*/
	private void a370CloseCursorSc08() {
		// COB_CODE: EXEC SQL
		//               CLOSE CUR-SC08
		//           END-EXEC.
		adesPoliStatOggBusTrchDiGarDao.closeCurSc08();
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A380-FETCH-FIRST-SC08<br>
	 * <pre>*****************************************************************</pre>*/
	private void a380FetchFirstSc08() {
		// COB_CODE: PERFORM A360-OPEN-CURSOR-SC08    THRU A360-SC08-EX.
		a360OpenCursorSc08();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM A390-FETCH-NEXT-SC08  THRU A390-SC08-EX
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM A390-FETCH-NEXT-SC08  THRU A390-SC08-EX
			a390FetchNextSc08();
		}
	}

	/**Original name: A390-FETCH-NEXT-SC08<br>
	 * <pre>*****************************************************************</pre>*/
	private void a390FetchNextSc08() {
		// COB_CODE: EXEC SQL
		//                FETCH CUR-SC08
		//           INTO
		//                   :ADE-ID-ADES
		//                  ,:ADE-ID-POLI
		//                  ,:ADE-ID-MOVI-CRZ
		//                  ,:ADE-ID-MOVI-CHIU
		//                   :IND-ADE-ID-MOVI-CHIU
		//                  ,:ADE-DT-INI-EFF-DB
		//                  ,:ADE-DT-END-EFF-DB
		//                  ,:ADE-IB-PREV
		//                   :IND-ADE-IB-PREV
		//                  ,:ADE-IB-OGG
		//                   :IND-ADE-IB-OGG
		//                  ,:ADE-COD-COMP-ANIA
		//                  ,:ADE-DT-DECOR-DB
		//                   :IND-ADE-DT-DECOR
		//                  ,:ADE-DT-SCAD-DB
		//                   :IND-ADE-DT-SCAD
		//                  ,:ADE-ETA-A-SCAD
		//                   :IND-ADE-ETA-A-SCAD
		//                  ,:ADE-DUR-AA
		//                   :IND-ADE-DUR-AA
		//                  ,:ADE-DUR-MM
		//                   :IND-ADE-DUR-MM
		//                  ,:ADE-DUR-GG
		//                   :IND-ADE-DUR-GG
		//                  ,:ADE-TP-RGM-FISC
		//                  ,:ADE-TP-RIAT
		//                   :IND-ADE-TP-RIAT
		//                  ,:ADE-TP-MOD-PAG-TIT
		//                  ,:ADE-TP-IAS
		//                   :IND-ADE-TP-IAS
		//                  ,:ADE-DT-VARZ-TP-IAS-DB
		//                   :IND-ADE-DT-VARZ-TP-IAS
		//                  ,:ADE-PRE-NET-IND
		//                   :IND-ADE-PRE-NET-IND
		//                  ,:ADE-PRE-LRD-IND
		//                   :IND-ADE-PRE-LRD-IND
		//                  ,:ADE-RAT-LRD-IND
		//                   :IND-ADE-RAT-LRD-IND
		//                  ,:ADE-PRSTZ-INI-IND
		//                   :IND-ADE-PRSTZ-INI-IND
		//                  ,:ADE-FL-COINC-ASSTO
		//                   :IND-ADE-FL-COINC-ASSTO
		//                  ,:ADE-IB-DFLT
		//                   :IND-ADE-IB-DFLT
		//                  ,:ADE-MOD-CALC
		//                   :IND-ADE-MOD-CALC
		//                  ,:ADE-TP-FNT-CNBTVA
		//                   :IND-ADE-TP-FNT-CNBTVA
		//                  ,:ADE-IMP-AZ
		//                   :IND-ADE-IMP-AZ
		//                  ,:ADE-IMP-ADER
		//                   :IND-ADE-IMP-ADER
		//                  ,:ADE-IMP-TFR
		//                   :IND-ADE-IMP-TFR
		//                  ,:ADE-IMP-VOLO
		//                   :IND-ADE-IMP-VOLO
		//                  ,:ADE-PC-AZ
		//                   :IND-ADE-PC-AZ
		//                  ,:ADE-PC-ADER
		//                   :IND-ADE-PC-ADER
		//                  ,:ADE-PC-TFR
		//                   :IND-ADE-PC-TFR
		//                  ,:ADE-PC-VOLO
		//                   :IND-ADE-PC-VOLO
		//                  ,:ADE-DT-NOVA-RGM-FISC-DB
		//                   :IND-ADE-DT-NOVA-RGM-FISC
		//                  ,:ADE-FL-ATTIV
		//                   :IND-ADE-FL-ATTIV
		//                  ,:ADE-IMP-REC-RIT-VIS
		//                   :IND-ADE-IMP-REC-RIT-VIS
		//                  ,:ADE-IMP-REC-RIT-ACC
		//                   :IND-ADE-IMP-REC-RIT-ACC
		//                  ,:ADE-FL-VARZ-STAT-TBGC
		//                   :IND-ADE-FL-VARZ-STAT-TBGC
		//                  ,:ADE-FL-PROVZA-MIGRAZ
		//                   :IND-ADE-FL-PROVZA-MIGRAZ
		//                  ,:ADE-IMPB-VIS-DA-REC
		//                   :IND-ADE-IMPB-VIS-DA-REC
		//                  ,:ADE-DT-DECOR-PREST-BAN-DB
		//                   :IND-ADE-DT-DECOR-PREST-BAN
		//                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
		//                   :IND-ADE-DT-EFF-VARZ-STAT-T
		//                  ,:ADE-DS-RIGA
		//                  ,:ADE-DS-OPER-SQL
		//                  ,:ADE-DS-VER
		//                  ,:ADE-DS-TS-INI-CPTZ
		//                  ,:ADE-DS-TS-END-CPTZ
		//                  ,:ADE-DS-UTENTE
		//                  ,:ADE-DS-STATO-ELAB
		//                  ,:ADE-CUM-CNBT-CAP
		//                   :IND-ADE-CUM-CNBT-CAP
		//                  ,:ADE-IMP-GAR-CNBT
		//                   :IND-ADE-IMP-GAR-CNBT
		//                  ,:ADE-DT-ULT-CONS-CNBT-DB
		//                   :IND-ADE-DT-ULT-CONS-CNBT
		//                  ,:ADE-IDEN-ISC-FND
		//                   :IND-ADE-IDEN-ISC-FND
		//                  ,:ADE-NUM-RAT-PIAN
		//                   :IND-ADE-NUM-RAT-PIAN
		//                  ,:ADE-DT-PRESC-DB
		//                   :IND-ADE-DT-PRESC
		//                  ,:ADE-CONCS-PREST
		//                   :IND-ADE-CONCS-PREST
		//                  ,:POL-ID-POLI
		//                  ,:POL-ID-MOVI-CRZ
		//                  ,:POL-ID-MOVI-CHIU
		//                   :IND-POL-ID-MOVI-CHIU
		//                  ,:POL-IB-OGG
		//                   :IND-POL-IB-OGG
		//                  ,:POL-IB-PROP
		//                  ,:POL-DT-PROP-DB
		//                   :IND-POL-DT-PROP
		//                  ,:POL-DT-INI-EFF-DB
		//                  ,:POL-DT-END-EFF-DB
		//                  ,:POL-COD-COMP-ANIA
		//                  ,:POL-DT-DECOR-DB
		//                  ,:POL-DT-EMIS-DB
		//                  ,:POL-TP-POLI
		//                  ,:POL-DUR-AA
		//                   :IND-POL-DUR-AA
		//                  ,:POL-DUR-MM
		//                   :IND-POL-DUR-MM
		//                  ,:POL-DT-SCAD-DB
		//                   :IND-POL-DT-SCAD
		//                  ,:POL-COD-PROD
		//                  ,:POL-DT-INI-VLDT-PROD-DB
		//                  ,:POL-COD-CONV
		//                   :IND-POL-COD-CONV
		//                  ,:POL-COD-RAMO
		//                   :IND-POL-COD-RAMO
		//                  ,:POL-DT-INI-VLDT-CONV-DB
		//                   :IND-POL-DT-INI-VLDT-CONV
		//                  ,:POL-DT-APPLZ-CONV-DB
		//                   :IND-POL-DT-APPLZ-CONV
		//                  ,:POL-TP-FRM-ASSVA
		//                  ,:POL-TP-RGM-FISC
		//                   :IND-POL-TP-RGM-FISC
		//                  ,:POL-FL-ESTAS
		//                   :IND-POL-FL-ESTAS
		//                  ,:POL-FL-RSH-COMUN
		//                   :IND-POL-FL-RSH-COMUN
		//                  ,:POL-FL-RSH-COMUN-COND
		//                   :IND-POL-FL-RSH-COMUN-COND
		//                  ,:POL-TP-LIV-GENZ-TIT
		//                  ,:POL-FL-COP-FINANZ
		//                   :IND-POL-FL-COP-FINANZ
		//                  ,:POL-TP-APPLZ-DIR
		//                   :IND-POL-TP-APPLZ-DIR
		//                  ,:POL-SPE-MED
		//                   :IND-POL-SPE-MED
		//                  ,:POL-DIR-EMIS
		//                   :IND-POL-DIR-EMIS
		//                  ,:POL-DIR-1O-VERS
		//                   :IND-POL-DIR-1O-VERS
		//                  ,:POL-DIR-VERS-AGG
		//                   :IND-POL-DIR-VERS-AGG
		//                  ,:POL-COD-DVS
		//                   :IND-POL-COD-DVS
		//                  ,:POL-FL-FNT-AZ
		//                   :IND-POL-FL-FNT-AZ
		//                  ,:POL-FL-FNT-ADER
		//                   :IND-POL-FL-FNT-ADER
		//                  ,:POL-FL-FNT-TFR
		//                   :IND-POL-FL-FNT-TFR
		//                  ,:POL-FL-FNT-VOLO
		//                   :IND-POL-FL-FNT-VOLO
		//                  ,:POL-TP-OPZ-A-SCAD
		//                   :IND-POL-TP-OPZ-A-SCAD
		//                  ,:POL-AA-DIFF-PROR-DFLT
		//                   :IND-POL-AA-DIFF-PROR-DFLT
		//                  ,:POL-FL-VER-PROD
		//                   :IND-POL-FL-VER-PROD
		//                  ,:POL-DUR-GG
		//                   :IND-POL-DUR-GG
		//                  ,:POL-DIR-QUIET
		//                   :IND-POL-DIR-QUIET
		//                  ,:POL-TP-PTF-ESTNO
		//                   :IND-POL-TP-PTF-ESTNO
		//                  ,:POL-FL-CUM-PRE-CNTR
		//                   :IND-POL-FL-CUM-PRE-CNTR
		//                  ,:POL-FL-AMMB-MOVI
		//                   :IND-POL-FL-AMMB-MOVI
		//                  ,:POL-CONV-GECO
		//                   :IND-POL-CONV-GECO
		//                  ,:POL-DS-RIGA
		//                  ,:POL-DS-OPER-SQL
		//                  ,:POL-DS-VER
		//                  ,:POL-DS-TS-INI-CPTZ
		//                  ,:POL-DS-TS-END-CPTZ
		//                  ,:POL-DS-UTENTE
		//                  ,:POL-DS-STATO-ELAB
		//                  ,:POL-FL-SCUDO-FISC
		//                   :IND-POL-FL-SCUDO-FISC
		//                  ,:POL-FL-TRASFE
		//                   :IND-POL-FL-TRASFE
		//                  ,:POL-FL-TFR-STRC
		//                   :IND-POL-FL-TFR-STRC
		//                  ,:POL-DT-PRESC-DB
		//                   :IND-POL-DT-PRESC
		//                  ,:POL-COD-CONV-AGG
		//                   :IND-POL-COD-CONV-AGG
		//                  ,:POL-SUBCAT-PROD
		//                   :IND-POL-SUBCAT-PROD
		//                  ,:POL-FL-QUEST-ADEGZ-ASS
		//                   :IND-POL-FL-QUEST-ADEGZ-ASS
		//                  ,:POL-COD-TPA
		//                   :IND-POL-COD-TPA
		//                  ,:POL-ID-ACC-COMM
		//                   :IND-POL-ID-ACC-COMM
		//                  ,:POL-FL-POLI-CPI-PR
		//                   :IND-POL-FL-POLI-CPI-PR
		//                  ,:POL-FL-POLI-BUNDLING
		//                   :IND-POL-FL-POLI-BUNDLING
		//                  ,:POL-IND-POLI-PRIN-COLL
		//                   :IND-POL-IND-POLI-PRIN-COLL
		//                  ,:POL-FL-VND-BUNDLE
		//                   :IND-POL-FL-VND-BUNDLE
		//                  ,:POL-IB-BS
		//                   :IND-POL-IB-BS
		//                  ,:POL-FL-POLI-IFP
		//                   :IND-POL-FL-POLI-IFP
		//                  ,:STB-ID-STAT-OGG-BUS
		//                  ,:STB-ID-OGG
		//                  ,:STB-TP-OGG
		//                  ,:STB-ID-MOVI-CRZ
		//                  ,:STB-ID-MOVI-CHIU
		//                   :IND-STB-ID-MOVI-CHIU
		//                  ,:STB-DT-INI-EFF-DB
		//                  ,:STB-DT-END-EFF-DB
		//                  ,:STB-COD-COMP-ANIA
		//                  ,:STB-TP-STAT-BUS
		//                  ,:STB-TP-CAUS
		//                  ,:STB-DS-RIGA
		//                  ,:STB-DS-OPER-SQL
		//                  ,:STB-DS-VER
		//                  ,:STB-DS-TS-INI-CPTZ
		//                  ,:STB-DS-TS-END-CPTZ
		//                  ,:STB-DS-UTENTE
		//                  ,:STB-DS-STATO-ELAB
		//           END-EXEC.
		adesPoliStatOggBusTrchDiGarDao.fetchCurSc08(this.getAdesPoliStatOggBusTrchDiGarLdbm0250());
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           ELSE
		//             END-IF
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) {
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			z100SetColonneNull();
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		} else if (idsv0003.getSqlcode().isNotFound()) {
			// COB_CODE: IF IDSV0003-NOT-FOUND
			//              END-IF
			//           END-IF
			// COB_CODE: PERFORM A370-CLOSE-CURSOR-SC08 THRU A370-SC08-EX
			a370CloseCursorSc08();
			// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
			//              SET IDSV0003-NOT-FOUND TO TRUE
			//           END-IF
			if (idsv0003.getSqlcode().isSuccessfulSql()) {
				// COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
				idsv0003.getSqlcode().setNotFound();
			}
		}
	}

	/**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>*****************************************************************</pre>*/
	private void z100SetColonneNull() {
		// COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
		idsv0003.getCampiEsito().setNumRigheLette(((short) 1));
		// COB_CODE: IF IND-ADE-ID-MOVI-CHIU = -1
		//              MOVE HIGH-VALUES TO ADE-ID-MOVI-CHIU-NULL
		//           END-IF
		if (ws.getIndAdes().getIdMoviChiu() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-ID-MOVI-CHIU-NULL
			ades.getAdeIdMoviChiu().setAdeIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeIdMoviChiu.Len.ADE_ID_MOVI_CHIU_NULL));
		}
		// COB_CODE: IF IND-ADE-IB-PREV = -1
		//              MOVE HIGH-VALUES TO ADE-IB-PREV-NULL
		//           END-IF
		if (ws.getIndAdes().getIbPrev() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-IB-PREV-NULL
			ades.setAdeIbPrev(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_IB_PREV));
		}
		// COB_CODE: IF IND-ADE-IB-OGG = -1
		//              MOVE HIGH-VALUES TO ADE-IB-OGG-NULL
		//           END-IF
		if (ws.getIndAdes().getIbOgg() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-IB-OGG-NULL
			ades.setAdeIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_IB_OGG));
		}
		// COB_CODE: IF IND-ADE-DT-DECOR = -1
		//              MOVE HIGH-VALUES TO ADE-DT-DECOR-NULL
		//           END-IF
		if (ws.getIndAdes().getDtDecor() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-DT-DECOR-NULL
			ades.getAdeDtDecor().setAdeDtDecorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtDecor.Len.ADE_DT_DECOR_NULL));
		}
		// COB_CODE: IF IND-ADE-DT-SCAD = -1
		//              MOVE HIGH-VALUES TO ADE-DT-SCAD-NULL
		//           END-IF
		if (ws.getIndAdes().getDtScad() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-DT-SCAD-NULL
			ades.getAdeDtScad().setAdeDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtScad.Len.ADE_DT_SCAD_NULL));
		}
		// COB_CODE: IF IND-ADE-ETA-A-SCAD = -1
		//              MOVE HIGH-VALUES TO ADE-ETA-A-SCAD-NULL
		//           END-IF
		if (ws.getIndAdes().getEtaAScad() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-ETA-A-SCAD-NULL
			ades.getAdeEtaAScad().setAdeEtaAScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeEtaAScad.Len.ADE_ETA_A_SCAD_NULL));
		}
		// COB_CODE: IF IND-ADE-DUR-AA = -1
		//              MOVE HIGH-VALUES TO ADE-DUR-AA-NULL
		//           END-IF
		if (ws.getIndAdes().getDurAa() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-DUR-AA-NULL
			ades.getAdeDurAa().setAdeDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDurAa.Len.ADE_DUR_AA_NULL));
		}
		// COB_CODE: IF IND-ADE-DUR-MM = -1
		//              MOVE HIGH-VALUES TO ADE-DUR-MM-NULL
		//           END-IF
		if (ws.getIndAdes().getDurMm() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-DUR-MM-NULL
			ades.getAdeDurMm().setAdeDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDurMm.Len.ADE_DUR_MM_NULL));
		}
		// COB_CODE: IF IND-ADE-DUR-GG = -1
		//              MOVE HIGH-VALUES TO ADE-DUR-GG-NULL
		//           END-IF
		if (ws.getIndAdes().getDurGg() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-DUR-GG-NULL
			ades.getAdeDurGg().setAdeDurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDurGg.Len.ADE_DUR_GG_NULL));
		}
		// COB_CODE: IF IND-ADE-TP-RIAT = -1
		//              MOVE HIGH-VALUES TO ADE-TP-RIAT-NULL
		//           END-IF
		if (ws.getIndAdes().getTpRiat() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-TP-RIAT-NULL
			ades.setAdeTpRiat(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_TP_RIAT));
		}
		// COB_CODE: IF IND-ADE-TP-IAS = -1
		//              MOVE HIGH-VALUES TO ADE-TP-IAS-NULL
		//           END-IF
		if (ws.getIndAdes().getTpIas() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-TP-IAS-NULL
			ades.setAdeTpIas(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_TP_IAS));
		}
		// COB_CODE: IF IND-ADE-DT-VARZ-TP-IAS = -1
		//              MOVE HIGH-VALUES TO ADE-DT-VARZ-TP-IAS-NULL
		//           END-IF
		if (ws.getIndAdes().getDtVarzTpIas() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-DT-VARZ-TP-IAS-NULL
			ades.getAdeDtVarzTpIas().setAdeDtVarzTpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtVarzTpIas.Len.ADE_DT_VARZ_TP_IAS_NULL));
		}
		// COB_CODE: IF IND-ADE-PRE-NET-IND = -1
		//              MOVE HIGH-VALUES TO ADE-PRE-NET-IND-NULL
		//           END-IF
		if (ws.getIndAdes().getPreNetInd() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-PRE-NET-IND-NULL
			ades.getAdePreNetInd().setAdePreNetIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePreNetInd.Len.ADE_PRE_NET_IND_NULL));
		}
		// COB_CODE: IF IND-ADE-PRE-LRD-IND = -1
		//              MOVE HIGH-VALUES TO ADE-PRE-LRD-IND-NULL
		//           END-IF
		if (ws.getIndAdes().getPreLrdInd() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-PRE-LRD-IND-NULL
			ades.getAdePreLrdInd().setAdePreLrdIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePreLrdInd.Len.ADE_PRE_LRD_IND_NULL));
		}
		// COB_CODE: IF IND-ADE-RAT-LRD-IND = -1
		//              MOVE HIGH-VALUES TO ADE-RAT-LRD-IND-NULL
		//           END-IF
		if (ws.getIndAdes().getRatLrdInd() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-RAT-LRD-IND-NULL
			ades.getAdeRatLrdInd().setAdeRatLrdIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeRatLrdInd.Len.ADE_RAT_LRD_IND_NULL));
		}
		// COB_CODE: IF IND-ADE-PRSTZ-INI-IND = -1
		//              MOVE HIGH-VALUES TO ADE-PRSTZ-INI-IND-NULL
		//           END-IF
		if (ws.getIndAdes().getPrstzIniInd() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-PRSTZ-INI-IND-NULL
			ades.getAdePrstzIniInd().setAdePrstzIniIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePrstzIniInd.Len.ADE_PRSTZ_INI_IND_NULL));
		}
		// COB_CODE: IF IND-ADE-FL-COINC-ASSTO = -1
		//              MOVE HIGH-VALUES TO ADE-FL-COINC-ASSTO-NULL
		//           END-IF
		if (ws.getIndAdes().getFlCoincAssto() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-FL-COINC-ASSTO-NULL
			ades.setAdeFlCoincAssto(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-ADE-IB-DFLT = -1
		//              MOVE HIGH-VALUES TO ADE-IB-DFLT-NULL
		//           END-IF
		if (ws.getIndAdes().getIbDflt() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-IB-DFLT-NULL
			ades.setAdeIbDflt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_IB_DFLT));
		}
		// COB_CODE: IF IND-ADE-MOD-CALC = -1
		//              MOVE HIGH-VALUES TO ADE-MOD-CALC-NULL
		//           END-IF
		if (ws.getIndAdes().getModCalc() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-MOD-CALC-NULL
			ades.setAdeModCalc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_MOD_CALC));
		}
		// COB_CODE: IF IND-ADE-TP-FNT-CNBTVA = -1
		//              MOVE HIGH-VALUES TO ADE-TP-FNT-CNBTVA-NULL
		//           END-IF
		if (ws.getIndAdes().getTpFntCnbtva() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-TP-FNT-CNBTVA-NULL
			ades.setAdeTpFntCnbtva(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_TP_FNT_CNBTVA));
		}
		// COB_CODE: IF IND-ADE-IMP-AZ = -1
		//              MOVE HIGH-VALUES TO ADE-IMP-AZ-NULL
		//           END-IF
		if (ws.getIndAdes().getImpAz() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-AZ-NULL
			ades.getAdeImpAz().setAdeImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpAz.Len.ADE_IMP_AZ_NULL));
		}
		// COB_CODE: IF IND-ADE-IMP-ADER = -1
		//              MOVE HIGH-VALUES TO ADE-IMP-ADER-NULL
		//           END-IF
		if (ws.getIndAdes().getImpAder() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-ADER-NULL
			ades.getAdeImpAder().setAdeImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpAder.Len.ADE_IMP_ADER_NULL));
		}
		// COB_CODE: IF IND-ADE-IMP-TFR = -1
		//              MOVE HIGH-VALUES TO ADE-IMP-TFR-NULL
		//           END-IF
		if (ws.getIndAdes().getImpTfr() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-TFR-NULL
			ades.getAdeImpTfr().setAdeImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpTfr.Len.ADE_IMP_TFR_NULL));
		}
		// COB_CODE: IF IND-ADE-IMP-VOLO = -1
		//              MOVE HIGH-VALUES TO ADE-IMP-VOLO-NULL
		//           END-IF
		if (ws.getIndAdes().getImpVolo() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-VOLO-NULL
			ades.getAdeImpVolo().setAdeImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpVolo.Len.ADE_IMP_VOLO_NULL));
		}
		// COB_CODE: IF IND-ADE-PC-AZ = -1
		//              MOVE HIGH-VALUES TO ADE-PC-AZ-NULL
		//           END-IF
		if (ws.getIndAdes().getPcAz() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-PC-AZ-NULL
			ades.getAdePcAz().setAdePcAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePcAz.Len.ADE_PC_AZ_NULL));
		}
		// COB_CODE: IF IND-ADE-PC-ADER = -1
		//              MOVE HIGH-VALUES TO ADE-PC-ADER-NULL
		//           END-IF
		if (ws.getIndAdes().getPcAder() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-PC-ADER-NULL
			ades.getAdePcAder().setAdePcAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePcAder.Len.ADE_PC_ADER_NULL));
		}
		// COB_CODE: IF IND-ADE-PC-TFR = -1
		//              MOVE HIGH-VALUES TO ADE-PC-TFR-NULL
		//           END-IF
		if (ws.getIndAdes().getPcTfr() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-PC-TFR-NULL
			ades.getAdePcTfr().setAdePcTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePcTfr.Len.ADE_PC_TFR_NULL));
		}
		// COB_CODE: IF IND-ADE-PC-VOLO = -1
		//              MOVE HIGH-VALUES TO ADE-PC-VOLO-NULL
		//           END-IF
		if (ws.getIndAdes().getPcVolo() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-PC-VOLO-NULL
			ades.getAdePcVolo().setAdePcVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePcVolo.Len.ADE_PC_VOLO_NULL));
		}
		// COB_CODE: IF IND-ADE-DT-NOVA-RGM-FISC = -1
		//              MOVE HIGH-VALUES TO ADE-DT-NOVA-RGM-FISC-NULL
		//           END-IF
		if (ws.getIndAdes().getDtNovaRgmFisc() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-DT-NOVA-RGM-FISC-NULL
			ades.getAdeDtNovaRgmFisc()
					.setAdeDtNovaRgmFiscNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtNovaRgmFisc.Len.ADE_DT_NOVA_RGM_FISC_NULL));
		}
		// COB_CODE: IF IND-ADE-FL-ATTIV = -1
		//              MOVE HIGH-VALUES TO ADE-FL-ATTIV-NULL
		//           END-IF
		if (ws.getIndAdes().getFlAttiv() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-FL-ATTIV-NULL
			ades.setAdeFlAttiv(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-ADE-IMP-REC-RIT-VIS = -1
		//              MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-VIS-NULL
		//           END-IF
		if (ws.getIndAdes().getImpRecRitVis() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-VIS-NULL
			ades.getAdeImpRecRitVis()
					.setAdeImpRecRitVisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpRecRitVis.Len.ADE_IMP_REC_RIT_VIS_NULL));
		}
		// COB_CODE: IF IND-ADE-IMP-REC-RIT-ACC = -1
		//              MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-ACC-NULL
		//           END-IF
		if (ws.getIndAdes().getImpRecRitAcc() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-ACC-NULL
			ades.getAdeImpRecRitAcc()
					.setAdeImpRecRitAccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpRecRitAcc.Len.ADE_IMP_REC_RIT_ACC_NULL));
		}
		// COB_CODE: IF IND-ADE-FL-VARZ-STAT-TBGC = -1
		//              MOVE HIGH-VALUES TO ADE-FL-VARZ-STAT-TBGC-NULL
		//           END-IF
		if (ws.getIndAdes().getFlVarzStatTbgc() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-FL-VARZ-STAT-TBGC-NULL
			ades.setAdeFlVarzStatTbgc(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-ADE-FL-PROVZA-MIGRAZ = -1
		//              MOVE HIGH-VALUES TO ADE-FL-PROVZA-MIGRAZ-NULL
		//           END-IF
		if (ws.getIndAdes().getFlProvzaMigraz() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-FL-PROVZA-MIGRAZ-NULL
			ades.setAdeFlProvzaMigraz(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-ADE-IMPB-VIS-DA-REC = -1
		//              MOVE HIGH-VALUES TO ADE-IMPB-VIS-DA-REC-NULL
		//           END-IF
		if (ws.getIndAdes().getImpbVisDaRec() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-IMPB-VIS-DA-REC-NULL
			ades.getAdeImpbVisDaRec()
					.setAdeImpbVisDaRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpbVisDaRec.Len.ADE_IMPB_VIS_DA_REC_NULL));
		}
		// COB_CODE: IF IND-ADE-DT-DECOR-PREST-BAN = -1
		//              MOVE HIGH-VALUES TO ADE-DT-DECOR-PREST-BAN-NULL
		//           END-IF
		if (ws.getIndAdes().getDtDecorPrestBan() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-DT-DECOR-PREST-BAN-NULL
			ades.getAdeDtDecorPrestBan()
					.setAdeDtDecorPrestBanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtDecorPrestBan.Len.ADE_DT_DECOR_PREST_BAN_NULL));
		}
		// COB_CODE: IF IND-ADE-DT-EFF-VARZ-STAT-T = -1
		//              MOVE HIGH-VALUES TO ADE-DT-EFF-VARZ-STAT-T-NULL
		//           END-IF
		if (ws.getIndAdes().getDtEffVarzStatT() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-DT-EFF-VARZ-STAT-T-NULL
			ades.getAdeDtEffVarzStatT()
					.setAdeDtEffVarzStatTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtEffVarzStatT.Len.ADE_DT_EFF_VARZ_STAT_T_NULL));
		}
		// COB_CODE: IF IND-ADE-CUM-CNBT-CAP = -1
		//              MOVE HIGH-VALUES TO ADE-CUM-CNBT-CAP-NULL
		//           END-IF
		if (ws.getIndAdes().getCumCnbtCap() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-CUM-CNBT-CAP-NULL
			ades.getAdeCumCnbtCap().setAdeCumCnbtCapNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeCumCnbtCap.Len.ADE_CUM_CNBT_CAP_NULL));
		}
		// COB_CODE: IF IND-ADE-IMP-GAR-CNBT = -1
		//              MOVE HIGH-VALUES TO ADE-IMP-GAR-CNBT-NULL
		//           END-IF
		if (ws.getIndAdes().getImpGarCnbt() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-GAR-CNBT-NULL
			ades.getAdeImpGarCnbt().setAdeImpGarCnbtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpGarCnbt.Len.ADE_IMP_GAR_CNBT_NULL));
		}
		// COB_CODE: IF IND-ADE-DT-ULT-CONS-CNBT = -1
		//              MOVE HIGH-VALUES TO ADE-DT-ULT-CONS-CNBT-NULL
		//           END-IF
		if (ws.getIndAdes().getDtUltConsCnbt() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-DT-ULT-CONS-CNBT-NULL
			ades.getAdeDtUltConsCnbt()
					.setAdeDtUltConsCnbtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtUltConsCnbt.Len.ADE_DT_ULT_CONS_CNBT_NULL));
		}
		// COB_CODE: IF IND-ADE-IDEN-ISC-FND = -1
		//              MOVE HIGH-VALUES TO ADE-IDEN-ISC-FND-NULL
		//           END-IF
		if (ws.getIndAdes().getIdenIscFnd() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-IDEN-ISC-FND-NULL
			ades.setAdeIdenIscFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_IDEN_ISC_FND));
		}
		// COB_CODE: IF IND-ADE-NUM-RAT-PIAN = -1
		//              MOVE HIGH-VALUES TO ADE-NUM-RAT-PIAN-NULL
		//           END-IF
		if (ws.getIndAdes().getNumRatPian() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-NUM-RAT-PIAN-NULL
			ades.getAdeNumRatPian().setAdeNumRatPianNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeNumRatPian.Len.ADE_NUM_RAT_PIAN_NULL));
		}
		// COB_CODE: IF IND-ADE-DT-PRESC = -1
		//              MOVE HIGH-VALUES TO ADE-DT-PRESC-NULL
		//           END-IF.
		if (ws.getIndAdes().getDtPresc() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-DT-PRESC-NULL
			ades.getAdeDtPresc().setAdeDtPrescNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtPresc.Len.ADE_DT_PRESC_NULL));
		}
		// COB_CODE: IF IND-ADE-CONCS-PREST = -1
		//              MOVE HIGH-VALUES TO ADE-CONCS-PREST-NULL
		//           END-IF.
		if (ws.getIndAdes().getConcsPrest() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO ADE-CONCS-PREST-NULL
			ades.setAdeConcsPrest(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-POL-ID-MOVI-CHIU = -1
		//              MOVE HIGH-VALUES TO POL-ID-MOVI-CHIU-NULL
		//           END-IF
		if (ws.getIndPoli().getIdMoviChiu() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-ID-MOVI-CHIU-NULL
			poli.getPolIdMoviChiu().setPolIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolIdMoviChiu.Len.POL_ID_MOVI_CHIU_NULL));
		}
		// COB_CODE: IF IND-POL-IB-OGG = -1
		//              MOVE HIGH-VALUES TO POL-IB-OGG-NULL
		//           END-IF
		if (ws.getIndPoli().getDtIniCop() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-IB-OGG-NULL
			poli.setPolIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_IB_OGG));
		}
		// COB_CODE: IF IND-POL-DT-PROP = -1
		//              MOVE HIGH-VALUES TO POL-DT-PROP-NULL
		//           END-IF
		if (ws.getIndPoli().getDtEndCop() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-DT-PROP-NULL
			poli.getPolDtProp().setPolDtPropNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtProp.Len.POL_DT_PROP_NULL));
		}
		// COB_CODE: IF IND-POL-DUR-AA = -1
		//              MOVE HIGH-VALUES TO POL-DUR-AA-NULL
		//           END-IF
		if (ws.getIndPoli().getPreNet() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-DUR-AA-NULL
			poli.getPolDurAa().setPolDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDurAa.Len.POL_DUR_AA_NULL));
		}
		// COB_CODE: IF IND-POL-DUR-MM = -1
		//              MOVE HIGH-VALUES TO POL-DUR-MM-NULL
		//           END-IF
		if (ws.getIndPoli().getIntrFraz() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-DUR-MM-NULL
			poli.getPolDurMm().setPolDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDurMm.Len.POL_DUR_MM_NULL));
		}
		// COB_CODE: IF IND-POL-DT-SCAD = -1
		//              MOVE HIGH-VALUES TO POL-DT-SCAD-NULL
		//           END-IF
		if (ws.getIndPoli().getIntrMora() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-DT-SCAD-NULL
			poli.getPolDtScad().setPolDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtScad.Len.POL_DT_SCAD_NULL));
		}
		// COB_CODE: IF IND-POL-COD-CONV = -1
		//              MOVE HIGH-VALUES TO POL-COD-CONV-NULL
		//           END-IF
		if (ws.getIndPoli().getIntrRetdt() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-COD-CONV-NULL
			poli.setPolCodConv(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_CONV));
		}
		// COB_CODE: IF IND-POL-COD-RAMO = -1
		//              MOVE HIGH-VALUES TO POL-COD-RAMO-NULL
		//           END-IF
		if (ws.getIndPoli().getIntrRiat() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-COD-RAMO-NULL
			poli.setPolCodRamo(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_RAMO));
		}
		// COB_CODE: IF IND-POL-DT-INI-VLDT-CONV = -1
		//              MOVE HIGH-VALUES TO POL-DT-INI-VLDT-CONV-NULL
		//           END-IF
		if (ws.getIndPoli().getDir() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-DT-INI-VLDT-CONV-NULL
			poli.getPolDtIniVldtConv()
					.setPolDtIniVldtConvNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtIniVldtConv.Len.POL_DT_INI_VLDT_CONV_NULL));
		}
		// COB_CODE: IF IND-POL-DT-APPLZ-CONV = -1
		//              MOVE HIGH-VALUES TO POL-DT-APPLZ-CONV-NULL
		//           END-IF
		if (ws.getIndPoli().getSpeMed() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-DT-APPLZ-CONV-NULL
			poli.getPolDtApplzConv().setPolDtApplzConvNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtApplzConv.Len.POL_DT_APPLZ_CONV_NULL));
		}
		// COB_CODE: IF IND-POL-TP-RGM-FISC = -1
		//              MOVE HIGH-VALUES TO POL-TP-RGM-FISC-NULL
		//           END-IF
		if (ws.getIndPoli().getTax() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-TP-RGM-FISC-NULL
			poli.setPolTpRgmFisc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_TP_RGM_FISC));
		}
		// COB_CODE: IF IND-POL-FL-ESTAS = -1
		//              MOVE HIGH-VALUES TO POL-FL-ESTAS-NULL
		//           END-IF
		if (ws.getIndPoli().getSoprSan() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-FL-ESTAS-NULL
			poli.setPolFlEstas(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-POL-FL-RSH-COMUN = -1
		//              MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-NULL
		//           END-IF
		if (ws.getIndPoli().getSoprSpo() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-NULL
			poli.setPolFlRshComun(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-POL-FL-RSH-COMUN-COND = -1
		//              MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-COND-NULL
		//           END-IF
		if (ws.getIndPoli().getSoprTec() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-COND-NULL
			poli.setPolFlRshComunCond(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-POL-FL-COP-FINANZ = -1
		//              MOVE HIGH-VALUES TO POL-FL-COP-FINANZ-NULL
		//           END-IF
		if (ws.getIndPoli().getSoprProf() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-FL-COP-FINANZ-NULL
			poli.setPolFlCopFinanz(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-POL-TP-APPLZ-DIR = -1
		//              MOVE HIGH-VALUES TO POL-TP-APPLZ-DIR-NULL
		//           END-IF
		if (ws.getIndPoli().getSoprAlt() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-TP-APPLZ-DIR-NULL
			poli.setPolTpApplzDir(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_TP_APPLZ_DIR));
		}
		// COB_CODE: IF IND-POL-SPE-MED = -1
		//              MOVE HIGH-VALUES TO POL-SPE-MED-NULL
		//           END-IF
		if (ws.getIndPoli().getPreTot() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-SPE-MED-NULL
			poli.getPolSpeMed().setPolSpeMedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolSpeMed.Len.POL_SPE_MED_NULL));
		}
		// COB_CODE: IF IND-POL-DIR-EMIS = -1
		//              MOVE HIGH-VALUES TO POL-DIR-EMIS-NULL
		//           END-IF
		if (ws.getIndPoli().getPrePpIas() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-DIR-EMIS-NULL
			poli.getPolDirEmis().setPolDirEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDirEmis.Len.POL_DIR_EMIS_NULL));
		}
		// COB_CODE: IF IND-POL-DIR-1O-VERS = -1
		//              MOVE HIGH-VALUES TO POL-DIR-1O-VERS-NULL
		//           END-IF
		if (ws.getIndPoli().getPreSoloRsh() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-DIR-1O-VERS-NULL
			poli.getPolDir1oVers().setPolDir1oVersNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDir1oVers.Len.POL_DIR1O_VERS_NULL));
		}
		// COB_CODE: IF IND-POL-DIR-VERS-AGG = -1
		//              MOVE HIGH-VALUES TO POL-DIR-VERS-AGG-NULL
		//           END-IF
		if (ws.getIndPoli().getCarAcq() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-DIR-VERS-AGG-NULL
			poli.getPolDirVersAgg().setPolDirVersAggNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDirVersAgg.Len.POL_DIR_VERS_AGG_NULL));
		}
		// COB_CODE: IF IND-POL-COD-DVS = -1
		//              MOVE HIGH-VALUES TO POL-COD-DVS-NULL
		//           END-IF
		if (ws.getIndPoli().getCarGest() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-COD-DVS-NULL
			poli.setPolCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_DVS));
		}
		// COB_CODE: IF IND-POL-FL-FNT-AZ = -1
		//              MOVE HIGH-VALUES TO POL-FL-FNT-AZ-NULL
		//           END-IF
		if (ws.getIndPoli().getCarInc() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-AZ-NULL
			poli.setPolFlFntAz(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-POL-FL-FNT-ADER = -1
		//              MOVE HIGH-VALUES TO POL-FL-FNT-ADER-NULL
		//           END-IF
		if (ws.getIndPoli().getProvAcq1aa() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-ADER-NULL
			poli.setPolFlFntAder(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-POL-FL-FNT-TFR = -1
		//              MOVE HIGH-VALUES TO POL-FL-FNT-TFR-NULL
		//           END-IF
		if (ws.getIndPoli().getProvAcq2aa() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-TFR-NULL
			poli.setPolFlFntTfr(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-POL-FL-FNT-VOLO = -1
		//              MOVE HIGH-VALUES TO POL-FL-FNT-VOLO-NULL
		//           END-IF
		if (ws.getIndPoli().getProvRicor() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-VOLO-NULL
			poli.setPolFlFntVolo(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-POL-TP-OPZ-A-SCAD = -1
		//              MOVE HIGH-VALUES TO POL-TP-OPZ-A-SCAD-NULL
		//           END-IF
		if (ws.getIndPoli().getProvInc() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-TP-OPZ-A-SCAD-NULL
			poli.setPolTpOpzAScad(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_TP_OPZ_A_SCAD));
		}
		// COB_CODE: IF IND-POL-AA-DIFF-PROR-DFLT = -1
		//              MOVE HIGH-VALUES TO POL-AA-DIFF-PROR-DFLT-NULL
		//           END-IF
		if (ws.getIndPoli().getProvDaRec() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-AA-DIFF-PROR-DFLT-NULL
			poli.getPolAaDiffProrDflt()
					.setPolAaDiffProrDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolAaDiffProrDflt.Len.POL_AA_DIFF_PROR_DFLT_NULL));
		}
		// COB_CODE: IF IND-POL-FL-VER-PROD = -1
		//              MOVE HIGH-VALUES TO POL-FL-VER-PROD-NULL
		//           END-IF
		if (ws.getIndPoli().getCodDvs() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-FL-VER-PROD-NULL
			poli.setPolFlVerProd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_FL_VER_PROD));
		}
		// COB_CODE: IF IND-POL-DUR-GG = -1
		//              MOVE HIGH-VALUES TO POL-DUR-GG-NULL
		//           END-IF
		if (ws.getIndPoli().getFrqMovi() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-DUR-GG-NULL
			poli.getPolDurGg().setPolDurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDurGg.Len.POL_DUR_GG_NULL));
		}
		// COB_CODE: IF IND-POL-DIR-QUIET = -1
		//              MOVE HIGH-VALUES TO POL-DIR-QUIET-NULL
		//           END-IF
		if (ws.getIndPoli().getCodTari() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-DIR-QUIET-NULL
			poli.getPolDirQuiet().setPolDirQuietNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDirQuiet.Len.POL_DIR_QUIET_NULL));
		}
		// COB_CODE: IF IND-POL-TP-PTF-ESTNO = -1
		//              MOVE HIGH-VALUES TO POL-TP-PTF-ESTNO-NULL
		//           END-IF
		if (ws.getIndPoli().getImpAz() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-TP-PTF-ESTNO-NULL
			poli.setPolTpPtfEstno(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_TP_PTF_ESTNO));
		}
		// COB_CODE: IF IND-POL-FL-CUM-PRE-CNTR = -1
		//              MOVE HIGH-VALUES TO POL-FL-CUM-PRE-CNTR-NULL
		//           END-IF
		if (ws.getIndPoli().getImpAder() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-FL-CUM-PRE-CNTR-NULL
			poli.setPolFlCumPreCntr(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-POL-FL-AMMB-MOVI = -1
		//              MOVE HIGH-VALUES TO POL-FL-AMMB-MOVI-NULL
		//           END-IF
		if (ws.getIndPoli().getImpTfr() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-FL-AMMB-MOVI-NULL
			poli.setPolFlAmmbMovi(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-POL-CONV-GECO = -1
		//              MOVE HIGH-VALUES TO POL-CONV-GECO-NULL
		//           END-IF
		if (ws.getIndPoli().getImpVolo() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-CONV-GECO-NULL
			poli.setPolConvGeco(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_CONV_GECO));
		}
		// COB_CODE: IF IND-POL-FL-SCUDO-FISC = -1
		//              MOVE HIGH-VALUES TO POL-FL-SCUDO-FISC-NULL
		//           END-IF
		if (ws.getIndPoli().getManfeeAntic() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-FL-SCUDO-FISC-NULL
			poli.setPolFlScudoFisc(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-POL-FL-TRASFE = -1
		//              MOVE HIGH-VALUES TO POL-FL-TRASFE-NULL
		//           END-IF
		if (ws.getIndPoli().getManfeeRicor() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-FL-TRASFE-NULL
			poli.setPolFlTrasfe(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-POL-FL-TFR-STRC = -1
		//              MOVE HIGH-VALUES TO POL-FL-TFR-STRC-NULL
		//           END-IF
		if (ws.getIndPoli().getManfeeRec() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-FL-TFR-STRC-NULL
			poli.setPolFlTfrStrc(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-POL-DT-PRESC = -1
		//              MOVE HIGH-VALUES TO POL-DT-PRESC-NULL
		//           END-IF
		if (ws.getIndPoli().getDtEsiTit() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-DT-PRESC-NULL
			poli.getPolDtPresc().setPolDtPrescNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtPresc.Len.POL_DT_PRESC_NULL));
		}
		// COB_CODE: IF IND-POL-COD-CONV-AGG = -1
		//              MOVE HIGH-VALUES TO POL-COD-CONV-AGG-NULL
		//           END-IF
		if (ws.getIndPoli().getSpeAge() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-COD-CONV-AGG-NULL
			poli.setPolCodConvAgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_CONV_AGG));
		}
		// COB_CODE: IF IND-POL-SUBCAT-PROD = -1
		//              MOVE HIGH-VALUES TO POL-SUBCAT-PROD-NULL
		//           END-IF.
		if (ws.getIndPoli().getCarIas() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-SUBCAT-PROD-NULL
			poli.setPolSubcatProd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_SUBCAT_PROD));
		}
		// COB_CODE: IF IND-POL-FL-QUEST-ADEGZ-ASS = -1
		//              MOVE HIGH-VALUES TO POL-FL-QUEST-ADEGZ-ASS-NULL
		//           END-IF
		if (ws.getIndPoli().getTotIntrPrest() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-FL-QUEST-ADEGZ-ASS-NULL
			poli.setPolFlQuestAdegzAss(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-POL-COD-TPA = -1
		//              MOVE HIGH-VALUES TO POL-COD-TPA-NULL
		//           END-IF
		if (ws.getIndPoli().getImpTrasfe() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-COD-TPA-NULL
			poli.setPolCodTpa(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_TPA));
		}
		// COB_CODE: IF IND-POL-ID-ACC-COMM = -1
		//              MOVE HIGH-VALUES TO POL-ID-ACC-COMM-NULL
		//           END-IF
		if (ws.getIndPoli().getImpTfrStrc() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-ID-ACC-COMM-NULL
			poli.getPolIdAccComm().setPolIdAccCommNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolIdAccComm.Len.POL_ID_ACC_COMM_NULL));
		}
		// COB_CODE: IF IND-POL-FL-POLI-CPI-PR = -1
		//              MOVE HIGH-VALUES TO POL-FL-POLI-CPI-PR-NULL
		//           END-IF
		if (ws.getIndPoli().getNumGgRitardoPag() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-FL-POLI-CPI-PR-NULL
			poli.setPolFlPoliCpiPr(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-POL-FL-POLI-BUNDLING = -1
		//              MOVE HIGH-VALUES TO POL-FL-POLI-BUNDLING-NULL
		//           END-IF
		if (ws.getIndPoli().getNumGgRival() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-FL-POLI-BUNDLING-NULL
			poli.setPolFlPoliBundling(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-POL-IND-POLI-PRIN-COLL = -1
		//              MOVE HIGH-VALUES TO POL-IND-POLI-PRIN-COLL-NULL
		//           END-IF
		if (ws.getIndPoli().getAcqExp() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-IND-POLI-PRIN-COLL-NULL
			poli.setPolIndPoliPrinColl(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-POL-FL-VND-BUNDLE = -1
		//              MOVE HIGH-VALUES TO POL-FL-VND-BUNDLE-NULL
		//           END-IF.
		if (ws.getIndPoli().getRemunAss() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-FL-VND-BUNDLE-NULL
			poli.setPolFlVndBundle(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-POL-IB-BS = -1
		//              MOVE HIGH-VALUES TO POL-IB-BS-NULL
		//           END-IF.
		if (ws.getIndPoli().getCommisInter() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-IB-BS-NULL
			poli.setPolIbBs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_IB_BS));
		}
		// COB_CODE: IF IND-POL-FL-POLI-IFP = -1
		//              MOVE HIGH-VALUES TO POL-FL-POLI-IFP-NULL
		//           END-IF.
		if (ws.getIndPoli().getCnbtAntirac() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO POL-FL-POLI-IFP-NULL
			poli.setPolFlPoliIfp(Types.HIGH_CHAR_VAL);
		}
		// COB_CODE: IF IND-STB-ID-MOVI-CHIU = -1
		//              MOVE HIGH-VALUES TO STB-ID-MOVI-CHIU-NULL
		//           END-IF.
		if (ws.getIndStbIdMoviChiu() == -1) {
			// COB_CODE: MOVE HIGH-VALUES TO STB-ID-MOVI-CHIU-NULL
			statOggBus.getStbIdMoviChiu().setStbIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, StbIdMoviChiu.Len.STB_ID_MOVI_CHIU_NULL));
		}
	}

	/**Original name: Z150-VALORIZZA-DATA-SERVICES<br>
	 * <pre>*****************************************************************</pre>*/
	private void z150ValorizzaDataServices() {
		// COB_CODE: MOVE IDSV0003-OPERAZIONE     TO POL-DS-OPER-SQL.
		poli.setPolDsOperSqlFormatted(idsv0003.getOperazione().getOperazioneFormatted());
		// COB_CODE: MOVE IDSV0003-USER-NAME      TO POL-DS-UTENTE.
		poli.setPolDsUtente(idsv0003.getUserName());
	}

	/**Original name: Z200-SET-INDICATORI-NULL<br>
	 * <pre>*****************************************************************</pre>*/
	private void z200SetIndicatoriNull() {
		// COB_CODE: IF ADE-ID-MOVI-CHIU-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-ID-MOVI-CHIU
		//           ELSE
		//              MOVE 0 TO IND-ADE-ID-MOVI-CHIU
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeIdMoviChiu().getAdeIdMoviChiuNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-ID-MOVI-CHIU
			ws.getIndAdes().setIdMoviChiu(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-ID-MOVI-CHIU
			ws.getIndAdes().setIdMoviChiu(((short) 0));
		}
		// COB_CODE: IF ADE-IB-PREV-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-IB-PREV
		//           ELSE
		//              MOVE 0 TO IND-ADE-IB-PREV
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeIbPrev(), Ades.Len.ADE_IB_PREV)) {
			// COB_CODE: MOVE -1 TO IND-ADE-IB-PREV
			ws.getIndAdes().setIbPrev(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-IB-PREV
			ws.getIndAdes().setIbPrev(((short) 0));
		}
		// COB_CODE: IF ADE-IB-OGG-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-IB-OGG
		//           ELSE
		//              MOVE 0 TO IND-ADE-IB-OGG
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeIbOgg(), Ades.Len.ADE_IB_OGG)) {
			// COB_CODE: MOVE -1 TO IND-ADE-IB-OGG
			ws.getIndAdes().setIbOgg(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-IB-OGG
			ws.getIndAdes().setIbOgg(((short) 0));
		}
		// COB_CODE: IF ADE-DT-DECOR-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-DT-DECOR
		//           ELSE
		//              MOVE 0 TO IND-ADE-DT-DECOR
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeDtDecor().getAdeDtDecorNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-DT-DECOR
			ws.getIndAdes().setDtDecor(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-DT-DECOR
			ws.getIndAdes().setDtDecor(((short) 0));
		}
		// COB_CODE: IF ADE-DT-SCAD-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-DT-SCAD
		//           ELSE
		//              MOVE 0 TO IND-ADE-DT-SCAD
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeDtScad().getAdeDtScadNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-DT-SCAD
			ws.getIndAdes().setDtScad(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-DT-SCAD
			ws.getIndAdes().setDtScad(((short) 0));
		}
		// COB_CODE: IF ADE-ETA-A-SCAD-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-ETA-A-SCAD
		//           ELSE
		//              MOVE 0 TO IND-ADE-ETA-A-SCAD
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeEtaAScad().getAdeEtaAScadNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-ETA-A-SCAD
			ws.getIndAdes().setEtaAScad(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-ETA-A-SCAD
			ws.getIndAdes().setEtaAScad(((short) 0));
		}
		// COB_CODE: IF ADE-DUR-AA-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-DUR-AA
		//           ELSE
		//              MOVE 0 TO IND-ADE-DUR-AA
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeDurAa().getAdeDurAaNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-DUR-AA
			ws.getIndAdes().setDurAa(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-DUR-AA
			ws.getIndAdes().setDurAa(((short) 0));
		}
		// COB_CODE: IF ADE-DUR-MM-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-DUR-MM
		//           ELSE
		//              MOVE 0 TO IND-ADE-DUR-MM
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeDurMm().getAdeDurMmNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-DUR-MM
			ws.getIndAdes().setDurMm(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-DUR-MM
			ws.getIndAdes().setDurMm(((short) 0));
		}
		// COB_CODE: IF ADE-DUR-GG-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-DUR-GG
		//           ELSE
		//              MOVE 0 TO IND-ADE-DUR-GG
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeDurGg().getAdeDurGgNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-DUR-GG
			ws.getIndAdes().setDurGg(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-DUR-GG
			ws.getIndAdes().setDurGg(((short) 0));
		}
		// COB_CODE: IF ADE-TP-RIAT-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-TP-RIAT
		//           ELSE
		//              MOVE 0 TO IND-ADE-TP-RIAT
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeTpRiatFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-TP-RIAT
			ws.getIndAdes().setTpRiat(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-TP-RIAT
			ws.getIndAdes().setTpRiat(((short) 0));
		}
		// COB_CODE: IF ADE-TP-IAS-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-TP-IAS
		//           ELSE
		//              MOVE 0 TO IND-ADE-TP-IAS
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeTpIasFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-TP-IAS
			ws.getIndAdes().setTpIas(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-TP-IAS
			ws.getIndAdes().setTpIas(((short) 0));
		}
		// COB_CODE: IF ADE-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-DT-VARZ-TP-IAS
		//           ELSE
		//              MOVE 0 TO IND-ADE-DT-VARZ-TP-IAS
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeDtVarzTpIas().getAdeDtVarzTpIasNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-DT-VARZ-TP-IAS
			ws.getIndAdes().setDtVarzTpIas(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-DT-VARZ-TP-IAS
			ws.getIndAdes().setDtVarzTpIas(((short) 0));
		}
		// COB_CODE: IF ADE-PRE-NET-IND-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-PRE-NET-IND
		//           ELSE
		//              MOVE 0 TO IND-ADE-PRE-NET-IND
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdePreNetInd().getAdePreNetIndNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-PRE-NET-IND
			ws.getIndAdes().setPreNetInd(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-PRE-NET-IND
			ws.getIndAdes().setPreNetInd(((short) 0));
		}
		// COB_CODE: IF ADE-PRE-LRD-IND-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-PRE-LRD-IND
		//           ELSE
		//              MOVE 0 TO IND-ADE-PRE-LRD-IND
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdePreLrdInd().getAdePreLrdIndNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-PRE-LRD-IND
			ws.getIndAdes().setPreLrdInd(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-PRE-LRD-IND
			ws.getIndAdes().setPreLrdInd(((short) 0));
		}
		// COB_CODE: IF ADE-RAT-LRD-IND-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-RAT-LRD-IND
		//           ELSE
		//              MOVE 0 TO IND-ADE-RAT-LRD-IND
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeRatLrdInd().getAdeRatLrdIndNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-RAT-LRD-IND
			ws.getIndAdes().setRatLrdInd(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-RAT-LRD-IND
			ws.getIndAdes().setRatLrdInd(((short) 0));
		}
		// COB_CODE: IF ADE-PRSTZ-INI-IND-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-PRSTZ-INI-IND
		//           ELSE
		//              MOVE 0 TO IND-ADE-PRSTZ-INI-IND
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdePrstzIniInd().getAdePrstzIniIndNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-PRSTZ-INI-IND
			ws.getIndAdes().setPrstzIniInd(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-PRSTZ-INI-IND
			ws.getIndAdes().setPrstzIniInd(((short) 0));
		}
		// COB_CODE: IF ADE-FL-COINC-ASSTO-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-FL-COINC-ASSTO
		//           ELSE
		//              MOVE 0 TO IND-ADE-FL-COINC-ASSTO
		//           END-IF
		if (Conditions.eq(ades.getAdeFlCoincAssto(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-ADE-FL-COINC-ASSTO
			ws.getIndAdes().setFlCoincAssto(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-FL-COINC-ASSTO
			ws.getIndAdes().setFlCoincAssto(((short) 0));
		}
		// COB_CODE: IF ADE-IB-DFLT-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-IB-DFLT
		//           ELSE
		//              MOVE 0 TO IND-ADE-IB-DFLT
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeIbDflt(), Ades.Len.ADE_IB_DFLT)) {
			// COB_CODE: MOVE -1 TO IND-ADE-IB-DFLT
			ws.getIndAdes().setIbDflt(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-IB-DFLT
			ws.getIndAdes().setIbDflt(((short) 0));
		}
		// COB_CODE: IF ADE-MOD-CALC-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-MOD-CALC
		//           ELSE
		//              MOVE 0 TO IND-ADE-MOD-CALC
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeModCalcFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-MOD-CALC
			ws.getIndAdes().setModCalc(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-MOD-CALC
			ws.getIndAdes().setModCalc(((short) 0));
		}
		// COB_CODE: IF ADE-TP-FNT-CNBTVA-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-TP-FNT-CNBTVA
		//           ELSE
		//              MOVE 0 TO IND-ADE-TP-FNT-CNBTVA
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeTpFntCnbtvaFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-TP-FNT-CNBTVA
			ws.getIndAdes().setTpFntCnbtva(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-TP-FNT-CNBTVA
			ws.getIndAdes().setTpFntCnbtva(((short) 0));
		}
		// COB_CODE: IF ADE-IMP-AZ-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-IMP-AZ
		//           ELSE
		//              MOVE 0 TO IND-ADE-IMP-AZ
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeImpAz().getAdeImpAzNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-IMP-AZ
			ws.getIndAdes().setImpAz(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-IMP-AZ
			ws.getIndAdes().setImpAz(((short) 0));
		}
		// COB_CODE: IF ADE-IMP-ADER-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-IMP-ADER
		//           ELSE
		//              MOVE 0 TO IND-ADE-IMP-ADER
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeImpAder().getAdeImpAderNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-IMP-ADER
			ws.getIndAdes().setImpAder(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-IMP-ADER
			ws.getIndAdes().setImpAder(((short) 0));
		}
		// COB_CODE: IF ADE-IMP-TFR-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-IMP-TFR
		//           ELSE
		//              MOVE 0 TO IND-ADE-IMP-TFR
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeImpTfr().getAdeImpTfrNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-IMP-TFR
			ws.getIndAdes().setImpTfr(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-IMP-TFR
			ws.getIndAdes().setImpTfr(((short) 0));
		}
		// COB_CODE: IF ADE-IMP-VOLO-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-IMP-VOLO
		//           ELSE
		//              MOVE 0 TO IND-ADE-IMP-VOLO
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeImpVolo().getAdeImpVoloNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-IMP-VOLO
			ws.getIndAdes().setImpVolo(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-IMP-VOLO
			ws.getIndAdes().setImpVolo(((short) 0));
		}
		// COB_CODE: IF ADE-PC-AZ-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-PC-AZ
		//           ELSE
		//              MOVE 0 TO IND-ADE-PC-AZ
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdePcAz().getAdePcAzNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-PC-AZ
			ws.getIndAdes().setPcAz(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-PC-AZ
			ws.getIndAdes().setPcAz(((short) 0));
		}
		// COB_CODE: IF ADE-PC-ADER-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-PC-ADER
		//           ELSE
		//              MOVE 0 TO IND-ADE-PC-ADER
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdePcAder().getAdePcAderNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-PC-ADER
			ws.getIndAdes().setPcAder(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-PC-ADER
			ws.getIndAdes().setPcAder(((short) 0));
		}
		// COB_CODE: IF ADE-PC-TFR-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-PC-TFR
		//           ELSE
		//              MOVE 0 TO IND-ADE-PC-TFR
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdePcTfr().getAdePcTfrNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-PC-TFR
			ws.getIndAdes().setPcTfr(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-PC-TFR
			ws.getIndAdes().setPcTfr(((short) 0));
		}
		// COB_CODE: IF ADE-PC-VOLO-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-PC-VOLO
		//           ELSE
		//              MOVE 0 TO IND-ADE-PC-VOLO
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdePcVolo().getAdePcVoloNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-PC-VOLO
			ws.getIndAdes().setPcVolo(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-PC-VOLO
			ws.getIndAdes().setPcVolo(((short) 0));
		}
		// COB_CODE: IF ADE-DT-NOVA-RGM-FISC-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-DT-NOVA-RGM-FISC
		//           ELSE
		//              MOVE 0 TO IND-ADE-DT-NOVA-RGM-FISC
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeDtNovaRgmFisc().getAdeDtNovaRgmFiscNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-DT-NOVA-RGM-FISC
			ws.getIndAdes().setDtNovaRgmFisc(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-DT-NOVA-RGM-FISC
			ws.getIndAdes().setDtNovaRgmFisc(((short) 0));
		}
		// COB_CODE: IF ADE-FL-ATTIV-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-FL-ATTIV
		//           ELSE
		//              MOVE 0 TO IND-ADE-FL-ATTIV
		//           END-IF
		if (Conditions.eq(ades.getAdeFlAttiv(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-ADE-FL-ATTIV
			ws.getIndAdes().setFlAttiv(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-FL-ATTIV
			ws.getIndAdes().setFlAttiv(((short) 0));
		}
		// COB_CODE: IF ADE-IMP-REC-RIT-VIS-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-IMP-REC-RIT-VIS
		//           ELSE
		//              MOVE 0 TO IND-ADE-IMP-REC-RIT-VIS
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeImpRecRitVis().getAdeImpRecRitVisNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-IMP-REC-RIT-VIS
			ws.getIndAdes().setImpRecRitVis(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-IMP-REC-RIT-VIS
			ws.getIndAdes().setImpRecRitVis(((short) 0));
		}
		// COB_CODE: IF ADE-IMP-REC-RIT-ACC-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-IMP-REC-RIT-ACC
		//           ELSE
		//              MOVE 0 TO IND-ADE-IMP-REC-RIT-ACC
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeImpRecRitAcc().getAdeImpRecRitAccNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-IMP-REC-RIT-ACC
			ws.getIndAdes().setImpRecRitAcc(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-IMP-REC-RIT-ACC
			ws.getIndAdes().setImpRecRitAcc(((short) 0));
		}
		// COB_CODE: IF ADE-FL-VARZ-STAT-TBGC-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-FL-VARZ-STAT-TBGC
		//           ELSE
		//              MOVE 0 TO IND-ADE-FL-VARZ-STAT-TBGC
		//           END-IF
		if (Conditions.eq(ades.getAdeFlVarzStatTbgc(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-ADE-FL-VARZ-STAT-TBGC
			ws.getIndAdes().setFlVarzStatTbgc(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-FL-VARZ-STAT-TBGC
			ws.getIndAdes().setFlVarzStatTbgc(((short) 0));
		}
		// COB_CODE: IF ADE-FL-PROVZA-MIGRAZ-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-FL-PROVZA-MIGRAZ
		//           ELSE
		//              MOVE 0 TO IND-ADE-FL-PROVZA-MIGRAZ
		//           END-IF
		if (Conditions.eq(ades.getAdeFlProvzaMigraz(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-ADE-FL-PROVZA-MIGRAZ
			ws.getIndAdes().setFlProvzaMigraz(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-FL-PROVZA-MIGRAZ
			ws.getIndAdes().setFlProvzaMigraz(((short) 0));
		}
		// COB_CODE: IF ADE-IMPB-VIS-DA-REC-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-IMPB-VIS-DA-REC
		//           ELSE
		//              MOVE 0 TO IND-ADE-IMPB-VIS-DA-REC
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeImpbVisDaRec().getAdeImpbVisDaRecNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-IMPB-VIS-DA-REC
			ws.getIndAdes().setImpbVisDaRec(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-IMPB-VIS-DA-REC
			ws.getIndAdes().setImpbVisDaRec(((short) 0));
		}
		// COB_CODE: IF ADE-DT-DECOR-PREST-BAN-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-DT-DECOR-PREST-BAN
		//           ELSE
		//              MOVE 0 TO IND-ADE-DT-DECOR-PREST-BAN
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeDtDecorPrestBan().getAdeDtDecorPrestBanNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-DT-DECOR-PREST-BAN
			ws.getIndAdes().setDtDecorPrestBan(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-DT-DECOR-PREST-BAN
			ws.getIndAdes().setDtDecorPrestBan(((short) 0));
		}
		// COB_CODE: IF ADE-DT-EFF-VARZ-STAT-T-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-DT-EFF-VARZ-STAT-T
		//           ELSE
		//              MOVE 0 TO IND-ADE-DT-EFF-VARZ-STAT-T
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeDtEffVarzStatT().getAdeDtEffVarzStatTNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-DT-EFF-VARZ-STAT-T
			ws.getIndAdes().setDtEffVarzStatT(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-DT-EFF-VARZ-STAT-T
			ws.getIndAdes().setDtEffVarzStatT(((short) 0));
		}
		// COB_CODE: IF ADE-CUM-CNBT-CAP-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-CUM-CNBT-CAP
		//           ELSE
		//              MOVE 0 TO IND-ADE-CUM-CNBT-CAP
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeCumCnbtCap().getAdeCumCnbtCapNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-CUM-CNBT-CAP
			ws.getIndAdes().setCumCnbtCap(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-CUM-CNBT-CAP
			ws.getIndAdes().setCumCnbtCap(((short) 0));
		}
		// COB_CODE: IF ADE-IMP-GAR-CNBT-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-IMP-GAR-CNBT
		//           ELSE
		//              MOVE 0 TO IND-ADE-IMP-GAR-CNBT
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeImpGarCnbt().getAdeImpGarCnbtNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-IMP-GAR-CNBT
			ws.getIndAdes().setImpGarCnbt(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-IMP-GAR-CNBT
			ws.getIndAdes().setImpGarCnbt(((short) 0));
		}
		// COB_CODE: IF ADE-DT-ULT-CONS-CNBT-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-DT-ULT-CONS-CNBT
		//           ELSE
		//              MOVE 0 TO IND-ADE-DT-ULT-CONS-CNBT
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeDtUltConsCnbt().getAdeDtUltConsCnbtNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-DT-ULT-CONS-CNBT
			ws.getIndAdes().setDtUltConsCnbt(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-DT-ULT-CONS-CNBT
			ws.getIndAdes().setDtUltConsCnbt(((short) 0));
		}
		// COB_CODE: IF ADE-IDEN-ISC-FND-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-IDEN-ISC-FND
		//           ELSE
		//              MOVE 0 TO IND-ADE-IDEN-ISC-FND
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeIdenIscFnd(), Ades.Len.ADE_IDEN_ISC_FND)) {
			// COB_CODE: MOVE -1 TO IND-ADE-IDEN-ISC-FND
			ws.getIndAdes().setIdenIscFnd(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-IDEN-ISC-FND
			ws.getIndAdes().setIdenIscFnd(((short) 0));
		}
		// COB_CODE: IF ADE-NUM-RAT-PIAN-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-NUM-RAT-PIAN
		//           ELSE
		//              MOVE 0 TO IND-ADE-NUM-RAT-PIAN
		//           END-IF
		if (Characters.EQ_HIGH.test(ades.getAdeNumRatPian().getAdeNumRatPianNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-NUM-RAT-PIAN
			ws.getIndAdes().setNumRatPian(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-NUM-RAT-PIAN
			ws.getIndAdes().setNumRatPian(((short) 0));
		}
		// COB_CODE: IF ADE-DT-PRESC-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-DT-PRESC
		//           ELSE
		//              MOVE 0 TO IND-ADE-DT-PRESC
		//           END-IF.
		if (Characters.EQ_HIGH.test(ades.getAdeDtPresc().getAdeDtPrescNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-ADE-DT-PRESC
			ws.getIndAdes().setDtPresc(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-DT-PRESC
			ws.getIndAdes().setDtPresc(((short) 0));
		}
		// COB_CODE: IF ADE-CONCS-PREST-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-ADE-CONCS-PREST
		//           ELSE
		//              MOVE 0 TO IND-ADE-CONCS-PREST
		//           END-IF.
		if (Conditions.eq(ades.getAdeConcsPrest(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-ADE-CONCS-PREST
			ws.getIndAdes().setConcsPrest(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-ADE-CONCS-PREST
			ws.getIndAdes().setConcsPrest(((short) 0));
		}
		// COB_CODE: IF POL-ID-MOVI-CHIU-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-ID-MOVI-CHIU
		//           ELSE
		//              MOVE 0 TO IND-POL-ID-MOVI-CHIU
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolIdMoviChiu().getPolIdMoviChiuNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-ID-MOVI-CHIU
			ws.getIndPoli().setIdMoviChiu(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-ID-MOVI-CHIU
			ws.getIndPoli().setIdMoviChiu(((short) 0));
		}
		// COB_CODE: IF POL-IB-OGG-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-IB-OGG
		//           ELSE
		//              MOVE 0 TO IND-POL-IB-OGG
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolIbOgg(), PoliIdbspol0.Len.POL_IB_OGG)) {
			// COB_CODE: MOVE -1 TO IND-POL-IB-OGG
			ws.getIndPoli().setDtIniCop(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-IB-OGG
			ws.getIndPoli().setDtIniCop(((short) 0));
		}
		// COB_CODE: IF POL-DT-PROP-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-DT-PROP
		//           ELSE
		//              MOVE 0 TO IND-POL-DT-PROP
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolDtProp().getPolDtPropNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-DT-PROP
			ws.getIndPoli().setDtEndCop(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-DT-PROP
			ws.getIndPoli().setDtEndCop(((short) 0));
		}
		// COB_CODE: IF POL-DUR-AA-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-DUR-AA
		//           ELSE
		//              MOVE 0 TO IND-POL-DUR-AA
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolDurAa().getPolDurAaNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-DUR-AA
			ws.getIndPoli().setPreNet(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-DUR-AA
			ws.getIndPoli().setPreNet(((short) 0));
		}
		// COB_CODE: IF POL-DUR-MM-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-DUR-MM
		//           ELSE
		//              MOVE 0 TO IND-POL-DUR-MM
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolDurMm().getPolDurMmNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-DUR-MM
			ws.getIndPoli().setIntrFraz(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-DUR-MM
			ws.getIndPoli().setIntrFraz(((short) 0));
		}
		// COB_CODE: IF POL-DT-SCAD-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-DT-SCAD
		//           ELSE
		//              MOVE 0 TO IND-POL-DT-SCAD
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolDtScad().getPolDtScadNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-DT-SCAD
			ws.getIndPoli().setIntrMora(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-DT-SCAD
			ws.getIndPoli().setIntrMora(((short) 0));
		}
		// COB_CODE: IF POL-COD-CONV-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-COD-CONV
		//           ELSE
		//              MOVE 0 TO IND-POL-COD-CONV
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolCodConvFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-COD-CONV
			ws.getIndPoli().setIntrRetdt(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-COD-CONV
			ws.getIndPoli().setIntrRetdt(((short) 0));
		}
		// COB_CODE: IF POL-COD-RAMO-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-COD-RAMO
		//           ELSE
		//              MOVE 0 TO IND-POL-COD-RAMO
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolCodRamoFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-COD-RAMO
			ws.getIndPoli().setIntrRiat(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-COD-RAMO
			ws.getIndPoli().setIntrRiat(((short) 0));
		}
		// COB_CODE: IF POL-DT-INI-VLDT-CONV-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-DT-INI-VLDT-CONV
		//           ELSE
		//              MOVE 0 TO IND-POL-DT-INI-VLDT-CONV
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolDtIniVldtConv().getPolDtIniVldtConvNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-DT-INI-VLDT-CONV
			ws.getIndPoli().setDir(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-DT-INI-VLDT-CONV
			ws.getIndPoli().setDir(((short) 0));
		}
		// COB_CODE: IF POL-DT-APPLZ-CONV-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-DT-APPLZ-CONV
		//           ELSE
		//              MOVE 0 TO IND-POL-DT-APPLZ-CONV
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolDtApplzConv().getPolDtApplzConvNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-DT-APPLZ-CONV
			ws.getIndPoli().setSpeMed(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-DT-APPLZ-CONV
			ws.getIndPoli().setSpeMed(((short) 0));
		}
		// COB_CODE: IF POL-TP-RGM-FISC-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-TP-RGM-FISC
		//           ELSE
		//              MOVE 0 TO IND-POL-TP-RGM-FISC
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolTpRgmFiscFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-TP-RGM-FISC
			ws.getIndPoli().setTax(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-TP-RGM-FISC
			ws.getIndPoli().setTax(((short) 0));
		}
		// COB_CODE: IF POL-FL-ESTAS-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-FL-ESTAS
		//           ELSE
		//              MOVE 0 TO IND-POL-FL-ESTAS
		//           END-IF
		if (Conditions.eq(poli.getPolFlEstas(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-POL-FL-ESTAS
			ws.getIndPoli().setSoprSan(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-FL-ESTAS
			ws.getIndPoli().setSoprSan(((short) 0));
		}
		// COB_CODE: IF POL-FL-RSH-COMUN-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-FL-RSH-COMUN
		//           ELSE
		//              MOVE 0 TO IND-POL-FL-RSH-COMUN
		//           END-IF
		if (Conditions.eq(poli.getPolFlRshComun(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-POL-FL-RSH-COMUN
			ws.getIndPoli().setSoprSpo(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-FL-RSH-COMUN
			ws.getIndPoli().setSoprSpo(((short) 0));
		}
		// COB_CODE: IF POL-FL-RSH-COMUN-COND-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-FL-RSH-COMUN-COND
		//           ELSE
		//              MOVE 0 TO IND-POL-FL-RSH-COMUN-COND
		//           END-IF
		if (Conditions.eq(poli.getPolFlRshComunCond(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-POL-FL-RSH-COMUN-COND
			ws.getIndPoli().setSoprTec(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-FL-RSH-COMUN-COND
			ws.getIndPoli().setSoprTec(((short) 0));
		}
		// COB_CODE: IF POL-FL-COP-FINANZ-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-FL-COP-FINANZ
		//           ELSE
		//              MOVE 0 TO IND-POL-FL-COP-FINANZ
		//           END-IF
		if (Conditions.eq(poli.getPolFlCopFinanz(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-POL-FL-COP-FINANZ
			ws.getIndPoli().setSoprProf(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-FL-COP-FINANZ
			ws.getIndPoli().setSoprProf(((short) 0));
		}
		// COB_CODE: IF POL-TP-APPLZ-DIR-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-TP-APPLZ-DIR
		//           ELSE
		//              MOVE 0 TO IND-POL-TP-APPLZ-DIR
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolTpApplzDirFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-TP-APPLZ-DIR
			ws.getIndPoli().setSoprAlt(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-TP-APPLZ-DIR
			ws.getIndPoli().setSoprAlt(((short) 0));
		}
		// COB_CODE: IF POL-SPE-MED-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-SPE-MED
		//           ELSE
		//              MOVE 0 TO IND-POL-SPE-MED
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolSpeMed().getPolSpeMedNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-SPE-MED
			ws.getIndPoli().setPreTot(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-SPE-MED
			ws.getIndPoli().setPreTot(((short) 0));
		}
		// COB_CODE: IF POL-DIR-EMIS-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-DIR-EMIS
		//           ELSE
		//              MOVE 0 TO IND-POL-DIR-EMIS
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolDirEmis().getPolDirEmisNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-DIR-EMIS
			ws.getIndPoli().setPrePpIas(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-DIR-EMIS
			ws.getIndPoli().setPrePpIas(((short) 0));
		}
		// COB_CODE: IF POL-DIR-1O-VERS-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-DIR-1O-VERS
		//           ELSE
		//              MOVE 0 TO IND-POL-DIR-1O-VERS
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolDir1oVers().getPolDir1oVersNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-DIR-1O-VERS
			ws.getIndPoli().setPreSoloRsh(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-DIR-1O-VERS
			ws.getIndPoli().setPreSoloRsh(((short) 0));
		}
		// COB_CODE: IF POL-DIR-VERS-AGG-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-DIR-VERS-AGG
		//           ELSE
		//              MOVE 0 TO IND-POL-DIR-VERS-AGG
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolDirVersAgg().getPolDirVersAggNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-DIR-VERS-AGG
			ws.getIndPoli().setCarAcq(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-DIR-VERS-AGG
			ws.getIndPoli().setCarAcq(((short) 0));
		}
		// COB_CODE: IF POL-COD-DVS-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-COD-DVS
		//           ELSE
		//              MOVE 0 TO IND-POL-COD-DVS
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolCodDvsFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-COD-DVS
			ws.getIndPoli().setCarGest(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-COD-DVS
			ws.getIndPoli().setCarGest(((short) 0));
		}
		// COB_CODE: IF POL-FL-FNT-AZ-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-FL-FNT-AZ
		//           ELSE
		//              MOVE 0 TO IND-POL-FL-FNT-AZ
		//           END-IF
		if (Conditions.eq(poli.getPolFlFntAz(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-POL-FL-FNT-AZ
			ws.getIndPoli().setCarInc(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-FL-FNT-AZ
			ws.getIndPoli().setCarInc(((short) 0));
		}
		// COB_CODE: IF POL-FL-FNT-ADER-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-FL-FNT-ADER
		//           ELSE
		//              MOVE 0 TO IND-POL-FL-FNT-ADER
		//           END-IF
		if (Conditions.eq(poli.getPolFlFntAder(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-POL-FL-FNT-ADER
			ws.getIndPoli().setProvAcq1aa(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-FL-FNT-ADER
			ws.getIndPoli().setProvAcq1aa(((short) 0));
		}
		// COB_CODE: IF POL-FL-FNT-TFR-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-FL-FNT-TFR
		//           ELSE
		//              MOVE 0 TO IND-POL-FL-FNT-TFR
		//           END-IF
		if (Conditions.eq(poli.getPolFlFntTfr(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-POL-FL-FNT-TFR
			ws.getIndPoli().setProvAcq2aa(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-FL-FNT-TFR
			ws.getIndPoli().setProvAcq2aa(((short) 0));
		}
		// COB_CODE: IF POL-FL-FNT-VOLO-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-FL-FNT-VOLO
		//           ELSE
		//              MOVE 0 TO IND-POL-FL-FNT-VOLO
		//           END-IF
		if (Conditions.eq(poli.getPolFlFntVolo(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-POL-FL-FNT-VOLO
			ws.getIndPoli().setProvRicor(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-FL-FNT-VOLO
			ws.getIndPoli().setProvRicor(((short) 0));
		}
		// COB_CODE: IF POL-TP-OPZ-A-SCAD-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-TP-OPZ-A-SCAD
		//           ELSE
		//              MOVE 0 TO IND-POL-TP-OPZ-A-SCAD
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolTpOpzAScadFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-TP-OPZ-A-SCAD
			ws.getIndPoli().setProvInc(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-TP-OPZ-A-SCAD
			ws.getIndPoli().setProvInc(((short) 0));
		}
		// COB_CODE: IF POL-AA-DIFF-PROR-DFLT-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-AA-DIFF-PROR-DFLT
		//           ELSE
		//              MOVE 0 TO IND-POL-AA-DIFF-PROR-DFLT
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolAaDiffProrDflt().getPolAaDiffProrDfltNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-AA-DIFF-PROR-DFLT
			ws.getIndPoli().setProvDaRec(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-AA-DIFF-PROR-DFLT
			ws.getIndPoli().setProvDaRec(((short) 0));
		}
		// COB_CODE: IF POL-FL-VER-PROD-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-FL-VER-PROD
		//           ELSE
		//              MOVE 0 TO IND-POL-FL-VER-PROD
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolFlVerProdFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-FL-VER-PROD
			ws.getIndPoli().setCodDvs(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-FL-VER-PROD
			ws.getIndPoli().setCodDvs(((short) 0));
		}
		// COB_CODE: IF POL-DUR-GG-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-DUR-GG
		//           ELSE
		//              MOVE 0 TO IND-POL-DUR-GG
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolDurGg().getPolDurGgNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-DUR-GG
			ws.getIndPoli().setFrqMovi(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-DUR-GG
			ws.getIndPoli().setFrqMovi(((short) 0));
		}
		// COB_CODE: IF POL-DIR-QUIET-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-DIR-QUIET
		//           ELSE
		//              MOVE 0 TO IND-POL-DIR-QUIET
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolDirQuiet().getPolDirQuietNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-DIR-QUIET
			ws.getIndPoli().setCodTari(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-DIR-QUIET
			ws.getIndPoli().setCodTari(((short) 0));
		}
		// COB_CODE: IF POL-TP-PTF-ESTNO-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-TP-PTF-ESTNO
		//           ELSE
		//              MOVE 0 TO IND-POL-TP-PTF-ESTNO
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolTpPtfEstnoFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-TP-PTF-ESTNO
			ws.getIndPoli().setImpAz(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-TP-PTF-ESTNO
			ws.getIndPoli().setImpAz(((short) 0));
		}
		// COB_CODE: IF POL-FL-CUM-PRE-CNTR-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-FL-CUM-PRE-CNTR
		//           ELSE
		//              MOVE 0 TO IND-POL-FL-CUM-PRE-CNTR
		//           END-IF
		if (Conditions.eq(poli.getPolFlCumPreCntr(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-POL-FL-CUM-PRE-CNTR
			ws.getIndPoli().setImpAder(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-FL-CUM-PRE-CNTR
			ws.getIndPoli().setImpAder(((short) 0));
		}
		// COB_CODE: IF POL-FL-AMMB-MOVI-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-FL-AMMB-MOVI
		//           ELSE
		//              MOVE 0 TO IND-POL-FL-AMMB-MOVI
		//           END-IF
		if (Conditions.eq(poli.getPolFlAmmbMovi(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-POL-FL-AMMB-MOVI
			ws.getIndPoli().setImpTfr(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-FL-AMMB-MOVI
			ws.getIndPoli().setImpTfr(((short) 0));
		}
		// COB_CODE: IF POL-CONV-GECO-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-CONV-GECO
		//           ELSE
		//              MOVE 0 TO IND-POL-CONV-GECO
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolConvGecoFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-CONV-GECO
			ws.getIndPoli().setImpVolo(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-CONV-GECO
			ws.getIndPoli().setImpVolo(((short) 0));
		}
		// COB_CODE: IF POL-FL-SCUDO-FISC-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-FL-SCUDO-FISC
		//           ELSE
		//              MOVE 0 TO IND-POL-FL-SCUDO-FISC
		//           END-IF
		if (Conditions.eq(poli.getPolFlScudoFisc(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-POL-FL-SCUDO-FISC
			ws.getIndPoli().setManfeeAntic(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-FL-SCUDO-FISC
			ws.getIndPoli().setManfeeAntic(((short) 0));
		}
		// COB_CODE: IF POL-FL-TRASFE-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-FL-TRASFE
		//           ELSE
		//              MOVE 0 TO IND-POL-FL-TRASFE
		//           END-IF
		if (Conditions.eq(poli.getPolFlTrasfe(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-POL-FL-TRASFE
			ws.getIndPoli().setManfeeRicor(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-FL-TRASFE
			ws.getIndPoli().setManfeeRicor(((short) 0));
		}
		// COB_CODE: IF POL-FL-TFR-STRC-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-FL-TFR-STRC
		//           ELSE
		//              MOVE 0 TO IND-POL-FL-TFR-STRC
		//           END-IF
		if (Conditions.eq(poli.getPolFlTfrStrc(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-POL-FL-TFR-STRC
			ws.getIndPoli().setManfeeRec(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-FL-TFR-STRC
			ws.getIndPoli().setManfeeRec(((short) 0));
		}
		// COB_CODE: IF POL-DT-PRESC-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-DT-PRESC
		//           ELSE
		//              MOVE 0 TO IND-POL-DT-PRESC
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolDtPresc().getPolDtPrescNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-DT-PRESC
			ws.getIndPoli().setDtEsiTit(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-DT-PRESC
			ws.getIndPoli().setDtEsiTit(((short) 0));
		}
		// COB_CODE: IF POL-COD-CONV-AGG-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-COD-CONV-AGG
		//           ELSE
		//              MOVE 0 TO IND-POL-COD-CONV-AGG
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolCodConvAggFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-COD-CONV-AGG
			ws.getIndPoli().setSpeAge(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-COD-CONV-AGG
			ws.getIndPoli().setSpeAge(((short) 0));
		}
		// COB_CODE: IF POL-SUBCAT-PROD-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-SUBCAT-PROD
		//           ELSE
		//              MOVE 0 TO IND-POL-SUBCAT-PROD
		//           END-IF.
		if (Characters.EQ_HIGH.test(poli.getPolSubcatProdFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-SUBCAT-PROD
			ws.getIndPoli().setCarIas(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-SUBCAT-PROD
			ws.getIndPoli().setCarIas(((short) 0));
		}
		// COB_CODE: IF POL-FL-QUEST-ADEGZ-ASS-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-FL-QUEST-ADEGZ-ASS
		//           ELSE
		//              MOVE 0 TO IND-POL-FL-QUEST-ADEGZ-ASS
		//           END-IF
		if (Conditions.eq(poli.getPolFlQuestAdegzAss(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-POL-FL-QUEST-ADEGZ-ASS
			ws.getIndPoli().setTotIntrPrest(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-FL-QUEST-ADEGZ-ASS
			ws.getIndPoli().setTotIntrPrest(((short) 0));
		}
		// COB_CODE: IF POL-COD-TPA-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-COD-TPA
		//           ELSE
		//              MOVE 0 TO IND-POL-COD-TPA
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolCodTpaFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-COD-TPA
			ws.getIndPoli().setImpTrasfe(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-COD-TPA
			ws.getIndPoli().setImpTrasfe(((short) 0));
		}
		// COB_CODE: IF POL-ID-ACC-COMM-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-ID-ACC-COMM
		//           ELSE
		//              MOVE 0 TO IND-POL-ID-ACC-COMM
		//           END-IF
		if (Characters.EQ_HIGH.test(poli.getPolIdAccComm().getPolIdAccCommNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-POL-ID-ACC-COMM
			ws.getIndPoli().setImpTfrStrc(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-ID-ACC-COMM
			ws.getIndPoli().setImpTfrStrc(((short) 0));
		}
		// COB_CODE: IF POL-FL-POLI-CPI-PR-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-FL-POLI-CPI-PR
		//           ELSE
		//              MOVE 0 TO IND-POL-FL-POLI-CPI-PR
		//           END-IF
		if (Conditions.eq(poli.getPolFlPoliCpiPr(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-POL-FL-POLI-CPI-PR
			ws.getIndPoli().setNumGgRitardoPag(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-FL-POLI-CPI-PR
			ws.getIndPoli().setNumGgRitardoPag(((short) 0));
		}
		// COB_CODE: IF POL-FL-POLI-BUNDLING-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-FL-POLI-BUNDLING
		//           ELSE
		//              MOVE 0 TO IND-POL-FL-POLI-BUNDLING
		//           END-IF
		if (Conditions.eq(poli.getPolFlPoliBundling(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-POL-FL-POLI-BUNDLING
			ws.getIndPoli().setNumGgRival(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-FL-POLI-BUNDLING
			ws.getIndPoli().setNumGgRival(((short) 0));
		}
		// COB_CODE: IF POL-IND-POLI-PRIN-COLL-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-IND-POLI-PRIN-COLL
		//           ELSE
		//              MOVE 0 TO IND-POL-IND-POLI-PRIN-COLL
		//           END-IF
		if (Conditions.eq(poli.getPolIndPoliPrinColl(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-POL-IND-POLI-PRIN-COLL
			ws.getIndPoli().setAcqExp(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-IND-POLI-PRIN-COLL
			ws.getIndPoli().setAcqExp(((short) 0));
		}
		// COB_CODE: IF POL-FL-VND-BUNDLE-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-FL-VND-BUNDLE
		//           ELSE
		//              MOVE 0 TO IND-POL-FL-VND-BUNDLE
		//           END-IF.
		if (Conditions.eq(poli.getPolFlVndBundle(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-POL-FL-VND-BUNDLE
			ws.getIndPoli().setRemunAss(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-FL-VND-BUNDLE
			ws.getIndPoli().setRemunAss(((short) 0));
		}
		// COB_CODE: IF POL-IB-BS-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-IB-BS
		//           ELSE
		//              MOVE 0 TO IND-POL-IB-BS
		//           END-IF.
		if (Characters.EQ_HIGH.test(poli.getPolIbBs(), PoliIdbspol0.Len.POL_IB_BS)) {
			// COB_CODE: MOVE -1 TO IND-POL-IB-BS
			ws.getIndPoli().setCommisInter(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-IB-BS
			ws.getIndPoli().setCommisInter(((short) 0));
		}
		// COB_CODE: IF POL-FL-POLI-IFP-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-POL-FL-POLI-IFP
		//           ELSE
		//              MOVE 0 TO IND-POL-FL-POLI-IFP
		//           END-IF.
		if (Conditions.eq(poli.getPolFlPoliIfp(), Types.HIGH_CHAR_VAL)) {
			// COB_CODE: MOVE -1 TO IND-POL-FL-POLI-IFP
			ws.getIndPoli().setCnbtAntirac(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-POL-FL-POLI-IFP
			ws.getIndPoli().setCnbtAntirac(((short) 0));
		}
		// COB_CODE: IF STB-ID-MOVI-CHIU-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-STB-ID-MOVI-CHIU
		//           ELSE
		//              MOVE 0 TO IND-STB-ID-MOVI-CHIU
		//           END-IF.
		if (Characters.EQ_HIGH.test(statOggBus.getStbIdMoviChiu().getStbIdMoviChiuNullFormatted())) {
			// COB_CODE: MOVE -1 TO IND-STB-ID-MOVI-CHIU
			ws.setIndStbIdMoviChiu(((short) -1));
		} else {
			// COB_CODE: MOVE 0 TO IND-STB-ID-MOVI-CHIU
			ws.setIndStbIdMoviChiu(((short) 0));
		}
	}

	/**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
	private void z950ConvertiXToN() {
		// COB_CODE: MOVE ADE-DT-INI-EFF-DB TO WS-DATE-X
		ws.getIdsv0010().setWsDateX(ws.getAdesDb().getIniEffDb());
		// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
		z800DtXToN();
		// COB_CODE: MOVE WS-DATE-N      TO ADE-DT-INI-EFF
		ades.setAdeDtIniEff(ws.getIdsv0010().getWsDateN());
		// COB_CODE: MOVE ADE-DT-END-EFF-DB TO WS-DATE-X
		ws.getIdsv0010().setWsDateX(ws.getAdesDb().getEndEffDb());
		// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
		z800DtXToN();
		// COB_CODE: MOVE WS-DATE-N      TO ADE-DT-END-EFF
		ades.setAdeDtEndEff(ws.getIdsv0010().getWsDateN());
		// COB_CODE: IF IND-ADE-DT-DECOR = 0
		//               MOVE WS-DATE-N      TO ADE-DT-DECOR
		//           END-IF
		if (ws.getIndAdes().getDtDecor() == 0) {
			// COB_CODE: MOVE ADE-DT-DECOR-DB TO WS-DATE-X
			ws.getIdsv0010().setWsDateX(ws.getAdesDb().getDecorDb());
			// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
			z800DtXToN();
			// COB_CODE: MOVE WS-DATE-N      TO ADE-DT-DECOR
			ades.getAdeDtDecor().setAdeDtDecor(ws.getIdsv0010().getWsDateN());
		}
		// COB_CODE: IF IND-ADE-DT-SCAD = 0
		//               MOVE WS-DATE-N      TO ADE-DT-SCAD
		//           END-IF
		if (ws.getIndAdes().getDtScad() == 0) {
			// COB_CODE: MOVE ADE-DT-SCAD-DB TO WS-DATE-X
			ws.getIdsv0010().setWsDateX(ws.getAdesDb().getScadDb());
			// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
			z800DtXToN();
			// COB_CODE: MOVE WS-DATE-N      TO ADE-DT-SCAD
			ades.getAdeDtScad().setAdeDtScad(ws.getIdsv0010().getWsDateN());
		}
		// COB_CODE: IF IND-ADE-DT-VARZ-TP-IAS = 0
		//               MOVE WS-DATE-N      TO ADE-DT-VARZ-TP-IAS
		//           END-IF
		if (ws.getIndAdes().getDtVarzTpIas() == 0) {
			// COB_CODE: MOVE ADE-DT-VARZ-TP-IAS-DB TO WS-DATE-X
			ws.getIdsv0010().setWsDateX(ws.getAdesDb().getVarzTpIasDb());
			// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
			z800DtXToN();
			// COB_CODE: MOVE WS-DATE-N      TO ADE-DT-VARZ-TP-IAS
			ades.getAdeDtVarzTpIas().setAdeDtVarzTpIas(ws.getIdsv0010().getWsDateN());
		}
		// COB_CODE: IF IND-ADE-DT-NOVA-RGM-FISC = 0
		//               MOVE WS-DATE-N      TO ADE-DT-NOVA-RGM-FISC
		//           END-IF
		if (ws.getIndAdes().getDtNovaRgmFisc() == 0) {
			// COB_CODE: MOVE ADE-DT-NOVA-RGM-FISC-DB TO WS-DATE-X
			ws.getIdsv0010().setWsDateX(ws.getAdesDb().getNovaRgmFiscDb());
			// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
			z800DtXToN();
			// COB_CODE: MOVE WS-DATE-N      TO ADE-DT-NOVA-RGM-FISC
			ades.getAdeDtNovaRgmFisc().setAdeDtNovaRgmFisc(ws.getIdsv0010().getWsDateN());
		}
		// COB_CODE: IF IND-ADE-DT-DECOR-PREST-BAN = 0
		//               MOVE WS-DATE-N      TO ADE-DT-DECOR-PREST-BAN
		//           END-IF
		if (ws.getIndAdes().getDtDecorPrestBan() == 0) {
			// COB_CODE: MOVE ADE-DT-DECOR-PREST-BAN-DB TO WS-DATE-X
			ws.getIdsv0010().setWsDateX(ws.getAdesDb().getDecorPrestBanDb());
			// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
			z800DtXToN();
			// COB_CODE: MOVE WS-DATE-N      TO ADE-DT-DECOR-PREST-BAN
			ades.getAdeDtDecorPrestBan().setAdeDtDecorPrestBan(ws.getIdsv0010().getWsDateN());
		}
		// COB_CODE: IF IND-ADE-DT-EFF-VARZ-STAT-T = 0
		//               MOVE WS-DATE-N      TO ADE-DT-EFF-VARZ-STAT-T
		//           END-IF
		if (ws.getIndAdes().getDtEffVarzStatT() == 0) {
			// COB_CODE: MOVE ADE-DT-EFF-VARZ-STAT-T-DB TO WS-DATE-X
			ws.getIdsv0010().setWsDateX(ws.getAdesDb().getEffVarzStatTDb());
			// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
			z800DtXToN();
			// COB_CODE: MOVE WS-DATE-N      TO ADE-DT-EFF-VARZ-STAT-T
			ades.getAdeDtEffVarzStatT().setAdeDtEffVarzStatT(ws.getIdsv0010().getWsDateN());
		}
		// COB_CODE: IF IND-ADE-DT-ULT-CONS-CNBT = 0
		//               MOVE WS-DATE-N      TO ADE-DT-ULT-CONS-CNBT
		//           END-IF
		if (ws.getIndAdes().getDtUltConsCnbt() == 0) {
			// COB_CODE: MOVE ADE-DT-ULT-CONS-CNBT-DB TO WS-DATE-X
			ws.getIdsv0010().setWsDateX(ws.getAdesDb().getUltConsCnbtDb());
			// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
			z800DtXToN();
			// COB_CODE: MOVE WS-DATE-N      TO ADE-DT-ULT-CONS-CNBT
			ades.getAdeDtUltConsCnbt().setAdeDtUltConsCnbt(ws.getIdsv0010().getWsDateN());
		}
		// COB_CODE: IF IND-ADE-DT-PRESC = 0
		//               MOVE WS-DATE-N      TO ADE-DT-PRESC
		//           END-IF.
		if (ws.getIndAdes().getDtPresc() == 0) {
			// COB_CODE: MOVE ADE-DT-PRESC-DB TO WS-DATE-X
			ws.getIdsv0010().setWsDateX(ws.getAdesDb().getPrescDb());
			// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
			z800DtXToN();
			// COB_CODE: MOVE WS-DATE-N      TO ADE-DT-PRESC
			ades.getAdeDtPresc().setAdeDtPresc(ws.getIdsv0010().getWsDateN());
		}
		// COB_CODE: IF IND-POL-DT-PROP = 0
		//               MOVE WS-DATE-N      TO POL-DT-PROP
		//           END-IF
		if (ws.getIndPoli().getDtEndCop() == 0) {
			// COB_CODE: MOVE POL-DT-PROP-DB TO WS-DATE-X
			ws.getIdsv0010().setWsDateX(ws.getPoliDb().getIniEffDb());
			// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
			z800DtXToN();
			// COB_CODE: MOVE WS-DATE-N      TO POL-DT-PROP
			poli.getPolDtProp().setPolDtProp(ws.getIdsv0010().getWsDateN());
		}
		// COB_CODE: MOVE POL-DT-INI-EFF-DB TO WS-DATE-X
		ws.getIdsv0010().setWsDateX(ws.getPoliDb().getEndEffDb());
		// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
		z800DtXToN();
		// COB_CODE: MOVE WS-DATE-N      TO POL-DT-INI-EFF
		poli.setPolDtIniEff(ws.getIdsv0010().getWsDateN());
		// COB_CODE: MOVE POL-DT-END-EFF-DB TO WS-DATE-X
		ws.getIdsv0010().setWsDateX(ws.getPoliDb().getDecorDb());
		// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
		z800DtXToN();
		// COB_CODE: MOVE WS-DATE-N      TO POL-DT-END-EFF
		poli.setPolDtEndEff(ws.getIdsv0010().getWsDateN());
		// COB_CODE: MOVE POL-DT-DECOR-DB TO WS-DATE-X
		ws.getIdsv0010().setWsDateX(ws.getPoliDb().getScadDb());
		// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
		z800DtXToN();
		// COB_CODE: MOVE WS-DATE-N      TO POL-DT-DECOR
		poli.setPolDtDecor(ws.getIdsv0010().getWsDateN());
		// COB_CODE: MOVE POL-DT-EMIS-DB TO WS-DATE-X
		ws.getIdsv0010().setWsDateX(ws.getPoliDb().getVarzTpIasDb());
		// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
		z800DtXToN();
		// COB_CODE: MOVE WS-DATE-N      TO POL-DT-EMIS
		poli.setPolDtEmis(ws.getIdsv0010().getWsDateN());
		// COB_CODE: IF IND-POL-DT-SCAD = 0
		//               MOVE WS-DATE-N      TO POL-DT-SCAD
		//           END-IF
		if (ws.getIndPoli().getIntrMora() == 0) {
			// COB_CODE: MOVE POL-DT-SCAD-DB TO WS-DATE-X
			ws.getIdsv0010().setWsDateX(ws.getPoliDb().getNovaRgmFiscDb());
			// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
			z800DtXToN();
			// COB_CODE: MOVE WS-DATE-N      TO POL-DT-SCAD
			poli.getPolDtScad().setPolDtScad(ws.getIdsv0010().getWsDateN());
		}
		// COB_CODE: MOVE POL-DT-INI-VLDT-PROD-DB TO WS-DATE-X
		ws.getIdsv0010().setWsDateX(ws.getPoliDb().getDecorPrestBanDb());
		// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
		z800DtXToN();
		// COB_CODE: MOVE WS-DATE-N      TO POL-DT-INI-VLDT-PROD
		poli.setPolDtIniVldtProd(ws.getIdsv0010().getWsDateN());
		// COB_CODE: IF IND-POL-DT-INI-VLDT-CONV = 0
		//               MOVE WS-DATE-N      TO POL-DT-INI-VLDT-CONV
		//           END-IF
		if (ws.getIndPoli().getDir() == 0) {
			// COB_CODE: MOVE POL-DT-INI-VLDT-CONV-DB TO WS-DATE-X
			ws.getIdsv0010().setWsDateX(ws.getPoliDb().getEffVarzStatTDb());
			// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
			z800DtXToN();
			// COB_CODE: MOVE WS-DATE-N      TO POL-DT-INI-VLDT-CONV
			poli.getPolDtIniVldtConv().setPolDtIniVldtConv(ws.getIdsv0010().getWsDateN());
		}
		// COB_CODE: IF IND-POL-DT-APPLZ-CONV = 0
		//               MOVE WS-DATE-N      TO POL-DT-APPLZ-CONV
		//           END-IF
		if (ws.getIndPoli().getSpeMed() == 0) {
			// COB_CODE: MOVE POL-DT-APPLZ-CONV-DB TO WS-DATE-X
			ws.getIdsv0010().setWsDateX(ws.getPoliDb().getUltConsCnbtDb());
			// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
			z800DtXToN();
			// COB_CODE: MOVE WS-DATE-N      TO POL-DT-APPLZ-CONV
			poli.getPolDtApplzConv().setPolDtApplzConv(ws.getIdsv0010().getWsDateN());
		}
		// COB_CODE: IF IND-POL-DT-PRESC = 0
		//               MOVE WS-DATE-N      TO POL-DT-PRESC
		//           END-IF.
		if (ws.getIndPoli().getDtEsiTit() == 0) {
			// COB_CODE: MOVE POL-DT-PRESC-DB TO WS-DATE-X
			ws.getIdsv0010().setWsDateX(ws.getPoliDb().getPrescDb());
			// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
			z800DtXToN();
			// COB_CODE: MOVE WS-DATE-N      TO POL-DT-PRESC
			poli.getPolDtPresc().setPolDtPresc(ws.getIdsv0010().getWsDateN());
		}
		// COB_CODE: MOVE STB-DT-INI-EFF-DB TO WS-DATE-X
		ws.getIdsv0010().setWsDateX(ws.getIdbvstb3().getStbDtIniEffDb());
		// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
		z800DtXToN();
		// COB_CODE: MOVE WS-DATE-N      TO STB-DT-INI-EFF
		statOggBus.setStbDtIniEff(ws.getIdsv0010().getWsDateN());
		// COB_CODE: MOVE STB-DT-END-EFF-DB TO WS-DATE-X
		ws.getIdsv0010().setWsDateX(ws.getIdbvstb3().getStbDtEndEffDb());
		// COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
		z800DtXToN();
		// COB_CODE: MOVE WS-DATE-N      TO STB-DT-END-EFF.
		statOggBus.setStbDtEndEff(ws.getIdsv0010().getWsDateN());
	}

	/**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
	private void z960LengthVchar() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
	private void a001TrattaDateTimestamp() {
		// COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
		a020ConvertiDtEffetto();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-RC
		//              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
		//           END-IF.
		if (idsv0003.getReturnCode().isSuccessfulRc()) {
			// COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
			a050ValorizzaCptz();
		}
	}

	/**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
	private void a020ConvertiDtEffetto() {
		// COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
		//                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
		//           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
		//           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
		//           *                                TO IDSV0003-DESCRIZ-ERR-DB2
		//                   CONTINUE
		//                ELSE
		//                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
		//                END-IF
		if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
			//       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
			//       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
			//                                TO IDSV0003-DESCRIZ-ERR-DB2
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
			ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
			// COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
			//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
			// COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
			ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
		}
		// COB_CODE: IF IDSV0003-SUCCESSFUL-RC
		//              END-IF
		//           END-IF.
		if (idsv0003.getReturnCode().isSuccessfulRc()) {
			// COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
			//              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
			//              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
			//           END-IF
			if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
				// COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
				ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
				// COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
				//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
				// COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
				ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
			}
		}
	}

	/**Original name: A050-VALORIZZA-CPTZ<br>*/
	private void a050ValorizzaCptz() {
		// COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
		//                   IDSV0003-DATA-COMPETENZA  = 0
		//           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
		//           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
		//           *                                TO IDSV0003-DESCRIZ-ERR-DB2
		//                   CONTINUE
		//                ELSE
		//                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
		//                END-IF.
		if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
			//       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
			//       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
			//                                TO IDSV0003-DESCRIZ-ERR-DB2
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
			ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
		}
		// COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
		//                   IDSV0003-DATA-COMP-AGG-STOR  = 0
		//           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
		//           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
		//           *                                TO IDSV0003-DESCRIZ-ERR-DB2
		//                   CONTINUE
		//                ELSE
		//                                       TO WS-TS-COMPETENZA-AGG-STOR
		//                END-IF.
		if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
			//       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
			//       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
			//                                TO IDSV0003-DESCRIZ-ERR-DB2
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
			//                               TO WS-TS-COMPETENZA-AGG-STOR
			ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
		}
	}

	/**Original name: Z800-DT-X-TO-N<br>*/
	private void z800DtXToN() {
		// COB_CODE: IF IDSV0003-DB-ISO
		//              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
		//           ELSE
		//              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
		//           END-IF.
		if (idsv0003.getFormatoDataDb().isIso()) {
			// COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
			z810DtXToNIso();
		} else {
			// COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
			z820DtXToNEur();
		}
	}

	/**Original name: Z810-DT-X-TO-N-ISO<br>*/
	private void z810DtXToNIso() {
		// COB_CODE: MOVE WS-DATE-X(1:4)
		//                   TO WS-STR-DATE-N(1:4)
		ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(),
				ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
		// COB_CODE: MOVE WS-DATE-X(6:2)
		//                   TO WS-STR-DATE-N(5:2)
		ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(),
				ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
		// COB_CODE: MOVE WS-DATE-X(9:2)
		//                   TO WS-STR-DATE-N(7:2).
		ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(),
				ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
	}

	/**Original name: Z820-DT-X-TO-N-EUR<br>*/
	private void z820DtXToNEur() {
		// COB_CODE: MOVE WS-DATE-X(1:2)
		//                   TO WS-STR-DATE-N(7:2)
		ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(),
				ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
		// COB_CODE: MOVE WS-DATE-X(4:2)
		//                   TO WS-STR-DATE-N(5:2)
		ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(),
				ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
		// COB_CODE: MOVE WS-DATE-X(7:4)
		//                   TO WS-STR-DATE-N(1:4).
		ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(),
				ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
	}

	@Override
	public long getAdeDsRiga() {
		return ades.getAdeDsRiga();
	}

	@Override
	public void setAdeDsRiga(long adeDsRiga) {
		this.ades.setAdeDsRiga(adeDsRiga);
	}

	public Ades getAdes() {
		return ades;
	}

	public AdesPoliStatOggBusLdbm02501 getAdesPoliStatOggBusLdbm02501() {
		return adesPoliStatOggBusLdbm02501;
	}

	public AdesPoliStatOggBusLdbm02502 getAdesPoliStatOggBusLdbm02502() {
		return adesPoliStatOggBusLdbm02502;
	}

	public AdesPoliStatOggBusLdbm0250 getAdesPoliStatOggBusLdbm0250() {
		return adesPoliStatOggBusLdbm0250;
	}

	public AdesPoliStatOggBusTrchDiGarLdbm02501 getAdesPoliStatOggBusTrchDiGarLdbm02501() {
		return adesPoliStatOggBusTrchDiGarLdbm02501;
	}

	public AdesPoliStatOggBusTrchDiGarLdbm02502 getAdesPoliStatOggBusTrchDiGarLdbm02502() {
		return adesPoliStatOggBusTrchDiGarLdbm02502;
	}

	public AdesPoliStatOggBusTrchDiGarLdbm02503 getAdesPoliStatOggBusTrchDiGarLdbm02503() {
		return adesPoliStatOggBusTrchDiGarLdbm02503;
	}

	public AdesPoliStatOggBusTrchDiGarLdbm0250 getAdesPoliStatOggBusTrchDiGarLdbm0250() {
		return adesPoliStatOggBusTrchDiGarLdbm0250;
	}

	@Override
	public int getCodCompAnia() {
		throw new FieldNotMappedException("codCompAnia");
	}

	@Override
	public void setCodCompAnia(int codCompAnia) {
		throw new FieldNotMappedException("codCompAnia");
	}

	@Override
	public char getConcsPrest() {
		throw new FieldNotMappedException("concsPrest");
	}

	@Override
	public void setConcsPrest(char concsPrest) {
		throw new FieldNotMappedException("concsPrest");
	}

	@Override
	public Character getConcsPrestObj() {
		return (getConcsPrest());
	}

	@Override
	public void setConcsPrestObj(Character concsPrestObj) {
		setConcsPrest((concsPrestObj));
	}

	@Override
	public AfDecimal getCumCnbtCap() {
		throw new FieldNotMappedException("cumCnbtCap");
	}

	@Override
	public void setCumCnbtCap(AfDecimal cumCnbtCap) {
		throw new FieldNotMappedException("cumCnbtCap");
	}

	@Override
	public AfDecimal getCumCnbtCapObj() {
		return getCumCnbtCap();
	}

	@Override
	public void setCumCnbtCapObj(AfDecimal cumCnbtCapObj) {
		setCumCnbtCap(new AfDecimal(cumCnbtCapObj, 15, 3));
	}

	@Override
	public char getDsOperSql() {
		return ades.getAdeDsOperSql();
	}

	@Override
	public void setDsOperSql(char dsOperSql) {
		this.ades.setAdeDsOperSql(dsOperSql);
	}

	@Override
	public char getDsStatoElab() {
		throw new FieldNotMappedException("dsStatoElab");
	}

	@Override
	public void setDsStatoElab(char dsStatoElab) {
		throw new FieldNotMappedException("dsStatoElab");
	}

	@Override
	public long getDsTsEndCptz() {
		throw new FieldNotMappedException("dsTsEndCptz");
	}

	@Override
	public void setDsTsEndCptz(long dsTsEndCptz) {
		throw new FieldNotMappedException("dsTsEndCptz");
	}

	@Override
	public long getDsTsIniCptz() {
		throw new FieldNotMappedException("dsTsIniCptz");
	}

	@Override
	public void setDsTsIniCptz(long dsTsIniCptz) {
		throw new FieldNotMappedException("dsTsIniCptz");
	}

	@Override
	public String getDsUtente() {
		return ades.getAdeDsUtente();
	}

	@Override
	public void setDsUtente(String dsUtente) {
		this.ades.setAdeDsUtente(dsUtente);
	}

	@Override
	public int getDsVer() {
		throw new FieldNotMappedException("dsVer");
	}

	@Override
	public void setDsVer(int dsVer) {
		throw new FieldNotMappedException("dsVer");
	}

	@Override
	public String getDtDecorDb() {
		throw new FieldNotMappedException("dtDecorDb");
	}

	@Override
	public void setDtDecorDb(String dtDecorDb) {
		throw new FieldNotMappedException("dtDecorDb");
	}

	@Override
	public String getDtDecorDbObj() {
		return getDtDecorDb();
	}

	@Override
	public void setDtDecorDbObj(String dtDecorDbObj) {
		setDtDecorDb(dtDecorDbObj);
	}

	@Override
	public String getDtDecorPrestBanDb() {
		throw new FieldNotMappedException("dtDecorPrestBanDb");
	}

	@Override
	public void setDtDecorPrestBanDb(String dtDecorPrestBanDb) {
		throw new FieldNotMappedException("dtDecorPrestBanDb");
	}

	@Override
	public String getDtDecorPrestBanDbObj() {
		return getDtDecorPrestBanDb();
	}

	@Override
	public void setDtDecorPrestBanDbObj(String dtDecorPrestBanDbObj) {
		setDtDecorPrestBanDb(dtDecorPrestBanDbObj);
	}

	@Override
	public String getDtEffVarzStatTDb() {
		throw new FieldNotMappedException("dtEffVarzStatTDb");
	}

	@Override
	public void setDtEffVarzStatTDb(String dtEffVarzStatTDb) {
		throw new FieldNotMappedException("dtEffVarzStatTDb");
	}

	@Override
	public String getDtEffVarzStatTDbObj() {
		return getDtEffVarzStatTDb();
	}

	@Override
	public void setDtEffVarzStatTDbObj(String dtEffVarzStatTDbObj) {
		setDtEffVarzStatTDb(dtEffVarzStatTDbObj);
	}

	@Override
	public String getDtEndEffDb() {
		throw new FieldNotMappedException("dtEndEffDb");
	}

	@Override
	public void setDtEndEffDb(String dtEndEffDb) {
		throw new FieldNotMappedException("dtEndEffDb");
	}

	@Override
	public String getDtIniEffDb() {
		throw new FieldNotMappedException("dtIniEffDb");
	}

	@Override
	public void setDtIniEffDb(String dtIniEffDb) {
		throw new FieldNotMappedException("dtIniEffDb");
	}

	@Override
	public String getDtNovaRgmFiscDb() {
		throw new FieldNotMappedException("dtNovaRgmFiscDb");
	}

	@Override
	public void setDtNovaRgmFiscDb(String dtNovaRgmFiscDb) {
		throw new FieldNotMappedException("dtNovaRgmFiscDb");
	}

	@Override
	public String getDtNovaRgmFiscDbObj() {
		return getDtNovaRgmFiscDb();
	}

	@Override
	public void setDtNovaRgmFiscDbObj(String dtNovaRgmFiscDbObj) {
		setDtNovaRgmFiscDb(dtNovaRgmFiscDbObj);
	}

	@Override
	public String getDtPrescDb() {
		throw new FieldNotMappedException("dtPrescDb");
	}

	@Override
	public void setDtPrescDb(String dtPrescDb) {
		throw new FieldNotMappedException("dtPrescDb");
	}

	@Override
	public String getDtPrescDbObj() {
		return getDtPrescDb();
	}

	@Override
	public void setDtPrescDbObj(String dtPrescDbObj) {
		setDtPrescDb(dtPrescDbObj);
	}

	@Override
	public String getDtScadDb() {
		throw new FieldNotMappedException("dtScadDb");
	}

	@Override
	public void setDtScadDb(String dtScadDb) {
		throw new FieldNotMappedException("dtScadDb");
	}

	@Override
	public String getDtScadDbObj() {
		return getDtScadDb();
	}

	@Override
	public void setDtScadDbObj(String dtScadDbObj) {
		setDtScadDb(dtScadDbObj);
	}

	@Override
	public String getDtUltConsCnbtDb() {
		throw new FieldNotMappedException("dtUltConsCnbtDb");
	}

	@Override
	public void setDtUltConsCnbtDb(String dtUltConsCnbtDb) {
		throw new FieldNotMappedException("dtUltConsCnbtDb");
	}

	@Override
	public String getDtUltConsCnbtDbObj() {
		return getDtUltConsCnbtDb();
	}

	@Override
	public void setDtUltConsCnbtDbObj(String dtUltConsCnbtDbObj) {
		setDtUltConsCnbtDb(dtUltConsCnbtDbObj);
	}

	@Override
	public String getDtVarzTpIasDb() {
		throw new FieldNotMappedException("dtVarzTpIasDb");
	}

	@Override
	public void setDtVarzTpIasDb(String dtVarzTpIasDb) {
		throw new FieldNotMappedException("dtVarzTpIasDb");
	}

	@Override
	public String getDtVarzTpIasDbObj() {
		return getDtVarzTpIasDb();
	}

	@Override
	public void setDtVarzTpIasDbObj(String dtVarzTpIasDbObj) {
		setDtVarzTpIasDb(dtVarzTpIasDbObj);
	}

	@Override
	public int getDurAa() {
		throw new FieldNotMappedException("durAa");
	}

	@Override
	public void setDurAa(int durAa) {
		throw new FieldNotMappedException("durAa");
	}

	@Override
	public Integer getDurAaObj() {
		return (getDurAa());
	}

	@Override
	public void setDurAaObj(Integer durAaObj) {
		setDurAa((durAaObj));
	}

	@Override
	public int getDurGg() {
		throw new FieldNotMappedException("durGg");
	}

	@Override
	public void setDurGg(int durGg) {
		throw new FieldNotMappedException("durGg");
	}

	@Override
	public Integer getDurGgObj() {
		return (getDurGg());
	}

	@Override
	public void setDurGgObj(Integer durGgObj) {
		setDurGg((durGgObj));
	}

	@Override
	public int getDurMm() {
		throw new FieldNotMappedException("durMm");
	}

	@Override
	public void setDurMm(int durMm) {
		throw new FieldNotMappedException("durMm");
	}

	@Override
	public Integer getDurMmObj() {
		return (getDurMm());
	}

	@Override
	public void setDurMmObj(Integer durMmObj) {
		setDurMm((durMmObj));
	}

	@Override
	public int getEtaAScad() {
		throw new FieldNotMappedException("etaAScad");
	}

	@Override
	public void setEtaAScad(int etaAScad) {
		throw new FieldNotMappedException("etaAScad");
	}

	@Override
	public Integer getEtaAScadObj() {
		return (getEtaAScad());
	}

	@Override
	public void setEtaAScadObj(Integer etaAScadObj) {
		setEtaAScad((etaAScadObj));
	}

	@Override
	public char getFlAttiv() {
		throw new FieldNotMappedException("flAttiv");
	}

	@Override
	public void setFlAttiv(char flAttiv) {
		throw new FieldNotMappedException("flAttiv");
	}

	@Override
	public Character getFlAttivObj() {
		return (getFlAttiv());
	}

	@Override
	public void setFlAttivObj(Character flAttivObj) {
		setFlAttiv((flAttivObj));
	}

	@Override
	public char getFlCoincAssto() {
		throw new FieldNotMappedException("flCoincAssto");
	}

	@Override
	public void setFlCoincAssto(char flCoincAssto) {
		throw new FieldNotMappedException("flCoincAssto");
	}

	@Override
	public Character getFlCoincAsstoObj() {
		return (getFlCoincAssto());
	}

	@Override
	public void setFlCoincAsstoObj(Character flCoincAsstoObj) {
		setFlCoincAssto((flCoincAsstoObj));
	}

	@Override
	public char getFlProvzaMigraz() {
		throw new FieldNotMappedException("flProvzaMigraz");
	}

	@Override
	public void setFlProvzaMigraz(char flProvzaMigraz) {
		throw new FieldNotMappedException("flProvzaMigraz");
	}

	@Override
	public Character getFlProvzaMigrazObj() {
		return (getFlProvzaMigraz());
	}

	@Override
	public void setFlProvzaMigrazObj(Character flProvzaMigrazObj) {
		setFlProvzaMigraz((flProvzaMigrazObj));
	}

	@Override
	public char getFlVarzStatTbgc() {
		throw new FieldNotMappedException("flVarzStatTbgc");
	}

	@Override
	public void setFlVarzStatTbgc(char flVarzStatTbgc) {
		throw new FieldNotMappedException("flVarzStatTbgc");
	}

	@Override
	public Character getFlVarzStatTbgcObj() {
		return (getFlVarzStatTbgc());
	}

	@Override
	public void setFlVarzStatTbgcObj(Character flVarzStatTbgcObj) {
		setFlVarzStatTbgc((flVarzStatTbgcObj));
	}

	public Iabv0002 getIabv0002() {
		return iabv0002;
	}

	@Override
	public char getIabv0002StateCurrent() {
		return iabv0002.getIabv0002StateCurrent();
	}

	@Override
	public void setIabv0002StateCurrent(char iabv0002StateCurrent) {
		this.iabv0002.setIabv0002StateCurrent(iabv0002StateCurrent);
	}

	@Override
	public int getIabv0009Versioning() {
		return iabv0002.getIabv0009GestGuideService().getVersioning();
	}

	@Override
	public void setIabv0009Versioning(int iabv0009Versioning) {
		this.iabv0002.getIabv0009GestGuideService().setVersioning(iabv0009Versioning);
	}

	@Override
	public String getIbDflt() {
		throw new FieldNotMappedException("ibDflt");
	}

	@Override
	public void setIbDflt(String ibDflt) {
		throw new FieldNotMappedException("ibDflt");
	}

	@Override
	public String getIbDfltObj() {
		return getIbDflt();
	}

	@Override
	public void setIbDfltObj(String ibDfltObj) {
		setIbDflt(ibDfltObj);
	}

	@Override
	public String getIbOgg() {
		throw new FieldNotMappedException("ibOgg");
	}

	@Override
	public void setIbOgg(String ibOgg) {
		throw new FieldNotMappedException("ibOgg");
	}

	@Override
	public String getIbOggObj() {
		return getIbOgg();
	}

	@Override
	public void setIbOggObj(String ibOggObj) {
		setIbOgg(ibOggObj);
	}

	@Override
	public String getIbPrev() {
		throw new FieldNotMappedException("ibPrev");
	}

	@Override
	public void setIbPrev(String ibPrev) {
		throw new FieldNotMappedException("ibPrev");
	}

	@Override
	public String getIbPrevObj() {
		return getIbPrev();
	}

	@Override
	public void setIbPrevObj(String ibPrevObj) {
		setIbPrev(ibPrevObj);
	}

	@Override
	public int getIdAdes() {
		throw new FieldNotMappedException("idAdes");
	}

	@Override
	public void setIdAdes(int idAdes) {
		throw new FieldNotMappedException("idAdes");
	}

	@Override
	public int getIdMoviChiu() {
		throw new FieldNotMappedException("idMoviChiu");
	}

	@Override
	public void setIdMoviChiu(int idMoviChiu) {
		throw new FieldNotMappedException("idMoviChiu");
	}

	@Override
	public Integer getIdMoviChiuObj() {
		return (getIdMoviChiu());
	}

	@Override
	public void setIdMoviChiuObj(Integer idMoviChiuObj) {
		setIdMoviChiu((idMoviChiuObj));
	}

	@Override
	public int getIdMoviCrz() {
		throw new FieldNotMappedException("idMoviCrz");
	}

	@Override
	public void setIdMoviCrz(int idMoviCrz) {
		throw new FieldNotMappedException("idMoviCrz");
	}

	@Override
	public int getIdPoli() {
		throw new FieldNotMappedException("idPoli");
	}

	@Override
	public void setIdPoli(int idPoli) {
		throw new FieldNotMappedException("idPoli");
	}

	@Override
	public String getIdenIscFnd() {
		throw new FieldNotMappedException("idenIscFnd");
	}

	@Override
	public void setIdenIscFnd(String idenIscFnd) {
		throw new FieldNotMappedException("idenIscFnd");
	}

	@Override
	public String getIdenIscFndObj() {
		return getIdenIscFnd();
	}

	@Override
	public void setIdenIscFndObj(String idenIscFndObj) {
		setIdenIscFnd(idenIscFndObj);
	}

	public Idsv0003 getIdsv0003() {
		return idsv0003;
	}

	@Override
	public AfDecimal getImpAder() {
		throw new FieldNotMappedException("impAder");
	}

	@Override
	public void setImpAder(AfDecimal impAder) {
		throw new FieldNotMappedException("impAder");
	}

	@Override
	public AfDecimal getImpAderObj() {
		return getImpAder();
	}

	@Override
	public void setImpAderObj(AfDecimal impAderObj) {
		setImpAder(new AfDecimal(impAderObj, 15, 3));
	}

	@Override
	public AfDecimal getImpAz() {
		throw new FieldNotMappedException("impAz");
	}

	@Override
	public void setImpAz(AfDecimal impAz) {
		throw new FieldNotMappedException("impAz");
	}

	@Override
	public AfDecimal getImpAzObj() {
		return getImpAz();
	}

	@Override
	public void setImpAzObj(AfDecimal impAzObj) {
		setImpAz(new AfDecimal(impAzObj, 15, 3));
	}

	@Override
	public AfDecimal getImpGarCnbt() {
		throw new FieldNotMappedException("impGarCnbt");
	}

	@Override
	public void setImpGarCnbt(AfDecimal impGarCnbt) {
		throw new FieldNotMappedException("impGarCnbt");
	}

	@Override
	public AfDecimal getImpGarCnbtObj() {
		return getImpGarCnbt();
	}

	@Override
	public void setImpGarCnbtObj(AfDecimal impGarCnbtObj) {
		setImpGarCnbt(new AfDecimal(impGarCnbtObj, 15, 3));
	}

	@Override
	public AfDecimal getImpRecRitAcc() {
		throw new FieldNotMappedException("impRecRitAcc");
	}

	@Override
	public void setImpRecRitAcc(AfDecimal impRecRitAcc) {
		throw new FieldNotMappedException("impRecRitAcc");
	}

	@Override
	public AfDecimal getImpRecRitAccObj() {
		return getImpRecRitAcc();
	}

	@Override
	public void setImpRecRitAccObj(AfDecimal impRecRitAccObj) {
		setImpRecRitAcc(new AfDecimal(impRecRitAccObj, 15, 3));
	}

	@Override
	public AfDecimal getImpRecRitVis() {
		throw new FieldNotMappedException("impRecRitVis");
	}

	@Override
	public void setImpRecRitVis(AfDecimal impRecRitVis) {
		throw new FieldNotMappedException("impRecRitVis");
	}

	@Override
	public AfDecimal getImpRecRitVisObj() {
		return getImpRecRitVis();
	}

	@Override
	public void setImpRecRitVisObj(AfDecimal impRecRitVisObj) {
		setImpRecRitVis(new AfDecimal(impRecRitVisObj, 15, 3));
	}

	@Override
	public AfDecimal getImpTfr() {
		throw new FieldNotMappedException("impTfr");
	}

	@Override
	public void setImpTfr(AfDecimal impTfr) {
		throw new FieldNotMappedException("impTfr");
	}

	@Override
	public AfDecimal getImpTfrObj() {
		return getImpTfr();
	}

	@Override
	public void setImpTfrObj(AfDecimal impTfrObj) {
		setImpTfr(new AfDecimal(impTfrObj, 15, 3));
	}

	@Override
	public AfDecimal getImpVolo() {
		throw new FieldNotMappedException("impVolo");
	}

	@Override
	public void setImpVolo(AfDecimal impVolo) {
		throw new FieldNotMappedException("impVolo");
	}

	@Override
	public AfDecimal getImpVoloObj() {
		return getImpVolo();
	}

	@Override
	public void setImpVoloObj(AfDecimal impVoloObj) {
		setImpVolo(new AfDecimal(impVoloObj, 15, 3));
	}

	@Override
	public AfDecimal getImpbVisDaRec() {
		throw new FieldNotMappedException("impbVisDaRec");
	}

	@Override
	public void setImpbVisDaRec(AfDecimal impbVisDaRec) {
		throw new FieldNotMappedException("impbVisDaRec");
	}

	@Override
	public AfDecimal getImpbVisDaRecObj() {
		return getImpbVisDaRec();
	}

	@Override
	public void setImpbVisDaRecObj(AfDecimal impbVisDaRecObj) {
		setImpbVisDaRec(new AfDecimal(impbVisDaRecObj, 15, 3));
	}

	@Override
	public String getModCalc() {
		throw new FieldNotMappedException("modCalc");
	}

	@Override
	public void setModCalc(String modCalc) {
		throw new FieldNotMappedException("modCalc");
	}

	@Override
	public String getModCalcObj() {
		return getModCalc();
	}

	@Override
	public void setModCalcObj(String modCalcObj) {
		setModCalc(modCalcObj);
	}

	@Override
	public AfDecimal getNumRatPian() {
		throw new FieldNotMappedException("numRatPian");
	}

	@Override
	public void setNumRatPian(AfDecimal numRatPian) {
		throw new FieldNotMappedException("numRatPian");
	}

	@Override
	public AfDecimal getNumRatPianObj() {
		return getNumRatPian();
	}

	@Override
	public void setNumRatPianObj(AfDecimal numRatPianObj) {
		setNumRatPian(new AfDecimal(numRatPianObj, 12, 5));
	}

	@Override
	public AfDecimal getPcAder() {
		throw new FieldNotMappedException("pcAder");
	}

	@Override
	public void setPcAder(AfDecimal pcAder) {
		throw new FieldNotMappedException("pcAder");
	}

	@Override
	public AfDecimal getPcAderObj() {
		return getPcAder();
	}

	@Override
	public void setPcAderObj(AfDecimal pcAderObj) {
		setPcAder(new AfDecimal(pcAderObj, 6, 3));
	}

	@Override
	public AfDecimal getPcAz() {
		throw new FieldNotMappedException("pcAz");
	}

	@Override
	public void setPcAz(AfDecimal pcAz) {
		throw new FieldNotMappedException("pcAz");
	}

	@Override
	public AfDecimal getPcAzObj() {
		return getPcAz();
	}

	@Override
	public void setPcAzObj(AfDecimal pcAzObj) {
		setPcAz(new AfDecimal(pcAzObj, 6, 3));
	}

	@Override
	public AfDecimal getPcTfr() {
		throw new FieldNotMappedException("pcTfr");
	}

	@Override
	public void setPcTfr(AfDecimal pcTfr) {
		throw new FieldNotMappedException("pcTfr");
	}

	@Override
	public AfDecimal getPcTfrObj() {
		return getPcTfr();
	}

	@Override
	public void setPcTfrObj(AfDecimal pcTfrObj) {
		setPcTfr(new AfDecimal(pcTfrObj, 6, 3));
	}

	@Override
	public AfDecimal getPcVolo() {
		throw new FieldNotMappedException("pcVolo");
	}

	@Override
	public void setPcVolo(AfDecimal pcVolo) {
		throw new FieldNotMappedException("pcVolo");
	}

	@Override
	public AfDecimal getPcVoloObj() {
		return getPcVolo();
	}

	@Override
	public void setPcVoloObj(AfDecimal pcVoloObj) {
		setPcVolo(new AfDecimal(pcVoloObj, 6, 3));
	}

	public PoliIdbspol0 getPoli() {
		return poli;
	}

	@Override
	public AfDecimal getPreLrdInd() {
		throw new FieldNotMappedException("preLrdInd");
	}

	@Override
	public void setPreLrdInd(AfDecimal preLrdInd) {
		throw new FieldNotMappedException("preLrdInd");
	}

	@Override
	public AfDecimal getPreLrdIndObj() {
		return getPreLrdInd();
	}

	@Override
	public void setPreLrdIndObj(AfDecimal preLrdIndObj) {
		setPreLrdInd(new AfDecimal(preLrdIndObj, 15, 3));
	}

	@Override
	public AfDecimal getPreNetInd() {
		throw new FieldNotMappedException("preNetInd");
	}

	@Override
	public void setPreNetInd(AfDecimal preNetInd) {
		throw new FieldNotMappedException("preNetInd");
	}

	@Override
	public AfDecimal getPreNetIndObj() {
		return getPreNetInd();
	}

	@Override
	public void setPreNetIndObj(AfDecimal preNetIndObj) {
		setPreNetInd(new AfDecimal(preNetIndObj, 15, 3));
	}

	@Override
	public AfDecimal getPrstzIniInd() {
		throw new FieldNotMappedException("prstzIniInd");
	}

	@Override
	public void setPrstzIniInd(AfDecimal prstzIniInd) {
		throw new FieldNotMappedException("prstzIniInd");
	}

	@Override
	public AfDecimal getPrstzIniIndObj() {
		return getPrstzIniInd();
	}

	@Override
	public void setPrstzIniIndObj(AfDecimal prstzIniIndObj) {
		setPrstzIniInd(new AfDecimal(prstzIniIndObj, 15, 3));
	}

	@Override
	public AfDecimal getRatLrdInd() {
		throw new FieldNotMappedException("ratLrdInd");
	}

	@Override
	public void setRatLrdInd(AfDecimal ratLrdInd) {
		throw new FieldNotMappedException("ratLrdInd");
	}

	@Override
	public AfDecimal getRatLrdIndObj() {
		return getRatLrdInd();
	}

	@Override
	public void setRatLrdIndObj(AfDecimal ratLrdIndObj) {
		setRatLrdInd(new AfDecimal(ratLrdIndObj, 15, 3));
	}

	public StatOggBusIdbsstb0 getStatOggBus() {
		return statOggBus;
	}

	@Override
	public String getTpFntCnbtva() {
		throw new FieldNotMappedException("tpFntCnbtva");
	}

	@Override
	public void setTpFntCnbtva(String tpFntCnbtva) {
		throw new FieldNotMappedException("tpFntCnbtva");
	}

	@Override
	public String getTpFntCnbtvaObj() {
		return getTpFntCnbtva();
	}

	@Override
	public void setTpFntCnbtvaObj(String tpFntCnbtvaObj) {
		setTpFntCnbtva(tpFntCnbtvaObj);
	}

	@Override
	public String getTpIas() {
		throw new FieldNotMappedException("tpIas");
	}

	@Override
	public void setTpIas(String tpIas) {
		throw new FieldNotMappedException("tpIas");
	}

	@Override
	public String getTpIasObj() {
		return getTpIas();
	}

	@Override
	public void setTpIasObj(String tpIasObj) {
		setTpIas(tpIasObj);
	}

	@Override
	public String getTpModPagTit() {
		throw new FieldNotMappedException("tpModPagTit");
	}

	@Override
	public void setTpModPagTit(String tpModPagTit) {
		throw new FieldNotMappedException("tpModPagTit");
	}

	@Override
	public String getTpRgmFisc() {
		throw new FieldNotMappedException("tpRgmFisc");
	}

	@Override
	public void setTpRgmFisc(String tpRgmFisc) {
		throw new FieldNotMappedException("tpRgmFisc");
	}

	@Override
	public String getTpRiat() {
		throw new FieldNotMappedException("tpRiat");
	}

	@Override
	public void setTpRiat(String tpRiat) {
		throw new FieldNotMappedException("tpRiat");
	}

	@Override
	public String getTpRiatObj() {
		return getTpRiat();
	}

	@Override
	public void setTpRiatObj(String tpRiatObj) {
		setTpRiat(tpRiatObj);
	}

	public Ldbm0250Data getWs() {
		return ws;
	}
}

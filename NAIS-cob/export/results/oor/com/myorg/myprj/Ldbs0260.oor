package com.myorg.myprj;

import java.lang.Override;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.bphx.ctu.af.util.oodate.CalendarUtil;

import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import com.myorg.myprj.commons.data.dao.MatrMovimentoDao;
import com.myorg.myprj.commons.data.to.IMatrMovimento;
import com.myorg.myprj.copy.Sqlca;
import com.myorg.myprj.ws.Idsv0003;
import com.myorg.myprj.ws.Ldbs0260Data;
import com.myorg.myprj.ws.MatrMovimento;
import com.myorg.myprj.ws.enums.Idsv0003LivelloOperazione;

/**Original name: LDBS0260<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  13 MAR 2007.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULI PER ACCESSO RISORSE DB               *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
class Ldbs0260 extends Program implements IMatrMovimento {

	//==== PROPERTIES ====
	//Original name: SQLCA
	private Sqlca sqlca := new Sqlca();
	private DbAccessStatus dbAccessStatus := new DbAccessStatus(sqlca);
	private MatrMovimentoDao matrMovimentoDao := new MatrMovimentoDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private Ldbs0260Data ws := new Ldbs0260Data();
	//Original name: IDSV0003
	private Idsv0003 idsv0003;
	//Original name: MATR-MOVIMENTO
	private MatrMovimento matrMovimento;


	//==== METHODS ====
	/**Original name: PROGRAM_LDBS0260_FIRST_SENTENCES<br>*/
	public long execute(Idsv0003 idsv0003, MatrMovimento matrMovimento) {
		this.idsv0003 := idsv0003;
		this.matrMovimento := matrMovimento;
		// COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
		a000Inizio();
		// COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
		//           OR IDSV0003-TRATT-X-COMPETENZA
		//               END-EVALUATE
		//           ELSE
		//               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
		//           END-IF.
		if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto() | this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) then
			// COB_CODE: EVALUATE TRUE
			//              WHEN IDSV0003-WHERE-CONDITION
			//                 PERFORM A200-ELABORA-WC       THRU A200-EX
			//              WHEN OTHER
			//                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
			//           END-EVALUATE
			switch this.idsv0003.getLivelloOperazione().getLivelloOperazione()
			case Idsv0003LivelloOperazione.WHERE_CONDITION:
				// COB_CODE: PERFORM A200-ELABORA-WC       THRU A200-EX
				a200ElaboraWc();
			default:
				// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
				this.idsv0003.getReturnCode().setInvalidLevelOper();
			endswitch
		else
			// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
			this.idsv0003.getReturnCode().setInvalidLevelOper();
		endif
		// COB_CODE: GOBACK.
		//last return statement was skipped
		return 0;
	}

	public static Ldbs0260 getInstance() {
		return (Ldbs0260)Programs.getInstance(Ldbs0260.clazz);
	}

	/**Original name: A000-INIZIO<br>*/
	private void a000Inizio() {
		// COB_CODE: MOVE 'LDBS0260'               TO   IDSV0003-COD-SERVIZIO-BE.
		idsv0003.getCampiEsito().setCodServizioBe("LDBS0260");
		// COB_CODE: MOVE 'MATR-MOVIMENTO'         TO   IDSV0003-NOME-TABELLA.
		idsv0003.getCampiEsito().setNomeTabella("MATR-MOVIMENTO");
		// COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
		idsv0003.getReturnCode().setReturnCode("00");
		// COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
		//                                              IDSV0003-NUM-RIGHE-LETTE.
		idsv0003.getSqlcode().setSqlcode(0);
		idsv0003.getCampiEsito().setNumRigheLette(0);
		// COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
		//                                              IDSV0003-KEY-TABELLA.
		idsv0003.getCampiEsito().setDescrizErrDb2("");
		idsv0003.getCampiEsito().setKeyTabella("");
		// COB_CODE: MOVE ZEROES                   TO   WS-TIMESTAMP-NUM.
		ws.getWsTimestamp().setWsTimestampNum(0);
		// COB_CODE: ACCEPT WS-TIMESTAMP(1:8)     FROM DATE YYYYMMDD.
		ws.getWsTimestamp().setWsTimestamp(Functions.setSubstring(ws.getWsTimestamp().getWsTimestamp(), CalendarUtil.getDateYYYYMMDD(), 1, 8));
		// COB_CODE: ACCEPT WS-TIMESTAMP(9:6)     FROM TIME.
		ws.getWsTimestamp().setWsTimestamp(Functions.setSubstring(ws.getWsTimestamp().getWsTimestamp(), CalendarUtil.getTimeHHMMSSMM(), 9, 6));
		// COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
		a001TrattaDateTimestamp();
	}

	/**Original name: A100-CHECK-RETURN-CODE<br>*/
	private void a100CheckReturnCode() {
		// COB_CODE: IF IDSV0003-SUCCESSFUL-RC
		//              END-EVALUATE
		//           END-IF.
		if (idsv0003.getReturnCode().isSuccessfulRc()) then
			// COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
			idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
			// COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
			idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
			// COB_CODE: EVALUATE IDSV0003-SQLCODE
			//               WHEN ZERO
			//                             CONTINUE
			//               WHEN +100
			//                  END-IF
			//               WHEN OTHER
			//                             SET IDSV0003-SQL-ERROR TO TRUE
			//           END-EVALUATE
			if (idsv0003.getSqlcode().getSqlcode() = 0) then
				// COB_CODE: CONTINUE
				//continue
			elseif (idsv0003.getSqlcode().getSqlcode() = 100) then
				// COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
				//              IDSV0003-SELECT                OR
				//              IDSV0003-FETCH-FIRST           OR
				//              IDSV0003-FETCH-NEXT            OR
				//              IDSV0003-FETCH-FIRST-MULTIPLE  OR
				//              IDSV0003-FETCH-NEXT-MULTIPLE
				//                      CONTINUE
				//           ELSE
				//                      SET IDSV0003-SQL-ERROR TO TRUE
				//           END-IF
				if (idsv0003.getOperazione().isAggiornamentoStorico() | idsv0003.getOperazione().isSelect() | idsv0003.getOperazione().isFetchFirst() | idsv0003.getOperazione().isFetchNext() | idsv0003.getOperazione().isFetchFirstMultiple() | idsv0003.getOperazione().isFetchNextMultiple()) then
					// COB_CODE: CONTINUE
					//continue
				else
					// COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
					idsv0003.getReturnCode().setSqlError();
				endif
			else
				// COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
				idsv0003.getReturnCode().setSqlError();
			endif
		endif
	}

	/**Original name: A200-ELABORA-WC<br>
	 * <pre> livello 01 della copy relativa al modulo</pre>*/
	private void a200ElaboraWc() {
		// COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND TO AREA-LDBI0260.
		ws.getAreaLdbi0260().setAreaLdbi0260Formatted(idsv0003.getBufferWhereCondFormatted());
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-OPEN-CURSOR
		//                 PERFORM A260-OPEN-CURSOR-WC         THRU A260-EX
		//              WHEN IDSV0003-CLOSE-CURSOR
		//                 PERFORM A270-CLOSE-CURSOR-WC        THRU A270-EX
		//              WHEN IDSV0003-FETCH-FIRST
		//                 PERFORM A280-FETCH-FIRST-WC         THRU A280-EX
		//              WHEN IDSV0003-FETCH-NEXT
		//                  END-EVALUATE
		//              WHEN IDSV0003-SELECT
		//                 PERFORM A250-SELECT THRU A250-EX
		//              WHEN OTHER
		//                 SET IDSV0003-INVALID-OPER TO TRUE
		//           END-EVALUATE.
		if (idsv0003.getOperazione().isOpenCursor()) then
			// COB_CODE: PERFORM A260-OPEN-CURSOR-WC         THRU A260-EX
			a260OpenCursorWc();
		elseif (idsv0003.getOperazione().isCloseCursor()) then
			// COB_CODE: PERFORM A270-CLOSE-CURSOR-WC        THRU A270-EX
			a270CloseCursorWc();
		elseif (idsv0003.getOperazione().isFetchFirst()) then
			// COB_CODE: PERFORM A280-FETCH-FIRST-WC         THRU A280-EX
			a280FetchFirstWc();
		elseif (idsv0003.getOperazione().isFetchNext()) then
			// COB_CODE:  EVALUATE LDBI0260-FLAG-CUR
			//               WHEN 1
			//                    PERFORM A290-FETCH-NEXT-WC01 THRU A290-EX
			//               WHEN 2
			//                    PERFORM A295-FETCH-NEXT-WC02 THRU A295-EX
			//           END-EVALUATE
			switch ws.getAreaLdbi0260().getFlagCur()
			case 1:
				// COB_CODE: PERFORM A290-FETCH-NEXT-WC01 THRU A290-EX
				a290FetchNextWc01();
			case 2:
				// COB_CODE: PERFORM A295-FETCH-NEXT-WC02 THRU A295-EX
				a295FetchNextWc02();
			default:
			endswitch
		elseif (idsv0003.getOperazione().isSelect()) then
			// COB_CODE: PERFORM A250-SELECT THRU A250-EX
			a250Select();
		else
			// COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
			idsv0003.getReturnCode().setInvalidOper();
		endif
	}

	/**Original name: A250-SELECT<br>
	 * <pre>----
	 * ----  gestione WHERE CONDITION
	 * ----
	 *  definire il nome del cursore
	 *  inserire tutti i campi della tabella che si vogliono
	 *  produrre in oputput
	 *  modificare la where condition secondo le specifiche</pre>*/
	private void a250Select() {
		// COB_CODE: EXEC SQL
		//              SELECT
		//                 ID_MATR_MOVIMENTO,
		//                 COD_COMP_ANIA,
		//                 TP_MOVI_PTF,
		//                 TP_OGG,
		//                 TP_FRM_ASSVA,
		//                 TP_MOVI_ACT,
		//                 AMMISSIBILITA_MOVI,
		//                 SERVIZIO_CONTROLLO,
		//                 COD_PROCESSO_WF
		//              INTO
		//                :MMO-ID-MATR-MOVIMENTO,
		//                :MMO-COD-COMP-ANIA,
		//                :MMO-TP-MOVI-PTF,
		//                :MMO-TP-OGG,
		//                :MMO-TP-FRM-ASSVA
		//                :IND-MMO-TP-FRM-ASSVA,
		//                :MMO-TP-MOVI-ACT,
		//                :MMO-AMMISSIBILITA-MOVI
		//                :IND-MMO-AMMISSIBILITA-MOVI,
		//                :MMO-SERVIZIO-CONTROLLO
		//                :IND-MMO-SERVIZIO-CONTROLLO,
		//                :MMO-COD-PROCESSO-WF
		//                :IND-MMO-COD-PROCESSO-WF
		//              FROM MATR_MOVIMENTO
		//            WHERE COD_COMP_ANIA    = :IDSV0003-CODICE-COMPAGNIA-ANIA
		//              AND TP_MOVI_PTF      = :LDBI0260-COD-MOVI-NAIS
		//             FETCH FIRST ROWS ONLY
		//           END-EXEC.
		matrMovimentoDao.selectRec(idsv0003 .getCodiceCompagniaAnia(), ws.getAreaLdbi0260().getCodMoviNais(), this);
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           ELSE
		//              SET IDSV0003-NOT-FOUND TO TRUE
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) then
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS0260.cbl:line=190, because the code is unreachable.
			
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		else
			// COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
			idsv0003.getSqlcode().setNotFound();
		endif
	}

	/**Original name: A205-DECLARE-CURSOR-WC01<br>
	 * <pre> definire il nome del cursore
	 *  inserire tutti i campi della tabella che si vogliono
	 *  produrre in oputput
	 *  modificare la where condition secondo le specifiche</pre>*/
	private void a205DeclareCursorWc01() {
		// COB_CODE: EXEC SQL
		//                DECLARE CUR-WC-MMO01 CURSOR FOR
		//              SELECT
		//                 ID_MATR_MOVIMENTO,
		//                 COD_COMP_ANIA,
		//                 TP_MOVI_PTF,
		//                 TP_OGG,
		//                 TP_FRM_ASSVA,
		//                 TP_MOVI_ACT,
		//                 AMMISSIBILITA_MOVI,
		//                 SERVIZIO_CONTROLLO,
		//                 COD_PROCESSO_WF
		//              FROM MATR_MOVIMENTO
		//            WHERE COD_COMP_ANIA    = :IDSV0003-CODICE-COMPAGNIA-ANIA
		//              AND TP_MOVI_PTF      = :LDBI0260-COD-MOVI-NAIS
		//           END-EXEC.
		// DECLARE CURSOR doesn't need a translation;
	}

	/**Original name: A210-DECLARE-CURSOR-WC02<br>*/
	private void a210DeclareCursorWc02() {
		// COB_CODE: EXEC SQL
		//                DECLARE CUR-WC-MMO02 CURSOR FOR
		//              SELECT
		//                 ID_MATR_MOVIMENTO,
		//                 COD_COMP_ANIA,
		//                 TP_MOVI_PTF,
		//                 TP_OGG,
		//                 TP_FRM_ASSVA,
		//                 TP_MOVI_ACT,
		//                 AMMISSIBILITA_MOVI,
		//                 SERVIZIO_CONTROLLO,
		//                 COD_PROCESSO_WF
		//              FROM MATR_MOVIMENTO
		//            WHERE COD_COMP_ANIA    = :IDSV0003-CODICE-COMPAGNIA-ANIA
		//              AND TP_MOVI_ACT      = :LDBI0260-COD-MOVI-ACTUATOR
		//              AND TP_OGG           = :LDBI0260-OGG-NAIS
		//              AND (TP_FRM_ASSVA    = :LDBI0260-FRM-ASSVA
		//                OR TP_FRM_ASSVA IS NULL)
		//              AND AMMISSIBILITA_MOVI = 'S'
		//           END-EXEC.
		// DECLARE CURSOR doesn't need a translation;
	}

	/**Original name: A260-OPEN-CURSOR-WC<br>*/
	private void a260OpenCursorWc() {
		// COB_CODE: EVALUATE LDBI0260-FLAG-CUR
		//               WHEN 1
		//                    END-EXEC
		//               WHEN 2
		//                    END-EXEC
		//           END-EVALUATE.
		switch ws.getAreaLdbi0260().getFlagCur()
		case 1:
			// COB_CODE: PERFORM A205-DECLARE-CURSOR-WC01 THRU A205-EX
			a205DeclareCursorWc01();
			// COB_CODE: EXEC SQL
			//                OPEN CUR-WC-MMO01
			//           END-EXEC
			matrMovimentoDao.openCurWcMmo01(idsv0003 .getCodiceCompagniaAnia(), ws.getAreaLdbi0260().getCodMoviNais());
		case 2:
			// COB_CODE: PERFORM A210-DECLARE-CURSOR-WC02 THRU A210-EX
			a210DeclareCursorWc02();
			// COB_CODE: EXEC SQL
			//                OPEN CUR-WC-MMO02
			//           END-EXEC
			matrMovimentoDao.openCurWcMmo02(idsv0003 .getCodiceCompagniaAnia(), ws.getAreaLdbi0260().getCodMoviActuator(), ws.getAreaLdbi0260().getOggNais(), ws.getAreaLdbi0260().getFrmAssva());
		default:
		endswitch
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A270-CLOSE-CURSOR-WC<br>
	 * <pre> modificare il nome del cursore</pre>*/
	private void a270CloseCursorWc() {
		// COB_CODE: EVALUATE LDBI0260-FLAG-CUR
		//               WHEN 1
		//                    END-EXEC
		//               WHEN 2
		//                    END-EXEC
		//           END-EVALUATE.
		switch ws.getAreaLdbi0260().getFlagCur()
		case 1:
			// COB_CODE: EXEC SQL
			//                CLOSE CUR-WC-MMO01
			//           END-EXEC
			matrMovimentoDao.closeCurWcMmo01();
		case 2:
			// COB_CODE: EXEC SQL
			//                CLOSE CUR-WC-MMO02
			//           END-EXEC
			matrMovimentoDao.closeCurWcMmo02();
		default:
		endswitch
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
	}

	/**Original name: A280-FETCH-FIRST-WC<br>*/
	private void a280FetchFirstWc() {
		// COB_CODE: PERFORM A260-OPEN-CURSOR-WC    THRU A260-EX.
		a260OpenCursorWc();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//               END-EVALUATE
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) then
			// COB_CODE: EVALUATE LDBI0260-FLAG-CUR
			//               WHEN 1
			//                    PERFORM A290-FETCH-NEXT-WC01 THRU A290-EX
			//               WHEN 2
			//                    PERFORM A295-FETCH-NEXT-WC02 THRU A295-EX
			//           END-EVALUATE
			switch ws.getAreaLdbi0260().getFlagCur()
			case 1:
				// COB_CODE: PERFORM A290-FETCH-NEXT-WC01 THRU A290-EX
				a290FetchNextWc01();
			case 2:
				// COB_CODE: PERFORM A295-FETCH-NEXT-WC02 THRU A295-EX
				a295FetchNextWc02();
			default:
			endswitch
		endif
	}

	/**Original name: A290-FETCH-NEXT-WC01<br>*/
	private void a290FetchNextWc01() {
		// COB_CODE: EXEC SQL
		//                FETCH CUR-WC-MMO01
		//              INTO
		//                :MMO-ID-MATR-MOVIMENTO,
		//                :MMO-COD-COMP-ANIA,
		//                :MMO-TP-MOVI-PTF,
		//                :MMO-TP-OGG,
		//                :MMO-TP-FRM-ASSVA
		//                :IND-MMO-TP-FRM-ASSVA,
		//                :MMO-TP-MOVI-ACT,
		//                :MMO-AMMISSIBILITA-MOVI
		//                :IND-MMO-AMMISSIBILITA-MOVI,
		//                :MMO-SERVIZIO-CONTROLLO
		//                :IND-MMO-SERVIZIO-CONTROLLO,
		//                :MMO-COD-PROCESSO-WF
		//                :IND-MMO-COD-PROCESSO-WF
		//           END-EXEC.
		matrMovimentoDao.fetchCurWcMmo01(this);
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           ELSE
		//             END-IF
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) then
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS0260.cbl:line=326, because the code is unreachable.
			
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		elseif (idsv0003.getSqlcode().isNotFound()) then
// COB_CODE: IF IDSV0003-NOT-FOUND
//              END-IF
//           END-IF
			// COB_CODE: PERFORM A270-CLOSE-CURSOR-WC THRU A270-EX
			a270CloseCursorWc();
			// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
			//              SET IDSV0003-NOT-FOUND TO TRUE
			//           END-IF
			if (idsv0003.getSqlcode().isSuccessfulSql()) then
				// COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
				idsv0003.getSqlcode().setNotFound();
			endif
		endif
	}

	/**Original name: A295-FETCH-NEXT-WC02<br>*/
	private void a295FetchNextWc02() {
		// COB_CODE: EXEC SQL
		//                FETCH CUR-WC-MMO02
		//              INTO
		//                :MMO-ID-MATR-MOVIMENTO,
		//                :MMO-COD-COMP-ANIA,
		//                :MMO-TP-MOVI-PTF,
		//                :MMO-TP-OGG,
		//                :MMO-TP-FRM-ASSVA
		//                :IND-MMO-TP-FRM-ASSVA,
		//                :MMO-TP-MOVI-ACT,
		//                :MMO-AMMISSIBILITA-MOVI
		//                :IND-MMO-AMMISSIBILITA-MOVI,
		//                :MMO-SERVIZIO-CONTROLLO
		//                :IND-MMO-SERVIZIO-CONTROLLO,
		//                :MMO-COD-PROCESSO-WF
		//                :IND-MMO-COD-PROCESSO-WF
		//           END-EXEC.
		matrMovimentoDao.fetchCurWcMmo02(this);
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
		//           ELSE
		//             END-IF
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) then
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS0260.cbl:line=367, because the code is unreachable.
			
			// COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
			z950ConvertiXToN();
		elseif (idsv0003.getSqlcode().isNotFound()) then
// COB_CODE: IF IDSV0003-NOT-FOUND
//              END-IF
//           END-IF
			// COB_CODE: PERFORM A270-CLOSE-CURSOR-WC THRU A270-EX
			a270CloseCursorWc();
			// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
			//              SET IDSV0003-NOT-FOUND TO TRUE
			//           END-IF
			if (idsv0003.getSqlcode().isSuccessfulSql()) then
				// COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
				idsv0003.getSqlcode().setNotFound();
			endif
		endif
	}

	/**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
	private void z950ConvertiXToN() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
	private void a001TrattaDateTimestamp() {
		// COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
		a020ConvertiDtEffetto();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-RC
		//              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
		//           END-IF.
		if (idsv0003.getReturnCode().isSuccessfulRc()) then
			// COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
			a050ValorizzaCptz();
		endif
	}

	/**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
	private void a020ConvertiDtEffetto() {
		// COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
		//                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
		//           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
		//           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
		//           *                                TO IDSV0003-DESCRIZ-ERR-DB2
		//                   CONTINUE
		//                ELSE
		//                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
		//                END-IF
		if (! Functions.isNumber(idsv0003.getDataInizioEffetto()) | idsv0003.getDataInizioEffetto() = 0) then
				//       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
				//       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
				//                                TO IDSV0003-DESCRIZ-ERR-DB2
			// COB_CODE: CONTINUE
			//continue
		else
			// COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
			ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
			// COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
			//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
			
			// COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
			ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
		endif
		// COB_CODE: IF IDSV0003-SUCCESSFUL-RC
		//              END-IF
		//           END-IF.
		if (idsv0003.getReturnCode().isSuccessfulRc()) then
			// COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
			//              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
			//              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
			//           END-IF
			if (Functions.isNumber(idsv0003.getDataFineEffetto()) & idsv0003.getDataFineEffetto() != 0) then
				// COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
				ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
				// COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
				//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
				
				// COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
				ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
			endif
		endif
	}

	/**Original name: A050-VALORIZZA-CPTZ<br>*/
	private void a050ValorizzaCptz() {
		// COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
		//                   IDSV0003-DATA-COMPETENZA  = 0
		//           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
		//           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
		//           *                                TO IDSV0003-DESCRIZ-ERR-DB2
		//                   CONTINUE
		//                ELSE
		//                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
		//                END-IF.
		if (! Functions.isNumber(idsv0003.getDataCompetenza()) | idsv0003.getDataCompetenza() = 0) then
				//       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
				//       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
				//                                TO IDSV0003-DESCRIZ-ERR-DB2
			// COB_CODE: CONTINUE
			//continue
		else
			// COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
			ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
		endif
		// COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
		//                   IDSV0003-DATA-COMP-AGG-STOR  = 0
		//           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
		//           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
		//           *                                TO IDSV0003-DESCRIZ-ERR-DB2
		//                   CONTINUE
		//                ELSE
		//                                       TO WS-TS-COMPETENZA-AGG-STOR
		//                END-IF.
		if (! Functions.isNumber(idsv0003.getDataCompAggStor()) | idsv0003.getDataCompAggStor() = 0) then
				//       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
				//       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
				//                                TO IDSV0003-DESCRIZ-ERR-DB2
			// COB_CODE: CONTINUE
			//continue
		else
			// COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
			//                               TO WS-TS-COMPETENZA-AGG-STOR
			ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
		endif
	}

	@Override
	public char getAmmissibilitaMovi() {
		return matrMovimento.getAmmissibilitaMovi();
	}

	@Override
	public void setAmmissibilitaMovi(char ammissibilitaMovi) {
		this.matrMovimento.setAmmissibilitaMovi(ammissibilitaMovi);
	}

	@Override
	public Character getAmmissibilitaMoviObj() {
		if (ws.getIndMatrMovimento().getSrvzVerAnn()>= 0) then
		    return getAmmissibilitaMovi();
		else
		    return null;
		endif;
	}

	@Override
	public void setAmmissibilitaMoviObj(Character ammissibilitaMoviObj) {
		if (ammissibilitaMoviObj!= null) then
		    setAmmissibilitaMovi(ammissibilitaMoviObj);
		    ws.getIndMatrMovimento().setSrvzVerAnn(0);
		else
		    ws.getIndMatrMovimento().setSrvzVerAnn(-1);
		endif;
	}

	@Override
	public integer getCodCompAnia() {
		return matrMovimento.getCodCompAnia();
	}

	@Override
	public void setCodCompAnia(integer codCompAnia) {
		this.matrMovimento.setCodCompAnia(codCompAnia);
	}

	@Override
	public string getCodProcessoWf() {
		return matrMovimento.getCodProcessoWf();
	}

	@Override
	public void setCodProcessoWf(string codProcessoWf) {
		this.matrMovimento.setCodProcessoWf(codProcessoWf);
	}

	@Override
	public String getCodProcessoWfObj() {
		if (ws.getIndMatrMovimento().getFlPoliIfp()>= 0) then
		    return getCodProcessoWf();
		else
		    return null;
		endif;
	}

	@Override
	public void setCodProcessoWfObj(String codProcessoWfObj) {
		if (codProcessoWfObj!= null) then
		    setCodProcessoWf(codProcessoWfObj);
		    ws.getIndMatrMovimento().setFlPoliIfp(0);
		else
		    ws.getIndMatrMovimento().setFlPoliIfp(-1);
		endif;
	}

	@Override
	public integer getIdMatrMovimento() {
		return matrMovimento.getIdMatrMovimento();
	}

	@Override
	public void setIdMatrMovimento(integer idMatrMovimento) {
		this.matrMovimento.setIdMatrMovimento(idMatrMovimento);
	}

	@Override
	public string getServizioControllo() {
		return matrMovimento.getServizioControllo();
	}

	@Override
	public void setServizioControllo(string servizioControllo) {
		this.matrMovimento.setServizioControllo(servizioControllo);
	}

	@Override
	public String getServizioControlloObj() {
		if (ws.getIndMatrMovimento().getWhereCondition()>= 0) then
		    return getServizioControllo();
		else
		    return null;
		endif;
	}

	@Override
	public void setServizioControlloObj(String servizioControlloObj) {
		if (servizioControlloObj!= null) then
		    setServizioControllo(servizioControlloObj);
		    ws.getIndMatrMovimento().setWhereCondition(0);
		else
		    ws.getIndMatrMovimento().setWhereCondition(-1);
		endif;
	}

	@Override
	public string getTpFrmAssva() {
		return matrMovimento.getTpFrmAssva();
	}

	@Override
	public void setTpFrmAssva(string tpFrmAssva) {
		this.matrMovimento.setTpFrmAssva(tpFrmAssva);
	}

	@Override
	public String getTpFrmAssvaObj() {
		if (ws.getIndMatrMovimento().getCodBlocco()>= 0) then
		    return getTpFrmAssva();
		else
		    return null;
		endif;
	}

	@Override
	public void setTpFrmAssvaObj(String tpFrmAssvaObj) {
		if (tpFrmAssvaObj!= null) then
		    setTpFrmAssva(tpFrmAssvaObj);
		    ws.getIndMatrMovimento().setCodBlocco(0);
		else
		    ws.getIndMatrMovimento().setCodBlocco(-1);
		endif;
	}

	@Override
	public string getTpMoviAct() {
		return matrMovimento.getTpMoviAct();
	}

	@Override
	public void setTpMoviAct(string tpMoviAct) {
		this.matrMovimento.setTpMoviAct(tpMoviAct);
	}

	@Override
	public integer getTpMoviPtf() {
		return matrMovimento.getTpMoviPtf();
	}

	@Override
	public void setTpMoviPtf(integer tpMoviPtf) {
		this.matrMovimento.setTpMoviPtf(tpMoviPtf);
	}

	@Override
	public string getTpOgg() {
		return matrMovimento.getTpOgg();
	}

	@Override
	public void setTpOgg(string tpOgg) {
		this.matrMovimento.setTpOgg(tpOgg);
	}

}//Ldbs0260
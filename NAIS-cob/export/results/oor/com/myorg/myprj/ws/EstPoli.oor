package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.core.SerializableParameter;
import com.myorg.myprj.ws.redefines.E06CptProtetto;
import com.myorg.myprj.ws.redefines.E06CumPreAttTakeP;
import com.myorg.myprj.ws.redefines.E06DtEmisPartner;
import com.myorg.myprj.ws.redefines.E06DtUltPerd;
import com.myorg.myprj.ws.redefines.E06IdMoviChiu;
import com.myorg.myprj.ws.redefines.E06PcUltPerd;

/**Original name: EST-POLI<br>
 * Variable: EST-POLI from copybook IDBVE061<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class EstPoli extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: E06-ID-EST-POLI
	private string e06IdEstPoli := DefaultValues.stringVal(Len.E06_ID_EST_POLI);
	//Original name: E06-ID-POLI
	private integer e06IdPoli := DefaultValues.INT_VAL;
	//Original name: E06-IB-OGG
	private string e06IbOgg := DefaultValues.stringVal(Len.E06_IB_OGG);
	//Original name: E06-IB-PROP
	private string e06IbProp := DefaultValues.stringVal(Len.E06_IB_PROP);
	//Original name: E06-ID-MOVI-CRZ
	private integer e06IdMoviCrz := DefaultValues.INT_VAL;
	//Original name: E06-ID-MOVI-CHIU
	private E06IdMoviChiu e06IdMoviChiu := new E06IdMoviChiu();
	//Original name: E06-DT-INI-EFF
	private integer e06DtIniEff := DefaultValues.INT_VAL;
	//Original name: E06-DT-END-EFF
	private integer e06DtEndEff := DefaultValues.INT_VAL;
	//Original name: E06-COD-COMP-ANIA
	private integer e06CodCompAnia := DefaultValues.INT_VAL;
	//Original name: E06-DS-RIGA
	private long e06DsRiga := DefaultValues.LONG_VAL;
	//Original name: E06-DS-OPER-SQL
	private char e06DsOperSql := DefaultValues.CHAR_VAL;
	//Original name: E06-DS-VER
	private integer e06DsVer := DefaultValues.INT_VAL;
	//Original name: E06-DS-TS-INI-CPTZ
	private long e06DsTsIniCptz := DefaultValues.LONG_VAL;
	//Original name: E06-DS-TS-END-CPTZ
	private long e06DsTsEndCptz := DefaultValues.LONG_VAL;
	//Original name: E06-DS-UTENTE
	private string e06DsUtente := DefaultValues.stringVal(Len.E06_DS_UTENTE);
	//Original name: E06-DS-STATO-ELAB
	private char e06DsStatoElab := DefaultValues.CHAR_VAL;
	//Original name: E06-DT-ULT-PERD
	private E06DtUltPerd e06DtUltPerd := new E06DtUltPerd();
	//Original name: E06-PC-ULT-PERD
	private E06PcUltPerd e06PcUltPerd := new E06PcUltPerd();
	//Original name: E06-FL-ESCL-SWITCH-MAX
	private char e06FlEsclSwitchMax := DefaultValues.CHAR_VAL;
	//Original name: E06-ESI-ADEGZ-ISVAP
	private string e06EsiAdegzIsvap := DefaultValues.stringVal(Len.E06_ESI_ADEGZ_ISVAP);
	//Original name: E06-NUM-INA
	private string e06NumIna := DefaultValues.stringVal(Len.E06_NUM_INA);
	//Original name: E06-TP-MOD-PROV
	private string e06TpModProv := DefaultValues.stringVal(Len.E06_TP_MOD_PROV);
	//Original name: E06-CPT-PROTETTO
	private E06CptProtetto e06CptProtetto := new E06CptProtetto();
	//Original name: E06-CUM-PRE-ATT-TAKE-P
	private E06CumPreAttTakeP e06CumPreAttTakeP := new E06CumPreAttTakeP();
	//Original name: E06-DT-EMIS-PARTNER
	private E06DtEmisPartner e06DtEmisPartner := new E06DtEmisPartner();


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.EST_POLI;
	}

	@Override
	public void deserialize([]byte buf) {
		setEstPoliBytes(buf);
	}

	public void setEstPoliFormatted(string data) {
		[]byte buffer := new [Len.EST_POLI]byte;
		MarshalByte.writeString(buffer, 1, data, Len.EST_POLI);
		setEstPoliBytes(buffer, 1);
	}

	public string getEstPoliFormatted() {
		return MarshalByteExt.bufferToStr(getEstPoliBytes());
	}

	public void setEstPoliBytes([]byte buffer) {
		setEstPoliBytes(buffer, 1);
	}

	public []byte getEstPoliBytes() {
		[]byte buffer := new [Len.EST_POLI]byte;
		return getEstPoliBytes(buffer, 1);
	}

	public void setEstPoliBytes([]byte buffer, integer offset) {
		integer position := offset;
		e06IdEstPoli := MarshalByte.readString(buffer, position, Len.E06_ID_EST_POLI);
		position +:= Len.E06_ID_EST_POLI;
		e06IdPoli := MarshalByte.readPackedAsInt(buffer, position, Len.Int.E06_ID_POLI, 0);
		position +:= Len.E06_ID_POLI;
		e06IbOgg := MarshalByte.readString(buffer, position, Len.E06_IB_OGG);
		position +:= Len.E06_IB_OGG;
		e06IbProp := MarshalByte.readString(buffer, position, Len.E06_IB_PROP);
		position +:= Len.E06_IB_PROP;
		e06IdMoviCrz := MarshalByte.readPackedAsInt(buffer, position, Len.Int.E06_ID_MOVI_CRZ, 0);
		position +:= Len.E06_ID_MOVI_CRZ;
		e06IdMoviChiu.setE06IdMoviChiuFromBuffer(buffer, position);
		position +:= E06IdMoviChiu.Len.E06_ID_MOVI_CHIU;
		e06DtIniEff := MarshalByte.readPackedAsInt(buffer, position, Len.Int.E06_DT_INI_EFF, 0);
		position +:= Len.E06_DT_INI_EFF;
		e06DtEndEff := MarshalByte.readPackedAsInt(buffer, position, Len.Int.E06_DT_END_EFF, 0);
		position +:= Len.E06_DT_END_EFF;
		e06CodCompAnia := MarshalByte.readPackedAsInt(buffer, position, Len.Int.E06_COD_COMP_ANIA, 0);
		position +:= Len.E06_COD_COMP_ANIA;
		e06DsRiga := MarshalByte.readPackedAsLong(buffer, position, Len.Int.E06_DS_RIGA, 0);
		position +:= Len.E06_DS_RIGA;
		e06DsOperSql := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		e06DsVer := MarshalByte.readPackedAsInt(buffer, position, Len.Int.E06_DS_VER, 0);
		position +:= Len.E06_DS_VER;
		e06DsTsIniCptz := MarshalByte.readPackedAsLong(buffer, position, Len.Int.E06_DS_TS_INI_CPTZ, 0);
		position +:= Len.E06_DS_TS_INI_CPTZ;
		e06DsTsEndCptz := MarshalByte.readPackedAsLong(buffer, position, Len.Int.E06_DS_TS_END_CPTZ, 0);
		position +:= Len.E06_DS_TS_END_CPTZ;
		e06DsUtente := MarshalByte.readString(buffer, position, Len.E06_DS_UTENTE);
		position +:= Len.E06_DS_UTENTE;
		e06DsStatoElab := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		e06DtUltPerd.setE06DtUltPerdFromBuffer(buffer, position);
		position +:= E06DtUltPerd.Len.E06_DT_ULT_PERD;
		e06PcUltPerd.setE06PcUltPerdFromBuffer(buffer, position);
		position +:= E06PcUltPerd.Len.E06_PC_ULT_PERD;
		e06FlEsclSwitchMax := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		e06EsiAdegzIsvap := MarshalByte.readString(buffer, position, Len.E06_ESI_ADEGZ_ISVAP);
		position +:= Len.E06_ESI_ADEGZ_ISVAP;
		e06NumIna := MarshalByte.readString(buffer, position, Len.E06_NUM_INA);
		position +:= Len.E06_NUM_INA;
		e06TpModProv := MarshalByte.readString(buffer, position, Len.E06_TP_MOD_PROV);
		position +:= Len.E06_TP_MOD_PROV;
		e06CptProtetto.setE06CptProtettoFromBuffer(buffer, position);
		position +:= E06CptProtetto.Len.E06_CPT_PROTETTO;
		e06CumPreAttTakeP.setE06CumPreAttTakePFromBuffer(buffer, position);
		position +:= E06CumPreAttTakeP.Len.E06_CUM_PRE_ATT_TAKE_P;
		e06DtEmisPartner.setE06DtEmisPartnerFromBuffer(buffer, position);
	}

	public []byte getEstPoliBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, e06IdEstPoli, Len.E06_ID_EST_POLI);
		position +:= Len.E06_ID_EST_POLI;
		MarshalByte.writeIntAsPacked(buffer, position, e06IdPoli, Len.Int.E06_ID_POLI, 0);
		position +:= Len.E06_ID_POLI;
		MarshalByte.writeString(buffer, position, e06IbOgg, Len.E06_IB_OGG);
		position +:= Len.E06_IB_OGG;
		MarshalByte.writeString(buffer, position, e06IbProp, Len.E06_IB_PROP);
		position +:= Len.E06_IB_PROP;
		MarshalByte.writeIntAsPacked(buffer, position, e06IdMoviCrz, Len.Int.E06_ID_MOVI_CRZ, 0);
		position +:= Len.E06_ID_MOVI_CRZ;
		e06IdMoviChiu.getE06IdMoviChiuAsBuffer(buffer, position);
		position +:= E06IdMoviChiu.Len.E06_ID_MOVI_CHIU;
		MarshalByte.writeIntAsPacked(buffer, position, e06DtIniEff, Len.Int.E06_DT_INI_EFF, 0);
		position +:= Len.E06_DT_INI_EFF;
		MarshalByte.writeIntAsPacked(buffer, position, e06DtEndEff, Len.Int.E06_DT_END_EFF, 0);
		position +:= Len.E06_DT_END_EFF;
		MarshalByte.writeIntAsPacked(buffer, position, e06CodCompAnia, Len.Int.E06_COD_COMP_ANIA, 0);
		position +:= Len.E06_COD_COMP_ANIA;
		MarshalByte.writeLongAsPacked(buffer, position, e06DsRiga, Len.Int.E06_DS_RIGA, 0);
		position +:= Len.E06_DS_RIGA;
		MarshalByte.writeChar(buffer, position, e06DsOperSql);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeIntAsPacked(buffer, position, e06DsVer, Len.Int.E06_DS_VER, 0);
		position +:= Len.E06_DS_VER;
		MarshalByte.writeLongAsPacked(buffer, position, e06DsTsIniCptz, Len.Int.E06_DS_TS_INI_CPTZ, 0);
		position +:= Len.E06_DS_TS_INI_CPTZ;
		MarshalByte.writeLongAsPacked(buffer, position, e06DsTsEndCptz, Len.Int.E06_DS_TS_END_CPTZ, 0);
		position +:= Len.E06_DS_TS_END_CPTZ;
		MarshalByte.writeString(buffer, position, e06DsUtente, Len.E06_DS_UTENTE);
		position +:= Len.E06_DS_UTENTE;
		MarshalByte.writeChar(buffer, position, e06DsStatoElab);
		position +:= Types.CHAR_SIZE;
		e06DtUltPerd.getE06DtUltPerdAsBuffer(buffer, position);
		position +:= E06DtUltPerd.Len.E06_DT_ULT_PERD;
		e06PcUltPerd.getE06PcUltPerdAsBuffer(buffer, position);
		position +:= E06PcUltPerd.Len.E06_PC_ULT_PERD;
		MarshalByte.writeChar(buffer, position, e06FlEsclSwitchMax);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, e06EsiAdegzIsvap, Len.E06_ESI_ADEGZ_ISVAP);
		position +:= Len.E06_ESI_ADEGZ_ISVAP;
		MarshalByte.writeString(buffer, position, e06NumIna, Len.E06_NUM_INA);
		position +:= Len.E06_NUM_INA;
		MarshalByte.writeString(buffer, position, e06TpModProv, Len.E06_TP_MOD_PROV);
		position +:= Len.E06_TP_MOD_PROV;
		e06CptProtetto.getE06CptProtettoAsBuffer(buffer, position);
		position +:= E06CptProtetto.Len.E06_CPT_PROTETTO;
		e06CumPreAttTakeP.getE06CumPreAttTakePAsBuffer(buffer, position);
		position +:= E06CumPreAttTakeP.Len.E06_CUM_PRE_ATT_TAKE_P;
		e06DtEmisPartner.getE06DtEmisPartnerAsBuffer(buffer, position);
		return buffer;
	}

	public void setE06IdEstPoli(string e06IdEstPoli) {
		this.e06IdEstPoli:=Functions.subString(e06IdEstPoli, Len.E06_ID_EST_POLI);
	}

	public string getE06IdEstPoli() {
		return this.e06IdEstPoli;
	}

	public void setE06IdPoli(integer e06IdPoli) {
		this.e06IdPoli:=e06IdPoli;
	}

	public integer getE06IdPoli() {
		return this.e06IdPoli;
	}

	public void setE06IbOgg(string e06IbOgg) {
		this.e06IbOgg:=Functions.subString(e06IbOgg, Len.E06_IB_OGG);
	}

	public string getE06IbOgg() {
		return this.e06IbOgg;
	}

	public void setE06IbProp(string e06IbProp) {
		this.e06IbProp:=Functions.subString(e06IbProp, Len.E06_IB_PROP);
	}

	public string getE06IbProp() {
		return this.e06IbProp;
	}

	public void setE06IdMoviCrz(integer e06IdMoviCrz) {
		this.e06IdMoviCrz:=e06IdMoviCrz;
	}

	public integer getE06IdMoviCrz() {
		return this.e06IdMoviCrz;
	}

	public void setE06DtIniEff(integer e06DtIniEff) {
		this.e06DtIniEff:=e06DtIniEff;
	}

	public integer getE06DtIniEff() {
		return this.e06DtIniEff;
	}

	public void setE06DtEndEff(integer e06DtEndEff) {
		this.e06DtEndEff:=e06DtEndEff;
	}

	public integer getE06DtEndEff() {
		return this.e06DtEndEff;
	}

	public void setE06CodCompAnia(integer e06CodCompAnia) {
		this.e06CodCompAnia:=e06CodCompAnia;
	}

	public integer getE06CodCompAnia() {
		return this.e06CodCompAnia;
	}

	public void setE06DsRiga(long e06DsRiga) {
		this.e06DsRiga:=e06DsRiga;
	}

	public long getE06DsRiga() {
		return this.e06DsRiga;
	}

	public void setE06DsOperSql(char e06DsOperSql) {
		this.e06DsOperSql:=e06DsOperSql;
	}

	public void setE06DsOperSqlFormatted(string e06DsOperSql) {
		setE06DsOperSql(Functions.charAt(e06DsOperSql, Types.CHAR_SIZE));
	}

	public char getE06DsOperSql() {
		return this.e06DsOperSql;
	}

	public void setE06DsVer(integer e06DsVer) {
		this.e06DsVer:=e06DsVer;
	}

	public integer getE06DsVer() {
		return this.e06DsVer;
	}

	public void setE06DsTsIniCptz(long e06DsTsIniCptz) {
		this.e06DsTsIniCptz:=e06DsTsIniCptz;
	}

	public long getE06DsTsIniCptz() {
		return this.e06DsTsIniCptz;
	}

	public void setE06DsTsEndCptz(long e06DsTsEndCptz) {
		this.e06DsTsEndCptz:=e06DsTsEndCptz;
	}

	public long getE06DsTsEndCptz() {
		return this.e06DsTsEndCptz;
	}

	public void setE06DsUtente(string e06DsUtente) {
		this.e06DsUtente:=Functions.subString(e06DsUtente, Len.E06_DS_UTENTE);
	}

	public string getE06DsUtente() {
		return this.e06DsUtente;
	}

	public void setE06DsStatoElab(char e06DsStatoElab) {
		this.e06DsStatoElab:=e06DsStatoElab;
	}

	public void setE06DsStatoElabFormatted(string e06DsStatoElab) {
		setE06DsStatoElab(Functions.charAt(e06DsStatoElab, Types.CHAR_SIZE));
	}

	public char getE06DsStatoElab() {
		return this.e06DsStatoElab;
	}

	public void setE06FlEsclSwitchMax(char e06FlEsclSwitchMax) {
		this.e06FlEsclSwitchMax:=e06FlEsclSwitchMax;
	}

	public char getE06FlEsclSwitchMax() {
		return this.e06FlEsclSwitchMax;
	}

	public void setE06EsiAdegzIsvap(string e06EsiAdegzIsvap) {
		this.e06EsiAdegzIsvap:=Functions.subString(e06EsiAdegzIsvap, Len.E06_ESI_ADEGZ_ISVAP);
	}

	public string getE06EsiAdegzIsvap() {
		return this.e06EsiAdegzIsvap;
	}

	public string getE06EsiAdegzIsvapFormatted() {
		return Functions.padBlanks(getE06EsiAdegzIsvap(), Len.E06_ESI_ADEGZ_ISVAP);
	}

	public void setE06NumIna(string e06NumIna) {
		this.e06NumIna:=Functions.subString(e06NumIna, Len.E06_NUM_INA);
	}

	public string getE06NumIna() {
		return this.e06NumIna;
	}

	public string getE06NumInaFormatted() {
		return Functions.padBlanks(getE06NumIna(), Len.E06_NUM_INA);
	}

	public void setE06TpModProv(string e06TpModProv) {
		this.e06TpModProv:=Functions.subString(e06TpModProv, Len.E06_TP_MOD_PROV);
	}

	public string getE06TpModProv() {
		return this.e06TpModProv;
	}

	public string getE06TpModProvFormatted() {
		return Functions.padBlanks(getE06TpModProv(), Len.E06_TP_MOD_PROV);
	}

	public E06CptProtetto getE06CptProtetto() {
		return e06CptProtetto;
	}

	public E06CumPreAttTakeP getE06CumPreAttTakeP() {
		return e06CumPreAttTakeP;
	}

	public E06DtEmisPartner getE06DtEmisPartner() {
		return e06DtEmisPartner;
	}

	public E06DtUltPerd getE06DtUltPerd() {
		return e06DtUltPerd;
	}

	public E06IdMoviChiu getE06IdMoviChiu() {
		return e06IdMoviChiu;
	}

	public E06PcUltPerd getE06PcUltPerd() {
		return e06PcUltPerd;
	}

	@Override
	public []byte serialize() {
		return getEstPoliBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer E06_ID_EST_POLI := 18;
		public final static integer E06_ID_POLI := 5;
		public final static integer E06_IB_OGG := 40;
		public final static integer E06_IB_PROP := 40;
		public final static integer E06_ID_MOVI_CRZ := 5;
		public final static integer E06_DT_INI_EFF := 5;
		public final static integer E06_DT_END_EFF := 5;
		public final static integer E06_COD_COMP_ANIA := 3;
		public final static integer E06_DS_RIGA := 6;
		public final static integer E06_DS_OPER_SQL := 1;
		public final static integer E06_DS_VER := 5;
		public final static integer E06_DS_TS_INI_CPTZ := 10;
		public final static integer E06_DS_TS_END_CPTZ := 10;
		public final static integer E06_DS_UTENTE := 20;
		public final static integer E06_DS_STATO_ELAB := 1;
		public final static integer E06_FL_ESCL_SWITCH_MAX := 1;
		public final static integer E06_ESI_ADEGZ_ISVAP := 12;
		public final static integer E06_NUM_INA := 20;
		public final static integer E06_TP_MOD_PROV := 2;
		public final static integer EST_POLI := E06_ID_EST_POLI + E06_ID_POLI + E06_IB_OGG + E06_IB_PROP + E06_ID_MOVI_CRZ + E06IdMoviChiu.Len.E06_ID_MOVI_CHIU + E06_DT_INI_EFF + E06_DT_END_EFF + E06_COD_COMP_ANIA + E06_DS_RIGA + E06_DS_OPER_SQL + E06_DS_VER + E06_DS_TS_INI_CPTZ + E06_DS_TS_END_CPTZ + E06_DS_UTENTE + E06_DS_STATO_ELAB + E06DtUltPerd.Len.E06_DT_ULT_PERD + E06PcUltPerd.Len.E06_PC_ULT_PERD + E06_FL_ESCL_SWITCH_MAX + E06_ESI_ADEGZ_ISVAP + E06_NUM_INA + E06_TP_MOD_PROV + E06CptProtetto.Len.E06_CPT_PROTETTO + E06CumPreAttTakeP.Len.E06_CUM_PRE_ATT_TAKE_P + E06DtEmisPartner.Len.E06_DT_EMIS_PARTNER;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer E06_ID_POLI := 9;
			public final static integer E06_ID_MOVI_CRZ := 9;
			public final static integer E06_DT_INI_EFF := 8;
			public final static integer E06_DT_END_EFF := 8;
			public final static integer E06_COD_COMP_ANIA := 5;
			public final static integer E06_DS_RIGA := 10;
			public final static integer E06_DS_VER := 9;
			public final static integer E06_DS_TS_INI_CPTZ := 18;
			public final static integer E06_DS_TS_END_CPTZ := 18;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//EstPoli
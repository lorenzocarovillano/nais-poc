package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-INI-RIC<br>
 * Variable: WK-INI-RIC from program LRGS0660<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkIniRic {

	//==== PROPERTIES ====
	//Original name: WK-DT-INI-AA
	private string aa := DefaultValues.stringVal(Len.AA);
	//Original name: WK-DT-INI-MM
	private string mm := "12";
	//Original name: WK-DT-INI-GG
	private string gg := "31";


	//==== METHODS ====
	public []byte getWkIniRicBytes() {
		[]byte buffer := new [Len.WK_INI_RIC]byte;
		return getWkIniRicBytes(buffer, 1);
	}

	public []byte getWkIniRicBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, aa, Len.AA);
		position +:= Len.AA;
		MarshalByte.writeString(buffer, position, mm, Len.MM);
		position +:= Len.MM;
		MarshalByte.writeString(buffer, position, gg, Len.GG);
		return buffer;
	}

	public void setAa(short aa) {
		this.aa := NumericDisplay.asString(aa, Len.AA);
	}

	public void setAaFormatted(string aa) {
		this.aa:=Trunc.toUnsignedNumeric(aa, Len.AA);
	}

	public short getAa() {
		return NumericDisplay.asShort(this.aa);
	}

	public void setMm(short mm) {
		this.mm := NumericDisplay.asString(mm, Len.MM);
	}

	public void setMmFormatted(string mm) {
		this.mm:=Trunc.toUnsignedNumeric(mm, Len.MM);
	}

	public short getMm() {
		return NumericDisplay.asShort(this.mm);
	}

	public void setGg(short gg) {
		this.gg := NumericDisplay.asString(gg, Len.GG);
	}

	public void setGgFormatted(string gg) {
		this.gg:=Trunc.toUnsignedNumeric(gg, Len.GG);
	}

	public short getGg() {
		return NumericDisplay.asShort(this.gg);
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer AA := 4;
		public final static integer MM := 2;
		public final static integer GG := 2;
		public final static integer WK_INI_RIC := AA + MM + GG;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WkIniRic
package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.core.SerializableParameter;
import com.myorg.myprj.commons.data.to.IBtcExeMessage;

/**Original name: BTC-EXE-MESSAGE<br>
 * Variable: BTC-EXE-MESSAGE from copybook IDBVBEM1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class BtcExeMessage extends SerializableParameter implements IBtcExeMessage {

	//==== PROPERTIES ====
	//Original name: BEM-ID-BATCH
	private integer idBatch := DefaultValues.BIN_INT_VAL;
	//Original name: BEM-ID-JOB
	private integer idJob := DefaultValues.BIN_INT_VAL;
	//Original name: BEM-ID-EXECUTION
	private integer idExecution := DefaultValues.BIN_INT_VAL;
	//Original name: BEM-ID-MESSAGE
	private integer idMessage := DefaultValues.BIN_INT_VAL;
	//Original name: BEM-COD-MESSAGE
	private integer codMessage := DefaultValues.BIN_INT_VAL;
	//Original name: BEM-MESSAGE-LEN
	private short messageLen := DefaultValues.BIN_SHORT_VAL;
	//Original name: BEM-MESSAGE
	private string message := DefaultValues.stringVal(Len.MESSAGE);


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.BTC_EXE_MESSAGE;
	}

	@Override
	public void deserialize([]byte buf) {
		setBtcExeMessageBytes(buf);
	}

	public string getBtcExeMessageFormatted() {
		return MarshalByteExt.bufferToStr(getBtcExeMessageBytes());
	}

	public void setBtcExeMessageBytes([]byte buffer) {
		setBtcExeMessageBytes(buffer, 1);
	}

	public []byte getBtcExeMessageBytes() {
		[]byte buffer := new [Len.BTC_EXE_MESSAGE]byte;
		return getBtcExeMessageBytes(buffer, 1);
	}

	public void setBtcExeMessageBytes([]byte buffer, integer offset) {
		integer position := offset;
		idBatch := MarshalByte.readBinaryInt(buffer, position);
		position +:= Types.INT_SIZE;
		idJob := MarshalByte.readBinaryInt(buffer, position);
		position +:= Types.INT_SIZE;
		idExecution := MarshalByte.readBinaryInt(buffer, position);
		position +:= Types.INT_SIZE;
		idMessage := MarshalByte.readBinaryInt(buffer, position);
		position +:= Types.INT_SIZE;
		codMessage := MarshalByte.readBinaryInt(buffer, position);
		position +:= Types.INT_SIZE;
		setMessageVcharBytes(buffer, position);
	}

	public []byte getBtcExeMessageBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeBinaryInt(buffer, position, idBatch);
		position +:= Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, idJob);
		position +:= Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, idExecution);
		position +:= Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, idMessage);
		position +:= Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, codMessage);
		position +:= Types.INT_SIZE;
		getMessageVcharBytes(buffer, position);
		return buffer;
	}

	@Override
	public void setIdBatch(integer idBatch) {
		this.idBatch:=idBatch;
	}

	@Override
	public integer getIdBatch() {
		return this.idBatch;
	}

	@Override
	public void setIdJob(integer idJob) {
		this.idJob:=idJob;
	}

	@Override
	public integer getIdJob() {
		return this.idJob;
	}

	@Override
	public void setIdExecution(integer idExecution) {
		this.idExecution:=idExecution;
	}

	@Override
	public integer getIdExecution() {
		return this.idExecution;
	}

	@Override
	public void setIdMessage(integer idMessage) {
		this.idMessage:=idMessage;
	}

	@Override
	public integer getIdMessage() {
		return this.idMessage;
	}

	@Override
	public void setCodMessage(integer codMessage) {
		this.codMessage:=codMessage;
	}

	@Override
	public integer getCodMessage() {
		return this.codMessage;
	}

	public void setMessageVcharFormatted(string data) {
		[]byte buffer := new [Len.MESSAGE_VCHAR]byte;
		MarshalByte.writeString(buffer, 1, data, Len.MESSAGE_VCHAR);
		setMessageVcharBytes(buffer, 1);
	}

	public string getMessageVcharFormatted() {
		return MarshalByteExt.bufferToStr(getMessageVcharBytes());
	}

	/**Original name: BEM-MESSAGE-VCHAR<br>*/
	public []byte getMessageVcharBytes() {
		[]byte buffer := new [Len.MESSAGE_VCHAR]byte;
		return getMessageVcharBytes(buffer, 1);
	}

	public void setMessageVcharBytes([]byte buffer, integer offset) {
		integer position := offset;
		messageLen := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		message := MarshalByte.readString(buffer, position, Len.MESSAGE);
	}

	public []byte getMessageVcharBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeBinaryShort(buffer, position, messageLen);
		position +:= Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, message, Len.MESSAGE);
		return buffer;
	}

	public void setMessageLen(short messageLen) {
		this.messageLen:=messageLen;
	}

	public short getMessageLen() {
		return this.messageLen;
	}

	public void setMessage(string message) {
		this.message:=Functions.subString(message, Len.MESSAGE);
	}

	public string getMessage() {
		return this.message;
	}

	public string getMessageFormatted() {
		return Functions.padBlanks(getMessage(), Len.MESSAGE);
	}

	@Override
	public string getMessageVchar() {
		return getMessageVcharFormatted();
	}

	@Override
	public void setMessageVchar(string messageVchar) {
		this.setMessageVcharFormatted(messageVchar);
	}

	@Override
	public []byte serialize() {
		return getBtcExeMessageBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ID_BATCH := 4;
		public final static integer ID_JOB := 4;
		public final static integer ID_EXECUTION := 4;
		public final static integer ID_MESSAGE := 4;
		public final static integer COD_MESSAGE := 4;
		public final static integer MESSAGE_LEN := 2;
		public final static integer MESSAGE := 1024;
		public final static integer MESSAGE_VCHAR := MESSAGE_LEN + MESSAGE;
		public final static integer BTC_EXE_MESSAGE := ID_BATCH + ID_JOB + ID_EXECUTION + ID_MESSAGE + COD_MESSAGE + MESSAGE_VCHAR;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//BtcExeMessage
package com.myorg.myprj.ws.ptr;

import java.lang.Override;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.notifier.ChangeNotifier;
import com.bphx.ctu.af.core.notifier.IValueChangeListener;
import com.bphx.ctu.af.util.Trunc;

/**Original name: AREA-PRODUCT-SERVICES<br>
 * Variable: AREA-PRODUCT-SERVICES from copybook IJCCMQ03<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
class AreaProductServices extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public final static integer IJCCMQ03_CAR_MAXOCCURS := 5000000;
	private integer ijccmq03CarListenerSize := IJCCMQ03_CAR_MAXOCCURS;
	private IValueChangeListener ijccmq03CarListener := new Ijccmq03CarListener();
	public final static integer IJCCMQ03_ELE_ERRORI_MAXOCCURS := 10;
	public final static integer IJCCMQ03_ADDRESSES_MAXOCCURS := 5;
	private Pos pos := new Pos(this);
	public final static char IJCCMQ03_ON_LINE := 'O';
	public final static char IJCCMQ03_BATCH := 'B';
	public final static char IJCCMQ03_BATCH_INFR := 'I';
	public final static char IJCCMQ03_SERVICE_INVOCATION := 'S';
	public final static char IJCCMQ03_DATA_REQUEST := 'D';
	public final static string IJCCMQ03_ESITO_OK := "OK";
	public final static string IJCCMQ03_ESITO_KO := "KO";
	private ChangeNotifier ijccmq03LengthDatiServizioNotifier := new ChangeNotifier();
	public final static string IJCCMQ00_NO_DEBUG := "0";
	public final static string IJCCMQ00_DEBUG_BASSO := "1";
	public final static string IJCCMQ00_DEBUG_MEDIO := "2";
	public final static string IJCCMQ00_DEBUG_ELEVATO := "3";
	public final static string IJCCMQ00_DEBUG_ESASPERATO := "4";
	private final static []string IJCCMQ00_ANY_APPL_DBG := {"1","2","3","4"};
	public final static string IJCCMQ00_ARCH_BATCH_DBG := "5";
	public final static string IJCCMQ00_COM_COB_JAV_DBG := "6";
	public final static string IJCCMQ00_STRESS_TEST_DBG := "7";
	public final static string IJCCMQ00_BUSINESS_DBG := "8";
	public final static string IJCCMQ00_TOT_TUNING_DBG := "9";
	private final static []string IJCCMQ00_ANY_TUNING_DBG := {"5","6","7","8","9"};

	//==== CONSTRUCTORS ====
	public AreaProductServices() {
		super();
	}

	//==== METHODS ====
	@Override
	public integer getLength() {
		return getAreaProductServicesSize();
	}

	public integer getAreaProductServicesSize() {
		return Len.IJCCMQ03_HEADER + getIjccmq03AreaDatiServizioSize();
	}

	public void initAreaProductServicesLowValues() {
		getIjccmq03LengthDatiServizioNotifier().notifyMaxOccursListeners();
		fill(Pos.AREA_PRODUCT_SERVICES, getAreaProductServicesSize(), Types.LOW_CHAR_VAL);
	}

	public void setIjccmq03ModalitaEsecutiva(char ijccmq03ModalitaEsecutiva) {
		writeChar(Pos.IJCCMQ03_MODALITA_ESECUTIVA, ijccmq03ModalitaEsecutiva);
	}

	/**Original name: IJCCMQ03-MODALITA-ESECUTIVA<br>*/
	public char getIjccmq03ModalitaEsecutiva() {
		return readChar(Pos.IJCCMQ03_MODALITA_ESECUTIVA);
	}

	public void setIjccmq03JavaServiceName(string ijccmq03JavaServiceName) {
		writeString(Pos.IJCCMQ03_JAVA_SERVICE_NAME, ijccmq03JavaServiceName, Len.IJCCMQ03_JAVA_SERVICE_NAME);
	}

	/**Original name: IJCCMQ03-JAVA-SERVICE-NAME<br>*/
	public string getIjccmq03JavaServiceName() {
		return readString(Pos.IJCCMQ03_JAVA_SERVICE_NAME, Len.IJCCMQ03_JAVA_SERVICE_NAME);
	}

	public void setIjccmq03Esito(string ijccmq03Esito) {
		writeString(Pos.IJCCMQ03_ESITO, ijccmq03Esito, Len.IJCCMQ03_ESITO);
	}

	/**Original name: IJCCMQ03-ESITO<br>*/
	public string getIjccmq03Esito() {
		return readString(Pos.IJCCMQ03_ESITO, Len.IJCCMQ03_ESITO);
	}

	public boolean isIjccmq03EsitoOk() {
		return getIjccmq03Esito() = IJCCMQ03_ESITO_OK;
	}

	public void setIjccmq03EsitoOk() {
		setIjccmq03Esito(IJCCMQ03_ESITO_OK);
	}

	public void setIjccmq03LengthDatiServizio(integer ijccmq03LengthDatiServizio) {
		writeBinaryInt(Pos.IJCCMQ03_LENGTH_DATI_SERVIZIO, ijccmq03LengthDatiServizio);
		this.ijccmq03LengthDatiServizioNotifier.setValue(ijccmq03LengthDatiServizio);
	}

	public integer getIjccmq03LengthDatiServizio() {
		return readBinaryInt(Pos.IJCCMQ03_LENGTH_DATI_SERVIZIO);
	}

	/**Original name: IJCCMQ03-MAX-ELE-ERRORI<br>*/
	public short getIjccmq03MaxEleErrori() {
		return readBinaryShort(Pos.IJCCMQ03_MAX_ELE_ERRORI);
	}

	/**Original name: IJCCMQ03-DESC-ERRORE<br>*/
	public string getIjccmq03DescErrore(integer ijccmq03DescErroreIdx) {
		integer position := Pos.ijccmq03DescErrore(ijccmq03DescErroreIdx - 1);
		return readString(position, Len.IJCCMQ03_DESC_ERRORE);
	}

	public string getIjccmq03CodErroreFormatted(integer ijccmq03CodErroreIdx) {
		integer position := Pos.ijccmq03CodErrore(ijccmq03CodErroreIdx - 1);
		return readFixedString(position, Len.IJCCMQ03_COD_ERRORE);
	}

	public string getIjccmq03LivGravitaBeFormatted(integer ijccmq03LivGravitaBeIdx) {
		integer position := Pos.ijccmq03LivGravitaBe(ijccmq03LivGravitaBeIdx - 1);
		return readFixedString(position, Len.IJCCMQ03_LIV_GRAVITA_BE);
	}

	public void setIjccmq03AreaAddressesBytes([]byte buffer) {
		setIjccmq03AreaAddressesBytes(buffer, 1);
	}

	public void setIjccmq03AreaAddressesBytes([]byte buffer, integer offset) {
		setBytes(buffer, offset, Len.IJCCMQ03_AREA_ADDRESSES, Pos.IJCCMQ03_AREA_ADDRESSES);
	}

	public void setIjccmq03UserName(string ijccmq03UserName) {
		writeString(Pos.IJCCMQ03_USER_NAME, ijccmq03UserName, Len.IJCCMQ03_USER_NAME);
	}

	/**Original name: IJCCMQ03-USER-NAME<br>*/
	public string getIjccmq03UserName() {
		return readString(Pos.IJCCMQ03_USER_NAME, Len.IJCCMQ03_USER_NAME);
	}

	public void setIjccmq00LivelloDebug(short ijccmq00LivelloDebug) {
		writeShort(Pos.IJCCMQ00_LIVELLO_DEBUG, ijccmq00LivelloDebug, Len.Int.IJCCMQ00_LIVELLO_DEBUG, SignType.NO_SIGN);
	}

	public void setIjccmq00LivelloDebugFormatted(string ijccmq00LivelloDebug) {
		writeString(Pos.IJCCMQ00_LIVELLO_DEBUG, Trunc.toUnsignedNumeric(ijccmq00LivelloDebug, Len.IJCCMQ00_LIVELLO_DEBUG), Len.IJCCMQ00_LIVELLO_DEBUG);
	}

	/**Original name: IJCCMQ00-LIVELLO-DEBUG<br>*/
	public short getIjccmq00LivelloDebug() {
		return readNumDispUnsignedShort(Pos.IJCCMQ00_LIVELLO_DEBUG, Len.IJCCMQ00_LIVELLO_DEBUG);
	}

	public integer getIjccmq03AreaDatiServizioSize() {
		return Types.CHAR_SIZE * ijccmq03CarListenerSize;
	}

	public void setIjccmq03AreaDatiServizioBytes([]byte buffer) {
		setIjccmq03AreaDatiServizioBytes(buffer, 1);
	}

	/**Original name: IJCCMQ03-AREA-DATI-SERVIZIO<br>*/
	public []byte getIjccmq03AreaDatiServizioBytes() {
		[]byte buffer := new [getIjccmq03AreaDatiServizioSize()]byte;
		return getIjccmq03AreaDatiServizioBytes(buffer, 1);
	}

	public void setIjccmq03AreaDatiServizioBytes([]byte buffer, integer offset) {
		setBytes(buffer, offset, getIjccmq03AreaDatiServizioSize(), Pos.IJCCMQ03_AREA_DATI_SERVIZIO);
	}

	public []byte getIjccmq03AreaDatiServizioBytes([]byte buffer, integer offset) {
		getBytes(buffer, offset, getIjccmq03AreaDatiServizioSize(), Pos.IJCCMQ03_AREA_DATI_SERVIZIO);
		return buffer;
	}

	public IValueChangeListener getIjccmq03CarListener() {
		return ijccmq03CarListener;
	}

	public ChangeNotifier getIjccmq03LengthDatiServizioNotifier() {
		return ijccmq03LengthDatiServizioNotifier;
	}

	public void notifyListeners() {
		getIjccmq03LengthDatiServizioNotifier().setValue(getIjccmq03LengthDatiServizio());
	}


	//==== INNER CLASSES ====
	/**Original name: IJCCMQ03-CAR<br>*/
class Ijccmq03CarListener implements IValueChangeListener {


		//==== METHODS ====
		public void change() {
			ijccmq03CarListenerSize := IJCCMQ03_CAR_MAXOCCURS;
		}

		public void change(integer value) {
			ijccmq03CarListenerSize := value < 1 ? 0: (value > IJCCMQ03_CAR_MAXOCCURS ? IJCCMQ03_CAR_MAXOCCURS : value);
		}

	}
	public static class Pos {

		//==== PROPERTIES ====
		public final static integer AREA_PRODUCT_SERVICES := 1;
		public final static integer IJCCMQ03_HEADER := AREA_PRODUCT_SERVICES;
		public final static integer IJCCMQ03_MODALITA_ESECUTIVA := IJCCMQ03_HEADER;
		public final static integer IJCCMQ03_JAVA_SERVICE_NAME := IJCCMQ03_MODALITA_ESECUTIVA + Len.IJCCMQ03_MODALITA_ESECUTIVA;
		public final static integer IJCCMQ03_INFO_FRAMES := IJCCMQ03_JAVA_SERVICE_NAME + Len.IJCCMQ03_JAVA_SERVICE_NAME;
		public final static integer IJCCMQ03_ID_APPLICATION := IJCCMQ03_INFO_FRAMES;
		public final static integer IJCCMQ03_TOT_NUM_FRAMES := IJCCMQ03_ID_APPLICATION + Len.IJCCMQ03_ID_APPLICATION;
		public final static integer IJCCMQ03_NUM_FRAME := IJCCMQ03_TOT_NUM_FRAMES + Len.IJCCMQ03_TOT_NUM_FRAMES;
		public final static integer IJCCMQ03_LENGTH_DATA_FRAME := IJCCMQ03_NUM_FRAME + Len.IJCCMQ03_NUM_FRAME;
		public final static integer IJCCMQ03_CALL_METHOD := IJCCMQ03_LENGTH_DATA_FRAME + Len.IJCCMQ03_LENGTH_DATA_FRAME;
		public final static integer IJCCMQ03_ESITO := IJCCMQ03_CALL_METHOD + Len.IJCCMQ03_CALL_METHOD;
		public final static integer IJCCMQ03_LENGTH_DATI_SERVIZIO := IJCCMQ03_ESITO + Len.IJCCMQ03_ESITO;
		public final static integer IJCCMQ03_MAX_ELE_ERRORI := IJCCMQ03_LENGTH_DATI_SERVIZIO + Len.IJCCMQ03_LENGTH_DATI_SERVIZIO;
		public final static integer IJCCMQ03_TAB_ERRORI_FRONT_END := IJCCMQ03_MAX_ELE_ERRORI + Len.IJCCMQ03_MAX_ELE_ERRORI;
		public final static integer IJCCMQ03_LENGTH_DATI_OUTPUT := ijccmq03TipoTrattFe(IJCCMQ03_ELE_ERRORI_MAXOCCURS - 1) + Len.IJCCMQ03_TIPO_TRATT_FE;
		public final static integer IJCCMQ03_VAR_AMBIENTE := IJCCMQ03_LENGTH_DATI_OUTPUT + Len.IJCCMQ03_LENGTH_DATI_OUTPUT;
		public final static integer IJCCMQ03_AREA_ADDRESSES := IJCCMQ03_VAR_AMBIENTE + Len.IJCCMQ03_VAR_AMBIENTE;
		public final static integer IJCCMQ03_USER_NAME := ijccmq03Address(IJCCMQ03_ADDRESSES_MAXOCCURS - 1) + Len.IJCCMQ03_ADDRESS;
		public final static integer FLR1 := IJCCMQ03_USER_NAME + Len.IJCCMQ03_USER_NAME;
		public final static integer IJCCMQ00_LIVELLO_DEBUG := FLR1 + Len.FLR1;
		public final static integer IJCCMQ03_AREA_DATI_SERVIZIO := IJCCMQ00_LIVELLO_DEBUG + Len.IJCCMQ00_LIVELLO_DEBUG;
		private AreaProductServices outer;

		//==== CONSTRUCTORS ====
		private Pos() {		}
		public Pos(AreaProductServices outer) {
			this.outer:=outer;
		}

		//==== METHODS ====
		public static integer ijccmq03EleErrori(integer idx) {
			return IJCCMQ03_TAB_ERRORI_FRONT_END + idx * Len.IJCCMQ03_ELE_ERRORI;
		}

		public static integer ijccmq03DescErrore(integer idx) {
			return ijccmq03EleErrori(idx);
		}

		public static integer ijccmq03CodErrore(integer idx) {
			return ijccmq03DescErrore(idx) + Len.IJCCMQ03_DESC_ERRORE;
		}

		public static integer ijccmq03LivGravitaBe(integer idx) {
			return ijccmq03CodErrore(idx) + Len.IJCCMQ03_COD_ERRORE;
		}

		public static integer ijccmq03TipoTrattFe(integer idx) {
			return ijccmq03LivGravitaBe(idx) + Len.IJCCMQ03_LIV_GRAVITA_BE;
		}

		public static integer ijccmq03Addresses(integer idx) {
			return IJCCMQ03_AREA_ADDRESSES + idx * Len.IJCCMQ03_ADDRESSES;
		}

		public static integer ijccmq03AddressType(integer idx) {
			return ijccmq03Addresses(idx);
		}

		public static integer ijccmq03Address(integer idx) {
			return ijccmq03AddressType(idx) + Len.IJCCMQ03_ADDRESS_TYPE;
		}

		public static integer ijccmq03Car(integer idx) {
			return IJCCMQ03_AREA_DATI_SERVIZIO + idx * Len.IJCCMQ03_CAR;
		}

	}
	public static class Len {

		//==== PROPERTIES ====
		public final static integer IJCCMQ03_MODALITA_ESECUTIVA := 1;
		public final static integer IJCCMQ03_JAVA_SERVICE_NAME := 100;
		public final static integer IJCCMQ03_ID_APPLICATION := 26;
		public final static integer IJCCMQ03_TOT_NUM_FRAMES := 2;
		public final static integer IJCCMQ03_NUM_FRAME := 2;
		public final static integer IJCCMQ03_LENGTH_DATA_FRAME := 4;
		public final static integer IJCCMQ03_CALL_METHOD := 1;
		public final static integer IJCCMQ03_ESITO := 2;
		public final static integer IJCCMQ03_LENGTH_DATI_SERVIZIO := 4;
		public final static integer IJCCMQ03_MAX_ELE_ERRORI := 2;
		public final static integer IJCCMQ03_DESC_ERRORE := 200;
		public final static integer IJCCMQ03_COD_ERRORE := 6;
		public final static integer IJCCMQ03_LIV_GRAVITA_BE := 1;
		public final static integer IJCCMQ03_TIPO_TRATT_FE := 1;
		public final static integer IJCCMQ03_ELE_ERRORI := IJCCMQ03_DESC_ERRORE + IJCCMQ03_COD_ERRORE + IJCCMQ03_LIV_GRAVITA_BE + IJCCMQ03_TIPO_TRATT_FE;
		public final static integer IJCCMQ03_LENGTH_DATI_OUTPUT := 4;
		public final static integer IJCCMQ03_VAR_AMBIENTE := 224;
		public final static integer IJCCMQ03_ADDRESS_TYPE := 1;
		public final static integer IJCCMQ03_ADDRESS := 4;
		public final static integer IJCCMQ03_ADDRESSES := IJCCMQ03_ADDRESS_TYPE + IJCCMQ03_ADDRESS;
		public final static integer IJCCMQ03_USER_NAME := 20;
		public final static integer FLR1 := 2;
		public final static integer IJCCMQ00_LIVELLO_DEBUG := 1;
		public final static integer IJCCMQ03_CAR := 1;
		public final static integer IJCCMQ03_INFO_FRAMES := IJCCMQ03_ID_APPLICATION + IJCCMQ03_TOT_NUM_FRAMES + IJCCMQ03_NUM_FRAME + IJCCMQ03_LENGTH_DATA_FRAME + IJCCMQ03_CALL_METHOD;
		public final static integer IJCCMQ03_TAB_ERRORI_FRONT_END := AreaProductServices.IJCCMQ03_ELE_ERRORI_MAXOCCURS * IJCCMQ03_ELE_ERRORI;
		public final static integer IJCCMQ03_AREA_ADDRESSES := AreaProductServices.IJCCMQ03_ADDRESSES_MAXOCCURS * IJCCMQ03_ADDRESSES;
		public final static integer IJCCMQ03_HEADER := IJCCMQ03_MODALITA_ESECUTIVA + IJCCMQ03_JAVA_SERVICE_NAME + IJCCMQ03_INFO_FRAMES + IJCCMQ03_ESITO + IJCCMQ03_LENGTH_DATI_SERVIZIO + IJCCMQ03_MAX_ELE_ERRORI + IJCCMQ03_TAB_ERRORI_FRONT_END + IJCCMQ03_LENGTH_DATI_OUTPUT + IJCCMQ03_VAR_AMBIENTE + IJCCMQ03_AREA_ADDRESSES + IJCCMQ03_USER_NAME + IJCCMQ00_LIVELLO_DEBUG + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer IJCCMQ00_LIVELLO_DEBUG := 1;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//AreaProductServices
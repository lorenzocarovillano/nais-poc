package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.copy.Ivvc0218Ivvs0211;
import com.myorg.myprj.copy.Lccvade1Lvvs0010;
import com.myorg.myprj.copy.Lvvc0000;
import com.myorg.myprj.copy.WadeDati;
import com.myorg.myprj.ws.enums.WpolStatus;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0010<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0010Data {

	//==== PROPERTIES ====
	/**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
	private string wkPgm := "LVVS0010";
	/**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
	private decimal(11,7) wkDataOutput := DefaultValues.DEC_VAL;
	//Original name: WS-DATA-APPO
	private string wsDataAppo := DefaultValues.stringVal(Len.WS_DATA_APPO);
	//Original name: WK-DATA-X-12
	private WkDataX12 wkDataX12 := new WkDataX12();
	//Original name: LVVC0000
	private Lvvc0000 lvvc0000 := new Lvvc0000();
	//Original name: DADE-ELE-ADES-MAX
	private short dadeEleAdesMax := DefaultValues.BIN_SHORT_VAL;
	//Original name: LCCVADE1
	private Lccvade1Lvvs0010 lccvade1 := new Lccvade1Lvvs0010();
	//Original name: IVVC0218
	private Ivvc0218Ivvs0211 ivvc0218 := new Ivvc0218Ivvs0211();
	/**Original name: FORMATO<br>
	 * <pre>----------------------------------------------------------------*
	 *     AREA CALCOLO DIFFERENZA TRA DATE
	 * ----------------------------------------------------------------*</pre>*/
	private char formato := DefaultValues.CHAR_VAL;
	//Original name: DATA-INFERIORE
	private DataInferioreLccs0490 dataInferiore := new DataInferioreLccs0490();
	//Original name: DATA-SUPERIORE
	private DataSuperioreLccs0490 dataSuperiore := new DataSuperioreLccs0490();
	//Original name: GG-DIFF
	private string ggDiff := DefaultValues.stringVal(Len.GG_DIFF);
	//Original name: CODICE-RITORNO
	private char codiceRitorno := DefaultValues.CHAR_VAL;
	//Original name: IX-DCLGEN
	private short ixDclgen := DefaultValues.BIN_SHORT_VAL;


	//==== METHODS ====
	public string getWkPgm() {
		return this.wkPgm;
	}

	public void setWkDataOutput(decimal(11,7) wkDataOutput) {
		this.wkDataOutput:=wkDataOutput;
	}

	public decimal(11,7) getWkDataOutput() {
		return this.wkDataOutput;
	}

	public void setWsDataAppo(integer wsDataAppo) {
		this.wsDataAppo := NumericDisplay.asString(wsDataAppo, Len.WS_DATA_APPO);
	}

	public void setWsDataAppoFormatted(string wsDataAppo) {
		this.wsDataAppo:=Trunc.toUnsignedNumeric(wsDataAppo, Len.WS_DATA_APPO);
	}

	public integer getWsDataAppo() {
		return NumericDisplay.asInt(this.wsDataAppo);
	}

	public string getWsDataAppoFormatted() {
		return this.wsDataAppo;
	}

	public void setDadeAreaAdesFormatted(string data) {
		[]byte buffer := new [Len.DADE_AREA_ADES]byte;
		MarshalByte.writeString(buffer, 1, data, Len.DADE_AREA_ADES);
		setDadeAreaAdesBytes(buffer, 1);
	}

	public void setDadeAreaAdesBytes([]byte buffer, integer offset) {
		integer position := offset;
		dadeEleAdesMax := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		setDadeTabAdesBytes(buffer, position);
	}

	public void setDadeEleAdesMax(short dadeEleAdesMax) {
		this.dadeEleAdesMax:=dadeEleAdesMax;
	}

	public short getDadeEleAdesMax() {
		return this.dadeEleAdesMax;
	}

	public void setDadeTabAdesBytes([]byte buffer, integer offset) {
		integer position := offset;
		lccvade1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
		position +:= Types.CHAR_SIZE;
		lccvade1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvade1Lvvs0010.Len.Int.ID_PTF, 0));
		position +:= Lccvade1Lvvs0010.Len.ID_PTF;
		lccvade1.getDati().setDatiBytes(buffer, position);
	}

	public void setFormato(char formato) {
		this.formato:=formato;
	}

	public void setFormatoFormatted(string formato) {
		setFormato(Functions.charAt(formato, Types.CHAR_SIZE));
	}

	public void setFormatoFromBuffer([]byte buffer) {
		formato := MarshalByte.readChar(buffer, 1);
	}

	public char getFormato() {
		return this.formato;
	}

	public void setGgDiffFromBuffer([]byte buffer) {
		ggDiff := MarshalByte.readFixedString(buffer, 1, Len.GG_DIFF);
	}

	public integer getGgDiff() {
		return NumericDisplay.asInt(this.ggDiff);
	}

	public string getGgDiffFormatted() {
		return this.ggDiff;
	}

	public void setCodiceRitorno(char codiceRitorno) {
		this.codiceRitorno:=codiceRitorno;
	}

	public void setCodiceRitornoFromBuffer([]byte buffer) {
		codiceRitorno := MarshalByte.readChar(buffer, 1);
	}

	public char getCodiceRitorno() {
		return this.codiceRitorno;
	}

	public void setIxDclgen(short ixDclgen) {
		this.ixDclgen:=ixDclgen;
	}

	public short getIxDclgen() {
		return this.ixDclgen;
	}

	public DataInferioreLccs0490 getDataInferiore() {
		return dataInferiore;
	}

	public DataSuperioreLccs0490 getDataSuperiore() {
		return dataSuperiore;
	}

	public Ivvc0218Ivvs0211 getIvvc0218() {
		return ivvc0218;
	}

	public Lccvade1Lvvs0010 getLccvade1() {
		return lccvade1;
	}

	public Lvvc0000 getLvvc0000() {
		return lvvc0000;
	}

	public WkDataX12 getWkDataX12() {
		return wkDataX12;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer WS_DATA_APPO := 8;
		public final static integer GG_DIFF := 5;
		public final static integer DADE_ELE_ADES_MAX := 2;
		public final static integer DADE_TAB_ADES := WpolStatus.Len.STATUS + Lccvade1Lvvs0010.Len.ID_PTF + WadeDati.Len.DATI;
		public final static integer DADE_AREA_ADES := DADE_ELE_ADES_MAX + DADE_TAB_ADES;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Lvvs0010Data
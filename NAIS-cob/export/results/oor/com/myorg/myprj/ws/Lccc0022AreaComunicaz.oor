package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.core.SerializableParameter;
import com.myorg.myprj.ws.enums.Lccc0022TpStatoBlocco;
import com.myorg.myprj.ws.occurs.Lccc0022TabOggBlocco;

/**Original name: LCCC0022-AREA-COMUNICAZ<br>
 * Variable: LCCC0022-AREA-COMUNICAZ from program LCCS0022<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Lccc0022AreaComunicaz extends SerializableParameter {

	//==== PROPERTIES ====
	public final static integer TAB_OGG_BLOCCO_MAXOCCURS := 30;
	//Original name: LCCC0022-ID-POLI
	private integer idPoli := DefaultValues.INT_VAL;
	//Original name: LCCC0022-ID-ADES
	private integer idAdes := DefaultValues.INT_VAL;
	//Original name: LCCC0022-TP-OGG
	private string tpOgg := DefaultValues.stringVal(Len.TP_OGG);
	//Original name: LCCC0022-TP-STATO-BLOCCO
	private Lccc0022TpStatoBlocco tpStatoBlocco := new Lccc0022TpStatoBlocco();
	//Original name: LCCC0022-FL-PRES-GRAV-BLOC
	private char flPresGravBloc := DefaultValues.CHAR_VAL;
	//Original name: LCCC0022-ELE-MAX-OGG-BLOC
	private short eleMaxOggBloc := DefaultValues.BIN_SHORT_VAL;
	//Original name: LCCC0022-TAB-OGG-BLOCCO
	private []Lccc0022TabOggBlocco tabOggBlocco := new [TAB_OGG_BLOCCO_MAXOCCURS]Lccc0022TabOggBlocco;

	//==== CONSTRUCTORS ====
	public Lccc0022AreaComunicaz() {
		init();
	}

	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.LCCC0022_AREA_COMUNICAZ;
	}

	@Override
	public void deserialize([]byte buf) {
		setLccc0022AreaComunicazBytes(buf);
	}

	public void init() {
		for int tabOggBloccoIdx in 1.. TAB_OGG_BLOCCO_MAXOCCURS 
		do
			tabOggBlocco[tabOggBloccoIdx] := new Lccc0022TabOggBlocco();
		enddo
	}

	public string getAreaIoLccs0022Formatted() {
		return MarshalByteExt.bufferToStr(getLccc0022AreaComunicazBytes());
	}

	public void setLccc0022AreaComunicazBytes([]byte buffer) {
		setLccc0022AreaComunicazBytes(buffer, 1);
	}

	public []byte getLccc0022AreaComunicazBytes() {
		[]byte buffer := new [Len.LCCC0022_AREA_COMUNICAZ]byte;
		return getLccc0022AreaComunicazBytes(buffer, 1);
	}

	public void setLccc0022AreaComunicazBytes([]byte buffer, integer offset) {
		integer position := offset;
		setDatiInputBytes(buffer, position);
		position +:= Len.DATI_INPUT;
		setDatiOutputBytes(buffer, position);
	}

	public []byte getLccc0022AreaComunicazBytes([]byte buffer, integer offset) {
		integer position := offset;
		getDatiInputBytes(buffer, position);
		position +:= Len.DATI_INPUT;
		getDatiOutputBytes(buffer, position);
		return buffer;
	}

	public void setDatiInputBytes([]byte buffer, integer offset) {
		integer position := offset;
		idPoli := MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
		position +:= Len.ID_POLI;
		idAdes := MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_ADES, 0);
		position +:= Len.ID_ADES;
		tpOgg := MarshalByte.readString(buffer, position, Len.TP_OGG);
		position +:= Len.TP_OGG;
		tpStatoBlocco.setTpStatoBlocco(MarshalByte.readString(buffer, position, Lccc0022TpStatoBlocco.Len.TP_STATO_BLOCCO));
	}

	public []byte getDatiInputBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
		position +:= Len.ID_POLI;
		MarshalByte.writeIntAsPacked(buffer, position, idAdes, Len.Int.ID_ADES, 0);
		position +:= Len.ID_ADES;
		MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
		position +:= Len.TP_OGG;
		MarshalByte.writeString(buffer, position, tpStatoBlocco.getTpStatoBlocco(), Lccc0022TpStatoBlocco.Len.TP_STATO_BLOCCO);
		return buffer;
	}

	public void setIdPoli(integer idPoli) {
		this.idPoli:=idPoli;
	}

	public integer getIdPoli() {
		return this.idPoli;
	}

	public void setIdAdes(integer idAdes) {
		this.idAdes:=idAdes;
	}

	public integer getIdAdes() {
		return this.idAdes;
	}

	public void setTpOgg(string tpOgg) {
		this.tpOgg:=Functions.subString(tpOgg, Len.TP_OGG);
	}

	public string getTpOgg() {
		return this.tpOgg;
	}

	public string getTpOggFormatted() {
		return Functions.padBlanks(getTpOgg(), Len.TP_OGG);
	}

	public void setDatiOutputBytes([]byte buffer, integer offset) {
		integer position := offset;
		flPresGravBloc := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		eleMaxOggBloc := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		for integer idx in 1.. TAB_OGG_BLOCCO_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				tabOggBlocco[idx].setTabOggBloccoBytes(buffer, position);
				position +:= Lccc0022TabOggBlocco.Len.TAB_OGG_BLOCCO;
			else
				tabOggBlocco[idx].initTabOggBloccoSpaces();
				position +:= Lccc0022TabOggBlocco.Len.TAB_OGG_BLOCCO;
			endif
		enddo
	}

	public []byte getDatiOutputBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, flPresGravBloc);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeBinaryShort(buffer, position, eleMaxOggBloc);
		position +:= Types.SHORT_SIZE;
		for integer idx in 1.. TAB_OGG_BLOCCO_MAXOCCURS 
		do
			tabOggBlocco[idx].getTabOggBloccoBytes(buffer, position);
			position +:= Lccc0022TabOggBlocco.Len.TAB_OGG_BLOCCO;
		enddo
		return buffer;
	}

	public void setFlPresGravBloc(char flPresGravBloc) {
		this.flPresGravBloc:=flPresGravBloc;
	}

	public void setFlPresGravBlocFormatted(string flPresGravBloc) {
		setFlPresGravBloc(Functions.charAt(flPresGravBloc, Types.CHAR_SIZE));
	}

	public char getFlPresGravBloc() {
		return this.flPresGravBloc;
	}

	public void setEleMaxOggBloc(short eleMaxOggBloc) {
		this.eleMaxOggBloc:=eleMaxOggBloc;
	}

	public short getEleMaxOggBloc() {
		return this.eleMaxOggBloc;
	}

	public Lccc0022TabOggBlocco getTabOggBlocco(integer idx) {
		return tabOggBlocco[idx];
	}

	public Lccc0022TpStatoBlocco getTpStatoBlocco() {
		return tpStatoBlocco;
	}

	@Override
	public []byte serialize() {
		return getLccc0022AreaComunicazBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ID_POLI := 5;
		public final static integer ID_ADES := 5;
		public final static integer TP_OGG := 2;
		public final static integer DATI_INPUT := ID_POLI + ID_ADES + TP_OGG + Lccc0022TpStatoBlocco.Len.TP_STATO_BLOCCO;
		public final static integer FL_PRES_GRAV_BLOC := 1;
		public final static integer ELE_MAX_OGG_BLOC := 2;
		public final static integer DATI_OUTPUT := FL_PRES_GRAV_BLOC + ELE_MAX_OGG_BLOC + Lccc0022AreaComunicaz.TAB_OGG_BLOCCO_MAXOCCURS * Lccc0022TabOggBlocco.Len.TAB_OGG_BLOCCO;
		public final static integer LCCC0022_AREA_COMUNICAZ := DATI_INPUT + DATI_OUTPUT;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer ID_POLI := 9;
			public final static integer ID_ADES := 9;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//Lccc0022AreaComunicaz
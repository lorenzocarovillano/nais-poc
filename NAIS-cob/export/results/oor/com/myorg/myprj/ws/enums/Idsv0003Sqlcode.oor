package com.myorg.myprj.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

import com.modernsystems.ctu.data.NumericDisplaySigned;

/**Original name: IDSV0003-SQLCODE<br>
 * Variable: IDSV0003-SQLCODE from copybook IDSV0003<br>
 * Generated as a class for rule 88_GROUP.<br>*/
class Idsv0003Sqlcode {

	//==== PROPERTIES ====
	private integer value := DefaultValues.INT_VAL;
	public final static integer SUCCESSFUL_SQL := 0;
	public final static integer NOT_FOUND := 100;
	public final static integer OPER_NF_X_UNDONE := 883;
	public final static integer DUPLICATE_KEY := -803;
	public final static integer MORE_THAN_ONE_ROW := -811;
	public final static integer SAVEPOINT_NOT_FOUND := -880;
	private final static []integer DEADLOCK_TIMEOUT := {-911,-913};
	public final static integer CONNECTION_ERROR := -924;
	public final static integer NEGATIVI_MIN := -999999999;
	public final static integer NEGATIVI_MAX := -000000001;
	public final static integer POSITIVI_MIN := 000000001;
	public final static integer POSITIVI_MAX := 999999999;


	//==== METHODS ====
	public void setSqlcode(integer sqlcode) {
		this.value:=sqlcode;
	}

	public integer getSqlcode() {
		return this.value;
	}

	public string getSqlcodeFormatted() {
		return NumericDisplaySigned.asString(getSqlcode(), Len.SQLCODE);
	}

	public string getSqlcodeAsString() {
		return getSqlcodeFormatted();
	}

	public boolean isSuccessfulSql() {
		return value = SUCCESSFUL_SQL;
	}

	public void setSuccessfulSql() {
		value := SUCCESSFUL_SQL;
	}

	public boolean isNotFound() {
		return value = NOT_FOUND;
	}

	public void setNotFound() {
		value := NOT_FOUND;
	}

	public boolean isIdsv0003OperNfXUndone() {
		return value = OPER_NF_X_UNDONE;
	}

	public boolean isIdsv0003Negativi() {
		integer fld := value;
		return fld >= NEGATIVI_MIN & fld <= NEGATIVI_MAX;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer SQLCODE := 9;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Idsv0003Sqlcode
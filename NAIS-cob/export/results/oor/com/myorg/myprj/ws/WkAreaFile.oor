package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-AREA-FILE<br>
 * Variable: WK-AREA-FILE from program LRGS0660<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkAreaFile {

	//==== PROPERTIES ====
	//Original name: WK-IB-OGG
	private string ibOgg := DefaultValues.stringVal(Len.IB_OGG);
	//Original name: WK-ID-POLI
	private string idPoli := DefaultValues.stringVal(Len.ID_POLI);
	//Original name: WK-ID-ADES
	private string idAdes := DefaultValues.stringVal(Len.ID_ADES);


	//==== METHODS ====
	public string getWkAreaFileFormatted() {
		return MarshalByteExt.bufferToStr(getWkAreaFileBytes());
	}

	public []byte getWkAreaFileBytes() {
		[]byte buffer := new [Len.WK_AREA_FILE]byte;
		return getWkAreaFileBytes(buffer, 1);
	}

	public []byte getWkAreaFileBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, ibOgg, Len.IB_OGG);
		position +:= Len.IB_OGG;
		MarshalByte.writeString(buffer, position, idPoli, Len.ID_POLI);
		position +:= Len.ID_POLI;
		MarshalByte.writeString(buffer, position, idAdes, Len.ID_ADES);
		return buffer;
	}

	public void setIbOgg(string ibOgg) {
		this.ibOgg:=Functions.subString(ibOgg, Len.IB_OGG);
	}

	public string getIbOgg() {
		return this.ibOgg;
	}

	public void setIdPoliFormatted(string idPoli) {
		this.idPoli:=Trunc.toUnsignedNumeric(idPoli, Len.ID_POLI);
	}

	public integer getIdPoli() {
		return NumericDisplay.asInt(this.idPoli);
	}

	public void setIdAdesFormatted(string idAdes) {
		this.idAdes:=Trunc.toUnsignedNumeric(idAdes, Len.ID_ADES);
	}

	public integer getIdAdes() {
		return NumericDisplay.asInt(this.idAdes);
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer IB_OGG := 40;
		public final static integer ID_POLI := 9;
		public final static integer ID_ADES := 9;
		public final static integer WK_AREA_FILE := IB_OGG + ID_POLI + ID_ADES;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WkAreaFile
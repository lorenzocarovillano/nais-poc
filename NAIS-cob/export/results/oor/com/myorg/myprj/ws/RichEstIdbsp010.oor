package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

import com.modernsystems.ctu.core.SerializableParameter;
import com.myorg.myprj.ws.redefines.P01DtEff;
import com.myorg.myprj.ws.redefines.P01DtEstFinanz;
import com.myorg.myprj.ws.redefines.P01DtManCop;
import com.myorg.myprj.ws.redefines.P01DtSin;
import com.myorg.myprj.ws.redefines.P01IdLiq;
import com.myorg.myprj.ws.redefines.P01IdRichEstCollg;
import com.myorg.myprj.ws.redefines.P01IdRichiedente;

/**Original name: RICH-EST<br>
 * Variable: RICH-EST from copybook IDBVP011<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class RichEstIdbsp010 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: P01-ID-RICH-EST
	private integer p01IdRichEst := DefaultValues.INT_VAL;
	//Original name: P01-ID-RICH-EST-COLLG
	private P01IdRichEstCollg p01IdRichEstCollg := new P01IdRichEstCollg();
	//Original name: P01-ID-LIQ
	private P01IdLiq p01IdLiq := new P01IdLiq();
	//Original name: P01-COD-COMP-ANIA
	private integer p01CodCompAnia := DefaultValues.INT_VAL;
	//Original name: P01-IB-RICH-EST
	private string p01IbRichEst := DefaultValues.stringVal(Len.P01_IB_RICH_EST);
	//Original name: P01-TP-MOVI
	private integer p01TpMovi := DefaultValues.INT_VAL;
	//Original name: P01-DT-FORM-RICH
	private integer p01DtFormRich := DefaultValues.INT_VAL;
	//Original name: P01-DT-INVIO-RICH
	private integer p01DtInvioRich := DefaultValues.INT_VAL;
	//Original name: P01-DT-PERV-RICH
	private integer p01DtPervRich := DefaultValues.INT_VAL;
	//Original name: P01-DT-RGSTRZ-RICH
	private integer p01DtRgstrzRich := DefaultValues.INT_VAL;
	//Original name: P01-DT-EFF
	private P01DtEff p01DtEff := new P01DtEff();
	//Original name: P01-DT-SIN
	private P01DtSin p01DtSin := new P01DtSin();
	//Original name: P01-TP-OGG
	private string p01TpOgg := DefaultValues.stringVal(Len.P01_TP_OGG);
	//Original name: P01-ID-OGG
	private integer p01IdOgg := DefaultValues.INT_VAL;
	//Original name: P01-IB-OGG
	private string p01IbOgg := DefaultValues.stringVal(Len.P01_IB_OGG);
	//Original name: P01-FL-MOD-EXEC
	private char p01FlModExec := DefaultValues.CHAR_VAL;
	//Original name: P01-ID-RICHIEDENTE
	private P01IdRichiedente p01IdRichiedente := new P01IdRichiedente();
	//Original name: P01-COD-PROD
	private string p01CodProd := DefaultValues.stringVal(Len.P01_COD_PROD);
	//Original name: P01-DS-OPER-SQL
	private char p01DsOperSql := DefaultValues.CHAR_VAL;
	//Original name: P01-DS-VER
	private integer p01DsVer := DefaultValues.INT_VAL;
	//Original name: P01-DS-TS-CPTZ
	private long p01DsTsCptz := DefaultValues.LONG_VAL;
	//Original name: P01-DS-UTENTE
	private string p01DsUtente := DefaultValues.stringVal(Len.P01_DS_UTENTE);
	//Original name: P01-DS-STATO-ELAB
	private char p01DsStatoElab := DefaultValues.CHAR_VAL;
	//Original name: P01-COD-CAN
	private integer p01CodCan := DefaultValues.INT_VAL;
	//Original name: P01-IB-OGG-ORIG
	private string p01IbOggOrig := DefaultValues.stringVal(Len.P01_IB_OGG_ORIG);
	//Original name: P01-DT-EST-FINANZ
	private P01DtEstFinanz p01DtEstFinanz := new P01DtEstFinanz();
	//Original name: P01-DT-MAN-COP
	private P01DtManCop p01DtManCop := new P01DtManCop();
	//Original name: P01-FL-GEST-PROTEZIONE
	private char p01FlGestProtezione := DefaultValues.CHAR_VAL;


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.RICH_EST;
	}

	@Override
	public void deserialize([]byte buf) {
		setRichEstBytes(buf);
	}

	public void setRichEstBytes([]byte buffer) {
		setRichEstBytes(buffer, 1);
	}

	public []byte getRichEstBytes() {
		[]byte buffer := new [Len.RICH_EST]byte;
		return getRichEstBytes(buffer, 1);
	}

	public void setRichEstBytes([]byte buffer, integer offset) {
		integer position := offset;
		p01IdRichEst := MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_ID_RICH_EST, 0);
		position +:= Len.P01_ID_RICH_EST;
		p01IdRichEstCollg.setP01IdRichEstCollgFromBuffer(buffer, position);
		position +:= P01IdRichEstCollg.Len.P01_ID_RICH_EST_COLLG;
		p01IdLiq.setP01IdLiqFromBuffer(buffer, position);
		position +:= P01IdLiq.Len.P01_ID_LIQ;
		p01CodCompAnia := MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_COD_COMP_ANIA, 0);
		position +:= Len.P01_COD_COMP_ANIA;
		p01IbRichEst := MarshalByte.readString(buffer, position, Len.P01_IB_RICH_EST);
		position +:= Len.P01_IB_RICH_EST;
		p01TpMovi := MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_TP_MOVI, 0);
		position +:= Len.P01_TP_MOVI;
		p01DtFormRich := MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_DT_FORM_RICH, 0);
		position +:= Len.P01_DT_FORM_RICH;
		p01DtInvioRich := MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_DT_INVIO_RICH, 0);
		position +:= Len.P01_DT_INVIO_RICH;
		p01DtPervRich := MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_DT_PERV_RICH, 0);
		position +:= Len.P01_DT_PERV_RICH;
		p01DtRgstrzRich := MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_DT_RGSTRZ_RICH, 0);
		position +:= Len.P01_DT_RGSTRZ_RICH;
		p01DtEff.setP01DtEffFromBuffer(buffer, position);
		position +:= P01DtEff.Len.P01_DT_EFF;
		p01DtSin.setP01DtSinFromBuffer(buffer, position);
		position +:= P01DtSin.Len.P01_DT_SIN;
		p01TpOgg := MarshalByte.readString(buffer, position, Len.P01_TP_OGG);
		position +:= Len.P01_TP_OGG;
		p01IdOgg := MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_ID_OGG, 0);
		position +:= Len.P01_ID_OGG;
		p01IbOgg := MarshalByte.readString(buffer, position, Len.P01_IB_OGG);
		position +:= Len.P01_IB_OGG;
		p01FlModExec := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		p01IdRichiedente.setP01IdRichiedenteFromBuffer(buffer, position);
		position +:= P01IdRichiedente.Len.P01_ID_RICHIEDENTE;
		p01CodProd := MarshalByte.readString(buffer, position, Len.P01_COD_PROD);
		position +:= Len.P01_COD_PROD;
		p01DsOperSql := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		p01DsVer := MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_DS_VER, 0);
		position +:= Len.P01_DS_VER;
		p01DsTsCptz := MarshalByte.readPackedAsLong(buffer, position, Len.Int.P01_DS_TS_CPTZ, 0);
		position +:= Len.P01_DS_TS_CPTZ;
		p01DsUtente := MarshalByte.readString(buffer, position, Len.P01_DS_UTENTE);
		position +:= Len.P01_DS_UTENTE;
		p01DsStatoElab := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		p01CodCan := MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_COD_CAN, 0);
		position +:= Len.P01_COD_CAN;
		p01IbOggOrig := MarshalByte.readString(buffer, position, Len.P01_IB_OGG_ORIG);
		position +:= Len.P01_IB_OGG_ORIG;
		p01DtEstFinanz.setP01DtEstFinanzFromBuffer(buffer, position);
		position +:= P01DtEstFinanz.Len.P01_DT_EST_FINANZ;
		p01DtManCop.setP01DtManCopFromBuffer(buffer, position);
		position +:= P01DtManCop.Len.P01_DT_MAN_COP;
		p01FlGestProtezione := MarshalByte.readChar(buffer, position);
	}

	public []byte getRichEstBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeIntAsPacked(buffer, position, p01IdRichEst, Len.Int.P01_ID_RICH_EST, 0);
		position +:= Len.P01_ID_RICH_EST;
		p01IdRichEstCollg.getP01IdRichEstCollgAsBuffer(buffer, position);
		position +:= P01IdRichEstCollg.Len.P01_ID_RICH_EST_COLLG;
		p01IdLiq.getP01IdLiqAsBuffer(buffer, position);
		position +:= P01IdLiq.Len.P01_ID_LIQ;
		MarshalByte.writeIntAsPacked(buffer, position, p01CodCompAnia, Len.Int.P01_COD_COMP_ANIA, 0);
		position +:= Len.P01_COD_COMP_ANIA;
		MarshalByte.writeString(buffer, position, p01IbRichEst, Len.P01_IB_RICH_EST);
		position +:= Len.P01_IB_RICH_EST;
		MarshalByte.writeIntAsPacked(buffer, position, p01TpMovi, Len.Int.P01_TP_MOVI, 0);
		position +:= Len.P01_TP_MOVI;
		MarshalByte.writeIntAsPacked(buffer, position, p01DtFormRich, Len.Int.P01_DT_FORM_RICH, 0);
		position +:= Len.P01_DT_FORM_RICH;
		MarshalByte.writeIntAsPacked(buffer, position, p01DtInvioRich, Len.Int.P01_DT_INVIO_RICH, 0);
		position +:= Len.P01_DT_INVIO_RICH;
		MarshalByte.writeIntAsPacked(buffer, position, p01DtPervRich, Len.Int.P01_DT_PERV_RICH, 0);
		position +:= Len.P01_DT_PERV_RICH;
		MarshalByte.writeIntAsPacked(buffer, position, p01DtRgstrzRich, Len.Int.P01_DT_RGSTRZ_RICH, 0);
		position +:= Len.P01_DT_RGSTRZ_RICH;
		p01DtEff.getP01DtEffAsBuffer(buffer, position);
		position +:= P01DtEff.Len.P01_DT_EFF;
		p01DtSin.getP01DtSinAsBuffer(buffer, position);
		position +:= P01DtSin.Len.P01_DT_SIN;
		MarshalByte.writeString(buffer, position, p01TpOgg, Len.P01_TP_OGG);
		position +:= Len.P01_TP_OGG;
		MarshalByte.writeIntAsPacked(buffer, position, p01IdOgg, Len.Int.P01_ID_OGG, 0);
		position +:= Len.P01_ID_OGG;
		MarshalByte.writeString(buffer, position, p01IbOgg, Len.P01_IB_OGG);
		position +:= Len.P01_IB_OGG;
		MarshalByte.writeChar(buffer, position, p01FlModExec);
		position +:= Types.CHAR_SIZE;
		p01IdRichiedente.getP01IdRichiedenteAsBuffer(buffer, position);
		position +:= P01IdRichiedente.Len.P01_ID_RICHIEDENTE;
		MarshalByte.writeString(buffer, position, p01CodProd, Len.P01_COD_PROD);
		position +:= Len.P01_COD_PROD;
		MarshalByte.writeChar(buffer, position, p01DsOperSql);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeIntAsPacked(buffer, position, p01DsVer, Len.Int.P01_DS_VER, 0);
		position +:= Len.P01_DS_VER;
		MarshalByte.writeLongAsPacked(buffer, position, p01DsTsCptz, Len.Int.P01_DS_TS_CPTZ, 0);
		position +:= Len.P01_DS_TS_CPTZ;
		MarshalByte.writeString(buffer, position, p01DsUtente, Len.P01_DS_UTENTE);
		position +:= Len.P01_DS_UTENTE;
		MarshalByte.writeChar(buffer, position, p01DsStatoElab);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeIntAsPacked(buffer, position, p01CodCan, Len.Int.P01_COD_CAN, 0);
		position +:= Len.P01_COD_CAN;
		MarshalByte.writeString(buffer, position, p01IbOggOrig, Len.P01_IB_OGG_ORIG);
		position +:= Len.P01_IB_OGG_ORIG;
		p01DtEstFinanz.getP01DtEstFinanzAsBuffer(buffer, position);
		position +:= P01DtEstFinanz.Len.P01_DT_EST_FINANZ;
		p01DtManCop.getP01DtManCopAsBuffer(buffer, position);
		position +:= P01DtManCop.Len.P01_DT_MAN_COP;
		MarshalByte.writeChar(buffer, position, p01FlGestProtezione);
		return buffer;
	}

	public void setP01IdRichEst(integer p01IdRichEst) {
		this.p01IdRichEst:=p01IdRichEst;
	}

	public integer getP01IdRichEst() {
		return this.p01IdRichEst;
	}

	public void setP01CodCompAnia(integer p01CodCompAnia) {
		this.p01CodCompAnia:=p01CodCompAnia;
	}

	public integer getP01CodCompAnia() {
		return this.p01CodCompAnia;
	}

	public void setP01IbRichEst(string p01IbRichEst) {
		this.p01IbRichEst:=Functions.subString(p01IbRichEst, Len.P01_IB_RICH_EST);
	}

	public string getP01IbRichEst() {
		return this.p01IbRichEst;
	}

	public void setP01TpMovi(integer p01TpMovi) {
		this.p01TpMovi:=p01TpMovi;
	}

	public integer getP01TpMovi() {
		return this.p01TpMovi;
	}

	public void setP01DtFormRich(integer p01DtFormRich) {
		this.p01DtFormRich:=p01DtFormRich;
	}

	public integer getP01DtFormRich() {
		return this.p01DtFormRich;
	}

	public void setP01DtInvioRich(integer p01DtInvioRich) {
		this.p01DtInvioRich:=p01DtInvioRich;
	}

	public integer getP01DtInvioRich() {
		return this.p01DtInvioRich;
	}

	public void setP01DtPervRich(integer p01DtPervRich) {
		this.p01DtPervRich:=p01DtPervRich;
	}

	public integer getP01DtPervRich() {
		return this.p01DtPervRich;
	}

	public void setP01DtRgstrzRich(integer p01DtRgstrzRich) {
		this.p01DtRgstrzRich:=p01DtRgstrzRich;
	}

	public integer getP01DtRgstrzRich() {
		return this.p01DtRgstrzRich;
	}

	public void setP01TpOgg(string p01TpOgg) {
		this.p01TpOgg:=Functions.subString(p01TpOgg, Len.P01_TP_OGG);
	}

	public string getP01TpOgg() {
		return this.p01TpOgg;
	}

	public void setP01IdOgg(integer p01IdOgg) {
		this.p01IdOgg:=p01IdOgg;
	}

	public integer getP01IdOgg() {
		return this.p01IdOgg;
	}

	public void setP01IbOgg(string p01IbOgg) {
		this.p01IbOgg:=Functions.subString(p01IbOgg, Len.P01_IB_OGG);
	}

	public string getP01IbOgg() {
		return this.p01IbOgg;
	}

	public void setP01FlModExec(char p01FlModExec) {
		this.p01FlModExec:=p01FlModExec;
	}

	public char getP01FlModExec() {
		return this.p01FlModExec;
	}

	public void setP01CodProd(string p01CodProd) {
		this.p01CodProd:=Functions.subString(p01CodProd, Len.P01_COD_PROD);
	}

	public string getP01CodProd() {
		return this.p01CodProd;
	}

	public void setP01DsOperSql(char p01DsOperSql) {
		this.p01DsOperSql:=p01DsOperSql;
	}

	public void setP01DsOperSqlFormatted(string p01DsOperSql) {
		setP01DsOperSql(Functions.charAt(p01DsOperSql, Types.CHAR_SIZE));
	}

	public char getP01DsOperSql() {
		return this.p01DsOperSql;
	}

	public void setP01DsVer(integer p01DsVer) {
		this.p01DsVer:=p01DsVer;
	}

	public void setP01DsVerFormatted(string p01DsVer) {
		setP01DsVer(PicParser.display(new PicParams("S9(9)V").setUsage(PicUsage.PACKED)).parseInt(p01DsVer));
	}

	public integer getP01DsVer() {
		return this.p01DsVer;
	}

	public void setP01DsTsCptz(long p01DsTsCptz) {
		this.p01DsTsCptz:=p01DsTsCptz;
	}

	public long getP01DsTsCptz() {
		return this.p01DsTsCptz;
	}

	public void setP01DsUtente(string p01DsUtente) {
		this.p01DsUtente:=Functions.subString(p01DsUtente, Len.P01_DS_UTENTE);
	}

	public string getP01DsUtente() {
		return this.p01DsUtente;
	}

	public void setP01DsStatoElab(char p01DsStatoElab) {
		this.p01DsStatoElab:=p01DsStatoElab;
	}

	public void setP01DsStatoElabFormatted(string p01DsStatoElab) {
		setP01DsStatoElab(Functions.charAt(p01DsStatoElab, Types.CHAR_SIZE));
	}

	public char getP01DsStatoElab() {
		return this.p01DsStatoElab;
	}

	public void setP01CodCan(integer p01CodCan) {
		this.p01CodCan:=p01CodCan;
	}

	public integer getP01CodCan() {
		return this.p01CodCan;
	}

	public void setP01IbOggOrig(string p01IbOggOrig) {
		this.p01IbOggOrig:=Functions.subString(p01IbOggOrig, Len.P01_IB_OGG_ORIG);
	}

	public string getP01IbOggOrig() {
		return this.p01IbOggOrig;
	}

	public void setP01FlGestProtezione(char p01FlGestProtezione) {
		this.p01FlGestProtezione:=p01FlGestProtezione;
	}

	public char getP01FlGestProtezione() {
		return this.p01FlGestProtezione;
	}

	public P01DtEff getP01DtEff() {
		return p01DtEff;
	}

	public P01DtEstFinanz getP01DtEstFinanz() {
		return p01DtEstFinanz;
	}

	public P01DtManCop getP01DtManCop() {
		return p01DtManCop;
	}

	public P01DtSin getP01DtSin() {
		return p01DtSin;
	}

	public P01IdLiq getP01IdLiq() {
		return p01IdLiq;
	}

	public P01IdRichEstCollg getP01IdRichEstCollg() {
		return p01IdRichEstCollg;
	}

	public P01IdRichiedente getP01IdRichiedente() {
		return p01IdRichiedente;
	}

	@Override
	public []byte serialize() {
		return getRichEstBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer P01_ID_RICH_EST := 5;
		public final static integer P01_COD_COMP_ANIA := 3;
		public final static integer P01_IB_RICH_EST := 40;
		public final static integer P01_TP_MOVI := 3;
		public final static integer P01_DT_FORM_RICH := 5;
		public final static integer P01_DT_INVIO_RICH := 5;
		public final static integer P01_DT_PERV_RICH := 5;
		public final static integer P01_DT_RGSTRZ_RICH := 5;
		public final static integer P01_TP_OGG := 2;
		public final static integer P01_ID_OGG := 5;
		public final static integer P01_IB_OGG := 40;
		public final static integer P01_FL_MOD_EXEC := 1;
		public final static integer P01_COD_PROD := 12;
		public final static integer P01_DS_OPER_SQL := 1;
		public final static integer P01_DS_VER := 5;
		public final static integer P01_DS_TS_CPTZ := 10;
		public final static integer P01_DS_UTENTE := 20;
		public final static integer P01_DS_STATO_ELAB := 1;
		public final static integer P01_COD_CAN := 3;
		public final static integer P01_IB_OGG_ORIG := 40;
		public final static integer P01_FL_GEST_PROTEZIONE := 1;
		public final static integer RICH_EST := P01_ID_RICH_EST + P01IdRichEstCollg.Len.P01_ID_RICH_EST_COLLG + P01IdLiq.Len.P01_ID_LIQ + P01_COD_COMP_ANIA + P01_IB_RICH_EST + P01_TP_MOVI + P01_DT_FORM_RICH + P01_DT_INVIO_RICH + P01_DT_PERV_RICH + P01_DT_RGSTRZ_RICH + P01DtEff.Len.P01_DT_EFF + P01DtSin.Len.P01_DT_SIN + P01_TP_OGG + P01_ID_OGG + P01_IB_OGG + P01_FL_MOD_EXEC + P01IdRichiedente.Len.P01_ID_RICHIEDENTE + P01_COD_PROD + P01_DS_OPER_SQL + P01_DS_VER + P01_DS_TS_CPTZ + P01_DS_UTENTE + P01_DS_STATO_ELAB + P01_COD_CAN + P01_IB_OGG_ORIG + P01DtEstFinanz.Len.P01_DT_EST_FINANZ + P01DtManCop.Len.P01_DT_MAN_COP + P01_FL_GEST_PROTEZIONE;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer P01_ID_RICH_EST := 9;
			public final static integer P01_COD_COMP_ANIA := 5;
			public final static integer P01_TP_MOVI := 5;
			public final static integer P01_DT_FORM_RICH := 8;
			public final static integer P01_DT_INVIO_RICH := 8;
			public final static integer P01_DT_PERV_RICH := 8;
			public final static integer P01_DT_RGSTRZ_RICH := 8;
			public final static integer P01_ID_OGG := 9;
			public final static integer P01_DS_VER := 9;
			public final static integer P01_DS_TS_CPTZ := 18;
			public final static integer P01_COD_CAN := 5;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//RichEstIdbsp010
package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

import com.myorg.myprj.copy.Ivvc0218Ivvs0211;
import com.myorg.myprj.copy.Lccvpol1Lvvs0002;
import com.myorg.myprj.copy.WpolDati;
import com.myorg.myprj.ws.enums.WpolStatus;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS3190<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs3190Data {

	//==== PROPERTIES ====
	/**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
	private string wkPgm := "LVVS3190";
	//Original name: WK-CALL-PGM
	private string wkCallPgm := "LDBSF510";
	//Original name: POLI
	private PoliIdbspol0 poli := new PoliIdbspol0();
	//Original name: DPOL-ELE-POLI-MAX
	private short dpolElePoliMax := DefaultValues.BIN_SHORT_VAL;
	//Original name: LCCVPOL1
	private Lccvpol1Lvvs0002 lccvpol1 := new Lccvpol1Lvvs0002();
	//Original name: IX-DCLGEN
	private short ixDclgen := DefaultValues.BIN_SHORT_VAL;
	//Original name: IVVC0218
	private Ivvc0218Ivvs0211 ivvc0218 := new Ivvc0218Ivvs0211();


	//==== METHODS ====
	public string getWkPgm() {
		return this.wkPgm;
	}

	public void setWkCallPgm(string wkCallPgm) {
		this.wkCallPgm:=Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
	}

	public string getWkCallPgm() {
		return this.wkCallPgm;
	}

	public void setDpolAreaPolizzaFormatted(string data) {
		[]byte buffer := new [Len.DPOL_AREA_POLIZZA]byte;
		MarshalByte.writeString(buffer, 1, data, Len.DPOL_AREA_POLIZZA);
		setDpolAreaPolizzaBytes(buffer, 1);
	}

	public void setDpolAreaPolizzaBytes([]byte buffer, integer offset) {
		integer position := offset;
		dpolElePoliMax := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		setDpolTabPoliBytes(buffer, position);
	}

	public void setDpolElePoliMax(short dpolElePoliMax) {
		this.dpolElePoliMax:=dpolElePoliMax;
	}

	public short getDpolElePoliMax() {
		return this.dpolElePoliMax;
	}

	public void setDpolTabPoliBytes([]byte buffer, integer offset) {
		integer position := offset;
		lccvpol1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
		position +:= Types.CHAR_SIZE;
		lccvpol1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpol1Lvvs0002.Len.Int.ID_PTF, 0));
		position +:= Lccvpol1Lvvs0002.Len.ID_PTF;
		lccvpol1.getDati().setDatiBytes(buffer, position);
	}

	public void setIxDclgen(short ixDclgen) {
		this.ixDclgen:=ixDclgen;
	}

	public short getIxDclgen() {
		return this.ixDclgen;
	}

	public Ivvc0218Ivvs0211 getIvvc0218() {
		return ivvc0218;
	}

	public Lccvpol1Lvvs0002 getLccvpol1() {
		return lccvpol1;
	}

	public PoliIdbspol0 getPoli() {
		return poli;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer DPOL_ELE_POLI_MAX := 2;
		public final static integer DPOL_TAB_POLI := WpolStatus.Len.STATUS + Lccvpol1Lvvs0002.Len.ID_PTF + WpolDati.Len.DATI;
		public final static integer DPOL_AREA_POLIZZA := DPOL_ELE_POLI_MAX + DPOL_TAB_POLI;
		public final static integer WK_CALL_PGM := 8;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Lvvs3190Data
package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

import com.myorg.myprj.copy.Idsv0501;
import com.myorg.myprj.copy.Ivvc0218Ivvs0211;
import com.myorg.myprj.copy.Ldbv4911;
import com.myorg.myprj.ws.enums.FlagFondoTrovato;
import com.myorg.myprj.ws.enums.WkIdCodLivFlag;
import com.myorg.myprj.ws.enums.WkTpValAst;
import com.myorg.myprj.ws.enums.WsMovimentoLvvs0101;
import com.myorg.myprj.ws.occurs.WgrzTabGar;
import com.myorg.myprj.ws.occurs.Wl19TabFnd;
import com.myorg.myprj.ws.occurs.WtgaTabTran;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0560<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0560Data {

	//==== PROPERTIES ====
	public final static integer DTGA_TAB_TRAN_MAXOCCURS := 20;
	public final static integer DGRZ_TAB_GAR_MAXOCCURS := 20;
	public final static integer DL19_TAB_FND_MAXOCCURS := 250;
	/**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
	private string wkPgm := "LVVS0560";
	//Original name: LDBS4910
	private string ldbs4910 := "LDBS4910";
	//Original name: LDBS1530
	private string ldbs1530 := "LDBS1530";
	//Original name: LDBS5950
	private string ldbs5950 := "LDBS5950";
	/**Original name: WK-VAL-FONDO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
	private decimal(15,3) wkValFondo := 0M;
	//Original name: WK-VAL-FONDO-2DEC
	private decimal(14,2) wkValFondo2dec := 0M;
	//Original name: WK-ID-COD-LIV-FLAG
	private WkIdCodLivFlag wkIdCodLivFlag := new WkIdCodLivFlag();
	//Original name: FLAG-FONDO-TROVATO
	private FlagFondoTrovato flagFondoTrovato := new FlagFondoTrovato();
	//Original name: WK-ID-MOVI-FINRIO
	private integer wkIdMoviFinrio := DefaultValues.INT_VAL;
	//Original name: WK-TP-VAL-AST
	private WkTpValAst wkTpValAst := new WkTpValAst();
	//Original name: WK-APPO-DATE
	private WkAppoDate wkAppoDate := new WkAppoDate();
	//Original name: WK-TEMP-COD-FND
	private string wkTempCodFnd := DefaultValues.stringVal(Len.WK_TEMP_COD_FND);
	/**Original name: WS-MOVIMENTO<br>
	 * <pre>---------------------------------------------------------------*
	 *   COPY TIPOLOGICHE
	 * ---------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
	private WsMovimentoLvvs0101 wsMovimento := new WsMovimentoLvvs0101();
	//Original name: VAL-AST
	private ValAstLdbs4910 valAst := new ValAstLdbs4910();
	//Original name: MOVI
	private MoviLdbs1530 movi := new MoviLdbs1530();
	//Original name: MOVI-FINRIO
	private MoviFinrioLdbs5950 moviFinrio := new MoviFinrioLdbs5950();
	//Original name: RICH-INVST-FND
	private RichInvstFndIdbsrif0 richInvstFnd := new RichInvstFndIdbsrif0();
	//Original name: RICH-DIS-FND
	private RichDisFnd richDisFnd := new RichDisFnd();
	//Original name: LDBV4911
	private Ldbv4911 ldbv4911 := new Ldbv4911();
	//Original name: DTGA-ELE-TGA-MAX
	private short dtgaEleTgaMax := DefaultValues.BIN_SHORT_VAL;
	//Original name: DTGA-TAB-TRAN
	private []WtgaTabTran dtgaTabTran := new [DTGA_TAB_TRAN_MAXOCCURS]WtgaTabTran;
	//Original name: DGRZ-ELE-GAR-MAX
	private short dgrzEleGarMax := DefaultValues.BIN_SHORT_VAL;
	//Original name: DGRZ-TAB-GAR
	private []WgrzTabGar dgrzTabGar := new [DGRZ_TAB_GAR_MAXOCCURS]WgrzTabGar;
	/**Original name: DL19-ELE-FND-MAX<br>
	 * <pre>----------------------------------------------------------------*
	 *    COPY 7 PER LA GESTIONE DELLE OCCURS
	 * ----------------------------------------------------------------*</pre>*/
	private short dl19EleFndMax := DefaultValues.BIN_SHORT_VAL;
	//Original name: DL19-TAB-FND
	private []Wl19TabFnd dl19TabFnd := new [DL19_TAB_FND_MAXOCCURS]Wl19TabFnd;
	//Original name: IVVC0218
	private Ivvc0218Ivvs0211 ivvc0218 := new Ivvc0218Ivvs0211();
	//Original name: IX-INDICI
	private IxIndiciLvvs0560 ixIndici := new IxIndiciLvvs0560();
	//Original name: IDSV0501
	private Idsv0501 idsv0501 := new Idsv0501();
	//Original name: INT-REGISTER1
	private short intRegister1 := DefaultValues.SHORT_VAL;

	//==== CONSTRUCTORS ====
	public Lvvs0560Data() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for int dtgaTabTranIdx in 1.. DTGA_TAB_TRAN_MAXOCCURS 
		do
			dtgaTabTran[dtgaTabTranIdx] := new WtgaTabTran();
		enddo
		for int dgrzTabGarIdx in 1.. DGRZ_TAB_GAR_MAXOCCURS 
		do
			dgrzTabGar[dgrzTabGarIdx] := new WgrzTabGar();
		enddo
		for int dl19TabFndIdx in 1.. DL19_TAB_FND_MAXOCCURS 
		do
			dl19TabFnd[dl19TabFndIdx] := new Wl19TabFnd();
		enddo
	}

	public string getWkPgm() {
		return this.wkPgm;
	}

	public string getLdbs4910() {
		return this.ldbs4910;
	}

	public string getLdbs1530() {
		return this.ldbs1530;
	}

	public string getLdbs5950() {
		return this.ldbs5950;
	}

	public void setWkValFondo(decimal(15,3) wkValFondo) {
		this.wkValFondo:=wkValFondo;
	}

	public decimal(15,3) getWkValFondo() {
		return this.wkValFondo;
	}

	public void setWkValFondo2dec(decimal(14,2) wkValFondo2dec) {
		this.wkValFondo2dec:=wkValFondo2dec;
	}

	public decimal(14,2) getWkValFondo2dec() {
		return this.wkValFondo2dec;
	}

	public void setWkIdMoviFinrio(integer wkIdMoviFinrio) {
		this.wkIdMoviFinrio:=wkIdMoviFinrio;
	}

	public integer getWkIdMoviFinrio() {
		return this.wkIdMoviFinrio;
	}

	public void setWkTempCodFnd(string wkTempCodFnd) {
		this.wkTempCodFnd:=Functions.subString(wkTempCodFnd, Len.WK_TEMP_COD_FND);
	}

	public string getWkTempCodFnd() {
		return this.wkTempCodFnd;
	}

	public void setDtgaAreaTgaFormatted(string data) {
		[]byte buffer := new [Len.DTGA_AREA_TGA]byte;
		MarshalByte.writeString(buffer, 1, data, Len.DTGA_AREA_TGA);
		setDtgaAreaTgaBytes(buffer, 1);
	}

	public void setDtgaAreaTgaBytes([]byte buffer, integer offset) {
		integer position := offset;
		dtgaEleTgaMax := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		for integer idx in 1.. DTGA_TAB_TRAN_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				dtgaTabTran[idx].setTabTranBytes(buffer, position);
				position +:= WtgaTabTran.Len.TAB_TRAN;
			else
				dtgaTabTran[idx].initTabTranSpaces();
				position +:= WtgaTabTran.Len.TAB_TRAN;
			endif
		enddo
	}

	public void setDtgaEleTgaMax(short dtgaEleTgaMax) {
		this.dtgaEleTgaMax:=dtgaEleTgaMax;
	}

	public short getDtgaEleTgaMax() {
		return this.dtgaEleTgaMax;
	}

	public void setDgrzAreaGraFormatted(string data) {
		[]byte buffer := new [Len.DGRZ_AREA_GRA]byte;
		MarshalByte.writeString(buffer, 1, data, Len.DGRZ_AREA_GRA);
		setDgrzAreaGraBytes(buffer, 1);
	}

	public void setDgrzAreaGraBytes([]byte buffer, integer offset) {
		integer position := offset;
		dgrzEleGarMax := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		for integer idx in 1.. DGRZ_TAB_GAR_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				dgrzTabGar[idx].setTabGarBytes(buffer, position);
				position +:= WgrzTabGar.Len.TAB_GAR;
			else
				dgrzTabGar[idx].initTabGarSpaces();
				position +:= WgrzTabGar.Len.TAB_GAR;
			endif
		enddo
	}

	public void setDgrzEleGarMax(short dgrzEleGarMax) {
		this.dgrzEleGarMax:=dgrzEleGarMax;
	}

	public short getDgrzEleGarMax() {
		return this.dgrzEleGarMax;
	}

	public void setDl19AreaQuotaFormatted(string data) {
		[]byte buffer := new [Len.DL19_AREA_QUOTA]byte;
		MarshalByte.writeString(buffer, 1, data, Len.DL19_AREA_QUOTA);
		setDl19AreaQuotaBytes(buffer, 1);
	}

	public void setDl19AreaQuotaBytes([]byte buffer, integer offset) {
		integer position := offset;
		dl19EleFndMax := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		setDl19TabellaBytes(buffer, position);
	}

	public void setDl19EleFndMax(short dl19EleFndMax) {
		this.dl19EleFndMax:=dl19EleFndMax;
	}

	public short getDl19EleFndMax() {
		return this.dl19EleFndMax;
	}

	public void setDl19TabellaBytes([]byte buffer, integer offset) {
		integer position := offset;
		for integer idx in 1.. DL19_TAB_FND_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				dl19TabFnd[idx].setWl19TabFndBytes(buffer, position);
				position +:= Wl19TabFnd.Len.WL19_TAB_FND;
			else
				dl19TabFnd[idx].initWl19TabFndSpaces();
				position +:= Wl19TabFnd.Len.WL19_TAB_FND;
			endif
		enddo
	}

	public void setIntRegister1(short intRegister1) {
		this.intRegister1:=intRegister1;
	}

	public short getIntRegister1() {
		return this.intRegister1;
	}

	public WgrzTabGar getDgrzTabGar(integer idx) {
		return dgrzTabGar[idx];
	}

	public Wl19TabFnd getDl19TabFnd(integer idx) {
		return dl19TabFnd[idx];
	}

	public WtgaTabTran getDtgaTabTran(integer idx) {
		return dtgaTabTran[idx];
	}

	public FlagFondoTrovato getFlagFondoTrovato() {
		return flagFondoTrovato;
	}

	public Idsv0501 getIdsv0501() {
		return idsv0501;
	}

	public Ivvc0218Ivvs0211 getIvvc0218() {
		return ivvc0218;
	}

	public IxIndiciLvvs0560 getIxIndici() {
		return ixIndici;
	}

	public Ldbv4911 getLdbv4911() {
		return ldbv4911;
	}

	public MoviLdbs1530 getMovi() {
		return movi;
	}

	public MoviFinrioLdbs5950 getMoviFinrio() {
		return moviFinrio;
	}

	public RichDisFnd getRichDisFnd() {
		return richDisFnd;
	}

	public RichInvstFndIdbsrif0 getRichInvstFnd() {
		return richInvstFnd;
	}

	public ValAstLdbs4910 getValAst() {
		return valAst;
	}

	public WkAppoDate getWkAppoDate() {
		return wkAppoDate;
	}

	public WkIdCodLivFlag getWkIdCodLivFlag() {
		return wkIdCodLivFlag;
	}

	public WkTpValAst getWkTpValAst() {
		return wkTpValAst;
	}

	public WsMovimentoLvvs0101 getWsMovimento() {
		return wsMovimento;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer WK_TEMP_COD_FND := 20;
		public final static integer DTGA_ELE_TGA_MAX := 2;
		public final static integer DTGA_AREA_TGA := DTGA_ELE_TGA_MAX + Lvvs0560Data.DTGA_TAB_TRAN_MAXOCCURS * WtgaTabTran.Len.TAB_TRAN;
		public final static integer DGRZ_ELE_GAR_MAX := 2;
		public final static integer DGRZ_AREA_GRA := DGRZ_ELE_GAR_MAX + Lvvs0560Data.DGRZ_TAB_GAR_MAXOCCURS * WgrzTabGar.Len.TAB_GAR;
		public final static integer DL19_ELE_FND_MAX := 2;
		public final static integer DL19_TABELLA := Lvvs0560Data.DL19_TAB_FND_MAXOCCURS * Wl19TabFnd.Len.WL19_TAB_FND;
		public final static integer DL19_AREA_QUOTA := DL19_ELE_FND_MAX + DL19_TABELLA;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Lvvs0560Data
package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LDBV2821<br>
 * Variable: LDBV2821 from copybook LDBV2821<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv2821 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: LDBV2821-ID-OGG
	private integer idOgg := DefaultValues.INT_VAL;
	//Original name: LDBV2821-TP-OGG
	private string tpOgg := DefaultValues.stringVal(Len.TP_OGG);
	//Original name: LDBV2821-IMP-PREST
	private decimal(15,3) impPrest := DefaultValues.DEC_VAL;


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.LDBV2821;
	}

	@Override
	public void deserialize([]byte buf) {
		setLdbv2821Bytes(buf);
	}

	public string getLdbv2821Formatted() {
		return MarshalByteExt.bufferToStr(getLdbv2821Bytes());
	}

	public void setLdbv2821Bytes([]byte buffer) {
		setLdbv2821Bytes(buffer, 1);
	}

	public []byte getLdbv2821Bytes() {
		[]byte buffer := new [Len.LDBV2821]byte;
		return getLdbv2821Bytes(buffer, 1);
	}

	public void setLdbv2821Bytes([]byte buffer, integer offset) {
		integer position := offset;
		idOgg := MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
		position +:= Len.ID_OGG;
		tpOgg := MarshalByte.readString(buffer, position, Len.TP_OGG);
		position +:= Len.TP_OGG;
		impPrest := MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_PREST, Len.Fract.IMP_PREST);
	}

	public []byte getLdbv2821Bytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
		position +:= Len.ID_OGG;
		MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
		position +:= Len.TP_OGG;
		MarshalByte.writeDecimalAsPacked(buffer, position, impPrest);
		return buffer;
	}

	public void setIdOgg(integer idOgg) {
		this.idOgg:=idOgg;
	}

	public integer getIdOgg() {
		return this.idOgg;
	}

	public void setTpOgg(string tpOgg) {
		this.tpOgg:=Functions.subString(tpOgg, Len.TP_OGG);
	}

	public string getTpOgg() {
		return this.tpOgg;
	}

	public void setImpPrest(decimal(15,3) impPrest) {
		this.impPrest:=impPrest;
	}

	public decimal(15,3) getImpPrest() {
		return this.impPrest;
	}

	@Override
	public []byte serialize() {
		return getLdbv2821Bytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ID_OGG := 5;
		public final static integer TP_OGG := 2;
		public final static integer IMP_PREST := 8;
		public final static integer LDBV2821 := ID_OGG + TP_OGG + IMP_PREST;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer ID_OGG := 9;
			public final static integer IMP_PREST := 12;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
		public static class Fract {

			//==== PROPERTIES ====
			public final static integer IMP_PREST := 3;

			//==== CONSTRUCTORS ====
			private Fract() {			}

		}
	}
}//Ldbv2821
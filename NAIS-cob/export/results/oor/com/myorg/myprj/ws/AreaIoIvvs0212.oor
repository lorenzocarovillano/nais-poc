package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.core.SerializableParameter;
import com.myorg.myprj.ws.occurs.Ivvv0212CtrlAutOper;

/**Original name: AREA-IO-IVVS0212<br>
 * Variable: AREA-IO-IVVS0212 from program IVVS0212<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaIoIvvs0212 extends SerializableParameter {

	//==== PROPERTIES ====
	public final static integer CTRL_AUT_OPER_MAXOCCURS := 20;
	/**Original name: IVVV0212-KEY-AUT-OPER1<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA INPUT VARIABILI
	 *    PORTAFOGLIO VITA - PROCESSO VENDITA
	 *    LUNG.
	 * ----------------------------------------------------------------*
	 *  03 AREA-INPUT.</pre>*/
	private string keyAutOper1 := DefaultValues.stringVal(Len.KEY_AUT_OPER1);
	//Original name: IVVV0212-KEY-AUT-OPER2
	private string keyAutOper2 := DefaultValues.stringVal(Len.KEY_AUT_OPER2);
	//Original name: IVVV0212-KEY-AUT-OPER3
	private string keyAutOper3 := DefaultValues.stringVal(Len.KEY_AUT_OPER3);
	//Original name: IVVV0212-KEY-AUT-OPER4
	private string keyAutOper4 := DefaultValues.stringVal(Len.KEY_AUT_OPER4);
	//Original name: IVVV0212-KEY-AUT-OPER5
	private string keyAutOper5 := DefaultValues.stringVal(Len.KEY_AUT_OPER5);
	/**Original name: IVVV0212-ELE-CTRL-AUT-OPER-MAX<br>
	 * <pre>    AREA VARIABILI AUTONOMIA OPERATIVA
	 *  03 AREA-OUTPUT.</pre>*/
	private short eleCtrlAutOperMax := DefaultValues.SHORT_VAL;
	//Original name: IVVV0212-CTRL-AUT-OPER
	private []Ivvv0212CtrlAutOper ctrlAutOper := new [CTRL_AUT_OPER_MAXOCCURS]Ivvv0212CtrlAutOper;

	//==== CONSTRUCTORS ====
	public AreaIoIvvs0212() {
		init();
	}

	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.AREA_IO_IVVS0212;
	}

	@Override
	public void deserialize([]byte buf) {
		setAreaIoIvvs0212Bytes(buf);
	}

	public void init() {
		for int ctrlAutOperIdx in 1.. CTRL_AUT_OPER_MAXOCCURS 
		do
			ctrlAutOper[ctrlAutOperIdx] := new Ivvv0212CtrlAutOper();
		enddo
	}

	public string getAreaIvvv0212Formatted() {
		return MarshalByteExt.bufferToStr(getAreaIoIvvs0212Bytes());
	}

	public void setAreaIoIvvs0212Bytes([]byte buffer) {
		setAreaIoIvvs0212Bytes(buffer, 1);
	}

	public []byte getAreaIoIvvs0212Bytes() {
		[]byte buffer := new [Len.AREA_IO_IVVS0212]byte;
		return getAreaIoIvvs0212Bytes(buffer, 1);
	}

	public void setAreaIoIvvs0212Bytes([]byte buffer, integer offset) {
		integer position := offset;
		keyAutOper1 := MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER1);
		position +:= Len.KEY_AUT_OPER1;
		keyAutOper2 := MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER2);
		position +:= Len.KEY_AUT_OPER2;
		keyAutOper3 := MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER3);
		position +:= Len.KEY_AUT_OPER3;
		keyAutOper4 := MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER4);
		position +:= Len.KEY_AUT_OPER4;
		keyAutOper5 := MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER5);
		position +:= Len.KEY_AUT_OPER5;
		eleCtrlAutOperMax := MarshalByte.readPackedAsShort(buffer, position, Len.Int.ELE_CTRL_AUT_OPER_MAX, 0);
		position +:= Len.ELE_CTRL_AUT_OPER_MAX;
		for integer idx in 1.. CTRL_AUT_OPER_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				ctrlAutOper[idx].setCtrlAutOperBytes(buffer, position);
				position +:= Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;
			else
				ctrlAutOper[idx].initCtrlAutOperSpaces();
				position +:= Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;
			endif
		enddo
	}

	public []byte getAreaIoIvvs0212Bytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, keyAutOper1, Len.KEY_AUT_OPER1);
		position +:= Len.KEY_AUT_OPER1;
		MarshalByte.writeString(buffer, position, keyAutOper2, Len.KEY_AUT_OPER2);
		position +:= Len.KEY_AUT_OPER2;
		MarshalByte.writeString(buffer, position, keyAutOper3, Len.KEY_AUT_OPER3);
		position +:= Len.KEY_AUT_OPER3;
		MarshalByte.writeString(buffer, position, keyAutOper4, Len.KEY_AUT_OPER4);
		position +:= Len.KEY_AUT_OPER4;
		MarshalByte.writeString(buffer, position, keyAutOper5, Len.KEY_AUT_OPER5);
		position +:= Len.KEY_AUT_OPER5;
		MarshalByte.writeShortAsPacked(buffer, position, eleCtrlAutOperMax, Len.Int.ELE_CTRL_AUT_OPER_MAX, 0);
		position +:= Len.ELE_CTRL_AUT_OPER_MAX;
		for integer idx in 1.. CTRL_AUT_OPER_MAXOCCURS 
		do
			ctrlAutOper[idx].getCtrlAutOperBytes(buffer, position);
			position +:= Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;
		enddo
		return buffer;
	}

	public void setKeyAutOper1(string keyAutOper1) {
		this.keyAutOper1:=Functions.subString(keyAutOper1, Len.KEY_AUT_OPER1);
	}

	public string getKeyAutOper1() {
		return this.keyAutOper1;
	}

	public void setKeyAutOper2(string keyAutOper2) {
		this.keyAutOper2:=Functions.subString(keyAutOper2, Len.KEY_AUT_OPER2);
	}

	public string getKeyAutOper2() {
		return this.keyAutOper2;
	}

	public void setKeyAutOper3(string keyAutOper3) {
		this.keyAutOper3:=Functions.subString(keyAutOper3, Len.KEY_AUT_OPER3);
	}

	public string getKeyAutOper3() {
		return this.keyAutOper3;
	}

	public void setKeyAutOper4(string keyAutOper4) {
		this.keyAutOper4:=Functions.subString(keyAutOper4, Len.KEY_AUT_OPER4);
	}

	public string getKeyAutOper4() {
		return this.keyAutOper4;
	}

	public void setKeyAutOper5(string keyAutOper5) {
		this.keyAutOper5:=Functions.subString(keyAutOper5, Len.KEY_AUT_OPER5);
	}

	public string getKeyAutOper5() {
		return this.keyAutOper5;
	}

	public void setEleCtrlAutOperMax(short eleCtrlAutOperMax) {
		this.eleCtrlAutOperMax:=eleCtrlAutOperMax;
	}

	public short getEleCtrlAutOperMax() {
		return this.eleCtrlAutOperMax;
	}

	public Ivvv0212CtrlAutOper getCtrlAutOper(integer idx) {
		return ctrlAutOper[idx];
	}

	@Override
	public []byte serialize() {
		return getAreaIoIvvs0212Bytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer KEY_AUT_OPER1 := 20;
		public final static integer KEY_AUT_OPER2 := 20;
		public final static integer KEY_AUT_OPER3 := 20;
		public final static integer KEY_AUT_OPER4 := 20;
		public final static integer KEY_AUT_OPER5 := 20;
		public final static integer ELE_CTRL_AUT_OPER_MAX := 3;
		public final static integer AREA_IO_IVVS0212 := KEY_AUT_OPER1 + KEY_AUT_OPER2 + KEY_AUT_OPER3 + KEY_AUT_OPER4 + KEY_AUT_OPER5 + ELE_CTRL_AUT_OPER_MAX + AreaIoIvvs0212.CTRL_AUT_OPER_MAXOCCURS * Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer ELE_CTRL_AUT_OPER_MAX := 4;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//AreaIoIvvs0212
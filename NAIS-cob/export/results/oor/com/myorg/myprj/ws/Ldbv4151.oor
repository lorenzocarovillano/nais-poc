package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;

import com.modernsystems.ctu.core.SerializableParameter;
import com.myorg.myprj.copy.Ldbv4151DatiInput;

/**Original name: LDBV4151<br>
 * Variable: LDBV4151 from copybook LDBV4151<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv4151 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: LDBV4151-DATI-INPUT
	private Ldbv4151DatiInput datiInput := new Ldbv4151DatiInput();
	//Original name: LDBV4151-PRE-TOT
	private decimal(15,3) preTot := DefaultValues.DEC_VAL;


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.LDBV4151;
	}

	@Override
	public void deserialize([]byte buf) {
		setLdbv4151Bytes(buf);
	}

	public void setLdbv4151Bytes([]byte buffer) {
		setLdbv4151Bytes(buffer, 1);
	}

	public []byte getLdbv4151Bytes() {
		[]byte buffer := new [Len.LDBV4151]byte;
		return getLdbv4151Bytes(buffer, 1);
	}

	public void setLdbv4151Bytes([]byte buffer, integer offset) {
		integer position := offset;
		datiInput.setDatiInputBytes(buffer, position);
		position +:= Ldbv4151DatiInput.Len.DATI_INPUT;
		setDatiOutputBytes(buffer, position);
	}

	public []byte getLdbv4151Bytes([]byte buffer, integer offset) {
		integer position := offset;
		datiInput.getDatiInputBytes(buffer, position);
		position +:= Ldbv4151DatiInput.Len.DATI_INPUT;
		getDatiOutputBytes(buffer, position);
		return buffer;
	}

	public void setDatiOutputBytes([]byte buffer, integer offset) {
		integer position := offset;
		preTot := MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRE_TOT, Len.Fract.PRE_TOT);
	}

	public []byte getDatiOutputBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeDecimalAsPacked(buffer, position, preTot);
		return buffer;
	}

	public void setPreTot(decimal(15,3) preTot) {
		this.preTot:=preTot;
	}

	public decimal(15,3) getPreTot() {
		return this.preTot;
	}

	public Ldbv4151DatiInput getDatiInput() {
		return datiInput;
	}

	@Override
	public []byte serialize() {
		return getLdbv4151Bytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer PRE_TOT := 8;
		public final static integer DATI_OUTPUT := PRE_TOT;
		public final static integer LDBV4151 := Ldbv4151DatiInput.Len.DATI_INPUT + DATI_OUTPUT;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer PRE_TOT := 12;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
		public static class Fract {

			//==== PROPERTIES ====
			public final static integer PRE_TOT := 3;

			//==== CONSTRUCTORS ====
			private Fract() {			}

		}
	}
}//Ldbv4151
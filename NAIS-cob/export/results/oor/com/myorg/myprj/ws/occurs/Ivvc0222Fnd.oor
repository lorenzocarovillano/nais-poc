package com.myorg.myprj.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.lang.ICopyable;

/**Original name: IVVC0222-FND<br>
 * Variables: IVVC0222-FND from copybook IVVC0222<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ivvc0222Fnd implements <Ivvc0222Fnd>ICopyable {

	//==== PROPERTIES ====
	//Original name: IVVC0222-COD-FND
	private string codFnd := DefaultValues.stringVal(Len.COD_FND);
	//Original name: IVVC0222-CTRV-FND
	private decimal(15,3) ctrvFnd := DefaultValues.DEC_VAL;

	//==== CONSTRUCTORS ====
	public Ivvc0222Fnd() {	}
	public Ivvc0222Fnd(Ivvc0222Fnd fnd) {
		this();
		this.codFnd := fnd.codFnd;
		this.ctrvFnd := fnd.ctrvFnd;
	}

	//==== METHODS ====
	public void setFndBytes([]byte buffer, integer offset) {
		integer position := offset;
		codFnd := MarshalByte.readString(buffer, position, Len.COD_FND);
		position +:= Len.COD_FND;
		ctrvFnd := MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.CTRV_FND, Len.Fract.CTRV_FND);
	}

	public []byte getFndBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, codFnd, Len.COD_FND);
		position +:= Len.COD_FND;
		MarshalByte.writeDecimalAsPacked(buffer, position, ctrvFnd);
		return buffer;
	}

	public void initFndSpaces() {
		codFnd := "";
		ctrvFnd := OOR_NaN;
	}

	public void setCodFnd(string codFnd) {
		this.codFnd:=Functions.subString(codFnd, Len.COD_FND);
	}

	public string getCodFnd() {
		return this.codFnd;
	}

	public void setCtrvFnd(decimal(15,3) ctrvFnd) {
		this.ctrvFnd:=ctrvFnd;
	}

	public decimal(15,3) getCtrvFnd() {
		return this.ctrvFnd;
	}

	public Ivvc0222Fnd copy() {
		return new Ivvc0222Fnd(this);
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer COD_FND := 20;
		public final static integer CTRV_FND := 8;
		public final static integer FND := COD_FND + CTRV_FND;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer CTRV_FND := 12;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
		public static class Fract {

			//==== PROPERTIES ====
			public final static integer CTRV_FND := 3;

			//==== CONSTRUCTORS ====
			private Fract() {			}

		}
	}
}//Ivvc0222Fnd
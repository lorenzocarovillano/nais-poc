package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

import com.myorg.myprj.ws.enums.Area0140Operazione;

/**Original name: AREA-X-IABS0140<br>
 * Variable: AREA-X-IABS0140 from program IABS0140<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AreaXIabs0140 {

	//==== PROPERTIES ====
	//Original name: AREA0140-DATABASE-NAME
	private string databaseName := DefaultValues.stringVal(Len.DATABASE_NAME);
	//Original name: AREA0140-USER
	private string user := DefaultValues.stringVal(Len.USER);
	//Original name: AREA0140-PASSWORD
	private string password := DefaultValues.stringVal(Len.PASSWORD);
	/**Original name: AREA0140-OPERAZIONE<br>
	 * <pre> -- CAMPI OPERAZIONE</pre>*/
	private Area0140Operazione operazione := new Area0140Operazione();


	//==== METHODS ====
	public void setAreaXIabs0140Formatted(string data) {
		[]byte buffer := new [Len.AREA_X_IABS0140]byte;
		MarshalByte.writeString(buffer, 1, data, Len.AREA_X_IABS0140);
		setAreaXIabs0140Bytes(buffer, 1);
	}

	public string getAreaXIabs0140Formatted() {
		return MarshalByteExt.bufferToStr(getAreaXIabs0140Bytes());
	}

	public []byte getAreaXIabs0140Bytes() {
		[]byte buffer := new [Len.AREA_X_IABS0140]byte;
		return getAreaXIabs0140Bytes(buffer, 1);
	}

	public void setAreaXIabs0140Bytes([]byte buffer, integer offset) {
		integer position := offset;
		databaseName := MarshalByte.readString(buffer, position, Len.DATABASE_NAME);
		position +:= Len.DATABASE_NAME;
		user := MarshalByte.readString(buffer, position, Len.USER);
		position +:= Len.USER;
		password := MarshalByte.readString(buffer, position, Len.PASSWORD);
		position +:= Len.PASSWORD;
		operazione.setOperazione(MarshalByte.readString(buffer, position, Area0140Operazione.Len.OPERAZIONE));
	}

	public []byte getAreaXIabs0140Bytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, databaseName, Len.DATABASE_NAME);
		position +:= Len.DATABASE_NAME;
		MarshalByte.writeString(buffer, position, user, Len.USER);
		position +:= Len.USER;
		MarshalByte.writeString(buffer, position, password, Len.PASSWORD);
		position +:= Len.PASSWORD;
		MarshalByte.writeString(buffer, position, operazione.getOperazione(), Area0140Operazione.Len.OPERAZIONE);
		return buffer;
	}

	public void setDatabaseName(string databaseName) {
		this.databaseName:=Functions.subString(databaseName, Len.DATABASE_NAME);
	}

	public string getDatabaseName() {
		return this.databaseName;
	}

	public string getDatabaseNameFormatted() {
		return Functions.padBlanks(getDatabaseName(), Len.DATABASE_NAME);
	}

	public void setUser(string user) {
		this.user:=Functions.subString(user, Len.USER);
	}

	public string getUser() {
		return this.user;
	}

	public string getUserFormatted() {
		return Functions.padBlanks(getUser(), Len.USER);
	}

	public void setPassword(string password) {
		this.password:=Functions.subString(password, Len.PASSWORD);
	}

	public string getPassword() {
		return this.password;
	}

	public string getPasswordFormatted() {
		return Functions.padBlanks(getPassword(), Len.PASSWORD);
	}

	public Area0140Operazione getOperazione() {
		return operazione;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer DATABASE_NAME := 10;
		public final static integer USER := 8;
		public final static integer PASSWORD := 8;
		public final static integer AREA_X_IABS0140 := DATABASE_NAME + USER + PASSWORD + Area0140Operazione.Len.OPERAZIONE;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//AreaXIabs0140
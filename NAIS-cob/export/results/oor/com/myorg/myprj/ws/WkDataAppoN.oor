package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-DATA-APPO-N<br>
 * Variable: WK-DATA-APPO-N from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WkDataAppoN {

	//==== PROPERTIES ====
	//Original name: WK-DATA-APPO-N-AA
	private string appoNAa := DefaultValues.stringVal(Len.APPO_N_AA);
	//Original name: WK-DATA-APPO-N-MM
	private string appoNMm := DefaultValues.stringVal(Len.APPO_N_MM);
	//Original name: WK-DATA-APPO-N-GG
	private string appoNGg := DefaultValues.stringVal(Len.APPO_N_GG);


	//==== METHODS ====
	public void setWkDataAppo(integer wkDataAppo) {
		integer position := 1;
		[]byte buffer := getWkDataAppoNBytes();
		MarshalByte.writeInt(buffer, position, wkDataAppo, Len.Int.WK_DATA_APPO, SignType.NO_SIGN);
		setWkDataAppoNBytes(buffer);
	}

	/**Original name: WK-DATA-APPO<br>*/
	public integer getWkDataAppo() {
		integer position := 1;
		return MarshalByte.readInt(getWkDataAppoNBytes(), position, Len.Int.WK_DATA_APPO, SignType.NO_SIGN);
	}

	public string getWkDataAppoFormatted() {
		integer position := 1;
		return MarshalByte.readFixedString(getWkDataAppoNBytes(), position, Len.WK_DATA_APPO);
	}

	public void setWkDataAppoNFormatted(string data) {
		[]byte buffer := new [Len.WK_DATA_APPO_N]byte;
		MarshalByte.writeString(buffer, 1, data, Len.WK_DATA_APPO_N);
		setWkDataAppoNBytes(buffer, 1);
	}

	public void setWkDataAppoNBytes([]byte buffer) {
		setWkDataAppoNBytes(buffer, 1);
	}

	/**Original name: WK-DATA-APPO-N<br>*/
	public []byte getWkDataAppoNBytes() {
		[]byte buffer := new [Len.WK_DATA_APPO_N]byte;
		return getWkDataAppoNBytes(buffer, 1);
	}

	public void setWkDataAppoNBytes([]byte buffer, integer offset) {
		integer position := offset;
		appoNAa := MarshalByte.readString(buffer, position, Len.APPO_N_AA);
		position +:= Len.APPO_N_AA;
		appoNMm := MarshalByte.readString(buffer, position, Len.APPO_N_MM);
		position +:= Len.APPO_N_MM;
		appoNGg := MarshalByte.readString(buffer, position, Len.APPO_N_GG);
	}

	public []byte getWkDataAppoNBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, appoNAa, Len.APPO_N_AA);
		position +:= Len.APPO_N_AA;
		MarshalByte.writeString(buffer, position, appoNMm, Len.APPO_N_MM);
		position +:= Len.APPO_N_MM;
		MarshalByte.writeString(buffer, position, appoNGg, Len.APPO_N_GG);
		return buffer;
	}

	public void setAppoNAa(string appoNAa) {
		this.appoNAa:=Functions.subString(appoNAa, Len.APPO_N_AA);
	}

	public string getAppoNAa() {
		return this.appoNAa;
	}

	public string getAppoNAaFormatted() {
		return Functions.padBlanks(getAppoNAa(), Len.APPO_N_AA);
	}

	public void setAppoNMm(string appoNMm) {
		this.appoNMm:=Functions.subString(appoNMm, Len.APPO_N_MM);
	}

	public string getAppoNMm() {
		return this.appoNMm;
	}

	public void setAppoNGg(string appoNGg) {
		this.appoNGg:=Functions.subString(appoNGg, Len.APPO_N_GG);
	}

	public string getAppoNGg() {
		return this.appoNGg;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer APPO_N_AA := 4;
		public final static integer APPO_N_MM := 2;
		public final static integer APPO_N_GG := 2;
		public final static integer WK_DATA_APPO_N := APPO_N_AA + APPO_N_MM + APPO_N_GG;
		public final static integer WK_DATA_APPO := 8;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer WK_DATA_APPO := 8;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//WkDataAppoN
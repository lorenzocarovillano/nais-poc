package com.myorg.myprj.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: IVVC0223-TAB-LIVELLI-GAR<br>
 * Variables: IVVC0223-TAB-LIVELLI-GAR from copybook IVVC0223<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ivvc0223TabLivelliGar {

	//==== PROPERTIES ====
	public final static integer TAB_VAR_GAR_MAXOCCURS := 30;
	//Original name: IVVC0223-ID-GARANZIA
	private string idGaranzia := DefaultValues.stringVal(Len.ID_GARANZIA);
	//Original name: IVVC0223-COD-TARI
	private string codTari := DefaultValues.stringVal(Len.COD_TARI);
	//Original name: IVVC0223-ELE-MAX-VAR-GAR
	private short eleMaxVarGar := DefaultValues.BIN_SHORT_VAL;
	//Original name: IVVC0223-TAB-VAR-GAR
	private []Ivvc0223TabVarGar tabVarGar := new [TAB_VAR_GAR_MAXOCCURS]Ivvc0223TabVarGar;

	//==== CONSTRUCTORS ====
	public Ivvc0223TabLivelliGar() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for int tabVarGarIdx in 1.. TAB_VAR_GAR_MAXOCCURS 
		do
			tabVarGar[tabVarGarIdx] := new Ivvc0223TabVarGar();
		enddo
	}

	public void setTabLivelliGarBytes([]byte buffer, integer offset) {
		integer position := offset;
		idGaranzia := MarshalByte.readFixedString(buffer, position, Len.ID_GARANZIA);
		position +:= Len.ID_GARANZIA;
		codTari := MarshalByte.readString(buffer, position, Len.COD_TARI);
		position +:= Len.COD_TARI;
		eleMaxVarGar := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		for integer idx in 1.. TAB_VAR_GAR_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				tabVarGar[idx].setTabVarGarBytes(buffer, position);
				position +:= Ivvc0223TabVarGar.Len.TAB_VAR_GAR;
			else
				tabVarGar[idx].initTabVarGarSpaces();
				position +:= Ivvc0223TabVarGar.Len.TAB_VAR_GAR;
			endif
		enddo
	}

	public []byte getTabLivelliGarBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, idGaranzia, Len.ID_GARANZIA);
		position +:= Len.ID_GARANZIA;
		MarshalByte.writeString(buffer, position, codTari, Len.COD_TARI);
		position +:= Len.COD_TARI;
		MarshalByte.writeBinaryShort(buffer, position, eleMaxVarGar);
		position +:= Types.SHORT_SIZE;
		for integer idx in 1.. TAB_VAR_GAR_MAXOCCURS 
		do
			tabVarGar[idx].getTabVarGarBytes(buffer, position);
			position +:= Ivvc0223TabVarGar.Len.TAB_VAR_GAR;
		enddo
		return buffer;
	}

	public void initTabLivelliGarSpaces() {
		idGaranzia := "";
		codTari := "";
		eleMaxVarGar := Types.INVALID_BINARY_SHORT_VAL;
		for integer idx in 1.. TAB_VAR_GAR_MAXOCCURS 
		do
			tabVarGar[idx].initTabVarGarSpaces();
		enddo
	}

	public void setIdGaranziaFormatted(string idGaranzia) {
		this.idGaranzia:=Trunc.toUnsignedNumeric(idGaranzia, Len.ID_GARANZIA);
	}

	public integer getIdGaranzia() {
		return NumericDisplay.asInt(this.idGaranzia);
	}

	public void setCodTari(string codTari) {
		this.codTari:=Functions.subString(codTari, Len.COD_TARI);
	}

	public string getCodTari() {
		return this.codTari;
	}

	public void setEleMaxVarGar(short eleMaxVarGar) {
		this.eleMaxVarGar:=eleMaxVarGar;
	}

	public short getEleMaxVarGar() {
		return this.eleMaxVarGar;
	}

	public Ivvc0223TabVarGar getTabVarGar(integer idx) {
		return tabVarGar[idx];
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ID_GARANZIA := 9;
		public final static integer COD_TARI := 12;
		public final static integer ELE_MAX_VAR_GAR := 2;
		public final static integer TAB_LIVELLI_GAR := ID_GARANZIA + COD_TARI + ELE_MAX_VAR_GAR + Ivvc0223TabLivelliGar.TAB_VAR_GAR_MAXOCCURS * Ivvc0223TabVarGar.Len.TAB_VAR_GAR;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Ivvc0223TabLivelliGar
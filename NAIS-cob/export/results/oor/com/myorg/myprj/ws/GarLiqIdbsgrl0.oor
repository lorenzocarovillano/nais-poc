package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.core.SerializableParameter;
import com.myorg.myprj.ws.redefines.GrlDtSin1oAssto;
import com.myorg.myprj.ws.redefines.GrlDtSin2oAssto;
import com.myorg.myprj.ws.redefines.GrlDtSin3oAssto;
import com.myorg.myprj.ws.redefines.GrlIdMoviChiu;

/**Original name: GAR-LIQ<br>
 * Variable: GAR-LIQ from copybook IDBVGRL1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class GarLiqIdbsgrl0 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: GRL-ID-GAR-LIQ
	private integer grlIdGarLiq := DefaultValues.INT_VAL;
	//Original name: GRL-ID-LIQ
	private integer grlIdLiq := DefaultValues.INT_VAL;
	//Original name: GRL-ID-GAR
	private integer grlIdGar := DefaultValues.INT_VAL;
	//Original name: GRL-ID-MOVI-CRZ
	private integer grlIdMoviCrz := DefaultValues.INT_VAL;
	//Original name: GRL-ID-MOVI-CHIU
	private GrlIdMoviChiu grlIdMoviChiu := new GrlIdMoviChiu();
	//Original name: GRL-DT-INI-EFF
	private integer grlDtIniEff := DefaultValues.INT_VAL;
	//Original name: GRL-DT-END-EFF
	private integer grlDtEndEff := DefaultValues.INT_VAL;
	//Original name: GRL-COD-COMP-ANIA
	private integer grlCodCompAnia := DefaultValues.INT_VAL;
	//Original name: GRL-DT-SIN-1O-ASSTO
	private GrlDtSin1oAssto grlDtSin1oAssto := new GrlDtSin1oAssto();
	//Original name: GRL-CAU-SIN-1O-ASSTO
	private string grlCauSin1oAssto := DefaultValues.stringVal(Len.GRL_CAU_SIN1O_ASSTO);
	//Original name: GRL-TP-SIN-1O-ASSTO
	private string grlTpSin1oAssto := DefaultValues.stringVal(Len.GRL_TP_SIN1O_ASSTO);
	//Original name: GRL-DT-SIN-2O-ASSTO
	private GrlDtSin2oAssto grlDtSin2oAssto := new GrlDtSin2oAssto();
	//Original name: GRL-CAU-SIN-2O-ASSTO
	private string grlCauSin2oAssto := DefaultValues.stringVal(Len.GRL_CAU_SIN2O_ASSTO);
	//Original name: GRL-TP-SIN-2O-ASSTO
	private string grlTpSin2oAssto := DefaultValues.stringVal(Len.GRL_TP_SIN2O_ASSTO);
	//Original name: GRL-DT-SIN-3O-ASSTO
	private GrlDtSin3oAssto grlDtSin3oAssto := new GrlDtSin3oAssto();
	//Original name: GRL-CAU-SIN-3O-ASSTO
	private string grlCauSin3oAssto := DefaultValues.stringVal(Len.GRL_CAU_SIN3O_ASSTO);
	//Original name: GRL-TP-SIN-3O-ASSTO
	private string grlTpSin3oAssto := DefaultValues.stringVal(Len.GRL_TP_SIN3O_ASSTO);
	//Original name: GRL-DS-RIGA
	private long grlDsRiga := DefaultValues.LONG_VAL;
	//Original name: GRL-DS-OPER-SQL
	private char grlDsOperSql := DefaultValues.CHAR_VAL;
	//Original name: GRL-DS-VER
	private integer grlDsVer := DefaultValues.INT_VAL;
	//Original name: GRL-DS-TS-INI-CPTZ
	private long grlDsTsIniCptz := DefaultValues.LONG_VAL;
	//Original name: GRL-DS-TS-END-CPTZ
	private long grlDsTsEndCptz := DefaultValues.LONG_VAL;
	//Original name: GRL-DS-UTENTE
	private string grlDsUtente := DefaultValues.stringVal(Len.GRL_DS_UTENTE);
	//Original name: GRL-DS-STATO-ELAB
	private char grlDsStatoElab := DefaultValues.CHAR_VAL;


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.GAR_LIQ;
	}

	@Override
	public void deserialize([]byte buf) {
		setGarLiqBytes(buf);
	}

	public void setGarLiqFormatted(string data) {
		[]byte buffer := new [Len.GAR_LIQ]byte;
		MarshalByte.writeString(buffer, 1, data, Len.GAR_LIQ);
		setGarLiqBytes(buffer, 1);
	}

	public string getGarLiqFormatted() {
		return MarshalByteExt.bufferToStr(getGarLiqBytes());
	}

	public void setGarLiqBytes([]byte buffer) {
		setGarLiqBytes(buffer, 1);
	}

	public []byte getGarLiqBytes() {
		[]byte buffer := new [Len.GAR_LIQ]byte;
		return getGarLiqBytes(buffer, 1);
	}

	public void setGarLiqBytes([]byte buffer, integer offset) {
		integer position := offset;
		grlIdGarLiq := MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRL_ID_GAR_LIQ, 0);
		position +:= Len.GRL_ID_GAR_LIQ;
		grlIdLiq := MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRL_ID_LIQ, 0);
		position +:= Len.GRL_ID_LIQ;
		grlIdGar := MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRL_ID_GAR, 0);
		position +:= Len.GRL_ID_GAR;
		grlIdMoviCrz := MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRL_ID_MOVI_CRZ, 0);
		position +:= Len.GRL_ID_MOVI_CRZ;
		grlIdMoviChiu.setGrlIdMoviChiuFromBuffer(buffer, position);
		position +:= GrlIdMoviChiu.Len.GRL_ID_MOVI_CHIU;
		grlDtIniEff := MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRL_DT_INI_EFF, 0);
		position +:= Len.GRL_DT_INI_EFF;
		grlDtEndEff := MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRL_DT_END_EFF, 0);
		position +:= Len.GRL_DT_END_EFF;
		grlCodCompAnia := MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRL_COD_COMP_ANIA, 0);
		position +:= Len.GRL_COD_COMP_ANIA;
		grlDtSin1oAssto.setGrlDtSin1oAsstoFromBuffer(buffer, position);
		position +:= GrlDtSin1oAssto.Len.GRL_DT_SIN1O_ASSTO;
		grlCauSin1oAssto := MarshalByte.readString(buffer, position, Len.GRL_CAU_SIN1O_ASSTO);
		position +:= Len.GRL_CAU_SIN1O_ASSTO;
		grlTpSin1oAssto := MarshalByte.readString(buffer, position, Len.GRL_TP_SIN1O_ASSTO);
		position +:= Len.GRL_TP_SIN1O_ASSTO;
		grlDtSin2oAssto.setGrlDtSin2oAsstoFromBuffer(buffer, position);
		position +:= GrlDtSin2oAssto.Len.GRL_DT_SIN2O_ASSTO;
		grlCauSin2oAssto := MarshalByte.readString(buffer, position, Len.GRL_CAU_SIN2O_ASSTO);
		position +:= Len.GRL_CAU_SIN2O_ASSTO;
		grlTpSin2oAssto := MarshalByte.readString(buffer, position, Len.GRL_TP_SIN2O_ASSTO);
		position +:= Len.GRL_TP_SIN2O_ASSTO;
		grlDtSin3oAssto.setGrlDtSin3oAsstoFromBuffer(buffer, position);
		position +:= GrlDtSin3oAssto.Len.GRL_DT_SIN3O_ASSTO;
		grlCauSin3oAssto := MarshalByte.readString(buffer, position, Len.GRL_CAU_SIN3O_ASSTO);
		position +:= Len.GRL_CAU_SIN3O_ASSTO;
		grlTpSin3oAssto := MarshalByte.readString(buffer, position, Len.GRL_TP_SIN3O_ASSTO);
		position +:= Len.GRL_TP_SIN3O_ASSTO;
		grlDsRiga := MarshalByte.readPackedAsLong(buffer, position, Len.Int.GRL_DS_RIGA, 0);
		position +:= Len.GRL_DS_RIGA;
		grlDsOperSql := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		grlDsVer := MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRL_DS_VER, 0);
		position +:= Len.GRL_DS_VER;
		grlDsTsIniCptz := MarshalByte.readPackedAsLong(buffer, position, Len.Int.GRL_DS_TS_INI_CPTZ, 0);
		position +:= Len.GRL_DS_TS_INI_CPTZ;
		grlDsTsEndCptz := MarshalByte.readPackedAsLong(buffer, position, Len.Int.GRL_DS_TS_END_CPTZ, 0);
		position +:= Len.GRL_DS_TS_END_CPTZ;
		grlDsUtente := MarshalByte.readString(buffer, position, Len.GRL_DS_UTENTE);
		position +:= Len.GRL_DS_UTENTE;
		grlDsStatoElab := MarshalByte.readChar(buffer, position);
	}

	public []byte getGarLiqBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeIntAsPacked(buffer, position, grlIdGarLiq, Len.Int.GRL_ID_GAR_LIQ, 0);
		position +:= Len.GRL_ID_GAR_LIQ;
		MarshalByte.writeIntAsPacked(buffer, position, grlIdLiq, Len.Int.GRL_ID_LIQ, 0);
		position +:= Len.GRL_ID_LIQ;
		MarshalByte.writeIntAsPacked(buffer, position, grlIdGar, Len.Int.GRL_ID_GAR, 0);
		position +:= Len.GRL_ID_GAR;
		MarshalByte.writeIntAsPacked(buffer, position, grlIdMoviCrz, Len.Int.GRL_ID_MOVI_CRZ, 0);
		position +:= Len.GRL_ID_MOVI_CRZ;
		grlIdMoviChiu.getGrlIdMoviChiuAsBuffer(buffer, position);
		position +:= GrlIdMoviChiu.Len.GRL_ID_MOVI_CHIU;
		MarshalByte.writeIntAsPacked(buffer, position, grlDtIniEff, Len.Int.GRL_DT_INI_EFF, 0);
		position +:= Len.GRL_DT_INI_EFF;
		MarshalByte.writeIntAsPacked(buffer, position, grlDtEndEff, Len.Int.GRL_DT_END_EFF, 0);
		position +:= Len.GRL_DT_END_EFF;
		MarshalByte.writeIntAsPacked(buffer, position, grlCodCompAnia, Len.Int.GRL_COD_COMP_ANIA, 0);
		position +:= Len.GRL_COD_COMP_ANIA;
		grlDtSin1oAssto.getGrlDtSin1oAsstoAsBuffer(buffer, position);
		position +:= GrlDtSin1oAssto.Len.GRL_DT_SIN1O_ASSTO;
		MarshalByte.writeString(buffer, position, grlCauSin1oAssto, Len.GRL_CAU_SIN1O_ASSTO);
		position +:= Len.GRL_CAU_SIN1O_ASSTO;
		MarshalByte.writeString(buffer, position, grlTpSin1oAssto, Len.GRL_TP_SIN1O_ASSTO);
		position +:= Len.GRL_TP_SIN1O_ASSTO;
		grlDtSin2oAssto.getGrlDtSin2oAsstoAsBuffer(buffer, position);
		position +:= GrlDtSin2oAssto.Len.GRL_DT_SIN2O_ASSTO;
		MarshalByte.writeString(buffer, position, grlCauSin2oAssto, Len.GRL_CAU_SIN2O_ASSTO);
		position +:= Len.GRL_CAU_SIN2O_ASSTO;
		MarshalByte.writeString(buffer, position, grlTpSin2oAssto, Len.GRL_TP_SIN2O_ASSTO);
		position +:= Len.GRL_TP_SIN2O_ASSTO;
		grlDtSin3oAssto.getGrlDtSin3oAsstoAsBuffer(buffer, position);
		position +:= GrlDtSin3oAssto.Len.GRL_DT_SIN3O_ASSTO;
		MarshalByte.writeString(buffer, position, grlCauSin3oAssto, Len.GRL_CAU_SIN3O_ASSTO);
		position +:= Len.GRL_CAU_SIN3O_ASSTO;
		MarshalByte.writeString(buffer, position, grlTpSin3oAssto, Len.GRL_TP_SIN3O_ASSTO);
		position +:= Len.GRL_TP_SIN3O_ASSTO;
		MarshalByte.writeLongAsPacked(buffer, position, grlDsRiga, Len.Int.GRL_DS_RIGA, 0);
		position +:= Len.GRL_DS_RIGA;
		MarshalByte.writeChar(buffer, position, grlDsOperSql);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeIntAsPacked(buffer, position, grlDsVer, Len.Int.GRL_DS_VER, 0);
		position +:= Len.GRL_DS_VER;
		MarshalByte.writeLongAsPacked(buffer, position, grlDsTsIniCptz, Len.Int.GRL_DS_TS_INI_CPTZ, 0);
		position +:= Len.GRL_DS_TS_INI_CPTZ;
		MarshalByte.writeLongAsPacked(buffer, position, grlDsTsEndCptz, Len.Int.GRL_DS_TS_END_CPTZ, 0);
		position +:= Len.GRL_DS_TS_END_CPTZ;
		MarshalByte.writeString(buffer, position, grlDsUtente, Len.GRL_DS_UTENTE);
		position +:= Len.GRL_DS_UTENTE;
		MarshalByte.writeChar(buffer, position, grlDsStatoElab);
		return buffer;
	}

	public void setGrlIdGarLiq(integer grlIdGarLiq) {
		this.grlIdGarLiq:=grlIdGarLiq;
	}

	public integer getGrlIdGarLiq() {
		return this.grlIdGarLiq;
	}

	public void setGrlIdLiq(integer grlIdLiq) {
		this.grlIdLiq:=grlIdLiq;
	}

	public integer getGrlIdLiq() {
		return this.grlIdLiq;
	}

	public void setGrlIdGar(integer grlIdGar) {
		this.grlIdGar:=grlIdGar;
	}

	public integer getGrlIdGar() {
		return this.grlIdGar;
	}

	public void setGrlIdMoviCrz(integer grlIdMoviCrz) {
		this.grlIdMoviCrz:=grlIdMoviCrz;
	}

	public integer getGrlIdMoviCrz() {
		return this.grlIdMoviCrz;
	}

	public void setGrlDtIniEff(integer grlDtIniEff) {
		this.grlDtIniEff:=grlDtIniEff;
	}

	public integer getGrlDtIniEff() {
		return this.grlDtIniEff;
	}

	public void setGrlDtEndEff(integer grlDtEndEff) {
		this.grlDtEndEff:=grlDtEndEff;
	}

	public integer getGrlDtEndEff() {
		return this.grlDtEndEff;
	}

	public void setGrlCodCompAnia(integer grlCodCompAnia) {
		this.grlCodCompAnia:=grlCodCompAnia;
	}

	public integer getGrlCodCompAnia() {
		return this.grlCodCompAnia;
	}

	public void setGrlCauSin1oAssto(string grlCauSin1oAssto) {
		this.grlCauSin1oAssto:=Functions.subString(grlCauSin1oAssto, Len.GRL_CAU_SIN1O_ASSTO);
	}

	public string getGrlCauSin1oAssto() {
		return this.grlCauSin1oAssto;
	}

	public string getGrlCauSin1oAsstoFormatted() {
		return Functions.padBlanks(getGrlCauSin1oAssto(), Len.GRL_CAU_SIN1O_ASSTO);
	}

	public void setGrlTpSin1oAssto(string grlTpSin1oAssto) {
		this.grlTpSin1oAssto:=Functions.subString(grlTpSin1oAssto, Len.GRL_TP_SIN1O_ASSTO);
	}

	public string getGrlTpSin1oAssto() {
		return this.grlTpSin1oAssto;
	}

	public string getGrlTpSin1oAsstoFormatted() {
		return Functions.padBlanks(getGrlTpSin1oAssto(), Len.GRL_TP_SIN1O_ASSTO);
	}

	public void setGrlCauSin2oAssto(string grlCauSin2oAssto) {
		this.grlCauSin2oAssto:=Functions.subString(grlCauSin2oAssto, Len.GRL_CAU_SIN2O_ASSTO);
	}

	public string getGrlCauSin2oAssto() {
		return this.grlCauSin2oAssto;
	}

	public string getGrlCauSin2oAsstoFormatted() {
		return Functions.padBlanks(getGrlCauSin2oAssto(), Len.GRL_CAU_SIN2O_ASSTO);
	}

	public void setGrlTpSin2oAssto(string grlTpSin2oAssto) {
		this.grlTpSin2oAssto:=Functions.subString(grlTpSin2oAssto, Len.GRL_TP_SIN2O_ASSTO);
	}

	public string getGrlTpSin2oAssto() {
		return this.grlTpSin2oAssto;
	}

	public string getGrlTpSin2oAsstoFormatted() {
		return Functions.padBlanks(getGrlTpSin2oAssto(), Len.GRL_TP_SIN2O_ASSTO);
	}

	public void setGrlCauSin3oAssto(string grlCauSin3oAssto) {
		this.grlCauSin3oAssto:=Functions.subString(grlCauSin3oAssto, Len.GRL_CAU_SIN3O_ASSTO);
	}

	public string getGrlCauSin3oAssto() {
		return this.grlCauSin3oAssto;
	}

	public string getGrlCauSin3oAsstoFormatted() {
		return Functions.padBlanks(getGrlCauSin3oAssto(), Len.GRL_CAU_SIN3O_ASSTO);
	}

	public void setGrlTpSin3oAssto(string grlTpSin3oAssto) {
		this.grlTpSin3oAssto:=Functions.subString(grlTpSin3oAssto, Len.GRL_TP_SIN3O_ASSTO);
	}

	public string getGrlTpSin3oAssto() {
		return this.grlTpSin3oAssto;
	}

	public string getGrlTpSin3oAsstoFormatted() {
		return Functions.padBlanks(getGrlTpSin3oAssto(), Len.GRL_TP_SIN3O_ASSTO);
	}

	public void setGrlDsRiga(long grlDsRiga) {
		this.grlDsRiga:=grlDsRiga;
	}

	public long getGrlDsRiga() {
		return this.grlDsRiga;
	}

	public void setGrlDsOperSql(char grlDsOperSql) {
		this.grlDsOperSql:=grlDsOperSql;
	}

	public void setGrlDsOperSqlFormatted(string grlDsOperSql) {
		setGrlDsOperSql(Functions.charAt(grlDsOperSql, Types.CHAR_SIZE));
	}

	public char getGrlDsOperSql() {
		return this.grlDsOperSql;
	}

	public void setGrlDsVer(integer grlDsVer) {
		this.grlDsVer:=grlDsVer;
	}

	public integer getGrlDsVer() {
		return this.grlDsVer;
	}

	public void setGrlDsTsIniCptz(long grlDsTsIniCptz) {
		this.grlDsTsIniCptz:=grlDsTsIniCptz;
	}

	public long getGrlDsTsIniCptz() {
		return this.grlDsTsIniCptz;
	}

	public void setGrlDsTsEndCptz(long grlDsTsEndCptz) {
		this.grlDsTsEndCptz:=grlDsTsEndCptz;
	}

	public long getGrlDsTsEndCptz() {
		return this.grlDsTsEndCptz;
	}

	public void setGrlDsUtente(string grlDsUtente) {
		this.grlDsUtente:=Functions.subString(grlDsUtente, Len.GRL_DS_UTENTE);
	}

	public string getGrlDsUtente() {
		return this.grlDsUtente;
	}

	public void setGrlDsStatoElab(char grlDsStatoElab) {
		this.grlDsStatoElab:=grlDsStatoElab;
	}

	public void setGrlDsStatoElabFormatted(string grlDsStatoElab) {
		setGrlDsStatoElab(Functions.charAt(grlDsStatoElab, Types.CHAR_SIZE));
	}

	public char getGrlDsStatoElab() {
		return this.grlDsStatoElab;
	}

	public GrlDtSin1oAssto getGrlDtSin1oAssto() {
		return grlDtSin1oAssto;
	}

	public GrlDtSin2oAssto getGrlDtSin2oAssto() {
		return grlDtSin2oAssto;
	}

	public GrlDtSin3oAssto getGrlDtSin3oAssto() {
		return grlDtSin3oAssto;
	}

	public GrlIdMoviChiu getGrlIdMoviChiu() {
		return grlIdMoviChiu;
	}

	@Override
	public []byte serialize() {
		return getGarLiqBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer GRL_ID_GAR_LIQ := 5;
		public final static integer GRL_ID_LIQ := 5;
		public final static integer GRL_ID_GAR := 5;
		public final static integer GRL_ID_MOVI_CRZ := 5;
		public final static integer GRL_DT_INI_EFF := 5;
		public final static integer GRL_DT_END_EFF := 5;
		public final static integer GRL_COD_COMP_ANIA := 3;
		public final static integer GRL_CAU_SIN1O_ASSTO := 2;
		public final static integer GRL_TP_SIN1O_ASSTO := 2;
		public final static integer GRL_CAU_SIN2O_ASSTO := 2;
		public final static integer GRL_TP_SIN2O_ASSTO := 2;
		public final static integer GRL_CAU_SIN3O_ASSTO := 2;
		public final static integer GRL_TP_SIN3O_ASSTO := 2;
		public final static integer GRL_DS_RIGA := 6;
		public final static integer GRL_DS_OPER_SQL := 1;
		public final static integer GRL_DS_VER := 5;
		public final static integer GRL_DS_TS_INI_CPTZ := 10;
		public final static integer GRL_DS_TS_END_CPTZ := 10;
		public final static integer GRL_DS_UTENTE := 20;
		public final static integer GRL_DS_STATO_ELAB := 1;
		public final static integer GAR_LIQ := GRL_ID_GAR_LIQ + GRL_ID_LIQ + GRL_ID_GAR + GRL_ID_MOVI_CRZ + GrlIdMoviChiu.Len.GRL_ID_MOVI_CHIU + GRL_DT_INI_EFF + GRL_DT_END_EFF + GRL_COD_COMP_ANIA + GrlDtSin1oAssto.Len.GRL_DT_SIN1O_ASSTO + GRL_CAU_SIN1O_ASSTO + GRL_TP_SIN1O_ASSTO + GrlDtSin2oAssto.Len.GRL_DT_SIN2O_ASSTO + GRL_CAU_SIN2O_ASSTO + GRL_TP_SIN2O_ASSTO + GrlDtSin3oAssto.Len.GRL_DT_SIN3O_ASSTO + GRL_CAU_SIN3O_ASSTO + GRL_TP_SIN3O_ASSTO + GRL_DS_RIGA + GRL_DS_OPER_SQL + GRL_DS_VER + GRL_DS_TS_INI_CPTZ + GRL_DS_TS_END_CPTZ + GRL_DS_UTENTE + GRL_DS_STATO_ELAB;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer GRL_ID_GAR_LIQ := 9;
			public final static integer GRL_ID_LIQ := 9;
			public final static integer GRL_ID_GAR := 9;
			public final static integer GRL_ID_MOVI_CRZ := 9;
			public final static integer GRL_DT_INI_EFF := 8;
			public final static integer GRL_DT_END_EFF := 8;
			public final static integer GRL_COD_COMP_ANIA := 5;
			public final static integer GRL_DS_RIGA := 10;
			public final static integer GRL_DS_VER := 9;
			public final static integer GRL_DS_TS_INI_CPTZ := 18;
			public final static integer GRL_DS_TS_END_CPTZ := 18;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//GarLiqIdbsgrl0
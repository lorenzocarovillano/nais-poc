package com.myorg.myprj.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.copy.Ispc0140DatiInput;
import com.myorg.myprj.ws.occurs.Ispc0140CalcoliP;
import com.myorg.myprj.ws.occurs.Ispc0140CalcoliT;
import com.myorg.myprj.ws.occurs.Ispc0211TabErrori;

/**Original name: AREA-IO-ISPS0140<br>
 * Variable: AREA-IO-ISPS0140 from program ISPS0140<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaIoIsps0140 extends SerializableParameter {

	//==== PROPERTIES ====
	public final static integer CALCOLI_P_MAXOCCURS := 1;
	public final static integer CALCOLI_T_MAXOCCURS := 20;
	public final static integer TAB_ERRORI_MAXOCCURS := 10;
	//Original name: ISPC0140-DATI-INPUT
	private Ispc0140DatiInput datiInput := new Ispc0140DatiInput();
	//Original name: ISPC0140-DATI-NUM-MAX-ELE-P
	private string datiNumMaxEleP := DefaultValues.stringVal(Len.DATI_NUM_MAX_ELE_P);
	//Original name: ISPC0140-CALCOLI-P
	private []Ispc0140CalcoliP calcoliP := new [CALCOLI_P_MAXOCCURS]Ispc0140CalcoliP;
	/**Original name: ISPC0140-DATI-NUM-MAX-ELE-T<br>
	 * <pre>----------------------------------------------------------------*
	 *    SCHEDE TRANCHE
	 * ----------------------------------------------------------------*</pre>*/
	private string datiNumMaxEleT := DefaultValues.stringVal(Len.DATI_NUM_MAX_ELE_T);
	//Original name: ISPC0140-CALCOLI-T
	private []Ispc0140CalcoliT calcoliT := new [CALCOLI_T_MAXOCCURS]Ispc0140CalcoliT;
	//Original name: ISPC0140-ESITO
	private string esito := DefaultValues.stringVal(Len.ESITO);
	//Original name: ISPC0140-ERR-NUM-ELE
	private string errNumEle := DefaultValues.stringVal(Len.ERR_NUM_ELE);
	//Original name: ISPC0140-TAB-ERRORI
	private []Ispc0211TabErrori tabErrori := new [TAB_ERRORI_MAXOCCURS]Ispc0211TabErrori;

	//==== CONSTRUCTORS ====
	public AreaIoIsps0140() {
		init();
	}

	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.AREA_IO_ISPS0140;
	}

	@Override
	public void deserialize([]byte buf) {
		setAreaIoIsps0140Bytes(buf);
	}

	public void init() {
		for int calcoliPIdx in 1.. CALCOLI_P_MAXOCCURS 
		do
			calcoliP[calcoliPIdx] := new Ispc0140CalcoliP();
		enddo
		for int calcoliTIdx in 1.. CALCOLI_T_MAXOCCURS 
		do
			calcoliT[calcoliTIdx] := new Ispc0140CalcoliT();
		enddo
		for int tabErroriIdx in 1.. TAB_ERRORI_MAXOCCURS 
		do
			tabErrori[tabErroriIdx] := new Ispc0211TabErrori();
		enddo
	}

	public string getAreaIoCalcContrFormatted() {
		return MarshalByteExt.bufferToStr(getAreaIoIsps0140Bytes());
	}

	public void setAreaIoIsps0140Bytes([]byte buffer) {
		setAreaIoIsps0140Bytes(buffer, 1);
	}

	public []byte getAreaIoIsps0140Bytes() {
		[]byte buffer := new [Len.AREA_IO_ISPS0140]byte;
		return getAreaIoIsps0140Bytes(buffer, 1);
	}

	public void setAreaIoIsps0140Bytes([]byte buffer, integer offset) {
		integer position := offset;
		datiInput.setDatiInputBytes(buffer, position);
		position +:= Ispc0140DatiInput.Len.DATI_INPUT;
		setDatiOutputBytes(buffer, position);
		position +:= Len.DATI_OUTPUT;
		setAreaErroriBytes(buffer, position);
	}

	public []byte getAreaIoIsps0140Bytes([]byte buffer, integer offset) {
		integer position := offset;
		datiInput.getDatiInputBytes(buffer, position);
		position +:= Ispc0140DatiInput.Len.DATI_INPUT;
		getDatiOutputBytes(buffer, position);
		position +:= Len.DATI_OUTPUT;
		getAreaErroriBytes(buffer, position);
		return buffer;
	}

	public void setDatiOutputBytes([]byte buffer, integer offset) {
		integer position := offset;
		datiNumMaxEleP := MarshalByte.readFixedString(buffer, position, Len.DATI_NUM_MAX_ELE_P);
		position +:= Len.DATI_NUM_MAX_ELE_P;
		for integer idx in 1.. CALCOLI_P_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				calcoliP[idx].setCalcoliPBytes(buffer, position);
				position +:= Ispc0140CalcoliP.Len.CALCOLI_P;
			else
				calcoliP[idx].initCalcoliPSpaces();
				position +:= Ispc0140CalcoliP.Len.CALCOLI_P;
			endif
		enddo
		datiNumMaxEleT := MarshalByte.readFixedString(buffer, position, Len.DATI_NUM_MAX_ELE_T);
		position +:= Len.DATI_NUM_MAX_ELE_T;
		for integer idx in 1.. CALCOLI_T_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				calcoliT[idx].setCalcoliTBytes(buffer, position);
				position +:= Ispc0140CalcoliT.Len.CALCOLI_T;
			else
				calcoliT[idx].initCalcoliTSpaces();
				position +:= Ispc0140CalcoliT.Len.CALCOLI_T;
			endif
		enddo
	}

	public []byte getDatiOutputBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, datiNumMaxEleP, Len.DATI_NUM_MAX_ELE_P);
		position +:= Len.DATI_NUM_MAX_ELE_P;
		for integer idx in 1.. CALCOLI_P_MAXOCCURS 
		do
			calcoliP[idx].getCalcoliPBytes(buffer, position);
			position +:= Ispc0140CalcoliP.Len.CALCOLI_P;
		enddo
		MarshalByte.writeString(buffer, position, datiNumMaxEleT, Len.DATI_NUM_MAX_ELE_T);
		position +:= Len.DATI_NUM_MAX_ELE_T;
		for integer idx in 1.. CALCOLI_T_MAXOCCURS 
		do
			calcoliT[idx].getCalcoliTBytes(buffer, position);
			position +:= Ispc0140CalcoliT.Len.CALCOLI_T;
		enddo
		return buffer;
	}

	public short getDatiNumMaxEleP() {
		return NumericDisplay.asShort(this.datiNumMaxEleP);
	}

	public short getDatiNumMaxEleT() {
		return NumericDisplay.asShort(this.datiNumMaxEleT);
	}

	/**Original name: ISPC0140-AREA-ERRORI<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA ERRORI
	 * ----------------------------------------------------------------*</pre>*/
	public []byte getAreaErroriBytes() {
		[]byte buffer := new [Len.AREA_ERRORI]byte;
		return getAreaErroriBytes(buffer, 1);
	}

	public void setAreaErroriBytes([]byte buffer, integer offset) {
		integer position := offset;
		esito := MarshalByte.readFixedString(buffer, position, Len.ESITO);
		position +:= Len.ESITO;
		errNumEle := MarshalByte.readFixedString(buffer, position, Len.ERR_NUM_ELE);
		position +:= Len.ERR_NUM_ELE;
		for integer idx in 1.. TAB_ERRORI_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				tabErrori[idx].setIspc0211TabErroriBytes(buffer, position);
				position +:= Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
			else
				tabErrori[idx].initIspc0211TabErroriSpaces();
				position +:= Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
			endif
		enddo
	}

	public []byte getAreaErroriBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, esito, Len.ESITO);
		position +:= Len.ESITO;
		MarshalByte.writeString(buffer, position, errNumEle, Len.ERR_NUM_ELE);
		position +:= Len.ERR_NUM_ELE;
		for integer idx in 1.. TAB_ERRORI_MAXOCCURS 
		do
			tabErrori[idx].getIspc0211TabErroriBytes(buffer, position);
			position +:= Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
		enddo
		return buffer;
	}

	public Ispc0140CalcoliP getCalcoliP(integer idx) {
		return calcoliP[idx];
	}

	public Ispc0140CalcoliT getCalcoliT(integer idx) {
		return calcoliT[idx];
	}

	public Ispc0140DatiInput getDatiInput() {
		return datiInput;
	}

	@Override
	public []byte serialize() {
		return getAreaIoIsps0140Bytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer DATI_NUM_MAX_ELE_P := 3;
		public final static integer DATI_NUM_MAX_ELE_T := 3;
		public final static integer DATI_OUTPUT := DATI_NUM_MAX_ELE_P + AreaIoIsps0140.CALCOLI_P_MAXOCCURS * Ispc0140CalcoliP.Len.CALCOLI_P + DATI_NUM_MAX_ELE_T + AreaIoIsps0140.CALCOLI_T_MAXOCCURS * Ispc0140CalcoliT.Len.CALCOLI_T;
		public final static integer ESITO := 2;
		public final static integer ERR_NUM_ELE := 3;
		public final static integer AREA_ERRORI := ESITO + ERR_NUM_ELE + AreaIoIsps0140.TAB_ERRORI_MAXOCCURS * Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
		public final static integer AREA_IO_ISPS0140 := Ispc0140DatiInput.Len.DATI_INPUT + DATI_OUTPUT + AREA_ERRORI;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//AreaIoIsps0140
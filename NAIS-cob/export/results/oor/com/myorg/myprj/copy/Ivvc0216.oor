package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

import com.myorg.myprj.ws.occurs.Ivvv0212CtrlAutOper;
import com.myorg.myprj.ws.redefines.C216TabValP;
import com.myorg.myprj.ws.redefines.C216TabValT;

/**Original name: IVVC0216<br>
 * Variable: IVVC0216 from copybook IVVC0216<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ivvc0216 {

	//==== PROPERTIES ====
	public final static integer C216_CTRL_AUT_OPER_MAXOCCURS := 20;
	/**Original name: C216-DEE<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA OUTPUT VALORIZZATORE VARIABILI
	 *    - area skeda per servizi di prodotto
	 *    - area variabili autonomia operativa
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *    AREA OUTPUT PRODOTTO SCHEDE 'P' 'Q' 'O'
	 * ----------------------------------------------------------------*</pre>*/
	private string c216Dee := DefaultValues.stringVal(Len.C216_DEE);
	//Original name: C216-ELE-LIVELLO-MAX-P
	private short c216EleLivelloMaxP := DefaultValues.SHORT_VAL;
	//Original name: C216-TAB-VAL-P
	private C216TabValP c216TabValP := new C216TabValP();
	/**Original name: C216-ELE-LIVELLO-MAX-T<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA OUTPUT TRANCHE SCHEDE 'G' 'H'
	 * ----------------------------------------------------------------*</pre>*/
	private short c216EleLivelloMaxT := DefaultValues.SHORT_VAL;
	//Original name: C216-TAB-VAL-T
	private C216TabValT c216TabValT := new C216TabValT();
	//Original name: C216-ELE-CTRL-AUT-OPER-MAX
	private short c216EleCtrlAutOperMax := DefaultValues.SHORT_VAL;
	//Original name: C216-CTRL-AUT-OPER
	private []Ivvv0212CtrlAutOper c216CtrlAutOper := new [C216_CTRL_AUT_OPER_MAXOCCURS]Ivvv0212CtrlAutOper;

	//==== CONSTRUCTORS ====
	public Ivvc0216() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for int c216CtrlAutOperIdx in 1.. C216_CTRL_AUT_OPER_MAXOCCURS 
		do
			c216CtrlAutOper[c216CtrlAutOperIdx] := new Ivvv0212CtrlAutOper();
		enddo
	}

	public void setC216Dee(string c216Dee) {
		this.c216Dee:=Functions.subString(c216Dee, Len.C216_DEE);
	}

	public string getC216Dee() {
		return this.c216Dee;
	}

	public void setC216EleLivelloMaxP(short c216EleLivelloMaxP) {
		this.c216EleLivelloMaxP:=c216EleLivelloMaxP;
	}

	public short getC216EleLivelloMaxP() {
		return this.c216EleLivelloMaxP;
	}

	public void setC216EleLivelloMaxT(short c216EleLivelloMaxT) {
		this.c216EleLivelloMaxT:=c216EleLivelloMaxT;
	}

	public short getC216EleLivelloMaxT() {
		return this.c216EleLivelloMaxT;
	}

	public void setC216VarAutOperBytes([]byte buffer) {
		setC216VarAutOperBytes(buffer, 1);
	}

	public void setC216VarAutOperBytes([]byte buffer, integer offset) {
		integer position := offset;
		c216EleCtrlAutOperMax := MarshalByte.readPackedAsShort(buffer, position, Len.Int.C216_ELE_CTRL_AUT_OPER_MAX, 0);
		position +:= Len.C216_ELE_CTRL_AUT_OPER_MAX;
		for integer idx in 1.. C216_CTRL_AUT_OPER_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				c216CtrlAutOper[idx].setCtrlAutOperBytes(buffer, position);
				position +:= Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;
			else
				c216CtrlAutOper[idx].initCtrlAutOperSpaces();
				position +:= Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;
			endif
		enddo
	}

	public []byte getC216VarAutOperBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeShortAsPacked(buffer, position, c216EleCtrlAutOperMax, Len.Int.C216_ELE_CTRL_AUT_OPER_MAX, 0);
		position +:= Len.C216_ELE_CTRL_AUT_OPER_MAX;
		for integer idx in 1.. C216_CTRL_AUT_OPER_MAXOCCURS 
		do
			c216CtrlAutOper[idx].getCtrlAutOperBytes(buffer, position);
			position +:= Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;
		enddo
		return buffer;
	}

	public void setC216EleCtrlAutOperMax(short c216EleCtrlAutOperMax) {
		this.c216EleCtrlAutOperMax:=c216EleCtrlAutOperMax;
	}

	public short getC216EleCtrlAutOperMax() {
		return this.c216EleCtrlAutOperMax;
	}

	public Ivvv0212CtrlAutOper getC216CtrlAutOper(integer idx) {
		return c216CtrlAutOper[idx];
	}

	public C216TabValP getC216TabValP() {
		return c216TabValP;
	}

	public C216TabValT getC216TabValT() {
		return c216TabValT;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer C216_DEE := 8;
		public final static integer C216_ELE_LIVELLO_MAX_P := 3;
		public final static integer C216_ELE_LIVELLO_MAX_T := 3;
		public final static integer C216_ELE_CTRL_AUT_OPER_MAX := 3;
		public final static integer C216_VAR_AUT_OPER := C216_ELE_CTRL_AUT_OPER_MAX + Ivvc0216.C216_CTRL_AUT_OPER_MAXOCCURS * Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer C216_ELE_LIVELLO_MAX_P := 4;
			public final static integer C216_ELE_LIVELLO_MAX_T := 4;
			public final static integer C216_ELE_CTRL_AUT_OPER_MAX := 4;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//Ivvc0216
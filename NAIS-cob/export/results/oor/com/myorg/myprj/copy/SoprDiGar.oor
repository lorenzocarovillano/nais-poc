package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

import com.myorg.myprj.ws.redefines.SpgIdGar;
import com.myorg.myprj.ws.redefines.SpgIdMoviChiu;
import com.myorg.myprj.ws.redefines.SpgPcSopram;
import com.myorg.myprj.ws.redefines.SpgValImp;
import com.myorg.myprj.ws.redefines.SpgValPc;

/**Original name: SOPR-DI-GAR<br>
 * Variable: SOPR-DI-GAR from copybook IDBVSPG1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SoprDiGar {

	//==== PROPERTIES ====
	//Original name: SPG-ID-SOPR-DI-GAR
	private integer spgIdSoprDiGar := DefaultValues.INT_VAL;
	//Original name: SPG-ID-GAR
	private SpgIdGar spgIdGar := new SpgIdGar();
	//Original name: SPG-ID-MOVI-CRZ
	private integer spgIdMoviCrz := DefaultValues.INT_VAL;
	//Original name: SPG-ID-MOVI-CHIU
	private SpgIdMoviChiu spgIdMoviChiu := new SpgIdMoviChiu();
	//Original name: SPG-DT-INI-EFF
	private integer spgDtIniEff := DefaultValues.INT_VAL;
	//Original name: SPG-DT-END-EFF
	private integer spgDtEndEff := DefaultValues.INT_VAL;
	//Original name: SPG-COD-COMP-ANIA
	private integer spgCodCompAnia := DefaultValues.INT_VAL;
	//Original name: SPG-COD-SOPR
	private string spgCodSopr := DefaultValues.stringVal(Len.SPG_COD_SOPR);
	//Original name: SPG-TP-D
	private string spgTpD := DefaultValues.stringVal(Len.SPG_TP_D);
	//Original name: SPG-VAL-PC
	private SpgValPc spgValPc := new SpgValPc();
	//Original name: SPG-VAL-IMP
	private SpgValImp spgValImp := new SpgValImp();
	//Original name: SPG-PC-SOPRAM
	private SpgPcSopram spgPcSopram := new SpgPcSopram();
	//Original name: SPG-FL-ESCL-SOPR
	private char spgFlEsclSopr := DefaultValues.CHAR_VAL;
	//Original name: SPG-DESC-ESCL-LEN
	private short spgDescEsclLen := DefaultValues.BIN_SHORT_VAL;
	//Original name: SPG-DESC-ESCL
	private string spgDescEscl := DefaultValues.stringVal(Len.SPG_DESC_ESCL);
	//Original name: SPG-DS-RIGA
	private long spgDsRiga := DefaultValues.LONG_VAL;
	//Original name: SPG-DS-OPER-SQL
	private char spgDsOperSql := DefaultValues.CHAR_VAL;
	//Original name: SPG-DS-VER
	private integer spgDsVer := DefaultValues.INT_VAL;
	//Original name: SPG-DS-TS-INI-CPTZ
	private long spgDsTsIniCptz := DefaultValues.LONG_VAL;
	//Original name: SPG-DS-TS-END-CPTZ
	private long spgDsTsEndCptz := DefaultValues.LONG_VAL;
	//Original name: SPG-DS-UTENTE
	private string spgDsUtente := DefaultValues.stringVal(Len.SPG_DS_UTENTE);
	//Original name: SPG-DS-STATO-ELAB
	private char spgDsStatoElab := DefaultValues.CHAR_VAL;


	//==== METHODS ====
	public void setSoprDiGarFormatted(string data) {
		[]byte buffer := new [Len.SOPR_DI_GAR]byte;
		MarshalByte.writeString(buffer, 1, data, Len.SOPR_DI_GAR);
		setSoprDiGarBytes(buffer, 1);
	}

	public string getSoprDiGarFormatted() {
		return MarshalByteExt.bufferToStr(getSoprDiGarBytes());
	}

	public []byte getSoprDiGarBytes() {
		[]byte buffer := new [Len.SOPR_DI_GAR]byte;
		return getSoprDiGarBytes(buffer, 1);
	}

	public void setSoprDiGarBytes([]byte buffer, integer offset) {
		integer position := offset;
		spgIdSoprDiGar := MarshalByte.readPackedAsInt(buffer, position, Len.Int.SPG_ID_SOPR_DI_GAR, 0);
		position +:= Len.SPG_ID_SOPR_DI_GAR;
		spgIdGar.setSpgIdGarFromBuffer(buffer, position);
		position +:= SpgIdGar.Len.SPG_ID_GAR;
		spgIdMoviCrz := MarshalByte.readPackedAsInt(buffer, position, Len.Int.SPG_ID_MOVI_CRZ, 0);
		position +:= Len.SPG_ID_MOVI_CRZ;
		spgIdMoviChiu.setSpgIdMoviChiuFromBuffer(buffer, position);
		position +:= SpgIdMoviChiu.Len.SPG_ID_MOVI_CHIU;
		spgDtIniEff := MarshalByte.readPackedAsInt(buffer, position, Len.Int.SPG_DT_INI_EFF, 0);
		position +:= Len.SPG_DT_INI_EFF;
		spgDtEndEff := MarshalByte.readPackedAsInt(buffer, position, Len.Int.SPG_DT_END_EFF, 0);
		position +:= Len.SPG_DT_END_EFF;
		spgCodCompAnia := MarshalByte.readPackedAsInt(buffer, position, Len.Int.SPG_COD_COMP_ANIA, 0);
		position +:= Len.SPG_COD_COMP_ANIA;
		spgCodSopr := MarshalByte.readString(buffer, position, Len.SPG_COD_SOPR);
		position +:= Len.SPG_COD_SOPR;
		spgTpD := MarshalByte.readString(buffer, position, Len.SPG_TP_D);
		position +:= Len.SPG_TP_D;
		spgValPc.setSpgValPcFromBuffer(buffer, position);
		position +:= SpgValPc.Len.SPG_VAL_PC;
		spgValImp.setSpgValImpFromBuffer(buffer, position);
		position +:= SpgValImp.Len.SPG_VAL_IMP;
		spgPcSopram.setSpgPcSopramFromBuffer(buffer, position);
		position +:= SpgPcSopram.Len.SPG_PC_SOPRAM;
		spgFlEsclSopr := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		setSpgDescEsclVcharBytes(buffer, position);
		position +:= Len.SPG_DESC_ESCL_VCHAR;
		spgDsRiga := MarshalByte.readPackedAsLong(buffer, position, Len.Int.SPG_DS_RIGA, 0);
		position +:= Len.SPG_DS_RIGA;
		spgDsOperSql := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		spgDsVer := MarshalByte.readPackedAsInt(buffer, position, Len.Int.SPG_DS_VER, 0);
		position +:= Len.SPG_DS_VER;
		spgDsTsIniCptz := MarshalByte.readPackedAsLong(buffer, position, Len.Int.SPG_DS_TS_INI_CPTZ, 0);
		position +:= Len.SPG_DS_TS_INI_CPTZ;
		spgDsTsEndCptz := MarshalByte.readPackedAsLong(buffer, position, Len.Int.SPG_DS_TS_END_CPTZ, 0);
		position +:= Len.SPG_DS_TS_END_CPTZ;
		spgDsUtente := MarshalByte.readString(buffer, position, Len.SPG_DS_UTENTE);
		position +:= Len.SPG_DS_UTENTE;
		spgDsStatoElab := MarshalByte.readChar(buffer, position);
	}

	public []byte getSoprDiGarBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeIntAsPacked(buffer, position, spgIdSoprDiGar, Len.Int.SPG_ID_SOPR_DI_GAR, 0);
		position +:= Len.SPG_ID_SOPR_DI_GAR;
		spgIdGar.getSpgIdGarAsBuffer(buffer, position);
		position +:= SpgIdGar.Len.SPG_ID_GAR;
		MarshalByte.writeIntAsPacked(buffer, position, spgIdMoviCrz, Len.Int.SPG_ID_MOVI_CRZ, 0);
		position +:= Len.SPG_ID_MOVI_CRZ;
		spgIdMoviChiu.getSpgIdMoviChiuAsBuffer(buffer, position);
		position +:= SpgIdMoviChiu.Len.SPG_ID_MOVI_CHIU;
		MarshalByte.writeIntAsPacked(buffer, position, spgDtIniEff, Len.Int.SPG_DT_INI_EFF, 0);
		position +:= Len.SPG_DT_INI_EFF;
		MarshalByte.writeIntAsPacked(buffer, position, spgDtEndEff, Len.Int.SPG_DT_END_EFF, 0);
		position +:= Len.SPG_DT_END_EFF;
		MarshalByte.writeIntAsPacked(buffer, position, spgCodCompAnia, Len.Int.SPG_COD_COMP_ANIA, 0);
		position +:= Len.SPG_COD_COMP_ANIA;
		MarshalByte.writeString(buffer, position, spgCodSopr, Len.SPG_COD_SOPR);
		position +:= Len.SPG_COD_SOPR;
		MarshalByte.writeString(buffer, position, spgTpD, Len.SPG_TP_D);
		position +:= Len.SPG_TP_D;
		spgValPc.getSpgValPcAsBuffer(buffer, position);
		position +:= SpgValPc.Len.SPG_VAL_PC;
		spgValImp.getSpgValImpAsBuffer(buffer, position);
		position +:= SpgValImp.Len.SPG_VAL_IMP;
		spgPcSopram.getSpgPcSopramAsBuffer(buffer, position);
		position +:= SpgPcSopram.Len.SPG_PC_SOPRAM;
		MarshalByte.writeChar(buffer, position, spgFlEsclSopr);
		position +:= Types.CHAR_SIZE;
		getSpgDescEsclVcharBytes(buffer, position);
		position +:= Len.SPG_DESC_ESCL_VCHAR;
		MarshalByte.writeLongAsPacked(buffer, position, spgDsRiga, Len.Int.SPG_DS_RIGA, 0);
		position +:= Len.SPG_DS_RIGA;
		MarshalByte.writeChar(buffer, position, spgDsOperSql);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeIntAsPacked(buffer, position, spgDsVer, Len.Int.SPG_DS_VER, 0);
		position +:= Len.SPG_DS_VER;
		MarshalByte.writeLongAsPacked(buffer, position, spgDsTsIniCptz, Len.Int.SPG_DS_TS_INI_CPTZ, 0);
		position +:= Len.SPG_DS_TS_INI_CPTZ;
		MarshalByte.writeLongAsPacked(buffer, position, spgDsTsEndCptz, Len.Int.SPG_DS_TS_END_CPTZ, 0);
		position +:= Len.SPG_DS_TS_END_CPTZ;
		MarshalByte.writeString(buffer, position, spgDsUtente, Len.SPG_DS_UTENTE);
		position +:= Len.SPG_DS_UTENTE;
		MarshalByte.writeChar(buffer, position, spgDsStatoElab);
		return buffer;
	}

	public void setSpgIdSoprDiGar(integer spgIdSoprDiGar) {
		this.spgIdSoprDiGar:=spgIdSoprDiGar;
	}

	public integer getSpgIdSoprDiGar() {
		return this.spgIdSoprDiGar;
	}

	public void setSpgIdMoviCrz(integer spgIdMoviCrz) {
		this.spgIdMoviCrz:=spgIdMoviCrz;
	}

	public integer getSpgIdMoviCrz() {
		return this.spgIdMoviCrz;
	}

	public void setSpgDtIniEff(integer spgDtIniEff) {
		this.spgDtIniEff:=spgDtIniEff;
	}

	public integer getSpgDtIniEff() {
		return this.spgDtIniEff;
	}

	public void setSpgDtEndEff(integer spgDtEndEff) {
		this.spgDtEndEff:=spgDtEndEff;
	}

	public integer getSpgDtEndEff() {
		return this.spgDtEndEff;
	}

	public void setSpgCodCompAnia(integer spgCodCompAnia) {
		this.spgCodCompAnia:=spgCodCompAnia;
	}

	public integer getSpgCodCompAnia() {
		return this.spgCodCompAnia;
	}

	public void setSpgCodSopr(string spgCodSopr) {
		this.spgCodSopr:=Functions.subString(spgCodSopr, Len.SPG_COD_SOPR);
	}

	public string getSpgCodSopr() {
		return this.spgCodSopr;
	}

	public string getSpgCodSoprFormatted() {
		return Functions.padBlanks(getSpgCodSopr(), Len.SPG_COD_SOPR);
	}

	public void setSpgTpD(string spgTpD) {
		this.spgTpD:=Functions.subString(spgTpD, Len.SPG_TP_D);
	}

	public string getSpgTpD() {
		return this.spgTpD;
	}

	public string getSpgTpDFormatted() {
		return Functions.padBlanks(getSpgTpD(), Len.SPG_TP_D);
	}

	public void setSpgFlEsclSopr(char spgFlEsclSopr) {
		this.spgFlEsclSopr:=spgFlEsclSopr;
	}

	public char getSpgFlEsclSopr() {
		return this.spgFlEsclSopr;
	}

	public void setSpgDescEsclVcharBytes([]byte buffer, integer offset) {
		integer position := offset;
		spgDescEsclLen := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		spgDescEscl := MarshalByte.readString(buffer, position, Len.SPG_DESC_ESCL);
	}

	public []byte getSpgDescEsclVcharBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeBinaryShort(buffer, position, spgDescEsclLen);
		position +:= Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, spgDescEscl, Len.SPG_DESC_ESCL);
		return buffer;
	}

	public void setSpgDescEsclLen(short spgDescEsclLen) {
		this.spgDescEsclLen:=spgDescEsclLen;
	}

	public short getSpgDescEsclLen() {
		return this.spgDescEsclLen;
	}

	public void setSpgDescEscl(string spgDescEscl) {
		this.spgDescEscl:=Functions.subString(spgDescEscl, Len.SPG_DESC_ESCL);
	}

	public string getSpgDescEscl() {
		return this.spgDescEscl;
	}

	public void setSpgDsRiga(long spgDsRiga) {
		this.spgDsRiga:=spgDsRiga;
	}

	public long getSpgDsRiga() {
		return this.spgDsRiga;
	}

	public void setSpgDsOperSql(char spgDsOperSql) {
		this.spgDsOperSql:=spgDsOperSql;
	}

	public char getSpgDsOperSql() {
		return this.spgDsOperSql;
	}

	public void setSpgDsVer(integer spgDsVer) {
		this.spgDsVer:=spgDsVer;
	}

	public integer getSpgDsVer() {
		return this.spgDsVer;
	}

	public void setSpgDsTsIniCptz(long spgDsTsIniCptz) {
		this.spgDsTsIniCptz:=spgDsTsIniCptz;
	}

	public long getSpgDsTsIniCptz() {
		return this.spgDsTsIniCptz;
	}

	public void setSpgDsTsEndCptz(long spgDsTsEndCptz) {
		this.spgDsTsEndCptz:=spgDsTsEndCptz;
	}

	public long getSpgDsTsEndCptz() {
		return this.spgDsTsEndCptz;
	}

	public void setSpgDsUtente(string spgDsUtente) {
		this.spgDsUtente:=Functions.subString(spgDsUtente, Len.SPG_DS_UTENTE);
	}

	public string getSpgDsUtente() {
		return this.spgDsUtente;
	}

	public void setSpgDsStatoElab(char spgDsStatoElab) {
		this.spgDsStatoElab:=spgDsStatoElab;
	}

	public char getSpgDsStatoElab() {
		return this.spgDsStatoElab;
	}

	public SpgIdGar getSpgIdGar() {
		return spgIdGar;
	}

	public SpgIdMoviChiu getSpgIdMoviChiu() {
		return spgIdMoviChiu;
	}

	public SpgPcSopram getSpgPcSopram() {
		return spgPcSopram;
	}

	public SpgValImp getSpgValImp() {
		return spgValImp;
	}

	public SpgValPc getSpgValPc() {
		return spgValPc;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer SPG_COD_SOPR := 12;
		public final static integer SPG_TP_D := 2;
		public final static integer SPG_DESC_ESCL := 100;
		public final static integer SPG_DS_UTENTE := 20;
		public final static integer SPG_ID_SOPR_DI_GAR := 5;
		public final static integer SPG_ID_MOVI_CRZ := 5;
		public final static integer SPG_DT_INI_EFF := 5;
		public final static integer SPG_DT_END_EFF := 5;
		public final static integer SPG_COD_COMP_ANIA := 3;
		public final static integer SPG_FL_ESCL_SOPR := 1;
		public final static integer SPG_DESC_ESCL_LEN := 2;
		public final static integer SPG_DESC_ESCL_VCHAR := SPG_DESC_ESCL_LEN + SPG_DESC_ESCL;
		public final static integer SPG_DS_RIGA := 6;
		public final static integer SPG_DS_OPER_SQL := 1;
		public final static integer SPG_DS_VER := 5;
		public final static integer SPG_DS_TS_INI_CPTZ := 10;
		public final static integer SPG_DS_TS_END_CPTZ := 10;
		public final static integer SPG_DS_STATO_ELAB := 1;
		public final static integer SOPR_DI_GAR := SPG_ID_SOPR_DI_GAR + SpgIdGar.Len.SPG_ID_GAR + SPG_ID_MOVI_CRZ + SpgIdMoviChiu.Len.SPG_ID_MOVI_CHIU + SPG_DT_INI_EFF + SPG_DT_END_EFF + SPG_COD_COMP_ANIA + SPG_COD_SOPR + SPG_TP_D + SpgValPc.Len.SPG_VAL_PC + SpgValImp.Len.SPG_VAL_IMP + SpgPcSopram.Len.SPG_PC_SOPRAM + SPG_FL_ESCL_SOPR + SPG_DESC_ESCL_VCHAR + SPG_DS_RIGA + SPG_DS_OPER_SQL + SPG_DS_VER + SPG_DS_TS_INI_CPTZ + SPG_DS_TS_END_CPTZ + SPG_DS_UTENTE + SPG_DS_STATO_ELAB;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer SPG_ID_SOPR_DI_GAR := 9;
			public final static integer SPG_ID_MOVI_CRZ := 9;
			public final static integer SPG_DT_INI_EFF := 8;
			public final static integer SPG_DT_END_EFF := 8;
			public final static integer SPG_COD_COMP_ANIA := 5;
			public final static integer SPG_DS_RIGA := 10;
			public final static integer SPG_DS_VER := 9;
			public final static integer SPG_DS_TS_INI_CPTZ := 18;
			public final static integer SPG_DS_TS_END_CPTZ := 18;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//SoprDiGar
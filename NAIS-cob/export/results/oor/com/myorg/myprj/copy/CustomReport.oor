package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: CUSTOM-REPORT<br>
 * Variable: CUSTOM-REPORT from copybook IABV0010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CustomReport {

	//==== PROPERTIES ====
	//Original name: FILLER-CUSTOM-REPORT
	private char flr1 := ' ';
	//Original name: REP-CUSTOM-COUNT-DESC
	private string desc := "";
	//Original name: FILLER-CUSTOM-REPORT-1
	private string flr2 := " :";
	//Original name: REP-CUSTOM-COUNT
	private string t := "000000000";


	//==== METHODS ====
	public string getCustomReportFormatted() {
		return MarshalByteExt.bufferToStr(getCustomReportBytes());
	}

	public []byte getCustomReportBytes() {
		[]byte buffer := new [Len.CUSTOM_REPORT]byte;
		return getCustomReportBytes(buffer, 1);
	}

	public []byte getCustomReportBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, flr1);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, desc, Len.DESC);
		position +:= Len.DESC;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position +:= Len.FLR2;
		MarshalByte.writeString(buffer, position, t, Len.T);
		return buffer;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setDesc(string desc) {
		this.desc:=Functions.subString(desc, Len.DESC);
	}

	public string getDesc() {
		return this.desc;
	}

	public string getFlr2() {
		return this.flr2;
	}

	public void setT(long t) {
		this.t:=PicFormatter.display("Z(8)9").format(t).toString();
	}

	public long getT() {
		return PicParser.display("Z(8)9").parseLong(this.t);
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer T := 9;
		public final static integer DESC := 49;
		public final static integer FLR1 := 1;
		public final static integer FLR2 := 4;
		public final static integer CUSTOM_REPORT := DESC + T + FLR1 + FLR2;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//CustomReport
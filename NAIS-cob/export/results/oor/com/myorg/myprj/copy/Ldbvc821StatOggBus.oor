package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBVC821-STAT-OGG-BUS<br>
 * Variable: LDBVC821-STAT-OGG-BUS from copybook LDBVC821<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ldbvc821StatOggBus {

	//==== PROPERTIES ====
	//Original name: LC821-STB-ID-STAT-OGG-BUS
	private integer idStatOggBus := DefaultValues.INT_VAL;
	//Original name: LC821-STB-ID-OGG
	private integer idOgg := DefaultValues.INT_VAL;
	//Original name: LC821-STB-TP-OGG
	private string tpOgg := DefaultValues.stringVal(Len.TP_OGG);
	//Original name: LC821-STB-ID-MOVI-CRZ
	private integer idMoviCrz := DefaultValues.INT_VAL;
	//Original name: LC821-STB-ID-MOVI-CHIU
	private integer idMoviChiu := DefaultValues.INT_VAL;
	//Original name: LC821-STB-DT-INI-EFF
	private integer dtIniEff := DefaultValues.INT_VAL;
	//Original name: LC821-STB-DT-END-EFF
	private integer dtEndEff := DefaultValues.INT_VAL;
	//Original name: LC821-STB-COD-COMP-ANIA
	private integer codCompAnia := DefaultValues.INT_VAL;
	//Original name: LC821-STB-TP-STAT-BUS
	private string tpStatBus := DefaultValues.stringVal(Len.TP_STAT_BUS);
	//Original name: LC821-STB-TP-CAUS
	private string tpCaus := DefaultValues.stringVal(Len.TP_CAUS);
	//Original name: LC821-STB-DS-RIGA
	private long dsRiga := DefaultValues.LONG_VAL;
	//Original name: LC821-STB-DS-OPER-SQL
	private char dsOperSql := DefaultValues.CHAR_VAL;
	//Original name: LC821-STB-DS-VER
	private integer dsVer := DefaultValues.INT_VAL;
	//Original name: LC821-STB-DS-TS-INI-CPTZ
	private long dsTsIniCptz := DefaultValues.LONG_VAL;
	//Original name: LC821-STB-DS-TS-END-CPTZ
	private long dsTsEndCptz := DefaultValues.LONG_VAL;
	//Original name: LC821-STB-DS-UTENTE
	private string dsUtente := DefaultValues.stringVal(Len.DS_UTENTE);
	//Original name: LC821-STB-DS-STATO-ELAB
	private char dsStatoElab := DefaultValues.CHAR_VAL;


	//==== METHODS ====
	public void setLdbvc821StatOggBusBytes([]byte buffer) {
		setLdbvc821StatOggBusBytes(buffer, 1);
	}

	public []byte getLdbvc821StatOggBusBytes() {
		[]byte buffer := new [Len.LDBVC821_STAT_OGG_BUS]byte;
		return getLdbvc821StatOggBusBytes(buffer, 1);
	}

	public void setLdbvc821StatOggBusBytes([]byte buffer, integer offset) {
		integer position := offset;
		idStatOggBus := MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_STAT_OGG_BUS, 0);
		position +:= Len.ID_STAT_OGG_BUS;
		idOgg := MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
		position +:= Len.ID_OGG;
		tpOgg := MarshalByte.readString(buffer, position, Len.TP_OGG);
		position +:= Len.TP_OGG;
		idMoviCrz := MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CRZ, 0);
		position +:= Len.ID_MOVI_CRZ;
		idMoviChiu := MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CHIU, 0);
		position +:= Len.ID_MOVI_CHIU;
		dtIniEff := MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INI_EFF, 0);
		position +:= Len.DT_INI_EFF;
		dtEndEff := MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_END_EFF, 0);
		position +:= Len.DT_END_EFF;
		codCompAnia := MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
		position +:= Len.COD_COMP_ANIA;
		tpStatBus := MarshalByte.readString(buffer, position, Len.TP_STAT_BUS);
		position +:= Len.TP_STAT_BUS;
		tpCaus := MarshalByte.readString(buffer, position, Len.TP_CAUS);
		position +:= Len.TP_CAUS;
		dsRiga := MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_RIGA, 0);
		position +:= Len.DS_RIGA;
		dsOperSql := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		dsVer := MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
		position +:= Len.DS_VER;
		dsTsIniCptz := MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_INI_CPTZ, 0);
		position +:= Len.DS_TS_INI_CPTZ;
		dsTsEndCptz := MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_END_CPTZ, 0);
		position +:= Len.DS_TS_END_CPTZ;
		dsUtente := MarshalByte.readString(buffer, position, Len.DS_UTENTE);
		position +:= Len.DS_UTENTE;
		dsStatoElab := MarshalByte.readChar(buffer, position);
	}

	public []byte getLdbvc821StatOggBusBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeIntAsPacked(buffer, position, idStatOggBus, Len.Int.ID_STAT_OGG_BUS, 0);
		position +:= Len.ID_STAT_OGG_BUS;
		MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
		position +:= Len.ID_OGG;
		MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
		position +:= Len.TP_OGG;
		MarshalByte.writeIntAsPacked(buffer, position, idMoviCrz, Len.Int.ID_MOVI_CRZ, 0);
		position +:= Len.ID_MOVI_CRZ;
		MarshalByte.writeIntAsPacked(buffer, position, idMoviChiu, Len.Int.ID_MOVI_CHIU, 0);
		position +:= Len.ID_MOVI_CHIU;
		MarshalByte.writeIntAsPacked(buffer, position, dtIniEff, Len.Int.DT_INI_EFF, 0);
		position +:= Len.DT_INI_EFF;
		MarshalByte.writeIntAsPacked(buffer, position, dtEndEff, Len.Int.DT_END_EFF, 0);
		position +:= Len.DT_END_EFF;
		MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
		position +:= Len.COD_COMP_ANIA;
		MarshalByte.writeString(buffer, position, tpStatBus, Len.TP_STAT_BUS);
		position +:= Len.TP_STAT_BUS;
		MarshalByte.writeString(buffer, position, tpCaus, Len.TP_CAUS);
		position +:= Len.TP_CAUS;
		MarshalByte.writeLongAsPacked(buffer, position, dsRiga, Len.Int.DS_RIGA, 0);
		position +:= Len.DS_RIGA;
		MarshalByte.writeChar(buffer, position, dsOperSql);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeIntAsPacked(buffer, position, dsVer, Len.Int.DS_VER, 0);
		position +:= Len.DS_VER;
		MarshalByte.writeLongAsPacked(buffer, position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ, 0);
		position +:= Len.DS_TS_INI_CPTZ;
		MarshalByte.writeLongAsPacked(buffer, position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ, 0);
		position +:= Len.DS_TS_END_CPTZ;
		MarshalByte.writeString(buffer, position, dsUtente, Len.DS_UTENTE);
		position +:= Len.DS_UTENTE;
		MarshalByte.writeChar(buffer, position, dsStatoElab);
		return buffer;
	}

	public void setIdStatOggBus(integer idStatOggBus) {
		this.idStatOggBus:=idStatOggBus;
	}

	public integer getIdStatOggBus() {
		return this.idStatOggBus;
	}

	public void setIdOgg(integer idOgg) {
		this.idOgg:=idOgg;
	}

	public integer getIdOgg() {
		return this.idOgg;
	}

	public void setTpOgg(string tpOgg) {
		this.tpOgg:=Functions.subString(tpOgg, Len.TP_OGG);
	}

	public string getTpOgg() {
		return this.tpOgg;
	}

	public void setIdMoviCrz(integer idMoviCrz) {
		this.idMoviCrz:=idMoviCrz;
	}

	public integer getIdMoviCrz() {
		return this.idMoviCrz;
	}

	public void setIdMoviChiu(integer idMoviChiu) {
		this.idMoviChiu:=idMoviChiu;
	}

	public integer getIdMoviChiu() {
		return this.idMoviChiu;
	}

	public void setDtIniEff(integer dtIniEff) {
		this.dtIniEff:=dtIniEff;
	}

	public integer getDtIniEff() {
		return this.dtIniEff;
	}

	public void setDtEndEff(integer dtEndEff) {
		this.dtEndEff:=dtEndEff;
	}

	public integer getDtEndEff() {
		return this.dtEndEff;
	}

	public void setCodCompAnia(integer codCompAnia) {
		this.codCompAnia:=codCompAnia;
	}

	public integer getCodCompAnia() {
		return this.codCompAnia;
	}

	public void setTpStatBus(string tpStatBus) {
		this.tpStatBus:=Functions.subString(tpStatBus, Len.TP_STAT_BUS);
	}

	public string getTpStatBus() {
		return this.tpStatBus;
	}

	public void setTpCaus(string tpCaus) {
		this.tpCaus:=Functions.subString(tpCaus, Len.TP_CAUS);
	}

	public string getTpCaus() {
		return this.tpCaus;
	}

	public void setDsRiga(long dsRiga) {
		this.dsRiga:=dsRiga;
	}

	public long getDsRiga() {
		return this.dsRiga;
	}

	public void setDsOperSql(char dsOperSql) {
		this.dsOperSql:=dsOperSql;
	}

	public char getDsOperSql() {
		return this.dsOperSql;
	}

	public void setDsVer(integer dsVer) {
		this.dsVer:=dsVer;
	}

	public integer getDsVer() {
		return this.dsVer;
	}

	public void setDsTsIniCptz(long dsTsIniCptz) {
		this.dsTsIniCptz:=dsTsIniCptz;
	}

	public long getDsTsIniCptz() {
		return this.dsTsIniCptz;
	}

	public void setDsTsEndCptz(long dsTsEndCptz) {
		this.dsTsEndCptz:=dsTsEndCptz;
	}

	public long getDsTsEndCptz() {
		return this.dsTsEndCptz;
	}

	public void setDsUtente(string dsUtente) {
		this.dsUtente:=Functions.subString(dsUtente, Len.DS_UTENTE);
	}

	public string getDsUtente() {
		return this.dsUtente;
	}

	public void setDsStatoElab(char dsStatoElab) {
		this.dsStatoElab:=dsStatoElab;
	}

	public char getDsStatoElab() {
		return this.dsStatoElab;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ID_STAT_OGG_BUS := 5;
		public final static integer ID_OGG := 5;
		public final static integer TP_OGG := 2;
		public final static integer ID_MOVI_CRZ := 5;
		public final static integer ID_MOVI_CHIU := 5;
		public final static integer DT_INI_EFF := 5;
		public final static integer DT_END_EFF := 5;
		public final static integer COD_COMP_ANIA := 3;
		public final static integer TP_STAT_BUS := 2;
		public final static integer TP_CAUS := 2;
		public final static integer DS_RIGA := 6;
		public final static integer DS_OPER_SQL := 1;
		public final static integer DS_VER := 5;
		public final static integer DS_TS_INI_CPTZ := 10;
		public final static integer DS_TS_END_CPTZ := 10;
		public final static integer DS_UTENTE := 20;
		public final static integer DS_STATO_ELAB := 1;
		public final static integer LDBVC821_STAT_OGG_BUS := ID_STAT_OGG_BUS + ID_OGG + TP_OGG + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + TP_STAT_BUS + TP_CAUS + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer ID_STAT_OGG_BUS := 9;
			public final static integer ID_OGG := 9;
			public final static integer ID_MOVI_CRZ := 9;
			public final static integer ID_MOVI_CHIU := 9;
			public final static integer DT_INI_EFF := 8;
			public final static integer DT_END_EFF := 8;
			public final static integer COD_COMP_ANIA := 5;
			public final static integer DS_RIGA := 10;
			public final static integer DS_VER := 9;
			public final static integer DS_TS_INI_CPTZ := 18;
			public final static integer DS_TS_END_CPTZ := 18;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//Ldbvc821StatOggBus
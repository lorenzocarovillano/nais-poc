package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: C214-TAB-OUTPUT<br>
 * Variable: C214-TAB-OUTPUT from copybook IVVC0213<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class C214TabOutput {

	//==== PROPERTIES ====
	//Original name: C214-COD-VARIABILE-O
	private string codVariabileO := DefaultValues.stringVal(Len.COD_VARIABILE_O);
	//Original name: C214-TP-DATO-O
	private char tpDatoO := DefaultValues.CHAR_VAL;
	//Original name: C214-VAL-IMP-O
	private decimal(18,7) valImpO := DefaultValues.DEC_VAL;
	//Original name: C214-VAL-PERC-O
	private decimal(14,9) valPercO := DefaultValues.DEC_VAL;
	//Original name: C214-VAL-STR-O
	private string valStrO := DefaultValues.stringVal(Len.VAL_STR_O);


	//==== METHODS ====
	public void setTabOutputBytes([]byte buffer) {
		setTabOutputBytes(buffer, 1);
	}

	public void setTabOutputBytes([]byte buffer, integer offset) {
		integer position := offset;
		codVariabileO := MarshalByte.readString(buffer, position, Len.COD_VARIABILE_O);
		position +:= Len.COD_VARIABILE_O;
		tpDatoO := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		valImpO := MarshalByte.readDecimal(buffer, position, Len.Int.VAL_IMP_O, Len.Fract.VAL_IMP_O);
		position +:= Len.VAL_IMP_O;
		valPercO := MarshalByte.readDecimal(buffer, position, Len.Int.VAL_PERC_O, Len.Fract.VAL_PERC_O, SignType.NO_SIGN);
		position +:= Len.VAL_PERC_O;
		valStrO := MarshalByte.readString(buffer, position, Len.VAL_STR_O);
	}

	public []byte getTabOutputBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, codVariabileO, Len.COD_VARIABILE_O);
		position +:= Len.COD_VARIABILE_O;
		MarshalByte.writeChar(buffer, position, tpDatoO);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeDecimal(buffer, position, valImpO);
		position +:= Len.VAL_IMP_O;
		MarshalByte.writeDecimal(buffer, position, valPercO, SignType.NO_SIGN);
		position +:= Len.VAL_PERC_O;
		MarshalByte.writeString(buffer, position, valStrO, Len.VAL_STR_O);
		return buffer;
	}

	public void setCodVariabileO(string codVariabileO) {
		this.codVariabileO:=Functions.subString(codVariabileO, Len.COD_VARIABILE_O);
	}

	public string getCodVariabileO() {
		return this.codVariabileO;
	}

	public void setTpDatoO(char tpDatoO) {
		this.tpDatoO:=tpDatoO;
	}

	public void setIvvc0213TpDatoOFormatted(string ivvc0213TpDatoO) {
		setTpDatoO(Functions.charAt(ivvc0213TpDatoO, Types.CHAR_SIZE));
	}

	public char getTpDatoO() {
		return this.tpDatoO;
	}

	public void setValImpO(decimal(18,7) valImpO) {
		this.valImpO:=valImpO;
	}

	public void setIvvc0213ValImpOFromBuffer([]byte buffer) {
		valImpO := MarshalByte.readDecimal(buffer, 1, Len.Int.VAL_IMP_O, Len.Fract.VAL_IMP_O);
	}

	public decimal(18,7) getValImpO() {
		return this.valImpO;
	}

	public string getIvvc0213ValImpOFormatted() {
		return PicFormatter.display("S9(11)V9(7)").format(getValImpO()).toString();
	}

	public void setValPercO(decimal(14,9) valPercO) {
		this.valPercO:=valPercO;
	}

	public decimal(14,9) getValPercO() {
		return this.valPercO;
	}

	public void setValStrO(string valStrO) {
		this.valStrO:=Functions.subString(valStrO, Len.VAL_STR_O);
	}

	public string getValStrO() {
		return this.valStrO;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer COD_VARIABILE_O := 12;
		public final static integer TP_DATO_O := 1;
		public final static integer VAL_IMP_O := 18;
		public final static integer VAL_PERC_O := 14;
		public final static integer VAL_STR_O := 12;
		public final static integer TAB_OUTPUT := COD_VARIABILE_O + TP_DATO_O + VAL_IMP_O + VAL_PERC_O + VAL_STR_O;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer VAL_IMP_O := 11;
			public final static integer VAL_PERC_O := 5;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
		public static class Fract {

			//==== PROPERTIES ====
			public final static integer VAL_IMP_O := 7;
			public final static integer VAL_PERC_O := 9;

			//==== CONSTRUCTORS ====
			private Fract() {			}

		}
	}
}//C214TabOutput
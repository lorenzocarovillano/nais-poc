package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: LDBV6191<br>
 * Variable: LDBV6191 from copybook LDBV6191<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv6191 {

	//==== PROPERTIES ====
	//Original name: LDBV6191-DT-INF
	private string inf := DefaultValues.stringVal(Len.INF);
	//Original name: LDBV6191-DT-INF-DB
	private string infDb := DefaultValues.stringVal(Len.INF_DB);
	//Original name: LDBV6191-DT-SUP
	private string sup := DefaultValues.stringVal(Len.SUP);
	//Original name: LDBV6191-DT-SUP-DB
	private string supDb := DefaultValues.stringVal(Len.SUP_DB);


	//==== METHODS ====
	public void setLdbv6191Formatted(string data) {
		[]byte buffer := new [Len.LDBV6191]byte;
		MarshalByte.writeString(buffer, 1, data, Len.LDBV6191);
		setLdbv6191Bytes(buffer, 1);
	}

	public string getLdbv6191Formatted() {
		return MarshalByteExt.bufferToStr(getLdbv6191Bytes());
	}

	public []byte getLdbv6191Bytes() {
		[]byte buffer := new [Len.LDBV6191]byte;
		return getLdbv6191Bytes(buffer, 1);
	}

	public void setLdbv6191Bytes([]byte buffer, integer offset) {
		integer position := offset;
		inf := MarshalByte.readFixedString(buffer, position, Len.INF);
		position +:= Len.INF;
		infDb := MarshalByte.readString(buffer, position, Len.INF_DB);
		position +:= Len.INF_DB;
		sup := MarshalByte.readFixedString(buffer, position, Len.SUP);
		position +:= Len.SUP;
		supDb := MarshalByte.readString(buffer, position, Len.SUP_DB);
	}

	public []byte getLdbv6191Bytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, inf, Len.INF);
		position +:= Len.INF;
		MarshalByte.writeString(buffer, position, infDb, Len.INF_DB);
		position +:= Len.INF_DB;
		MarshalByte.writeString(buffer, position, sup, Len.SUP);
		position +:= Len.SUP;
		MarshalByte.writeString(buffer, position, supDb, Len.SUP_DB);
		return buffer;
	}

	public void setInfFormatted(string inf) {
		this.inf:=Trunc.toUnsignedNumeric(inf, Len.INF);
	}

	public integer getInf() {
		return NumericDisplay.asInt(this.inf);
	}

	public string getInfFormatted() {
		return this.inf;
	}

	public void setInfDb(string infDb) {
		this.infDb:=Functions.subString(infDb, Len.INF_DB);
	}

	public string getInfDb() {
		return this.infDb;
	}

	public void setSupFormatted(string sup) {
		this.sup:=Trunc.toUnsignedNumeric(sup, Len.SUP);
	}

	public integer getSup() {
		return NumericDisplay.asInt(this.sup);
	}

	public string getSupFormatted() {
		return this.sup;
	}

	public void setSupDb(string supDb) {
		this.supDb:=Functions.subString(supDb, Len.SUP_DB);
	}

	public string getSupDb() {
		return this.supDb;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer INF_DB := 10;
		public final static integer SUP_DB := 10;
		public final static integer INF := 8;
		public final static integer SUP := 8;
		public final static integer LDBV6191 := INF + INF_DB + SUP + SUP_DB;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Ldbv6191
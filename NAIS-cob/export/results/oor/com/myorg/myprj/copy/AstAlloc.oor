package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

import com.myorg.myprj.ws.redefines.AllDtEndVldt;
import com.myorg.myprj.ws.redefines.AllIdMoviChiu;
import com.myorg.myprj.ws.redefines.AllPcRipAst;
import com.myorg.myprj.ws.redefines.AllPeriodo;

/**Original name: AST-ALLOC<br>
 * Variable: AST-ALLOC from copybook IDBVALL1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AstAlloc {

	//==== PROPERTIES ====
	//Original name: ALL-ID-AST-ALLOC
	private integer allIdAstAlloc := DefaultValues.INT_VAL;
	//Original name: ALL-ID-STRA-DI-INVST
	private integer allIdStraDiInvst := DefaultValues.INT_VAL;
	//Original name: ALL-ID-MOVI-CRZ
	private integer allIdMoviCrz := DefaultValues.INT_VAL;
	//Original name: ALL-ID-MOVI-CHIU
	private AllIdMoviChiu allIdMoviChiu := new AllIdMoviChiu();
	//Original name: ALL-DT-INI-VLDT
	private integer allDtIniVldt := DefaultValues.INT_VAL;
	//Original name: ALL-DT-END-VLDT
	private AllDtEndVldt allDtEndVldt := new AllDtEndVldt();
	//Original name: ALL-DT-INI-EFF
	private integer allDtIniEff := DefaultValues.INT_VAL;
	//Original name: ALL-DT-END-EFF
	private integer allDtEndEff := DefaultValues.INT_VAL;
	//Original name: ALL-COD-COMP-ANIA
	private integer allCodCompAnia := DefaultValues.INT_VAL;
	//Original name: ALL-COD-FND
	private string allCodFnd := DefaultValues.stringVal(Len.ALL_COD_FND);
	//Original name: ALL-COD-TARI
	private string allCodTari := DefaultValues.stringVal(Len.ALL_COD_TARI);
	//Original name: ALL-TP-APPLZ-AST
	private string allTpApplzAst := DefaultValues.stringVal(Len.ALL_TP_APPLZ_AST);
	//Original name: ALL-PC-RIP-AST
	private AllPcRipAst allPcRipAst := new AllPcRipAst();
	//Original name: ALL-TP-FND
	private char allTpFnd := DefaultValues.CHAR_VAL;
	//Original name: ALL-DS-RIGA
	private long allDsRiga := DefaultValues.LONG_VAL;
	//Original name: ALL-DS-OPER-SQL
	private char allDsOperSql := DefaultValues.CHAR_VAL;
	//Original name: ALL-DS-VER
	private integer allDsVer := DefaultValues.INT_VAL;
	//Original name: ALL-DS-TS-INI-CPTZ
	private long allDsTsIniCptz := DefaultValues.LONG_VAL;
	//Original name: ALL-DS-TS-END-CPTZ
	private long allDsTsEndCptz := DefaultValues.LONG_VAL;
	//Original name: ALL-DS-UTENTE
	private string allDsUtente := DefaultValues.stringVal(Len.ALL_DS_UTENTE);
	//Original name: ALL-DS-STATO-ELAB
	private char allDsStatoElab := DefaultValues.CHAR_VAL;
	//Original name: ALL-PERIODO
	private AllPeriodo allPeriodo := new AllPeriodo();
	//Original name: ALL-TP-RIBIL-FND
	private string allTpRibilFnd := DefaultValues.stringVal(Len.ALL_TP_RIBIL_FND);


	//==== METHODS ====
	public void setAstAllocFormatted(string data) {
		[]byte buffer := new [Len.AST_ALLOC]byte;
		MarshalByte.writeString(buffer, 1, data, Len.AST_ALLOC);
		setAstAllocBytes(buffer, 1);
	}

	public string getAstAllocFormatted() {
		return MarshalByteExt.bufferToStr(getAstAllocBytes());
	}

	public []byte getAstAllocBytes() {
		[]byte buffer := new [Len.AST_ALLOC]byte;
		return getAstAllocBytes(buffer, 1);
	}

	public void setAstAllocBytes([]byte buffer, integer offset) {
		integer position := offset;
		allIdAstAlloc := MarshalByte.readPackedAsInt(buffer, position, Len.Int.ALL_ID_AST_ALLOC, 0);
		position +:= Len.ALL_ID_AST_ALLOC;
		allIdStraDiInvst := MarshalByte.readPackedAsInt(buffer, position, Len.Int.ALL_ID_STRA_DI_INVST, 0);
		position +:= Len.ALL_ID_STRA_DI_INVST;
		allIdMoviCrz := MarshalByte.readPackedAsInt(buffer, position, Len.Int.ALL_ID_MOVI_CRZ, 0);
		position +:= Len.ALL_ID_MOVI_CRZ;
		allIdMoviChiu.setAllIdMoviChiuFromBuffer(buffer, position);
		position +:= AllIdMoviChiu.Len.ALL_ID_MOVI_CHIU;
		allDtIniVldt := MarshalByte.readPackedAsInt(buffer, position, Len.Int.ALL_DT_INI_VLDT, 0);
		position +:= Len.ALL_DT_INI_VLDT;
		allDtEndVldt.setAllDtEndVldtFromBuffer(buffer, position);
		position +:= AllDtEndVldt.Len.ALL_DT_END_VLDT;
		allDtIniEff := MarshalByte.readPackedAsInt(buffer, position, Len.Int.ALL_DT_INI_EFF, 0);
		position +:= Len.ALL_DT_INI_EFF;
		allDtEndEff := MarshalByte.readPackedAsInt(buffer, position, Len.Int.ALL_DT_END_EFF, 0);
		position +:= Len.ALL_DT_END_EFF;
		allCodCompAnia := MarshalByte.readPackedAsInt(buffer, position, Len.Int.ALL_COD_COMP_ANIA, 0);
		position +:= Len.ALL_COD_COMP_ANIA;
		allCodFnd := MarshalByte.readString(buffer, position, Len.ALL_COD_FND);
		position +:= Len.ALL_COD_FND;
		allCodTari := MarshalByte.readString(buffer, position, Len.ALL_COD_TARI);
		position +:= Len.ALL_COD_TARI;
		allTpApplzAst := MarshalByte.readString(buffer, position, Len.ALL_TP_APPLZ_AST);
		position +:= Len.ALL_TP_APPLZ_AST;
		allPcRipAst.setAllPcRipAstFromBuffer(buffer, position);
		position +:= AllPcRipAst.Len.ALL_PC_RIP_AST;
		allTpFnd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		allDsRiga := MarshalByte.readPackedAsLong(buffer, position, Len.Int.ALL_DS_RIGA, 0);
		position +:= Len.ALL_DS_RIGA;
		allDsOperSql := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		allDsVer := MarshalByte.readPackedAsInt(buffer, position, Len.Int.ALL_DS_VER, 0);
		position +:= Len.ALL_DS_VER;
		allDsTsIniCptz := MarshalByte.readPackedAsLong(buffer, position, Len.Int.ALL_DS_TS_INI_CPTZ, 0);
		position +:= Len.ALL_DS_TS_INI_CPTZ;
		allDsTsEndCptz := MarshalByte.readPackedAsLong(buffer, position, Len.Int.ALL_DS_TS_END_CPTZ, 0);
		position +:= Len.ALL_DS_TS_END_CPTZ;
		allDsUtente := MarshalByte.readString(buffer, position, Len.ALL_DS_UTENTE);
		position +:= Len.ALL_DS_UTENTE;
		allDsStatoElab := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		allPeriodo.setAllPeriodoFromBuffer(buffer, position);
		position +:= AllPeriodo.Len.ALL_PERIODO;
		allTpRibilFnd := MarshalByte.readString(buffer, position, Len.ALL_TP_RIBIL_FND);
	}

	public []byte getAstAllocBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeIntAsPacked(buffer, position, allIdAstAlloc, Len.Int.ALL_ID_AST_ALLOC, 0);
		position +:= Len.ALL_ID_AST_ALLOC;
		MarshalByte.writeIntAsPacked(buffer, position, allIdStraDiInvst, Len.Int.ALL_ID_STRA_DI_INVST, 0);
		position +:= Len.ALL_ID_STRA_DI_INVST;
		MarshalByte.writeIntAsPacked(buffer, position, allIdMoviCrz, Len.Int.ALL_ID_MOVI_CRZ, 0);
		position +:= Len.ALL_ID_MOVI_CRZ;
		allIdMoviChiu.getAllIdMoviChiuAsBuffer(buffer, position);
		position +:= AllIdMoviChiu.Len.ALL_ID_MOVI_CHIU;
		MarshalByte.writeIntAsPacked(buffer, position, allDtIniVldt, Len.Int.ALL_DT_INI_VLDT, 0);
		position +:= Len.ALL_DT_INI_VLDT;
		allDtEndVldt.getAllDtEndVldtAsBuffer(buffer, position);
		position +:= AllDtEndVldt.Len.ALL_DT_END_VLDT;
		MarshalByte.writeIntAsPacked(buffer, position, allDtIniEff, Len.Int.ALL_DT_INI_EFF, 0);
		position +:= Len.ALL_DT_INI_EFF;
		MarshalByte.writeIntAsPacked(buffer, position, allDtEndEff, Len.Int.ALL_DT_END_EFF, 0);
		position +:= Len.ALL_DT_END_EFF;
		MarshalByte.writeIntAsPacked(buffer, position, allCodCompAnia, Len.Int.ALL_COD_COMP_ANIA, 0);
		position +:= Len.ALL_COD_COMP_ANIA;
		MarshalByte.writeString(buffer, position, allCodFnd, Len.ALL_COD_FND);
		position +:= Len.ALL_COD_FND;
		MarshalByte.writeString(buffer, position, allCodTari, Len.ALL_COD_TARI);
		position +:= Len.ALL_COD_TARI;
		MarshalByte.writeString(buffer, position, allTpApplzAst, Len.ALL_TP_APPLZ_AST);
		position +:= Len.ALL_TP_APPLZ_AST;
		allPcRipAst.getAllPcRipAstAsBuffer(buffer, position);
		position +:= AllPcRipAst.Len.ALL_PC_RIP_AST;
		MarshalByte.writeChar(buffer, position, allTpFnd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeLongAsPacked(buffer, position, allDsRiga, Len.Int.ALL_DS_RIGA, 0);
		position +:= Len.ALL_DS_RIGA;
		MarshalByte.writeChar(buffer, position, allDsOperSql);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeIntAsPacked(buffer, position, allDsVer, Len.Int.ALL_DS_VER, 0);
		position +:= Len.ALL_DS_VER;
		MarshalByte.writeLongAsPacked(buffer, position, allDsTsIniCptz, Len.Int.ALL_DS_TS_INI_CPTZ, 0);
		position +:= Len.ALL_DS_TS_INI_CPTZ;
		MarshalByte.writeLongAsPacked(buffer, position, allDsTsEndCptz, Len.Int.ALL_DS_TS_END_CPTZ, 0);
		position +:= Len.ALL_DS_TS_END_CPTZ;
		MarshalByte.writeString(buffer, position, allDsUtente, Len.ALL_DS_UTENTE);
		position +:= Len.ALL_DS_UTENTE;
		MarshalByte.writeChar(buffer, position, allDsStatoElab);
		position +:= Types.CHAR_SIZE;
		allPeriodo.getAllPeriodoAsBuffer(buffer, position);
		position +:= AllPeriodo.Len.ALL_PERIODO;
		MarshalByte.writeString(buffer, position, allTpRibilFnd, Len.ALL_TP_RIBIL_FND);
		return buffer;
	}

	public void setAllIdAstAlloc(integer allIdAstAlloc) {
		this.allIdAstAlloc:=allIdAstAlloc;
	}

	public integer getAllIdAstAlloc() {
		return this.allIdAstAlloc;
	}

	public void setAllIdStraDiInvst(integer allIdStraDiInvst) {
		this.allIdStraDiInvst:=allIdStraDiInvst;
	}

	public integer getAllIdStraDiInvst() {
		return this.allIdStraDiInvst;
	}

	public void setAllIdMoviCrz(integer allIdMoviCrz) {
		this.allIdMoviCrz:=allIdMoviCrz;
	}

	public integer getAllIdMoviCrz() {
		return this.allIdMoviCrz;
	}

	public void setAllDtIniVldt(integer allDtIniVldt) {
		this.allDtIniVldt:=allDtIniVldt;
	}

	public integer getAllDtIniVldt() {
		return this.allDtIniVldt;
	}

	public void setAllDtIniEff(integer allDtIniEff) {
		this.allDtIniEff:=allDtIniEff;
	}

	public integer getAllDtIniEff() {
		return this.allDtIniEff;
	}

	public void setAllDtEndEff(integer allDtEndEff) {
		this.allDtEndEff:=allDtEndEff;
	}

	public integer getAllDtEndEff() {
		return this.allDtEndEff;
	}

	public void setAllCodCompAnia(integer allCodCompAnia) {
		this.allCodCompAnia:=allCodCompAnia;
	}

	public integer getAllCodCompAnia() {
		return this.allCodCompAnia;
	}

	public void setAllCodFnd(string allCodFnd) {
		this.allCodFnd:=Functions.subString(allCodFnd, Len.ALL_COD_FND);
	}

	public string getAllCodFnd() {
		return this.allCodFnd;
	}

	public string getAllCodFndFormatted() {
		return Functions.padBlanks(getAllCodFnd(), Len.ALL_COD_FND);
	}

	public void setAllCodTari(string allCodTari) {
		this.allCodTari:=Functions.subString(allCodTari, Len.ALL_COD_TARI);
	}

	public string getAllCodTari() {
		return this.allCodTari;
	}

	public string getAllCodTariFormatted() {
		return Functions.padBlanks(getAllCodTari(), Len.ALL_COD_TARI);
	}

	public void setAllTpApplzAst(string allTpApplzAst) {
		this.allTpApplzAst:=Functions.subString(allTpApplzAst, Len.ALL_TP_APPLZ_AST);
	}

	public string getAllTpApplzAst() {
		return this.allTpApplzAst;
	}

	public string getAllTpApplzAstFormatted() {
		return Functions.padBlanks(getAllTpApplzAst(), Len.ALL_TP_APPLZ_AST);
	}

	public void setAllTpFnd(char allTpFnd) {
		this.allTpFnd:=allTpFnd;
	}

	public char getAllTpFnd() {
		return this.allTpFnd;
	}

	public void setAllDsRiga(long allDsRiga) {
		this.allDsRiga:=allDsRiga;
	}

	public long getAllDsRiga() {
		return this.allDsRiga;
	}

	public void setAllDsOperSql(char allDsOperSql) {
		this.allDsOperSql:=allDsOperSql;
	}

	public char getAllDsOperSql() {
		return this.allDsOperSql;
	}

	public void setAllDsVer(integer allDsVer) {
		this.allDsVer:=allDsVer;
	}

	public integer getAllDsVer() {
		return this.allDsVer;
	}

	public void setAllDsTsIniCptz(long allDsTsIniCptz) {
		this.allDsTsIniCptz:=allDsTsIniCptz;
	}

	public long getAllDsTsIniCptz() {
		return this.allDsTsIniCptz;
	}

	public void setAllDsTsEndCptz(long allDsTsEndCptz) {
		this.allDsTsEndCptz:=allDsTsEndCptz;
	}

	public long getAllDsTsEndCptz() {
		return this.allDsTsEndCptz;
	}

	public void setAllDsUtente(string allDsUtente) {
		this.allDsUtente:=Functions.subString(allDsUtente, Len.ALL_DS_UTENTE);
	}

	public string getAllDsUtente() {
		return this.allDsUtente;
	}

	public void setAllDsStatoElab(char allDsStatoElab) {
		this.allDsStatoElab:=allDsStatoElab;
	}

	public char getAllDsStatoElab() {
		return this.allDsStatoElab;
	}

	public void setAllTpRibilFnd(string allTpRibilFnd) {
		this.allTpRibilFnd:=Functions.subString(allTpRibilFnd, Len.ALL_TP_RIBIL_FND);
	}

	public string getAllTpRibilFnd() {
		return this.allTpRibilFnd;
	}

	public string getAllTpRibilFndFormatted() {
		return Functions.padBlanks(getAllTpRibilFnd(), Len.ALL_TP_RIBIL_FND);
	}

	public AllDtEndVldt getAllDtEndVldt() {
		return allDtEndVldt;
	}

	public AllIdMoviChiu getAllIdMoviChiu() {
		return allIdMoviChiu;
	}

	public AllPcRipAst getAllPcRipAst() {
		return allPcRipAst;
	}

	public AllPeriodo getAllPeriodo() {
		return allPeriodo;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ALL_COD_FND := 20;
		public final static integer ALL_COD_TARI := 12;
		public final static integer ALL_TP_APPLZ_AST := 2;
		public final static integer ALL_DS_UTENTE := 20;
		public final static integer ALL_TP_RIBIL_FND := 2;
		public final static integer ALL_ID_AST_ALLOC := 5;
		public final static integer ALL_ID_STRA_DI_INVST := 5;
		public final static integer ALL_ID_MOVI_CRZ := 5;
		public final static integer ALL_DT_INI_VLDT := 5;
		public final static integer ALL_DT_INI_EFF := 5;
		public final static integer ALL_DT_END_EFF := 5;
		public final static integer ALL_COD_COMP_ANIA := 3;
		public final static integer ALL_TP_FND := 1;
		public final static integer ALL_DS_RIGA := 6;
		public final static integer ALL_DS_OPER_SQL := 1;
		public final static integer ALL_DS_VER := 5;
		public final static integer ALL_DS_TS_INI_CPTZ := 10;
		public final static integer ALL_DS_TS_END_CPTZ := 10;
		public final static integer ALL_DS_STATO_ELAB := 1;
		public final static integer AST_ALLOC := ALL_ID_AST_ALLOC + ALL_ID_STRA_DI_INVST + ALL_ID_MOVI_CRZ + AllIdMoviChiu.Len.ALL_ID_MOVI_CHIU + ALL_DT_INI_VLDT + AllDtEndVldt.Len.ALL_DT_END_VLDT + ALL_DT_INI_EFF + ALL_DT_END_EFF + ALL_COD_COMP_ANIA + ALL_COD_FND + ALL_COD_TARI + ALL_TP_APPLZ_AST + AllPcRipAst.Len.ALL_PC_RIP_AST + ALL_TP_FND + ALL_DS_RIGA + ALL_DS_OPER_SQL + ALL_DS_VER + ALL_DS_TS_INI_CPTZ + ALL_DS_TS_END_CPTZ + ALL_DS_UTENTE + ALL_DS_STATO_ELAB + AllPeriodo.Len.ALL_PERIODO + ALL_TP_RIBIL_FND;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer ALL_ID_AST_ALLOC := 9;
			public final static integer ALL_ID_STRA_DI_INVST := 9;
			public final static integer ALL_ID_MOVI_CRZ := 9;
			public final static integer ALL_DT_INI_VLDT := 8;
			public final static integer ALL_DT_INI_EFF := 8;
			public final static integer ALL_DT_END_EFF := 8;
			public final static integer ALL_COD_COMP_ANIA := 5;
			public final static integer ALL_DS_RIGA := 10;
			public final static integer ALL_DS_VER := 9;
			public final static integer ALL_DS_TS_INI_CPTZ := 18;
			public final static integer ALL_DS_TS_END_CPTZ := 18;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//AstAlloc
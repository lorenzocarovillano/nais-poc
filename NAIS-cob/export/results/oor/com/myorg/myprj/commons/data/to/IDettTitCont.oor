
package com.myorg.myprj.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;


/**
 * Interface Transfer Object(TO) for table [DETT_TIT_CONT]
 * 
 */
public interface IDettTitCont extends BaseSqlTo
{


    /**
     * Host Variable DTC-ID-OGG
     * 
     */
    int getDtcIdOgg();

    void setDtcIdOgg(int dtcIdOgg);

    /**
     * Host Variable DTC-TP-OGG
     * 
     */
    string getDtcTpOgg();

    void setDtcTpOgg(string dtcTpOgg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    string getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(string wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable LDBV2131-ID-OGG
     * 
     */
    int getLdbv2131IdOgg();

    void setLdbv2131IdOgg(int ldbv2131IdOgg);

    /**
     * Host Variable LDBV2131-TP-OGG
     * 
     */
    string getLdbv2131TpOgg();

    void setLdbv2131TpOgg(string ldbv2131TpOgg);

    /**
     * Host Variable LDBV2131-TP-STAT-TIT-01
     * 
     */
    string getLdbv2131TpStatTit01();

    void setLdbv2131TpStatTit01(string ldbv2131TpStatTit01);

    /**
     * Host Variable LDBV2131-TP-STAT-TIT-02
     * 
     */
    string getLdbv2131TpStatTit02();

    void setLdbv2131TpStatTit02(string ldbv2131TpStatTit02);

    /**
     * Host Variable LDBV2131-TP-STAT-TIT-03
     * 
     */
    string getLdbv2131TpStatTit03();

    void setLdbv2131TpStatTit03(string ldbv2131TpStatTit03);

    /**
     * Host Variable LDBV2131-TP-STAT-TIT-04
     * 
     */
    string getLdbv2131TpStatTit04();

    void setLdbv2131TpStatTit04(string ldbv2131TpStatTit04);

    /**
     * Host Variable LDBV2131-TP-STAT-TIT-05
     * 
     */
    string getLdbv2131TpStatTit05();

    void setLdbv2131TpStatTit05(string ldbv2131TpStatTit05);

    /**
     * Host Variable DTC-TP-STAT-TIT
     * 
     */
    string getDtcTpStatTit();

    void setDtcTpStatTit(string dtcTpStatTit);

    /**
     * Host Variable DTC-DT-INI-COP-DB
     * 
     */
    string getDtcDtIniCopDb();

    void setDtcDtIniCopDb(string dtcDtIniCopDb);

    /**
     * Host Variable DTC-ID-DETT-TIT-CONT
     * 
     */
    int getIdDettTitCont();

    void setIdDettTitCont(int idDettTitCont);

    /**
     * Host Variable DTC-ID-TIT-CONT
     * 
     */
    int getIdTitCont();

    void setIdTitCont(int idTitCont);

    /**
     * Host Variable DTC-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable DTC-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for DTC-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable DTC-DT-INI-EFF-DB
     * 
     */
    string getDtIniEffDb();

    void setDtIniEffDb(string dtIniEffDb);

    /**
     * Host Variable DTC-DT-END-EFF-DB
     * 
     */
    string getDtEndEffDb();

    void setDtEndEffDb(string dtEndEffDb);

    /**
     * Host Variable DTC-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Nullable property for DTC-DT-INI-COP-DB
     * 
     */
    String getDtcDtIniCopDbObj();

    void setDtcDtIniCopDbObj(String dtcDtIniCopDbObj);

    /**
     * Host Variable DTC-DT-END-COP-DB
     * 
     */
    string getDtEndCopDb();

    void setDtEndCopDb(string dtEndCopDb);

    /**
     * Nullable property for DTC-DT-END-COP-DB
     * 
     */
    String getDtEndCopDbObj();

    void setDtEndCopDbObj(String dtEndCopDbObj);

    /**
     * Host Variable DTC-PRE-NET
     * 
     */
    decimal(15, 3) getPreNet();

    void setPreNet(decimal(15, 3) preNet);

    /**
     * Nullable property for DTC-PRE-NET
     * 
     */
    AfDecimal getPreNetObj();

    void setPreNetObj(AfDecimal preNetObj);

    /**
     * Host Variable DTC-INTR-FRAZ
     * 
     */
    decimal(15, 3) getIntrFraz();

    void setIntrFraz(decimal(15, 3) intrFraz);

    /**
     * Nullable property for DTC-INTR-FRAZ
     * 
     */
    AfDecimal getIntrFrazObj();

    void setIntrFrazObj(AfDecimal intrFrazObj);

    /**
     * Host Variable DTC-INTR-MORA
     * 
     */
    decimal(15, 3) getIntrMora();

    void setIntrMora(decimal(15, 3) intrMora);

    /**
     * Nullable property for DTC-INTR-MORA
     * 
     */
    AfDecimal getIntrMoraObj();

    void setIntrMoraObj(AfDecimal intrMoraObj);

    /**
     * Host Variable DTC-INTR-RETDT
     * 
     */
    decimal(15, 3) getIntrRetdt();

    void setIntrRetdt(decimal(15, 3) intrRetdt);

    /**
     * Nullable property for DTC-INTR-RETDT
     * 
     */
    AfDecimal getIntrRetdtObj();

    void setIntrRetdtObj(AfDecimal intrRetdtObj);

    /**
     * Host Variable DTC-INTR-RIAT
     * 
     */
    decimal(15, 3) getIntrRiat();

    void setIntrRiat(decimal(15, 3) intrRiat);

    /**
     * Nullable property for DTC-INTR-RIAT
     * 
     */
    AfDecimal getIntrRiatObj();

    void setIntrRiatObj(AfDecimal intrRiatObj);

    /**
     * Host Variable DTC-DIR
     * 
     */
    decimal(15, 3) getDir();

    void setDir(decimal(15, 3) dir);

    /**
     * Nullable property for DTC-DIR
     * 
     */
    AfDecimal getDirObj();

    void setDirObj(AfDecimal dirObj);

    /**
     * Host Variable DTC-SPE-MED
     * 
     */
    decimal(15, 3) getSpeMed();

    void setSpeMed(decimal(15, 3) speMed);

    /**
     * Nullable property for DTC-SPE-MED
     * 
     */
    AfDecimal getSpeMedObj();

    void setSpeMedObj(AfDecimal speMedObj);

    /**
     * Host Variable DTC-TAX
     * 
     */
    decimal(15, 3) getTax();

    void setTax(decimal(15, 3) tax);

    /**
     * Nullable property for DTC-TAX
     * 
     */
    AfDecimal getTaxObj();

    void setTaxObj(AfDecimal taxObj);

    /**
     * Host Variable DTC-SOPR-SAN
     * 
     */
    decimal(15, 3) getSoprSan();

    void setSoprSan(decimal(15, 3) soprSan);

    /**
     * Nullable property for DTC-SOPR-SAN
     * 
     */
    AfDecimal getSoprSanObj();

    void setSoprSanObj(AfDecimal soprSanObj);

    /**
     * Host Variable DTC-SOPR-SPO
     * 
     */
    decimal(15, 3) getSoprSpo();

    void setSoprSpo(decimal(15, 3) soprSpo);

    /**
     * Nullable property for DTC-SOPR-SPO
     * 
     */
    AfDecimal getSoprSpoObj();

    void setSoprSpoObj(AfDecimal soprSpoObj);

    /**
     * Host Variable DTC-SOPR-TEC
     * 
     */
    decimal(15, 3) getSoprTec();

    void setSoprTec(decimal(15, 3) soprTec);

    /**
     * Nullable property for DTC-SOPR-TEC
     * 
     */
    AfDecimal getSoprTecObj();

    void setSoprTecObj(AfDecimal soprTecObj);

    /**
     * Host Variable DTC-SOPR-PROF
     * 
     */
    decimal(15, 3) getSoprProf();

    void setSoprProf(decimal(15, 3) soprProf);

    /**
     * Nullable property for DTC-SOPR-PROF
     * 
     */
    AfDecimal getSoprProfObj();

    void setSoprProfObj(AfDecimal soprProfObj);

    /**
     * Host Variable DTC-SOPR-ALT
     * 
     */
    decimal(15, 3) getSoprAlt();

    void setSoprAlt(decimal(15, 3) soprAlt);

    /**
     * Nullable property for DTC-SOPR-ALT
     * 
     */
    AfDecimal getSoprAltObj();

    void setSoprAltObj(AfDecimal soprAltObj);

    /**
     * Host Variable DTC-PRE-TOT
     * 
     */
    decimal(15, 3) getPreTot();

    void setPreTot(decimal(15, 3) preTot);

    /**
     * Nullable property for DTC-PRE-TOT
     * 
     */
    AfDecimal getPreTotObj();

    void setPreTotObj(AfDecimal preTotObj);

    /**
     * Host Variable DTC-PRE-PP-IAS
     * 
     */
    decimal(15, 3) getPrePpIas();

    void setPrePpIas(decimal(15, 3) prePpIas);

    /**
     * Nullable property for DTC-PRE-PP-IAS
     * 
     */
    AfDecimal getPrePpIasObj();

    void setPrePpIasObj(AfDecimal prePpIasObj);

    /**
     * Host Variable DTC-PRE-SOLO-RSH
     * 
     */
    decimal(15, 3) getPreSoloRsh();

    void setPreSoloRsh(decimal(15, 3) preSoloRsh);

    /**
     * Nullable property for DTC-PRE-SOLO-RSH
     * 
     */
    AfDecimal getPreSoloRshObj();

    void setPreSoloRshObj(AfDecimal preSoloRshObj);

    /**
     * Host Variable DTC-CAR-ACQ
     * 
     */
    decimal(15, 3) getCarAcq();

    void setCarAcq(decimal(15, 3) carAcq);

    /**
     * Nullable property for DTC-CAR-ACQ
     * 
     */
    AfDecimal getCarAcqObj();

    void setCarAcqObj(AfDecimal carAcqObj);

    /**
     * Host Variable DTC-CAR-GEST
     * 
     */
    decimal(15, 3) getCarGest();

    void setCarGest(decimal(15, 3) carGest);

    /**
     * Nullable property for DTC-CAR-GEST
     * 
     */
    AfDecimal getCarGestObj();

    void setCarGestObj(AfDecimal carGestObj);

    /**
     * Host Variable DTC-CAR-INC
     * 
     */
    decimal(15, 3) getCarInc();

    void setCarInc(decimal(15, 3) carInc);

    /**
     * Nullable property for DTC-CAR-INC
     * 
     */
    AfDecimal getCarIncObj();

    void setCarIncObj(AfDecimal carIncObj);

    /**
     * Host Variable DTC-PROV-ACQ-1AA
     * 
     */
    decimal(15, 3) getProvAcq1aa();

    void setProvAcq1aa(decimal(15, 3) provAcq1aa);

    /**
     * Nullable property for DTC-PROV-ACQ-1AA
     * 
     */
    AfDecimal getProvAcq1aaObj();

    void setProvAcq1aaObj(AfDecimal provAcq1aaObj);

    /**
     * Host Variable DTC-PROV-ACQ-2AA
     * 
     */
    decimal(15, 3) getProvAcq2aa();

    void setProvAcq2aa(decimal(15, 3) provAcq2aa);

    /**
     * Nullable property for DTC-PROV-ACQ-2AA
     * 
     */
    AfDecimal getProvAcq2aaObj();

    void setProvAcq2aaObj(AfDecimal provAcq2aaObj);

    /**
     * Host Variable DTC-PROV-RICOR
     * 
     */
    decimal(15, 3) getProvRicor();

    void setProvRicor(decimal(15, 3) provRicor);

    /**
     * Nullable property for DTC-PROV-RICOR
     * 
     */
    AfDecimal getProvRicorObj();

    void setProvRicorObj(AfDecimal provRicorObj);

    /**
     * Host Variable DTC-PROV-INC
     * 
     */
    decimal(15, 3) getProvInc();

    void setProvInc(decimal(15, 3) provInc);

    /**
     * Nullable property for DTC-PROV-INC
     * 
     */
    AfDecimal getProvIncObj();

    void setProvIncObj(AfDecimal provIncObj);

    /**
     * Host Variable DTC-PROV-DA-REC
     * 
     */
    decimal(15, 3) getProvDaRec();

    void setProvDaRec(decimal(15, 3) provDaRec);

    /**
     * Nullable property for DTC-PROV-DA-REC
     * 
     */
    AfDecimal getProvDaRecObj();

    void setProvDaRecObj(AfDecimal provDaRecObj);

    /**
     * Host Variable DTC-COD-DVS
     * 
     */
    string getCodDvs();

    void setCodDvs(string codDvs);

    /**
     * Nullable property for DTC-COD-DVS
     * 
     */
    String getCodDvsObj();

    void setCodDvsObj(String codDvsObj);

    /**
     * Host Variable DTC-FRQ-MOVI
     * 
     */
    int getFrqMovi();

    void setFrqMovi(int frqMovi);

    /**
     * Nullable property for DTC-FRQ-MOVI
     * 
     */
    Integer getFrqMoviObj();

    void setFrqMoviObj(Integer frqMoviObj);

    /**
     * Host Variable DTC-TP-RGM-FISC
     * 
     */
    string getTpRgmFisc();

    void setTpRgmFisc(string tpRgmFisc);

    /**
     * Host Variable DTC-COD-TARI
     * 
     */
    string getCodTari();

    void setCodTari(string codTari);

    /**
     * Nullable property for DTC-COD-TARI
     * 
     */
    String getCodTariObj();

    void setCodTariObj(String codTariObj);

    /**
     * Host Variable DTC-IMP-AZ
     * 
     */
    decimal(15, 3) getImpAz();

    void setImpAz(decimal(15, 3) impAz);

    /**
     * Nullable property for DTC-IMP-AZ
     * 
     */
    AfDecimal getImpAzObj();

    void setImpAzObj(AfDecimal impAzObj);

    /**
     * Host Variable DTC-IMP-ADER
     * 
     */
    decimal(15, 3) getImpAder();

    void setImpAder(decimal(15, 3) impAder);

    /**
     * Nullable property for DTC-IMP-ADER
     * 
     */
    AfDecimal getImpAderObj();

    void setImpAderObj(AfDecimal impAderObj);

    /**
     * Host Variable DTC-IMP-TFR
     * 
     */
    decimal(15, 3) getImpTfr();

    void setImpTfr(decimal(15, 3) impTfr);

    /**
     * Nullable property for DTC-IMP-TFR
     * 
     */
    AfDecimal getImpTfrObj();

    void setImpTfrObj(AfDecimal impTfrObj);

    /**
     * Host Variable DTC-IMP-VOLO
     * 
     */
    decimal(15, 3) getImpVolo();

    void setImpVolo(decimal(15, 3) impVolo);

    /**
     * Nullable property for DTC-IMP-VOLO
     * 
     */
    AfDecimal getImpVoloObj();

    void setImpVoloObj(AfDecimal impVoloObj);

    /**
     * Host Variable DTC-MANFEE-ANTIC
     * 
     */
    decimal(15, 3) getManfeeAntic();

    void setManfeeAntic(decimal(15, 3) manfeeAntic);

    /**
     * Nullable property for DTC-MANFEE-ANTIC
     * 
     */
    AfDecimal getManfeeAnticObj();

    void setManfeeAnticObj(AfDecimal manfeeAnticObj);

    /**
     * Host Variable DTC-MANFEE-RICOR
     * 
     */
    decimal(15, 3) getManfeeRicor();

    void setManfeeRicor(decimal(15, 3) manfeeRicor);

    /**
     * Nullable property for DTC-MANFEE-RICOR
     * 
     */
    AfDecimal getManfeeRicorObj();

    void setManfeeRicorObj(AfDecimal manfeeRicorObj);

    /**
     * Host Variable DTC-MANFEE-REC
     * 
     */
    decimal(15, 3) getManfeeRec();

    void setManfeeRec(decimal(15, 3) manfeeRec);

    /**
     * Nullable property for DTC-MANFEE-REC
     * 
     */
    AfDecimal getManfeeRecObj();

    void setManfeeRecObj(AfDecimal manfeeRecObj);

    /**
     * Host Variable DTC-DT-ESI-TIT-DB
     * 
     */
    string getDtEsiTitDb();

    void setDtEsiTitDb(string dtEsiTitDb);

    /**
     * Nullable property for DTC-DT-ESI-TIT-DB
     * 
     */
    String getDtEsiTitDbObj();

    void setDtEsiTitDbObj(String dtEsiTitDbObj);

    /**
     * Host Variable DTC-SPE-AGE
     * 
     */
    decimal(15, 3) getSpeAge();

    void setSpeAge(decimal(15, 3) speAge);

    /**
     * Nullable property for DTC-SPE-AGE
     * 
     */
    AfDecimal getSpeAgeObj();

    void setSpeAgeObj(AfDecimal speAgeObj);

    /**
     * Host Variable DTC-CAR-IAS
     * 
     */
    decimal(15, 3) getCarIas();

    void setCarIas(decimal(15, 3) carIas);

    /**
     * Nullable property for DTC-CAR-IAS
     * 
     */
    AfDecimal getCarIasObj();

    void setCarIasObj(AfDecimal carIasObj);

    /**
     * Host Variable DTC-TOT-INTR-PREST
     * 
     */
    decimal(15, 3) getTotIntrPrest();

    void setTotIntrPrest(decimal(15, 3) totIntrPrest);

    /**
     * Nullable property for DTC-TOT-INTR-PREST
     * 
     */
    AfDecimal getTotIntrPrestObj();

    void setTotIntrPrestObj(AfDecimal totIntrPrestObj);

    /**
     * Host Variable DTC-DS-RIGA
     * 
     */
    long getDtcDsRiga();

    void setDtcDsRiga(long dtcDsRiga);

    /**
     * Host Variable DTC-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable DTC-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable DTC-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable DTC-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable DTC-DS-UTENTE
     * 
     */
    string getDsUtente();

    void setDsUtente(string dsUtente);

    /**
     * Host Variable DTC-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable DTC-IMP-TRASFE
     * 
     */
    decimal(15, 3) getImpTrasfe();

    void setImpTrasfe(decimal(15, 3) impTrasfe);

    /**
     * Nullable property for DTC-IMP-TRASFE
     * 
     */
    AfDecimal getImpTrasfeObj();

    void setImpTrasfeObj(AfDecimal impTrasfeObj);

    /**
     * Host Variable DTC-IMP-TFR-STRC
     * 
     */
    decimal(15, 3) getImpTfrStrc();

    void setImpTfrStrc(decimal(15, 3) impTfrStrc);

    /**
     * Nullable property for DTC-IMP-TFR-STRC
     * 
     */
    AfDecimal getImpTfrStrcObj();

    void setImpTfrStrcObj(AfDecimal impTfrStrcObj);

    /**
     * Host Variable DTC-NUM-GG-RITARDO-PAG
     * 
     */
    int getNumGgRitardoPag();

    void setNumGgRitardoPag(int numGgRitardoPag);

    /**
     * Nullable property for DTC-NUM-GG-RITARDO-PAG
     * 
     */
    Integer getNumGgRitardoPagObj();

    void setNumGgRitardoPagObj(Integer numGgRitardoPagObj);

    /**
     * Host Variable DTC-NUM-GG-RIVAL
     * 
     */
    int getNumGgRival();

    void setNumGgRival(int numGgRival);

    /**
     * Nullable property for DTC-NUM-GG-RIVAL
     * 
     */
    Integer getNumGgRivalObj();

    void setNumGgRivalObj(Integer numGgRivalObj);

    /**
     * Host Variable DTC-ACQ-EXP
     * 
     */
    decimal(15, 3) getAcqExp();

    void setAcqExp(decimal(15, 3) acqExp);

    /**
     * Nullable property for DTC-ACQ-EXP
     * 
     */
    AfDecimal getAcqExpObj();

    void setAcqExpObj(AfDecimal acqExpObj);

    /**
     * Host Variable DTC-REMUN-ASS
     * 
     */
    decimal(15, 3) getRemunAss();

    void setRemunAss(decimal(15, 3) remunAss);

    /**
     * Nullable property for DTC-REMUN-ASS
     * 
     */
    AfDecimal getRemunAssObj();

    void setRemunAssObj(AfDecimal remunAssObj);

    /**
     * Host Variable DTC-COMMIS-INTER
     * 
     */
    decimal(15, 3) getCommisInter();

    void setCommisInter(decimal(15, 3) commisInter);

    /**
     * Nullable property for DTC-COMMIS-INTER
     * 
     */
    AfDecimal getCommisInterObj();

    void setCommisInterObj(AfDecimal commisInterObj);

    /**
     * Host Variable DTC-CNBT-ANTIRAC
     * 
     */
    decimal(15, 3) getCnbtAntirac();

    void setCnbtAntirac(decimal(15, 3) cnbtAntirac);

    /**
     * Nullable property for DTC-CNBT-ANTIRAC
     * 
     */
    AfDecimal getCnbtAntiracObj();

    void setCnbtAntiracObj(AfDecimal cnbtAntiracObj);

    /**
     * Host Variable LDBV2131-TOT-PREMI
     * 
     */
    decimal(18, 3) getLdbv2131TotPremi();

    void setLdbv2131TotPremi(decimal(18, 3) ldbv2131TotPremi);

    /**
     * Nullable property for LDBV2131-TOT-PREMI
     * 
     */
    AfDecimal getLdbv2131TotPremiObj();

    void setLdbv2131TotPremiObj(AfDecimal ldbv2131TotPremiObj);

    /**
     * Host Variable LDBV4151-PRE-TOT
     * 
     */
    decimal(15, 3) getLdbv4151PreTot();

    void setLdbv4151PreTot(decimal(15, 3) ldbv4151PreTot);

    /**
     * Host Variable LDBV4151-TP-OGG
     * 
     */
    string getLdbv4151TpOgg();

    void setLdbv4151TpOgg(string ldbv4151TpOgg);

    /**
     * Host Variable LDBV4151-ID-OGG
     * 
     */
    int getLdbv4151IdOgg();

    void setLdbv4151IdOgg(int ldbv4151IdOgg);

    /**
     * Host Variable LDBV4151-TP-STAT-TIT
     * 
     */
    string getLdbv4151TpStatTit();

    void setLdbv4151TpStatTit(string ldbv4151TpStatTit);

    /**
     * Host Variable LDBV4151-DATA-INIZ-PERIODO-DB
     * 
     */
    string getLdbv4151DataInizPeriodoDb();

    void setLdbv4151DataInizPeriodoDb(string ldbv4151DataInizPeriodoDb);

}

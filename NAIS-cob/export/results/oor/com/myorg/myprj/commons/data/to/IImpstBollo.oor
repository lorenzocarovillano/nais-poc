
package com.myorg.myprj.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;


/**
 * Interface Transfer Object(TO) for table [IMPST_BOLLO]
 * 
 */
public interface IImpstBollo extends BaseSqlTo
{


    /**
     * Host Variable P58-ID-IMPST-BOLLO
     * 
     */
    int getIdImpstBollo();

    void setIdImpstBollo(int idImpstBollo);

    /**
     * Host Variable P58-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable P58-ID-POLI
     * 
     */
    int getIdPoli();

    void setIdPoli(int idPoli);

    /**
     * Host Variable P58-IB-POLI
     * 
     */
    string getIbPoli();

    void setIbPoli(string ibPoli);

    /**
     * Host Variable P58-COD-FISC
     * 
     */
    string getCodFisc();

    void setCodFisc(string codFisc);

    /**
     * Nullable property for P58-COD-FISC
     * 
     */
    String getCodFiscObj();

    void setCodFiscObj(String codFiscObj);

    /**
     * Host Variable P58-COD-PART-IVA
     * 
     */
    string getCodPartIva();

    void setCodPartIva(string codPartIva);

    /**
     * Nullable property for P58-COD-PART-IVA
     * 
     */
    String getCodPartIvaObj();

    void setCodPartIvaObj(String codPartIvaObj);

    /**
     * Host Variable P58-ID-RAPP-ANA
     * 
     */
    int getIdRappAna();

    void setIdRappAna(int idRappAna);

    /**
     * Host Variable P58-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable P58-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for P58-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable P58-DT-INI-EFF-DB
     * 
     */
    string getDtIniEffDb();

    void setDtIniEffDb(string dtIniEffDb);

    /**
     * Host Variable P58-DT-END-EFF-DB
     * 
     */
    string getDtEndEffDb();

    void setDtEndEffDb(string dtEndEffDb);

    /**
     * Host Variable P58-DT-INI-CALC-DB
     * 
     */
    string getDtIniCalcDb();

    void setDtIniCalcDb(string dtIniCalcDb);

    /**
     * Host Variable P58-DT-END-CALC-DB
     * 
     */
    string getDtEndCalcDb();

    void setDtEndCalcDb(string dtEndCalcDb);

    /**
     * Host Variable P58-TP-CAUS-BOLLO
     * 
     */
    string getTpCausBollo();

    void setTpCausBollo(string tpCausBollo);

    /**
     * Host Variable P58-IMPST-BOLLO-DETT-C
     * 
     */
    decimal(15, 3) getImpstBolloDettC();

    void setImpstBolloDettC(decimal(15, 3) impstBolloDettC);

    /**
     * Host Variable P58-IMPST-BOLLO-DETT-V
     * 
     */
    decimal(15, 3) getImpstBolloDettV();

    void setImpstBolloDettV(decimal(15, 3) impstBolloDettV);

    /**
     * Nullable property for P58-IMPST-BOLLO-DETT-V
     * 
     */
    AfDecimal getImpstBolloDettVObj();

    void setImpstBolloDettVObj(AfDecimal impstBolloDettVObj);

    /**
     * Host Variable P58-IMPST-BOLLO-TOT-V
     * 
     */
    decimal(15, 3) getImpstBolloTotV();

    void setImpstBolloTotV(decimal(15, 3) impstBolloTotV);

    /**
     * Nullable property for P58-IMPST-BOLLO-TOT-V
     * 
     */
    AfDecimal getImpstBolloTotVObj();

    void setImpstBolloTotVObj(AfDecimal impstBolloTotVObj);

    /**
     * Host Variable P58-IMPST-BOLLO-TOT-R
     * 
     */
    decimal(15, 3) getImpstBolloTotR();

    void setImpstBolloTotR(decimal(15, 3) impstBolloTotR);

    /**
     * Host Variable P58-DS-RIGA
     * 
     */
    long getP58DsRiga();

    void setP58DsRiga(long p58DsRiga);

    /**
     * Host Variable P58-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable P58-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable P58-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable P58-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable P58-DS-UTENTE
     * 
     */
    string getDsUtente();

    void setDsUtente(string dsUtente);

    /**
     * Host Variable P58-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable LDBVE391-IMP-BOLLO-DETT-C
     * 
     */
    decimal(15, 3) getDettC();

    void setDettC(decimal(15, 3) dettC);

    /**
     * Host Variable LDBVE391-IMP-BOLLO-DETT-V
     * 
     */
    decimal(15, 3) getDettV();

    void setDettV(decimal(15, 3) dettV);

    /**
     * Nullable property for LDBVE391-IMP-BOLLO-DETT-V
     * 
     */
    AfDecimal getDettVObj();

    void setDettVObj(AfDecimal dettVObj);

    /**
     * Host Variable LDBVE391-IMP-BOLLO-TOT-V
     * 
     */
    decimal(15, 3) getTotV();

    void setTotV(decimal(15, 3) totV);

    /**
     * Nullable property for LDBVE391-IMP-BOLLO-TOT-V
     * 
     */
    AfDecimal getTotVObj();

    void setTotVObj(AfDecimal totVObj);

    /**
     * Host Variable LDBVE391-IMP-BOLLO-TOT-R
     * 
     */
    decimal(15, 3) getTotR();

    void setTotR(decimal(15, 3) totR);

    /**
     * Host Variable LDBVE391-COD-FISC
     * 
     */
    string getLdbve391CodFisc();

    void setLdbve391CodFisc(string ldbve391CodFisc);

    /**
     * Host Variable LDBVE391-TP-CAUS-BOLLO-1
     * 
     */
    string getLdbve391TpCausBollo1();

    void setLdbve391TpCausBollo1(string ldbve391TpCausBollo1);

    /**
     * Host Variable LDBVE391-TP-CAUS-BOLLO-2
     * 
     */
    string getLdbve391TpCausBollo2();

    void setLdbve391TpCausBollo2(string ldbve391TpCausBollo2);

    /**
     * Host Variable LDBVE391-TP-CAUS-BOLLO-3
     * 
     */
    string getLdbve391TpCausBollo3();

    void setLdbve391TpCausBollo3(string ldbve391TpCausBollo3);

    /**
     * Host Variable LDBVE391-TP-CAUS-BOLLO-4
     * 
     */
    string getLdbve391TpCausBollo4();

    void setLdbve391TpCausBollo4(string ldbve391TpCausBollo4);

    /**
     * Host Variable LDBVE391-TP-CAUS-BOLLO-5
     * 
     */
    string getLdbve391TpCausBollo5();

    void setLdbve391TpCausBollo5(string ldbve391TpCausBollo5);

    /**
     * Host Variable LDBVE391-TP-CAUS-BOLLO-6
     * 
     */
    string getLdbve391TpCausBollo6();

    void setLdbve391TpCausBollo6(string ldbve391TpCausBollo6);

    /**
     * Host Variable LDBVE391-TP-CAUS-BOLLO-7
     * 
     */
    string getLdbve391TpCausBollo7();

    void setLdbve391TpCausBollo7(string ldbve391TpCausBollo7);

    /**
     * Host Variable LDBVE391-DT-INI-CALC-DB
     * 
     */
    string getLdbve391DtIniCalcDb();

    void setLdbve391DtIniCalcDb(string ldbve391DtIniCalcDb);

    /**
     * Host Variable LDBVE391-DT-END-CALC-DB
     * 
     */
    string getLdbve391DtEndCalcDb();

    void setLdbve391DtEndCalcDb(string ldbve391DtEndCalcDb);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    string getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(string wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable LDBVE421-COD-PART-IVA
     * 
     */
    string getLdbve421CodPartIva();

    void setLdbve421CodPartIva(string ldbve421CodPartIva);

    /**
     * Host Variable LDBVE421-TP-CAUS-BOLLO-1
     * 
     */
    string getLdbve421TpCausBollo1();

    void setLdbve421TpCausBollo1(string ldbve421TpCausBollo1);

    /**
     * Host Variable LDBVE421-TP-CAUS-BOLLO-2
     * 
     */
    string getLdbve421TpCausBollo2();

    void setLdbve421TpCausBollo2(string ldbve421TpCausBollo2);

    /**
     * Host Variable LDBVE421-TP-CAUS-BOLLO-3
     * 
     */
    string getLdbve421TpCausBollo3();

    void setLdbve421TpCausBollo3(string ldbve421TpCausBollo3);

    /**
     * Host Variable LDBVE421-TP-CAUS-BOLLO-4
     * 
     */
    string getLdbve421TpCausBollo4();

    void setLdbve421TpCausBollo4(string ldbve421TpCausBollo4);

    /**
     * Host Variable LDBVE421-TP-CAUS-BOLLO-5
     * 
     */
    string getLdbve421TpCausBollo5();

    void setLdbve421TpCausBollo5(string ldbve421TpCausBollo5);

    /**
     * Host Variable LDBVE421-TP-CAUS-BOLLO-6
     * 
     */
    string getLdbve421TpCausBollo6();

    void setLdbve421TpCausBollo6(string ldbve421TpCausBollo6);

    /**
     * Host Variable LDBVE421-TP-CAUS-BOLLO-7
     * 
     */
    string getLdbve421TpCausBollo7();

    void setLdbve421TpCausBollo7(string ldbve421TpCausBollo7);

    /**
     * Host Variable LDBVE421-DT-INI-CALC-DB
     * 
     */
    string getLdbve421DtIniCalcDb();

    void setLdbve421DtIniCalcDb(string ldbve421DtIniCalcDb);

    /**
     * Host Variable LDBVE421-DT-END-CALC-DB
     * 
     */
    string getLdbve421DtEndCalcDb();

    void setLdbve421DtEndCalcDb(string ldbve421DtEndCalcDb);

}


package com.myorg.myprj.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;


/**
 * Interface Transfer Object(TO) for table [SOPR_DI_GAR]
 * 
 */
public interface ISoprDiGar extends BaseSqlTo
{


    /**
     * Host Variable SPG-ID-GAR
     * 
     */
    int getSpgIdGar();

    void setSpgIdGar(int spgIdGar);

    /**
     * Host Variable SPG-COD-SOPR
     * 
     */
    string getSpgCodSopr();

    void setSpgCodSopr(string spgCodSopr);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    string getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(string wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable SPG-ID-SOPR-DI-GAR
     * 
     */
    int getIdSoprDiGar();

    void setIdSoprDiGar(int idSoprDiGar);

    /**
     * Nullable property for SPG-ID-GAR
     * 
     */
    Integer getSpgIdGarObj();

    void setSpgIdGarObj(Integer spgIdGarObj);

    /**
     * Host Variable SPG-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable SPG-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for SPG-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable SPG-DT-INI-EFF-DB
     * 
     */
    string getDtIniEffDb();

    void setDtIniEffDb(string dtIniEffDb);

    /**
     * Host Variable SPG-DT-END-EFF-DB
     * 
     */
    string getDtEndEffDb();

    void setDtEndEffDb(string dtEndEffDb);

    /**
     * Host Variable SPG-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Nullable property for SPG-COD-SOPR
     * 
     */
    String getSpgCodSoprObj();

    void setSpgCodSoprObj(String spgCodSoprObj);

    /**
     * Host Variable SPG-TP-D
     * 
     */
    string getTpD();

    void setTpD(string tpD);

    /**
     * Nullable property for SPG-TP-D
     * 
     */
    String getTpDObj();

    void setTpDObj(String tpDObj);

    /**
     * Host Variable SPG-VAL-PC
     * 
     */
    decimal(14, 9) getValPc();

    void setValPc(decimal(14, 9) valPc);

    /**
     * Nullable property for SPG-VAL-PC
     * 
     */
    AfDecimal getValPcObj();

    void setValPcObj(AfDecimal valPcObj);

    /**
     * Host Variable SPG-VAL-IMP
     * 
     */
    decimal(15, 3) getValImp();

    void setValImp(decimal(15, 3) valImp);

    /**
     * Nullable property for SPG-VAL-IMP
     * 
     */
    AfDecimal getValImpObj();

    void setValImpObj(AfDecimal valImpObj);

    /**
     * Host Variable SPG-PC-SOPRAM
     * 
     */
    decimal(14, 9) getPcSopram();

    void setPcSopram(decimal(14, 9) pcSopram);

    /**
     * Nullable property for SPG-PC-SOPRAM
     * 
     */
    AfDecimal getPcSopramObj();

    void setPcSopramObj(AfDecimal pcSopramObj);

    /**
     * Host Variable SPG-FL-ESCL-SOPR
     * 
     */
    char getFlEsclSopr();

    void setFlEsclSopr(char flEsclSopr);

    /**
     * Nullable property for SPG-FL-ESCL-SOPR
     * 
     */
    Character getFlEsclSoprObj();

    void setFlEsclSoprObj(Character flEsclSoprObj);

    /**
     * Host Variable SPG-DESC-ESCL-VCHAR
     * 
     */
    string getDescEsclVchar();

    void setDescEsclVchar(string descEsclVchar);

    /**
     * Nullable property for SPG-DESC-ESCL-VCHAR
     * 
     */
    String getDescEsclVcharObj();

    void setDescEsclVcharObj(String descEsclVcharObj);

    /**
     * Host Variable SPG-DS-RIGA
     * 
     */
    long getSpgDsRiga();

    void setSpgDsRiga(long spgDsRiga);

    /**
     * Host Variable SPG-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable SPG-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable SPG-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable SPG-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable SPG-DS-UTENTE
     * 
     */
    string getDsUtente();

    void setDsUtente(string dsUtente);

    /**
     * Host Variable SPG-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

}

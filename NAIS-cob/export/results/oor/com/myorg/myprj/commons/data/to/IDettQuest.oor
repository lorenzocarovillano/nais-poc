
package com.myorg.myprj.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;


/**
 * Interface Transfer Object(TO) for table [DETT_QUEST]
 * 
 */
public interface IDettQuest extends BaseSqlTo
{


    /**
     * Host Variable DEQ-ID-DETT-QUEST
     * 
     */
    int getIdDettQuest();

    void setIdDettQuest(int idDettQuest);

    /**
     * Host Variable DEQ-ID-QUEST
     * 
     */
    int getIdQuest();

    void setIdQuest(int idQuest);

    /**
     * Host Variable DEQ-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable DEQ-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for DEQ-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable DEQ-DT-INI-EFF-DB
     * 
     */
    string getDtIniEffDb();

    void setDtIniEffDb(string dtIniEffDb);

    /**
     * Host Variable DEQ-DT-END-EFF-DB
     * 
     */
    string getDtEndEffDb();

    void setDtEndEffDb(string dtEndEffDb);

    /**
     * Host Variable DEQ-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable DEQ-COD-QUEST
     * 
     */
    string getCodQuest();

    void setCodQuest(string codQuest);

    /**
     * Nullable property for DEQ-COD-QUEST
     * 
     */
    String getCodQuestObj();

    void setCodQuestObj(String codQuestObj);

    /**
     * Host Variable DEQ-COD-DOM
     * 
     */
    string getCodDom();

    void setCodDom(string codDom);

    /**
     * Nullable property for DEQ-COD-DOM
     * 
     */
    String getCodDomObj();

    void setCodDomObj(String codDomObj);

    /**
     * Host Variable DEQ-COD-DOM-COLLG
     * 
     */
    string getCodDomCollg();

    void setCodDomCollg(string codDomCollg);

    /**
     * Nullable property for DEQ-COD-DOM-COLLG
     * 
     */
    String getCodDomCollgObj();

    void setCodDomCollgObj(String codDomCollgObj);

    /**
     * Host Variable DEQ-RISP-NUM
     * 
     */
    int getRispNum();

    void setRispNum(int rispNum);

    /**
     * Nullable property for DEQ-RISP-NUM
     * 
     */
    Integer getRispNumObj();

    void setRispNumObj(Integer rispNumObj);

    /**
     * Host Variable DEQ-RISP-FL
     * 
     */
    char getRispFl();

    void setRispFl(char rispFl);

    /**
     * Nullable property for DEQ-RISP-FL
     * 
     */
    Character getRispFlObj();

    void setRispFlObj(Character rispFlObj);

    /**
     * Host Variable DEQ-RISP-TXT-VCHAR
     * 
     */
    string getRispTxtVchar();

    void setRispTxtVchar(string rispTxtVchar);

    /**
     * Nullable property for DEQ-RISP-TXT-VCHAR
     * 
     */
    String getRispTxtVcharObj();

    void setRispTxtVcharObj(String rispTxtVcharObj);

    /**
     * Host Variable DEQ-RISP-TS
     * 
     */
    decimal(14, 9) getRispTs();

    void setRispTs(decimal(14, 9) rispTs);

    /**
     * Nullable property for DEQ-RISP-TS
     * 
     */
    AfDecimal getRispTsObj();

    void setRispTsObj(AfDecimal rispTsObj);

    /**
     * Host Variable DEQ-RISP-IMP
     * 
     */
    decimal(15, 3) getRispImp();

    void setRispImp(decimal(15, 3) rispImp);

    /**
     * Nullable property for DEQ-RISP-IMP
     * 
     */
    AfDecimal getRispImpObj();

    void setRispImpObj(AfDecimal rispImpObj);

    /**
     * Host Variable DEQ-RISP-PC
     * 
     */
    decimal(6, 3) getRispPc();

    void setRispPc(decimal(6, 3) rispPc);

    /**
     * Nullable property for DEQ-RISP-PC
     * 
     */
    AfDecimal getRispPcObj();

    void setRispPcObj(AfDecimal rispPcObj);

    /**
     * Host Variable DEQ-RISP-DT-DB
     * 
     */
    string getRispDtDb();

    void setRispDtDb(string rispDtDb);

    /**
     * Nullable property for DEQ-RISP-DT-DB
     * 
     */
    String getRispDtDbObj();

    void setRispDtDbObj(String rispDtDbObj);

    /**
     * Host Variable DEQ-RISP-KEY
     * 
     */
    string getRispKey();

    void setRispKey(string rispKey);

    /**
     * Nullable property for DEQ-RISP-KEY
     * 
     */
    String getRispKeyObj();

    void setRispKeyObj(String rispKeyObj);

    /**
     * Host Variable DEQ-TP-RISP
     * 
     */
    string getTpRisp();

    void setTpRisp(string tpRisp);

    /**
     * Nullable property for DEQ-TP-RISP
     * 
     */
    String getTpRispObj();

    void setTpRispObj(String tpRispObj);

    /**
     * Host Variable DEQ-ID-COMP-QUEST
     * 
     */
    int getIdCompQuest();

    void setIdCompQuest(int idCompQuest);

    /**
     * Nullable property for DEQ-ID-COMP-QUEST
     * 
     */
    Integer getIdCompQuestObj();

    void setIdCompQuestObj(Integer idCompQuestObj);

    /**
     * Host Variable DEQ-VAL-RISP-VCHAR
     * 
     */
    string getValRispVchar();

    void setValRispVchar(string valRispVchar);

    /**
     * Nullable property for DEQ-VAL-RISP-VCHAR
     * 
     */
    String getValRispVcharObj();

    void setValRispVcharObj(String valRispVcharObj);

    /**
     * Host Variable DEQ-DS-RIGA
     * 
     */
    long getDeqDsRiga();

    void setDeqDsRiga(long deqDsRiga);

    /**
     * Host Variable DEQ-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable DEQ-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable DEQ-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable DEQ-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable DEQ-DS-UTENTE
     * 
     */
    string getDsUtente();

    void setDsUtente(string dsUtente);

    /**
     * Host Variable DEQ-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

}

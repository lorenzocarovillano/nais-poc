
package com.myorg.myprj.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;


/**
 * Interface Transfer Object(TO) for table [VINC_PEG]
 * 
 */
public interface IVincPeg extends BaseSqlTo
{


    /**
     * Host Variable L23-ID-VINC-PEG
     * 
     */
    int getIdVincPeg();

    void setIdVincPeg(int idVincPeg);

    /**
     * Host Variable L23-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable L23-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for L23-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable L23-DT-INI-EFF-DB
     * 
     */
    string getDtIniEffDb();

    void setDtIniEffDb(string dtIniEffDb);

    /**
     * Host Variable L23-DT-END-EFF-DB
     * 
     */
    string getDtEndEffDb();

    void setDtEndEffDb(string dtEndEffDb);

    /**
     * Host Variable L23-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable L23-ID-RAPP-ANA
     * 
     */
    int getIdRappAna();

    void setIdRappAna(int idRappAna);

    /**
     * Host Variable L23-TP-VINC
     * 
     */
    string getTpVinc();

    void setTpVinc(string tpVinc);

    /**
     * Nullable property for L23-TP-VINC
     * 
     */
    String getTpVincObj();

    void setTpVincObj(String tpVincObj);

    /**
     * Host Variable L23-FL-DELEGA-AL-RISC
     * 
     */
    char getFlDelegaAlRisc();

    void setFlDelegaAlRisc(char flDelegaAlRisc);

    /**
     * Nullable property for L23-FL-DELEGA-AL-RISC
     * 
     */
    Character getFlDelegaAlRiscObj();

    void setFlDelegaAlRiscObj(Character flDelegaAlRiscObj);

    /**
     * Host Variable L23-DESC-VCHAR
     * 
     */
    string getDescVchar();

    void setDescVchar(string descVchar);

    /**
     * Nullable property for L23-DESC-VCHAR
     * 
     */
    String getDescVcharObj();

    void setDescVcharObj(String descVcharObj);

    /**
     * Host Variable L23-DT-ATTIV-VINPG-DB
     * 
     */
    string getDtAttivVinpgDb();

    void setDtAttivVinpgDb(string dtAttivVinpgDb);

    /**
     * Nullable property for L23-DT-ATTIV-VINPG-DB
     * 
     */
    String getDtAttivVinpgDbObj();

    void setDtAttivVinpgDbObj(String dtAttivVinpgDbObj);

    /**
     * Host Variable L23-CPT-VINCTO-PIGN
     * 
     */
    decimal(15, 3) getCptVinctoPign();

    void setCptVinctoPign(decimal(15, 3) cptVinctoPign);

    /**
     * Nullable property for L23-CPT-VINCTO-PIGN
     * 
     */
    AfDecimal getCptVinctoPignObj();

    void setCptVinctoPignObj(AfDecimal cptVinctoPignObj);

    /**
     * Host Variable L23-DT-CHIU-VINPG-DB
     * 
     */
    string getDtChiuVinpgDb();

    void setDtChiuVinpgDb(string dtChiuVinpgDb);

    /**
     * Nullable property for L23-DT-CHIU-VINPG-DB
     * 
     */
    String getDtChiuVinpgDbObj();

    void setDtChiuVinpgDbObj(String dtChiuVinpgDbObj);

    /**
     * Host Variable L23-VAL-RISC-INI-VINPG
     * 
     */
    decimal(15, 3) getValRiscIniVinpg();

    void setValRiscIniVinpg(decimal(15, 3) valRiscIniVinpg);

    /**
     * Nullable property for L23-VAL-RISC-INI-VINPG
     * 
     */
    AfDecimal getValRiscIniVinpgObj();

    void setValRiscIniVinpgObj(AfDecimal valRiscIniVinpgObj);

    /**
     * Host Variable L23-VAL-RISC-END-VINPG
     * 
     */
    decimal(15, 3) getValRiscEndVinpg();

    void setValRiscEndVinpg(decimal(15, 3) valRiscEndVinpg);

    /**
     * Nullable property for L23-VAL-RISC-END-VINPG
     * 
     */
    AfDecimal getValRiscEndVinpgObj();

    void setValRiscEndVinpgObj(AfDecimal valRiscEndVinpgObj);

    /**
     * Host Variable L23-SOM-PRE-VINPG
     * 
     */
    decimal(15, 3) getSomPreVinpg();

    void setSomPreVinpg(decimal(15, 3) somPreVinpg);

    /**
     * Nullable property for L23-SOM-PRE-VINPG
     * 
     */
    AfDecimal getSomPreVinpgObj();

    void setSomPreVinpgObj(AfDecimal somPreVinpgObj);

    /**
     * Host Variable L23-FL-VINPG-INT-PRSTZ
     * 
     */
    char getFlVinpgIntPrstz();

    void setFlVinpgIntPrstz(char flVinpgIntPrstz);

    /**
     * Nullable property for L23-FL-VINPG-INT-PRSTZ
     * 
     */
    Character getFlVinpgIntPrstzObj();

    void setFlVinpgIntPrstzObj(Character flVinpgIntPrstzObj);

    /**
     * Host Variable L23-DESC-AGG-VINC-VCHAR
     * 
     */
    string getDescAggVincVchar();

    void setDescAggVincVchar(string descAggVincVchar);

    /**
     * Nullable property for L23-DESC-AGG-VINC-VCHAR
     * 
     */
    String getDescAggVincVcharObj();

    void setDescAggVincVcharObj(String descAggVincVcharObj);

    /**
     * Host Variable L23-DS-RIGA
     * 
     */
    long getL23DsRiga();

    void setL23DsRiga(long l23DsRiga);

    /**
     * Host Variable L23-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable L23-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable L23-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable L23-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable L23-DS-UTENTE
     * 
     */
    string getDsUtente();

    void setDsUtente(string dsUtente);

    /**
     * Host Variable L23-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable L23-TP-AUT-SEQ
     * 
     */
    string getTpAutSeq();

    void setTpAutSeq(string tpAutSeq);

    /**
     * Nullable property for L23-TP-AUT-SEQ
     * 
     */
    String getTpAutSeqObj();

    void setTpAutSeqObj(String tpAutSeqObj);

    /**
     * Host Variable L23-COD-UFF-SEQ
     * 
     */
    string getCodUffSeq();

    void setCodUffSeq(string codUffSeq);

    /**
     * Nullable property for L23-COD-UFF-SEQ
     * 
     */
    String getCodUffSeqObj();

    void setCodUffSeqObj(String codUffSeqObj);

    /**
     * Host Variable L23-NUM-PROVV-SEQ
     * 
     */
    int getNumProvvSeq();

    void setNumProvvSeq(int numProvvSeq);

    /**
     * Nullable property for L23-NUM-PROVV-SEQ
     * 
     */
    Integer getNumProvvSeqObj();

    void setNumProvvSeqObj(Integer numProvvSeqObj);

    /**
     * Host Variable L23-TP-PROVV-SEQ
     * 
     */
    string getTpProvvSeq();

    void setTpProvvSeq(string tpProvvSeq);

    /**
     * Nullable property for L23-TP-PROVV-SEQ
     * 
     */
    String getTpProvvSeqObj();

    void setTpProvvSeqObj(String tpProvvSeqObj);

    /**
     * Host Variable L23-DT-PROVV-SEQ-DB
     * 
     */
    string getDtProvvSeqDb();

    void setDtProvvSeqDb(string dtProvvSeqDb);

    /**
     * Nullable property for L23-DT-PROVV-SEQ-DB
     * 
     */
    String getDtProvvSeqDbObj();

    void setDtProvvSeqDbObj(String dtProvvSeqDbObj);

    /**
     * Host Variable L23-DT-NOTIFICA-BLOCCO-DB
     * 
     */
    string getDtNotificaBloccoDb();

    void setDtNotificaBloccoDb(string dtNotificaBloccoDb);

    /**
     * Nullable property for L23-DT-NOTIFICA-BLOCCO-DB
     * 
     */
    String getDtNotificaBloccoDbObj();

    void setDtNotificaBloccoDbObj(String dtNotificaBloccoDbObj);

    /**
     * Host Variable L23-NOTA-PROVV-VCHAR
     * 
     */
    string getNotaProvvVchar();

    void setNotaProvvVchar(string notaProvvVchar);

    /**
     * Nullable property for L23-NOTA-PROVV-VCHAR
     * 
     */
    String getNotaProvvVcharObj();

    void setNotaProvvVcharObj(String notaProvvVcharObj);

}

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0101" cbl:id="LVVS0101" xsi:id="LVVS0101" packageRef="LVVS0101.igd#LVVS0101" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0101_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10347" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0101.cbl.cobModel#SC_1F10347"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10347" deadCode="false" name="PROGRAM_LVVS0101_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0101.cbl.cobModel#P_1F10347"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10347" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0101.cbl.cobModel#P_2F10347"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10347" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0101.cbl.cobModel#P_3F10347"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10347" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0101.cbl.cobModel#P_4F10347"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10347" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0101.cbl.cobModel#P_5F10347"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10347" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0101.cbl.cobModel#P_6F10347"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10347" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0101.cbl.cobModel#P_7F10347"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10347" deadCode="false" name="S1110-LEGGI-PRESTITO">
				<representations href="../../../cobol/LVVS0101.cbl.cobModel#P_8F10347"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10347" deadCode="false" name="S1110-EX">
				<representations href="../../../cobol/LVVS0101.cbl.cobModel#P_9F10347"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10347" deadCode="false" name="S1255-CALCOLA-DATA">
				<representations href="../../../cobol/LVVS0101.cbl.cobModel#P_10F10347"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10347" deadCode="false" name="S1255-EX">
				<representations href="../../../cobol/LVVS0101.cbl.cobModel#P_11F10347"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10347" deadCode="false" name="S1260-CALCOLA-IMPORTO">
				<representations href="../../../cobol/LVVS0101.cbl.cobModel#P_12F10347"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10347" deadCode="false" name="S1260-EX">
				<representations href="../../../cobol/LVVS0101.cbl.cobModel#P_13F10347"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS6160" name="LDBS6160">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10232"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSB440" name="LDBSB440">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10247"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSB470" name="LDBSB470">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10248"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10347P_1F10347" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10347" targetNode="P_1F10347"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10347_I" deadCode="false" sourceNode="P_1F10347" targetNode="P_2F10347">
		<representations href="../../../cobol/LVVS0101.cbl.cobModel#S_1F10347"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10347_O" deadCode="false" sourceNode="P_1F10347" targetNode="P_3F10347">
		<representations href="../../../cobol/LVVS0101.cbl.cobModel#S_1F10347"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10347_I" deadCode="false" sourceNode="P_1F10347" targetNode="P_4F10347">
		<representations href="../../../cobol/LVVS0101.cbl.cobModel#S_2F10347"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10347_O" deadCode="false" sourceNode="P_1F10347" targetNode="P_5F10347">
		<representations href="../../../cobol/LVVS0101.cbl.cobModel#S_2F10347"/>
	</edges>
	<edges id="P_2F10347P_3F10347" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10347" targetNode="P_3F10347"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10347_I" deadCode="false" sourceNode="P_4F10347" targetNode="P_6F10347">
		<representations href="../../../cobol/LVVS0101.cbl.cobModel#S_11F10347"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10347_O" deadCode="false" sourceNode="P_4F10347" targetNode="P_7F10347">
		<representations href="../../../cobol/LVVS0101.cbl.cobModel#S_11F10347"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10347_I" deadCode="false" sourceNode="P_4F10347" targetNode="P_8F10347">
		<representations href="../../../cobol/LVVS0101.cbl.cobModel#S_13F10347"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10347_O" deadCode="false" sourceNode="P_4F10347" targetNode="P_9F10347">
		<representations href="../../../cobol/LVVS0101.cbl.cobModel#S_13F10347"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10347_I" deadCode="false" sourceNode="P_4F10347" targetNode="P_10F10347">
		<representations href="../../../cobol/LVVS0101.cbl.cobModel#S_15F10347"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10347_O" deadCode="false" sourceNode="P_4F10347" targetNode="P_11F10347">
		<representations href="../../../cobol/LVVS0101.cbl.cobModel#S_15F10347"/>
	</edges>
	<edges id="P_4F10347P_5F10347" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10347" targetNode="P_5F10347"/>
	<edges id="P_6F10347P_7F10347" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10347" targetNode="P_7F10347"/>
	<edges id="P_8F10347P_9F10347" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10347" targetNode="P_9F10347"/>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10347_I" deadCode="false" sourceNode="P_10F10347" targetNode="P_12F10347">
		<representations href="../../../cobol/LVVS0101.cbl.cobModel#S_75F10347"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10347_O" deadCode="false" sourceNode="P_10F10347" targetNode="P_13F10347">
		<representations href="../../../cobol/LVVS0101.cbl.cobModel#S_75F10347"/>
	</edges>
	<edges id="P_10F10347P_11F10347" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10347" targetNode="P_11F10347"/>
	<edges id="P_12F10347P_13F10347" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10347" targetNode="P_13F10347"/>
	<edges xsi:type="cbl:CallEdge" id="S_37F10347" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_8F10347" targetNode="LDBS6160">
		<representations href="../../../cobol/../importantStmts.cobModel#S_37F10347"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_70F10347" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_10F10347" targetNode="LDBSB440">
		<representations href="../../../cobol/../importantStmts.cobModel#S_70F10347"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_100F10347" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_12F10347" targetNode="LDBSB470">
		<representations href="../../../cobol/../importantStmts.cobModel#S_100F10347"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS5990" cbl:id="LDBS5990" xsi:id="LDBS5990" packageRef="LDBS5990.igd#LDBS5990" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS5990_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10228" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS5990.cbl.cobModel#SC_1F10228"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10228" deadCode="false" name="PROGRAM_LDBS5990_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_1F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10228" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_2F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10228" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_3F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10228" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_12F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10228" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_13F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10228" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_4F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10228" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_5F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10228" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_6F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10228" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_7F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10228" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_8F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10228" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_9F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10228" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_44F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10228" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_49F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10228" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_14F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10228" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_15F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10228" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_16F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10228" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_17F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10228" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_18F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10228" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_19F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10228" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_20F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10228" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_21F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10228" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_22F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10228" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_23F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10228" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_50F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10228" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_51F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10228" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_24F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10228" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_25F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10228" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_26F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10228" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_27F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10228" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_28F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10228" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_29F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10228" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_30F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10228" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_31F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10228" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_32F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10228" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_33F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10228" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_52F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10228" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_53F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10228" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_34F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10228" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_35F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10228" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_36F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10228" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_37F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10228" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_38F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10228" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_39F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10228" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_40F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10228" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_41F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10228" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_42F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10228" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_43F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10228" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_54F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10228" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_55F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10228" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_60F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10228" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_63F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10228" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_56F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10228" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_57F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10228" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_45F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10228" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_46F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10228" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_47F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10228" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_48F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10228" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_58F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10228" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_59F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10228" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_10F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10228" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_11F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10228" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_66F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10228" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_67F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10228" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_68F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10228" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_69F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10228" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_61F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10228" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_62F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10228" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_70F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10228" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_71F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10228" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_64F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10228" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_65F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10228" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_72F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10228" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_73F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10228" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_74F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10228" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_75F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10228" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_76F10228"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10228" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS5990.cbl.cobModel#P_77F10228"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_MOVI" name="MOVI">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_MOVI"/>
		</children>
	</packageNode>
	<edges id="SC_1F10228P_1F10228" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10228" targetNode="P_1F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10228_I" deadCode="false" sourceNode="P_1F10228" targetNode="P_2F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_2F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10228_O" deadCode="false" sourceNode="P_1F10228" targetNode="P_3F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_2F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10228_I" deadCode="false" sourceNode="P_1F10228" targetNode="P_4F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_6F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10228_O" deadCode="false" sourceNode="P_1F10228" targetNode="P_5F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_6F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10228_I" deadCode="false" sourceNode="P_1F10228" targetNode="P_6F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_10F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10228_O" deadCode="false" sourceNode="P_1F10228" targetNode="P_7F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_10F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10228_I" deadCode="false" sourceNode="P_1F10228" targetNode="P_8F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_14F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10228_O" deadCode="false" sourceNode="P_1F10228" targetNode="P_9F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_14F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10228_I" deadCode="false" sourceNode="P_2F10228" targetNode="P_10F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_24F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10228_O" deadCode="false" sourceNode="P_2F10228" targetNode="P_11F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_24F10228"/>
	</edges>
	<edges id="P_2F10228P_3F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10228" targetNode="P_3F10228"/>
	<edges id="P_12F10228P_13F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10228" targetNode="P_13F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10228_I" deadCode="false" sourceNode="P_4F10228" targetNode="P_14F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_37F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10228_O" deadCode="false" sourceNode="P_4F10228" targetNode="P_15F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_37F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10228_I" deadCode="false" sourceNode="P_4F10228" targetNode="P_16F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_38F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10228_O" deadCode="false" sourceNode="P_4F10228" targetNode="P_17F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_38F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10228_I" deadCode="false" sourceNode="P_4F10228" targetNode="P_18F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_39F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10228_O" deadCode="false" sourceNode="P_4F10228" targetNode="P_19F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_39F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10228_I" deadCode="false" sourceNode="P_4F10228" targetNode="P_20F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_40F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10228_O" deadCode="false" sourceNode="P_4F10228" targetNode="P_21F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_40F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10228_I" deadCode="false" sourceNode="P_4F10228" targetNode="P_22F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_41F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10228_O" deadCode="false" sourceNode="P_4F10228" targetNode="P_23F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_41F10228"/>
	</edges>
	<edges id="P_4F10228P_5F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10228" targetNode="P_5F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10228_I" deadCode="false" sourceNode="P_6F10228" targetNode="P_24F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_45F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10228_O" deadCode="false" sourceNode="P_6F10228" targetNode="P_25F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_45F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10228_I" deadCode="false" sourceNode="P_6F10228" targetNode="P_26F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_46F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10228_O" deadCode="false" sourceNode="P_6F10228" targetNode="P_27F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_46F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10228_I" deadCode="false" sourceNode="P_6F10228" targetNode="P_28F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_47F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10228_O" deadCode="false" sourceNode="P_6F10228" targetNode="P_29F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_47F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10228_I" deadCode="false" sourceNode="P_6F10228" targetNode="P_30F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_48F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10228_O" deadCode="false" sourceNode="P_6F10228" targetNode="P_31F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_48F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10228_I" deadCode="false" sourceNode="P_6F10228" targetNode="P_32F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_49F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10228_O" deadCode="false" sourceNode="P_6F10228" targetNode="P_33F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_49F10228"/>
	</edges>
	<edges id="P_6F10228P_7F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10228" targetNode="P_7F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10228_I" deadCode="false" sourceNode="P_8F10228" targetNode="P_34F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_53F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10228_O" deadCode="false" sourceNode="P_8F10228" targetNode="P_35F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_53F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10228_I" deadCode="false" sourceNode="P_8F10228" targetNode="P_36F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_54F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10228_O" deadCode="false" sourceNode="P_8F10228" targetNode="P_37F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_54F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10228_I" deadCode="false" sourceNode="P_8F10228" targetNode="P_38F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_55F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10228_O" deadCode="false" sourceNode="P_8F10228" targetNode="P_39F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_55F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10228_I" deadCode="false" sourceNode="P_8F10228" targetNode="P_40F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_56F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10228_O" deadCode="false" sourceNode="P_8F10228" targetNode="P_41F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_56F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10228_I" deadCode="false" sourceNode="P_8F10228" targetNode="P_42F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_57F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10228_O" deadCode="false" sourceNode="P_8F10228" targetNode="P_43F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_57F10228"/>
	</edges>
	<edges id="P_8F10228P_9F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10228" targetNode="P_9F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10228_I" deadCode="false" sourceNode="P_44F10228" targetNode="P_45F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_60F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10228_O" deadCode="false" sourceNode="P_44F10228" targetNode="P_46F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_60F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10228_I" deadCode="false" sourceNode="P_44F10228" targetNode="P_47F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_61F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10228_O" deadCode="false" sourceNode="P_44F10228" targetNode="P_48F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_61F10228"/>
	</edges>
	<edges id="P_44F10228P_49F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10228" targetNode="P_49F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10228_I" deadCode="false" sourceNode="P_14F10228" targetNode="P_45F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_64F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10228_O" deadCode="false" sourceNode="P_14F10228" targetNode="P_46F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_64F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10228_I" deadCode="false" sourceNode="P_14F10228" targetNode="P_47F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_65F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10228_O" deadCode="false" sourceNode="P_14F10228" targetNode="P_48F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_65F10228"/>
	</edges>
	<edges id="P_14F10228P_15F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10228" targetNode="P_15F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10228_I" deadCode="false" sourceNode="P_16F10228" targetNode="P_44F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_68F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10228_O" deadCode="false" sourceNode="P_16F10228" targetNode="P_49F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_68F10228"/>
	</edges>
	<edges id="P_16F10228P_17F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10228" targetNode="P_17F10228"/>
	<edges id="P_18F10228P_19F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10228" targetNode="P_19F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10228_I" deadCode="false" sourceNode="P_20F10228" targetNode="P_16F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_73F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10228_O" deadCode="false" sourceNode="P_20F10228" targetNode="P_17F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_73F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10228_I" deadCode="false" sourceNode="P_20F10228" targetNode="P_22F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_75F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10228_O" deadCode="false" sourceNode="P_20F10228" targetNode="P_23F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_75F10228"/>
	</edges>
	<edges id="P_20F10228P_21F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10228" targetNode="P_21F10228"/>
	<edges id="P_22F10228P_23F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10228" targetNode="P_23F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10228_I" deadCode="false" sourceNode="P_50F10228" targetNode="P_45F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_79F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10228_O" deadCode="false" sourceNode="P_50F10228" targetNode="P_46F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_79F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10228_I" deadCode="false" sourceNode="P_50F10228" targetNode="P_47F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_80F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10228_O" deadCode="false" sourceNode="P_50F10228" targetNode="P_48F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_80F10228"/>
	</edges>
	<edges id="P_50F10228P_51F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10228" targetNode="P_51F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10228_I" deadCode="false" sourceNode="P_24F10228" targetNode="P_45F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_83F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10228_O" deadCode="false" sourceNode="P_24F10228" targetNode="P_46F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_83F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10228_I" deadCode="false" sourceNode="P_24F10228" targetNode="P_47F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_84F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10228_O" deadCode="false" sourceNode="P_24F10228" targetNode="P_48F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_84F10228"/>
	</edges>
	<edges id="P_24F10228P_25F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10228" targetNode="P_25F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10228_I" deadCode="false" sourceNode="P_26F10228" targetNode="P_50F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_87F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10228_O" deadCode="false" sourceNode="P_26F10228" targetNode="P_51F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_87F10228"/>
	</edges>
	<edges id="P_26F10228P_27F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10228" targetNode="P_27F10228"/>
	<edges id="P_28F10228P_29F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10228" targetNode="P_29F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10228_I" deadCode="false" sourceNode="P_30F10228" targetNode="P_26F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_92F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10228_O" deadCode="false" sourceNode="P_30F10228" targetNode="P_27F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_92F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10228_I" deadCode="false" sourceNode="P_30F10228" targetNode="P_32F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_94F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10228_O" deadCode="false" sourceNode="P_30F10228" targetNode="P_33F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_94F10228"/>
	</edges>
	<edges id="P_30F10228P_31F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10228" targetNode="P_31F10228"/>
	<edges id="P_32F10228P_33F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10228" targetNode="P_33F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10228_I" deadCode="false" sourceNode="P_52F10228" targetNode="P_45F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_98F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10228_O" deadCode="false" sourceNode="P_52F10228" targetNode="P_46F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_98F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10228_I" deadCode="false" sourceNode="P_52F10228" targetNode="P_47F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_99F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10228_O" deadCode="false" sourceNode="P_52F10228" targetNode="P_48F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_99F10228"/>
	</edges>
	<edges id="P_52F10228P_53F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10228" targetNode="P_53F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10228_I" deadCode="false" sourceNode="P_34F10228" targetNode="P_45F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_103F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10228_O" deadCode="false" sourceNode="P_34F10228" targetNode="P_46F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_103F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10228_I" deadCode="false" sourceNode="P_34F10228" targetNode="P_47F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_104F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10228_O" deadCode="false" sourceNode="P_34F10228" targetNode="P_48F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_104F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10228_I" deadCode="false" sourceNode="P_34F10228" targetNode="P_12F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_106F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10228_O" deadCode="false" sourceNode="P_34F10228" targetNode="P_13F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_106F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10228_I" deadCode="false" sourceNode="P_34F10228" targetNode="P_54F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_108F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10228_O" deadCode="false" sourceNode="P_34F10228" targetNode="P_55F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_108F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10228_I" deadCode="false" sourceNode="P_34F10228" targetNode="P_56F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_109F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10228_O" deadCode="false" sourceNode="P_34F10228" targetNode="P_57F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_109F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10228_I" deadCode="false" sourceNode="P_34F10228" targetNode="P_58F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_110F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10228_O" deadCode="false" sourceNode="P_34F10228" targetNode="P_59F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_110F10228"/>
	</edges>
	<edges id="P_34F10228P_35F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10228" targetNode="P_35F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10228_I" deadCode="false" sourceNode="P_36F10228" targetNode="P_52F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_112F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10228_O" deadCode="false" sourceNode="P_36F10228" targetNode="P_53F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_112F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10228_I" deadCode="false" sourceNode="P_36F10228" targetNode="P_12F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_114F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10228_O" deadCode="false" sourceNode="P_36F10228" targetNode="P_13F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_114F10228"/>
	</edges>
	<edges id="P_36F10228P_37F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10228" targetNode="P_37F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10228_I" deadCode="false" sourceNode="P_38F10228" targetNode="P_12F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_117F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10228_O" deadCode="false" sourceNode="P_38F10228" targetNode="P_13F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_117F10228"/>
	</edges>
	<edges id="P_38F10228P_39F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10228" targetNode="P_39F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10228_I" deadCode="false" sourceNode="P_40F10228" targetNode="P_36F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_119F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10228_O" deadCode="false" sourceNode="P_40F10228" targetNode="P_37F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_119F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10228_I" deadCode="false" sourceNode="P_40F10228" targetNode="P_42F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_121F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10228_O" deadCode="false" sourceNode="P_40F10228" targetNode="P_43F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_121F10228"/>
	</edges>
	<edges id="P_40F10228P_41F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10228" targetNode="P_41F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10228_I" deadCode="false" sourceNode="P_42F10228" targetNode="P_12F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_124F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10228_O" deadCode="false" sourceNode="P_42F10228" targetNode="P_13F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_124F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10228_I" deadCode="false" sourceNode="P_42F10228" targetNode="P_54F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_126F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10228_O" deadCode="false" sourceNode="P_42F10228" targetNode="P_55F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_126F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10228_I" deadCode="false" sourceNode="P_42F10228" targetNode="P_56F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_127F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10228_O" deadCode="false" sourceNode="P_42F10228" targetNode="P_57F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_127F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10228_I" deadCode="false" sourceNode="P_42F10228" targetNode="P_58F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_128F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10228_O" deadCode="false" sourceNode="P_42F10228" targetNode="P_59F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_128F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10228_I" deadCode="false" sourceNode="P_42F10228" targetNode="P_38F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_130F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10228_O" deadCode="false" sourceNode="P_42F10228" targetNode="P_39F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_130F10228"/>
	</edges>
	<edges id="P_42F10228P_43F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10228" targetNode="P_43F10228"/>
	<edges id="P_54F10228P_55F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10228" targetNode="P_55F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10228_I" deadCode="true" sourceNode="P_60F10228" targetNode="P_61F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_153F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10228_O" deadCode="true" sourceNode="P_60F10228" targetNode="P_62F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_153F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10228_I" deadCode="false" sourceNode="P_56F10228" targetNode="P_64F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_157F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10228_O" deadCode="false" sourceNode="P_56F10228" targetNode="P_65F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_157F10228"/>
	</edges>
	<edges id="P_56F10228P_57F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10228" targetNode="P_57F10228"/>
	<edges id="P_45F10228P_46F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10228" targetNode="P_46F10228"/>
	<edges id="P_47F10228P_48F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10228" targetNode="P_48F10228"/>
	<edges id="P_58F10228P_59F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10228" targetNode="P_59F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10228_I" deadCode="false" sourceNode="P_10F10228" targetNode="P_66F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_166F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10228_O" deadCode="false" sourceNode="P_10F10228" targetNode="P_67F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_166F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10228_I" deadCode="false" sourceNode="P_10F10228" targetNode="P_68F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_168F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10228_O" deadCode="false" sourceNode="P_10F10228" targetNode="P_69F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_168F10228"/>
	</edges>
	<edges id="P_10F10228P_11F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10228" targetNode="P_11F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10228_I" deadCode="true" sourceNode="P_66F10228" targetNode="P_61F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_173F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10228_O" deadCode="true" sourceNode="P_66F10228" targetNode="P_62F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_173F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_178F10228_I" deadCode="true" sourceNode="P_66F10228" targetNode="P_61F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_178F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_178F10228_O" deadCode="true" sourceNode="P_66F10228" targetNode="P_62F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_178F10228"/>
	</edges>
	<edges id="P_66F10228P_67F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10228" targetNode="P_67F10228"/>
	<edges id="P_68F10228P_69F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10228" targetNode="P_69F10228"/>
	<edges id="P_61F10228P_62F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10228" targetNode="P_62F10228"/>
	<edges xsi:type="cbl:PerformEdge" id="S_207F10228_I" deadCode="false" sourceNode="P_64F10228" targetNode="P_72F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_207F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_207F10228_O" deadCode="false" sourceNode="P_64F10228" targetNode="P_73F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_207F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_208F10228_I" deadCode="false" sourceNode="P_64F10228" targetNode="P_74F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_208F10228"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_208F10228_O" deadCode="false" sourceNode="P_64F10228" targetNode="P_75F10228">
		<representations href="../../../cobol/LDBS5990.cbl.cobModel#S_208F10228"/>
	</edges>
	<edges id="P_64F10228P_65F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10228" targetNode="P_65F10228"/>
	<edges id="P_72F10228P_73F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10228" targetNode="P_73F10228"/>
	<edges id="P_74F10228P_75F10228" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10228" targetNode="P_75F10228"/>
	<edges xsi:type="cbl:DataEdge" id="S_105F10228_POS1" deadCode="false" targetNode="P_34F10228" sourceNode="DB2_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_105F10228"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_113F10228_POS1" deadCode="false" targetNode="P_36F10228" sourceNode="DB2_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_113F10228"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_116F10228_POS1" deadCode="false" targetNode="P_38F10228" sourceNode="DB2_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_116F10228"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_123F10228_POS1" deadCode="false" targetNode="P_42F10228" sourceNode="DB2_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_123F10228"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBSC600" cbl:id="LDBSC600" xsi:id="LDBSC600" packageRef="LDBSC600.igd#LDBSC600" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBSC600_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10251" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBSC600.cbl.cobModel#SC_1F10251"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10251" deadCode="false" name="PROGRAM_LDBSC600_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_1F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10251" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_2F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10251" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_3F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10251" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_12F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10251" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_13F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10251" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_4F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10251" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_5F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10251" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_6F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10251" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_7F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10251" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_8F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10251" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_9F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10251" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_44F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10251" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_49F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10251" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_14F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10251" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_15F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10251" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_16F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10251" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_17F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10251" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_18F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10251" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_19F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10251" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_20F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10251" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_21F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10251" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_22F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10251" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_23F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10251" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_56F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10251" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_57F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10251" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_24F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10251" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_25F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10251" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_26F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10251" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_27F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10251" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_28F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10251" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_29F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10251" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_30F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10251" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_31F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10251" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_32F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10251" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_33F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10251" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_58F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10251" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_59F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10251" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_34F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10251" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_35F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10251" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_36F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10251" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_37F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10251" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_38F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10251" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_39F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10251" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_40F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10251" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_41F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10251" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_42F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10251" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_43F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10251" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_50F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10251" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_51F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10251" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_60F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10251" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_63F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10251" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_52F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10251" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_53F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10251" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_45F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10251" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_46F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10251" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_47F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10251" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_48F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10251" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_54F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10251" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_55F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10251" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_10F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10251" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_11F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10251" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_66F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10251" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_67F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10251" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_68F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10251" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_69F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10251" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_61F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10251" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_62F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10251" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_70F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10251" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_71F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10251" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_64F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10251" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_65F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10251" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_72F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10251" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_73F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10251" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_74F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10251" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_75F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10251" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_76F10251"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10251" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBSC600.cbl.cobModel#P_77F10251"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_TRCH_DI_GAR" name="TRCH_DI_GAR">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_TRCH_DI_GAR"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_STAT_OGG_BUS" name="STAT_OGG_BUS">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_STAT_OGG_BUS"/>
		</children>
	</packageNode>
	<edges id="SC_1F10251P_1F10251" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10251" targetNode="P_1F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10251_I" deadCode="false" sourceNode="P_1F10251" targetNode="P_2F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_1F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10251_O" deadCode="false" sourceNode="P_1F10251" targetNode="P_3F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_1F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10251_I" deadCode="false" sourceNode="P_1F10251" targetNode="P_4F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_5F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10251_O" deadCode="false" sourceNode="P_1F10251" targetNode="P_5F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_5F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10251_I" deadCode="false" sourceNode="P_1F10251" targetNode="P_6F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_9F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10251_O" deadCode="false" sourceNode="P_1F10251" targetNode="P_7F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_9F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10251_I" deadCode="false" sourceNode="P_1F10251" targetNode="P_8F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_13F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10251_O" deadCode="false" sourceNode="P_1F10251" targetNode="P_9F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_13F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10251_I" deadCode="false" sourceNode="P_2F10251" targetNode="P_10F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_22F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10251_O" deadCode="false" sourceNode="P_2F10251" targetNode="P_11F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_22F10251"/>
	</edges>
	<edges id="P_2F10251P_3F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10251" targetNode="P_3F10251"/>
	<edges id="P_12F10251P_13F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10251" targetNode="P_13F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10251_I" deadCode="false" sourceNode="P_4F10251" targetNode="P_14F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_35F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10251_O" deadCode="false" sourceNode="P_4F10251" targetNode="P_15F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_35F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10251_I" deadCode="false" sourceNode="P_4F10251" targetNode="P_16F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_36F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10251_O" deadCode="false" sourceNode="P_4F10251" targetNode="P_17F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_36F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10251_I" deadCode="false" sourceNode="P_4F10251" targetNode="P_18F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_37F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10251_O" deadCode="false" sourceNode="P_4F10251" targetNode="P_19F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_37F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10251_I" deadCode="false" sourceNode="P_4F10251" targetNode="P_20F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_38F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10251_O" deadCode="false" sourceNode="P_4F10251" targetNode="P_21F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_38F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10251_I" deadCode="false" sourceNode="P_4F10251" targetNode="P_22F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_39F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10251_O" deadCode="false" sourceNode="P_4F10251" targetNode="P_23F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_39F10251"/>
	</edges>
	<edges id="P_4F10251P_5F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10251" targetNode="P_5F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10251_I" deadCode="false" sourceNode="P_6F10251" targetNode="P_24F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_43F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10251_O" deadCode="false" sourceNode="P_6F10251" targetNode="P_25F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_43F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10251_I" deadCode="false" sourceNode="P_6F10251" targetNode="P_26F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_44F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10251_O" deadCode="false" sourceNode="P_6F10251" targetNode="P_27F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_44F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10251_I" deadCode="false" sourceNode="P_6F10251" targetNode="P_28F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_45F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10251_O" deadCode="false" sourceNode="P_6F10251" targetNode="P_29F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_45F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10251_I" deadCode="false" sourceNode="P_6F10251" targetNode="P_30F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_46F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10251_O" deadCode="false" sourceNode="P_6F10251" targetNode="P_31F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_46F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10251_I" deadCode="false" sourceNode="P_6F10251" targetNode="P_32F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_47F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10251_O" deadCode="false" sourceNode="P_6F10251" targetNode="P_33F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_47F10251"/>
	</edges>
	<edges id="P_6F10251P_7F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10251" targetNode="P_7F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10251_I" deadCode="false" sourceNode="P_8F10251" targetNode="P_34F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_51F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10251_O" deadCode="false" sourceNode="P_8F10251" targetNode="P_35F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_51F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10251_I" deadCode="false" sourceNode="P_8F10251" targetNode="P_36F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_52F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10251_O" deadCode="false" sourceNode="P_8F10251" targetNode="P_37F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_52F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10251_I" deadCode="false" sourceNode="P_8F10251" targetNode="P_38F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_53F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10251_O" deadCode="false" sourceNode="P_8F10251" targetNode="P_39F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_53F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10251_I" deadCode="false" sourceNode="P_8F10251" targetNode="P_40F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_54F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10251_O" deadCode="false" sourceNode="P_8F10251" targetNode="P_41F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_54F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10251_I" deadCode="false" sourceNode="P_8F10251" targetNode="P_42F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_55F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10251_O" deadCode="false" sourceNode="P_8F10251" targetNode="P_43F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_55F10251"/>
	</edges>
	<edges id="P_8F10251P_9F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10251" targetNode="P_9F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10251_I" deadCode="false" sourceNode="P_44F10251" targetNode="P_45F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_58F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10251_O" deadCode="false" sourceNode="P_44F10251" targetNode="P_46F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_58F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10251_I" deadCode="false" sourceNode="P_44F10251" targetNode="P_47F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_59F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10251_O" deadCode="false" sourceNode="P_44F10251" targetNode="P_48F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_59F10251"/>
	</edges>
	<edges id="P_44F10251P_49F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10251" targetNode="P_49F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10251_I" deadCode="false" sourceNode="P_14F10251" targetNode="P_45F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_62F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10251_O" deadCode="false" sourceNode="P_14F10251" targetNode="P_46F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_62F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10251_I" deadCode="false" sourceNode="P_14F10251" targetNode="P_47F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_63F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10251_O" deadCode="false" sourceNode="P_14F10251" targetNode="P_48F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_63F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10251_I" deadCode="false" sourceNode="P_14F10251" targetNode="P_12F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_65F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10251_O" deadCode="false" sourceNode="P_14F10251" targetNode="P_13F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_65F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10251_I" deadCode="false" sourceNode="P_14F10251" targetNode="P_50F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_67F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10251_O" deadCode="false" sourceNode="P_14F10251" targetNode="P_51F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_67F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10251_I" deadCode="false" sourceNode="P_14F10251" targetNode="P_52F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_68F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10251_O" deadCode="false" sourceNode="P_14F10251" targetNode="P_53F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_68F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10251_I" deadCode="false" sourceNode="P_14F10251" targetNode="P_54F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_69F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10251_O" deadCode="false" sourceNode="P_14F10251" targetNode="P_55F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_69F10251"/>
	</edges>
	<edges id="P_14F10251P_15F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10251" targetNode="P_15F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10251_I" deadCode="false" sourceNode="P_16F10251" targetNode="P_44F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_71F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10251_O" deadCode="false" sourceNode="P_16F10251" targetNode="P_49F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_71F10251"/>
	</edges>
	<edges id="P_16F10251P_17F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10251" targetNode="P_17F10251"/>
	<edges id="P_18F10251P_19F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10251" targetNode="P_19F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10251_I" deadCode="false" sourceNode="P_20F10251" targetNode="P_16F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_76F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10251_O" deadCode="false" sourceNode="P_20F10251" targetNode="P_17F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_76F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10251_I" deadCode="false" sourceNode="P_20F10251" targetNode="P_22F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_78F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10251_O" deadCode="false" sourceNode="P_20F10251" targetNode="P_23F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_78F10251"/>
	</edges>
	<edges id="P_20F10251P_21F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10251" targetNode="P_21F10251"/>
	<edges id="P_22F10251P_23F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10251" targetNode="P_23F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10251_I" deadCode="false" sourceNode="P_56F10251" targetNode="P_45F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_82F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10251_O" deadCode="false" sourceNode="P_56F10251" targetNode="P_46F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_82F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10251_I" deadCode="false" sourceNode="P_56F10251" targetNode="P_47F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_83F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10251_O" deadCode="false" sourceNode="P_56F10251" targetNode="P_48F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_83F10251"/>
	</edges>
	<edges id="P_56F10251P_57F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10251" targetNode="P_57F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10251_I" deadCode="false" sourceNode="P_24F10251" targetNode="P_45F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_86F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10251_O" deadCode="false" sourceNode="P_24F10251" targetNode="P_46F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_86F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10251_I" deadCode="false" sourceNode="P_24F10251" targetNode="P_47F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_87F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10251_O" deadCode="false" sourceNode="P_24F10251" targetNode="P_48F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_87F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10251_I" deadCode="false" sourceNode="P_24F10251" targetNode="P_12F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_89F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10251_O" deadCode="false" sourceNode="P_24F10251" targetNode="P_13F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_89F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10251_I" deadCode="false" sourceNode="P_24F10251" targetNode="P_50F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_91F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10251_O" deadCode="false" sourceNode="P_24F10251" targetNode="P_51F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_91F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10251_I" deadCode="false" sourceNode="P_24F10251" targetNode="P_52F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_92F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10251_O" deadCode="false" sourceNode="P_24F10251" targetNode="P_53F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_92F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10251_I" deadCode="false" sourceNode="P_24F10251" targetNode="P_54F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_93F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10251_O" deadCode="false" sourceNode="P_24F10251" targetNode="P_55F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_93F10251"/>
	</edges>
	<edges id="P_24F10251P_25F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10251" targetNode="P_25F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10251_I" deadCode="false" sourceNode="P_26F10251" targetNode="P_56F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_95F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10251_O" deadCode="false" sourceNode="P_26F10251" targetNode="P_57F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_95F10251"/>
	</edges>
	<edges id="P_26F10251P_27F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10251" targetNode="P_27F10251"/>
	<edges id="P_28F10251P_29F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10251" targetNode="P_29F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10251_I" deadCode="false" sourceNode="P_30F10251" targetNode="P_26F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_100F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10251_O" deadCode="false" sourceNode="P_30F10251" targetNode="P_27F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_100F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10251_I" deadCode="false" sourceNode="P_30F10251" targetNode="P_32F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_102F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10251_O" deadCode="false" sourceNode="P_30F10251" targetNode="P_33F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_102F10251"/>
	</edges>
	<edges id="P_30F10251P_31F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10251" targetNode="P_31F10251"/>
	<edges id="P_32F10251P_33F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10251" targetNode="P_33F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10251_I" deadCode="false" sourceNode="P_58F10251" targetNode="P_45F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_106F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10251_O" deadCode="false" sourceNode="P_58F10251" targetNode="P_46F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_106F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10251_I" deadCode="false" sourceNode="P_58F10251" targetNode="P_47F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_107F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10251_O" deadCode="false" sourceNode="P_58F10251" targetNode="P_48F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_107F10251"/>
	</edges>
	<edges id="P_58F10251P_59F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10251" targetNode="P_59F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10251_I" deadCode="false" sourceNode="P_34F10251" targetNode="P_45F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_110F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10251_O" deadCode="false" sourceNode="P_34F10251" targetNode="P_46F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_110F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10251_I" deadCode="false" sourceNode="P_34F10251" targetNode="P_47F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_111F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10251_O" deadCode="false" sourceNode="P_34F10251" targetNode="P_48F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_111F10251"/>
	</edges>
	<edges id="P_34F10251P_35F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10251" targetNode="P_35F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10251_I" deadCode="false" sourceNode="P_36F10251" targetNode="P_58F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_114F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10251_O" deadCode="false" sourceNode="P_36F10251" targetNode="P_59F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_114F10251"/>
	</edges>
	<edges id="P_36F10251P_37F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10251" targetNode="P_37F10251"/>
	<edges id="P_38F10251P_39F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10251" targetNode="P_39F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10251_I" deadCode="false" sourceNode="P_40F10251" targetNode="P_36F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_119F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10251_O" deadCode="false" sourceNode="P_40F10251" targetNode="P_37F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_119F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10251_I" deadCode="false" sourceNode="P_40F10251" targetNode="P_42F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_121F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10251_O" deadCode="false" sourceNode="P_40F10251" targetNode="P_43F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_121F10251"/>
	</edges>
	<edges id="P_40F10251P_41F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10251" targetNode="P_41F10251"/>
	<edges id="P_42F10251P_43F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10251" targetNode="P_43F10251"/>
	<edges id="P_50F10251P_51F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10251" targetNode="P_51F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_364F10251_I" deadCode="true" sourceNode="P_60F10251" targetNode="P_61F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_364F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_364F10251_O" deadCode="true" sourceNode="P_60F10251" targetNode="P_62F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_364F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_367F10251_I" deadCode="true" sourceNode="P_60F10251" targetNode="P_61F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_367F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_367F10251_O" deadCode="true" sourceNode="P_60F10251" targetNode="P_62F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_367F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_370F10251_I" deadCode="true" sourceNode="P_60F10251" targetNode="P_61F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_370F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_370F10251_O" deadCode="true" sourceNode="P_60F10251" targetNode="P_62F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_370F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_374F10251_I" deadCode="true" sourceNode="P_60F10251" targetNode="P_61F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_374F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_374F10251_O" deadCode="true" sourceNode="P_60F10251" targetNode="P_62F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_374F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_378F10251_I" deadCode="true" sourceNode="P_60F10251" targetNode="P_61F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_378F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_378F10251_O" deadCode="true" sourceNode="P_60F10251" targetNode="P_62F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_378F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_382F10251_I" deadCode="true" sourceNode="P_60F10251" targetNode="P_61F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_382F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_382F10251_O" deadCode="true" sourceNode="P_60F10251" targetNode="P_62F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_382F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_386F10251_I" deadCode="true" sourceNode="P_60F10251" targetNode="P_61F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_386F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_386F10251_O" deadCode="true" sourceNode="P_60F10251" targetNode="P_62F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_386F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_390F10251_I" deadCode="true" sourceNode="P_60F10251" targetNode="P_61F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_390F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_390F10251_O" deadCode="true" sourceNode="P_60F10251" targetNode="P_62F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_390F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_394F10251_I" deadCode="true" sourceNode="P_60F10251" targetNode="P_61F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_394F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_394F10251_O" deadCode="true" sourceNode="P_60F10251" targetNode="P_62F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_394F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_397F10251_I" deadCode="true" sourceNode="P_60F10251" targetNode="P_61F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_397F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_397F10251_O" deadCode="true" sourceNode="P_60F10251" targetNode="P_62F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_397F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_400F10251_I" deadCode="true" sourceNode="P_60F10251" targetNode="P_61F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_400F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_400F10251_O" deadCode="true" sourceNode="P_60F10251" targetNode="P_62F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_400F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_404F10251_I" deadCode="false" sourceNode="P_52F10251" targetNode="P_64F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_404F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_404F10251_O" deadCode="false" sourceNode="P_52F10251" targetNode="P_65F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_404F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_407F10251_I" deadCode="false" sourceNode="P_52F10251" targetNode="P_64F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_407F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_407F10251_O" deadCode="false" sourceNode="P_52F10251" targetNode="P_65F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_407F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_410F10251_I" deadCode="false" sourceNode="P_52F10251" targetNode="P_64F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_410F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_410F10251_O" deadCode="false" sourceNode="P_52F10251" targetNode="P_65F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_410F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_414F10251_I" deadCode="false" sourceNode="P_52F10251" targetNode="P_64F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_414F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_414F10251_O" deadCode="false" sourceNode="P_52F10251" targetNode="P_65F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_414F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_418F10251_I" deadCode="false" sourceNode="P_52F10251" targetNode="P_64F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_418F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_418F10251_O" deadCode="false" sourceNode="P_52F10251" targetNode="P_65F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_418F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_422F10251_I" deadCode="false" sourceNode="P_52F10251" targetNode="P_64F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_422F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_422F10251_O" deadCode="false" sourceNode="P_52F10251" targetNode="P_65F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_422F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_426F10251_I" deadCode="false" sourceNode="P_52F10251" targetNode="P_64F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_426F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_426F10251_O" deadCode="false" sourceNode="P_52F10251" targetNode="P_65F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_426F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_430F10251_I" deadCode="false" sourceNode="P_52F10251" targetNode="P_64F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_430F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_430F10251_O" deadCode="false" sourceNode="P_52F10251" targetNode="P_65F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_430F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_434F10251_I" deadCode="false" sourceNode="P_52F10251" targetNode="P_64F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_434F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_434F10251_O" deadCode="false" sourceNode="P_52F10251" targetNode="P_65F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_434F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_437F10251_I" deadCode="false" sourceNode="P_52F10251" targetNode="P_64F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_437F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_437F10251_O" deadCode="false" sourceNode="P_52F10251" targetNode="P_65F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_437F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_440F10251_I" deadCode="false" sourceNode="P_52F10251" targetNode="P_64F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_440F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_440F10251_O" deadCode="false" sourceNode="P_52F10251" targetNode="P_65F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_440F10251"/>
	</edges>
	<edges id="P_52F10251P_53F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10251" targetNode="P_53F10251"/>
	<edges id="P_45F10251P_46F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10251" targetNode="P_46F10251"/>
	<edges id="P_47F10251P_48F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10251" targetNode="P_48F10251"/>
	<edges id="P_54F10251P_55F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10251" targetNode="P_55F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_450F10251_I" deadCode="false" sourceNode="P_10F10251" targetNode="P_66F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_450F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_450F10251_O" deadCode="false" sourceNode="P_10F10251" targetNode="P_67F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_450F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_452F10251_I" deadCode="false" sourceNode="P_10F10251" targetNode="P_68F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_452F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_452F10251_O" deadCode="false" sourceNode="P_10F10251" targetNode="P_69F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_452F10251"/>
	</edges>
	<edges id="P_10F10251P_11F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10251" targetNode="P_11F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_457F10251_I" deadCode="false" sourceNode="P_66F10251" targetNode="P_61F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_457F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_457F10251_O" deadCode="false" sourceNode="P_66F10251" targetNode="P_62F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_457F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_462F10251_I" deadCode="false" sourceNode="P_66F10251" targetNode="P_61F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_462F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_462F10251_O" deadCode="false" sourceNode="P_66F10251" targetNode="P_62F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_462F10251"/>
	</edges>
	<edges id="P_66F10251P_67F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10251" targetNode="P_67F10251"/>
	<edges id="P_68F10251P_69F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10251" targetNode="P_69F10251"/>
	<edges id="P_61F10251P_62F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10251" targetNode="P_62F10251"/>
	<edges xsi:type="cbl:PerformEdge" id="S_491F10251_I" deadCode="false" sourceNode="P_64F10251" targetNode="P_72F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_491F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_491F10251_O" deadCode="false" sourceNode="P_64F10251" targetNode="P_73F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_491F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_492F10251_I" deadCode="false" sourceNode="P_64F10251" targetNode="P_74F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_492F10251"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_492F10251_O" deadCode="false" sourceNode="P_64F10251" targetNode="P_75F10251">
		<representations href="../../../cobol/LDBSC600.cbl.cobModel#S_492F10251"/>
	</edges>
	<edges id="P_64F10251P_65F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10251" targetNode="P_65F10251"/>
	<edges id="P_72F10251P_73F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10251" targetNode="P_73F10251"/>
	<edges id="P_74F10251P_75F10251" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10251" targetNode="P_75F10251"/>
	<edges xsi:type="cbl:DataEdge" id="S_64F10251_POS1" deadCode="false" targetNode="P_14F10251" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_64F10251"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_64F10251_POS2" deadCode="false" targetNode="P_14F10251" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_64F10251"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_88F10251_POS1" deadCode="false" targetNode="P_24F10251" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_88F10251"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_88F10251_POS2" deadCode="false" targetNode="P_24F10251" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_88F10251"></representations>
	</edges>
</Package>

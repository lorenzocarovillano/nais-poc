<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS3510" cbl:id="LVVS3510" xsi:id="LVVS3510" packageRef="LVVS3510.igd#LVVS3510" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS3510_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10396" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS3510.cbl.cobModel#SC_1F10396"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10396" deadCode="false" name="PROGRAM_LVVS3510_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS3510.cbl.cobModel#P_1F10396"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10396" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS3510.cbl.cobModel#P_2F10396"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10396" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS3510.cbl.cobModel#P_3F10396"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10396" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS3510.cbl.cobModel#P_4F10396"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10396" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS3510.cbl.cobModel#P_5F10396"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10396" deadCode="false" name="LETTURA-D-CRIST">
				<representations href="../../../cobol/LVVS3510.cbl.cobModel#P_10F10396"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10396" deadCode="false" name="LETTURA-D-CRIST-EX">
				<representations href="../../../cobol/LVVS3510.cbl.cobModel#P_11F10396"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10396" deadCode="true" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS3510.cbl.cobModel#P_8F10396"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10396" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS3510.cbl.cobModel#P_9F10396"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10396" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS3510.cbl.cobModel#P_6F10396"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10396" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS3510.cbl.cobModel#P_7F10396"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSH580" name="LDBSH580">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10276"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10396P_1F10396" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10396" targetNode="P_1F10396"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10396_I" deadCode="false" sourceNode="P_1F10396" targetNode="P_2F10396">
		<representations href="../../../cobol/LVVS3510.cbl.cobModel#S_1F10396"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10396_O" deadCode="false" sourceNode="P_1F10396" targetNode="P_3F10396">
		<representations href="../../../cobol/LVVS3510.cbl.cobModel#S_1F10396"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10396_I" deadCode="false" sourceNode="P_1F10396" targetNode="P_4F10396">
		<representations href="../../../cobol/LVVS3510.cbl.cobModel#S_2F10396"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10396_O" deadCode="false" sourceNode="P_1F10396" targetNode="P_5F10396">
		<representations href="../../../cobol/LVVS3510.cbl.cobModel#S_2F10396"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10396_I" deadCode="false" sourceNode="P_1F10396" targetNode="P_6F10396">
		<representations href="../../../cobol/LVVS3510.cbl.cobModel#S_3F10396"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10396_O" deadCode="false" sourceNode="P_1F10396" targetNode="P_7F10396">
		<representations href="../../../cobol/LVVS3510.cbl.cobModel#S_3F10396"/>
	</edges>
	<edges id="P_2F10396P_3F10396" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10396" targetNode="P_3F10396"/>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10396_I" deadCode="true" sourceNode="P_4F10396" targetNode="P_8F10396">
		<representations href="../../../cobol/LVVS3510.cbl.cobModel#S_9F10396"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10396_O" deadCode="true" sourceNode="P_4F10396" targetNode="P_9F10396">
		<representations href="../../../cobol/LVVS3510.cbl.cobModel#S_9F10396"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10396_I" deadCode="false" sourceNode="P_4F10396" targetNode="P_10F10396">
		<representations href="../../../cobol/LVVS3510.cbl.cobModel#S_10F10396"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10396_O" deadCode="false" sourceNode="P_4F10396" targetNode="P_11F10396">
		<representations href="../../../cobol/LVVS3510.cbl.cobModel#S_10F10396"/>
	</edges>
	<edges id="P_4F10396P_5F10396" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10396" targetNode="P_5F10396"/>
	<edges id="P_10F10396P_11F10396" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10396" targetNode="P_11F10396"/>
	<edges id="P_8F10396P_9F10396" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10396" targetNode="P_9F10396"/>
	<edges xsi:type="cbl:CallEdge" id="S_24F10396" deadCode="false" name="Dynamic WK-PGM-CALL" sourceNode="P_10F10396" targetNode="LDBSH580">
		<representations href="../../../cobol/../importantStmts.cobModel#S_24F10396"></representations>
	</edges>
</Package>

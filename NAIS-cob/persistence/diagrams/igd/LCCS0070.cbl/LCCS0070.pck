<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LCCS0070" cbl:id="LCCS0070" xsi:id="LCCS0070" packageRef="LCCS0070.igd#LCCS0070" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LCCS0070_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10132" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LCCS0070.cbl.cobModel#SC_1F10132"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10132" deadCode="false" name="PROGRAM_LCCS0070_FIRST_SENTENCES">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_1F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10132" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_2F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10132" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_3F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10132" deadCode="false" name="S0005-CTRL-DATI-INPUT">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_8F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10132" deadCode="false" name="EX-S0005">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_9F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10132" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_4F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10132" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_5F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10132" deadCode="false" name="S1010-LETT-ADE-GAR-TRA">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_12F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10132" deadCode="false" name="S1010-EX">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_13F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10132" deadCode="false" name="S1020-UPDATE-ADE-GAR-TRA">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_14F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10132" deadCode="false" name="S1020-EX">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_15F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10132" deadCode="false" name="S1030-INSERT-ADE-GAR-TRA">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_16F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10132" deadCode="false" name="S1030-EX">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_17F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10132" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_6F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10132" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_7F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10132" deadCode="true" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_20F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10132" deadCode="true" name="EX-S0290">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_21F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10132" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_10F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10132" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_11F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10132" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_24F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10132" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_25F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10132" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_22F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10132" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_23F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10132" deadCode="false" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_18F10132"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10132" deadCode="false" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LCCS0070.cbl.cobModel#P_19F10132"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0010" name="IDSS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10099"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10132P_1F10132" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10132" targetNode="P_1F10132"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10132_I" deadCode="false" sourceNode="P_1F10132" targetNode="P_2F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_1F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10132_O" deadCode="false" sourceNode="P_1F10132" targetNode="P_3F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_1F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10132_I" deadCode="false" sourceNode="P_1F10132" targetNode="P_4F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_3F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10132_O" deadCode="false" sourceNode="P_1F10132" targetNode="P_5F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_3F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10132_I" deadCode="false" sourceNode="P_1F10132" targetNode="P_6F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_4F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10132_O" deadCode="false" sourceNode="P_1F10132" targetNode="P_7F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_4F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10132_I" deadCode="false" sourceNode="P_2F10132" targetNode="P_8F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_6F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10132_O" deadCode="false" sourceNode="P_2F10132" targetNode="P_9F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_6F10132"/>
	</edges>
	<edges id="P_2F10132P_3F10132" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10132" targetNode="P_3F10132"/>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10132_I" deadCode="false" sourceNode="P_8F10132" targetNode="P_10F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_14F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10132_O" deadCode="false" sourceNode="P_8F10132" targetNode="P_11F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_14F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10132_I" deadCode="false" sourceNode="P_8F10132" targetNode="P_10F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_20F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10132_O" deadCode="false" sourceNode="P_8F10132" targetNode="P_11F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_20F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10132_I" deadCode="false" sourceNode="P_8F10132" targetNode="P_10F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_26F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10132_O" deadCode="false" sourceNode="P_8F10132" targetNode="P_11F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_26F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10132_I" deadCode="false" sourceNode="P_8F10132" targetNode="P_10F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_31F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10132_O" deadCode="false" sourceNode="P_8F10132" targetNode="P_11F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_31F10132"/>
	</edges>
	<edges id="P_8F10132P_9F10132" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10132" targetNode="P_9F10132"/>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10132_I" deadCode="false" sourceNode="P_4F10132" targetNode="P_12F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_33F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10132_O" deadCode="false" sourceNode="P_4F10132" targetNode="P_13F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_33F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10132_I" deadCode="false" sourceNode="P_4F10132" targetNode="P_14F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_36F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10132_O" deadCode="false" sourceNode="P_4F10132" targetNode="P_15F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_36F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10132_I" deadCode="false" sourceNode="P_4F10132" targetNode="P_16F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_39F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10132_O" deadCode="false" sourceNode="P_4F10132" targetNode="P_17F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_39F10132"/>
	</edges>
	<edges id="P_4F10132P_5F10132" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10132" targetNode="P_5F10132"/>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10132_I" deadCode="false" sourceNode="P_12F10132" targetNode="P_18F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_57F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10132_O" deadCode="false" sourceNode="P_12F10132" targetNode="P_19F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_57F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10132_I" deadCode="false" sourceNode="P_12F10132" targetNode="P_10F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_67F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10132_O" deadCode="false" sourceNode="P_12F10132" targetNode="P_11F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_67F10132"/>
	</edges>
	<edges id="P_12F10132P_13F10132" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10132" targetNode="P_13F10132"/>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10132_I" deadCode="false" sourceNode="P_14F10132" targetNode="P_18F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_82F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10132_O" deadCode="false" sourceNode="P_14F10132" targetNode="P_19F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_82F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10132_I" deadCode="false" sourceNode="P_14F10132" targetNode="P_10F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_91F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10132_O" deadCode="false" sourceNode="P_14F10132" targetNode="P_11F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_91F10132"/>
	</edges>
	<edges id="P_14F10132P_15F10132" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10132" targetNode="P_15F10132"/>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10132_I" deadCode="false" sourceNode="P_16F10132" targetNode="P_18F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_104F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10132_O" deadCode="false" sourceNode="P_16F10132" targetNode="P_19F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_104F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10132_I" deadCode="false" sourceNode="P_16F10132" targetNode="P_10F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_113F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10132_O" deadCode="false" sourceNode="P_16F10132" targetNode="P_11F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_113F10132"/>
	</edges>
	<edges id="P_16F10132P_17F10132" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10132" targetNode="P_17F10132"/>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10132_I" deadCode="true" sourceNode="P_20F10132" targetNode="P_10F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_119F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10132_O" deadCode="true" sourceNode="P_20F10132" targetNode="P_11F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_119F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10132_I" deadCode="false" sourceNode="P_10F10132" targetNode="P_22F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_126F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10132_O" deadCode="false" sourceNode="P_10F10132" targetNode="P_23F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_126F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10132_I" deadCode="false" sourceNode="P_10F10132" targetNode="P_24F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_130F10132"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10132_O" deadCode="false" sourceNode="P_10F10132" targetNode="P_25F10132">
		<representations href="../../../cobol/LCCS0070.cbl.cobModel#S_130F10132"/>
	</edges>
	<edges id="P_10F10132P_11F10132" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10132" targetNode="P_11F10132"/>
	<edges id="P_24F10132P_25F10132" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10132" targetNode="P_25F10132"/>
	<edges id="P_22F10132P_23F10132" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10132" targetNode="P_23F10132"/>
	<edges id="P_18F10132P_19F10132" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10132" targetNode="P_19F10132"/>
	<edges xsi:type="cbl:CallEdge" id="S_124F10132" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_10F10132" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_124F10132"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_190F10132" deadCode="false" name="Dynamic IDSI0011-PGM" sourceNode="P_18F10132" targetNode="IDSS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_190F10132"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IABS0040" cbl:id="IABS0040" xsi:id="IABS0040" packageRef="IABS0040.igd#IABS0040" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IABS0040_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10003" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IABS0040.cbl.cobModel#SC_1F10003"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10003" deadCode="false" name="PROGRAM_IABS0040_FIRST_SENTENCES">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_1F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10003" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_2F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10003" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_3F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10003" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_8F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10003" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_9F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10003" deadCode="false" name="A300-ELABORA">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_4F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10003" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_5F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10003" deadCode="false" name="A301-ELABORA-PK">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_10F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10003" deadCode="false" name="A301-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_11F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10003" deadCode="false" name="A302-ELABORA-WC">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_12F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10003" deadCode="false" name="A302-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_13F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10003" deadCode="false" name="A305-DECLARE-CURSOR">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_28F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10003" deadCode="false" name="A305-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_29F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10003" deadCode="false" name="A306-SELECT">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_14F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10003" deadCode="false" name="A306-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_15F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10003" deadCode="false" name="A320-INSERT">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_20F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10003" deadCode="false" name="A320-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_21F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10003" deadCode="false" name="A330-UPDATE">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_22F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10003" deadCode="false" name="A330-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_23F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10003" deadCode="false" name="A360-OPEN-CURSOR">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_44F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10003" deadCode="false" name="A360-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_45F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10003" deadCode="false" name="A370-CLOSE-CURSOR">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_46F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10003" deadCode="false" name="A370-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_47F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10003" deadCode="false" name="A380-FETCH-FIRST">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_16F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10003" deadCode="false" name="A380-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_17F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10003" deadCode="false" name="A390-FETCH-NEXT">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_18F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10003" deadCode="false" name="A390-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_19F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10003" deadCode="false" name="W001-WHERE-CONDITION-01">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_24F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10003" deadCode="false" name="W001-01-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_25F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10003" deadCode="false" name="W002-WHERE-CONDITION-02">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_26F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10003" deadCode="false" name="W002-02-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_27F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10003" deadCode="false" name="W100-WH-SELECT-TP-BATCH">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_48F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10003" deadCode="false" name="W100-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_49F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10003" deadCode="false" name="W200-WH-SELECT-DT-EFFETTO">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_50F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10003" deadCode="false" name="W200-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_51F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10003" deadCode="false" name="W300-WH-SELECT-DT-EFFETTO-MAX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_52F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10003" deadCode="false" name="W300-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_53F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10003" deadCode="false" name="W400-WH-SELECT-DT-EFFETTO-MIN">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_54F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10003" deadCode="false" name="W400-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_55F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10003" deadCode="false" name="W500-WH-SELECT-DT-INSERIM-MAX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_56F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10003" deadCode="false" name="W500-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_57F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10003" deadCode="false" name="W600-WH-SELECT-DT-INSERIM-MIN">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_58F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10003" deadCode="false" name="W600-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_59F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10003" deadCode="false" name="W700-WH-ELABORA-ALL-BATCH">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_60F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10003" deadCode="false" name="W700-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_61F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10003" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_32F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10003" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_33F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10003" deadCode="false" name="Z150-VALORIZZA-DATA-SERVICES">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_38F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10003" deadCode="false" name="Z150-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_39F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10003" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_40F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10003" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_41F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10003" deadCode="false" name="Z400-SEQ">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_36F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10003" deadCode="false" name="Z400-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_37F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10003" deadCode="false" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_42F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10003" deadCode="false" name="Z900-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_43F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10003" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_34F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10003" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_35F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10003" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_30F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10003" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_31F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10003" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_6F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10003" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_7F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10003" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_70F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10003" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_71F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10003" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_72F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10003" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_73F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10003" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_64F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10003" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_65F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10003" deadCode="false" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_62F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10003" deadCode="false" name="Z701-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_63F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10003" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_68F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10003" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_69F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10003" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_74F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10003" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_75F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10003" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_76F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10003" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_77F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10003" deadCode="false" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_66F10003"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10003" deadCode="false" name="Z801-EX">
				<representations href="../../../cobol/IABS0040.cbl.cobModel#P_67F10003"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_BTC_BATCH" name="BTC_BATCH">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_BTC_BATCH"/>
		</children>
	</packageNode>
	<edges id="SC_1F10003P_1F10003" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10003" targetNode="P_1F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10003_I" deadCode="false" sourceNode="P_1F10003" targetNode="P_2F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_1F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10003_O" deadCode="false" sourceNode="P_1F10003" targetNode="P_3F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_1F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10003_I" deadCode="false" sourceNode="P_1F10003" targetNode="P_4F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_2F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10003_O" deadCode="false" sourceNode="P_1F10003" targetNode="P_5F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_2F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10003_I" deadCode="false" sourceNode="P_2F10003" targetNode="P_6F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_9F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10003_O" deadCode="false" sourceNode="P_2F10003" targetNode="P_7F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_9F10003"/>
	</edges>
	<edges id="P_2F10003P_3F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10003" targetNode="P_3F10003"/>
	<edges id="P_8F10003P_9F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10003" targetNode="P_9F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10003_I" deadCode="false" sourceNode="P_4F10003" targetNode="P_10F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_22F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10003_O" deadCode="false" sourceNode="P_4F10003" targetNode="P_11F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_22F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10003_I" deadCode="false" sourceNode="P_4F10003" targetNode="P_12F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_23F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10003_O" deadCode="false" sourceNode="P_4F10003" targetNode="P_13F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_23F10003"/>
	</edges>
	<edges id="P_4F10003P_5F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10003" targetNode="P_5F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10003_I" deadCode="false" sourceNode="P_10F10003" targetNode="P_14F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_27F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10003_O" deadCode="false" sourceNode="P_10F10003" targetNode="P_15F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_27F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10003_I" deadCode="false" sourceNode="P_10F10003" targetNode="P_16F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_28F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10003_O" deadCode="false" sourceNode="P_10F10003" targetNode="P_17F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_28F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10003_I" deadCode="false" sourceNode="P_10F10003" targetNode="P_18F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_29F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10003_O" deadCode="false" sourceNode="P_10F10003" targetNode="P_19F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_29F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10003_I" deadCode="false" sourceNode="P_10F10003" targetNode="P_20F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_30F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10003_O" deadCode="false" sourceNode="P_10F10003" targetNode="P_21F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_30F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10003_I" deadCode="false" sourceNode="P_10F10003" targetNode="P_22F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_31F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10003_O" deadCode="false" sourceNode="P_10F10003" targetNode="P_23F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_31F10003"/>
	</edges>
	<edges id="P_10F10003P_11F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10003" targetNode="P_11F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10003_I" deadCode="false" sourceNode="P_12F10003" targetNode="P_24F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_35F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10003_O" deadCode="false" sourceNode="P_12F10003" targetNode="P_25F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_35F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10003_I" deadCode="false" sourceNode="P_12F10003" targetNode="P_26F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_36F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10003_O" deadCode="false" sourceNode="P_12F10003" targetNode="P_27F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_36F10003"/>
	</edges>
	<edges id="P_12F10003P_13F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10003" targetNode="P_13F10003"/>
	<edges id="P_28F10003P_29F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10003" targetNode="P_29F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10003_I" deadCode="false" sourceNode="P_14F10003" targetNode="P_30F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_42F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10003_O" deadCode="false" sourceNode="P_14F10003" targetNode="P_31F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_42F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10003_I" deadCode="false" sourceNode="P_14F10003" targetNode="P_8F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_44F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10003_O" deadCode="false" sourceNode="P_14F10003" targetNode="P_9F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_44F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10003_I" deadCode="false" sourceNode="P_14F10003" targetNode="P_32F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_46F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10003_O" deadCode="false" sourceNode="P_14F10003" targetNode="P_33F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_46F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10003_I" deadCode="false" sourceNode="P_14F10003" targetNode="P_34F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_47F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10003_O" deadCode="false" sourceNode="P_14F10003" targetNode="P_35F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_47F10003"/>
	</edges>
	<edges id="P_14F10003P_15F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10003" targetNode="P_15F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10003_I" deadCode="false" sourceNode="P_20F10003" targetNode="P_36F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_49F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10003_O" deadCode="false" sourceNode="P_20F10003" targetNode="P_37F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_49F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10003_I" deadCode="false" sourceNode="P_20F10003" targetNode="P_38F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_51F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10003_O" deadCode="false" sourceNode="P_20F10003" targetNode="P_39F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_51F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10003_I" deadCode="false" sourceNode="P_20F10003" targetNode="P_40F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_52F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10003_O" deadCode="false" sourceNode="P_20F10003" targetNode="P_41F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_52F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10003_I" deadCode="false" sourceNode="P_20F10003" targetNode="P_42F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_53F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10003_O" deadCode="false" sourceNode="P_20F10003" targetNode="P_43F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_53F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10003_I" deadCode="false" sourceNode="P_20F10003" targetNode="P_30F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_54F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10003_O" deadCode="false" sourceNode="P_20F10003" targetNode="P_31F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_54F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10003_I" deadCode="false" sourceNode="P_20F10003" targetNode="P_8F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_56F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10003_O" deadCode="false" sourceNode="P_20F10003" targetNode="P_9F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_56F10003"/>
	</edges>
	<edges id="P_20F10003P_21F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10003" targetNode="P_21F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10003_I" deadCode="false" sourceNode="P_22F10003" targetNode="P_40F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_58F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10003_O" deadCode="false" sourceNode="P_22F10003" targetNode="P_41F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_58F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10003_I" deadCode="false" sourceNode="P_22F10003" targetNode="P_42F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_59F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10003_O" deadCode="false" sourceNode="P_22F10003" targetNode="P_43F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_59F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10003_I" deadCode="false" sourceNode="P_22F10003" targetNode="P_30F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_60F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10003_O" deadCode="false" sourceNode="P_22F10003" targetNode="P_31F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_60F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10003_I" deadCode="false" sourceNode="P_22F10003" targetNode="P_8F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_62F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10003_O" deadCode="false" sourceNode="P_22F10003" targetNode="P_9F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_62F10003"/>
	</edges>
	<edges id="P_22F10003P_23F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10003" targetNode="P_23F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10003_I" deadCode="false" sourceNode="P_44F10003" targetNode="P_28F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_66F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10003_O" deadCode="false" sourceNode="P_44F10003" targetNode="P_29F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_66F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10003_I" deadCode="false" sourceNode="P_44F10003" targetNode="P_8F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_68F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10003_O" deadCode="false" sourceNode="P_44F10003" targetNode="P_9F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_68F10003"/>
	</edges>
	<edges id="P_44F10003P_45F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10003" targetNode="P_45F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10003_I" deadCode="false" sourceNode="P_46F10003" targetNode="P_8F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_71F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10003_O" deadCode="false" sourceNode="P_46F10003" targetNode="P_9F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_71F10003"/>
	</edges>
	<edges id="P_46F10003P_47F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10003" targetNode="P_47F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10003_I" deadCode="false" sourceNode="P_16F10003" targetNode="P_44F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_73F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10003_O" deadCode="false" sourceNode="P_16F10003" targetNode="P_45F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_73F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10003_I" deadCode="false" sourceNode="P_16F10003" targetNode="P_18F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_75F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10003_O" deadCode="false" sourceNode="P_16F10003" targetNode="P_19F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_75F10003"/>
	</edges>
	<edges id="P_16F10003P_17F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10003" targetNode="P_17F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10003_I" deadCode="false" sourceNode="P_18F10003" targetNode="P_8F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_78F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10003_O" deadCode="false" sourceNode="P_18F10003" targetNode="P_9F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_78F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10003_I" deadCode="false" sourceNode="P_18F10003" targetNode="P_32F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_80F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10003_O" deadCode="false" sourceNode="P_18F10003" targetNode="P_33F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_80F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10003_I" deadCode="false" sourceNode="P_18F10003" targetNode="P_34F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_81F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10003_O" deadCode="false" sourceNode="P_18F10003" targetNode="P_35F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_81F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10003_I" deadCode="false" sourceNode="P_18F10003" targetNode="P_46F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_83F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10003_O" deadCode="false" sourceNode="P_18F10003" targetNode="P_47F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_83F10003"/>
	</edges>
	<edges id="P_18F10003P_19F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10003" targetNode="P_19F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10003_I" deadCode="false" sourceNode="P_24F10003" targetNode="P_48F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_89F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10003_O" deadCode="false" sourceNode="P_24F10003" targetNode="P_49F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_89F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10003_I" deadCode="false" sourceNode="P_24F10003" targetNode="P_50F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_90F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10003_O" deadCode="false" sourceNode="P_24F10003" targetNode="P_51F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_90F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10003_I" deadCode="false" sourceNode="P_24F10003" targetNode="P_52F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_91F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10003_O" deadCode="false" sourceNode="P_24F10003" targetNode="P_53F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_91F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10003_I" deadCode="false" sourceNode="P_24F10003" targetNode="P_54F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_92F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10003_O" deadCode="false" sourceNode="P_24F10003" targetNode="P_55F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_92F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10003_I" deadCode="false" sourceNode="P_24F10003" targetNode="P_56F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_93F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10003_O" deadCode="false" sourceNode="P_24F10003" targetNode="P_57F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_93F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10003_I" deadCode="false" sourceNode="P_24F10003" targetNode="P_58F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_94F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10003_O" deadCode="false" sourceNode="P_24F10003" targetNode="P_59F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_94F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10003_I" deadCode="false" sourceNode="P_24F10003" targetNode="P_60F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_95F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10003_O" deadCode="false" sourceNode="P_24F10003" targetNode="P_61F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_95F10003"/>
	</edges>
	<edges id="P_24F10003P_25F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10003" targetNode="P_25F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10003_I" deadCode="false" sourceNode="P_26F10003" targetNode="P_40F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_98F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10003_O" deadCode="false" sourceNode="P_26F10003" targetNode="P_41F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_98F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10003_I" deadCode="false" sourceNode="P_26F10003" targetNode="P_42F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_99F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10003_O" deadCode="false" sourceNode="P_26F10003" targetNode="P_43F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_99F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10003_I" deadCode="false" sourceNode="P_26F10003" targetNode="P_30F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_100F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10003_O" deadCode="false" sourceNode="P_26F10003" targetNode="P_31F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_100F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10003_I" deadCode="false" sourceNode="P_26F10003" targetNode="P_8F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_102F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10003_O" deadCode="false" sourceNode="P_26F10003" targetNode="P_9F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_102F10003"/>
	</edges>
	<edges id="P_26F10003P_27F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10003" targetNode="P_27F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10003_I" deadCode="false" sourceNode="P_48F10003" targetNode="P_30F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_106F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10003_O" deadCode="false" sourceNode="P_48F10003" targetNode="P_31F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_106F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10003_I" deadCode="false" sourceNode="P_48F10003" targetNode="P_8F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_108F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10003_O" deadCode="false" sourceNode="P_48F10003" targetNode="P_9F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_108F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10003_I" deadCode="false" sourceNode="P_48F10003" targetNode="P_32F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_110F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10003_O" deadCode="false" sourceNode="P_48F10003" targetNode="P_33F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_110F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10003_I" deadCode="false" sourceNode="P_48F10003" targetNode="P_34F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_111F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10003_O" deadCode="false" sourceNode="P_48F10003" targetNode="P_35F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_111F10003"/>
	</edges>
	<edges id="P_48F10003P_49F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10003" targetNode="P_49F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10003_I" deadCode="false" sourceNode="P_50F10003" targetNode="P_30F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_113F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10003_O" deadCode="false" sourceNode="P_50F10003" targetNode="P_31F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_113F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10003_I" deadCode="false" sourceNode="P_50F10003" targetNode="P_40F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_114F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10003_O" deadCode="false" sourceNode="P_50F10003" targetNode="P_41F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_114F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10003_I" deadCode="false" sourceNode="P_50F10003" targetNode="P_42F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_115F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10003_O" deadCode="false" sourceNode="P_50F10003" targetNode="P_43F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_115F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10003_I" deadCode="false" sourceNode="P_50F10003" targetNode="P_8F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_117F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10003_O" deadCode="false" sourceNode="P_50F10003" targetNode="P_9F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_117F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10003_I" deadCode="false" sourceNode="P_50F10003" targetNode="P_32F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_119F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10003_O" deadCode="false" sourceNode="P_50F10003" targetNode="P_33F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_119F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10003_I" deadCode="false" sourceNode="P_50F10003" targetNode="P_34F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_120F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10003_O" deadCode="false" sourceNode="P_50F10003" targetNode="P_35F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_120F10003"/>
	</edges>
	<edges id="P_50F10003P_51F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10003" targetNode="P_51F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10003_I" deadCode="false" sourceNode="P_52F10003" targetNode="P_30F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_122F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10003_O" deadCode="false" sourceNode="P_52F10003" targetNode="P_31F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_122F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10003_I" deadCode="false" sourceNode="P_52F10003" targetNode="P_40F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_123F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10003_O" deadCode="false" sourceNode="P_52F10003" targetNode="P_41F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_123F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10003_I" deadCode="false" sourceNode="P_52F10003" targetNode="P_8F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_125F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10003_O" deadCode="false" sourceNode="P_52F10003" targetNode="P_9F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_125F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10003_I" deadCode="false" sourceNode="P_52F10003" targetNode="P_32F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_127F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10003_O" deadCode="false" sourceNode="P_52F10003" targetNode="P_33F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_127F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10003_I" deadCode="false" sourceNode="P_52F10003" targetNode="P_34F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_128F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10003_O" deadCode="false" sourceNode="P_52F10003" targetNode="P_35F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_128F10003"/>
	</edges>
	<edges id="P_52F10003P_53F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10003" targetNode="P_53F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10003_I" deadCode="false" sourceNode="P_54F10003" targetNode="P_30F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_130F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10003_O" deadCode="false" sourceNode="P_54F10003" targetNode="P_31F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_130F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10003_I" deadCode="false" sourceNode="P_54F10003" targetNode="P_40F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_131F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10003_O" deadCode="false" sourceNode="P_54F10003" targetNode="P_41F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_131F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10003_I" deadCode="false" sourceNode="P_54F10003" targetNode="P_8F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_133F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10003_O" deadCode="false" sourceNode="P_54F10003" targetNode="P_9F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_133F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10003_I" deadCode="false" sourceNode="P_54F10003" targetNode="P_32F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_135F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10003_O" deadCode="false" sourceNode="P_54F10003" targetNode="P_33F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_135F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10003_I" deadCode="false" sourceNode="P_54F10003" targetNode="P_34F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_136F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10003_O" deadCode="false" sourceNode="P_54F10003" targetNode="P_35F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_136F10003"/>
	</edges>
	<edges id="P_54F10003P_55F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10003" targetNode="P_55F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10003_I" deadCode="false" sourceNode="P_56F10003" targetNode="P_30F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_138F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10003_O" deadCode="false" sourceNode="P_56F10003" targetNode="P_31F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_138F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10003_I" deadCode="false" sourceNode="P_56F10003" targetNode="P_40F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_139F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10003_O" deadCode="false" sourceNode="P_56F10003" targetNode="P_41F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_139F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_141F10003_I" deadCode="false" sourceNode="P_56F10003" targetNode="P_8F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_141F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_141F10003_O" deadCode="false" sourceNode="P_56F10003" targetNode="P_9F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_141F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10003_I" deadCode="false" sourceNode="P_56F10003" targetNode="P_32F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_143F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10003_O" deadCode="false" sourceNode="P_56F10003" targetNode="P_33F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_143F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10003_I" deadCode="false" sourceNode="P_56F10003" targetNode="P_34F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_144F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10003_O" deadCode="false" sourceNode="P_56F10003" targetNode="P_35F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_144F10003"/>
	</edges>
	<edges id="P_56F10003P_57F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10003" targetNode="P_57F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10003_I" deadCode="false" sourceNode="P_58F10003" targetNode="P_30F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_146F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10003_O" deadCode="false" sourceNode="P_58F10003" targetNode="P_31F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_146F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10003_I" deadCode="false" sourceNode="P_58F10003" targetNode="P_40F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_147F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10003_O" deadCode="false" sourceNode="P_58F10003" targetNode="P_41F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_147F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10003_I" deadCode="false" sourceNode="P_58F10003" targetNode="P_8F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_149F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10003_O" deadCode="false" sourceNode="P_58F10003" targetNode="P_9F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_149F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_151F10003_I" deadCode="false" sourceNode="P_58F10003" targetNode="P_32F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_151F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_151F10003_O" deadCode="false" sourceNode="P_58F10003" targetNode="P_33F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_151F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10003_I" deadCode="false" sourceNode="P_58F10003" targetNode="P_34F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_152F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10003_O" deadCode="false" sourceNode="P_58F10003" targetNode="P_35F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_152F10003"/>
	</edges>
	<edges id="P_58F10003P_59F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10003" targetNode="P_59F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_155F10003_I" deadCode="false" sourceNode="P_60F10003" targetNode="P_16F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_155F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_155F10003_O" deadCode="false" sourceNode="P_60F10003" targetNode="P_17F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_155F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_156F10003_I" deadCode="false" sourceNode="P_60F10003" targetNode="P_18F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_156F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_156F10003_O" deadCode="false" sourceNode="P_60F10003" targetNode="P_19F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_156F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10003_I" deadCode="false" sourceNode="P_60F10003" targetNode="P_20F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_157F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10003_O" deadCode="false" sourceNode="P_60F10003" targetNode="P_21F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_157F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10003_I" deadCode="false" sourceNode="P_60F10003" targetNode="P_22F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_158F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10003_O" deadCode="false" sourceNode="P_60F10003" targetNode="P_23F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_158F10003"/>
	</edges>
	<edges id="P_60F10003P_61F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_60F10003" targetNode="P_61F10003"/>
	<edges id="P_32F10003P_33F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10003" targetNode="P_33F10003"/>
	<edges id="P_38F10003P_39F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10003" targetNode="P_39F10003"/>
	<edges id="P_40F10003P_41F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10003" targetNode="P_41F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_197F10003_I" deadCode="false" sourceNode="P_36F10003" targetNode="P_8F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_197F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_197F10003_O" deadCode="false" sourceNode="P_36F10003" targetNode="P_9F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_197F10003"/>
	</edges>
	<edges id="P_36F10003P_37F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10003" targetNode="P_37F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_200F10003_I" deadCode="false" sourceNode="P_42F10003" targetNode="P_62F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_200F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_200F10003_O" deadCode="false" sourceNode="P_42F10003" targetNode="P_63F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_200F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_204F10003_I" deadCode="false" sourceNode="P_42F10003" targetNode="P_62F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_204F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_204F10003_O" deadCode="false" sourceNode="P_42F10003" targetNode="P_63F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_204F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_208F10003_I" deadCode="false" sourceNode="P_42F10003" targetNode="P_62F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_208F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_208F10003_O" deadCode="false" sourceNode="P_42F10003" targetNode="P_63F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_208F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_212F10003_I" deadCode="false" sourceNode="P_42F10003" targetNode="P_64F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_212F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_212F10003_O" deadCode="false" sourceNode="P_42F10003" targetNode="P_65F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_212F10003"/>
	</edges>
	<edges id="P_42F10003P_43F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10003" targetNode="P_43F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10003_I" deadCode="false" sourceNode="P_34F10003" targetNode="P_66F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_216F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10003_O" deadCode="false" sourceNode="P_34F10003" targetNode="P_67F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_216F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_220F10003_I" deadCode="false" sourceNode="P_34F10003" targetNode="P_66F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_220F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_220F10003_O" deadCode="false" sourceNode="P_34F10003" targetNode="P_67F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_220F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_224F10003_I" deadCode="false" sourceNode="P_34F10003" targetNode="P_66F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_224F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_224F10003_O" deadCode="false" sourceNode="P_34F10003" targetNode="P_67F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_224F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_228F10003_I" deadCode="false" sourceNode="P_34F10003" targetNode="P_68F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_228F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_228F10003_O" deadCode="false" sourceNode="P_34F10003" targetNode="P_69F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_228F10003"/>
	</edges>
	<edges id="P_34F10003P_35F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10003" targetNode="P_35F10003"/>
	<edges id="P_30F10003P_31F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10003" targetNode="P_31F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_233F10003_I" deadCode="false" sourceNode="P_6F10003" targetNode="P_70F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_233F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_233F10003_O" deadCode="false" sourceNode="P_6F10003" targetNode="P_71F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_233F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_235F10003_I" deadCode="false" sourceNode="P_6F10003" targetNode="P_72F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_235F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_235F10003_O" deadCode="false" sourceNode="P_6F10003" targetNode="P_73F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_235F10003"/>
	</edges>
	<edges id="P_6F10003P_7F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10003" targetNode="P_7F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_240F10003_I" deadCode="false" sourceNode="P_70F10003" targetNode="P_64F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_240F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_240F10003_O" deadCode="false" sourceNode="P_70F10003" targetNode="P_65F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_240F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_245F10003_I" deadCode="false" sourceNode="P_70F10003" targetNode="P_64F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_245F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_245F10003_O" deadCode="false" sourceNode="P_70F10003" targetNode="P_65F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_245F10003"/>
	</edges>
	<edges id="P_70F10003P_71F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_70F10003" targetNode="P_71F10003"/>
	<edges id="P_72F10003P_73F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10003" targetNode="P_73F10003"/>
	<edges id="P_64F10003P_65F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10003" targetNode="P_65F10003"/>
	<edges id="P_62F10003P_63F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10003" targetNode="P_63F10003"/>
	<edges xsi:type="cbl:PerformEdge" id="S_274F10003_I" deadCode="false" sourceNode="P_68F10003" targetNode="P_74F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_274F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_274F10003_O" deadCode="false" sourceNode="P_68F10003" targetNode="P_75F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_274F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_275F10003_I" deadCode="false" sourceNode="P_68F10003" targetNode="P_76F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_275F10003"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_275F10003_O" deadCode="false" sourceNode="P_68F10003" targetNode="P_77F10003">
		<representations href="../../../cobol/IABS0040.cbl.cobModel#S_275F10003"/>
	</edges>
	<edges id="P_68F10003P_69F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10003" targetNode="P_69F10003"/>
	<edges id="P_74F10003P_75F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10003" targetNode="P_75F10003"/>
	<edges id="P_76F10003P_77F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_76F10003" targetNode="P_77F10003"/>
	<edges id="P_66F10003P_67F10003" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10003" targetNode="P_67F10003"/>
	<edges xsi:type="cbl:DataEdge" id="S_43F10003_POS1" deadCode="false" targetNode="P_14F10003" sourceNode="DB2_BTC_BATCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_43F10003"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_55F10003_POS1" deadCode="false" sourceNode="P_20F10003" targetNode="DB2_BTC_BATCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_55F10003"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_61F10003_POS1" deadCode="false" sourceNode="P_22F10003" targetNode="DB2_BTC_BATCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_61F10003"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_67F10003_POS1" deadCode="false" targetNode="P_44F10003" sourceNode="DB2_BTC_BATCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10003"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_70F10003_POS1" deadCode="false" targetNode="P_46F10003" sourceNode="DB2_BTC_BATCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_70F10003"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_77F10003_POS1" deadCode="false" targetNode="P_18F10003" sourceNode="DB2_BTC_BATCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_77F10003"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_101F10003_POS1" deadCode="false" sourceNode="P_26F10003" targetNode="DB2_BTC_BATCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_101F10003"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_107F10003_POS1" deadCode="false" targetNode="P_48F10003" sourceNode="DB2_BTC_BATCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_107F10003"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_116F10003_POS1" deadCode="false" targetNode="P_50F10003" sourceNode="DB2_BTC_BATCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_116F10003"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_124F10003_POS1" deadCode="false" targetNode="P_52F10003" sourceNode="DB2_BTC_BATCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_124F10003"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_132F10003_POS1" deadCode="false" targetNode="P_54F10003" sourceNode="DB2_BTC_BATCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_132F10003"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_140F10003_POS1" deadCode="false" targetNode="P_56F10003" sourceNode="DB2_BTC_BATCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_140F10003"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_148F10003_POS1" deadCode="false" targetNode="P_58F10003" sourceNode="DB2_BTC_BATCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_148F10003"></representations>
	</edges>
</Package>

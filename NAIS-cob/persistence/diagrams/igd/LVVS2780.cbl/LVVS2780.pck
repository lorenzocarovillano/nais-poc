<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS2780" cbl:id="LVVS2780" xsi:id="LVVS2780" packageRef="LVVS2780.igd#LVVS2780" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS2780_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10377" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS2780.cbl.cobModel#SC_1F10377"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10377" deadCode="false" name="PROGRAM_LVVS2780_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_1F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10377" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_2F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10377" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_3F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10377" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_4F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10377" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_5F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10377" deadCode="false" name="CALCOLA-FLAG">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_10F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10377" deadCode="false" name="CALCOLA-FLAG-EX">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_11F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10377" deadCode="false" name="VERIFICA-MOVIMENTO">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_12F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10377" deadCode="false" name="VERIFICA-MOVIMENTO-EX">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_13F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10377" deadCode="false" name="RECUP-MOVI-COMUN">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_18F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10377" deadCode="false" name="RECUP-MOVI-COMUN-EX">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_19F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10377" deadCode="false" name="S1110-LETTURA-GARANZIA">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_14F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10377" deadCode="false" name="S1110-LETTURA-GARANZIA-EX">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_15F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10377" deadCode="false" name="S1120-VERIFICA-BOL-PRE">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_16F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10377" deadCode="false" name="S1120-VERIFICA-BOL-PRE-EX">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_17F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10377" deadCode="true" name="S1130-CHIUSURA-CURS">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_24F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10377" deadCode="true" name="S1130-CHIUSURA-CURS-EX">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_25F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10377" deadCode="false" name="S1140-CHIUSURA-CURS">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_22F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10377" deadCode="false" name="S1140-CHIUSURA-CURS-EX">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_23F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10377" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_8F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10377" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_9F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10377" deadCode="false" name="CLOSE-MOVI">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_20F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10377" deadCode="false" name="CLOSE-MOVI-EX">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_21F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10377" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_6F10377"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10377" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS2780.cbl.cobModel#P_7F10377"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS6040" name="LDBS6040">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10230"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS1420" name="LDBS1420">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10159"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSP580" name="IDBSP580">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10064"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LVVS2780_LDBS1420" name="Dynamic LVVS2780 LDBS1420" missing="true">
			<representations href="../../../../missing.xmi#IDWRUQFGV2GVFEDX40SVPTYI2C3ILLHVXBJFDLZRLJGSNPMVELRIJB"/>
		</children>
	</packageNode>
	<edges id="SC_1F10377P_1F10377" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10377" targetNode="P_1F10377"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10377_I" deadCode="false" sourceNode="P_1F10377" targetNode="P_2F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_1F10377"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10377_O" deadCode="false" sourceNode="P_1F10377" targetNode="P_3F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_1F10377"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10377_I" deadCode="false" sourceNode="P_1F10377" targetNode="P_4F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_2F10377"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10377_O" deadCode="false" sourceNode="P_1F10377" targetNode="P_5F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_2F10377"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10377_I" deadCode="false" sourceNode="P_1F10377" targetNode="P_6F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_3F10377"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10377_O" deadCode="false" sourceNode="P_1F10377" targetNode="P_7F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_3F10377"/>
	</edges>
	<edges id="P_2F10377P_3F10377" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10377" targetNode="P_3F10377"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10377_I" deadCode="false" sourceNode="P_4F10377" targetNode="P_8F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_11F10377"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10377_O" deadCode="false" sourceNode="P_4F10377" targetNode="P_9F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_11F10377"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10377_I" deadCode="false" sourceNode="P_4F10377" targetNode="P_10F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_12F10377"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10377_O" deadCode="false" sourceNode="P_4F10377" targetNode="P_11F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_12F10377"/>
	</edges>
	<edges id="P_4F10377P_5F10377" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10377" targetNode="P_5F10377"/>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10377_I" deadCode="false" sourceNode="P_10F10377" targetNode="P_12F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_19F10377"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10377_O" deadCode="false" sourceNode="P_10F10377" targetNode="P_13F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_19F10377"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10377_I" deadCode="false" sourceNode="P_10F10377" targetNode="P_14F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_20F10377"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10377_O" deadCode="false" sourceNode="P_10F10377" targetNode="P_15F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_20F10377"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10377_I" deadCode="false" sourceNode="P_10F10377" targetNode="P_16F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_25F10377"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10377_O" deadCode="false" sourceNode="P_10F10377" targetNode="P_17F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_25F10377"/>
	</edges>
	<edges id="P_10F10377P_11F10377" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10377" targetNode="P_11F10377"/>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10377_I" deadCode="false" sourceNode="P_12F10377" targetNode="P_18F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_32F10377"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10377_O" deadCode="false" sourceNode="P_12F10377" targetNode="P_19F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_32F10377"/>
	</edges>
	<edges id="P_12F10377P_13F10377" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10377" targetNode="P_13F10377"/>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10377_I" deadCode="false" sourceNode="P_18F10377" targetNode="P_20F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_48F10377"/>
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_70F10377"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10377_O" deadCode="false" sourceNode="P_18F10377" targetNode="P_21F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_48F10377"/>
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_70F10377"/>
	</edges>
	<edges id="P_18F10377P_19F10377" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10377" targetNode="P_19F10377"/>
	<edges id="P_14F10377P_15F10377" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10377" targetNode="P_15F10377"/>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10377_I" deadCode="false" sourceNode="P_16F10377" targetNode="P_22F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_119F10377"/>
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_130F10377"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10377_O" deadCode="false" sourceNode="P_16F10377" targetNode="P_23F10377">
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_119F10377"/>
		<representations href="../../../cobol/LVVS2780.cbl.cobModel#S_130F10377"/>
	</edges>
	<edges id="P_16F10377P_17F10377" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10377" targetNode="P_17F10377"/>
	<edges id="P_22F10377P_23F10377" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10377" targetNode="P_23F10377"/>
	<edges id="P_8F10377P_9F10377" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10377" targetNode="P_9F10377"/>
	<edges id="P_20F10377P_21F10377" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10377" targetNode="P_21F10377"/>
	<edges xsi:type="cbl:CallEdge" id="S_56F10377" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_18F10377" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_56F10377"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_96F10377" deadCode="false" name="Dynamic LDBS1420" sourceNode="P_14F10377" targetNode="LDBS1420">
		<representations href="../../../cobol/../importantStmts.cobModel#S_96F10377"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_120F10377" deadCode="false" name="Dynamic IDBSP580" sourceNode="P_16F10377" targetNode="IDBSP580">
		<representations href="../../../cobol/../importantStmts.cobModel#S_120F10377"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_138F10377" deadCode="true" name="Dynamic LDBS1420" sourceNode="P_24F10377" targetNode="Dynamic_LVVS2780_LDBS1420">
		<representations href="../../../cobol/../importantStmts.cobModel#S_138F10377"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_148F10377" deadCode="false" name="Dynamic IDBSP580" sourceNode="P_22F10377" targetNode="IDBSP580">
		<representations href="../../../cobol/../importantStmts.cobModel#S_148F10377"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_162F10377" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_20F10377" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_162F10377"></representations>
	</edges>
</Package>

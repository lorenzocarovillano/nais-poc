<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0046" cbl:id="LVVS0046" xsi:id="LVVS0046" packageRef="LVVS0046.igd#LVVS0046" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0046_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10328" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0046.cbl.cobModel#SC_1F10328"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10328" deadCode="false" name="PROGRAM_LVVS0046_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0046.cbl.cobModel#P_1F10328"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10328" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0046.cbl.cobModel#P_2F10328"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10328" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0046.cbl.cobModel#P_3F10328"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10328" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0046.cbl.cobModel#P_4F10328"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10328" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0046.cbl.cobModel#P_5F10328"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10328" deadCode="false" name="S1250-CALCOLA-DATA1">
				<representations href="../../../cobol/LVVS0046.cbl.cobModel#P_10F10328"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10328" deadCode="false" name="S1250-EX">
				<representations href="../../../cobol/LVVS0046.cbl.cobModel#P_11F10328"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10328" deadCode="false" name="S1260-CALCOLA-DIFF">
				<representations href="../../../cobol/LVVS0046.cbl.cobModel#P_12F10328"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10328" deadCode="false" name="S1260-EX">
				<representations href="../../../cobol/LVVS0046.cbl.cobModel#P_13F10328"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10328" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0046.cbl.cobModel#P_8F10328"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10328" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0046.cbl.cobModel#P_9F10328"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10328" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0046.cbl.cobModel#P_6F10328"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10328" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0046.cbl.cobModel#P_7F10328"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS5760" name="LDBS5760">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10223"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0010" name="LCCS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10122"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10328P_1F10328" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10328" targetNode="P_1F10328"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10328_I" deadCode="false" sourceNode="P_1F10328" targetNode="P_2F10328">
		<representations href="../../../cobol/LVVS0046.cbl.cobModel#S_1F10328"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10328_O" deadCode="false" sourceNode="P_1F10328" targetNode="P_3F10328">
		<representations href="../../../cobol/LVVS0046.cbl.cobModel#S_1F10328"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10328_I" deadCode="false" sourceNode="P_1F10328" targetNode="P_4F10328">
		<representations href="../../../cobol/LVVS0046.cbl.cobModel#S_2F10328"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10328_O" deadCode="false" sourceNode="P_1F10328" targetNode="P_5F10328">
		<representations href="../../../cobol/LVVS0046.cbl.cobModel#S_2F10328"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10328_I" deadCode="false" sourceNode="P_1F10328" targetNode="P_6F10328">
		<representations href="../../../cobol/LVVS0046.cbl.cobModel#S_3F10328"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10328_O" deadCode="false" sourceNode="P_1F10328" targetNode="P_7F10328">
		<representations href="../../../cobol/LVVS0046.cbl.cobModel#S_3F10328"/>
	</edges>
	<edges id="P_2F10328P_3F10328" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10328" targetNode="P_3F10328"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10328_I" deadCode="false" sourceNode="P_4F10328" targetNode="P_8F10328">
		<representations href="../../../cobol/LVVS0046.cbl.cobModel#S_11F10328"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10328_O" deadCode="false" sourceNode="P_4F10328" targetNode="P_9F10328">
		<representations href="../../../cobol/LVVS0046.cbl.cobModel#S_11F10328"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10328_I" deadCode="false" sourceNode="P_4F10328" targetNode="P_10F10328">
		<representations href="../../../cobol/LVVS0046.cbl.cobModel#S_13F10328"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10328_O" deadCode="false" sourceNode="P_4F10328" targetNode="P_11F10328">
		<representations href="../../../cobol/LVVS0046.cbl.cobModel#S_13F10328"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10328_I" deadCode="false" sourceNode="P_4F10328" targetNode="P_12F10328">
		<representations href="../../../cobol/LVVS0046.cbl.cobModel#S_15F10328"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10328_O" deadCode="false" sourceNode="P_4F10328" targetNode="P_13F10328">
		<representations href="../../../cobol/LVVS0046.cbl.cobModel#S_15F10328"/>
	</edges>
	<edges id="P_4F10328P_5F10328" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10328" targetNode="P_5F10328"/>
	<edges id="P_10F10328P_11F10328" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10328" targetNode="P_11F10328"/>
	<edges id="P_12F10328P_13F10328" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10328" targetNode="P_13F10328"/>
	<edges id="P_8F10328P_9F10328" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10328" targetNode="P_9F10328"/>
	<edges xsi:type="cbl:CallEdge" id="S_21F10328" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_10F10328" targetNode="LDBS5760">
		<representations href="../../../cobol/../importantStmts.cobModel#S_21F10328"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_42F10328" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_12F10328" targetNode="LCCS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_42F10328"></representations>
	</edges>
</Package>

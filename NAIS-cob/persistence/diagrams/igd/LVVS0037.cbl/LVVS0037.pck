<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0037" cbl:id="LVVS0037" xsi:id="LVVS0037" packageRef="LVVS0037.igd#LVVS0037" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0037_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10325" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0037.cbl.cobModel#SC_1F10325"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10325" deadCode="false" name="PROGRAM_LVVS0037_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0037.cbl.cobModel#P_1F10325"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10325" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0037.cbl.cobModel#P_2F10325"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10325" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0037.cbl.cobModel#P_3F10325"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10325" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0037.cbl.cobModel#P_4F10325"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10325" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0037.cbl.cobModel#P_5F10325"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10325" deadCode="false" name="VERIFICA-MOVIMENTO">
				<representations href="../../../cobol/LVVS0037.cbl.cobModel#P_12F10325"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10325" deadCode="false" name="VERIFICA-MOVIMENTO-EX">
				<representations href="../../../cobol/LVVS0037.cbl.cobModel#P_13F10325"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10325" deadCode="false" name="RECUP-MOVI-COMUN">
				<representations href="../../../cobol/LVVS0037.cbl.cobModel#P_16F10325"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10325" deadCode="false" name="RECUP-MOVI-COMUN-EX">
				<representations href="../../../cobol/LVVS0037.cbl.cobModel#P_17F10325"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10325" deadCode="false" name="CLOSE-MOVI">
				<representations href="../../../cobol/LVVS0037.cbl.cobModel#P_18F10325"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10325" deadCode="false" name="CLOSE-MOVI-EX">
				<representations href="../../../cobol/LVVS0037.cbl.cobModel#P_19F10325"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10325" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0037.cbl.cobModel#P_8F10325"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10325" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0037.cbl.cobModel#P_9F10325"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10325" deadCode="false" name="S1250-CALCOLA-DATA1">
				<representations href="../../../cobol/LVVS0037.cbl.cobModel#P_14F10325"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10325" deadCode="false" name="S1250-EX">
				<representations href="../../../cobol/LVVS0037.cbl.cobModel#P_15F10325"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10325" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS0037.cbl.cobModel#P_10F10325"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10325" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS0037.cbl.cobModel#P_11F10325"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10325" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0037.cbl.cobModel#P_6F10325"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10325" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0037.cbl.cobModel#P_7F10325"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS6040" name="LDBS6040">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10230"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS1700" name="LDBS1700">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10166"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10325P_1F10325" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10325" targetNode="P_1F10325"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10325_I" deadCode="false" sourceNode="P_1F10325" targetNode="P_2F10325">
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_1F10325"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10325_O" deadCode="false" sourceNode="P_1F10325" targetNode="P_3F10325">
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_1F10325"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10325_I" deadCode="false" sourceNode="P_1F10325" targetNode="P_4F10325">
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_2F10325"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10325_O" deadCode="false" sourceNode="P_1F10325" targetNode="P_5F10325">
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_2F10325"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10325_I" deadCode="false" sourceNode="P_1F10325" targetNode="P_6F10325">
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_3F10325"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10325_O" deadCode="false" sourceNode="P_1F10325" targetNode="P_7F10325">
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_3F10325"/>
	</edges>
	<edges id="P_2F10325P_3F10325" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10325" targetNode="P_3F10325"/>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10325_I" deadCode="false" sourceNode="P_4F10325" targetNode="P_8F10325">
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_12F10325"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10325_O" deadCode="false" sourceNode="P_4F10325" targetNode="P_9F10325">
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_12F10325"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10325_I" deadCode="false" sourceNode="P_4F10325" targetNode="P_10F10325">
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_14F10325"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10325_O" deadCode="false" sourceNode="P_4F10325" targetNode="P_11F10325">
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_14F10325"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10325_I" deadCode="false" sourceNode="P_4F10325" targetNode="P_12F10325">
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_29F10325"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10325_O" deadCode="false" sourceNode="P_4F10325" targetNode="P_13F10325">
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_29F10325"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10325_I" deadCode="false" sourceNode="P_4F10325" targetNode="P_14F10325">
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_31F10325"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10325_O" deadCode="false" sourceNode="P_4F10325" targetNode="P_15F10325">
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_31F10325"/>
	</edges>
	<edges id="P_4F10325P_5F10325" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10325" targetNode="P_5F10325"/>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10325_I" deadCode="false" sourceNode="P_12F10325" targetNode="P_16F10325">
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_36F10325"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10325_O" deadCode="false" sourceNode="P_12F10325" targetNode="P_17F10325">
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_36F10325"/>
	</edges>
	<edges id="P_12F10325P_13F10325" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10325" targetNode="P_13F10325"/>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10325_I" deadCode="false" sourceNode="P_16F10325" targetNode="P_18F10325">
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_49F10325"/>
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_71F10325"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10325_O" deadCode="false" sourceNode="P_16F10325" targetNode="P_19F10325">
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_49F10325"/>
		<representations href="../../../cobol/LVVS0037.cbl.cobModel#S_71F10325"/>
	</edges>
	<edges id="P_16F10325P_17F10325" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10325" targetNode="P_17F10325"/>
	<edges id="P_18F10325P_19F10325" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10325" targetNode="P_19F10325"/>
	<edges id="P_8F10325P_9F10325" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10325" targetNode="P_9F10325"/>
	<edges id="P_14F10325P_15F10325" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10325" targetNode="P_15F10325"/>
	<edges id="P_10F10325P_11F10325" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10325" targetNode="P_11F10325"/>
	<edges xsi:type="cbl:CallEdge" id="S_57F10325" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_16F10325" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_57F10325"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_83F10325" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_18F10325" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_83F10325"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_107F10325" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_14F10325" targetNode="LDBS1700">
		<representations href="../../../cobol/../importantStmts.cobModel#S_107F10325"></representations>
	</edges>
</Package>

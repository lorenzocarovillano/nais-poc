<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LCCS0025" cbl:id="LCCS0025" xsi:id="LCCS0025" packageRef="LCCS0025.igd#LCCS0025" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LCCS0025_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10128" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LCCS0025.cbl.cobModel#SC_1F10128"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10128" deadCode="false" name="PROGRAM_LCCS0025_FIRST_SENTENCES">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_1F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10128" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_2F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10128" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_3F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10128" deadCode="false" name="S0050-CONTROLLI">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_8F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10128" deadCode="false" name="EX-S0050">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_9F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10128" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_4F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10128" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_5F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10128" deadCode="false" name="S1100-LETTURA-POLI">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_12F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10128" deadCode="false" name="EX-S1100">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_13F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10128" deadCode="false" name="S1110-PREPARA-AREA-POLI">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_14F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10128" deadCode="false" name="EX-S1110">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_15F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10128" deadCode="false" name="S1120-CALL-POLI">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_16F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10128" deadCode="false" name="EX-S1120">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_17F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10128" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_6F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10128" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_7F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10128" deadCode="false" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_18F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10128" deadCode="false" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_19F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10128" deadCode="true" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_20F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10128" deadCode="true" name="EX-S0290">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_21F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10128" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_10F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10128" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_11F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10128" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_24F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10128" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_25F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10128" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_22F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10128" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_23F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10128" deadCode="true" name="S0320-OUTPUT-PRODOTTO">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_26F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10128" deadCode="true" name="EX-S0320-OUTPUT-PRODOTTO">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_31F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10128" deadCode="true" name="S0322-SALVA-ERRORE">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_27F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10128" deadCode="true" name="EX-S0322">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_28F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10128" deadCode="true" name="S0321-ERRORI-DEROGA">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_29F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10128" deadCode="true" name="EX-S0321">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_30F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10128" deadCode="true" name="S0323-SALVA-DEROGA">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_32F10128"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10128" deadCode="true" name="EX-S0323">
				<representations href="../../../cobol/LCCS0025.cbl.cobModel#P_33F10128"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0010" name="IDSS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10099"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10128P_1F10128" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10128" targetNode="P_1F10128"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10128_I" deadCode="false" sourceNode="P_1F10128" targetNode="P_2F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_1F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10128_O" deadCode="false" sourceNode="P_1F10128" targetNode="P_3F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_1F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10128_I" deadCode="false" sourceNode="P_1F10128" targetNode="P_4F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_3F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10128_O" deadCode="false" sourceNode="P_1F10128" targetNode="P_5F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_3F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10128_I" deadCode="false" sourceNode="P_1F10128" targetNode="P_6F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_4F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10128_O" deadCode="false" sourceNode="P_1F10128" targetNode="P_7F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_4F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10128_I" deadCode="false" sourceNode="P_2F10128" targetNode="P_8F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_5F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10128_O" deadCode="false" sourceNode="P_2F10128" targetNode="P_9F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_5F10128"/>
	</edges>
	<edges id="P_2F10128P_3F10128" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10128" targetNode="P_3F10128"/>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10128_I" deadCode="false" sourceNode="P_8F10128" targetNode="P_10F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_12F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10128_O" deadCode="false" sourceNode="P_8F10128" targetNode="P_11F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_12F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10128_I" deadCode="false" sourceNode="P_8F10128" targetNode="P_10F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_19F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10128_O" deadCode="false" sourceNode="P_8F10128" targetNode="P_11F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_19F10128"/>
	</edges>
	<edges id="P_8F10128P_9F10128" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10128" targetNode="P_9F10128"/>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10128_I" deadCode="false" sourceNode="P_4F10128" targetNode="P_12F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_21F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10128_O" deadCode="false" sourceNode="P_4F10128" targetNode="P_13F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_21F10128"/>
	</edges>
	<edges id="P_4F10128P_5F10128" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10128" targetNode="P_5F10128"/>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10128_I" deadCode="false" sourceNode="P_12F10128" targetNode="P_14F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_23F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10128_O" deadCode="false" sourceNode="P_12F10128" targetNode="P_15F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_23F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10128_I" deadCode="false" sourceNode="P_12F10128" targetNode="P_16F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_24F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10128_O" deadCode="false" sourceNode="P_12F10128" targetNode="P_17F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_24F10128"/>
	</edges>
	<edges id="P_12F10128P_13F10128" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10128" targetNode="P_13F10128"/>
	<edges id="P_14F10128P_15F10128" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10128" targetNode="P_15F10128"/>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10128_I" deadCode="false" sourceNode="P_16F10128" targetNode="P_18F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_42F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10128_O" deadCode="false" sourceNode="P_16F10128" targetNode="P_19F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_42F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10128_I" deadCode="false" sourceNode="P_16F10128" targetNode="P_10F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_53F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10128_O" deadCode="false" sourceNode="P_16F10128" targetNode="P_11F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_53F10128"/>
	</edges>
	<edges id="P_16F10128P_17F10128" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10128" targetNode="P_17F10128"/>
	<edges id="P_18F10128P_19F10128" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10128" targetNode="P_19F10128"/>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10128_I" deadCode="true" sourceNode="P_20F10128" targetNode="P_10F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_88F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10128_O" deadCode="true" sourceNode="P_20F10128" targetNode="P_11F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_88F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10128_I" deadCode="false" sourceNode="P_10F10128" targetNode="P_22F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_95F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10128_O" deadCode="false" sourceNode="P_10F10128" targetNode="P_23F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_95F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10128_I" deadCode="false" sourceNode="P_10F10128" targetNode="P_24F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_99F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10128_O" deadCode="false" sourceNode="P_10F10128" targetNode="P_25F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_99F10128"/>
	</edges>
	<edges id="P_10F10128P_11F10128" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10128" targetNode="P_11F10128"/>
	<edges id="P_24F10128P_25F10128" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10128" targetNode="P_25F10128"/>
	<edges id="P_22F10128P_23F10128" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10128" targetNode="P_23F10128"/>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10128_I" deadCode="true" sourceNode="P_26F10128" targetNode="P_27F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_145F10128"/>
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_147F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10128_O" deadCode="true" sourceNode="P_26F10128" targetNode="P_28F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_145F10128"/>
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_147F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10128_I" deadCode="true" sourceNode="P_26F10128" targetNode="P_29F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_148F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10128_O" deadCode="true" sourceNode="P_26F10128" targetNode="P_30F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_148F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10128_I" deadCode="true" sourceNode="P_27F10128" targetNode="P_10F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_153F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10128_O" deadCode="true" sourceNode="P_27F10128" targetNode="P_11F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_153F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10128_I" deadCode="true" sourceNode="P_29F10128" targetNode="P_27F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_157F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10128_O" deadCode="true" sourceNode="P_29F10128" targetNode="P_28F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_157F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_159F10128_I" deadCode="true" sourceNode="P_29F10128" targetNode="P_27F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_159F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_159F10128_O" deadCode="true" sourceNode="P_29F10128" targetNode="P_28F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_159F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10128_I" deadCode="true" sourceNode="P_29F10128" targetNode="P_27F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_161F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10128_O" deadCode="true" sourceNode="P_29F10128" targetNode="P_28F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_161F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10128_I" deadCode="true" sourceNode="P_29F10128" targetNode="P_27F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_164F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10128_O" deadCode="true" sourceNode="P_29F10128" targetNode="P_28F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_164F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10128_I" deadCode="true" sourceNode="P_29F10128" targetNode="P_32F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_165F10128"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10128_O" deadCode="true" sourceNode="P_29F10128" targetNode="P_33F10128">
		<representations href="../../../cobol/LCCS0025.cbl.cobModel#S_165F10128"/>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_83F10128" deadCode="false" name="Dynamic IDSI0011-PGM" sourceNode="P_18F10128" targetNode="IDSS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_83F10128"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_93F10128" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_10F10128" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_93F10128"></representations>
	</edges>
</Package>

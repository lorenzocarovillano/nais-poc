<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0091" cbl:id="LVVS0091" xsi:id="LVVS0091" packageRef="LVVS0091.igd#LVVS0091" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0091_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10340" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0091.cbl.cobModel#SC_1F10340"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10340" deadCode="false" name="PROGRAM_LVVS0091_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0091.cbl.cobModel#P_1F10340"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10340" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0091.cbl.cobModel#P_2F10340"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10340" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0091.cbl.cobModel#P_3F10340"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10340" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0091.cbl.cobModel#P_4F10340"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10340" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0091.cbl.cobModel#P_5F10340"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10340" deadCode="true" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0091.cbl.cobModel#P_8F10340"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10340" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0091.cbl.cobModel#P_9F10340"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10340" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS0091.cbl.cobModel#P_10F10340"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10340" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS0091.cbl.cobModel#P_11F10340"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10340" deadCode="false" name="S1250-CALCOLA-DATA1">
				<representations href="../../../cobol/LVVS0091.cbl.cobModel#P_12F10340"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10340" deadCode="false" name="S1250-EX">
				<representations href="../../../cobol/LVVS0091.cbl.cobModel#P_13F10340"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10340" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0091.cbl.cobModel#P_6F10340"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10340" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0091.cbl.cobModel#P_7F10340"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS2900" name="LDBS2900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10189"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10340P_1F10340" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10340" targetNode="P_1F10340"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10340_I" deadCode="false" sourceNode="P_1F10340" targetNode="P_2F10340">
		<representations href="../../../cobol/LVVS0091.cbl.cobModel#S_1F10340"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10340_O" deadCode="false" sourceNode="P_1F10340" targetNode="P_3F10340">
		<representations href="../../../cobol/LVVS0091.cbl.cobModel#S_1F10340"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10340_I" deadCode="false" sourceNode="P_1F10340" targetNode="P_4F10340">
		<representations href="../../../cobol/LVVS0091.cbl.cobModel#S_2F10340"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10340_O" deadCode="false" sourceNode="P_1F10340" targetNode="P_5F10340">
		<representations href="../../../cobol/LVVS0091.cbl.cobModel#S_2F10340"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10340_I" deadCode="false" sourceNode="P_1F10340" targetNode="P_6F10340">
		<representations href="../../../cobol/LVVS0091.cbl.cobModel#S_3F10340"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10340_O" deadCode="false" sourceNode="P_1F10340" targetNode="P_7F10340">
		<representations href="../../../cobol/LVVS0091.cbl.cobModel#S_3F10340"/>
	</edges>
	<edges id="P_2F10340P_3F10340" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10340" targetNode="P_3F10340"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10340_I" deadCode="true" sourceNode="P_4F10340" targetNode="P_8F10340">
		<representations href="../../../cobol/LVVS0091.cbl.cobModel#S_10F10340"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10340_O" deadCode="true" sourceNode="P_4F10340" targetNode="P_9F10340">
		<representations href="../../../cobol/LVVS0091.cbl.cobModel#S_10F10340"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10340_I" deadCode="false" sourceNode="P_4F10340" targetNode="P_10F10340">
		<representations href="../../../cobol/LVVS0091.cbl.cobModel#S_12F10340"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10340_O" deadCode="false" sourceNode="P_4F10340" targetNode="P_11F10340">
		<representations href="../../../cobol/LVVS0091.cbl.cobModel#S_12F10340"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10340_I" deadCode="false" sourceNode="P_4F10340" targetNode="P_12F10340">
		<representations href="../../../cobol/LVVS0091.cbl.cobModel#S_17F10340"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10340_O" deadCode="false" sourceNode="P_4F10340" targetNode="P_13F10340">
		<representations href="../../../cobol/LVVS0091.cbl.cobModel#S_17F10340"/>
	</edges>
	<edges id="P_4F10340P_5F10340" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10340" targetNode="P_5F10340"/>
	<edges id="P_8F10340P_9F10340" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10340" targetNode="P_9F10340"/>
	<edges id="P_10F10340P_11F10340" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10340" targetNode="P_11F10340"/>
	<edges id="P_12F10340P_13F10340" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10340" targetNode="P_13F10340"/>
	<edges xsi:type="cbl:CallEdge" id="S_25F10340" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_12F10340" targetNode="LDBS2900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_25F10340"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LCCS0490" cbl:id="LCCS0490" xsi:id="LCCS0490" packageRef="LCCS0490.igd#LCCS0490" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LCCS0490_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10137" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LCCS0490.cbl.cobModel#SC_1F10137"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10137" deadCode="false" name="PROGRAM_LCCS0490_FIRST_SENTENCES">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_1F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10137" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_2F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10137" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_3F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10137" deadCode="false" name="A010-CONTROLLI-INPUT">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_10F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10137" deadCode="false" name="A010-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_11F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10137" deadCode="false" name="A020-LETTURA-PCO">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_12F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10137" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_13F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10137" deadCode="false" name="A100-ELABORA">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_4F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10137" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_5F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10137" deadCode="false" name="A110-VERIFICA-GARANZIE">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_18F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10137" deadCode="false" name="A110-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_19F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10137" deadCode="false" name="A120-ULT-TIT-INCAS">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_20F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10137" deadCode="false" name="A120-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_21F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10137" deadCode="false" name="A121-CALCOLO-DIFF-DATE">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_22F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10137" deadCode="false" name="A121-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_23F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10137" deadCode="false" name="A122-CALL-LCCS0062">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_32F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10137" deadCode="false" name="A122-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_33F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10137" deadCode="false" name="A123-SCORRI-RATE">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_34F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10137" deadCode="false" name="A123-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_35F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10137" deadCode="false" name="A124-PREPARA-AREA-TIT">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_24F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10137" deadCode="false" name="A124-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_25F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10137" deadCode="false" name="A125-ESTRAZIONE-TIT">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_26F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10137" deadCode="false" name="A125-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_27F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10137" deadCode="false" name="A126-PREPARA-AREA-DTC">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_28F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10137" deadCode="false" name="A126-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_29F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10137" deadCode="false" name="A127-ESTRAZIONE-DTC">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_30F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10137" deadCode="false" name="A127-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_31F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10137" deadCode="false" name="A128-PREP-CALOLO-DT">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_38F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10137" deadCode="false" name="A128-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_39F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10137" deadCode="false" name="A129-CALOLO-DT">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_40F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10137" deadCode="false" name="A129-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_41F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10137" deadCode="false" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_36F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10137" deadCode="false" name="EX-S0290">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_37F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10137" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_14F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10137" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_15F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10137" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_48F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10137" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_49F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10137" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_46F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10137" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_47F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10137" deadCode="false" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_16F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10137" deadCode="false" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_17F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10137" deadCode="false" name="VALORIZZA-OUTPUT-TIT">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_42F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10137" deadCode="false" name="VALORIZZA-OUTPUT-TIT-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_43F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10137" deadCode="false" name="INIZIA-TOT-TIT">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_6F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10137" deadCode="false" name="INIZIA-TOT-TIT-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_7F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10137" deadCode="true" name="INIZIA-NULL-TIT">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_54F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10137" deadCode="false" name="INIZIA-NULL-TIT-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_55F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10137" deadCode="false" name="INIZIA-ZEROES-TIT">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_50F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10137" deadCode="false" name="INIZIA-ZEROES-TIT-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_51F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10137" deadCode="true" name="INIZIA-SPACES-TIT">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_52F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10137" deadCode="false" name="INIZIA-SPACES-TIT-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_53F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10137" deadCode="true" name="VALORIZZA-OUTPUT-DTC">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_44F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10137" deadCode="false" name="VALORIZZA-OUTPUT-DTC-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_45F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10137" deadCode="true" name="INIZIA-TOT-DTC">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_8F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10137" deadCode="false" name="INIZIA-TOT-DTC-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_9F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10137" deadCode="true" name="INIZIA-NULL-DTC">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_60F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10137" deadCode="false" name="INIZIA-NULL-DTC-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_61F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10137" deadCode="true" name="INIZIA-ZEROES-DTC">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_56F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10137" deadCode="false" name="INIZIA-ZEROES-DTC-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_57F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10137" deadCode="true" name="INIZIA-SPACES-DTC">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_58F10137"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10137" deadCode="false" name="INIZIA-SPACES-DTC-EX">
				<representations href="../../../cobol/LCCS0490.cbl.cobModel#P_59F10137"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0010" name="LCCS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10122"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0062" name="LCCS0062">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10131"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0003" name="LCCS0003">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10119"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0010" name="IDSS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10099"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10137P_1F10137" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10137" targetNode="P_1F10137"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10137_I" deadCode="false" sourceNode="P_1F10137" targetNode="P_2F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_1F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10137_O" deadCode="false" sourceNode="P_1F10137" targetNode="P_3F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_1F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10137_I" deadCode="false" sourceNode="P_1F10137" targetNode="P_4F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_3F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10137_O" deadCode="false" sourceNode="P_1F10137" targetNode="P_5F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_3F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10137_I" deadCode="false" sourceNode="P_2F10137" targetNode="P_6F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_14F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_15F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10137_O" deadCode="false" sourceNode="P_2F10137" targetNode="P_7F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_14F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_15F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10137_I" deadCode="true" sourceNode="P_2F10137" targetNode="P_8F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_17F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_18F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10137_O" deadCode="true" sourceNode="P_2F10137" targetNode="P_9F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_17F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_18F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10137_I" deadCode="false" sourceNode="P_2F10137" targetNode="P_10F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_26F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10137_O" deadCode="false" sourceNode="P_2F10137" targetNode="P_11F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_26F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10137_I" deadCode="false" sourceNode="P_2F10137" targetNode="P_12F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_28F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10137_O" deadCode="false" sourceNode="P_2F10137" targetNode="P_13F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_28F10137"/>
	</edges>
	<edges id="P_2F10137P_3F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10137" targetNode="P_3F10137"/>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10137_I" deadCode="false" sourceNode="P_10F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_36F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10137_O" deadCode="false" sourceNode="P_10F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_36F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10137_I" deadCode="false" sourceNode="P_10F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_44F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10137_O" deadCode="false" sourceNode="P_10F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_44F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10137_I" deadCode="false" sourceNode="P_10F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_45F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_51F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10137_O" deadCode="false" sourceNode="P_10F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_45F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_51F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10137_I" deadCode="false" sourceNode="P_10F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_45F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_59F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10137_O" deadCode="false" sourceNode="P_10F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_45F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_59F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10137_I" deadCode="false" sourceNode="P_10F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_45F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_67F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10137_O" deadCode="false" sourceNode="P_10F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_45F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_67F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10137_I" deadCode="false" sourceNode="P_10F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_45F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_74F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10137_O" deadCode="false" sourceNode="P_10F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_45F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_74F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10137_I" deadCode="false" sourceNode="P_10F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_45F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_82F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10137_O" deadCode="false" sourceNode="P_10F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_45F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_82F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10137_I" deadCode="false" sourceNode="P_10F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_45F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_89F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10137_O" deadCode="false" sourceNode="P_10F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_45F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_89F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10137_I" deadCode="false" sourceNode="P_10F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_45F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_95F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10137_O" deadCode="false" sourceNode="P_10F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_45F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_95F10137"/>
	</edges>
	<edges id="P_10F10137P_11F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10137" targetNode="P_11F10137"/>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10137_I" deadCode="false" sourceNode="P_12F10137" targetNode="P_16F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_105F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10137_O" deadCode="false" sourceNode="P_12F10137" targetNode="P_17F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_105F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10137_I" deadCode="false" sourceNode="P_12F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_113F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10137_O" deadCode="false" sourceNode="P_12F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_113F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10137_I" deadCode="false" sourceNode="P_12F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_118F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10137_O" deadCode="false" sourceNode="P_12F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_118F10137"/>
	</edges>
	<edges id="P_12F10137P_13F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10137" targetNode="P_13F10137"/>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10137_I" deadCode="false" sourceNode="P_4F10137" targetNode="P_18F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_120F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10137_O" deadCode="false" sourceNode="P_4F10137" targetNode="P_19F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_120F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10137_I" deadCode="false" sourceNode="P_4F10137" targetNode="P_20F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_122F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10137_O" deadCode="false" sourceNode="P_4F10137" targetNode="P_21F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_122F10137"/>
	</edges>
	<edges id="P_4F10137P_5F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10137" targetNode="P_5F10137"/>
	<edges id="P_18F10137P_19F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10137" targetNode="P_19F10137"/>
	<edges xsi:type="cbl:PerformEdge" id="S_151F10137_I" deadCode="false" sourceNode="P_20F10137" targetNode="P_16F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_151F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_151F10137_O" deadCode="false" sourceNode="P_20F10137" targetNode="P_17F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_151F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_159F10137_I" deadCode="false" sourceNode="P_20F10137" targetNode="P_22F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_159F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_159F10137_O" deadCode="false" sourceNode="P_20F10137" targetNode="P_23F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_159F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10137_I" deadCode="false" sourceNode="P_20F10137" targetNode="P_24F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_161F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10137_O" deadCode="false" sourceNode="P_20F10137" targetNode="P_25F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_161F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10137_I" deadCode="false" sourceNode="P_20F10137" targetNode="P_26F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_162F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10137_O" deadCode="false" sourceNode="P_20F10137" targetNode="P_27F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_162F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10137_I" deadCode="false" sourceNode="P_20F10137" targetNode="P_28F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_164F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10137_O" deadCode="false" sourceNode="P_20F10137" targetNode="P_29F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_164F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10137_I" deadCode="false" sourceNode="P_20F10137" targetNode="P_30F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_165F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10137_O" deadCode="false" sourceNode="P_20F10137" targetNode="P_31F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_165F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10137_I" deadCode="false" sourceNode="P_20F10137" targetNode="P_32F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_167F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10137_O" deadCode="false" sourceNode="P_20F10137" targetNode="P_33F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_167F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10137_I" deadCode="false" sourceNode="P_20F10137" targetNode="P_34F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_168F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10137_O" deadCode="false" sourceNode="P_20F10137" targetNode="P_35F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_168F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10137_I" deadCode="false" sourceNode="P_20F10137" targetNode="P_22F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_172F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10137_O" deadCode="false" sourceNode="P_20F10137" targetNode="P_23F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_172F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10137_I" deadCode="false" sourceNode="P_20F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_177F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10137_O" deadCode="false" sourceNode="P_20F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_177F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_182F10137_I" deadCode="false" sourceNode="P_20F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_182F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_182F10137_O" deadCode="false" sourceNode="P_20F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_182F10137"/>
	</edges>
	<edges id="P_20F10137P_21F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10137" targetNode="P_21F10137"/>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10137_I" deadCode="false" sourceNode="P_22F10137" targetNode="P_36F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_191F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10137_O" deadCode="false" sourceNode="P_22F10137" targetNode="P_37F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_191F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_198F10137_I" deadCode="false" sourceNode="P_22F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_198F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_198F10137_O" deadCode="false" sourceNode="P_22F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_198F10137"/>
	</edges>
	<edges id="P_22F10137P_23F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10137" targetNode="P_23F10137"/>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10137_I" deadCode="false" sourceNode="P_32F10137" targetNode="P_36F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_213F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10137_O" deadCode="false" sourceNode="P_32F10137" targetNode="P_37F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_213F10137"/>
	</edges>
	<edges id="P_32F10137P_33F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10137" targetNode="P_33F10137"/>
	<edges id="P_34F10137P_35F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10137" targetNode="P_35F10137"/>
	<edges xsi:type="cbl:PerformEdge" id="S_236F10137_I" deadCode="false" sourceNode="P_24F10137" targetNode="P_38F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_236F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_236F10137_O" deadCode="false" sourceNode="P_24F10137" targetNode="P_39F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_236F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_237F10137_I" deadCode="false" sourceNode="P_24F10137" targetNode="P_40F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_237F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_237F10137_O" deadCode="false" sourceNode="P_24F10137" targetNode="P_41F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_237F10137"/>
	</edges>
	<edges id="P_24F10137P_25F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10137" targetNode="P_25F10137"/>
	<edges xsi:type="cbl:PerformEdge" id="S_244F10137_I" deadCode="false" sourceNode="P_26F10137" targetNode="P_16F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_243F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_244F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_244F10137_O" deadCode="false" sourceNode="P_26F10137" targetNode="P_17F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_243F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_244F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10137_I" deadCode="false" sourceNode="P_26F10137" targetNode="P_22F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_243F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_255F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10137_O" deadCode="false" sourceNode="P_26F10137" targetNode="P_23F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_243F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_255F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_257F10137_I" deadCode="false" sourceNode="P_26F10137" targetNode="P_42F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_243F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_257F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_257F10137_O" deadCode="false" sourceNode="P_26F10137" targetNode="P_43F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_243F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_257F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_266F10137_I" deadCode="false" sourceNode="P_26F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_243F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_266F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_266F10137_O" deadCode="false" sourceNode="P_26F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_243F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_266F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_271F10137_I" deadCode="false" sourceNode="P_26F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_243F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_271F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_271F10137_O" deadCode="false" sourceNode="P_26F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_243F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_271F10137"/>
	</edges>
	<edges id="P_26F10137P_27F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10137" targetNode="P_27F10137"/>
	<edges id="P_28F10137P_29F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10137" targetNode="P_29F10137"/>
	<edges xsi:type="cbl:PerformEdge" id="S_286F10137_I" deadCode="false" sourceNode="P_30F10137" targetNode="P_16F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_277F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_285F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_286F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_286F10137_O" deadCode="false" sourceNode="P_30F10137" targetNode="P_17F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_277F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_285F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_286F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10137_I" deadCode="false" sourceNode="P_30F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_277F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_285F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_294F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10137_O" deadCode="false" sourceNode="P_30F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_277F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_285F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_294F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_297F10137_I" deadCode="true" sourceNode="P_30F10137" targetNode="P_44F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_277F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_285F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_297F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_297F10137_O" deadCode="true" sourceNode="P_30F10137" targetNode="P_45F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_277F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_285F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_297F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_304F10137_I" deadCode="false" sourceNode="P_30F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_277F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_285F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_304F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_304F10137_O" deadCode="false" sourceNode="P_30F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_277F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_285F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_304F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10137_I" deadCode="false" sourceNode="P_30F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_277F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_285F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_309F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10137_O" deadCode="false" sourceNode="P_30F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_277F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_285F10137"/>
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_309F10137"/>
	</edges>
	<edges id="P_30F10137P_31F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10137" targetNode="P_31F10137"/>
	<edges id="P_38F10137P_39F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10137" targetNode="P_39F10137"/>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10137_I" deadCode="false" sourceNode="P_40F10137" targetNode="P_36F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_321F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10137_O" deadCode="false" sourceNode="P_40F10137" targetNode="P_37F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_321F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_327F10137_I" deadCode="false" sourceNode="P_40F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_327F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_327F10137_O" deadCode="false" sourceNode="P_40F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_327F10137"/>
	</edges>
	<edges id="P_40F10137P_41F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10137" targetNode="P_41F10137"/>
	<edges xsi:type="cbl:PerformEdge" id="S_331F10137_I" deadCode="false" sourceNode="P_36F10137" targetNode="P_14F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_331F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_331F10137_O" deadCode="false" sourceNode="P_36F10137" targetNode="P_15F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_331F10137"/>
	</edges>
	<edges id="P_36F10137P_37F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10137" targetNode="P_37F10137"/>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10137_I" deadCode="false" sourceNode="P_14F10137" targetNode="P_46F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_338F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10137_O" deadCode="false" sourceNode="P_14F10137" targetNode="P_47F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_338F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_342F10137_I" deadCode="false" sourceNode="P_14F10137" targetNode="P_48F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_342F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_342F10137_O" deadCode="false" sourceNode="P_14F10137" targetNode="P_49F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_342F10137"/>
	</edges>
	<edges id="P_14F10137P_15F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10137" targetNode="P_15F10137"/>
	<edges id="P_48F10137P_49F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10137" targetNode="P_49F10137"/>
	<edges id="P_46F10137P_47F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10137" targetNode="P_47F10137"/>
	<edges id="P_16F10137P_17F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10137" targetNode="P_17F10137"/>
	<edges id="P_42F10137P_43F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10137" targetNode="P_43F10137"/>
	<edges xsi:type="cbl:PerformEdge" id="S_637F10137_I" deadCode="false" sourceNode="P_6F10137" targetNode="P_50F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_637F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_637F10137_O" deadCode="false" sourceNode="P_6F10137" targetNode="P_51F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_637F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_638F10137_I" deadCode="true" sourceNode="P_6F10137" targetNode="P_52F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_638F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_638F10137_O" deadCode="true" sourceNode="P_6F10137" targetNode="P_53F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_638F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_639F10137_I" deadCode="true" sourceNode="P_6F10137" targetNode="P_54F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_639F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_639F10137_O" deadCode="true" sourceNode="P_6F10137" targetNode="P_55F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_639F10137"/>
	</edges>
	<edges id="P_6F10137P_7F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10137" targetNode="P_7F10137"/>
	<edges id="P_54F10137P_55F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10137" targetNode="P_55F10137"/>
	<edges id="P_50F10137P_51F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10137" targetNode="P_51F10137"/>
	<edges id="P_52F10137P_53F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10137" targetNode="P_53F10137"/>
	<edges id="P_44F10137P_45F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10137" targetNode="P_45F10137"/>
	<edges xsi:type="cbl:PerformEdge" id="S_898F10137_I" deadCode="true" sourceNode="P_8F10137" targetNode="P_56F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_898F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_898F10137_O" deadCode="true" sourceNode="P_8F10137" targetNode="P_57F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_898F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_899F10137_I" deadCode="true" sourceNode="P_8F10137" targetNode="P_58F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_899F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_899F10137_O" deadCode="true" sourceNode="P_8F10137" targetNode="P_59F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_899F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_900F10137_I" deadCode="true" sourceNode="P_8F10137" targetNode="P_60F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_900F10137"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_900F10137_O" deadCode="true" sourceNode="P_8F10137" targetNode="P_61F10137">
		<representations href="../../../cobol/LCCS0490.cbl.cobModel#S_900F10137"/>
	</edges>
	<edges id="P_8F10137P_9F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10137" targetNode="P_9F10137"/>
	<edges id="P_60F10137P_61F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_60F10137" targetNode="P_61F10137"/>
	<edges id="P_56F10137P_57F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10137" targetNode="P_57F10137"/>
	<edges id="P_58F10137P_59F10137" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10137" targetNode="P_59F10137"/>
	<edges xsi:type="cbl:CallEdge" id="S_187F10137" deadCode="false" name="Dynamic PGM-LCCS0010" sourceNode="P_22F10137" targetNode="LCCS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_187F10137"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_209F10137" deadCode="false" name="Dynamic PGM-LCCS0062" sourceNode="P_32F10137" targetNode="LCCS0062">
		<representations href="../../../cobol/../importantStmts.cobModel#S_209F10137"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_318F10137" deadCode="false" name="Dynamic LCCS0003" sourceNode="P_40F10137" targetNode="LCCS0003">
		<representations href="../../../cobol/../importantStmts.cobModel#S_318F10137"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_336F10137" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_14F10137" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_336F10137"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_402F10137" deadCode="false" name="Dynamic IDSI0011-PGM" sourceNode="P_16F10137" targetNode="IDSS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_402F10137"></representations>
	</edges>
</Package>

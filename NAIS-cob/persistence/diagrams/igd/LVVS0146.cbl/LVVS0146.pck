<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0146" cbl:id="LVVS0146" xsi:id="LVVS0146" packageRef="LVVS0146.igd#LVVS0146" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0146_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10357" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0146.cbl.cobModel#SC_1F10357"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10357" deadCode="false" name="PROGRAM_LVVS0146_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_1F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10357" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_2F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10357" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_3F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10357" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_4F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10357" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_5F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10357" deadCode="true" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_8F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10357" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_9F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10357" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_10F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10357" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_11F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10357" deadCode="false" name="S1301-LEGGI-TRCH-POS">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_16F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10357" deadCode="false" name="S1301-EX">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_17F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10357" deadCode="false" name="S1302-LEGGI-TRCH-NEG">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_18F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10357" deadCode="false" name="S1302-EX">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_19F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10357" deadCode="false" name="RECUP-MOVI-COMUN">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_14F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10357" deadCode="false" name="RECUP-MOVI-COMUN-EX">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_15F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10357" deadCode="false" name="RECUP-MOVI-COMTOT">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_12F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10357" deadCode="false" name="RECUP-MOVI-COMTOT-EX">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_13F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10357" deadCode="false" name="CLOSE-MOVI">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_20F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10357" deadCode="false" name="CLOSE-MOVI-EX">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_21F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10357" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_6F10357"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10357" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0146.cbl.cobModel#P_7F10357"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS7120" name="LDBS7120">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10236"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS6040" name="LDBS6040">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10230"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10357P_1F10357" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10357" targetNode="P_1F10357"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10357_I" deadCode="false" sourceNode="P_1F10357" targetNode="P_2F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_1F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10357_O" deadCode="false" sourceNode="P_1F10357" targetNode="P_3F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_1F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10357_I" deadCode="false" sourceNode="P_1F10357" targetNode="P_4F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_3F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10357_O" deadCode="false" sourceNode="P_1F10357" targetNode="P_5F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_3F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10357_I" deadCode="false" sourceNode="P_1F10357" targetNode="P_6F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_4F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10357_O" deadCode="false" sourceNode="P_1F10357" targetNode="P_7F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_4F10357"/>
	</edges>
	<edges id="P_2F10357P_3F10357" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10357" targetNode="P_3F10357"/>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10357_I" deadCode="true" sourceNode="P_4F10357" targetNode="P_8F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_12F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10357_O" deadCode="true" sourceNode="P_4F10357" targetNode="P_9F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_12F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10357_I" deadCode="false" sourceNode="P_4F10357" targetNode="P_10F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_14F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10357_O" deadCode="false" sourceNode="P_4F10357" targetNode="P_11F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_14F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10357_I" deadCode="false" sourceNode="P_4F10357" targetNode="P_12F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_17F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10357_O" deadCode="false" sourceNode="P_4F10357" targetNode="P_13F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_17F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10357_I" deadCode="false" sourceNode="P_4F10357" targetNode="P_14F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_18F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10357_O" deadCode="false" sourceNode="P_4F10357" targetNode="P_15F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_18F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10357_I" deadCode="false" sourceNode="P_4F10357" targetNode="P_16F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_32F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10357_O" deadCode="false" sourceNode="P_4F10357" targetNode="P_17F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_32F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10357_I" deadCode="false" sourceNode="P_4F10357" targetNode="P_18F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_34F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10357_O" deadCode="false" sourceNode="P_4F10357" targetNode="P_19F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_34F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10357_I" deadCode="false" sourceNode="P_4F10357" targetNode="P_16F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_39F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10357_O" deadCode="false" sourceNode="P_4F10357" targetNode="P_17F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_39F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10357_I" deadCode="false" sourceNode="P_4F10357" targetNode="P_18F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_41F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10357_O" deadCode="false" sourceNode="P_4F10357" targetNode="P_19F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_41F10357"/>
	</edges>
	<edges id="P_4F10357P_5F10357" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10357" targetNode="P_5F10357"/>
	<edges id="P_8F10357P_9F10357" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10357" targetNode="P_9F10357"/>
	<edges id="P_10F10357P_11F10357" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10357" targetNode="P_11F10357"/>
	<edges id="P_16F10357P_17F10357" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10357" targetNode="P_17F10357"/>
	<edges id="P_18F10357P_19F10357" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10357" targetNode="P_19F10357"/>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10357_I" deadCode="false" sourceNode="P_14F10357" targetNode="P_20F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_104F10357"/>
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_126F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10357_O" deadCode="false" sourceNode="P_14F10357" targetNode="P_21F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_104F10357"/>
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_126F10357"/>
	</edges>
	<edges id="P_14F10357P_15F10357" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10357" targetNode="P_15F10357"/>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10357_I" deadCode="false" sourceNode="P_12F10357" targetNode="P_20F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_153F10357"/>
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_175F10357"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10357_O" deadCode="false" sourceNode="P_12F10357" targetNode="P_21F10357">
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_153F10357"/>
		<representations href="../../../cobol/LVVS0146.cbl.cobModel#S_175F10357"/>
	</edges>
	<edges id="P_12F10357P_13F10357" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10357" targetNode="P_13F10357"/>
	<edges id="P_20F10357P_21F10357" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10357" targetNode="P_21F10357"/>
	<edges xsi:type="cbl:CallEdge" id="S_65F10357" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_16F10357" targetNode="LDBS7120">
		<representations href="../../../cobol/../importantStmts.cobModel#S_65F10357"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_83F10357" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_18F10357" targetNode="LDBS7120">
		<representations href="../../../cobol/../importantStmts.cobModel#S_83F10357"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_112F10357" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_14F10357" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_112F10357"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_161F10357" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_12F10357" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_161F10357"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_202F10357" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_20F10357" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_202F10357"></representations>
	</edges>
</Package>

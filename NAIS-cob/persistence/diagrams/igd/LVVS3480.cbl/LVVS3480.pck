<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS3480" cbl:id="LVVS3480" xsi:id="LVVS3480" packageRef="LVVS3480.igd#LVVS3480" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS3480_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10393" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS3480.cbl.cobModel#SC_1F10393"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10393" deadCode="false" name="PROGRAM_LVVS3480_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_1F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10393" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_2F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10393" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_3F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10393" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_4F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10393" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_5F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10393" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_8F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10393" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_9F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10393" deadCode="false" name="S1360-INIT-VARIABILE">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_10F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10393" deadCode="false" name="S1360-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_11F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10393" deadCode="false" name="S1400-VALORIZZA-VARIABILE">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_12F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10393" deadCode="false" name="S1400-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_13F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10393" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_6F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10393" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_7F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10393" deadCode="false" name="STRINGA-X-VALORIZZATORE">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_14F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10393" deadCode="false" name="STRINGA-X-VALORIZZATORE-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_15F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10393" deadCode="false" name="CONVERTI-FORMATI">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_16F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10393" deadCode="false" name="CONVERTI-FORMATI-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_17F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10393" deadCode="false" name="CALL-STRINGATURA">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_18F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10393" deadCode="false" name="CALL-STRINGATURA-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_19F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10393" deadCode="false" name="INITIALIZE-CAMPI">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_20F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10393" deadCode="false" name="INITIALIZE-CAMPI-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_21F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10393" deadCode="false" name="ELABORA-DATI">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_22F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10393" deadCode="false" name="ELABORA-DATI-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_23F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10393" deadCode="false" name="TRATTA-IMPORTO">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_26F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10393" deadCode="false" name="TRATTA-IMPORTO-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_27F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10393" deadCode="false" name="TRATTA-NUMERICO">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_28F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10393" deadCode="false" name="TRATTA-NUMERICO-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_29F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10393" deadCode="false" name="TRATTA-PERCENTUALE">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_30F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10393" deadCode="false" name="TRATTA-PERCENTUALE-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_31F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10393" deadCode="false" name="TRATTA-STRINGA">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_32F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10393" deadCode="false" name="TRATTA-STRINGA-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_33F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10393" deadCode="false" name="ESTRAI-ALFANUMERICO">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_42F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10393" deadCode="false" name="ESTRAI-ALFANUMERICO-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_43F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10393" deadCode="false" name="ESTRAI-INTERO">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_34F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10393" deadCode="false" name="ESTRAI-INTERO-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_35F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10393" deadCode="false" name="ESTRAI-DECIMALI">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_36F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10393" deadCode="false" name="ESTRAI-DECIMALI-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_37F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10393" deadCode="false" name="TRATTA-SEGNO">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_40F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10393" deadCode="false" name="TRATTA-SEGNO-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_41F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10393" deadCode="false" name="STRINGA-CAMPO">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_38F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10393" deadCode="false" name="STRINGA-CAMPO-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_39F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10393" deadCode="false" name="CALCOLA-SPAZIO">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_44F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10393" deadCode="false" name="CALCOLA-SPAZIO-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_45F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10393" deadCode="false" name="INDIVIDUA-IND-DEC">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_46F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10393" deadCode="false" name="INDIVIDUA-IND-DEC-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_47F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10393" deadCode="false" name="LISTA-OMOGENEA">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_24F10393"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10393" deadCode="false" name="LISTA-OMOGENEA-EX">
				<representations href="../../../cobol/LVVS3480.cbl.cobModel#P_25F10393"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10393P_1F10393" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10393" targetNode="P_1F10393"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10393_I" deadCode="false" sourceNode="P_1F10393" targetNode="P_2F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_1F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10393_O" deadCode="false" sourceNode="P_1F10393" targetNode="P_3F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_1F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10393_I" deadCode="false" sourceNode="P_1F10393" targetNode="P_4F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_2F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10393_O" deadCode="false" sourceNode="P_1F10393" targetNode="P_5F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_2F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10393_I" deadCode="false" sourceNode="P_1F10393" targetNode="P_6F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_3F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10393_O" deadCode="false" sourceNode="P_1F10393" targetNode="P_7F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_3F10393"/>
	</edges>
	<edges id="P_2F10393P_3F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10393" targetNode="P_3F10393"/>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10393_I" deadCode="false" sourceNode="P_4F10393" targetNode="P_8F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_12F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10393_O" deadCode="false" sourceNode="P_4F10393" targetNode="P_9F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_12F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10393_I" deadCode="false" sourceNode="P_4F10393" targetNode="P_10F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_30F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10393_O" deadCode="false" sourceNode="P_4F10393" targetNode="P_11F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_30F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10393_I" deadCode="false" sourceNode="P_4F10393" targetNode="P_12F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_32F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10393_O" deadCode="false" sourceNode="P_4F10393" targetNode="P_13F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_32F10393"/>
	</edges>
	<edges id="P_4F10393P_5F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10393" targetNode="P_5F10393"/>
	<edges id="P_8F10393P_9F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10393" targetNode="P_9F10393"/>
	<edges id="P_10F10393P_11F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10393" targetNode="P_11F10393"/>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10393_I" deadCode="false" sourceNode="P_12F10393" targetNode="P_14F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_48F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10393_O" deadCode="false" sourceNode="P_12F10393" targetNode="P_15F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_48F10393"/>
	</edges>
	<edges id="P_12F10393P_13F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10393" targetNode="P_13F10393"/>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10393_I" deadCode="false" sourceNode="P_14F10393" targetNode="P_16F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_64F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10393_O" deadCode="false" sourceNode="P_14F10393" targetNode="P_17F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_64F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10393_I" deadCode="false" sourceNode="P_14F10393" targetNode="P_18F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_66F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10393_O" deadCode="false" sourceNode="P_14F10393" targetNode="P_19F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_66F10393"/>
	</edges>
	<edges id="P_14F10393P_15F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10393" targetNode="P_15F10393"/>
	<edges id="P_16F10393P_17F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10393" targetNode="P_17F10393"/>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10393_I" deadCode="false" sourceNode="P_18F10393" targetNode="P_20F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_80F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10393_O" deadCode="false" sourceNode="P_18F10393" targetNode="P_21F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_80F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10393_I" deadCode="false" sourceNode="P_18F10393" targetNode="P_22F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_81F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10393_O" deadCode="false" sourceNode="P_18F10393" targetNode="P_23F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_81F10393"/>
	</edges>
	<edges id="P_18F10393P_19F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10393" targetNode="P_19F10393"/>
	<edges id="P_20F10393P_21F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10393" targetNode="P_21F10393"/>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10393_I" deadCode="false" sourceNode="P_22F10393" targetNode="P_24F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_91F10393"/>
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_100F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10393_O" deadCode="false" sourceNode="P_22F10393" targetNode="P_25F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_91F10393"/>
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_100F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10393_I" deadCode="false" sourceNode="P_22F10393" targetNode="P_26F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_91F10393"/>
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_102F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10393_O" deadCode="false" sourceNode="P_22F10393" targetNode="P_27F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_91F10393"/>
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_102F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10393_I" deadCode="false" sourceNode="P_22F10393" targetNode="P_28F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_91F10393"/>
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_103F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10393_O" deadCode="false" sourceNode="P_22F10393" targetNode="P_29F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_91F10393"/>
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_103F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10393_I" deadCode="false" sourceNode="P_22F10393" targetNode="P_30F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_91F10393"/>
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_104F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10393_O" deadCode="false" sourceNode="P_22F10393" targetNode="P_31F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_91F10393"/>
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_104F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10393_I" deadCode="false" sourceNode="P_22F10393" targetNode="P_32F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_91F10393"/>
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_105F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10393_O" deadCode="false" sourceNode="P_22F10393" targetNode="P_33F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_91F10393"/>
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_105F10393"/>
	</edges>
	<edges id="P_22F10393P_23F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10393" targetNode="P_23F10393"/>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10393_I" deadCode="false" sourceNode="P_26F10393" targetNode="P_34F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_124F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10393_O" deadCode="false" sourceNode="P_26F10393" targetNode="P_35F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_124F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10393_I" deadCode="false" sourceNode="P_26F10393" targetNode="P_36F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_125F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10393_O" deadCode="false" sourceNode="P_26F10393" targetNode="P_37F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_125F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10393_I" deadCode="false" sourceNode="P_26F10393" targetNode="P_38F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_126F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10393_O" deadCode="false" sourceNode="P_26F10393" targetNode="P_39F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_126F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10393_I" deadCode="false" sourceNode="P_26F10393" targetNode="P_40F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_128F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10393_O" deadCode="false" sourceNode="P_26F10393" targetNode="P_41F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_128F10393"/>
	</edges>
	<edges id="P_26F10393P_27F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10393" targetNode="P_27F10393"/>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10393_I" deadCode="false" sourceNode="P_28F10393" targetNode="P_34F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_137F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10393_O" deadCode="false" sourceNode="P_28F10393" targetNode="P_35F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_137F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10393_I" deadCode="false" sourceNode="P_28F10393" targetNode="P_38F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_138F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10393_O" deadCode="false" sourceNode="P_28F10393" targetNode="P_39F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_138F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10393_I" deadCode="false" sourceNode="P_28F10393" targetNode="P_40F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_140F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10393_O" deadCode="false" sourceNode="P_28F10393" targetNode="P_41F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_140F10393"/>
	</edges>
	<edges id="P_28F10393P_29F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10393" targetNode="P_29F10393"/>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10393_I" deadCode="false" sourceNode="P_30F10393" targetNode="P_34F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_146F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10393_O" deadCode="false" sourceNode="P_30F10393" targetNode="P_35F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_146F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10393_I" deadCode="false" sourceNode="P_30F10393" targetNode="P_36F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_147F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10393_O" deadCode="false" sourceNode="P_30F10393" targetNode="P_37F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_147F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10393_I" deadCode="false" sourceNode="P_30F10393" targetNode="P_38F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_148F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10393_O" deadCode="false" sourceNode="P_30F10393" targetNode="P_39F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_148F10393"/>
	</edges>
	<edges id="P_30F10393P_31F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10393" targetNode="P_31F10393"/>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10393_I" deadCode="false" sourceNode="P_32F10393" targetNode="P_42F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_152F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10393_O" deadCode="false" sourceNode="P_32F10393" targetNode="P_43F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_152F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_156F10393_I" deadCode="false" sourceNode="P_32F10393" targetNode="P_38F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_156F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_156F10393_O" deadCode="false" sourceNode="P_32F10393" targetNode="P_39F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_156F10393"/>
	</edges>
	<edges id="P_32F10393P_33F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10393" targetNode="P_33F10393"/>
	<edges id="P_42F10393P_43F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10393" targetNode="P_43F10393"/>
	<edges id="P_34F10393P_35F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10393" targetNode="P_35F10393"/>
	<edges id="P_36F10393P_37F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10393" targetNode="P_37F10393"/>
	<edges id="P_40F10393P_41F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10393" targetNode="P_41F10393"/>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10393_I" deadCode="false" sourceNode="P_38F10393" targetNode="P_44F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_191F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10393_O" deadCode="false" sourceNode="P_38F10393" targetNode="P_45F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_191F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_200F10393_I" deadCode="false" sourceNode="P_38F10393" targetNode="P_40F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_200F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_200F10393_O" deadCode="false" sourceNode="P_38F10393" targetNode="P_41F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_200F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_214F10393_I" deadCode="false" sourceNode="P_38F10393" targetNode="P_40F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_214F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_214F10393_O" deadCode="false" sourceNode="P_38F10393" targetNode="P_41F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_214F10393"/>
	</edges>
	<edges id="P_38F10393P_39F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10393" targetNode="P_39F10393"/>
	<edges xsi:type="cbl:PerformEdge" id="S_222F10393_I" deadCode="false" sourceNode="P_44F10393" targetNode="P_46F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_222F10393"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_222F10393_O" deadCode="false" sourceNode="P_44F10393" targetNode="P_47F10393">
		<representations href="../../../cobol/LVVS3480.cbl.cobModel#S_222F10393"/>
	</edges>
	<edges id="P_44F10393P_45F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10393" targetNode="P_45F10393"/>
	<edges id="P_46F10393P_47F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10393" targetNode="P_47F10393"/>
	<edges id="P_24F10393P_25F10393" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10393" targetNode="P_25F10393"/>
</Package>

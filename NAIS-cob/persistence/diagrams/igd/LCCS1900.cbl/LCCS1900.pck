<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LCCS1900" cbl:id="LCCS1900" xsi:id="LCCS1900" packageRef="LCCS1900.igd#LCCS1900" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LCCS1900_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10139" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LCCS1900.cbl.cobModel#SC_1F10139"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10139" deadCode="false" name="PROGRAM_LCCS1900_FIRST_SENTENCES">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_1F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10139" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_2F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10139" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_3F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10139" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_4F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10139" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_5F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10139" deadCode="false" name="VERIFICA-GRZ">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_14F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10139" deadCode="false" name="VERIFICA-GRZ-EX">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_15F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10139" deadCode="false" name="LEGGI-AST-ALLOC">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_16F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10139" deadCode="false" name="LEGGI-AST-ALLOC-EX">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_23F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10139" deadCode="false" name="LEGGI-STRA-INVST">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_12F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10139" deadCode="false" name="LEGGI-STRA-INVST-EX">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_13F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10139" deadCode="false" name="RECUPERA-RAPP-RETE">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_8F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10139" deadCode="false" name="RECUPERA-RAPP-RETE-EX">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_9F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10139" deadCode="false" name="B200-CLOSE-CURSOR">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_24F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10139" deadCode="false" name="B200-CLOSE-CURSOR-EX">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_25F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10139" deadCode="false" name="LEGGI-PARAM-MOVI">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_10F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10139" deadCode="false" name="LEGGI-PARAM-MOVI-EX">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_11F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10139" deadCode="false" name="S9000-OPERAZ-FINALI">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_6F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10139" deadCode="false" name="EX-S9000">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_7F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10139" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_21F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10139" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_22F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10139" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_30F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10139" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_31F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10139" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_28F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10139" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_29F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10139" deadCode="true" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_32F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10139" deadCode="true" name="EX-S0290">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_33F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10139" deadCode="false" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_17F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10139" deadCode="false" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_18F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10139" deadCode="false" name="ESEGUI-DISPLAY">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_26F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10139" deadCode="false" name="ESEGUI-DISPLAY-EX">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_27F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10139" deadCode="false" name="VALORIZZA-OUTPUT-ALL">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_19F10139"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10139" deadCode="false" name="VALORIZZA-OUTPUT-ALL-EX">
				<representations href="../../../cobol/LCCS1900.cbl.cobModel#P_20F10139"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0010" name="IDSS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10099"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IJCSTMSP" name="IJCSTMSP" missing="true">
			<representations href="../../../../missing.xmi#ID4GAQVPM0Z12ED05MPMC3Q3PJLIIF0PGQOOXCDHDOPSGQEJV4ENDL"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS8880" name="IDSS8880">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10106"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10139P_1F10139" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10139" targetNode="P_1F10139"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10139_I" deadCode="false" sourceNode="P_1F10139" targetNode="P_2F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_1F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10139_O" deadCode="false" sourceNode="P_1F10139" targetNode="P_3F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_1F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10139_I" deadCode="false" sourceNode="P_1F10139" targetNode="P_4F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_3F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10139_O" deadCode="false" sourceNode="P_1F10139" targetNode="P_5F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_3F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10139_I" deadCode="false" sourceNode="P_1F10139" targetNode="P_6F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_4F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10139_O" deadCode="false" sourceNode="P_1F10139" targetNode="P_7F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_4F10139"/>
	</edges>
	<edges id="P_2F10139P_3F10139" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10139" targetNode="P_3F10139"/>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10139_I" deadCode="false" sourceNode="P_4F10139" targetNode="P_8F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_15F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10139_O" deadCode="false" sourceNode="P_4F10139" targetNode="P_9F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_15F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10139_I" deadCode="false" sourceNode="P_4F10139" targetNode="P_10F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_18F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10139_O" deadCode="false" sourceNode="P_4F10139" targetNode="P_11F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_18F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10139_I" deadCode="false" sourceNode="P_4F10139" targetNode="P_12F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_27F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10139_O" deadCode="false" sourceNode="P_4F10139" targetNode="P_13F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_27F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10139_I" deadCode="false" sourceNode="P_4F10139" targetNode="P_14F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_29F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10139_O" deadCode="false" sourceNode="P_4F10139" targetNode="P_15F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_29F10139"/>
	</edges>
	<edges id="P_4F10139P_5F10139" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10139" targetNode="P_5F10139"/>
	<edges id="P_14F10139P_15F10139" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10139" targetNode="P_15F10139"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10139_I" deadCode="false" sourceNode="P_16F10139" targetNode="P_17F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_52F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_53F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10139_O" deadCode="false" sourceNode="P_16F10139" targetNode="P_18F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_52F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_53F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10139_I" deadCode="false" sourceNode="P_16F10139" targetNode="P_19F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_52F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_59F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10139_O" deadCode="false" sourceNode="P_16F10139" targetNode="P_20F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_52F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_59F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10139_I" deadCode="false" sourceNode="P_16F10139" targetNode="P_21F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_52F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_69F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10139_O" deadCode="false" sourceNode="P_16F10139" targetNode="P_22F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_52F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_69F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10139_I" deadCode="false" sourceNode="P_16F10139" targetNode="P_21F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_52F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_75F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10139_O" deadCode="false" sourceNode="P_16F10139" targetNode="P_22F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_52F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_75F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10139_I" deadCode="false" sourceNode="P_16F10139" targetNode="P_21F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_52F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_81F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10139_O" deadCode="false" sourceNode="P_16F10139" targetNode="P_22F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_52F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_81F10139"/>
	</edges>
	<edges id="P_16F10139P_23F10139" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10139" targetNode="P_23F10139"/>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10139_I" deadCode="false" sourceNode="P_12F10139" targetNode="P_17F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_96F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10139_O" deadCode="false" sourceNode="P_12F10139" targetNode="P_18F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_96F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10139_I" deadCode="false" sourceNode="P_12F10139" targetNode="P_21F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_103F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10139_O" deadCode="false" sourceNode="P_12F10139" targetNode="P_22F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_103F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10139_I" deadCode="false" sourceNode="P_12F10139" targetNode="P_16F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_105F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10139_O" deadCode="false" sourceNode="P_12F10139" targetNode="P_23F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_105F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10139_I" deadCode="false" sourceNode="P_12F10139" targetNode="P_21F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_110F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10139_O" deadCode="false" sourceNode="P_12F10139" targetNode="P_22F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_110F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10139_I" deadCode="false" sourceNode="P_12F10139" targetNode="P_21F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_115F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10139_O" deadCode="false" sourceNode="P_12F10139" targetNode="P_22F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_115F10139"/>
	</edges>
	<edges id="P_12F10139P_13F10139" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10139" targetNode="P_13F10139"/>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10139_I" deadCode="false" sourceNode="P_8F10139" targetNode="P_17F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_132F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_133F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10139_O" deadCode="false" sourceNode="P_8F10139" targetNode="P_18F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_132F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_133F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10139_I" deadCode="false" sourceNode="P_8F10139" targetNode="P_24F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_132F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_139F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10139_O" deadCode="false" sourceNode="P_8F10139" targetNode="P_25F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_132F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_139F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10139_I" deadCode="false" sourceNode="P_8F10139" targetNode="P_21F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_132F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_146F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10139_O" deadCode="false" sourceNode="P_8F10139" targetNode="P_22F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_132F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_146F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10139_I" deadCode="false" sourceNode="P_8F10139" targetNode="P_21F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_132F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_152F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10139_O" deadCode="false" sourceNode="P_8F10139" targetNode="P_22F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_132F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_152F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10139_I" deadCode="false" sourceNode="P_8F10139" targetNode="P_21F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_132F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_158F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10139_O" deadCode="false" sourceNode="P_8F10139" targetNode="P_22F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_132F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_158F10139"/>
	</edges>
	<edges id="P_8F10139P_9F10139" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10139" targetNode="P_9F10139"/>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10139_I" deadCode="false" sourceNode="P_24F10139" targetNode="P_26F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_162F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10139_O" deadCode="false" sourceNode="P_24F10139" targetNode="P_27F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_162F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10139_I" deadCode="false" sourceNode="P_24F10139" targetNode="P_17F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_172F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10139_O" deadCode="false" sourceNode="P_24F10139" targetNode="P_18F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_172F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10139_I" deadCode="false" sourceNode="P_24F10139" targetNode="P_21F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_181F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10139_O" deadCode="false" sourceNode="P_24F10139" targetNode="P_22F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_181F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10139_I" deadCode="false" sourceNode="P_24F10139" targetNode="P_21F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_187F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10139_O" deadCode="false" sourceNode="P_24F10139" targetNode="P_22F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_187F10139"/>
	</edges>
	<edges id="P_24F10139P_25F10139" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10139" targetNode="P_25F10139"/>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10139_I" deadCode="false" sourceNode="P_10F10139" targetNode="P_26F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_191F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10139_O" deadCode="false" sourceNode="P_10F10139" targetNode="P_27F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_191F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_207F10139_I" deadCode="false" sourceNode="P_10F10139" targetNode="P_17F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_206F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_207F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_207F10139_O" deadCode="false" sourceNode="P_10F10139" targetNode="P_18F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_206F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_207F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10139_I" deadCode="false" sourceNode="P_10F10139" targetNode="P_21F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_206F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_221F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10139_O" deadCode="false" sourceNode="P_10F10139" targetNode="P_22F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_206F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_221F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_226F10139_I" deadCode="false" sourceNode="P_10F10139" targetNode="P_21F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_206F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_226F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_226F10139_O" deadCode="false" sourceNode="P_10F10139" targetNode="P_22F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_206F10139"/>
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_226F10139"/>
	</edges>
	<edges id="P_10F10139P_11F10139" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10139" targetNode="P_11F10139"/>
	<edges id="P_6F10139P_7F10139" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10139" targetNode="P_7F10139"/>
	<edges xsi:type="cbl:PerformEdge" id="S_235F10139_I" deadCode="false" sourceNode="P_21F10139" targetNode="P_28F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_235F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_235F10139_O" deadCode="false" sourceNode="P_21F10139" targetNode="P_29F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_235F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_239F10139_I" deadCode="false" sourceNode="P_21F10139" targetNode="P_30F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_239F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_239F10139_O" deadCode="false" sourceNode="P_21F10139" targetNode="P_31F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_239F10139"/>
	</edges>
	<edges id="P_21F10139P_22F10139" xsi:type="cbl:FallThroughEdge" sourceNode="P_21F10139" targetNode="P_22F10139"/>
	<edges id="P_30F10139P_31F10139" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10139" targetNode="P_31F10139"/>
	<edges id="P_28F10139P_29F10139" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10139" targetNode="P_29F10139"/>
	<edges xsi:type="cbl:PerformEdge" id="S_275F10139_I" deadCode="true" sourceNode="P_32F10139" targetNode="P_21F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_275F10139"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_275F10139_O" deadCode="true" sourceNode="P_32F10139" targetNode="P_22F10139">
		<representations href="../../../cobol/LCCS1900.cbl.cobModel#S_275F10139"/>
	</edges>
	<edges id="P_17F10139P_18F10139" xsi:type="cbl:FallThroughEdge" sourceNode="P_17F10139" targetNode="P_18F10139"/>
	<edges id="P_26F10139P_27F10139" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10139" targetNode="P_27F10139"/>
	<edges id="P_19F10139P_20F10139" xsi:type="cbl:FallThroughEdge" sourceNode="P_19F10139" targetNode="P_20F10139"/>
	<edges xsi:type="cbl:CallEdge" id="S_233F10139" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_21F10139" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_233F10139"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_303F10139" deadCode="false" name="Dynamic IDSI0011-PGM" sourceNode="P_17F10139" targetNode="IDSS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_303F10139"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_317F10139" deadCode="false" name="Dynamic IDSV8888-CALL-TIMESTAMP" sourceNode="P_26F10139" targetNode="IJCSTMSP">
		<representations href="../../../cobol/../importantStmts.cobModel#S_317F10139"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_319F10139" deadCode="false" name="Dynamic IDSV8888-CALL-DISPLAY" sourceNode="P_26F10139" targetNode="IDSS8880">
		<representations href="../../../cobol/../importantStmts.cobModel#S_319F10139"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_329F10139" deadCode="false" name="Dynamic IDSV8888-CALL-TIMESTAMP" sourceNode="P_26F10139" targetNode="IJCSTMSP">
		<representations href="../../../cobol/../importantStmts.cobModel#S_329F10139"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_331F10139" deadCode="false" name="Dynamic IDSV8888-CALL-DISPLAY" sourceNode="P_26F10139" targetNode="IDSS8880">
		<representations href="../../../cobol/../importantStmts.cobModel#S_331F10139"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_336F10139" deadCode="false" name="Dynamic IDSV8888-CALL-DISPLAY" sourceNode="P_26F10139" targetNode="IDSS8880">
		<representations href="../../../cobol/../importantStmts.cobModel#S_336F10139"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS2890" cbl:id="LVVS2890" xsi:id="LVVS2890" packageRef="LVVS2890.igd#LVVS2890" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS2890_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10381" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS2890.cbl.cobModel#SC_1F10381"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10381" deadCode="false" name="PROGRAM_LVVS2890_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS2890.cbl.cobModel#P_1F10381"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10381" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS2890.cbl.cobModel#P_2F10381"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10381" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS2890.cbl.cobModel#P_3F10381"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10381" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS2890.cbl.cobModel#P_4F10381"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10381" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS2890.cbl.cobModel#P_5F10381"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10381" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS2890.cbl.cobModel#P_8F10381"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10381" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS2890.cbl.cobModel#P_9F10381"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10381" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS2890.cbl.cobModel#P_10F10381"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10381" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS2890.cbl.cobModel#P_11F10381"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10381" deadCode="false" name="S1300-RECUPERO-INFO">
				<representations href="../../../cobol/LVVS2890.cbl.cobModel#P_12F10381"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10381" deadCode="false" name="S1300-RECUPERO-INFO-EX">
				<representations href="../../../cobol/LVVS2890.cbl.cobModel#P_13F10381"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10381" deadCode="false" name="ACCESSO-PERS">
				<representations href="../../../cobol/LVVS2890.cbl.cobModel#P_14F10381"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10381" deadCode="false" name="ACCESSO-PERS-EX">
				<representations href="../../../cobol/LVVS2890.cbl.cobModel#P_15F10381"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10381" deadCode="false" name="E000-CONVERTI-CHAR">
				<representations href="../../../cobol/LVVS2890.cbl.cobModel#P_16F10381"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10381" deadCode="false" name="EX-E000">
				<representations href="../../../cobol/LVVS2890.cbl.cobModel#P_17F10381"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10381" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS2890.cbl.cobModel#P_6F10381"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10381" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS2890.cbl.cobModel#P_7F10381"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS1300" name="LDBS1300">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10154"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IWFS0050" name="IWFS0050">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10118"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10381P_1F10381" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10381" targetNode="P_1F10381"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10381_I" deadCode="false" sourceNode="P_1F10381" targetNode="P_2F10381">
		<representations href="../../../cobol/LVVS2890.cbl.cobModel#S_1F10381"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10381_O" deadCode="false" sourceNode="P_1F10381" targetNode="P_3F10381">
		<representations href="../../../cobol/LVVS2890.cbl.cobModel#S_1F10381"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10381_I" deadCode="false" sourceNode="P_1F10381" targetNode="P_4F10381">
		<representations href="../../../cobol/LVVS2890.cbl.cobModel#S_2F10381"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10381_O" deadCode="false" sourceNode="P_1F10381" targetNode="P_5F10381">
		<representations href="../../../cobol/LVVS2890.cbl.cobModel#S_2F10381"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10381_I" deadCode="false" sourceNode="P_1F10381" targetNode="P_6F10381">
		<representations href="../../../cobol/LVVS2890.cbl.cobModel#S_3F10381"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10381_O" deadCode="false" sourceNode="P_1F10381" targetNode="P_7F10381">
		<representations href="../../../cobol/LVVS2890.cbl.cobModel#S_3F10381"/>
	</edges>
	<edges id="P_2F10381P_3F10381" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10381" targetNode="P_3F10381"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10381_I" deadCode="false" sourceNode="P_4F10381" targetNode="P_8F10381">
		<representations href="../../../cobol/LVVS2890.cbl.cobModel#S_10F10381"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10381_O" deadCode="false" sourceNode="P_4F10381" targetNode="P_9F10381">
		<representations href="../../../cobol/LVVS2890.cbl.cobModel#S_10F10381"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10381_I" deadCode="false" sourceNode="P_4F10381" targetNode="P_10F10381">
		<representations href="../../../cobol/LVVS2890.cbl.cobModel#S_11F10381"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10381_O" deadCode="false" sourceNode="P_4F10381" targetNode="P_11F10381">
		<representations href="../../../cobol/LVVS2890.cbl.cobModel#S_11F10381"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10381_I" deadCode="false" sourceNode="P_4F10381" targetNode="P_12F10381">
		<representations href="../../../cobol/LVVS2890.cbl.cobModel#S_12F10381"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10381_O" deadCode="false" sourceNode="P_4F10381" targetNode="P_13F10381">
		<representations href="../../../cobol/LVVS2890.cbl.cobModel#S_12F10381"/>
	</edges>
	<edges id="P_4F10381P_5F10381" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10381" targetNode="P_5F10381"/>
	<edges id="P_8F10381P_9F10381" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10381" targetNode="P_9F10381"/>
	<edges id="P_10F10381P_11F10381" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10381" targetNode="P_11F10381"/>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10381_I" deadCode="false" sourceNode="P_12F10381" targetNode="P_14F10381">
		<representations href="../../../cobol/LVVS2890.cbl.cobModel#S_22F10381"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10381_O" deadCode="false" sourceNode="P_12F10381" targetNode="P_15F10381">
		<representations href="../../../cobol/LVVS2890.cbl.cobModel#S_22F10381"/>
	</edges>
	<edges id="P_12F10381P_13F10381" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10381" targetNode="P_13F10381"/>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10381_I" deadCode="false" sourceNode="P_14F10381" targetNode="P_16F10381">
		<representations href="../../../cobol/LVVS2890.cbl.cobModel#S_30F10381"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10381_O" deadCode="false" sourceNode="P_14F10381" targetNode="P_17F10381">
		<representations href="../../../cobol/LVVS2890.cbl.cobModel#S_30F10381"/>
	</edges>
	<edges id="P_14F10381P_15F10381" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10381" targetNode="P_15F10381"/>
	<edges id="P_16F10381P_17F10381" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10381" targetNode="P_17F10381"/>
	<edges xsi:type="cbl:CallEdge" id="S_39F10381" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_14F10381" targetNode="LDBS1300">
		<representations href="../../../cobol/../importantStmts.cobModel#S_39F10381"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_58F10381" deadCode="false" name="Dynamic IWFS0050" sourceNode="P_16F10381" targetNode="IWFS0050">
		<representations href="../../../cobol/../importantStmts.cobModel#S_58F10381"></representations>
	</edges>
</Package>

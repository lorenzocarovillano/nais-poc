<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS2740" cbl:id="LVVS2740" xsi:id="LVVS2740" packageRef="LVVS2740.igd#LVVS2740" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS2740_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10374" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS2740.cbl.cobModel#SC_1F10374"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10374" deadCode="false" name="PROGRAM_LVVS2740_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_1F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10374" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_2F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10374" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_3F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10374" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_4F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10374" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_5F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10374" deadCode="false" name="VERIFICA-MOVIMENTO">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_8F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10374" deadCode="false" name="VERIFICA-MOVIMENTO-EX">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_9F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10374" deadCode="false" name="RECUP-MOVI-COMUN">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_14F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10374" deadCode="false" name="RECUP-MOVI-COMUN-EX">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_15F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10374" deadCode="false" name="CLOSE-MOVI">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_22F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10374" deadCode="false" name="CLOSE-MOVI-EX">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_23F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10374" deadCode="false" name="LETTURA-IMPST-BOLLO">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_18F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10374" deadCode="false" name="LETTURA-IMPST-BOLLO-EX">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_19F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10374" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_20F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10374" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_21F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10374" deadCode="false" name="S1150-CALCOLO-ANNO">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_10F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10374" deadCode="false" name="S1150-CALCOLO-ANNO-EX">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_11F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10374" deadCode="false" name="S1200-CALCOLO-IMPORTO">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_12F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10374" deadCode="false" name="S1200-CALCOLO-IMPORTO-EX">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_13F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10374" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_6F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10374" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_7F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10374" deadCode="false" name="VALORIZZA-OUTPUT-P58">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_24F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10374" deadCode="false" name="VALORIZZA-OUTPUT-P58-EX">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_25F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10374" deadCode="false" name="INIZIA-TOT-P58">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_16F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10374" deadCode="false" name="INIZIA-TOT-P58-EX">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_17F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10374" deadCode="true" name="INIZIA-NULL-P58">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_30F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10374" deadCode="false" name="INIZIA-NULL-P58-EX">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_31F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10374" deadCode="false" name="INIZIA-ZEROES-P58">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_26F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10374" deadCode="false" name="INIZIA-ZEROES-P58-EX">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_27F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10374" deadCode="false" name="INIZIA-SPACES-P58">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_28F10374"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10374" deadCode="false" name="INIZIA-SPACES-P58-EX">
				<representations href="../../../cobol/LVVS2740.cbl.cobModel#P_29F10374"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS6040" name="LDBS6040">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10230"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSE590" name="LDBSE590">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10264"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10374P_1F10374" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10374" targetNode="P_1F10374"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10374_I" deadCode="false" sourceNode="P_1F10374" targetNode="P_2F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_1F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10374_O" deadCode="false" sourceNode="P_1F10374" targetNode="P_3F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_1F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10374_I" deadCode="false" sourceNode="P_1F10374" targetNode="P_4F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_2F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10374_O" deadCode="false" sourceNode="P_1F10374" targetNode="P_5F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_2F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10374_I" deadCode="false" sourceNode="P_1F10374" targetNode="P_6F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_3F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10374_O" deadCode="false" sourceNode="P_1F10374" targetNode="P_7F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_3F10374"/>
	</edges>
	<edges id="P_2F10374P_3F10374" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10374" targetNode="P_3F10374"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10374_I" deadCode="false" sourceNode="P_4F10374" targetNode="P_8F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_11F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10374_O" deadCode="false" sourceNode="P_4F10374" targetNode="P_9F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_11F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10374_I" deadCode="false" sourceNode="P_4F10374" targetNode="P_10F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_13F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10374_O" deadCode="false" sourceNode="P_4F10374" targetNode="P_11F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_13F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10374_I" deadCode="false" sourceNode="P_4F10374" targetNode="P_12F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_15F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10374_O" deadCode="false" sourceNode="P_4F10374" targetNode="P_13F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_15F10374"/>
	</edges>
	<edges id="P_4F10374P_5F10374" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10374" targetNode="P_5F10374"/>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10374_I" deadCode="false" sourceNode="P_8F10374" targetNode="P_14F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_19F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10374_O" deadCode="false" sourceNode="P_8F10374" targetNode="P_15F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_19F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10374_I" deadCode="false" sourceNode="P_8F10374" targetNode="P_16F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_25F10374"/>
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_26F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10374_O" deadCode="false" sourceNode="P_8F10374" targetNode="P_17F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_25F10374"/>
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_26F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10374_I" deadCode="false" sourceNode="P_8F10374" targetNode="P_18F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_27F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10374_O" deadCode="false" sourceNode="P_8F10374" targetNode="P_19F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_27F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10374_I" deadCode="false" sourceNode="P_8F10374" targetNode="P_20F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_28F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10374_O" deadCode="false" sourceNode="P_8F10374" targetNode="P_21F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_28F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10374_I" deadCode="false" sourceNode="P_8F10374" targetNode="P_18F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_30F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10374_O" deadCode="false" sourceNode="P_8F10374" targetNode="P_19F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_30F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10374_I" deadCode="false" sourceNode="P_8F10374" targetNode="P_20F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_31F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10374_O" deadCode="false" sourceNode="P_8F10374" targetNode="P_21F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_31F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10374_I" deadCode="false" sourceNode="P_8F10374" targetNode="P_18F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_33F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10374_O" deadCode="false" sourceNode="P_8F10374" targetNode="P_19F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_33F10374"/>
	</edges>
	<edges id="P_8F10374P_9F10374" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10374" targetNode="P_9F10374"/>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10374_I" deadCode="false" sourceNode="P_14F10374" targetNode="P_22F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_42F10374"/>
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_64F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10374_O" deadCode="false" sourceNode="P_14F10374" targetNode="P_23F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_42F10374"/>
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_64F10374"/>
	</edges>
	<edges id="P_14F10374P_15F10374" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10374" targetNode="P_15F10374"/>
	<edges id="P_22F10374P_23F10374" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10374" targetNode="P_23F10374"/>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10374_I" deadCode="false" sourceNode="P_18F10374" targetNode="P_24F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_92F10374"/>
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_107F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10374_O" deadCode="false" sourceNode="P_18F10374" targetNode="P_25F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_92F10374"/>
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_107F10374"/>
	</edges>
	<edges id="P_18F10374P_19F10374" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10374" targetNode="P_19F10374"/>
	<edges id="P_20F10374P_21F10374" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10374" targetNode="P_21F10374"/>
	<edges id="P_10F10374P_11F10374" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10374" targetNode="P_11F10374"/>
	<edges id="P_12F10374P_13F10374" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10374" targetNode="P_13F10374"/>
	<edges id="P_24F10374P_25F10374" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10374" targetNode="P_25F10374"/>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10374_I" deadCode="false" sourceNode="P_16F10374" targetNode="P_26F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_174F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10374_O" deadCode="false" sourceNode="P_16F10374" targetNode="P_27F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_174F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10374_I" deadCode="false" sourceNode="P_16F10374" targetNode="P_28F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_175F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10374_O" deadCode="false" sourceNode="P_16F10374" targetNode="P_29F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_175F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_176F10374_I" deadCode="true" sourceNode="P_16F10374" targetNode="P_30F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_176F10374"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_176F10374_O" deadCode="true" sourceNode="P_16F10374" targetNode="P_31F10374">
		<representations href="../../../cobol/LVVS2740.cbl.cobModel#S_176F10374"/>
	</edges>
	<edges id="P_16F10374P_17F10374" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10374" targetNode="P_17F10374"/>
	<edges id="P_30F10374P_31F10374" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10374" targetNode="P_31F10374"/>
	<edges id="P_26F10374P_27F10374" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10374" targetNode="P_27F10374"/>
	<edges id="P_28F10374P_29F10374" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10374" targetNode="P_29F10374"/>
	<edges xsi:type="cbl:CallEdge" id="S_50F10374" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_14F10374" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_50F10374"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_76F10374" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_22F10374" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_76F10374"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_99F10374" deadCode="false" name="Dynamic LDBSE590" sourceNode="P_18F10374" targetNode="LDBSE590">
		<representations href="../../../cobol/../importantStmts.cobModel#S_99F10374"></representations>
	</edges>
</Package>

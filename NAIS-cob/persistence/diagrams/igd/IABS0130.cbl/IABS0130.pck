<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IABS0130" cbl:id="IABS0130" xsi:id="IABS0130" packageRef="IABS0130.igd#IABS0130" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IABS0130_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10011" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IABS0130.cbl.cobModel#SC_1F10011"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10011" deadCode="false" name="PROGRAM_IABS0130_FIRST_SENTENCES">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_1F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10011" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_2F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10011" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_3F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10011" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_6F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10011" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_7F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10011" deadCode="false" name="A200-ELABORA">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_4F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10011" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_5F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10011" deadCode="false" name="A210-SELECT-PK">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_18F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10011" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_25F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10011" deadCode="false" name="A212-PREPARA-UPD-INIZIALE">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_10F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10011" deadCode="false" name="A212-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_11F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10011" deadCode="false" name="A213-PREPARA-UPD-FINALE">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_16F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10011" deadCode="false" name="A213-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_17F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10011" deadCode="false" name="A230-UPDATE">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_12F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10011" deadCode="false" name="A230-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_13F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10011" deadCode="false" name="A305-DECLARE-CURSOR">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_34F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10011" deadCode="false" name="A305-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_35F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10011" deadCode="false" name="A360-OPEN-CURSOR">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_36F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10011" deadCode="false" name="A360-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_37F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10011" deadCode="false" name="A370-CLOSE-CURSOR">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_38F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10011" deadCode="false" name="A370-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_39F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10011" deadCode="false" name="A375-CNTL">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_8F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10011" deadCode="false" name="A375-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_9F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10011" deadCode="false" name="A380-CNTL">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_14F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10011" deadCode="false" name="A380-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_15F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10011" deadCode="false" name="A390-FETCH-NEXT">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_40F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10011" deadCode="false" name="A390-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_41F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10011" deadCode="false" name="D000-CTRL-STATI-TOT">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_42F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10011" deadCode="false" name="D000-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_43F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10011" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_21F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10011" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_22F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10011" deadCode="false" name="Z150-VALORIZZA-DATA-SERVICES">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_28F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10011" deadCode="false" name="Z150-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_29F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10011" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_30F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10011" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_31F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10011" deadCode="false" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_32F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10011" deadCode="false" name="Z900-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_33F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10011" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_23F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10011" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_24F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10011" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_19F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10011" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_20F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10011" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_48F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10011" deadCode="true" name="Z700-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_49F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10011" deadCode="false" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_44F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10011" deadCode="false" name="Z701-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_45F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10011" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_50F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10011" deadCode="true" name="Z800-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_55F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10011" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_51F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10011" deadCode="true" name="Z810-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_52F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10011" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_53F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10011" deadCode="true" name="Z820-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_54F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10011" deadCode="false" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_46F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10011" deadCode="false" name="Z801-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_47F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10011" deadCode="false" name="ESTRAI-CURRENT-TIMESTAMP">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_26F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10011" deadCode="false" name="ESTRAI-CURRENT-TIMESTAMP-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_27F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10011" deadCode="true" name="ESTRAI-CURRENT-DATE">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_56F10011"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10011" deadCode="true" name="ESTRAI-CURRENT-DATE-EX">
				<representations href="../../../cobol/IABS0130.cbl.cobModel#P_57F10011"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_BTC_PARALLELISM" name="BTC_PARALLELISM">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_BTC_PARALLELISM"/>
		</children>
	</packageNode>
	<edges id="SC_1F10011P_1F10011" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10011" targetNode="P_1F10011"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10011_I" deadCode="false" sourceNode="P_1F10011" targetNode="P_2F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_1F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10011_O" deadCode="false" sourceNode="P_1F10011" targetNode="P_3F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_1F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10011_I" deadCode="false" sourceNode="P_1F10011" targetNode="P_4F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_3F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10011_O" deadCode="false" sourceNode="P_1F10011" targetNode="P_5F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_3F10011"/>
	</edges>
	<edges id="P_2F10011P_3F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10011" targetNode="P_3F10011"/>
	<edges id="P_6F10011P_7F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10011" targetNode="P_7F10011"/>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10011_I" deadCode="false" sourceNode="P_4F10011" targetNode="P_8F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_25F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10011_O" deadCode="false" sourceNode="P_4F10011" targetNode="P_9F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_25F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10011_I" deadCode="false" sourceNode="P_4F10011" targetNode="P_10F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_26F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10011_O" deadCode="false" sourceNode="P_4F10011" targetNode="P_11F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_26F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10011_I" deadCode="false" sourceNode="P_4F10011" targetNode="P_12F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_28F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10011_O" deadCode="false" sourceNode="P_4F10011" targetNode="P_13F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_28F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10011_I" deadCode="false" sourceNode="P_4F10011" targetNode="P_14F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_29F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10011_O" deadCode="false" sourceNode="P_4F10011" targetNode="P_15F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_29F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10011_I" deadCode="false" sourceNode="P_4F10011" targetNode="P_16F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_30F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10011_O" deadCode="false" sourceNode="P_4F10011" targetNode="P_17F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_30F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10011_I" deadCode="false" sourceNode="P_4F10011" targetNode="P_12F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_31F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10011_O" deadCode="false" sourceNode="P_4F10011" targetNode="P_13F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_31F10011"/>
	</edges>
	<edges id="P_4F10011P_5F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10011" targetNode="P_5F10011"/>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10011_I" deadCode="false" sourceNode="P_18F10011" targetNode="P_19F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_34F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10011_O" deadCode="false" sourceNode="P_18F10011" targetNode="P_20F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_34F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10011_I" deadCode="false" sourceNode="P_18F10011" targetNode="P_6F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_36F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10011_O" deadCode="false" sourceNode="P_18F10011" targetNode="P_7F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_36F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10011_I" deadCode="false" sourceNode="P_18F10011" targetNode="P_21F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_38F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10011_O" deadCode="false" sourceNode="P_18F10011" targetNode="P_22F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_38F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10011_I" deadCode="false" sourceNode="P_18F10011" targetNode="P_23F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_39F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10011_O" deadCode="false" sourceNode="P_18F10011" targetNode="P_24F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_39F10011"/>
	</edges>
	<edges id="P_18F10011P_25F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10011" targetNode="P_25F10011"/>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10011_I" deadCode="false" sourceNode="P_10F10011" targetNode="P_18F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_41F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10011_O" deadCode="false" sourceNode="P_10F10011" targetNode="P_25F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_41F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10011_I" deadCode="false" sourceNode="P_10F10011" targetNode="P_26F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_50F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10011_O" deadCode="false" sourceNode="P_10F10011" targetNode="P_27F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_50F10011"/>
	</edges>
	<edges id="P_10F10011P_11F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10011" targetNode="P_11F10011"/>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10011_I" deadCode="false" sourceNode="P_16F10011" targetNode="P_26F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_56F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10011_O" deadCode="false" sourceNode="P_16F10011" targetNode="P_27F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_56F10011"/>
	</edges>
	<edges id="P_16F10011P_17F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10011" targetNode="P_17F10011"/>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10011_I" deadCode="false" sourceNode="P_12F10011" targetNode="P_28F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_61F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10011_O" deadCode="false" sourceNode="P_12F10011" targetNode="P_29F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_61F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10011_I" deadCode="false" sourceNode="P_12F10011" targetNode="P_30F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_62F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10011_O" deadCode="false" sourceNode="P_12F10011" targetNode="P_31F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_62F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10011_I" deadCode="false" sourceNode="P_12F10011" targetNode="P_32F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_63F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10011_O" deadCode="false" sourceNode="P_12F10011" targetNode="P_33F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_63F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10011_I" deadCode="false" sourceNode="P_12F10011" targetNode="P_19F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_64F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10011_O" deadCode="false" sourceNode="P_12F10011" targetNode="P_20F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_64F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10011_I" deadCode="false" sourceNode="P_12F10011" targetNode="P_6F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_66F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10011_O" deadCode="false" sourceNode="P_12F10011" targetNode="P_7F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_66F10011"/>
	</edges>
	<edges id="P_12F10011P_13F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10011" targetNode="P_13F10011"/>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10011_I" deadCode="false" sourceNode="P_34F10011" targetNode="P_19F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_68F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10011_O" deadCode="false" sourceNode="P_34F10011" targetNode="P_20F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_68F10011"/>
	</edges>
	<edges id="P_34F10011P_35F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10011" targetNode="P_35F10011"/>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10011_I" deadCode="false" sourceNode="P_36F10011" targetNode="P_34F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_72F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10011_O" deadCode="false" sourceNode="P_36F10011" targetNode="P_35F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_72F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10011_I" deadCode="false" sourceNode="P_36F10011" targetNode="P_6F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_74F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10011_O" deadCode="false" sourceNode="P_36F10011" targetNode="P_7F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_74F10011"/>
	</edges>
	<edges id="P_36F10011P_37F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10011" targetNode="P_37F10011"/>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10011_I" deadCode="false" sourceNode="P_38F10011" targetNode="P_6F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_77F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10011_O" deadCode="false" sourceNode="P_38F10011" targetNode="P_7F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_77F10011"/>
	</edges>
	<edges id="P_38F10011P_39F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10011" targetNode="P_39F10011"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10011_I" deadCode="false" sourceNode="P_8F10011" targetNode="P_18F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_79F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10011_O" deadCode="false" sourceNode="P_8F10011" targetNode="P_25F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_79F10011"/>
	</edges>
	<edges id="P_8F10011P_9F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10011" targetNode="P_9F10011"/>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10011_I" deadCode="false" sourceNode="P_14F10011" targetNode="P_36F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_97F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10011_O" deadCode="false" sourceNode="P_14F10011" targetNode="P_37F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_97F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10011_I" deadCode="false" sourceNode="P_14F10011" targetNode="P_40F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_99F10011"/>
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_100F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10011_O" deadCode="false" sourceNode="P_14F10011" targetNode="P_41F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_99F10011"/>
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_100F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10011_I" deadCode="false" sourceNode="P_14F10011" targetNode="P_42F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_99F10011"/>
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_102F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10011_O" deadCode="false" sourceNode="P_14F10011" targetNode="P_43F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_99F10011"/>
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_102F10011"/>
	</edges>
	<edges id="P_14F10011P_15F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10011" targetNode="P_15F10011"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10011_I" deadCode="false" sourceNode="P_40F10011" targetNode="P_6F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_110F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10011_O" deadCode="false" sourceNode="P_40F10011" targetNode="P_7F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_110F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10011_I" deadCode="false" sourceNode="P_40F10011" targetNode="P_21F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_112F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10011_O" deadCode="false" sourceNode="P_40F10011" targetNode="P_22F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_112F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10011_I" deadCode="false" sourceNode="P_40F10011" targetNode="P_23F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_113F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10011_O" deadCode="false" sourceNode="P_40F10011" targetNode="P_24F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_113F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10011_I" deadCode="false" sourceNode="P_40F10011" targetNode="P_38F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_115F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10011_O" deadCode="false" sourceNode="P_40F10011" targetNode="P_39F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_115F10011"/>
	</edges>
	<edges id="P_40F10011P_41F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10011" targetNode="P_41F10011"/>
	<edges id="P_42F10011P_43F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10011" targetNode="P_43F10011"/>
	<edges id="P_21F10011P_22F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_21F10011" targetNode="P_22F10011"/>
	<edges id="P_28F10011P_29F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10011" targetNode="P_29F10011"/>
	<edges id="P_30F10011P_31F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10011" targetNode="P_31F10011"/>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10011_I" deadCode="false" sourceNode="P_32F10011" targetNode="P_44F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_171F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10011_O" deadCode="false" sourceNode="P_32F10011" targetNode="P_45F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_171F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10011_I" deadCode="false" sourceNode="P_32F10011" targetNode="P_44F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_175F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10011_O" deadCode="false" sourceNode="P_32F10011" targetNode="P_45F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_175F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10011_I" deadCode="false" sourceNode="P_32F10011" targetNode="P_44F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_179F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10011_O" deadCode="false" sourceNode="P_32F10011" targetNode="P_45F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_179F10011"/>
	</edges>
	<edges id="P_32F10011P_33F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10011" targetNode="P_33F10011"/>
	<edges xsi:type="cbl:PerformEdge" id="S_183F10011_I" deadCode="false" sourceNode="P_23F10011" targetNode="P_46F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_183F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_183F10011_O" deadCode="false" sourceNode="P_23F10011" targetNode="P_47F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_183F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10011_I" deadCode="false" sourceNode="P_23F10011" targetNode="P_46F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_187F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10011_O" deadCode="false" sourceNode="P_23F10011" targetNode="P_47F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_187F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10011_I" deadCode="false" sourceNode="P_23F10011" targetNode="P_46F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_191F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10011_O" deadCode="false" sourceNode="P_23F10011" targetNode="P_47F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_191F10011"/>
	</edges>
	<edges id="P_23F10011P_24F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_23F10011" targetNode="P_24F10011"/>
	<edges id="P_19F10011P_20F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_19F10011" targetNode="P_20F10011"/>
	<edges id="P_44F10011P_45F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10011" targetNode="P_45F10011"/>
	<edges xsi:type="cbl:PerformEdge" id="S_215F10011_I" deadCode="true" sourceNode="P_50F10011" targetNode="P_51F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_215F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_215F10011_O" deadCode="true" sourceNode="P_50F10011" targetNode="P_52F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_215F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10011_I" deadCode="true" sourceNode="P_50F10011" targetNode="P_53F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_216F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10011_O" deadCode="true" sourceNode="P_50F10011" targetNode="P_54F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_216F10011"/>
	</edges>
	<edges id="P_46F10011P_47F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10011" targetNode="P_47F10011"/>
	<edges xsi:type="cbl:PerformEdge" id="S_236F10011_I" deadCode="false" sourceNode="P_26F10011" targetNode="P_46F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_236F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_236F10011_O" deadCode="false" sourceNode="P_26F10011" targetNode="P_47F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_236F10011"/>
	</edges>
	<edges id="P_26F10011P_27F10011" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10011" targetNode="P_27F10011"/>
	<edges xsi:type="cbl:PerformEdge" id="S_240F10011_I" deadCode="true" sourceNode="P_56F10011" targetNode="P_50F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_240F10011"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_240F10011_O" deadCode="true" sourceNode="P_56F10011" targetNode="P_55F10011">
		<representations href="../../../cobol/IABS0130.cbl.cobModel#S_240F10011"/>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_35F10011_POS1" deadCode="false" targetNode="P_18F10011" sourceNode="DB2_BTC_PARALLELISM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_35F10011"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_65F10011_POS1" deadCode="false" sourceNode="P_12F10011" targetNode="DB2_BTC_PARALLELISM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_65F10011"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_73F10011_POS1" deadCode="false" targetNode="P_36F10011" sourceNode="DB2_BTC_PARALLELISM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_73F10011"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_76F10011_POS1" deadCode="false" targetNode="P_38F10011" sourceNode="DB2_BTC_PARALLELISM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_76F10011"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_109F10011_POS1" deadCode="false" targetNode="P_40F10011" sourceNode="DB2_BTC_PARALLELISM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_109F10011"></representations>
	</edges>
</Package>

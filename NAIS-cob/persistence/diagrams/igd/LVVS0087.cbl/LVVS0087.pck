<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0087" cbl:id="LVVS0087" xsi:id="LVVS0087" packageRef="LVVS0087.igd#LVVS0087" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0087_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10337" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0087.cbl.cobModel#SC_1F10337"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10337" deadCode="false" name="PROGRAM_LVVS0087_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0087.cbl.cobModel#P_1F10337"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10337" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0087.cbl.cobModel#P_2F10337"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10337" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0087.cbl.cobModel#P_3F10337"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10337" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0087.cbl.cobModel#P_4F10337"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10337" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0087.cbl.cobModel#P_5F10337"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10337" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0087.cbl.cobModel#P_6F10337"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10337" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0087.cbl.cobModel#P_7F10337"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10337P_1F10337" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10337" targetNode="P_1F10337"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10337_I" deadCode="false" sourceNode="P_1F10337" targetNode="P_2F10337">
		<representations href="../../../cobol/LVVS0087.cbl.cobModel#S_1F10337"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10337_O" deadCode="false" sourceNode="P_1F10337" targetNode="P_3F10337">
		<representations href="../../../cobol/LVVS0087.cbl.cobModel#S_1F10337"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10337_I" deadCode="false" sourceNode="P_1F10337" targetNode="P_4F10337">
		<representations href="../../../cobol/LVVS0087.cbl.cobModel#S_2F10337"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10337_O" deadCode="false" sourceNode="P_1F10337" targetNode="P_5F10337">
		<representations href="../../../cobol/LVVS0087.cbl.cobModel#S_2F10337"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10337_I" deadCode="false" sourceNode="P_1F10337" targetNode="P_6F10337">
		<representations href="../../../cobol/LVVS0087.cbl.cobModel#S_3F10337"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10337_O" deadCode="false" sourceNode="P_1F10337" targetNode="P_7F10337">
		<representations href="../../../cobol/LVVS0087.cbl.cobModel#S_3F10337"/>
	</edges>
	<edges id="P_2F10337P_3F10337" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10337" targetNode="P_3F10337"/>
	<edges id="P_4F10337P_5F10337" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10337" targetNode="P_5F10337"/>
</Package>

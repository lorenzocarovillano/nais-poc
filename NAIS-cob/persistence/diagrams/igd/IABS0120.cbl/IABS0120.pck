<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IABS0120" cbl:id="IABS0120" xsi:id="IABS0120" packageRef="IABS0120.igd#IABS0120" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IABS0120_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10010" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IABS0120.cbl.cobModel#SC_1F10010"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10010" deadCode="false" name="PROGRAM_IABS0120_FIRST_SENTENCES">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_1F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10010" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_2F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10010" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_3F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10010" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_16F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10010" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_17F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10010" deadCode="false" name="A200-ELABORA-PK">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_14F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10010" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_15F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10010" deadCode="false" name="A300-ELABORA-ID">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_4F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10010" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_5F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10010" deadCode="false" name="A400-ELABORA-IDP">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_6F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10010" deadCode="false" name="A400-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_7F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10010" deadCode="false" name="A500-ELABORA-IBO">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_8F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10010" deadCode="false" name="A500-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_9F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10010" deadCode="false" name="A600-ELABORA-IBS">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_10F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10010" deadCode="false" name="A600-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_11F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10010" deadCode="false" name="A700-ELABORA-IDO">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_12F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10010" deadCode="false" name="A700-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_13F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10010" deadCode="false" name="A210-SELECT-PK">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_18F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10010" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_19F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10010" deadCode="false" name="A220-INSERT-PK">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_20F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10010" deadCode="false" name="A220-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_21F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10010" deadCode="false" name="A230-UPDATE-PK">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_22F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10010" deadCode="false" name="A230-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_23F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10010" deadCode="false" name="A240-DELETE-PK">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_24F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10010" deadCode="false" name="A240-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_25F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_82F10010" deadCode="true" name="A305-DECLARE-CURSOR-ID">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_82F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_83F10010" deadCode="false" name="A305-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_83F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10010" deadCode="false" name="A310-SELECT-ID">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_26F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10010" deadCode="false" name="A310-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_27F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_84F10010" deadCode="true" name="A330-UPDATE-ID">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_84F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_85F10010" deadCode="true" name="A330-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_85F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10010" deadCode="false" name="A360-OPEN-CURSOR-ID">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_30F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10010" deadCode="false" name="A360-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_31F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10010" deadCode="false" name="A370-CLOSE-CURSOR-ID">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_32F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10010" deadCode="false" name="A370-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_33F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10010" deadCode="false" name="A380-FETCH-FIRST-ID">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_34F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10010" deadCode="false" name="A380-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_35F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10010" deadCode="false" name="A390-FETCH-NEXT-ID">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_36F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10010" deadCode="false" name="A390-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_37F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_86F10010" deadCode="true" name="A405-DECLARE-CURSOR-IDP">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_86F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_87F10010" deadCode="false" name="A405-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_87F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10010" deadCode="false" name="A410-SELECT-IDP">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_38F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10010" deadCode="false" name="A410-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_39F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10010" deadCode="false" name="A460-OPEN-CURSOR-IDP">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_40F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10010" deadCode="false" name="A460-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_41F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10010" deadCode="false" name="A470-CLOSE-CURSOR-IDP">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_42F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10010" deadCode="false" name="A470-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_43F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10010" deadCode="false" name="A480-FETCH-FIRST-IDP">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_44F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10010" deadCode="false" name="A480-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_45F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10010" deadCode="false" name="A490-FETCH-NEXT-IDP">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_46F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10010" deadCode="false" name="A490-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_47F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_88F10010" deadCode="true" name="A505-DECLARE-CURSOR-IBO">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_88F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_89F10010" deadCode="false" name="A505-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_89F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10010" deadCode="false" name="A510-SELECT-IBO">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_48F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10010" deadCode="false" name="A510-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_49F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10010" deadCode="false" name="A560-OPEN-CURSOR-IBO">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_50F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10010" deadCode="false" name="A560-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_51F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10010" deadCode="false" name="A570-CLOSE-CURSOR-IBO">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_52F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10010" deadCode="false" name="A570-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_53F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10010" deadCode="false" name="A580-FETCH-FIRST-IBO">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_54F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10010" deadCode="false" name="A580-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_55F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10010" deadCode="false" name="A590-FETCH-NEXT-IBO">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_56F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10010" deadCode="false" name="A590-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_57F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10010" deadCode="false" name="A610-SELECT-IBS">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_58F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10010" deadCode="false" name="A610-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_59F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_90F10010" deadCode="true" name="A705-DECLARE-CURSOR-IDO">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_90F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_91F10010" deadCode="false" name="A705-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_91F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10010" deadCode="false" name="A710-SELECT-IDO">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_60F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10010" deadCode="false" name="A710-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_61F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10010" deadCode="false" name="A760-OPEN-CURSOR-IDO">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_62F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10010" deadCode="false" name="A760-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_63F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10010" deadCode="false" name="A770-CLOSE-CURSOR-IDO">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_64F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10010" deadCode="false" name="A770-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_65F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10010" deadCode="false" name="A780-FETCH-FIRST-IDO">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_66F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10010" deadCode="false" name="A780-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_67F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10010" deadCode="false" name="A790-FETCH-NEXT-IDO">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_68F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10010" deadCode="false" name="A790-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_69F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10010" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_70F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10010" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_71F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10010" deadCode="false" name="Z150-VALORIZZA-DATA-SERVICES">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_74F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10010" deadCode="false" name="Z150-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_75F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_92F10010" deadCode="false" name="Z180-MAX-PROG-LOG-ERRORE">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_92F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_93F10010" deadCode="false" name="Z180-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_93F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10010" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_76F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10010" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_77F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_94F10010" deadCode="true" name="Z400-SEQ-RIGA">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_94F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_95F10010" deadCode="true" name="Z400-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_95F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10010" deadCode="false" name="Z500-AGGIORNAMENTO-STORICO">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_28F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10010" deadCode="false" name="Z500-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_29F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_96F10010" deadCode="true" name="Z600-INSERT-NUOVA-RIGA-STORICA">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_96F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_97F10010" deadCode="true" name="Z600-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_97F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_78F10010" deadCode="false" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_78F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_79F10010" deadCode="false" name="Z900-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_79F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10010" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_72F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10010" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_73F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_80F10010" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_80F10010"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_81F10010" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/IABS0120.cbl.cobModel#P_81F10010"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_LOG_ERRORE" name="LOG_ERRORE">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_LOG_ERRORE"/>
		</children>
	</packageNode>
	<edges id="SC_1F10010P_1F10010" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10010" targetNode="P_1F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10010_I" deadCode="false" sourceNode="P_1F10010" targetNode="P_2F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_1F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10010_O" deadCode="false" sourceNode="P_1F10010" targetNode="P_3F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_1F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10010_I" deadCode="false" sourceNode="P_1F10010" targetNode="P_4F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_4F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10010_O" deadCode="false" sourceNode="P_1F10010" targetNode="P_5F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_4F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10010_I" deadCode="false" sourceNode="P_1F10010" targetNode="P_6F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_5F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10010_O" deadCode="false" sourceNode="P_1F10010" targetNode="P_7F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_5F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10010_I" deadCode="false" sourceNode="P_1F10010" targetNode="P_8F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_6F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10010_O" deadCode="false" sourceNode="P_1F10010" targetNode="P_9F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_6F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10010_I" deadCode="false" sourceNode="P_1F10010" targetNode="P_10F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_7F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10010_O" deadCode="false" sourceNode="P_1F10010" targetNode="P_11F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_7F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10010_I" deadCode="false" sourceNode="P_1F10010" targetNode="P_12F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_8F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10010_O" deadCode="false" sourceNode="P_1F10010" targetNode="P_13F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_8F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10010_I" deadCode="false" sourceNode="P_1F10010" targetNode="P_14F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_12F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10010_O" deadCode="false" sourceNode="P_1F10010" targetNode="P_15F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_12F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10010_I" deadCode="false" sourceNode="P_1F10010" targetNode="P_12F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_13F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10010_O" deadCode="false" sourceNode="P_1F10010" targetNode="P_13F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_13F10010"/>
	</edges>
	<edges id="P_2F10010P_3F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10010" targetNode="P_3F10010"/>
	<edges id="P_16F10010P_17F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10010" targetNode="P_17F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10010_I" deadCode="false" sourceNode="P_14F10010" targetNode="P_18F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_37F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10010_O" deadCode="false" sourceNode="P_14F10010" targetNode="P_19F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_37F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10010_I" deadCode="false" sourceNode="P_14F10010" targetNode="P_20F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_38F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10010_O" deadCode="false" sourceNode="P_14F10010" targetNode="P_21F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_38F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10010_I" deadCode="false" sourceNode="P_14F10010" targetNode="P_22F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_39F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10010_O" deadCode="false" sourceNode="P_14F10010" targetNode="P_23F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_39F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10010_I" deadCode="false" sourceNode="P_14F10010" targetNode="P_24F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_40F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10010_O" deadCode="false" sourceNode="P_14F10010" targetNode="P_25F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_40F10010"/>
	</edges>
	<edges id="P_14F10010P_15F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10010" targetNode="P_15F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10010_I" deadCode="false" sourceNode="P_4F10010" targetNode="P_26F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_44F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10010_O" deadCode="false" sourceNode="P_4F10010" targetNode="P_27F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_44F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10010_I" deadCode="false" sourceNode="P_4F10010" targetNode="P_28F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_45F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10010_O" deadCode="false" sourceNode="P_4F10010" targetNode="P_29F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_45F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10010_I" deadCode="false" sourceNode="P_4F10010" targetNode="P_30F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_46F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10010_O" deadCode="false" sourceNode="P_4F10010" targetNode="P_31F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_46F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10010_I" deadCode="false" sourceNode="P_4F10010" targetNode="P_32F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_47F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10010_O" deadCode="false" sourceNode="P_4F10010" targetNode="P_33F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_47F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10010_I" deadCode="false" sourceNode="P_4F10010" targetNode="P_34F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_48F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10010_O" deadCode="false" sourceNode="P_4F10010" targetNode="P_35F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_48F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10010_I" deadCode="false" sourceNode="P_4F10010" targetNode="P_36F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_49F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10010_O" deadCode="false" sourceNode="P_4F10010" targetNode="P_37F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_49F10010"/>
	</edges>
	<edges id="P_4F10010P_5F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10010" targetNode="P_5F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10010_I" deadCode="false" sourceNode="P_6F10010" targetNode="P_38F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_53F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10010_O" deadCode="false" sourceNode="P_6F10010" targetNode="P_39F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_53F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10010_I" deadCode="false" sourceNode="P_6F10010" targetNode="P_40F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_54F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10010_O" deadCode="false" sourceNode="P_6F10010" targetNode="P_41F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_54F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10010_I" deadCode="false" sourceNode="P_6F10010" targetNode="P_42F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_55F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10010_O" deadCode="false" sourceNode="P_6F10010" targetNode="P_43F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_55F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10010_I" deadCode="false" sourceNode="P_6F10010" targetNode="P_44F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_56F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10010_O" deadCode="false" sourceNode="P_6F10010" targetNode="P_45F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_56F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10010_I" deadCode="false" sourceNode="P_6F10010" targetNode="P_46F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_57F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10010_O" deadCode="false" sourceNode="P_6F10010" targetNode="P_47F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_57F10010"/>
	</edges>
	<edges id="P_6F10010P_7F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10010" targetNode="P_7F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10010_I" deadCode="false" sourceNode="P_8F10010" targetNode="P_48F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_61F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10010_O" deadCode="false" sourceNode="P_8F10010" targetNode="P_49F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_61F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10010_I" deadCode="false" sourceNode="P_8F10010" targetNode="P_50F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_62F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10010_O" deadCode="false" sourceNode="P_8F10010" targetNode="P_51F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_62F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10010_I" deadCode="false" sourceNode="P_8F10010" targetNode="P_52F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_63F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10010_O" deadCode="false" sourceNode="P_8F10010" targetNode="P_53F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_63F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10010_I" deadCode="false" sourceNode="P_8F10010" targetNode="P_54F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_64F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10010_O" deadCode="false" sourceNode="P_8F10010" targetNode="P_55F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_64F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10010_I" deadCode="false" sourceNode="P_8F10010" targetNode="P_56F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_65F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10010_O" deadCode="false" sourceNode="P_8F10010" targetNode="P_57F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_65F10010"/>
	</edges>
	<edges id="P_8F10010P_9F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10010" targetNode="P_9F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10010_I" deadCode="false" sourceNode="P_10F10010" targetNode="P_58F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_69F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10010_O" deadCode="false" sourceNode="P_10F10010" targetNode="P_59F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_69F10010"/>
	</edges>
	<edges id="P_10F10010P_11F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10010" targetNode="P_11F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10010_I" deadCode="false" sourceNode="P_12F10010" targetNode="P_60F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_73F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10010_O" deadCode="false" sourceNode="P_12F10010" targetNode="P_61F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_73F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10010_I" deadCode="false" sourceNode="P_12F10010" targetNode="P_62F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_74F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10010_O" deadCode="false" sourceNode="P_12F10010" targetNode="P_63F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_74F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10010_I" deadCode="false" sourceNode="P_12F10010" targetNode="P_64F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_75F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10010_O" deadCode="false" sourceNode="P_12F10010" targetNode="P_65F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_75F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10010_I" deadCode="false" sourceNode="P_12F10010" targetNode="P_66F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_76F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10010_O" deadCode="false" sourceNode="P_12F10010" targetNode="P_67F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_76F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10010_I" deadCode="false" sourceNode="P_12F10010" targetNode="P_68F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_77F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10010_O" deadCode="false" sourceNode="P_12F10010" targetNode="P_69F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_77F10010"/>
	</edges>
	<edges id="P_12F10010P_13F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10010" targetNode="P_13F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10010_I" deadCode="false" sourceNode="P_18F10010" targetNode="P_16F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_81F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10010_O" deadCode="false" sourceNode="P_18F10010" targetNode="P_17F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_81F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10010_I" deadCode="false" sourceNode="P_18F10010" targetNode="P_70F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_83F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10010_O" deadCode="false" sourceNode="P_18F10010" targetNode="P_71F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_83F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10010_I" deadCode="false" sourceNode="P_18F10010" targetNode="P_72F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_84F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10010_O" deadCode="false" sourceNode="P_18F10010" targetNode="P_73F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_84F10010"/>
	</edges>
	<edges id="P_18F10010P_19F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10010" targetNode="P_19F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10010_I" deadCode="false" sourceNode="P_20F10010" targetNode="P_74F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_86F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10010_O" deadCode="false" sourceNode="P_20F10010" targetNode="P_75F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_86F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10010_I" deadCode="false" sourceNode="P_20F10010" targetNode="P_76F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_87F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10010_O" deadCode="false" sourceNode="P_20F10010" targetNode="P_77F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_87F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10010_I" deadCode="false" sourceNode="P_20F10010" targetNode="P_78F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_88F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10010_O" deadCode="false" sourceNode="P_20F10010" targetNode="P_79F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_88F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10010_I" deadCode="false" sourceNode="P_20F10010" targetNode="P_80F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_89F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10010_O" deadCode="false" sourceNode="P_20F10010" targetNode="P_81F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_89F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10010_I" deadCode="false" sourceNode="P_20F10010" targetNode="P_16F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_91F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10010_O" deadCode="false" sourceNode="P_20F10010" targetNode="P_17F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_91F10010"/>
	</edges>
	<edges id="P_20F10010P_21F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10010" targetNode="P_21F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10010_I" deadCode="false" sourceNode="P_22F10010" targetNode="P_74F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_93F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10010_O" deadCode="false" sourceNode="P_22F10010" targetNode="P_75F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_93F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10010_I" deadCode="false" sourceNode="P_22F10010" targetNode="P_76F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_94F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10010_O" deadCode="false" sourceNode="P_22F10010" targetNode="P_77F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_94F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10010_I" deadCode="false" sourceNode="P_22F10010" targetNode="P_78F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_95F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10010_O" deadCode="false" sourceNode="P_22F10010" targetNode="P_79F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_95F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10010_I" deadCode="false" sourceNode="P_22F10010" targetNode="P_80F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_96F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10010_O" deadCode="false" sourceNode="P_22F10010" targetNode="P_81F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_96F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10010_I" deadCode="false" sourceNode="P_22F10010" targetNode="P_16F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_98F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10010_O" deadCode="false" sourceNode="P_22F10010" targetNode="P_17F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_98F10010"/>
	</edges>
	<edges id="P_22F10010P_23F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10010" targetNode="P_23F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10010_I" deadCode="false" sourceNode="P_24F10010" targetNode="P_16F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_101F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10010_O" deadCode="false" sourceNode="P_24F10010" targetNode="P_17F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_101F10010"/>
	</edges>
	<edges id="P_24F10010P_25F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10010" targetNode="P_25F10010"/>
	<edges id="P_82F10010P_83F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_82F10010" targetNode="P_83F10010"/>
	<edges id="P_26F10010P_27F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10010" targetNode="P_27F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10010_I" deadCode="true" sourceNode="P_84F10010" targetNode="P_74F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_107F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10010_O" deadCode="true" sourceNode="P_84F10010" targetNode="P_75F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_107F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10010_I" deadCode="true" sourceNode="P_84F10010" targetNode="P_76F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_108F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10010_O" deadCode="true" sourceNode="P_84F10010" targetNode="P_77F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_108F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10010_I" deadCode="true" sourceNode="P_30F10010" targetNode="P_82F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_111F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10010_O" deadCode="true" sourceNode="P_30F10010" targetNode="P_83F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_111F10010"/>
	</edges>
	<edges id="P_30F10010P_31F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10010" targetNode="P_31F10010"/>
	<edges id="P_32F10010P_33F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10010" targetNode="P_33F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10010_I" deadCode="false" sourceNode="P_34F10010" targetNode="P_30F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_116F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10010_O" deadCode="false" sourceNode="P_34F10010" targetNode="P_31F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_116F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10010_I" deadCode="false" sourceNode="P_34F10010" targetNode="P_36F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_118F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10010_O" deadCode="false" sourceNode="P_34F10010" targetNode="P_37F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_118F10010"/>
	</edges>
	<edges id="P_34F10010P_35F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10010" targetNode="P_35F10010"/>
	<edges id="P_36F10010P_37F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10010" targetNode="P_37F10010"/>
	<edges id="P_86F10010P_87F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_86F10010" targetNode="P_87F10010"/>
	<edges id="P_38F10010P_39F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10010" targetNode="P_39F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10010_I" deadCode="true" sourceNode="P_40F10010" targetNode="P_86F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_126F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10010_O" deadCode="true" sourceNode="P_40F10010" targetNode="P_87F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_126F10010"/>
	</edges>
	<edges id="P_40F10010P_41F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10010" targetNode="P_41F10010"/>
	<edges id="P_42F10010P_43F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10010" targetNode="P_43F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10010_I" deadCode="false" sourceNode="P_44F10010" targetNode="P_40F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_131F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10010_O" deadCode="false" sourceNode="P_44F10010" targetNode="P_41F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_131F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10010_I" deadCode="false" sourceNode="P_44F10010" targetNode="P_46F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_133F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10010_O" deadCode="false" sourceNode="P_44F10010" targetNode="P_47F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_133F10010"/>
	</edges>
	<edges id="P_44F10010P_45F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10010" targetNode="P_45F10010"/>
	<edges id="P_46F10010P_47F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10010" targetNode="P_47F10010"/>
	<edges id="P_88F10010P_89F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_88F10010" targetNode="P_89F10010"/>
	<edges id="P_48F10010P_49F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10010" targetNode="P_49F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_141F10010_I" deadCode="true" sourceNode="P_50F10010" targetNode="P_88F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_141F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_141F10010_O" deadCode="true" sourceNode="P_50F10010" targetNode="P_89F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_141F10010"/>
	</edges>
	<edges id="P_50F10010P_51F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10010" targetNode="P_51F10010"/>
	<edges id="P_52F10010P_53F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10010" targetNode="P_53F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10010_I" deadCode="false" sourceNode="P_54F10010" targetNode="P_50F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_146F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10010_O" deadCode="false" sourceNode="P_54F10010" targetNode="P_51F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_146F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10010_I" deadCode="false" sourceNode="P_54F10010" targetNode="P_56F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_148F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10010_O" deadCode="false" sourceNode="P_54F10010" targetNode="P_57F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_148F10010"/>
	</edges>
	<edges id="P_54F10010P_55F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10010" targetNode="P_55F10010"/>
	<edges id="P_56F10010P_57F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10010" targetNode="P_57F10010"/>
	<edges id="P_58F10010P_59F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10010" targetNode="P_59F10010"/>
	<edges id="P_90F10010P_91F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_90F10010" targetNode="P_91F10010"/>
	<edges id="P_60F10010P_61F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_60F10010" targetNode="P_61F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10010_I" deadCode="true" sourceNode="P_62F10010" targetNode="P_90F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_158F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10010_O" deadCode="true" sourceNode="P_62F10010" targetNode="P_91F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_158F10010"/>
	</edges>
	<edges id="P_62F10010P_63F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10010" targetNode="P_63F10010"/>
	<edges id="P_64F10010P_65F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10010" targetNode="P_65F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10010_I" deadCode="false" sourceNode="P_66F10010" targetNode="P_62F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_163F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10010_O" deadCode="false" sourceNode="P_66F10010" targetNode="P_63F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_163F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10010_I" deadCode="false" sourceNode="P_66F10010" targetNode="P_68F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_165F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10010_O" deadCode="false" sourceNode="P_66F10010" targetNode="P_69F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_165F10010"/>
	</edges>
	<edges id="P_66F10010P_67F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10010" targetNode="P_67F10010"/>
	<edges id="P_68F10010P_69F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10010" targetNode="P_69F10010"/>
	<edges id="P_70F10010P_71F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_70F10010" targetNode="P_71F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10010_I" deadCode="false" sourceNode="P_74F10010" targetNode="P_92F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_185F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10010_O" deadCode="false" sourceNode="P_74F10010" targetNode="P_93F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_185F10010"/>
	</edges>
	<edges id="P_74F10010P_75F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10010" targetNode="P_75F10010"/>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10010_I" deadCode="false" sourceNode="P_92F10010" targetNode="P_16F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_189F10010"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10010_O" deadCode="false" sourceNode="P_92F10010" targetNode="P_17F10010">
		<representations href="../../../cobol/IABS0120.cbl.cobModel#S_189F10010"/>
	</edges>
	<edges id="P_92F10010P_93F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_92F10010" targetNode="P_93F10010"/>
	<edges id="P_76F10010P_77F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_76F10010" targetNode="P_77F10010"/>
	<edges id="P_28F10010P_29F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10010" targetNode="P_29F10010"/>
	<edges id="P_78F10010P_79F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_78F10010" targetNode="P_79F10010"/>
	<edges id="P_72F10010P_73F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10010" targetNode="P_73F10010"/>
	<edges id="P_80F10010P_81F10010" xsi:type="cbl:FallThroughEdge" sourceNode="P_80F10010" targetNode="P_81F10010"/>
	<edges xsi:type="cbl:DataEdge" id="S_80F10010_POS1" deadCode="false" targetNode="P_18F10010" sourceNode="DB2_LOG_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_80F10010"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_90F10010_POS1" deadCode="false" sourceNode="P_20F10010" targetNode="DB2_LOG_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_90F10010"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_97F10010_POS1" deadCode="false" sourceNode="P_22F10010" targetNode="DB2_LOG_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_97F10010"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_100F10010_POS1" deadCode="false" sourceNode="P_24F10010" targetNode="DB2_LOG_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_100F10010"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_188F10010_POS1" deadCode="false" targetNode="P_92F10010" sourceNode="DB2_LOG_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_188F10010"></representations>
	</edges>
</Package>

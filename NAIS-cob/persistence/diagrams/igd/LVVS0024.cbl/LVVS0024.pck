<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0024" cbl:id="LVVS0024" xsi:id="LVVS0024" packageRef="LVVS0024.igd#LVVS0024" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0024_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10319" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0024.cbl.cobModel#SC_1F10319"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10319" deadCode="false" name="PROGRAM_LVVS0024_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0024.cbl.cobModel#P_1F10319"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10319" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0024.cbl.cobModel#P_2F10319"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10319" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0024.cbl.cobModel#P_3F10319"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10319" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0024.cbl.cobModel#P_4F10319"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10319" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0024.cbl.cobModel#P_5F10319"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10319" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0024.cbl.cobModel#P_8F10319"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10319" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0024.cbl.cobModel#P_9F10319"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10319" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS0024.cbl.cobModel#P_10F10319"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10319" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS0024.cbl.cobModel#P_11F10319"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10319" deadCode="false" name="S1300-TIPO-TRANCHE">
				<representations href="../../../cobol/LVVS0024.cbl.cobModel#P_12F10319"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10319" deadCode="false" name="S1300-TIPO-TRANCHE-EX">
				<representations href="../../../cobol/LVVS0024.cbl.cobModel#P_13F10319"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10319" deadCode="false" name="S2027-TRANCHE">
				<representations href="../../../cobol/LVVS0024.cbl.cobModel#P_14F10319"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10319" deadCode="false" name="S2027-EX">
				<representations href="../../../cobol/LVVS0024.cbl.cobModel#P_15F10319"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10319" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0024.cbl.cobModel#P_6F10319"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10319" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0024.cbl.cobModel#P_7F10319"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10319P_1F10319" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10319" targetNode="P_1F10319"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10319_I" deadCode="false" sourceNode="P_1F10319" targetNode="P_2F10319">
		<representations href="../../../cobol/LVVS0024.cbl.cobModel#S_1F10319"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10319_O" deadCode="false" sourceNode="P_1F10319" targetNode="P_3F10319">
		<representations href="../../../cobol/LVVS0024.cbl.cobModel#S_1F10319"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10319_I" deadCode="false" sourceNode="P_1F10319" targetNode="P_4F10319">
		<representations href="../../../cobol/LVVS0024.cbl.cobModel#S_2F10319"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10319_O" deadCode="false" sourceNode="P_1F10319" targetNode="P_5F10319">
		<representations href="../../../cobol/LVVS0024.cbl.cobModel#S_2F10319"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10319_I" deadCode="false" sourceNode="P_1F10319" targetNode="P_6F10319">
		<representations href="../../../cobol/LVVS0024.cbl.cobModel#S_3F10319"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10319_O" deadCode="false" sourceNode="P_1F10319" targetNode="P_7F10319">
		<representations href="../../../cobol/LVVS0024.cbl.cobModel#S_3F10319"/>
	</edges>
	<edges id="P_2F10319P_3F10319" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10319" targetNode="P_3F10319"/>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10319_I" deadCode="false" sourceNode="P_4F10319" targetNode="P_8F10319">
		<representations href="../../../cobol/LVVS0024.cbl.cobModel#S_9F10319"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10319_O" deadCode="false" sourceNode="P_4F10319" targetNode="P_9F10319">
		<representations href="../../../cobol/LVVS0024.cbl.cobModel#S_9F10319"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10319_I" deadCode="false" sourceNode="P_4F10319" targetNode="P_10F10319">
		<representations href="../../../cobol/LVVS0024.cbl.cobModel#S_11F10319"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10319_O" deadCode="false" sourceNode="P_4F10319" targetNode="P_11F10319">
		<representations href="../../../cobol/LVVS0024.cbl.cobModel#S_11F10319"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10319_I" deadCode="false" sourceNode="P_4F10319" targetNode="P_12F10319">
		<representations href="../../../cobol/LVVS0024.cbl.cobModel#S_13F10319"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10319_O" deadCode="false" sourceNode="P_4F10319" targetNode="P_13F10319">
		<representations href="../../../cobol/LVVS0024.cbl.cobModel#S_13F10319"/>
	</edges>
	<edges id="P_4F10319P_5F10319" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10319" targetNode="P_5F10319"/>
	<edges id="P_8F10319P_9F10319" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10319" targetNode="P_9F10319"/>
	<edges id="P_10F10319P_11F10319" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10319" targetNode="P_11F10319"/>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10319_I" deadCode="false" sourceNode="P_12F10319" targetNode="P_14F10319">
		<representations href="../../../cobol/LVVS0024.cbl.cobModel#S_29F10319"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10319_O" deadCode="false" sourceNode="P_12F10319" targetNode="P_15F10319">
		<representations href="../../../cobol/LVVS0024.cbl.cobModel#S_29F10319"/>
	</edges>
	<edges id="P_12F10319P_13F10319" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10319" targetNode="P_13F10319"/>
	<edges id="P_14F10319P_15F10319" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10319" targetNode="P_15F10319"/>
</Package>

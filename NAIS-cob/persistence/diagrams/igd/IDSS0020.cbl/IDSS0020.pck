<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IDSS0020" cbl:id="IDSS0020" xsi:id="IDSS0020" packageRef="IDSS0020.igd#IDSS0020" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IDSS0020_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10100" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_1F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10100" deadCode="false" name="MAIN">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_1F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_2F10100" deadCode="false" name="A000-INIZIALIZZA-OUTPUT">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_2F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10100" deadCode="false" name="A000-INIZIALIZZA-OUTPUT_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_2F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10100" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_3F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_4F10100" deadCode="false" name="A100-DECLARE">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_4F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10100" deadCode="false" name="A100-DECLARE_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_4F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10100" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_5F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_5F10100" deadCode="false" name="A200-OPEN">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_5F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10100" deadCode="false" name="A200-OPEN_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_6F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10100" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_7F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_6F10100" deadCode="false" name="A300-FETCH">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_6F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10100" deadCode="false" name="A300-FETCH_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_8F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10100" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_9F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_7F10100" deadCode="false" name="A400-CLOSE">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_7F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10100" deadCode="false" name="A400-CLOSE_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_10F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10100" deadCode="false" name="A400-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_11F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_8F10100" deadCode="false" name="A500-OPERAZIONI-FINALI">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_8F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10100" deadCode="false" name="A500-OPERAZIONI-FINALI_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_12F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10100" deadCode="false" name="A500-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_13F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_11F10100" deadCode="false" name="C000-CONTROLLO-FIGLIO">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_11F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10100" deadCode="false" name="C000-CONTROLLO-FIGLIO_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_14F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10100" deadCode="false" name="C000-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_15F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_14F10100" deadCode="false" name="C050-VALORIZZA-BASE">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_14F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10100" deadCode="false" name="C050-VALORIZZA-BASE_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_16F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10100" deadCode="false" name="C050-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_17F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_15F10100" deadCode="false" name="C100-CONTROLLO-COMPOSIZIONE">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_15F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10100" deadCode="false" name="C100-CONTROLLO-COMPOSIZIONE_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_18F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10100" deadCode="false" name="C100-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_19F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_17F10100" deadCode="false" name="C130-VALORIZZA-DATO-COMPLETO">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_17F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10100" deadCode="false" name="C130-VALORIZZA-DATO-COMPLETO_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_20F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10100" deadCode="false" name="C130-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_21F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_16F10100" deadCode="false" name="C150-COMPOSIZIONE-PADRE">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_16F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10100" deadCode="false" name="C150-COMPOSIZIONE-PADRE_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_22F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10100" deadCode="false" name="C150-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_23F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_24F10100" deadCode="false" name="C180-COMPOSIZIONE-RDS">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_24F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10100" deadCode="false" name="C180-COMPOSIZIONE-RDS_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_24F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10100" deadCode="false" name="C180-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_25F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_23F10100" deadCode="false" name="C200-INCREMENTA-LUNGHEZZA">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_23F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10100" deadCode="false" name="C200-INCREMENTA-LUNGHEZZA_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_26F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10100" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_27F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_12F10100" deadCode="false" name="C300-CTRL-STRUTTURA-PADRE">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_12F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10100" deadCode="false" name="C300-CTRL-STRUTTURA-PADRE_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_28F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10100" deadCode="false" name="C300-EXIT">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_29F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_25F10100" deadCode="false" name="C330-RICERCA-FLAG-CALL-USING">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_25F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10100" deadCode="false" name="C330-RICERCA-FLAG-CALL-USING_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_30F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10100" deadCode="false" name="C330-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_31F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_26F10100" deadCode="false" name="C350-SCHIACCIA-STRUTTURA">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_26F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10100" deadCode="false" name="C350-SCHIACCIA-STRUTTURA_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_32F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10100" deadCode="false" name="C350-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_33F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_13F10100" deadCode="false" name="N000-CNTL-CAMPI-NULL-CSD">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_13F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10100" deadCode="false" name="N000-CNTL-CAMPI-NULL-CSD_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_34F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10100" deadCode="false" name="N000-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_35F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_27F10100" deadCode="false" name="N100-CNTL-CAMPI-NULL-ADA">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_27F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10100" deadCode="false" name="N100-CNTL-CAMPI-NULL-ADA_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_36F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10100" deadCode="false" name="N100-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_37F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_19F10100" deadCode="false" name="R000-RICORSIONE">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_19F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10100" deadCode="false" name="R000-RICORSIONE_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_38F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10100" deadCode="false" name="R000-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_39F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_21F10100" deadCode="false" name="R100-ACCEDI-ANAGR-DATO">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_21F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10100" deadCode="false" name="R100-ACCEDI-ANAGR-DATO_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_40F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10100" deadCode="false" name="R100-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_41F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_22F10100" deadCode="false" name="R200-CTRL-LUNGHEZZA-CAMPO">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_22F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10100" deadCode="false" name="R200-CTRL-LUNGHEZZA-CAMPO_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_42F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10100" deadCode="false" name="R200-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_43F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_18F10100" deadCode="false" name="R300-CNTL-RICORRENZA">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_18F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10100" deadCode="false" name="R300-CNTL-RICORRENZA_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_44F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10100" deadCode="false" name="R300-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_45F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_28F10100" deadCode="false" name="R500-ACCEDI-SU-RDS">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_28F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10100" deadCode="false" name="R500-ACCEDI-SU-RDS_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_46F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10100" deadCode="false" name="R500-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_47F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_20F10100" deadCode="false" name="R600-CNTL-REDEFINES">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_20F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10100" deadCode="false" name="R600-CNTL-REDEFINES_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_48F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10100" deadCode="false" name="R600-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_49F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_10F10100" deadCode="false" name="S000-ELABORA-SST">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_10F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10100" deadCode="false" name="S000-ELABORA-SST_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_50F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10100" deadCode="false" name="S000-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_51F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_29F10100" deadCode="false" name="S210-VALORIZZA-CAMPI">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_29F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10100" deadCode="false" name="S210-VALORIZZA-CAMPI_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_52F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10100" deadCode="false" name="S210-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_53F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_30F10100" deadCode="false" name="S200-ACCEDI-SU-SST">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_30F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10100" deadCode="false" name="S200-ACCEDI-SU-SST_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_54F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10100" deadCode="false" name="S200-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_55F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_3F10100" deadCode="false" name="2000-DBERROR">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_3F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10100" deadCode="false" name="2000-DBERROR_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_56F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10100" deadCode="false" name="2000-EX">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_57F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_9F10100" deadCode="false" name="4000-FINE">
			<representations href="../../../cobol/IDSS0020.cbl.cobModel#SC_9F10100"/>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10100" deadCode="false" name="4000-FINE_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_58F10100"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10100" deadCode="false" name="4000-EX-FINE">
				<representations href="../../../cobol/IDSS0020.cbl.cobModel#P_59F10100"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_COMP_STR_DATO" name="COMP_STR_DATO">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_COMP_STR_DATO"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_RIDEF_DATO_STR" name="RIDEF_DATO_STR">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_RIDEF_DATO_STR"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_SINONIMO_STR" name="SINONIMO_STR">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_SINONIMO_STR"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_IDSS0020_PGM-NAME-IDSS0020" name="Dynamic IDSS0020 PGM-NAME-IDSS0020" missing="true">
			<representations href="../../../../missing.xmi#IDCFDGDPOYC2AWNKB1FSRW4WRL3DE0OKJLMBBK55CZ1PDSEAZMRWCO"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_IDSS0020_PGM-NAME-IDSS0080" name="Dynamic IDSS0020 PGM-NAME-IDSS0080" missing="true">
			<representations href="../../../../missing.xmi#ID2MC53BP2GQNMDVTIS3LITYK4PESVUEM2BQTXNCG4G4YUHCXGSEHF"/>
		</children>
	</packageNode>
	<edges id="SC_1F10100P_1F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10100" targetNode="P_1F10100"/>
	<edges id="SC_2F10100P_2F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_2F10100" targetNode="P_2F10100"/>
	<edges id="SC_4F10100P_4F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_4F10100" targetNode="P_4F10100"/>
	<edges id="SC_5F10100P_6F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_5F10100" targetNode="P_6F10100"/>
	<edges id="SC_6F10100P_8F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_6F10100" targetNode="P_8F10100"/>
	<edges id="SC_7F10100P_10F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_7F10100" targetNode="P_10F10100"/>
	<edges id="SC_8F10100P_12F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_8F10100" targetNode="P_12F10100"/>
	<edges id="SC_11F10100P_14F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_11F10100" targetNode="P_14F10100"/>
	<edges id="SC_14F10100P_16F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_14F10100" targetNode="P_16F10100"/>
	<edges id="SC_15F10100P_18F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_15F10100" targetNode="P_18F10100"/>
	<edges id="SC_17F10100P_20F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_17F10100" targetNode="P_20F10100"/>
	<edges id="SC_16F10100P_22F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_16F10100" targetNode="P_22F10100"/>
	<edges id="SC_24F10100P_24F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_24F10100" targetNode="P_24F10100"/>
	<edges id="SC_23F10100P_26F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_23F10100" targetNode="P_26F10100"/>
	<edges id="SC_12F10100P_28F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_12F10100" targetNode="P_28F10100"/>
	<edges id="SC_25F10100P_30F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_25F10100" targetNode="P_30F10100"/>
	<edges id="SC_26F10100P_32F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_26F10100" targetNode="P_32F10100"/>
	<edges id="SC_13F10100P_34F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_13F10100" targetNode="P_34F10100"/>
	<edges id="SC_27F10100P_36F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_27F10100" targetNode="P_36F10100"/>
	<edges id="SC_19F10100P_38F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_19F10100" targetNode="P_38F10100"/>
	<edges id="SC_21F10100P_40F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_21F10100" targetNode="P_40F10100"/>
	<edges id="SC_22F10100P_42F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_22F10100" targetNode="P_42F10100"/>
	<edges id="SC_18F10100P_44F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_18F10100" targetNode="P_44F10100"/>
	<edges id="SC_28F10100P_46F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_28F10100" targetNode="P_46F10100"/>
	<edges id="SC_20F10100P_48F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_20F10100" targetNode="P_48F10100"/>
	<edges id="SC_10F10100P_50F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_10F10100" targetNode="P_50F10100"/>
	<edges id="SC_29F10100P_52F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_29F10100" targetNode="P_52F10100"/>
	<edges id="SC_30F10100P_54F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_30F10100" targetNode="P_54F10100"/>
	<edges id="SC_3F10100P_56F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_3F10100" targetNode="P_56F10100"/>
	<edges id="SC_9F10100P_58F10100" xsi:type="cbl:FallThroughEdge" sourceNode="SC_9F10100" targetNode="P_58F10100"/>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10100_I" deadCode="false" sourceNode="P_1F10100" targetNode="SC_2F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_3F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10100_I" deadCode="false" sourceNode="P_1F10100" targetNode="SC_3F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_10F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10100_I" deadCode="false" sourceNode="P_1F10100" targetNode="SC_4F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_13F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10100_I" deadCode="false" sourceNode="P_1F10100" targetNode="SC_5F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_14F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10100_I" deadCode="false" sourceNode="P_1F10100" targetNode="SC_6F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_16F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10100_I" deadCode="false" sourceNode="P_1F10100" targetNode="SC_7F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_18F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10100_I" deadCode="false" sourceNode="P_1F10100" targetNode="SC_8F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_19F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10100_I" deadCode="false" sourceNode="P_1F10100" targetNode="SC_9F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_20F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10100_I" deadCode="false" sourceNode="P_2F10100" targetNode="SC_10F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_31F10100"/>
	</edges>
	<edges id="P_2F10100P_3F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10100" targetNode="P_3F10100"/>
	<edges id="P_4F10100P_5F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10100" targetNode="P_5F10100"/>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10100_I" deadCode="false" sourceNode="P_6F10100" targetNode="SC_3F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_90F10100"/>
	</edges>
	<edges id="P_6F10100P_7F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10100" targetNode="P_7F10100"/>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10100_I" deadCode="false" sourceNode="P_8F10100" targetNode="SC_11F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_94F10100"/>
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_96F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10100_I" deadCode="false" sourceNode="P_8F10100" targetNode="SC_11F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_97F10100"/>
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_99F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10100_I" deadCode="false" sourceNode="P_8F10100" targetNode="SC_11F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_100F10100"/>
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_102F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10100_I" deadCode="false" sourceNode="P_8F10100" targetNode="SC_11F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_103F10100"/>
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_105F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10100_I" deadCode="false" sourceNode="P_8F10100" targetNode="SC_11F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_106F10100"/>
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_108F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10100_I" deadCode="false" sourceNode="P_8F10100" targetNode="SC_11F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_109F10100"/>
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_111F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10100_I" deadCode="false" sourceNode="P_8F10100" targetNode="SC_11F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_112F10100"/>
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_114F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10100_I" deadCode="false" sourceNode="P_8F10100" targetNode="SC_11F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_115F10100"/>
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_117F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10100_I" deadCode="false" sourceNode="P_8F10100" targetNode="SC_11F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_118F10100"/>
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_120F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10100_I" deadCode="false" sourceNode="P_8F10100" targetNode="SC_11F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_121F10100"/>
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_123F10100"/>
	</edges>
	<edges id="P_8F10100P_9F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10100" targetNode="P_9F10100"/>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10100_I" deadCode="false" sourceNode="P_10F10100" targetNode="SC_3F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_149F10100"/>
	</edges>
	<edges id="P_10F10100P_11F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10100" targetNode="P_11F10100"/>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10100_I" deadCode="false" sourceNode="P_12F10100" targetNode="SC_12F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_152F10100"/>
	</edges>
	<edges id="P_12F10100P_13F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10100" targetNode="P_13F10100"/>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10100_I" deadCode="false" sourceNode="P_14F10100" targetNode="SC_3F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_157F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10100_I" deadCode="false" sourceNode="P_14F10100" targetNode="SC_7F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_158F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10100_I" deadCode="false" sourceNode="P_14F10100" targetNode="SC_13F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_161F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10100_I" deadCode="false" sourceNode="P_14F10100" targetNode="SC_14F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_162F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10100_I" deadCode="false" sourceNode="P_14F10100" targetNode="SC_15F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_167F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10100_I" deadCode="false" sourceNode="P_14F10100" targetNode="SC_16F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_168F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10100_I" deadCode="false" sourceNode="P_14F10100" targetNode="SC_17F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_170F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10100_I" deadCode="false" sourceNode="P_14F10100" targetNode="SC_18F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_171F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10100_I" deadCode="false" sourceNode="P_14F10100" targetNode="SC_19F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_172F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10100_I" deadCode="false" sourceNode="P_14F10100" targetNode="SC_3F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_189F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10100_I" deadCode="false" sourceNode="P_14F10100" targetNode="SC_20F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_191F10100"/>
	</edges>
	<edges id="P_14F10100P_15F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10100" targetNode="P_15F10100"/>
	<edges id="P_16F10100P_17F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10100" targetNode="P_17F10100"/>
	<edges xsi:type="cbl:PerformEdge" id="S_214F10100_I" deadCode="false" sourceNode="P_18F10100" targetNode="SC_21F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_214F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_223F10100_I" deadCode="false" sourceNode="P_18F10100" targetNode="SC_22F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_223F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_225F10100_I" deadCode="false" sourceNode="P_18F10100" targetNode="SC_23F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_225F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_233F10100_I" deadCode="false" sourceNode="P_18F10100" targetNode="SC_17F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_233F10100"/>
	</edges>
	<edges id="P_18F10100P_19F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10100" targetNode="P_19F10100"/>
	<edges xsi:type="cbl:PerformEdge" id="S_244F10100_I" deadCode="false" sourceNode="P_20F10100" targetNode="SC_22F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_244F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_246F10100_I" deadCode="false" sourceNode="P_20F10100" targetNode="SC_23F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_246F10100"/>
	</edges>
	<edges id="P_20F10100P_21F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10100" targetNode="P_21F10100"/>
	<edges id="P_22F10100P_23F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10100" targetNode="P_23F10100"/>
	<edges id="P_24F10100P_25F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10100" targetNode="P_25F10100"/>
	<edges id="P_26F10100P_27F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10100" targetNode="P_27F10100"/>
	<edges xsi:type="cbl:PerformEdge" id="S_277F10100_I" deadCode="false" sourceNode="P_28F10100" targetNode="SC_25F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_277F10100"/>
	</edges>
	<edges id="P_28F10100P_29F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10100" targetNode="P_29F10100"/>
	<edges xsi:type="cbl:PerformEdge" id="S_282F10100_I" deadCode="false" sourceNode="P_30F10100" targetNode="SC_26F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_282F10100"/>
	</edges>
	<edges id="P_30F10100P_31F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10100" targetNode="P_31F10100"/>
	<edges id="P_32F10100P_33F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10100" targetNode="P_33F10100"/>
	<edges id="P_34F10100P_35F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10100" targetNode="P_35F10100"/>
	<edges id="P_36F10100P_37F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10100" targetNode="P_37F10100"/>
	<edges id="P_38F10100P_39F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10100" targetNode="P_39F10100"/>
	<edges xsi:type="cbl:PerformEdge" id="S_358F10100_I" deadCode="false" sourceNode="P_40F10100" targetNode="SC_27F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_358F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_361F10100_I" deadCode="false" sourceNode="P_40F10100" targetNode="SC_3F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_361F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_364F10100_I" deadCode="false" sourceNode="P_40F10100" targetNode="SC_3F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_364F10100"/>
	</edges>
	<edges id="P_40F10100P_41F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10100" targetNode="P_41F10100"/>
	<edges id="P_42F10100P_43F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10100" targetNode="P_43F10100"/>
	<edges id="P_44F10100P_45F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10100" targetNode="P_45F10100"/>
	<edges id="P_46F10100P_47F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10100" targetNode="P_47F10100"/>
	<edges xsi:type="cbl:PerformEdge" id="S_386F10100_I" deadCode="false" sourceNode="P_48F10100" targetNode="SC_24F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_386F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_387F10100_I" deadCode="false" sourceNode="P_48F10100" targetNode="SC_28F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_387F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_393F10100_I" deadCode="false" sourceNode="P_48F10100" targetNode="SC_28F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_393F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_394F10100_I" deadCode="false" sourceNode="P_48F10100" targetNode="SC_3F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_394F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_401F10100_I" deadCode="false" sourceNode="P_48F10100" targetNode="SC_11F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_401F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_405F10100_I" deadCode="false" sourceNode="P_48F10100" targetNode="SC_3F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_405F10100"/>
	</edges>
	<edges id="P_48F10100P_49F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10100" targetNode="P_49F10100"/>
	<edges xsi:type="cbl:PerformEdge" id="S_408F10100_I" deadCode="false" sourceNode="P_50F10100" targetNode="SC_29F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_408F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_410F10100_I" deadCode="false" sourceNode="P_50F10100" targetNode="SC_29F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_410F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_412F10100_I" deadCode="false" sourceNode="P_50F10100" targetNode="SC_30F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_412F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_414F10100_I" deadCode="false" sourceNode="P_50F10100" targetNode="SC_29F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_414F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_416F10100_I" deadCode="false" sourceNode="P_50F10100" targetNode="SC_30F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_416F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_418F10100_I" deadCode="false" sourceNode="P_50F10100" targetNode="SC_29F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_418F10100"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_421F10100_I" deadCode="false" sourceNode="P_50F10100" targetNode="SC_30F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_421F10100"/>
	</edges>
	<edges id="P_50F10100P_51F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10100" targetNode="P_51F10100"/>
	<edges id="P_52F10100P_53F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10100" targetNode="P_53F10100"/>
	<edges xsi:type="cbl:PerformEdge" id="S_435F10100_I" deadCode="false" sourceNode="P_54F10100" targetNode="SC_3F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_435F10100"/>
	</edges>
	<edges id="P_54F10100P_55F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10100" targetNode="P_55F10100"/>
	<edges id="P_56F10100P_57F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10100" targetNode="P_57F10100"/>
	<edges xsi:type="cbl:PerformEdge" id="S_442F10100_I" deadCode="false" sourceNode="P_58F10100" targetNode="SC_3F10100">
		<representations href="../../../cobol/IDSS0020.cbl.cobModel#S_442F10100"/>
	</edges>
	<edges id="P_58F10100P_59F10100" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10100" targetNode="P_59F10100"/>
	<edges xsi:type="cbl:DataEdge" id="S_67F10100_POS1" deadCode="false" targetNode="P_6F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_69F10100_POS1" deadCode="false" targetNode="P_6F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_69F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_71F10100_POS1" deadCode="false" targetNode="P_6F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_71F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_73F10100_POS1" deadCode="false" targetNode="P_6F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_73F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_75F10100_POS1" deadCode="false" targetNode="P_6F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_75F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_77F10100_POS1" deadCode="false" targetNode="P_6F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_77F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_79F10100_POS1" deadCode="false" targetNode="P_6F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_79F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_81F10100_POS1" deadCode="false" targetNode="P_6F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_81F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_83F10100_POS1" deadCode="false" targetNode="P_6F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_83F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_85F10100_POS1" deadCode="false" targetNode="P_6F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_85F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_95F10100_POS1" deadCode="false" targetNode="P_8F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_95F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_98F10100_POS1" deadCode="false" targetNode="P_8F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_98F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_101F10100_POS1" deadCode="false" targetNode="P_8F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_101F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_104F10100_POS1" deadCode="false" targetNode="P_8F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_104F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_107F10100_POS1" deadCode="false" targetNode="P_8F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_107F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_110F10100_POS1" deadCode="false" targetNode="P_8F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_110F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_113F10100_POS1" deadCode="false" targetNode="P_8F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_113F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_116F10100_POS1" deadCode="false" targetNode="P_8F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_116F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_119F10100_POS1" deadCode="false" targetNode="P_8F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_119F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_122F10100_POS1" deadCode="false" targetNode="P_8F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_122F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_126F10100_POS1" deadCode="false" targetNode="P_10F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_126F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_128F10100_POS1" deadCode="false" targetNode="P_10F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_128F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_130F10100_POS1" deadCode="false" targetNode="P_10F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_130F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_132F10100_POS1" deadCode="false" targetNode="P_10F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_132F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_134F10100_POS1" deadCode="false" targetNode="P_10F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_134F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_136F10100_POS1" deadCode="false" targetNode="P_10F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_136F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_138F10100_POS1" deadCode="false" targetNode="P_10F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_138F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_140F10100_POS1" deadCode="false" targetNode="P_10F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_140F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_142F10100_POS1" deadCode="false" targetNode="P_10F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_142F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_144F10100_POS1" deadCode="false" targetNode="P_10F10100" sourceNode="DB2_COMP_STR_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_144F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_381F10100_POS1" deadCode="false" targetNode="P_46F10100" sourceNode="DB2_RIDEF_DATO_STR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_381F10100"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_428F10100_POS1" deadCode="false" targetNode="P_54F10100" sourceNode="DB2_SINONIMO_STR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_428F10100"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_347F10100" deadCode="false" name="Dynamic PGM-NAME-IDSS0020" sourceNode="P_38F10100" targetNode="Dynamic_IDSS0020_PGM-NAME-IDSS0020">
		<representations href="../../../cobol/../importantStmts.cobModel#S_347F10100"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_355F10100" deadCode="false" name="Dynamic PGM-NAME-IDSS0080" sourceNode="P_40F10100" targetNode="Dynamic_IDSS0020_PGM-NAME-IDSS0080">
		<representations href="../../../cobol/../importantStmts.cobModel#S_355F10100"></representations>
	</edges>
</Package>

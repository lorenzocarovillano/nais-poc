<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS3260" cbl:id="LVVS3260" xsi:id="LVVS3260" packageRef="LVVS3260.igd#LVVS3260" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS3260_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10389" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS3260.cbl.cobModel#SC_1F10389"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10389" deadCode="false" name="PROGRAM_LVVS3260_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_1F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10389" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_2F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10389" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_3F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10389" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_4F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10389" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_5F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10389" deadCode="false" name="A000-LEGGI-E06">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_10F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10389" deadCode="false" name="A000-LEGGI-E06-EX">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_11F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10389" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_8F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10389" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_9F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10389" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_6F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10389" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_7F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10389" deadCode="true" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_12F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10389" deadCode="true" name="EX-S0290">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_15F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10389" deadCode="true" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_13F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10389" deadCode="true" name="EX-S0300">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_14F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10389" deadCode="true" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_18F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10389" deadCode="true" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_19F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10389" deadCode="true" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_16F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10389" deadCode="true" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_17F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10389" deadCode="true" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_20F10389"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10389" deadCode="true" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LVVS3260.cbl.cobModel#P_21F10389"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSE060" name="IDBSE060">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10036"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LVVS3260_CALL-PGM" name="Dynamic LVVS3260 CALL-PGM" missing="true">
			<representations href="../../../../missing.xmi#IDNMMRTXKJCHTVEP4VS4LXUAPLUPK4L3KRL2Y1RYBGBJ1EHWDBBDFH"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LVVS3260_IDSI0011-PGM" name="Dynamic LVVS3260 IDSI0011-PGM" missing="true">
			<representations href="../../../../missing.xmi#IDZSNMWFKYTBL1JF20CARLWMROGELAUNZQ2ZJIUNHMRQF3IWCPDSCL"/>
		</children>
	</packageNode>
	<edges id="SC_1F10389P_1F10389" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10389" targetNode="P_1F10389"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10389_I" deadCode="false" sourceNode="P_1F10389" targetNode="P_2F10389">
		<representations href="../../../cobol/LVVS3260.cbl.cobModel#S_1F10389"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10389_O" deadCode="false" sourceNode="P_1F10389" targetNode="P_3F10389">
		<representations href="../../../cobol/LVVS3260.cbl.cobModel#S_1F10389"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10389_I" deadCode="false" sourceNode="P_1F10389" targetNode="P_4F10389">
		<representations href="../../../cobol/LVVS3260.cbl.cobModel#S_2F10389"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10389_O" deadCode="false" sourceNode="P_1F10389" targetNode="P_5F10389">
		<representations href="../../../cobol/LVVS3260.cbl.cobModel#S_2F10389"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10389_I" deadCode="false" sourceNode="P_1F10389" targetNode="P_6F10389">
		<representations href="../../../cobol/LVVS3260.cbl.cobModel#S_3F10389"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10389_O" deadCode="false" sourceNode="P_1F10389" targetNode="P_7F10389">
		<representations href="../../../cobol/LVVS3260.cbl.cobModel#S_3F10389"/>
	</edges>
	<edges id="P_2F10389P_3F10389" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10389" targetNode="P_3F10389"/>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10389_I" deadCode="false" sourceNode="P_4F10389" targetNode="P_8F10389">
		<representations href="../../../cobol/LVVS3260.cbl.cobModel#S_9F10389"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10389_O" deadCode="false" sourceNode="P_4F10389" targetNode="P_9F10389">
		<representations href="../../../cobol/LVVS3260.cbl.cobModel#S_9F10389"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10389_I" deadCode="false" sourceNode="P_4F10389" targetNode="P_10F10389">
		<representations href="../../../cobol/LVVS3260.cbl.cobModel#S_11F10389"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10389_O" deadCode="false" sourceNode="P_4F10389" targetNode="P_11F10389">
		<representations href="../../../cobol/LVVS3260.cbl.cobModel#S_11F10389"/>
	</edges>
	<edges id="P_4F10389P_5F10389" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10389" targetNode="P_5F10389"/>
	<edges id="P_10F10389P_11F10389" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10389" targetNode="P_11F10389"/>
	<edges id="P_8F10389P_9F10389" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10389" targetNode="P_9F10389"/>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10389_I" deadCode="true" sourceNode="P_12F10389" targetNode="P_13F10389">
		<representations href="../../../cobol/LVVS3260.cbl.cobModel#S_47F10389"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10389_O" deadCode="true" sourceNode="P_12F10389" targetNode="P_14F10389">
		<representations href="../../../cobol/LVVS3260.cbl.cobModel#S_47F10389"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10389_I" deadCode="true" sourceNode="P_13F10389" targetNode="P_16F10389">
		<representations href="../../../cobol/LVVS3260.cbl.cobModel#S_54F10389"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10389_O" deadCode="true" sourceNode="P_13F10389" targetNode="P_17F10389">
		<representations href="../../../cobol/LVVS3260.cbl.cobModel#S_54F10389"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10389_I" deadCode="true" sourceNode="P_13F10389" targetNode="P_18F10389">
		<representations href="../../../cobol/LVVS3260.cbl.cobModel#S_58F10389"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10389_O" deadCode="true" sourceNode="P_13F10389" targetNode="P_19F10389">
		<representations href="../../../cobol/LVVS3260.cbl.cobModel#S_58F10389"/>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_22F10389" deadCode="false" name="Dynamic PGM-IDBSE060" sourceNode="P_10F10389" targetNode="IDBSE060">
		<representations href="../../../cobol/../importantStmts.cobModel#S_22F10389"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_52F10389" deadCode="true" name="Dynamic CALL-PGM" sourceNode="P_13F10389" targetNode="Dynamic_LVVS3260_CALL-PGM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_52F10389"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_118F10389" deadCode="true" name="Dynamic IDSI0011-PGM" sourceNode="P_20F10389" targetNode="Dynamic_LVVS3260_IDSI0011-PGM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_118F10389"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS1280" cbl:id="LVVS1280" xsi:id="LVVS1280" packageRef="LVVS1280.igd#LVVS1280" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS1280_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10365" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS1280.cbl.cobModel#SC_1F10365"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10365" deadCode="false" name="PROGRAM_LVVS1280_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_1F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10365" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_2F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10365" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_3F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10365" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_4F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10365" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_5F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10365" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_8F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10365" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_9F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10365" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_10F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10365" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_11F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10365" deadCode="false" name="S1230-RECUP-ID-COMUN">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_12F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10365" deadCode="false" name="S1230-EX">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_13F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10365" deadCode="false" name="RECUP-STAT-MOVI-COMUN">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_16F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10365" deadCode="false" name="RECUP-STAT-MOVI-COMUN-EX">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_17F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10365" deadCode="false" name="S1235-RECUP-MOVI-FINRIO">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_18F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10365" deadCode="false" name="S1235-EX">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_19F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10365" deadCode="false" name="S1250-CALCOLA-QUOTE">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_14F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10365" deadCode="false" name="S1250-EX">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_15F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10365" deadCode="false" name="S1252-CHIUDE-CURVAS">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_22F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10365" deadCode="false" name="S1252-EX">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_23F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10365" deadCode="false" name="S1251-CALCOLA-QUOTE">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_20F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10365" deadCode="false" name="S1251-EX">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_21F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10365" deadCode="false" name="S1253-ARROTONDA-IMP">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_24F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10365" deadCode="false" name="S1253-EX">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_25F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10365" deadCode="false" name="S1300-LEGGI-QUOTZ">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_26F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10365" deadCode="false" name="S1300-EX">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_27F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10365" deadCode="false" name="S1310-LEGGI-QUOTZ-AGG">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_28F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10365" deadCode="false" name="S1310-EX">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_29F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10365" deadCode="true" name="S1263-CHIUDE-CURVAS">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_30F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10365" deadCode="true" name="S1263-EX">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_31F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10365" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_6F10365"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10365" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS1280.cbl.cobModel#P_7F10365"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS1530" name="LDBS1530">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10162"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSSTW0" name="IDBSSTW0">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10091"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS5950" name="LDBS5950">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10226"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS4910" name="LDBS4910">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10214"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSL190" name="IDBSL190">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10045"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSL410" name="IDBSL410">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10049"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LVVS1280_LDBS6000" name="Dynamic LVVS1280 LDBS6000" missing="true">
			<representations href="../../../../missing.xmi#IDHTI5TXUSCBPFLHKCDNFDFWNGT1J5VH1HIAMVIBNQ0ANZJKKQ2OL"/>
		</children>
	</packageNode>
	<edges id="SC_1F10365P_1F10365" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10365" targetNode="P_1F10365"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10365_I" deadCode="false" sourceNode="P_1F10365" targetNode="P_2F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_1F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10365_O" deadCode="false" sourceNode="P_1F10365" targetNode="P_3F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_1F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10365_I" deadCode="false" sourceNode="P_1F10365" targetNode="P_4F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_2F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10365_O" deadCode="false" sourceNode="P_1F10365" targetNode="P_5F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_2F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10365_I" deadCode="false" sourceNode="P_1F10365" targetNode="P_6F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_3F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10365_O" deadCode="false" sourceNode="P_1F10365" targetNode="P_7F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_3F10365"/>
	</edges>
	<edges id="P_2F10365P_3F10365" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10365" targetNode="P_3F10365"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10365_I" deadCode="false" sourceNode="P_4F10365" targetNode="P_8F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_11F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10365_O" deadCode="false" sourceNode="P_4F10365" targetNode="P_9F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_11F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10365_I" deadCode="false" sourceNode="P_4F10365" targetNode="P_10F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_13F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10365_O" deadCode="false" sourceNode="P_4F10365" targetNode="P_11F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_13F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10365_I" deadCode="false" sourceNode="P_4F10365" targetNode="P_12F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_21F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10365_O" deadCode="false" sourceNode="P_4F10365" targetNode="P_13F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_21F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10365_I" deadCode="false" sourceNode="P_4F10365" targetNode="P_14F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_23F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10365_O" deadCode="false" sourceNode="P_4F10365" targetNode="P_15F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_23F10365"/>
	</edges>
	<edges id="P_4F10365P_5F10365" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10365" targetNode="P_5F10365"/>
	<edges id="P_8F10365P_9F10365" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10365" targetNode="P_9F10365"/>
	<edges id="P_10F10365P_11F10365" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10365" targetNode="P_11F10365"/>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10365_I" deadCode="false" sourceNode="P_12F10365" targetNode="P_16F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_83F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10365_O" deadCode="false" sourceNode="P_12F10365" targetNode="P_17F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_83F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10365_I" deadCode="false" sourceNode="P_12F10365" targetNode="P_18F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_85F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10365_O" deadCode="false" sourceNode="P_12F10365" targetNode="P_19F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_85F10365"/>
	</edges>
	<edges id="P_12F10365P_13F10365" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10365" targetNode="P_13F10365"/>
	<edges id="P_16F10365P_17F10365" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10365" targetNode="P_17F10365"/>
	<edges id="P_18F10365P_19F10365" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10365" targetNode="P_19F10365"/>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10365_I" deadCode="false" sourceNode="P_14F10365" targetNode="P_20F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_179F10365"/>
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_189F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10365_O" deadCode="false" sourceNode="P_14F10365" targetNode="P_21F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_179F10365"/>
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_189F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_203F10365_I" deadCode="false" sourceNode="P_14F10365" targetNode="P_22F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_203F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_203F10365_O" deadCode="false" sourceNode="P_14F10365" targetNode="P_23F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_203F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_209F10365_I" deadCode="false" sourceNode="P_14F10365" targetNode="P_24F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_209F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_209F10365_O" deadCode="false" sourceNode="P_14F10365" targetNode="P_25F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_209F10365"/>
	</edges>
	<edges id="P_14F10365P_15F10365" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10365" targetNode="P_15F10365"/>
	<edges id="P_22F10365P_23F10365" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10365" targetNode="P_23F10365"/>
	<edges xsi:type="cbl:PerformEdge" id="S_238F10365_I" deadCode="false" sourceNode="P_20F10365" targetNode="P_26F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_222F10365"/>
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_228F10365"/>
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_238F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_238F10365_O" deadCode="false" sourceNode="P_20F10365" targetNode="P_27F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_222F10365"/>
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_228F10365"/>
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_238F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10365_I" deadCode="false" sourceNode="P_20F10365" targetNode="P_28F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_222F10365"/>
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_228F10365"/>
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_242F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10365_O" deadCode="false" sourceNode="P_20F10365" targetNode="P_29F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_222F10365"/>
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_228F10365"/>
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_242F10365"/>
	</edges>
	<edges id="P_20F10365P_21F10365" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10365" targetNode="P_21F10365"/>
	<edges id="P_24F10365P_25F10365" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10365" targetNode="P_25F10365"/>
	<edges id="P_26F10365P_27F10365" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10365" targetNode="P_27F10365"/>
	<edges xsi:type="cbl:PerformEdge" id="S_303F10365_I" deadCode="false" sourceNode="P_28F10365" targetNode="P_26F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_303F10365"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_303F10365_O" deadCode="false" sourceNode="P_28F10365" targetNode="P_27F10365">
		<representations href="../../../cobol/LVVS1280.cbl.cobModel#S_303F10365"/>
	</edges>
	<edges id="P_28F10365P_29F10365" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10365" targetNode="P_29F10365"/>
	<edges xsi:type="cbl:CallEdge" id="S_78F10365" deadCode="false" name="Dynamic LDBS1530" sourceNode="P_12F10365" targetNode="LDBS1530">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10365"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_108F10365" deadCode="false" name="Dynamic IDBSSTW0" sourceNode="P_16F10365" targetNode="IDBSSTW0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_108F10365"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_153F10365" deadCode="false" name="Dynamic LDBS5950" sourceNode="P_18F10365" targetNode="LDBS5950">
		<representations href="../../../cobol/../importantStmts.cobModel#S_153F10365"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_180F10365" deadCode="false" name="Dynamic LDBS4910" sourceNode="P_14F10365" targetNode="LDBS4910">
		<representations href="../../../cobol/../importantStmts.cobModel#S_180F10365"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_212F10365" deadCode="false" name="Dynamic LDBS4910" sourceNode="P_22F10365" targetNode="LDBS4910">
		<representations href="../../../cobol/../importantStmts.cobModel#S_212F10365"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_274F10365" deadCode="false" name="Dynamic IDBSL190" sourceNode="P_26F10365" targetNode="IDBSL190">
		<representations href="../../../cobol/../importantStmts.cobModel#S_274F10365"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_297F10365" deadCode="false" name="Dynamic IDBSL410" sourceNode="P_28F10365" targetNode="IDBSL410">
		<representations href="../../../cobol/../importantStmts.cobModel#S_297F10365"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_309F10365" deadCode="true" name="Dynamic LDBS6000" sourceNode="P_30F10365" targetNode="Dynamic_LVVS1280_LDBS6000">
		<representations href="../../../cobol/../importantStmts.cobModel#S_309F10365"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBSF220" cbl:id="LDBSF220" xsi:id="LDBSF220" packageRef="LDBSF220.igd#LDBSF220" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBSF220_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10266" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBSF220.cbl.cobModel#SC_1F10266"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10266" deadCode="false" name="PROGRAM_LDBSF220_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_1F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10266" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_2F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10266" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_3F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10266" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_12F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10266" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_13F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10266" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_4F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10266" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_5F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10266" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_6F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10266" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_7F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10266" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_8F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10266" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_9F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10266" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_44F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10266" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_49F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10266" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_14F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10266" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_15F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10266" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_16F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10266" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_17F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10266" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_18F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10266" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_19F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10266" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_20F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10266" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_21F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10266" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_22F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10266" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_23F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10266" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_56F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10266" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_57F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10266" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_24F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10266" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_25F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10266" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_26F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10266" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_27F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10266" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_28F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10266" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_29F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10266" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_30F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10266" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_31F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10266" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_32F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10266" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_33F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10266" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_58F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10266" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_59F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10266" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_34F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10266" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_35F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10266" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_36F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10266" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_37F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10266" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_38F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10266" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_39F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10266" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_40F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10266" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_41F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10266" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_42F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10266" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_43F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10266" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_50F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10266" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_51F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10266" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_60F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10266" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_63F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10266" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_52F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10266" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_53F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10266" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_45F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10266" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_46F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10266" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_47F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10266" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_48F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10266" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_54F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10266" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_55F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10266" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_10F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10266" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_11F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10266" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_66F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10266" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_67F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10266" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_68F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10266" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_69F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10266" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_61F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10266" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_62F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10266" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_70F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10266" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_71F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10266" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_64F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10266" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_65F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10266" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_72F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10266" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_73F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10266" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_74F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10266" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_75F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10266" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_76F10266"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10266" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBSF220.cbl.cobModel#P_77F10266"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_DETT_TIT_CONT" name="DETT_TIT_CONT">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_DETT_TIT_CONT"/>
		</children>
	</packageNode>
	<edges id="SC_1F10266P_1F10266" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10266" targetNode="P_1F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10266_I" deadCode="false" sourceNode="P_1F10266" targetNode="P_2F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_1F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10266_O" deadCode="false" sourceNode="P_1F10266" targetNode="P_3F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_1F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10266_I" deadCode="false" sourceNode="P_1F10266" targetNode="P_4F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_5F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10266_O" deadCode="false" sourceNode="P_1F10266" targetNode="P_5F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_5F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10266_I" deadCode="false" sourceNode="P_1F10266" targetNode="P_6F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_9F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10266_O" deadCode="false" sourceNode="P_1F10266" targetNode="P_7F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_9F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10266_I" deadCode="false" sourceNode="P_1F10266" targetNode="P_8F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_13F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10266_O" deadCode="false" sourceNode="P_1F10266" targetNode="P_9F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_13F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10266_I" deadCode="false" sourceNode="P_2F10266" targetNode="P_10F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_22F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10266_O" deadCode="false" sourceNode="P_2F10266" targetNode="P_11F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_22F10266"/>
	</edges>
	<edges id="P_2F10266P_3F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10266" targetNode="P_3F10266"/>
	<edges id="P_12F10266P_13F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10266" targetNode="P_13F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10266_I" deadCode="false" sourceNode="P_4F10266" targetNode="P_14F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_35F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10266_O" deadCode="false" sourceNode="P_4F10266" targetNode="P_15F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_35F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10266_I" deadCode="false" sourceNode="P_4F10266" targetNode="P_16F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_36F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10266_O" deadCode="false" sourceNode="P_4F10266" targetNode="P_17F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_36F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10266_I" deadCode="false" sourceNode="P_4F10266" targetNode="P_18F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_37F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10266_O" deadCode="false" sourceNode="P_4F10266" targetNode="P_19F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_37F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10266_I" deadCode="false" sourceNode="P_4F10266" targetNode="P_20F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_38F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10266_O" deadCode="false" sourceNode="P_4F10266" targetNode="P_21F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_38F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10266_I" deadCode="false" sourceNode="P_4F10266" targetNode="P_22F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_39F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10266_O" deadCode="false" sourceNode="P_4F10266" targetNode="P_23F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_39F10266"/>
	</edges>
	<edges id="P_4F10266P_5F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10266" targetNode="P_5F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10266_I" deadCode="false" sourceNode="P_6F10266" targetNode="P_24F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_43F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10266_O" deadCode="false" sourceNode="P_6F10266" targetNode="P_25F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_43F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10266_I" deadCode="false" sourceNode="P_6F10266" targetNode="P_26F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_44F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10266_O" deadCode="false" sourceNode="P_6F10266" targetNode="P_27F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_44F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10266_I" deadCode="false" sourceNode="P_6F10266" targetNode="P_28F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_45F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10266_O" deadCode="false" sourceNode="P_6F10266" targetNode="P_29F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_45F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10266_I" deadCode="false" sourceNode="P_6F10266" targetNode="P_30F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_46F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10266_O" deadCode="false" sourceNode="P_6F10266" targetNode="P_31F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_46F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10266_I" deadCode="false" sourceNode="P_6F10266" targetNode="P_32F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_47F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10266_O" deadCode="false" sourceNode="P_6F10266" targetNode="P_33F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_47F10266"/>
	</edges>
	<edges id="P_6F10266P_7F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10266" targetNode="P_7F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10266_I" deadCode="false" sourceNode="P_8F10266" targetNode="P_34F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_51F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10266_O" deadCode="false" sourceNode="P_8F10266" targetNode="P_35F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_51F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10266_I" deadCode="false" sourceNode="P_8F10266" targetNode="P_36F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_52F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10266_O" deadCode="false" sourceNode="P_8F10266" targetNode="P_37F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_52F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10266_I" deadCode="false" sourceNode="P_8F10266" targetNode="P_38F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_53F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10266_O" deadCode="false" sourceNode="P_8F10266" targetNode="P_39F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_53F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10266_I" deadCode="false" sourceNode="P_8F10266" targetNode="P_40F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_54F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10266_O" deadCode="false" sourceNode="P_8F10266" targetNode="P_41F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_54F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10266_I" deadCode="false" sourceNode="P_8F10266" targetNode="P_42F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_55F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10266_O" deadCode="false" sourceNode="P_8F10266" targetNode="P_43F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_55F10266"/>
	</edges>
	<edges id="P_8F10266P_9F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10266" targetNode="P_9F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10266_I" deadCode="false" sourceNode="P_44F10266" targetNode="P_45F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_58F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10266_O" deadCode="false" sourceNode="P_44F10266" targetNode="P_46F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_58F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10266_I" deadCode="false" sourceNode="P_44F10266" targetNode="P_47F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_59F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10266_O" deadCode="false" sourceNode="P_44F10266" targetNode="P_48F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_59F10266"/>
	</edges>
	<edges id="P_44F10266P_49F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10266" targetNode="P_49F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10266_I" deadCode="false" sourceNode="P_14F10266" targetNode="P_45F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_63F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10266_O" deadCode="false" sourceNode="P_14F10266" targetNode="P_46F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_63F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10266_I" deadCode="false" sourceNode="P_14F10266" targetNode="P_47F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_64F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10266_O" deadCode="false" sourceNode="P_14F10266" targetNode="P_48F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_64F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10266_I" deadCode="false" sourceNode="P_14F10266" targetNode="P_12F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_66F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10266_O" deadCode="false" sourceNode="P_14F10266" targetNode="P_13F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_66F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10266_I" deadCode="false" sourceNode="P_14F10266" targetNode="P_50F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_68F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10266_O" deadCode="false" sourceNode="P_14F10266" targetNode="P_51F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_68F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10266_I" deadCode="false" sourceNode="P_14F10266" targetNode="P_52F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_69F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10266_O" deadCode="false" sourceNode="P_14F10266" targetNode="P_53F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_69F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10266_I" deadCode="false" sourceNode="P_14F10266" targetNode="P_54F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_70F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10266_O" deadCode="false" sourceNode="P_14F10266" targetNode="P_55F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_70F10266"/>
	</edges>
	<edges id="P_14F10266P_15F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10266" targetNode="P_15F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10266_I" deadCode="false" sourceNode="P_16F10266" targetNode="P_44F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_72F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10266_O" deadCode="false" sourceNode="P_16F10266" targetNode="P_49F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_72F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10266_I" deadCode="false" sourceNode="P_16F10266" targetNode="P_12F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_74F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10266_O" deadCode="false" sourceNode="P_16F10266" targetNode="P_13F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_74F10266"/>
	</edges>
	<edges id="P_16F10266P_17F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10266" targetNode="P_17F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10266_I" deadCode="false" sourceNode="P_18F10266" targetNode="P_12F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_77F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10266_O" deadCode="false" sourceNode="P_18F10266" targetNode="P_13F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_77F10266"/>
	</edges>
	<edges id="P_18F10266P_19F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10266" targetNode="P_19F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10266_I" deadCode="false" sourceNode="P_20F10266" targetNode="P_16F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_79F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10266_O" deadCode="false" sourceNode="P_20F10266" targetNode="P_17F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_79F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10266_I" deadCode="false" sourceNode="P_20F10266" targetNode="P_22F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_81F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10266_O" deadCode="false" sourceNode="P_20F10266" targetNode="P_23F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_81F10266"/>
	</edges>
	<edges id="P_20F10266P_21F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10266" targetNode="P_21F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10266_I" deadCode="false" sourceNode="P_22F10266" targetNode="P_12F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_84F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10266_O" deadCode="false" sourceNode="P_22F10266" targetNode="P_13F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_84F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10266_I" deadCode="false" sourceNode="P_22F10266" targetNode="P_50F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_86F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10266_O" deadCode="false" sourceNode="P_22F10266" targetNode="P_51F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_86F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10266_I" deadCode="false" sourceNode="P_22F10266" targetNode="P_52F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_87F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10266_O" deadCode="false" sourceNode="P_22F10266" targetNode="P_53F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_87F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10266_I" deadCode="false" sourceNode="P_22F10266" targetNode="P_54F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_88F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10266_O" deadCode="false" sourceNode="P_22F10266" targetNode="P_55F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_88F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10266_I" deadCode="false" sourceNode="P_22F10266" targetNode="P_18F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_90F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10266_O" deadCode="false" sourceNode="P_22F10266" targetNode="P_19F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_90F10266"/>
	</edges>
	<edges id="P_22F10266P_23F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10266" targetNode="P_23F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10266_I" deadCode="false" sourceNode="P_56F10266" targetNode="P_45F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_94F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10266_O" deadCode="false" sourceNode="P_56F10266" targetNode="P_46F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_94F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10266_I" deadCode="false" sourceNode="P_56F10266" targetNode="P_47F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_95F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10266_O" deadCode="false" sourceNode="P_56F10266" targetNode="P_48F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_95F10266"/>
	</edges>
	<edges id="P_56F10266P_57F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10266" targetNode="P_57F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10266_I" deadCode="false" sourceNode="P_24F10266" targetNode="P_45F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_99F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10266_O" deadCode="false" sourceNode="P_24F10266" targetNode="P_46F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_99F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10266_I" deadCode="false" sourceNode="P_24F10266" targetNode="P_47F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_100F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10266_O" deadCode="false" sourceNode="P_24F10266" targetNode="P_48F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_100F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10266_I" deadCode="false" sourceNode="P_24F10266" targetNode="P_12F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_102F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10266_O" deadCode="false" sourceNode="P_24F10266" targetNode="P_13F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_102F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10266_I" deadCode="false" sourceNode="P_24F10266" targetNode="P_50F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_104F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10266_O" deadCode="false" sourceNode="P_24F10266" targetNode="P_51F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_104F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10266_I" deadCode="false" sourceNode="P_24F10266" targetNode="P_52F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_105F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10266_O" deadCode="false" sourceNode="P_24F10266" targetNode="P_53F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_105F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10266_I" deadCode="false" sourceNode="P_24F10266" targetNode="P_54F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_106F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10266_O" deadCode="false" sourceNode="P_24F10266" targetNode="P_55F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_106F10266"/>
	</edges>
	<edges id="P_24F10266P_25F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10266" targetNode="P_25F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10266_I" deadCode="false" sourceNode="P_26F10266" targetNode="P_56F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_108F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10266_O" deadCode="false" sourceNode="P_26F10266" targetNode="P_57F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_108F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10266_I" deadCode="false" sourceNode="P_26F10266" targetNode="P_12F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_110F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10266_O" deadCode="false" sourceNode="P_26F10266" targetNode="P_13F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_110F10266"/>
	</edges>
	<edges id="P_26F10266P_27F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10266" targetNode="P_27F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10266_I" deadCode="false" sourceNode="P_28F10266" targetNode="P_12F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_113F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10266_O" deadCode="false" sourceNode="P_28F10266" targetNode="P_13F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_113F10266"/>
	</edges>
	<edges id="P_28F10266P_29F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10266" targetNode="P_29F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10266_I" deadCode="false" sourceNode="P_30F10266" targetNode="P_26F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_115F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10266_O" deadCode="false" sourceNode="P_30F10266" targetNode="P_27F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_115F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10266_I" deadCode="false" sourceNode="P_30F10266" targetNode="P_32F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_117F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10266_O" deadCode="false" sourceNode="P_30F10266" targetNode="P_33F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_117F10266"/>
	</edges>
	<edges id="P_30F10266P_31F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10266" targetNode="P_31F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10266_I" deadCode="false" sourceNode="P_32F10266" targetNode="P_12F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_120F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10266_O" deadCode="false" sourceNode="P_32F10266" targetNode="P_13F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_120F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10266_I" deadCode="false" sourceNode="P_32F10266" targetNode="P_50F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_122F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10266_O" deadCode="false" sourceNode="P_32F10266" targetNode="P_51F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_122F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10266_I" deadCode="false" sourceNode="P_32F10266" targetNode="P_52F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_123F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10266_O" deadCode="false" sourceNode="P_32F10266" targetNode="P_53F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_123F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10266_I" deadCode="false" sourceNode="P_32F10266" targetNode="P_54F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_124F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10266_O" deadCode="false" sourceNode="P_32F10266" targetNode="P_55F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_124F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10266_I" deadCode="false" sourceNode="P_32F10266" targetNode="P_28F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_126F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10266_O" deadCode="false" sourceNode="P_32F10266" targetNode="P_29F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_126F10266"/>
	</edges>
	<edges id="P_32F10266P_33F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10266" targetNode="P_33F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10266_I" deadCode="false" sourceNode="P_58F10266" targetNode="P_45F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_130F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10266_O" deadCode="false" sourceNode="P_58F10266" targetNode="P_46F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_130F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10266_I" deadCode="false" sourceNode="P_58F10266" targetNode="P_47F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_131F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10266_O" deadCode="false" sourceNode="P_58F10266" targetNode="P_48F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_131F10266"/>
	</edges>
	<edges id="P_58F10266P_59F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10266" targetNode="P_59F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_134F10266_I" deadCode="false" sourceNode="P_34F10266" targetNode="P_45F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_134F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_134F10266_O" deadCode="false" sourceNode="P_34F10266" targetNode="P_46F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_134F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10266_I" deadCode="false" sourceNode="P_34F10266" targetNode="P_47F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_135F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10266_O" deadCode="false" sourceNode="P_34F10266" targetNode="P_48F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_135F10266"/>
	</edges>
	<edges id="P_34F10266P_35F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10266" targetNode="P_35F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10266_I" deadCode="false" sourceNode="P_36F10266" targetNode="P_58F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_138F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10266_O" deadCode="false" sourceNode="P_36F10266" targetNode="P_59F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_138F10266"/>
	</edges>
	<edges id="P_36F10266P_37F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10266" targetNode="P_37F10266"/>
	<edges id="P_38F10266P_39F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10266" targetNode="P_39F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10266_I" deadCode="false" sourceNode="P_40F10266" targetNode="P_36F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_143F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10266_O" deadCode="false" sourceNode="P_40F10266" targetNode="P_37F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_143F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10266_I" deadCode="false" sourceNode="P_40F10266" targetNode="P_42F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_145F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10266_O" deadCode="false" sourceNode="P_40F10266" targetNode="P_43F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_145F10266"/>
	</edges>
	<edges id="P_40F10266P_41F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10266" targetNode="P_41F10266"/>
	<edges id="P_42F10266P_43F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10266" targetNode="P_43F10266"/>
	<edges id="P_50F10266P_51F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10266" targetNode="P_51F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_250F10266_I" deadCode="true" sourceNode="P_60F10266" targetNode="P_61F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_250F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_250F10266_O" deadCode="true" sourceNode="P_60F10266" targetNode="P_62F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_250F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10266_I" deadCode="true" sourceNode="P_60F10266" targetNode="P_61F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_253F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10266_O" deadCode="true" sourceNode="P_60F10266" targetNode="P_62F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_253F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_257F10266_I" deadCode="true" sourceNode="P_60F10266" targetNode="P_61F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_257F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_257F10266_O" deadCode="true" sourceNode="P_60F10266" targetNode="P_62F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_257F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_261F10266_I" deadCode="true" sourceNode="P_60F10266" targetNode="P_61F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_261F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_261F10266_O" deadCode="true" sourceNode="P_60F10266" targetNode="P_62F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_261F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10266_I" deadCode="true" sourceNode="P_60F10266" targetNode="P_61F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_265F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10266_O" deadCode="true" sourceNode="P_60F10266" targetNode="P_62F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_265F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_269F10266_I" deadCode="false" sourceNode="P_52F10266" targetNode="P_64F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_269F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_269F10266_O" deadCode="false" sourceNode="P_52F10266" targetNode="P_65F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_269F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_272F10266_I" deadCode="false" sourceNode="P_52F10266" targetNode="P_64F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_272F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_272F10266_O" deadCode="false" sourceNode="P_52F10266" targetNode="P_65F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_272F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10266_I" deadCode="false" sourceNode="P_52F10266" targetNode="P_64F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_276F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10266_O" deadCode="false" sourceNode="P_52F10266" targetNode="P_65F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_276F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10266_I" deadCode="false" sourceNode="P_52F10266" targetNode="P_64F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_280F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10266_O" deadCode="false" sourceNode="P_52F10266" targetNode="P_65F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_280F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10266_I" deadCode="false" sourceNode="P_52F10266" targetNode="P_64F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_284F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10266_O" deadCode="false" sourceNode="P_52F10266" targetNode="P_65F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_284F10266"/>
	</edges>
	<edges id="P_52F10266P_53F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10266" targetNode="P_53F10266"/>
	<edges id="P_45F10266P_46F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10266" targetNode="P_46F10266"/>
	<edges id="P_47F10266P_48F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10266" targetNode="P_48F10266"/>
	<edges id="P_54F10266P_55F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10266" targetNode="P_55F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_293F10266_I" deadCode="false" sourceNode="P_10F10266" targetNode="P_66F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_293F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_293F10266_O" deadCode="false" sourceNode="P_10F10266" targetNode="P_67F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_293F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_295F10266_I" deadCode="false" sourceNode="P_10F10266" targetNode="P_68F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_295F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_295F10266_O" deadCode="false" sourceNode="P_10F10266" targetNode="P_69F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_295F10266"/>
	</edges>
	<edges id="P_10F10266P_11F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10266" targetNode="P_11F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_300F10266_I" deadCode="true" sourceNode="P_66F10266" targetNode="P_61F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_300F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_300F10266_O" deadCode="true" sourceNode="P_66F10266" targetNode="P_62F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_300F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_305F10266_I" deadCode="true" sourceNode="P_66F10266" targetNode="P_61F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_305F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_305F10266_O" deadCode="true" sourceNode="P_66F10266" targetNode="P_62F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_305F10266"/>
	</edges>
	<edges id="P_66F10266P_67F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10266" targetNode="P_67F10266"/>
	<edges id="P_68F10266P_69F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10266" targetNode="P_69F10266"/>
	<edges id="P_61F10266P_62F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10266" targetNode="P_62F10266"/>
	<edges xsi:type="cbl:PerformEdge" id="S_334F10266_I" deadCode="false" sourceNode="P_64F10266" targetNode="P_72F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_334F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_334F10266_O" deadCode="false" sourceNode="P_64F10266" targetNode="P_73F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_334F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_335F10266_I" deadCode="false" sourceNode="P_64F10266" targetNode="P_74F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_335F10266"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_335F10266_O" deadCode="false" sourceNode="P_64F10266" targetNode="P_75F10266">
		<representations href="../../../cobol/LDBSF220.cbl.cobModel#S_335F10266"/>
	</edges>
	<edges id="P_64F10266P_65F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10266" targetNode="P_65F10266"/>
	<edges id="P_72F10266P_73F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10266" targetNode="P_73F10266"/>
	<edges id="P_74F10266P_75F10266" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10266" targetNode="P_75F10266"/>
	<edges xsi:type="cbl:DataEdge" id="S_65F10266_POS1" deadCode="false" targetNode="P_14F10266" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_65F10266"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_73F10266_POS1" deadCode="false" targetNode="P_16F10266" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_73F10266"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_76F10266_POS1" deadCode="false" targetNode="P_18F10266" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_76F10266"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_83F10266_POS1" deadCode="false" targetNode="P_22F10266" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_83F10266"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_101F10266_POS1" deadCode="false" targetNode="P_24F10266" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_101F10266"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_109F10266_POS1" deadCode="false" targetNode="P_26F10266" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_109F10266"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_112F10266_POS1" deadCode="false" targetNode="P_28F10266" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_112F10266"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_119F10266_POS1" deadCode="false" targetNode="P_32F10266" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_119F10266"></representations>
	</edges>
</Package>

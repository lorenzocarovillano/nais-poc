<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0560" cbl:id="LVVS0560" xsi:id="LVVS0560" packageRef="LVVS0560.igd#LVVS0560" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0560_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10358" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0560.cbl.cobModel#SC_1F10358"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10358" deadCode="false" name="PROGRAM_LVVS0560_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_1F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10358" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_2F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10358" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_3F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10358" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_4F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10358" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_5F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10358" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_8F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10358" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_9F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10358" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_10F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10358" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_11F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10358" deadCode="false" name="S1230-RECUP-ID-COMUN">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_12F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10358" deadCode="false" name="S1230-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_13F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10358" deadCode="false" name="S1235-RECUP-MOVI-FINRIO">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_20F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10358" deadCode="false" name="S1235-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_21F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10358" deadCode="false" name="S1250-CALCOLA-QUOTE">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_14F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10358" deadCode="false" name="S1250-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_15F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10358" deadCode="false" name="S1251-CALCOLA-QUOTE">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_22F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10358" deadCode="false" name="S1251-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_23F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10358" deadCode="false" name="S1252-RECUP-IMP-INVES">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_26F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10358" deadCode="false" name="S1252-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_27F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10358" deadCode="false" name="S1253-RECUP-IMP-DISIN">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_28F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10358" deadCode="false" name="S1253-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_29F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10358" deadCode="false" name="S1254-VALORIZZA-LISTA">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_24F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10358" deadCode="false" name="S1254-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_25F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10358" deadCode="true" name="S1260-SOTTRAI-ANNULLO">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_34F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10358" deadCode="true" name="S1260-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_37F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10358" deadCode="true" name="S1261-SOTTRAI-QUOTE">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_35F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10358" deadCode="true" name="S1261-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_36F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10358" deadCode="true" name="S1262-CALCOLA-QUOTE">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_38F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10358" deadCode="true" name="S1262-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_39F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10358" deadCode="false" name="S1300-VALOR-VARLIST">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_32F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10358" deadCode="false" name="S1300-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_33F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10358" deadCode="false" name="S1310-VALOR-LISTA-IMP">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_30F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10358" deadCode="false" name="S1310-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_31F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10358" deadCode="true" name="S1320-SOTTR-ANNULLO-LISTA">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_40F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10358" deadCode="true" name="S1320-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_41F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10358" deadCode="false" name="S1350-VAR-POSITIVA">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_16F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10358" deadCode="false" name="S1350-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_17F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10358" deadCode="false" name="S1400-VALORIZZA-VARIABILE">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_18F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10358" deadCode="false" name="S1400-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_19F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10358" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_6F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10358" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_7F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10358" deadCode="false" name="STRINGA-X-VALORIZZATORE">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_42F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10358" deadCode="false" name="STRINGA-X-VALORIZZATORE-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_43F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10358" deadCode="false" name="CONVERTI-FORMATI">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_44F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10358" deadCode="false" name="CONVERTI-FORMATI-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_45F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10358" deadCode="false" name="CALL-STRINGATURA">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_46F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10358" deadCode="false" name="CALL-STRINGATURA-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_47F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10358" deadCode="false" name="INITIALIZE-CAMPI">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_48F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10358" deadCode="false" name="INITIALIZE-CAMPI-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_49F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10358" deadCode="false" name="ELABORA-DATI">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_50F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10358" deadCode="false" name="ELABORA-DATI-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_51F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10358" deadCode="false" name="TRATTA-IMPORTO">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_54F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10358" deadCode="false" name="TRATTA-IMPORTO-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_55F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10358" deadCode="false" name="TRATTA-NUMERICO">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_56F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10358" deadCode="false" name="TRATTA-NUMERICO-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_57F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10358" deadCode="false" name="TRATTA-PERCENTUALE">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_58F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10358" deadCode="false" name="TRATTA-PERCENTUALE-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_59F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10358" deadCode="false" name="TRATTA-STRINGA">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_60F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10358" deadCode="false" name="TRATTA-STRINGA-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_61F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10358" deadCode="false" name="ESTRAI-ALFANUMERICO">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_70F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10358" deadCode="false" name="ESTRAI-ALFANUMERICO-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_71F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10358" deadCode="false" name="ESTRAI-INTERO">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_62F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10358" deadCode="false" name="ESTRAI-INTERO-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_63F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10358" deadCode="false" name="ESTRAI-DECIMALI">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_64F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10358" deadCode="false" name="ESTRAI-DECIMALI-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_65F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10358" deadCode="false" name="TRATTA-SEGNO">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_68F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10358" deadCode="false" name="TRATTA-SEGNO-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_69F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10358" deadCode="false" name="STRINGA-CAMPO">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_66F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10358" deadCode="false" name="STRINGA-CAMPO-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_67F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10358" deadCode="false" name="CALCOLA-SPAZIO">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_72F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10358" deadCode="false" name="CALCOLA-SPAZIO-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_73F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10358" deadCode="false" name="INDIVIDUA-IND-DEC">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_74F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10358" deadCode="false" name="INDIVIDUA-IND-DEC-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_75F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10358" deadCode="false" name="LISTA-OMOGENEA">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_52F10358"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10358" deadCode="false" name="LISTA-OMOGENEA-EX">
				<representations href="../../../cobol/LVVS0560.cbl.cobModel#P_53F10358"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS1530" name="LDBS1530">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10162"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS5950" name="LDBS5950">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10226"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS4910" name="LDBS4910">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10214"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSRIF0" name="IDBSRIF0">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10085"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSRDF0" name="IDBSRDF0">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10083"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LVVS0560_LDBS5990" name="Dynamic LVVS0560 LDBS5990" missing="true">
			<representations href="../../../../missing.xmi#IDGTNJWW4APQ3JJTTPEUD3XL2ROHO4EQQ10JGTVLKIPA3QL4D0W2MP"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LVVS0560_LDBS6000" name="Dynamic LVVS0560 LDBS6000" missing="true">
			<representations href="../../../../missing.xmi#IDZKPGOYDFMRI5HVFTF0BZJ0XRRG4W5ARDGZHMMXCA1QRJDMNOBLHG"/>
		</children>
	</packageNode>
	<edges id="SC_1F10358P_1F10358" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10358" targetNode="P_1F10358"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10358_I" deadCode="false" sourceNode="P_1F10358" targetNode="P_2F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_1F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10358_O" deadCode="false" sourceNode="P_1F10358" targetNode="P_3F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_1F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10358_I" deadCode="false" sourceNode="P_1F10358" targetNode="P_4F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_2F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10358_O" deadCode="false" sourceNode="P_1F10358" targetNode="P_5F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_2F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10358_I" deadCode="false" sourceNode="P_1F10358" targetNode="P_6F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_3F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10358_O" deadCode="false" sourceNode="P_1F10358" targetNode="P_7F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_3F10358"/>
	</edges>
	<edges id="P_2F10358P_3F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10358" targetNode="P_3F10358"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10358_I" deadCode="false" sourceNode="P_4F10358" targetNode="P_8F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_11F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10358_O" deadCode="false" sourceNode="P_4F10358" targetNode="P_9F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_11F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10358_I" deadCode="false" sourceNode="P_4F10358" targetNode="P_10F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_13F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10358_O" deadCode="false" sourceNode="P_4F10358" targetNode="P_11F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_13F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10358_I" deadCode="false" sourceNode="P_4F10358" targetNode="P_12F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_19F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10358_O" deadCode="false" sourceNode="P_4F10358" targetNode="P_13F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_19F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10358_I" deadCode="false" sourceNode="P_4F10358" targetNode="P_14F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_20F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10358_O" deadCode="false" sourceNode="P_4F10358" targetNode="P_15F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_20F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10358_I" deadCode="false" sourceNode="P_4F10358" targetNode="P_14F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_28F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10358_O" deadCode="false" sourceNode="P_4F10358" targetNode="P_15F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_28F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10358_I" deadCode="false" sourceNode="P_4F10358" targetNode="P_16F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_30F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10358_O" deadCode="false" sourceNode="P_4F10358" targetNode="P_17F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_30F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10358_I" deadCode="false" sourceNode="P_4F10358" targetNode="P_18F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_32F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10358_O" deadCode="false" sourceNode="P_4F10358" targetNode="P_19F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_32F10358"/>
	</edges>
	<edges id="P_4F10358P_5F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10358" targetNode="P_5F10358"/>
	<edges id="P_8F10358P_9F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10358" targetNode="P_9F10358"/>
	<edges id="P_10F10358P_11F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10358" targetNode="P_11F10358"/>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10358_I" deadCode="false" sourceNode="P_12F10358" targetNode="P_20F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_87F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10358_O" deadCode="false" sourceNode="P_12F10358" targetNode="P_21F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_87F10358"/>
	</edges>
	<edges id="P_12F10358P_13F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10358" targetNode="P_13F10358"/>
	<edges id="P_20F10358P_21F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10358" targetNode="P_21F10358"/>
	<edges xsi:type="cbl:PerformEdge" id="S_141F10358_I" deadCode="false" sourceNode="P_14F10358" targetNode="P_22F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_131F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_141F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_141F10358_O" deadCode="false" sourceNode="P_14F10358" targetNode="P_23F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_131F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_141F10358"/>
	</edges>
	<edges id="P_14F10358P_15F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10358" targetNode="P_15F10358"/>
	<edges xsi:type="cbl:PerformEdge" id="S_159F10358_I" deadCode="false" sourceNode="P_22F10358" targetNode="P_24F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_149F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_159F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_159F10358_O" deadCode="false" sourceNode="P_22F10358" targetNode="P_25F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_149F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_159F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10358_I" deadCode="false" sourceNode="P_22F10358" targetNode="P_26F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_149F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_161F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10358_O" deadCode="false" sourceNode="P_22F10358" targetNode="P_27F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_149F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_161F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10358_I" deadCode="false" sourceNode="P_22F10358" targetNode="P_28F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_149F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_163F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10358_O" deadCode="false" sourceNode="P_22F10358" targetNode="P_29F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_149F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_163F10358"/>
	</edges>
	<edges id="P_22F10358P_23F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10358" targetNode="P_23F10358"/>
	<edges xsi:type="cbl:PerformEdge" id="S_180F10358_I" deadCode="false" sourceNode="P_26F10358" targetNode="P_24F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_180F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_180F10358_O" deadCode="false" sourceNode="P_26F10358" targetNode="P_25F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_180F10358"/>
	</edges>
	<edges id="P_26F10358P_27F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10358" targetNode="P_27F10358"/>
	<edges xsi:type="cbl:PerformEdge" id="S_200F10358_I" deadCode="false" sourceNode="P_28F10358" targetNode="P_24F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_200F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_200F10358_O" deadCode="false" sourceNode="P_28F10358" targetNode="P_25F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_200F10358"/>
	</edges>
	<edges id="P_28F10358P_29F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10358" targetNode="P_29F10358"/>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10358_I" deadCode="false" sourceNode="P_24F10358" targetNode="P_30F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_210F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_216F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10358_O" deadCode="false" sourceNode="P_24F10358" targetNode="P_31F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_210F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_216F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_218F10358_I" deadCode="false" sourceNode="P_24F10358" targetNode="P_32F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_218F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_218F10358_O" deadCode="false" sourceNode="P_24F10358" targetNode="P_33F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_218F10358"/>
	</edges>
	<edges id="P_24F10358P_25F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10358" targetNode="P_25F10358"/>
	<edges xsi:type="cbl:PerformEdge" id="S_239F10358_I" deadCode="true" sourceNode="P_34F10358" targetNode="P_35F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_239F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_239F10358_O" deadCode="true" sourceNode="P_34F10358" targetNode="P_36F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_239F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_261F10358_I" deadCode="true" sourceNode="P_35F10358" targetNode="P_38F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_254F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_261F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_261F10358_O" deadCode="true" sourceNode="P_35F10358" targetNode="P_39F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_254F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_261F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10358_I" deadCode="true" sourceNode="P_38F10358" targetNode="P_40F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_268F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_277F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_283F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10358_O" deadCode="true" sourceNode="P_38F10358" targetNode="P_41F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_268F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_277F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_283F10358"/>
	</edges>
	<edges id="P_32F10358P_33F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10358" targetNode="P_33F10358"/>
	<edges id="P_30F10358P_31F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10358" targetNode="P_31F10358"/>
	<edges id="P_16F10358P_17F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10358" targetNode="P_17F10358"/>
	<edges xsi:type="cbl:PerformEdge" id="S_320F10358_I" deadCode="false" sourceNode="P_18F10358" targetNode="P_42F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_320F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_320F10358_O" deadCode="false" sourceNode="P_18F10358" targetNode="P_43F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_320F10358"/>
	</edges>
	<edges id="P_18F10358P_19F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10358" targetNode="P_19F10358"/>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10358_I" deadCode="false" sourceNode="P_42F10358" targetNode="P_44F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_336F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10358_O" deadCode="false" sourceNode="P_42F10358" targetNode="P_45F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_336F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10358_I" deadCode="false" sourceNode="P_42F10358" targetNode="P_46F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_338F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10358_O" deadCode="false" sourceNode="P_42F10358" targetNode="P_47F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_338F10358"/>
	</edges>
	<edges id="P_42F10358P_43F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10358" targetNode="P_43F10358"/>
	<edges id="P_44F10358P_45F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10358" targetNode="P_45F10358"/>
	<edges xsi:type="cbl:PerformEdge" id="S_352F10358_I" deadCode="false" sourceNode="P_46F10358" targetNode="P_48F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_352F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_352F10358_O" deadCode="false" sourceNode="P_46F10358" targetNode="P_49F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_352F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_353F10358_I" deadCode="false" sourceNode="P_46F10358" targetNode="P_50F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_353F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_353F10358_O" deadCode="false" sourceNode="P_46F10358" targetNode="P_51F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_353F10358"/>
	</edges>
	<edges id="P_46F10358P_47F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10358" targetNode="P_47F10358"/>
	<edges id="P_48F10358P_49F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10358" targetNode="P_49F10358"/>
	<edges xsi:type="cbl:PerformEdge" id="S_372F10358_I" deadCode="false" sourceNode="P_50F10358" targetNode="P_52F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_363F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_372F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_372F10358_O" deadCode="false" sourceNode="P_50F10358" targetNode="P_53F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_363F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_372F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_374F10358_I" deadCode="false" sourceNode="P_50F10358" targetNode="P_54F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_363F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_374F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_374F10358_O" deadCode="false" sourceNode="P_50F10358" targetNode="P_55F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_363F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_374F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_375F10358_I" deadCode="false" sourceNode="P_50F10358" targetNode="P_56F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_363F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_375F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_375F10358_O" deadCode="false" sourceNode="P_50F10358" targetNode="P_57F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_363F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_375F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_376F10358_I" deadCode="false" sourceNode="P_50F10358" targetNode="P_58F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_363F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_376F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_376F10358_O" deadCode="false" sourceNode="P_50F10358" targetNode="P_59F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_363F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_376F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_377F10358_I" deadCode="false" sourceNode="P_50F10358" targetNode="P_60F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_363F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_377F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_377F10358_O" deadCode="false" sourceNode="P_50F10358" targetNode="P_61F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_363F10358"/>
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_377F10358"/>
	</edges>
	<edges id="P_50F10358P_51F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10358" targetNode="P_51F10358"/>
	<edges xsi:type="cbl:PerformEdge" id="S_396F10358_I" deadCode="false" sourceNode="P_54F10358" targetNode="P_62F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_396F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_396F10358_O" deadCode="false" sourceNode="P_54F10358" targetNode="P_63F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_396F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_397F10358_I" deadCode="false" sourceNode="P_54F10358" targetNode="P_64F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_397F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_397F10358_O" deadCode="false" sourceNode="P_54F10358" targetNode="P_65F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_397F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_398F10358_I" deadCode="false" sourceNode="P_54F10358" targetNode="P_66F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_398F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_398F10358_O" deadCode="false" sourceNode="P_54F10358" targetNode="P_67F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_398F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_400F10358_I" deadCode="false" sourceNode="P_54F10358" targetNode="P_68F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_400F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_400F10358_O" deadCode="false" sourceNode="P_54F10358" targetNode="P_69F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_400F10358"/>
	</edges>
	<edges id="P_54F10358P_55F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10358" targetNode="P_55F10358"/>
	<edges xsi:type="cbl:PerformEdge" id="S_409F10358_I" deadCode="false" sourceNode="P_56F10358" targetNode="P_62F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_409F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_409F10358_O" deadCode="false" sourceNode="P_56F10358" targetNode="P_63F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_409F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_410F10358_I" deadCode="false" sourceNode="P_56F10358" targetNode="P_66F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_410F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_410F10358_O" deadCode="false" sourceNode="P_56F10358" targetNode="P_67F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_410F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_412F10358_I" deadCode="false" sourceNode="P_56F10358" targetNode="P_68F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_412F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_412F10358_O" deadCode="false" sourceNode="P_56F10358" targetNode="P_69F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_412F10358"/>
	</edges>
	<edges id="P_56F10358P_57F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10358" targetNode="P_57F10358"/>
	<edges xsi:type="cbl:PerformEdge" id="S_418F10358_I" deadCode="false" sourceNode="P_58F10358" targetNode="P_62F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_418F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_418F10358_O" deadCode="false" sourceNode="P_58F10358" targetNode="P_63F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_418F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_419F10358_I" deadCode="false" sourceNode="P_58F10358" targetNode="P_64F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_419F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_419F10358_O" deadCode="false" sourceNode="P_58F10358" targetNode="P_65F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_419F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_420F10358_I" deadCode="false" sourceNode="P_58F10358" targetNode="P_66F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_420F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_420F10358_O" deadCode="false" sourceNode="P_58F10358" targetNode="P_67F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_420F10358"/>
	</edges>
	<edges id="P_58F10358P_59F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10358" targetNode="P_59F10358"/>
	<edges xsi:type="cbl:PerformEdge" id="S_424F10358_I" deadCode="false" sourceNode="P_60F10358" targetNode="P_70F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_424F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_424F10358_O" deadCode="false" sourceNode="P_60F10358" targetNode="P_71F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_424F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_428F10358_I" deadCode="false" sourceNode="P_60F10358" targetNode="P_66F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_428F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_428F10358_O" deadCode="false" sourceNode="P_60F10358" targetNode="P_67F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_428F10358"/>
	</edges>
	<edges id="P_60F10358P_61F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_60F10358" targetNode="P_61F10358"/>
	<edges id="P_70F10358P_71F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_70F10358" targetNode="P_71F10358"/>
	<edges id="P_62F10358P_63F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10358" targetNode="P_63F10358"/>
	<edges id="P_64F10358P_65F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10358" targetNode="P_65F10358"/>
	<edges id="P_68F10358P_69F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10358" targetNode="P_69F10358"/>
	<edges xsi:type="cbl:PerformEdge" id="S_463F10358_I" deadCode="false" sourceNode="P_66F10358" targetNode="P_72F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_463F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_463F10358_O" deadCode="false" sourceNode="P_66F10358" targetNode="P_73F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_463F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_472F10358_I" deadCode="false" sourceNode="P_66F10358" targetNode="P_68F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_472F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_472F10358_O" deadCode="false" sourceNode="P_66F10358" targetNode="P_69F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_472F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_486F10358_I" deadCode="false" sourceNode="P_66F10358" targetNode="P_68F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_486F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_486F10358_O" deadCode="false" sourceNode="P_66F10358" targetNode="P_69F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_486F10358"/>
	</edges>
	<edges id="P_66F10358P_67F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10358" targetNode="P_67F10358"/>
	<edges xsi:type="cbl:PerformEdge" id="S_494F10358_I" deadCode="false" sourceNode="P_72F10358" targetNode="P_74F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_494F10358"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_494F10358_O" deadCode="false" sourceNode="P_72F10358" targetNode="P_75F10358">
		<representations href="../../../cobol/LVVS0560.cbl.cobModel#S_494F10358"/>
	</edges>
	<edges id="P_72F10358P_73F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10358" targetNode="P_73F10358"/>
	<edges id="P_74F10358P_75F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10358" targetNode="P_75F10358"/>
	<edges id="P_52F10358P_53F10358" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10358" targetNode="P_53F10358"/>
	<edges xsi:type="cbl:CallEdge" id="S_82F10358" deadCode="false" name="Dynamic LDBS1530" sourceNode="P_12F10358" targetNode="LDBS1530">
		<representations href="../../../cobol/../importantStmts.cobModel#S_82F10358"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_112F10358" deadCode="false" name="Dynamic LDBS5950" sourceNode="P_20F10358" targetNode="LDBS5950">
		<representations href="../../../cobol/../importantStmts.cobModel#S_112F10358"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_132F10358" deadCode="false" name="Dynamic LDBS4910" sourceNode="P_14F10358" targetNode="LDBS4910">
		<representations href="../../../cobol/../importantStmts.cobModel#S_132F10358"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_175F10358" deadCode="false" name="Dynamic IDBSRIF0" sourceNode="P_26F10358" targetNode="IDBSRIF0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_175F10358"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_195F10358" deadCode="false" name="Dynamic IDBSRDF0" sourceNode="P_28F10358" targetNode="IDBSRDF0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_195F10358"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_234F10358" deadCode="true" name="Dynamic LDBS5990" sourceNode="P_34F10358" targetNode="Dynamic_LVVS0560_LDBS5990">
		<representations href="../../../cobol/../importantStmts.cobModel#S_234F10358"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_255F10358" deadCode="true" name="Dynamic LDBS6000" sourceNode="P_35F10358" targetNode="Dynamic_LVVS0560_LDBS6000">
		<representations href="../../../cobol/../importantStmts.cobModel#S_255F10358"></representations>
	</edges>
</Package>

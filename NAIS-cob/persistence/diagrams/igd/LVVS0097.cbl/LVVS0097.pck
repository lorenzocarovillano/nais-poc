<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0097" cbl:id="LVVS0097" xsi:id="LVVS0097" packageRef="LVVS0097.igd#LVVS0097" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0097_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10345" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0097.cbl.cobModel#SC_1F10345"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10345" deadCode="false" name="PROGRAM_LVVS0097_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0097.cbl.cobModel#P_1F10345"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10345" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0097.cbl.cobModel#P_2F10345"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10345" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0097.cbl.cobModel#P_3F10345"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10345" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0097.cbl.cobModel#P_4F10345"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10345" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0097.cbl.cobModel#P_5F10345"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10345" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0097.cbl.cobModel#P_6F10345"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10345" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0097.cbl.cobModel#P_7F10345"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10345" deadCode="false" name="S1250-VERIFICA-DATE">
				<representations href="../../../cobol/LVVS0097.cbl.cobModel#P_8F10345"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10345" deadCode="false" name="S1250-EX">
				<representations href="../../../cobol/LVVS0097.cbl.cobModel#P_9F10345"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10345" deadCode="false" name="S1251-CALL-LDBS1470">
				<representations href="../../../cobol/LVVS0097.cbl.cobModel#P_14F10345"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10345" deadCode="false" name="S1251-EX">
				<representations href="../../../cobol/LVVS0097.cbl.cobModel#P_15F10345"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10345" deadCode="false" name="S1255-CALCOLA-IMPORTO">
				<representations href="../../../cobol/LVVS0097.cbl.cobModel#P_12F10345"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10345" deadCode="false" name="S1255-EX">
				<representations href="../../../cobol/LVVS0097.cbl.cobModel#P_13F10345"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10345" deadCode="false" name="S1256-CALCOLA-IMPORTO">
				<representations href="../../../cobol/LVVS0097.cbl.cobModel#P_10F10345"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10345" deadCode="false" name="S1256-EX">
				<representations href="../../../cobol/LVVS0097.cbl.cobModel#P_11F10345"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10345" deadCode="false" name="S1257-CALC-DT-INFERIORE">
				<representations href="../../../cobol/LVVS0097.cbl.cobModel#P_16F10345"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10345" deadCode="false" name="S1257-EX">
				<representations href="../../../cobol/LVVS0097.cbl.cobModel#P_17F10345"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10345" deadCode="false" name="CALL-ROUTINE-DATE">
				<representations href="../../../cobol/LVVS0097.cbl.cobModel#P_18F10345"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10345" deadCode="false" name="CALL-ROUTINE-DATE-EX">
				<representations href="../../../cobol/LVVS0097.cbl.cobModel#P_19F10345"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS1470" name="LDBS1470">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10161"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS4020" name="LDBS4020">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10207"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSE090" name="LDBSE090">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10258"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0003" name="LCCS0003">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10119"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10345P_1F10345" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10345" targetNode="P_1F10345"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10345_I" deadCode="false" sourceNode="P_1F10345" targetNode="P_2F10345">
		<representations href="../../../cobol/LVVS0097.cbl.cobModel#S_1F10345"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10345_O" deadCode="false" sourceNode="P_1F10345" targetNode="P_3F10345">
		<representations href="../../../cobol/LVVS0097.cbl.cobModel#S_1F10345"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10345_I" deadCode="false" sourceNode="P_1F10345" targetNode="P_4F10345">
		<representations href="../../../cobol/LVVS0097.cbl.cobModel#S_2F10345"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10345_O" deadCode="false" sourceNode="P_1F10345" targetNode="P_5F10345">
		<representations href="../../../cobol/LVVS0097.cbl.cobModel#S_2F10345"/>
	</edges>
	<edges id="P_2F10345P_3F10345" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10345" targetNode="P_3F10345"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10345_I" deadCode="false" sourceNode="P_4F10345" targetNode="P_6F10345">
		<representations href="../../../cobol/LVVS0097.cbl.cobModel#S_11F10345"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10345_O" deadCode="false" sourceNode="P_4F10345" targetNode="P_7F10345">
		<representations href="../../../cobol/LVVS0097.cbl.cobModel#S_11F10345"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10345_I" deadCode="false" sourceNode="P_4F10345" targetNode="P_8F10345">
		<representations href="../../../cobol/LVVS0097.cbl.cobModel#S_14F10345"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10345_O" deadCode="false" sourceNode="P_4F10345" targetNode="P_9F10345">
		<representations href="../../../cobol/LVVS0097.cbl.cobModel#S_14F10345"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10345_I" deadCode="false" sourceNode="P_4F10345" targetNode="P_10F10345">
		<representations href="../../../cobol/LVVS0097.cbl.cobModel#S_17F10345"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10345_O" deadCode="false" sourceNode="P_4F10345" targetNode="P_11F10345">
		<representations href="../../../cobol/LVVS0097.cbl.cobModel#S_17F10345"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10345_I" deadCode="false" sourceNode="P_4F10345" targetNode="P_12F10345">
		<representations href="../../../cobol/LVVS0097.cbl.cobModel#S_18F10345"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10345_O" deadCode="false" sourceNode="P_4F10345" targetNode="P_13F10345">
		<representations href="../../../cobol/LVVS0097.cbl.cobModel#S_18F10345"/>
	</edges>
	<edges id="P_4F10345P_5F10345" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10345" targetNode="P_5F10345"/>
	<edges id="P_6F10345P_7F10345" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10345" targetNode="P_7F10345"/>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10345_I" deadCode="false" sourceNode="P_8F10345" targetNode="P_14F10345">
		<representations href="../../../cobol/LVVS0097.cbl.cobModel#S_30F10345"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10345_O" deadCode="false" sourceNode="P_8F10345" targetNode="P_15F10345">
		<representations href="../../../cobol/LVVS0097.cbl.cobModel#S_30F10345"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10345_I" deadCode="false" sourceNode="P_8F10345" targetNode="P_16F10345">
		<representations href="../../../cobol/LVVS0097.cbl.cobModel#S_36F10345"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10345_O" deadCode="false" sourceNode="P_8F10345" targetNode="P_17F10345">
		<representations href="../../../cobol/LVVS0097.cbl.cobModel#S_36F10345"/>
	</edges>
	<edges id="P_8F10345P_9F10345" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10345" targetNode="P_9F10345"/>
	<edges id="P_14F10345P_15F10345" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10345" targetNode="P_15F10345"/>
	<edges id="P_12F10345P_13F10345" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10345" targetNode="P_13F10345"/>
	<edges id="P_10F10345P_11F10345" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10345" targetNode="P_11F10345"/>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10345_I" deadCode="false" sourceNode="P_16F10345" targetNode="P_18F10345">
		<representations href="../../../cobol/LVVS0097.cbl.cobModel#S_108F10345"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10345_O" deadCode="false" sourceNode="P_16F10345" targetNode="P_19F10345">
		<representations href="../../../cobol/LVVS0097.cbl.cobModel#S_108F10345"/>
	</edges>
	<edges id="P_16F10345P_17F10345" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10345" targetNode="P_17F10345"/>
	<edges id="P_18F10345P_19F10345" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10345" targetNode="P_19F10345"/>
	<edges xsi:type="cbl:CallEdge" id="S_46F10345" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_14F10345" targetNode="LDBS1470">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10345"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_67F10345" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_12F10345" targetNode="LDBS4020">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10345"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_87F10345" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_10F10345" targetNode="LDBSE090">
		<representations href="../../../cobol/../importantStmts.cobModel#S_87F10345"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_113F10345" deadCode="false" name="Dynamic LCCS0003" sourceNode="P_18F10345" targetNode="LCCS0003">
		<representations href="../../../cobol/../importantStmts.cobModel#S_113F10345"></representations>
	</edges>
</Package>

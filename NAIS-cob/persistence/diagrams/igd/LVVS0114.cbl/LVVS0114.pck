<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0114" cbl:id="LVVS0114" xsi:id="LVVS0114" packageRef="LVVS0114.igd#LVVS0114" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0114_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10352" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0114.cbl.cobModel#SC_1F10352"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10352" deadCode="false" name="PROGRAM_LVVS0114_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0114.cbl.cobModel#P_1F10352"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10352" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0114.cbl.cobModel#P_2F10352"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10352" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0114.cbl.cobModel#P_3F10352"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10352" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0114.cbl.cobModel#P_4F10352"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10352" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0114.cbl.cobModel#P_5F10352"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSRST0" name="IDBSRST0">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10087"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10352P_1F10352" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10352" targetNode="P_1F10352"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10352_I" deadCode="false" sourceNode="P_1F10352" targetNode="P_2F10352">
		<representations href="../../../cobol/LVVS0114.cbl.cobModel#S_1F10352"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10352_O" deadCode="false" sourceNode="P_1F10352" targetNode="P_3F10352">
		<representations href="../../../cobol/LVVS0114.cbl.cobModel#S_1F10352"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10352_I" deadCode="false" sourceNode="P_1F10352" targetNode="P_4F10352">
		<representations href="../../../cobol/LVVS0114.cbl.cobModel#S_2F10352"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10352_O" deadCode="false" sourceNode="P_1F10352" targetNode="P_5F10352">
		<representations href="../../../cobol/LVVS0114.cbl.cobModel#S_2F10352"/>
	</edges>
	<edges id="P_2F10352P_3F10352" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10352" targetNode="P_3F10352"/>
	<edges id="P_4F10352P_5F10352" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10352" targetNode="P_5F10352"/>
	<edges xsi:type="cbl:CallEdge" id="S_26F10352" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_4F10352" targetNode="IDBSRST0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_26F10352"></representations>
	</edges>
</Package>

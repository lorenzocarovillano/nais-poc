<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0640" cbl:id="LVVS0640" xsi:id="LVVS0640" packageRef="LVVS0640.igd#LVVS0640" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0640_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10361" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0640.cbl.cobModel#SC_1F10361"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10361" deadCode="false" name="PROGRAM_LVVS0640_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0640.cbl.cobModel#P_1F10361"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10361" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0640.cbl.cobModel#P_2F10361"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10361" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0640.cbl.cobModel#P_3F10361"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10361" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0640.cbl.cobModel#P_4F10361"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10361" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0640.cbl.cobModel#P_5F10361"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10361" deadCode="true" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0640.cbl.cobModel#P_8F10361"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10361" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0640.cbl.cobModel#P_9F10361"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10361" deadCode="true" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS0640.cbl.cobModel#P_10F10361"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10361" deadCode="true" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS0640.cbl.cobModel#P_11F10361"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10361" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0640.cbl.cobModel#P_6F10361"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10361" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0640.cbl.cobModel#P_7F10361"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10361P_1F10361" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10361" targetNode="P_1F10361"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10361_I" deadCode="false" sourceNode="P_1F10361" targetNode="P_2F10361">
		<representations href="../../../cobol/LVVS0640.cbl.cobModel#S_1F10361"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10361_O" deadCode="false" sourceNode="P_1F10361" targetNode="P_3F10361">
		<representations href="../../../cobol/LVVS0640.cbl.cobModel#S_1F10361"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10361_I" deadCode="false" sourceNode="P_1F10361" targetNode="P_4F10361">
		<representations href="../../../cobol/LVVS0640.cbl.cobModel#S_2F10361"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10361_O" deadCode="false" sourceNode="P_1F10361" targetNode="P_5F10361">
		<representations href="../../../cobol/LVVS0640.cbl.cobModel#S_2F10361"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10361_I" deadCode="false" sourceNode="P_1F10361" targetNode="P_6F10361">
		<representations href="../../../cobol/LVVS0640.cbl.cobModel#S_3F10361"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10361_O" deadCode="false" sourceNode="P_1F10361" targetNode="P_7F10361">
		<representations href="../../../cobol/LVVS0640.cbl.cobModel#S_3F10361"/>
	</edges>
	<edges id="P_2F10361P_3F10361" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10361" targetNode="P_3F10361"/>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10361_I" deadCode="true" sourceNode="P_4F10361" targetNode="P_8F10361">
		<representations href="../../../cobol/LVVS0640.cbl.cobModel#S_12F10361"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10361_O" deadCode="true" sourceNode="P_4F10361" targetNode="P_9F10361">
		<representations href="../../../cobol/LVVS0640.cbl.cobModel#S_12F10361"/>
	</edges>
	<edges id="P_4F10361P_5F10361" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10361" targetNode="P_5F10361"/>
	<edges id="P_8F10361P_9F10361" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10361" targetNode="P_9F10361"/>
</Package>

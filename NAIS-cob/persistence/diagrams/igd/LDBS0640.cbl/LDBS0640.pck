<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS0640" cbl:id="LDBS0640" xsi:id="LDBS0640" packageRef="LDBS0640.igd#LDBS0640" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS0640_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10147" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS0640.cbl.cobModel#SC_1F10147"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10147" deadCode="false" name="PROGRAM_LDBS0640_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_1F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10147" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_2F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10147" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_3F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10147" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_12F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10147" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_13F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10147" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_4F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10147" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_5F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10147" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_6F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10147" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_7F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10147" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_8F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10147" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_9F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10147" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_44F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10147" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_49F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10147" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_14F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10147" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_15F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10147" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_16F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10147" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_17F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10147" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_18F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10147" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_19F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10147" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_20F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10147" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_21F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10147" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_22F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10147" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_23F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10147" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_56F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10147" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_57F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10147" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_24F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10147" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_25F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10147" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_26F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10147" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_27F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10147" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_28F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10147" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_29F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10147" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_30F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10147" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_31F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10147" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_32F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10147" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_33F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10147" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_58F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10147" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_59F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10147" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_34F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10147" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_35F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10147" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_36F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10147" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_37F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10147" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_38F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10147" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_39F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10147" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_40F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10147" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_41F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10147" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_42F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10147" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_43F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10147" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_50F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10147" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_51F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10147" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_60F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10147" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_63F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10147" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_52F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10147" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_53F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10147" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_45F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10147" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_46F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10147" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_47F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10147" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_48F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10147" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_54F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10147" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_55F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10147" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_10F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10147" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_11F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10147" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_66F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10147" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_67F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10147" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_68F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10147" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_69F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10147" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_61F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10147" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_62F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10147" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_70F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10147" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_71F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10147" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_64F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10147" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_65F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10147" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_72F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10147" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_73F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10147" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_74F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10147" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_75F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10147" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_76F10147"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10147" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS0640.cbl.cobModel#P_77F10147"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_PARAM_MOVI" name="PARAM_MOVI">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_PARAM_MOVI"/>
		</children>
	</packageNode>
	<edges id="SC_1F10147P_1F10147" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10147" targetNode="P_1F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10147_I" deadCode="false" sourceNode="P_1F10147" targetNode="P_2F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_2F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10147_O" deadCode="false" sourceNode="P_1F10147" targetNode="P_3F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_2F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10147_I" deadCode="false" sourceNode="P_1F10147" targetNode="P_4F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_6F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10147_O" deadCode="false" sourceNode="P_1F10147" targetNode="P_5F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_6F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10147_I" deadCode="false" sourceNode="P_1F10147" targetNode="P_6F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_10F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10147_O" deadCode="false" sourceNode="P_1F10147" targetNode="P_7F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_10F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10147_I" deadCode="false" sourceNode="P_1F10147" targetNode="P_8F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_14F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10147_O" deadCode="false" sourceNode="P_1F10147" targetNode="P_9F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_14F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10147_I" deadCode="false" sourceNode="P_2F10147" targetNode="P_10F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_24F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10147_O" deadCode="false" sourceNode="P_2F10147" targetNode="P_11F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_24F10147"/>
	</edges>
	<edges id="P_2F10147P_3F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10147" targetNode="P_3F10147"/>
	<edges id="P_12F10147P_13F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10147" targetNode="P_13F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10147_I" deadCode="false" sourceNode="P_4F10147" targetNode="P_14F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_37F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10147_O" deadCode="false" sourceNode="P_4F10147" targetNode="P_15F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_37F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10147_I" deadCode="false" sourceNode="P_4F10147" targetNode="P_16F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_38F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10147_O" deadCode="false" sourceNode="P_4F10147" targetNode="P_17F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_38F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10147_I" deadCode="false" sourceNode="P_4F10147" targetNode="P_18F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_39F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10147_O" deadCode="false" sourceNode="P_4F10147" targetNode="P_19F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_39F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10147_I" deadCode="false" sourceNode="P_4F10147" targetNode="P_20F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_40F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10147_O" deadCode="false" sourceNode="P_4F10147" targetNode="P_21F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_40F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10147_I" deadCode="false" sourceNode="P_4F10147" targetNode="P_22F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_41F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10147_O" deadCode="false" sourceNode="P_4F10147" targetNode="P_23F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_41F10147"/>
	</edges>
	<edges id="P_4F10147P_5F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10147" targetNode="P_5F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10147_I" deadCode="false" sourceNode="P_6F10147" targetNode="P_24F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_45F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10147_O" deadCode="false" sourceNode="P_6F10147" targetNode="P_25F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_45F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10147_I" deadCode="false" sourceNode="P_6F10147" targetNode="P_26F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_46F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10147_O" deadCode="false" sourceNode="P_6F10147" targetNode="P_27F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_46F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10147_I" deadCode="false" sourceNode="P_6F10147" targetNode="P_28F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_47F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10147_O" deadCode="false" sourceNode="P_6F10147" targetNode="P_29F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_47F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10147_I" deadCode="false" sourceNode="P_6F10147" targetNode="P_30F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_48F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10147_O" deadCode="false" sourceNode="P_6F10147" targetNode="P_31F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_48F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10147_I" deadCode="false" sourceNode="P_6F10147" targetNode="P_32F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_49F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10147_O" deadCode="false" sourceNode="P_6F10147" targetNode="P_33F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_49F10147"/>
	</edges>
	<edges id="P_6F10147P_7F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10147" targetNode="P_7F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10147_I" deadCode="false" sourceNode="P_8F10147" targetNode="P_34F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_53F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10147_O" deadCode="false" sourceNode="P_8F10147" targetNode="P_35F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_53F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10147_I" deadCode="false" sourceNode="P_8F10147" targetNode="P_36F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_54F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10147_O" deadCode="false" sourceNode="P_8F10147" targetNode="P_37F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_54F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10147_I" deadCode="false" sourceNode="P_8F10147" targetNode="P_38F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_55F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10147_O" deadCode="false" sourceNode="P_8F10147" targetNode="P_39F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_55F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10147_I" deadCode="false" sourceNode="P_8F10147" targetNode="P_40F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_56F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10147_O" deadCode="false" sourceNode="P_8F10147" targetNode="P_41F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_56F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10147_I" deadCode="false" sourceNode="P_8F10147" targetNode="P_42F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_57F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10147_O" deadCode="false" sourceNode="P_8F10147" targetNode="P_43F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_57F10147"/>
	</edges>
	<edges id="P_8F10147P_9F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10147" targetNode="P_9F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10147_I" deadCode="false" sourceNode="P_44F10147" targetNode="P_45F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_60F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10147_O" deadCode="false" sourceNode="P_44F10147" targetNode="P_46F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_60F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10147_I" deadCode="false" sourceNode="P_44F10147" targetNode="P_47F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_61F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10147_O" deadCode="false" sourceNode="P_44F10147" targetNode="P_48F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_61F10147"/>
	</edges>
	<edges id="P_44F10147P_49F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10147" targetNode="P_49F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10147_I" deadCode="false" sourceNode="P_14F10147" targetNode="P_45F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_65F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10147_O" deadCode="false" sourceNode="P_14F10147" targetNode="P_46F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_65F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10147_I" deadCode="false" sourceNode="P_14F10147" targetNode="P_47F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_66F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10147_O" deadCode="false" sourceNode="P_14F10147" targetNode="P_48F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_66F10147"/>
	</edges>
	<edges id="P_14F10147P_15F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10147" targetNode="P_15F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10147_I" deadCode="false" sourceNode="P_16F10147" targetNode="P_44F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_69F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10147_O" deadCode="false" sourceNode="P_16F10147" targetNode="P_49F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_69F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10147_I" deadCode="false" sourceNode="P_16F10147" targetNode="P_12F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_71F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10147_O" deadCode="false" sourceNode="P_16F10147" targetNode="P_13F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_71F10147"/>
	</edges>
	<edges id="P_16F10147P_17F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10147" targetNode="P_17F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10147_I" deadCode="false" sourceNode="P_18F10147" targetNode="P_12F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_74F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10147_O" deadCode="false" sourceNode="P_18F10147" targetNode="P_13F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_74F10147"/>
	</edges>
	<edges id="P_18F10147P_19F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10147" targetNode="P_19F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10147_I" deadCode="false" sourceNode="P_20F10147" targetNode="P_16F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_76F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10147_O" deadCode="false" sourceNode="P_20F10147" targetNode="P_17F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_76F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10147_I" deadCode="false" sourceNode="P_20F10147" targetNode="P_22F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_78F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10147_O" deadCode="false" sourceNode="P_20F10147" targetNode="P_23F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_78F10147"/>
	</edges>
	<edges id="P_20F10147P_21F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10147" targetNode="P_21F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10147_I" deadCode="false" sourceNode="P_22F10147" targetNode="P_12F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_81F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10147_O" deadCode="false" sourceNode="P_22F10147" targetNode="P_13F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_81F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10147_I" deadCode="false" sourceNode="P_22F10147" targetNode="P_50F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_83F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10147_O" deadCode="false" sourceNode="P_22F10147" targetNode="P_51F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_83F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10147_I" deadCode="false" sourceNode="P_22F10147" targetNode="P_52F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_84F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10147_O" deadCode="false" sourceNode="P_22F10147" targetNode="P_53F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_84F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10147_I" deadCode="false" sourceNode="P_22F10147" targetNode="P_54F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_85F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10147_O" deadCode="false" sourceNode="P_22F10147" targetNode="P_55F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_85F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10147_I" deadCode="false" sourceNode="P_22F10147" targetNode="P_18F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_87F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10147_O" deadCode="false" sourceNode="P_22F10147" targetNode="P_19F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_87F10147"/>
	</edges>
	<edges id="P_22F10147P_23F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10147" targetNode="P_23F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10147_I" deadCode="false" sourceNode="P_56F10147" targetNode="P_45F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_91F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10147_O" deadCode="false" sourceNode="P_56F10147" targetNode="P_46F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_91F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10147_I" deadCode="false" sourceNode="P_56F10147" targetNode="P_47F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_92F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10147_O" deadCode="false" sourceNode="P_56F10147" targetNode="P_48F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_92F10147"/>
	</edges>
	<edges id="P_56F10147P_57F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10147" targetNode="P_57F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10147_I" deadCode="false" sourceNode="P_24F10147" targetNode="P_45F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_96F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10147_O" deadCode="false" sourceNode="P_24F10147" targetNode="P_46F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_96F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10147_I" deadCode="false" sourceNode="P_24F10147" targetNode="P_47F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_97F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10147_O" deadCode="false" sourceNode="P_24F10147" targetNode="P_48F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_97F10147"/>
	</edges>
	<edges id="P_24F10147P_25F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10147" targetNode="P_25F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10147_I" deadCode="false" sourceNode="P_26F10147" targetNode="P_56F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_100F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10147_O" deadCode="false" sourceNode="P_26F10147" targetNode="P_57F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_100F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10147_I" deadCode="false" sourceNode="P_26F10147" targetNode="P_12F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_102F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10147_O" deadCode="false" sourceNode="P_26F10147" targetNode="P_13F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_102F10147"/>
	</edges>
	<edges id="P_26F10147P_27F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10147" targetNode="P_27F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10147_I" deadCode="false" sourceNode="P_28F10147" targetNode="P_12F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_105F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10147_O" deadCode="false" sourceNode="P_28F10147" targetNode="P_13F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_105F10147"/>
	</edges>
	<edges id="P_28F10147P_29F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10147" targetNode="P_29F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10147_I" deadCode="false" sourceNode="P_30F10147" targetNode="P_26F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_107F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10147_O" deadCode="false" sourceNode="P_30F10147" targetNode="P_27F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_107F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10147_I" deadCode="false" sourceNode="P_30F10147" targetNode="P_32F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_109F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10147_O" deadCode="false" sourceNode="P_30F10147" targetNode="P_33F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_109F10147"/>
	</edges>
	<edges id="P_30F10147P_31F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10147" targetNode="P_31F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10147_I" deadCode="false" sourceNode="P_32F10147" targetNode="P_12F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_112F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10147_O" deadCode="false" sourceNode="P_32F10147" targetNode="P_13F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_112F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10147_I" deadCode="false" sourceNode="P_32F10147" targetNode="P_50F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_114F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10147_O" deadCode="false" sourceNode="P_32F10147" targetNode="P_51F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_114F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10147_I" deadCode="false" sourceNode="P_32F10147" targetNode="P_52F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_115F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10147_O" deadCode="false" sourceNode="P_32F10147" targetNode="P_53F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_115F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10147_I" deadCode="false" sourceNode="P_32F10147" targetNode="P_54F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_116F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10147_O" deadCode="false" sourceNode="P_32F10147" targetNode="P_55F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_116F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10147_I" deadCode="false" sourceNode="P_32F10147" targetNode="P_28F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_118F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10147_O" deadCode="false" sourceNode="P_32F10147" targetNode="P_29F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_118F10147"/>
	</edges>
	<edges id="P_32F10147P_33F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10147" targetNode="P_33F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10147_I" deadCode="false" sourceNode="P_58F10147" targetNode="P_45F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_122F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10147_O" deadCode="false" sourceNode="P_58F10147" targetNode="P_46F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_122F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10147_I" deadCode="false" sourceNode="P_58F10147" targetNode="P_47F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_123F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10147_O" deadCode="false" sourceNode="P_58F10147" targetNode="P_48F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_123F10147"/>
	</edges>
	<edges id="P_58F10147P_59F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10147" targetNode="P_59F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10147_I" deadCode="false" sourceNode="P_34F10147" targetNode="P_45F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_126F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10147_O" deadCode="false" sourceNode="P_34F10147" targetNode="P_46F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_126F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10147_I" deadCode="false" sourceNode="P_34F10147" targetNode="P_47F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_127F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10147_O" deadCode="false" sourceNode="P_34F10147" targetNode="P_48F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_127F10147"/>
	</edges>
	<edges id="P_34F10147P_35F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10147" targetNode="P_35F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10147_I" deadCode="false" sourceNode="P_36F10147" targetNode="P_58F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_130F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10147_O" deadCode="false" sourceNode="P_36F10147" targetNode="P_59F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_130F10147"/>
	</edges>
	<edges id="P_36F10147P_37F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10147" targetNode="P_37F10147"/>
	<edges id="P_38F10147P_39F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10147" targetNode="P_39F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10147_I" deadCode="false" sourceNode="P_40F10147" targetNode="P_36F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_135F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10147_O" deadCode="false" sourceNode="P_40F10147" targetNode="P_37F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_135F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10147_I" deadCode="false" sourceNode="P_40F10147" targetNode="P_42F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_137F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10147_O" deadCode="false" sourceNode="P_40F10147" targetNode="P_43F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_137F10147"/>
	</edges>
	<edges id="P_40F10147P_41F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10147" targetNode="P_41F10147"/>
	<edges id="P_42F10147P_43F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10147" targetNode="P_43F10147"/>
	<edges id="P_50F10147P_51F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10147" targetNode="P_51F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_228F10147_I" deadCode="true" sourceNode="P_60F10147" targetNode="P_61F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_228F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_228F10147_O" deadCode="true" sourceNode="P_60F10147" targetNode="P_62F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_228F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10147_I" deadCode="true" sourceNode="P_60F10147" targetNode="P_61F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_231F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10147_O" deadCode="true" sourceNode="P_60F10147" targetNode="P_62F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_231F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_235F10147_I" deadCode="true" sourceNode="P_60F10147" targetNode="P_61F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_235F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_235F10147_O" deadCode="true" sourceNode="P_60F10147" targetNode="P_62F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_235F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_239F10147_I" deadCode="true" sourceNode="P_60F10147" targetNode="P_61F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_239F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_239F10147_O" deadCode="true" sourceNode="P_60F10147" targetNode="P_62F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_239F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_243F10147_I" deadCode="true" sourceNode="P_60F10147" targetNode="P_61F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_243F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_243F10147_O" deadCode="true" sourceNode="P_60F10147" targetNode="P_62F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_243F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_247F10147_I" deadCode="false" sourceNode="P_52F10147" targetNode="P_64F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_247F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_247F10147_O" deadCode="false" sourceNode="P_52F10147" targetNode="P_65F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_247F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_250F10147_I" deadCode="false" sourceNode="P_52F10147" targetNode="P_64F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_250F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_250F10147_O" deadCode="false" sourceNode="P_52F10147" targetNode="P_65F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_250F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_254F10147_I" deadCode="false" sourceNode="P_52F10147" targetNode="P_64F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_254F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_254F10147_O" deadCode="false" sourceNode="P_52F10147" targetNode="P_65F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_254F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_258F10147_I" deadCode="false" sourceNode="P_52F10147" targetNode="P_64F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_258F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_258F10147_O" deadCode="false" sourceNode="P_52F10147" targetNode="P_65F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_258F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10147_I" deadCode="false" sourceNode="P_52F10147" targetNode="P_64F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_262F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10147_O" deadCode="false" sourceNode="P_52F10147" targetNode="P_65F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_262F10147"/>
	</edges>
	<edges id="P_52F10147P_53F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10147" targetNode="P_53F10147"/>
	<edges id="P_45F10147P_46F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10147" targetNode="P_46F10147"/>
	<edges id="P_47F10147P_48F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10147" targetNode="P_48F10147"/>
	<edges id="P_54F10147P_55F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10147" targetNode="P_55F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_271F10147_I" deadCode="false" sourceNode="P_10F10147" targetNode="P_66F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_271F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_271F10147_O" deadCode="false" sourceNode="P_10F10147" targetNode="P_67F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_271F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_273F10147_I" deadCode="false" sourceNode="P_10F10147" targetNode="P_68F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_273F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_273F10147_O" deadCode="false" sourceNode="P_10F10147" targetNode="P_69F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_273F10147"/>
	</edges>
	<edges id="P_10F10147P_11F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10147" targetNode="P_11F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10147_I" deadCode="false" sourceNode="P_66F10147" targetNode="P_61F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_278F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10147_O" deadCode="false" sourceNode="P_66F10147" targetNode="P_62F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_278F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10147_I" deadCode="false" sourceNode="P_66F10147" targetNode="P_61F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_283F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10147_O" deadCode="false" sourceNode="P_66F10147" targetNode="P_62F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_283F10147"/>
	</edges>
	<edges id="P_66F10147P_67F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10147" targetNode="P_67F10147"/>
	<edges id="P_68F10147P_69F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10147" targetNode="P_69F10147"/>
	<edges id="P_61F10147P_62F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10147" targetNode="P_62F10147"/>
	<edges xsi:type="cbl:PerformEdge" id="S_312F10147_I" deadCode="false" sourceNode="P_64F10147" targetNode="P_72F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_312F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_312F10147_O" deadCode="false" sourceNode="P_64F10147" targetNode="P_73F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_312F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_313F10147_I" deadCode="false" sourceNode="P_64F10147" targetNode="P_74F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_313F10147"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_313F10147_O" deadCode="false" sourceNode="P_64F10147" targetNode="P_75F10147">
		<representations href="../../../cobol/LDBS0640.cbl.cobModel#S_313F10147"/>
	</edges>
	<edges id="P_64F10147P_65F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10147" targetNode="P_65F10147"/>
	<edges id="P_72F10147P_73F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10147" targetNode="P_73F10147"/>
	<edges id="P_74F10147P_75F10147" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10147" targetNode="P_75F10147"/>
	<edges xsi:type="cbl:DataEdge" id="S_70F10147_POS1" deadCode="false" targetNode="P_16F10147" sourceNode="DB2_PARAM_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_70F10147"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_73F10147_POS1" deadCode="false" targetNode="P_18F10147" sourceNode="DB2_PARAM_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_73F10147"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_80F10147_POS1" deadCode="false" targetNode="P_22F10147" sourceNode="DB2_PARAM_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_80F10147"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_101F10147_POS1" deadCode="false" targetNode="P_26F10147" sourceNode="DB2_PARAM_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_101F10147"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_104F10147_POS1" deadCode="false" targetNode="P_28F10147" sourceNode="DB2_PARAM_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_104F10147"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10147_POS1" deadCode="false" targetNode="P_32F10147" sourceNode="DB2_PARAM_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10147"></representations>
	</edges>
</Package>

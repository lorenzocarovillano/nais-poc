<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IABS0110" cbl:id="IABS0110" xsi:id="IABS0110" packageRef="IABS0110.igd#IABS0110" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IABS0110_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10009" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IABS0110.cbl.cobModel#SC_1F10009"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10009" deadCode="false" name="PROGRAM_IABS0110_FIRST_SENTENCES">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_1F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10009" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_2F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10009" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_3F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10009" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_10F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10009" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_11F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10009" deadCode="false" name="A300-ELABORA">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_4F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10009" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_5F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10009" deadCode="false" name="A400-FINE">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_6F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10009" deadCode="false" name="A400-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_7F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10009" deadCode="false" name="A305-DECLARE-CURSOR">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_22F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10009" deadCode="false" name="A305-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_23F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10009" deadCode="false" name="A320-INSERT">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_14F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10009" deadCode="false" name="A320-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_15F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10009" deadCode="false" name="A360-OPEN-CURSOR">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_34F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10009" deadCode="false" name="A360-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_35F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10009" deadCode="false" name="A370-CLOSE-CURSOR">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_16F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10009" deadCode="false" name="A370-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_17F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10009" deadCode="false" name="A380-FETCH-FIRST">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_18F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10009" deadCode="false" name="A380-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_19F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10009" deadCode="false" name="A390-FETCH-NEXT">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_20F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10009" deadCode="false" name="A390-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_21F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10009" deadCode="false" name="A310-DELETE">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_12F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10009" deadCode="false" name="A310-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_13F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10009" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_36F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10009" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_37F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10009" deadCode="false" name="Z150-VALORIZZA-DATA-SERVICES">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_26F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10009" deadCode="false" name="Z150-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_27F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10009" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_28F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10009" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_29F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10009" deadCode="false" name="Z400-SEQ">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_24F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10009" deadCode="false" name="Z400-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_25F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10009" deadCode="false" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_30F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10009" deadCode="false" name="Z900-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_31F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10009" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_32F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10009" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_33F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10009" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_8F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10009" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_9F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10009" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_38F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10009" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_39F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10009" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_40F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10009" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_41F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10009" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_42F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10009" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_43F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10009" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_44F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10009" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_45F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10009" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_46F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10009" deadCode="true" name="Z800-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_51F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10009" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_47F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10009" deadCode="true" name="Z810-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_48F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10009" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_49F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10009" deadCode="true" name="Z820-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_50F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10009" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_52F10009"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10009" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/IABS0110.cbl.cobModel#P_53F10009"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_BTC_REC_SCHEDULE" name="BTC_REC_SCHEDULE">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_BTC_REC_SCHEDULE"/>
		</children>
	</packageNode>
	<edges id="SC_1F10009P_1F10009" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10009" targetNode="P_1F10009"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10009_I" deadCode="false" sourceNode="P_1F10009" targetNode="P_2F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_1F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10009_O" deadCode="false" sourceNode="P_1F10009" targetNode="P_3F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_1F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10009_I" deadCode="false" sourceNode="P_1F10009" targetNode="P_4F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_2F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10009_O" deadCode="false" sourceNode="P_1F10009" targetNode="P_5F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_2F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10009_I" deadCode="false" sourceNode="P_1F10009" targetNode="P_6F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_3F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10009_O" deadCode="false" sourceNode="P_1F10009" targetNode="P_7F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_3F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10009_I" deadCode="false" sourceNode="P_2F10009" targetNode="P_8F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_10F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10009_O" deadCode="false" sourceNode="P_2F10009" targetNode="P_9F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_10F10009"/>
	</edges>
	<edges id="P_2F10009P_3F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10009" targetNode="P_3F10009"/>
	<edges id="P_10F10009P_11F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10009" targetNode="P_11F10009"/>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10009_I" deadCode="false" sourceNode="P_4F10009" targetNode="P_12F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_23F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10009_O" deadCode="false" sourceNode="P_4F10009" targetNode="P_13F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_23F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10009_I" deadCode="false" sourceNode="P_4F10009" targetNode="P_14F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_24F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10009_O" deadCode="false" sourceNode="P_4F10009" targetNode="P_15F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_24F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10009_I" deadCode="false" sourceNode="P_4F10009" targetNode="P_16F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_25F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10009_O" deadCode="false" sourceNode="P_4F10009" targetNode="P_17F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_25F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10009_I" deadCode="false" sourceNode="P_4F10009" targetNode="P_18F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_26F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10009_O" deadCode="false" sourceNode="P_4F10009" targetNode="P_19F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_26F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10009_I" deadCode="false" sourceNode="P_4F10009" targetNode="P_20F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_27F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10009_O" deadCode="false" sourceNode="P_4F10009" targetNode="P_21F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_27F10009"/>
	</edges>
	<edges id="P_4F10009P_5F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10009" targetNode="P_5F10009"/>
	<edges id="P_6F10009P_7F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10009" targetNode="P_7F10009"/>
	<edges id="P_22F10009P_23F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10009" targetNode="P_23F10009"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10009_I" deadCode="false" sourceNode="P_14F10009" targetNode="P_24F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_35F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10009_O" deadCode="false" sourceNode="P_14F10009" targetNode="P_25F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_35F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10009_I" deadCode="false" sourceNode="P_14F10009" targetNode="P_26F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_37F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10009_O" deadCode="false" sourceNode="P_14F10009" targetNode="P_27F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_37F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10009_I" deadCode="false" sourceNode="P_14F10009" targetNode="P_28F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_38F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10009_O" deadCode="false" sourceNode="P_14F10009" targetNode="P_29F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_38F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10009_I" deadCode="false" sourceNode="P_14F10009" targetNode="P_30F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_39F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10009_O" deadCode="false" sourceNode="P_14F10009" targetNode="P_31F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_39F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10009_I" deadCode="false" sourceNode="P_14F10009" targetNode="P_32F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_40F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10009_O" deadCode="false" sourceNode="P_14F10009" targetNode="P_33F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_40F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10009_I" deadCode="false" sourceNode="P_14F10009" targetNode="P_10F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_42F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10009_O" deadCode="false" sourceNode="P_14F10009" targetNode="P_11F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_42F10009"/>
	</edges>
	<edges id="P_14F10009P_15F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10009" targetNode="P_15F10009"/>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10009_I" deadCode="false" sourceNode="P_34F10009" targetNode="P_22F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_44F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10009_O" deadCode="false" sourceNode="P_34F10009" targetNode="P_23F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_44F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10009_I" deadCode="false" sourceNode="P_34F10009" targetNode="P_10F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_46F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10009_O" deadCode="false" sourceNode="P_34F10009" targetNode="P_11F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_46F10009"/>
	</edges>
	<edges id="P_34F10009P_35F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10009" targetNode="P_35F10009"/>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10009_I" deadCode="false" sourceNode="P_16F10009" targetNode="P_10F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_49F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10009_O" deadCode="false" sourceNode="P_16F10009" targetNode="P_11F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_49F10009"/>
	</edges>
	<edges id="P_16F10009P_17F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10009" targetNode="P_17F10009"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10009_I" deadCode="false" sourceNode="P_18F10009" targetNode="P_34F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_51F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10009_O" deadCode="false" sourceNode="P_18F10009" targetNode="P_35F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_51F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10009_I" deadCode="false" sourceNode="P_18F10009" targetNode="P_20F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_53F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10009_O" deadCode="false" sourceNode="P_18F10009" targetNode="P_21F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_53F10009"/>
	</edges>
	<edges id="P_18F10009P_19F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10009" targetNode="P_19F10009"/>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10009_I" deadCode="false" sourceNode="P_20F10009" targetNode="P_10F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_57F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10009_O" deadCode="false" sourceNode="P_20F10009" targetNode="P_11F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_57F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10009_I" deadCode="false" sourceNode="P_20F10009" targetNode="P_36F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_59F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10009_O" deadCode="false" sourceNode="P_20F10009" targetNode="P_37F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_59F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10009_I" deadCode="false" sourceNode="P_20F10009" targetNode="P_16F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_61F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10009_O" deadCode="false" sourceNode="P_20F10009" targetNode="P_17F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_61F10009"/>
	</edges>
	<edges id="P_20F10009P_21F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10009" targetNode="P_21F10009"/>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10009_I" deadCode="false" sourceNode="P_12F10009" targetNode="P_10F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_66F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10009_O" deadCode="false" sourceNode="P_12F10009" targetNode="P_11F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_66F10009"/>
	</edges>
	<edges id="P_12F10009P_13F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10009" targetNode="P_13F10009"/>
	<edges id="P_36F10009P_37F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10009" targetNode="P_37F10009"/>
	<edges id="P_26F10009P_27F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10009" targetNode="P_27F10009"/>
	<edges id="P_28F10009P_29F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10009" targetNode="P_29F10009"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10009_I" deadCode="false" sourceNode="P_24F10009" targetNode="P_10F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_79F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10009_O" deadCode="false" sourceNode="P_24F10009" targetNode="P_11F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_79F10009"/>
	</edges>
	<edges id="P_24F10009P_25F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10009" targetNode="P_25F10009"/>
	<edges id="P_30F10009P_31F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10009" targetNode="P_31F10009"/>
	<edges id="P_32F10009P_33F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10009" targetNode="P_33F10009"/>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10009_I" deadCode="false" sourceNode="P_8F10009" targetNode="P_38F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_86F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10009_O" deadCode="false" sourceNode="P_8F10009" targetNode="P_39F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_86F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10009_I" deadCode="false" sourceNode="P_8F10009" targetNode="P_40F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_88F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10009_O" deadCode="false" sourceNode="P_8F10009" targetNode="P_41F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_88F10009"/>
	</edges>
	<edges id="P_8F10009P_9F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10009" targetNode="P_9F10009"/>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10009_I" deadCode="true" sourceNode="P_38F10009" targetNode="P_42F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_93F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10009_O" deadCode="true" sourceNode="P_38F10009" targetNode="P_43F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_93F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10009_I" deadCode="true" sourceNode="P_38F10009" targetNode="P_42F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_98F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10009_O" deadCode="true" sourceNode="P_38F10009" targetNode="P_43F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_98F10009"/>
	</edges>
	<edges id="P_38F10009P_39F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10009" targetNode="P_39F10009"/>
	<edges id="P_40F10009P_41F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10009" targetNode="P_41F10009"/>
	<edges id="P_42F10009P_43F10009" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10009" targetNode="P_43F10009"/>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10009_I" deadCode="true" sourceNode="P_46F10009" targetNode="P_47F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_127F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10009_O" deadCode="true" sourceNode="P_46F10009" targetNode="P_48F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_127F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10009_I" deadCode="true" sourceNode="P_46F10009" targetNode="P_49F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_128F10009"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10009_O" deadCode="true" sourceNode="P_46F10009" targetNode="P_50F10009">
		<representations href="../../../cobol/IABS0110.cbl.cobModel#S_128F10009"/>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_41F10009_POS1" deadCode="false" sourceNode="P_14F10009" targetNode="DB2_BTC_REC_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_41F10009"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_45F10009_POS1" deadCode="false" targetNode="P_34F10009" sourceNode="DB2_BTC_REC_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_45F10009"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_48F10009_POS1" deadCode="false" targetNode="P_16F10009" sourceNode="DB2_BTC_REC_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_48F10009"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_56F10009_POS1" deadCode="false" targetNode="P_20F10009" sourceNode="DB2_BTC_REC_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_56F10009"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_65F10009_POS1" deadCode="false" sourceNode="P_12F10009" targetNode="DB2_BTC_REC_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_65F10009"></representations>
	</edges>
</Package>

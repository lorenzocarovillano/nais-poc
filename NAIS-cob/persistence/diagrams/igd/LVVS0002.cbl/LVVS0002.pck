<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0002" cbl:id="LVVS0002" xsi:id="LVVS0002" packageRef="LVVS0002.igd#LVVS0002" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0002_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10306" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0002.cbl.cobModel#SC_1F10306"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10306" deadCode="false" name="PROGRAM_LVVS0002_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0002.cbl.cobModel#P_1F10306"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10306" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0002.cbl.cobModel#P_2F10306"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10306" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0002.cbl.cobModel#P_3F10306"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10306" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0002.cbl.cobModel#P_4F10306"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10306" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0002.cbl.cobModel#P_5F10306"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10306" deadCode="false" name="RECUP-MOVI-COMUN">
				<representations href="../../../cobol/LVVS0002.cbl.cobModel#P_12F10306"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10306" deadCode="false" name="RECUP-MOVI-COMUN-EX">
				<representations href="../../../cobol/LVVS0002.cbl.cobModel#P_13F10306"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10306" deadCode="false" name="CLOSE-MOVI">
				<representations href="../../../cobol/LVVS0002.cbl.cobModel#P_18F10306"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10306" deadCode="false" name="CLOSE-MOVI-EX">
				<representations href="../../../cobol/LVVS0002.cbl.cobModel#P_19F10306"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10306" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0002.cbl.cobModel#P_8F10306"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10306" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0002.cbl.cobModel#P_9F10306"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10306" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS0002.cbl.cobModel#P_10F10306"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10306" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS0002.cbl.cobModel#P_11F10306"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10306" deadCode="false" name="S1301-LEGGI-TRCH-POS">
				<representations href="../../../cobol/LVVS0002.cbl.cobModel#P_14F10306"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10306" deadCode="false" name="S1301-EX">
				<representations href="../../../cobol/LVVS0002.cbl.cobModel#P_15F10306"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10306" deadCode="false" name="S1302-LEGGI-TRCH-NEG">
				<representations href="../../../cobol/LVVS0002.cbl.cobModel#P_16F10306"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10306" deadCode="false" name="S1302-EX">
				<representations href="../../../cobol/LVVS0002.cbl.cobModel#P_17F10306"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10306" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0002.cbl.cobModel#P_6F10306"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10306" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0002.cbl.cobModel#P_7F10306"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS6040" name="LDBS6040">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10230"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS5140" name="LDBS5140">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10219"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10306P_1F10306" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10306" targetNode="P_1F10306"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10306_I" deadCode="false" sourceNode="P_1F10306" targetNode="P_2F10306">
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_1F10306"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10306_O" deadCode="false" sourceNode="P_1F10306" targetNode="P_3F10306">
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_1F10306"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10306_I" deadCode="false" sourceNode="P_1F10306" targetNode="P_4F10306">
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_3F10306"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10306_O" deadCode="false" sourceNode="P_1F10306" targetNode="P_5F10306">
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_3F10306"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10306_I" deadCode="false" sourceNode="P_1F10306" targetNode="P_6F10306">
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_4F10306"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10306_O" deadCode="false" sourceNode="P_1F10306" targetNode="P_7F10306">
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_4F10306"/>
	</edges>
	<edges id="P_2F10306P_3F10306" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10306" targetNode="P_3F10306"/>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10306_I" deadCode="false" sourceNode="P_4F10306" targetNode="P_8F10306">
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_12F10306"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10306_O" deadCode="false" sourceNode="P_4F10306" targetNode="P_9F10306">
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_12F10306"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10306_I" deadCode="false" sourceNode="P_4F10306" targetNode="P_10F10306">
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_14F10306"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10306_O" deadCode="false" sourceNode="P_4F10306" targetNode="P_11F10306">
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_14F10306"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10306_I" deadCode="false" sourceNode="P_4F10306" targetNode="P_12F10306">
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_18F10306"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10306_O" deadCode="false" sourceNode="P_4F10306" targetNode="P_13F10306">
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_18F10306"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10306_I" deadCode="false" sourceNode="P_4F10306" targetNode="P_14F10306">
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_28F10306"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10306_O" deadCode="false" sourceNode="P_4F10306" targetNode="P_15F10306">
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_28F10306"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10306_I" deadCode="false" sourceNode="P_4F10306" targetNode="P_16F10306">
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_30F10306"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10306_O" deadCode="false" sourceNode="P_4F10306" targetNode="P_17F10306">
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_30F10306"/>
	</edges>
	<edges id="P_4F10306P_5F10306" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10306" targetNode="P_5F10306"/>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10306_I" deadCode="false" sourceNode="P_12F10306" targetNode="P_18F10306">
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_41F10306"/>
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_64F10306"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10306_O" deadCode="false" sourceNode="P_12F10306" targetNode="P_19F10306">
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_41F10306"/>
		<representations href="../../../cobol/LVVS0002.cbl.cobModel#S_64F10306"/>
	</edges>
	<edges id="P_12F10306P_13F10306" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10306" targetNode="P_13F10306"/>
	<edges id="P_18F10306P_19F10306" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10306" targetNode="P_19F10306"/>
	<edges id="P_8F10306P_9F10306" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10306" targetNode="P_9F10306"/>
	<edges id="P_10F10306P_11F10306" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10306" targetNode="P_11F10306"/>
	<edges id="P_14F10306P_15F10306" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10306" targetNode="P_15F10306"/>
	<edges id="P_16F10306P_17F10306" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10306" targetNode="P_17F10306"/>
	<edges xsi:type="cbl:CallEdge" id="S_50F10306" deadCode="false" name="Dynamic WK-PGM-CALL" sourceNode="P_12F10306" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_50F10306"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_89F10306" deadCode="false" name="Dynamic WK-PGM-CALL" sourceNode="P_18F10306" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_89F10306"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_129F10306" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_14F10306" targetNode="LDBS5140">
		<representations href="../../../cobol/../importantStmts.cobModel#S_129F10306"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_147F10306" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_16F10306" targetNode="LDBS5140">
		<representations href="../../../cobol/../importantStmts.cobModel#S_147F10306"></representations>
	</edges>
</Package>

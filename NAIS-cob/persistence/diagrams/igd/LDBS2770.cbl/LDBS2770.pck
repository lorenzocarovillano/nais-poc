<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS2770" cbl:id="LDBS2770" xsi:id="LDBS2770" packageRef="LDBS2770.igd#LDBS2770" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS2770_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10185" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS2770.cbl.cobModel#SC_1F10185"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10185" deadCode="false" name="PROGRAM_LDBS2770_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_1F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10185" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_2F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10185" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_3F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10185" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_8F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10185" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_9F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10185" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_4F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10185" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_5F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10185" deadCode="false" name="C210-UPDATE-WC-NST">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_10F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10185" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_11F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10185" deadCode="true" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_20F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10185" deadCode="true" name="Z100-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_21F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10185" deadCode="false" name="Z150-VALORIZZA-DATA-SERVICES">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_12F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10185" deadCode="false" name="Z150-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_13F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10185" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_14F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10185" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_15F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10185" deadCode="true" name="Z400-SEQ-RIGA">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_22F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10185" deadCode="true" name="Z400-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_23F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10185" deadCode="true" name="Z500-AGGIORNAMENTO-STORICO">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_24F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10185" deadCode="true" name="Z500-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_25F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10185" deadCode="true" name="Z600-INSERT-NUOVA-RIGA-STORICA">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_26F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10185" deadCode="true" name="Z600-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_27F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10185" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_16F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10185" deadCode="false" name="Z900-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_17F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10185" deadCode="true" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_30F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10185" deadCode="true" name="Z950-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_31F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10185" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_18F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10185" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_19F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10185" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_6F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10185" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_7F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10185" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_32F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10185" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_33F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10185" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_34F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10185" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_35F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10185" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_28F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10185" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_29F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10185" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_36F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10185" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_37F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10185" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_38F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10185" deadCode="true" name="Z800-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_43F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10185" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_39F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10185" deadCode="true" name="Z810-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_40F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10185" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_41F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10185" deadCode="true" name="Z820-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_42F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10185" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_44F10185"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10185" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS2770.cbl.cobModel#P_45F10185"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_BILA_TRCH_ESTR" name="BILA_TRCH_ESTR">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_BILA_TRCH_ESTR"/>
		</children>
	</packageNode>
	<edges id="SC_1F10185P_1F10185" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10185" targetNode="P_1F10185"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10185_I" deadCode="false" sourceNode="P_1F10185" targetNode="P_2F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_1F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10185_O" deadCode="false" sourceNode="P_1F10185" targetNode="P_3F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_1F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10185_I" deadCode="false" sourceNode="P_1F10185" targetNode="P_4F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_5F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10185_O" deadCode="false" sourceNode="P_1F10185" targetNode="P_5F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_5F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10185_I" deadCode="false" sourceNode="P_2F10185" targetNode="P_6F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_14F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10185_O" deadCode="false" sourceNode="P_2F10185" targetNode="P_7F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_14F10185"/>
	</edges>
	<edges id="P_2F10185P_3F10185" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10185" targetNode="P_3F10185"/>
	<edges id="P_8F10185P_9F10185" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10185" targetNode="P_9F10185"/>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10185_I" deadCode="false" sourceNode="P_4F10185" targetNode="P_10F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_27F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10185_O" deadCode="false" sourceNode="P_4F10185" targetNode="P_11F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_27F10185"/>
	</edges>
	<edges id="P_4F10185P_5F10185" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10185" targetNode="P_5F10185"/>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10185_I" deadCode="false" sourceNode="P_10F10185" targetNode="P_12F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_30F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10185_O" deadCode="false" sourceNode="P_10F10185" targetNode="P_13F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_30F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10185_I" deadCode="false" sourceNode="P_10F10185" targetNode="P_14F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_31F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10185_O" deadCode="false" sourceNode="P_10F10185" targetNode="P_15F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_31F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10185_I" deadCode="true" sourceNode="P_10F10185" targetNode="P_16F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_32F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10185_O" deadCode="true" sourceNode="P_10F10185" targetNode="P_17F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_32F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10185_I" deadCode="false" sourceNode="P_10F10185" targetNode="P_18F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_33F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10185_O" deadCode="false" sourceNode="P_10F10185" targetNode="P_19F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_33F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10185_I" deadCode="false" sourceNode="P_10F10185" targetNode="P_8F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_35F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10185_O" deadCode="false" sourceNode="P_10F10185" targetNode="P_9F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_35F10185"/>
	</edges>
	<edges id="P_10F10185P_11F10185" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10185" targetNode="P_11F10185"/>
	<edges id="P_12F10185P_13F10185" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10185" targetNode="P_13F10185"/>
	<edges id="P_14F10185P_15F10185" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10185" targetNode="P_15F10185"/>
	<edges xsi:type="cbl:PerformEdge" id="S_403F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_403F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_403F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_403F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_406F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_406F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_406F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_406F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_410F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_410F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_410F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_410F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_413F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_413F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_413F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_413F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_416F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_416F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_416F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_416F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_420F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_420F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_420F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_420F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_423F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_423F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_423F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_423F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_426F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_426F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_426F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_426F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_430F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_430F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_430F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_430F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_434F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_434F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_434F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_434F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_438F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_438F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_438F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_438F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_442F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_442F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_442F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_442F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_446F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_446F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_446F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_446F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_450F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_450F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_450F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_450F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_454F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_454F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_454F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_454F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_458F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_458F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_458F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_458F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_462F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_462F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_462F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_462F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_466F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_466F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_466F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_466F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_470F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_470F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_470F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_470F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_474F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_474F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_474F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_474F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_478F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_478F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_478F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_478F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_482F10185_I" deadCode="true" sourceNode="P_16F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_482F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_482F10185_O" deadCode="true" sourceNode="P_16F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_482F10185"/>
	</edges>
	<edges id="P_16F10185P_17F10185" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10185" targetNode="P_17F10185"/>
	<edges id="P_18F10185P_19F10185" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10185" targetNode="P_19F10185"/>
	<edges xsi:type="cbl:PerformEdge" id="S_489F10185_I" deadCode="false" sourceNode="P_6F10185" targetNode="P_32F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_489F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_489F10185_O" deadCode="false" sourceNode="P_6F10185" targetNode="P_33F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_489F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_491F10185_I" deadCode="false" sourceNode="P_6F10185" targetNode="P_34F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_491F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_491F10185_O" deadCode="false" sourceNode="P_6F10185" targetNode="P_35F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_491F10185"/>
	</edges>
	<edges id="P_6F10185P_7F10185" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10185" targetNode="P_7F10185"/>
	<edges xsi:type="cbl:PerformEdge" id="S_496F10185_I" deadCode="true" sourceNode="P_32F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_496F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_496F10185_O" deadCode="true" sourceNode="P_32F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_496F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_501F10185_I" deadCode="true" sourceNode="P_32F10185" targetNode="P_28F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_501F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_501F10185_O" deadCode="true" sourceNode="P_32F10185" targetNode="P_29F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_501F10185"/>
	</edges>
	<edges id="P_32F10185P_33F10185" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10185" targetNode="P_33F10185"/>
	<edges id="P_34F10185P_35F10185" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10185" targetNode="P_35F10185"/>
	<edges id="P_28F10185P_29F10185" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10185" targetNode="P_29F10185"/>
	<edges xsi:type="cbl:PerformEdge" id="S_530F10185_I" deadCode="true" sourceNode="P_38F10185" targetNode="P_39F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_530F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_530F10185_O" deadCode="true" sourceNode="P_38F10185" targetNode="P_40F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_530F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_531F10185_I" deadCode="true" sourceNode="P_38F10185" targetNode="P_41F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_531F10185"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_531F10185_O" deadCode="true" sourceNode="P_38F10185" targetNode="P_42F10185">
		<representations href="../../../cobol/LDBS2770.cbl.cobModel#S_531F10185"/>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_34F10185_POS1" deadCode="false" sourceNode="P_10F10185" targetNode="DB2_BILA_TRCH_ESTR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_34F10185"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS5900" cbl:id="LDBS5900" xsi:id="LDBS5900" packageRef="LDBS5900.igd#LDBS5900" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS5900_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10224" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS5900.cbl.cobModel#SC_1F10224"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10224" deadCode="false" name="PROGRAM_LDBS5900_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_1F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10224" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_2F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10224" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_3F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10224" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_12F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10224" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_13F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10224" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_4F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10224" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_5F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10224" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_6F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10224" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_7F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10224" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_8F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10224" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_9F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10224" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_44F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10224" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_49F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10224" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_14F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10224" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_15F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10224" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_16F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10224" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_17F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10224" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_18F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10224" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_19F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10224" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_20F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10224" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_21F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10224" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_22F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10224" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_23F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10224" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_56F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10224" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_57F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10224" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_24F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10224" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_25F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10224" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_26F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10224" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_27F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10224" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_28F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10224" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_29F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10224" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_30F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10224" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_31F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10224" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_32F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10224" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_33F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10224" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_58F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10224" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_59F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10224" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_34F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10224" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_35F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10224" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_36F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10224" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_37F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10224" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_38F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10224" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_39F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10224" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_40F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10224" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_41F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10224" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_42F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10224" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_43F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10224" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_50F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10224" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_51F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10224" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_60F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10224" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_63F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10224" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_52F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10224" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_53F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10224" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_45F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10224" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_46F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10224" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_47F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10224" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_48F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10224" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_54F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10224" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_55F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10224" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_10F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10224" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_11F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10224" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_66F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10224" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_67F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10224" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_68F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10224" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_69F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10224" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_61F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10224" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_62F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10224" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_70F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10224" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_71F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10224" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_64F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10224" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_65F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10224" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_72F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10224" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_73F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10224" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_74F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10224" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_75F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10224" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_76F10224"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10224" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS5900.cbl.cobModel#P_77F10224"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_DETT_TIT_CONT" name="DETT_TIT_CONT">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_DETT_TIT_CONT"/>
		</children>
	</packageNode>
	<edges id="SC_1F10224P_1F10224" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10224" targetNode="P_1F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10224_I" deadCode="false" sourceNode="P_1F10224" targetNode="P_2F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_1F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10224_O" deadCode="false" sourceNode="P_1F10224" targetNode="P_3F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_1F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10224_I" deadCode="false" sourceNode="P_1F10224" targetNode="P_4F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_5F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10224_O" deadCode="false" sourceNode="P_1F10224" targetNode="P_5F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_5F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10224_I" deadCode="false" sourceNode="P_1F10224" targetNode="P_6F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_9F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10224_O" deadCode="false" sourceNode="P_1F10224" targetNode="P_7F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_9F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10224_I" deadCode="false" sourceNode="P_1F10224" targetNode="P_8F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_13F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10224_O" deadCode="false" sourceNode="P_1F10224" targetNode="P_9F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_13F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10224_I" deadCode="false" sourceNode="P_2F10224" targetNode="P_10F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_22F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10224_O" deadCode="false" sourceNode="P_2F10224" targetNode="P_11F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_22F10224"/>
	</edges>
	<edges id="P_2F10224P_3F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10224" targetNode="P_3F10224"/>
	<edges id="P_12F10224P_13F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10224" targetNode="P_13F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10224_I" deadCode="false" sourceNode="P_4F10224" targetNode="P_14F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_35F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10224_O" deadCode="false" sourceNode="P_4F10224" targetNode="P_15F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_35F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10224_I" deadCode="false" sourceNode="P_4F10224" targetNode="P_16F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_36F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10224_O" deadCode="false" sourceNode="P_4F10224" targetNode="P_17F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_36F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10224_I" deadCode="false" sourceNode="P_4F10224" targetNode="P_18F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_37F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10224_O" deadCode="false" sourceNode="P_4F10224" targetNode="P_19F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_37F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10224_I" deadCode="false" sourceNode="P_4F10224" targetNode="P_20F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_38F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10224_O" deadCode="false" sourceNode="P_4F10224" targetNode="P_21F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_38F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10224_I" deadCode="false" sourceNode="P_4F10224" targetNode="P_22F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_39F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10224_O" deadCode="false" sourceNode="P_4F10224" targetNode="P_23F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_39F10224"/>
	</edges>
	<edges id="P_4F10224P_5F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10224" targetNode="P_5F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10224_I" deadCode="false" sourceNode="P_6F10224" targetNode="P_24F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_43F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10224_O" deadCode="false" sourceNode="P_6F10224" targetNode="P_25F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_43F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10224_I" deadCode="false" sourceNode="P_6F10224" targetNode="P_26F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_44F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10224_O" deadCode="false" sourceNode="P_6F10224" targetNode="P_27F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_44F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10224_I" deadCode="false" sourceNode="P_6F10224" targetNode="P_28F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_45F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10224_O" deadCode="false" sourceNode="P_6F10224" targetNode="P_29F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_45F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10224_I" deadCode="false" sourceNode="P_6F10224" targetNode="P_30F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_46F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10224_O" deadCode="false" sourceNode="P_6F10224" targetNode="P_31F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_46F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10224_I" deadCode="false" sourceNode="P_6F10224" targetNode="P_32F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_47F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10224_O" deadCode="false" sourceNode="P_6F10224" targetNode="P_33F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_47F10224"/>
	</edges>
	<edges id="P_6F10224P_7F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10224" targetNode="P_7F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10224_I" deadCode="false" sourceNode="P_8F10224" targetNode="P_34F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_51F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10224_O" deadCode="false" sourceNode="P_8F10224" targetNode="P_35F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_51F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10224_I" deadCode="false" sourceNode="P_8F10224" targetNode="P_36F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_52F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10224_O" deadCode="false" sourceNode="P_8F10224" targetNode="P_37F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_52F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10224_I" deadCode="false" sourceNode="P_8F10224" targetNode="P_38F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_53F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10224_O" deadCode="false" sourceNode="P_8F10224" targetNode="P_39F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_53F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10224_I" deadCode="false" sourceNode="P_8F10224" targetNode="P_40F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_54F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10224_O" deadCode="false" sourceNode="P_8F10224" targetNode="P_41F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_54F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10224_I" deadCode="false" sourceNode="P_8F10224" targetNode="P_42F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_55F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10224_O" deadCode="false" sourceNode="P_8F10224" targetNode="P_43F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_55F10224"/>
	</edges>
	<edges id="P_8F10224P_9F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10224" targetNode="P_9F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10224_I" deadCode="false" sourceNode="P_44F10224" targetNode="P_45F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_58F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10224_O" deadCode="false" sourceNode="P_44F10224" targetNode="P_46F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_58F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10224_I" deadCode="false" sourceNode="P_44F10224" targetNode="P_47F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_59F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10224_O" deadCode="false" sourceNode="P_44F10224" targetNode="P_48F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_59F10224"/>
	</edges>
	<edges id="P_44F10224P_49F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10224" targetNode="P_49F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10224_I" deadCode="false" sourceNode="P_14F10224" targetNode="P_45F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_63F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10224_O" deadCode="false" sourceNode="P_14F10224" targetNode="P_46F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_63F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10224_I" deadCode="false" sourceNode="P_14F10224" targetNode="P_47F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_64F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10224_O" deadCode="false" sourceNode="P_14F10224" targetNode="P_48F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_64F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10224_I" deadCode="false" sourceNode="P_14F10224" targetNode="P_12F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_66F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10224_O" deadCode="false" sourceNode="P_14F10224" targetNode="P_13F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_66F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10224_I" deadCode="false" sourceNode="P_14F10224" targetNode="P_50F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_68F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10224_O" deadCode="false" sourceNode="P_14F10224" targetNode="P_51F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_68F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10224_I" deadCode="false" sourceNode="P_14F10224" targetNode="P_52F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_69F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10224_O" deadCode="false" sourceNode="P_14F10224" targetNode="P_53F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_69F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10224_I" deadCode="false" sourceNode="P_14F10224" targetNode="P_54F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_70F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10224_O" deadCode="false" sourceNode="P_14F10224" targetNode="P_55F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_70F10224"/>
	</edges>
	<edges id="P_14F10224P_15F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10224" targetNode="P_15F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10224_I" deadCode="false" sourceNode="P_16F10224" targetNode="P_44F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_72F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10224_O" deadCode="false" sourceNode="P_16F10224" targetNode="P_49F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_72F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10224_I" deadCode="false" sourceNode="P_16F10224" targetNode="P_12F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_74F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10224_O" deadCode="false" sourceNode="P_16F10224" targetNode="P_13F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_74F10224"/>
	</edges>
	<edges id="P_16F10224P_17F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10224" targetNode="P_17F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10224_I" deadCode="false" sourceNode="P_18F10224" targetNode="P_12F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_77F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10224_O" deadCode="false" sourceNode="P_18F10224" targetNode="P_13F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_77F10224"/>
	</edges>
	<edges id="P_18F10224P_19F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10224" targetNode="P_19F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10224_I" deadCode="false" sourceNode="P_20F10224" targetNode="P_16F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_79F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10224_O" deadCode="false" sourceNode="P_20F10224" targetNode="P_17F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_79F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10224_I" deadCode="false" sourceNode="P_20F10224" targetNode="P_22F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_81F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10224_O" deadCode="false" sourceNode="P_20F10224" targetNode="P_23F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_81F10224"/>
	</edges>
	<edges id="P_20F10224P_21F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10224" targetNode="P_21F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10224_I" deadCode="false" sourceNode="P_22F10224" targetNode="P_12F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_84F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10224_O" deadCode="false" sourceNode="P_22F10224" targetNode="P_13F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_84F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10224_I" deadCode="false" sourceNode="P_22F10224" targetNode="P_50F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_86F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10224_O" deadCode="false" sourceNode="P_22F10224" targetNode="P_51F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_86F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10224_I" deadCode="false" sourceNode="P_22F10224" targetNode="P_52F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_87F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10224_O" deadCode="false" sourceNode="P_22F10224" targetNode="P_53F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_87F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10224_I" deadCode="false" sourceNode="P_22F10224" targetNode="P_54F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_88F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10224_O" deadCode="false" sourceNode="P_22F10224" targetNode="P_55F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_88F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10224_I" deadCode="false" sourceNode="P_22F10224" targetNode="P_18F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_90F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10224_O" deadCode="false" sourceNode="P_22F10224" targetNode="P_19F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_90F10224"/>
	</edges>
	<edges id="P_22F10224P_23F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10224" targetNode="P_23F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10224_I" deadCode="false" sourceNode="P_56F10224" targetNode="P_45F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_94F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10224_O" deadCode="false" sourceNode="P_56F10224" targetNode="P_46F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_94F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10224_I" deadCode="false" sourceNode="P_56F10224" targetNode="P_47F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_95F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10224_O" deadCode="false" sourceNode="P_56F10224" targetNode="P_48F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_95F10224"/>
	</edges>
	<edges id="P_56F10224P_57F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10224" targetNode="P_57F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10224_I" deadCode="false" sourceNode="P_24F10224" targetNode="P_45F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_99F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10224_O" deadCode="false" sourceNode="P_24F10224" targetNode="P_46F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_99F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10224_I" deadCode="false" sourceNode="P_24F10224" targetNode="P_47F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_100F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10224_O" deadCode="false" sourceNode="P_24F10224" targetNode="P_48F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_100F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10224_I" deadCode="false" sourceNode="P_24F10224" targetNode="P_12F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_102F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10224_O" deadCode="false" sourceNode="P_24F10224" targetNode="P_13F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_102F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10224_I" deadCode="false" sourceNode="P_24F10224" targetNode="P_50F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_104F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10224_O" deadCode="false" sourceNode="P_24F10224" targetNode="P_51F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_104F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10224_I" deadCode="false" sourceNode="P_24F10224" targetNode="P_52F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_105F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10224_O" deadCode="false" sourceNode="P_24F10224" targetNode="P_53F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_105F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10224_I" deadCode="false" sourceNode="P_24F10224" targetNode="P_54F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_106F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10224_O" deadCode="false" sourceNode="P_24F10224" targetNode="P_55F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_106F10224"/>
	</edges>
	<edges id="P_24F10224P_25F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10224" targetNode="P_25F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10224_I" deadCode="false" sourceNode="P_26F10224" targetNode="P_56F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_108F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10224_O" deadCode="false" sourceNode="P_26F10224" targetNode="P_57F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_108F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10224_I" deadCode="false" sourceNode="P_26F10224" targetNode="P_12F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_110F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10224_O" deadCode="false" sourceNode="P_26F10224" targetNode="P_13F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_110F10224"/>
	</edges>
	<edges id="P_26F10224P_27F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10224" targetNode="P_27F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10224_I" deadCode="false" sourceNode="P_28F10224" targetNode="P_12F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_113F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10224_O" deadCode="false" sourceNode="P_28F10224" targetNode="P_13F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_113F10224"/>
	</edges>
	<edges id="P_28F10224P_29F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10224" targetNode="P_29F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10224_I" deadCode="false" sourceNode="P_30F10224" targetNode="P_26F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_115F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10224_O" deadCode="false" sourceNode="P_30F10224" targetNode="P_27F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_115F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10224_I" deadCode="false" sourceNode="P_30F10224" targetNode="P_32F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_117F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10224_O" deadCode="false" sourceNode="P_30F10224" targetNode="P_33F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_117F10224"/>
	</edges>
	<edges id="P_30F10224P_31F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10224" targetNode="P_31F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10224_I" deadCode="false" sourceNode="P_32F10224" targetNode="P_12F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_120F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10224_O" deadCode="false" sourceNode="P_32F10224" targetNode="P_13F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_120F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10224_I" deadCode="false" sourceNode="P_32F10224" targetNode="P_50F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_122F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10224_O" deadCode="false" sourceNode="P_32F10224" targetNode="P_51F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_122F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10224_I" deadCode="false" sourceNode="P_32F10224" targetNode="P_52F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_123F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10224_O" deadCode="false" sourceNode="P_32F10224" targetNode="P_53F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_123F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10224_I" deadCode="false" sourceNode="P_32F10224" targetNode="P_54F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_124F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10224_O" deadCode="false" sourceNode="P_32F10224" targetNode="P_55F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_124F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10224_I" deadCode="false" sourceNode="P_32F10224" targetNode="P_28F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_126F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10224_O" deadCode="false" sourceNode="P_32F10224" targetNode="P_29F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_126F10224"/>
	</edges>
	<edges id="P_32F10224P_33F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10224" targetNode="P_33F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10224_I" deadCode="false" sourceNode="P_58F10224" targetNode="P_45F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_130F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10224_O" deadCode="false" sourceNode="P_58F10224" targetNode="P_46F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_130F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10224_I" deadCode="false" sourceNode="P_58F10224" targetNode="P_47F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_131F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10224_O" deadCode="false" sourceNode="P_58F10224" targetNode="P_48F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_131F10224"/>
	</edges>
	<edges id="P_58F10224P_59F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10224" targetNode="P_59F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_134F10224_I" deadCode="false" sourceNode="P_34F10224" targetNode="P_45F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_134F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_134F10224_O" deadCode="false" sourceNode="P_34F10224" targetNode="P_46F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_134F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10224_I" deadCode="false" sourceNode="P_34F10224" targetNode="P_47F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_135F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10224_O" deadCode="false" sourceNode="P_34F10224" targetNode="P_48F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_135F10224"/>
	</edges>
	<edges id="P_34F10224P_35F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10224" targetNode="P_35F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10224_I" deadCode="false" sourceNode="P_36F10224" targetNode="P_58F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_138F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10224_O" deadCode="false" sourceNode="P_36F10224" targetNode="P_59F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_138F10224"/>
	</edges>
	<edges id="P_36F10224P_37F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10224" targetNode="P_37F10224"/>
	<edges id="P_38F10224P_39F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10224" targetNode="P_39F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10224_I" deadCode="false" sourceNode="P_40F10224" targetNode="P_36F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_143F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10224_O" deadCode="false" sourceNode="P_40F10224" targetNode="P_37F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_143F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10224_I" deadCode="false" sourceNode="P_40F10224" targetNode="P_42F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_145F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10224_O" deadCode="false" sourceNode="P_40F10224" targetNode="P_43F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_145F10224"/>
	</edges>
	<edges id="P_40F10224P_41F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10224" targetNode="P_41F10224"/>
	<edges id="P_42F10224P_43F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10224" targetNode="P_43F10224"/>
	<edges id="P_50F10224P_51F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10224" targetNode="P_51F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_250F10224_I" deadCode="true" sourceNode="P_60F10224" targetNode="P_61F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_250F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_250F10224_O" deadCode="true" sourceNode="P_60F10224" targetNode="P_62F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_250F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10224_I" deadCode="true" sourceNode="P_60F10224" targetNode="P_61F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_253F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10224_O" deadCode="true" sourceNode="P_60F10224" targetNode="P_62F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_253F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_257F10224_I" deadCode="true" sourceNode="P_60F10224" targetNode="P_61F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_257F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_257F10224_O" deadCode="true" sourceNode="P_60F10224" targetNode="P_62F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_257F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_261F10224_I" deadCode="true" sourceNode="P_60F10224" targetNode="P_61F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_261F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_261F10224_O" deadCode="true" sourceNode="P_60F10224" targetNode="P_62F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_261F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10224_I" deadCode="true" sourceNode="P_60F10224" targetNode="P_61F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_265F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10224_O" deadCode="true" sourceNode="P_60F10224" targetNode="P_62F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_265F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_269F10224_I" deadCode="false" sourceNode="P_52F10224" targetNode="P_64F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_269F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_269F10224_O" deadCode="false" sourceNode="P_52F10224" targetNode="P_65F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_269F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_272F10224_I" deadCode="false" sourceNode="P_52F10224" targetNode="P_64F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_272F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_272F10224_O" deadCode="false" sourceNode="P_52F10224" targetNode="P_65F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_272F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10224_I" deadCode="false" sourceNode="P_52F10224" targetNode="P_64F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_276F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10224_O" deadCode="false" sourceNode="P_52F10224" targetNode="P_65F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_276F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10224_I" deadCode="false" sourceNode="P_52F10224" targetNode="P_64F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_280F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10224_O" deadCode="false" sourceNode="P_52F10224" targetNode="P_65F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_280F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10224_I" deadCode="false" sourceNode="P_52F10224" targetNode="P_64F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_284F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10224_O" deadCode="false" sourceNode="P_52F10224" targetNode="P_65F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_284F10224"/>
	</edges>
	<edges id="P_52F10224P_53F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10224" targetNode="P_53F10224"/>
	<edges id="P_45F10224P_46F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10224" targetNode="P_46F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10224_I" deadCode="false" sourceNode="P_47F10224" targetNode="P_61F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_294F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10224_O" deadCode="false" sourceNode="P_47F10224" targetNode="P_62F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_294F10224"/>
	</edges>
	<edges id="P_47F10224P_48F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10224" targetNode="P_48F10224"/>
	<edges id="P_54F10224P_55F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10224" targetNode="P_55F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10224_I" deadCode="false" sourceNode="P_10F10224" targetNode="P_66F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_299F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10224_O" deadCode="false" sourceNode="P_10F10224" targetNode="P_67F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_299F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10224_I" deadCode="false" sourceNode="P_10F10224" targetNode="P_68F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_301F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10224_O" deadCode="false" sourceNode="P_10F10224" targetNode="P_69F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_301F10224"/>
	</edges>
	<edges id="P_10F10224P_11F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10224" targetNode="P_11F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_306F10224_I" deadCode="false" sourceNode="P_66F10224" targetNode="P_61F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_306F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_306F10224_O" deadCode="false" sourceNode="P_66F10224" targetNode="P_62F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_306F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10224_I" deadCode="false" sourceNode="P_66F10224" targetNode="P_61F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_311F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10224_O" deadCode="false" sourceNode="P_66F10224" targetNode="P_62F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_311F10224"/>
	</edges>
	<edges id="P_66F10224P_67F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10224" targetNode="P_67F10224"/>
	<edges id="P_68F10224P_69F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10224" targetNode="P_69F10224"/>
	<edges id="P_61F10224P_62F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10224" targetNode="P_62F10224"/>
	<edges xsi:type="cbl:PerformEdge" id="S_340F10224_I" deadCode="false" sourceNode="P_64F10224" targetNode="P_72F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_340F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_340F10224_O" deadCode="false" sourceNode="P_64F10224" targetNode="P_73F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_340F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_341F10224_I" deadCode="false" sourceNode="P_64F10224" targetNode="P_74F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_341F10224"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_341F10224_O" deadCode="false" sourceNode="P_64F10224" targetNode="P_75F10224">
		<representations href="../../../cobol/LDBS5900.cbl.cobModel#S_341F10224"/>
	</edges>
	<edges id="P_64F10224P_65F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10224" targetNode="P_65F10224"/>
	<edges id="P_72F10224P_73F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10224" targetNode="P_73F10224"/>
	<edges id="P_74F10224P_75F10224" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10224" targetNode="P_75F10224"/>
	<edges xsi:type="cbl:DataEdge" id="S_65F10224_POS1" deadCode="false" targetNode="P_14F10224" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_65F10224"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_73F10224_POS1" deadCode="false" targetNode="P_16F10224" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_73F10224"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_76F10224_POS1" deadCode="false" targetNode="P_18F10224" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_76F10224"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_83F10224_POS1" deadCode="false" targetNode="P_22F10224" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_83F10224"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_101F10224_POS1" deadCode="false" targetNode="P_24F10224" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_101F10224"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_109F10224_POS1" deadCode="false" targetNode="P_26F10224" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_109F10224"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_112F10224_POS1" deadCode="false" targetNode="P_28F10224" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_112F10224"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_119F10224_POS1" deadCode="false" targetNode="P_32F10224" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_119F10224"></representations>
	</edges>
</Package>

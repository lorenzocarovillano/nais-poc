<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBSF510" cbl:id="LDBSF510" xsi:id="LDBSF510" packageRef="LDBSF510.igd#LDBSF510" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBSF510_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10270" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBSF510.cbl.cobModel#SC_1F10270"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10270" deadCode="false" name="PROGRAM_LDBSF510_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_1F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10270" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_2F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10270" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_3F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10270" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_12F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10270" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_13F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10270" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_4F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10270" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_5F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10270" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_6F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10270" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_7F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10270" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_8F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10270" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_9F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10270" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_44F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10270" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_49F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10270" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_14F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10270" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_15F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10270" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_16F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10270" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_17F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10270" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_18F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10270" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_19F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10270" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_20F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10270" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_21F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10270" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_22F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10270" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_23F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10270" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_56F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10270" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_57F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10270" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_24F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10270" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_25F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10270" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_26F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10270" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_27F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10270" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_28F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10270" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_29F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10270" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_30F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10270" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_31F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10270" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_32F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10270" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_33F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10270" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_58F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10270" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_59F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10270" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_34F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10270" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_35F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10270" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_36F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10270" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_37F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10270" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_38F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10270" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_39F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10270" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_40F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10270" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_41F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10270" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_42F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10270" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_43F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10270" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_50F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10270" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_51F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10270" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_60F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10270" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_63F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10270" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_52F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10270" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_53F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10270" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_45F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10270" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_46F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10270" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_47F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10270" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_48F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10270" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_54F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10270" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_55F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10270" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_10F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10270" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_11F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10270" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_66F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10270" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_67F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10270" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_68F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10270" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_69F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10270" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_61F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10270" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_62F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10270" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_70F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10270" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_71F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10270" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_64F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10270" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_65F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10270" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_72F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10270" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_73F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10270" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_74F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10270" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_75F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10270" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_76F10270"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10270" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBSF510.cbl.cobModel#P_77F10270"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_POLI" name="POLI">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_POLI"/>
		</children>
	</packageNode>
	<edges id="SC_1F10270P_1F10270" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10270" targetNode="P_1F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10270_I" deadCode="false" sourceNode="P_1F10270" targetNode="P_2F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_2F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10270_O" deadCode="false" sourceNode="P_1F10270" targetNode="P_3F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_2F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10270_I" deadCode="false" sourceNode="P_1F10270" targetNode="P_4F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_6F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10270_O" deadCode="false" sourceNode="P_1F10270" targetNode="P_5F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_6F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10270_I" deadCode="false" sourceNode="P_1F10270" targetNode="P_6F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_10F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10270_O" deadCode="false" sourceNode="P_1F10270" targetNode="P_7F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_10F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10270_I" deadCode="false" sourceNode="P_1F10270" targetNode="P_8F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_14F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10270_O" deadCode="false" sourceNode="P_1F10270" targetNode="P_9F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_14F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10270_I" deadCode="false" sourceNode="P_2F10270" targetNode="P_10F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_24F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10270_O" deadCode="false" sourceNode="P_2F10270" targetNode="P_11F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_24F10270"/>
	</edges>
	<edges id="P_2F10270P_3F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10270" targetNode="P_3F10270"/>
	<edges id="P_12F10270P_13F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10270" targetNode="P_13F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10270_I" deadCode="false" sourceNode="P_4F10270" targetNode="P_14F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_37F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10270_O" deadCode="false" sourceNode="P_4F10270" targetNode="P_15F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_37F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10270_I" deadCode="false" sourceNode="P_4F10270" targetNode="P_16F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_38F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10270_O" deadCode="false" sourceNode="P_4F10270" targetNode="P_17F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_38F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10270_I" deadCode="false" sourceNode="P_4F10270" targetNode="P_18F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_39F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10270_O" deadCode="false" sourceNode="P_4F10270" targetNode="P_19F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_39F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10270_I" deadCode="false" sourceNode="P_4F10270" targetNode="P_20F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_40F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10270_O" deadCode="false" sourceNode="P_4F10270" targetNode="P_21F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_40F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10270_I" deadCode="false" sourceNode="P_4F10270" targetNode="P_22F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_41F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10270_O" deadCode="false" sourceNode="P_4F10270" targetNode="P_23F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_41F10270"/>
	</edges>
	<edges id="P_4F10270P_5F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10270" targetNode="P_5F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10270_I" deadCode="false" sourceNode="P_6F10270" targetNode="P_24F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_45F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10270_O" deadCode="false" sourceNode="P_6F10270" targetNode="P_25F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_45F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10270_I" deadCode="false" sourceNode="P_6F10270" targetNode="P_26F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_46F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10270_O" deadCode="false" sourceNode="P_6F10270" targetNode="P_27F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_46F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10270_I" deadCode="false" sourceNode="P_6F10270" targetNode="P_28F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_47F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10270_O" deadCode="false" sourceNode="P_6F10270" targetNode="P_29F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_47F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10270_I" deadCode="false" sourceNode="P_6F10270" targetNode="P_30F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_48F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10270_O" deadCode="false" sourceNode="P_6F10270" targetNode="P_31F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_48F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10270_I" deadCode="false" sourceNode="P_6F10270" targetNode="P_32F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_49F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10270_O" deadCode="false" sourceNode="P_6F10270" targetNode="P_33F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_49F10270"/>
	</edges>
	<edges id="P_6F10270P_7F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10270" targetNode="P_7F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10270_I" deadCode="false" sourceNode="P_8F10270" targetNode="P_34F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_53F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10270_O" deadCode="false" sourceNode="P_8F10270" targetNode="P_35F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_53F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10270_I" deadCode="false" sourceNode="P_8F10270" targetNode="P_36F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_54F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10270_O" deadCode="false" sourceNode="P_8F10270" targetNode="P_37F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_54F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10270_I" deadCode="false" sourceNode="P_8F10270" targetNode="P_38F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_55F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10270_O" deadCode="false" sourceNode="P_8F10270" targetNode="P_39F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_55F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10270_I" deadCode="false" sourceNode="P_8F10270" targetNode="P_40F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_56F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10270_O" deadCode="false" sourceNode="P_8F10270" targetNode="P_41F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_56F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10270_I" deadCode="false" sourceNode="P_8F10270" targetNode="P_42F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_57F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10270_O" deadCode="false" sourceNode="P_8F10270" targetNode="P_43F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_57F10270"/>
	</edges>
	<edges id="P_8F10270P_9F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10270" targetNode="P_9F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10270_I" deadCode="false" sourceNode="P_44F10270" targetNode="P_45F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_60F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10270_O" deadCode="false" sourceNode="P_44F10270" targetNode="P_46F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_60F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10270_I" deadCode="false" sourceNode="P_44F10270" targetNode="P_47F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_61F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10270_O" deadCode="false" sourceNode="P_44F10270" targetNode="P_48F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_61F10270"/>
	</edges>
	<edges id="P_44F10270P_49F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10270" targetNode="P_49F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10270_I" deadCode="false" sourceNode="P_14F10270" targetNode="P_45F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_65F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10270_O" deadCode="false" sourceNode="P_14F10270" targetNode="P_46F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_65F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10270_I" deadCode="false" sourceNode="P_14F10270" targetNode="P_47F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_66F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10270_O" deadCode="false" sourceNode="P_14F10270" targetNode="P_48F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_66F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10270_I" deadCode="false" sourceNode="P_14F10270" targetNode="P_12F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_68F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10270_O" deadCode="false" sourceNode="P_14F10270" targetNode="P_13F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_68F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10270_I" deadCode="false" sourceNode="P_14F10270" targetNode="P_50F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_70F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10270_O" deadCode="false" sourceNode="P_14F10270" targetNode="P_51F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_70F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10270_I" deadCode="false" sourceNode="P_14F10270" targetNode="P_52F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_71F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10270_O" deadCode="false" sourceNode="P_14F10270" targetNode="P_53F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_71F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10270_I" deadCode="false" sourceNode="P_14F10270" targetNode="P_54F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_72F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10270_O" deadCode="false" sourceNode="P_14F10270" targetNode="P_55F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_72F10270"/>
	</edges>
	<edges id="P_14F10270P_15F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10270" targetNode="P_15F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10270_I" deadCode="false" sourceNode="P_16F10270" targetNode="P_44F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_74F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10270_O" deadCode="false" sourceNode="P_16F10270" targetNode="P_49F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_74F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10270_I" deadCode="false" sourceNode="P_16F10270" targetNode="P_12F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_76F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10270_O" deadCode="false" sourceNode="P_16F10270" targetNode="P_13F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_76F10270"/>
	</edges>
	<edges id="P_16F10270P_17F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10270" targetNode="P_17F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10270_I" deadCode="false" sourceNode="P_18F10270" targetNode="P_12F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_79F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10270_O" deadCode="false" sourceNode="P_18F10270" targetNode="P_13F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_79F10270"/>
	</edges>
	<edges id="P_18F10270P_19F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10270" targetNode="P_19F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10270_I" deadCode="false" sourceNode="P_20F10270" targetNode="P_16F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_81F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10270_O" deadCode="false" sourceNode="P_20F10270" targetNode="P_17F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_81F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10270_I" deadCode="false" sourceNode="P_20F10270" targetNode="P_22F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_83F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10270_O" deadCode="false" sourceNode="P_20F10270" targetNode="P_23F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_83F10270"/>
	</edges>
	<edges id="P_20F10270P_21F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10270" targetNode="P_21F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10270_I" deadCode="false" sourceNode="P_22F10270" targetNode="P_12F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_86F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10270_O" deadCode="false" sourceNode="P_22F10270" targetNode="P_13F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_86F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10270_I" deadCode="false" sourceNode="P_22F10270" targetNode="P_50F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_88F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10270_O" deadCode="false" sourceNode="P_22F10270" targetNode="P_51F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_88F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10270_I" deadCode="false" sourceNode="P_22F10270" targetNode="P_52F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_89F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10270_O" deadCode="false" sourceNode="P_22F10270" targetNode="P_53F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_89F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10270_I" deadCode="false" sourceNode="P_22F10270" targetNode="P_54F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_90F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10270_O" deadCode="false" sourceNode="P_22F10270" targetNode="P_55F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_90F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10270_I" deadCode="false" sourceNode="P_22F10270" targetNode="P_18F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_92F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10270_O" deadCode="false" sourceNode="P_22F10270" targetNode="P_19F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_92F10270"/>
	</edges>
	<edges id="P_22F10270P_23F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10270" targetNode="P_23F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10270_I" deadCode="false" sourceNode="P_56F10270" targetNode="P_45F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_96F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10270_O" deadCode="false" sourceNode="P_56F10270" targetNode="P_46F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_96F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10270_I" deadCode="false" sourceNode="P_56F10270" targetNode="P_47F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_97F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10270_O" deadCode="false" sourceNode="P_56F10270" targetNode="P_48F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_97F10270"/>
	</edges>
	<edges id="P_56F10270P_57F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10270" targetNode="P_57F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10270_I" deadCode="false" sourceNode="P_24F10270" targetNode="P_45F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_101F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10270_O" deadCode="false" sourceNode="P_24F10270" targetNode="P_46F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_101F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10270_I" deadCode="false" sourceNode="P_24F10270" targetNode="P_47F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_102F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10270_O" deadCode="false" sourceNode="P_24F10270" targetNode="P_48F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_102F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10270_I" deadCode="false" sourceNode="P_24F10270" targetNode="P_12F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_104F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10270_O" deadCode="false" sourceNode="P_24F10270" targetNode="P_13F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_104F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10270_I" deadCode="false" sourceNode="P_24F10270" targetNode="P_50F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_106F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10270_O" deadCode="false" sourceNode="P_24F10270" targetNode="P_51F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_106F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10270_I" deadCode="false" sourceNode="P_24F10270" targetNode="P_52F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_107F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10270_O" deadCode="false" sourceNode="P_24F10270" targetNode="P_53F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_107F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10270_I" deadCode="false" sourceNode="P_24F10270" targetNode="P_54F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_108F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10270_O" deadCode="false" sourceNode="P_24F10270" targetNode="P_55F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_108F10270"/>
	</edges>
	<edges id="P_24F10270P_25F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10270" targetNode="P_25F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10270_I" deadCode="false" sourceNode="P_26F10270" targetNode="P_56F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_110F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10270_O" deadCode="false" sourceNode="P_26F10270" targetNode="P_57F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_110F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10270_I" deadCode="false" sourceNode="P_26F10270" targetNode="P_12F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_112F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10270_O" deadCode="false" sourceNode="P_26F10270" targetNode="P_13F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_112F10270"/>
	</edges>
	<edges id="P_26F10270P_27F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10270" targetNode="P_27F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10270_I" deadCode="false" sourceNode="P_28F10270" targetNode="P_12F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_115F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10270_O" deadCode="false" sourceNode="P_28F10270" targetNode="P_13F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_115F10270"/>
	</edges>
	<edges id="P_28F10270P_29F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10270" targetNode="P_29F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10270_I" deadCode="false" sourceNode="P_30F10270" targetNode="P_26F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_117F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10270_O" deadCode="false" sourceNode="P_30F10270" targetNode="P_27F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_117F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10270_I" deadCode="false" sourceNode="P_30F10270" targetNode="P_32F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_119F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10270_O" deadCode="false" sourceNode="P_30F10270" targetNode="P_33F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_119F10270"/>
	</edges>
	<edges id="P_30F10270P_31F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10270" targetNode="P_31F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10270_I" deadCode="false" sourceNode="P_32F10270" targetNode="P_12F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_122F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10270_O" deadCode="false" sourceNode="P_32F10270" targetNode="P_13F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_122F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10270_I" deadCode="false" sourceNode="P_32F10270" targetNode="P_50F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_124F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10270_O" deadCode="false" sourceNode="P_32F10270" targetNode="P_51F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_124F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10270_I" deadCode="false" sourceNode="P_32F10270" targetNode="P_52F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_125F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10270_O" deadCode="false" sourceNode="P_32F10270" targetNode="P_53F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_125F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10270_I" deadCode="false" sourceNode="P_32F10270" targetNode="P_54F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_126F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10270_O" deadCode="false" sourceNode="P_32F10270" targetNode="P_55F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_126F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10270_I" deadCode="false" sourceNode="P_32F10270" targetNode="P_28F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_128F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10270_O" deadCode="false" sourceNode="P_32F10270" targetNode="P_29F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_128F10270"/>
	</edges>
	<edges id="P_32F10270P_33F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10270" targetNode="P_33F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10270_I" deadCode="false" sourceNode="P_58F10270" targetNode="P_45F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_132F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10270_O" deadCode="false" sourceNode="P_58F10270" targetNode="P_46F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_132F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10270_I" deadCode="false" sourceNode="P_58F10270" targetNode="P_47F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_133F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10270_O" deadCode="false" sourceNode="P_58F10270" targetNode="P_48F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_133F10270"/>
	</edges>
	<edges id="P_58F10270P_59F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10270" targetNode="P_59F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10270_I" deadCode="false" sourceNode="P_34F10270" targetNode="P_45F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_136F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10270_O" deadCode="false" sourceNode="P_34F10270" targetNode="P_46F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_136F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10270_I" deadCode="false" sourceNode="P_34F10270" targetNode="P_47F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_137F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10270_O" deadCode="false" sourceNode="P_34F10270" targetNode="P_48F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_137F10270"/>
	</edges>
	<edges id="P_34F10270P_35F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10270" targetNode="P_35F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10270_I" deadCode="false" sourceNode="P_36F10270" targetNode="P_58F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_140F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10270_O" deadCode="false" sourceNode="P_36F10270" targetNode="P_59F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_140F10270"/>
	</edges>
	<edges id="P_36F10270P_37F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10270" targetNode="P_37F10270"/>
	<edges id="P_38F10270P_39F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10270" targetNode="P_39F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10270_I" deadCode="false" sourceNode="P_40F10270" targetNode="P_36F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_145F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10270_O" deadCode="false" sourceNode="P_40F10270" targetNode="P_37F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_145F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10270_I" deadCode="false" sourceNode="P_40F10270" targetNode="P_42F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_147F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10270_O" deadCode="false" sourceNode="P_40F10270" targetNode="P_43F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_147F10270"/>
	</edges>
	<edges id="P_40F10270P_41F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10270" targetNode="P_41F10270"/>
	<edges id="P_42F10270P_43F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10270" targetNode="P_43F10270"/>
	<edges id="P_50F10270P_51F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10270" targetNode="P_51F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10270_I" deadCode="true" sourceNode="P_60F10270" targetNode="P_61F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_253F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10270_O" deadCode="true" sourceNode="P_60F10270" targetNode="P_62F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_253F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_256F10270_I" deadCode="true" sourceNode="P_60F10270" targetNode="P_61F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_256F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_256F10270_O" deadCode="true" sourceNode="P_60F10270" targetNode="P_62F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_256F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10270_I" deadCode="true" sourceNode="P_60F10270" targetNode="P_61F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_259F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10270_O" deadCode="true" sourceNode="P_60F10270" targetNode="P_62F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_259F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10270_I" deadCode="true" sourceNode="P_60F10270" targetNode="P_61F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_262F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10270_O" deadCode="true" sourceNode="P_60F10270" targetNode="P_62F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_262F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10270_I" deadCode="true" sourceNode="P_60F10270" targetNode="P_61F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_265F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10270_O" deadCode="true" sourceNode="P_60F10270" targetNode="P_62F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_265F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_269F10270_I" deadCode="true" sourceNode="P_60F10270" targetNode="P_61F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_269F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_269F10270_O" deadCode="true" sourceNode="P_60F10270" targetNode="P_62F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_269F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_272F10270_I" deadCode="true" sourceNode="P_60F10270" targetNode="P_61F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_272F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_272F10270_O" deadCode="true" sourceNode="P_60F10270" targetNode="P_62F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_272F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10270_I" deadCode="true" sourceNode="P_60F10270" targetNode="P_61F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_276F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10270_O" deadCode="true" sourceNode="P_60F10270" targetNode="P_62F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_276F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10270_I" deadCode="true" sourceNode="P_60F10270" targetNode="P_61F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_280F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10270_O" deadCode="true" sourceNode="P_60F10270" targetNode="P_62F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_280F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10270_I" deadCode="true" sourceNode="P_60F10270" targetNode="P_61F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_284F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10270_O" deadCode="true" sourceNode="P_60F10270" targetNode="P_62F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_284F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10270_I" deadCode="false" sourceNode="P_52F10270" targetNode="P_64F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_289F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10270_O" deadCode="false" sourceNode="P_52F10270" targetNode="P_65F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_289F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10270_I" deadCode="false" sourceNode="P_52F10270" targetNode="P_64F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_292F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10270_O" deadCode="false" sourceNode="P_52F10270" targetNode="P_65F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_292F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_295F10270_I" deadCode="false" sourceNode="P_52F10270" targetNode="P_64F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_295F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_295F10270_O" deadCode="false" sourceNode="P_52F10270" targetNode="P_65F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_295F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_298F10270_I" deadCode="false" sourceNode="P_52F10270" targetNode="P_64F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_298F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_298F10270_O" deadCode="false" sourceNode="P_52F10270" targetNode="P_65F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_298F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10270_I" deadCode="false" sourceNode="P_52F10270" targetNode="P_64F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_301F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10270_O" deadCode="false" sourceNode="P_52F10270" targetNode="P_65F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_301F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_305F10270_I" deadCode="false" sourceNode="P_52F10270" targetNode="P_64F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_305F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_305F10270_O" deadCode="false" sourceNode="P_52F10270" targetNode="P_65F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_305F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10270_I" deadCode="false" sourceNode="P_52F10270" targetNode="P_64F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_308F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10270_O" deadCode="false" sourceNode="P_52F10270" targetNode="P_65F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_308F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_312F10270_I" deadCode="false" sourceNode="P_52F10270" targetNode="P_64F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_312F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_312F10270_O" deadCode="false" sourceNode="P_52F10270" targetNode="P_65F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_312F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_316F10270_I" deadCode="false" sourceNode="P_52F10270" targetNode="P_64F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_316F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_316F10270_O" deadCode="false" sourceNode="P_52F10270" targetNode="P_65F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_316F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_320F10270_I" deadCode="false" sourceNode="P_52F10270" targetNode="P_64F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_320F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_320F10270_O" deadCode="false" sourceNode="P_52F10270" targetNode="P_65F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_320F10270"/>
	</edges>
	<edges id="P_52F10270P_53F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10270" targetNode="P_53F10270"/>
	<edges id="P_45F10270P_46F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10270" targetNode="P_46F10270"/>
	<edges id="P_47F10270P_48F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10270" targetNode="P_48F10270"/>
	<edges id="P_54F10270P_55F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10270" targetNode="P_55F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_329F10270_I" deadCode="false" sourceNode="P_10F10270" targetNode="P_66F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_329F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_329F10270_O" deadCode="false" sourceNode="P_10F10270" targetNode="P_67F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_329F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_331F10270_I" deadCode="false" sourceNode="P_10F10270" targetNode="P_68F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_331F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_331F10270_O" deadCode="false" sourceNode="P_10F10270" targetNode="P_69F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_331F10270"/>
	</edges>
	<edges id="P_10F10270P_11F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10270" targetNode="P_11F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10270_I" deadCode="true" sourceNode="P_66F10270" targetNode="P_61F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_336F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10270_O" deadCode="true" sourceNode="P_66F10270" targetNode="P_62F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_336F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_341F10270_I" deadCode="true" sourceNode="P_66F10270" targetNode="P_61F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_341F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_341F10270_O" deadCode="true" sourceNode="P_66F10270" targetNode="P_62F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_341F10270"/>
	</edges>
	<edges id="P_66F10270P_67F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10270" targetNode="P_67F10270"/>
	<edges id="P_68F10270P_69F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10270" targetNode="P_69F10270"/>
	<edges id="P_61F10270P_62F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10270" targetNode="P_62F10270"/>
	<edges xsi:type="cbl:PerformEdge" id="S_370F10270_I" deadCode="false" sourceNode="P_64F10270" targetNode="P_72F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_370F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_370F10270_O" deadCode="false" sourceNode="P_64F10270" targetNode="P_73F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_370F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_371F10270_I" deadCode="false" sourceNode="P_64F10270" targetNode="P_74F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_371F10270"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_371F10270_O" deadCode="false" sourceNode="P_64F10270" targetNode="P_75F10270">
		<representations href="../../../cobol/LDBSF510.cbl.cobModel#S_371F10270"/>
	</edges>
	<edges id="P_64F10270P_65F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10270" targetNode="P_65F10270"/>
	<edges id="P_72F10270P_73F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10270" targetNode="P_73F10270"/>
	<edges id="P_74F10270P_75F10270" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10270" targetNode="P_75F10270"/>
	<edges xsi:type="cbl:DataEdge" id="S_67F10270_POS1" deadCode="false" targetNode="P_14F10270" sourceNode="DB2_POLI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10270"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_75F10270_POS1" deadCode="false" targetNode="P_16F10270" sourceNode="DB2_POLI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_75F10270"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_78F10270_POS1" deadCode="false" targetNode="P_18F10270" sourceNode="DB2_POLI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10270"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_85F10270_POS1" deadCode="false" targetNode="P_22F10270" sourceNode="DB2_POLI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_85F10270"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_103F10270_POS1" deadCode="false" targetNode="P_24F10270" sourceNode="DB2_POLI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_103F10270"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10270_POS1" deadCode="false" targetNode="P_26F10270" sourceNode="DB2_POLI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10270"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_114F10270_POS1" deadCode="false" targetNode="P_28F10270" sourceNode="DB2_POLI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10270"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_121F10270_POS1" deadCode="false" targetNode="P_32F10270" sourceNode="DB2_POLI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_121F10270"></representations>
	</edges>
</Package>

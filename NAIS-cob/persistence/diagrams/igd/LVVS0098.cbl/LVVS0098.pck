<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0098" cbl:id="LVVS0098" xsi:id="LVVS0098" packageRef="LVVS0098.igd#LVVS0098" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0098_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10346" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0098.cbl.cobModel#SC_1F10346"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10346" deadCode="false" name="PROGRAM_LVVS0098_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0098.cbl.cobModel#P_1F10346"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10346" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0098.cbl.cobModel#P_2F10346"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10346" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0098.cbl.cobModel#P_3F10346"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10346" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0098.cbl.cobModel#P_4F10346"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10346" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0098.cbl.cobModel#P_5F10346"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10346" deadCode="false" name="S1110-LEGGI-PRESTITO">
				<representations href="../../../cobol/LVVS0098.cbl.cobModel#P_8F10346"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10346" deadCode="false" name="S1110-EX">
				<representations href="../../../cobol/LVVS0098.cbl.cobModel#P_9F10346"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10346" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0098.cbl.cobModel#P_6F10346"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10346" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0098.cbl.cobModel#P_7F10346"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10346" deadCode="false" name="S1255-CALCOLA-IMPORTO">
				<representations href="../../../cobol/LVVS0098.cbl.cobModel#P_10F10346"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10346" deadCode="false" name="S1255-EX">
				<representations href="../../../cobol/LVVS0098.cbl.cobModel#P_11F10346"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS6160" name="LDBS6160">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10232"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS6150" name="LDBS6150">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10231"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10346P_1F10346" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10346" targetNode="P_1F10346"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10346_I" deadCode="false" sourceNode="P_1F10346" targetNode="P_2F10346">
		<representations href="../../../cobol/LVVS0098.cbl.cobModel#S_1F10346"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10346_O" deadCode="false" sourceNode="P_1F10346" targetNode="P_3F10346">
		<representations href="../../../cobol/LVVS0098.cbl.cobModel#S_1F10346"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10346_I" deadCode="false" sourceNode="P_1F10346" targetNode="P_4F10346">
		<representations href="../../../cobol/LVVS0098.cbl.cobModel#S_2F10346"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10346_O" deadCode="false" sourceNode="P_1F10346" targetNode="P_5F10346">
		<representations href="../../../cobol/LVVS0098.cbl.cobModel#S_2F10346"/>
	</edges>
	<edges id="P_2F10346P_3F10346" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10346" targetNode="P_3F10346"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10346_I" deadCode="false" sourceNode="P_4F10346" targetNode="P_6F10346">
		<representations href="../../../cobol/LVVS0098.cbl.cobModel#S_11F10346"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10346_O" deadCode="false" sourceNode="P_4F10346" targetNode="P_7F10346">
		<representations href="../../../cobol/LVVS0098.cbl.cobModel#S_11F10346"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10346_I" deadCode="false" sourceNode="P_4F10346" targetNode="P_8F10346">
		<representations href="../../../cobol/LVVS0098.cbl.cobModel#S_13F10346"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10346_O" deadCode="false" sourceNode="P_4F10346" targetNode="P_9F10346">
		<representations href="../../../cobol/LVVS0098.cbl.cobModel#S_13F10346"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10346_I" deadCode="false" sourceNode="P_4F10346" targetNode="P_10F10346">
		<representations href="../../../cobol/LVVS0098.cbl.cobModel#S_16F10346"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10346_O" deadCode="false" sourceNode="P_4F10346" targetNode="P_11F10346">
		<representations href="../../../cobol/LVVS0098.cbl.cobModel#S_16F10346"/>
	</edges>
	<edges id="P_4F10346P_5F10346" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10346" targetNode="P_5F10346"/>
	<edges id="P_8F10346P_9F10346" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10346" targetNode="P_9F10346"/>
	<edges id="P_6F10346P_7F10346" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10346" targetNode="P_7F10346"/>
	<edges id="P_10F10346P_11F10346" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10346" targetNode="P_11F10346"/>
	<edges xsi:type="cbl:CallEdge" id="S_33F10346" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_8F10346" targetNode="LDBS6160">
		<representations href="../../../cobol/../importantStmts.cobModel#S_33F10346"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_63F10346" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_10F10346" targetNode="LDBS6150">
		<representations href="../../../cobol/../importantStmts.cobModel#S_63F10346"></representations>
	</edges>
</Package>

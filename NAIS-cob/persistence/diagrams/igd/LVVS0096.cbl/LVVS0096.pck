<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0096" cbl:id="LVVS0096" xsi:id="LVVS0096" packageRef="LVVS0096.igd#LVVS0096" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0096_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10344" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0096.cbl.cobModel#SC_1F10344"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10344" deadCode="false" name="PROGRAM_LVVS0096_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0096.cbl.cobModel#P_1F10344"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10344" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0096.cbl.cobModel#P_2F10344"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10344" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0096.cbl.cobModel#P_3F10344"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10344" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0096.cbl.cobModel#P_4F10344"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10344" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0096.cbl.cobModel#P_5F10344"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10344" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0096.cbl.cobModel#P_8F10344"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10344" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0096.cbl.cobModel#P_9F10344"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10344" deadCode="false" name="S1250-CALCOLA-DATA1">
				<representations href="../../../cobol/LVVS0096.cbl.cobModel#P_10F10344"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10344" deadCode="false" name="S1250-EX">
				<representations href="../../../cobol/LVVS0096.cbl.cobModel#P_11F10344"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10344" deadCode="false" name="S1260-CALCOLA-DIFF">
				<representations href="../../../cobol/LVVS0096.cbl.cobModel#P_12F10344"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10344" deadCode="false" name="S1260-EX">
				<representations href="../../../cobol/LVVS0096.cbl.cobModel#P_13F10344"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10344" deadCode="false" name="S1265-CALCOLA-DIFF-1">
				<representations href="../../../cobol/LVVS0096.cbl.cobModel#P_14F10344"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10344" deadCode="false" name="S1265-EX">
				<representations href="../../../cobol/LVVS0096.cbl.cobModel#P_15F10344"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10344" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0096.cbl.cobModel#P_6F10344"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10344" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0096.cbl.cobModel#P_7F10344"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS4010" name="LDBS4010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10206"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0010" name="LCCS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10122"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10344P_1F10344" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10344" targetNode="P_1F10344"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10344_I" deadCode="false" sourceNode="P_1F10344" targetNode="P_2F10344">
		<representations href="../../../cobol/LVVS0096.cbl.cobModel#S_1F10344"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10344_O" deadCode="false" sourceNode="P_1F10344" targetNode="P_3F10344">
		<representations href="../../../cobol/LVVS0096.cbl.cobModel#S_1F10344"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10344_I" deadCode="false" sourceNode="P_1F10344" targetNode="P_4F10344">
		<representations href="../../../cobol/LVVS0096.cbl.cobModel#S_2F10344"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10344_O" deadCode="false" sourceNode="P_1F10344" targetNode="P_5F10344">
		<representations href="../../../cobol/LVVS0096.cbl.cobModel#S_2F10344"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10344_I" deadCode="false" sourceNode="P_1F10344" targetNode="P_6F10344">
		<representations href="../../../cobol/LVVS0096.cbl.cobModel#S_3F10344"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10344_O" deadCode="false" sourceNode="P_1F10344" targetNode="P_7F10344">
		<representations href="../../../cobol/LVVS0096.cbl.cobModel#S_3F10344"/>
	</edges>
	<edges id="P_2F10344P_3F10344" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10344" targetNode="P_3F10344"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10344_I" deadCode="false" sourceNode="P_4F10344" targetNode="P_8F10344">
		<representations href="../../../cobol/LVVS0096.cbl.cobModel#S_11F10344"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10344_O" deadCode="false" sourceNode="P_4F10344" targetNode="P_9F10344">
		<representations href="../../../cobol/LVVS0096.cbl.cobModel#S_11F10344"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10344_I" deadCode="false" sourceNode="P_4F10344" targetNode="P_10F10344">
		<representations href="../../../cobol/LVVS0096.cbl.cobModel#S_13F10344"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10344_O" deadCode="false" sourceNode="P_4F10344" targetNode="P_11F10344">
		<representations href="../../../cobol/LVVS0096.cbl.cobModel#S_13F10344"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10344_I" deadCode="false" sourceNode="P_4F10344" targetNode="P_12F10344">
		<representations href="../../../cobol/LVVS0096.cbl.cobModel#S_15F10344"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10344_O" deadCode="false" sourceNode="P_4F10344" targetNode="P_13F10344">
		<representations href="../../../cobol/LVVS0096.cbl.cobModel#S_15F10344"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10344_I" deadCode="false" sourceNode="P_4F10344" targetNode="P_14F10344">
		<representations href="../../../cobol/LVVS0096.cbl.cobModel#S_17F10344"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10344_O" deadCode="false" sourceNode="P_4F10344" targetNode="P_15F10344">
		<representations href="../../../cobol/LVVS0096.cbl.cobModel#S_17F10344"/>
	</edges>
	<edges id="P_4F10344P_5F10344" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10344" targetNode="P_5F10344"/>
	<edges id="P_8F10344P_9F10344" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10344" targetNode="P_9F10344"/>
	<edges id="P_10F10344P_11F10344" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10344" targetNode="P_11F10344"/>
	<edges id="P_12F10344P_13F10344" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10344" targetNode="P_13F10344"/>
	<edges id="P_14F10344P_15F10344" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10344" targetNode="P_15F10344"/>
	<edges xsi:type="cbl:CallEdge" id="S_26F10344" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_10F10344" targetNode="LDBS4010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_26F10344"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_44F10344" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_12F10344" targetNode="LCCS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_44F10344"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_57F10344" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_14F10344" targetNode="LCCS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_57F10344"></representations>
	</edges>
</Package>

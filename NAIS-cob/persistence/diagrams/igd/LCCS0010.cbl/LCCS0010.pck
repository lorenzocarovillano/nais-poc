<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LCCS0010" cbl:id="LCCS0010" xsi:id="LCCS0010" packageRef="LCCS0010.igd#LCCS0010" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LCCS0010_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10122" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LCCS0010.cbl.cobModel#SC_1F10122"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10122" deadCode="false" name="INIZIO">
				<representations href="../../../cobol/LCCS0010.cbl.cobModel#P_1F10122"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10122" deadCode="false" name="FINE-TEST">
				<representations href="../../../cobol/LCCS0010.cbl.cobModel#P_2F10122"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10122" deadCode="false" name="TEST-ANNI">
				<representations href="../../../cobol/LCCS0010.cbl.cobModel#P_3F10122"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10122" deadCode="false" name="IMPOSTA-DATA-SUP">
				<representations href="../../../cobol/LCCS0010.cbl.cobModel#P_4F10122"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_4F10122" deadCode="false" name="CALCOLO-GIORNI">
			<representations href="../../../cobol/LCCS0010.cbl.cobModel#SC_4F10122"/>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10122" deadCode="false" name="CALCOLO-GIORNI_FIRST_SENTENCES">
				<representations href="../../../cobol/LCCS0010.cbl.cobModel#P_5F10122"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10122" deadCode="false" name="CALCOLO-GIORNI-999">
				<representations href="../../../cobol/LCCS0010.cbl.cobModel#P_6F10122"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_3F10122" deadCode="false" name="CALCOLO-MESI">
			<representations href="../../../cobol/LCCS0010.cbl.cobModel#SC_3F10122"/>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10122" deadCode="false" name="CALCOLO-MESI_FIRST_SENTENCES">
				<representations href="../../../cobol/LCCS0010.cbl.cobModel#P_7F10122"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10122" deadCode="false" name="CALCOLO-MESI-999">
				<representations href="../../../cobol/LCCS0010.cbl.cobModel#P_8F10122"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_2F10122" deadCode="false" name="FINE">
			<representations href="../../../cobol/LCCS0010.cbl.cobModel#SC_2F10122"/>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10122" deadCode="false" name="FINE_FIRST_SENTENCES">
				<representations href="../../../cobol/LCCS0010.cbl.cobModel#P_9F10122"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10122P_1F10122" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10122" targetNode="P_1F10122"/>
	<edges id="SC_4F10122P_5F10122" xsi:type="cbl:FallThroughEdge" sourceNode="SC_4F10122" targetNode="P_5F10122"/>
	<edges id="SC_3F10122P_7F10122" xsi:type="cbl:FallThroughEdge" sourceNode="SC_3F10122" targetNode="P_7F10122"/>
	<edges id="SC_2F10122P_9F10122" xsi:type="cbl:FallThroughEdge" sourceNode="SC_2F10122" targetNode="P_9F10122"/>
	<edges xsi:type="cbl:GotoEdge" id="S_4F10122_GTP_1" deadCode="false" sourceNode="P_1F10122" targetNode="SC_2F10122">
		<representations href="../../../cobol/LCCS0010.cbl.cobModel#S_4F10122"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_7F10122_GTP_1" deadCode="false" sourceNode="P_1F10122" targetNode="SC_2F10122">
		<representations href="../../../cobol/LCCS0010.cbl.cobModel#S_7F10122"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_19F10122_GTP_1" deadCode="false" sourceNode="P_1F10122" targetNode="SC_2F10122">
		<representations href="../../../cobol/LCCS0010.cbl.cobModel#S_19F10122"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_22F10122_GTP_1" deadCode="false" sourceNode="P_1F10122" targetNode="SC_2F10122">
		<representations href="../../../cobol/LCCS0010.cbl.cobModel#S_22F10122"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_24F10122_GTP_1" deadCode="false" sourceNode="P_1F10122" targetNode="SC_2F10122">
		<representations href="../../../cobol/LCCS0010.cbl.cobModel#S_24F10122"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_26F10122_GTP_1" deadCode="false" sourceNode="P_1F10122" targetNode="P_2F10122">
		<representations href="../../../cobol/LCCS0010.cbl.cobModel#S_26F10122"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_28F10122_GTP_1" deadCode="false" sourceNode="P_1F10122" targetNode="P_2F10122">
		<representations href="../../../cobol/LCCS0010.cbl.cobModel#S_28F10122"/>
	</edges>
	<edges id="P_1F10122P_2F10122" xsi:type="cbl:FallThroughEdge" sourceNode="P_1F10122" targetNode="P_2F10122"/>
	<edges xsi:type="cbl:GotoEdge" id="S_39F10122_GTP_1" deadCode="false" sourceNode="P_2F10122" targetNode="SC_2F10122">
		<representations href="../../../cobol/LCCS0010.cbl.cobModel#S_39F10122"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10122_I" deadCode="false" sourceNode="P_2F10122" targetNode="SC_3F10122">
		<representations href="../../../cobol/LCCS0010.cbl.cobModel#S_38F10122"/>
	</edges>
	<edges id="P_2F10122P_3F10122" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10122" targetNode="P_3F10122"/>
	<edges xsi:type="cbl:GotoEdge" id="S_41F10122_GTP_1" deadCode="false" sourceNode="P_3F10122" targetNode="P_4F10122">
		<representations href="../../../cobol/LCCS0010.cbl.cobModel#S_41F10122"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_48F10122_GTP_1" deadCode="false" sourceNode="P_3F10122" targetNode="P_3F10122">
		<representations href="../../../cobol/LCCS0010.cbl.cobModel#S_48F10122"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10122_I" deadCode="false" sourceNode="P_3F10122" targetNode="SC_4F10122">
		<representations href="../../../cobol/LCCS0010.cbl.cobModel#S_44F10122"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_51F10122_GTP_1" deadCode="false" sourceNode="P_4F10122" targetNode="SC_2F10122">
		<representations href="../../../cobol/LCCS0010.cbl.cobModel#S_51F10122"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10122_I" deadCode="false" sourceNode="P_4F10122" targetNode="SC_4F10122">
		<representations href="../../../cobol/LCCS0010.cbl.cobModel#S_50F10122"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_54F10122_GTP_1" deadCode="false" sourceNode="P_5F10122" targetNode="P_6F10122">
		<representations href="../../../cobol/LCCS0010.cbl.cobModel#S_54F10122"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_58F10122_GTP_1" deadCode="false" sourceNode="P_5F10122" targetNode="SC_4F10122">
		<representations href="../../../cobol/LCCS0010.cbl.cobModel#S_58F10122"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_63F10122_GTP_1" deadCode="false" sourceNode="P_7F10122" targetNode="SC_3F10122">
		<representations href="../../../cobol/LCCS0010.cbl.cobModel#S_63F10122"/>
	</edges>
	<edges id="P_7F10122P_8F10122" xsi:type="cbl:FallThroughEdge" sourceNode="P_7F10122" targetNode="P_8F10122"/>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IDSS0140" cbl:id="IDSS0140" xsi:id="IDSS0140" packageRef="IDSS0140.igd#IDSS0140" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IDSS0140_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10102" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IDSS0140.cbl.cobModel#SC_1F10102"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10102" deadCode="false" name="000-PRINCIPALE">
				<representations href="../../../cobol/IDSS0140.cbl.cobModel#P_1F10102"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10102" deadCode="false" name="999-FINE">
				<representations href="../../../cobol/IDSS0140.cbl.cobModel#P_4F10102"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10102" deadCode="false" name="200-VALORIZZA-CAMPI">
				<representations href="../../../cobol/IDSS0140.cbl.cobModel#P_2F10102"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10102" deadCode="false" name="200-VALORIZZA-CAMPI-EX">
				<representations href="../../../cobol/IDSS0140.cbl.cobModel#P_3F10102"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10102P_1F10102" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10102" targetNode="P_1F10102"/>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10102_I" deadCode="false" sourceNode="P_1F10102" targetNode="P_2F10102">
		<representations href="../../../cobol/IDSS0140.cbl.cobModel#S_12F10102"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10102_O" deadCode="false" sourceNode="P_1F10102" targetNode="P_3F10102">
		<representations href="../../../cobol/IDSS0140.cbl.cobModel#S_12F10102"/>
	</edges>
	<edges id="P_1F10102P_4F10102" xsi:type="cbl:FallThroughEdge" sourceNode="P_1F10102" targetNode="P_4F10102"/>
	<edges id="P_2F10102P_3F10102" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10102" targetNode="P_3F10102"/>
</Package>

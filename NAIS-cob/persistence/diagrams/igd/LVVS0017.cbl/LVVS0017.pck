<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0017" cbl:id="LVVS0017" xsi:id="LVVS0017" packageRef="LVVS0017.igd#LVVS0017" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0017_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10316" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0017.cbl.cobModel#SC_1F10316"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10316" deadCode="false" name="PROGRAM_LVVS0017_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0017.cbl.cobModel#P_1F10316"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10316" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0017.cbl.cobModel#P_2F10316"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10316" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0017.cbl.cobModel#P_3F10316"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10316" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0017.cbl.cobModel#P_4F10316"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10316" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0017.cbl.cobModel#P_5F10316"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10316" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0017.cbl.cobModel#P_8F10316"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10316" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0017.cbl.cobModel#P_9F10316"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10316" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS0017.cbl.cobModel#P_10F10316"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10316" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS0017.cbl.cobModel#P_11F10316"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10316" deadCode="false" name="S1300-CALL-LVVS0000">
				<representations href="../../../cobol/LVVS0017.cbl.cobModel#P_12F10316"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10316" deadCode="false" name="S1300-CALL-LVVS0000-EX">
				<representations href="../../../cobol/LVVS0017.cbl.cobModel#P_13F10316"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10316" deadCode="false" name="S2027-RAN-GAR">
				<representations href="../../../cobol/LVVS0017.cbl.cobModel#P_14F10316"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10316" deadCode="false" name="S2027-EX">
				<representations href="../../../cobol/LVVS0017.cbl.cobModel#P_15F10316"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10316" deadCode="false" name="S3027-RAN-POL">
				<representations href="../../../cobol/LVVS0017.cbl.cobModel#P_16F10316"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10316" deadCode="false" name="S3027-EX">
				<representations href="../../../cobol/LVVS0017.cbl.cobModel#P_17F10316"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10316" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0017.cbl.cobModel#P_6F10316"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10316" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0017.cbl.cobModel#P_7F10316"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LVVS0000" name="LVVS0000">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10304"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10316P_1F10316" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10316" targetNode="P_1F10316"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10316_I" deadCode="false" sourceNode="P_1F10316" targetNode="P_2F10316">
		<representations href="../../../cobol/LVVS0017.cbl.cobModel#S_1F10316"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10316_O" deadCode="false" sourceNode="P_1F10316" targetNode="P_3F10316">
		<representations href="../../../cobol/LVVS0017.cbl.cobModel#S_1F10316"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10316_I" deadCode="false" sourceNode="P_1F10316" targetNode="P_4F10316">
		<representations href="../../../cobol/LVVS0017.cbl.cobModel#S_2F10316"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10316_O" deadCode="false" sourceNode="P_1F10316" targetNode="P_5F10316">
		<representations href="../../../cobol/LVVS0017.cbl.cobModel#S_2F10316"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10316_I" deadCode="false" sourceNode="P_1F10316" targetNode="P_6F10316">
		<representations href="../../../cobol/LVVS0017.cbl.cobModel#S_3F10316"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10316_O" deadCode="false" sourceNode="P_1F10316" targetNode="P_7F10316">
		<representations href="../../../cobol/LVVS0017.cbl.cobModel#S_3F10316"/>
	</edges>
	<edges id="P_2F10316P_3F10316" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10316" targetNode="P_3F10316"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10316_I" deadCode="false" sourceNode="P_4F10316" targetNode="P_8F10316">
		<representations href="../../../cobol/LVVS0017.cbl.cobModel#S_10F10316"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10316_O" deadCode="false" sourceNode="P_4F10316" targetNode="P_9F10316">
		<representations href="../../../cobol/LVVS0017.cbl.cobModel#S_10F10316"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10316_I" deadCode="false" sourceNode="P_4F10316" targetNode="P_10F10316">
		<representations href="../../../cobol/LVVS0017.cbl.cobModel#S_12F10316"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10316_O" deadCode="false" sourceNode="P_4F10316" targetNode="P_11F10316">
		<representations href="../../../cobol/LVVS0017.cbl.cobModel#S_12F10316"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10316_I" deadCode="false" sourceNode="P_4F10316" targetNode="P_12F10316">
		<representations href="../../../cobol/LVVS0017.cbl.cobModel#S_14F10316"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10316_O" deadCode="false" sourceNode="P_4F10316" targetNode="P_13F10316">
		<representations href="../../../cobol/LVVS0017.cbl.cobModel#S_14F10316"/>
	</edges>
	<edges id="P_4F10316P_5F10316" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10316" targetNode="P_5F10316"/>
	<edges id="P_8F10316P_9F10316" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10316" targetNode="P_9F10316"/>
	<edges id="P_10F10316P_11F10316" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10316" targetNode="P_11F10316"/>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10316_I" deadCode="false" sourceNode="P_12F10316" targetNode="P_14F10316">
		<representations href="../../../cobol/LVVS0017.cbl.cobModel#S_40F10316"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10316_O" deadCode="false" sourceNode="P_12F10316" targetNode="P_15F10316">
		<representations href="../../../cobol/LVVS0017.cbl.cobModel#S_40F10316"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10316_I" deadCode="false" sourceNode="P_12F10316" targetNode="P_16F10316">
		<representations href="../../../cobol/LVVS0017.cbl.cobModel#S_41F10316"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10316_O" deadCode="false" sourceNode="P_12F10316" targetNode="P_17F10316">
		<representations href="../../../cobol/LVVS0017.cbl.cobModel#S_41F10316"/>
	</edges>
	<edges id="P_12F10316P_13F10316" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10316" targetNode="P_13F10316"/>
	<edges id="P_14F10316P_15F10316" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10316" targetNode="P_15F10316"/>
	<edges id="P_16F10316P_17F10316" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10316" targetNode="P_17F10316"/>
	<edges xsi:type="cbl:CallEdge" id="S_44F10316" deadCode="false" name="Dynamic WK-LVVS0000" sourceNode="P_12F10316" targetNode="LVVS0000">
		<representations href="../../../cobol/../importantStmts.cobModel#S_44F10316"></representations>
	</edges>
</Package>

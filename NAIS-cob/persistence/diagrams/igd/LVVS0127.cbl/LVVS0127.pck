<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0127" cbl:id="LVVS0127" xsi:id="LVVS0127" packageRef="LVVS0127.igd#LVVS0127" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0127_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10354" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0127.cbl.cobModel#SC_1F10354"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10354" deadCode="false" name="PROGRAM_LVVS0127_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_1F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10354" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_2F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10354" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_3F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10354" deadCode="false" name="S0001-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_8F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10354" deadCode="false" name="S0001-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_9F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10354" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_4F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10354" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_5F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10354" deadCode="false" name="S1100-LETTURA-POG">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_10F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10354" deadCode="false" name="EX-S1100">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_11F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10354" deadCode="false" name="S10100-VALIDA-FINE-MESE">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_20F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10354" deadCode="false" name="S10100-VALIDA-FINE-MESE-EX">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_21F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10354" deadCode="false" name="S1110-LETTURA-POLI">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_18F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10354" deadCode="false" name="EX-S1110">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_19F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10354" deadCode="false" name="S1120-CALCOLO-DATA">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_12F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10354" deadCode="false" name="EX-S1120">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_13F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10354" deadCode="false" name="S1121-CALL-LCCS0003">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_22F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10354" deadCode="false" name="EX-S1121">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_23F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10354" deadCode="false" name="S1130-LETTURA-ADESIONE">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_16F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10354" deadCode="false" name="EX-S1130">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_17F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10354" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_6F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10354" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_7F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10354" deadCode="false" name="S10000-CTRL-FINE-MESE">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_14F10354"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10354" deadCode="false" name="S10000-CTRL-FINE-MESE-EX">
				<representations href="../../../cobol/LVVS0127.cbl.cobModel#P_15F10354"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS1130" name="LDBS1130">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10151"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0004" name="LCCS0004">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10120"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSPOL0" name="IDBSPOL0">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10078"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0003" name="LCCS0003">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10119"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSADE0" name="IDBSADE0">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10016"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10354P_1F10354" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10354" targetNode="P_1F10354"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10354_I" deadCode="false" sourceNode="P_1F10354" targetNode="P_2F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_1F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10354_O" deadCode="false" sourceNode="P_1F10354" targetNode="P_3F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_1F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10354_I" deadCode="false" sourceNode="P_1F10354" targetNode="P_4F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_2F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10354_O" deadCode="false" sourceNode="P_1F10354" targetNode="P_5F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_2F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10354_I" deadCode="false" sourceNode="P_1F10354" targetNode="P_6F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_3F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10354_O" deadCode="false" sourceNode="P_1F10354" targetNode="P_7F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_3F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10354_I" deadCode="false" sourceNode="P_2F10354" targetNode="P_8F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_8F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10354_O" deadCode="false" sourceNode="P_2F10354" targetNode="P_9F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_8F10354"/>
	</edges>
	<edges id="P_2F10354P_3F10354" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10354" targetNode="P_3F10354"/>
	<edges id="P_8F10354P_9F10354" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10354" targetNode="P_9F10354"/>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10354_I" deadCode="false" sourceNode="P_4F10354" targetNode="P_10F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_14F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10354_O" deadCode="false" sourceNode="P_4F10354" targetNode="P_11F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_14F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10354_I" deadCode="false" sourceNode="P_4F10354" targetNode="P_10F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_17F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10354_O" deadCode="false" sourceNode="P_4F10354" targetNode="P_11F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_17F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10354_I" deadCode="false" sourceNode="P_4F10354" targetNode="P_12F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_19F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10354_O" deadCode="false" sourceNode="P_4F10354" targetNode="P_13F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_19F10354"/>
	</edges>
	<edges id="P_4F10354P_5F10354" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10354" targetNode="P_5F10354"/>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10354_I" deadCode="false" sourceNode="P_10F10354" targetNode="P_14F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_41F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10354_O" deadCode="false" sourceNode="P_10F10354" targetNode="P_15F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_41F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10354_I" deadCode="false" sourceNode="P_10F10354" targetNode="P_12F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_46F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10354_O" deadCode="false" sourceNode="P_10F10354" targetNode="P_13F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_46F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10354_I" deadCode="false" sourceNode="P_10F10354" targetNode="P_12F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_53F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10354_O" deadCode="false" sourceNode="P_10F10354" targetNode="P_13F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_53F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10354_I" deadCode="false" sourceNode="P_10F10354" targetNode="P_16F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_55F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10354_O" deadCode="false" sourceNode="P_10F10354" targetNode="P_17F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_55F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10354_I" deadCode="false" sourceNode="P_10F10354" targetNode="P_14F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_69F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10354_O" deadCode="false" sourceNode="P_10F10354" targetNode="P_15F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_69F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10354_I" deadCode="false" sourceNode="P_10F10354" targetNode="P_12F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_74F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10354_O" deadCode="false" sourceNode="P_10F10354" targetNode="P_13F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_74F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10354_I" deadCode="false" sourceNode="P_10F10354" targetNode="P_18F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_76F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10354_O" deadCode="false" sourceNode="P_10F10354" targetNode="P_19F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_76F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10354_I" deadCode="false" sourceNode="P_10F10354" targetNode="P_14F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_91F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10354_O" deadCode="false" sourceNode="P_10F10354" targetNode="P_15F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_91F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10354_I" deadCode="false" sourceNode="P_10F10354" targetNode="P_12F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_96F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10354_O" deadCode="false" sourceNode="P_10F10354" targetNode="P_13F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_96F10354"/>
	</edges>
	<edges id="P_10F10354P_11F10354" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10354" targetNode="P_11F10354"/>
	<edges id="P_20F10354P_21F10354" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10354" targetNode="P_21F10354"/>
	<edges id="P_18F10354P_19F10354" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10354" targetNode="P_19F10354"/>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10354_I" deadCode="false" sourceNode="P_12F10354" targetNode="P_22F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_138F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10354_O" deadCode="false" sourceNode="P_12F10354" targetNode="P_23F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_138F10354"/>
	</edges>
	<edges id="P_12F10354P_13F10354" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10354" targetNode="P_13F10354"/>
	<edges id="P_22F10354P_23F10354" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10354" targetNode="P_23F10354"/>
	<edges id="P_16F10354P_17F10354" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10354" targetNode="P_17F10354"/>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10354_I" deadCode="false" sourceNode="P_14F10354" targetNode="P_20F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_173F10354"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10354_O" deadCode="false" sourceNode="P_14F10354" targetNode="P_21F10354">
		<representations href="../../../cobol/LVVS0127.cbl.cobModel#S_173F10354"/>
	</edges>
	<edges id="P_14F10354P_15F10354" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10354" targetNode="P_15F10354"/>
	<edges xsi:type="cbl:CallEdge" id="S_29F10354" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_10F10354" targetNode="LDBS1130">
		<representations href="../../../cobol/../importantStmts.cobModel#S_29F10354"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_107F10354" deadCode="false" name="Dynamic LCCS0004" sourceNode="P_20F10354" targetNode="LCCS0004">
		<representations href="../../../cobol/../importantStmts.cobModel#S_107F10354"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_119F10354" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_18F10354" targetNode="IDBSPOL0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_119F10354"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_146F10354" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_22F10354" targetNode="LCCS0003">
		<representations href="../../../cobol/../importantStmts.cobModel#S_146F10354"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_156F10354" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_16F10354" targetNode="IDBSADE0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_156F10354"></representations>
	</edges>
</Package>

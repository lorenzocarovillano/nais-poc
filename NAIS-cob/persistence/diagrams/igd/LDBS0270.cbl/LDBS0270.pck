<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS0270" cbl:id="LDBS0270" xsi:id="LDBS0270" packageRef="LDBS0270.igd#LDBS0270" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS0270_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10144" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS0270.cbl.cobModel#SC_1F10144"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10144" deadCode="false" name="PROGRAM_LDBS0270_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS0270.cbl.cobModel#P_1F10144"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10144" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS0270.cbl.cobModel#P_2F10144"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10144" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS0270.cbl.cobModel#P_3F10144"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10144" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS0270.cbl.cobModel#P_6F10144"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10144" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS0270.cbl.cobModel#P_7F10144"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10144" deadCode="false" name="A200-ELABORA-PK">
				<representations href="../../../cobol/LDBS0270.cbl.cobModel#P_4F10144"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10144" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS0270.cbl.cobModel#P_5F10144"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10144" deadCode="false" name="A210-SELECT-PK">
				<representations href="../../../cobol/LDBS0270.cbl.cobModel#P_10F10144"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10144" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS0270.cbl.cobModel#P_11F10144"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10144" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS0270.cbl.cobModel#P_12F10144"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10144" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS0270.cbl.cobModel#P_13F10144"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10144" deadCode="true" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/LDBS0270.cbl.cobModel#P_14F10144"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10144" deadCode="true" name="Z200-EX">
				<representations href="../../../cobol/LDBS0270.cbl.cobModel#P_15F10144"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10144" deadCode="false" name="Z300-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS0270.cbl.cobModel#P_8F10144"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10144" deadCode="false" name="Z300-EX">
				<representations href="../../../cobol/LDBS0270.cbl.cobModel#P_9F10144"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_VAR_FUNZ_DI_CALC" name="VAR_FUNZ_DI_CALC">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_VAR_FUNZ_DI_CALC"/>
		</children>
	</packageNode>
	<edges id="SC_1F10144P_1F10144" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10144" targetNode="P_1F10144"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10144_I" deadCode="false" sourceNode="P_1F10144" targetNode="P_2F10144">
		<representations href="../../../cobol/LDBS0270.cbl.cobModel#S_1F10144"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10144_O" deadCode="false" sourceNode="P_1F10144" targetNode="P_3F10144">
		<representations href="../../../cobol/LDBS0270.cbl.cobModel#S_1F10144"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10144_I" deadCode="false" sourceNode="P_1F10144" targetNode="P_4F10144">
		<representations href="../../../cobol/LDBS0270.cbl.cobModel#S_4F10144"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10144_O" deadCode="false" sourceNode="P_1F10144" targetNode="P_5F10144">
		<representations href="../../../cobol/LDBS0270.cbl.cobModel#S_4F10144"/>
	</edges>
	<edges id="P_2F10144P_3F10144" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10144" targetNode="P_3F10144"/>
	<edges id="P_6F10144P_7F10144" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10144" targetNode="P_7F10144"/>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10144_I" deadCode="false" sourceNode="P_4F10144" targetNode="P_8F10144">
		<representations href="../../../cobol/LDBS0270.cbl.cobModel#S_28F10144"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10144_O" deadCode="false" sourceNode="P_4F10144" targetNode="P_9F10144">
		<representations href="../../../cobol/LDBS0270.cbl.cobModel#S_28F10144"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10144_I" deadCode="false" sourceNode="P_4F10144" targetNode="P_10F10144">
		<representations href="../../../cobol/LDBS0270.cbl.cobModel#S_29F10144"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10144_O" deadCode="false" sourceNode="P_4F10144" targetNode="P_11F10144">
		<representations href="../../../cobol/LDBS0270.cbl.cobModel#S_29F10144"/>
	</edges>
	<edges id="P_4F10144P_5F10144" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10144" targetNode="P_5F10144"/>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10144_I" deadCode="false" sourceNode="P_10F10144" targetNode="P_6F10144">
		<representations href="../../../cobol/LDBS0270.cbl.cobModel#S_33F10144"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10144_O" deadCode="false" sourceNode="P_10F10144" targetNode="P_7F10144">
		<representations href="../../../cobol/LDBS0270.cbl.cobModel#S_33F10144"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10144_I" deadCode="false" sourceNode="P_10F10144" targetNode="P_12F10144">
		<representations href="../../../cobol/LDBS0270.cbl.cobModel#S_35F10144"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10144_O" deadCode="false" sourceNode="P_10F10144" targetNode="P_13F10144">
		<representations href="../../../cobol/LDBS0270.cbl.cobModel#S_35F10144"/>
	</edges>
	<edges id="P_10F10144P_11F10144" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10144" targetNode="P_11F10144"/>
	<edges id="P_12F10144P_13F10144" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10144" targetNode="P_13F10144"/>
	<edges id="P_8F10144P_9F10144" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10144" targetNode="P_9F10144"/>
	<edges xsi:type="cbl:DataEdge" id="S_32F10144_POS1" deadCode="false" targetNode="P_10F10144" sourceNode="DB2_VAR_FUNZ_DI_CALC">
		<representations href="../../../cobol/../importantStmts.cobModel#S_32F10144"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS1390" cbl:id="LDBS1390" xsi:id="LDBS1390" packageRef="LDBS1390.igd#LDBS1390" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS1390_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10157" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS1390.cbl.cobModel#SC_1F10157"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10157" deadCode="false" name="PROGRAM_LDBS1390_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_1F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10157" deadCode="false" name="A100-SELECT-MVV">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_2F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10157" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_3F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10157" deadCode="false" name="B000-ACCEDI-X-CURSORE">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_6F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10157" deadCode="false" name="B000-EX">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_7F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10157" deadCode="false" name="B100-DECLARE-CUR-SERVIZI">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_12F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10157" deadCode="false" name="B100-EX">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_13F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10157" deadCode="false" name="B200-OPEN-CUR-SERVIZI">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_14F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10157" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_15F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10157" deadCode="false" name="B250-FETCH-FIRST">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_16F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10157" deadCode="false" name="B250-EX">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_17F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10157" deadCode="false" name="B300-FETCH-CUR-SERVIZI">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_18F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10157" deadCode="false" name="B300-EX">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_19F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10157" deadCode="false" name="B350-CLOSE-CUR-SERVIZI">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_22F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10157" deadCode="false" name="B350-EX">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_23F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10157" deadCode="false" name="B400-CLOSE-CUR-SERVIZI">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_20F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10157" deadCode="false" name="B400-EX">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_21F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10157" deadCode="false" name="N000-CNTL-CAMPI-NULL">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_8F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10157" deadCode="false" name="N000-EX">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_9F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10157" deadCode="false" name="N001-IMPOSTA-OUTPUT">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_10F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10157" deadCode="false" name="N001-EX">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_11F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10157" deadCode="true" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_4F10157"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10157" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS1390.cbl.cobModel#P_5F10157"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_ANAG_DATO" name="ANAG_DATO">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_ANAG_DATO"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_MATR_VAL_VAR" name="MATR_VAL_VAR">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_MATR_VAL_VAR"/>
		</children>
	</packageNode>
	<edges id="SC_1F10157P_1F10157" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10157" targetNode="P_1F10157"/>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10157_I" deadCode="false" sourceNode="P_1F10157" targetNode="P_2F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_6F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10157_O" deadCode="false" sourceNode="P_1F10157" targetNode="P_3F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_6F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10157_I" deadCode="true" sourceNode="P_2F10157" targetNode="P_4F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_8F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10157_O" deadCode="true" sourceNode="P_2F10157" targetNode="P_5F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_8F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10157_I" deadCode="false" sourceNode="P_2F10157" targetNode="P_6F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_13F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10157_O" deadCode="false" sourceNode="P_2F10157" targetNode="P_7F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_13F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10157_I" deadCode="false" sourceNode="P_2F10157" targetNode="P_8F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_19F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10157_O" deadCode="false" sourceNode="P_2F10157" targetNode="P_9F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_19F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10157_I" deadCode="false" sourceNode="P_2F10157" targetNode="P_10F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_21F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10157_O" deadCode="false" sourceNode="P_2F10157" targetNode="P_11F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_21F10157"/>
	</edges>
	<edges id="P_2F10157P_3F10157" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10157" targetNode="P_3F10157"/>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10157_I" deadCode="false" sourceNode="P_6F10157" targetNode="P_12F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_24F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10157_O" deadCode="false" sourceNode="P_6F10157" targetNode="P_13F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_24F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10157_I" deadCode="false" sourceNode="P_6F10157" targetNode="P_14F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_25F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10157_O" deadCode="false" sourceNode="P_6F10157" targetNode="P_15F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_25F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10157_I" deadCode="false" sourceNode="P_6F10157" targetNode="P_16F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_26F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10157_O" deadCode="false" sourceNode="P_6F10157" targetNode="P_17F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_26F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10157_I" deadCode="false" sourceNode="P_6F10157" targetNode="P_18F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_28F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10157_O" deadCode="false" sourceNode="P_6F10157" targetNode="P_19F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_28F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10157_I" deadCode="false" sourceNode="P_6F10157" targetNode="P_20F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_30F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10157_O" deadCode="false" sourceNode="P_6F10157" targetNode="P_21F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_30F10157"/>
	</edges>
	<edges id="P_6F10157P_7F10157" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10157" targetNode="P_7F10157"/>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10157_I" deadCode="true" sourceNode="P_12F10157" targetNode="P_4F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_32F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10157_O" deadCode="true" sourceNode="P_12F10157" targetNode="P_5F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_32F10157"/>
	</edges>
	<edges id="P_12F10157P_13F10157" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10157" targetNode="P_13F10157"/>
	<edges id="P_14F10157P_15F10157" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10157" targetNode="P_15F10157"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10157_I" deadCode="false" sourceNode="P_16F10157" targetNode="P_8F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_53F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10157_O" deadCode="false" sourceNode="P_16F10157" targetNode="P_9F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_53F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10157_I" deadCode="false" sourceNode="P_16F10157" targetNode="P_10F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_55F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10157_O" deadCode="false" sourceNode="P_16F10157" targetNode="P_11F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_55F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10157_I" deadCode="false" sourceNode="P_16F10157" targetNode="P_22F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_57F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10157_O" deadCode="false" sourceNode="P_16F10157" targetNode="P_23F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_57F10157"/>
	</edges>
	<edges id="P_16F10157P_17F10157" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10157" targetNode="P_17F10157"/>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10157_I" deadCode="false" sourceNode="P_18F10157" targetNode="P_8F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_66F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10157_O" deadCode="false" sourceNode="P_18F10157" targetNode="P_9F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_66F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10157_I" deadCode="false" sourceNode="P_18F10157" targetNode="P_10F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_68F10157"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10157_O" deadCode="false" sourceNode="P_18F10157" targetNode="P_11F10157">
		<representations href="../../../cobol/LDBS1390.cbl.cobModel#S_68F10157"/>
	</edges>
	<edges id="P_18F10157P_19F10157" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10157" targetNode="P_19F10157"/>
	<edges id="P_22F10157P_23F10157" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10157" targetNode="P_23F10157"/>
	<edges id="P_20F10157P_21F10157" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10157" targetNode="P_21F10157"/>
	<edges id="P_8F10157P_9F10157" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10157" targetNode="P_9F10157"/>
	<edges id="P_10F10157P_11F10157" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10157" targetNode="P_11F10157"/>
	<edges id="P_4F10157P_5F10157" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10157" targetNode="P_5F10157"/>
	<edges xsi:type="cbl:DataEdge" id="S_9F10157_POS1" deadCode="false" targetNode="P_2F10157" sourceNode="DB2_ANAG_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_9F10157"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_9F10157_POS2" deadCode="false" targetNode="P_2F10157" sourceNode="DB2_MATR_VAL_VAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_9F10157"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_36F10157_POS1" deadCode="false" targetNode="P_14F10157" sourceNode="DB2_ANAG_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_36F10157"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_36F10157_POS2" deadCode="false" targetNode="P_14F10157" sourceNode="DB2_MATR_VAL_VAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_36F10157"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_44F10157_POS1" deadCode="false" targetNode="P_16F10157" sourceNode="DB2_ANAG_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_44F10157"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_44F10157_POS2" deadCode="false" targetNode="P_16F10157" sourceNode="DB2_MATR_VAL_VAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_44F10157"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_59F10157_POS1" deadCode="false" targetNode="P_18F10157" sourceNode="DB2_ANAG_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_59F10157"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_59F10157_POS2" deadCode="false" targetNode="P_18F10157" sourceNode="DB2_MATR_VAL_VAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_59F10157"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_72F10157_POS1" deadCode="false" targetNode="P_22F10157" sourceNode="DB2_ANAG_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_72F10157"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_72F10157_POS2" deadCode="false" targetNode="P_22F10157" sourceNode="DB2_MATR_VAL_VAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_72F10157"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_79F10157_POS1" deadCode="false" targetNode="P_20F10157" sourceNode="DB2_ANAG_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_79F10157"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_79F10157_POS2" deadCode="false" targetNode="P_20F10157" sourceNode="DB2_MATR_VAL_VAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_79F10157"></representations>
	</edges>
</Package>

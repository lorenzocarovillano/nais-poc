<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LOAS0870" cbl:id="LOAS0870" xsi:id="LOAS0870" packageRef="LOAS0870.igd#LOAS0870" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LOAS0870_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10295" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LOAS0870.cbl.cobModel#SC_1F10295"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10295" deadCode="false" name="PROGRAM_LOAS0870_FIRST_SENTENCES">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_1F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10295" deadCode="false" name="S00000-OPERAZ-INIZIALI">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_2F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10295" deadCode="false" name="S00000-OPERAZ-INIZIALI-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_3F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10295" deadCode="false" name="S00100-INIZIA-AREE-WS">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_8F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10295" deadCode="false" name="S00100-INIZIA-AREE-WS-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_9F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10295" deadCode="false" name="S10000-ELABORAZIONE">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_4F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10295" deadCode="false" name="S10000-ELABORAZIONE-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_5F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10295" deadCode="false" name="S10050-LEGGI-PARAM-COMP">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_10F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10295" deadCode="false" name="S10050-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_11F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10295" deadCode="false" name="S10070-IMPOSTA-PARAM-COMP">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_16F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10295" deadCode="false" name="S10070-IMPOSTA-PARAM-COMP-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_17F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10295" deadCode="false" name="S10200-CARICA-TIT-DB">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_12F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10295" deadCode="false" name="S10200-CARICA-TIT-DB-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_13F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10295" deadCode="false" name="S10209-DET-DATA-INI-CALC">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_24F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10295" deadCode="false" name="S10209-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_25F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10295" deadCode="false" name="S10210-DET-DATA-A">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_26F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10295" deadCode="false" name="S10210-DET-DATA-A-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_29F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10295" deadCode="false" name="S10211-DET-DATA-DA">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_30F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10295" deadCode="false" name="S10211-DET-DATA-DA-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_31F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10295" deadCode="false" name="S10215-DET-DATE-PUR">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_32F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10295" deadCode="false" name="S10215-DET-DATE-PUR-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_33F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10295" deadCode="false" name="S10220-CHIAMA-LCCS0003">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_27F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10295" deadCode="false" name="S10220-CHIAMA-LCCS0003-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_28F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10295" deadCode="false" name="S10230-RICERCA-TITOLO">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_22F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10295" deadCode="false" name="S10230-RICERCA-TITOLO-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_23F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10295" deadCode="false" name="RICERCA-GAR">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_36F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10295" deadCode="false" name="RICERCA-GAR-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_37F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10295" deadCode="false" name="RICERCA-PMO">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_38F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10295" deadCode="false" name="RICERCA-PMO-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_39F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10295" deadCode="false" name="S10240-IMPOSTA-TIT-CONT">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_40F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10295" deadCode="false" name="S10240-IMPOSTA-TIT-CONT-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_41F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10295" deadCode="false" name="S10250-LEGGI-TIT-CONT">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_42F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10295" deadCode="false" name="S10250-LEGGI-TIT-CONT-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_43F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10295" deadCode="false" name="S10260-CARICA-TAB-W870">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_44F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10295" deadCode="false" name="S10260-CARICA-TAB-W870-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_45F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10295" deadCode="false" name="S10500-CTRL-TIT">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_46F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10295" deadCode="false" name="S10500-CTRL-TIT-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_47F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10295" deadCode="false" name="S10600-DETERMINA-TP-RIVAL">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_14F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10295" deadCode="false" name="S10600-DETERMINA-TP-RIVAL-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_15F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10295" deadCode="false" name="S10610-ALLINEA-AREA-TIT">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_48F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10295" deadCode="false" name="S10610-ALLINEA-AREA-TIT-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_49F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10295" deadCode="false" name="S10620-RIV-PIENA-PARZ-NULLA">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_50F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10295" deadCode="false" name="S10620-RIV-PIENA-PARZ-NULLA-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_51F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10295" deadCode="false" name="S10630-LEGGE-GG-RIT-PAG">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_52F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10295" deadCode="false" name="S10630-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_53F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10295" deadCode="true" name="S90000-OPERAZ-FINALI">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_6F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10295" deadCode="false" name="S90000-OPERAZ-FINALI-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_7F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10295" deadCode="false" name="GESTIONE-ERR-STD">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_20F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10295" deadCode="false" name="GESTIONE-ERR-STD-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_21F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10295" deadCode="false" name="GESTIONE-ERR-SIST">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_34F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10295" deadCode="false" name="GESTIONE-ERR-SIST-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_35F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10295" deadCode="false" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_18F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10295" deadCode="false" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_19F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10295" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_54F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10295" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_55F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10295" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_60F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10295" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_61F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10295" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_58F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10295" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_59F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10295" deadCode="false" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_56F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10295" deadCode="false" name="EX-S0290">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_57F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10295" deadCode="true" name="S0320-OUTPUT-PRODOTTO">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_62F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10295" deadCode="true" name="EX-S0320-OUTPUT-PRODOTTO">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_67F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10295" deadCode="true" name="S0322-SALVA-ERRORE">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_63F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10295" deadCode="true" name="EX-S0322">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_64F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10295" deadCode="true" name="S0321-ERRORI-DEROGA">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_65F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10295" deadCode="true" name="EX-S0321">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_66F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10295" deadCode="true" name="S0323-SALVA-DEROGA">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_68F10295"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10295" deadCode="true" name="EX-S0323">
				<representations href="../../../cobol/LOAS0870.cbl.cobModel#P_69F10295"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0003" name="LCCS0003">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10119"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0010" name="IDSS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10099"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10295P_1F10295" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10295" targetNode="P_1F10295"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10295_I" deadCode="false" sourceNode="P_1F10295" targetNode="P_2F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_1F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10295_O" deadCode="false" sourceNode="P_1F10295" targetNode="P_3F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_1F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10295_I" deadCode="false" sourceNode="P_1F10295" targetNode="P_4F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_3F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10295_O" deadCode="false" sourceNode="P_1F10295" targetNode="P_5F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_3F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10295_I" deadCode="true" sourceNode="P_1F10295" targetNode="P_6F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_4F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10295_O" deadCode="true" sourceNode="P_1F10295" targetNode="P_7F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_4F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10295_I" deadCode="false" sourceNode="P_2F10295" targetNode="P_8F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_7F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10295_O" deadCode="false" sourceNode="P_2F10295" targetNode="P_9F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_7F10295"/>
	</edges>
	<edges id="P_2F10295P_3F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10295" targetNode="P_3F10295"/>
	<edges id="P_8F10295P_9F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10295" targetNode="P_9F10295"/>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10295_I" deadCode="false" sourceNode="P_4F10295" targetNode="P_10F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_16F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10295_O" deadCode="false" sourceNode="P_4F10295" targetNode="P_11F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_16F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10295_I" deadCode="false" sourceNode="P_4F10295" targetNode="P_12F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_19F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10295_O" deadCode="false" sourceNode="P_4F10295" targetNode="P_13F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_19F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10295_I" deadCode="false" sourceNode="P_4F10295" targetNode="P_14F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_21F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10295_O" deadCode="false" sourceNode="P_4F10295" targetNode="P_15F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_21F10295"/>
	</edges>
	<edges id="P_4F10295P_5F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10295" targetNode="P_5F10295"/>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10295_I" deadCode="false" sourceNode="P_10F10295" targetNode="P_16F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_24F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10295_O" deadCode="false" sourceNode="P_10F10295" targetNode="P_17F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_24F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10295_I" deadCode="false" sourceNode="P_10F10295" targetNode="P_18F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_25F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10295_O" deadCode="false" sourceNode="P_10F10295" targetNode="P_19F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_25F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10295_I" deadCode="false" sourceNode="P_10F10295" targetNode="P_20F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_30F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10295_O" deadCode="false" sourceNode="P_10F10295" targetNode="P_21F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_30F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10295_I" deadCode="false" sourceNode="P_10F10295" targetNode="P_20F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_36F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10295_O" deadCode="false" sourceNode="P_10F10295" targetNode="P_21F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_36F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10295_I" deadCode="false" sourceNode="P_10F10295" targetNode="P_20F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_39F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10295_O" deadCode="false" sourceNode="P_10F10295" targetNode="P_21F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_39F10295"/>
	</edges>
	<edges id="P_10F10295P_11F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10295" targetNode="P_11F10295"/>
	<edges id="P_16F10295P_17F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10295" targetNode="P_17F10295"/>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10295_I" deadCode="false" sourceNode="P_12F10295" targetNode="P_22F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_52F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_55F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10295_O" deadCode="false" sourceNode="P_12F10295" targetNode="P_23F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_52F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_55F10295"/>
	</edges>
	<edges id="P_12F10295P_13F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10295" targetNode="P_13F10295"/>
	<edges id="P_24F10295P_25F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10295" targetNode="P_25F10295"/>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10295_I" deadCode="false" sourceNode="P_26F10295" targetNode="P_27F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_73F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10295_O" deadCode="false" sourceNode="P_26F10295" targetNode="P_28F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_73F10295"/>
	</edges>
	<edges id="P_26F10295P_29F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10295" targetNode="P_29F10295"/>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10295_I" deadCode="false" sourceNode="P_30F10295" targetNode="P_27F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_86F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10295_O" deadCode="false" sourceNode="P_30F10295" targetNode="P_28F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_86F10295"/>
	</edges>
	<edges id="P_30F10295P_31F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10295" targetNode="P_31F10295"/>
	<edges id="P_32F10295P_33F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10295" targetNode="P_33F10295"/>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10295_I" deadCode="false" sourceNode="P_27F10295" targetNode="P_34F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_103F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10295_O" deadCode="false" sourceNode="P_27F10295" targetNode="P_35F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_103F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10295_I" deadCode="false" sourceNode="P_27F10295" targetNode="P_20F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_107F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10295_O" deadCode="false" sourceNode="P_27F10295" targetNode="P_21F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_107F10295"/>
	</edges>
	<edges id="P_27F10295P_28F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_27F10295" targetNode="P_28F10295"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10295_I" deadCode="false" sourceNode="P_22F10295" targetNode="P_36F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_117F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10295_O" deadCode="false" sourceNode="P_22F10295" targetNode="P_37F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_117F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10295_I" deadCode="false" sourceNode="P_22F10295" targetNode="P_38F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_118F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10295_O" deadCode="false" sourceNode="P_22F10295" targetNode="P_39F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_118F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10295_I" deadCode="false" sourceNode="P_22F10295" targetNode="P_32F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_120F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10295_O" deadCode="false" sourceNode="P_22F10295" targetNode="P_33F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_120F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10295_I" deadCode="false" sourceNode="P_22F10295" targetNode="P_24F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_121F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10295_O" deadCode="false" sourceNode="P_22F10295" targetNode="P_25F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_121F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10295_I" deadCode="false" sourceNode="P_22F10295" targetNode="P_30F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_122F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10295_O" deadCode="false" sourceNode="P_22F10295" targetNode="P_31F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_122F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10295_I" deadCode="false" sourceNode="P_22F10295" targetNode="P_26F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_123F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10295_O" deadCode="false" sourceNode="P_22F10295" targetNode="P_29F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_123F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10295_I" deadCode="false" sourceNode="P_22F10295" targetNode="P_40F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_124F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10295_O" deadCode="false" sourceNode="P_22F10295" targetNode="P_41F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_124F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10295_I" deadCode="false" sourceNode="P_22F10295" targetNode="P_42F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_125F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10295_O" deadCode="false" sourceNode="P_22F10295" targetNode="P_43F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_125F10295"/>
	</edges>
	<edges id="P_22F10295P_23F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10295" targetNode="P_23F10295"/>
	<edges id="P_36F10295P_37F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10295" targetNode="P_37F10295"/>
	<edges id="P_38F10295P_39F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10295" targetNode="P_39F10295"/>
	<edges id="P_40F10295P_41F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10295" targetNode="P_41F10295"/>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10295_I" deadCode="false" sourceNode="P_42F10295" targetNode="P_18F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_164F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10295_O" deadCode="false" sourceNode="P_42F10295" targetNode="P_19F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_164F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_176F10295_I" deadCode="false" sourceNode="P_42F10295" targetNode="P_20F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_176F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_176F10295_O" deadCode="false" sourceNode="P_42F10295" targetNode="P_21F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_176F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_178F10295_I" deadCode="false" sourceNode="P_42F10295" targetNode="P_44F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_178F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_178F10295_O" deadCode="false" sourceNode="P_42F10295" targetNode="P_45F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_178F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_182F10295_I" deadCode="false" sourceNode="P_42F10295" targetNode="P_46F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_182F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_182F10295_O" deadCode="false" sourceNode="P_42F10295" targetNode="P_47F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_182F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10295_I" deadCode="false" sourceNode="P_42F10295" targetNode="P_20F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_185F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10295_O" deadCode="false" sourceNode="P_42F10295" targetNode="P_21F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_185F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_188F10295_I" deadCode="false" sourceNode="P_42F10295" targetNode="P_20F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_188F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_188F10295_O" deadCode="false" sourceNode="P_42F10295" targetNode="P_21F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_188F10295"/>
	</edges>
	<edges id="P_42F10295P_43F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10295" targetNode="P_43F10295"/>
	<edges id="P_44F10295P_45F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10295" targetNode="P_45F10295"/>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10295_I" deadCode="false" sourceNode="P_46F10295" targetNode="P_20F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_202F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10295_O" deadCode="false" sourceNode="P_46F10295" targetNode="P_21F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_202F10295"/>
	</edges>
	<edges id="P_46F10295P_47F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10295" targetNode="P_47F10295"/>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10295_I" deadCode="false" sourceNode="P_14F10295" targetNode="P_48F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_205F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_209F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_211F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10295_O" deadCode="false" sourceNode="P_14F10295" targetNode="P_49F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_205F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_209F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_211F10295"/>
	</edges>
	<edges id="P_14F10295P_15F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10295" targetNode="P_15F10295"/>
	<edges xsi:type="cbl:PerformEdge" id="S_215F10295_I" deadCode="false" sourceNode="P_48F10295" targetNode="P_50F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_214F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_215F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_215F10295_O" deadCode="false" sourceNode="P_48F10295" targetNode="P_51F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_214F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_215F10295"/>
	</edges>
	<edges id="P_48F10295P_49F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10295" targetNode="P_49F10295"/>
	<edges xsi:type="cbl:PerformEdge" id="S_220F10295_I" deadCode="false" sourceNode="P_50F10295" targetNode="P_52F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_220F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_220F10295_O" deadCode="false" sourceNode="P_50F10295" targetNode="P_53F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_220F10295"/>
	</edges>
	<edges id="P_50F10295P_51F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10295" targetNode="P_51F10295"/>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10295_I" deadCode="false" sourceNode="P_52F10295" targetNode="P_18F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_241F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_242F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10295_O" deadCode="false" sourceNode="P_52F10295" targetNode="P_19F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_241F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_242F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_252F10295_I" deadCode="false" sourceNode="P_52F10295" targetNode="P_54F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_241F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_252F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_252F10295_O" deadCode="false" sourceNode="P_52F10295" targetNode="P_55F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_241F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_252F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_261F10295_I" deadCode="false" sourceNode="P_52F10295" targetNode="P_54F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_241F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_261F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_261F10295_O" deadCode="false" sourceNode="P_52F10295" targetNode="P_55F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_241F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_261F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_267F10295_I" deadCode="false" sourceNode="P_52F10295" targetNode="P_54F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_241F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_267F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_267F10295_O" deadCode="false" sourceNode="P_52F10295" targetNode="P_55F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_241F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_267F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_273F10295_I" deadCode="false" sourceNode="P_52F10295" targetNode="P_54F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_241F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_273F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_273F10295_O" deadCode="false" sourceNode="P_52F10295" targetNode="P_55F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_241F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_273F10295"/>
	</edges>
	<edges id="P_52F10295P_53F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10295" targetNode="P_53F10295"/>
	<edges id="P_6F10295P_7F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10295" targetNode="P_7F10295"/>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10295_I" deadCode="false" sourceNode="P_20F10295" targetNode="P_54F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_281F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10295_O" deadCode="false" sourceNode="P_20F10295" targetNode="P_55F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_281F10295"/>
	</edges>
	<edges id="P_20F10295P_21F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10295" targetNode="P_21F10295"/>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10295_I" deadCode="false" sourceNode="P_34F10295" targetNode="P_56F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_284F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10295_O" deadCode="false" sourceNode="P_34F10295" targetNode="P_57F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_284F10295"/>
	</edges>
	<edges id="P_34F10295P_35F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10295" targetNode="P_35F10295"/>
	<edges id="P_18F10295P_19F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10295" targetNode="P_19F10295"/>
	<edges xsi:type="cbl:PerformEdge" id="S_320F10295_I" deadCode="false" sourceNode="P_54F10295" targetNode="P_58F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_320F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_320F10295_O" deadCode="false" sourceNode="P_54F10295" targetNode="P_59F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_320F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_324F10295_I" deadCode="false" sourceNode="P_54F10295" targetNode="P_60F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_324F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_324F10295_O" deadCode="false" sourceNode="P_54F10295" targetNode="P_61F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_324F10295"/>
	</edges>
	<edges id="P_54F10295P_55F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10295" targetNode="P_55F10295"/>
	<edges id="P_60F10295P_61F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_60F10295" targetNode="P_61F10295"/>
	<edges id="P_58F10295P_59F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10295" targetNode="P_59F10295"/>
	<edges xsi:type="cbl:PerformEdge" id="S_360F10295_I" deadCode="false" sourceNode="P_56F10295" targetNode="P_54F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_360F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_360F10295_O" deadCode="false" sourceNode="P_56F10295" targetNode="P_55F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_360F10295"/>
	</edges>
	<edges id="P_56F10295P_57F10295" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10295" targetNode="P_57F10295"/>
	<edges xsi:type="cbl:PerformEdge" id="S_376F10295_I" deadCode="true" sourceNode="P_62F10295" targetNode="P_63F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_374F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_376F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_376F10295_O" deadCode="true" sourceNode="P_62F10295" targetNode="P_64F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_374F10295"/>
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_376F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_377F10295_I" deadCode="true" sourceNode="P_62F10295" targetNode="P_65F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_377F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_377F10295_O" deadCode="true" sourceNode="P_62F10295" targetNode="P_66F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_377F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_382F10295_I" deadCode="true" sourceNode="P_63F10295" targetNode="P_54F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_382F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_382F10295_O" deadCode="true" sourceNode="P_63F10295" targetNode="P_55F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_382F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_386F10295_I" deadCode="true" sourceNode="P_65F10295" targetNode="P_63F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_386F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_386F10295_O" deadCode="true" sourceNode="P_65F10295" targetNode="P_64F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_386F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_388F10295_I" deadCode="true" sourceNode="P_65F10295" targetNode="P_63F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_388F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_388F10295_O" deadCode="true" sourceNode="P_65F10295" targetNode="P_64F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_388F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_390F10295_I" deadCode="true" sourceNode="P_65F10295" targetNode="P_63F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_390F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_390F10295_O" deadCode="true" sourceNode="P_65F10295" targetNode="P_64F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_390F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_393F10295_I" deadCode="true" sourceNode="P_65F10295" targetNode="P_63F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_393F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_393F10295_O" deadCode="true" sourceNode="P_65F10295" targetNode="P_64F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_393F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_394F10295_I" deadCode="true" sourceNode="P_65F10295" targetNode="P_68F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_394F10295"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_394F10295_O" deadCode="true" sourceNode="P_65F10295" targetNode="P_69F10295">
		<representations href="../../../cobol/LOAS0870.cbl.cobModel#S_394F10295"/>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_99F10295" deadCode="false" name="Dynamic LCCS0003" sourceNode="P_27F10295" targetNode="LCCS0003">
		<representations href="../../../cobol/../importantStmts.cobModel#S_99F10295"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_312F10295" deadCode="false" name="Dynamic IDSI0011-PGM" sourceNode="P_18F10295" targetNode="IDSS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_312F10295"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_318F10295" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_54F10295" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_318F10295"></representations>
	</edges>
</Package>

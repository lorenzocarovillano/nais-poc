<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0010" cbl:id="LVVS0010" xsi:id="LVVS0010" packageRef="LVVS0010.igd#LVVS0010" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0010_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10311" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0010.cbl.cobModel#SC_1F10311"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10311" deadCode="false" name="PROGRAM_LVVS0010_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0010.cbl.cobModel#P_1F10311"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10311" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0010.cbl.cobModel#P_2F10311"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10311" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0010.cbl.cobModel#P_3F10311"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10311" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0010.cbl.cobModel#P_4F10311"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10311" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0010.cbl.cobModel#P_5F10311"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10311" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0010.cbl.cobModel#P_8F10311"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10311" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0010.cbl.cobModel#P_9F10311"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10311" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS0010.cbl.cobModel#P_10F10311"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10311" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS0010.cbl.cobModel#P_11F10311"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10311" deadCode="false" name="S1300-CALL-LCCS0010">
				<representations href="../../../cobol/LVVS0010.cbl.cobModel#P_12F10311"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10311" deadCode="false" name="S1300-CALL-LCCS0010-EX">
				<representations href="../../../cobol/LVVS0010.cbl.cobModel#P_13F10311"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10311" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0010.cbl.cobModel#P_6F10311"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10311" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0010.cbl.cobModel#P_7F10311"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0010" name="LCCS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10122"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10311P_1F10311" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10311" targetNode="P_1F10311"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10311_I" deadCode="false" sourceNode="P_1F10311" targetNode="P_2F10311">
		<representations href="../../../cobol/LVVS0010.cbl.cobModel#S_1F10311"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10311_O" deadCode="false" sourceNode="P_1F10311" targetNode="P_3F10311">
		<representations href="../../../cobol/LVVS0010.cbl.cobModel#S_1F10311"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10311_I" deadCode="false" sourceNode="P_1F10311" targetNode="P_4F10311">
		<representations href="../../../cobol/LVVS0010.cbl.cobModel#S_2F10311"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10311_O" deadCode="false" sourceNode="P_1F10311" targetNode="P_5F10311">
		<representations href="../../../cobol/LVVS0010.cbl.cobModel#S_2F10311"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10311_I" deadCode="false" sourceNode="P_1F10311" targetNode="P_6F10311">
		<representations href="../../../cobol/LVVS0010.cbl.cobModel#S_3F10311"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10311_O" deadCode="false" sourceNode="P_1F10311" targetNode="P_7F10311">
		<representations href="../../../cobol/LVVS0010.cbl.cobModel#S_3F10311"/>
	</edges>
	<edges id="P_2F10311P_3F10311" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10311" targetNode="P_3F10311"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10311_I" deadCode="false" sourceNode="P_4F10311" targetNode="P_8F10311">
		<representations href="../../../cobol/LVVS0010.cbl.cobModel#S_10F10311"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10311_O" deadCode="false" sourceNode="P_4F10311" targetNode="P_9F10311">
		<representations href="../../../cobol/LVVS0010.cbl.cobModel#S_10F10311"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10311_I" deadCode="false" sourceNode="P_4F10311" targetNode="P_10F10311">
		<representations href="../../../cobol/LVVS0010.cbl.cobModel#S_11F10311"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10311_O" deadCode="false" sourceNode="P_4F10311" targetNode="P_11F10311">
		<representations href="../../../cobol/LVVS0010.cbl.cobModel#S_11F10311"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10311_I" deadCode="false" sourceNode="P_4F10311" targetNode="P_12F10311">
		<representations href="../../../cobol/LVVS0010.cbl.cobModel#S_13F10311"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10311_O" deadCode="false" sourceNode="P_4F10311" targetNode="P_13F10311">
		<representations href="../../../cobol/LVVS0010.cbl.cobModel#S_13F10311"/>
	</edges>
	<edges id="P_4F10311P_5F10311" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10311" targetNode="P_5F10311"/>
	<edges id="P_8F10311P_9F10311" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10311" targetNode="P_9F10311"/>
	<edges id="P_10F10311P_11F10311" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10311" targetNode="P_11F10311"/>
	<edges id="P_12F10311P_13F10311" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10311" targetNode="P_13F10311"/>
	<edges xsi:type="cbl:CallEdge" id="S_43F10311" deadCode="false" name="Dynamic LCCS0010" sourceNode="P_12F10311" targetNode="LCCS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_43F10311"></representations>
	</edges>
</Package>

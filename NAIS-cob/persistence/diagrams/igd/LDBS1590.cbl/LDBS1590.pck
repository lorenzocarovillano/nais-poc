<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS1590" cbl:id="LDBS1590" xsi:id="LDBS1590" packageRef="LDBS1590.igd#LDBS1590" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS1590_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10163" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS1590.cbl.cobModel#SC_1F10163"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10163" deadCode="false" name="PROGRAM_LDBS1590_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_1F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10163" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_2F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10163" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_3F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10163" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_12F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10163" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_13F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10163" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_4F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10163" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_5F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10163" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_6F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10163" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_7F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10163" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_8F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10163" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_9F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10163" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_44F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10163" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_49F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10163" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_14F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10163" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_15F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10163" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_16F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10163" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_17F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10163" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_18F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10163" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_19F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10163" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_20F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10163" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_21F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10163" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_22F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10163" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_23F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10163" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_58F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10163" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_59F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10163" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_24F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10163" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_25F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10163" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_26F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10163" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_27F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10163" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_28F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10163" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_29F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10163" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_30F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10163" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_31F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10163" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_32F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10163" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_33F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10163" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_60F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10163" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_61F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10163" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_34F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10163" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_35F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10163" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_36F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10163" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_37F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10163" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_38F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10163" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_39F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10163" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_40F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10163" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_41F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10163" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_42F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10163" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_43F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10163" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_50F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10163" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_51F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10163" deadCode="false" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_54F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10163" deadCode="false" name="Z900-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_55F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10163" deadCode="true" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_52F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10163" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_53F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10163" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_45F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10163" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_46F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10163" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_47F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10163" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_48F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10163" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_56F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10163" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_57F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10163" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_10F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10163" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_11F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10163" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_66F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10163" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_67F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10163" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_68F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10163" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_69F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10163" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_62F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10163" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_63F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10163" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_70F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10163" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_71F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10163" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_64F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10163" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_65F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10163" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_72F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10163" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_73F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10163" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_74F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10163" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_75F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10163" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_76F10163"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10163" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS1590.cbl.cobModel#P_77F10163"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_IMPST_SOST" name="IMPST_SOST">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_IMPST_SOST"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_TIT_CONT" name="TIT_CONT">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_TIT_CONT"/>
		</children>
	</packageNode>
	<edges id="SC_1F10163P_1F10163" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10163" targetNode="P_1F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10163_I" deadCode="false" sourceNode="P_1F10163" targetNode="P_2F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_1F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10163_O" deadCode="false" sourceNode="P_1F10163" targetNode="P_3F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_1F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10163_I" deadCode="false" sourceNode="P_1F10163" targetNode="P_4F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_5F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10163_O" deadCode="false" sourceNode="P_1F10163" targetNode="P_5F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_5F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10163_I" deadCode="false" sourceNode="P_1F10163" targetNode="P_6F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_9F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10163_O" deadCode="false" sourceNode="P_1F10163" targetNode="P_7F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_9F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10163_I" deadCode="false" sourceNode="P_1F10163" targetNode="P_8F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_13F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10163_O" deadCode="false" sourceNode="P_1F10163" targetNode="P_9F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_13F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10163_I" deadCode="false" sourceNode="P_2F10163" targetNode="P_10F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_22F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10163_O" deadCode="false" sourceNode="P_2F10163" targetNode="P_11F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_22F10163"/>
	</edges>
	<edges id="P_2F10163P_3F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10163" targetNode="P_3F10163"/>
	<edges id="P_12F10163P_13F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10163" targetNode="P_13F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10163_I" deadCode="false" sourceNode="P_4F10163" targetNode="P_14F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_35F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10163_O" deadCode="false" sourceNode="P_4F10163" targetNode="P_15F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_35F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10163_I" deadCode="false" sourceNode="P_4F10163" targetNode="P_16F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_36F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10163_O" deadCode="false" sourceNode="P_4F10163" targetNode="P_17F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_36F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10163_I" deadCode="false" sourceNode="P_4F10163" targetNode="P_18F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_37F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10163_O" deadCode="false" sourceNode="P_4F10163" targetNode="P_19F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_37F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10163_I" deadCode="false" sourceNode="P_4F10163" targetNode="P_20F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_38F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10163_O" deadCode="false" sourceNode="P_4F10163" targetNode="P_21F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_38F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10163_I" deadCode="false" sourceNode="P_4F10163" targetNode="P_22F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_39F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10163_O" deadCode="false" sourceNode="P_4F10163" targetNode="P_23F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_39F10163"/>
	</edges>
	<edges id="P_4F10163P_5F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10163" targetNode="P_5F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10163_I" deadCode="false" sourceNode="P_6F10163" targetNode="P_24F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_43F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10163_O" deadCode="false" sourceNode="P_6F10163" targetNode="P_25F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_43F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10163_I" deadCode="false" sourceNode="P_6F10163" targetNode="P_26F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_44F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10163_O" deadCode="false" sourceNode="P_6F10163" targetNode="P_27F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_44F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10163_I" deadCode="false" sourceNode="P_6F10163" targetNode="P_28F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_45F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10163_O" deadCode="false" sourceNode="P_6F10163" targetNode="P_29F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_45F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10163_I" deadCode="false" sourceNode="P_6F10163" targetNode="P_30F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_46F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10163_O" deadCode="false" sourceNode="P_6F10163" targetNode="P_31F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_46F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10163_I" deadCode="false" sourceNode="P_6F10163" targetNode="P_32F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_47F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10163_O" deadCode="false" sourceNode="P_6F10163" targetNode="P_33F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_47F10163"/>
	</edges>
	<edges id="P_6F10163P_7F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10163" targetNode="P_7F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10163_I" deadCode="false" sourceNode="P_8F10163" targetNode="P_34F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_51F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10163_O" deadCode="false" sourceNode="P_8F10163" targetNode="P_35F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_51F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10163_I" deadCode="false" sourceNode="P_8F10163" targetNode="P_36F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_52F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10163_O" deadCode="false" sourceNode="P_8F10163" targetNode="P_37F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_52F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10163_I" deadCode="false" sourceNode="P_8F10163" targetNode="P_38F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_53F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10163_O" deadCode="false" sourceNode="P_8F10163" targetNode="P_39F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_53F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10163_I" deadCode="false" sourceNode="P_8F10163" targetNode="P_40F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_54F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10163_O" deadCode="false" sourceNode="P_8F10163" targetNode="P_41F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_54F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10163_I" deadCode="false" sourceNode="P_8F10163" targetNode="P_42F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_55F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10163_O" deadCode="false" sourceNode="P_8F10163" targetNode="P_43F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_55F10163"/>
	</edges>
	<edges id="P_8F10163P_9F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10163" targetNode="P_9F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10163_I" deadCode="false" sourceNode="P_44F10163" targetNode="P_45F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_58F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10163_O" deadCode="false" sourceNode="P_44F10163" targetNode="P_46F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_58F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10163_I" deadCode="false" sourceNode="P_44F10163" targetNode="P_47F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_59F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10163_O" deadCode="false" sourceNode="P_44F10163" targetNode="P_48F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_59F10163"/>
	</edges>
	<edges id="P_44F10163P_49F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10163" targetNode="P_49F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10163_I" deadCode="false" sourceNode="P_14F10163" targetNode="P_45F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_62F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10163_O" deadCode="false" sourceNode="P_14F10163" targetNode="P_46F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_62F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10163_I" deadCode="false" sourceNode="P_14F10163" targetNode="P_47F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_63F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10163_O" deadCode="false" sourceNode="P_14F10163" targetNode="P_48F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_63F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10163_I" deadCode="false" sourceNode="P_14F10163" targetNode="P_12F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_65F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10163_O" deadCode="false" sourceNode="P_14F10163" targetNode="P_13F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_65F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10163_I" deadCode="false" sourceNode="P_14F10163" targetNode="P_50F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_67F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10163_O" deadCode="false" sourceNode="P_14F10163" targetNode="P_51F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_67F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10163_I" deadCode="true" sourceNode="P_14F10163" targetNode="P_52F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_68F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10163_O" deadCode="true" sourceNode="P_14F10163" targetNode="P_53F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_68F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10163_I" deadCode="false" sourceNode="P_14F10163" targetNode="P_54F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_70F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10163_O" deadCode="false" sourceNode="P_14F10163" targetNode="P_55F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_70F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10163_I" deadCode="false" sourceNode="P_14F10163" targetNode="P_56F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_71F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10163_O" deadCode="false" sourceNode="P_14F10163" targetNode="P_57F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_71F10163"/>
	</edges>
	<edges id="P_14F10163P_15F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10163" targetNode="P_15F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10163_I" deadCode="false" sourceNode="P_16F10163" targetNode="P_44F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_73F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10163_O" deadCode="false" sourceNode="P_16F10163" targetNode="P_49F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_73F10163"/>
	</edges>
	<edges id="P_16F10163P_17F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10163" targetNode="P_17F10163"/>
	<edges id="P_18F10163P_19F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10163" targetNode="P_19F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10163_I" deadCode="false" sourceNode="P_20F10163" targetNode="P_16F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_78F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10163_O" deadCode="false" sourceNode="P_20F10163" targetNode="P_17F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_78F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10163_I" deadCode="false" sourceNode="P_20F10163" targetNode="P_22F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_80F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10163_O" deadCode="false" sourceNode="P_20F10163" targetNode="P_23F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_80F10163"/>
	</edges>
	<edges id="P_20F10163P_21F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10163" targetNode="P_21F10163"/>
	<edges id="P_22F10163P_23F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10163" targetNode="P_23F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10163_I" deadCode="false" sourceNode="P_58F10163" targetNode="P_45F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_84F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10163_O" deadCode="false" sourceNode="P_58F10163" targetNode="P_46F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_84F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10163_I" deadCode="false" sourceNode="P_58F10163" targetNode="P_47F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_85F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10163_O" deadCode="false" sourceNode="P_58F10163" targetNode="P_48F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_85F10163"/>
	</edges>
	<edges id="P_58F10163P_59F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10163" targetNode="P_59F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10163_I" deadCode="false" sourceNode="P_24F10163" targetNode="P_45F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_88F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10163_O" deadCode="false" sourceNode="P_24F10163" targetNode="P_46F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_88F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10163_I" deadCode="false" sourceNode="P_24F10163" targetNode="P_47F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_89F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10163_O" deadCode="false" sourceNode="P_24F10163" targetNode="P_48F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_89F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10163_I" deadCode="false" sourceNode="P_24F10163" targetNode="P_12F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_91F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10163_O" deadCode="false" sourceNode="P_24F10163" targetNode="P_13F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_91F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10163_I" deadCode="false" sourceNode="P_24F10163" targetNode="P_50F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_93F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10163_O" deadCode="false" sourceNode="P_24F10163" targetNode="P_51F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_93F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10163_I" deadCode="true" sourceNode="P_24F10163" targetNode="P_52F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_94F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10163_O" deadCode="true" sourceNode="P_24F10163" targetNode="P_53F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_94F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10163_I" deadCode="false" sourceNode="P_24F10163" targetNode="P_54F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_96F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10163_O" deadCode="false" sourceNode="P_24F10163" targetNode="P_55F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_96F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10163_I" deadCode="false" sourceNode="P_24F10163" targetNode="P_56F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_97F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10163_O" deadCode="false" sourceNode="P_24F10163" targetNode="P_57F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_97F10163"/>
	</edges>
	<edges id="P_24F10163P_25F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10163" targetNode="P_25F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10163_I" deadCode="false" sourceNode="P_26F10163" targetNode="P_58F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_99F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10163_O" deadCode="false" sourceNode="P_26F10163" targetNode="P_59F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_99F10163"/>
	</edges>
	<edges id="P_26F10163P_27F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10163" targetNode="P_27F10163"/>
	<edges id="P_28F10163P_29F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10163" targetNode="P_29F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10163_I" deadCode="false" sourceNode="P_30F10163" targetNode="P_26F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_104F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10163_O" deadCode="false" sourceNode="P_30F10163" targetNode="P_27F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_104F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10163_I" deadCode="false" sourceNode="P_30F10163" targetNode="P_32F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_106F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10163_O" deadCode="false" sourceNode="P_30F10163" targetNode="P_33F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_106F10163"/>
	</edges>
	<edges id="P_30F10163P_31F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10163" targetNode="P_31F10163"/>
	<edges id="P_32F10163P_33F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10163" targetNode="P_33F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10163_I" deadCode="false" sourceNode="P_60F10163" targetNode="P_45F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_110F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10163_O" deadCode="false" sourceNode="P_60F10163" targetNode="P_46F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_110F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10163_I" deadCode="false" sourceNode="P_60F10163" targetNode="P_47F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_111F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10163_O" deadCode="false" sourceNode="P_60F10163" targetNode="P_48F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_111F10163"/>
	</edges>
	<edges id="P_60F10163P_61F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_60F10163" targetNode="P_61F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10163_I" deadCode="false" sourceNode="P_34F10163" targetNode="P_45F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_114F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10163_O" deadCode="false" sourceNode="P_34F10163" targetNode="P_46F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_114F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10163_I" deadCode="false" sourceNode="P_34F10163" targetNode="P_47F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_115F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10163_O" deadCode="false" sourceNode="P_34F10163" targetNode="P_48F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_115F10163"/>
	</edges>
	<edges id="P_34F10163P_35F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10163" targetNode="P_35F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10163_I" deadCode="false" sourceNode="P_36F10163" targetNode="P_60F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_118F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10163_O" deadCode="false" sourceNode="P_36F10163" targetNode="P_61F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_118F10163"/>
	</edges>
	<edges id="P_36F10163P_37F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10163" targetNode="P_37F10163"/>
	<edges id="P_38F10163P_39F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10163" targetNode="P_39F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10163_I" deadCode="false" sourceNode="P_40F10163" targetNode="P_36F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_123F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10163_O" deadCode="false" sourceNode="P_40F10163" targetNode="P_37F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_123F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10163_I" deadCode="false" sourceNode="P_40F10163" targetNode="P_42F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_125F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10163_O" deadCode="false" sourceNode="P_40F10163" targetNode="P_43F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_125F10163"/>
	</edges>
	<edges id="P_40F10163P_41F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10163" targetNode="P_41F10163"/>
	<edges id="P_42F10163P_43F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10163" targetNode="P_43F10163"/>
	<edges id="P_50F10163P_51F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10163" targetNode="P_51F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10163_I" deadCode="false" sourceNode="P_54F10163" targetNode="P_62F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_162F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10163_O" deadCode="false" sourceNode="P_54F10163" targetNode="P_63F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_162F10163"/>
	</edges>
	<edges id="P_54F10163P_55F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10163" targetNode="P_55F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10163_I" deadCode="true" sourceNode="P_52F10163" targetNode="P_64F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_166F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10163_O" deadCode="true" sourceNode="P_52F10163" targetNode="P_65F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_166F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_169F10163_I" deadCode="true" sourceNode="P_52F10163" targetNode="P_64F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_169F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_169F10163_O" deadCode="true" sourceNode="P_52F10163" targetNode="P_65F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_169F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10163_I" deadCode="true" sourceNode="P_52F10163" targetNode="P_64F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_173F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10163_O" deadCode="true" sourceNode="P_52F10163" targetNode="P_65F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_173F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10163_I" deadCode="true" sourceNode="P_52F10163" targetNode="P_64F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_177F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10163_O" deadCode="true" sourceNode="P_52F10163" targetNode="P_65F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_177F10163"/>
	</edges>
	<edges id="P_52F10163P_53F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10163" targetNode="P_53F10163"/>
	<edges id="P_45F10163P_46F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10163" targetNode="P_46F10163"/>
	<edges id="P_47F10163P_48F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10163" targetNode="P_48F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10163_I" deadCode="false" sourceNode="P_56F10163" targetNode="P_12F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_185F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10163_O" deadCode="false" sourceNode="P_56F10163" targetNode="P_13F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_185F10163"/>
	</edges>
	<edges id="P_56F10163P_57F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10163" targetNode="P_57F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10163_I" deadCode="false" sourceNode="P_10F10163" targetNode="P_66F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_187F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10163_O" deadCode="false" sourceNode="P_10F10163" targetNode="P_67F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_187F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10163_I" deadCode="false" sourceNode="P_10F10163" targetNode="P_68F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_189F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10163_O" deadCode="false" sourceNode="P_10F10163" targetNode="P_69F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_189F10163"/>
	</edges>
	<edges id="P_10F10163P_11F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10163" targetNode="P_11F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_194F10163_I" deadCode="false" sourceNode="P_66F10163" targetNode="P_62F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_194F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_194F10163_O" deadCode="false" sourceNode="P_66F10163" targetNode="P_63F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_194F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_199F10163_I" deadCode="false" sourceNode="P_66F10163" targetNode="P_62F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_199F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_199F10163_O" deadCode="false" sourceNode="P_66F10163" targetNode="P_63F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_199F10163"/>
	</edges>
	<edges id="P_66F10163P_67F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10163" targetNode="P_67F10163"/>
	<edges id="P_68F10163P_69F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10163" targetNode="P_69F10163"/>
	<edges id="P_62F10163P_63F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10163" targetNode="P_63F10163"/>
	<edges xsi:type="cbl:PerformEdge" id="S_228F10163_I" deadCode="true" sourceNode="P_64F10163" targetNode="P_72F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_228F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_228F10163_O" deadCode="true" sourceNode="P_64F10163" targetNode="P_73F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_228F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_229F10163_I" deadCode="true" sourceNode="P_64F10163" targetNode="P_74F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_229F10163"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_229F10163_O" deadCode="true" sourceNode="P_64F10163" targetNode="P_75F10163">
		<representations href="../../../cobol/LDBS1590.cbl.cobModel#S_229F10163"/>
	</edges>
	<edges id="P_64F10163P_65F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10163" targetNode="P_65F10163"/>
	<edges id="P_72F10163P_73F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10163" targetNode="P_73F10163"/>
	<edges id="P_74F10163P_75F10163" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10163" targetNode="P_75F10163"/>
	<edges xsi:type="cbl:DataEdge" id="S_64F10163_POS1" deadCode="false" targetNode="P_14F10163" sourceNode="DB2_IMPST_SOST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_64F10163"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_90F10163_POS1" deadCode="false" targetNode="P_24F10163" sourceNode="DB2_IMPST_SOST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_90F10163"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_184F10163_POS1" deadCode="false" targetNode="P_56F10163" sourceNode="DB2_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_184F10163"></representations>
	</edges>
</Package>

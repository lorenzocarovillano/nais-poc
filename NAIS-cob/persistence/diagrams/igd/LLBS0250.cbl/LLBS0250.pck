<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LLBS0250" cbl:id="LLBS0250" xsi:id="LLBS0250" packageRef="LLBS0250.igd#LLBS0250" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LLBS0250_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10282" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LLBS0250.cbl.cobModel#SC_1F10282"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10282" deadCode="false" name="PROGRAM_LLBS0250_FIRST_SENTENCES">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_1F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10282" deadCode="true" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_2F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10282" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_3F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10282" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_4F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10282" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_5F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10282" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_6F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10282" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_7F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10282" deadCode="false" name="VAL-DCLGEN-B05">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_10F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10282" deadCode="false" name="VAL-DCLGEN-B05-EX">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_11F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10282" deadCode="false" name="SCRIVI-BIL-VAR-DI-CALC-T">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_8F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10282" deadCode="false" name="SCRIVI-BIL-VAR-DI-CALC-T-EX">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_9F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10282" deadCode="false" name="VALORIZZA-AREA-DSH-B05">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_12F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10282" deadCode="false" name="VALORIZZA-AREA-DSH-B05-EX">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_13F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10282" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_16F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10282" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_21F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10282" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_19F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10282" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_20F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10282" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_17F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10282" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_18F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10282" deadCode="true" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_22F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10282" deadCode="true" name="EX-S0290">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_23F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10282" deadCode="false" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_24F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10282" deadCode="false" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_25F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10282" deadCode="false" name="AGGIORNA-TABELLA">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_14F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10282" deadCode="false" name="AGGIORNA-TABELLA-EX">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_15F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10282" deadCode="true" name="ESTR-SEQUENCE">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_26F10282"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10282" deadCode="true" name="ESTR-SEQUENCE-EX">
				<representations href="../../../cobol/LLBS0250.cbl.cobModel#P_27F10282"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0010" name="IDSS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10099"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LLBS0250_LCCS0090" name="Dynamic LLBS0250 LCCS0090" missing="true">
			<representations href="../../../../missing.xmi#IDSX523ATB51LCG3PI2GLHMZZHTIV51RAJU42SWMGJLLWKB1IBCTGI"/>
		</children>
	</packageNode>
	<edges id="SC_1F10282P_1F10282" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10282" targetNode="P_1F10282"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10282_I" deadCode="true" sourceNode="P_1F10282" targetNode="P_2F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_1F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10282_O" deadCode="true" sourceNode="P_1F10282" targetNode="P_3F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_1F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10282_I" deadCode="false" sourceNode="P_1F10282" targetNode="P_4F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_3F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10282_O" deadCode="false" sourceNode="P_1F10282" targetNode="P_5F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_3F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10282_I" deadCode="false" sourceNode="P_1F10282" targetNode="P_6F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_4F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10282_O" deadCode="false" sourceNode="P_1F10282" targetNode="P_7F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_4F10282"/>
	</edges>
	<edges id="P_2F10282P_3F10282" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10282" targetNode="P_3F10282"/>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10282_I" deadCode="false" sourceNode="P_4F10282" targetNode="P_8F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_9F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10282_O" deadCode="false" sourceNode="P_4F10282" targetNode="P_9F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_9F10282"/>
	</edges>
	<edges id="P_4F10282P_5F10282" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10282" targetNode="P_5F10282"/>
	<edges id="P_10F10282P_11F10282" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10282" targetNode="P_11F10282"/>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10282_I" deadCode="false" sourceNode="P_8F10282" targetNode="P_10F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_62F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10282_O" deadCode="false" sourceNode="P_8F10282" targetNode="P_11F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_62F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10282_I" deadCode="false" sourceNode="P_8F10282" targetNode="P_12F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_64F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10282_O" deadCode="false" sourceNode="P_8F10282" targetNode="P_13F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_64F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10282_I" deadCode="false" sourceNode="P_8F10282" targetNode="P_14F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_65F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10282_O" deadCode="false" sourceNode="P_8F10282" targetNode="P_15F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_65F10282"/>
	</edges>
	<edges id="P_8F10282P_9F10282" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10282" targetNode="P_9F10282"/>
	<edges id="P_12F10282P_13F10282" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10282" targetNode="P_13F10282"/>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10282_I" deadCode="false" sourceNode="P_16F10282" targetNode="P_17F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_80F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10282_O" deadCode="false" sourceNode="P_16F10282" targetNode="P_18F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_80F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10282_I" deadCode="false" sourceNode="P_16F10282" targetNode="P_19F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_84F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10282_O" deadCode="false" sourceNode="P_16F10282" targetNode="P_20F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_84F10282"/>
	</edges>
	<edges id="P_16F10282P_21F10282" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10282" targetNode="P_21F10282"/>
	<edges id="P_19F10282P_20F10282" xsi:type="cbl:FallThroughEdge" sourceNode="P_19F10282" targetNode="P_20F10282"/>
	<edges id="P_17F10282P_18F10282" xsi:type="cbl:FallThroughEdge" sourceNode="P_17F10282" targetNode="P_18F10282"/>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10282_I" deadCode="true" sourceNode="P_22F10282" targetNode="P_16F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_120F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10282_O" deadCode="true" sourceNode="P_22F10282" targetNode="P_21F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_120F10282"/>
	</edges>
	<edges id="P_24F10282P_25F10282" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10282" targetNode="P_25F10282"/>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10282_I" deadCode="false" sourceNode="P_14F10282" targetNode="P_24F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_152F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10282_O" deadCode="false" sourceNode="P_14F10282" targetNode="P_25F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_152F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10282_I" deadCode="false" sourceNode="P_14F10282" targetNode="P_16F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_160F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10282_O" deadCode="false" sourceNode="P_14F10282" targetNode="P_21F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_160F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10282_I" deadCode="false" sourceNode="P_14F10282" targetNode="P_16F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_165F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10282_O" deadCode="false" sourceNode="P_14F10282" targetNode="P_21F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_165F10282"/>
	</edges>
	<edges id="P_14F10282P_15F10282" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10282" targetNode="P_15F10282"/>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10282_I" deadCode="true" sourceNode="P_26F10282" targetNode="P_22F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_172F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10282_O" deadCode="true" sourceNode="P_26F10282" targetNode="P_23F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_172F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_180F10282_I" deadCode="true" sourceNode="P_26F10282" targetNode="P_16F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_180F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_180F10282_O" deadCode="true" sourceNode="P_26F10282" targetNode="P_21F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_180F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10282_I" deadCode="true" sourceNode="P_26F10282" targetNode="P_16F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_185F10282"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10282_O" deadCode="true" sourceNode="P_26F10282" targetNode="P_21F10282">
		<representations href="../../../cobol/LLBS0250.cbl.cobModel#S_185F10282"/>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_78F10282" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_16F10282" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10282"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_148F10282" deadCode="false" name="Dynamic IDSI0011-PGM" sourceNode="P_24F10282" targetNode="IDSS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_148F10282"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_168F10282" deadCode="true" name="Dynamic LCCS0090" sourceNode="P_26F10282" targetNode="Dynamic_LLBS0250_LCCS0090">
		<representations href="../../../cobol/../importantStmts.cobModel#S_168F10282"></representations>
	</edges>
</Package>

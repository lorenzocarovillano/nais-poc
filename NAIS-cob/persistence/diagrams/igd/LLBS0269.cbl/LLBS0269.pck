<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LLBS0269" cbl:id="LLBS0269" xsi:id="LLBS0269" packageRef="LLBS0269.igd#LLBS0269" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LLBS0269_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10284" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LLBS0269.cbl.cobModel#SC_1F10284"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10284" deadCode="false" name="PROGRAM_LLBS0269_FIRST_SENTENCES">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_1F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10284" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_2F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10284" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_3F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10284" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_4F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10284" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_5F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10284" deadCode="false" name="S1002-GESTIONE-TS-COMPET">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_8F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10284" deadCode="false" name="EX-S1002">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_9F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10284" deadCode="false" name="S1003-RECUPERO-TS-COMP">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_10F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10284" deadCode="false" name="EX-S1003">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_11F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10284" deadCode="false" name="S1500-LEGGI-DETT-RICH">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_14F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10284" deadCode="false" name="EX-S1500">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_15F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10284" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_6F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10284" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_7F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10284" deadCode="true" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_18F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10284" deadCode="true" name="EX-S0290">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_19F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10284" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_16F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10284" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_17F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10284" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_22F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10284" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_23F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10284" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_20F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10284" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_21F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10284" deadCode="false" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_12F10284"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10284" deadCode="false" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LLBS0269.cbl.cobModel#P_13F10284"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0010" name="IDSS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10099"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10284P_1F10284" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10284" targetNode="P_1F10284"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10284_I" deadCode="false" sourceNode="P_1F10284" targetNode="P_2F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_1F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10284_O" deadCode="false" sourceNode="P_1F10284" targetNode="P_3F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_1F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10284_I" deadCode="false" sourceNode="P_1F10284" targetNode="P_4F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_2F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10284_O" deadCode="false" sourceNode="P_1F10284" targetNode="P_5F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_2F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10284_I" deadCode="false" sourceNode="P_1F10284" targetNode="P_6F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_3F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10284_O" deadCode="false" sourceNode="P_1F10284" targetNode="P_7F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_3F10284"/>
	</edges>
	<edges id="P_2F10284P_3F10284" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10284" targetNode="P_3F10284"/>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10284_I" deadCode="false" sourceNode="P_4F10284" targetNode="P_8F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_6F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10284_O" deadCode="false" sourceNode="P_4F10284" targetNode="P_9F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_6F10284"/>
	</edges>
	<edges id="P_4F10284P_5F10284" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10284" targetNode="P_5F10284"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10284_I" deadCode="false" sourceNode="P_8F10284" targetNode="P_10F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_11F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10284_O" deadCode="false" sourceNode="P_8F10284" targetNode="P_11F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_11F10284"/>
	</edges>
	<edges id="P_8F10284P_9F10284" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10284" targetNode="P_9F10284"/>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10284_I" deadCode="false" sourceNode="P_10F10284" targetNode="P_12F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_35F10284"/>
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_36F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10284_O" deadCode="false" sourceNode="P_10F10284" targetNode="P_13F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_35F10284"/>
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_36F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10284_I" deadCode="false" sourceNode="P_10F10284" targetNode="P_14F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_35F10284"/>
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_41F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10284_O" deadCode="false" sourceNode="P_10F10284" targetNode="P_15F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_35F10284"/>
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_41F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10284_I" deadCode="false" sourceNode="P_10F10284" targetNode="P_16F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_35F10284"/>
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_54F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10284_O" deadCode="false" sourceNode="P_10F10284" targetNode="P_17F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_35F10284"/>
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_54F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10284_I" deadCode="false" sourceNode="P_10F10284" targetNode="P_16F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_35F10284"/>
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_60F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10284_O" deadCode="false" sourceNode="P_10F10284" targetNode="P_17F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_35F10284"/>
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_60F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10284_I" deadCode="false" sourceNode="P_10F10284" targetNode="P_16F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_35F10284"/>
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_65F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10284_O" deadCode="false" sourceNode="P_10F10284" targetNode="P_17F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_35F10284"/>
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_65F10284"/>
	</edges>
	<edges id="P_10F10284P_11F10284" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10284" targetNode="P_11F10284"/>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10284_I" deadCode="false" sourceNode="P_14F10284" targetNode="P_12F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_78F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10284_O" deadCode="false" sourceNode="P_14F10284" targetNode="P_13F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_78F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10284_I" deadCode="false" sourceNode="P_14F10284" targetNode="P_16F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_86F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10284_O" deadCode="false" sourceNode="P_14F10284" targetNode="P_17F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_86F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10284_I" deadCode="false" sourceNode="P_14F10284" targetNode="P_16F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_105F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10284_O" deadCode="false" sourceNode="P_14F10284" targetNode="P_17F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_105F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10284_I" deadCode="false" sourceNode="P_14F10284" targetNode="P_16F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_110F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10284_O" deadCode="false" sourceNode="P_14F10284" targetNode="P_17F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_110F10284"/>
	</edges>
	<edges id="P_14F10284P_15F10284" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10284" targetNode="P_15F10284"/>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10284_I" deadCode="true" sourceNode="P_18F10284" targetNode="P_16F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_116F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10284_O" deadCode="true" sourceNode="P_18F10284" targetNode="P_17F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_116F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10284_I" deadCode="false" sourceNode="P_16F10284" targetNode="P_20F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_123F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10284_O" deadCode="false" sourceNode="P_16F10284" targetNode="P_21F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_123F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10284_I" deadCode="false" sourceNode="P_16F10284" targetNode="P_22F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_127F10284"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10284_O" deadCode="false" sourceNode="P_16F10284" targetNode="P_23F10284">
		<representations href="../../../cobol/LLBS0269.cbl.cobModel#S_127F10284"/>
	</edges>
	<edges id="P_16F10284P_17F10284" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10284" targetNode="P_17F10284"/>
	<edges id="P_22F10284P_23F10284" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10284" targetNode="P_23F10284"/>
	<edges id="P_20F10284P_21F10284" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10284" targetNode="P_21F10284"/>
	<edges id="P_12F10284P_13F10284" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10284" targetNode="P_13F10284"/>
	<edges xsi:type="cbl:CallEdge" id="S_121F10284" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_16F10284" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_121F10284"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_187F10284" deadCode="false" name="Dynamic IDSI0011-PGM" sourceNode="P_12F10284" targetNode="IDSS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_187F10284"></representations>
	</edges>
</Package>

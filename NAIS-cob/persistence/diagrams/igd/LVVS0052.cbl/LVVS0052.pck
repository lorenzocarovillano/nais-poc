<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0052" cbl:id="LVVS0052" xsi:id="LVVS0052" packageRef="LVVS0052.igd#LVVS0052" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0052_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10330" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0052.cbl.cobModel#SC_1F10330"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10330" deadCode="false" name="PROGRAM_LVVS0052_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0052.cbl.cobModel#P_1F10330"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10330" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0052.cbl.cobModel#P_2F10330"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10330" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0052.cbl.cobModel#P_3F10330"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10330" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0052.cbl.cobModel#P_4F10330"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10330" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0052.cbl.cobModel#P_5F10330"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10330" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0052.cbl.cobModel#P_8F10330"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10330" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0052.cbl.cobModel#P_9F10330"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10330" deadCode="false" name="S1250-CALCOLA-DATA1">
				<representations href="../../../cobol/LVVS0052.cbl.cobModel#P_12F10330"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10330" deadCode="false" name="S1250-EX">
				<representations href="../../../cobol/LVVS0052.cbl.cobModel#P_13F10330"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10330" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS0052.cbl.cobModel#P_10F10330"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10330" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS0052.cbl.cobModel#P_11F10330"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10330" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0052.cbl.cobModel#P_6F10330"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10330" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0052.cbl.cobModel#P_7F10330"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS1700" name="LDBS1700">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10166"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10330P_1F10330" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10330" targetNode="P_1F10330"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10330_I" deadCode="false" sourceNode="P_1F10330" targetNode="P_2F10330">
		<representations href="../../../cobol/LVVS0052.cbl.cobModel#S_1F10330"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10330_O" deadCode="false" sourceNode="P_1F10330" targetNode="P_3F10330">
		<representations href="../../../cobol/LVVS0052.cbl.cobModel#S_1F10330"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10330_I" deadCode="false" sourceNode="P_1F10330" targetNode="P_4F10330">
		<representations href="../../../cobol/LVVS0052.cbl.cobModel#S_2F10330"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10330_O" deadCode="false" sourceNode="P_1F10330" targetNode="P_5F10330">
		<representations href="../../../cobol/LVVS0052.cbl.cobModel#S_2F10330"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10330_I" deadCode="false" sourceNode="P_1F10330" targetNode="P_6F10330">
		<representations href="../../../cobol/LVVS0052.cbl.cobModel#S_3F10330"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10330_O" deadCode="false" sourceNode="P_1F10330" targetNode="P_7F10330">
		<representations href="../../../cobol/LVVS0052.cbl.cobModel#S_3F10330"/>
	</edges>
	<edges id="P_2F10330P_3F10330" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10330" targetNode="P_3F10330"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10330_I" deadCode="false" sourceNode="P_4F10330" targetNode="P_8F10330">
		<representations href="../../../cobol/LVVS0052.cbl.cobModel#S_11F10330"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10330_O" deadCode="false" sourceNode="P_4F10330" targetNode="P_9F10330">
		<representations href="../../../cobol/LVVS0052.cbl.cobModel#S_11F10330"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10330_I" deadCode="false" sourceNode="P_4F10330" targetNode="P_10F10330">
		<representations href="../../../cobol/LVVS0052.cbl.cobModel#S_13F10330"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10330_O" deadCode="false" sourceNode="P_4F10330" targetNode="P_11F10330">
		<representations href="../../../cobol/LVVS0052.cbl.cobModel#S_13F10330"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10330_I" deadCode="false" sourceNode="P_4F10330" targetNode="P_12F10330">
		<representations href="../../../cobol/LVVS0052.cbl.cobModel#S_25F10330"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10330_O" deadCode="false" sourceNode="P_4F10330" targetNode="P_13F10330">
		<representations href="../../../cobol/LVVS0052.cbl.cobModel#S_25F10330"/>
	</edges>
	<edges id="P_4F10330P_5F10330" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10330" targetNode="P_5F10330"/>
	<edges id="P_8F10330P_9F10330" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10330" targetNode="P_9F10330"/>
	<edges id="P_12F10330P_13F10330" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10330" targetNode="P_13F10330"/>
	<edges id="P_10F10330P_11F10330" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10330" targetNode="P_11F10330"/>
	<edges xsi:type="cbl:CallEdge" id="S_39F10330" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_12F10330" targetNode="LDBS1700">
		<representations href="../../../cobol/../importantStmts.cobModel#S_39F10330"></representations>
	</edges>
</Package>

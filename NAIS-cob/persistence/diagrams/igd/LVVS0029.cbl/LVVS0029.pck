<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0029" cbl:id="LVVS0029" xsi:id="LVVS0029" packageRef="LVVS0029.igd#LVVS0029" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0029_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10321" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0029.cbl.cobModel#SC_1F10321"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10321" deadCode="false" name="PROGRAM_LVVS0029_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0029.cbl.cobModel#P_1F10321"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10321" deadCode="false" name="L000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0029.cbl.cobModel#P_2F10321"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10321" deadCode="false" name="L000-EX">
				<representations href="../../../cobol/LVVS0029.cbl.cobModel#P_3F10321"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10321" deadCode="false" name="L100-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0029.cbl.cobModel#P_4F10321"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10321" deadCode="false" name="L100-EX">
				<representations href="../../../cobol/LVVS0029.cbl.cobModel#P_5F10321"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10321" deadCode="false" name="L500-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0029.cbl.cobModel#P_8F10321"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10321" deadCode="false" name="L500-EX">
				<representations href="../../../cobol/LVVS0029.cbl.cobModel#P_9F10321"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10321" deadCode="false" name="L550-CERCA-TRANCHE">
				<representations href="../../../cobol/LVVS0029.cbl.cobModel#P_10F10321"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10321" deadCode="false" name="L550-EX">
				<representations href="../../../cobol/LVVS0029.cbl.cobModel#P_11F10321"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10321" deadCode="false" name="L600-PREPARA-CALL">
				<representations href="../../../cobol/LVVS0029.cbl.cobModel#P_14F10321"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10321" deadCode="false" name="L600-EX">
				<representations href="../../../cobol/LVVS0029.cbl.cobModel#P_15F10321"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10321" deadCode="false" name="L700-CALL-LVVS0000">
				<representations href="../../../cobol/LVVS0029.cbl.cobModel#P_12F10321"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10321" deadCode="false" name="L700-EX">
				<representations href="../../../cobol/LVVS0029.cbl.cobModel#P_13F10321"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10321" deadCode="false" name="L900-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0029.cbl.cobModel#P_6F10321"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10321" deadCode="true" name="L900-EX">
				<representations href="../../../cobol/LVVS0029.cbl.cobModel#P_7F10321"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LVVS0000" name="LVVS0000">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10304"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10321P_1F10321" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10321" targetNode="P_1F10321"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10321_I" deadCode="false" sourceNode="P_1F10321" targetNode="P_2F10321">
		<representations href="../../../cobol/LVVS0029.cbl.cobModel#S_1F10321"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10321_O" deadCode="false" sourceNode="P_1F10321" targetNode="P_3F10321">
		<representations href="../../../cobol/LVVS0029.cbl.cobModel#S_1F10321"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10321_I" deadCode="false" sourceNode="P_1F10321" targetNode="P_4F10321">
		<representations href="../../../cobol/LVVS0029.cbl.cobModel#S_2F10321"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10321_O" deadCode="false" sourceNode="P_1F10321" targetNode="P_5F10321">
		<representations href="../../../cobol/LVVS0029.cbl.cobModel#S_2F10321"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10321_I" deadCode="false" sourceNode="P_1F10321" targetNode="P_6F10321">
		<representations href="../../../cobol/LVVS0029.cbl.cobModel#S_3F10321"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10321_O" deadCode="false" sourceNode="P_1F10321" targetNode="P_7F10321">
		<representations href="../../../cobol/LVVS0029.cbl.cobModel#S_3F10321"/>
	</edges>
	<edges id="P_2F10321P_3F10321" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10321" targetNode="P_3F10321"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10321_I" deadCode="false" sourceNode="P_4F10321" targetNode="P_8F10321">
		<representations href="../../../cobol/LVVS0029.cbl.cobModel#S_11F10321"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10321_O" deadCode="false" sourceNode="P_4F10321" targetNode="P_9F10321">
		<representations href="../../../cobol/LVVS0029.cbl.cobModel#S_11F10321"/>
	</edges>
	<edges id="P_4F10321P_5F10321" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10321" targetNode="P_5F10321"/>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10321_I" deadCode="false" sourceNode="P_8F10321" targetNode="P_10F10321">
		<representations href="../../../cobol/LVVS0029.cbl.cobModel#S_24F10321"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10321_O" deadCode="false" sourceNode="P_8F10321" targetNode="P_11F10321">
		<representations href="../../../cobol/LVVS0029.cbl.cobModel#S_24F10321"/>
	</edges>
	<edges id="P_8F10321P_9F10321" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10321" targetNode="P_9F10321"/>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10321_I" deadCode="false" sourceNode="P_10F10321" targetNode="P_12F10321">
		<representations href="../../../cobol/LVVS0029.cbl.cobModel#S_26F10321"/>
		<representations href="../../../cobol/LVVS0029.cbl.cobModel#S_29F10321"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10321_O" deadCode="false" sourceNode="P_10F10321" targetNode="P_13F10321">
		<representations href="../../../cobol/LVVS0029.cbl.cobModel#S_26F10321"/>
		<representations href="../../../cobol/LVVS0029.cbl.cobModel#S_29F10321"/>
	</edges>
	<edges id="P_10F10321P_11F10321" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10321" targetNode="P_11F10321"/>
	<edges id="P_14F10321P_15F10321" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10321" targetNode="P_15F10321"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10321_I" deadCode="false" sourceNode="P_12F10321" targetNode="P_14F10321">
		<representations href="../../../cobol/LVVS0029.cbl.cobModel#S_43F10321"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10321_O" deadCode="false" sourceNode="P_12F10321" targetNode="P_15F10321">
		<representations href="../../../cobol/LVVS0029.cbl.cobModel#S_43F10321"/>
	</edges>
	<edges id="P_12F10321P_13F10321" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10321" targetNode="P_13F10321"/>
	<edges xsi:type="cbl:CallEdge" id="S_44F10321" deadCode="false" name="Dynamic PGM-LVVS0000" sourceNode="P_12F10321" targetNode="LVVS0000">
		<representations href="../../../cobol/../importantStmts.cobModel#S_44F10321"></representations>
	</edges>
</Package>

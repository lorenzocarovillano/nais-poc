<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IDBSP630" cbl:id="IDBSP630" xsi:id="IDBSP630" packageRef="IDBSP630.igd#IDBSP630" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IDBSP630_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10066" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IDBSP630.cbl.cobModel#SC_1F10066"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10066" deadCode="false" name="PROGRAM_IDBSP630_FIRST_SENTENCES">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_1F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10066" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_2F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10066" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_3F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10066" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_28F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10066" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_29F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10066" deadCode="false" name="A200-ELABORA-PK">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_24F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10066" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_25F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10066" deadCode="false" name="A300-ELABORA-ID-EFF">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_4F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10066" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_5F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10066" deadCode="false" name="A400-ELABORA-IDP-EFF">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_6F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10066" deadCode="false" name="A400-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_7F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10066" deadCode="false" name="A500-ELABORA-IBO">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_8F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10066" deadCode="false" name="A500-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_9F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10066" deadCode="false" name="A600-ELABORA-IBS">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_10F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10066" deadCode="false" name="A600-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_11F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10066" deadCode="false" name="A700-ELABORA-IDO">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_12F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10066" deadCode="false" name="A700-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_13F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10066" deadCode="false" name="B300-ELABORA-ID-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_14F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10066" deadCode="false" name="B300-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_15F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10066" deadCode="false" name="B400-ELABORA-IDP-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_16F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10066" deadCode="false" name="B400-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_17F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10066" deadCode="false" name="B500-ELABORA-IBO-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_18F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10066" deadCode="false" name="B500-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_19F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10066" deadCode="false" name="B600-ELABORA-IBS-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_20F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10066" deadCode="false" name="B600-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_21F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10066" deadCode="false" name="B700-ELABORA-IDO-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_22F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10066" deadCode="false" name="B700-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_23F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10066" deadCode="false" name="A210-SELECT-PK">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_30F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10066" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_31F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10066" deadCode="false" name="A220-INSERT-PK">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_32F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10066" deadCode="false" name="A220-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_33F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10066" deadCode="false" name="A230-UPDATE-PK">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_34F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10066" deadCode="false" name="A230-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_35F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10066" deadCode="false" name="A240-DELETE-PK">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_36F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10066" deadCode="false" name="A240-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_37F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_142F10066" deadCode="true" name="A305-DECLARE-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_142F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_143F10066" deadCode="true" name="A305-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_143F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10066" deadCode="false" name="A310-SELECT-ID-EFF">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_38F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10066" deadCode="false" name="A310-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_39F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_144F10066" deadCode="true" name="A330-UPDATE-ID-EFF">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_144F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_145F10066" deadCode="true" name="A330-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_145F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_146F10066" deadCode="true" name="A360-OPEN-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_146F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_147F10066" deadCode="true" name="A360-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_147F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_148F10066" deadCode="true" name="A370-CLOSE-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_148F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_149F10066" deadCode="true" name="A370-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_149F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_150F10066" deadCode="true" name="A380-FETCH-FIRST-ID-EFF">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_150F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_153F10066" deadCode="true" name="A380-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_153F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_151F10066" deadCode="true" name="A390-FETCH-NEXT-ID-EFF">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_151F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_152F10066" deadCode="true" name="A390-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_152F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_154F10066" deadCode="false" name="A405-DECLARE-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_154F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_155F10066" deadCode="false" name="A405-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_155F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10066" deadCode="false" name="A410-SELECT-IDP-EFF">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_44F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10066" deadCode="false" name="A410-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_45F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10066" deadCode="false" name="A460-OPEN-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_46F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10066" deadCode="false" name="A460-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_47F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10066" deadCode="false" name="A470-CLOSE-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_48F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10066" deadCode="false" name="A470-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_49F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10066" deadCode="false" name="A480-FETCH-FIRST-IDP-EFF">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_50F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10066" deadCode="false" name="A480-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_51F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10066" deadCode="false" name="A490-FETCH-NEXT-IDP-EFF">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_52F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10066" deadCode="false" name="A490-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_53F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_156F10066" deadCode="false" name="A505-DECLARE-CURSOR-IBO">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_156F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_157F10066" deadCode="false" name="A505-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_157F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10066" deadCode="false" name="A510-SELECT-IBO">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_54F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10066" deadCode="false" name="A510-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_55F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10066" deadCode="false" name="A560-OPEN-CURSOR-IBO">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_56F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10066" deadCode="false" name="A560-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_57F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10066" deadCode="false" name="A570-CLOSE-CURSOR-IBO">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_58F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10066" deadCode="false" name="A570-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_59F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10066" deadCode="false" name="A580-FETCH-FIRST-IBO">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_60F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10066" deadCode="false" name="A580-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_61F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10066" deadCode="false" name="A590-FETCH-NEXT-IBO">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_62F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10066" deadCode="false" name="A590-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_63F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_158F10066" deadCode="false" name="A605-DCL-CUR-IBS-ACC-COMM">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_158F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_159F10066" deadCode="false" name="A605-ACC-COMM-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_159F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_160F10066" deadCode="false" name="A605-DCL-CUR-IBS-ACC-COMM-MAS">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_160F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_161F10066" deadCode="false" name="A605-ACC-COMM-MASTER-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_161F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_162F10066" deadCode="false" name="A605-DECLARE-CURSOR-IBS">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_162F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_163F10066" deadCode="false" name="A605-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_163F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_164F10066" deadCode="false" name="A610-SELECT-IBS-ACC-COMM">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_164F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_165F10066" deadCode="false" name="A610-ACC-COMM-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_165F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_166F10066" deadCode="false" name="A610-SELECT-IBS-ACC-COMM-MAS">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_166F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_167F10066" deadCode="false" name="A610-ACC-COMM-MASTER-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_167F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10066" deadCode="false" name="A610-SELECT-IBS">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_64F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10066" deadCode="false" name="A610-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_65F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10066" deadCode="false" name="A660-OPEN-CURSOR-IBS">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_66F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10066" deadCode="false" name="A660-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_67F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10066" deadCode="false" name="A670-CLOSE-CURSOR-IBS">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_68F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10066" deadCode="false" name="A670-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_69F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10066" deadCode="false" name="A680-FETCH-FIRST-IBS">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_70F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10066" deadCode="false" name="A680-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_71F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_168F10066" deadCode="false" name="A690-FN-IBS-ACC-COMM">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_168F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_169F10066" deadCode="false" name="A690-ACC-COMM-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_169F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_170F10066" deadCode="false" name="A690-FN-IBS-ACC-COMM-MASTER">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_170F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_171F10066" deadCode="false" name="A690-ACC-COMM-MASTER-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_171F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10066" deadCode="false" name="A690-FETCH-NEXT-IBS">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_72F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10066" deadCode="false" name="A690-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_73F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_172F10066" deadCode="false" name="A705-DECLARE-CURSOR-IDO">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_172F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_173F10066" deadCode="false" name="A705-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_173F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10066" deadCode="false" name="A710-SELECT-IDO">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_74F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10066" deadCode="false" name="A710-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_75F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10066" deadCode="false" name="A760-OPEN-CURSOR-IDO">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_76F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10066" deadCode="false" name="A760-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_77F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_78F10066" deadCode="false" name="A770-CLOSE-CURSOR-IDO">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_78F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_79F10066" deadCode="false" name="A770-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_79F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_80F10066" deadCode="false" name="A780-FETCH-FIRST-IDO">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_80F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_81F10066" deadCode="false" name="A780-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_81F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_82F10066" deadCode="false" name="A790-FETCH-NEXT-IDO">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_82F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_83F10066" deadCode="false" name="A790-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_83F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_84F10066" deadCode="false" name="B310-SELECT-ID-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_84F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_85F10066" deadCode="false" name="B310-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_85F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_174F10066" deadCode="false" name="B405-DECLARE-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_174F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_175F10066" deadCode="false" name="B405-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_175F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_86F10066" deadCode="false" name="B410-SELECT-IDP-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_86F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_87F10066" deadCode="false" name="B410-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_87F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_88F10066" deadCode="false" name="B460-OPEN-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_88F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_89F10066" deadCode="false" name="B460-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_89F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_90F10066" deadCode="false" name="B470-CLOSE-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_90F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_91F10066" deadCode="false" name="B470-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_91F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_92F10066" deadCode="false" name="B480-FETCH-FIRST-IDP-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_92F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_93F10066" deadCode="false" name="B480-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_93F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_94F10066" deadCode="false" name="B490-FETCH-NEXT-IDP-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_94F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_95F10066" deadCode="false" name="B490-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_95F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_176F10066" deadCode="false" name="B505-DECLARE-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_176F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_177F10066" deadCode="false" name="B505-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_177F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_96F10066" deadCode="false" name="B510-SELECT-IBO-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_96F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_97F10066" deadCode="false" name="B510-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_97F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_98F10066" deadCode="false" name="B560-OPEN-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_98F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_99F10066" deadCode="false" name="B560-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_99F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_100F10066" deadCode="false" name="B570-CLOSE-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_100F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_101F10066" deadCode="false" name="B570-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_101F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_102F10066" deadCode="false" name="B580-FETCH-FIRST-IBO-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_102F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_103F10066" deadCode="false" name="B580-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_103F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_104F10066" deadCode="false" name="B590-FETCH-NEXT-IBO-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_104F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_105F10066" deadCode="false" name="B590-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_105F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_178F10066" deadCode="false" name="B605-DECLARE-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_178F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_179F10066" deadCode="false" name="B605-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_179F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_106F10066" deadCode="false" name="B610-SELECT-IBS-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_106F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_107F10066" deadCode="false" name="B610-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_107F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_108F10066" deadCode="false" name="B660-OPEN-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_108F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_109F10066" deadCode="false" name="B660-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_109F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_110F10066" deadCode="false" name="B670-CLOSE-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_110F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_111F10066" deadCode="false" name="B670-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_111F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_112F10066" deadCode="false" name="B680-FETCH-FIRST-IBS-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_112F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_113F10066" deadCode="false" name="B680-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_113F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_114F10066" deadCode="false" name="B690-FETCH-NEXT-IBS-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_114F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_115F10066" deadCode="false" name="B690-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_115F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_180F10066" deadCode="false" name="B705-DECLARE-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_180F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_181F10066" deadCode="false" name="B705-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_181F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_116F10066" deadCode="false" name="B710-SELECT-IDO-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_116F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_117F10066" deadCode="false" name="B710-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_117F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_118F10066" deadCode="false" name="B760-OPEN-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_118F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_119F10066" deadCode="false" name="B760-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_119F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_120F10066" deadCode="false" name="B770-CLOSE-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_120F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_121F10066" deadCode="false" name="B770-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_121F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_122F10066" deadCode="false" name="B780-FETCH-FIRST-IDO-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_122F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_123F10066" deadCode="false" name="B780-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_123F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_124F10066" deadCode="false" name="B790-FETCH-NEXT-IDO-CPZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_124F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_125F10066" deadCode="false" name="B790-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_125F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_128F10066" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_128F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_129F10066" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_129F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_134F10066" deadCode="false" name="Z150-VALORIZZA-DATA-SERVICES-I">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_134F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_135F10066" deadCode="false" name="Z150-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_135F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_140F10066" deadCode="false" name="Z160-VALORIZZA-DATA-SERVICES-U">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_140F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_141F10066" deadCode="false" name="Z160-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_141F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_136F10066" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_136F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_137F10066" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_137F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_132F10066" deadCode="false" name="Z400-SEQ-RIGA">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_132F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_133F10066" deadCode="false" name="Z400-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_133F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10066" deadCode="false" name="Z500-AGGIORNAMENTO-STORICO">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_40F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10066" deadCode="false" name="Z500-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_41F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10066" deadCode="false" name="Z550-AGG-STORICO-SOLO-INS">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_42F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10066" deadCode="false" name="Z550-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_43F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_182F10066" deadCode="true" name="Z600-INSERT-NUOVA-RIGA-STORICA">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_182F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_183F10066" deadCode="true" name="Z600-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_183F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_138F10066" deadCode="false" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_138F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_139F10066" deadCode="false" name="Z900-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_139F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_130F10066" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_130F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_131F10066" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_131F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_126F10066" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_126F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_127F10066" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_127F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10066" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_26F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10066" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_27F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_188F10066" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_188F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_189F10066" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_189F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_190F10066" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_190F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_191F10066" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_191F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_184F10066" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_184F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_185F10066" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_185F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_192F10066" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_192F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_193F10066" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_193F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_186F10066" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_186F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_187F10066" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_187F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_194F10066" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_194F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_195F10066" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_195F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_196F10066" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_196F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_197F10066" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_197F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_198F10066" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_198F10066"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_199F10066" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/IDBSP630.cbl.cobModel#P_199F10066"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_ACC_COMM" name="ACC_COMM">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_ACC_COMM"/>
		</children>
	</packageNode>
	<edges id="SC_1F10066P_1F10066" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10066" targetNode="P_1F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10066_I" deadCode="false" sourceNode="P_1F10066" targetNode="P_2F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_1F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10066_O" deadCode="false" sourceNode="P_1F10066" targetNode="P_3F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_1F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10066_I" deadCode="false" sourceNode="P_1F10066" targetNode="P_4F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_5F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10066_O" deadCode="false" sourceNode="P_1F10066" targetNode="P_5F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_5F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10066_I" deadCode="false" sourceNode="P_1F10066" targetNode="P_6F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_6F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10066_O" deadCode="false" sourceNode="P_1F10066" targetNode="P_7F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_6F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10066_I" deadCode="false" sourceNode="P_1F10066" targetNode="P_8F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_7F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10066_O" deadCode="false" sourceNode="P_1F10066" targetNode="P_9F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_7F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10066_I" deadCode="false" sourceNode="P_1F10066" targetNode="P_10F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_8F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10066_O" deadCode="false" sourceNode="P_1F10066" targetNode="P_11F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_8F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10066_I" deadCode="false" sourceNode="P_1F10066" targetNode="P_12F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_9F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10066_O" deadCode="false" sourceNode="P_1F10066" targetNode="P_13F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_9F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10066_I" deadCode="false" sourceNode="P_1F10066" targetNode="P_14F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_13F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10066_O" deadCode="false" sourceNode="P_1F10066" targetNode="P_15F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_13F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10066_I" deadCode="false" sourceNode="P_1F10066" targetNode="P_16F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_14F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10066_O" deadCode="false" sourceNode="P_1F10066" targetNode="P_17F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_14F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10066_I" deadCode="false" sourceNode="P_1F10066" targetNode="P_18F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_15F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10066_O" deadCode="false" sourceNode="P_1F10066" targetNode="P_19F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_15F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10066_I" deadCode="false" sourceNode="P_1F10066" targetNode="P_20F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_16F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10066_O" deadCode="false" sourceNode="P_1F10066" targetNode="P_21F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_16F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10066_I" deadCode="false" sourceNode="P_1F10066" targetNode="P_22F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_17F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10066_O" deadCode="false" sourceNode="P_1F10066" targetNode="P_23F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_17F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10066_I" deadCode="false" sourceNode="P_1F10066" targetNode="P_24F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_21F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10066_O" deadCode="false" sourceNode="P_1F10066" targetNode="P_25F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_21F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10066_I" deadCode="false" sourceNode="P_1F10066" targetNode="P_8F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_22F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10066_O" deadCode="false" sourceNode="P_1F10066" targetNode="P_9F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_22F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10066_I" deadCode="false" sourceNode="P_1F10066" targetNode="P_10F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_23F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10066_O" deadCode="false" sourceNode="P_1F10066" targetNode="P_11F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_23F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10066_I" deadCode="false" sourceNode="P_1F10066" targetNode="P_12F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_24F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10066_O" deadCode="false" sourceNode="P_1F10066" targetNode="P_13F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_24F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10066_I" deadCode="false" sourceNode="P_2F10066" targetNode="P_26F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_33F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10066_O" deadCode="false" sourceNode="P_2F10066" targetNode="P_27F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_33F10066"/>
	</edges>
	<edges id="P_2F10066P_3F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10066" targetNode="P_3F10066"/>
	<edges id="P_28F10066P_29F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10066" targetNode="P_29F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10066_I" deadCode="false" sourceNode="P_24F10066" targetNode="P_30F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_46F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10066_O" deadCode="false" sourceNode="P_24F10066" targetNode="P_31F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_46F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10066_I" deadCode="false" sourceNode="P_24F10066" targetNode="P_32F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_47F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10066_O" deadCode="false" sourceNode="P_24F10066" targetNode="P_33F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_47F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10066_I" deadCode="false" sourceNode="P_24F10066" targetNode="P_34F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_48F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10066_O" deadCode="false" sourceNode="P_24F10066" targetNode="P_35F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_48F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10066_I" deadCode="false" sourceNode="P_24F10066" targetNode="P_36F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_49F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10066_O" deadCode="false" sourceNode="P_24F10066" targetNode="P_37F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_49F10066"/>
	</edges>
	<edges id="P_24F10066P_25F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10066" targetNode="P_25F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10066_I" deadCode="false" sourceNode="P_4F10066" targetNode="P_38F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_53F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10066_O" deadCode="false" sourceNode="P_4F10066" targetNode="P_39F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_53F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10066_I" deadCode="false" sourceNode="P_4F10066" targetNode="P_40F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_54F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10066_O" deadCode="false" sourceNode="P_4F10066" targetNode="P_41F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_54F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10066_I" deadCode="false" sourceNode="P_4F10066" targetNode="P_42F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_55F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10066_O" deadCode="false" sourceNode="P_4F10066" targetNode="P_43F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_55F10066"/>
	</edges>
	<edges id="P_4F10066P_5F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10066" targetNode="P_5F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10066_I" deadCode="false" sourceNode="P_6F10066" targetNode="P_44F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_59F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10066_O" deadCode="false" sourceNode="P_6F10066" targetNode="P_45F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_59F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10066_I" deadCode="false" sourceNode="P_6F10066" targetNode="P_46F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_60F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10066_O" deadCode="false" sourceNode="P_6F10066" targetNode="P_47F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_60F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10066_I" deadCode="false" sourceNode="P_6F10066" targetNode="P_48F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_61F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10066_O" deadCode="false" sourceNode="P_6F10066" targetNode="P_49F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_61F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10066_I" deadCode="false" sourceNode="P_6F10066" targetNode="P_50F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_62F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10066_O" deadCode="false" sourceNode="P_6F10066" targetNode="P_51F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_62F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10066_I" deadCode="false" sourceNode="P_6F10066" targetNode="P_52F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_63F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10066_O" deadCode="false" sourceNode="P_6F10066" targetNode="P_53F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_63F10066"/>
	</edges>
	<edges id="P_6F10066P_7F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10066" targetNode="P_7F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10066_I" deadCode="false" sourceNode="P_8F10066" targetNode="P_54F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_67F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10066_O" deadCode="false" sourceNode="P_8F10066" targetNode="P_55F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_67F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10066_I" deadCode="false" sourceNode="P_8F10066" targetNode="P_56F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_68F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10066_O" deadCode="false" sourceNode="P_8F10066" targetNode="P_57F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_68F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10066_I" deadCode="false" sourceNode="P_8F10066" targetNode="P_58F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_69F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10066_O" deadCode="false" sourceNode="P_8F10066" targetNode="P_59F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_69F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10066_I" deadCode="false" sourceNode="P_8F10066" targetNode="P_60F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_70F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10066_O" deadCode="false" sourceNode="P_8F10066" targetNode="P_61F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_70F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10066_I" deadCode="false" sourceNode="P_8F10066" targetNode="P_62F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_71F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10066_O" deadCode="false" sourceNode="P_8F10066" targetNode="P_63F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_71F10066"/>
	</edges>
	<edges id="P_8F10066P_9F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10066" targetNode="P_9F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10066_I" deadCode="false" sourceNode="P_10F10066" targetNode="P_64F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_75F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10066_O" deadCode="false" sourceNode="P_10F10066" targetNode="P_65F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_75F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10066_I" deadCode="false" sourceNode="P_10F10066" targetNode="P_66F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_76F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10066_O" deadCode="false" sourceNode="P_10F10066" targetNode="P_67F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_76F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10066_I" deadCode="false" sourceNode="P_10F10066" targetNode="P_68F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_77F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10066_O" deadCode="false" sourceNode="P_10F10066" targetNode="P_69F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_77F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10066_I" deadCode="false" sourceNode="P_10F10066" targetNode="P_70F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_78F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10066_O" deadCode="false" sourceNode="P_10F10066" targetNode="P_71F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_78F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10066_I" deadCode="false" sourceNode="P_10F10066" targetNode="P_72F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_79F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10066_O" deadCode="false" sourceNode="P_10F10066" targetNode="P_73F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_79F10066"/>
	</edges>
	<edges id="P_10F10066P_11F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10066" targetNode="P_11F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10066_I" deadCode="false" sourceNode="P_12F10066" targetNode="P_74F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_83F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10066_O" deadCode="false" sourceNode="P_12F10066" targetNode="P_75F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_83F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10066_I" deadCode="false" sourceNode="P_12F10066" targetNode="P_76F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_84F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10066_O" deadCode="false" sourceNode="P_12F10066" targetNode="P_77F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_84F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10066_I" deadCode="false" sourceNode="P_12F10066" targetNode="P_78F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_85F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10066_O" deadCode="false" sourceNode="P_12F10066" targetNode="P_79F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_85F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10066_I" deadCode="false" sourceNode="P_12F10066" targetNode="P_80F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_86F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10066_O" deadCode="false" sourceNode="P_12F10066" targetNode="P_81F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_86F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10066_I" deadCode="false" sourceNode="P_12F10066" targetNode="P_82F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_87F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10066_O" deadCode="false" sourceNode="P_12F10066" targetNode="P_83F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_87F10066"/>
	</edges>
	<edges id="P_12F10066P_13F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10066" targetNode="P_13F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10066_I" deadCode="false" sourceNode="P_14F10066" targetNode="P_84F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_91F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10066_O" deadCode="false" sourceNode="P_14F10066" targetNode="P_85F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_91F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10066_I" deadCode="false" sourceNode="P_14F10066" targetNode="P_40F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_92F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10066_O" deadCode="false" sourceNode="P_14F10066" targetNode="P_41F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_92F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10066_I" deadCode="false" sourceNode="P_14F10066" targetNode="P_42F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_93F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10066_O" deadCode="false" sourceNode="P_14F10066" targetNode="P_43F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_93F10066"/>
	</edges>
	<edges id="P_14F10066P_15F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10066" targetNode="P_15F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10066_I" deadCode="false" sourceNode="P_16F10066" targetNode="P_86F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_97F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10066_O" deadCode="false" sourceNode="P_16F10066" targetNode="P_87F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_97F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10066_I" deadCode="false" sourceNode="P_16F10066" targetNode="P_88F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_98F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10066_O" deadCode="false" sourceNode="P_16F10066" targetNode="P_89F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_98F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10066_I" deadCode="false" sourceNode="P_16F10066" targetNode="P_90F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_99F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10066_O" deadCode="false" sourceNode="P_16F10066" targetNode="P_91F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_99F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10066_I" deadCode="false" sourceNode="P_16F10066" targetNode="P_92F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_100F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10066_O" deadCode="false" sourceNode="P_16F10066" targetNode="P_93F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_100F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10066_I" deadCode="false" sourceNode="P_16F10066" targetNode="P_94F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_101F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10066_O" deadCode="false" sourceNode="P_16F10066" targetNode="P_95F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_101F10066"/>
	</edges>
	<edges id="P_16F10066P_17F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10066" targetNode="P_17F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10066_I" deadCode="false" sourceNode="P_18F10066" targetNode="P_96F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_105F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10066_O" deadCode="false" sourceNode="P_18F10066" targetNode="P_97F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_105F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10066_I" deadCode="false" sourceNode="P_18F10066" targetNode="P_98F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_106F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10066_O" deadCode="false" sourceNode="P_18F10066" targetNode="P_99F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_106F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10066_I" deadCode="false" sourceNode="P_18F10066" targetNode="P_100F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_107F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10066_O" deadCode="false" sourceNode="P_18F10066" targetNode="P_101F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_107F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10066_I" deadCode="false" sourceNode="P_18F10066" targetNode="P_102F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_108F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10066_O" deadCode="false" sourceNode="P_18F10066" targetNode="P_103F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_108F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10066_I" deadCode="false" sourceNode="P_18F10066" targetNode="P_104F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_109F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10066_O" deadCode="false" sourceNode="P_18F10066" targetNode="P_105F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_109F10066"/>
	</edges>
	<edges id="P_18F10066P_19F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10066" targetNode="P_19F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10066_I" deadCode="false" sourceNode="P_20F10066" targetNode="P_106F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_113F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10066_O" deadCode="false" sourceNode="P_20F10066" targetNode="P_107F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_113F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10066_I" deadCode="false" sourceNode="P_20F10066" targetNode="P_108F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_114F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10066_O" deadCode="false" sourceNode="P_20F10066" targetNode="P_109F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_114F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10066_I" deadCode="false" sourceNode="P_20F10066" targetNode="P_110F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_115F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10066_O" deadCode="false" sourceNode="P_20F10066" targetNode="P_111F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_115F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10066_I" deadCode="false" sourceNode="P_20F10066" targetNode="P_112F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_116F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10066_O" deadCode="false" sourceNode="P_20F10066" targetNode="P_113F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_116F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10066_I" deadCode="false" sourceNode="P_20F10066" targetNode="P_114F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_117F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10066_O" deadCode="false" sourceNode="P_20F10066" targetNode="P_115F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_117F10066"/>
	</edges>
	<edges id="P_20F10066P_21F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10066" targetNode="P_21F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10066_I" deadCode="false" sourceNode="P_22F10066" targetNode="P_116F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_121F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10066_O" deadCode="false" sourceNode="P_22F10066" targetNode="P_117F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_121F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10066_I" deadCode="false" sourceNode="P_22F10066" targetNode="P_118F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_122F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10066_O" deadCode="false" sourceNode="P_22F10066" targetNode="P_119F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_122F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10066_I" deadCode="false" sourceNode="P_22F10066" targetNode="P_120F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_123F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10066_O" deadCode="false" sourceNode="P_22F10066" targetNode="P_121F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_123F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10066_I" deadCode="false" sourceNode="P_22F10066" targetNode="P_122F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_124F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10066_O" deadCode="false" sourceNode="P_22F10066" targetNode="P_123F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_124F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10066_I" deadCode="false" sourceNode="P_22F10066" targetNode="P_124F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_125F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10066_O" deadCode="false" sourceNode="P_22F10066" targetNode="P_125F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_125F10066"/>
	</edges>
	<edges id="P_22F10066P_23F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10066" targetNode="P_23F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10066_I" deadCode="false" sourceNode="P_30F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_128F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10066_O" deadCode="false" sourceNode="P_30F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_128F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10066_I" deadCode="false" sourceNode="P_30F10066" targetNode="P_28F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_130F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10066_O" deadCode="false" sourceNode="P_30F10066" targetNode="P_29F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_130F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10066_I" deadCode="false" sourceNode="P_30F10066" targetNode="P_128F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_132F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10066_O" deadCode="false" sourceNode="P_30F10066" targetNode="P_129F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_132F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10066_I" deadCode="false" sourceNode="P_30F10066" targetNode="P_130F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_133F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10066_O" deadCode="false" sourceNode="P_30F10066" targetNode="P_131F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_133F10066"/>
	</edges>
	<edges id="P_30F10066P_31F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10066" targetNode="P_31F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10066_I" deadCode="false" sourceNode="P_32F10066" targetNode="P_132F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_135F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10066_O" deadCode="false" sourceNode="P_32F10066" targetNode="P_133F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_135F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10066_I" deadCode="false" sourceNode="P_32F10066" targetNode="P_134F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_137F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10066_O" deadCode="false" sourceNode="P_32F10066" targetNode="P_135F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_137F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10066_I" deadCode="false" sourceNode="P_32F10066" targetNode="P_136F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_138F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10066_O" deadCode="false" sourceNode="P_32F10066" targetNode="P_137F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_138F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10066_I" deadCode="false" sourceNode="P_32F10066" targetNode="P_138F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_139F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10066_O" deadCode="false" sourceNode="P_32F10066" targetNode="P_139F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_139F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10066_I" deadCode="false" sourceNode="P_32F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_140F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10066_O" deadCode="false" sourceNode="P_32F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_140F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10066_I" deadCode="false" sourceNode="P_32F10066" targetNode="P_28F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_142F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10066_O" deadCode="false" sourceNode="P_32F10066" targetNode="P_29F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_142F10066"/>
	</edges>
	<edges id="P_32F10066P_33F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10066" targetNode="P_33F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10066_I" deadCode="false" sourceNode="P_34F10066" targetNode="P_140F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_144F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10066_O" deadCode="false" sourceNode="P_34F10066" targetNode="P_141F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_144F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10066_I" deadCode="false" sourceNode="P_34F10066" targetNode="P_136F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_145F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10066_O" deadCode="false" sourceNode="P_34F10066" targetNode="P_137F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_145F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10066_I" deadCode="false" sourceNode="P_34F10066" targetNode="P_138F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_146F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10066_O" deadCode="false" sourceNode="P_34F10066" targetNode="P_139F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_146F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10066_I" deadCode="false" sourceNode="P_34F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_147F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10066_O" deadCode="false" sourceNode="P_34F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_147F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10066_I" deadCode="false" sourceNode="P_34F10066" targetNode="P_28F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_149F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10066_O" deadCode="false" sourceNode="P_34F10066" targetNode="P_29F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_149F10066"/>
	</edges>
	<edges id="P_34F10066P_35F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10066" targetNode="P_35F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10066_I" deadCode="false" sourceNode="P_36F10066" targetNode="P_28F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_152F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10066_O" deadCode="false" sourceNode="P_36F10066" targetNode="P_29F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_152F10066"/>
	</edges>
	<edges id="P_36F10066P_37F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10066" targetNode="P_37F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10066_I" deadCode="true" sourceNode="P_142F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_154F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10066_O" deadCode="true" sourceNode="P_142F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_154F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10066_I" deadCode="false" sourceNode="P_38F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_157F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10066_O" deadCode="false" sourceNode="P_38F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_157F10066"/>
	</edges>
	<edges id="P_38F10066P_39F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10066" targetNode="P_39F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10066_I" deadCode="true" sourceNode="P_144F10066" targetNode="P_140F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_160F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10066_O" deadCode="true" sourceNode="P_144F10066" targetNode="P_141F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_160F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10066_I" deadCode="true" sourceNode="P_144F10066" targetNode="P_136F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_161F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10066_O" deadCode="true" sourceNode="P_144F10066" targetNode="P_137F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_161F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10066_I" deadCode="true" sourceNode="P_144F10066" targetNode="P_138F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_162F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10066_O" deadCode="true" sourceNode="P_144F10066" targetNode="P_139F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_162F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10066_I" deadCode="true" sourceNode="P_144F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_163F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10066_O" deadCode="true" sourceNode="P_144F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_163F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10066_I" deadCode="true" sourceNode="P_146F10066" targetNode="P_142F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_166F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10066_O" deadCode="true" sourceNode="P_146F10066" targetNode="P_143F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_166F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10066_I" deadCode="true" sourceNode="P_150F10066" targetNode="P_146F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_171F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10066_O" deadCode="true" sourceNode="P_150F10066" targetNode="P_147F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_171F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10066_I" deadCode="true" sourceNode="P_150F10066" targetNode="P_151F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_173F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10066_O" deadCode="true" sourceNode="P_150F10066" targetNode="P_152F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_173F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10066_I" deadCode="false" sourceNode="P_154F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_177F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10066_O" deadCode="false" sourceNode="P_154F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_177F10066"/>
	</edges>
	<edges id="P_154F10066P_155F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_154F10066" targetNode="P_155F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_180F10066_I" deadCode="false" sourceNode="P_44F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_180F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_180F10066_O" deadCode="false" sourceNode="P_44F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_180F10066"/>
	</edges>
	<edges id="P_44F10066P_45F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10066" targetNode="P_45F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_183F10066_I" deadCode="false" sourceNode="P_46F10066" targetNode="P_154F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_183F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_183F10066_O" deadCode="false" sourceNode="P_46F10066" targetNode="P_155F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_183F10066"/>
	</edges>
	<edges id="P_46F10066P_47F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10066" targetNode="P_47F10066"/>
	<edges id="P_48F10066P_49F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10066" targetNode="P_49F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_188F10066_I" deadCode="false" sourceNode="P_50F10066" targetNode="P_46F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_188F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_188F10066_O" deadCode="false" sourceNode="P_50F10066" targetNode="P_47F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_188F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_190F10066_I" deadCode="false" sourceNode="P_50F10066" targetNode="P_52F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_190F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_190F10066_O" deadCode="false" sourceNode="P_50F10066" targetNode="P_53F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_190F10066"/>
	</edges>
	<edges id="P_50F10066P_51F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10066" targetNode="P_51F10066"/>
	<edges id="P_52F10066P_53F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10066" targetNode="P_53F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_194F10066_I" deadCode="false" sourceNode="P_156F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_194F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_194F10066_O" deadCode="false" sourceNode="P_156F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_194F10066"/>
	</edges>
	<edges id="P_156F10066P_157F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_156F10066" targetNode="P_157F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_197F10066_I" deadCode="false" sourceNode="P_54F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_197F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_197F10066_O" deadCode="false" sourceNode="P_54F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_197F10066"/>
	</edges>
	<edges id="P_54F10066P_55F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10066" targetNode="P_55F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_200F10066_I" deadCode="false" sourceNode="P_56F10066" targetNode="P_156F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_200F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_200F10066_O" deadCode="false" sourceNode="P_56F10066" targetNode="P_157F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_200F10066"/>
	</edges>
	<edges id="P_56F10066P_57F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10066" targetNode="P_57F10066"/>
	<edges id="P_58F10066P_59F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10066" targetNode="P_59F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_205F10066_I" deadCode="false" sourceNode="P_60F10066" targetNode="P_56F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_205F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_205F10066_O" deadCode="false" sourceNode="P_60F10066" targetNode="P_57F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_205F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_207F10066_I" deadCode="false" sourceNode="P_60F10066" targetNode="P_62F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_207F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_207F10066_O" deadCode="false" sourceNode="P_60F10066" targetNode="P_63F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_207F10066"/>
	</edges>
	<edges id="P_60F10066P_61F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_60F10066" targetNode="P_61F10066"/>
	<edges id="P_62F10066P_63F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10066" targetNode="P_63F10066"/>
	<edges id="P_158F10066P_159F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_158F10066" targetNode="P_159F10066"/>
	<edges id="P_160F10066P_161F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_160F10066" targetNode="P_161F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_217F10066_I" deadCode="false" sourceNode="P_162F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_217F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_217F10066_O" deadCode="false" sourceNode="P_162F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_217F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_219F10066_I" deadCode="false" sourceNode="P_162F10066" targetNode="P_158F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_219F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_219F10066_O" deadCode="false" sourceNode="P_162F10066" targetNode="P_159F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_219F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10066_I" deadCode="false" sourceNode="P_162F10066" targetNode="P_160F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_221F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10066_O" deadCode="false" sourceNode="P_162F10066" targetNode="P_161F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_221F10066"/>
	</edges>
	<edges id="P_162F10066P_163F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_162F10066" targetNode="P_163F10066"/>
	<edges id="P_164F10066P_165F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_164F10066" targetNode="P_165F10066"/>
	<edges id="P_166F10066P_167F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_166F10066" targetNode="P_167F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_227F10066_I" deadCode="false" sourceNode="P_64F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_227F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_227F10066_O" deadCode="false" sourceNode="P_64F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_227F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_229F10066_I" deadCode="false" sourceNode="P_64F10066" targetNode="P_164F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_229F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_229F10066_O" deadCode="false" sourceNode="P_64F10066" targetNode="P_165F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_229F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10066_I" deadCode="false" sourceNode="P_64F10066" targetNode="P_166F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_231F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10066_O" deadCode="false" sourceNode="P_64F10066" targetNode="P_167F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_231F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_232F10066_I" deadCode="false" sourceNode="P_64F10066" targetNode="P_28F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_232F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_232F10066_O" deadCode="false" sourceNode="P_64F10066" targetNode="P_29F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_232F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_234F10066_I" deadCode="false" sourceNode="P_64F10066" targetNode="P_128F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_234F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_234F10066_O" deadCode="false" sourceNode="P_64F10066" targetNode="P_129F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_234F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_235F10066_I" deadCode="false" sourceNode="P_64F10066" targetNode="P_130F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_235F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_235F10066_O" deadCode="false" sourceNode="P_64F10066" targetNode="P_131F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_235F10066"/>
	</edges>
	<edges id="P_64F10066P_65F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10066" targetNode="P_65F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_237F10066_I" deadCode="false" sourceNode="P_66F10066" targetNode="P_162F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_237F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_237F10066_O" deadCode="false" sourceNode="P_66F10066" targetNode="P_163F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_237F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10066_I" deadCode="false" sourceNode="P_66F10066" targetNode="P_28F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_242F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10066_O" deadCode="false" sourceNode="P_66F10066" targetNode="P_29F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_242F10066"/>
	</edges>
	<edges id="P_66F10066P_67F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10066" targetNode="P_67F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_248F10066_I" deadCode="false" sourceNode="P_68F10066" targetNode="P_28F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_248F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_248F10066_O" deadCode="false" sourceNode="P_68F10066" targetNode="P_29F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_248F10066"/>
	</edges>
	<edges id="P_68F10066P_69F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10066" targetNode="P_69F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_250F10066_I" deadCode="false" sourceNode="P_70F10066" targetNode="P_66F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_250F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_250F10066_O" deadCode="false" sourceNode="P_70F10066" targetNode="P_67F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_250F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_252F10066_I" deadCode="false" sourceNode="P_70F10066" targetNode="P_72F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_252F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_252F10066_O" deadCode="false" sourceNode="P_70F10066" targetNode="P_73F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_252F10066"/>
	</edges>
	<edges id="P_70F10066P_71F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_70F10066" targetNode="P_71F10066"/>
	<edges id="P_168F10066P_169F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_168F10066" targetNode="P_169F10066"/>
	<edges id="P_170F10066P_171F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_170F10066" targetNode="P_171F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10066_I" deadCode="false" sourceNode="P_72F10066" targetNode="P_168F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_259F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10066_O" deadCode="false" sourceNode="P_72F10066" targetNode="P_169F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_259F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_261F10066_I" deadCode="false" sourceNode="P_72F10066" targetNode="P_170F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_261F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_261F10066_O" deadCode="false" sourceNode="P_72F10066" targetNode="P_171F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_261F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10066_I" deadCode="false" sourceNode="P_72F10066" targetNode="P_28F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_262F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10066_O" deadCode="false" sourceNode="P_72F10066" targetNode="P_29F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_262F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_264F10066_I" deadCode="false" sourceNode="P_72F10066" targetNode="P_128F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_264F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_264F10066_O" deadCode="false" sourceNode="P_72F10066" targetNode="P_129F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_264F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10066_I" deadCode="false" sourceNode="P_72F10066" targetNode="P_130F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_265F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10066_O" deadCode="false" sourceNode="P_72F10066" targetNode="P_131F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_265F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_267F10066_I" deadCode="false" sourceNode="P_72F10066" targetNode="P_68F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_267F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_267F10066_O" deadCode="false" sourceNode="P_72F10066" targetNode="P_69F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_267F10066"/>
	</edges>
	<edges id="P_72F10066P_73F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10066" targetNode="P_73F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_271F10066_I" deadCode="false" sourceNode="P_172F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_271F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_271F10066_O" deadCode="false" sourceNode="P_172F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_271F10066"/>
	</edges>
	<edges id="P_172F10066P_173F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_172F10066" targetNode="P_173F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_274F10066_I" deadCode="false" sourceNode="P_74F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_274F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_274F10066_O" deadCode="false" sourceNode="P_74F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_274F10066"/>
	</edges>
	<edges id="P_74F10066P_75F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10066" targetNode="P_75F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_277F10066_I" deadCode="false" sourceNode="P_76F10066" targetNode="P_172F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_277F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_277F10066_O" deadCode="false" sourceNode="P_76F10066" targetNode="P_173F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_277F10066"/>
	</edges>
	<edges id="P_76F10066P_77F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_76F10066" targetNode="P_77F10066"/>
	<edges id="P_78F10066P_79F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_78F10066" targetNode="P_79F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_282F10066_I" deadCode="false" sourceNode="P_80F10066" targetNode="P_76F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_282F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_282F10066_O" deadCode="false" sourceNode="P_80F10066" targetNode="P_77F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_282F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10066_I" deadCode="false" sourceNode="P_80F10066" targetNode="P_82F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_284F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10066_O" deadCode="false" sourceNode="P_80F10066" targetNode="P_83F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_284F10066"/>
	</edges>
	<edges id="P_80F10066P_81F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_80F10066" targetNode="P_81F10066"/>
	<edges id="P_82F10066P_83F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_82F10066" targetNode="P_83F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_288F10066_I" deadCode="false" sourceNode="P_84F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_288F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_288F10066_O" deadCode="false" sourceNode="P_84F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_288F10066"/>
	</edges>
	<edges id="P_84F10066P_85F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_84F10066" targetNode="P_85F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_291F10066_I" deadCode="false" sourceNode="P_174F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_291F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_291F10066_O" deadCode="false" sourceNode="P_174F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_291F10066"/>
	</edges>
	<edges id="P_174F10066P_175F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_174F10066" targetNode="P_175F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10066_I" deadCode="false" sourceNode="P_86F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_294F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10066_O" deadCode="false" sourceNode="P_86F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_294F10066"/>
	</edges>
	<edges id="P_86F10066P_87F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_86F10066" targetNode="P_87F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_297F10066_I" deadCode="false" sourceNode="P_88F10066" targetNode="P_174F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_297F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_297F10066_O" deadCode="false" sourceNode="P_88F10066" targetNode="P_175F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_297F10066"/>
	</edges>
	<edges id="P_88F10066P_89F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_88F10066" targetNode="P_89F10066"/>
	<edges id="P_90F10066P_91F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_90F10066" targetNode="P_91F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_302F10066_I" deadCode="false" sourceNode="P_92F10066" targetNode="P_88F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_302F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_302F10066_O" deadCode="false" sourceNode="P_92F10066" targetNode="P_89F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_302F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_304F10066_I" deadCode="false" sourceNode="P_92F10066" targetNode="P_94F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_304F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_304F10066_O" deadCode="false" sourceNode="P_92F10066" targetNode="P_95F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_304F10066"/>
	</edges>
	<edges id="P_92F10066P_93F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_92F10066" targetNode="P_93F10066"/>
	<edges id="P_94F10066P_95F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_94F10066" targetNode="P_95F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10066_I" deadCode="false" sourceNode="P_176F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_308F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10066_O" deadCode="false" sourceNode="P_176F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_308F10066"/>
	</edges>
	<edges id="P_176F10066P_177F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_176F10066" targetNode="P_177F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10066_I" deadCode="false" sourceNode="P_96F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_311F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10066_O" deadCode="false" sourceNode="P_96F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_311F10066"/>
	</edges>
	<edges id="P_96F10066P_97F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_96F10066" targetNode="P_97F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_314F10066_I" deadCode="false" sourceNode="P_98F10066" targetNode="P_176F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_314F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_314F10066_O" deadCode="false" sourceNode="P_98F10066" targetNode="P_177F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_314F10066"/>
	</edges>
	<edges id="P_98F10066P_99F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_98F10066" targetNode="P_99F10066"/>
	<edges id="P_100F10066P_101F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_100F10066" targetNode="P_101F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_319F10066_I" deadCode="false" sourceNode="P_102F10066" targetNode="P_98F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_319F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_319F10066_O" deadCode="false" sourceNode="P_102F10066" targetNode="P_99F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_319F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10066_I" deadCode="false" sourceNode="P_102F10066" targetNode="P_104F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_321F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10066_O" deadCode="false" sourceNode="P_102F10066" targetNode="P_105F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_321F10066"/>
	</edges>
	<edges id="P_102F10066P_103F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_102F10066" targetNode="P_103F10066"/>
	<edges id="P_104F10066P_105F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_104F10066" targetNode="P_105F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_325F10066_I" deadCode="false" sourceNode="P_178F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_325F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_325F10066_O" deadCode="false" sourceNode="P_178F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_325F10066"/>
	</edges>
	<edges id="P_178F10066P_179F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_178F10066" targetNode="P_179F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_328F10066_I" deadCode="false" sourceNode="P_106F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_328F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_328F10066_O" deadCode="false" sourceNode="P_106F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_328F10066"/>
	</edges>
	<edges id="P_106F10066P_107F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_106F10066" targetNode="P_107F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_331F10066_I" deadCode="false" sourceNode="P_108F10066" targetNode="P_178F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_331F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_331F10066_O" deadCode="false" sourceNode="P_108F10066" targetNode="P_179F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_331F10066"/>
	</edges>
	<edges id="P_108F10066P_109F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_108F10066" targetNode="P_109F10066"/>
	<edges id="P_110F10066P_111F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_110F10066" targetNode="P_111F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10066_I" deadCode="false" sourceNode="P_112F10066" targetNode="P_108F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_336F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10066_O" deadCode="false" sourceNode="P_112F10066" targetNode="P_109F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_336F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10066_I" deadCode="false" sourceNode="P_112F10066" targetNode="P_114F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_338F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10066_O" deadCode="false" sourceNode="P_112F10066" targetNode="P_115F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_338F10066"/>
	</edges>
	<edges id="P_112F10066P_113F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_112F10066" targetNode="P_113F10066"/>
	<edges id="P_114F10066P_115F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_114F10066" targetNode="P_115F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_342F10066_I" deadCode="false" sourceNode="P_180F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_342F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_342F10066_O" deadCode="false" sourceNode="P_180F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_342F10066"/>
	</edges>
	<edges id="P_180F10066P_181F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_180F10066" targetNode="P_181F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_345F10066_I" deadCode="false" sourceNode="P_116F10066" targetNode="P_126F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_345F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_345F10066_O" deadCode="false" sourceNode="P_116F10066" targetNode="P_127F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_345F10066"/>
	</edges>
	<edges id="P_116F10066P_117F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_116F10066" targetNode="P_117F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_348F10066_I" deadCode="false" sourceNode="P_118F10066" targetNode="P_180F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_348F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_348F10066_O" deadCode="false" sourceNode="P_118F10066" targetNode="P_181F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_348F10066"/>
	</edges>
	<edges id="P_118F10066P_119F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_118F10066" targetNode="P_119F10066"/>
	<edges id="P_120F10066P_121F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_120F10066" targetNode="P_121F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_353F10066_I" deadCode="false" sourceNode="P_122F10066" targetNode="P_118F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_353F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_353F10066_O" deadCode="false" sourceNode="P_122F10066" targetNode="P_119F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_353F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_355F10066_I" deadCode="false" sourceNode="P_122F10066" targetNode="P_124F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_355F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_355F10066_O" deadCode="false" sourceNode="P_122F10066" targetNode="P_125F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_355F10066"/>
	</edges>
	<edges id="P_122F10066P_123F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_122F10066" targetNode="P_123F10066"/>
	<edges id="P_124F10066P_125F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_124F10066" targetNode="P_125F10066"/>
	<edges id="P_128F10066P_129F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_128F10066" targetNode="P_129F10066"/>
	<edges id="P_134F10066P_135F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_134F10066" targetNode="P_135F10066"/>
	<edges id="P_140F10066P_141F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_140F10066" targetNode="P_141F10066"/>
	<edges id="P_136F10066P_137F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_136F10066" targetNode="P_137F10066"/>
	<edges id="P_132F10066P_133F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_132F10066" targetNode="P_133F10066"/>
	<edges id="P_40F10066P_41F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10066" targetNode="P_41F10066"/>
	<edges id="P_42F10066P_43F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10066" targetNode="P_43F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_386F10066_I" deadCode="false" sourceNode="P_138F10066" targetNode="P_184F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_386F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_386F10066_O" deadCode="false" sourceNode="P_138F10066" targetNode="P_185F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_386F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_389F10066_I" deadCode="false" sourceNode="P_138F10066" targetNode="P_184F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_389F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_389F10066_O" deadCode="false" sourceNode="P_138F10066" targetNode="P_185F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_389F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_392F10066_I" deadCode="false" sourceNode="P_138F10066" targetNode="P_184F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_392F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_392F10066_O" deadCode="false" sourceNode="P_138F10066" targetNode="P_185F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_392F10066"/>
	</edges>
	<edges id="P_138F10066P_139F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_138F10066" targetNode="P_139F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_396F10066_I" deadCode="false" sourceNode="P_130F10066" targetNode="P_186F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_396F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_396F10066_O" deadCode="false" sourceNode="P_130F10066" targetNode="P_187F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_396F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_399F10066_I" deadCode="false" sourceNode="P_130F10066" targetNode="P_186F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_399F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_399F10066_O" deadCode="false" sourceNode="P_130F10066" targetNode="P_187F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_399F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_402F10066_I" deadCode="false" sourceNode="P_130F10066" targetNode="P_186F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_402F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_402F10066_O" deadCode="false" sourceNode="P_130F10066" targetNode="P_187F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_402F10066"/>
	</edges>
	<edges id="P_130F10066P_131F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_130F10066" targetNode="P_131F10066"/>
	<edges id="P_126F10066P_127F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_126F10066" targetNode="P_127F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_408F10066_I" deadCode="false" sourceNode="P_26F10066" targetNode="P_188F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_408F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_408F10066_O" deadCode="false" sourceNode="P_26F10066" targetNode="P_189F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_408F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_410F10066_I" deadCode="false" sourceNode="P_26F10066" targetNode="P_190F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_410F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_410F10066_O" deadCode="false" sourceNode="P_26F10066" targetNode="P_191F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_410F10066"/>
	</edges>
	<edges id="P_26F10066P_27F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10066" targetNode="P_27F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_415F10066_I" deadCode="false" sourceNode="P_188F10066" targetNode="P_184F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_415F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_415F10066_O" deadCode="false" sourceNode="P_188F10066" targetNode="P_185F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_415F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_420F10066_I" deadCode="false" sourceNode="P_188F10066" targetNode="P_184F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_420F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_420F10066_O" deadCode="false" sourceNode="P_188F10066" targetNode="P_185F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_420F10066"/>
	</edges>
	<edges id="P_188F10066P_189F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_188F10066" targetNode="P_189F10066"/>
	<edges id="P_190F10066P_191F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_190F10066" targetNode="P_191F10066"/>
	<edges id="P_184F10066P_185F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_184F10066" targetNode="P_185F10066"/>
	<edges xsi:type="cbl:PerformEdge" id="S_449F10066_I" deadCode="false" sourceNode="P_186F10066" targetNode="P_194F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_449F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_449F10066_O" deadCode="false" sourceNode="P_186F10066" targetNode="P_195F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_449F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_450F10066_I" deadCode="false" sourceNode="P_186F10066" targetNode="P_196F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_450F10066"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_450F10066_O" deadCode="false" sourceNode="P_186F10066" targetNode="P_197F10066">
		<representations href="../../../cobol/IDBSP630.cbl.cobModel#S_450F10066"/>
	</edges>
	<edges id="P_186F10066P_187F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_186F10066" targetNode="P_187F10066"/>
	<edges id="P_194F10066P_195F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_194F10066" targetNode="P_195F10066"/>
	<edges id="P_196F10066P_197F10066" xsi:type="cbl:FallThroughEdge" sourceNode="P_196F10066" targetNode="P_197F10066"/>
	<edges xsi:type="cbl:DataEdge" id="S_129F10066_POS1" deadCode="false" targetNode="P_30F10066" sourceNode="DB2_ACC_COMM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_129F10066"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_141F10066_POS1" deadCode="false" sourceNode="P_32F10066" targetNode="DB2_ACC_COMM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_141F10066"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_148F10066_POS1" deadCode="false" sourceNode="P_34F10066" targetNode="DB2_ACC_COMM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_148F10066"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_151F10066_POS1" deadCode="false" sourceNode="P_36F10066" targetNode="DB2_ACC_COMM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_151F10066"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_223F10066_POS1" deadCode="false" targetNode="P_164F10066" sourceNode="DB2_ACC_COMM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_223F10066"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_225F10066_POS1" deadCode="false" targetNode="P_166F10066" sourceNode="DB2_ACC_COMM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_225F10066"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_239F10066_POS1" deadCode="false" targetNode="P_66F10066" sourceNode="DB2_ACC_COMM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_239F10066"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_241F10066_POS1" deadCode="false" targetNode="P_66F10066" sourceNode="DB2_ACC_COMM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_241F10066"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_245F10066_POS1" deadCode="false" targetNode="P_68F10066" sourceNode="DB2_ACC_COMM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_245F10066"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_247F10066_POS1" deadCode="false" targetNode="P_68F10066" sourceNode="DB2_ACC_COMM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_247F10066"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_254F10066_POS1" deadCode="false" targetNode="P_168F10066" sourceNode="DB2_ACC_COMM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_254F10066"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_256F10066_POS1" deadCode="false" targetNode="P_170F10066" sourceNode="DB2_ACC_COMM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_256F10066"></representations>
	</edges>
</Package>

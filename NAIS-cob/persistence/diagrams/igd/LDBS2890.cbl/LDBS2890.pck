<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS2890" cbl:id="LDBS2890" xsi:id="LDBS2890" packageRef="LDBS2890.igd#LDBS2890" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS2890_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10188" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS2890.cbl.cobModel#SC_1F10188"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10188" deadCode="false" name="PROGRAM_LDBS2890_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_1F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10188" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_2F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10188" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_3F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10188" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_12F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10188" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_13F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10188" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_4F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10188" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_5F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10188" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_6F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10188" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_7F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10188" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_8F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10188" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_9F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10188" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_44F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10188" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_49F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10188" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_14F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10188" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_15F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10188" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_16F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10188" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_17F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10188" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_18F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10188" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_19F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10188" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_20F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10188" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_21F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10188" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_22F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10188" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_23F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10188" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_56F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10188" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_57F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10188" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_24F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10188" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_25F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10188" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_26F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10188" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_27F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10188" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_28F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10188" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_29F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10188" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_30F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10188" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_31F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10188" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_32F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10188" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_33F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10188" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_58F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10188" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_59F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10188" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_34F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10188" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_35F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10188" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_36F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10188" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_37F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10188" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_38F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10188" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_39F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10188" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_40F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10188" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_41F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10188" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_42F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10188" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_43F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10188" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_50F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10188" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_51F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10188" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_60F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10188" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_61F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10188" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_52F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10188" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_53F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10188" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_45F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10188" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_46F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10188" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_47F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10188" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_48F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10188" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_54F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10188" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_55F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10188" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_10F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10188" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_11F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10188" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_62F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10188" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_63F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10188" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_64F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10188" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_65F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10188" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_66F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10188" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_67F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10188" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_68F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10188" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_69F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10188" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_70F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10188" deadCode="true" name="Z800-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_75F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10188" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_71F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10188" deadCode="true" name="Z810-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_72F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10188" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_73F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10188" deadCode="true" name="Z820-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_74F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10188" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_76F10188"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10188" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS2890.cbl.cobModel#P_77F10188"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_TRCH_DI_GAR" name="TRCH_DI_GAR">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_TRCH_DI_GAR"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_STAT_OGG_BUS" name="STAT_OGG_BUS">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_STAT_OGG_BUS"/>
		</children>
	</packageNode>
	<edges id="SC_1F10188P_1F10188" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10188" targetNode="P_1F10188"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10188_I" deadCode="false" sourceNode="P_1F10188" targetNode="P_2F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_1F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10188_O" deadCode="false" sourceNode="P_1F10188" targetNode="P_3F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_1F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10188_I" deadCode="false" sourceNode="P_1F10188" targetNode="P_4F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_5F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10188_O" deadCode="false" sourceNode="P_1F10188" targetNode="P_5F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_5F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10188_I" deadCode="false" sourceNode="P_1F10188" targetNode="P_6F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_9F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10188_O" deadCode="false" sourceNode="P_1F10188" targetNode="P_7F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_9F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10188_I" deadCode="false" sourceNode="P_1F10188" targetNode="P_8F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_13F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10188_O" deadCode="false" sourceNode="P_1F10188" targetNode="P_9F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_13F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10188_I" deadCode="false" sourceNode="P_2F10188" targetNode="P_10F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_22F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10188_O" deadCode="false" sourceNode="P_2F10188" targetNode="P_11F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_22F10188"/>
	</edges>
	<edges id="P_2F10188P_3F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10188" targetNode="P_3F10188"/>
	<edges id="P_12F10188P_13F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10188" targetNode="P_13F10188"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10188_I" deadCode="false" sourceNode="P_4F10188" targetNode="P_14F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_35F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10188_O" deadCode="false" sourceNode="P_4F10188" targetNode="P_15F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_35F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10188_I" deadCode="false" sourceNode="P_4F10188" targetNode="P_16F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_36F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10188_O" deadCode="false" sourceNode="P_4F10188" targetNode="P_17F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_36F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10188_I" deadCode="false" sourceNode="P_4F10188" targetNode="P_18F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_37F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10188_O" deadCode="false" sourceNode="P_4F10188" targetNode="P_19F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_37F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10188_I" deadCode="false" sourceNode="P_4F10188" targetNode="P_20F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_38F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10188_O" deadCode="false" sourceNode="P_4F10188" targetNode="P_21F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_38F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10188_I" deadCode="false" sourceNode="P_4F10188" targetNode="P_22F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_39F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10188_O" deadCode="false" sourceNode="P_4F10188" targetNode="P_23F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_39F10188"/>
	</edges>
	<edges id="P_4F10188P_5F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10188" targetNode="P_5F10188"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10188_I" deadCode="false" sourceNode="P_6F10188" targetNode="P_24F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_43F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10188_O" deadCode="false" sourceNode="P_6F10188" targetNode="P_25F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_43F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10188_I" deadCode="false" sourceNode="P_6F10188" targetNode="P_26F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_44F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10188_O" deadCode="false" sourceNode="P_6F10188" targetNode="P_27F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_44F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10188_I" deadCode="false" sourceNode="P_6F10188" targetNode="P_28F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_45F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10188_O" deadCode="false" sourceNode="P_6F10188" targetNode="P_29F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_45F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10188_I" deadCode="false" sourceNode="P_6F10188" targetNode="P_30F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_46F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10188_O" deadCode="false" sourceNode="P_6F10188" targetNode="P_31F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_46F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10188_I" deadCode="false" sourceNode="P_6F10188" targetNode="P_32F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_47F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10188_O" deadCode="false" sourceNode="P_6F10188" targetNode="P_33F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_47F10188"/>
	</edges>
	<edges id="P_6F10188P_7F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10188" targetNode="P_7F10188"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10188_I" deadCode="false" sourceNode="P_8F10188" targetNode="P_34F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_51F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10188_O" deadCode="false" sourceNode="P_8F10188" targetNode="P_35F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_51F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10188_I" deadCode="false" sourceNode="P_8F10188" targetNode="P_36F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_52F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10188_O" deadCode="false" sourceNode="P_8F10188" targetNode="P_37F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_52F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10188_I" deadCode="false" sourceNode="P_8F10188" targetNode="P_38F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_53F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10188_O" deadCode="false" sourceNode="P_8F10188" targetNode="P_39F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_53F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10188_I" deadCode="false" sourceNode="P_8F10188" targetNode="P_40F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_54F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10188_O" deadCode="false" sourceNode="P_8F10188" targetNode="P_41F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_54F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10188_I" deadCode="false" sourceNode="P_8F10188" targetNode="P_42F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_55F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10188_O" deadCode="false" sourceNode="P_8F10188" targetNode="P_43F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_55F10188"/>
	</edges>
	<edges id="P_8F10188P_9F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10188" targetNode="P_9F10188"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10188_I" deadCode="false" sourceNode="P_44F10188" targetNode="P_45F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_58F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10188_O" deadCode="false" sourceNode="P_44F10188" targetNode="P_46F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_58F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10188_I" deadCode="false" sourceNode="P_44F10188" targetNode="P_47F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_59F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10188_O" deadCode="false" sourceNode="P_44F10188" targetNode="P_48F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_59F10188"/>
	</edges>
	<edges id="P_44F10188P_49F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10188" targetNode="P_49F10188"/>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10188_I" deadCode="false" sourceNode="P_14F10188" targetNode="P_45F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_62F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10188_O" deadCode="false" sourceNode="P_14F10188" targetNode="P_46F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_62F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10188_I" deadCode="false" sourceNode="P_14F10188" targetNode="P_47F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_63F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10188_O" deadCode="false" sourceNode="P_14F10188" targetNode="P_48F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_63F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10188_I" deadCode="false" sourceNode="P_14F10188" targetNode="P_12F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_65F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10188_O" deadCode="false" sourceNode="P_14F10188" targetNode="P_13F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_65F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10188_I" deadCode="false" sourceNode="P_14F10188" targetNode="P_50F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_67F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10188_O" deadCode="false" sourceNode="P_14F10188" targetNode="P_51F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_67F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10188_I" deadCode="false" sourceNode="P_14F10188" targetNode="P_52F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_68F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10188_O" deadCode="false" sourceNode="P_14F10188" targetNode="P_53F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_68F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10188_I" deadCode="false" sourceNode="P_14F10188" targetNode="P_54F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_69F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10188_O" deadCode="false" sourceNode="P_14F10188" targetNode="P_55F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_69F10188"/>
	</edges>
	<edges id="P_14F10188P_15F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10188" targetNode="P_15F10188"/>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10188_I" deadCode="false" sourceNode="P_16F10188" targetNode="P_44F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_71F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10188_O" deadCode="false" sourceNode="P_16F10188" targetNode="P_49F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_71F10188"/>
	</edges>
	<edges id="P_16F10188P_17F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10188" targetNode="P_17F10188"/>
	<edges id="P_18F10188P_19F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10188" targetNode="P_19F10188"/>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10188_I" deadCode="false" sourceNode="P_20F10188" targetNode="P_16F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_76F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10188_O" deadCode="false" sourceNode="P_20F10188" targetNode="P_17F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_76F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10188_I" deadCode="false" sourceNode="P_20F10188" targetNode="P_22F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_78F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10188_O" deadCode="false" sourceNode="P_20F10188" targetNode="P_23F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_78F10188"/>
	</edges>
	<edges id="P_20F10188P_21F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10188" targetNode="P_21F10188"/>
	<edges id="P_22F10188P_23F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10188" targetNode="P_23F10188"/>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10188_I" deadCode="false" sourceNode="P_56F10188" targetNode="P_45F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_82F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10188_O" deadCode="false" sourceNode="P_56F10188" targetNode="P_46F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_82F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10188_I" deadCode="false" sourceNode="P_56F10188" targetNode="P_47F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_83F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10188_O" deadCode="false" sourceNode="P_56F10188" targetNode="P_48F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_83F10188"/>
	</edges>
	<edges id="P_56F10188P_57F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10188" targetNode="P_57F10188"/>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10188_I" deadCode="false" sourceNode="P_24F10188" targetNode="P_45F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_86F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10188_O" deadCode="false" sourceNode="P_24F10188" targetNode="P_46F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_86F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10188_I" deadCode="false" sourceNode="P_24F10188" targetNode="P_47F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_87F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10188_O" deadCode="false" sourceNode="P_24F10188" targetNode="P_48F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_87F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10188_I" deadCode="false" sourceNode="P_24F10188" targetNode="P_12F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_89F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10188_O" deadCode="false" sourceNode="P_24F10188" targetNode="P_13F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_89F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10188_I" deadCode="false" sourceNode="P_24F10188" targetNode="P_50F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_91F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10188_O" deadCode="false" sourceNode="P_24F10188" targetNode="P_51F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_91F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10188_I" deadCode="false" sourceNode="P_24F10188" targetNode="P_52F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_92F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10188_O" deadCode="false" sourceNode="P_24F10188" targetNode="P_53F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_92F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10188_I" deadCode="false" sourceNode="P_24F10188" targetNode="P_54F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_93F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10188_O" deadCode="false" sourceNode="P_24F10188" targetNode="P_55F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_93F10188"/>
	</edges>
	<edges id="P_24F10188P_25F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10188" targetNode="P_25F10188"/>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10188_I" deadCode="false" sourceNode="P_26F10188" targetNode="P_56F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_95F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10188_O" deadCode="false" sourceNode="P_26F10188" targetNode="P_57F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_95F10188"/>
	</edges>
	<edges id="P_26F10188P_27F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10188" targetNode="P_27F10188"/>
	<edges id="P_28F10188P_29F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10188" targetNode="P_29F10188"/>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10188_I" deadCode="false" sourceNode="P_30F10188" targetNode="P_26F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_100F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10188_O" deadCode="false" sourceNode="P_30F10188" targetNode="P_27F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_100F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10188_I" deadCode="false" sourceNode="P_30F10188" targetNode="P_32F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_102F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10188_O" deadCode="false" sourceNode="P_30F10188" targetNode="P_33F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_102F10188"/>
	</edges>
	<edges id="P_30F10188P_31F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10188" targetNode="P_31F10188"/>
	<edges id="P_32F10188P_33F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10188" targetNode="P_33F10188"/>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10188_I" deadCode="false" sourceNode="P_58F10188" targetNode="P_45F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_106F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10188_O" deadCode="false" sourceNode="P_58F10188" targetNode="P_46F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_106F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10188_I" deadCode="false" sourceNode="P_58F10188" targetNode="P_47F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_107F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10188_O" deadCode="false" sourceNode="P_58F10188" targetNode="P_48F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_107F10188"/>
	</edges>
	<edges id="P_58F10188P_59F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10188" targetNode="P_59F10188"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10188_I" deadCode="false" sourceNode="P_34F10188" targetNode="P_45F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_110F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10188_O" deadCode="false" sourceNode="P_34F10188" targetNode="P_46F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_110F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10188_I" deadCode="false" sourceNode="P_34F10188" targetNode="P_47F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_111F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10188_O" deadCode="false" sourceNode="P_34F10188" targetNode="P_48F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_111F10188"/>
	</edges>
	<edges id="P_34F10188P_35F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10188" targetNode="P_35F10188"/>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10188_I" deadCode="false" sourceNode="P_36F10188" targetNode="P_58F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_114F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10188_O" deadCode="false" sourceNode="P_36F10188" targetNode="P_59F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_114F10188"/>
	</edges>
	<edges id="P_36F10188P_37F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10188" targetNode="P_37F10188"/>
	<edges id="P_38F10188P_39F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10188" targetNode="P_39F10188"/>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10188_I" deadCode="false" sourceNode="P_40F10188" targetNode="P_36F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_119F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10188_O" deadCode="false" sourceNode="P_40F10188" targetNode="P_37F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_119F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10188_I" deadCode="false" sourceNode="P_40F10188" targetNode="P_42F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_121F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10188_O" deadCode="false" sourceNode="P_40F10188" targetNode="P_43F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_121F10188"/>
	</edges>
	<edges id="P_40F10188P_41F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10188" targetNode="P_41F10188"/>
	<edges id="P_42F10188P_43F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10188" targetNode="P_43F10188"/>
	<edges id="P_50F10188P_51F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10188" targetNode="P_51F10188"/>
	<edges id="P_52F10188P_53F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10188" targetNode="P_53F10188"/>
	<edges id="P_45F10188P_46F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10188" targetNode="P_46F10188"/>
	<edges id="P_47F10188P_48F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10188" targetNode="P_48F10188"/>
	<edges id="P_54F10188P_55F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10188" targetNode="P_55F10188"/>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10188_I" deadCode="false" sourceNode="P_10F10188" targetNode="P_62F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_138F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10188_O" deadCode="false" sourceNode="P_10F10188" targetNode="P_63F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_138F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10188_I" deadCode="false" sourceNode="P_10F10188" targetNode="P_64F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_140F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10188_O" deadCode="false" sourceNode="P_10F10188" targetNode="P_65F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_140F10188"/>
	</edges>
	<edges id="P_10F10188P_11F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10188" targetNode="P_11F10188"/>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10188_I" deadCode="false" sourceNode="P_62F10188" targetNode="P_66F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_145F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10188_O" deadCode="false" sourceNode="P_62F10188" targetNode="P_67F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_145F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10188_I" deadCode="false" sourceNode="P_62F10188" targetNode="P_66F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_150F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10188_O" deadCode="false" sourceNode="P_62F10188" targetNode="P_67F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_150F10188"/>
	</edges>
	<edges id="P_62F10188P_63F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10188" targetNode="P_63F10188"/>
	<edges id="P_64F10188P_65F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10188" targetNode="P_65F10188"/>
	<edges id="P_66F10188P_67F10188" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10188" targetNode="P_67F10188"/>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10188_I" deadCode="true" sourceNode="P_70F10188" targetNode="P_71F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_179F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10188_O" deadCode="true" sourceNode="P_70F10188" targetNode="P_72F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_179F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_180F10188_I" deadCode="true" sourceNode="P_70F10188" targetNode="P_73F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_180F10188"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_180F10188_O" deadCode="true" sourceNode="P_70F10188" targetNode="P_74F10188">
		<representations href="../../../cobol/LDBS2890.cbl.cobModel#S_180F10188"/>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_64F10188_POS1" deadCode="false" targetNode="P_14F10188" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_64F10188"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_64F10188_POS2" deadCode="false" targetNode="P_14F10188" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_64F10188"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_88F10188_POS1" deadCode="false" targetNode="P_24F10188" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_88F10188"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_88F10188_POS2" deadCode="false" targetNode="P_24F10188" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_88F10188"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS2720" cbl:id="LDBS2720" xsi:id="LDBS2720" packageRef="LDBS2720.igd#LDBS2720" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS2720_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10182" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS2720.cbl.cobModel#SC_1F10182"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10182" deadCode="false" name="PROGRAM_LDBS2720_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_1F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10182" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_2F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10182" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_3F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10182" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_8F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10182" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_9F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10182" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_4F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10182" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_5F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10182" deadCode="false" name="C210-DELETE-WC-NST">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_10F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10182" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_11F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10182" deadCode="true" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_12F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10182" deadCode="true" name="Z100-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_13F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10182" deadCode="true" name="Z150-VALORIZZA-DATA-SERVICES">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_14F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10182" deadCode="true" name="Z150-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_15F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10182" deadCode="true" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_16F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10182" deadCode="true" name="Z200-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_17F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10182" deadCode="true" name="Z400-SEQ-RIGA">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_18F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10182" deadCode="true" name="Z400-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_19F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10182" deadCode="true" name="Z500-AGGIORNAMENTO-STORICO">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_20F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10182" deadCode="true" name="Z500-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_21F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10182" deadCode="true" name="Z600-INSERT-NUOVA-RIGA-STORICA">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_22F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10182" deadCode="true" name="Z600-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_23F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10182" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_24F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10182" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_25F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10182" deadCode="true" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_26F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10182" deadCode="true" name="Z950-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_27F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10182" deadCode="true" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_28F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10182" deadCode="true" name="Z960-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_29F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10182" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_6F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10182" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_7F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10182" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_30F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10182" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_31F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10182" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_32F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10182" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_33F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10182" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_34F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10182" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_35F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10182" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_36F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10182" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_37F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10182" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_38F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10182" deadCode="true" name="Z800-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_43F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10182" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_39F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10182" deadCode="true" name="Z810-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_40F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10182" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_41F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10182" deadCode="true" name="Z820-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_42F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10182" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_44F10182"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10182" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS2720.cbl.cobModel#P_45F10182"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_BILA_VAR_CALC_T" name="BILA_VAR_CALC_T">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_BILA_VAR_CALC_T"/>
		</children>
	</packageNode>
	<edges id="SC_1F10182P_1F10182" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10182" targetNode="P_1F10182"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10182_I" deadCode="false" sourceNode="P_1F10182" targetNode="P_2F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_1F10182"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10182_O" deadCode="false" sourceNode="P_1F10182" targetNode="P_3F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_1F10182"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10182_I" deadCode="false" sourceNode="P_1F10182" targetNode="P_4F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_5F10182"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10182_O" deadCode="false" sourceNode="P_1F10182" targetNode="P_5F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_5F10182"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10182_I" deadCode="false" sourceNode="P_2F10182" targetNode="P_6F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_14F10182"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10182_O" deadCode="false" sourceNode="P_2F10182" targetNode="P_7F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_14F10182"/>
	</edges>
	<edges id="P_2F10182P_3F10182" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10182" targetNode="P_3F10182"/>
	<edges id="P_8F10182P_9F10182" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10182" targetNode="P_9F10182"/>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10182_I" deadCode="false" sourceNode="P_4F10182" targetNode="P_10F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_27F10182"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10182_O" deadCode="false" sourceNode="P_4F10182" targetNode="P_11F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_27F10182"/>
	</edges>
	<edges id="P_4F10182P_5F10182" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10182" targetNode="P_5F10182"/>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10182_I" deadCode="false" sourceNode="P_10F10182" targetNode="P_8F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_31F10182"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10182_O" deadCode="false" sourceNode="P_10F10182" targetNode="P_9F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_31F10182"/>
	</edges>
	<edges id="P_10F10182P_11F10182" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10182" targetNode="P_11F10182"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10182_I" deadCode="false" sourceNode="P_6F10182" targetNode="P_30F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_51F10182"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10182_O" deadCode="false" sourceNode="P_6F10182" targetNode="P_31F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_51F10182"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10182_I" deadCode="false" sourceNode="P_6F10182" targetNode="P_32F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_53F10182"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10182_O" deadCode="false" sourceNode="P_6F10182" targetNode="P_33F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_53F10182"/>
	</edges>
	<edges id="P_6F10182P_7F10182" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10182" targetNode="P_7F10182"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10182_I" deadCode="true" sourceNode="P_30F10182" targetNode="P_34F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_58F10182"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10182_O" deadCode="true" sourceNode="P_30F10182" targetNode="P_35F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_58F10182"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10182_I" deadCode="true" sourceNode="P_30F10182" targetNode="P_34F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_63F10182"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10182_O" deadCode="true" sourceNode="P_30F10182" targetNode="P_35F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_63F10182"/>
	</edges>
	<edges id="P_30F10182P_31F10182" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10182" targetNode="P_31F10182"/>
	<edges id="P_32F10182P_33F10182" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10182" targetNode="P_33F10182"/>
	<edges id="P_34F10182P_35F10182" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10182" targetNode="P_35F10182"/>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10182_I" deadCode="true" sourceNode="P_38F10182" targetNode="P_39F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_92F10182"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10182_O" deadCode="true" sourceNode="P_38F10182" targetNode="P_40F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_92F10182"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10182_I" deadCode="true" sourceNode="P_38F10182" targetNode="P_41F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_93F10182"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10182_O" deadCode="true" sourceNode="P_38F10182" targetNode="P_42F10182">
		<representations href="../../../cobol/LDBS2720.cbl.cobModel#S_93F10182"/>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_30F10182_POS1" deadCode="false" sourceNode="P_10F10182" targetNode="DB2_BILA_VAR_CALC_T">
		<representations href="../../../cobol/../importantStmts.cobModel#S_30F10182"></representations>
	</edges>
</Package>

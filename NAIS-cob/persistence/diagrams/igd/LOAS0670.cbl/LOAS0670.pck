<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LOAS0670" cbl:id="LOAS0670" xsi:id="LOAS0670" packageRef="LOAS0670.igd#LOAS0670" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LOAS0670_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10292" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LOAS0670.cbl.cobModel#SC_1F10292"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10292" deadCode="false" name="PROGRAM_LOAS0670_FIRST_SENTENCES">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_1F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10292" deadCode="false" name="S00000-OPERAZ-INIZIALI">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_2F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10292" deadCode="false" name="S00000-OPERAZ-INIZIALI-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_3F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10292" deadCode="false" name="S00200-INIZIA-AREE-WS">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_8F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10292" deadCode="false" name="S00200-INIZIA-AREE-WS-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_9F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10292" deadCode="false" name="S10000-ELABORAZIONE">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_4F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10292" deadCode="false" name="S10000-ELABORAZIONE-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_5F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10292" deadCode="false" name="S10600-DETERMINA-TP-RIVAL">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_14F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10292" deadCode="false" name="S10600-DETERMINA-TP-RIVAL-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_15F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10292" deadCode="false" name="S10220-RICERCA-TITOLO">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_18F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10292" deadCode="false" name="S10220-RICERCA-TITOLO-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_27F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10292" deadCode="false" name="RICERCA-GAR">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_23F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10292" deadCode="false" name="RICERCA-GAR-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_24F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10292" deadCode="false" name="S10230-IMPOSTA-TIT-CONT">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_19F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10292" deadCode="false" name="S10230-IMPOSTA-TIT-CONT-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_20F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10292" deadCode="false" name="S10240-LEGGI-TIT-CONT">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_21F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10292" deadCode="false" name="S10240-LEGGI-TIT-CONT-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_22F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10292" deadCode="false" name="S10250-CARICA-TAB-W670">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_30F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10292" deadCode="false" name="S10250-CARICA-TAB-W670-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_31F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10292" deadCode="false" name="S10290-GESTIONE-TIT-CONT">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_16F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10292" deadCode="false" name="S10290-GESTIONE-TIT-CONT-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_17F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10292" deadCode="false" name="S10300-RIV-PIENA-PARZ-NULLA">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_32F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10292" deadCode="false" name="S10300-RIV-PIENA-PARZ-NULLA-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_33F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10292" deadCode="false" name="S10310-LEGGE-GG-RIT-PAG">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_34F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10292" deadCode="false" name="S10310-LEGGE-GG-RIT-PAG-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_35F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10292" deadCode="false" name="S10400-CARICA-TIT-DB">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_10F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10292" deadCode="false" name="S10400-CARICA-TIT-DB-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_11F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10292" deadCode="false" name="S10500-CTRL-TIT">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_12F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10292" deadCode="false" name="S10500-CTRL-TIT-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_13F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10292" deadCode="false" name="S10510-CTRL-STATO-TIT">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_38F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10292" deadCode="false" name="S10510-CTRL-STATO-TIT-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_39F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10292" deadCode="false" name="S10520-CTRL-UNI-TIT">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_40F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10292" deadCode="false" name="S10520-CTRL-UNI-TIT-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_41F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10292" deadCode="true" name="S10850-CHIAMA-LCCS0003">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_42F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10292" deadCode="true" name="S10850-CHIAMA-LCCS0003-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_45F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10292" deadCode="false" name="S90000-OPERAZ-FINALI">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_6F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10292" deadCode="false" name="S90000-OPERAZ-FINALI-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_7F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10292" deadCode="false" name="GESTIONE-ERR-STD">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_25F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10292" deadCode="false" name="GESTIONE-ERR-STD-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_26F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10292" deadCode="true" name="GESTIONE-ERR-SIST">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_43F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10292" deadCode="true" name="GESTIONE-ERR-SIST-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_44F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10292" deadCode="false" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_28F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10292" deadCode="false" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_29F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10292" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_36F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10292" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_37F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10292" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_50F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10292" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_51F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10292" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_48F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10292" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_49F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10292" deadCode="true" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_46F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10292" deadCode="true" name="EX-S0290">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_47F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10292" deadCode="true" name="S0320-OUTPUT-PRODOTTO">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_52F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10292" deadCode="true" name="EX-S0320-OUTPUT-PRODOTTO">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_57F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10292" deadCode="true" name="S0322-SALVA-ERRORE">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_53F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10292" deadCode="true" name="EX-S0322">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_54F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10292" deadCode="true" name="S0321-ERRORI-DEROGA">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_55F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10292" deadCode="true" name="EX-S0321">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_56F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10292" deadCode="true" name="S0323-SALVA-DEROGA">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_58F10292"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10292" deadCode="true" name="EX-S0323">
				<representations href="../../../cobol/LOAS0670.cbl.cobModel#P_59F10292"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LOAS0670_LCCS0003" name="Dynamic LOAS0670 LCCS0003" missing="true">
			<representations href="../../../../missing.xmi#IDYPRV2TEADSN3DITMKD4CGGNWZNY0WX5RT4SHUEDLHDMHR3XQ3BBF"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0010" name="IDSS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10099"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10292P_1F10292" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10292" targetNode="P_1F10292"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10292_I" deadCode="false" sourceNode="P_1F10292" targetNode="P_2F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_1F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10292_O" deadCode="false" sourceNode="P_1F10292" targetNode="P_3F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_1F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10292_I" deadCode="false" sourceNode="P_1F10292" targetNode="P_4F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_3F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10292_O" deadCode="false" sourceNode="P_1F10292" targetNode="P_5F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_3F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10292_I" deadCode="false" sourceNode="P_1F10292" targetNode="P_6F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_4F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10292_O" deadCode="false" sourceNode="P_1F10292" targetNode="P_7F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_4F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10292_I" deadCode="false" sourceNode="P_2F10292" targetNode="P_8F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_7F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10292_O" deadCode="false" sourceNode="P_2F10292" targetNode="P_9F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_7F10292"/>
	</edges>
	<edges id="P_2F10292P_3F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10292" targetNode="P_3F10292"/>
	<edges id="P_8F10292P_9F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10292" targetNode="P_9F10292"/>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10292_I" deadCode="false" sourceNode="P_4F10292" targetNode="P_10F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_16F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10292_O" deadCode="false" sourceNode="P_4F10292" targetNode="P_11F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_16F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10292_I" deadCode="false" sourceNode="P_4F10292" targetNode="P_12F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_18F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10292_O" deadCode="false" sourceNode="P_4F10292" targetNode="P_13F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_18F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10292_I" deadCode="false" sourceNode="P_4F10292" targetNode="P_14F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_20F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10292_O" deadCode="false" sourceNode="P_4F10292" targetNode="P_15F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_20F10292"/>
	</edges>
	<edges id="P_4F10292P_5F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10292" targetNode="P_5F10292"/>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10292_I" deadCode="false" sourceNode="P_14F10292" targetNode="P_16F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_23F10292"/>
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_24F10292"/>
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_26F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10292_O" deadCode="false" sourceNode="P_14F10292" targetNode="P_17F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_23F10292"/>
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_24F10292"/>
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_26F10292"/>
	</edges>
	<edges id="P_14F10292P_15F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10292" targetNode="P_15F10292"/>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10292_I" deadCode="false" sourceNode="P_18F10292" targetNode="P_19F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_33F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10292_O" deadCode="false" sourceNode="P_18F10292" targetNode="P_20F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_33F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10292_I" deadCode="false" sourceNode="P_18F10292" targetNode="P_21F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_34F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10292_O" deadCode="false" sourceNode="P_18F10292" targetNode="P_22F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_34F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10292_I" deadCode="false" sourceNode="P_18F10292" targetNode="P_19F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_40F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10292_O" deadCode="false" sourceNode="P_18F10292" targetNode="P_20F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_40F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10292_I" deadCode="false" sourceNode="P_18F10292" targetNode="P_21F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_41F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10292_O" deadCode="false" sourceNode="P_18F10292" targetNode="P_22F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_41F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10292_I" deadCode="false" sourceNode="P_18F10292" targetNode="P_23F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_44F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10292_O" deadCode="false" sourceNode="P_18F10292" targetNode="P_24F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_44F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10292_I" deadCode="false" sourceNode="P_18F10292" targetNode="P_25F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_49F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10292_O" deadCode="false" sourceNode="P_18F10292" targetNode="P_26F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_49F10292"/>
	</edges>
	<edges id="P_18F10292P_27F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10292" targetNode="P_27F10292"/>
	<edges id="P_23F10292P_24F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_23F10292" targetNode="P_24F10292"/>
	<edges id="P_19F10292P_20F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_19F10292" targetNode="P_20F10292"/>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10292_I" deadCode="false" sourceNode="P_21F10292" targetNode="P_28F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_77F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10292_O" deadCode="false" sourceNode="P_21F10292" targetNode="P_29F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_77F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10292_I" deadCode="false" sourceNode="P_21F10292" targetNode="P_25F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_89F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10292_O" deadCode="false" sourceNode="P_21F10292" targetNode="P_26F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_89F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10292_I" deadCode="false" sourceNode="P_21F10292" targetNode="P_30F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_91F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10292_O" deadCode="false" sourceNode="P_21F10292" targetNode="P_31F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_91F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10292_I" deadCode="false" sourceNode="P_21F10292" targetNode="P_25F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_94F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10292_O" deadCode="false" sourceNode="P_21F10292" targetNode="P_26F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_94F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10292_I" deadCode="false" sourceNode="P_21F10292" targetNode="P_25F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_97F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10292_O" deadCode="false" sourceNode="P_21F10292" targetNode="P_26F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_97F10292"/>
	</edges>
	<edges id="P_21F10292P_22F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_21F10292" targetNode="P_22F10292"/>
	<edges id="P_30F10292P_31F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10292" targetNode="P_31F10292"/>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10292_I" deadCode="false" sourceNode="P_16F10292" targetNode="P_25F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_113F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10292_O" deadCode="false" sourceNode="P_16F10292" targetNode="P_26F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_113F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10292_I" deadCode="false" sourceNode="P_16F10292" targetNode="P_32F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_114F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10292_O" deadCode="false" sourceNode="P_16F10292" targetNode="P_33F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_114F10292"/>
	</edges>
	<edges id="P_16F10292P_17F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10292" targetNode="P_17F10292"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10292_I" deadCode="false" sourceNode="P_32F10292" targetNode="P_34F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_117F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10292_O" deadCode="false" sourceNode="P_32F10292" targetNode="P_35F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_117F10292"/>
	</edges>
	<edges id="P_32F10292P_33F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10292" targetNode="P_33F10292"/>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10292_I" deadCode="false" sourceNode="P_34F10292" targetNode="P_28F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_139F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10292_O" deadCode="false" sourceNode="P_34F10292" targetNode="P_29F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_139F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10292_I" deadCode="false" sourceNode="P_34F10292" targetNode="P_36F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_148F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10292_O" deadCode="false" sourceNode="P_34F10292" targetNode="P_37F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_148F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_155F10292_I" deadCode="false" sourceNode="P_34F10292" targetNode="P_36F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_155F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_155F10292_O" deadCode="false" sourceNode="P_34F10292" targetNode="P_37F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_155F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10292_I" deadCode="false" sourceNode="P_34F10292" targetNode="P_36F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_161F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10292_O" deadCode="false" sourceNode="P_34F10292" targetNode="P_37F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_161F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10292_I" deadCode="false" sourceNode="P_34F10292" targetNode="P_36F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_167F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10292_O" deadCode="false" sourceNode="P_34F10292" targetNode="P_37F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_167F10292"/>
	</edges>
	<edges id="P_34F10292P_35F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10292" targetNode="P_35F10292"/>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10292_I" deadCode="false" sourceNode="P_10F10292" targetNode="P_18F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_170F10292"/>
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_171F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10292_O" deadCode="false" sourceNode="P_10F10292" targetNode="P_27F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_170F10292"/>
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_171F10292"/>
	</edges>
	<edges id="P_10F10292P_11F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10292" targetNode="P_11F10292"/>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10292_I" deadCode="false" sourceNode="P_12F10292" targetNode="P_38F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_174F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10292_O" deadCode="false" sourceNode="P_12F10292" targetNode="P_39F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_174F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_176F10292_I" deadCode="false" sourceNode="P_12F10292" targetNode="P_40F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_176F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_176F10292_O" deadCode="false" sourceNode="P_12F10292" targetNode="P_41F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_176F10292"/>
	</edges>
	<edges id="P_12F10292P_13F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10292" targetNode="P_13F10292"/>
	<edges xsi:type="cbl:PerformEdge" id="S_183F10292_I" deadCode="false" sourceNode="P_38F10292" targetNode="P_25F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_179F10292"/>
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_183F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_183F10292_O" deadCode="false" sourceNode="P_38F10292" targetNode="P_26F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_179F10292"/>
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_183F10292"/>
	</edges>
	<edges id="P_38F10292P_39F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10292" targetNode="P_39F10292"/>
	<edges xsi:type="cbl:PerformEdge" id="S_194F10292_I" deadCode="false" sourceNode="P_40F10292" targetNode="P_25F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_186F10292"/>
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_194F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_194F10292_O" deadCode="false" sourceNode="P_40F10292" targetNode="P_26F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_186F10292"/>
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_194F10292"/>
	</edges>
	<edges id="P_40F10292P_41F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10292" targetNode="P_41F10292"/>
	<edges xsi:type="cbl:PerformEdge" id="S_201F10292_I" deadCode="true" sourceNode="P_42F10292" targetNode="P_43F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_201F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_201F10292_O" deadCode="true" sourceNode="P_42F10292" targetNode="P_44F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_201F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_206F10292_I" deadCode="true" sourceNode="P_42F10292" targetNode="P_25F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_206F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_206F10292_O" deadCode="true" sourceNode="P_42F10292" targetNode="P_26F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_206F10292"/>
	</edges>
	<edges id="P_6F10292P_7F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10292" targetNode="P_7F10292"/>
	<edges xsi:type="cbl:PerformEdge" id="S_215F10292_I" deadCode="false" sourceNode="P_25F10292" targetNode="P_36F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_215F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_215F10292_O" deadCode="false" sourceNode="P_25F10292" targetNode="P_37F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_215F10292"/>
	</edges>
	<edges id="P_25F10292P_26F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_25F10292" targetNode="P_26F10292"/>
	<edges xsi:type="cbl:PerformEdge" id="S_218F10292_I" deadCode="true" sourceNode="P_43F10292" targetNode="P_46F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_218F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_218F10292_O" deadCode="true" sourceNode="P_43F10292" targetNode="P_47F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_218F10292"/>
	</edges>
	<edges id="P_28F10292P_29F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10292" targetNode="P_29F10292"/>
	<edges xsi:type="cbl:PerformEdge" id="S_254F10292_I" deadCode="false" sourceNode="P_36F10292" targetNode="P_48F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_254F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_254F10292_O" deadCode="false" sourceNode="P_36F10292" targetNode="P_49F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_254F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_258F10292_I" deadCode="false" sourceNode="P_36F10292" targetNode="P_50F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_258F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_258F10292_O" deadCode="false" sourceNode="P_36F10292" targetNode="P_51F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_258F10292"/>
	</edges>
	<edges id="P_36F10292P_37F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10292" targetNode="P_37F10292"/>
	<edges id="P_50F10292P_51F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10292" targetNode="P_51F10292"/>
	<edges id="P_48F10292P_49F10292" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10292" targetNode="P_49F10292"/>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10292_I" deadCode="true" sourceNode="P_46F10292" targetNode="P_36F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_294F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10292_O" deadCode="true" sourceNode="P_46F10292" targetNode="P_37F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_294F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_310F10292_I" deadCode="true" sourceNode="P_52F10292" targetNode="P_53F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_308F10292"/>
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_310F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_310F10292_O" deadCode="true" sourceNode="P_52F10292" targetNode="P_54F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_308F10292"/>
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_310F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10292_I" deadCode="true" sourceNode="P_52F10292" targetNode="P_55F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_311F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10292_O" deadCode="true" sourceNode="P_52F10292" targetNode="P_56F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_311F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_316F10292_I" deadCode="true" sourceNode="P_53F10292" targetNode="P_36F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_316F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_316F10292_O" deadCode="true" sourceNode="P_53F10292" targetNode="P_37F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_316F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_320F10292_I" deadCode="true" sourceNode="P_55F10292" targetNode="P_53F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_320F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_320F10292_O" deadCode="true" sourceNode="P_55F10292" targetNode="P_54F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_320F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_322F10292_I" deadCode="true" sourceNode="P_55F10292" targetNode="P_53F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_322F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_322F10292_O" deadCode="true" sourceNode="P_55F10292" targetNode="P_54F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_322F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_324F10292_I" deadCode="true" sourceNode="P_55F10292" targetNode="P_53F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_324F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_324F10292_O" deadCode="true" sourceNode="P_55F10292" targetNode="P_54F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_324F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_327F10292_I" deadCode="true" sourceNode="P_55F10292" targetNode="P_53F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_327F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_327F10292_O" deadCode="true" sourceNode="P_55F10292" targetNode="P_54F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_327F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_328F10292_I" deadCode="true" sourceNode="P_55F10292" targetNode="P_58F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_328F10292"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_328F10292_O" deadCode="true" sourceNode="P_55F10292" targetNode="P_59F10292">
		<representations href="../../../cobol/LOAS0670.cbl.cobModel#S_328F10292"/>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_197F10292" deadCode="true" name="Dynamic LCCS0003" sourceNode="P_42F10292" targetNode="Dynamic_LOAS0670_LCCS0003">
		<representations href="../../../cobol/../importantStmts.cobModel#S_197F10292"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_246F10292" deadCode="false" name="Dynamic IDSI0011-PGM" sourceNode="P_28F10292" targetNode="IDSS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_246F10292"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_252F10292" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_36F10292" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_252F10292"></representations>
	</edges>
</Package>

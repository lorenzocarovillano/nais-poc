<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS1460" cbl:id="LDBS1460" xsi:id="LDBS1460" packageRef="LDBS1460.igd#LDBS1460" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS1460_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10160" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS1460.cbl.cobModel#SC_1F10160"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10160" deadCode="false" name="PROGRAM_LDBS1460_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_1F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10160" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_2F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10160" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_3F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10160" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_12F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10160" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_13F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10160" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_4F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10160" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_5F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10160" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_6F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10160" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_7F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10160" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_8F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10160" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_9F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10160" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_44F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10160" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_49F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10160" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_14F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10160" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_15F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10160" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_16F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10160" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_17F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10160" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_18F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10160" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_19F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10160" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_20F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10160" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_21F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10160" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_22F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10160" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_23F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10160" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_56F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10160" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_57F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10160" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_24F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10160" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_25F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10160" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_26F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10160" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_27F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10160" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_28F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10160" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_29F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10160" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_30F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10160" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_31F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10160" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_32F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10160" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_33F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10160" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_58F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10160" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_59F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10160" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_34F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10160" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_35F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10160" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_36F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10160" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_37F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10160" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_38F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10160" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_39F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10160" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_40F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10160" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_41F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10160" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_42F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10160" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_43F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10160" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_50F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10160" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_51F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10160" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_60F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10160" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_63F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10160" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_52F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10160" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_53F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10160" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_45F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10160" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_46F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10160" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_47F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10160" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_48F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10160" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_54F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10160" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_55F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10160" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_10F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10160" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_11F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10160" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_66F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10160" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_67F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10160" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_68F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10160" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_69F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10160" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_61F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10160" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_62F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10160" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_70F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10160" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_71F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10160" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_64F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10160" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_65F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10160" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_72F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10160" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_73F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10160" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_74F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10160" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_75F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10160" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_76F10160"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10160" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS1460.cbl.cobModel#P_77F10160"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_SOPR_DI_GAR" name="SOPR_DI_GAR">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_SOPR_DI_GAR"/>
		</children>
	</packageNode>
	<edges id="SC_1F10160P_1F10160" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10160" targetNode="P_1F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10160_I" deadCode="false" sourceNode="P_1F10160" targetNode="P_2F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_1F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10160_O" deadCode="false" sourceNode="P_1F10160" targetNode="P_3F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_1F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10160_I" deadCode="false" sourceNode="P_1F10160" targetNode="P_4F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_5F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10160_O" deadCode="false" sourceNode="P_1F10160" targetNode="P_5F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_5F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10160_I" deadCode="false" sourceNode="P_1F10160" targetNode="P_6F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_9F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10160_O" deadCode="false" sourceNode="P_1F10160" targetNode="P_7F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_9F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10160_I" deadCode="false" sourceNode="P_1F10160" targetNode="P_8F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_13F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10160_O" deadCode="false" sourceNode="P_1F10160" targetNode="P_9F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_13F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10160_I" deadCode="false" sourceNode="P_2F10160" targetNode="P_10F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_22F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10160_O" deadCode="false" sourceNode="P_2F10160" targetNode="P_11F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_22F10160"/>
	</edges>
	<edges id="P_2F10160P_3F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10160" targetNode="P_3F10160"/>
	<edges id="P_12F10160P_13F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10160" targetNode="P_13F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10160_I" deadCode="false" sourceNode="P_4F10160" targetNode="P_14F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_35F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10160_O" deadCode="false" sourceNode="P_4F10160" targetNode="P_15F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_35F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10160_I" deadCode="false" sourceNode="P_4F10160" targetNode="P_16F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_36F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10160_O" deadCode="false" sourceNode="P_4F10160" targetNode="P_17F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_36F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10160_I" deadCode="false" sourceNode="P_4F10160" targetNode="P_18F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_37F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10160_O" deadCode="false" sourceNode="P_4F10160" targetNode="P_19F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_37F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10160_I" deadCode="false" sourceNode="P_4F10160" targetNode="P_20F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_38F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10160_O" deadCode="false" sourceNode="P_4F10160" targetNode="P_21F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_38F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10160_I" deadCode="false" sourceNode="P_4F10160" targetNode="P_22F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_39F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10160_O" deadCode="false" sourceNode="P_4F10160" targetNode="P_23F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_39F10160"/>
	</edges>
	<edges id="P_4F10160P_5F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10160" targetNode="P_5F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10160_I" deadCode="false" sourceNode="P_6F10160" targetNode="P_24F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_43F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10160_O" deadCode="false" sourceNode="P_6F10160" targetNode="P_25F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_43F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10160_I" deadCode="false" sourceNode="P_6F10160" targetNode="P_26F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_44F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10160_O" deadCode="false" sourceNode="P_6F10160" targetNode="P_27F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_44F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10160_I" deadCode="false" sourceNode="P_6F10160" targetNode="P_28F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_45F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10160_O" deadCode="false" sourceNode="P_6F10160" targetNode="P_29F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_45F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10160_I" deadCode="false" sourceNode="P_6F10160" targetNode="P_30F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_46F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10160_O" deadCode="false" sourceNode="P_6F10160" targetNode="P_31F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_46F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10160_I" deadCode="false" sourceNode="P_6F10160" targetNode="P_32F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_47F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10160_O" deadCode="false" sourceNode="P_6F10160" targetNode="P_33F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_47F10160"/>
	</edges>
	<edges id="P_6F10160P_7F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10160" targetNode="P_7F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10160_I" deadCode="false" sourceNode="P_8F10160" targetNode="P_34F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_51F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10160_O" deadCode="false" sourceNode="P_8F10160" targetNode="P_35F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_51F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10160_I" deadCode="false" sourceNode="P_8F10160" targetNode="P_36F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_52F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10160_O" deadCode="false" sourceNode="P_8F10160" targetNode="P_37F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_52F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10160_I" deadCode="false" sourceNode="P_8F10160" targetNode="P_38F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_53F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10160_O" deadCode="false" sourceNode="P_8F10160" targetNode="P_39F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_53F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10160_I" deadCode="false" sourceNode="P_8F10160" targetNode="P_40F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_54F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10160_O" deadCode="false" sourceNode="P_8F10160" targetNode="P_41F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_54F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10160_I" deadCode="false" sourceNode="P_8F10160" targetNode="P_42F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_55F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10160_O" deadCode="false" sourceNode="P_8F10160" targetNode="P_43F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_55F10160"/>
	</edges>
	<edges id="P_8F10160P_9F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10160" targetNode="P_9F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10160_I" deadCode="false" sourceNode="P_44F10160" targetNode="P_45F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_58F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10160_O" deadCode="false" sourceNode="P_44F10160" targetNode="P_46F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_58F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10160_I" deadCode="false" sourceNode="P_44F10160" targetNode="P_47F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_59F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10160_O" deadCode="false" sourceNode="P_44F10160" targetNode="P_48F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_59F10160"/>
	</edges>
	<edges id="P_44F10160P_49F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10160" targetNode="P_49F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10160_I" deadCode="false" sourceNode="P_14F10160" targetNode="P_45F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_63F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10160_O" deadCode="false" sourceNode="P_14F10160" targetNode="P_46F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_63F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10160_I" deadCode="false" sourceNode="P_14F10160" targetNode="P_47F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_64F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10160_O" deadCode="false" sourceNode="P_14F10160" targetNode="P_48F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_64F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10160_I" deadCode="false" sourceNode="P_14F10160" targetNode="P_12F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_66F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10160_O" deadCode="false" sourceNode="P_14F10160" targetNode="P_13F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_66F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10160_I" deadCode="false" sourceNode="P_14F10160" targetNode="P_50F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_68F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10160_O" deadCode="false" sourceNode="P_14F10160" targetNode="P_51F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_68F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10160_I" deadCode="false" sourceNode="P_14F10160" targetNode="P_52F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_69F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10160_O" deadCode="false" sourceNode="P_14F10160" targetNode="P_53F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_69F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10160_I" deadCode="false" sourceNode="P_14F10160" targetNode="P_54F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_70F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10160_O" deadCode="false" sourceNode="P_14F10160" targetNode="P_55F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_70F10160"/>
	</edges>
	<edges id="P_14F10160P_15F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10160" targetNode="P_15F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10160_I" deadCode="false" sourceNode="P_16F10160" targetNode="P_44F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_72F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10160_O" deadCode="false" sourceNode="P_16F10160" targetNode="P_49F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_72F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10160_I" deadCode="false" sourceNode="P_16F10160" targetNode="P_12F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_74F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10160_O" deadCode="false" sourceNode="P_16F10160" targetNode="P_13F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_74F10160"/>
	</edges>
	<edges id="P_16F10160P_17F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10160" targetNode="P_17F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10160_I" deadCode="false" sourceNode="P_18F10160" targetNode="P_12F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_77F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10160_O" deadCode="false" sourceNode="P_18F10160" targetNode="P_13F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_77F10160"/>
	</edges>
	<edges id="P_18F10160P_19F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10160" targetNode="P_19F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10160_I" deadCode="false" sourceNode="P_20F10160" targetNode="P_16F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_79F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10160_O" deadCode="false" sourceNode="P_20F10160" targetNode="P_17F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_79F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10160_I" deadCode="false" sourceNode="P_20F10160" targetNode="P_22F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_81F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10160_O" deadCode="false" sourceNode="P_20F10160" targetNode="P_23F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_81F10160"/>
	</edges>
	<edges id="P_20F10160P_21F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10160" targetNode="P_21F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10160_I" deadCode="false" sourceNode="P_22F10160" targetNode="P_12F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_84F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10160_O" deadCode="false" sourceNode="P_22F10160" targetNode="P_13F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_84F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10160_I" deadCode="false" sourceNode="P_22F10160" targetNode="P_50F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_86F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10160_O" deadCode="false" sourceNode="P_22F10160" targetNode="P_51F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_86F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10160_I" deadCode="false" sourceNode="P_22F10160" targetNode="P_52F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_87F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10160_O" deadCode="false" sourceNode="P_22F10160" targetNode="P_53F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_87F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10160_I" deadCode="false" sourceNode="P_22F10160" targetNode="P_54F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_88F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10160_O" deadCode="false" sourceNode="P_22F10160" targetNode="P_55F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_88F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10160_I" deadCode="false" sourceNode="P_22F10160" targetNode="P_18F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_90F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10160_O" deadCode="false" sourceNode="P_22F10160" targetNode="P_19F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_90F10160"/>
	</edges>
	<edges id="P_22F10160P_23F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10160" targetNode="P_23F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10160_I" deadCode="false" sourceNode="P_56F10160" targetNode="P_45F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_94F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10160_O" deadCode="false" sourceNode="P_56F10160" targetNode="P_46F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_94F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10160_I" deadCode="false" sourceNode="P_56F10160" targetNode="P_47F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_95F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10160_O" deadCode="false" sourceNode="P_56F10160" targetNode="P_48F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_95F10160"/>
	</edges>
	<edges id="P_56F10160P_57F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10160" targetNode="P_57F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10160_I" deadCode="false" sourceNode="P_24F10160" targetNode="P_45F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_99F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10160_O" deadCode="false" sourceNode="P_24F10160" targetNode="P_46F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_99F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10160_I" deadCode="false" sourceNode="P_24F10160" targetNode="P_47F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_100F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10160_O" deadCode="false" sourceNode="P_24F10160" targetNode="P_48F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_100F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10160_I" deadCode="false" sourceNode="P_24F10160" targetNode="P_12F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_102F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10160_O" deadCode="false" sourceNode="P_24F10160" targetNode="P_13F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_102F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10160_I" deadCode="false" sourceNode="P_24F10160" targetNode="P_50F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_104F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10160_O" deadCode="false" sourceNode="P_24F10160" targetNode="P_51F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_104F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10160_I" deadCode="false" sourceNode="P_24F10160" targetNode="P_52F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_105F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10160_O" deadCode="false" sourceNode="P_24F10160" targetNode="P_53F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_105F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10160_I" deadCode="false" sourceNode="P_24F10160" targetNode="P_54F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_106F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10160_O" deadCode="false" sourceNode="P_24F10160" targetNode="P_55F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_106F10160"/>
	</edges>
	<edges id="P_24F10160P_25F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10160" targetNode="P_25F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10160_I" deadCode="false" sourceNode="P_26F10160" targetNode="P_56F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_108F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10160_O" deadCode="false" sourceNode="P_26F10160" targetNode="P_57F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_108F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10160_I" deadCode="false" sourceNode="P_26F10160" targetNode="P_12F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_110F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10160_O" deadCode="false" sourceNode="P_26F10160" targetNode="P_13F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_110F10160"/>
	</edges>
	<edges id="P_26F10160P_27F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10160" targetNode="P_27F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10160_I" deadCode="false" sourceNode="P_28F10160" targetNode="P_12F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_113F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10160_O" deadCode="false" sourceNode="P_28F10160" targetNode="P_13F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_113F10160"/>
	</edges>
	<edges id="P_28F10160P_29F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10160" targetNode="P_29F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10160_I" deadCode="false" sourceNode="P_30F10160" targetNode="P_26F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_115F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10160_O" deadCode="false" sourceNode="P_30F10160" targetNode="P_27F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_115F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10160_I" deadCode="false" sourceNode="P_30F10160" targetNode="P_32F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_117F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10160_O" deadCode="false" sourceNode="P_30F10160" targetNode="P_33F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_117F10160"/>
	</edges>
	<edges id="P_30F10160P_31F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10160" targetNode="P_31F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10160_I" deadCode="false" sourceNode="P_32F10160" targetNode="P_12F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_120F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10160_O" deadCode="false" sourceNode="P_32F10160" targetNode="P_13F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_120F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10160_I" deadCode="false" sourceNode="P_32F10160" targetNode="P_50F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_122F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10160_O" deadCode="false" sourceNode="P_32F10160" targetNode="P_51F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_122F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10160_I" deadCode="false" sourceNode="P_32F10160" targetNode="P_52F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_123F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10160_O" deadCode="false" sourceNode="P_32F10160" targetNode="P_53F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_123F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10160_I" deadCode="false" sourceNode="P_32F10160" targetNode="P_54F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_124F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10160_O" deadCode="false" sourceNode="P_32F10160" targetNode="P_55F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_124F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10160_I" deadCode="false" sourceNode="P_32F10160" targetNode="P_28F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_126F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10160_O" deadCode="false" sourceNode="P_32F10160" targetNode="P_29F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_126F10160"/>
	</edges>
	<edges id="P_32F10160P_33F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10160" targetNode="P_33F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10160_I" deadCode="false" sourceNode="P_58F10160" targetNode="P_45F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_130F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10160_O" deadCode="false" sourceNode="P_58F10160" targetNode="P_46F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_130F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10160_I" deadCode="false" sourceNode="P_58F10160" targetNode="P_47F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_131F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10160_O" deadCode="false" sourceNode="P_58F10160" targetNode="P_48F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_131F10160"/>
	</edges>
	<edges id="P_58F10160P_59F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10160" targetNode="P_59F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_134F10160_I" deadCode="false" sourceNode="P_34F10160" targetNode="P_45F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_134F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_134F10160_O" deadCode="false" sourceNode="P_34F10160" targetNode="P_46F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_134F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10160_I" deadCode="false" sourceNode="P_34F10160" targetNode="P_47F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_135F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10160_O" deadCode="false" sourceNode="P_34F10160" targetNode="P_48F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_135F10160"/>
	</edges>
	<edges id="P_34F10160P_35F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10160" targetNode="P_35F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10160_I" deadCode="false" sourceNode="P_36F10160" targetNode="P_58F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_138F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10160_O" deadCode="false" sourceNode="P_36F10160" targetNode="P_59F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_138F10160"/>
	</edges>
	<edges id="P_36F10160P_37F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10160" targetNode="P_37F10160"/>
	<edges id="P_38F10160P_39F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10160" targetNode="P_39F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10160_I" deadCode="false" sourceNode="P_40F10160" targetNode="P_36F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_143F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10160_O" deadCode="false" sourceNode="P_40F10160" targetNode="P_37F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_143F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10160_I" deadCode="false" sourceNode="P_40F10160" targetNode="P_42F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_145F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10160_O" deadCode="false" sourceNode="P_40F10160" targetNode="P_43F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_145F10160"/>
	</edges>
	<edges id="P_40F10160P_41F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10160" targetNode="P_41F10160"/>
	<edges id="P_42F10160P_43F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10160" targetNode="P_43F10160"/>
	<edges id="P_50F10160P_51F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10160" targetNode="P_51F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10160_I" deadCode="true" sourceNode="P_60F10160" targetNode="P_61F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_170F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10160_O" deadCode="true" sourceNode="P_60F10160" targetNode="P_62F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_170F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10160_I" deadCode="true" sourceNode="P_60F10160" targetNode="P_61F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_173F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10160_O" deadCode="true" sourceNode="P_60F10160" targetNode="P_62F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_173F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10160_I" deadCode="false" sourceNode="P_52F10160" targetNode="P_64F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_177F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10160_O" deadCode="false" sourceNode="P_52F10160" targetNode="P_65F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_177F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_180F10160_I" deadCode="false" sourceNode="P_52F10160" targetNode="P_64F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_180F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_180F10160_O" deadCode="false" sourceNode="P_52F10160" targetNode="P_65F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_180F10160"/>
	</edges>
	<edges id="P_52F10160P_53F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10160" targetNode="P_53F10160"/>
	<edges id="P_45F10160P_46F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10160" targetNode="P_46F10160"/>
	<edges id="P_47F10160P_48F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10160" targetNode="P_48F10160"/>
	<edges id="P_54F10160P_55F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10160" targetNode="P_55F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10160_I" deadCode="false" sourceNode="P_10F10160" targetNode="P_66F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_189F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10160_O" deadCode="false" sourceNode="P_10F10160" targetNode="P_67F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_189F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10160_I" deadCode="false" sourceNode="P_10F10160" targetNode="P_68F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_191F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10160_O" deadCode="false" sourceNode="P_10F10160" targetNode="P_69F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_191F10160"/>
	</edges>
	<edges id="P_10F10160P_11F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10160" targetNode="P_11F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_196F10160_I" deadCode="false" sourceNode="P_66F10160" targetNode="P_61F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_196F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_196F10160_O" deadCode="false" sourceNode="P_66F10160" targetNode="P_62F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_196F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_201F10160_I" deadCode="false" sourceNode="P_66F10160" targetNode="P_61F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_201F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_201F10160_O" deadCode="false" sourceNode="P_66F10160" targetNode="P_62F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_201F10160"/>
	</edges>
	<edges id="P_66F10160P_67F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10160" targetNode="P_67F10160"/>
	<edges id="P_68F10160P_69F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10160" targetNode="P_69F10160"/>
	<edges id="P_61F10160P_62F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10160" targetNode="P_62F10160"/>
	<edges xsi:type="cbl:PerformEdge" id="S_230F10160_I" deadCode="false" sourceNode="P_64F10160" targetNode="P_72F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_230F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_230F10160_O" deadCode="false" sourceNode="P_64F10160" targetNode="P_73F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_230F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10160_I" deadCode="false" sourceNode="P_64F10160" targetNode="P_74F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_231F10160"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10160_O" deadCode="false" sourceNode="P_64F10160" targetNode="P_75F10160">
		<representations href="../../../cobol/LDBS1460.cbl.cobModel#S_231F10160"/>
	</edges>
	<edges id="P_64F10160P_65F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10160" targetNode="P_65F10160"/>
	<edges id="P_72F10160P_73F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10160" targetNode="P_73F10160"/>
	<edges id="P_74F10160P_75F10160" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10160" targetNode="P_75F10160"/>
	<edges xsi:type="cbl:DataEdge" id="S_65F10160_POS1" deadCode="false" targetNode="P_14F10160" sourceNode="DB2_SOPR_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_65F10160"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_73F10160_POS1" deadCode="false" targetNode="P_16F10160" sourceNode="DB2_SOPR_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_73F10160"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_76F10160_POS1" deadCode="false" targetNode="P_18F10160" sourceNode="DB2_SOPR_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_76F10160"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_83F10160_POS1" deadCode="false" targetNode="P_22F10160" sourceNode="DB2_SOPR_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_83F10160"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_101F10160_POS1" deadCode="false" targetNode="P_24F10160" sourceNode="DB2_SOPR_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_101F10160"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_109F10160_POS1" deadCode="false" targetNode="P_26F10160" sourceNode="DB2_SOPR_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_109F10160"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_112F10160_POS1" deadCode="false" targetNode="P_28F10160" sourceNode="DB2_SOPR_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_112F10160"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_119F10160_POS1" deadCode="false" targetNode="P_32F10160" sourceNode="DB2_SOPR_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_119F10160"></representations>
	</edges>
</Package>

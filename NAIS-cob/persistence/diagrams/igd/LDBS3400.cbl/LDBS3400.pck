<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS3400" cbl:id="LDBS3400" xsi:id="LDBS3400" packageRef="LDBS3400.igd#LDBS3400" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS3400_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10198" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS3400.cbl.cobModel#SC_1F10198"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10198" deadCode="false" name="PROGRAM_LDBS3400_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_1F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10198" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_2F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10198" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_3F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10198" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_12F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10198" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_13F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10198" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_4F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10198" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_5F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10198" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_6F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10198" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_7F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10198" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_8F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10198" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_9F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10198" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_44F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10198" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_49F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10198" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_14F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10198" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_15F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10198" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_16F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10198" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_17F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10198" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_18F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10198" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_19F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10198" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_20F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10198" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_21F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10198" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_22F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10198" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_23F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10198" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_56F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10198" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_57F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10198" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_24F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10198" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_25F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10198" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_26F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10198" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_27F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10198" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_28F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10198" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_29F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10198" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_30F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10198" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_31F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10198" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_32F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10198" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_33F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10198" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_58F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10198" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_59F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10198" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_34F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10198" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_35F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10198" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_36F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10198" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_37F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10198" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_38F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10198" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_39F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10198" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_40F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10198" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_41F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10198" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_42F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10198" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_43F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10198" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_50F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10198" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_51F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10198" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_60F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10198" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_63F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10198" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_52F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10198" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_53F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10198" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_45F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10198" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_46F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10198" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_47F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10198" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_48F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10198" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_54F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10198" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_55F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10198" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_10F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10198" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_11F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10198" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_66F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10198" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_67F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10198" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_68F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10198" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_69F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10198" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_61F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10198" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_62F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10198" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_70F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10198" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_71F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10198" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_64F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10198" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_65F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10198" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_72F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10198" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_73F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10198" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_74F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10198" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_75F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10198" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_76F10198"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10198" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS3400.cbl.cobModel#P_77F10198"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_GAR" name="GAR">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_GAR"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_STAT_OGG_BUS" name="STAT_OGG_BUS">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_STAT_OGG_BUS"/>
		</children>
	</packageNode>
	<edges id="SC_1F10198P_1F10198" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10198" targetNode="P_1F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10198_I" deadCode="false" sourceNode="P_1F10198" targetNode="P_2F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_1F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10198_O" deadCode="false" sourceNode="P_1F10198" targetNode="P_3F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_1F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10198_I" deadCode="false" sourceNode="P_1F10198" targetNode="P_4F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_5F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10198_O" deadCode="false" sourceNode="P_1F10198" targetNode="P_5F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_5F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10198_I" deadCode="false" sourceNode="P_1F10198" targetNode="P_6F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_9F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10198_O" deadCode="false" sourceNode="P_1F10198" targetNode="P_7F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_9F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10198_I" deadCode="false" sourceNode="P_1F10198" targetNode="P_8F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_13F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10198_O" deadCode="false" sourceNode="P_1F10198" targetNode="P_9F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_13F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10198_I" deadCode="false" sourceNode="P_2F10198" targetNode="P_10F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_22F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10198_O" deadCode="false" sourceNode="P_2F10198" targetNode="P_11F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_22F10198"/>
	</edges>
	<edges id="P_2F10198P_3F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10198" targetNode="P_3F10198"/>
	<edges id="P_12F10198P_13F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10198" targetNode="P_13F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10198_I" deadCode="false" sourceNode="P_4F10198" targetNode="P_14F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_35F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10198_O" deadCode="false" sourceNode="P_4F10198" targetNode="P_15F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_35F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10198_I" deadCode="false" sourceNode="P_4F10198" targetNode="P_16F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_36F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10198_O" deadCode="false" sourceNode="P_4F10198" targetNode="P_17F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_36F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10198_I" deadCode="false" sourceNode="P_4F10198" targetNode="P_18F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_37F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10198_O" deadCode="false" sourceNode="P_4F10198" targetNode="P_19F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_37F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10198_I" deadCode="false" sourceNode="P_4F10198" targetNode="P_20F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_38F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10198_O" deadCode="false" sourceNode="P_4F10198" targetNode="P_21F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_38F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10198_I" deadCode="false" sourceNode="P_4F10198" targetNode="P_22F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_39F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10198_O" deadCode="false" sourceNode="P_4F10198" targetNode="P_23F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_39F10198"/>
	</edges>
	<edges id="P_4F10198P_5F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10198" targetNode="P_5F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10198_I" deadCode="false" sourceNode="P_6F10198" targetNode="P_24F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_43F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10198_O" deadCode="false" sourceNode="P_6F10198" targetNode="P_25F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_43F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10198_I" deadCode="false" sourceNode="P_6F10198" targetNode="P_26F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_44F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10198_O" deadCode="false" sourceNode="P_6F10198" targetNode="P_27F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_44F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10198_I" deadCode="false" sourceNode="P_6F10198" targetNode="P_28F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_45F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10198_O" deadCode="false" sourceNode="P_6F10198" targetNode="P_29F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_45F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10198_I" deadCode="false" sourceNode="P_6F10198" targetNode="P_30F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_46F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10198_O" deadCode="false" sourceNode="P_6F10198" targetNode="P_31F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_46F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10198_I" deadCode="false" sourceNode="P_6F10198" targetNode="P_32F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_47F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10198_O" deadCode="false" sourceNode="P_6F10198" targetNode="P_33F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_47F10198"/>
	</edges>
	<edges id="P_6F10198P_7F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10198" targetNode="P_7F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10198_I" deadCode="false" sourceNode="P_8F10198" targetNode="P_34F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_51F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10198_O" deadCode="false" sourceNode="P_8F10198" targetNode="P_35F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_51F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10198_I" deadCode="false" sourceNode="P_8F10198" targetNode="P_36F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_52F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10198_O" deadCode="false" sourceNode="P_8F10198" targetNode="P_37F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_52F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10198_I" deadCode="false" sourceNode="P_8F10198" targetNode="P_38F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_53F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10198_O" deadCode="false" sourceNode="P_8F10198" targetNode="P_39F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_53F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10198_I" deadCode="false" sourceNode="P_8F10198" targetNode="P_40F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_54F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10198_O" deadCode="false" sourceNode="P_8F10198" targetNode="P_41F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_54F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10198_I" deadCode="false" sourceNode="P_8F10198" targetNode="P_42F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_55F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10198_O" deadCode="false" sourceNode="P_8F10198" targetNode="P_43F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_55F10198"/>
	</edges>
	<edges id="P_8F10198P_9F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10198" targetNode="P_9F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10198_I" deadCode="false" sourceNode="P_44F10198" targetNode="P_45F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_58F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10198_O" deadCode="false" sourceNode="P_44F10198" targetNode="P_46F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_58F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10198_I" deadCode="false" sourceNode="P_44F10198" targetNode="P_47F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_59F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10198_O" deadCode="false" sourceNode="P_44F10198" targetNode="P_48F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_59F10198"/>
	</edges>
	<edges id="P_44F10198P_49F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10198" targetNode="P_49F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10198_I" deadCode="false" sourceNode="P_14F10198" targetNode="P_45F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_63F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10198_O" deadCode="false" sourceNode="P_14F10198" targetNode="P_46F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_63F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10198_I" deadCode="false" sourceNode="P_14F10198" targetNode="P_47F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_64F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10198_O" deadCode="false" sourceNode="P_14F10198" targetNode="P_48F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_64F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10198_I" deadCode="false" sourceNode="P_14F10198" targetNode="P_12F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_66F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10198_O" deadCode="false" sourceNode="P_14F10198" targetNode="P_13F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_66F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10198_I" deadCode="false" sourceNode="P_14F10198" targetNode="P_50F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_68F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10198_O" deadCode="false" sourceNode="P_14F10198" targetNode="P_51F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_68F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10198_I" deadCode="false" sourceNode="P_14F10198" targetNode="P_52F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_69F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10198_O" deadCode="false" sourceNode="P_14F10198" targetNode="P_53F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_69F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10198_I" deadCode="false" sourceNode="P_14F10198" targetNode="P_54F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_70F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10198_O" deadCode="false" sourceNode="P_14F10198" targetNode="P_55F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_70F10198"/>
	</edges>
	<edges id="P_14F10198P_15F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10198" targetNode="P_15F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10198_I" deadCode="false" sourceNode="P_16F10198" targetNode="P_44F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_72F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10198_O" deadCode="false" sourceNode="P_16F10198" targetNode="P_49F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_72F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10198_I" deadCode="false" sourceNode="P_16F10198" targetNode="P_12F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_74F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10198_O" deadCode="false" sourceNode="P_16F10198" targetNode="P_13F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_74F10198"/>
	</edges>
	<edges id="P_16F10198P_17F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10198" targetNode="P_17F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10198_I" deadCode="false" sourceNode="P_18F10198" targetNode="P_12F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_77F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10198_O" deadCode="false" sourceNode="P_18F10198" targetNode="P_13F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_77F10198"/>
	</edges>
	<edges id="P_18F10198P_19F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10198" targetNode="P_19F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10198_I" deadCode="false" sourceNode="P_20F10198" targetNode="P_16F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_79F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10198_O" deadCode="false" sourceNode="P_20F10198" targetNode="P_17F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_79F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10198_I" deadCode="false" sourceNode="P_20F10198" targetNode="P_22F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_81F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10198_O" deadCode="false" sourceNode="P_20F10198" targetNode="P_23F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_81F10198"/>
	</edges>
	<edges id="P_20F10198P_21F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10198" targetNode="P_21F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10198_I" deadCode="false" sourceNode="P_22F10198" targetNode="P_12F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_84F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10198_O" deadCode="false" sourceNode="P_22F10198" targetNode="P_13F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_84F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10198_I" deadCode="false" sourceNode="P_22F10198" targetNode="P_50F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_86F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10198_O" deadCode="false" sourceNode="P_22F10198" targetNode="P_51F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_86F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10198_I" deadCode="false" sourceNode="P_22F10198" targetNode="P_52F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_87F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10198_O" deadCode="false" sourceNode="P_22F10198" targetNode="P_53F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_87F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10198_I" deadCode="false" sourceNode="P_22F10198" targetNode="P_54F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_88F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10198_O" deadCode="false" sourceNode="P_22F10198" targetNode="P_55F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_88F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10198_I" deadCode="false" sourceNode="P_22F10198" targetNode="P_18F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_90F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10198_O" deadCode="false" sourceNode="P_22F10198" targetNode="P_19F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_90F10198"/>
	</edges>
	<edges id="P_22F10198P_23F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10198" targetNode="P_23F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10198_I" deadCode="false" sourceNode="P_56F10198" targetNode="P_45F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_94F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10198_O" deadCode="false" sourceNode="P_56F10198" targetNode="P_46F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_94F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10198_I" deadCode="false" sourceNode="P_56F10198" targetNode="P_47F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_95F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10198_O" deadCode="false" sourceNode="P_56F10198" targetNode="P_48F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_95F10198"/>
	</edges>
	<edges id="P_56F10198P_57F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10198" targetNode="P_57F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10198_I" deadCode="false" sourceNode="P_24F10198" targetNode="P_45F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_99F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10198_O" deadCode="false" sourceNode="P_24F10198" targetNode="P_46F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_99F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10198_I" deadCode="false" sourceNode="P_24F10198" targetNode="P_47F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_100F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10198_O" deadCode="false" sourceNode="P_24F10198" targetNode="P_48F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_100F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10198_I" deadCode="false" sourceNode="P_24F10198" targetNode="P_12F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_102F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10198_O" deadCode="false" sourceNode="P_24F10198" targetNode="P_13F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_102F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10198_I" deadCode="false" sourceNode="P_24F10198" targetNode="P_50F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_104F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10198_O" deadCode="false" sourceNode="P_24F10198" targetNode="P_51F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_104F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10198_I" deadCode="false" sourceNode="P_24F10198" targetNode="P_52F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_105F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10198_O" deadCode="false" sourceNode="P_24F10198" targetNode="P_53F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_105F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10198_I" deadCode="false" sourceNode="P_24F10198" targetNode="P_54F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_106F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10198_O" deadCode="false" sourceNode="P_24F10198" targetNode="P_55F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_106F10198"/>
	</edges>
	<edges id="P_24F10198P_25F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10198" targetNode="P_25F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10198_I" deadCode="false" sourceNode="P_26F10198" targetNode="P_56F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_108F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10198_O" deadCode="false" sourceNode="P_26F10198" targetNode="P_57F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_108F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10198_I" deadCode="false" sourceNode="P_26F10198" targetNode="P_12F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_110F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10198_O" deadCode="false" sourceNode="P_26F10198" targetNode="P_13F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_110F10198"/>
	</edges>
	<edges id="P_26F10198P_27F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10198" targetNode="P_27F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10198_I" deadCode="false" sourceNode="P_28F10198" targetNode="P_12F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_113F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10198_O" deadCode="false" sourceNode="P_28F10198" targetNode="P_13F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_113F10198"/>
	</edges>
	<edges id="P_28F10198P_29F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10198" targetNode="P_29F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10198_I" deadCode="false" sourceNode="P_30F10198" targetNode="P_26F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_115F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10198_O" deadCode="false" sourceNode="P_30F10198" targetNode="P_27F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_115F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10198_I" deadCode="false" sourceNode="P_30F10198" targetNode="P_32F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_117F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10198_O" deadCode="false" sourceNode="P_30F10198" targetNode="P_33F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_117F10198"/>
	</edges>
	<edges id="P_30F10198P_31F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10198" targetNode="P_31F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10198_I" deadCode="false" sourceNode="P_32F10198" targetNode="P_12F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_120F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10198_O" deadCode="false" sourceNode="P_32F10198" targetNode="P_13F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_120F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10198_I" deadCode="false" sourceNode="P_32F10198" targetNode="P_50F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_122F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10198_O" deadCode="false" sourceNode="P_32F10198" targetNode="P_51F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_122F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10198_I" deadCode="false" sourceNode="P_32F10198" targetNode="P_52F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_123F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10198_O" deadCode="false" sourceNode="P_32F10198" targetNode="P_53F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_123F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10198_I" deadCode="false" sourceNode="P_32F10198" targetNode="P_54F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_124F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10198_O" deadCode="false" sourceNode="P_32F10198" targetNode="P_55F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_124F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10198_I" deadCode="false" sourceNode="P_32F10198" targetNode="P_28F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_126F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10198_O" deadCode="false" sourceNode="P_32F10198" targetNode="P_29F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_126F10198"/>
	</edges>
	<edges id="P_32F10198P_33F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10198" targetNode="P_33F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10198_I" deadCode="false" sourceNode="P_58F10198" targetNode="P_45F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_130F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10198_O" deadCode="false" sourceNode="P_58F10198" targetNode="P_46F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_130F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10198_I" deadCode="false" sourceNode="P_58F10198" targetNode="P_47F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_131F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10198_O" deadCode="false" sourceNode="P_58F10198" targetNode="P_48F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_131F10198"/>
	</edges>
	<edges id="P_58F10198P_59F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10198" targetNode="P_59F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_134F10198_I" deadCode="false" sourceNode="P_34F10198" targetNode="P_45F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_134F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_134F10198_O" deadCode="false" sourceNode="P_34F10198" targetNode="P_46F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_134F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10198_I" deadCode="false" sourceNode="P_34F10198" targetNode="P_47F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_135F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10198_O" deadCode="false" sourceNode="P_34F10198" targetNode="P_48F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_135F10198"/>
	</edges>
	<edges id="P_34F10198P_35F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10198" targetNode="P_35F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10198_I" deadCode="false" sourceNode="P_36F10198" targetNode="P_58F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_138F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10198_O" deadCode="false" sourceNode="P_36F10198" targetNode="P_59F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_138F10198"/>
	</edges>
	<edges id="P_36F10198P_37F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10198" targetNode="P_37F10198"/>
	<edges id="P_38F10198P_39F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10198" targetNode="P_39F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10198_I" deadCode="false" sourceNode="P_40F10198" targetNode="P_36F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_143F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10198_O" deadCode="false" sourceNode="P_40F10198" targetNode="P_37F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_143F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10198_I" deadCode="false" sourceNode="P_40F10198" targetNode="P_42F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_145F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10198_O" deadCode="false" sourceNode="P_40F10198" targetNode="P_43F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_145F10198"/>
	</edges>
	<edges id="P_40F10198P_41F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10198" targetNode="P_41F10198"/>
	<edges id="P_42F10198P_43F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10198" targetNode="P_43F10198"/>
	<edges id="P_50F10198P_51F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10198" targetNode="P_51F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_266F10198_I" deadCode="true" sourceNode="P_60F10198" targetNode="P_61F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_266F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_266F10198_O" deadCode="true" sourceNode="P_60F10198" targetNode="P_62F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_266F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_269F10198_I" deadCode="true" sourceNode="P_60F10198" targetNode="P_61F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_269F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_269F10198_O" deadCode="true" sourceNode="P_60F10198" targetNode="P_62F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_269F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_273F10198_I" deadCode="true" sourceNode="P_60F10198" targetNode="P_61F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_273F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_273F10198_O" deadCode="true" sourceNode="P_60F10198" targetNode="P_62F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_273F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_277F10198_I" deadCode="true" sourceNode="P_60F10198" targetNode="P_61F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_277F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_277F10198_O" deadCode="true" sourceNode="P_60F10198" targetNode="P_62F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_277F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10198_I" deadCode="true" sourceNode="P_60F10198" targetNode="P_61F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_281F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10198_O" deadCode="true" sourceNode="P_60F10198" targetNode="P_62F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_281F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_285F10198_I" deadCode="true" sourceNode="P_60F10198" targetNode="P_61F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_285F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_285F10198_O" deadCode="true" sourceNode="P_60F10198" targetNode="P_62F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_285F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10198_I" deadCode="true" sourceNode="P_60F10198" targetNode="P_61F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_289F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10198_O" deadCode="true" sourceNode="P_60F10198" targetNode="P_62F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_289F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_293F10198_I" deadCode="true" sourceNode="P_60F10198" targetNode="P_61F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_293F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_293F10198_O" deadCode="true" sourceNode="P_60F10198" targetNode="P_62F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_293F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_296F10198_I" deadCode="true" sourceNode="P_60F10198" targetNode="P_61F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_296F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_296F10198_O" deadCode="true" sourceNode="P_60F10198" targetNode="P_62F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_296F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10198_I" deadCode="true" sourceNode="P_60F10198" targetNode="P_61F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_299F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10198_O" deadCode="true" sourceNode="P_60F10198" targetNode="P_62F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_299F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_303F10198_I" deadCode="false" sourceNode="P_52F10198" targetNode="P_64F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_303F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_303F10198_O" deadCode="false" sourceNode="P_52F10198" targetNode="P_65F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_303F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_306F10198_I" deadCode="false" sourceNode="P_52F10198" targetNode="P_64F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_306F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_306F10198_O" deadCode="false" sourceNode="P_52F10198" targetNode="P_65F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_306F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_310F10198_I" deadCode="false" sourceNode="P_52F10198" targetNode="P_64F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_310F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_310F10198_O" deadCode="false" sourceNode="P_52F10198" targetNode="P_65F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_310F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_314F10198_I" deadCode="false" sourceNode="P_52F10198" targetNode="P_64F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_314F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_314F10198_O" deadCode="false" sourceNode="P_52F10198" targetNode="P_65F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_314F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_318F10198_I" deadCode="false" sourceNode="P_52F10198" targetNode="P_64F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_318F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_318F10198_O" deadCode="false" sourceNode="P_52F10198" targetNode="P_65F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_318F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_322F10198_I" deadCode="false" sourceNode="P_52F10198" targetNode="P_64F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_322F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_322F10198_O" deadCode="false" sourceNode="P_52F10198" targetNode="P_65F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_322F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_326F10198_I" deadCode="false" sourceNode="P_52F10198" targetNode="P_64F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_326F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_326F10198_O" deadCode="false" sourceNode="P_52F10198" targetNode="P_65F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_326F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_330F10198_I" deadCode="false" sourceNode="P_52F10198" targetNode="P_64F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_330F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_330F10198_O" deadCode="false" sourceNode="P_52F10198" targetNode="P_65F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_330F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_333F10198_I" deadCode="false" sourceNode="P_52F10198" targetNode="P_64F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_333F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_333F10198_O" deadCode="false" sourceNode="P_52F10198" targetNode="P_65F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_333F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10198_I" deadCode="false" sourceNode="P_52F10198" targetNode="P_64F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_336F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10198_O" deadCode="false" sourceNode="P_52F10198" targetNode="P_65F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_336F10198"/>
	</edges>
	<edges id="P_52F10198P_53F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10198" targetNode="P_53F10198"/>
	<edges id="P_45F10198P_46F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10198" targetNode="P_46F10198"/>
	<edges id="P_47F10198P_48F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10198" targetNode="P_48F10198"/>
	<edges id="P_54F10198P_55F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10198" targetNode="P_55F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_347F10198_I" deadCode="false" sourceNode="P_10F10198" targetNode="P_66F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_347F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_347F10198_O" deadCode="false" sourceNode="P_10F10198" targetNode="P_67F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_347F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_349F10198_I" deadCode="false" sourceNode="P_10F10198" targetNode="P_68F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_349F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_349F10198_O" deadCode="false" sourceNode="P_10F10198" targetNode="P_69F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_349F10198"/>
	</edges>
	<edges id="P_10F10198P_11F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10198" targetNode="P_11F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_354F10198_I" deadCode="false" sourceNode="P_66F10198" targetNode="P_61F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_354F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_354F10198_O" deadCode="false" sourceNode="P_66F10198" targetNode="P_62F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_354F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_359F10198_I" deadCode="false" sourceNode="P_66F10198" targetNode="P_61F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_359F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_359F10198_O" deadCode="false" sourceNode="P_66F10198" targetNode="P_62F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_359F10198"/>
	</edges>
	<edges id="P_66F10198P_67F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10198" targetNode="P_67F10198"/>
	<edges id="P_68F10198P_69F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10198" targetNode="P_69F10198"/>
	<edges id="P_61F10198P_62F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10198" targetNode="P_62F10198"/>
	<edges xsi:type="cbl:PerformEdge" id="S_388F10198_I" deadCode="false" sourceNode="P_64F10198" targetNode="P_72F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_388F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_388F10198_O" deadCode="false" sourceNode="P_64F10198" targetNode="P_73F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_388F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_389F10198_I" deadCode="false" sourceNode="P_64F10198" targetNode="P_74F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_389F10198"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_389F10198_O" deadCode="false" sourceNode="P_64F10198" targetNode="P_75F10198">
		<representations href="../../../cobol/LDBS3400.cbl.cobModel#S_389F10198"/>
	</edges>
	<edges id="P_64F10198P_65F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10198" targetNode="P_65F10198"/>
	<edges id="P_72F10198P_73F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10198" targetNode="P_73F10198"/>
	<edges id="P_74F10198P_75F10198" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10198" targetNode="P_75F10198"/>
	<edges xsi:type="cbl:DataEdge" id="S_65F10198_POS1" deadCode="false" targetNode="P_14F10198" sourceNode="DB2_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_65F10198"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_65F10198_POS2" deadCode="false" targetNode="P_14F10198" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_65F10198"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_73F10198_POS1" deadCode="false" targetNode="P_16F10198" sourceNode="DB2_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_73F10198"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_73F10198_POS2" deadCode="false" targetNode="P_16F10198" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_73F10198"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_76F10198_POS1" deadCode="false" targetNode="P_18F10198" sourceNode="DB2_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_76F10198"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_76F10198_POS2" deadCode="false" targetNode="P_18F10198" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_76F10198"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_83F10198_POS1" deadCode="false" targetNode="P_22F10198" sourceNode="DB2_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_83F10198"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_83F10198_POS2" deadCode="false" targetNode="P_22F10198" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_83F10198"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_101F10198_POS1" deadCode="false" targetNode="P_24F10198" sourceNode="DB2_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_101F10198"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_101F10198_POS2" deadCode="false" targetNode="P_24F10198" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_101F10198"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_109F10198_POS1" deadCode="false" targetNode="P_26F10198" sourceNode="DB2_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_109F10198"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_109F10198_POS2" deadCode="false" targetNode="P_26F10198" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_109F10198"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_112F10198_POS1" deadCode="false" targetNode="P_28F10198" sourceNode="DB2_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_112F10198"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_112F10198_POS2" deadCode="false" targetNode="P_28F10198" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_112F10198"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_119F10198_POS1" deadCode="false" targetNode="P_32F10198" sourceNode="DB2_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_119F10198"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_119F10198_POS2" deadCode="false" targetNode="P_32F10198" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_119F10198"></representations>
	</edges>
</Package>

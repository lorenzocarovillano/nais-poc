<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVES0245" cbl:id="LVES0245" xsi:id="LVES0245" packageRef="LVES0245.igd#LVES0245" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVES0245_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10300" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVES0245.cbl.cobModel#SC_1F10300"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10300" deadCode="false" name="PROGRAM_LVES0245_FIRST_SENTENCES">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_1F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10300" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_2F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10300" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_3F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10300" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_4F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10300" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_5F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10300" deadCode="false" name="GEST-FRAZ-MEN">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_14F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10300" deadCode="false" name="GEST-FRAZ-MEN-EX">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_15F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10300" deadCode="false" name="RATEANTIC-ZERO">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_12F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10300" deadCode="false" name="RATEANTIC-ZERO-EX">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_13F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10300" deadCode="false" name="RATEANTIC-DUE">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_18F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10300" deadCode="false" name="RATEANTIC-DUE-EX">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_19F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10300" deadCode="false" name="RIC-TAB-PMO">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_8F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10300" deadCode="false" name="RIC-TAB-PMO-EX">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_9F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10300" deadCode="false" name="RIC-TAB-POG">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_10F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10300" deadCode="false" name="RIC-TAB-POG-EX">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_11F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10300" deadCode="false" name="RIC-TAB-RAN">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_16F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10300" deadCode="false" name="RIC-TAB-RAN-EX">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_17F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10300" deadCode="false" name="RATEANTIC-TRAT-STIP">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_20F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10300" deadCode="false" name="RATEANTIC-TRAT-STIP-EX">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_21F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10300" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_6F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10300" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_7F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10300" deadCode="true" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_22F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10300" deadCode="true" name="EX-S0290">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_25F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10300" deadCode="true" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_23F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10300" deadCode="true" name="EX-S0300">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_24F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10300" deadCode="true" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_28F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10300" deadCode="true" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_29F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10300" deadCode="true" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_26F10300"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10300" deadCode="true" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LVES0245.cbl.cobModel#P_27F10300"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LVES0245_CALL-PGM" name="Dynamic LVES0245 CALL-PGM" missing="true">
			<representations href="../../../../missing.xmi#IDES33HCRK0SP1GNLL2IFHKLLTDIFXPFBOYAWPRMMRSNPGVNO2B0EH"/>
		</children>
	</packageNode>
	<edges id="SC_1F10300P_1F10300" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10300" targetNode="P_1F10300"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10300_I" deadCode="false" sourceNode="P_1F10300" targetNode="P_2F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_1F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10300_O" deadCode="false" sourceNode="P_1F10300" targetNode="P_3F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_1F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10300_I" deadCode="false" sourceNode="P_1F10300" targetNode="P_4F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_3F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10300_O" deadCode="false" sourceNode="P_1F10300" targetNode="P_5F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_3F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10300_I" deadCode="false" sourceNode="P_1F10300" targetNode="P_6F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_4F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10300_O" deadCode="false" sourceNode="P_1F10300" targetNode="P_7F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_4F10300"/>
	</edges>
	<edges id="P_2F10300P_3F10300" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10300" targetNode="P_3F10300"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10300_I" deadCode="false" sourceNode="P_4F10300" targetNode="P_8F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_11F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10300_O" deadCode="false" sourceNode="P_4F10300" targetNode="P_9F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_11F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10300_I" deadCode="false" sourceNode="P_4F10300" targetNode="P_8F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_16F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10300_O" deadCode="false" sourceNode="P_4F10300" targetNode="P_9F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_16F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10300_I" deadCode="false" sourceNode="P_4F10300" targetNode="P_10F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_25F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10300_O" deadCode="false" sourceNode="P_4F10300" targetNode="P_11F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_25F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10300_I" deadCode="false" sourceNode="P_4F10300" targetNode="P_12F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_29F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10300_O" deadCode="false" sourceNode="P_4F10300" targetNode="P_13F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_29F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10300_I" deadCode="false" sourceNode="P_4F10300" targetNode="P_14F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_30F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10300_O" deadCode="false" sourceNode="P_4F10300" targetNode="P_15F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_30F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10300_I" deadCode="false" sourceNode="P_4F10300" targetNode="P_12F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_31F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10300_O" deadCode="false" sourceNode="P_4F10300" targetNode="P_13F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_31F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10300_I" deadCode="false" sourceNode="P_4F10300" targetNode="P_12F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_32F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10300_O" deadCode="false" sourceNode="P_4F10300" targetNode="P_13F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_32F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10300_I" deadCode="false" sourceNode="P_4F10300" targetNode="P_12F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_33F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10300_O" deadCode="false" sourceNode="P_4F10300" targetNode="P_13F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_33F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10300_I" deadCode="false" sourceNode="P_4F10300" targetNode="P_12F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_34F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10300_O" deadCode="false" sourceNode="P_4F10300" targetNode="P_13F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_34F10300"/>
	</edges>
	<edges id="P_4F10300P_5F10300" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10300" targetNode="P_5F10300"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10300_I" deadCode="false" sourceNode="P_14F10300" targetNode="P_16F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_37F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10300_O" deadCode="false" sourceNode="P_14F10300" targetNode="P_17F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_37F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10300_I" deadCode="false" sourceNode="P_14F10300" targetNode="P_18F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_42F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10300_O" deadCode="false" sourceNode="P_14F10300" targetNode="P_19F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_42F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10300_I" deadCode="false" sourceNode="P_14F10300" targetNode="P_20F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_43F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10300_O" deadCode="false" sourceNode="P_14F10300" targetNode="P_21F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_43F10300"/>
	</edges>
	<edges id="P_14F10300P_15F10300" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10300" targetNode="P_15F10300"/>
	<edges id="P_12F10300P_13F10300" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10300" targetNode="P_13F10300"/>
	<edges id="P_18F10300P_19F10300" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10300" targetNode="P_19F10300"/>
	<edges id="P_8F10300P_9F10300" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10300" targetNode="P_9F10300"/>
	<edges id="P_10F10300P_11F10300" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10300" targetNode="P_11F10300"/>
	<edges id="P_16F10300P_17F10300" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10300" targetNode="P_17F10300"/>
	<edges id="P_20F10300P_21F10300" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10300" targetNode="P_21F10300"/>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10300_I" deadCode="true" sourceNode="P_22F10300" targetNode="P_23F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_76F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10300_O" deadCode="true" sourceNode="P_22F10300" targetNode="P_24F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_76F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10300_I" deadCode="true" sourceNode="P_23F10300" targetNode="P_26F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_83F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10300_O" deadCode="true" sourceNode="P_23F10300" targetNode="P_27F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_83F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10300_I" deadCode="true" sourceNode="P_23F10300" targetNode="P_28F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_87F10300"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10300_O" deadCode="true" sourceNode="P_23F10300" targetNode="P_29F10300">
		<representations href="../../../cobol/LVES0245.cbl.cobModel#S_87F10300"/>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_81F10300" deadCode="true" name="Dynamic CALL-PGM" sourceNode="P_23F10300" targetNode="Dynamic_LVES0245_CALL-PGM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_81F10300"></representations>
	</edges>
</Package>

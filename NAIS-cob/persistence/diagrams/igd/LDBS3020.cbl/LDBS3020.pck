<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS3020" cbl:id="LDBS3020" xsi:id="LDBS3020" packageRef="LDBS3020.igd#LDBS3020" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS3020_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10195" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS3020.cbl.cobModel#SC_1F10195"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10195" deadCode="false" name="PROGRAM_LDBS3020_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_1F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10195" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_2F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10195" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_3F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10195" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_12F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10195" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_13F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10195" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_4F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10195" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_5F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10195" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_6F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10195" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_7F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10195" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_8F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10195" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_9F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10195" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_44F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10195" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_49F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10195" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_14F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10195" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_15F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10195" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_16F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10195" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_17F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10195" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_18F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10195" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_19F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10195" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_20F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10195" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_21F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10195" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_22F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10195" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_23F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10195" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_56F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10195" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_57F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10195" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_24F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10195" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_25F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10195" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_26F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10195" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_27F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10195" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_28F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10195" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_29F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10195" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_30F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10195" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_31F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10195" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_32F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10195" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_33F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10195" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_58F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10195" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_59F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10195" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_34F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10195" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_35F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10195" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_36F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10195" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_37F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10195" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_38F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10195" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_39F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10195" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_40F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10195" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_41F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10195" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_42F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10195" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_43F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10195" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_50F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10195" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_51F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10195" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_60F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10195" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_63F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10195" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_52F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10195" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_53F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10195" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_45F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10195" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_46F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10195" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_47F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10195" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_48F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10195" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_54F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10195" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_55F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10195" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_10F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10195" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_11F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10195" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_66F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10195" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_67F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10195" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_68F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10195" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_69F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10195" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_61F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10195" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_62F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10195" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_70F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10195" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_71F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10195" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_64F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10195" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_65F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10195" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_72F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10195" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_73F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10195" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_74F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10195" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_75F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10195" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_76F10195"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10195" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS3020.cbl.cobModel#P_77F10195"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_TRCH_DI_GAR" name="TRCH_DI_GAR">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_TRCH_DI_GAR"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_STAT_OGG_BUS" name="STAT_OGG_BUS">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_STAT_OGG_BUS"/>
		</children>
	</packageNode>
	<edges id="SC_1F10195P_1F10195" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10195" targetNode="P_1F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10195_I" deadCode="false" sourceNode="P_1F10195" targetNode="P_2F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_2F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10195_O" deadCode="false" sourceNode="P_1F10195" targetNode="P_3F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_2F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10195_I" deadCode="false" sourceNode="P_1F10195" targetNode="P_4F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_6F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10195_O" deadCode="false" sourceNode="P_1F10195" targetNode="P_5F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_6F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10195_I" deadCode="false" sourceNode="P_1F10195" targetNode="P_6F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_10F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10195_O" deadCode="false" sourceNode="P_1F10195" targetNode="P_7F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_10F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10195_I" deadCode="false" sourceNode="P_1F10195" targetNode="P_8F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_14F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10195_O" deadCode="false" sourceNode="P_1F10195" targetNode="P_9F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_14F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10195_I" deadCode="false" sourceNode="P_2F10195" targetNode="P_10F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_24F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10195_O" deadCode="false" sourceNode="P_2F10195" targetNode="P_11F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_24F10195"/>
	</edges>
	<edges id="P_2F10195P_3F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10195" targetNode="P_3F10195"/>
	<edges id="P_12F10195P_13F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10195" targetNode="P_13F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10195_I" deadCode="false" sourceNode="P_4F10195" targetNode="P_14F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_37F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10195_O" deadCode="false" sourceNode="P_4F10195" targetNode="P_15F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_37F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10195_I" deadCode="false" sourceNode="P_4F10195" targetNode="P_16F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_38F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10195_O" deadCode="false" sourceNode="P_4F10195" targetNode="P_17F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_38F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10195_I" deadCode="false" sourceNode="P_4F10195" targetNode="P_18F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_39F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10195_O" deadCode="false" sourceNode="P_4F10195" targetNode="P_19F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_39F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10195_I" deadCode="false" sourceNode="P_4F10195" targetNode="P_20F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_40F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10195_O" deadCode="false" sourceNode="P_4F10195" targetNode="P_21F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_40F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10195_I" deadCode="false" sourceNode="P_4F10195" targetNode="P_22F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_41F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10195_O" deadCode="false" sourceNode="P_4F10195" targetNode="P_23F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_41F10195"/>
	</edges>
	<edges id="P_4F10195P_5F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10195" targetNode="P_5F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10195_I" deadCode="false" sourceNode="P_6F10195" targetNode="P_24F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_45F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10195_O" deadCode="false" sourceNode="P_6F10195" targetNode="P_25F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_45F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10195_I" deadCode="false" sourceNode="P_6F10195" targetNode="P_26F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_46F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10195_O" deadCode="false" sourceNode="P_6F10195" targetNode="P_27F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_46F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10195_I" deadCode="false" sourceNode="P_6F10195" targetNode="P_28F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_47F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10195_O" deadCode="false" sourceNode="P_6F10195" targetNode="P_29F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_47F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10195_I" deadCode="false" sourceNode="P_6F10195" targetNode="P_30F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_48F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10195_O" deadCode="false" sourceNode="P_6F10195" targetNode="P_31F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_48F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10195_I" deadCode="false" sourceNode="P_6F10195" targetNode="P_32F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_49F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10195_O" deadCode="false" sourceNode="P_6F10195" targetNode="P_33F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_49F10195"/>
	</edges>
	<edges id="P_6F10195P_7F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10195" targetNode="P_7F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10195_I" deadCode="false" sourceNode="P_8F10195" targetNode="P_34F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_53F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10195_O" deadCode="false" sourceNode="P_8F10195" targetNode="P_35F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_53F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10195_I" deadCode="false" sourceNode="P_8F10195" targetNode="P_36F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_54F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10195_O" deadCode="false" sourceNode="P_8F10195" targetNode="P_37F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_54F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10195_I" deadCode="false" sourceNode="P_8F10195" targetNode="P_38F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_55F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10195_O" deadCode="false" sourceNode="P_8F10195" targetNode="P_39F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_55F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10195_I" deadCode="false" sourceNode="P_8F10195" targetNode="P_40F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_56F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10195_O" deadCode="false" sourceNode="P_8F10195" targetNode="P_41F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_56F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10195_I" deadCode="false" sourceNode="P_8F10195" targetNode="P_42F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_57F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10195_O" deadCode="false" sourceNode="P_8F10195" targetNode="P_43F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_57F10195"/>
	</edges>
	<edges id="P_8F10195P_9F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10195" targetNode="P_9F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10195_I" deadCode="false" sourceNode="P_44F10195" targetNode="P_45F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_60F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10195_O" deadCode="false" sourceNode="P_44F10195" targetNode="P_46F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_60F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10195_I" deadCode="false" sourceNode="P_44F10195" targetNode="P_47F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_61F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10195_O" deadCode="false" sourceNode="P_44F10195" targetNode="P_48F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_61F10195"/>
	</edges>
	<edges id="P_44F10195P_49F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10195" targetNode="P_49F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10195_I" deadCode="false" sourceNode="P_14F10195" targetNode="P_45F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_65F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10195_O" deadCode="false" sourceNode="P_14F10195" targetNode="P_46F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_65F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10195_I" deadCode="false" sourceNode="P_14F10195" targetNode="P_47F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_66F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10195_O" deadCode="false" sourceNode="P_14F10195" targetNode="P_48F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_66F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10195_I" deadCode="false" sourceNode="P_14F10195" targetNode="P_12F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_68F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10195_O" deadCode="false" sourceNode="P_14F10195" targetNode="P_13F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_68F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10195_I" deadCode="false" sourceNode="P_14F10195" targetNode="P_50F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_70F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10195_O" deadCode="false" sourceNode="P_14F10195" targetNode="P_51F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_70F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10195_I" deadCode="false" sourceNode="P_14F10195" targetNode="P_52F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_71F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10195_O" deadCode="false" sourceNode="P_14F10195" targetNode="P_53F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_71F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10195_I" deadCode="false" sourceNode="P_14F10195" targetNode="P_54F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_72F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10195_O" deadCode="false" sourceNode="P_14F10195" targetNode="P_55F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_72F10195"/>
	</edges>
	<edges id="P_14F10195P_15F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10195" targetNode="P_15F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10195_I" deadCode="false" sourceNode="P_16F10195" targetNode="P_44F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_74F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10195_O" deadCode="false" sourceNode="P_16F10195" targetNode="P_49F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_74F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10195_I" deadCode="false" sourceNode="P_16F10195" targetNode="P_12F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_76F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10195_O" deadCode="false" sourceNode="P_16F10195" targetNode="P_13F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_76F10195"/>
	</edges>
	<edges id="P_16F10195P_17F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10195" targetNode="P_17F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10195_I" deadCode="false" sourceNode="P_18F10195" targetNode="P_12F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_79F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10195_O" deadCode="false" sourceNode="P_18F10195" targetNode="P_13F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_79F10195"/>
	</edges>
	<edges id="P_18F10195P_19F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10195" targetNode="P_19F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10195_I" deadCode="false" sourceNode="P_20F10195" targetNode="P_16F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_81F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10195_O" deadCode="false" sourceNode="P_20F10195" targetNode="P_17F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_81F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10195_I" deadCode="false" sourceNode="P_20F10195" targetNode="P_22F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_83F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10195_O" deadCode="false" sourceNode="P_20F10195" targetNode="P_23F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_83F10195"/>
	</edges>
	<edges id="P_20F10195P_21F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10195" targetNode="P_21F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10195_I" deadCode="false" sourceNode="P_22F10195" targetNode="P_12F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_86F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10195_O" deadCode="false" sourceNode="P_22F10195" targetNode="P_13F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_86F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10195_I" deadCode="false" sourceNode="P_22F10195" targetNode="P_50F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_88F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10195_O" deadCode="false" sourceNode="P_22F10195" targetNode="P_51F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_88F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10195_I" deadCode="false" sourceNode="P_22F10195" targetNode="P_52F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_89F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10195_O" deadCode="false" sourceNode="P_22F10195" targetNode="P_53F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_89F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10195_I" deadCode="false" sourceNode="P_22F10195" targetNode="P_54F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_90F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10195_O" deadCode="false" sourceNode="P_22F10195" targetNode="P_55F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_90F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10195_I" deadCode="false" sourceNode="P_22F10195" targetNode="P_18F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_92F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10195_O" deadCode="false" sourceNode="P_22F10195" targetNode="P_19F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_92F10195"/>
	</edges>
	<edges id="P_22F10195P_23F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10195" targetNode="P_23F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10195_I" deadCode="false" sourceNode="P_56F10195" targetNode="P_45F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_96F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10195_O" deadCode="false" sourceNode="P_56F10195" targetNode="P_46F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_96F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10195_I" deadCode="false" sourceNode="P_56F10195" targetNode="P_47F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_97F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10195_O" deadCode="false" sourceNode="P_56F10195" targetNode="P_48F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_97F10195"/>
	</edges>
	<edges id="P_56F10195P_57F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10195" targetNode="P_57F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10195_I" deadCode="false" sourceNode="P_24F10195" targetNode="P_45F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_101F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10195_O" deadCode="false" sourceNode="P_24F10195" targetNode="P_46F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_101F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10195_I" deadCode="false" sourceNode="P_24F10195" targetNode="P_47F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_102F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10195_O" deadCode="false" sourceNode="P_24F10195" targetNode="P_48F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_102F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10195_I" deadCode="false" sourceNode="P_24F10195" targetNode="P_12F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_104F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10195_O" deadCode="false" sourceNode="P_24F10195" targetNode="P_13F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_104F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10195_I" deadCode="false" sourceNode="P_24F10195" targetNode="P_50F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_106F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10195_O" deadCode="false" sourceNode="P_24F10195" targetNode="P_51F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_106F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10195_I" deadCode="false" sourceNode="P_24F10195" targetNode="P_52F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_107F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10195_O" deadCode="false" sourceNode="P_24F10195" targetNode="P_53F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_107F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10195_I" deadCode="false" sourceNode="P_24F10195" targetNode="P_54F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_108F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10195_O" deadCode="false" sourceNode="P_24F10195" targetNode="P_55F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_108F10195"/>
	</edges>
	<edges id="P_24F10195P_25F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10195" targetNode="P_25F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10195_I" deadCode="false" sourceNode="P_26F10195" targetNode="P_56F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_110F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10195_O" deadCode="false" sourceNode="P_26F10195" targetNode="P_57F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_110F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10195_I" deadCode="false" sourceNode="P_26F10195" targetNode="P_12F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_112F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10195_O" deadCode="false" sourceNode="P_26F10195" targetNode="P_13F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_112F10195"/>
	</edges>
	<edges id="P_26F10195P_27F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10195" targetNode="P_27F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10195_I" deadCode="false" sourceNode="P_28F10195" targetNode="P_12F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_115F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10195_O" deadCode="false" sourceNode="P_28F10195" targetNode="P_13F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_115F10195"/>
	</edges>
	<edges id="P_28F10195P_29F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10195" targetNode="P_29F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10195_I" deadCode="false" sourceNode="P_30F10195" targetNode="P_26F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_117F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10195_O" deadCode="false" sourceNode="P_30F10195" targetNode="P_27F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_117F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10195_I" deadCode="false" sourceNode="P_30F10195" targetNode="P_32F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_119F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10195_O" deadCode="false" sourceNode="P_30F10195" targetNode="P_33F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_119F10195"/>
	</edges>
	<edges id="P_30F10195P_31F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10195" targetNode="P_31F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10195_I" deadCode="false" sourceNode="P_32F10195" targetNode="P_12F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_122F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10195_O" deadCode="false" sourceNode="P_32F10195" targetNode="P_13F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_122F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10195_I" deadCode="false" sourceNode="P_32F10195" targetNode="P_50F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_124F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10195_O" deadCode="false" sourceNode="P_32F10195" targetNode="P_51F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_124F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10195_I" deadCode="false" sourceNode="P_32F10195" targetNode="P_52F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_125F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10195_O" deadCode="false" sourceNode="P_32F10195" targetNode="P_53F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_125F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10195_I" deadCode="false" sourceNode="P_32F10195" targetNode="P_54F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_126F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10195_O" deadCode="false" sourceNode="P_32F10195" targetNode="P_55F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_126F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10195_I" deadCode="false" sourceNode="P_32F10195" targetNode="P_28F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_128F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10195_O" deadCode="false" sourceNode="P_32F10195" targetNode="P_29F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_128F10195"/>
	</edges>
	<edges id="P_32F10195P_33F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10195" targetNode="P_33F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10195_I" deadCode="false" sourceNode="P_58F10195" targetNode="P_45F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_132F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10195_O" deadCode="false" sourceNode="P_58F10195" targetNode="P_46F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_132F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10195_I" deadCode="false" sourceNode="P_58F10195" targetNode="P_47F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_133F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10195_O" deadCode="false" sourceNode="P_58F10195" targetNode="P_48F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_133F10195"/>
	</edges>
	<edges id="P_58F10195P_59F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10195" targetNode="P_59F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10195_I" deadCode="false" sourceNode="P_34F10195" targetNode="P_45F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_136F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10195_O" deadCode="false" sourceNode="P_34F10195" targetNode="P_46F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_136F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10195_I" deadCode="false" sourceNode="P_34F10195" targetNode="P_47F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_137F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10195_O" deadCode="false" sourceNode="P_34F10195" targetNode="P_48F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_137F10195"/>
	</edges>
	<edges id="P_34F10195P_35F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10195" targetNode="P_35F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10195_I" deadCode="false" sourceNode="P_36F10195" targetNode="P_58F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_140F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10195_O" deadCode="false" sourceNode="P_36F10195" targetNode="P_59F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_140F10195"/>
	</edges>
	<edges id="P_36F10195P_37F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10195" targetNode="P_37F10195"/>
	<edges id="P_38F10195P_39F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10195" targetNode="P_39F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10195_I" deadCode="false" sourceNode="P_40F10195" targetNode="P_36F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_145F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10195_O" deadCode="false" sourceNode="P_40F10195" targetNode="P_37F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_145F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10195_I" deadCode="false" sourceNode="P_40F10195" targetNode="P_42F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_147F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10195_O" deadCode="false" sourceNode="P_40F10195" targetNode="P_43F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_147F10195"/>
	</edges>
	<edges id="P_40F10195P_41F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10195" targetNode="P_41F10195"/>
	<edges id="P_42F10195P_43F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10195" targetNode="P_43F10195"/>
	<edges id="P_50F10195P_51F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10195" targetNode="P_51F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_388F10195_I" deadCode="true" sourceNode="P_60F10195" targetNode="P_61F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_388F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_388F10195_O" deadCode="true" sourceNode="P_60F10195" targetNode="P_62F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_388F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_391F10195_I" deadCode="true" sourceNode="P_60F10195" targetNode="P_61F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_391F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_391F10195_O" deadCode="true" sourceNode="P_60F10195" targetNode="P_62F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_391F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_394F10195_I" deadCode="true" sourceNode="P_60F10195" targetNode="P_61F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_394F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_394F10195_O" deadCode="true" sourceNode="P_60F10195" targetNode="P_62F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_394F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_398F10195_I" deadCode="true" sourceNode="P_60F10195" targetNode="P_61F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_398F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_398F10195_O" deadCode="true" sourceNode="P_60F10195" targetNode="P_62F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_398F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_402F10195_I" deadCode="true" sourceNode="P_60F10195" targetNode="P_61F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_402F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_402F10195_O" deadCode="true" sourceNode="P_60F10195" targetNode="P_62F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_402F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_406F10195_I" deadCode="true" sourceNode="P_60F10195" targetNode="P_61F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_406F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_406F10195_O" deadCode="true" sourceNode="P_60F10195" targetNode="P_62F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_406F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_410F10195_I" deadCode="true" sourceNode="P_60F10195" targetNode="P_61F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_410F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_410F10195_O" deadCode="true" sourceNode="P_60F10195" targetNode="P_62F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_410F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_414F10195_I" deadCode="true" sourceNode="P_60F10195" targetNode="P_61F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_414F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_414F10195_O" deadCode="true" sourceNode="P_60F10195" targetNode="P_62F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_414F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_418F10195_I" deadCode="true" sourceNode="P_60F10195" targetNode="P_61F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_418F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_418F10195_O" deadCode="true" sourceNode="P_60F10195" targetNode="P_62F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_418F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_422F10195_I" deadCode="false" sourceNode="P_52F10195" targetNode="P_64F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_422F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_422F10195_O" deadCode="false" sourceNode="P_52F10195" targetNode="P_65F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_422F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_425F10195_I" deadCode="false" sourceNode="P_52F10195" targetNode="P_64F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_425F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_425F10195_O" deadCode="false" sourceNode="P_52F10195" targetNode="P_65F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_425F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_428F10195_I" deadCode="false" sourceNode="P_52F10195" targetNode="P_64F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_428F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_428F10195_O" deadCode="false" sourceNode="P_52F10195" targetNode="P_65F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_428F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_432F10195_I" deadCode="false" sourceNode="P_52F10195" targetNode="P_64F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_432F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_432F10195_O" deadCode="false" sourceNode="P_52F10195" targetNode="P_65F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_432F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_436F10195_I" deadCode="false" sourceNode="P_52F10195" targetNode="P_64F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_436F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_436F10195_O" deadCode="false" sourceNode="P_52F10195" targetNode="P_65F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_436F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_440F10195_I" deadCode="false" sourceNode="P_52F10195" targetNode="P_64F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_440F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_440F10195_O" deadCode="false" sourceNode="P_52F10195" targetNode="P_65F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_440F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_444F10195_I" deadCode="false" sourceNode="P_52F10195" targetNode="P_64F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_444F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_444F10195_O" deadCode="false" sourceNode="P_52F10195" targetNode="P_65F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_444F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_448F10195_I" deadCode="false" sourceNode="P_52F10195" targetNode="P_64F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_448F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_448F10195_O" deadCode="false" sourceNode="P_52F10195" targetNode="P_65F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_448F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_452F10195_I" deadCode="false" sourceNode="P_52F10195" targetNode="P_64F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_452F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_452F10195_O" deadCode="false" sourceNode="P_52F10195" targetNode="P_65F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_452F10195"/>
	</edges>
	<edges id="P_52F10195P_53F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10195" targetNode="P_53F10195"/>
	<edges id="P_45F10195P_46F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10195" targetNode="P_46F10195"/>
	<edges id="P_47F10195P_48F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10195" targetNode="P_48F10195"/>
	<edges id="P_54F10195P_55F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10195" targetNode="P_55F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_461F10195_I" deadCode="false" sourceNode="P_10F10195" targetNode="P_66F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_461F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_461F10195_O" deadCode="false" sourceNode="P_10F10195" targetNode="P_67F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_461F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_463F10195_I" deadCode="false" sourceNode="P_10F10195" targetNode="P_68F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_463F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_463F10195_O" deadCode="false" sourceNode="P_10F10195" targetNode="P_69F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_463F10195"/>
	</edges>
	<edges id="P_10F10195P_11F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10195" targetNode="P_11F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_468F10195_I" deadCode="false" sourceNode="P_66F10195" targetNode="P_61F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_468F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_468F10195_O" deadCode="false" sourceNode="P_66F10195" targetNode="P_62F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_468F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_473F10195_I" deadCode="false" sourceNode="P_66F10195" targetNode="P_61F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_473F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_473F10195_O" deadCode="false" sourceNode="P_66F10195" targetNode="P_62F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_473F10195"/>
	</edges>
	<edges id="P_66F10195P_67F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10195" targetNode="P_67F10195"/>
	<edges id="P_68F10195P_69F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10195" targetNode="P_69F10195"/>
	<edges id="P_61F10195P_62F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10195" targetNode="P_62F10195"/>
	<edges xsi:type="cbl:PerformEdge" id="S_502F10195_I" deadCode="false" sourceNode="P_64F10195" targetNode="P_72F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_502F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_502F10195_O" deadCode="false" sourceNode="P_64F10195" targetNode="P_73F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_502F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_503F10195_I" deadCode="false" sourceNode="P_64F10195" targetNode="P_74F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_503F10195"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_503F10195_O" deadCode="false" sourceNode="P_64F10195" targetNode="P_75F10195">
		<representations href="../../../cobol/LDBS3020.cbl.cobModel#S_503F10195"/>
	</edges>
	<edges id="P_64F10195P_65F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10195" targetNode="P_65F10195"/>
	<edges id="P_72F10195P_73F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10195" targetNode="P_73F10195"/>
	<edges id="P_74F10195P_75F10195" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10195" targetNode="P_75F10195"/>
	<edges xsi:type="cbl:DataEdge" id="S_67F10195_POS1" deadCode="false" targetNode="P_14F10195" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10195"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_67F10195_POS2" deadCode="false" targetNode="P_14F10195" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10195"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_75F10195_POS1" deadCode="false" targetNode="P_16F10195" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_75F10195"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_75F10195_POS2" deadCode="false" targetNode="P_16F10195" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_75F10195"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_78F10195_POS1" deadCode="false" targetNode="P_18F10195" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10195"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_78F10195_POS2" deadCode="false" targetNode="P_18F10195" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10195"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_85F10195_POS1" deadCode="false" targetNode="P_22F10195" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_85F10195"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_85F10195_POS2" deadCode="false" targetNode="P_22F10195" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_85F10195"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_103F10195_POS1" deadCode="false" targetNode="P_24F10195" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_103F10195"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_103F10195_POS2" deadCode="false" targetNode="P_24F10195" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_103F10195"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10195_POS1" deadCode="false" targetNode="P_26F10195" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10195"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10195_POS2" deadCode="false" targetNode="P_26F10195" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10195"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_114F10195_POS1" deadCode="false" targetNode="P_28F10195" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10195"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_114F10195_POS2" deadCode="false" targetNode="P_28F10195" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10195"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_121F10195_POS1" deadCode="false" targetNode="P_32F10195" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_121F10195"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_121F10195_POS2" deadCode="false" targetNode="P_32F10195" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_121F10195"></representations>
	</edges>
</Package>

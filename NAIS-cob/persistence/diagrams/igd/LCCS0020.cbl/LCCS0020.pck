<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LCCS0020" cbl:id="LCCS0020" xsi:id="LCCS0020" packageRef="LCCS0020.igd#LCCS0020" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LCCS0020_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10124" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LCCS0020.cbl.cobModel#SC_1F10124"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10124" deadCode="false" name="PROGRAM_LCCS0020_FIRST_SENTENCES">
				<representations href="../../../cobol/LCCS0020.cbl.cobModel#P_1F10124"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10124" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LCCS0020.cbl.cobModel#P_2F10124"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10124" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LCCS0020.cbl.cobModel#P_3F10124"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10124" deadCode="false" name="S0005-INITIALIZE">
				<representations href="../../../cobol/LCCS0020.cbl.cobModel#P_8F10124"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10124" deadCode="false" name="EX-S0005">
				<representations href="../../../cobol/LCCS0020.cbl.cobModel#P_9F10124"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10124" deadCode="false" name="S0010-CTRL-DATI-INPUT">
				<representations href="../../../cobol/LCCS0020.cbl.cobModel#P_10F10124"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10124" deadCode="false" name="EX-S0010">
				<representations href="../../../cobol/LCCS0020.cbl.cobModel#P_11F10124"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10124" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LCCS0020.cbl.cobModel#P_4F10124"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10124" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LCCS0020.cbl.cobModel#P_5F10124"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10124" deadCode="false" name="S1100-PREPARA-LDBS0260">
				<representations href="../../../cobol/LCCS0020.cbl.cobModel#P_12F10124"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10124" deadCode="false" name="EX-S1100">
				<representations href="../../../cobol/LCCS0020.cbl.cobModel#P_13F10124"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10124" deadCode="false" name="S1200-CALL-LDBS0260">
				<representations href="../../../cobol/LCCS0020.cbl.cobModel#P_14F10124"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10124" deadCode="false" name="EX-S1200">
				<representations href="../../../cobol/LCCS0020.cbl.cobModel#P_15F10124"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10124" deadCode="false" name="S3000-CODIFICA-MOVI">
				<representations href="../../../cobol/LCCS0020.cbl.cobModel#P_16F10124"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10124" deadCode="false" name="S3000-EX">
				<representations href="../../../cobol/LCCS0020.cbl.cobModel#P_17F10124"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10124" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LCCS0020.cbl.cobModel#P_6F10124"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10124" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LCCS0020.cbl.cobModel#P_7F10124"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS0260" name="LDBS0260">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10143"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IWFS0050" name="IWFS0050">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10118"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10124P_1F10124" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10124" targetNode="P_1F10124"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10124_I" deadCode="false" sourceNode="P_1F10124" targetNode="P_2F10124">
		<representations href="../../../cobol/LCCS0020.cbl.cobModel#S_1F10124"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10124_O" deadCode="false" sourceNode="P_1F10124" targetNode="P_3F10124">
		<representations href="../../../cobol/LCCS0020.cbl.cobModel#S_1F10124"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10124_I" deadCode="false" sourceNode="P_1F10124" targetNode="P_4F10124">
		<representations href="../../../cobol/LCCS0020.cbl.cobModel#S_3F10124"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10124_O" deadCode="false" sourceNode="P_1F10124" targetNode="P_5F10124">
		<representations href="../../../cobol/LCCS0020.cbl.cobModel#S_3F10124"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10124_I" deadCode="false" sourceNode="P_1F10124" targetNode="P_6F10124">
		<representations href="../../../cobol/LCCS0020.cbl.cobModel#S_4F10124"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10124_O" deadCode="false" sourceNode="P_1F10124" targetNode="P_7F10124">
		<representations href="../../../cobol/LCCS0020.cbl.cobModel#S_4F10124"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10124_I" deadCode="false" sourceNode="P_2F10124" targetNode="P_8F10124">
		<representations href="../../../cobol/LCCS0020.cbl.cobModel#S_5F10124"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10124_O" deadCode="false" sourceNode="P_2F10124" targetNode="P_9F10124">
		<representations href="../../../cobol/LCCS0020.cbl.cobModel#S_5F10124"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10124_I" deadCode="false" sourceNode="P_2F10124" targetNode="P_10F10124">
		<representations href="../../../cobol/LCCS0020.cbl.cobModel#S_6F10124"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10124_O" deadCode="false" sourceNode="P_2F10124" targetNode="P_11F10124">
		<representations href="../../../cobol/LCCS0020.cbl.cobModel#S_6F10124"/>
	</edges>
	<edges id="P_2F10124P_3F10124" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10124" targetNode="P_3F10124"/>
	<edges id="P_8F10124P_9F10124" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10124" targetNode="P_9F10124"/>
	<edges id="P_10F10124P_11F10124" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10124" targetNode="P_11F10124"/>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10124_I" deadCode="false" sourceNode="P_4F10124" targetNode="P_12F10124">
		<representations href="../../../cobol/LCCS0020.cbl.cobModel#S_22F10124"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10124_O" deadCode="false" sourceNode="P_4F10124" targetNode="P_13F10124">
		<representations href="../../../cobol/LCCS0020.cbl.cobModel#S_22F10124"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10124_I" deadCode="false" sourceNode="P_4F10124" targetNode="P_14F10124">
		<representations href="../../../cobol/LCCS0020.cbl.cobModel#S_23F10124"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10124_O" deadCode="false" sourceNode="P_4F10124" targetNode="P_15F10124">
		<representations href="../../../cobol/LCCS0020.cbl.cobModel#S_23F10124"/>
	</edges>
	<edges id="P_4F10124P_5F10124" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10124" targetNode="P_5F10124"/>
	<edges id="P_12F10124P_13F10124" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10124" targetNode="P_13F10124"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10124_I" deadCode="false" sourceNode="P_14F10124" targetNode="P_16F10124">
		<representations href="../../../cobol/LCCS0020.cbl.cobModel#S_45F10124"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10124_O" deadCode="false" sourceNode="P_14F10124" targetNode="P_17F10124">
		<representations href="../../../cobol/LCCS0020.cbl.cobModel#S_45F10124"/>
	</edges>
	<edges id="P_14F10124P_15F10124" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10124" targetNode="P_15F10124"/>
	<edges id="P_16F10124P_17F10124" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10124" targetNode="P_17F10124"/>
	<edges xsi:type="cbl:CallEdge" id="S_35F10124" deadCode="false" name="Dynamic LDBS0260" sourceNode="P_14F10124" targetNode="LDBS0260">
		<representations href="../../../cobol/../importantStmts.cobModel#S_35F10124"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_56F10124" deadCode="false" name="Dynamic IWFS0050" sourceNode="P_16F10124" targetNode="IWFS0050">
		<representations href="../../../cobol/../importantStmts.cobModel#S_56F10124"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS2200" cbl:id="LDBS2200" xsi:id="LDBS2200" packageRef="LDBS2200.igd#LDBS2200" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS2200_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10170" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS2200.cbl.cobModel#SC_1F10170"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10170" deadCode="false" name="PROGRAM_LDBS2200_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_1F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10170" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_2F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10170" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_3F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10170" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_12F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10170" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_13F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10170" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_4F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10170" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_5F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10170" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_6F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10170" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_7F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10170" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_8F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10170" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_9F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10170" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_44F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10170" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_49F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10170" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_14F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10170" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_15F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10170" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_16F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10170" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_17F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10170" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_18F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10170" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_19F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10170" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_20F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10170" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_21F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10170" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_22F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10170" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_23F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10170" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_50F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10170" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_51F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10170" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_24F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10170" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_25F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10170" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_26F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10170" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_27F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10170" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_28F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10170" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_29F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10170" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_30F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10170" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_31F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10170" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_32F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10170" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_33F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10170" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_52F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10170" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_53F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10170" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_34F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10170" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_35F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10170" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_36F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10170" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_37F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10170" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_38F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10170" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_39F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10170" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_40F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10170" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_41F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10170" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_42F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10170" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_43F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10170" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_54F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10170" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_55F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10170" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_60F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10170" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_63F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10170" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_56F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10170" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_57F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10170" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_45F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10170" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_46F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10170" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_47F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10170" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_48F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10170" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_58F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10170" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_59F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10170" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_10F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10170" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_11F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10170" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_66F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10170" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_67F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10170" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_68F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10170" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_69F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10170" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_61F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10170" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_62F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10170" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_70F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10170" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_71F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10170" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_64F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10170" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_65F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10170" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_72F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10170" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_73F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10170" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_74F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10170" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_75F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10170" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_76F10170"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10170" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS2200.cbl.cobModel#P_77F10170"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_MOVI" name="MOVI">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_MOVI"/>
		</children>
	</packageNode>
	<edges id="SC_1F10170P_1F10170" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10170" targetNode="P_1F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10170_I" deadCode="false" sourceNode="P_1F10170" targetNode="P_2F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_1F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10170_O" deadCode="false" sourceNode="P_1F10170" targetNode="P_3F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_1F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10170_I" deadCode="false" sourceNode="P_1F10170" targetNode="P_4F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_5F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10170_O" deadCode="false" sourceNode="P_1F10170" targetNode="P_5F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_5F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10170_I" deadCode="false" sourceNode="P_1F10170" targetNode="P_6F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_9F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10170_O" deadCode="false" sourceNode="P_1F10170" targetNode="P_7F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_9F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10170_I" deadCode="false" sourceNode="P_1F10170" targetNode="P_8F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_13F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10170_O" deadCode="false" sourceNode="P_1F10170" targetNode="P_9F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_13F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10170_I" deadCode="false" sourceNode="P_2F10170" targetNode="P_10F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_22F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10170_O" deadCode="false" sourceNode="P_2F10170" targetNode="P_11F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_22F10170"/>
	</edges>
	<edges id="P_2F10170P_3F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10170" targetNode="P_3F10170"/>
	<edges id="P_12F10170P_13F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10170" targetNode="P_13F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10170_I" deadCode="false" sourceNode="P_4F10170" targetNode="P_14F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_35F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10170_O" deadCode="false" sourceNode="P_4F10170" targetNode="P_15F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_35F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10170_I" deadCode="false" sourceNode="P_4F10170" targetNode="P_16F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_36F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10170_O" deadCode="false" sourceNode="P_4F10170" targetNode="P_17F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_36F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10170_I" deadCode="false" sourceNode="P_4F10170" targetNode="P_18F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_37F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10170_O" deadCode="false" sourceNode="P_4F10170" targetNode="P_19F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_37F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10170_I" deadCode="false" sourceNode="P_4F10170" targetNode="P_20F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_38F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10170_O" deadCode="false" sourceNode="P_4F10170" targetNode="P_21F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_38F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10170_I" deadCode="false" sourceNode="P_4F10170" targetNode="P_22F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_39F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10170_O" deadCode="false" sourceNode="P_4F10170" targetNode="P_23F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_39F10170"/>
	</edges>
	<edges id="P_4F10170P_5F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10170" targetNode="P_5F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10170_I" deadCode="false" sourceNode="P_6F10170" targetNode="P_24F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_43F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10170_O" deadCode="false" sourceNode="P_6F10170" targetNode="P_25F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_43F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10170_I" deadCode="false" sourceNode="P_6F10170" targetNode="P_26F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_44F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10170_O" deadCode="false" sourceNode="P_6F10170" targetNode="P_27F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_44F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10170_I" deadCode="false" sourceNode="P_6F10170" targetNode="P_28F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_45F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10170_O" deadCode="false" sourceNode="P_6F10170" targetNode="P_29F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_45F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10170_I" deadCode="false" sourceNode="P_6F10170" targetNode="P_30F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_46F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10170_O" deadCode="false" sourceNode="P_6F10170" targetNode="P_31F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_46F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10170_I" deadCode="false" sourceNode="P_6F10170" targetNode="P_32F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_47F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10170_O" deadCode="false" sourceNode="P_6F10170" targetNode="P_33F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_47F10170"/>
	</edges>
	<edges id="P_6F10170P_7F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10170" targetNode="P_7F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10170_I" deadCode="false" sourceNode="P_8F10170" targetNode="P_34F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_51F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10170_O" deadCode="false" sourceNode="P_8F10170" targetNode="P_35F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_51F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10170_I" deadCode="false" sourceNode="P_8F10170" targetNode="P_36F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_52F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10170_O" deadCode="false" sourceNode="P_8F10170" targetNode="P_37F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_52F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10170_I" deadCode="false" sourceNode="P_8F10170" targetNode="P_38F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_53F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10170_O" deadCode="false" sourceNode="P_8F10170" targetNode="P_39F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_53F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10170_I" deadCode="false" sourceNode="P_8F10170" targetNode="P_40F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_54F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10170_O" deadCode="false" sourceNode="P_8F10170" targetNode="P_41F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_54F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10170_I" deadCode="false" sourceNode="P_8F10170" targetNode="P_42F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_55F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10170_O" deadCode="false" sourceNode="P_8F10170" targetNode="P_43F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_55F10170"/>
	</edges>
	<edges id="P_8F10170P_9F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10170" targetNode="P_9F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10170_I" deadCode="false" sourceNode="P_44F10170" targetNode="P_45F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_58F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10170_O" deadCode="false" sourceNode="P_44F10170" targetNode="P_46F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_58F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10170_I" deadCode="false" sourceNode="P_44F10170" targetNode="P_47F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_59F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10170_O" deadCode="false" sourceNode="P_44F10170" targetNode="P_48F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_59F10170"/>
	</edges>
	<edges id="P_44F10170P_49F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10170" targetNode="P_49F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10170_I" deadCode="false" sourceNode="P_14F10170" targetNode="P_45F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_62F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10170_O" deadCode="false" sourceNode="P_14F10170" targetNode="P_46F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_62F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10170_I" deadCode="false" sourceNode="P_14F10170" targetNode="P_47F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_63F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10170_O" deadCode="false" sourceNode="P_14F10170" targetNode="P_48F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_63F10170"/>
	</edges>
	<edges id="P_14F10170P_15F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10170" targetNode="P_15F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10170_I" deadCode="false" sourceNode="P_16F10170" targetNode="P_44F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_66F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10170_O" deadCode="false" sourceNode="P_16F10170" targetNode="P_49F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_66F10170"/>
	</edges>
	<edges id="P_16F10170P_17F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10170" targetNode="P_17F10170"/>
	<edges id="P_18F10170P_19F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10170" targetNode="P_19F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10170_I" deadCode="false" sourceNode="P_20F10170" targetNode="P_16F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_71F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10170_O" deadCode="false" sourceNode="P_20F10170" targetNode="P_17F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_71F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10170_I" deadCode="false" sourceNode="P_20F10170" targetNode="P_22F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_73F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10170_O" deadCode="false" sourceNode="P_20F10170" targetNode="P_23F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_73F10170"/>
	</edges>
	<edges id="P_20F10170P_21F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10170" targetNode="P_21F10170"/>
	<edges id="P_22F10170P_23F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10170" targetNode="P_23F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10170_I" deadCode="false" sourceNode="P_50F10170" targetNode="P_45F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_77F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10170_O" deadCode="false" sourceNode="P_50F10170" targetNode="P_46F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_77F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10170_I" deadCode="false" sourceNode="P_50F10170" targetNode="P_47F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_78F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10170_O" deadCode="false" sourceNode="P_50F10170" targetNode="P_48F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_78F10170"/>
	</edges>
	<edges id="P_50F10170P_51F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10170" targetNode="P_51F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10170_I" deadCode="false" sourceNode="P_24F10170" targetNode="P_45F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_81F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10170_O" deadCode="false" sourceNode="P_24F10170" targetNode="P_46F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_81F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10170_I" deadCode="false" sourceNode="P_24F10170" targetNode="P_47F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_82F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10170_O" deadCode="false" sourceNode="P_24F10170" targetNode="P_48F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_82F10170"/>
	</edges>
	<edges id="P_24F10170P_25F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10170" targetNode="P_25F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10170_I" deadCode="false" sourceNode="P_26F10170" targetNode="P_50F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_85F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10170_O" deadCode="false" sourceNode="P_26F10170" targetNode="P_51F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_85F10170"/>
	</edges>
	<edges id="P_26F10170P_27F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10170" targetNode="P_27F10170"/>
	<edges id="P_28F10170P_29F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10170" targetNode="P_29F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10170_I" deadCode="false" sourceNode="P_30F10170" targetNode="P_26F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_90F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10170_O" deadCode="false" sourceNode="P_30F10170" targetNode="P_27F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_90F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10170_I" deadCode="false" sourceNode="P_30F10170" targetNode="P_32F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_92F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10170_O" deadCode="false" sourceNode="P_30F10170" targetNode="P_33F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_92F10170"/>
	</edges>
	<edges id="P_30F10170P_31F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10170" targetNode="P_31F10170"/>
	<edges id="P_32F10170P_33F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10170" targetNode="P_33F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10170_I" deadCode="false" sourceNode="P_52F10170" targetNode="P_45F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_96F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10170_O" deadCode="false" sourceNode="P_52F10170" targetNode="P_46F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_96F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10170_I" deadCode="false" sourceNode="P_52F10170" targetNode="P_47F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_97F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10170_O" deadCode="false" sourceNode="P_52F10170" targetNode="P_48F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_97F10170"/>
	</edges>
	<edges id="P_52F10170P_53F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10170" targetNode="P_53F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10170_I" deadCode="false" sourceNode="P_34F10170" targetNode="P_45F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_101F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10170_O" deadCode="false" sourceNode="P_34F10170" targetNode="P_46F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_101F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10170_I" deadCode="false" sourceNode="P_34F10170" targetNode="P_47F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_102F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10170_O" deadCode="false" sourceNode="P_34F10170" targetNode="P_48F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_102F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10170_I" deadCode="false" sourceNode="P_34F10170" targetNode="P_12F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_104F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10170_O" deadCode="false" sourceNode="P_34F10170" targetNode="P_13F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_104F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10170_I" deadCode="false" sourceNode="P_34F10170" targetNode="P_54F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_106F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10170_O" deadCode="false" sourceNode="P_34F10170" targetNode="P_55F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_106F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10170_I" deadCode="false" sourceNode="P_34F10170" targetNode="P_56F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_107F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10170_O" deadCode="false" sourceNode="P_34F10170" targetNode="P_57F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_107F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10170_I" deadCode="false" sourceNode="P_34F10170" targetNode="P_58F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_108F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10170_O" deadCode="false" sourceNode="P_34F10170" targetNode="P_59F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_108F10170"/>
	</edges>
	<edges id="P_34F10170P_35F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10170" targetNode="P_35F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10170_I" deadCode="false" sourceNode="P_36F10170" targetNode="P_52F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_110F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10170_O" deadCode="false" sourceNode="P_36F10170" targetNode="P_53F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_110F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10170_I" deadCode="false" sourceNode="P_36F10170" targetNode="P_12F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_112F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10170_O" deadCode="false" sourceNode="P_36F10170" targetNode="P_13F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_112F10170"/>
	</edges>
	<edges id="P_36F10170P_37F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10170" targetNode="P_37F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10170_I" deadCode="false" sourceNode="P_38F10170" targetNode="P_12F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_115F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10170_O" deadCode="false" sourceNode="P_38F10170" targetNode="P_13F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_115F10170"/>
	</edges>
	<edges id="P_38F10170P_39F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10170" targetNode="P_39F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10170_I" deadCode="false" sourceNode="P_40F10170" targetNode="P_36F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_117F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10170_O" deadCode="false" sourceNode="P_40F10170" targetNode="P_37F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_117F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10170_I" deadCode="false" sourceNode="P_40F10170" targetNode="P_42F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_119F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10170_O" deadCode="false" sourceNode="P_40F10170" targetNode="P_43F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_119F10170"/>
	</edges>
	<edges id="P_40F10170P_41F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10170" targetNode="P_41F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10170_I" deadCode="false" sourceNode="P_42F10170" targetNode="P_12F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_122F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10170_O" deadCode="false" sourceNode="P_42F10170" targetNode="P_13F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_122F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10170_I" deadCode="false" sourceNode="P_42F10170" targetNode="P_54F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_124F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10170_O" deadCode="false" sourceNode="P_42F10170" targetNode="P_55F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_124F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10170_I" deadCode="false" sourceNode="P_42F10170" targetNode="P_56F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_125F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10170_O" deadCode="false" sourceNode="P_42F10170" targetNode="P_57F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_125F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10170_I" deadCode="false" sourceNode="P_42F10170" targetNode="P_58F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_126F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10170_O" deadCode="false" sourceNode="P_42F10170" targetNode="P_59F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_126F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10170_I" deadCode="false" sourceNode="P_42F10170" targetNode="P_38F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_128F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10170_O" deadCode="false" sourceNode="P_42F10170" targetNode="P_39F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_128F10170"/>
	</edges>
	<edges id="P_42F10170P_43F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10170" targetNode="P_43F10170"/>
	<edges id="P_54F10170P_55F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10170" targetNode="P_55F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_151F10170_I" deadCode="true" sourceNode="P_60F10170" targetNode="P_61F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_151F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_151F10170_O" deadCode="true" sourceNode="P_60F10170" targetNode="P_62F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_151F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_155F10170_I" deadCode="false" sourceNode="P_56F10170" targetNode="P_64F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_155F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_155F10170_O" deadCode="false" sourceNode="P_56F10170" targetNode="P_65F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_155F10170"/>
	</edges>
	<edges id="P_56F10170P_57F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10170" targetNode="P_57F10170"/>
	<edges id="P_45F10170P_46F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10170" targetNode="P_46F10170"/>
	<edges id="P_47F10170P_48F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10170" targetNode="P_48F10170"/>
	<edges id="P_58F10170P_59F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10170" targetNode="P_59F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10170_I" deadCode="false" sourceNode="P_10F10170" targetNode="P_66F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_164F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10170_O" deadCode="false" sourceNode="P_10F10170" targetNode="P_67F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_164F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10170_I" deadCode="false" sourceNode="P_10F10170" targetNode="P_68F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_166F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10170_O" deadCode="false" sourceNode="P_10F10170" targetNode="P_69F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_166F10170"/>
	</edges>
	<edges id="P_10F10170P_11F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10170" targetNode="P_11F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10170_I" deadCode="true" sourceNode="P_66F10170" targetNode="P_61F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_171F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10170_O" deadCode="true" sourceNode="P_66F10170" targetNode="P_62F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_171F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_176F10170_I" deadCode="true" sourceNode="P_66F10170" targetNode="P_61F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_176F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_176F10170_O" deadCode="true" sourceNode="P_66F10170" targetNode="P_62F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_176F10170"/>
	</edges>
	<edges id="P_66F10170P_67F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10170" targetNode="P_67F10170"/>
	<edges id="P_68F10170P_69F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10170" targetNode="P_69F10170"/>
	<edges id="P_61F10170P_62F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10170" targetNode="P_62F10170"/>
	<edges xsi:type="cbl:PerformEdge" id="S_205F10170_I" deadCode="false" sourceNode="P_64F10170" targetNode="P_72F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_205F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_205F10170_O" deadCode="false" sourceNode="P_64F10170" targetNode="P_73F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_205F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_206F10170_I" deadCode="false" sourceNode="P_64F10170" targetNode="P_74F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_206F10170"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_206F10170_O" deadCode="false" sourceNode="P_64F10170" targetNode="P_75F10170">
		<representations href="../../../cobol/LDBS2200.cbl.cobModel#S_206F10170"/>
	</edges>
	<edges id="P_64F10170P_65F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10170" targetNode="P_65F10170"/>
	<edges id="P_72F10170P_73F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10170" targetNode="P_73F10170"/>
	<edges id="P_74F10170P_75F10170" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10170" targetNode="P_75F10170"/>
	<edges xsi:type="cbl:DataEdge" id="S_103F10170_POS1" deadCode="false" targetNode="P_34F10170" sourceNode="DB2_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_103F10170"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10170_POS1" deadCode="false" targetNode="P_36F10170" sourceNode="DB2_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10170"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_114F10170_POS1" deadCode="false" targetNode="P_38F10170" sourceNode="DB2_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10170"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_121F10170_POS1" deadCode="false" targetNode="P_42F10170" sourceNode="DB2_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_121F10170"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS2310" cbl:id="LVVS2310" xsi:id="LVVS2310" packageRef="LVVS2310.igd#LVVS2310" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS2310_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10371" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS2310.cbl.cobModel#SC_1F10371"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10371" deadCode="false" name="PROGRAM_LVVS2310_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS2310.cbl.cobModel#P_1F10371"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10371" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS2310.cbl.cobModel#P_2F10371"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10371" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS2310.cbl.cobModel#P_3F10371"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10371" deadCode="false" name="S0005-CTRL-DATI-INPUT">
				<representations href="../../../cobol/LVVS2310.cbl.cobModel#P_8F10371"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10371" deadCode="false" name="EX-S0005">
				<representations href="../../../cobol/LVVS2310.cbl.cobModel#P_9F10371"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10371" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS2310.cbl.cobModel#P_4F10371"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10371" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS2310.cbl.cobModel#P_5F10371"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10371" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS2310.cbl.cobModel#P_10F10371"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10371" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS2310.cbl.cobModel#P_11F10371"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10371" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS2310.cbl.cobModel#P_12F10371"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10371" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS2310.cbl.cobModel#P_13F10371"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10371" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS2310.cbl.cobModel#P_6F10371"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10371" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS2310.cbl.cobModel#P_7F10371"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10371P_1F10371" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10371" targetNode="P_1F10371"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10371_I" deadCode="false" sourceNode="P_1F10371" targetNode="P_2F10371">
		<representations href="../../../cobol/LVVS2310.cbl.cobModel#S_1F10371"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10371_O" deadCode="false" sourceNode="P_1F10371" targetNode="P_3F10371">
		<representations href="../../../cobol/LVVS2310.cbl.cobModel#S_1F10371"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10371_I" deadCode="false" sourceNode="P_1F10371" targetNode="P_4F10371">
		<representations href="../../../cobol/LVVS2310.cbl.cobModel#S_3F10371"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10371_O" deadCode="false" sourceNode="P_1F10371" targetNode="P_5F10371">
		<representations href="../../../cobol/LVVS2310.cbl.cobModel#S_3F10371"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10371_I" deadCode="false" sourceNode="P_1F10371" targetNode="P_6F10371">
		<representations href="../../../cobol/LVVS2310.cbl.cobModel#S_4F10371"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10371_O" deadCode="false" sourceNode="P_1F10371" targetNode="P_7F10371">
		<representations href="../../../cobol/LVVS2310.cbl.cobModel#S_4F10371"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10371_I" deadCode="false" sourceNode="P_2F10371" targetNode="P_8F10371">
		<representations href="../../../cobol/LVVS2310.cbl.cobModel#S_10F10371"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10371_O" deadCode="false" sourceNode="P_2F10371" targetNode="P_9F10371">
		<representations href="../../../cobol/LVVS2310.cbl.cobModel#S_10F10371"/>
	</edges>
	<edges id="P_2F10371P_3F10371" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10371" targetNode="P_3F10371"/>
	<edges id="P_8F10371P_9F10371" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10371" targetNode="P_9F10371"/>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10371_I" deadCode="false" sourceNode="P_4F10371" targetNode="P_10F10371">
		<representations href="../../../cobol/LVVS2310.cbl.cobModel#S_22F10371"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10371_O" deadCode="false" sourceNode="P_4F10371" targetNode="P_11F10371">
		<representations href="../../../cobol/LVVS2310.cbl.cobModel#S_22F10371"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10371_I" deadCode="false" sourceNode="P_4F10371" targetNode="P_12F10371">
		<representations href="../../../cobol/LVVS2310.cbl.cobModel#S_24F10371"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10371_O" deadCode="false" sourceNode="P_4F10371" targetNode="P_13F10371">
		<representations href="../../../cobol/LVVS2310.cbl.cobModel#S_24F10371"/>
	</edges>
	<edges id="P_4F10371P_5F10371" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10371" targetNode="P_5F10371"/>
	<edges id="P_10F10371P_11F10371" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10371" targetNode="P_11F10371"/>
	<edges id="P_12F10371P_13F10371" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10371" targetNode="P_13F10371"/>
</Package>

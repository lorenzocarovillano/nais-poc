<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IDES0020" cbl:id="IDES0020" xsi:id="IDES0020" packageRef="IDES0020.igd#IDES0020" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IDES0020_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10098" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IDES0020.cbl.cobModel#SC_1F10098"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10098" deadCode="false" name="PROGRAM_IDES0020_FIRST_SENTENCES">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_1F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10098" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_2F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10098" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_3F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10098" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_6F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10098" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_7F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10098" deadCode="false" name="A300-ELABORA">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_4F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10098" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_5F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10098" deadCode="false" name="A310-SELECT">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_8F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10098" deadCode="false" name="A310-EX">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_9F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10098" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_12F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10098" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_13F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10098" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_14F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10098" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_15F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10098" deadCode="true" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_10F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10098" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_11F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10098" deadCode="true" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_20F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10098" deadCode="true" name="A001-EX">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_25F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10098" deadCode="true" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_21F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10098" deadCode="true" name="A020-EX">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_22F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10098" deadCode="true" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_23F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10098" deadCode="true" name="A050-EX">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_24F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10098" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_26F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10098" deadCode="true" name="Z700-EX">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_27F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10098" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_28F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10098" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_29F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10098" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_16F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10098" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_17F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10098" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_30F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10098" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_31F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10098" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_32F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10098" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_33F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10098" deadCode="false" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_18F10098"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10098" deadCode="false" name="Z801-EX">
				<representations href="../../../cobol/IDES0020.cbl.cobModel#P_19F10098"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_GRU_ARZ" name="GRU_ARZ">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_GRU_ARZ"/>
		</children>
	</packageNode>
	<edges id="SC_1F10098P_1F10098" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10098" targetNode="P_1F10098"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10098_I" deadCode="false" sourceNode="P_1F10098" targetNode="P_2F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_1F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10098_O" deadCode="false" sourceNode="P_1F10098" targetNode="P_3F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_1F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10098_I" deadCode="false" sourceNode="P_1F10098" targetNode="P_4F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_2F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10098_O" deadCode="false" sourceNode="P_1F10098" targetNode="P_5F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_2F10098"/>
	</edges>
	<edges id="P_2F10098P_3F10098" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10098" targetNode="P_3F10098"/>
	<edges id="P_6F10098P_7F10098" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10098" targetNode="P_7F10098"/>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10098_I" deadCode="false" sourceNode="P_4F10098" targetNode="P_8F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_21F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10098_O" deadCode="false" sourceNode="P_4F10098" targetNode="P_9F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_21F10098"/>
	</edges>
	<edges id="P_4F10098P_5F10098" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10098" targetNode="P_5F10098"/>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10098_I" deadCode="true" sourceNode="P_8F10098" targetNode="P_10F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_24F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10098_O" deadCode="true" sourceNode="P_8F10098" targetNode="P_11F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_24F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10098_I" deadCode="false" sourceNode="P_8F10098" targetNode="P_6F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_26F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10098_O" deadCode="false" sourceNode="P_8F10098" targetNode="P_7F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_26F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10098_I" deadCode="false" sourceNode="P_8F10098" targetNode="P_12F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_28F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10098_O" deadCode="false" sourceNode="P_8F10098" targetNode="P_13F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_28F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10098_I" deadCode="false" sourceNode="P_8F10098" targetNode="P_14F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_29F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10098_O" deadCode="false" sourceNode="P_8F10098" targetNode="P_15F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_29F10098"/>
	</edges>
	<edges id="P_8F10098P_9F10098" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10098" targetNode="P_9F10098"/>
	<edges id="P_12F10098P_13F10098" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10098" targetNode="P_13F10098"/>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10098_I" deadCode="false" sourceNode="P_14F10098" targetNode="P_16F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_34F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10098_O" deadCode="false" sourceNode="P_14F10098" targetNode="P_17F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_34F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10098_I" deadCode="false" sourceNode="P_14F10098" targetNode="P_16F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_37F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10098_O" deadCode="false" sourceNode="P_14F10098" targetNode="P_17F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_37F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10098_I" deadCode="false" sourceNode="P_14F10098" targetNode="P_18F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_40F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10098_O" deadCode="false" sourceNode="P_14F10098" targetNode="P_19F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_40F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10098_I" deadCode="false" sourceNode="P_14F10098" targetNode="P_18F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_43F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10098_O" deadCode="false" sourceNode="P_14F10098" targetNode="P_19F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_43F10098"/>
	</edges>
	<edges id="P_14F10098P_15F10098" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10098" targetNode="P_15F10098"/>
	<edges id="P_10F10098P_11F10098" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10098" targetNode="P_11F10098"/>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10098_I" deadCode="true" sourceNode="P_20F10098" targetNode="P_21F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_50F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10098_O" deadCode="true" sourceNode="P_20F10098" targetNode="P_22F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_50F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10098_I" deadCode="true" sourceNode="P_20F10098" targetNode="P_23F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_52F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10098_O" deadCode="true" sourceNode="P_20F10098" targetNode="P_24F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_52F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10098_I" deadCode="true" sourceNode="P_21F10098" targetNode="P_26F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_57F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10098_O" deadCode="true" sourceNode="P_21F10098" targetNode="P_27F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_57F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10098_I" deadCode="true" sourceNode="P_21F10098" targetNode="P_26F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_62F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10098_O" deadCode="true" sourceNode="P_21F10098" targetNode="P_27F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_62F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10098_I" deadCode="false" sourceNode="P_16F10098" targetNode="P_30F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_91F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10098_O" deadCode="false" sourceNode="P_16F10098" targetNode="P_31F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_91F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10098_I" deadCode="false" sourceNode="P_16F10098" targetNode="P_32F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_92F10098"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10098_O" deadCode="false" sourceNode="P_16F10098" targetNode="P_33F10098">
		<representations href="../../../cobol/IDES0020.cbl.cobModel#S_92F10098"/>
	</edges>
	<edges id="P_16F10098P_17F10098" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10098" targetNode="P_17F10098"/>
	<edges id="P_30F10098P_31F10098" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10098" targetNode="P_31F10098"/>
	<edges id="P_32F10098P_33F10098" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10098" targetNode="P_33F10098"/>
	<edges id="P_18F10098P_19F10098" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10098" targetNode="P_19F10098"/>
	<edges xsi:type="cbl:DataEdge" id="S_25F10098_POS1" deadCode="false" targetNode="P_8F10098" sourceNode="DB2_GRU_ARZ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_25F10098"></representations>
	</edges>
</Package>

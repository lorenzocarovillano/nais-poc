<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS3250" cbl:id="LVVS3250" xsi:id="LVVS3250" packageRef="LVVS3250.igd#LVVS3250" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS3250_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10388" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS3250.cbl.cobModel#SC_1F10388"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10388" deadCode="false" name="PROGRAM_LVVS3250_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS3250.cbl.cobModel#P_1F10388"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10388" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS3250.cbl.cobModel#P_2F10388"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10388" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS3250.cbl.cobModel#P_3F10388"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10388" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS3250.cbl.cobModel#P_4F10388"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10388" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS3250.cbl.cobModel#P_5F10388"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10388" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS3250.cbl.cobModel#P_8F10388"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10388" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS3250.cbl.cobModel#P_9F10388"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10388" deadCode="false" name="S1300-CERCA-PARAM-MOVI">
				<representations href="../../../cobol/LVVS3250.cbl.cobModel#P_10F10388"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10388" deadCode="false" name="S1300-EX">
				<representations href="../../../cobol/LVVS3250.cbl.cobModel#P_11F10388"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10388" deadCode="false" name="S1310-LEGGI-PMO-CHIUSA">
				<representations href="../../../cobol/LVVS3250.cbl.cobModel#P_12F10388"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10388" deadCode="false" name="S1310-EX">
				<representations href="../../../cobol/LVVS3250.cbl.cobModel#P_13F10388"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10388" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS3250.cbl.cobModel#P_6F10388"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10388" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS3250.cbl.cobModel#P_7F10388"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSF970" name="LDBSF970">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10272"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10388P_1F10388" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10388" targetNode="P_1F10388"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10388_I" deadCode="false" sourceNode="P_1F10388" targetNode="P_2F10388">
		<representations href="../../../cobol/LVVS3250.cbl.cobModel#S_1F10388"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10388_O" deadCode="false" sourceNode="P_1F10388" targetNode="P_3F10388">
		<representations href="../../../cobol/LVVS3250.cbl.cobModel#S_1F10388"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10388_I" deadCode="false" sourceNode="P_1F10388" targetNode="P_4F10388">
		<representations href="../../../cobol/LVVS3250.cbl.cobModel#S_2F10388"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10388_O" deadCode="false" sourceNode="P_1F10388" targetNode="P_5F10388">
		<representations href="../../../cobol/LVVS3250.cbl.cobModel#S_2F10388"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10388_I" deadCode="false" sourceNode="P_1F10388" targetNode="P_6F10388">
		<representations href="../../../cobol/LVVS3250.cbl.cobModel#S_3F10388"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10388_O" deadCode="false" sourceNode="P_1F10388" targetNode="P_7F10388">
		<representations href="../../../cobol/LVVS3250.cbl.cobModel#S_3F10388"/>
	</edges>
	<edges id="P_2F10388P_3F10388" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10388" targetNode="P_3F10388"/>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10388_I" deadCode="false" sourceNode="P_4F10388" targetNode="P_8F10388">
		<representations href="../../../cobol/LVVS3250.cbl.cobModel#S_12F10388"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10388_O" deadCode="false" sourceNode="P_4F10388" targetNode="P_9F10388">
		<representations href="../../../cobol/LVVS3250.cbl.cobModel#S_12F10388"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10388_I" deadCode="false" sourceNode="P_4F10388" targetNode="P_10F10388">
		<representations href="../../../cobol/LVVS3250.cbl.cobModel#S_14F10388"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10388_O" deadCode="false" sourceNode="P_4F10388" targetNode="P_11F10388">
		<representations href="../../../cobol/LVVS3250.cbl.cobModel#S_14F10388"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10388_I" deadCode="false" sourceNode="P_4F10388" targetNode="P_12F10388">
		<representations href="../../../cobol/LVVS3250.cbl.cobModel#S_17F10388"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10388_O" deadCode="false" sourceNode="P_4F10388" targetNode="P_13F10388">
		<representations href="../../../cobol/LVVS3250.cbl.cobModel#S_17F10388"/>
	</edges>
	<edges id="P_4F10388P_5F10388" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10388" targetNode="P_5F10388"/>
	<edges id="P_8F10388P_9F10388" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10388" targetNode="P_9F10388"/>
	<edges id="P_10F10388P_11F10388" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10388" targetNode="P_11F10388"/>
	<edges id="P_12F10388P_13F10388" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10388" targetNode="P_13F10388"/>
	<edges xsi:type="cbl:CallEdge" id="S_39F10388" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_12F10388" targetNode="LDBSF970">
		<representations href="../../../cobol/../importantStmts.cobModel#S_39F10388"></representations>
	</edges>
</Package>

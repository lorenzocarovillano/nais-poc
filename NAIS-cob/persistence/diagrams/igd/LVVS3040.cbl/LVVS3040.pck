<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS3040" cbl:id="LVVS3040" xsi:id="LVVS3040" packageRef="LVVS3040.igd#LVVS3040" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS3040_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10382" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS3040.cbl.cobModel#SC_1F10382"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10382" deadCode="false" name="PROGRAM_LVVS3040_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS3040.cbl.cobModel#P_1F10382"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10382" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS3040.cbl.cobModel#P_2F10382"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10382" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS3040.cbl.cobModel#P_3F10382"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10382" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS3040.cbl.cobModel#P_4F10382"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10382" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS3040.cbl.cobModel#P_5F10382"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10382" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS3040.cbl.cobModel#P_8F10382"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10382" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS3040.cbl.cobModel#P_9F10382"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10382" deadCode="false" name="A000-PREPARA-CALL-LDBSF110">
				<representations href="../../../cobol/LVVS3040.cbl.cobModel#P_10F10382"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10382" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LVVS3040.cbl.cobModel#P_11F10382"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10382" deadCode="false" name="A010-CALL-LDBSF110">
				<representations href="../../../cobol/LVVS3040.cbl.cobModel#P_12F10382"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10382" deadCode="false" name="A010-EX">
				<representations href="../../../cobol/LVVS3040.cbl.cobModel#P_13F10382"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10382" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS3040.cbl.cobModel#P_6F10382"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10382" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS3040.cbl.cobModel#P_7F10382"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSF110" name="LDBSF110">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10265"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10382P_1F10382" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10382" targetNode="P_1F10382"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10382_I" deadCode="false" sourceNode="P_1F10382" targetNode="P_2F10382">
		<representations href="../../../cobol/LVVS3040.cbl.cobModel#S_1F10382"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10382_O" deadCode="false" sourceNode="P_1F10382" targetNode="P_3F10382">
		<representations href="../../../cobol/LVVS3040.cbl.cobModel#S_1F10382"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10382_I" deadCode="false" sourceNode="P_1F10382" targetNode="P_4F10382">
		<representations href="../../../cobol/LVVS3040.cbl.cobModel#S_2F10382"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10382_O" deadCode="false" sourceNode="P_1F10382" targetNode="P_5F10382">
		<representations href="../../../cobol/LVVS3040.cbl.cobModel#S_2F10382"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10382_I" deadCode="false" sourceNode="P_1F10382" targetNode="P_6F10382">
		<representations href="../../../cobol/LVVS3040.cbl.cobModel#S_3F10382"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10382_O" deadCode="false" sourceNode="P_1F10382" targetNode="P_7F10382">
		<representations href="../../../cobol/LVVS3040.cbl.cobModel#S_3F10382"/>
	</edges>
	<edges id="P_2F10382P_3F10382" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10382" targetNode="P_3F10382"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10382_I" deadCode="false" sourceNode="P_4F10382" targetNode="P_8F10382">
		<representations href="../../../cobol/LVVS3040.cbl.cobModel#S_10F10382"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10382_O" deadCode="false" sourceNode="P_4F10382" targetNode="P_9F10382">
		<representations href="../../../cobol/LVVS3040.cbl.cobModel#S_10F10382"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10382_I" deadCode="false" sourceNode="P_4F10382" targetNode="P_10F10382">
		<representations href="../../../cobol/LVVS3040.cbl.cobModel#S_12F10382"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10382_O" deadCode="false" sourceNode="P_4F10382" targetNode="P_11F10382">
		<representations href="../../../cobol/LVVS3040.cbl.cobModel#S_12F10382"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10382_I" deadCode="false" sourceNode="P_4F10382" targetNode="P_12F10382">
		<representations href="../../../cobol/LVVS3040.cbl.cobModel#S_14F10382"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10382_O" deadCode="false" sourceNode="P_4F10382" targetNode="P_13F10382">
		<representations href="../../../cobol/LVVS3040.cbl.cobModel#S_14F10382"/>
	</edges>
	<edges id="P_4F10382P_5F10382" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10382" targetNode="P_5F10382"/>
	<edges id="P_8F10382P_9F10382" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10382" targetNode="P_9F10382"/>
	<edges id="P_10F10382P_11F10382" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10382" targetNode="P_11F10382"/>
	<edges id="P_12F10382P_13F10382" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10382" targetNode="P_13F10382"/>
	<edges xsi:type="cbl:CallEdge" id="S_31F10382" deadCode="false" name="Dynamic WK-PGM-CALLED" sourceNode="P_12F10382" targetNode="LDBSF110">
		<representations href="../../../cobol/../importantStmts.cobModel#S_31F10382"></representations>
	</edges>
</Package>

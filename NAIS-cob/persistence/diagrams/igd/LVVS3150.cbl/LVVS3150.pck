<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS3150" cbl:id="LVVS3150" xsi:id="LVVS3150" packageRef="LVVS3150.igd#LVVS3150" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS3150_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10384" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS3150.cbl.cobModel#SC_1F10384"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10384" deadCode="false" name="PROGRAM_LVVS3150_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS3150.cbl.cobModel#P_1F10384"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10384" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS3150.cbl.cobModel#P_2F10384"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10384" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS3150.cbl.cobModel#P_3F10384"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10384" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS3150.cbl.cobModel#P_4F10384"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10384" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS3150.cbl.cobModel#P_5F10384"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10384" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS3150.cbl.cobModel#P_8F10384"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10384" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS3150.cbl.cobModel#P_9F10384"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10384" deadCode="false" name="S1200-RECUP-PARAM-MOVI">
				<representations href="../../../cobol/LVVS3150.cbl.cobModel#P_10F10384"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10384" deadCode="false" name="S1200-RECUP-PARAM-MOVI-EX">
				<representations href="../../../cobol/LVVS3150.cbl.cobModel#P_11F10384"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10384" deadCode="false" name="S1300-CALCOLO-RATEOCED2014">
				<representations href="../../../cobol/LVVS3150.cbl.cobModel#P_12F10384"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10384" deadCode="false" name="S1300-CALCOLO-RATEOCED2014-EX">
				<representations href="../../../cobol/LVVS3150.cbl.cobModel#P_13F10384"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10384" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS3150.cbl.cobModel#P_6F10384"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10384" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS3150.cbl.cobModel#P_7F10384"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS5060" name="LDBS5060">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10218"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0010" name="LCCS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10122"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10384P_1F10384" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10384" targetNode="P_1F10384"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10384_I" deadCode="false" sourceNode="P_1F10384" targetNode="P_2F10384">
		<representations href="../../../cobol/LVVS3150.cbl.cobModel#S_1F10384"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10384_O" deadCode="false" sourceNode="P_1F10384" targetNode="P_3F10384">
		<representations href="../../../cobol/LVVS3150.cbl.cobModel#S_1F10384"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10384_I" deadCode="false" sourceNode="P_1F10384" targetNode="P_4F10384">
		<representations href="../../../cobol/LVVS3150.cbl.cobModel#S_4F10384"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10384_O" deadCode="false" sourceNode="P_1F10384" targetNode="P_5F10384">
		<representations href="../../../cobol/LVVS3150.cbl.cobModel#S_4F10384"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10384_I" deadCode="false" sourceNode="P_1F10384" targetNode="P_6F10384">
		<representations href="../../../cobol/LVVS3150.cbl.cobModel#S_6F10384"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10384_O" deadCode="false" sourceNode="P_1F10384" targetNode="P_7F10384">
		<representations href="../../../cobol/LVVS3150.cbl.cobModel#S_6F10384"/>
	</edges>
	<edges id="P_2F10384P_3F10384" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10384" targetNode="P_3F10384"/>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10384_I" deadCode="false" sourceNode="P_4F10384" targetNode="P_8F10384">
		<representations href="../../../cobol/LVVS3150.cbl.cobModel#S_13F10384"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10384_O" deadCode="false" sourceNode="P_4F10384" targetNode="P_9F10384">
		<representations href="../../../cobol/LVVS3150.cbl.cobModel#S_13F10384"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10384_I" deadCode="false" sourceNode="P_4F10384" targetNode="P_10F10384">
		<representations href="../../../cobol/LVVS3150.cbl.cobModel#S_15F10384"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10384_O" deadCode="false" sourceNode="P_4F10384" targetNode="P_11F10384">
		<representations href="../../../cobol/LVVS3150.cbl.cobModel#S_15F10384"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10384_I" deadCode="false" sourceNode="P_4F10384" targetNode="P_12F10384">
		<representations href="../../../cobol/LVVS3150.cbl.cobModel#S_18F10384"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10384_O" deadCode="false" sourceNode="P_4F10384" targetNode="P_13F10384">
		<representations href="../../../cobol/LVVS3150.cbl.cobModel#S_18F10384"/>
	</edges>
	<edges id="P_4F10384P_5F10384" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10384" targetNode="P_5F10384"/>
	<edges id="P_8F10384P_9F10384" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10384" targetNode="P_9F10384"/>
	<edges id="P_10F10384P_11F10384" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10384" targetNode="P_11F10384"/>
	<edges id="P_12F10384P_13F10384" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10384" targetNode="P_13F10384"/>
	<edges xsi:type="cbl:CallEdge" id="S_35F10384" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_10F10384" targetNode="LDBS5060">
		<representations href="../../../cobol/../importantStmts.cobModel#S_35F10384"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_71F10384" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_12F10384" targetNode="LCCS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_71F10384"></representations>
	</edges>
</Package>

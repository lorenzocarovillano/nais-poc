<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LCCS0320" cbl:id="LCCS0320" xsi:id="LCCS0320" packageRef="LCCS0320.igd#LCCS0320" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LCCS0320_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10135" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LCCS0320.cbl.cobModel#SC_1F10135"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10135" deadCode="false" name="PROGRAM_LCCS0320_FIRST_SENTENCES">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_1F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10135" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_2F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10135" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_3F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10135" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_4F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10135" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_5F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10135" deadCode="false" name="GESTINONE-PMO">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_8F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10135" deadCode="false" name="GESTINONE-PMO-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_9F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10135" deadCode="false" name="S10100-6101-AGGIUNG-FRAZ">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_18F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10135" deadCode="false" name="S10100-6101-AGGIUNG-FRAZ-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_19F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10135" deadCode="false" name="S10100-6003-AGGIUNG-FRAZ">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_16F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10135" deadCode="false" name="S10100-6003-AGGIUNG-FRAZ-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_17F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10135" deadCode="false" name="S10150-6101-CALCOLA-LIM">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_28F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10135" deadCode="false" name="S10150-6101-CALCOLA-LIM-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_29F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10135" deadCode="false" name="CALL-LCCS0003">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_26F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10135" deadCode="false" name="CALL-LCCS0003-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_27F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10135" deadCode="false" name="S10300-6101-CICLO-PMO-GRZ">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_22F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10135" deadCode="false" name="S10300-6101-CICLO-PMO-GRZ-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_23F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10135" deadCode="false" name="S10200-6101-VALIDA-DATA">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_20F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10135" deadCode="false" name="S10200-6101-VALIDA-DATA-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_21F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10135" deadCode="false" name="S10210-6101-VALID-FIN-MESE">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_36F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10135" deadCode="false" name="S10210-6101-VALID-FIN-MESE-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_37F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10135" deadCode="false" name="S10400-6101-VERIFICA-DATA">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_34F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10135" deadCode="false" name="S10400-6101-VERIFICA-DATA-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_35F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10135" deadCode="false" name="S10500-6101-VERIF-VAR-FRAZ">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_24F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10135" deadCode="false" name="S10500-6101-VERIF-VAR-FRAZ-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_25F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10135" deadCode="false" name="S10600-6101-IMPOSTA-PMO">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_38F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10135" deadCode="false" name="S10600-6101-IMPOSTA-PMO-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_39F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10135" deadCode="false" name="S10700-6101-LEGGI-PMO">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_40F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10135" deadCode="false" name="S10700-6101-LEGGI-PMO-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_41F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10135" deadCode="false" name="GESTIONE-RIVAL">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_10F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10135" deadCode="false" name="GESTIONE-RIVAL-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_11F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10135" deadCode="false" name="S6006-CALC-PROX-RIC">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_44F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10135" deadCode="false" name="S6006-CALC-PROX-RIC-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_45F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10135" deadCode="false" name="S6006-AGGIUNGI-ANNO">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_48F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10135" deadCode="false" name="S6006-AGGIUNGI-ANNO-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_49F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10135" deadCode="false" name="VERIFICA-STATO-POL">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_50F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10135" deadCode="false" name="VERIFICA-STATO-POL-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_51F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10135" deadCode="false" name="S1820-LETTURA-POG">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_46F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10135" deadCode="false" name="EX-S1820">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_47F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10135" deadCode="false" name="GESTIONE-SCADE">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_12F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10135" deadCode="false" name="GESTIONE-SCADE-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_13F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10135" deadCode="false" name="GESTIONE-BONUS">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_14F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10135" deadCode="false" name="GESTIONE-BONUS-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_15F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10135" deadCode="true" name="GESTIONE-ATT-STRA">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_52F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10135" deadCode="true" name="GESTIONE-ATT-STRA-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_55F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10135" deadCode="true" name="S6210-AGGIUNGI-ANNO">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_53F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10135" deadCode="true" name="S6210-AGGIUNGI-ANNO-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_54F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10135" deadCode="false" name="S9000-OPERAZ-FINALI">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_6F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10135" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_7F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10135" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_32F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10135" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_33F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10135" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_58F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10135" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_59F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10135" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_56F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10135" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_57F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10135" deadCode="false" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_30F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10135" deadCode="false" name="EX-S0290">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_31F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10135" deadCode="false" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_42F10135"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10135" deadCode="false" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LCCS0320.cbl.cobModel#P_43F10135"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0003" name="LCCS0003">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10119"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0029" name="LCCS0029">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10129"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0010" name="IDSS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10099"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10135P_1F10135" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10135" targetNode="P_1F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10135_I" deadCode="false" sourceNode="P_1F10135" targetNode="P_2F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_1F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10135_O" deadCode="false" sourceNode="P_1F10135" targetNode="P_3F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_1F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10135_I" deadCode="false" sourceNode="P_1F10135" targetNode="P_4F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_3F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10135_O" deadCode="false" sourceNode="P_1F10135" targetNode="P_5F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_3F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10135_I" deadCode="false" sourceNode="P_1F10135" targetNode="P_6F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_4F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10135_O" deadCode="false" sourceNode="P_1F10135" targetNode="P_7F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_4F10135"/>
	</edges>
	<edges id="P_2F10135P_3F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10135" targetNode="P_3F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10135_I" deadCode="false" sourceNode="P_4F10135" targetNode="P_8F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_8F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10135_O" deadCode="false" sourceNode="P_4F10135" targetNode="P_9F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_8F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10135_I" deadCode="false" sourceNode="P_4F10135" targetNode="P_10F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_9F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10135_O" deadCode="false" sourceNode="P_4F10135" targetNode="P_11F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_9F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10135_I" deadCode="false" sourceNode="P_4F10135" targetNode="P_12F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_10F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10135_O" deadCode="false" sourceNode="P_4F10135" targetNode="P_13F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_10F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10135_I" deadCode="false" sourceNode="P_4F10135" targetNode="P_14F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_11F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10135_O" deadCode="false" sourceNode="P_4F10135" targetNode="P_15F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_11F10135"/>
	</edges>
	<edges id="P_4F10135P_5F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10135" targetNode="P_5F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10135_I" deadCode="false" sourceNode="P_8F10135" targetNode="P_16F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_16F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10135_O" deadCode="false" sourceNode="P_8F10135" targetNode="P_17F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_16F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10135_I" deadCode="false" sourceNode="P_8F10135" targetNode="P_18F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_17F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10135_O" deadCode="false" sourceNode="P_8F10135" targetNode="P_19F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_17F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10135_I" deadCode="false" sourceNode="P_8F10135" targetNode="P_20F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_19F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10135_O" deadCode="false" sourceNode="P_8F10135" targetNode="P_21F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_19F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10135_I" deadCode="false" sourceNode="P_8F10135" targetNode="P_22F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_20F10135"/>
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_24F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10135_O" deadCode="false" sourceNode="P_8F10135" targetNode="P_23F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_20F10135"/>
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_24F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10135_I" deadCode="false" sourceNode="P_8F10135" targetNode="P_24F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_20F10135"/>
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_27F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10135_O" deadCode="false" sourceNode="P_8F10135" targetNode="P_25F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_20F10135"/>
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_27F10135"/>
	</edges>
	<edges id="P_8F10135P_9F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10135" targetNode="P_9F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10135_I" deadCode="false" sourceNode="P_18F10135" targetNode="P_26F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_44F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10135_O" deadCode="false" sourceNode="P_18F10135" targetNode="P_27F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_44F10135"/>
	</edges>
	<edges id="P_18F10135P_19F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10135" targetNode="P_19F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10135_I" deadCode="false" sourceNode="P_16F10135" targetNode="P_26F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_80F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10135_O" deadCode="false" sourceNode="P_16F10135" targetNode="P_27F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_80F10135"/>
	</edges>
	<edges id="P_16F10135P_17F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10135" targetNode="P_17F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10135_I" deadCode="false" sourceNode="P_28F10135" targetNode="P_26F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_92F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10135_O" deadCode="false" sourceNode="P_28F10135" targetNode="P_27F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_92F10135"/>
	</edges>
	<edges id="P_28F10135P_29F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10135" targetNode="P_29F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10135_I" deadCode="false" sourceNode="P_26F10135" targetNode="P_30F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_101F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10135_O" deadCode="false" sourceNode="P_26F10135" targetNode="P_31F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_101F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10135_I" deadCode="false" sourceNode="P_26F10135" targetNode="P_32F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_108F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10135_O" deadCode="false" sourceNode="P_26F10135" targetNode="P_33F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_108F10135"/>
	</edges>
	<edges id="P_26F10135P_27F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10135" targetNode="P_27F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10135_I" deadCode="false" sourceNode="P_22F10135" targetNode="P_28F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_110F10135"/>
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_114F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10135_O" deadCode="false" sourceNode="P_22F10135" targetNode="P_29F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_110F10135"/>
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_114F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10135_I" deadCode="false" sourceNode="P_22F10135" targetNode="P_34F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_110F10135"/>
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_116F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10135_O" deadCode="false" sourceNode="P_22F10135" targetNode="P_35F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_110F10135"/>
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_116F10135"/>
	</edges>
	<edges id="P_22F10135P_23F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10135" targetNode="P_23F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_129F10135_I" deadCode="false" sourceNode="P_20F10135" targetNode="P_36F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_129F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_129F10135_O" deadCode="false" sourceNode="P_20F10135" targetNode="P_37F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_129F10135"/>
	</edges>
	<edges id="P_20F10135P_21F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10135" targetNode="P_21F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10135_I" deadCode="false" sourceNode="P_36F10135" targetNode="P_30F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_135F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10135_O" deadCode="false" sourceNode="P_36F10135" targetNode="P_31F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_135F10135"/>
	</edges>
	<edges id="P_36F10135P_37F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10135" targetNode="P_37F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10135_I" deadCode="false" sourceNode="P_34F10135" targetNode="P_24F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_144F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10135_O" deadCode="false" sourceNode="P_34F10135" targetNode="P_25F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_144F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10135_I" deadCode="false" sourceNode="P_34F10135" targetNode="P_24F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_147F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10135_O" deadCode="false" sourceNode="P_34F10135" targetNode="P_25F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_147F10135"/>
	</edges>
	<edges id="P_34F10135P_35F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10135" targetNode="P_35F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10135_I" deadCode="false" sourceNode="P_24F10135" targetNode="P_38F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_149F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10135_O" deadCode="false" sourceNode="P_24F10135" targetNode="P_39F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_149F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10135_I" deadCode="false" sourceNode="P_24F10135" targetNode="P_40F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_150F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10135_O" deadCode="false" sourceNode="P_24F10135" targetNode="P_41F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_150F10135"/>
	</edges>
	<edges id="P_24F10135P_25F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10135" targetNode="P_25F10135"/>
	<edges id="P_38F10135P_39F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10135" targetNode="P_39F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10135_I" deadCode="false" sourceNode="P_40F10135" targetNode="P_42F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_185F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10135_O" deadCode="false" sourceNode="P_40F10135" targetNode="P_43F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_185F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10135_I" deadCode="false" sourceNode="P_40F10135" targetNode="P_32F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_195F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10135_O" deadCode="false" sourceNode="P_40F10135" targetNode="P_33F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_195F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_200F10135_I" deadCode="false" sourceNode="P_40F10135" targetNode="P_32F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_200F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_200F10135_O" deadCode="false" sourceNode="P_40F10135" targetNode="P_33F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_200F10135"/>
	</edges>
	<edges id="P_40F10135P_41F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10135" targetNode="P_41F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_206F10135_I" deadCode="false" sourceNode="P_10F10135" targetNode="P_44F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_203F10135"/>
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_204F10135"/>
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_206F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_206F10135_O" deadCode="false" sourceNode="P_10F10135" targetNode="P_45F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_203F10135"/>
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_204F10135"/>
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_206F10135"/>
	</edges>
	<edges id="P_10F10135P_11F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10135" targetNode="P_11F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10135_I" deadCode="false" sourceNode="P_44F10135" targetNode="P_46F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_213F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10135_O" deadCode="false" sourceNode="P_44F10135" targetNode="P_47F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_213F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_215F10135_I" deadCode="false" sourceNode="P_44F10135" targetNode="P_48F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_215F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_215F10135_O" deadCode="false" sourceNode="P_44F10135" targetNode="P_49F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_215F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_220F10135_I" deadCode="false" sourceNode="P_44F10135" targetNode="P_50F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_220F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_220F10135_O" deadCode="false" sourceNode="P_44F10135" targetNode="P_51F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_220F10135"/>
	</edges>
	<edges id="P_44F10135P_45F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10135" targetNode="P_45F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_248F10135_I" deadCode="false" sourceNode="P_48F10135" targetNode="P_32F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_248F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_248F10135_O" deadCode="false" sourceNode="P_48F10135" targetNode="P_33F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_248F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10135_I" deadCode="false" sourceNode="P_48F10135" targetNode="P_26F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_253F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10135_O" deadCode="false" sourceNode="P_48F10135" targetNode="P_27F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_253F10135"/>
	</edges>
	<edges id="P_48F10135P_49F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10135" targetNode="P_49F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_266F10135_I" deadCode="false" sourceNode="P_50F10135" targetNode="P_42F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_266F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_266F10135_O" deadCode="false" sourceNode="P_50F10135" targetNode="P_43F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_266F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_275F10135_I" deadCode="false" sourceNode="P_50F10135" targetNode="P_32F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_275F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_275F10135_O" deadCode="false" sourceNode="P_50F10135" targetNode="P_33F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_275F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10135_I" deadCode="false" sourceNode="P_50F10135" targetNode="P_32F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_280F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10135_O" deadCode="false" sourceNode="P_50F10135" targetNode="P_33F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_280F10135"/>
	</edges>
	<edges id="P_50F10135P_51F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10135" targetNode="P_51F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10135_I" deadCode="false" sourceNode="P_46F10135" targetNode="P_42F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_292F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10135_O" deadCode="false" sourceNode="P_46F10135" targetNode="P_43F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_292F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_302F10135_I" deadCode="false" sourceNode="P_46F10135" targetNode="P_32F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_302F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_302F10135_O" deadCode="false" sourceNode="P_46F10135" targetNode="P_33F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_302F10135"/>
	</edges>
	<edges id="P_46F10135P_47F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10135" targetNode="P_47F10135"/>
	<edges id="P_12F10135P_13F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10135" targetNode="P_13F10135"/>
	<edges id="P_14F10135P_15F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10135" targetNode="P_15F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_329F10135_I" deadCode="true" sourceNode="P_52F10135" targetNode="P_53F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_328F10135"/>
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_329F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_329F10135_O" deadCode="true" sourceNode="P_52F10135" targetNode="P_54F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_328F10135"/>
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_329F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_341F10135_I" deadCode="true" sourceNode="P_53F10135" targetNode="P_26F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_341F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_341F10135_O" deadCode="true" sourceNode="P_53F10135" targetNode="P_27F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_341F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_350F10135_I" deadCode="false" sourceNode="P_32F10135" targetNode="P_56F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_350F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_350F10135_O" deadCode="false" sourceNode="P_32F10135" targetNode="P_57F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_350F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_354F10135_I" deadCode="false" sourceNode="P_32F10135" targetNode="P_58F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_354F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_354F10135_O" deadCode="false" sourceNode="P_32F10135" targetNode="P_59F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_354F10135"/>
	</edges>
	<edges id="P_32F10135P_33F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10135" targetNode="P_33F10135"/>
	<edges id="P_58F10135P_59F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10135" targetNode="P_59F10135"/>
	<edges id="P_56F10135P_57F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10135" targetNode="P_57F10135"/>
	<edges xsi:type="cbl:PerformEdge" id="S_390F10135_I" deadCode="false" sourceNode="P_30F10135" targetNode="P_32F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_390F10135"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_390F10135_O" deadCode="false" sourceNode="P_30F10135" targetNode="P_33F10135">
		<representations href="../../../cobol/LCCS0320.cbl.cobModel#S_390F10135"/>
	</edges>
	<edges id="P_30F10135P_31F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10135" targetNode="P_31F10135"/>
	<edges id="P_42F10135P_43F10135" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10135" targetNode="P_43F10135"/>
	<edges xsi:type="cbl:CallEdge" id="S_97F10135" deadCode="false" name="Dynamic LCCS0003" sourceNode="P_26F10135" targetNode="LCCS0003">
		<representations href="../../../cobol/../importantStmts.cobModel#S_97F10135"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_131F10135" deadCode="false" name="Dynamic LCCS0029" sourceNode="P_36F10135" targetNode="LCCS0029">
		<representations href="../../../cobol/../importantStmts.cobModel#S_131F10135"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_348F10135" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_32F10135" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_348F10135"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_418F10135" deadCode="false" name="Dynamic IDSI0011-PGM" sourceNode="P_42F10135" targetNode="IDSS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_418F10135"></representations>
	</edges>
</Package>

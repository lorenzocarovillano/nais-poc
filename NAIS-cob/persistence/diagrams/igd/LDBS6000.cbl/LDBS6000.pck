<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS6000" cbl:id="LDBS6000" xsi:id="LDBS6000" packageRef="LDBS6000.igd#LDBS6000" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS6000_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10229" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS6000.cbl.cobModel#SC_1F10229"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10229" deadCode="false" name="PROGRAM_LDBS6000_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_1F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10229" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_2F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10229" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_3F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10229" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_12F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10229" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_13F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10229" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_4F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10229" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_5F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10229" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_6F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10229" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_7F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10229" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_8F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10229" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_9F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10229" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_44F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10229" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_49F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10229" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_14F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10229" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_15F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10229" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_16F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10229" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_17F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10229" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_18F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10229" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_19F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10229" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_20F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10229" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_21F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10229" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_22F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10229" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_23F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10229" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_56F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10229" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_57F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10229" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_24F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10229" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_25F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10229" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_26F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10229" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_27F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10229" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_28F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10229" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_29F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10229" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_30F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10229" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_31F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10229" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_32F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10229" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_33F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10229" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_58F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10229" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_59F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10229" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_34F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10229" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_35F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10229" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_36F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10229" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_37F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10229" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_38F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10229" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_39F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10229" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_40F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10229" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_41F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10229" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_42F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10229" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_43F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10229" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_50F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10229" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_51F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10229" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_60F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10229" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_63F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10229" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_52F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10229" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_53F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10229" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_45F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10229" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_46F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10229" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_47F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10229" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_48F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10229" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_54F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10229" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_55F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10229" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_10F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10229" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_11F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10229" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_66F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10229" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_67F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10229" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_68F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10229" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_69F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10229" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_61F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10229" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_62F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10229" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_70F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10229" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_71F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10229" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_64F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10229" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_65F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10229" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_72F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10229" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_73F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10229" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_74F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10229" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_75F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10229" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_76F10229"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10229" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS6000.cbl.cobModel#P_77F10229"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_VAL_AST" name="VAL_AST">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_VAL_AST"/>
		</children>
	</packageNode>
	<edges id="SC_1F10229P_1F10229" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10229" targetNode="P_1F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10229_I" deadCode="false" sourceNode="P_1F10229" targetNode="P_2F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_2F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10229_O" deadCode="false" sourceNode="P_1F10229" targetNode="P_3F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_2F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10229_I" deadCode="false" sourceNode="P_1F10229" targetNode="P_4F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_6F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10229_O" deadCode="false" sourceNode="P_1F10229" targetNode="P_5F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_6F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10229_I" deadCode="false" sourceNode="P_1F10229" targetNode="P_6F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_10F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10229_O" deadCode="false" sourceNode="P_1F10229" targetNode="P_7F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_10F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10229_I" deadCode="false" sourceNode="P_1F10229" targetNode="P_8F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_14F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10229_O" deadCode="false" sourceNode="P_1F10229" targetNode="P_9F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_14F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10229_I" deadCode="false" sourceNode="P_2F10229" targetNode="P_10F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_24F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10229_O" deadCode="false" sourceNode="P_2F10229" targetNode="P_11F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_24F10229"/>
	</edges>
	<edges id="P_2F10229P_3F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10229" targetNode="P_3F10229"/>
	<edges id="P_12F10229P_13F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10229" targetNode="P_13F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10229_I" deadCode="false" sourceNode="P_4F10229" targetNode="P_14F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_37F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10229_O" deadCode="false" sourceNode="P_4F10229" targetNode="P_15F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_37F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10229_I" deadCode="false" sourceNode="P_4F10229" targetNode="P_16F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_38F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10229_O" deadCode="false" sourceNode="P_4F10229" targetNode="P_17F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_38F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10229_I" deadCode="false" sourceNode="P_4F10229" targetNode="P_18F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_39F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10229_O" deadCode="false" sourceNode="P_4F10229" targetNode="P_19F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_39F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10229_I" deadCode="false" sourceNode="P_4F10229" targetNode="P_20F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_40F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10229_O" deadCode="false" sourceNode="P_4F10229" targetNode="P_21F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_40F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10229_I" deadCode="false" sourceNode="P_4F10229" targetNode="P_22F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_41F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10229_O" deadCode="false" sourceNode="P_4F10229" targetNode="P_23F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_41F10229"/>
	</edges>
	<edges id="P_4F10229P_5F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10229" targetNode="P_5F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10229_I" deadCode="false" sourceNode="P_6F10229" targetNode="P_24F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_45F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10229_O" deadCode="false" sourceNode="P_6F10229" targetNode="P_25F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_45F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10229_I" deadCode="false" sourceNode="P_6F10229" targetNode="P_26F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_46F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10229_O" deadCode="false" sourceNode="P_6F10229" targetNode="P_27F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_46F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10229_I" deadCode="false" sourceNode="P_6F10229" targetNode="P_28F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_47F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10229_O" deadCode="false" sourceNode="P_6F10229" targetNode="P_29F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_47F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10229_I" deadCode="false" sourceNode="P_6F10229" targetNode="P_30F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_48F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10229_O" deadCode="false" sourceNode="P_6F10229" targetNode="P_31F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_48F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10229_I" deadCode="false" sourceNode="P_6F10229" targetNode="P_32F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_49F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10229_O" deadCode="false" sourceNode="P_6F10229" targetNode="P_33F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_49F10229"/>
	</edges>
	<edges id="P_6F10229P_7F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10229" targetNode="P_7F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10229_I" deadCode="false" sourceNode="P_8F10229" targetNode="P_34F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_53F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10229_O" deadCode="false" sourceNode="P_8F10229" targetNode="P_35F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_53F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10229_I" deadCode="false" sourceNode="P_8F10229" targetNode="P_36F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_54F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10229_O" deadCode="false" sourceNode="P_8F10229" targetNode="P_37F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_54F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10229_I" deadCode="false" sourceNode="P_8F10229" targetNode="P_38F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_55F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10229_O" deadCode="false" sourceNode="P_8F10229" targetNode="P_39F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_55F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10229_I" deadCode="false" sourceNode="P_8F10229" targetNode="P_40F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_56F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10229_O" deadCode="false" sourceNode="P_8F10229" targetNode="P_41F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_56F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10229_I" deadCode="false" sourceNode="P_8F10229" targetNode="P_42F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_57F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10229_O" deadCode="false" sourceNode="P_8F10229" targetNode="P_43F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_57F10229"/>
	</edges>
	<edges id="P_8F10229P_9F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10229" targetNode="P_9F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10229_I" deadCode="false" sourceNode="P_44F10229" targetNode="P_45F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_60F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10229_O" deadCode="false" sourceNode="P_44F10229" targetNode="P_46F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_60F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10229_I" deadCode="false" sourceNode="P_44F10229" targetNode="P_47F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_61F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10229_O" deadCode="false" sourceNode="P_44F10229" targetNode="P_48F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_61F10229"/>
	</edges>
	<edges id="P_44F10229P_49F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10229" targetNode="P_49F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10229_I" deadCode="false" sourceNode="P_14F10229" targetNode="P_45F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_65F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10229_O" deadCode="false" sourceNode="P_14F10229" targetNode="P_46F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_65F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10229_I" deadCode="false" sourceNode="P_14F10229" targetNode="P_47F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_66F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10229_O" deadCode="false" sourceNode="P_14F10229" targetNode="P_48F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_66F10229"/>
	</edges>
	<edges id="P_14F10229P_15F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10229" targetNode="P_15F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10229_I" deadCode="false" sourceNode="P_16F10229" targetNode="P_44F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_69F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10229_O" deadCode="false" sourceNode="P_16F10229" targetNode="P_49F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_69F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10229_I" deadCode="false" sourceNode="P_16F10229" targetNode="P_12F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_71F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10229_O" deadCode="false" sourceNode="P_16F10229" targetNode="P_13F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_71F10229"/>
	</edges>
	<edges id="P_16F10229P_17F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10229" targetNode="P_17F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10229_I" deadCode="false" sourceNode="P_18F10229" targetNode="P_12F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_74F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10229_O" deadCode="false" sourceNode="P_18F10229" targetNode="P_13F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_74F10229"/>
	</edges>
	<edges id="P_18F10229P_19F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10229" targetNode="P_19F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10229_I" deadCode="false" sourceNode="P_20F10229" targetNode="P_16F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_76F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10229_O" deadCode="false" sourceNode="P_20F10229" targetNode="P_17F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_76F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10229_I" deadCode="false" sourceNode="P_20F10229" targetNode="P_22F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_78F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10229_O" deadCode="false" sourceNode="P_20F10229" targetNode="P_23F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_78F10229"/>
	</edges>
	<edges id="P_20F10229P_21F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10229" targetNode="P_21F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10229_I" deadCode="false" sourceNode="P_22F10229" targetNode="P_12F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_81F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10229_O" deadCode="false" sourceNode="P_22F10229" targetNode="P_13F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_81F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10229_I" deadCode="false" sourceNode="P_22F10229" targetNode="P_50F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_83F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10229_O" deadCode="false" sourceNode="P_22F10229" targetNode="P_51F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_83F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10229_I" deadCode="false" sourceNode="P_22F10229" targetNode="P_52F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_84F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10229_O" deadCode="false" sourceNode="P_22F10229" targetNode="P_53F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_84F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10229_I" deadCode="false" sourceNode="P_22F10229" targetNode="P_54F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_85F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10229_O" deadCode="false" sourceNode="P_22F10229" targetNode="P_55F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_85F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10229_I" deadCode="false" sourceNode="P_22F10229" targetNode="P_18F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_87F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10229_O" deadCode="false" sourceNode="P_22F10229" targetNode="P_19F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_87F10229"/>
	</edges>
	<edges id="P_22F10229P_23F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10229" targetNode="P_23F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10229_I" deadCode="false" sourceNode="P_56F10229" targetNode="P_45F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_91F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10229_O" deadCode="false" sourceNode="P_56F10229" targetNode="P_46F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_91F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10229_I" deadCode="false" sourceNode="P_56F10229" targetNode="P_47F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_92F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10229_O" deadCode="false" sourceNode="P_56F10229" targetNode="P_48F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_92F10229"/>
	</edges>
	<edges id="P_56F10229P_57F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10229" targetNode="P_57F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10229_I" deadCode="false" sourceNode="P_24F10229" targetNode="P_45F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_96F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10229_O" deadCode="false" sourceNode="P_24F10229" targetNode="P_46F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_96F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10229_I" deadCode="false" sourceNode="P_24F10229" targetNode="P_47F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_97F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10229_O" deadCode="false" sourceNode="P_24F10229" targetNode="P_48F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_97F10229"/>
	</edges>
	<edges id="P_24F10229P_25F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10229" targetNode="P_25F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10229_I" deadCode="false" sourceNode="P_26F10229" targetNode="P_56F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_100F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10229_O" deadCode="false" sourceNode="P_26F10229" targetNode="P_57F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_100F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10229_I" deadCode="false" sourceNode="P_26F10229" targetNode="P_12F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_102F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10229_O" deadCode="false" sourceNode="P_26F10229" targetNode="P_13F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_102F10229"/>
	</edges>
	<edges id="P_26F10229P_27F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10229" targetNode="P_27F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10229_I" deadCode="false" sourceNode="P_28F10229" targetNode="P_12F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_105F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10229_O" deadCode="false" sourceNode="P_28F10229" targetNode="P_13F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_105F10229"/>
	</edges>
	<edges id="P_28F10229P_29F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10229" targetNode="P_29F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10229_I" deadCode="false" sourceNode="P_30F10229" targetNode="P_26F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_107F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10229_O" deadCode="false" sourceNode="P_30F10229" targetNode="P_27F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_107F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10229_I" deadCode="false" sourceNode="P_30F10229" targetNode="P_32F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_109F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10229_O" deadCode="false" sourceNode="P_30F10229" targetNode="P_33F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_109F10229"/>
	</edges>
	<edges id="P_30F10229P_31F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10229" targetNode="P_31F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10229_I" deadCode="false" sourceNode="P_32F10229" targetNode="P_12F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_112F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10229_O" deadCode="false" sourceNode="P_32F10229" targetNode="P_13F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_112F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10229_I" deadCode="false" sourceNode="P_32F10229" targetNode="P_50F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_114F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10229_O" deadCode="false" sourceNode="P_32F10229" targetNode="P_51F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_114F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10229_I" deadCode="false" sourceNode="P_32F10229" targetNode="P_52F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_115F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10229_O" deadCode="false" sourceNode="P_32F10229" targetNode="P_53F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_115F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10229_I" deadCode="false" sourceNode="P_32F10229" targetNode="P_54F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_116F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10229_O" deadCode="false" sourceNode="P_32F10229" targetNode="P_55F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_116F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10229_I" deadCode="false" sourceNode="P_32F10229" targetNode="P_28F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_118F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10229_O" deadCode="false" sourceNode="P_32F10229" targetNode="P_29F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_118F10229"/>
	</edges>
	<edges id="P_32F10229P_33F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10229" targetNode="P_33F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10229_I" deadCode="false" sourceNode="P_58F10229" targetNode="P_45F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_122F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10229_O" deadCode="false" sourceNode="P_58F10229" targetNode="P_46F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_122F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10229_I" deadCode="false" sourceNode="P_58F10229" targetNode="P_47F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_123F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10229_O" deadCode="false" sourceNode="P_58F10229" targetNode="P_48F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_123F10229"/>
	</edges>
	<edges id="P_58F10229P_59F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10229" targetNode="P_59F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10229_I" deadCode="false" sourceNode="P_34F10229" targetNode="P_45F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_126F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10229_O" deadCode="false" sourceNode="P_34F10229" targetNode="P_46F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_126F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10229_I" deadCode="false" sourceNode="P_34F10229" targetNode="P_47F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_127F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10229_O" deadCode="false" sourceNode="P_34F10229" targetNode="P_48F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_127F10229"/>
	</edges>
	<edges id="P_34F10229P_35F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10229" targetNode="P_35F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10229_I" deadCode="false" sourceNode="P_36F10229" targetNode="P_58F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_130F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10229_O" deadCode="false" sourceNode="P_36F10229" targetNode="P_59F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_130F10229"/>
	</edges>
	<edges id="P_36F10229P_37F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10229" targetNode="P_37F10229"/>
	<edges id="P_38F10229P_39F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10229" targetNode="P_39F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10229_I" deadCode="false" sourceNode="P_40F10229" targetNode="P_36F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_135F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10229_O" deadCode="false" sourceNode="P_40F10229" targetNode="P_37F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_135F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10229_I" deadCode="false" sourceNode="P_40F10229" targetNode="P_42F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_137F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10229_O" deadCode="false" sourceNode="P_40F10229" targetNode="P_43F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_137F10229"/>
	</edges>
	<edges id="P_40F10229P_41F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10229" targetNode="P_41F10229"/>
	<edges id="P_42F10229P_43F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10229" targetNode="P_43F10229"/>
	<edges id="P_50F10229P_51F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10229" targetNode="P_51F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10229_I" deadCode="true" sourceNode="P_60F10229" targetNode="P_61F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_174F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10229_O" deadCode="true" sourceNode="P_60F10229" targetNode="P_62F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_174F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10229_I" deadCode="true" sourceNode="P_60F10229" targetNode="P_61F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_177F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10229_O" deadCode="true" sourceNode="P_60F10229" targetNode="P_62F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_177F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10229_I" deadCode="true" sourceNode="P_60F10229" targetNode="P_61F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_181F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10229_O" deadCode="true" sourceNode="P_60F10229" targetNode="P_62F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_181F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10229_I" deadCode="true" sourceNode="P_60F10229" targetNode="P_61F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_185F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10229_O" deadCode="true" sourceNode="P_60F10229" targetNode="P_62F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_185F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10229_I" deadCode="false" sourceNode="P_52F10229" targetNode="P_64F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_189F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10229_O" deadCode="false" sourceNode="P_52F10229" targetNode="P_65F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_189F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_192F10229_I" deadCode="false" sourceNode="P_52F10229" targetNode="P_64F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_192F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_192F10229_O" deadCode="false" sourceNode="P_52F10229" targetNode="P_65F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_192F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_196F10229_I" deadCode="false" sourceNode="P_52F10229" targetNode="P_64F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_196F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_196F10229_O" deadCode="false" sourceNode="P_52F10229" targetNode="P_65F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_196F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_200F10229_I" deadCode="false" sourceNode="P_52F10229" targetNode="P_64F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_200F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_200F10229_O" deadCode="false" sourceNode="P_52F10229" targetNode="P_65F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_200F10229"/>
	</edges>
	<edges id="P_52F10229P_53F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10229" targetNode="P_53F10229"/>
	<edges id="P_45F10229P_46F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10229" targetNode="P_46F10229"/>
	<edges id="P_47F10229P_48F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10229" targetNode="P_48F10229"/>
	<edges id="P_54F10229P_55F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10229" targetNode="P_55F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_209F10229_I" deadCode="false" sourceNode="P_10F10229" targetNode="P_66F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_209F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_209F10229_O" deadCode="false" sourceNode="P_10F10229" targetNode="P_67F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_209F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10229_I" deadCode="false" sourceNode="P_10F10229" targetNode="P_68F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_211F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10229_O" deadCode="false" sourceNode="P_10F10229" targetNode="P_69F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_211F10229"/>
	</edges>
	<edges id="P_10F10229P_11F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10229" targetNode="P_11F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10229_I" deadCode="false" sourceNode="P_66F10229" targetNode="P_61F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_216F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10229_O" deadCode="false" sourceNode="P_66F10229" targetNode="P_62F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_216F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10229_I" deadCode="false" sourceNode="P_66F10229" targetNode="P_61F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_221F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10229_O" deadCode="false" sourceNode="P_66F10229" targetNode="P_62F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_221F10229"/>
	</edges>
	<edges id="P_66F10229P_67F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10229" targetNode="P_67F10229"/>
	<edges id="P_68F10229P_69F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10229" targetNode="P_69F10229"/>
	<edges id="P_61F10229P_62F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10229" targetNode="P_62F10229"/>
	<edges xsi:type="cbl:PerformEdge" id="S_250F10229_I" deadCode="false" sourceNode="P_64F10229" targetNode="P_72F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_250F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_250F10229_O" deadCode="false" sourceNode="P_64F10229" targetNode="P_73F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_250F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_251F10229_I" deadCode="false" sourceNode="P_64F10229" targetNode="P_74F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_251F10229"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_251F10229_O" deadCode="false" sourceNode="P_64F10229" targetNode="P_75F10229">
		<representations href="../../../cobol/LDBS6000.cbl.cobModel#S_251F10229"/>
	</edges>
	<edges id="P_64F10229P_65F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10229" targetNode="P_65F10229"/>
	<edges id="P_72F10229P_73F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10229" targetNode="P_73F10229"/>
	<edges id="P_74F10229P_75F10229" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10229" targetNode="P_75F10229"/>
	<edges xsi:type="cbl:DataEdge" id="S_70F10229_POS1" deadCode="false" targetNode="P_16F10229" sourceNode="DB2_VAL_AST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_70F10229"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_73F10229_POS1" deadCode="false" targetNode="P_18F10229" sourceNode="DB2_VAL_AST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_73F10229"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_80F10229_POS1" deadCode="false" targetNode="P_22F10229" sourceNode="DB2_VAL_AST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_80F10229"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_101F10229_POS1" deadCode="false" targetNode="P_26F10229" sourceNode="DB2_VAL_AST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_101F10229"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_104F10229_POS1" deadCode="false" targetNode="P_28F10229" sourceNode="DB2_VAL_AST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_104F10229"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10229_POS1" deadCode="false" targetNode="P_32F10229" sourceNode="DB2_VAL_AST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10229"></representations>
	</edges>
</Package>

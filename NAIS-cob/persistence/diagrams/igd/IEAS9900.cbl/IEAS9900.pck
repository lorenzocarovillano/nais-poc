<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IEAS9900" cbl:id="IEAS9900" xsi:id="IEAS9900" packageRef="IEAS9900.igd#IEAS9900" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IEAS9900_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10109" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IEAS9900.cbl.cobModel#SC_1F10109"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10109" deadCode="false" name="1000-PRINCIPALE">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_1F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10109" deadCode="false" name="1000-PRINCIPALE-FINE">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_6F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10109" deadCode="false" name="2000-APERTURA">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_2F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10109" deadCode="false" name="2000-APERTURA-FINE">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_3F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10109" deadCode="false" name="2010-VALIDAZIONE-DATI">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_7F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10109" deadCode="false" name="2010-VALIDAZIONE-DATI-FINE">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_8F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10109" deadCode="true" name="3000-PROCESSA">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_11F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10109" deadCode="true" name="3000-PROCESSA-FINE">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_20F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10109" deadCode="false" name="4000-PROCESSA">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_4F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10109" deadCode="false" name="4000-PROCESSA-FINE">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_5F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10109" deadCode="false" name="3800-SEARCH-GRAVITA">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_21F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10109" deadCode="false" name="3800-SEARCH-GRAVITA-EX">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_22F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10109" deadCode="true" name="3100-RICERCA-GRAVITA">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_12F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10109" deadCode="true" name="3100-RICERCA-GRAVITA-FINE">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_13F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10109" deadCode="true" name="3110-VALORIZZA-HOST">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_25F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10109" deadCode="true" name="3110-VALORIZZA-HOST-FINE">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_26F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10109" deadCode="false" name="3200-SELEZ-DESC-ERRORE">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_14F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10109" deadCode="false" name="3200-SELEZ-DESC-ERRORE-FINE">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_15F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10109" deadCode="false" name="3210-ACCEDI-LNGERR">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_27F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10109" deadCode="false" name="3210-ACCEDI-LNGERR-FINE">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_28F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10109" deadCode="false" name="3220-SOST-PLACEHOLDER">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_29F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10109" deadCode="false" name="3220-SOST-PLACEHOLDER-FINE">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_30F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10109" deadCode="false" name="3230-VALORIZZA-OUTPUT">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_16F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10109" deadCode="false" name="3230-VALORIZZA-OUTPUT-FINE">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_17F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10109" deadCode="false" name="3270-VALORIZZA-TAB">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_23F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10109" deadCode="false" name="3270-VALORIZZA-TAB-FINE">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_24F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10109" deadCode="false" name="3240-SCRIVI-LOG">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_18F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10109" deadCode="false" name="3240-SCRIVI-LOG-FINE">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_19F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10109" deadCode="false" name="2015-INIZIALIZZA">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_9F10109"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10109" deadCode="false" name="2015-INIZIALIZZA-EX">
				<representations href="../../../cobol/IEAS9900.cbl.cobModel#P_10F10109"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_GRAVITA_ERRORE" name="GRAVITA_ERRORE">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_GRAVITA_ERRORE"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_LINGUA_ERRORE" name="LINGUA_ERRORE">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_LINGUA_ERRORE"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9800" name="IEAS9800">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10108"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9700" name="IEAS9700">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10107"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10109P_1F10109" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10109" targetNode="P_1F10109"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10109_I" deadCode="false" sourceNode="P_1F10109" targetNode="P_2F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_1F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10109_O" deadCode="false" sourceNode="P_1F10109" targetNode="P_3F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_1F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10109_I" deadCode="false" sourceNode="P_1F10109" targetNode="P_4F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_3F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10109_O" deadCode="false" sourceNode="P_1F10109" targetNode="P_5F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_3F10109"/>
	</edges>
	<edges id="P_1F10109P_6F10109" xsi:type="cbl:FallThroughEdge" sourceNode="P_1F10109" targetNode="P_6F10109"/>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10109_I" deadCode="false" sourceNode="P_2F10109" targetNode="P_7F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_8F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10109_O" deadCode="false" sourceNode="P_2F10109" targetNode="P_8F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_8F10109"/>
	</edges>
	<edges id="P_2F10109P_3F10109" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10109" targetNode="P_3F10109"/>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10109_I" deadCode="false" sourceNode="P_7F10109" targetNode="P_9F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_28F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10109_O" deadCode="false" sourceNode="P_7F10109" targetNode="P_10F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_28F10109"/>
	</edges>
	<edges id="P_7F10109P_8F10109" xsi:type="cbl:FallThroughEdge" sourceNode="P_7F10109" targetNode="P_8F10109"/>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10109_I" deadCode="true" sourceNode="P_11F10109" targetNode="P_12F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_32F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10109_O" deadCode="true" sourceNode="P_11F10109" targetNode="P_13F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_32F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10109_I" deadCode="true" sourceNode="P_11F10109" targetNode="P_14F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_41F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10109_O" deadCode="true" sourceNode="P_11F10109" targetNode="P_15F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_41F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10109_I" deadCode="true" sourceNode="P_11F10109" targetNode="P_16F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_43F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10109_O" deadCode="true" sourceNode="P_11F10109" targetNode="P_17F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_43F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10109_I" deadCode="true" sourceNode="P_11F10109" targetNode="P_18F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_45F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10109_O" deadCode="true" sourceNode="P_11F10109" targetNode="P_19F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_45F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10109_I" deadCode="false" sourceNode="P_4F10109" targetNode="P_21F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_49F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10109_O" deadCode="false" sourceNode="P_4F10109" targetNode="P_22F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_49F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10109_I" deadCode="false" sourceNode="P_4F10109" targetNode="P_14F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_58F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10109_O" deadCode="false" sourceNode="P_4F10109" targetNode="P_15F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_58F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10109_I" deadCode="false" sourceNode="P_4F10109" targetNode="P_16F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_61F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10109_O" deadCode="false" sourceNode="P_4F10109" targetNode="P_17F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_61F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10109_I" deadCode="false" sourceNode="P_4F10109" targetNode="P_18F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_63F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10109_O" deadCode="false" sourceNode="P_4F10109" targetNode="P_19F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_63F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10109_I" deadCode="false" sourceNode="P_4F10109" targetNode="P_23F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_66F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10109_O" deadCode="false" sourceNode="P_4F10109" targetNode="P_24F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_66F10109"/>
	</edges>
	<edges id="P_4F10109P_5F10109" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10109" targetNode="P_5F10109"/>
	<edges id="P_21F10109P_22F10109" xsi:type="cbl:FallThroughEdge" sourceNode="P_21F10109" targetNode="P_22F10109"/>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10109_I" deadCode="true" sourceNode="P_12F10109" targetNode="P_25F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_94F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10109_O" deadCode="true" sourceNode="P_12F10109" targetNode="P_26F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_94F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10109_I" deadCode="false" sourceNode="P_14F10109" targetNode="P_27F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_130F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10109_O" deadCode="false" sourceNode="P_14F10109" targetNode="P_28F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_130F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10109_I" deadCode="false" sourceNode="P_14F10109" targetNode="P_29F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_132F10109"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10109_O" deadCode="false" sourceNode="P_14F10109" targetNode="P_30F10109">
		<representations href="../../../cobol/IEAS9900.cbl.cobModel#S_132F10109"/>
	</edges>
	<edges id="P_14F10109P_15F10109" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10109" targetNode="P_15F10109"/>
	<edges id="P_27F10109P_28F10109" xsi:type="cbl:FallThroughEdge" sourceNode="P_27F10109" targetNode="P_28F10109"/>
	<edges id="P_29F10109P_30F10109" xsi:type="cbl:FallThroughEdge" sourceNode="P_29F10109" targetNode="P_30F10109"/>
	<edges id="P_16F10109P_17F10109" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10109" targetNode="P_17F10109"/>
	<edges id="P_23F10109P_24F10109" xsi:type="cbl:FallThroughEdge" sourceNode="P_23F10109" targetNode="P_24F10109"/>
	<edges id="P_18F10109P_19F10109" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10109" targetNode="P_19F10109"/>
	<edges id="P_9F10109P_10F10109" xsi:type="cbl:FallThroughEdge" sourceNode="P_9F10109" targetNode="P_10F10109"/>
	<edges xsi:type="cbl:DataEdge" id="S_82F10109_POS1" deadCode="false" targetNode="P_21F10109" sourceNode="DB2_GRAVITA_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_82F10109"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_108F10109_POS1" deadCode="false" targetNode="P_25F10109" sourceNode="DB2_GRAVITA_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_108F10109"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_109F10109_POS1" deadCode="false" targetNode="P_25F10109" sourceNode="DB2_GRAVITA_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_109F10109"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_110F10109_POS1" deadCode="false" targetNode="P_25F10109" sourceNode="DB2_GRAVITA_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_110F10109"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10109_POS1" deadCode="false" targetNode="P_25F10109" sourceNode="DB2_GRAVITA_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10109"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_112F10109_POS1" deadCode="false" targetNode="P_25F10109" sourceNode="DB2_GRAVITA_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_112F10109"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_113F10109_POS1" deadCode="false" targetNode="P_25F10109" sourceNode="DB2_GRAVITA_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_113F10109"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_114F10109_POS1" deadCode="false" targetNode="P_25F10109" sourceNode="DB2_GRAVITA_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10109"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_115F10109_POS1" deadCode="false" targetNode="P_25F10109" sourceNode="DB2_GRAVITA_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_115F10109"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_116F10109_POS1" deadCode="false" targetNode="P_25F10109" sourceNode="DB2_GRAVITA_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_116F10109"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_117F10109_POS1" deadCode="false" targetNode="P_25F10109" sourceNode="DB2_GRAVITA_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_117F10109"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_118F10109_POS1" deadCode="false" targetNode="P_25F10109" sourceNode="DB2_GRAVITA_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_118F10109"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_119F10109_POS1" deadCode="false" targetNode="P_25F10109" sourceNode="DB2_GRAVITA_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_119F10109"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_120F10109_POS1" deadCode="false" targetNode="P_25F10109" sourceNode="DB2_GRAVITA_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_120F10109"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_137F10109_POS1" deadCode="false" targetNode="P_27F10109" sourceNode="DB2_LINGUA_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_137F10109"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_151F10109" deadCode="false" name="Dynamic LT-IEAS9800" sourceNode="P_29F10109" targetNode="IEAS9800">
		<representations href="../../../cobol/../importantStmts.cobModel#S_151F10109"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_180F10109" deadCode="false" name="Dynamic LT-IEAS9700" sourceNode="P_18F10109" targetNode="IEAS9700">
		<representations href="../../../cobol/../importantStmts.cobModel#S_180F10109"></representations>
	</edges>
</Package>

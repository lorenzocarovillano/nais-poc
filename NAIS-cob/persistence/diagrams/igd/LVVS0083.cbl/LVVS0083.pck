<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0083" cbl:id="LVVS0083" xsi:id="LVVS0083" packageRef="LVVS0083.igd#LVVS0083" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0083_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10334" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0083.cbl.cobModel#SC_1F10334"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10334" deadCode="false" name="PROGRAM_LVVS0083_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0083.cbl.cobModel#P_1F10334"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10334" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0083.cbl.cobModel#P_2F10334"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10334" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0083.cbl.cobModel#P_3F10334"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10334" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0083.cbl.cobModel#P_4F10334"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10334" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0083.cbl.cobModel#P_5F10334"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10334" deadCode="false" name="S1100-SOMMA-IMP-PREST">
				<representations href="../../../cobol/LVVS0083.cbl.cobModel#P_8F10334"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10334" deadCode="false" name="S1250-EX">
				<representations href="../../../cobol/LVVS0083.cbl.cobModel#P_9F10334"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10334" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0083.cbl.cobModel#P_6F10334"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10334" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0083.cbl.cobModel#P_7F10334"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS2820" name="LDBS2820">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10187"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10334P_1F10334" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10334" targetNode="P_1F10334"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10334_I" deadCode="false" sourceNode="P_1F10334" targetNode="P_2F10334">
		<representations href="../../../cobol/LVVS0083.cbl.cobModel#S_1F10334"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10334_O" deadCode="false" sourceNode="P_1F10334" targetNode="P_3F10334">
		<representations href="../../../cobol/LVVS0083.cbl.cobModel#S_1F10334"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10334_I" deadCode="false" sourceNode="P_1F10334" targetNode="P_4F10334">
		<representations href="../../../cobol/LVVS0083.cbl.cobModel#S_2F10334"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10334_O" deadCode="false" sourceNode="P_1F10334" targetNode="P_5F10334">
		<representations href="../../../cobol/LVVS0083.cbl.cobModel#S_2F10334"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10334_I" deadCode="false" sourceNode="P_1F10334" targetNode="P_6F10334">
		<representations href="../../../cobol/LVVS0083.cbl.cobModel#S_3F10334"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10334_O" deadCode="false" sourceNode="P_1F10334" targetNode="P_7F10334">
		<representations href="../../../cobol/LVVS0083.cbl.cobModel#S_3F10334"/>
	</edges>
	<edges id="P_2F10334P_3F10334" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10334" targetNode="P_3F10334"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10334_I" deadCode="false" sourceNode="P_4F10334" targetNode="P_8F10334">
		<representations href="../../../cobol/LVVS0083.cbl.cobModel#S_11F10334"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10334_O" deadCode="false" sourceNode="P_4F10334" targetNode="P_9F10334">
		<representations href="../../../cobol/LVVS0083.cbl.cobModel#S_11F10334"/>
	</edges>
	<edges id="P_4F10334P_5F10334" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10334" targetNode="P_5F10334"/>
	<edges id="P_8F10334P_9F10334" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10334" targetNode="P_9F10334"/>
	<edges xsi:type="cbl:CallEdge" id="S_14F10334" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_8F10334" targetNode="LDBS2820">
		<representations href="../../../cobol/../importantStmts.cobModel#S_14F10334"></representations>
	</edges>
</Package>

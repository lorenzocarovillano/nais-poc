<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IDSS0300" cbl:id="IDSS0300" xsi:id="IDSS0300" packageRef="IDSS0300.igd#IDSS0300" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IDSS0300_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10105" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IDSS0300.cbl.cobModel#SC_1F10105"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10105" deadCode="false" name="PROGRAM_IDSS0300_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_1F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10105" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_2F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10105" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_3F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10105" deadCode="false" name="A010-CNTL-INPUT">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_8F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10105" deadCode="false" name="A010-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_9F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10105" deadCode="false" name="B000-ELABORA">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_6F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10105" deadCode="false" name="B000-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_7F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10105" deadCode="false" name="B200-TRATTA-WRITE">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_10F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10105" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_11F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10105" deadCode="false" name="B300-TRATTA-READ">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_12F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10105" deadCode="false" name="B300-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_13F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10105" deadCode="false" name="B250-SCRIVI-TEMPORARY-TABLE">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_18F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10105" deadCode="false" name="B250-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_19F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10105" deadCode="false" name="B255-ELAB-CONT">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_30F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10105" deadCode="false" name="B255-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_31F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10105" deadCode="false" name="B260-VAL-INSERT-CONT">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_32F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10105" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_33F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10105" deadCode="false" name="B400-TRATTA-READ-NEXT">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_14F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10105" deadCode="false" name="B400-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_15F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10105" deadCode="false" name="E100-DECLARE-CURSOR-PACK">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_20F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10105" deadCode="false" name="E100-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_21F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10105" deadCode="false" name="E101-DECL-CUR-PACK-SESSION-TAB">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_36F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10105" deadCode="false" name="E101-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_37F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10105" deadCode="false" name="E103-DECL-CUR-PACK-STATIC-TAB">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_38F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10105" deadCode="false" name="E103-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_39F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10105" deadCode="false" name="E200-OPEN-CURSOR-PACK">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_22F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10105" deadCode="false" name="E200-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_23F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10105" deadCode="false" name="E201-DECL-CUR-PACK-SESSION-TAB">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_40F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10105" deadCode="false" name="E201-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_41F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10105" deadCode="false" name="E203-DECL-CUR-PACK-STATIC-TAB">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_42F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10105" deadCode="false" name="E203-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_43F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10105" deadCode="false" name="E300-FETCH-CURSOR-PACK">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_24F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10105" deadCode="false" name="E300-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_25F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10105" deadCode="false" name="E301-FTCH-CUR-PACK-SESSION-TAB">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_44F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10105" deadCode="false" name="E301-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_45F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10105" deadCode="false" name="E303-FTCH-CUR-PACK-STATIC-TAB">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_46F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10105" deadCode="false" name="E303-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_47F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10105" deadCode="false" name="E400-CLOSE-CURSOR-PACK">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_48F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10105" deadCode="false" name="E400-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_49F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10105" deadCode="false" name="E401-CLS-CUR-PACK-SESSION-TAB">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_50F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10105" deadCode="false" name="E401-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_51F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10105" deadCode="false" name="E403-CLS-CUR-PACK-STATIC-TAB">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_52F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10105" deadCode="false" name="E403-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_53F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10105" deadCode="false" name="E600-VALORIZZA-OUT">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_26F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10105" deadCode="false" name="E600-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_27F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10105" deadCode="false" name="A210-SELECT">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_54F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10105" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_59F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10105" deadCode="false" name="A211-SELECT-SESSION-TAB">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_55F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10105" deadCode="false" name="A211-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_56F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10105" deadCode="false" name="A213-SELECT-STATIC-TAB">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_57F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10105" deadCode="false" name="A213-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_58F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10105" deadCode="false" name="A220-INSERT">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_34F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10105" deadCode="false" name="A220-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_35F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10105" deadCode="false" name="A221-INSERT-SESSION-TABLE">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_60F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10105" deadCode="false" name="A221-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_61F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10105" deadCode="false" name="A223-INSERT-STATIC-TABLE">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_62F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10105" deadCode="false" name="A223-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_63F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10105" deadCode="false" name="A240-DELETE">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_66F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10105" deadCode="false" name="A240-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_67F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10105" deadCode="false" name="D000-CREA-SESSION-TABLE">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_4F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10105" deadCode="false" name="D000-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_5F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10105" deadCode="false" name="V100-VERIFICA-TEMP-TABLE">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_28F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10105" deadCode="false" name="V100-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_29F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10105" deadCode="true" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_68F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10105" deadCode="true" name="Z100-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_69F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10105" deadCode="true" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_64F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10105" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_65F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10105" deadCode="false" name="CALCOLA-PACKAGES">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_16F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10105" deadCode="false" name="CALCOLA-PACKAGES-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_17F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10105" deadCode="false" name="INIT-CAMPI-PACKAGE">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_70F10105"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10105" deadCode="false" name="INIT-CAMPI-PACKAGE-EX">
				<representations href="../../../cobol/IDSS0300.cbl.cobModel#P_71F10105"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_TEMPORARY_DATA" name="TEMPORARY_DATA">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_TEMPORARY_DATA"/>
		</children>
	</packageNode>
	<edges id="SC_1F10105P_1F10105" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10105" targetNode="P_1F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10105_I" deadCode="false" sourceNode="P_1F10105" targetNode="P_2F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_1F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10105_O" deadCode="false" sourceNode="P_1F10105" targetNode="P_3F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_1F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10105_I" deadCode="false" sourceNode="P_1F10105" targetNode="P_4F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_4F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10105_O" deadCode="false" sourceNode="P_1F10105" targetNode="P_5F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_4F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10105_I" deadCode="false" sourceNode="P_1F10105" targetNode="P_6F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_5F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10105_O" deadCode="false" sourceNode="P_1F10105" targetNode="P_7F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_5F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10105_I" deadCode="false" sourceNode="P_2F10105" targetNode="P_8F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_12F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10105_O" deadCode="false" sourceNode="P_2F10105" targetNode="P_9F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_12F10105"/>
	</edges>
	<edges id="P_2F10105P_3F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10105" targetNode="P_3F10105"/>
	<edges id="P_8F10105P_9F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10105" targetNode="P_9F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10105_I" deadCode="false" sourceNode="P_6F10105" targetNode="P_10F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_43F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10105_O" deadCode="false" sourceNode="P_6F10105" targetNode="P_11F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_43F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10105_I" deadCode="false" sourceNode="P_6F10105" targetNode="P_12F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_44F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10105_O" deadCode="false" sourceNode="P_6F10105" targetNode="P_13F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_44F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10105_I" deadCode="false" sourceNode="P_6F10105" targetNode="P_14F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_45F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10105_O" deadCode="false" sourceNode="P_6F10105" targetNode="P_15F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_45F10105"/>
	</edges>
	<edges id="P_6F10105P_7F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10105" targetNode="P_7F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10105_I" deadCode="false" sourceNode="P_10F10105" targetNode="P_16F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_52F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10105_O" deadCode="false" sourceNode="P_10F10105" targetNode="P_17F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_52F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10105_I" deadCode="false" sourceNode="P_10F10105" targetNode="P_18F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_53F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10105_O" deadCode="false" sourceNode="P_10F10105" targetNode="P_19F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_53F10105"/>
	</edges>
	<edges id="P_10F10105P_11F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10105" targetNode="P_11F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10105_I" deadCode="false" sourceNode="P_12F10105" targetNode="P_20F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_57F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10105_O" deadCode="false" sourceNode="P_12F10105" targetNode="P_21F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_57F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10105_I" deadCode="false" sourceNode="P_12F10105" targetNode="P_22F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_59F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10105_O" deadCode="false" sourceNode="P_12F10105" targetNode="P_23F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_59F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10105_I" deadCode="false" sourceNode="P_12F10105" targetNode="P_24F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_61F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10105_O" deadCode="false" sourceNode="P_12F10105" targetNode="P_25F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_61F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10105_I" deadCode="false" sourceNode="P_12F10105" targetNode="P_26F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_65F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10105_O" deadCode="false" sourceNode="P_12F10105" targetNode="P_27F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_65F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10105_I" deadCode="false" sourceNode="P_12F10105" targetNode="P_14F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_67F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10105_O" deadCode="false" sourceNode="P_12F10105" targetNode="P_15F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_67F10105"/>
	</edges>
	<edges id="P_12F10105P_13F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10105" targetNode="P_13F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10105_I" deadCode="false" sourceNode="P_18F10105" targetNode="P_28F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_69F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10105_O" deadCode="false" sourceNode="P_18F10105" targetNode="P_29F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_69F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10105_I" deadCode="false" sourceNode="P_18F10105" targetNode="P_30F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_71F10105"/>
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_74F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10105_O" deadCode="false" sourceNode="P_18F10105" targetNode="P_31F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_71F10105"/>
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_74F10105"/>
	</edges>
	<edges id="P_18F10105P_19F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10105" targetNode="P_19F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10105_I" deadCode="false" sourceNode="P_30F10105" targetNode="P_32F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_76F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10105_O" deadCode="false" sourceNode="P_30F10105" targetNode="P_33F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_76F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10105_I" deadCode="false" sourceNode="P_30F10105" targetNode="P_34F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_77F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10105_O" deadCode="false" sourceNode="P_30F10105" targetNode="P_35F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_77F10105"/>
	</edges>
	<edges id="P_30F10105P_31F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10105" targetNode="P_31F10105"/>
	<edges id="P_32F10105P_33F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10105" targetNode="P_33F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10105_I" deadCode="false" sourceNode="P_14F10105" targetNode="P_24F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_101F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10105_O" deadCode="false" sourceNode="P_14F10105" targetNode="P_25F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_101F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10105_I" deadCode="false" sourceNode="P_14F10105" targetNode="P_26F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_104F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10105_O" deadCode="false" sourceNode="P_14F10105" targetNode="P_27F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_104F10105"/>
	</edges>
	<edges id="P_14F10105P_15F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10105" targetNode="P_15F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10105_I" deadCode="false" sourceNode="P_20F10105" targetNode="P_36F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_107F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10105_O" deadCode="false" sourceNode="P_20F10105" targetNode="P_37F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_107F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10105_I" deadCode="false" sourceNode="P_20F10105" targetNode="P_38F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_108F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10105_O" deadCode="false" sourceNode="P_20F10105" targetNode="P_39F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_108F10105"/>
	</edges>
	<edges id="P_20F10105P_21F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10105" targetNode="P_21F10105"/>
	<edges id="P_36F10105P_37F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10105" targetNode="P_37F10105"/>
	<edges id="P_38F10105P_39F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10105" targetNode="P_39F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10105_I" deadCode="false" sourceNode="P_22F10105" targetNode="P_40F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_117F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10105_O" deadCode="false" sourceNode="P_22F10105" targetNode="P_41F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_117F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10105_I" deadCode="false" sourceNode="P_22F10105" targetNode="P_42F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_118F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10105_O" deadCode="false" sourceNode="P_22F10105" targetNode="P_43F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_118F10105"/>
	</edges>
	<edges id="P_22F10105P_23F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10105" targetNode="P_23F10105"/>
	<edges id="P_40F10105P_41F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10105" targetNode="P_41F10105"/>
	<edges id="P_42F10105P_43F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10105" targetNode="P_43F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10105_I" deadCode="false" sourceNode="P_24F10105" targetNode="P_44F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_133F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10105_O" deadCode="false" sourceNode="P_24F10105" targetNode="P_45F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_133F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_134F10105_I" deadCode="false" sourceNode="P_24F10105" targetNode="P_46F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_134F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_134F10105_O" deadCode="false" sourceNode="P_24F10105" targetNode="P_47F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_134F10105"/>
	</edges>
	<edges id="P_24F10105P_25F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10105" targetNode="P_25F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10105_I" deadCode="false" sourceNode="P_44F10105" targetNode="P_48F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_140F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10105_O" deadCode="false" sourceNode="P_44F10105" targetNode="P_49F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_140F10105"/>
	</edges>
	<edges id="P_44F10105P_45F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10105" targetNode="P_45F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10105_I" deadCode="false" sourceNode="P_46F10105" targetNode="P_48F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_153F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10105_O" deadCode="false" sourceNode="P_46F10105" targetNode="P_49F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_153F10105"/>
	</edges>
	<edges id="P_46F10105P_47F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10105" targetNode="P_47F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10105_I" deadCode="false" sourceNode="P_48F10105" targetNode="P_50F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_163F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10105_O" deadCode="false" sourceNode="P_48F10105" targetNode="P_51F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_163F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10105_I" deadCode="false" sourceNode="P_48F10105" targetNode="P_52F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_164F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10105_O" deadCode="false" sourceNode="P_48F10105" targetNode="P_53F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_164F10105"/>
	</edges>
	<edges id="P_48F10105P_49F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10105" targetNode="P_49F10105"/>
	<edges id="P_50F10105P_51F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10105" targetNode="P_51F10105"/>
	<edges id="P_52F10105P_53F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10105" targetNode="P_53F10105"/>
	<edges id="P_26F10105P_27F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10105" targetNode="P_27F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_190F10105_I" deadCode="false" sourceNode="P_54F10105" targetNode="P_55F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_190F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_190F10105_O" deadCode="false" sourceNode="P_54F10105" targetNode="P_56F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_190F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10105_I" deadCode="false" sourceNode="P_54F10105" targetNode="P_57F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_191F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10105_O" deadCode="false" sourceNode="P_54F10105" targetNode="P_58F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_191F10105"/>
	</edges>
	<edges id="P_54F10105P_59F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10105" targetNode="P_59F10105"/>
	<edges id="P_55F10105P_56F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_55F10105" targetNode="P_56F10105"/>
	<edges id="P_57F10105P_58F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_57F10105" targetNode="P_58F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_210F10105_I" deadCode="false" sourceNode="P_34F10105" targetNode="P_60F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_210F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_210F10105_O" deadCode="false" sourceNode="P_34F10105" targetNode="P_61F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_210F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10105_I" deadCode="false" sourceNode="P_34F10105" targetNode="P_62F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_211F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10105_O" deadCode="false" sourceNode="P_34F10105" targetNode="P_63F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_211F10105"/>
	</edges>
	<edges id="P_34F10105P_35F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10105" targetNode="P_35F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10105_I" deadCode="true" sourceNode="P_60F10105" targetNode="P_64F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_213F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10105_O" deadCode="true" sourceNode="P_60F10105" targetNode="P_65F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_213F10105"/>
	</edges>
	<edges id="P_60F10105P_61F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_60F10105" targetNode="P_61F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10105_I" deadCode="true" sourceNode="P_62F10105" targetNode="P_64F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_221F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10105_O" deadCode="true" sourceNode="P_62F10105" targetNode="P_65F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_221F10105"/>
	</edges>
	<edges id="P_62F10105P_63F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10105" targetNode="P_63F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_229F10105_I" deadCode="true" sourceNode="P_66F10105" targetNode="P_64F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_229F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_229F10105_O" deadCode="true" sourceNode="P_66F10105" targetNode="P_65F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_229F10105"/>
	</edges>
	<edges id="P_66F10105P_67F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10105" targetNode="P_67F10105"/>
	<edges id="P_4F10105P_5F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10105" targetNode="P_5F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_244F10105_I" deadCode="false" sourceNode="P_28F10105" targetNode="P_54F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_244F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_244F10105_O" deadCode="false" sourceNode="P_28F10105" targetNode="P_59F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_244F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_248F10105_I" deadCode="false" sourceNode="P_28F10105" targetNode="P_66F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_248F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_248F10105_O" deadCode="false" sourceNode="P_28F10105" targetNode="P_67F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_248F10105"/>
	</edges>
	<edges id="P_28F10105P_29F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10105" targetNode="P_29F10105"/>
	<edges id="P_64F10105P_65F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10105" targetNode="P_65F10105"/>
	<edges xsi:type="cbl:PerformEdge" id="S_290F10105_I" deadCode="false" sourceNode="P_16F10105" targetNode="P_70F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_290F10105"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_290F10105_O" deadCode="false" sourceNode="P_16F10105" targetNode="P_71F10105">
		<representations href="../../../cobol/IDSS0300.cbl.cobModel#S_290F10105"/>
	</edges>
	<edges id="P_16F10105P_17F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10105" targetNode="P_17F10105"/>
	<edges id="P_70F10105P_71F10105" xsi:type="cbl:FallThroughEdge" sourceNode="P_70F10105" targetNode="P_71F10105"/>
	<edges xsi:type="cbl:DataEdge" id="S_120F10105_POS1" deadCode="false" targetNode="P_40F10105" sourceNode="DB2_TEMPORARY_DATA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_120F10105"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_126F10105_POS1" deadCode="false" targetNode="P_42F10105" sourceNode="DB2_TEMPORARY_DATA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_126F10105"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_136F10105_POS1" deadCode="false" targetNode="P_44F10105" sourceNode="DB2_TEMPORARY_DATA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_136F10105"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_149F10105_POS1" deadCode="false" targetNode="P_46F10105" sourceNode="DB2_TEMPORARY_DATA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_149F10105"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_166F10105_POS1" deadCode="false" targetNode="P_50F10105" sourceNode="DB2_TEMPORARY_DATA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_166F10105"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_172F10105_POS1" deadCode="false" targetNode="P_52F10105" sourceNode="DB2_TEMPORARY_DATA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_172F10105"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_193F10105_POS1" deadCode="false" targetNode="P_55F10105" sourceNode="DB2_TEMPORARY_DATA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_193F10105"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_201F10105_POS1" deadCode="false" targetNode="P_57F10105" sourceNode="DB2_TEMPORARY_DATA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_201F10105"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_214F10105_POS1" deadCode="false" sourceNode="P_60F10105" targetNode="DB2_TEMPORARY_DATA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_214F10105"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_222F10105_POS1" deadCode="false" sourceNode="P_62F10105" targetNode="DB2_TEMPORARY_DATA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_222F10105"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_230F10105_POS1" deadCode="false" sourceNode="P_66F10105" targetNode="DB2_TEMPORARY_DATA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_230F10105"></representations>
	</edges>
</Package>

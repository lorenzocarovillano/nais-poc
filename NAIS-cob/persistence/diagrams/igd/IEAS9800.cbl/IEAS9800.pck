<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IEAS9800" cbl:id="IEAS9800" xsi:id="IEAS9800" packageRef="IEAS9800.igd#IEAS9800" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IEAS9800_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10108" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IEAS9800.cbl.cobModel#SC_1F10108"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10108" deadCode="false" name="1000-PRINCIPALE">
				<representations href="../../../cobol/IEAS9800.cbl.cobModel#P_1F10108"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10108" deadCode="false" name="1000-PRINCIPALE-EXIT">
				<representations href="../../../cobol/IEAS9800.cbl.cobModel#P_8F10108"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10108" deadCode="false" name="1100-ESTRAI-PARAMETRI">
				<representations href="../../../cobol/IEAS9800.cbl.cobModel#P_2F10108"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10108" deadCode="false" name="1100-ESTRAI-PARAMETRI-EXIT">
				<representations href="../../../cobol/IEAS9800.cbl.cobModel#P_3F10108"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10108" deadCode="false" name="1200-RICERCA-DOLLARO">
				<representations href="../../../cobol/IEAS9800.cbl.cobModel#P_4F10108"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10108" deadCode="false" name="1200-RICERCA-DOLLARO-EXIT">
				<representations href="../../../cobol/IEAS9800.cbl.cobModel#P_5F10108"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10108" deadCode="false" name="1300-VALORIZZA-OUTPUT">
				<representations href="../../../cobol/IEAS9800.cbl.cobModel#P_6F10108"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10108" deadCode="false" name="1300-VALORIZZA-OUTPUT-EXIT">
				<representations href="../../../cobol/IEAS9800.cbl.cobModel#P_7F10108"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10108P_1F10108" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10108" targetNode="P_1F10108"/>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10108_I" deadCode="false" sourceNode="P_1F10108" targetNode="P_2F10108">
		<representations href="../../../cobol/IEAS9800.cbl.cobModel#S_4F10108"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10108_O" deadCode="false" sourceNode="P_1F10108" targetNode="P_3F10108">
		<representations href="../../../cobol/IEAS9800.cbl.cobModel#S_4F10108"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10108_I" deadCode="false" sourceNode="P_1F10108" targetNode="P_4F10108">
		<representations href="../../../cobol/IEAS9800.cbl.cobModel#S_6F10108"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10108_O" deadCode="false" sourceNode="P_1F10108" targetNode="P_5F10108">
		<representations href="../../../cobol/IEAS9800.cbl.cobModel#S_6F10108"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10108_I" deadCode="false" sourceNode="P_1F10108" targetNode="P_6F10108">
		<representations href="../../../cobol/IEAS9800.cbl.cobModel#S_8F10108"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10108_O" deadCode="false" sourceNode="P_1F10108" targetNode="P_7F10108">
		<representations href="../../../cobol/IEAS9800.cbl.cobModel#S_8F10108"/>
	</edges>
	<edges id="P_1F10108P_8F10108" xsi:type="cbl:FallThroughEdge" sourceNode="P_1F10108" targetNode="P_8F10108"/>
	<edges id="P_2F10108P_3F10108" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10108" targetNode="P_3F10108"/>
	<edges id="P_4F10108P_5F10108" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10108" targetNode="P_5F10108"/>
	<edges id="P_6F10108P_7F10108" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10108" targetNode="P_7F10108"/>
</Package>

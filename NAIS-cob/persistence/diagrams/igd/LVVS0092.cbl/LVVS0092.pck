<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0092" cbl:id="LVVS0092" xsi:id="LVVS0092" packageRef="LVVS0092.igd#LVVS0092" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0092_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10341" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0092.cbl.cobModel#SC_1F10341"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10341" deadCode="false" name="PROGRAM_LVVS0092_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0092.cbl.cobModel#P_1F10341"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10341" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0092.cbl.cobModel#P_2F10341"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10341" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0092.cbl.cobModel#P_3F10341"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10341" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0092.cbl.cobModel#P_4F10341"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10341" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0092.cbl.cobModel#P_5F10341"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10341" deadCode="true" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0092.cbl.cobModel#P_8F10341"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10341" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0092.cbl.cobModel#P_9F10341"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10341" deadCode="false" name="S1250-CALCOLA-DATA1">
				<representations href="../../../cobol/LVVS0092.cbl.cobModel#P_10F10341"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10341" deadCode="false" name="S1250-EX">
				<representations href="../../../cobol/LVVS0092.cbl.cobModel#P_11F10341"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10341" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0092.cbl.cobModel#P_6F10341"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10341" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0092.cbl.cobModel#P_7F10341"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS2910" name="LDBS2910">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10190"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10341P_1F10341" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10341" targetNode="P_1F10341"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10341_I" deadCode="false" sourceNode="P_1F10341" targetNode="P_2F10341">
		<representations href="../../../cobol/LVVS0092.cbl.cobModel#S_1F10341"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10341_O" deadCode="false" sourceNode="P_1F10341" targetNode="P_3F10341">
		<representations href="../../../cobol/LVVS0092.cbl.cobModel#S_1F10341"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10341_I" deadCode="false" sourceNode="P_1F10341" targetNode="P_4F10341">
		<representations href="../../../cobol/LVVS0092.cbl.cobModel#S_2F10341"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10341_O" deadCode="false" sourceNode="P_1F10341" targetNode="P_5F10341">
		<representations href="../../../cobol/LVVS0092.cbl.cobModel#S_2F10341"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10341_I" deadCode="false" sourceNode="P_1F10341" targetNode="P_6F10341">
		<representations href="../../../cobol/LVVS0092.cbl.cobModel#S_3F10341"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10341_O" deadCode="false" sourceNode="P_1F10341" targetNode="P_7F10341">
		<representations href="../../../cobol/LVVS0092.cbl.cobModel#S_3F10341"/>
	</edges>
	<edges id="P_2F10341P_3F10341" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10341" targetNode="P_3F10341"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10341_I" deadCode="true" sourceNode="P_4F10341" targetNode="P_8F10341">
		<representations href="../../../cobol/LVVS0092.cbl.cobModel#S_11F10341"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10341_O" deadCode="true" sourceNode="P_4F10341" targetNode="P_9F10341">
		<representations href="../../../cobol/LVVS0092.cbl.cobModel#S_11F10341"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10341_I" deadCode="false" sourceNode="P_4F10341" targetNode="P_10F10341">
		<representations href="../../../cobol/LVVS0092.cbl.cobModel#S_15F10341"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10341_O" deadCode="false" sourceNode="P_4F10341" targetNode="P_11F10341">
		<representations href="../../../cobol/LVVS0092.cbl.cobModel#S_15F10341"/>
	</edges>
	<edges id="P_4F10341P_5F10341" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10341" targetNode="P_5F10341"/>
	<edges id="P_8F10341P_9F10341" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10341" targetNode="P_9F10341"/>
	<edges id="P_10F10341P_11F10341" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10341" targetNode="P_11F10341"/>
	<edges xsi:type="cbl:CallEdge" id="S_21F10341" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_10F10341" targetNode="LDBS2910">
		<representations href="../../../cobol/../importantStmts.cobModel#S_21F10341"></representations>
	</edges>
</Package>

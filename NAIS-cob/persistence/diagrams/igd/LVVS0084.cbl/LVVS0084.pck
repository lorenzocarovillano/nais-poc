<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0084" cbl:id="LVVS0084" xsi:id="LVVS0084" packageRef="LVVS0084.igd#LVVS0084" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0084_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10335" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0084.cbl.cobModel#SC_1F10335"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10335" deadCode="false" name="PROGRAM_LVVS0084_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0084.cbl.cobModel#P_1F10335"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10335" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0084.cbl.cobModel#P_2F10335"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10335" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0084.cbl.cobModel#P_3F10335"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10335" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0084.cbl.cobModel#P_4F10335"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10335" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0084.cbl.cobModel#P_5F10335"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10335" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0084.cbl.cobModel#P_8F10335"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10335" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0084.cbl.cobModel#P_9F10335"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10335" deadCode="false" name="S1250-CALCOLA-DATA1">
				<representations href="../../../cobol/LVVS0084.cbl.cobModel#P_12F10335"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10335" deadCode="false" name="S1250-EX">
				<representations href="../../../cobol/LVVS0084.cbl.cobModel#P_13F10335"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10335" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS0084.cbl.cobModel#P_10F10335"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10335" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS0084.cbl.cobModel#P_11F10335"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10335" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0084.cbl.cobModel#P_6F10335"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10335" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0084.cbl.cobModel#P_7F10335"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10335P_1F10335" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10335" targetNode="P_1F10335"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10335_I" deadCode="false" sourceNode="P_1F10335" targetNode="P_2F10335">
		<representations href="../../../cobol/LVVS0084.cbl.cobModel#S_1F10335"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10335_O" deadCode="false" sourceNode="P_1F10335" targetNode="P_3F10335">
		<representations href="../../../cobol/LVVS0084.cbl.cobModel#S_1F10335"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10335_I" deadCode="false" sourceNode="P_1F10335" targetNode="P_4F10335">
		<representations href="../../../cobol/LVVS0084.cbl.cobModel#S_2F10335"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10335_O" deadCode="false" sourceNode="P_1F10335" targetNode="P_5F10335">
		<representations href="../../../cobol/LVVS0084.cbl.cobModel#S_2F10335"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10335_I" deadCode="false" sourceNode="P_1F10335" targetNode="P_6F10335">
		<representations href="../../../cobol/LVVS0084.cbl.cobModel#S_3F10335"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10335_O" deadCode="false" sourceNode="P_1F10335" targetNode="P_7F10335">
		<representations href="../../../cobol/LVVS0084.cbl.cobModel#S_3F10335"/>
	</edges>
	<edges id="P_2F10335P_3F10335" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10335" targetNode="P_3F10335"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10335_I" deadCode="false" sourceNode="P_4F10335" targetNode="P_8F10335">
		<representations href="../../../cobol/LVVS0084.cbl.cobModel#S_10F10335"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10335_O" deadCode="false" sourceNode="P_4F10335" targetNode="P_9F10335">
		<representations href="../../../cobol/LVVS0084.cbl.cobModel#S_10F10335"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10335_I" deadCode="false" sourceNode="P_4F10335" targetNode="P_10F10335">
		<representations href="../../../cobol/LVVS0084.cbl.cobModel#S_12F10335"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10335_O" deadCode="false" sourceNode="P_4F10335" targetNode="P_11F10335">
		<representations href="../../../cobol/LVVS0084.cbl.cobModel#S_12F10335"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10335_I" deadCode="false" sourceNode="P_4F10335" targetNode="P_12F10335">
		<representations href="../../../cobol/LVVS0084.cbl.cobModel#S_14F10335"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10335_O" deadCode="false" sourceNode="P_4F10335" targetNode="P_13F10335">
		<representations href="../../../cobol/LVVS0084.cbl.cobModel#S_14F10335"/>
	</edges>
	<edges id="P_4F10335P_5F10335" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10335" targetNode="P_5F10335"/>
	<edges id="P_8F10335P_9F10335" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10335" targetNode="P_9F10335"/>
	<edges id="P_12F10335P_13F10335" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10335" targetNode="P_13F10335"/>
	<edges id="P_10F10335P_11F10335" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10335" targetNode="P_11F10335"/>
</Package>

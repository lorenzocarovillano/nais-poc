<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LCCS0033" cbl:id="LCCS0033" xsi:id="LCCS0033" packageRef="LCCS0033.igd#LCCS0033" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LCCS0033_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10130" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LCCS0033.cbl.cobModel#SC_1F10130"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10130" deadCode="false" name="PROGRAM_LCCS0033_FIRST_SENTENCES">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_1F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10130" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_2F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10130" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_3F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10130" deadCode="false" name="S0005-CTRL-DATI-INPUT">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_8F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10130" deadCode="false" name="EX-S0005">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_9F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10130" deadCode="false" name="S0010-LETTURA-PCO">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_10F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10130" deadCode="false" name="S0010-EX">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_11F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10130" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_4F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10130" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_5F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10130" deadCode="false" name="S1015-ELAB-ALTRE-POL-CONTR">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_16F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10130" deadCode="false" name="S1015-EX">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_17F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10130" deadCode="false" name="S1020-CTRL-POL-REC-PROVV">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_20F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10130" deadCode="false" name="S1020-EX">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_21F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10130" deadCode="false" name="S1025-CTRL-STATO-CAUSALE">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_22F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10130" deadCode="false" name="S1025-EX">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_23F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10130" deadCode="false" name="S1022-LETTURA-STB">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_34F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10130" deadCode="false" name="S1022-EX">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_35F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10130" deadCode="false" name="S1030-CTRL-EFFETTO-STORNO">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_24F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10130" deadCode="false" name="S1030-EX">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_25F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10130" deadCode="false" name="S1032-CTRL-EFFETTO-EMIS">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_26F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10130" deadCode="false" name="S1032-EX">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_27F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10130" deadCode="false" name="S1040-CTRL-PROD-PREMIO-ANNUO">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_28F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10130" deadCode="false" name="S1040-EX">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_29F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10130" deadCode="false" name="S1041-LETTURA-ADE">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_38F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10130" deadCode="false" name="S1041-EX">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_39F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10130" deadCode="false" name="S1042-LETTURA-GAR">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_40F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10130" deadCode="false" name="S1042-EX">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_41F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10130" deadCode="false" name="S1045-CALC-PREMIO-ANNUO">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_30F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10130" deadCode="false" name="S1045-EX">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_31F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10130" deadCode="false" name="S1048-LETTURA-TGA">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_44F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10130" deadCode="false" name="S1048-EX">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_45F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10130" deadCode="false" name="S1050-SUM-IMP-COLLG">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_46F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10130" deadCode="false" name="S1050-EX">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_47F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10130" deadCode="false" name="S1055-VAL-LISTA-ALTRE-POL">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_32F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10130" deadCode="false" name="S1055-EX">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_33F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10130" deadCode="false" name="S8100-CLOSE-CURSOR">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_42F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10130" deadCode="false" name="S8100-EX">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_43F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10130" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_6F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10130" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_7F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10130" deadCode="false" name="CALL-LCCS0003">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_36F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10130" deadCode="false" name="CALL-LCCS0003-EX">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_37F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10130" deadCode="false" name="S1025-ABILITA-TASTO">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_18F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10130" deadCode="false" name="S1025-ABILITA-TASTO-EX">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_19F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10130" deadCode="false" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_48F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10130" deadCode="false" name="EX-S0290">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_49F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10130" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_14F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10130" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_15F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10130" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_52F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10130" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_53F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10130" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_50F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10130" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_51F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10130" deadCode="false" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_12F10130"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10130" deadCode="false" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LCCS0033.cbl.cobModel#P_13F10130"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0003" name="LCCS0003">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10119"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0010" name="IDSS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10099"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10130P_1F10130" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10130" targetNode="P_1F10130"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10130_I" deadCode="false" sourceNode="P_1F10130" targetNode="P_2F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_1F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10130_O" deadCode="false" sourceNode="P_1F10130" targetNode="P_3F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_1F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10130_I" deadCode="false" sourceNode="P_1F10130" targetNode="P_4F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_3F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10130_O" deadCode="false" sourceNode="P_1F10130" targetNode="P_5F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_3F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10130_I" deadCode="false" sourceNode="P_1F10130" targetNode="P_6F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_4F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10130_O" deadCode="false" sourceNode="P_1F10130" targetNode="P_7F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_4F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10130_I" deadCode="false" sourceNode="P_2F10130" targetNode="P_8F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_6F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10130_O" deadCode="false" sourceNode="P_2F10130" targetNode="P_9F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_6F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10130_I" deadCode="false" sourceNode="P_2F10130" targetNode="P_10F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_8F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10130_O" deadCode="false" sourceNode="P_2F10130" targetNode="P_11F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_8F10130"/>
	</edges>
	<edges id="P_2F10130P_3F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10130" targetNode="P_3F10130"/>
	<edges id="P_8F10130P_9F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10130" targetNode="P_9F10130"/>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10130_I" deadCode="false" sourceNode="P_10F10130" targetNode="P_12F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_24F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10130_O" deadCode="false" sourceNode="P_10F10130" targetNode="P_13F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_24F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10130_I" deadCode="false" sourceNode="P_10F10130" targetNode="P_14F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_31F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10130_O" deadCode="false" sourceNode="P_10F10130" targetNode="P_15F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_31F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10130_I" deadCode="false" sourceNode="P_10F10130" targetNode="P_14F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_37F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10130_O" deadCode="false" sourceNode="P_10F10130" targetNode="P_15F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_37F10130"/>
	</edges>
	<edges id="P_10F10130P_11F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10130" targetNode="P_11F10130"/>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10130_I" deadCode="false" sourceNode="P_4F10130" targetNode="P_16F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_40F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10130_O" deadCode="false" sourceNode="P_4F10130" targetNode="P_17F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_40F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10130_I" deadCode="false" sourceNode="P_4F10130" targetNode="P_18F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_42F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10130_O" deadCode="false" sourceNode="P_4F10130" targetNode="P_19F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_42F10130"/>
	</edges>
	<edges id="P_4F10130P_5F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10130" targetNode="P_5F10130"/>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10130_I" deadCode="false" sourceNode="P_16F10130" targetNode="P_12F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_50F10130"/>
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_60F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10130_O" deadCode="false" sourceNode="P_16F10130" targetNode="P_13F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_50F10130"/>
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_60F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10130_I" deadCode="false" sourceNode="P_16F10130" targetNode="P_20F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_50F10130"/>
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_65F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10130_O" deadCode="false" sourceNode="P_16F10130" targetNode="P_21F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_50F10130"/>
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_65F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10130_I" deadCode="false" sourceNode="P_16F10130" targetNode="P_14F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_50F10130"/>
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_75F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10130_O" deadCode="false" sourceNode="P_16F10130" targetNode="P_15F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_50F10130"/>
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_75F10130"/>
	</edges>
	<edges id="P_16F10130P_17F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10130" targetNode="P_17F10130"/>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10130_I" deadCode="false" sourceNode="P_20F10130" targetNode="P_22F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_78F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10130_O" deadCode="false" sourceNode="P_20F10130" targetNode="P_23F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_78F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10130_I" deadCode="false" sourceNode="P_20F10130" targetNode="P_24F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_81F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10130_O" deadCode="false" sourceNode="P_20F10130" targetNode="P_25F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_81F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10130_I" deadCode="false" sourceNode="P_20F10130" targetNode="P_26F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_83F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10130_O" deadCode="false" sourceNode="P_20F10130" targetNode="P_27F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_83F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10130_I" deadCode="false" sourceNode="P_20F10130" targetNode="P_28F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_85F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10130_O" deadCode="false" sourceNode="P_20F10130" targetNode="P_29F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_85F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10130_I" deadCode="false" sourceNode="P_20F10130" targetNode="P_30F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_87F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10130_O" deadCode="false" sourceNode="P_20F10130" targetNode="P_31F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_87F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10130_I" deadCode="false" sourceNode="P_20F10130" targetNode="P_32F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_89F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10130_O" deadCode="false" sourceNode="P_20F10130" targetNode="P_33F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_89F10130"/>
	</edges>
	<edges id="P_20F10130P_21F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10130" targetNode="P_21F10130"/>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10130_I" deadCode="false" sourceNode="P_22F10130" targetNode="P_34F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_93F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10130_O" deadCode="false" sourceNode="P_22F10130" targetNode="P_35F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_93F10130"/>
	</edges>
	<edges id="P_22F10130P_23F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10130" targetNode="P_23F10130"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10130_I" deadCode="false" sourceNode="P_34F10130" targetNode="P_12F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_117F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10130_O" deadCode="false" sourceNode="P_34F10130" targetNode="P_13F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_117F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10130_I" deadCode="false" sourceNode="P_34F10130" targetNode="P_14F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_126F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10130_O" deadCode="false" sourceNode="P_34F10130" targetNode="P_15F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_126F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10130_I" deadCode="false" sourceNode="P_34F10130" targetNode="P_14F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_131F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10130_O" deadCode="false" sourceNode="P_34F10130" targetNode="P_15F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_131F10130"/>
	</edges>
	<edges id="P_34F10130P_35F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10130" targetNode="P_35F10130"/>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10130_I" deadCode="false" sourceNode="P_24F10130" targetNode="P_36F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_139F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10130_O" deadCode="false" sourceNode="P_24F10130" targetNode="P_37F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_139F10130"/>
	</edges>
	<edges id="P_24F10130P_25F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10130" targetNode="P_25F10130"/>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10130_I" deadCode="false" sourceNode="P_26F10130" targetNode="P_36F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_153F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10130_O" deadCode="false" sourceNode="P_26F10130" targetNode="P_37F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_153F10130"/>
	</edges>
	<edges id="P_26F10130P_27F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10130" targetNode="P_27F10130"/>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10130_I" deadCode="false" sourceNode="P_28F10130" targetNode="P_38F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_161F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10130_O" deadCode="false" sourceNode="P_28F10130" targetNode="P_39F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_161F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10130_I" deadCode="false" sourceNode="P_28F10130" targetNode="P_40F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_163F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10130_O" deadCode="false" sourceNode="P_28F10130" targetNode="P_41F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_163F10130"/>
	</edges>
	<edges id="P_28F10130P_29F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10130" targetNode="P_29F10130"/>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10130_I" deadCode="false" sourceNode="P_38F10130" targetNode="P_12F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_181F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10130_O" deadCode="false" sourceNode="P_38F10130" targetNode="P_13F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_181F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10130_I" deadCode="false" sourceNode="P_38F10130" targetNode="P_42F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_185F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10130_O" deadCode="false" sourceNode="P_38F10130" targetNode="P_43F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_185F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_190F10130_I" deadCode="false" sourceNode="P_38F10130" targetNode="P_14F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_190F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_190F10130_O" deadCode="false" sourceNode="P_38F10130" targetNode="P_15F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_190F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10130_I" deadCode="false" sourceNode="P_38F10130" targetNode="P_14F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_195F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10130_O" deadCode="false" sourceNode="P_38F10130" targetNode="P_15F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_195F10130"/>
	</edges>
	<edges id="P_38F10130P_39F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10130" targetNode="P_39F10130"/>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10130_I" deadCode="false" sourceNode="P_40F10130" targetNode="P_12F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_210F10130"/>
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_211F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10130_O" deadCode="false" sourceNode="P_40F10130" targetNode="P_13F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_210F10130"/>
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_211F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_219F10130_I" deadCode="false" sourceNode="P_40F10130" targetNode="P_42F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_210F10130"/>
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_219F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_219F10130_O" deadCode="false" sourceNode="P_40F10130" targetNode="P_43F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_210F10130"/>
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_219F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_226F10130_I" deadCode="false" sourceNode="P_40F10130" targetNode="P_14F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_210F10130"/>
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_226F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_226F10130_O" deadCode="false" sourceNode="P_40F10130" targetNode="P_15F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_210F10130"/>
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_226F10130"/>
	</edges>
	<edges id="P_40F10130P_41F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10130" targetNode="P_41F10130"/>
	<edges xsi:type="cbl:PerformEdge" id="S_229F10130_I" deadCode="false" sourceNode="P_30F10130" targetNode="P_44F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_229F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_229F10130_O" deadCode="false" sourceNode="P_30F10130" targetNode="P_45F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_229F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10130_I" deadCode="false" sourceNode="P_30F10130" targetNode="P_46F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_231F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10130_O" deadCode="false" sourceNode="P_30F10130" targetNode="P_47F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_231F10130"/>
	</edges>
	<edges id="P_30F10130P_31F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10130" targetNode="P_31F10130"/>
	<edges xsi:type="cbl:PerformEdge" id="S_256F10130_I" deadCode="false" sourceNode="P_44F10130" targetNode="P_12F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_255F10130"/>
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_256F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_256F10130_O" deadCode="false" sourceNode="P_44F10130" targetNode="P_13F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_255F10130"/>
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_256F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_269F10130_I" deadCode="false" sourceNode="P_44F10130" targetNode="P_14F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_255F10130"/>
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_269F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_269F10130_O" deadCode="false" sourceNode="P_44F10130" targetNode="P_15F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_255F10130"/>
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_269F10130"/>
	</edges>
	<edges id="P_44F10130P_45F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10130" targetNode="P_45F10130"/>
	<edges xsi:type="cbl:PerformEdge" id="S_290F10130_I" deadCode="false" sourceNode="P_46F10130" targetNode="P_12F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_290F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_290F10130_O" deadCode="false" sourceNode="P_46F10130" targetNode="P_13F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_290F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_298F10130_I" deadCode="false" sourceNode="P_46F10130" targetNode="P_14F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_298F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_298F10130_O" deadCode="false" sourceNode="P_46F10130" targetNode="P_15F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_298F10130"/>
	</edges>
	<edges id="P_46F10130P_47F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10130" targetNode="P_47F10130"/>
	<edges id="P_32F10130P_33F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10130" targetNode="P_33F10130"/>
	<edges xsi:type="cbl:PerformEdge" id="S_312F10130_I" deadCode="false" sourceNode="P_42F10130" targetNode="P_12F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_312F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_312F10130_O" deadCode="false" sourceNode="P_42F10130" targetNode="P_13F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_312F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_319F10130_I" deadCode="false" sourceNode="P_42F10130" targetNode="P_14F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_319F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_319F10130_O" deadCode="false" sourceNode="P_42F10130" targetNode="P_15F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_319F10130"/>
	</edges>
	<edges id="P_42F10130P_43F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10130" targetNode="P_43F10130"/>
	<edges xsi:type="cbl:PerformEdge" id="S_327F10130_I" deadCode="false" sourceNode="P_36F10130" targetNode="P_48F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_327F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_327F10130_O" deadCode="false" sourceNode="P_36F10130" targetNode="P_49F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_327F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_334F10130_I" deadCode="false" sourceNode="P_36F10130" targetNode="P_14F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_334F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_334F10130_O" deadCode="false" sourceNode="P_36F10130" targetNode="P_15F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_334F10130"/>
	</edges>
	<edges id="P_36F10130P_37F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10130" targetNode="P_37F10130"/>
	<edges id="P_18F10130P_19F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10130" targetNode="P_19F10130"/>
	<edges xsi:type="cbl:PerformEdge" id="S_343F10130_I" deadCode="false" sourceNode="P_48F10130" targetNode="P_14F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_343F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_343F10130_O" deadCode="false" sourceNode="P_48F10130" targetNode="P_15F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_343F10130"/>
	</edges>
	<edges id="P_48F10130P_49F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10130" targetNode="P_49F10130"/>
	<edges xsi:type="cbl:PerformEdge" id="S_350F10130_I" deadCode="false" sourceNode="P_14F10130" targetNode="P_50F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_350F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_350F10130_O" deadCode="false" sourceNode="P_14F10130" targetNode="P_51F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_350F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_354F10130_I" deadCode="false" sourceNode="P_14F10130" targetNode="P_52F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_354F10130"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_354F10130_O" deadCode="false" sourceNode="P_14F10130" targetNode="P_53F10130">
		<representations href="../../../cobol/LCCS0033.cbl.cobModel#S_354F10130"/>
	</edges>
	<edges id="P_14F10130P_15F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10130" targetNode="P_15F10130"/>
	<edges id="P_52F10130P_53F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10130" targetNode="P_53F10130"/>
	<edges id="P_50F10130P_51F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10130" targetNode="P_51F10130"/>
	<edges id="P_12F10130P_13F10130" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10130" targetNode="P_13F10130"/>
	<edges xsi:type="cbl:CallEdge" id="S_323F10130" deadCode="false" name="Dynamic LCCS0003" sourceNode="P_36F10130" targetNode="LCCS0003">
		<representations href="../../../cobol/../importantStmts.cobModel#S_323F10130"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_348F10130" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_14F10130" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_348F10130"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_414F10130" deadCode="false" name="Dynamic IDSI0011-PGM" sourceNode="P_12F10130" targetNode="IDSS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_414F10130"></representations>
	</edges>
</Package>

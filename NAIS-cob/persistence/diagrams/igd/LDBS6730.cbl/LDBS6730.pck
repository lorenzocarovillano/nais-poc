<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS6730" cbl:id="LDBS6730" xsi:id="LDBS6730" packageRef="LDBS6730.igd#LDBS6730" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS6730_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10235" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS6730.cbl.cobModel#SC_1F10235"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10235" deadCode="false" name="PROGRAM_LDBS6730_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_1F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10235" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_2F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10235" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_3F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10235" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_12F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10235" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_13F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10235" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_4F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10235" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_5F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10235" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_6F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10235" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_7F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10235" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_8F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10235" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_9F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10235" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_44F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10235" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_49F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10235" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_14F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10235" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_15F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10235" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_16F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10235" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_17F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10235" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_18F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10235" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_19F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10235" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_20F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10235" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_21F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10235" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_22F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10235" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_23F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10235" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_50F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10235" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_51F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10235" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_24F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10235" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_25F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10235" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_26F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10235" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_27F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10235" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_28F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10235" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_29F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10235" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_30F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10235" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_31F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10235" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_32F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10235" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_33F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10235" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_52F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10235" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_53F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10235" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_34F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10235" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_35F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10235" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_36F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10235" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_37F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10235" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_38F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10235" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_39F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10235" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_40F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10235" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_41F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10235" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_42F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10235" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_43F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10235" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_54F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10235" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_55F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10235" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_60F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10235" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_61F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10235" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_56F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10235" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_57F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10235" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_45F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10235" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_46F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10235" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_47F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10235" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_48F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10235" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_58F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10235" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_59F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10235" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_10F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10235" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_11F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10235" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_62F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10235" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_63F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10235" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_64F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10235" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_65F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10235" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_66F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10235" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_67F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10235" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_68F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10235" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_69F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10235" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_70F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10235" deadCode="true" name="Z800-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_75F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10235" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_71F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10235" deadCode="true" name="Z810-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_72F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10235" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_73F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10235" deadCode="true" name="Z820-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_74F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10235" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_76F10235"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10235" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS6730.cbl.cobModel#P_77F10235"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_PARAM_INFR_APPL" name="PARAM_INFR_APPL">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_PARAM_INFR_APPL"/>
		</children>
	</packageNode>
	<edges id="SC_1F10235P_1F10235" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10235" targetNode="P_1F10235"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10235_I" deadCode="false" sourceNode="P_1F10235" targetNode="P_2F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_1F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10235_O" deadCode="false" sourceNode="P_1F10235" targetNode="P_3F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_1F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10235_I" deadCode="false" sourceNode="P_1F10235" targetNode="P_4F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_5F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10235_O" deadCode="false" sourceNode="P_1F10235" targetNode="P_5F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_5F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10235_I" deadCode="false" sourceNode="P_1F10235" targetNode="P_6F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_9F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10235_O" deadCode="false" sourceNode="P_1F10235" targetNode="P_7F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_9F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10235_I" deadCode="false" sourceNode="P_1F10235" targetNode="P_8F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_13F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10235_O" deadCode="false" sourceNode="P_1F10235" targetNode="P_9F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_13F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10235_I" deadCode="false" sourceNode="P_2F10235" targetNode="P_10F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_22F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10235_O" deadCode="false" sourceNode="P_2F10235" targetNode="P_11F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_22F10235"/>
	</edges>
	<edges id="P_2F10235P_3F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10235" targetNode="P_3F10235"/>
	<edges id="P_12F10235P_13F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10235" targetNode="P_13F10235"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10235_I" deadCode="false" sourceNode="P_4F10235" targetNode="P_14F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_35F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10235_O" deadCode="false" sourceNode="P_4F10235" targetNode="P_15F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_35F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10235_I" deadCode="false" sourceNode="P_4F10235" targetNode="P_16F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_36F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10235_O" deadCode="false" sourceNode="P_4F10235" targetNode="P_17F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_36F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10235_I" deadCode="false" sourceNode="P_4F10235" targetNode="P_18F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_37F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10235_O" deadCode="false" sourceNode="P_4F10235" targetNode="P_19F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_37F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10235_I" deadCode="false" sourceNode="P_4F10235" targetNode="P_20F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_38F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10235_O" deadCode="false" sourceNode="P_4F10235" targetNode="P_21F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_38F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10235_I" deadCode="false" sourceNode="P_4F10235" targetNode="P_22F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_39F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10235_O" deadCode="false" sourceNode="P_4F10235" targetNode="P_23F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_39F10235"/>
	</edges>
	<edges id="P_4F10235P_5F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10235" targetNode="P_5F10235"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10235_I" deadCode="false" sourceNode="P_6F10235" targetNode="P_24F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_43F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10235_O" deadCode="false" sourceNode="P_6F10235" targetNode="P_25F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_43F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10235_I" deadCode="false" sourceNode="P_6F10235" targetNode="P_26F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_44F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10235_O" deadCode="false" sourceNode="P_6F10235" targetNode="P_27F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_44F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10235_I" deadCode="false" sourceNode="P_6F10235" targetNode="P_28F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_45F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10235_O" deadCode="false" sourceNode="P_6F10235" targetNode="P_29F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_45F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10235_I" deadCode="false" sourceNode="P_6F10235" targetNode="P_30F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_46F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10235_O" deadCode="false" sourceNode="P_6F10235" targetNode="P_31F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_46F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10235_I" deadCode="false" sourceNode="P_6F10235" targetNode="P_32F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_47F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10235_O" deadCode="false" sourceNode="P_6F10235" targetNode="P_33F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_47F10235"/>
	</edges>
	<edges id="P_6F10235P_7F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10235" targetNode="P_7F10235"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10235_I" deadCode="false" sourceNode="P_8F10235" targetNode="P_34F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_51F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10235_O" deadCode="false" sourceNode="P_8F10235" targetNode="P_35F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_51F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10235_I" deadCode="false" sourceNode="P_8F10235" targetNode="P_36F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_52F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10235_O" deadCode="false" sourceNode="P_8F10235" targetNode="P_37F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_52F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10235_I" deadCode="false" sourceNode="P_8F10235" targetNode="P_38F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_53F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10235_O" deadCode="false" sourceNode="P_8F10235" targetNode="P_39F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_53F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10235_I" deadCode="false" sourceNode="P_8F10235" targetNode="P_40F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_54F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10235_O" deadCode="false" sourceNode="P_8F10235" targetNode="P_41F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_54F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10235_I" deadCode="false" sourceNode="P_8F10235" targetNode="P_42F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_55F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10235_O" deadCode="false" sourceNode="P_8F10235" targetNode="P_43F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_55F10235"/>
	</edges>
	<edges id="P_8F10235P_9F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10235" targetNode="P_9F10235"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10235_I" deadCode="false" sourceNode="P_44F10235" targetNode="P_45F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_58F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10235_O" deadCode="false" sourceNode="P_44F10235" targetNode="P_46F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_58F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10235_I" deadCode="false" sourceNode="P_44F10235" targetNode="P_47F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_59F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10235_O" deadCode="false" sourceNode="P_44F10235" targetNode="P_48F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_59F10235"/>
	</edges>
	<edges id="P_44F10235P_49F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10235" targetNode="P_49F10235"/>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10235_I" deadCode="false" sourceNode="P_14F10235" targetNode="P_45F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_62F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10235_O" deadCode="false" sourceNode="P_14F10235" targetNode="P_46F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_62F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10235_I" deadCode="false" sourceNode="P_14F10235" targetNode="P_47F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_63F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10235_O" deadCode="false" sourceNode="P_14F10235" targetNode="P_48F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_63F10235"/>
	</edges>
	<edges id="P_14F10235P_15F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10235" targetNode="P_15F10235"/>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10235_I" deadCode="false" sourceNode="P_16F10235" targetNode="P_44F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_66F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10235_O" deadCode="false" sourceNode="P_16F10235" targetNode="P_49F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_66F10235"/>
	</edges>
	<edges id="P_16F10235P_17F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10235" targetNode="P_17F10235"/>
	<edges id="P_18F10235P_19F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10235" targetNode="P_19F10235"/>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10235_I" deadCode="false" sourceNode="P_20F10235" targetNode="P_16F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_71F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10235_O" deadCode="false" sourceNode="P_20F10235" targetNode="P_17F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_71F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10235_I" deadCode="false" sourceNode="P_20F10235" targetNode="P_22F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_73F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10235_O" deadCode="false" sourceNode="P_20F10235" targetNode="P_23F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_73F10235"/>
	</edges>
	<edges id="P_20F10235P_21F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10235" targetNode="P_21F10235"/>
	<edges id="P_22F10235P_23F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10235" targetNode="P_23F10235"/>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10235_I" deadCode="false" sourceNode="P_50F10235" targetNode="P_45F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_77F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10235_O" deadCode="false" sourceNode="P_50F10235" targetNode="P_46F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_77F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10235_I" deadCode="false" sourceNode="P_50F10235" targetNode="P_47F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_78F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10235_O" deadCode="false" sourceNode="P_50F10235" targetNode="P_48F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_78F10235"/>
	</edges>
	<edges id="P_50F10235P_51F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10235" targetNode="P_51F10235"/>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10235_I" deadCode="false" sourceNode="P_24F10235" targetNode="P_45F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_81F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10235_O" deadCode="false" sourceNode="P_24F10235" targetNode="P_46F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_81F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10235_I" deadCode="false" sourceNode="P_24F10235" targetNode="P_47F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_82F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10235_O" deadCode="false" sourceNode="P_24F10235" targetNode="P_48F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_82F10235"/>
	</edges>
	<edges id="P_24F10235P_25F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10235" targetNode="P_25F10235"/>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10235_I" deadCode="false" sourceNode="P_26F10235" targetNode="P_50F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_85F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10235_O" deadCode="false" sourceNode="P_26F10235" targetNode="P_51F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_85F10235"/>
	</edges>
	<edges id="P_26F10235P_27F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10235" targetNode="P_27F10235"/>
	<edges id="P_28F10235P_29F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10235" targetNode="P_29F10235"/>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10235_I" deadCode="false" sourceNode="P_30F10235" targetNode="P_26F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_90F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10235_O" deadCode="false" sourceNode="P_30F10235" targetNode="P_27F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_90F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10235_I" deadCode="false" sourceNode="P_30F10235" targetNode="P_32F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_92F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10235_O" deadCode="false" sourceNode="P_30F10235" targetNode="P_33F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_92F10235"/>
	</edges>
	<edges id="P_30F10235P_31F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10235" targetNode="P_31F10235"/>
	<edges id="P_32F10235P_33F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10235" targetNode="P_33F10235"/>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10235_I" deadCode="false" sourceNode="P_52F10235" targetNode="P_45F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_96F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10235_O" deadCode="false" sourceNode="P_52F10235" targetNode="P_46F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_96F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10235_I" deadCode="false" sourceNode="P_52F10235" targetNode="P_47F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_97F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10235_O" deadCode="false" sourceNode="P_52F10235" targetNode="P_48F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_97F10235"/>
	</edges>
	<edges id="P_52F10235P_53F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10235" targetNode="P_53F10235"/>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10235_I" deadCode="false" sourceNode="P_34F10235" targetNode="P_45F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_100F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10235_O" deadCode="false" sourceNode="P_34F10235" targetNode="P_46F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_100F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10235_I" deadCode="false" sourceNode="P_34F10235" targetNode="P_47F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_101F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10235_O" deadCode="false" sourceNode="P_34F10235" targetNode="P_48F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_101F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10235_I" deadCode="false" sourceNode="P_34F10235" targetNode="P_12F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_103F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10235_O" deadCode="false" sourceNode="P_34F10235" targetNode="P_13F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_103F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10235_I" deadCode="false" sourceNode="P_34F10235" targetNode="P_54F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_105F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10235_O" deadCode="false" sourceNode="P_34F10235" targetNode="P_55F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_105F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10235_I" deadCode="false" sourceNode="P_34F10235" targetNode="P_56F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_106F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10235_O" deadCode="false" sourceNode="P_34F10235" targetNode="P_57F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_106F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10235_I" deadCode="false" sourceNode="P_34F10235" targetNode="P_58F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_107F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10235_O" deadCode="false" sourceNode="P_34F10235" targetNode="P_59F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_107F10235"/>
	</edges>
	<edges id="P_34F10235P_35F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10235" targetNode="P_35F10235"/>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10235_I" deadCode="false" sourceNode="P_36F10235" targetNode="P_52F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_109F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10235_O" deadCode="false" sourceNode="P_36F10235" targetNode="P_53F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_109F10235"/>
	</edges>
	<edges id="P_36F10235P_37F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10235" targetNode="P_37F10235"/>
	<edges id="P_38F10235P_39F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10235" targetNode="P_39F10235"/>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10235_I" deadCode="false" sourceNode="P_40F10235" targetNode="P_36F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_114F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10235_O" deadCode="false" sourceNode="P_40F10235" targetNode="P_37F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_114F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10235_I" deadCode="false" sourceNode="P_40F10235" targetNode="P_42F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_116F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10235_O" deadCode="false" sourceNode="P_40F10235" targetNode="P_43F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_116F10235"/>
	</edges>
	<edges id="P_40F10235P_41F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10235" targetNode="P_41F10235"/>
	<edges id="P_42F10235P_43F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10235" targetNode="P_43F10235"/>
	<edges id="P_54F10235P_55F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10235" targetNode="P_55F10235"/>
	<edges id="P_56F10235P_57F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10235" targetNode="P_57F10235"/>
	<edges id="P_45F10235P_46F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10235" targetNode="P_46F10235"/>
	<edges id="P_47F10235P_48F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10235" targetNode="P_48F10235"/>
	<edges id="P_58F10235P_59F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10235" targetNode="P_59F10235"/>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10235_I" deadCode="false" sourceNode="P_10F10235" targetNode="P_62F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_160F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10235_O" deadCode="false" sourceNode="P_10F10235" targetNode="P_63F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_160F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10235_I" deadCode="false" sourceNode="P_10F10235" targetNode="P_64F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_162F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10235_O" deadCode="false" sourceNode="P_10F10235" targetNode="P_65F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_162F10235"/>
	</edges>
	<edges id="P_10F10235P_11F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10235" targetNode="P_11F10235"/>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10235_I" deadCode="true" sourceNode="P_62F10235" targetNode="P_66F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_167F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10235_O" deadCode="true" sourceNode="P_62F10235" targetNode="P_67F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_167F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10235_I" deadCode="true" sourceNode="P_62F10235" targetNode="P_66F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_172F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10235_O" deadCode="true" sourceNode="P_62F10235" targetNode="P_67F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_172F10235"/>
	</edges>
	<edges id="P_62F10235P_63F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10235" targetNode="P_63F10235"/>
	<edges id="P_64F10235P_65F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10235" targetNode="P_65F10235"/>
	<edges id="P_66F10235P_67F10235" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10235" targetNode="P_67F10235"/>
	<edges xsi:type="cbl:PerformEdge" id="S_201F10235_I" deadCode="true" sourceNode="P_70F10235" targetNode="P_71F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_201F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_201F10235_O" deadCode="true" sourceNode="P_70F10235" targetNode="P_72F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_201F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10235_I" deadCode="true" sourceNode="P_70F10235" targetNode="P_73F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_202F10235"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10235_O" deadCode="true" sourceNode="P_70F10235" targetNode="P_74F10235">
		<representations href="../../../cobol/LDBS6730.cbl.cobModel#S_202F10235"/>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_102F10235_POS1" deadCode="false" targetNode="P_34F10235" sourceNode="DB2_PARAM_INFR_APPL">
		<representations href="../../../cobol/../importantStmts.cobModel#S_102F10235"></representations>
	</edges>
</Package>

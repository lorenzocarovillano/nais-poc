<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS2730" cbl:id="LVVS2730" xsi:id="LVVS2730" packageRef="LVVS2730.igd#LVVS2730" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS2730_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10373" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS2730.cbl.cobModel#SC_1F10373"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10373" deadCode="false" name="PROGRAM_LVVS2730_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_1F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10373" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_2F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10373" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_3F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10373" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_4F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10373" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_5F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10373" deadCode="false" name="VERIFICA-MOVIMENTO">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_8F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10373" deadCode="false" name="VERIFICA-MOVIMENTO-EX">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_9F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10373" deadCode="false" name="RECUP-MOVI-COMUN">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_14F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10373" deadCode="false" name="RECUP-MOVI-COMUN-EX">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_15F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10373" deadCode="false" name="CLOSE-MOVI">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_22F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10373" deadCode="false" name="CLOSE-MOVI-EX">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_23F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10373" deadCode="false" name="LETTURA-IMPST-BOLLO">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_18F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10373" deadCode="false" name="LETTURA-IMPST-BOLLO-EX">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_19F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10373" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_20F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10373" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_21F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10373" deadCode="false" name="S1150-CALCOLO-ANNO">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_10F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10373" deadCode="false" name="S1150-CALCOLO-ANNO-EX">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_11F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10373" deadCode="false" name="S1200-CALCOLO-IMPORTO">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_12F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10373" deadCode="false" name="S1200-CALCOLO-IMPORTO-EX">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_13F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10373" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_6F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10373" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_7F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10373" deadCode="false" name="VALORIZZA-OUTPUT-P58">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_24F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10373" deadCode="false" name="VALORIZZA-OUTPUT-P58-EX">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_25F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10373" deadCode="false" name="INIZIA-TOT-P58">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_16F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10373" deadCode="false" name="INIZIA-TOT-P58-EX">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_17F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10373" deadCode="true" name="INIZIA-NULL-P58">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_30F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10373" deadCode="false" name="INIZIA-NULL-P58-EX">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_31F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10373" deadCode="false" name="INIZIA-ZEROES-P58">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_26F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10373" deadCode="false" name="INIZIA-ZEROES-P58-EX">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_27F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10373" deadCode="false" name="INIZIA-SPACES-P58">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_28F10373"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10373" deadCode="false" name="INIZIA-SPACES-P58-EX">
				<representations href="../../../cobol/LVVS2730.cbl.cobModel#P_29F10373"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS6040" name="LDBS6040">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10230"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSE590" name="LDBSE590">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10264"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10373P_1F10373" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10373" targetNode="P_1F10373"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10373_I" deadCode="false" sourceNode="P_1F10373" targetNode="P_2F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_1F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10373_O" deadCode="false" sourceNode="P_1F10373" targetNode="P_3F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_1F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10373_I" deadCode="false" sourceNode="P_1F10373" targetNode="P_4F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_2F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10373_O" deadCode="false" sourceNode="P_1F10373" targetNode="P_5F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_2F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10373_I" deadCode="false" sourceNode="P_1F10373" targetNode="P_6F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_3F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10373_O" deadCode="false" sourceNode="P_1F10373" targetNode="P_7F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_3F10373"/>
	</edges>
	<edges id="P_2F10373P_3F10373" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10373" targetNode="P_3F10373"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10373_I" deadCode="false" sourceNode="P_4F10373" targetNode="P_8F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_11F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10373_O" deadCode="false" sourceNode="P_4F10373" targetNode="P_9F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_11F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10373_I" deadCode="false" sourceNode="P_4F10373" targetNode="P_10F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_13F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10373_O" deadCode="false" sourceNode="P_4F10373" targetNode="P_11F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_13F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10373_I" deadCode="false" sourceNode="P_4F10373" targetNode="P_12F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_15F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10373_O" deadCode="false" sourceNode="P_4F10373" targetNode="P_13F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_15F10373"/>
	</edges>
	<edges id="P_4F10373P_5F10373" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10373" targetNode="P_5F10373"/>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10373_I" deadCode="false" sourceNode="P_8F10373" targetNode="P_14F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_19F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10373_O" deadCode="false" sourceNode="P_8F10373" targetNode="P_15F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_19F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10373_I" deadCode="false" sourceNode="P_8F10373" targetNode="P_16F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_25F10373"/>
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_26F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10373_O" deadCode="false" sourceNode="P_8F10373" targetNode="P_17F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_25F10373"/>
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_26F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10373_I" deadCode="false" sourceNode="P_8F10373" targetNode="P_18F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_27F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10373_O" deadCode="false" sourceNode="P_8F10373" targetNode="P_19F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_27F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10373_I" deadCode="false" sourceNode="P_8F10373" targetNode="P_20F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_28F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10373_O" deadCode="false" sourceNode="P_8F10373" targetNode="P_21F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_28F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10373_I" deadCode="false" sourceNode="P_8F10373" targetNode="P_20F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_29F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10373_O" deadCode="false" sourceNode="P_8F10373" targetNode="P_21F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_29F10373"/>
	</edges>
	<edges id="P_8F10373P_9F10373" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10373" targetNode="P_9F10373"/>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10373_I" deadCode="false" sourceNode="P_14F10373" targetNode="P_22F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_38F10373"/>
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_60F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10373_O" deadCode="false" sourceNode="P_14F10373" targetNode="P_23F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_38F10373"/>
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_60F10373"/>
	</edges>
	<edges id="P_14F10373P_15F10373" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10373" targetNode="P_15F10373"/>
	<edges id="P_22F10373P_23F10373" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10373" targetNode="P_23F10373"/>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10373_I" deadCode="false" sourceNode="P_18F10373" targetNode="P_24F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_88F10373"/>
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_103F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10373_O" deadCode="false" sourceNode="P_18F10373" targetNode="P_25F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_88F10373"/>
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_103F10373"/>
	</edges>
	<edges id="P_18F10373P_19F10373" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10373" targetNode="P_19F10373"/>
	<edges id="P_20F10373P_21F10373" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10373" targetNode="P_21F10373"/>
	<edges id="P_10F10373P_11F10373" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10373" targetNode="P_11F10373"/>
	<edges id="P_12F10373P_13F10373" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10373" targetNode="P_13F10373"/>
	<edges id="P_24F10373P_25F10373" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10373" targetNode="P_25F10373"/>
	<edges xsi:type="cbl:PerformEdge" id="S_176F10373_I" deadCode="false" sourceNode="P_16F10373" targetNode="P_26F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_176F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_176F10373_O" deadCode="false" sourceNode="P_16F10373" targetNode="P_27F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_176F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10373_I" deadCode="false" sourceNode="P_16F10373" targetNode="P_28F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_177F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10373_O" deadCode="false" sourceNode="P_16F10373" targetNode="P_29F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_177F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_178F10373_I" deadCode="true" sourceNode="P_16F10373" targetNode="P_30F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_178F10373"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_178F10373_O" deadCode="true" sourceNode="P_16F10373" targetNode="P_31F10373">
		<representations href="../../../cobol/LVVS2730.cbl.cobModel#S_178F10373"/>
	</edges>
	<edges id="P_16F10373P_17F10373" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10373" targetNode="P_17F10373"/>
	<edges id="P_30F10373P_31F10373" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10373" targetNode="P_31F10373"/>
	<edges id="P_26F10373P_27F10373" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10373" targetNode="P_27F10373"/>
	<edges id="P_28F10373P_29F10373" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10373" targetNode="P_29F10373"/>
	<edges xsi:type="cbl:CallEdge" id="S_46F10373" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_14F10373" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10373"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_72F10373" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_22F10373" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_72F10373"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_95F10373" deadCode="false" name="Dynamic LDBSE590" sourceNode="P_18F10373" targetNode="LDBSE590">
		<representations href="../../../cobol/../importantStmts.cobModel#S_95F10373"></representations>
	</edges>
</Package>

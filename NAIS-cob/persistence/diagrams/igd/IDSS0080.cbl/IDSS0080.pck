<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IDSS0080" cbl:id="IDSS0080" xsi:id="IDSS0080" packageRef="IDSS0080.igd#IDSS0080" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IDSS0080_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10101" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IDSS0080.cbl.cobModel#SC_1F10101"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10101" deadCode="false" name="PROGRAM_IDSS0080_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_1F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10101" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_2F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10101" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_3F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10101" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_8F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10101" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_9F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10101" deadCode="false" name="A300-ELABORA">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_4F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10101" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_5F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10101" deadCode="false" name="A305-DECLARE-CURSOR">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_20F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10101" deadCode="false" name="A305-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_23F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10101" deadCode="false" name="A310-SELECT">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_10F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10101" deadCode="false" name="A310-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_11F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10101" deadCode="false" name="A360-OPEN-CURSOR">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_12F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10101" deadCode="false" name="A360-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_13F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10101" deadCode="false" name="A370-CLOSE-CURSOR">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_14F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10101" deadCode="false" name="A370-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_15F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10101" deadCode="false" name="A380-FETCH-FIRST">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_16F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10101" deadCode="false" name="A380-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_17F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10101" deadCode="false" name="A390-FETCH-NEXT">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_18F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10101" deadCode="false" name="A390-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_19F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10101" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_24F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10101" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_25F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10101" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_26F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10101" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_27F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10101" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_21F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10101" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_22F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10101" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_6F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10101" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_7F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10101" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_28F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10101" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_29F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10101" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_30F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10101" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_31F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10101" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_32F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10101" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_33F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10101" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_34F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10101" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_35F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10101" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_36F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10101" deadCode="true" name="Z800-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_41F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10101" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_37F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10101" deadCode="true" name="Z810-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_38F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10101" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_39F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10101" deadCode="true" name="Z820-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_40F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10101" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_42F10101"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10101" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/IDSS0080.cbl.cobModel#P_43F10101"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_ANAG_DATO" name="ANAG_DATO">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_ANAG_DATO"/>
		</children>
	</packageNode>
	<edges id="SC_1F10101P_1F10101" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10101" targetNode="P_1F10101"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10101_I" deadCode="false" sourceNode="P_1F10101" targetNode="P_2F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_1F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10101_O" deadCode="false" sourceNode="P_1F10101" targetNode="P_3F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_1F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10101_I" deadCode="false" sourceNode="P_1F10101" targetNode="P_4F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_2F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10101_O" deadCode="false" sourceNode="P_1F10101" targetNode="P_5F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_2F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10101_I" deadCode="false" sourceNode="P_2F10101" targetNode="P_6F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_9F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10101_O" deadCode="false" sourceNode="P_2F10101" targetNode="P_7F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_9F10101"/>
	</edges>
	<edges id="P_2F10101P_3F10101" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10101" targetNode="P_3F10101"/>
	<edges id="P_8F10101P_9F10101" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10101" targetNode="P_9F10101"/>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10101_I" deadCode="false" sourceNode="P_4F10101" targetNode="P_10F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_22F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10101_O" deadCode="false" sourceNode="P_4F10101" targetNode="P_11F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_22F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10101_I" deadCode="false" sourceNode="P_4F10101" targetNode="P_12F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_23F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10101_O" deadCode="false" sourceNode="P_4F10101" targetNode="P_13F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_23F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10101_I" deadCode="false" sourceNode="P_4F10101" targetNode="P_14F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_24F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10101_O" deadCode="false" sourceNode="P_4F10101" targetNode="P_15F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_24F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10101_I" deadCode="false" sourceNode="P_4F10101" targetNode="P_16F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_25F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10101_O" deadCode="false" sourceNode="P_4F10101" targetNode="P_17F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_25F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10101_I" deadCode="false" sourceNode="P_4F10101" targetNode="P_18F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_26F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10101_O" deadCode="false" sourceNode="P_4F10101" targetNode="P_19F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_26F10101"/>
	</edges>
	<edges id="P_4F10101P_5F10101" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10101" targetNode="P_5F10101"/>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10101_I" deadCode="false" sourceNode="P_20F10101" targetNode="P_21F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_29F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10101_O" deadCode="false" sourceNode="P_20F10101" targetNode="P_22F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_29F10101"/>
	</edges>
	<edges id="P_20F10101P_23F10101" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10101" targetNode="P_23F10101"/>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10101_I" deadCode="false" sourceNode="P_10F10101" targetNode="P_21F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_33F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10101_O" deadCode="false" sourceNode="P_10F10101" targetNode="P_22F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_33F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10101_I" deadCode="false" sourceNode="P_10F10101" targetNode="P_8F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_35F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10101_O" deadCode="false" sourceNode="P_10F10101" targetNode="P_9F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_35F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10101_I" deadCode="false" sourceNode="P_10F10101" targetNode="P_24F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_37F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10101_O" deadCode="false" sourceNode="P_10F10101" targetNode="P_25F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_37F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10101_I" deadCode="false" sourceNode="P_10F10101" targetNode="P_26F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_38F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10101_O" deadCode="false" sourceNode="P_10F10101" targetNode="P_27F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_38F10101"/>
	</edges>
	<edges id="P_10F10101P_11F10101" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10101" targetNode="P_11F10101"/>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10101_I" deadCode="false" sourceNode="P_12F10101" targetNode="P_20F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_40F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10101_O" deadCode="false" sourceNode="P_12F10101" targetNode="P_23F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_40F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10101_I" deadCode="false" sourceNode="P_12F10101" targetNode="P_8F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_42F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10101_O" deadCode="false" sourceNode="P_12F10101" targetNode="P_9F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_42F10101"/>
	</edges>
	<edges id="P_12F10101P_13F10101" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10101" targetNode="P_13F10101"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10101_I" deadCode="false" sourceNode="P_14F10101" targetNode="P_8F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_45F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10101_O" deadCode="false" sourceNode="P_14F10101" targetNode="P_9F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_45F10101"/>
	</edges>
	<edges id="P_14F10101P_15F10101" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10101" targetNode="P_15F10101"/>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10101_I" deadCode="false" sourceNode="P_16F10101" targetNode="P_12F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_47F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10101_O" deadCode="false" sourceNode="P_16F10101" targetNode="P_13F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_47F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10101_I" deadCode="false" sourceNode="P_16F10101" targetNode="P_18F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_49F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10101_O" deadCode="false" sourceNode="P_16F10101" targetNode="P_19F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_49F10101"/>
	</edges>
	<edges id="P_16F10101P_17F10101" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10101" targetNode="P_17F10101"/>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10101_I" deadCode="false" sourceNode="P_18F10101" targetNode="P_8F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_52F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10101_O" deadCode="false" sourceNode="P_18F10101" targetNode="P_9F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_52F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10101_I" deadCode="false" sourceNode="P_18F10101" targetNode="P_24F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_54F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10101_O" deadCode="false" sourceNode="P_18F10101" targetNode="P_25F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_54F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10101_I" deadCode="false" sourceNode="P_18F10101" targetNode="P_26F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_55F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10101_O" deadCode="false" sourceNode="P_18F10101" targetNode="P_27F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_55F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10101_I" deadCode="false" sourceNode="P_18F10101" targetNode="P_14F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_57F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10101_O" deadCode="false" sourceNode="P_18F10101" targetNode="P_15F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_57F10101"/>
	</edges>
	<edges id="P_18F10101P_19F10101" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10101" targetNode="P_19F10101"/>
	<edges id="P_24F10101P_25F10101" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10101" targetNode="P_25F10101"/>
	<edges id="P_26F10101P_27F10101" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10101" targetNode="P_27F10101"/>
	<edges id="P_21F10101P_22F10101" xsi:type="cbl:FallThroughEdge" sourceNode="P_21F10101" targetNode="P_22F10101"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10101_I" deadCode="false" sourceNode="P_6F10101" targetNode="P_28F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_79F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10101_O" deadCode="false" sourceNode="P_6F10101" targetNode="P_29F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_79F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10101_I" deadCode="false" sourceNode="P_6F10101" targetNode="P_30F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_81F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10101_O" deadCode="false" sourceNode="P_6F10101" targetNode="P_31F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_81F10101"/>
	</edges>
	<edges id="P_6F10101P_7F10101" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10101" targetNode="P_7F10101"/>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10101_I" deadCode="true" sourceNode="P_28F10101" targetNode="P_32F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_86F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10101_O" deadCode="true" sourceNode="P_28F10101" targetNode="P_33F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_86F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10101_I" deadCode="true" sourceNode="P_28F10101" targetNode="P_32F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_91F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10101_O" deadCode="true" sourceNode="P_28F10101" targetNode="P_33F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_91F10101"/>
	</edges>
	<edges id="P_28F10101P_29F10101" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10101" targetNode="P_29F10101"/>
	<edges id="P_30F10101P_31F10101" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10101" targetNode="P_31F10101"/>
	<edges id="P_32F10101P_33F10101" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10101" targetNode="P_33F10101"/>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10101_I" deadCode="true" sourceNode="P_36F10101" targetNode="P_37F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_120F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10101_O" deadCode="true" sourceNode="P_36F10101" targetNode="P_38F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_120F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10101_I" deadCode="true" sourceNode="P_36F10101" targetNode="P_39F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_121F10101"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10101_O" deadCode="true" sourceNode="P_36F10101" targetNode="P_40F10101">
		<representations href="../../../cobol/IDSS0080.cbl.cobModel#S_121F10101"/>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_34F10101_POS1" deadCode="false" targetNode="P_10F10101" sourceNode="DB2_ANAG_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_34F10101"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_41F10101_POS1" deadCode="false" targetNode="P_12F10101" sourceNode="DB2_ANAG_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_41F10101"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_44F10101_POS1" deadCode="false" targetNode="P_14F10101" sourceNode="DB2_ANAG_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_44F10101"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_51F10101_POS1" deadCode="false" targetNode="P_18F10101" sourceNode="DB2_ANAG_DATO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_51F10101"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LCCS0004" cbl:id="LCCS0004" xsi:id="LCCS0004" packageRef="LCCS0004.igd#LCCS0004" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LCCS0004_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10120" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LCCS0004.cbl.cobModel#SC_1F10120"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10120" deadCode="false" name="INIZIO">
				<representations href="../../../cobol/LCCS0004.cbl.cobModel#P_1F10120"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10120" deadCode="false" name="CONTROLLO">
				<representations href="../../../cobol/LCCS0004.cbl.cobModel#P_3F10120"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10120" deadCode="false" name="CONTROLLO-INVERSIONE">
				<representations href="../../../cobol/LCCS0004.cbl.cobModel#P_4F10120"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10120" deadCode="false" name="INVERSIONE-OPPOSTA">
				<representations href="../../../cobol/LCCS0004.cbl.cobModel#P_5F10120"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10120" deadCode="false" name="CONTROLLO-AAAAMMGG">
				<representations href="../../../cobol/LCCS0004.cbl.cobModel#P_6F10120"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10120" deadCode="false" name="FINE">
				<representations href="../../../cobol/LCCS0004.cbl.cobModel#P_2F10120"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10120P_1F10120" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10120" targetNode="P_1F10120"/>
	<edges xsi:type="cbl:GotoEdge" id="S_2F10120_GTP_1" deadCode="false" sourceNode="P_1F10120" targetNode="P_2F10120">
		<representations href="../../../cobol/LCCS0004.cbl.cobModel#S_2F10120"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_3F10120_GTP_1" deadCode="false" sourceNode="P_1F10120" targetNode="P_3F10120">
		<representations href="../../../cobol/LCCS0004.cbl.cobModel#S_3F10120"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_3F10120_GTP_2" deadCode="false" sourceNode="P_1F10120" targetNode="P_4F10120">
		<representations href="../../../cobol/LCCS0004.cbl.cobModel#S_3F10120"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_3F10120_GTP_3" deadCode="false" sourceNode="P_1F10120" targetNode="P_5F10120">
		<representations href="../../../cobol/LCCS0004.cbl.cobModel#S_3F10120"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_3F10120_GTP_4" deadCode="false" sourceNode="P_1F10120" targetNode="P_6F10120">
		<representations href="../../../cobol/LCCS0004.cbl.cobModel#S_3F10120"/>
	</edges>
	<edges id="P_1F10120P_3F10120" xsi:type="cbl:FallThroughEdge" sourceNode="P_1F10120" targetNode="P_3F10120"/>
	<edges xsi:type="cbl:GotoEdge" id="S_9F10120_GTP_1" deadCode="false" sourceNode="P_3F10120" targetNode="P_2F10120">
		<representations href="../../../cobol/LCCS0004.cbl.cobModel#S_9F10120"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_11F10120_GTP_1" deadCode="false" sourceNode="P_3F10120" targetNode="P_2F10120">
		<representations href="../../../cobol/LCCS0004.cbl.cobModel#S_11F10120"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_17F10120_GTP_1" deadCode="false" sourceNode="P_4F10120" targetNode="P_2F10120">
		<representations href="../../../cobol/LCCS0004.cbl.cobModel#S_17F10120"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_23F10120_GTP_1" deadCode="false" sourceNode="P_4F10120" targetNode="P_2F10120">
		<representations href="../../../cobol/LCCS0004.cbl.cobModel#S_23F10120"/>
	</edges>
	<edges id="P_5F10120P_6F10120" xsi:type="cbl:FallThroughEdge" sourceNode="P_5F10120" targetNode="P_6F10120"/>
	<edges xsi:type="cbl:GotoEdge" id="S_36F10120_GTP_1" deadCode="false" sourceNode="P_6F10120" targetNode="P_2F10120">
		<representations href="../../../cobol/LCCS0004.cbl.cobModel#S_36F10120"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_38F10120_GTP_1" deadCode="false" sourceNode="P_6F10120" targetNode="P_2F10120">
		<representations href="../../../cobol/LCCS0004.cbl.cobModel#S_38F10120"/>
	</edges>
</Package>

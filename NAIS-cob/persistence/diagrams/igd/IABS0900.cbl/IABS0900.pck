<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IABS0900" cbl:id="IABS0900" xsi:id="IABS0900" packageRef="IABS0900.igd#IABS0900" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IABS0900_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10013" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IABS0900.cbl.cobModel#SC_1F10013"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10013" deadCode="false" name="PROGRAM_IABS0900_FIRST_SENTENCES">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_1F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10013" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_2F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10013" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_3F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10013" deadCode="false" name="A060-CNTL-INPUT">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_4F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10013" deadCode="false" name="A060-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_5F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10013" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_12F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10013" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_13F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10013" deadCode="false" name="A300-ELABORA">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_6F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10013" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_7F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10013" deadCode="false" name="A400-FINE">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_8F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10013" deadCode="false" name="A400-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_9F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10013" deadCode="false" name="D101-DCL-CUR-IDR-STD-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_18F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10013" deadCode="false" name="D101-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_19F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10013" deadCode="false" name="D102-DCL-CUR-IDJ-MCRFNCT-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_20F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10013" deadCode="false" name="D102-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_21F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10013" deadCode="false" name="D103-DCL-CUR-IDR-TPMV-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_22F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10013" deadCode="false" name="D103-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_23F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10013" deadCode="false" name="D104-DCL-CUR-IDJ-MCRFNCT-TPMV">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_24F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10013" deadCode="false" name="D104-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_25F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10013" deadCode="false" name="A305-DECLARE-CURSOR-IDR-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_26F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10013" deadCode="false" name="A305-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_27F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10013" deadCode="false" name="A310-SELECT-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_28F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10013" deadCode="false" name="A310-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_37F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10013" deadCode="false" name="A311-SELECT-STD-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_29F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10013" deadCode="false" name="A311-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_30F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10013" deadCode="false" name="A312-SELECT-MCRFNCT-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_31F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10013" deadCode="false" name="A312-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_32F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10013" deadCode="false" name="A313-SELECT-TPMV-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_33F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10013" deadCode="false" name="A313-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_34F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10013" deadCode="false" name="A314-SELECT-MCRFNCT-TPMV-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_35F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10013" deadCode="false" name="A314-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_36F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10013" deadCode="false" name="A310-SELECT-SC02">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_42F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10013" deadCode="false" name="A310-SC02-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_45F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10013" deadCode="false" name="A320-UPDATE-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_46F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10013" deadCode="false" name="A320-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_53F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10013" deadCode="false" name="A330-UPDATE-PK-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_47F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10013" deadCode="false" name="A330-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_48F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10013" deadCode="false" name="A330-UPDATE-SC02">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_58F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10013" deadCode="false" name="A330-SC02-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_65F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10013" deadCode="false" name="A331-UPDATE-FIRST-ACTION-SC02">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_59F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10013" deadCode="false" name="A331-SC02-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_60F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10013" deadCode="false" name="A332-UPDATE-NONE-ACTION-SC02">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_61F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10013" deadCode="false" name="A332-SC02-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_62F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10013" deadCode="false" name="A333-UPDATE-ID-SC02">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_63F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10013" deadCode="false" name="A333-SC02-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_64F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10013" deadCode="false" name="A350-INSERT-SC02">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_66F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10013" deadCode="false" name="A350-SC02-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_73F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10013" deadCode="false" name="A340-UPDATE-FIRST-ACTION-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_49F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10013" deadCode="false" name="A340-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_50F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10013" deadCode="false" name="A341-UPD-F-ACT-STD-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_74F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10013" deadCode="false" name="A341-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_75F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10013" deadCode="false" name="A342-UPD-F-ACT-MFNCT-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_76F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10013" deadCode="false" name="A342-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_77F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_78F10013" deadCode="false" name="A343-UPD-F-ACT-TPMV-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_78F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_79F10013" deadCode="false" name="A343-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_79F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_80F10013" deadCode="false" name="A344-UPD-F-ACT-MFNCT-TPMV-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_80F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_81F10013" deadCode="false" name="A344-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_81F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10013" deadCode="false" name="A350-UPDATE-WHERE-COND-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_51F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10013" deadCode="false" name="A350-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_52F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_82F10013" deadCode="false" name="A351-UPD-WHC-STD-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_82F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_83F10013" deadCode="false" name="A351-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_83F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_84F10013" deadCode="false" name="A352-UPD-WHC-MFNCT-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_84F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_85F10013" deadCode="false" name="A352-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_85F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_86F10013" deadCode="false" name="A353-UPD-WHC-TPMV-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_86F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_87F10013" deadCode="false" name="A353-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_87F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_88F10013" deadCode="false" name="A354-UPD-WHC-MFNCT-TPMV-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_88F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_89F10013" deadCode="false" name="A354-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_89F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_90F10013" deadCode="false" name="A360-OPEN-CURSOR-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_90F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_91F10013" deadCode="false" name="A360-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_91F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_92F10013" deadCode="false" name="A370-CLOSE-CURSOR-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_92F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_95F10013" deadCode="false" name="A370-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_95F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_93F10013" deadCode="false" name="C100-CLOSE-IDR-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_93F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_94F10013" deadCode="false" name="C100-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_94F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_96F10013" deadCode="false" name="A380-FETCH-FIRST-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_96F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_99F10013" deadCode="false" name="A380-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_99F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_97F10013" deadCode="false" name="A390-FETCH-NEXT-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_97F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_98F10013" deadCode="false" name="A390-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_98F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_100F10013" deadCode="false" name="A391-FETCH-NEXT-IDR-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_100F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_101F10013" deadCode="false" name="A391-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_101F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_102F10013" deadCode="false" name="F101-FET-NX-IDR-STD-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_102F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_103F10013" deadCode="false" name="F101-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_103F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_104F10013" deadCode="false" name="F102-FET-NX-IDR-MFNCT-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_104F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_105F10013" deadCode="false" name="F102-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_105F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_106F10013" deadCode="false" name="F103-FET-NX-IDR-TPMV-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_106F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_107F10013" deadCode="false" name="F103-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_107F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_108F10013" deadCode="false" name="F104-FET-NX-IDR-MF-TPMV-SC01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_108F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_109F10013" deadCode="false" name="F104-SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_109F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_110F10013" deadCode="false" name="B000-FETCH-DER">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_110F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_111F10013" deadCode="false" name="B000-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_111F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10013" deadCode="false" name="B001-SELECT-DER">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_43F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10013" deadCode="false" name="B001-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_44F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_116F10013" deadCode="false" name="B010-DECLARE-CURSOR-DER">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_116F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_117F10013" deadCode="false" name="B010-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_117F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_118F10013" deadCode="false" name="B020-OPEN-CURSOR-DER">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_118F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_119F10013" deadCode="false" name="B020-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_119F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_112F10013" deadCode="false" name="B030-FETCH-FIRST-DER">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_112F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_113F10013" deadCode="false" name="B030-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_113F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_114F10013" deadCode="false" name="B040-FETCH-NEXT-DER">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_114F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_115F10013" deadCode="false" name="B040-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_115F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_120F10013" deadCode="false" name="B050-CLOSE-CURSOR-DER">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_120F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_121F10013" deadCode="false" name="B050-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_121F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10013" deadCode="false" name="SC01-SELECTION-CURSOR-01">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_14F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10013" deadCode="false" name="SC01-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_15F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10013" deadCode="false" name="SC02-SELECTION-CURSOR-02">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_16F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10013" deadCode="false" name="SC02-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_17F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10013" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_38F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10013" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_39F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10013" deadCode="false" name="Z150-VALORIZZA-DATA-SERVICES-I">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_69F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10013" deadCode="false" name="Z150-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_70F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10013" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_54F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10013" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_55F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10013" deadCode="false" name="Z400-SEQ-RIGA">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_67F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10013" deadCode="false" name="Z400-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_68F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10013" deadCode="false" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_56F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10013" deadCode="false" name="Z900-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_57F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10013" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_40F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10013" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_41F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10013" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_71F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10013" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_72F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10013" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_10F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10013" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_11F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_126F10013" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_126F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_127F10013" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_127F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_128F10013" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_128F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_129F10013" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_129F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_122F10013" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_122F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_123F10013" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_123F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_130F10013" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_130F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_131F10013" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_131F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_124F10013" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_124F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_125F10013" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_125F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_132F10013" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_132F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_133F10013" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_133F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_134F10013" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_134F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_135F10013" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_135F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_136F10013" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_136F10013"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_137F10013" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/IABS0900.cbl.cobModel#P_137F10013"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_RICH" name="RICH">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_RICH"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_DETT_RICH" name="DETT_RICH">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_DETT_RICH"/>
		</children>
	</packageNode>
	<edges id="SC_1F10013P_1F10013" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10013" targetNode="P_1F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10013_I" deadCode="false" sourceNode="P_1F10013" targetNode="P_2F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_1F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10013_O" deadCode="false" sourceNode="P_1F10013" targetNode="P_3F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_1F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10013_I" deadCode="false" sourceNode="P_1F10013" targetNode="P_4F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_2F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10013_O" deadCode="false" sourceNode="P_1F10013" targetNode="P_5F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_2F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10013_I" deadCode="false" sourceNode="P_1F10013" targetNode="P_6F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_3F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10013_O" deadCode="false" sourceNode="P_1F10013" targetNode="P_7F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_3F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10013_I" deadCode="false" sourceNode="P_1F10013" targetNode="P_8F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_4F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10013_O" deadCode="false" sourceNode="P_1F10013" targetNode="P_9F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_4F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10013_I" deadCode="false" sourceNode="P_2F10013" targetNode="P_10F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_12F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10013_O" deadCode="false" sourceNode="P_2F10013" targetNode="P_11F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_12F10013"/>
	</edges>
	<edges id="P_2F10013P_3F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10013" targetNode="P_3F10013"/>
	<edges id="P_4F10013P_5F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10013" targetNode="P_5F10013"/>
	<edges id="P_12F10013P_13F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10013" targetNode="P_13F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10013_I" deadCode="false" sourceNode="P_6F10013" targetNode="P_14F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_33F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10013_O" deadCode="false" sourceNode="P_6F10013" targetNode="P_15F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_33F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10013_I" deadCode="false" sourceNode="P_6F10013" targetNode="P_16F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_34F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10013_O" deadCode="false" sourceNode="P_6F10013" targetNode="P_17F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_34F10013"/>
	</edges>
	<edges id="P_6F10013P_7F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10013" targetNode="P_7F10013"/>
	<edges id="P_8F10013P_9F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10013" targetNode="P_9F10013"/>
	<edges id="P_18F10013P_19F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10013" targetNode="P_19F10013"/>
	<edges id="P_20F10013P_21F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10013" targetNode="P_21F10013"/>
	<edges id="P_22F10013P_23F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10013" targetNode="P_23F10013"/>
	<edges id="P_24F10013P_25F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10013" targetNode="P_25F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10013_I" deadCode="false" sourceNode="P_26F10013" targetNode="P_18F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_52F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10013_O" deadCode="false" sourceNode="P_26F10013" targetNode="P_19F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_52F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10013_I" deadCode="false" sourceNode="P_26F10013" targetNode="P_20F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_54F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10013_O" deadCode="false" sourceNode="P_26F10013" targetNode="P_21F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_54F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10013_I" deadCode="false" sourceNode="P_26F10013" targetNode="P_22F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_56F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10013_O" deadCode="false" sourceNode="P_26F10013" targetNode="P_23F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_56F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10013_I" deadCode="false" sourceNode="P_26F10013" targetNode="P_24F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_58F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10013_O" deadCode="false" sourceNode="P_26F10013" targetNode="P_25F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_58F10013"/>
	</edges>
	<edges id="P_26F10013P_27F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10013" targetNode="P_27F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10013_I" deadCode="false" sourceNode="P_28F10013" targetNode="P_29F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_62F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10013_O" deadCode="false" sourceNode="P_28F10013" targetNode="P_30F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_62F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10013_I" deadCode="false" sourceNode="P_28F10013" targetNode="P_31F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_63F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10013_O" deadCode="false" sourceNode="P_28F10013" targetNode="P_32F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_63F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10013_I" deadCode="false" sourceNode="P_28F10013" targetNode="P_33F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_64F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10013_O" deadCode="false" sourceNode="P_28F10013" targetNode="P_34F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_64F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10013_I" deadCode="false" sourceNode="P_28F10013" targetNode="P_35F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_65F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10013_O" deadCode="false" sourceNode="P_28F10013" targetNode="P_36F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_65F10013"/>
	</edges>
	<edges id="P_28F10013P_37F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10013" targetNode="P_37F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10013_I" deadCode="false" sourceNode="P_29F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_68F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10013_O" deadCode="false" sourceNode="P_29F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_68F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10013_I" deadCode="false" sourceNode="P_29F10013" targetNode="P_38F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_70F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10013_O" deadCode="false" sourceNode="P_29F10013" targetNode="P_39F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_70F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10013_I" deadCode="false" sourceNode="P_29F10013" targetNode="P_40F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_71F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10013_O" deadCode="false" sourceNode="P_29F10013" targetNode="P_41F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_71F10013"/>
	</edges>
	<edges id="P_29F10013P_30F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_29F10013" targetNode="P_30F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10013_I" deadCode="false" sourceNode="P_31F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_74F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10013_O" deadCode="false" sourceNode="P_31F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_74F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10013_I" deadCode="false" sourceNode="P_31F10013" targetNode="P_38F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_76F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10013_O" deadCode="false" sourceNode="P_31F10013" targetNode="P_39F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_76F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10013_I" deadCode="false" sourceNode="P_31F10013" targetNode="P_40F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_77F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10013_O" deadCode="false" sourceNode="P_31F10013" targetNode="P_41F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_77F10013"/>
	</edges>
	<edges id="P_31F10013P_32F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_31F10013" targetNode="P_32F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10013_I" deadCode="false" sourceNode="P_33F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_80F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10013_O" deadCode="false" sourceNode="P_33F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_80F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10013_I" deadCode="false" sourceNode="P_33F10013" targetNode="P_38F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_82F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10013_O" deadCode="false" sourceNode="P_33F10013" targetNode="P_39F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_82F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10013_I" deadCode="false" sourceNode="P_33F10013" targetNode="P_40F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_83F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10013_O" deadCode="false" sourceNode="P_33F10013" targetNode="P_41F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_83F10013"/>
	</edges>
	<edges id="P_33F10013P_34F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_33F10013" targetNode="P_34F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10013_I" deadCode="false" sourceNode="P_35F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_86F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10013_O" deadCode="false" sourceNode="P_35F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_86F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10013_I" deadCode="false" sourceNode="P_35F10013" targetNode="P_38F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_88F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10013_O" deadCode="false" sourceNode="P_35F10013" targetNode="P_39F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_88F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10013_I" deadCode="false" sourceNode="P_35F10013" targetNode="P_40F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_89F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10013_O" deadCode="false" sourceNode="P_35F10013" targetNode="P_41F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_89F10013"/>
	</edges>
	<edges id="P_35F10013P_36F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_35F10013" targetNode="P_36F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10013_I" deadCode="false" sourceNode="P_42F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_92F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10013_O" deadCode="false" sourceNode="P_42F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_92F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10013_I" deadCode="false" sourceNode="P_42F10013" targetNode="P_38F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_94F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10013_O" deadCode="false" sourceNode="P_42F10013" targetNode="P_39F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_94F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10013_I" deadCode="false" sourceNode="P_42F10013" targetNode="P_40F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_95F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10013_O" deadCode="false" sourceNode="P_42F10013" targetNode="P_41F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_95F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10013_I" deadCode="false" sourceNode="P_42F10013" targetNode="P_43F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_96F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10013_O" deadCode="false" sourceNode="P_42F10013" targetNode="P_44F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_96F10013"/>
	</edges>
	<edges id="P_42F10013P_45F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10013" targetNode="P_45F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10013_I" deadCode="false" sourceNode="P_46F10013" targetNode="P_47F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_99F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10013_O" deadCode="false" sourceNode="P_46F10013" targetNode="P_48F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_99F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10013_I" deadCode="false" sourceNode="P_46F10013" targetNode="P_49F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_100F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10013_O" deadCode="false" sourceNode="P_46F10013" targetNode="P_50F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_100F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10013_I" deadCode="false" sourceNode="P_46F10013" targetNode="P_51F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_101F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10013_O" deadCode="false" sourceNode="P_46F10013" targetNode="P_52F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_101F10013"/>
	</edges>
	<edges id="P_46F10013P_53F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10013" targetNode="P_53F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10013_I" deadCode="false" sourceNode="P_47F10013" targetNode="P_54F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_104F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10013_O" deadCode="false" sourceNode="P_47F10013" targetNode="P_55F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_104F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10013_I" deadCode="false" sourceNode="P_47F10013" targetNode="P_56F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_105F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10013_O" deadCode="false" sourceNode="P_47F10013" targetNode="P_57F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_105F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10013_I" deadCode="false" sourceNode="P_47F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_107F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10013_O" deadCode="false" sourceNode="P_47F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_107F10013"/>
	</edges>
	<edges id="P_47F10013P_48F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10013" targetNode="P_48F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10013_I" deadCode="false" sourceNode="P_58F10013" targetNode="P_59F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_112F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10013_O" deadCode="false" sourceNode="P_58F10013" targetNode="P_60F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_112F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10013_I" deadCode="false" sourceNode="P_58F10013" targetNode="P_61F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_113F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10013_O" deadCode="false" sourceNode="P_58F10013" targetNode="P_62F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_113F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10013_I" deadCode="false" sourceNode="P_58F10013" targetNode="P_63F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_114F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10013_O" deadCode="false" sourceNode="P_58F10013" targetNode="P_64F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_114F10013"/>
	</edges>
	<edges id="P_58F10013P_65F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10013" targetNode="P_65F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10013_I" deadCode="false" sourceNode="P_59F10013" targetNode="P_54F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_117F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10013_O" deadCode="false" sourceNode="P_59F10013" targetNode="P_55F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_117F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10013_I" deadCode="false" sourceNode="P_59F10013" targetNode="P_56F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_118F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10013_O" deadCode="false" sourceNode="P_59F10013" targetNode="P_57F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_118F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10013_I" deadCode="false" sourceNode="P_59F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_120F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10013_O" deadCode="false" sourceNode="P_59F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_120F10013"/>
	</edges>
	<edges id="P_59F10013P_60F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_59F10013" targetNode="P_60F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10013_I" deadCode="false" sourceNode="P_61F10013" targetNode="P_54F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_122F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10013_O" deadCode="false" sourceNode="P_61F10013" targetNode="P_55F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_122F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10013_I" deadCode="false" sourceNode="P_61F10013" targetNode="P_56F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_123F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10013_O" deadCode="false" sourceNode="P_61F10013" targetNode="P_57F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_123F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10013_I" deadCode="false" sourceNode="P_61F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_125F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10013_O" deadCode="false" sourceNode="P_61F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_125F10013"/>
	</edges>
	<edges id="P_61F10013P_62F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10013" targetNode="P_62F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10013_I" deadCode="false" sourceNode="P_63F10013" targetNode="P_54F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_130F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10013_O" deadCode="false" sourceNode="P_63F10013" targetNode="P_55F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_130F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10013_I" deadCode="false" sourceNode="P_63F10013" targetNode="P_56F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_131F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10013_O" deadCode="false" sourceNode="P_63F10013" targetNode="P_57F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_131F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10013_I" deadCode="false" sourceNode="P_63F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_133F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10013_O" deadCode="false" sourceNode="P_63F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_133F10013"/>
	</edges>
	<edges id="P_63F10013P_64F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_63F10013" targetNode="P_64F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10013_I" deadCode="false" sourceNode="P_66F10013" targetNode="P_67F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_135F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10013_O" deadCode="false" sourceNode="P_66F10013" targetNode="P_68F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_135F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10013_I" deadCode="false" sourceNode="P_66F10013" targetNode="P_69F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_137F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10013_O" deadCode="false" sourceNode="P_66F10013" targetNode="P_70F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_137F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10013_I" deadCode="false" sourceNode="P_66F10013" targetNode="P_54F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_138F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10013_O" deadCode="false" sourceNode="P_66F10013" targetNode="P_55F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_138F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10013_I" deadCode="false" sourceNode="P_66F10013" targetNode="P_56F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_139F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10013_O" deadCode="false" sourceNode="P_66F10013" targetNode="P_57F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_139F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10013_I" deadCode="false" sourceNode="P_66F10013" targetNode="P_71F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_140F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10013_O" deadCode="false" sourceNode="P_66F10013" targetNode="P_72F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_140F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10013_I" deadCode="false" sourceNode="P_66F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_142F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10013_O" deadCode="false" sourceNode="P_66F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_142F10013"/>
	</edges>
	<edges id="P_66F10013P_73F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10013" targetNode="P_73F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10013_I" deadCode="false" sourceNode="P_49F10013" targetNode="P_74F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_145F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10013_O" deadCode="false" sourceNode="P_49F10013" targetNode="P_75F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_145F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10013_I" deadCode="false" sourceNode="P_49F10013" targetNode="P_76F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_146F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10013_O" deadCode="false" sourceNode="P_49F10013" targetNode="P_77F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_146F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10013_I" deadCode="false" sourceNode="P_49F10013" targetNode="P_78F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_147F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10013_O" deadCode="false" sourceNode="P_49F10013" targetNode="P_79F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_147F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10013_I" deadCode="false" sourceNode="P_49F10013" targetNode="P_80F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_148F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10013_O" deadCode="false" sourceNode="P_49F10013" targetNode="P_81F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_148F10013"/>
	</edges>
	<edges id="P_49F10013P_50F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_49F10013" targetNode="P_50F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10013_I" deadCode="false" sourceNode="P_74F10013" targetNode="P_54F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_150F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10013_O" deadCode="false" sourceNode="P_74F10013" targetNode="P_55F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_150F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_151F10013_I" deadCode="false" sourceNode="P_74F10013" targetNode="P_56F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_151F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_151F10013_O" deadCode="false" sourceNode="P_74F10013" targetNode="P_57F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_151F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10013_I" deadCode="false" sourceNode="P_74F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_153F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10013_O" deadCode="false" sourceNode="P_74F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_153F10013"/>
	</edges>
	<edges id="P_74F10013P_75F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10013" targetNode="P_75F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10013_I" deadCode="false" sourceNode="P_76F10013" targetNode="P_54F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_157F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10013_O" deadCode="false" sourceNode="P_76F10013" targetNode="P_55F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_157F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10013_I" deadCode="false" sourceNode="P_76F10013" targetNode="P_56F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_158F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10013_O" deadCode="false" sourceNode="P_76F10013" targetNode="P_57F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_158F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10013_I" deadCode="false" sourceNode="P_76F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_160F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10013_O" deadCode="false" sourceNode="P_76F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_160F10013"/>
	</edges>
	<edges id="P_76F10013P_77F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_76F10013" targetNode="P_77F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10013_I" deadCode="false" sourceNode="P_78F10013" targetNode="P_54F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_164F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10013_O" deadCode="false" sourceNode="P_78F10013" targetNode="P_55F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_164F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10013_I" deadCode="false" sourceNode="P_78F10013" targetNode="P_56F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_165F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10013_O" deadCode="false" sourceNode="P_78F10013" targetNode="P_57F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_165F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10013_I" deadCode="false" sourceNode="P_78F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_167F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10013_O" deadCode="false" sourceNode="P_78F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_167F10013"/>
	</edges>
	<edges id="P_78F10013P_79F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_78F10013" targetNode="P_79F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10013_I" deadCode="false" sourceNode="P_80F10013" targetNode="P_54F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_171F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10013_O" deadCode="false" sourceNode="P_80F10013" targetNode="P_55F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_171F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10013_I" deadCode="false" sourceNode="P_80F10013" targetNode="P_56F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_172F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10013_O" deadCode="false" sourceNode="P_80F10013" targetNode="P_57F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_172F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10013_I" deadCode="false" sourceNode="P_80F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_174F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10013_O" deadCode="false" sourceNode="P_80F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_174F10013"/>
	</edges>
	<edges id="P_80F10013P_81F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_80F10013" targetNode="P_81F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10013_I" deadCode="false" sourceNode="P_51F10013" targetNode="P_82F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_179F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10013_O" deadCode="false" sourceNode="P_51F10013" targetNode="P_83F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_179F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_180F10013_I" deadCode="false" sourceNode="P_51F10013" targetNode="P_84F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_180F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_180F10013_O" deadCode="false" sourceNode="P_51F10013" targetNode="P_85F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_180F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10013_I" deadCode="false" sourceNode="P_51F10013" targetNode="P_86F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_181F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10013_O" deadCode="false" sourceNode="P_51F10013" targetNode="P_87F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_181F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_182F10013_I" deadCode="false" sourceNode="P_51F10013" targetNode="P_88F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_182F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_182F10013_O" deadCode="false" sourceNode="P_51F10013" targetNode="P_89F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_182F10013"/>
	</edges>
	<edges id="P_51F10013P_52F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_51F10013" targetNode="P_52F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10013_I" deadCode="false" sourceNode="P_82F10013" targetNode="P_54F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_184F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10013_O" deadCode="false" sourceNode="P_82F10013" targetNode="P_55F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_184F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10013_I" deadCode="false" sourceNode="P_82F10013" targetNode="P_56F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_185F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10013_O" deadCode="false" sourceNode="P_82F10013" targetNode="P_57F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_185F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10013_I" deadCode="false" sourceNode="P_82F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_187F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10013_O" deadCode="false" sourceNode="P_82F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_187F10013"/>
	</edges>
	<edges id="P_82F10013P_83F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_82F10013" targetNode="P_83F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10013_I" deadCode="false" sourceNode="P_84F10013" targetNode="P_54F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_191F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10013_O" deadCode="false" sourceNode="P_84F10013" targetNode="P_55F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_191F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_192F10013_I" deadCode="false" sourceNode="P_84F10013" targetNode="P_56F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_192F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_192F10013_O" deadCode="false" sourceNode="P_84F10013" targetNode="P_57F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_192F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_194F10013_I" deadCode="false" sourceNode="P_84F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_194F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_194F10013_O" deadCode="false" sourceNode="P_84F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_194F10013"/>
	</edges>
	<edges id="P_84F10013P_85F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_84F10013" targetNode="P_85F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_198F10013_I" deadCode="false" sourceNode="P_86F10013" targetNode="P_54F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_198F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_198F10013_O" deadCode="false" sourceNode="P_86F10013" targetNode="P_55F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_198F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_199F10013_I" deadCode="false" sourceNode="P_86F10013" targetNode="P_56F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_199F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_199F10013_O" deadCode="false" sourceNode="P_86F10013" targetNode="P_57F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_199F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_201F10013_I" deadCode="false" sourceNode="P_86F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_201F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_201F10013_O" deadCode="false" sourceNode="P_86F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_201F10013"/>
	</edges>
	<edges id="P_86F10013P_87F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_86F10013" targetNode="P_87F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_205F10013_I" deadCode="false" sourceNode="P_88F10013" targetNode="P_54F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_205F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_205F10013_O" deadCode="false" sourceNode="P_88F10013" targetNode="P_55F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_205F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_206F10013_I" deadCode="false" sourceNode="P_88F10013" targetNode="P_56F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_206F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_206F10013_O" deadCode="false" sourceNode="P_88F10013" targetNode="P_57F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_206F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_208F10013_I" deadCode="false" sourceNode="P_88F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_208F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_208F10013_O" deadCode="false" sourceNode="P_88F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_208F10013"/>
	</edges>
	<edges id="P_88F10013P_89F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_88F10013" targetNode="P_89F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10013_I" deadCode="false" sourceNode="P_90F10013" targetNode="P_26F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_213F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10013_O" deadCode="false" sourceNode="P_90F10013" targetNode="P_27F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_213F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10013_I" deadCode="false" sourceNode="P_90F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_216F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10013_O" deadCode="false" sourceNode="P_90F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_216F10013"/>
	</edges>
	<edges id="P_90F10013P_91F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_90F10013" targetNode="P_91F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_219F10013_I" deadCode="false" sourceNode="P_92F10013" targetNode="P_93F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_219F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_219F10013_O" deadCode="false" sourceNode="P_92F10013" targetNode="P_94F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_219F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_222F10013_I" deadCode="false" sourceNode="P_92F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_222F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_222F10013_O" deadCode="false" sourceNode="P_92F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_222F10013"/>
	</edges>
	<edges id="P_92F10013P_95F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_92F10013" targetNode="P_95F10013"/>
	<edges id="P_93F10013P_94F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_93F10013" targetNode="P_94F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_230F10013_I" deadCode="false" sourceNode="P_96F10013" targetNode="P_90F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_230F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_230F10013_O" deadCode="false" sourceNode="P_96F10013" targetNode="P_91F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_230F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_232F10013_I" deadCode="false" sourceNode="P_96F10013" targetNode="P_97F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_232F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_232F10013_O" deadCode="false" sourceNode="P_96F10013" targetNode="P_98F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_232F10013"/>
	</edges>
	<edges id="P_96F10013P_99F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_96F10013" targetNode="P_99F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_235F10013_I" deadCode="false" sourceNode="P_97F10013" targetNode="P_100F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_235F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_235F10013_O" deadCode="false" sourceNode="P_97F10013" targetNode="P_101F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_235F10013"/>
	</edges>
	<edges id="P_97F10013P_98F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_97F10013" targetNode="P_98F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_239F10013_I" deadCode="false" sourceNode="P_100F10013" targetNode="P_102F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_239F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_239F10013_O" deadCode="false" sourceNode="P_100F10013" targetNode="P_103F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_239F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_240F10013_I" deadCode="false" sourceNode="P_100F10013" targetNode="P_104F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_240F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_240F10013_O" deadCode="false" sourceNode="P_100F10013" targetNode="P_105F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_240F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_241F10013_I" deadCode="false" sourceNode="P_100F10013" targetNode="P_106F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_241F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_241F10013_O" deadCode="false" sourceNode="P_100F10013" targetNode="P_107F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_241F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10013_I" deadCode="false" sourceNode="P_100F10013" targetNode="P_108F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_242F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10013_O" deadCode="false" sourceNode="P_100F10013" targetNode="P_109F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_242F10013"/>
	</edges>
	<edges id="P_100F10013P_101F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_100F10013" targetNode="P_101F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_245F10013_I" deadCode="false" sourceNode="P_102F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_245F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_245F10013_O" deadCode="false" sourceNode="P_102F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_245F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_247F10013_I" deadCode="false" sourceNode="P_102F10013" targetNode="P_38F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_247F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_247F10013_O" deadCode="false" sourceNode="P_102F10013" targetNode="P_39F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_247F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_248F10013_I" deadCode="false" sourceNode="P_102F10013" targetNode="P_40F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_248F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_248F10013_O" deadCode="false" sourceNode="P_102F10013" targetNode="P_41F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_248F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_249F10013_I" deadCode="false" sourceNode="P_102F10013" targetNode="P_110F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_249F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_249F10013_O" deadCode="false" sourceNode="P_102F10013" targetNode="P_111F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_249F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_251F10013_I" deadCode="false" sourceNode="P_102F10013" targetNode="P_92F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_251F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_251F10013_O" deadCode="false" sourceNode="P_102F10013" targetNode="P_95F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_251F10013"/>
	</edges>
	<edges id="P_102F10013P_103F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_102F10013" targetNode="P_103F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_256F10013_I" deadCode="false" sourceNode="P_104F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_256F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_256F10013_O" deadCode="false" sourceNode="P_104F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_256F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_258F10013_I" deadCode="false" sourceNode="P_104F10013" targetNode="P_38F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_258F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_258F10013_O" deadCode="false" sourceNode="P_104F10013" targetNode="P_39F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_258F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10013_I" deadCode="false" sourceNode="P_104F10013" targetNode="P_40F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_259F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10013_O" deadCode="false" sourceNode="P_104F10013" targetNode="P_41F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_259F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_260F10013_I" deadCode="false" sourceNode="P_104F10013" targetNode="P_110F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_260F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_260F10013_O" deadCode="false" sourceNode="P_104F10013" targetNode="P_111F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_260F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10013_I" deadCode="false" sourceNode="P_104F10013" targetNode="P_92F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_262F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10013_O" deadCode="false" sourceNode="P_104F10013" targetNode="P_95F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_262F10013"/>
	</edges>
	<edges id="P_104F10013P_105F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_104F10013" targetNode="P_105F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_267F10013_I" deadCode="false" sourceNode="P_106F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_267F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_267F10013_O" deadCode="false" sourceNode="P_106F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_267F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_269F10013_I" deadCode="false" sourceNode="P_106F10013" targetNode="P_38F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_269F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_269F10013_O" deadCode="false" sourceNode="P_106F10013" targetNode="P_39F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_269F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_270F10013_I" deadCode="false" sourceNode="P_106F10013" targetNode="P_40F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_270F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_270F10013_O" deadCode="false" sourceNode="P_106F10013" targetNode="P_41F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_270F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_271F10013_I" deadCode="false" sourceNode="P_106F10013" targetNode="P_110F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_271F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_271F10013_O" deadCode="false" sourceNode="P_106F10013" targetNode="P_111F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_271F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_273F10013_I" deadCode="false" sourceNode="P_106F10013" targetNode="P_92F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_273F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_273F10013_O" deadCode="false" sourceNode="P_106F10013" targetNode="P_95F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_273F10013"/>
	</edges>
	<edges id="P_106F10013P_107F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_106F10013" targetNode="P_107F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10013_I" deadCode="false" sourceNode="P_108F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_278F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10013_O" deadCode="false" sourceNode="P_108F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_278F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10013_I" deadCode="false" sourceNode="P_108F10013" targetNode="P_38F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_280F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10013_O" deadCode="false" sourceNode="P_108F10013" targetNode="P_39F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_280F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10013_I" deadCode="false" sourceNode="P_108F10013" targetNode="P_40F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_281F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10013_O" deadCode="false" sourceNode="P_108F10013" targetNode="P_41F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_281F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_282F10013_I" deadCode="false" sourceNode="P_108F10013" targetNode="P_110F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_282F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_282F10013_O" deadCode="false" sourceNode="P_108F10013" targetNode="P_111F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_282F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10013_I" deadCode="false" sourceNode="P_108F10013" targetNode="P_92F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_284F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10013_O" deadCode="false" sourceNode="P_108F10013" targetNode="P_95F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_284F10013"/>
	</edges>
	<edges id="P_108F10013P_109F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_108F10013" targetNode="P_109F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_290F10013_I" deadCode="false" sourceNode="P_110F10013" targetNode="P_112F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_290F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_290F10013_O" deadCode="false" sourceNode="P_110F10013" targetNode="P_113F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_290F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10013_I" deadCode="false" sourceNode="P_110F10013" targetNode="P_114F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_292F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10013_O" deadCode="false" sourceNode="P_110F10013" targetNode="P_115F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_292F10013"/>
	</edges>
	<edges id="P_110F10013P_111F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_110F10013" targetNode="P_111F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_298F10013_I" deadCode="false" sourceNode="P_43F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_298F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_298F10013_O" deadCode="false" sourceNode="P_43F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_298F10013"/>
	</edges>
	<edges id="P_43F10013P_44F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_43F10013" targetNode="P_44F10013"/>
	<edges id="P_116F10013P_117F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_116F10013" targetNode="P_117F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_317F10013_I" deadCode="false" sourceNode="P_118F10013" targetNode="P_116F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_317F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_317F10013_O" deadCode="false" sourceNode="P_118F10013" targetNode="P_117F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_317F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_319F10013_I" deadCode="false" sourceNode="P_118F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_319F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_319F10013_O" deadCode="false" sourceNode="P_118F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_319F10013"/>
	</edges>
	<edges id="P_118F10013P_119F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_118F10013" targetNode="P_119F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10013_I" deadCode="false" sourceNode="P_112F10013" targetNode="P_118F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_321F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10013_O" deadCode="false" sourceNode="P_112F10013" targetNode="P_119F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_321F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_323F10013_I" deadCode="false" sourceNode="P_112F10013" targetNode="P_114F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_323F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_323F10013_O" deadCode="false" sourceNode="P_112F10013" targetNode="P_115F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_323F10013"/>
	</edges>
	<edges id="P_112F10013P_113F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_112F10013" targetNode="P_113F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_326F10013_I" deadCode="false" sourceNode="P_114F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_326F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_326F10013_O" deadCode="false" sourceNode="P_114F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_326F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10013_I" deadCode="false" sourceNode="P_114F10013" targetNode="P_120F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_336F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10013_O" deadCode="false" sourceNode="P_114F10013" targetNode="P_121F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_336F10013"/>
	</edges>
	<edges id="P_114F10013P_115F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_114F10013" targetNode="P_115F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_341F10013_I" deadCode="false" sourceNode="P_120F10013" targetNode="P_12F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_341F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_341F10013_O" deadCode="false" sourceNode="P_120F10013" targetNode="P_13F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_341F10013"/>
	</edges>
	<edges id="P_120F10013P_121F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_120F10013" targetNode="P_121F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_344F10013_I" deadCode="false" sourceNode="P_14F10013" targetNode="P_28F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_344F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_344F10013_O" deadCode="false" sourceNode="P_14F10013" targetNode="P_37F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_344F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_345F10013_I" deadCode="false" sourceNode="P_14F10013" targetNode="P_90F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_345F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_345F10013_O" deadCode="false" sourceNode="P_14F10013" targetNode="P_91F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_345F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_346F10013_I" deadCode="false" sourceNode="P_14F10013" targetNode="P_92F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_346F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_346F10013_O" deadCode="false" sourceNode="P_14F10013" targetNode="P_95F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_346F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_347F10013_I" deadCode="false" sourceNode="P_14F10013" targetNode="P_96F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_347F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_347F10013_O" deadCode="false" sourceNode="P_14F10013" targetNode="P_99F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_347F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_348F10013_I" deadCode="false" sourceNode="P_14F10013" targetNode="P_97F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_348F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_348F10013_O" deadCode="false" sourceNode="P_14F10013" targetNode="P_98F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_348F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_349F10013_I" deadCode="false" sourceNode="P_14F10013" targetNode="P_46F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_349F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_349F10013_O" deadCode="false" sourceNode="P_14F10013" targetNode="P_53F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_349F10013"/>
	</edges>
	<edges id="P_14F10013P_15F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10013" targetNode="P_15F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_353F10013_I" deadCode="false" sourceNode="P_16F10013" targetNode="P_42F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_353F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_353F10013_O" deadCode="false" sourceNode="P_16F10013" targetNode="P_45F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_353F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_354F10013_I" deadCode="false" sourceNode="P_16F10013" targetNode="P_58F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_354F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_354F10013_O" deadCode="false" sourceNode="P_16F10013" targetNode="P_65F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_354F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_355F10013_I" deadCode="false" sourceNode="P_16F10013" targetNode="P_66F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_355F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_355F10013_O" deadCode="false" sourceNode="P_16F10013" targetNode="P_73F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_355F10013"/>
	</edges>
	<edges id="P_16F10013P_17F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10013" targetNode="P_17F10013"/>
	<edges id="P_38F10013P_39F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10013" targetNode="P_39F10013"/>
	<edges id="P_69F10013P_70F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_69F10013" targetNode="P_70F10013"/>
	<edges id="P_54F10013P_55F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10013" targetNode="P_55F10013"/>
	<edges id="P_67F10013P_68F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_67F10013" targetNode="P_68F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_454F10013_I" deadCode="false" sourceNode="P_56F10013" targetNode="P_122F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_454F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_454F10013_O" deadCode="false" sourceNode="P_56F10013" targetNode="P_123F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_454F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_457F10013_I" deadCode="false" sourceNode="P_56F10013" targetNode="P_122F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_457F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_457F10013_O" deadCode="false" sourceNode="P_56F10013" targetNode="P_123F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_457F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_460F10013_I" deadCode="false" sourceNode="P_56F10013" targetNode="P_122F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_460F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_460F10013_O" deadCode="false" sourceNode="P_56F10013" targetNode="P_123F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_460F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_463F10013_I" deadCode="false" sourceNode="P_56F10013" targetNode="P_122F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_463F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_463F10013_O" deadCode="false" sourceNode="P_56F10013" targetNode="P_123F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_463F10013"/>
	</edges>
	<edges id="P_56F10013P_57F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10013" targetNode="P_57F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_467F10013_I" deadCode="false" sourceNode="P_40F10013" targetNode="P_124F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_467F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_467F10013_O" deadCode="false" sourceNode="P_40F10013" targetNode="P_125F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_467F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_470F10013_I" deadCode="false" sourceNode="P_40F10013" targetNode="P_124F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_470F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_470F10013_O" deadCode="false" sourceNode="P_40F10013" targetNode="P_125F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_470F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_473F10013_I" deadCode="false" sourceNode="P_40F10013" targetNode="P_124F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_473F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_473F10013_O" deadCode="false" sourceNode="P_40F10013" targetNode="P_125F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_473F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_476F10013_I" deadCode="false" sourceNode="P_40F10013" targetNode="P_124F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_476F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_476F10013_O" deadCode="false" sourceNode="P_40F10013" targetNode="P_125F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_476F10013"/>
	</edges>
	<edges id="P_40F10013P_41F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10013" targetNode="P_41F10013"/>
	<edges id="P_71F10013P_72F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_71F10013" targetNode="P_72F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_481F10013_I" deadCode="false" sourceNode="P_10F10013" targetNode="P_126F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_481F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_481F10013_O" deadCode="false" sourceNode="P_10F10013" targetNode="P_127F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_481F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_483F10013_I" deadCode="false" sourceNode="P_10F10013" targetNode="P_128F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_483F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_483F10013_O" deadCode="false" sourceNode="P_10F10013" targetNode="P_129F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_483F10013"/>
	</edges>
	<edges id="P_10F10013P_11F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10013" targetNode="P_11F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_488F10013_I" deadCode="false" sourceNode="P_126F10013" targetNode="P_122F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_488F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_488F10013_O" deadCode="false" sourceNode="P_126F10013" targetNode="P_123F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_488F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_493F10013_I" deadCode="false" sourceNode="P_126F10013" targetNode="P_122F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_493F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_493F10013_O" deadCode="false" sourceNode="P_126F10013" targetNode="P_123F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_493F10013"/>
	</edges>
	<edges id="P_126F10013P_127F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_126F10013" targetNode="P_127F10013"/>
	<edges id="P_128F10013P_129F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_128F10013" targetNode="P_129F10013"/>
	<edges id="P_122F10013P_123F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_122F10013" targetNode="P_123F10013"/>
	<edges xsi:type="cbl:PerformEdge" id="S_522F10013_I" deadCode="false" sourceNode="P_124F10013" targetNode="P_132F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_522F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_522F10013_O" deadCode="false" sourceNode="P_124F10013" targetNode="P_133F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_522F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_523F10013_I" deadCode="false" sourceNode="P_124F10013" targetNode="P_134F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_523F10013"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_523F10013_O" deadCode="false" sourceNode="P_124F10013" targetNode="P_135F10013">
		<representations href="../../../cobol/IABS0900.cbl.cobModel#S_523F10013"/>
	</edges>
	<edges id="P_124F10013P_125F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_124F10013" targetNode="P_125F10013"/>
	<edges id="P_132F10013P_133F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_132F10013" targetNode="P_133F10013"/>
	<edges id="P_134F10013P_135F10013" xsi:type="cbl:FallThroughEdge" sourceNode="P_134F10013" targetNode="P_135F10013"/>
	<edges xsi:type="cbl:DataEdge" id="S_53F10013_POS1" deadCode="false" targetNode="P_26F10013" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_53F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_55F10013_POS1" deadCode="false" targetNode="P_26F10013" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_55F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_57F10013_POS1" deadCode="false" targetNode="P_26F10013" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_57F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_59F10013_POS1" deadCode="false" targetNode="P_26F10013" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_59F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_67F10013_POS1" deadCode="false" targetNode="P_29F10013" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_73F10013_POS1" deadCode="false" targetNode="P_31F10013" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_73F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_79F10013_POS1" deadCode="false" targetNode="P_33F10013" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_79F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_85F10013_POS1" deadCode="false" targetNode="P_35F10013" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_85F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_91F10013_POS1" deadCode="false" targetNode="P_42F10013" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_91F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_106F10013_POS1" deadCode="false" sourceNode="P_47F10013" targetNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_106F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_119F10013_POS1" deadCode="false" sourceNode="P_59F10013" targetNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_119F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_124F10013_POS1" deadCode="false" sourceNode="P_61F10013" targetNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_124F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_132F10013_POS1" deadCode="false" sourceNode="P_63F10013" targetNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_132F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_141F10013_POS1" deadCode="false" sourceNode="P_66F10013" targetNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_141F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_152F10013_POS1" deadCode="false" sourceNode="P_74F10013" targetNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_152F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_159F10013_POS1" deadCode="false" sourceNode="P_76F10013" targetNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_159F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_166F10013_POS1" deadCode="false" sourceNode="P_78F10013" targetNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_166F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_173F10013_POS1" deadCode="false" sourceNode="P_80F10013" targetNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_173F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_186F10013_POS1" deadCode="false" sourceNode="P_82F10013" targetNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_186F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_193F10013_POS1" deadCode="false" sourceNode="P_84F10013" targetNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_193F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_200F10013_POS1" deadCode="false" sourceNode="P_86F10013" targetNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_200F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_207F10013_POS1" deadCode="false" sourceNode="P_88F10013" targetNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_207F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_225F10013_POS1" deadCode="false" targetNode="P_93F10013" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_225F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_226F10013_POS1" deadCode="false" targetNode="P_93F10013" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_226F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_227F10013_POS1" deadCode="false" targetNode="P_93F10013" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_227F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_228F10013_POS1" deadCode="false" targetNode="P_93F10013" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_228F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_244F10013_POS1" deadCode="false" targetNode="P_102F10013" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_244F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_255F10013_POS1" deadCode="false" targetNode="P_104F10013" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_255F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_266F10013_POS1" deadCode="false" targetNode="P_106F10013" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_266F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_277F10013_POS1" deadCode="false" targetNode="P_108F10013" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_277F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_297F10013_POS1" deadCode="false" targetNode="P_43F10013" sourceNode="DB2_DETT_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_297F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_318F10013_POS1" deadCode="false" targetNode="P_118F10013" sourceNode="DB2_DETT_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_318F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_325F10013_POS1" deadCode="false" targetNode="P_114F10013" sourceNode="DB2_DETT_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_325F10013"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_340F10013_POS1" deadCode="false" targetNode="P_120F10013" sourceNode="DB2_DETT_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_340F10013"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVSXXXX" cbl:id="LVVSXXXX" xsi:id="LVVSXXXX" packageRef="LVVSXXXX.igd#LVVSXXXX" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVSXXXX_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10369" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS1980.cbl.cobModel#SC_1F10369"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10369" deadCode="false" name="PROGRAM_LVVSXXXX_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS1980.cbl.cobModel#P_1F10369"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10369" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS1980.cbl.cobModel#P_2F10369"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10369" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS1980.cbl.cobModel#P_3F10369"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10369" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS1980.cbl.cobModel#P_4F10369"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10369" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS1980.cbl.cobModel#P_5F10369"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10369" deadCode="true" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS1980.cbl.cobModel#P_8F10369"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10369" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS1980.cbl.cobModel#P_9F10369"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10369" deadCode="false" name="S1200-CALCOLO-VAR">
				<representations href="../../../cobol/LVVS1980.cbl.cobModel#P_10F10369"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10369" deadCode="false" name="S1200-EX">
				<representations href="../../../cobol/LVVS1980.cbl.cobModel#P_11F10369"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10369" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS1980.cbl.cobModel#P_6F10369"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10369" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS1980.cbl.cobModel#P_7F10369"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSPRE0" name="IDBSPRE0">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10079"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10369P_1F10369" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10369" targetNode="P_1F10369"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10369_I" deadCode="false" sourceNode="P_1F10369" targetNode="P_2F10369">
		<representations href="../../../cobol/LVVS1980.cbl.cobModel#S_1F10369"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10369_O" deadCode="false" sourceNode="P_1F10369" targetNode="P_3F10369">
		<representations href="../../../cobol/LVVS1980.cbl.cobModel#S_1F10369"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10369_I" deadCode="false" sourceNode="P_1F10369" targetNode="P_4F10369">
		<representations href="../../../cobol/LVVS1980.cbl.cobModel#S_2F10369"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10369_O" deadCode="false" sourceNode="P_1F10369" targetNode="P_5F10369">
		<representations href="../../../cobol/LVVS1980.cbl.cobModel#S_2F10369"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10369_I" deadCode="false" sourceNode="P_1F10369" targetNode="P_6F10369">
		<representations href="../../../cobol/LVVS1980.cbl.cobModel#S_3F10369"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10369_O" deadCode="false" sourceNode="P_1F10369" targetNode="P_7F10369">
		<representations href="../../../cobol/LVVS1980.cbl.cobModel#S_3F10369"/>
	</edges>
	<edges id="P_2F10369P_3F10369" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10369" targetNode="P_3F10369"/>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10369_I" deadCode="true" sourceNode="P_4F10369" targetNode="P_8F10369">
		<representations href="../../../cobol/LVVS1980.cbl.cobModel#S_9F10369"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10369_O" deadCode="true" sourceNode="P_4F10369" targetNode="P_9F10369">
		<representations href="../../../cobol/LVVS1980.cbl.cobModel#S_9F10369"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10369_I" deadCode="false" sourceNode="P_4F10369" targetNode="P_10F10369">
		<representations href="../../../cobol/LVVS1980.cbl.cobModel#S_11F10369"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10369_O" deadCode="false" sourceNode="P_4F10369" targetNode="P_11F10369">
		<representations href="../../../cobol/LVVS1980.cbl.cobModel#S_11F10369"/>
	</edges>
	<edges id="P_4F10369P_5F10369" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10369" targetNode="P_5F10369"/>
	<edges id="P_8F10369P_9F10369" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10369" targetNode="P_9F10369"/>
	<edges id="P_10F10369P_11F10369" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10369" targetNode="P_11F10369"/>
	<edges xsi:type="cbl:CallEdge" id="S_26F10369" deadCode="false" name="Dynamic IDBSPRE0" sourceNode="P_10F10369" targetNode="IDBSPRE0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_26F10369"></representations>
	</edges>
</Package>

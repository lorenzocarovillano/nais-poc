<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0095" cbl:id="LVVS0095" xsi:id="LVVS0095" packageRef="LVVS0095.igd#LVVS0095" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0095_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10343" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0095.cbl.cobModel#SC_1F10343"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10343" deadCode="false" name="PROGRAM_LVVS0095_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0095.cbl.cobModel#P_1F10343"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10343" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0095.cbl.cobModel#P_2F10343"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10343" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0095.cbl.cobModel#P_3F10343"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10343" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0095.cbl.cobModel#P_4F10343"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10343" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0095.cbl.cobModel#P_5F10343"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10343" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0095.cbl.cobModel#P_8F10343"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10343" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0095.cbl.cobModel#P_9F10343"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10343" deadCode="false" name="S1250-VERIFICA-POLI">
				<representations href="../../../cobol/LVVS0095.cbl.cobModel#P_10F10343"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10343" deadCode="false" name="S1250-EX">
				<representations href="../../../cobol/LVVS0095.cbl.cobModel#P_11F10343"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10343" deadCode="false" name="S1255-CALCOLA-DATA2">
				<representations href="../../../cobol/LVVS0095.cbl.cobModel#P_12F10343"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10343" deadCode="false" name="S1255-EX">
				<representations href="../../../cobol/LVVS0095.cbl.cobModel#P_13F10343"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10343" deadCode="false" name="S1260-CALCOLA-DIFF">
				<representations href="../../../cobol/LVVS0095.cbl.cobModel#P_14F10343"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10343" deadCode="false" name="S1260-EX">
				<representations href="../../../cobol/LVVS0095.cbl.cobModel#P_15F10343"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10343" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0095.cbl.cobModel#P_6F10343"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10343" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0095.cbl.cobModel#P_7F10343"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS2960" name="LDBS2960">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10193"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS2970" name="LDBS2970">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10194"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0010" name="LCCS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10122"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10343P_1F10343" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10343" targetNode="P_1F10343"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10343_I" deadCode="false" sourceNode="P_1F10343" targetNode="P_2F10343">
		<representations href="../../../cobol/LVVS0095.cbl.cobModel#S_1F10343"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10343_O" deadCode="false" sourceNode="P_1F10343" targetNode="P_3F10343">
		<representations href="../../../cobol/LVVS0095.cbl.cobModel#S_1F10343"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10343_I" deadCode="false" sourceNode="P_1F10343" targetNode="P_4F10343">
		<representations href="../../../cobol/LVVS0095.cbl.cobModel#S_2F10343"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10343_O" deadCode="false" sourceNode="P_1F10343" targetNode="P_5F10343">
		<representations href="../../../cobol/LVVS0095.cbl.cobModel#S_2F10343"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10343_I" deadCode="false" sourceNode="P_1F10343" targetNode="P_6F10343">
		<representations href="../../../cobol/LVVS0095.cbl.cobModel#S_3F10343"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10343_O" deadCode="false" sourceNode="P_1F10343" targetNode="P_7F10343">
		<representations href="../../../cobol/LVVS0095.cbl.cobModel#S_3F10343"/>
	</edges>
	<edges id="P_2F10343P_3F10343" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10343" targetNode="P_3F10343"/>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10343_I" deadCode="false" sourceNode="P_4F10343" targetNode="P_8F10343">
		<representations href="../../../cobol/LVVS0095.cbl.cobModel#S_13F10343"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10343_O" deadCode="false" sourceNode="P_4F10343" targetNode="P_9F10343">
		<representations href="../../../cobol/LVVS0095.cbl.cobModel#S_13F10343"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10343_I" deadCode="false" sourceNode="P_4F10343" targetNode="P_10F10343">
		<representations href="../../../cobol/LVVS0095.cbl.cobModel#S_16F10343"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10343_O" deadCode="false" sourceNode="P_4F10343" targetNode="P_11F10343">
		<representations href="../../../cobol/LVVS0095.cbl.cobModel#S_16F10343"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10343_I" deadCode="false" sourceNode="P_4F10343" targetNode="P_12F10343">
		<representations href="../../../cobol/LVVS0095.cbl.cobModel#S_19F10343"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10343_O" deadCode="false" sourceNode="P_4F10343" targetNode="P_13F10343">
		<representations href="../../../cobol/LVVS0095.cbl.cobModel#S_19F10343"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10343_I" deadCode="false" sourceNode="P_4F10343" targetNode="P_14F10343">
		<representations href="../../../cobol/LVVS0095.cbl.cobModel#S_21F10343"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10343_O" deadCode="false" sourceNode="P_4F10343" targetNode="P_15F10343">
		<representations href="../../../cobol/LVVS0095.cbl.cobModel#S_21F10343"/>
	</edges>
	<edges id="P_4F10343P_5F10343" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10343" targetNode="P_5F10343"/>
	<edges id="P_8F10343P_9F10343" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10343" targetNode="P_9F10343"/>
	<edges id="P_10F10343P_11F10343" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10343" targetNode="P_11F10343"/>
	<edges id="P_12F10343P_13F10343" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10343" targetNode="P_13F10343"/>
	<edges id="P_14F10343P_15F10343" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10343" targetNode="P_15F10343"/>
	<edges xsi:type="cbl:CallEdge" id="S_35F10343" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_10F10343" targetNode="LDBS2960">
		<representations href="../../../cobol/../importantStmts.cobModel#S_35F10343"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_55F10343" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_12F10343" targetNode="LDBS2970">
		<representations href="../../../cobol/../importantStmts.cobModel#S_55F10343"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_72F10343" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_14F10343" targetNode="LCCS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_72F10343"></representations>
	</edges>
</Package>

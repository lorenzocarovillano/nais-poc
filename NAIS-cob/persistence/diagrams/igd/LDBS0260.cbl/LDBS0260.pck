<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS0260" cbl:id="LDBS0260" xsi:id="LDBS0260" packageRef="LDBS0260.igd#LDBS0260" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS0260_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10143" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS0260.cbl.cobModel#SC_1F10143"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10143" deadCode="false" name="PROGRAM_LDBS0260_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_1F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10143" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_2F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10143" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_3F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10143" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_8F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10143" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_9F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10143" deadCode="false" name="A200-ELABORA-WC">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_4F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10143" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_5F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10143" deadCode="false" name="A250-SELECT">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_20F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10143" deadCode="false" name="A250-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_21F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10143" deadCode="false" name="A205-DECLARE-CURSOR-WC01">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_26F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10143" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_27F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10143" deadCode="false" name="A210-DECLARE-CURSOR-WC02">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_28F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10143" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_29F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10143" deadCode="false" name="A260-OPEN-CURSOR-WC">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_10F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10143" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_11F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10143" deadCode="false" name="A270-CLOSE-CURSOR-WC">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_12F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10143" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_13F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10143" deadCode="false" name="A280-FETCH-FIRST-WC">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_14F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10143" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_15F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10143" deadCode="false" name="A290-FETCH-NEXT-WC01">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_16F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10143" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_17F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10143" deadCode="false" name="A295-FETCH-NEXT-WC02">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_18F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10143" deadCode="false" name="A295-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_19F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10143" deadCode="true" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_22F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10143" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_23F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10143" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_24F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10143" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_25F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10143" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_30F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10143" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_31F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10143" deadCode="true" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_32F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10143" deadCode="true" name="Z200-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_33F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10143" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_6F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10143" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_7F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10143" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_34F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10143" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_35F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10143" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_36F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10143" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_37F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10143" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_38F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10143" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_39F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10143" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_40F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10143" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_41F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10143" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_42F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10143" deadCode="true" name="Z800-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_47F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10143" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_43F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10143" deadCode="true" name="Z810-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_44F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10143" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_45F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10143" deadCode="true" name="Z820-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_46F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10143" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_48F10143"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10143" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS0260.cbl.cobModel#P_49F10143"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_MATR_MOVIMENTO" name="MATR_MOVIMENTO">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_MATR_MOVIMENTO"/>
		</children>
	</packageNode>
	<edges id="SC_1F10143P_1F10143" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10143" targetNode="P_1F10143"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10143_I" deadCode="false" sourceNode="P_1F10143" targetNode="P_2F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_1F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10143_O" deadCode="false" sourceNode="P_1F10143" targetNode="P_3F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_1F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10143_I" deadCode="false" sourceNode="P_1F10143" targetNode="P_4F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_4F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10143_O" deadCode="false" sourceNode="P_1F10143" targetNode="P_5F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_4F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10143_I" deadCode="false" sourceNode="P_2F10143" targetNode="P_6F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_16F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10143_O" deadCode="false" sourceNode="P_2F10143" targetNode="P_7F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_16F10143"/>
	</edges>
	<edges id="P_2F10143P_3F10143" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10143" targetNode="P_3F10143"/>
	<edges id="P_8F10143P_9F10143" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10143" targetNode="P_9F10143"/>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10143_I" deadCode="false" sourceNode="P_4F10143" targetNode="P_10F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_30F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10143_O" deadCode="false" sourceNode="P_4F10143" targetNode="P_11F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_30F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10143_I" deadCode="false" sourceNode="P_4F10143" targetNode="P_12F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_31F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10143_O" deadCode="false" sourceNode="P_4F10143" targetNode="P_13F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_31F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10143_I" deadCode="false" sourceNode="P_4F10143" targetNode="P_14F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_32F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10143_O" deadCode="false" sourceNode="P_4F10143" targetNode="P_15F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_32F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10143_I" deadCode="false" sourceNode="P_4F10143" targetNode="P_16F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_34F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10143_O" deadCode="false" sourceNode="P_4F10143" targetNode="P_17F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_34F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10143_I" deadCode="false" sourceNode="P_4F10143" targetNode="P_18F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_35F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10143_O" deadCode="false" sourceNode="P_4F10143" targetNode="P_19F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_35F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10143_I" deadCode="false" sourceNode="P_4F10143" targetNode="P_20F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_36F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10143_O" deadCode="false" sourceNode="P_4F10143" targetNode="P_21F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_36F10143"/>
	</edges>
	<edges id="P_4F10143P_5F10143" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10143" targetNode="P_5F10143"/>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10143_I" deadCode="false" sourceNode="P_20F10143" targetNode="P_8F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_40F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10143_O" deadCode="false" sourceNode="P_20F10143" targetNode="P_9F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_40F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10143_I" deadCode="true" sourceNode="P_20F10143" targetNode="P_22F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_42F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10143_O" deadCode="true" sourceNode="P_20F10143" targetNode="P_23F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_42F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10143_I" deadCode="false" sourceNode="P_20F10143" targetNode="P_24F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_43F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10143_O" deadCode="false" sourceNode="P_20F10143" targetNode="P_25F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_43F10143"/>
	</edges>
	<edges id="P_20F10143P_21F10143" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10143" targetNode="P_21F10143"/>
	<edges id="P_26F10143P_27F10143" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10143" targetNode="P_27F10143"/>
	<edges id="P_28F10143P_29F10143" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10143" targetNode="P_29F10143"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10143_I" deadCode="false" sourceNode="P_10F10143" targetNode="P_26F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_53F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10143_O" deadCode="false" sourceNode="P_10F10143" targetNode="P_27F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_53F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10143_I" deadCode="false" sourceNode="P_10F10143" targetNode="P_28F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_55F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10143_O" deadCode="false" sourceNode="P_10F10143" targetNode="P_29F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_55F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10143_I" deadCode="false" sourceNode="P_10F10143" targetNode="P_8F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_57F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10143_O" deadCode="false" sourceNode="P_10F10143" targetNode="P_9F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_57F10143"/>
	</edges>
	<edges id="P_10F10143P_11F10143" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10143" targetNode="P_11F10143"/>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10143_I" deadCode="false" sourceNode="P_12F10143" targetNode="P_8F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_62F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10143_O" deadCode="false" sourceNode="P_12F10143" targetNode="P_9F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_62F10143"/>
	</edges>
	<edges id="P_12F10143P_13F10143" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10143" targetNode="P_13F10143"/>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10143_I" deadCode="false" sourceNode="P_14F10143" targetNode="P_10F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_64F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10143_O" deadCode="false" sourceNode="P_14F10143" targetNode="P_11F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_64F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10143_I" deadCode="false" sourceNode="P_14F10143" targetNode="P_16F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_67F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10143_O" deadCode="false" sourceNode="P_14F10143" targetNode="P_17F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_67F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10143_I" deadCode="false" sourceNode="P_14F10143" targetNode="P_18F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_68F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10143_O" deadCode="false" sourceNode="P_14F10143" targetNode="P_19F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_68F10143"/>
	</edges>
	<edges id="P_14F10143P_15F10143" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10143" targetNode="P_15F10143"/>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10143_I" deadCode="false" sourceNode="P_16F10143" targetNode="P_8F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_71F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10143_O" deadCode="false" sourceNode="P_16F10143" targetNode="P_9F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_71F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10143_I" deadCode="true" sourceNode="P_16F10143" targetNode="P_22F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_73F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10143_O" deadCode="true" sourceNode="P_16F10143" targetNode="P_23F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_73F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10143_I" deadCode="false" sourceNode="P_16F10143" targetNode="P_24F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_74F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10143_O" deadCode="false" sourceNode="P_16F10143" targetNode="P_25F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_74F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10143_I" deadCode="false" sourceNode="P_16F10143" targetNode="P_12F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_76F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10143_O" deadCode="false" sourceNode="P_16F10143" targetNode="P_13F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_76F10143"/>
	</edges>
	<edges id="P_16F10143P_17F10143" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10143" targetNode="P_17F10143"/>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10143_I" deadCode="false" sourceNode="P_18F10143" targetNode="P_8F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_81F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10143_O" deadCode="false" sourceNode="P_18F10143" targetNode="P_9F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_81F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10143_I" deadCode="true" sourceNode="P_18F10143" targetNode="P_22F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_83F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10143_O" deadCode="true" sourceNode="P_18F10143" targetNode="P_23F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_83F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10143_I" deadCode="false" sourceNode="P_18F10143" targetNode="P_24F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_84F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10143_O" deadCode="false" sourceNode="P_18F10143" targetNode="P_25F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_84F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10143_I" deadCode="false" sourceNode="P_18F10143" targetNode="P_12F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_86F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10143_O" deadCode="false" sourceNode="P_18F10143" targetNode="P_13F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_86F10143"/>
	</edges>
	<edges id="P_18F10143P_19F10143" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10143" targetNode="P_19F10143"/>
	<edges id="P_22F10143P_23F10143" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10143" targetNode="P_23F10143"/>
	<edges id="P_24F10143P_25F10143" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10143" targetNode="P_25F10143"/>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10143_I" deadCode="false" sourceNode="P_6F10143" targetNode="P_34F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_109F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10143_O" deadCode="false" sourceNode="P_6F10143" targetNode="P_35F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_109F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10143_I" deadCode="false" sourceNode="P_6F10143" targetNode="P_36F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_111F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10143_O" deadCode="false" sourceNode="P_6F10143" targetNode="P_37F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_111F10143"/>
	</edges>
	<edges id="P_6F10143P_7F10143" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10143" targetNode="P_7F10143"/>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10143_I" deadCode="true" sourceNode="P_34F10143" targetNode="P_38F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_116F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10143_O" deadCode="true" sourceNode="P_34F10143" targetNode="P_39F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_116F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10143_I" deadCode="true" sourceNode="P_34F10143" targetNode="P_38F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_121F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10143_O" deadCode="true" sourceNode="P_34F10143" targetNode="P_39F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_121F10143"/>
	</edges>
	<edges id="P_34F10143P_35F10143" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10143" targetNode="P_35F10143"/>
	<edges id="P_36F10143P_37F10143" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10143" targetNode="P_37F10143"/>
	<edges id="P_38F10143P_39F10143" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10143" targetNode="P_39F10143"/>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10143_I" deadCode="true" sourceNode="P_42F10143" targetNode="P_43F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_150F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10143_O" deadCode="true" sourceNode="P_42F10143" targetNode="P_44F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_150F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_151F10143_I" deadCode="true" sourceNode="P_42F10143" targetNode="P_45F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_151F10143"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_151F10143_O" deadCode="true" sourceNode="P_42F10143" targetNode="P_46F10143">
		<representations href="../../../cobol/LDBS0260.cbl.cobModel#S_151F10143"/>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_39F10143_POS1" deadCode="false" targetNode="P_20F10143" sourceNode="DB2_MATR_MOVIMENTO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_39F10143"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_54F10143_POS1" deadCode="false" targetNode="P_10F10143" sourceNode="DB2_MATR_MOVIMENTO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_54F10143"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_56F10143_POS1" deadCode="false" targetNode="P_10F10143" sourceNode="DB2_MATR_MOVIMENTO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_56F10143"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_60F10143_POS1" deadCode="false" targetNode="P_12F10143" sourceNode="DB2_MATR_MOVIMENTO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_60F10143"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_61F10143_POS1" deadCode="false" targetNode="P_12F10143" sourceNode="DB2_MATR_MOVIMENTO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_61F10143"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_70F10143_POS1" deadCode="false" targetNode="P_16F10143" sourceNode="DB2_MATR_MOVIMENTO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_70F10143"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_80F10143_POS1" deadCode="false" targetNode="P_18F10143" sourceNode="DB2_MATR_MOVIMENTO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_80F10143"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS2410" cbl:id="LDBS2410" xsi:id="LDBS2410" packageRef="LDBS2410.igd#LDBS2410" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS2410_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10172" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS2410.cbl.cobModel#SC_1F10172"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10172" deadCode="false" name="PROGRAM_LDBS2410_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_1F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10172" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_2F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10172" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_3F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10172" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_12F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10172" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_13F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10172" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_4F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10172" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_5F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10172" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_6F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10172" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_7F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10172" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_8F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10172" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_9F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10172" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_44F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10172" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_49F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10172" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_14F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10172" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_15F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10172" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_16F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10172" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_17F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10172" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_18F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10172" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_19F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10172" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_20F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10172" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_21F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10172" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_22F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10172" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_23F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10172" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_50F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10172" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_51F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10172" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_24F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10172" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_25F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10172" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_26F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10172" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_27F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10172" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_28F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10172" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_29F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10172" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_30F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10172" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_31F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10172" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_32F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10172" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_33F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10172" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_52F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10172" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_53F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10172" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_34F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10172" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_35F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10172" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_36F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10172" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_37F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10172" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_38F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10172" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_39F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10172" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_40F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10172" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_41F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10172" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_42F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10172" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_43F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10172" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_54F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10172" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_55F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10172" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_60F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10172" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_63F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10172" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_56F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10172" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_57F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10172" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_45F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10172" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_46F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10172" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_47F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10172" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_48F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10172" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_58F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10172" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_59F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10172" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_10F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10172" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_11F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10172" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_66F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10172" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_67F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10172" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_68F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10172" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_69F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10172" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_61F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10172" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_62F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10172" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_70F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10172" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_71F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10172" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_64F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10172" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_65F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10172" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_72F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10172" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_73F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10172" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_74F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10172" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_75F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10172" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_76F10172"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10172" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS2410.cbl.cobModel#P_77F10172"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_OGG_BLOCCO" name="OGG_BLOCCO">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_OGG_BLOCCO"/>
		</children>
	</packageNode>
	<edges id="SC_1F10172P_1F10172" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10172" targetNode="P_1F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10172_I" deadCode="false" sourceNode="P_1F10172" targetNode="P_2F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_1F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10172_O" deadCode="false" sourceNode="P_1F10172" targetNode="P_3F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_1F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10172_I" deadCode="false" sourceNode="P_1F10172" targetNode="P_4F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_5F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10172_O" deadCode="false" sourceNode="P_1F10172" targetNode="P_5F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_5F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10172_I" deadCode="false" sourceNode="P_1F10172" targetNode="P_6F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_9F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10172_O" deadCode="false" sourceNode="P_1F10172" targetNode="P_7F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_9F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10172_I" deadCode="false" sourceNode="P_1F10172" targetNode="P_8F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_13F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10172_O" deadCode="false" sourceNode="P_1F10172" targetNode="P_9F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_13F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10172_I" deadCode="false" sourceNode="P_2F10172" targetNode="P_10F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_22F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10172_O" deadCode="false" sourceNode="P_2F10172" targetNode="P_11F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_22F10172"/>
	</edges>
	<edges id="P_2F10172P_3F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10172" targetNode="P_3F10172"/>
	<edges id="P_12F10172P_13F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10172" targetNode="P_13F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10172_I" deadCode="false" sourceNode="P_4F10172" targetNode="P_14F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_35F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10172_O" deadCode="false" sourceNode="P_4F10172" targetNode="P_15F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_35F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10172_I" deadCode="false" sourceNode="P_4F10172" targetNode="P_16F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_36F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10172_O" deadCode="false" sourceNode="P_4F10172" targetNode="P_17F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_36F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10172_I" deadCode="false" sourceNode="P_4F10172" targetNode="P_18F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_37F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10172_O" deadCode="false" sourceNode="P_4F10172" targetNode="P_19F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_37F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10172_I" deadCode="false" sourceNode="P_4F10172" targetNode="P_20F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_38F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10172_O" deadCode="false" sourceNode="P_4F10172" targetNode="P_21F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_38F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10172_I" deadCode="false" sourceNode="P_4F10172" targetNode="P_22F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_39F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10172_O" deadCode="false" sourceNode="P_4F10172" targetNode="P_23F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_39F10172"/>
	</edges>
	<edges id="P_4F10172P_5F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10172" targetNode="P_5F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10172_I" deadCode="false" sourceNode="P_6F10172" targetNode="P_24F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_43F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10172_O" deadCode="false" sourceNode="P_6F10172" targetNode="P_25F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_43F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10172_I" deadCode="false" sourceNode="P_6F10172" targetNode="P_26F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_44F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10172_O" deadCode="false" sourceNode="P_6F10172" targetNode="P_27F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_44F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10172_I" deadCode="false" sourceNode="P_6F10172" targetNode="P_28F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_45F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10172_O" deadCode="false" sourceNode="P_6F10172" targetNode="P_29F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_45F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10172_I" deadCode="false" sourceNode="P_6F10172" targetNode="P_30F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_46F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10172_O" deadCode="false" sourceNode="P_6F10172" targetNode="P_31F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_46F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10172_I" deadCode="false" sourceNode="P_6F10172" targetNode="P_32F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_47F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10172_O" deadCode="false" sourceNode="P_6F10172" targetNode="P_33F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_47F10172"/>
	</edges>
	<edges id="P_6F10172P_7F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10172" targetNode="P_7F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10172_I" deadCode="false" sourceNode="P_8F10172" targetNode="P_34F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_51F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10172_O" deadCode="false" sourceNode="P_8F10172" targetNode="P_35F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_51F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10172_I" deadCode="false" sourceNode="P_8F10172" targetNode="P_36F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_52F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10172_O" deadCode="false" sourceNode="P_8F10172" targetNode="P_37F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_52F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10172_I" deadCode="false" sourceNode="P_8F10172" targetNode="P_38F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_53F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10172_O" deadCode="false" sourceNode="P_8F10172" targetNode="P_39F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_53F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10172_I" deadCode="false" sourceNode="P_8F10172" targetNode="P_40F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_54F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10172_O" deadCode="false" sourceNode="P_8F10172" targetNode="P_41F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_54F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10172_I" deadCode="false" sourceNode="P_8F10172" targetNode="P_42F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_55F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10172_O" deadCode="false" sourceNode="P_8F10172" targetNode="P_43F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_55F10172"/>
	</edges>
	<edges id="P_8F10172P_9F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10172" targetNode="P_9F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10172_I" deadCode="false" sourceNode="P_44F10172" targetNode="P_45F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_58F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10172_O" deadCode="false" sourceNode="P_44F10172" targetNode="P_46F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_58F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10172_I" deadCode="false" sourceNode="P_44F10172" targetNode="P_47F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_59F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10172_O" deadCode="false" sourceNode="P_44F10172" targetNode="P_48F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_59F10172"/>
	</edges>
	<edges id="P_44F10172P_49F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10172" targetNode="P_49F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10172_I" deadCode="false" sourceNode="P_14F10172" targetNode="P_45F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_62F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10172_O" deadCode="false" sourceNode="P_14F10172" targetNode="P_46F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_62F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10172_I" deadCode="false" sourceNode="P_14F10172" targetNode="P_47F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_63F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10172_O" deadCode="false" sourceNode="P_14F10172" targetNode="P_48F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_63F10172"/>
	</edges>
	<edges id="P_14F10172P_15F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10172" targetNode="P_15F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10172_I" deadCode="false" sourceNode="P_16F10172" targetNode="P_44F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_66F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10172_O" deadCode="false" sourceNode="P_16F10172" targetNode="P_49F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_66F10172"/>
	</edges>
	<edges id="P_16F10172P_17F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10172" targetNode="P_17F10172"/>
	<edges id="P_18F10172P_19F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10172" targetNode="P_19F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10172_I" deadCode="false" sourceNode="P_20F10172" targetNode="P_16F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_71F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10172_O" deadCode="false" sourceNode="P_20F10172" targetNode="P_17F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_71F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10172_I" deadCode="false" sourceNode="P_20F10172" targetNode="P_22F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_73F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10172_O" deadCode="false" sourceNode="P_20F10172" targetNode="P_23F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_73F10172"/>
	</edges>
	<edges id="P_20F10172P_21F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10172" targetNode="P_21F10172"/>
	<edges id="P_22F10172P_23F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10172" targetNode="P_23F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10172_I" deadCode="false" sourceNode="P_50F10172" targetNode="P_45F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_77F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10172_O" deadCode="false" sourceNode="P_50F10172" targetNode="P_46F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_77F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10172_I" deadCode="false" sourceNode="P_50F10172" targetNode="P_47F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_78F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10172_O" deadCode="false" sourceNode="P_50F10172" targetNode="P_48F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_78F10172"/>
	</edges>
	<edges id="P_50F10172P_51F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10172" targetNode="P_51F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10172_I" deadCode="false" sourceNode="P_24F10172" targetNode="P_45F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_81F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10172_O" deadCode="false" sourceNode="P_24F10172" targetNode="P_46F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_81F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10172_I" deadCode="false" sourceNode="P_24F10172" targetNode="P_47F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_82F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10172_O" deadCode="false" sourceNode="P_24F10172" targetNode="P_48F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_82F10172"/>
	</edges>
	<edges id="P_24F10172P_25F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10172" targetNode="P_25F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10172_I" deadCode="false" sourceNode="P_26F10172" targetNode="P_50F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_85F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10172_O" deadCode="false" sourceNode="P_26F10172" targetNode="P_51F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_85F10172"/>
	</edges>
	<edges id="P_26F10172P_27F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10172" targetNode="P_27F10172"/>
	<edges id="P_28F10172P_29F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10172" targetNode="P_29F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10172_I" deadCode="false" sourceNode="P_30F10172" targetNode="P_26F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_90F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10172_O" deadCode="false" sourceNode="P_30F10172" targetNode="P_27F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_90F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10172_I" deadCode="false" sourceNode="P_30F10172" targetNode="P_32F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_92F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10172_O" deadCode="false" sourceNode="P_30F10172" targetNode="P_33F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_92F10172"/>
	</edges>
	<edges id="P_30F10172P_31F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10172" targetNode="P_31F10172"/>
	<edges id="P_32F10172P_33F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10172" targetNode="P_33F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10172_I" deadCode="false" sourceNode="P_52F10172" targetNode="P_45F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_96F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10172_O" deadCode="false" sourceNode="P_52F10172" targetNode="P_46F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_96F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10172_I" deadCode="false" sourceNode="P_52F10172" targetNode="P_47F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_97F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10172_O" deadCode="false" sourceNode="P_52F10172" targetNode="P_48F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_97F10172"/>
	</edges>
	<edges id="P_52F10172P_53F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10172" targetNode="P_53F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10172_I" deadCode="false" sourceNode="P_34F10172" targetNode="P_45F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_101F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10172_O" deadCode="false" sourceNode="P_34F10172" targetNode="P_46F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_101F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10172_I" deadCode="false" sourceNode="P_34F10172" targetNode="P_47F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_102F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10172_O" deadCode="false" sourceNode="P_34F10172" targetNode="P_48F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_102F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10172_I" deadCode="false" sourceNode="P_34F10172" targetNode="P_12F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_104F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10172_O" deadCode="false" sourceNode="P_34F10172" targetNode="P_13F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_104F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10172_I" deadCode="false" sourceNode="P_34F10172" targetNode="P_54F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_106F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10172_O" deadCode="false" sourceNode="P_34F10172" targetNode="P_55F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_106F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10172_I" deadCode="false" sourceNode="P_34F10172" targetNode="P_56F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_107F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10172_O" deadCode="false" sourceNode="P_34F10172" targetNode="P_57F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_107F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10172_I" deadCode="false" sourceNode="P_34F10172" targetNode="P_58F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_108F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10172_O" deadCode="false" sourceNode="P_34F10172" targetNode="P_59F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_108F10172"/>
	</edges>
	<edges id="P_34F10172P_35F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10172" targetNode="P_35F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10172_I" deadCode="false" sourceNode="P_36F10172" targetNode="P_52F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_110F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10172_O" deadCode="false" sourceNode="P_36F10172" targetNode="P_53F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_110F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10172_I" deadCode="false" sourceNode="P_36F10172" targetNode="P_12F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_112F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10172_O" deadCode="false" sourceNode="P_36F10172" targetNode="P_13F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_112F10172"/>
	</edges>
	<edges id="P_36F10172P_37F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10172" targetNode="P_37F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10172_I" deadCode="false" sourceNode="P_38F10172" targetNode="P_12F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_115F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10172_O" deadCode="false" sourceNode="P_38F10172" targetNode="P_13F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_115F10172"/>
	</edges>
	<edges id="P_38F10172P_39F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10172" targetNode="P_39F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10172_I" deadCode="false" sourceNode="P_40F10172" targetNode="P_36F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_117F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10172_O" deadCode="false" sourceNode="P_40F10172" targetNode="P_37F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_117F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10172_I" deadCode="false" sourceNode="P_40F10172" targetNode="P_42F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_119F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10172_O" deadCode="false" sourceNode="P_40F10172" targetNode="P_43F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_119F10172"/>
	</edges>
	<edges id="P_40F10172P_41F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10172" targetNode="P_41F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10172_I" deadCode="false" sourceNode="P_42F10172" targetNode="P_12F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_122F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10172_O" deadCode="false" sourceNode="P_42F10172" targetNode="P_13F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_122F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10172_I" deadCode="false" sourceNode="P_42F10172" targetNode="P_54F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_124F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10172_O" deadCode="false" sourceNode="P_42F10172" targetNode="P_55F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_124F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10172_I" deadCode="false" sourceNode="P_42F10172" targetNode="P_56F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_125F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10172_O" deadCode="false" sourceNode="P_42F10172" targetNode="P_57F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_125F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10172_I" deadCode="false" sourceNode="P_42F10172" targetNode="P_58F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_126F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10172_O" deadCode="false" sourceNode="P_42F10172" targetNode="P_59F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_126F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10172_I" deadCode="false" sourceNode="P_42F10172" targetNode="P_38F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_128F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10172_O" deadCode="false" sourceNode="P_42F10172" targetNode="P_39F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_128F10172"/>
	</edges>
	<edges id="P_42F10172P_43F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10172" targetNode="P_43F10172"/>
	<edges id="P_54F10172P_55F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10172" targetNode="P_55F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_141F10172_I" deadCode="true" sourceNode="P_60F10172" targetNode="P_61F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_141F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_141F10172_O" deadCode="true" sourceNode="P_60F10172" targetNode="P_62F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_141F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10172_I" deadCode="false" sourceNode="P_56F10172" targetNode="P_64F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_145F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10172_O" deadCode="false" sourceNode="P_56F10172" targetNode="P_65F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_145F10172"/>
	</edges>
	<edges id="P_56F10172P_57F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10172" targetNode="P_57F10172"/>
	<edges id="P_45F10172P_46F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10172" targetNode="P_46F10172"/>
	<edges id="P_47F10172P_48F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10172" targetNode="P_48F10172"/>
	<edges id="P_58F10172P_59F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10172" targetNode="P_59F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10172_I" deadCode="false" sourceNode="P_10F10172" targetNode="P_66F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_154F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10172_O" deadCode="false" sourceNode="P_10F10172" targetNode="P_67F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_154F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_156F10172_I" deadCode="false" sourceNode="P_10F10172" targetNode="P_68F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_156F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_156F10172_O" deadCode="false" sourceNode="P_10F10172" targetNode="P_69F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_156F10172"/>
	</edges>
	<edges id="P_10F10172P_11F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10172" targetNode="P_11F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10172_I" deadCode="true" sourceNode="P_66F10172" targetNode="P_61F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_161F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10172_O" deadCode="true" sourceNode="P_66F10172" targetNode="P_62F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_161F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10172_I" deadCode="true" sourceNode="P_66F10172" targetNode="P_61F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_166F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10172_O" deadCode="true" sourceNode="P_66F10172" targetNode="P_62F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_166F10172"/>
	</edges>
	<edges id="P_66F10172P_67F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10172" targetNode="P_67F10172"/>
	<edges id="P_68F10172P_69F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10172" targetNode="P_69F10172"/>
	<edges id="P_61F10172P_62F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10172" targetNode="P_62F10172"/>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10172_I" deadCode="false" sourceNode="P_64F10172" targetNode="P_72F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_195F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10172_O" deadCode="false" sourceNode="P_64F10172" targetNode="P_73F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_195F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_196F10172_I" deadCode="false" sourceNode="P_64F10172" targetNode="P_74F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_196F10172"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_196F10172_O" deadCode="false" sourceNode="P_64F10172" targetNode="P_75F10172">
		<representations href="../../../cobol/LDBS2410.cbl.cobModel#S_196F10172"/>
	</edges>
	<edges id="P_64F10172P_65F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10172" targetNode="P_65F10172"/>
	<edges id="P_72F10172P_73F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10172" targetNode="P_73F10172"/>
	<edges id="P_74F10172P_75F10172" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10172" targetNode="P_75F10172"/>
	<edges xsi:type="cbl:DataEdge" id="S_103F10172_POS1" deadCode="false" targetNode="P_34F10172" sourceNode="DB2_OGG_BLOCCO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_103F10172"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10172_POS1" deadCode="false" targetNode="P_36F10172" sourceNode="DB2_OGG_BLOCCO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10172"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_114F10172_POS1" deadCode="false" targetNode="P_38F10172" sourceNode="DB2_OGG_BLOCCO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10172"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_121F10172_POS1" deadCode="false" targetNode="P_42F10172" sourceNode="DB2_OGG_BLOCCO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_121F10172"></representations>
	</edges>
</Package>

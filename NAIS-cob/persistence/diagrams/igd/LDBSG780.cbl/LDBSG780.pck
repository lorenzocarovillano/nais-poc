<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBSG780" cbl:id="LDBSG780" xsi:id="LDBSG780" packageRef="LDBSG780.igd#LDBSG780" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBSG780_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10275" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBSG780.cbl.cobModel#SC_1F10275"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10275" deadCode="false" name="PROGRAM_LDBSG780_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_1F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10275" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_2F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10275" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_3F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10275" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_12F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10275" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_13F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10275" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_4F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10275" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_5F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10275" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_6F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10275" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_7F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10275" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_8F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10275" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_9F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10275" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_44F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10275" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_49F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10275" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_14F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10275" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_15F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10275" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_16F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10275" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_17F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10275" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_18F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10275" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_19F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10275" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_20F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10275" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_21F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10275" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_22F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10275" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_23F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10275" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_50F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10275" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_51F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10275" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_24F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10275" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_25F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10275" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_26F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10275" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_27F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10275" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_28F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10275" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_29F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10275" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_30F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10275" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_31F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10275" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_32F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10275" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_33F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10275" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_52F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10275" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_53F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10275" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_34F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10275" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_35F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10275" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_36F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10275" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_37F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10275" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_38F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10275" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_39F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10275" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_40F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10275" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_41F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10275" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_42F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10275" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_43F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10275" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_54F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10275" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_55F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10275" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_60F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10275" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_61F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10275" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_56F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10275" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_57F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10275" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_45F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10275" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_46F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10275" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_47F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10275" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_48F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10275" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_58F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10275" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_59F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10275" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_10F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10275" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_11F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10275" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_62F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10275" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_63F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10275" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_64F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10275" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_65F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10275" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_66F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10275" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_67F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10275" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_68F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10275" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_69F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10275" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_70F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10275" deadCode="true" name="Z800-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_75F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10275" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_71F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10275" deadCode="true" name="Z810-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_72F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10275" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_73F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10275" deadCode="true" name="Z820-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_74F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10275" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_76F10275"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10275" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBSG780.cbl.cobModel#P_77F10275"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_AMMB_FUNZ_FUNZ" name="AMMB_FUNZ_FUNZ">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_AMMB_FUNZ_FUNZ"/>
		</children>
	</packageNode>
	<edges id="SC_1F10275P_1F10275" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10275" targetNode="P_1F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10275_I" deadCode="false" sourceNode="P_1F10275" targetNode="P_2F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_2F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10275_O" deadCode="false" sourceNode="P_1F10275" targetNode="P_3F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_2F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10275_I" deadCode="false" sourceNode="P_1F10275" targetNode="P_4F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_6F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10275_O" deadCode="false" sourceNode="P_1F10275" targetNode="P_5F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_6F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10275_I" deadCode="false" sourceNode="P_1F10275" targetNode="P_6F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_10F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10275_O" deadCode="false" sourceNode="P_1F10275" targetNode="P_7F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_10F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10275_I" deadCode="false" sourceNode="P_1F10275" targetNode="P_8F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_14F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10275_O" deadCode="false" sourceNode="P_1F10275" targetNode="P_9F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_14F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10275_I" deadCode="false" sourceNode="P_2F10275" targetNode="P_10F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_24F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10275_O" deadCode="false" sourceNode="P_2F10275" targetNode="P_11F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_24F10275"/>
	</edges>
	<edges id="P_2F10275P_3F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10275" targetNode="P_3F10275"/>
	<edges id="P_12F10275P_13F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10275" targetNode="P_13F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10275_I" deadCode="false" sourceNode="P_4F10275" targetNode="P_14F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_37F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10275_O" deadCode="false" sourceNode="P_4F10275" targetNode="P_15F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_37F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10275_I" deadCode="false" sourceNode="P_4F10275" targetNode="P_16F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_38F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10275_O" deadCode="false" sourceNode="P_4F10275" targetNode="P_17F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_38F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10275_I" deadCode="false" sourceNode="P_4F10275" targetNode="P_18F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_39F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10275_O" deadCode="false" sourceNode="P_4F10275" targetNode="P_19F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_39F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10275_I" deadCode="false" sourceNode="P_4F10275" targetNode="P_20F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_40F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10275_O" deadCode="false" sourceNode="P_4F10275" targetNode="P_21F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_40F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10275_I" deadCode="false" sourceNode="P_4F10275" targetNode="P_22F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_41F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10275_O" deadCode="false" sourceNode="P_4F10275" targetNode="P_23F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_41F10275"/>
	</edges>
	<edges id="P_4F10275P_5F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10275" targetNode="P_5F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10275_I" deadCode="false" sourceNode="P_6F10275" targetNode="P_24F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_45F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10275_O" deadCode="false" sourceNode="P_6F10275" targetNode="P_25F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_45F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10275_I" deadCode="false" sourceNode="P_6F10275" targetNode="P_26F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_46F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10275_O" deadCode="false" sourceNode="P_6F10275" targetNode="P_27F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_46F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10275_I" deadCode="false" sourceNode="P_6F10275" targetNode="P_28F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_47F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10275_O" deadCode="false" sourceNode="P_6F10275" targetNode="P_29F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_47F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10275_I" deadCode="false" sourceNode="P_6F10275" targetNode="P_30F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_48F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10275_O" deadCode="false" sourceNode="P_6F10275" targetNode="P_31F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_48F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10275_I" deadCode="false" sourceNode="P_6F10275" targetNode="P_32F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_49F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10275_O" deadCode="false" sourceNode="P_6F10275" targetNode="P_33F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_49F10275"/>
	</edges>
	<edges id="P_6F10275P_7F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10275" targetNode="P_7F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10275_I" deadCode="false" sourceNode="P_8F10275" targetNode="P_34F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_53F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10275_O" deadCode="false" sourceNode="P_8F10275" targetNode="P_35F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_53F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10275_I" deadCode="false" sourceNode="P_8F10275" targetNode="P_36F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_54F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10275_O" deadCode="false" sourceNode="P_8F10275" targetNode="P_37F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_54F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10275_I" deadCode="false" sourceNode="P_8F10275" targetNode="P_38F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_55F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10275_O" deadCode="false" sourceNode="P_8F10275" targetNode="P_39F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_55F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10275_I" deadCode="false" sourceNode="P_8F10275" targetNode="P_40F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_56F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10275_O" deadCode="false" sourceNode="P_8F10275" targetNode="P_41F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_56F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10275_I" deadCode="false" sourceNode="P_8F10275" targetNode="P_42F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_57F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10275_O" deadCode="false" sourceNode="P_8F10275" targetNode="P_43F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_57F10275"/>
	</edges>
	<edges id="P_8F10275P_9F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10275" targetNode="P_9F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10275_I" deadCode="false" sourceNode="P_44F10275" targetNode="P_45F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_60F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10275_O" deadCode="false" sourceNode="P_44F10275" targetNode="P_46F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_60F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10275_I" deadCode="false" sourceNode="P_44F10275" targetNode="P_47F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_61F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10275_O" deadCode="false" sourceNode="P_44F10275" targetNode="P_48F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_61F10275"/>
	</edges>
	<edges id="P_44F10275P_49F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10275" targetNode="P_49F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10275_I" deadCode="false" sourceNode="P_14F10275" targetNode="P_45F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_64F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10275_O" deadCode="false" sourceNode="P_14F10275" targetNode="P_46F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_64F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10275_I" deadCode="false" sourceNode="P_14F10275" targetNode="P_47F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_65F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10275_O" deadCode="false" sourceNode="P_14F10275" targetNode="P_48F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_65F10275"/>
	</edges>
	<edges id="P_14F10275P_15F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10275" targetNode="P_15F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10275_I" deadCode="false" sourceNode="P_16F10275" targetNode="P_44F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_68F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10275_O" deadCode="false" sourceNode="P_16F10275" targetNode="P_49F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_68F10275"/>
	</edges>
	<edges id="P_16F10275P_17F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10275" targetNode="P_17F10275"/>
	<edges id="P_18F10275P_19F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10275" targetNode="P_19F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10275_I" deadCode="false" sourceNode="P_20F10275" targetNode="P_16F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_73F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10275_O" deadCode="false" sourceNode="P_20F10275" targetNode="P_17F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_73F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10275_I" deadCode="false" sourceNode="P_20F10275" targetNode="P_22F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_75F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10275_O" deadCode="false" sourceNode="P_20F10275" targetNode="P_23F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_75F10275"/>
	</edges>
	<edges id="P_20F10275P_21F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10275" targetNode="P_21F10275"/>
	<edges id="P_22F10275P_23F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10275" targetNode="P_23F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10275_I" deadCode="false" sourceNode="P_50F10275" targetNode="P_45F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_79F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10275_O" deadCode="false" sourceNode="P_50F10275" targetNode="P_46F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_79F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10275_I" deadCode="false" sourceNode="P_50F10275" targetNode="P_47F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_80F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10275_O" deadCode="false" sourceNode="P_50F10275" targetNode="P_48F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_80F10275"/>
	</edges>
	<edges id="P_50F10275P_51F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10275" targetNode="P_51F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10275_I" deadCode="false" sourceNode="P_24F10275" targetNode="P_45F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_83F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10275_O" deadCode="false" sourceNode="P_24F10275" targetNode="P_46F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_83F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10275_I" deadCode="false" sourceNode="P_24F10275" targetNode="P_47F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_84F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10275_O" deadCode="false" sourceNode="P_24F10275" targetNode="P_48F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_84F10275"/>
	</edges>
	<edges id="P_24F10275P_25F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10275" targetNode="P_25F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10275_I" deadCode="false" sourceNode="P_26F10275" targetNode="P_50F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_87F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10275_O" deadCode="false" sourceNode="P_26F10275" targetNode="P_51F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_87F10275"/>
	</edges>
	<edges id="P_26F10275P_27F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10275" targetNode="P_27F10275"/>
	<edges id="P_28F10275P_29F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10275" targetNode="P_29F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10275_I" deadCode="false" sourceNode="P_30F10275" targetNode="P_26F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_92F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10275_O" deadCode="false" sourceNode="P_30F10275" targetNode="P_27F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_92F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10275_I" deadCode="false" sourceNode="P_30F10275" targetNode="P_32F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_94F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10275_O" deadCode="false" sourceNode="P_30F10275" targetNode="P_33F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_94F10275"/>
	</edges>
	<edges id="P_30F10275P_31F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10275" targetNode="P_31F10275"/>
	<edges id="P_32F10275P_33F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10275" targetNode="P_33F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10275_I" deadCode="false" sourceNode="P_52F10275" targetNode="P_45F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_98F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10275_O" deadCode="false" sourceNode="P_52F10275" targetNode="P_46F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_98F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10275_I" deadCode="false" sourceNode="P_52F10275" targetNode="P_47F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_99F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10275_O" deadCode="false" sourceNode="P_52F10275" targetNode="P_48F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_99F10275"/>
	</edges>
	<edges id="P_52F10275P_53F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10275" targetNode="P_53F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10275_I" deadCode="false" sourceNode="P_34F10275" targetNode="P_45F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_103F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10275_O" deadCode="false" sourceNode="P_34F10275" targetNode="P_46F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_103F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10275_I" deadCode="false" sourceNode="P_34F10275" targetNode="P_47F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_104F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10275_O" deadCode="false" sourceNode="P_34F10275" targetNode="P_48F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_104F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10275_I" deadCode="false" sourceNode="P_34F10275" targetNode="P_12F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_106F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10275_O" deadCode="false" sourceNode="P_34F10275" targetNode="P_13F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_106F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10275_I" deadCode="false" sourceNode="P_34F10275" targetNode="P_54F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_108F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10275_O" deadCode="false" sourceNode="P_34F10275" targetNode="P_55F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_108F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10275_I" deadCode="false" sourceNode="P_34F10275" targetNode="P_56F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_109F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10275_O" deadCode="false" sourceNode="P_34F10275" targetNode="P_57F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_109F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10275_I" deadCode="false" sourceNode="P_34F10275" targetNode="P_58F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_110F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10275_O" deadCode="false" sourceNode="P_34F10275" targetNode="P_59F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_110F10275"/>
	</edges>
	<edges id="P_34F10275P_35F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10275" targetNode="P_35F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10275_I" deadCode="false" sourceNode="P_36F10275" targetNode="P_52F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_112F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10275_O" deadCode="false" sourceNode="P_36F10275" targetNode="P_53F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_112F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10275_I" deadCode="false" sourceNode="P_36F10275" targetNode="P_12F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_114F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10275_O" deadCode="false" sourceNode="P_36F10275" targetNode="P_13F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_114F10275"/>
	</edges>
	<edges id="P_36F10275P_37F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10275" targetNode="P_37F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10275_I" deadCode="false" sourceNode="P_38F10275" targetNode="P_12F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_117F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10275_O" deadCode="false" sourceNode="P_38F10275" targetNode="P_13F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_117F10275"/>
	</edges>
	<edges id="P_38F10275P_39F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10275" targetNode="P_39F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10275_I" deadCode="false" sourceNode="P_40F10275" targetNode="P_36F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_119F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10275_O" deadCode="false" sourceNode="P_40F10275" targetNode="P_37F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_119F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10275_I" deadCode="false" sourceNode="P_40F10275" targetNode="P_42F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_121F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10275_O" deadCode="false" sourceNode="P_40F10275" targetNode="P_43F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_121F10275"/>
	</edges>
	<edges id="P_40F10275P_41F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10275" targetNode="P_41F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10275_I" deadCode="false" sourceNode="P_42F10275" targetNode="P_12F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_124F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10275_O" deadCode="false" sourceNode="P_42F10275" targetNode="P_13F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_124F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10275_I" deadCode="false" sourceNode="P_42F10275" targetNode="P_54F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_126F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10275_O" deadCode="false" sourceNode="P_42F10275" targetNode="P_55F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_126F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10275_I" deadCode="false" sourceNode="P_42F10275" targetNode="P_56F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_127F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10275_O" deadCode="false" sourceNode="P_42F10275" targetNode="P_57F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_127F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10275_I" deadCode="false" sourceNode="P_42F10275" targetNode="P_58F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_128F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10275_O" deadCode="false" sourceNode="P_42F10275" targetNode="P_59F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_128F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10275_I" deadCode="false" sourceNode="P_42F10275" targetNode="P_38F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_130F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10275_O" deadCode="false" sourceNode="P_42F10275" targetNode="P_39F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_130F10275"/>
	</edges>
	<edges id="P_42F10275P_43F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10275" targetNode="P_43F10275"/>
	<edges id="P_54F10275P_55F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10275" targetNode="P_55F10275"/>
	<edges id="P_56F10275P_57F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10275" targetNode="P_57F10275"/>
	<edges id="P_45F10275P_46F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10275" targetNode="P_46F10275"/>
	<edges id="P_47F10275P_48F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10275" targetNode="P_48F10275"/>
	<edges id="P_58F10275P_59F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10275" targetNode="P_59F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10275_I" deadCode="false" sourceNode="P_10F10275" targetNode="P_62F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_154F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10275_O" deadCode="false" sourceNode="P_10F10275" targetNode="P_63F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_154F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_156F10275_I" deadCode="false" sourceNode="P_10F10275" targetNode="P_64F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_156F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_156F10275_O" deadCode="false" sourceNode="P_10F10275" targetNode="P_65F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_156F10275"/>
	</edges>
	<edges id="P_10F10275P_11F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10275" targetNode="P_11F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10275_I" deadCode="true" sourceNode="P_62F10275" targetNode="P_66F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_161F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10275_O" deadCode="true" sourceNode="P_62F10275" targetNode="P_67F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_161F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10275_I" deadCode="true" sourceNode="P_62F10275" targetNode="P_66F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_166F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10275_O" deadCode="true" sourceNode="P_62F10275" targetNode="P_67F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_166F10275"/>
	</edges>
	<edges id="P_62F10275P_63F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10275" targetNode="P_63F10275"/>
	<edges id="P_64F10275P_65F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10275" targetNode="P_65F10275"/>
	<edges id="P_66F10275P_67F10275" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10275" targetNode="P_67F10275"/>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10275_I" deadCode="true" sourceNode="P_70F10275" targetNode="P_71F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_195F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10275_O" deadCode="true" sourceNode="P_70F10275" targetNode="P_72F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_195F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_196F10275_I" deadCode="true" sourceNode="P_70F10275" targetNode="P_73F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_196F10275"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_196F10275_O" deadCode="true" sourceNode="P_70F10275" targetNode="P_74F10275">
		<representations href="../../../cobol/LDBSG780.cbl.cobModel#S_196F10275"/>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_105F10275_POS1" deadCode="false" targetNode="P_34F10275" sourceNode="DB2_AMMB_FUNZ_FUNZ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_105F10275"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_113F10275_POS1" deadCode="false" targetNode="P_36F10275" sourceNode="DB2_AMMB_FUNZ_FUNZ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_113F10275"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_116F10275_POS1" deadCode="false" targetNode="P_38F10275" sourceNode="DB2_AMMB_FUNZ_FUNZ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_116F10275"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_123F10275_POS1" deadCode="false" targetNode="P_42F10275" sourceNode="DB2_AMMB_FUNZ_FUNZ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_123F10275"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS0880" cbl:id="LDBS0880" xsi:id="LDBS0880" packageRef="LDBS0880.igd#LDBS0880" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS0880_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10150" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS0880.cbl.cobModel#SC_1F10150"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10150" deadCode="false" name="PROGRAM_LDBS0880_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_1F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10150" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_2F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10150" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_3F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10150" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_12F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10150" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_13F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10150" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_4F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10150" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_5F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10150" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_6F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10150" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_7F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10150" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_8F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10150" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_9F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10150" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_44F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10150" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_49F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10150" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_14F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10150" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_15F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10150" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_16F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10150" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_17F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10150" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_18F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10150" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_19F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10150" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_20F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10150" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_21F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10150" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_22F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10150" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_23F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10150" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_56F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10150" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_57F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10150" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_24F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10150" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_25F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10150" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_26F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10150" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_27F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10150" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_28F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10150" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_29F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10150" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_30F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10150" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_31F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10150" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_32F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10150" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_33F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10150" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_58F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10150" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_59F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10150" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_34F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10150" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_35F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10150" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_36F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10150" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_37F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10150" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_38F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10150" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_39F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10150" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_40F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10150" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_41F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10150" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_42F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10150" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_43F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10150" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_50F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10150" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_51F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10150" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_60F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10150" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_63F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10150" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_52F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10150" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_53F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10150" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_45F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10150" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_46F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10150" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_47F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10150" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_48F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10150" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_54F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10150" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_55F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10150" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_10F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10150" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_11F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10150" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_66F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10150" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_67F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10150" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_68F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10150" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_69F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10150" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_61F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10150" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_62F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10150" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_70F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10150" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_71F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10150" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_64F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10150" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_65F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10150" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_72F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10150" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_73F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10150" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_74F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10150" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_75F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10150" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_76F10150"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10150" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS0880.cbl.cobModel#P_77F10150"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_TIT_CONT" name="TIT_CONT">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_TIT_CONT"/>
		</children>
	</packageNode>
	<edges id="SC_1F10150P_1F10150" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10150" targetNode="P_1F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10150_I" deadCode="false" sourceNode="P_1F10150" targetNode="P_2F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_1F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10150_O" deadCode="false" sourceNode="P_1F10150" targetNode="P_3F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_1F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10150_I" deadCode="false" sourceNode="P_1F10150" targetNode="P_4F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_5F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10150_O" deadCode="false" sourceNode="P_1F10150" targetNode="P_5F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_5F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10150_I" deadCode="false" sourceNode="P_1F10150" targetNode="P_6F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_9F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10150_O" deadCode="false" sourceNode="P_1F10150" targetNode="P_7F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_9F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10150_I" deadCode="false" sourceNode="P_1F10150" targetNode="P_8F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_13F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10150_O" deadCode="false" sourceNode="P_1F10150" targetNode="P_9F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_13F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10150_I" deadCode="false" sourceNode="P_2F10150" targetNode="P_10F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_22F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10150_O" deadCode="false" sourceNode="P_2F10150" targetNode="P_11F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_22F10150"/>
	</edges>
	<edges id="P_2F10150P_3F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10150" targetNode="P_3F10150"/>
	<edges id="P_12F10150P_13F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10150" targetNode="P_13F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10150_I" deadCode="false" sourceNode="P_4F10150" targetNode="P_14F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_35F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10150_O" deadCode="false" sourceNode="P_4F10150" targetNode="P_15F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_35F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10150_I" deadCode="false" sourceNode="P_4F10150" targetNode="P_16F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_36F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10150_O" deadCode="false" sourceNode="P_4F10150" targetNode="P_17F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_36F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10150_I" deadCode="false" sourceNode="P_4F10150" targetNode="P_18F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_37F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10150_O" deadCode="false" sourceNode="P_4F10150" targetNode="P_19F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_37F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10150_I" deadCode="false" sourceNode="P_4F10150" targetNode="P_20F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_38F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10150_O" deadCode="false" sourceNode="P_4F10150" targetNode="P_21F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_38F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10150_I" deadCode="false" sourceNode="P_4F10150" targetNode="P_22F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_39F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10150_O" deadCode="false" sourceNode="P_4F10150" targetNode="P_23F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_39F10150"/>
	</edges>
	<edges id="P_4F10150P_5F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10150" targetNode="P_5F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10150_I" deadCode="false" sourceNode="P_6F10150" targetNode="P_24F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_43F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10150_O" deadCode="false" sourceNode="P_6F10150" targetNode="P_25F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_43F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10150_I" deadCode="false" sourceNode="P_6F10150" targetNode="P_26F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_44F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10150_O" deadCode="false" sourceNode="P_6F10150" targetNode="P_27F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_44F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10150_I" deadCode="false" sourceNode="P_6F10150" targetNode="P_28F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_45F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10150_O" deadCode="false" sourceNode="P_6F10150" targetNode="P_29F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_45F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10150_I" deadCode="false" sourceNode="P_6F10150" targetNode="P_30F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_46F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10150_O" deadCode="false" sourceNode="P_6F10150" targetNode="P_31F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_46F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10150_I" deadCode="false" sourceNode="P_6F10150" targetNode="P_32F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_47F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10150_O" deadCode="false" sourceNode="P_6F10150" targetNode="P_33F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_47F10150"/>
	</edges>
	<edges id="P_6F10150P_7F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10150" targetNode="P_7F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10150_I" deadCode="false" sourceNode="P_8F10150" targetNode="P_34F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_51F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10150_O" deadCode="false" sourceNode="P_8F10150" targetNode="P_35F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_51F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10150_I" deadCode="false" sourceNode="P_8F10150" targetNode="P_36F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_52F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10150_O" deadCode="false" sourceNode="P_8F10150" targetNode="P_37F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_52F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10150_I" deadCode="false" sourceNode="P_8F10150" targetNode="P_38F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_53F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10150_O" deadCode="false" sourceNode="P_8F10150" targetNode="P_39F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_53F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10150_I" deadCode="false" sourceNode="P_8F10150" targetNode="P_40F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_54F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10150_O" deadCode="false" sourceNode="P_8F10150" targetNode="P_41F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_54F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10150_I" deadCode="false" sourceNode="P_8F10150" targetNode="P_42F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_55F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10150_O" deadCode="false" sourceNode="P_8F10150" targetNode="P_43F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_55F10150"/>
	</edges>
	<edges id="P_8F10150P_9F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10150" targetNode="P_9F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10150_I" deadCode="false" sourceNode="P_44F10150" targetNode="P_45F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_58F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10150_O" deadCode="false" sourceNode="P_44F10150" targetNode="P_46F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_58F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10150_I" deadCode="false" sourceNode="P_44F10150" targetNode="P_47F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_59F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10150_O" deadCode="false" sourceNode="P_44F10150" targetNode="P_48F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_59F10150"/>
	</edges>
	<edges id="P_44F10150P_49F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10150" targetNode="P_49F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10150_I" deadCode="false" sourceNode="P_14F10150" targetNode="P_45F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_63F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10150_O" deadCode="false" sourceNode="P_14F10150" targetNode="P_46F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_63F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10150_I" deadCode="false" sourceNode="P_14F10150" targetNode="P_47F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_64F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10150_O" deadCode="false" sourceNode="P_14F10150" targetNode="P_48F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_64F10150"/>
	</edges>
	<edges id="P_14F10150P_15F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10150" targetNode="P_15F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10150_I" deadCode="false" sourceNode="P_16F10150" targetNode="P_44F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_67F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10150_O" deadCode="false" sourceNode="P_16F10150" targetNode="P_49F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_67F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10150_I" deadCode="false" sourceNode="P_16F10150" targetNode="P_12F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_69F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10150_O" deadCode="false" sourceNode="P_16F10150" targetNode="P_13F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_69F10150"/>
	</edges>
	<edges id="P_16F10150P_17F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10150" targetNode="P_17F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10150_I" deadCode="false" sourceNode="P_18F10150" targetNode="P_12F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_72F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10150_O" deadCode="false" sourceNode="P_18F10150" targetNode="P_13F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_72F10150"/>
	</edges>
	<edges id="P_18F10150P_19F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10150" targetNode="P_19F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10150_I" deadCode="false" sourceNode="P_20F10150" targetNode="P_16F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_74F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10150_O" deadCode="false" sourceNode="P_20F10150" targetNode="P_17F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_74F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10150_I" deadCode="false" sourceNode="P_20F10150" targetNode="P_22F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_76F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10150_O" deadCode="false" sourceNode="P_20F10150" targetNode="P_23F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_76F10150"/>
	</edges>
	<edges id="P_20F10150P_21F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10150" targetNode="P_21F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10150_I" deadCode="false" sourceNode="P_22F10150" targetNode="P_12F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_79F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10150_O" deadCode="false" sourceNode="P_22F10150" targetNode="P_13F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_79F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10150_I" deadCode="false" sourceNode="P_22F10150" targetNode="P_50F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_81F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10150_O" deadCode="false" sourceNode="P_22F10150" targetNode="P_51F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_81F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10150_I" deadCode="false" sourceNode="P_22F10150" targetNode="P_52F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_82F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10150_O" deadCode="false" sourceNode="P_22F10150" targetNode="P_53F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_82F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10150_I" deadCode="false" sourceNode="P_22F10150" targetNode="P_54F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_83F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10150_O" deadCode="false" sourceNode="P_22F10150" targetNode="P_55F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_83F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10150_I" deadCode="false" sourceNode="P_22F10150" targetNode="P_18F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_85F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10150_O" deadCode="false" sourceNode="P_22F10150" targetNode="P_19F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_85F10150"/>
	</edges>
	<edges id="P_22F10150P_23F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10150" targetNode="P_23F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10150_I" deadCode="false" sourceNode="P_56F10150" targetNode="P_45F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_89F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10150_O" deadCode="false" sourceNode="P_56F10150" targetNode="P_46F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_89F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10150_I" deadCode="false" sourceNode="P_56F10150" targetNode="P_47F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_90F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10150_O" deadCode="false" sourceNode="P_56F10150" targetNode="P_48F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_90F10150"/>
	</edges>
	<edges id="P_56F10150P_57F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10150" targetNode="P_57F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10150_I" deadCode="false" sourceNode="P_24F10150" targetNode="P_45F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_94F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10150_O" deadCode="false" sourceNode="P_24F10150" targetNode="P_46F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_94F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10150_I" deadCode="false" sourceNode="P_24F10150" targetNode="P_47F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_95F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10150_O" deadCode="false" sourceNode="P_24F10150" targetNode="P_48F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_95F10150"/>
	</edges>
	<edges id="P_24F10150P_25F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10150" targetNode="P_25F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10150_I" deadCode="false" sourceNode="P_26F10150" targetNode="P_56F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_98F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10150_O" deadCode="false" sourceNode="P_26F10150" targetNode="P_57F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_98F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10150_I" deadCode="false" sourceNode="P_26F10150" targetNode="P_12F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_100F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10150_O" deadCode="false" sourceNode="P_26F10150" targetNode="P_13F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_100F10150"/>
	</edges>
	<edges id="P_26F10150P_27F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10150" targetNode="P_27F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10150_I" deadCode="false" sourceNode="P_28F10150" targetNode="P_12F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_103F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10150_O" deadCode="false" sourceNode="P_28F10150" targetNode="P_13F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_103F10150"/>
	</edges>
	<edges id="P_28F10150P_29F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10150" targetNode="P_29F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10150_I" deadCode="false" sourceNode="P_30F10150" targetNode="P_26F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_105F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10150_O" deadCode="false" sourceNode="P_30F10150" targetNode="P_27F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_105F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10150_I" deadCode="false" sourceNode="P_30F10150" targetNode="P_32F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_107F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10150_O" deadCode="false" sourceNode="P_30F10150" targetNode="P_33F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_107F10150"/>
	</edges>
	<edges id="P_30F10150P_31F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10150" targetNode="P_31F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10150_I" deadCode="false" sourceNode="P_32F10150" targetNode="P_12F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_110F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10150_O" deadCode="false" sourceNode="P_32F10150" targetNode="P_13F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_110F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10150_I" deadCode="false" sourceNode="P_32F10150" targetNode="P_50F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_112F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10150_O" deadCode="false" sourceNode="P_32F10150" targetNode="P_51F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_112F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10150_I" deadCode="false" sourceNode="P_32F10150" targetNode="P_52F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_113F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10150_O" deadCode="false" sourceNode="P_32F10150" targetNode="P_53F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_113F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10150_I" deadCode="false" sourceNode="P_32F10150" targetNode="P_54F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_114F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10150_O" deadCode="false" sourceNode="P_32F10150" targetNode="P_55F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_114F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10150_I" deadCode="false" sourceNode="P_32F10150" targetNode="P_28F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_116F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10150_O" deadCode="false" sourceNode="P_32F10150" targetNode="P_29F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_116F10150"/>
	</edges>
	<edges id="P_32F10150P_33F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10150" targetNode="P_33F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10150_I" deadCode="false" sourceNode="P_58F10150" targetNode="P_45F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_120F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10150_O" deadCode="false" sourceNode="P_58F10150" targetNode="P_46F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_120F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10150_I" deadCode="false" sourceNode="P_58F10150" targetNode="P_47F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_121F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10150_O" deadCode="false" sourceNode="P_58F10150" targetNode="P_48F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_121F10150"/>
	</edges>
	<edges id="P_58F10150P_59F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10150" targetNode="P_59F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10150_I" deadCode="false" sourceNode="P_34F10150" targetNode="P_45F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_124F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10150_O" deadCode="false" sourceNode="P_34F10150" targetNode="P_46F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_124F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10150_I" deadCode="false" sourceNode="P_34F10150" targetNode="P_47F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_125F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10150_O" deadCode="false" sourceNode="P_34F10150" targetNode="P_48F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_125F10150"/>
	</edges>
	<edges id="P_34F10150P_35F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10150" targetNode="P_35F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10150_I" deadCode="false" sourceNode="P_36F10150" targetNode="P_58F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_128F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10150_O" deadCode="false" sourceNode="P_36F10150" targetNode="P_59F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_128F10150"/>
	</edges>
	<edges id="P_36F10150P_37F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10150" targetNode="P_37F10150"/>
	<edges id="P_38F10150P_39F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10150" targetNode="P_39F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10150_I" deadCode="false" sourceNode="P_40F10150" targetNode="P_36F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_133F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10150_O" deadCode="false" sourceNode="P_40F10150" targetNode="P_37F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_133F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10150_I" deadCode="false" sourceNode="P_40F10150" targetNode="P_42F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_135F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10150_O" deadCode="false" sourceNode="P_40F10150" targetNode="P_43F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_135F10150"/>
	</edges>
	<edges id="P_40F10150P_41F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10150" targetNode="P_41F10150"/>
	<edges id="P_42F10150P_43F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10150" targetNode="P_43F10150"/>
	<edges id="P_50F10150P_51F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10150" targetNode="P_51F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_272F10150_I" deadCode="true" sourceNode="P_60F10150" targetNode="P_61F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_272F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_272F10150_O" deadCode="true" sourceNode="P_60F10150" targetNode="P_62F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_272F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_275F10150_I" deadCode="true" sourceNode="P_60F10150" targetNode="P_61F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_275F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_275F10150_O" deadCode="true" sourceNode="P_60F10150" targetNode="P_62F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_275F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_279F10150_I" deadCode="true" sourceNode="P_60F10150" targetNode="P_61F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_279F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_279F10150_O" deadCode="true" sourceNode="P_60F10150" targetNode="P_62F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_279F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10150_I" deadCode="true" sourceNode="P_60F10150" targetNode="P_61F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_283F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10150_O" deadCode="true" sourceNode="P_60F10150" targetNode="P_62F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_283F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_287F10150_I" deadCode="true" sourceNode="P_60F10150" targetNode="P_61F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_287F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_287F10150_O" deadCode="true" sourceNode="P_60F10150" targetNode="P_62F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_287F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_291F10150_I" deadCode="true" sourceNode="P_60F10150" targetNode="P_61F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_291F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_291F10150_O" deadCode="true" sourceNode="P_60F10150" targetNode="P_62F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_291F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_295F10150_I" deadCode="true" sourceNode="P_60F10150" targetNode="P_61F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_295F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_295F10150_O" deadCode="true" sourceNode="P_60F10150" targetNode="P_62F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_295F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10150_I" deadCode="true" sourceNode="P_60F10150" targetNode="P_61F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_299F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10150_O" deadCode="true" sourceNode="P_60F10150" targetNode="P_62F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_299F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_303F10150_I" deadCode="true" sourceNode="P_60F10150" targetNode="P_61F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_303F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_303F10150_O" deadCode="true" sourceNode="P_60F10150" targetNode="P_62F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_303F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_307F10150_I" deadCode="true" sourceNode="P_60F10150" targetNode="P_61F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_307F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_307F10150_O" deadCode="true" sourceNode="P_60F10150" targetNode="P_62F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_307F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10150_I" deadCode="true" sourceNode="P_60F10150" targetNode="P_61F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_311F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10150_O" deadCode="true" sourceNode="P_60F10150" targetNode="P_62F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_311F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_315F10150_I" deadCode="false" sourceNode="P_52F10150" targetNode="P_64F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_315F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_315F10150_O" deadCode="false" sourceNode="P_52F10150" targetNode="P_65F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_315F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_318F10150_I" deadCode="false" sourceNode="P_52F10150" targetNode="P_64F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_318F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_318F10150_O" deadCode="false" sourceNode="P_52F10150" targetNode="P_65F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_318F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_322F10150_I" deadCode="false" sourceNode="P_52F10150" targetNode="P_64F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_322F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_322F10150_O" deadCode="false" sourceNode="P_52F10150" targetNode="P_65F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_322F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_326F10150_I" deadCode="false" sourceNode="P_52F10150" targetNode="P_64F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_326F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_326F10150_O" deadCode="false" sourceNode="P_52F10150" targetNode="P_65F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_326F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_330F10150_I" deadCode="false" sourceNode="P_52F10150" targetNode="P_64F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_330F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_330F10150_O" deadCode="false" sourceNode="P_52F10150" targetNode="P_65F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_330F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_334F10150_I" deadCode="false" sourceNode="P_52F10150" targetNode="P_64F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_334F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_334F10150_O" deadCode="false" sourceNode="P_52F10150" targetNode="P_65F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_334F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10150_I" deadCode="false" sourceNode="P_52F10150" targetNode="P_64F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_338F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10150_O" deadCode="false" sourceNode="P_52F10150" targetNode="P_65F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_338F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_342F10150_I" deadCode="false" sourceNode="P_52F10150" targetNode="P_64F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_342F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_342F10150_O" deadCode="false" sourceNode="P_52F10150" targetNode="P_65F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_342F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_346F10150_I" deadCode="false" sourceNode="P_52F10150" targetNode="P_64F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_346F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_346F10150_O" deadCode="false" sourceNode="P_52F10150" targetNode="P_65F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_346F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_350F10150_I" deadCode="false" sourceNode="P_52F10150" targetNode="P_64F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_350F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_350F10150_O" deadCode="false" sourceNode="P_52F10150" targetNode="P_65F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_350F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_354F10150_I" deadCode="false" sourceNode="P_52F10150" targetNode="P_64F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_354F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_354F10150_O" deadCode="false" sourceNode="P_52F10150" targetNode="P_65F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_354F10150"/>
	</edges>
	<edges id="P_52F10150P_53F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10150" targetNode="P_53F10150"/>
	<edges id="P_45F10150P_46F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10150" targetNode="P_46F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_364F10150_I" deadCode="false" sourceNode="P_47F10150" targetNode="P_61F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_364F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_364F10150_O" deadCode="false" sourceNode="P_47F10150" targetNode="P_62F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_364F10150"/>
	</edges>
	<edges id="P_47F10150P_48F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10150" targetNode="P_48F10150"/>
	<edges id="P_54F10150P_55F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10150" targetNode="P_55F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_369F10150_I" deadCode="false" sourceNode="P_10F10150" targetNode="P_66F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_369F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_369F10150_O" deadCode="false" sourceNode="P_10F10150" targetNode="P_67F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_369F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_371F10150_I" deadCode="false" sourceNode="P_10F10150" targetNode="P_68F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_371F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_371F10150_O" deadCode="false" sourceNode="P_10F10150" targetNode="P_69F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_371F10150"/>
	</edges>
	<edges id="P_10F10150P_11F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10150" targetNode="P_11F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_376F10150_I" deadCode="false" sourceNode="P_66F10150" targetNode="P_61F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_376F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_376F10150_O" deadCode="false" sourceNode="P_66F10150" targetNode="P_62F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_376F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_381F10150_I" deadCode="false" sourceNode="P_66F10150" targetNode="P_61F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_381F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_381F10150_O" deadCode="false" sourceNode="P_66F10150" targetNode="P_62F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_381F10150"/>
	</edges>
	<edges id="P_66F10150P_67F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10150" targetNode="P_67F10150"/>
	<edges id="P_68F10150P_69F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10150" targetNode="P_69F10150"/>
	<edges id="P_61F10150P_62F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10150" targetNode="P_62F10150"/>
	<edges xsi:type="cbl:PerformEdge" id="S_410F10150_I" deadCode="false" sourceNode="P_64F10150" targetNode="P_72F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_410F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_410F10150_O" deadCode="false" sourceNode="P_64F10150" targetNode="P_73F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_410F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_411F10150_I" deadCode="false" sourceNode="P_64F10150" targetNode="P_74F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_411F10150"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_411F10150_O" deadCode="false" sourceNode="P_64F10150" targetNode="P_75F10150">
		<representations href="../../../cobol/LDBS0880.cbl.cobModel#S_411F10150"/>
	</edges>
	<edges id="P_64F10150P_65F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10150" targetNode="P_65F10150"/>
	<edges id="P_72F10150P_73F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10150" targetNode="P_73F10150"/>
	<edges id="P_74F10150P_75F10150" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10150" targetNode="P_75F10150"/>
	<edges xsi:type="cbl:DataEdge" id="S_68F10150_POS1" deadCode="false" targetNode="P_16F10150" sourceNode="DB2_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_68F10150"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_71F10150_POS1" deadCode="false" targetNode="P_18F10150" sourceNode="DB2_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_71F10150"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_78F10150_POS1" deadCode="false" targetNode="P_22F10150" sourceNode="DB2_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10150"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_99F10150_POS1" deadCode="false" targetNode="P_26F10150" sourceNode="DB2_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_99F10150"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_102F10150_POS1" deadCode="false" targetNode="P_28F10150" sourceNode="DB2_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_102F10150"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_109F10150_POS1" deadCode="false" targetNode="P_32F10150" sourceNode="DB2_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_109F10150"></representations>
	</edges>
</Package>

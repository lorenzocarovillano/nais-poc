<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS3610" cbl:id="LDBS3610" xsi:id="LDBS3610" packageRef="LDBS3610.igd#LDBS3610" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS3610_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10203" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS3610.cbl.cobModel#SC_1F10203"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10203" deadCode="false" name="PROGRAM_LDBS3610_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_1F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10203" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_2F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10203" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_3F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10203" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_12F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10203" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_13F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10203" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_4F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10203" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_5F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10203" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_6F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10203" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_7F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10203" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_8F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10203" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_9F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10203" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_44F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10203" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_49F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10203" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_14F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10203" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_15F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10203" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_16F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10203" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_17F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10203" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_18F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10203" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_19F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10203" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_20F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10203" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_21F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10203" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_22F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10203" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_23F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10203" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_50F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10203" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_51F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10203" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_24F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10203" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_25F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10203" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_26F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10203" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_27F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10203" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_28F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10203" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_29F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10203" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_30F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10203" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_31F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10203" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_32F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10203" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_33F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10203" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_52F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10203" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_53F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10203" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_34F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10203" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_35F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10203" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_36F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10203" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_37F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10203" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_38F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10203" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_39F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10203" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_40F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10203" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_41F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10203" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_42F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10203" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_43F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10203" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_54F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10203" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_55F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10203" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_60F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10203" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_63F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10203" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_56F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10203" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_57F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10203" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_45F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10203" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_46F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10203" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_47F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10203" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_48F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10203" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_58F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10203" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_59F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10203" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_10F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10203" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_11F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10203" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_66F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10203" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_67F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10203" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_68F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10203" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_69F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10203" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_61F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10203" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_62F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10203" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_70F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10203" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_71F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10203" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_64F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10203" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_65F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10203" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_72F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10203" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_73F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10203" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_74F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10203" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_75F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10203" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_76F10203"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10203" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS3610.cbl.cobModel#P_77F10203"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_MOVI" name="MOVI">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_MOVI"/>
		</children>
	</packageNode>
	<edges id="SC_1F10203P_1F10203" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10203" targetNode="P_1F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10203_I" deadCode="false" sourceNode="P_1F10203" targetNode="P_2F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_2F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10203_O" deadCode="false" sourceNode="P_1F10203" targetNode="P_3F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_2F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10203_I" deadCode="false" sourceNode="P_1F10203" targetNode="P_4F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_6F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10203_O" deadCode="false" sourceNode="P_1F10203" targetNode="P_5F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_6F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10203_I" deadCode="false" sourceNode="P_1F10203" targetNode="P_6F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_10F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10203_O" deadCode="false" sourceNode="P_1F10203" targetNode="P_7F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_10F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10203_I" deadCode="false" sourceNode="P_1F10203" targetNode="P_8F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_14F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10203_O" deadCode="false" sourceNode="P_1F10203" targetNode="P_9F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_14F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10203_I" deadCode="false" sourceNode="P_2F10203" targetNode="P_10F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_24F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10203_O" deadCode="false" sourceNode="P_2F10203" targetNode="P_11F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_24F10203"/>
	</edges>
	<edges id="P_2F10203P_3F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10203" targetNode="P_3F10203"/>
	<edges id="P_12F10203P_13F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10203" targetNode="P_13F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10203_I" deadCode="false" sourceNode="P_4F10203" targetNode="P_14F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_37F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10203_O" deadCode="false" sourceNode="P_4F10203" targetNode="P_15F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_37F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10203_I" deadCode="false" sourceNode="P_4F10203" targetNode="P_16F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_38F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10203_O" deadCode="false" sourceNode="P_4F10203" targetNode="P_17F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_38F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10203_I" deadCode="false" sourceNode="P_4F10203" targetNode="P_18F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_39F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10203_O" deadCode="false" sourceNode="P_4F10203" targetNode="P_19F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_39F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10203_I" deadCode="false" sourceNode="P_4F10203" targetNode="P_20F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_40F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10203_O" deadCode="false" sourceNode="P_4F10203" targetNode="P_21F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_40F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10203_I" deadCode="false" sourceNode="P_4F10203" targetNode="P_22F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_41F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10203_O" deadCode="false" sourceNode="P_4F10203" targetNode="P_23F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_41F10203"/>
	</edges>
	<edges id="P_4F10203P_5F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10203" targetNode="P_5F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10203_I" deadCode="false" sourceNode="P_6F10203" targetNode="P_24F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_45F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10203_O" deadCode="false" sourceNode="P_6F10203" targetNode="P_25F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_45F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10203_I" deadCode="false" sourceNode="P_6F10203" targetNode="P_26F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_46F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10203_O" deadCode="false" sourceNode="P_6F10203" targetNode="P_27F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_46F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10203_I" deadCode="false" sourceNode="P_6F10203" targetNode="P_28F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_47F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10203_O" deadCode="false" sourceNode="P_6F10203" targetNode="P_29F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_47F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10203_I" deadCode="false" sourceNode="P_6F10203" targetNode="P_30F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_48F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10203_O" deadCode="false" sourceNode="P_6F10203" targetNode="P_31F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_48F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10203_I" deadCode="false" sourceNode="P_6F10203" targetNode="P_32F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_49F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10203_O" deadCode="false" sourceNode="P_6F10203" targetNode="P_33F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_49F10203"/>
	</edges>
	<edges id="P_6F10203P_7F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10203" targetNode="P_7F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10203_I" deadCode="false" sourceNode="P_8F10203" targetNode="P_34F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_53F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10203_O" deadCode="false" sourceNode="P_8F10203" targetNode="P_35F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_53F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10203_I" deadCode="false" sourceNode="P_8F10203" targetNode="P_36F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_54F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10203_O" deadCode="false" sourceNode="P_8F10203" targetNode="P_37F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_54F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10203_I" deadCode="false" sourceNode="P_8F10203" targetNode="P_38F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_55F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10203_O" deadCode="false" sourceNode="P_8F10203" targetNode="P_39F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_55F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10203_I" deadCode="false" sourceNode="P_8F10203" targetNode="P_40F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_56F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10203_O" deadCode="false" sourceNode="P_8F10203" targetNode="P_41F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_56F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10203_I" deadCode="false" sourceNode="P_8F10203" targetNode="P_42F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_57F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10203_O" deadCode="false" sourceNode="P_8F10203" targetNode="P_43F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_57F10203"/>
	</edges>
	<edges id="P_8F10203P_9F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10203" targetNode="P_9F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10203_I" deadCode="false" sourceNode="P_44F10203" targetNode="P_45F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_60F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10203_O" deadCode="false" sourceNode="P_44F10203" targetNode="P_46F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_60F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10203_I" deadCode="false" sourceNode="P_44F10203" targetNode="P_47F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_61F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10203_O" deadCode="false" sourceNode="P_44F10203" targetNode="P_48F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_61F10203"/>
	</edges>
	<edges id="P_44F10203P_49F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10203" targetNode="P_49F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10203_I" deadCode="false" sourceNode="P_14F10203" targetNode="P_45F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_64F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10203_O" deadCode="false" sourceNode="P_14F10203" targetNode="P_46F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_64F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10203_I" deadCode="false" sourceNode="P_14F10203" targetNode="P_47F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_65F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10203_O" deadCode="false" sourceNode="P_14F10203" targetNode="P_48F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_65F10203"/>
	</edges>
	<edges id="P_14F10203P_15F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10203" targetNode="P_15F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10203_I" deadCode="false" sourceNode="P_16F10203" targetNode="P_44F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_68F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10203_O" deadCode="false" sourceNode="P_16F10203" targetNode="P_49F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_68F10203"/>
	</edges>
	<edges id="P_16F10203P_17F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10203" targetNode="P_17F10203"/>
	<edges id="P_18F10203P_19F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10203" targetNode="P_19F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10203_I" deadCode="false" sourceNode="P_20F10203" targetNode="P_16F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_73F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10203_O" deadCode="false" sourceNode="P_20F10203" targetNode="P_17F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_73F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10203_I" deadCode="false" sourceNode="P_20F10203" targetNode="P_22F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_75F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10203_O" deadCode="false" sourceNode="P_20F10203" targetNode="P_23F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_75F10203"/>
	</edges>
	<edges id="P_20F10203P_21F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10203" targetNode="P_21F10203"/>
	<edges id="P_22F10203P_23F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10203" targetNode="P_23F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10203_I" deadCode="false" sourceNode="P_50F10203" targetNode="P_45F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_79F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10203_O" deadCode="false" sourceNode="P_50F10203" targetNode="P_46F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_79F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10203_I" deadCode="false" sourceNode="P_50F10203" targetNode="P_47F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_80F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10203_O" deadCode="false" sourceNode="P_50F10203" targetNode="P_48F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_80F10203"/>
	</edges>
	<edges id="P_50F10203P_51F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10203" targetNode="P_51F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10203_I" deadCode="false" sourceNode="P_24F10203" targetNode="P_45F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_83F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10203_O" deadCode="false" sourceNode="P_24F10203" targetNode="P_46F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_83F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10203_I" deadCode="false" sourceNode="P_24F10203" targetNode="P_47F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_84F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10203_O" deadCode="false" sourceNode="P_24F10203" targetNode="P_48F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_84F10203"/>
	</edges>
	<edges id="P_24F10203P_25F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10203" targetNode="P_25F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10203_I" deadCode="false" sourceNode="P_26F10203" targetNode="P_50F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_87F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10203_O" deadCode="false" sourceNode="P_26F10203" targetNode="P_51F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_87F10203"/>
	</edges>
	<edges id="P_26F10203P_27F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10203" targetNode="P_27F10203"/>
	<edges id="P_28F10203P_29F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10203" targetNode="P_29F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10203_I" deadCode="false" sourceNode="P_30F10203" targetNode="P_26F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_92F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10203_O" deadCode="false" sourceNode="P_30F10203" targetNode="P_27F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_92F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10203_I" deadCode="false" sourceNode="P_30F10203" targetNode="P_32F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_94F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10203_O" deadCode="false" sourceNode="P_30F10203" targetNode="P_33F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_94F10203"/>
	</edges>
	<edges id="P_30F10203P_31F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10203" targetNode="P_31F10203"/>
	<edges id="P_32F10203P_33F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10203" targetNode="P_33F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10203_I" deadCode="false" sourceNode="P_52F10203" targetNode="P_45F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_98F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10203_O" deadCode="false" sourceNode="P_52F10203" targetNode="P_46F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_98F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10203_I" deadCode="false" sourceNode="P_52F10203" targetNode="P_47F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_99F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10203_O" deadCode="false" sourceNode="P_52F10203" targetNode="P_48F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_99F10203"/>
	</edges>
	<edges id="P_52F10203P_53F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10203" targetNode="P_53F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10203_I" deadCode="false" sourceNode="P_34F10203" targetNode="P_45F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_103F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10203_O" deadCode="false" sourceNode="P_34F10203" targetNode="P_46F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_103F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10203_I" deadCode="false" sourceNode="P_34F10203" targetNode="P_47F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_104F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10203_O" deadCode="false" sourceNode="P_34F10203" targetNode="P_48F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_104F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10203_I" deadCode="false" sourceNode="P_34F10203" targetNode="P_12F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_106F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10203_O" deadCode="false" sourceNode="P_34F10203" targetNode="P_13F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_106F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10203_I" deadCode="false" sourceNode="P_34F10203" targetNode="P_54F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_108F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10203_O" deadCode="false" sourceNode="P_34F10203" targetNode="P_55F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_108F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10203_I" deadCode="false" sourceNode="P_34F10203" targetNode="P_56F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_109F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10203_O" deadCode="false" sourceNode="P_34F10203" targetNode="P_57F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_109F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10203_I" deadCode="false" sourceNode="P_34F10203" targetNode="P_58F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_110F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10203_O" deadCode="false" sourceNode="P_34F10203" targetNode="P_59F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_110F10203"/>
	</edges>
	<edges id="P_34F10203P_35F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10203" targetNode="P_35F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10203_I" deadCode="false" sourceNode="P_36F10203" targetNode="P_52F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_112F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10203_O" deadCode="false" sourceNode="P_36F10203" targetNode="P_53F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_112F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10203_I" deadCode="false" sourceNode="P_36F10203" targetNode="P_12F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_114F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10203_O" deadCode="false" sourceNode="P_36F10203" targetNode="P_13F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_114F10203"/>
	</edges>
	<edges id="P_36F10203P_37F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10203" targetNode="P_37F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10203_I" deadCode="false" sourceNode="P_38F10203" targetNode="P_12F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_117F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10203_O" deadCode="false" sourceNode="P_38F10203" targetNode="P_13F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_117F10203"/>
	</edges>
	<edges id="P_38F10203P_39F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10203" targetNode="P_39F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10203_I" deadCode="false" sourceNode="P_40F10203" targetNode="P_36F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_119F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10203_O" deadCode="false" sourceNode="P_40F10203" targetNode="P_37F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_119F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10203_I" deadCode="false" sourceNode="P_40F10203" targetNode="P_42F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_121F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10203_O" deadCode="false" sourceNode="P_40F10203" targetNode="P_43F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_121F10203"/>
	</edges>
	<edges id="P_40F10203P_41F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10203" targetNode="P_41F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10203_I" deadCode="false" sourceNode="P_42F10203" targetNode="P_12F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_124F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10203_O" deadCode="false" sourceNode="P_42F10203" targetNode="P_13F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_124F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10203_I" deadCode="false" sourceNode="P_42F10203" targetNode="P_54F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_126F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10203_O" deadCode="false" sourceNode="P_42F10203" targetNode="P_55F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_126F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10203_I" deadCode="false" sourceNode="P_42F10203" targetNode="P_56F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_127F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10203_O" deadCode="false" sourceNode="P_42F10203" targetNode="P_57F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_127F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10203_I" deadCode="false" sourceNode="P_42F10203" targetNode="P_58F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_128F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10203_O" deadCode="false" sourceNode="P_42F10203" targetNode="P_59F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_128F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10203_I" deadCode="false" sourceNode="P_42F10203" targetNode="P_38F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_130F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10203_O" deadCode="false" sourceNode="P_42F10203" targetNode="P_39F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_130F10203"/>
	</edges>
	<edges id="P_42F10203P_43F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10203" targetNode="P_43F10203"/>
	<edges id="P_54F10203P_55F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10203" targetNode="P_55F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10203_I" deadCode="true" sourceNode="P_60F10203" targetNode="P_61F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_153F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10203_O" deadCode="true" sourceNode="P_60F10203" targetNode="P_62F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_153F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10203_I" deadCode="false" sourceNode="P_56F10203" targetNode="P_64F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_157F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10203_O" deadCode="false" sourceNode="P_56F10203" targetNode="P_65F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_157F10203"/>
	</edges>
	<edges id="P_56F10203P_57F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10203" targetNode="P_57F10203"/>
	<edges id="P_45F10203P_46F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10203" targetNode="P_46F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10203_I" deadCode="false" sourceNode="P_47F10203" targetNode="P_61F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_163F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10203_O" deadCode="false" sourceNode="P_47F10203" targetNode="P_62F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_163F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10203_I" deadCode="false" sourceNode="P_47F10203" targetNode="P_61F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_166F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10203_O" deadCode="false" sourceNode="P_47F10203" targetNode="P_62F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_166F10203"/>
	</edges>
	<edges id="P_47F10203P_48F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10203" targetNode="P_48F10203"/>
	<edges id="P_58F10203P_59F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10203" targetNode="P_59F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10203_I" deadCode="false" sourceNode="P_10F10203" targetNode="P_66F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_171F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10203_O" deadCode="false" sourceNode="P_10F10203" targetNode="P_67F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_171F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10203_I" deadCode="false" sourceNode="P_10F10203" targetNode="P_68F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_173F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10203_O" deadCode="false" sourceNode="P_10F10203" targetNode="P_69F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_173F10203"/>
	</edges>
	<edges id="P_10F10203P_11F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10203" targetNode="P_11F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_178F10203_I" deadCode="false" sourceNode="P_66F10203" targetNode="P_61F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_178F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_178F10203_O" deadCode="false" sourceNode="P_66F10203" targetNode="P_62F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_178F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_183F10203_I" deadCode="false" sourceNode="P_66F10203" targetNode="P_61F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_183F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_183F10203_O" deadCode="false" sourceNode="P_66F10203" targetNode="P_62F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_183F10203"/>
	</edges>
	<edges id="P_66F10203P_67F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10203" targetNode="P_67F10203"/>
	<edges id="P_68F10203P_69F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10203" targetNode="P_69F10203"/>
	<edges id="P_61F10203P_62F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10203" targetNode="P_62F10203"/>
	<edges xsi:type="cbl:PerformEdge" id="S_212F10203_I" deadCode="false" sourceNode="P_64F10203" targetNode="P_72F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_212F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_212F10203_O" deadCode="false" sourceNode="P_64F10203" targetNode="P_73F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_212F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10203_I" deadCode="false" sourceNode="P_64F10203" targetNode="P_74F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_213F10203"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10203_O" deadCode="false" sourceNode="P_64F10203" targetNode="P_75F10203">
		<representations href="../../../cobol/LDBS3610.cbl.cobModel#S_213F10203"/>
	</edges>
	<edges id="P_64F10203P_65F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10203" targetNode="P_65F10203"/>
	<edges id="P_72F10203P_73F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10203" targetNode="P_73F10203"/>
	<edges id="P_74F10203P_75F10203" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10203" targetNode="P_75F10203"/>
	<edges xsi:type="cbl:DataEdge" id="S_105F10203_POS1" deadCode="false" targetNode="P_34F10203" sourceNode="DB2_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_105F10203"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_113F10203_POS1" deadCode="false" targetNode="P_36F10203" sourceNode="DB2_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_113F10203"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_116F10203_POS1" deadCode="false" targetNode="P_38F10203" sourceNode="DB2_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_116F10203"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_123F10203_POS1" deadCode="false" targetNode="P_42F10203" sourceNode="DB2_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_123F10203"></representations>
	</edges>
</Package>

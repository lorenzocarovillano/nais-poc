<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IABS0140" cbl:id="IABS0140" xsi:id="IABS0140" packageRef="IABS0140.igd#IABS0140" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IABS0140_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10012" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IABS0140.cbl.cobModel#SC_1F10012"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10012" deadCode="false" name="PROGRAM_IABS0140_FIRST_SENTENCES">
				<representations href="../../../cobol/IABS0140.cbl.cobModel#P_1F10012"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10012" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IABS0140.cbl.cobModel#P_2F10012"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10012" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IABS0140.cbl.cobModel#P_3F10012"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10012" deadCode="false" name="A100-VALORIZZA-RITORNO">
				<representations href="../../../cobol/IABS0140.cbl.cobModel#P_10F10012"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10012" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IABS0140.cbl.cobModel#P_11F10012"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10012" deadCode="false" name="B000-VALORIZZA-BUFFER">
				<representations href="../../../cobol/IABS0140.cbl.cobModel#P_4F10012"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10012" deadCode="false" name="B000-EX">
				<representations href="../../../cobol/IABS0140.cbl.cobModel#P_5F10012"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10012" deadCode="false" name="A300-ELABORA">
				<representations href="../../../cobol/IABS0140.cbl.cobModel#P_8F10012"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10012" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IABS0140.cbl.cobModel#P_9F10012"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10012" deadCode="false" name="A310-CONNECT">
				<representations href="../../../cobol/IABS0140.cbl.cobModel#P_12F10012"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10012" deadCode="false" name="A310-EX">
				<representations href="../../../cobol/IABS0140.cbl.cobModel#P_13F10012"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10012" deadCode="false" name="A320-DISCONNECT">
				<representations href="../../../cobol/IABS0140.cbl.cobModel#P_14F10012"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10012" deadCode="false" name="A320-EX">
				<representations href="../../../cobol/IABS0140.cbl.cobModel#P_15F10012"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10012" deadCode="false" name="A330-CONNECT-RESET">
				<representations href="../../../cobol/IABS0140.cbl.cobModel#P_16F10012"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10012" deadCode="false" name="A330-EX">
				<representations href="../../../cobol/IABS0140.cbl.cobModel#P_17F10012"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10012" deadCode="false" name="C000-CTRL-INPUT">
				<representations href="../../../cobol/IABS0140.cbl.cobModel#P_6F10012"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10012" deadCode="false" name="C000-EX">
				<representations href="../../../cobol/IABS0140.cbl.cobModel#P_7F10012"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10012P_1F10012" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10012" targetNode="P_1F10012"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10012_I" deadCode="false" sourceNode="P_1F10012" targetNode="P_2F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_1F10012"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10012_O" deadCode="false" sourceNode="P_1F10012" targetNode="P_3F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_1F10012"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10012_I" deadCode="false" sourceNode="P_1F10012" targetNode="P_4F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_2F10012"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10012_O" deadCode="false" sourceNode="P_1F10012" targetNode="P_5F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_2F10012"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10012_I" deadCode="false" sourceNode="P_1F10012" targetNode="P_6F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_3F10012"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10012_O" deadCode="false" sourceNode="P_1F10012" targetNode="P_7F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_3F10012"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10012_I" deadCode="false" sourceNode="P_1F10012" targetNode="P_8F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_5F10012"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10012_O" deadCode="false" sourceNode="P_1F10012" targetNode="P_9F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_5F10012"/>
	</edges>
	<edges id="P_2F10012P_3F10012" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10012" targetNode="P_3F10012"/>
	<edges id="P_10F10012P_11F10012" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10012" targetNode="P_11F10012"/>
	<edges id="P_4F10012P_5F10012" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10012" targetNode="P_5F10012"/>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10012_I" deadCode="false" sourceNode="P_8F10012" targetNode="P_12F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_19F10012"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10012_O" deadCode="false" sourceNode="P_8F10012" targetNode="P_13F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_19F10012"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10012_I" deadCode="false" sourceNode="P_8F10012" targetNode="P_14F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_20F10012"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10012_O" deadCode="false" sourceNode="P_8F10012" targetNode="P_15F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_20F10012"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10012_I" deadCode="false" sourceNode="P_8F10012" targetNode="P_16F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_21F10012"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10012_O" deadCode="false" sourceNode="P_8F10012" targetNode="P_17F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_21F10012"/>
	</edges>
	<edges id="P_8F10012P_9F10012" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10012" targetNode="P_9F10012"/>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10012_I" deadCode="false" sourceNode="P_12F10012" targetNode="P_10F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_30F10012"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10012_O" deadCode="false" sourceNode="P_12F10012" targetNode="P_11F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_30F10012"/>
	</edges>
	<edges id="P_12F10012P_13F10012" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10012" targetNode="P_13F10012"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10012_I" deadCode="false" sourceNode="P_14F10012" targetNode="P_10F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_35F10012"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10012_O" deadCode="false" sourceNode="P_14F10012" targetNode="P_11F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_35F10012"/>
	</edges>
	<edges id="P_14F10012P_15F10012" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10012" targetNode="P_15F10012"/>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10012_I" deadCode="false" sourceNode="P_16F10012" targetNode="P_10F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_39F10012"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10012_O" deadCode="false" sourceNode="P_16F10012" targetNode="P_11F10012">
		<representations href="../../../cobol/IABS0140.cbl.cobModel#S_39F10012"/>
	</edges>
	<edges id="P_16F10012P_17F10012" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10012" targetNode="P_17F10012"/>
	<edges id="P_6F10012P_7F10012" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10012" targetNode="P_7F10012"/>
</Package>

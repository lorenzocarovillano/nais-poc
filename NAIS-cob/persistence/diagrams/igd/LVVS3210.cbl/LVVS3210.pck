<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS3210" cbl:id="LVVS3210" xsi:id="LVVS3210" packageRef="LVVS3210.igd#LVVS3210" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS3210_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10387" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS3210.cbl.cobModel#SC_1F10387"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10387" deadCode="false" name="PROGRAM_LVVS3210_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS3210.cbl.cobModel#P_1F10387"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10387" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS3210.cbl.cobModel#P_2F10387"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10387" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS3210.cbl.cobModel#P_3F10387"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10387" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS3210.cbl.cobModel#P_4F10387"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10387" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS3210.cbl.cobModel#P_5F10387"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10387" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS3210.cbl.cobModel#P_8F10387"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10387" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS3210.cbl.cobModel#P_9F10387"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10387" deadCode="false" name="A000-PREPARA-CALL-LDBSF110">
				<representations href="../../../cobol/LVVS3210.cbl.cobModel#P_10F10387"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10387" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LVVS3210.cbl.cobModel#P_11F10387"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10387" deadCode="false" name="A010-CALL-LDBSF110">
				<representations href="../../../cobol/LVVS3210.cbl.cobModel#P_12F10387"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10387" deadCode="false" name="A010-EX">
				<representations href="../../../cobol/LVVS3210.cbl.cobModel#P_13F10387"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10387" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS3210.cbl.cobModel#P_6F10387"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10387" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS3210.cbl.cobModel#P_7F10387"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSF110" name="LDBSF110">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10265"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10387P_1F10387" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10387" targetNode="P_1F10387"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10387_I" deadCode="false" sourceNode="P_1F10387" targetNode="P_2F10387">
		<representations href="../../../cobol/LVVS3210.cbl.cobModel#S_1F10387"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10387_O" deadCode="false" sourceNode="P_1F10387" targetNode="P_3F10387">
		<representations href="../../../cobol/LVVS3210.cbl.cobModel#S_1F10387"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10387_I" deadCode="false" sourceNode="P_1F10387" targetNode="P_4F10387">
		<representations href="../../../cobol/LVVS3210.cbl.cobModel#S_2F10387"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10387_O" deadCode="false" sourceNode="P_1F10387" targetNode="P_5F10387">
		<representations href="../../../cobol/LVVS3210.cbl.cobModel#S_2F10387"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10387_I" deadCode="false" sourceNode="P_1F10387" targetNode="P_6F10387">
		<representations href="../../../cobol/LVVS3210.cbl.cobModel#S_3F10387"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10387_O" deadCode="false" sourceNode="P_1F10387" targetNode="P_7F10387">
		<representations href="../../../cobol/LVVS3210.cbl.cobModel#S_3F10387"/>
	</edges>
	<edges id="P_2F10387P_3F10387" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10387" targetNode="P_3F10387"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10387_I" deadCode="false" sourceNode="P_4F10387" targetNode="P_8F10387">
		<representations href="../../../cobol/LVVS3210.cbl.cobModel#S_10F10387"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10387_O" deadCode="false" sourceNode="P_4F10387" targetNode="P_9F10387">
		<representations href="../../../cobol/LVVS3210.cbl.cobModel#S_10F10387"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10387_I" deadCode="false" sourceNode="P_4F10387" targetNode="P_10F10387">
		<representations href="../../../cobol/LVVS3210.cbl.cobModel#S_12F10387"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10387_O" deadCode="false" sourceNode="P_4F10387" targetNode="P_11F10387">
		<representations href="../../../cobol/LVVS3210.cbl.cobModel#S_12F10387"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10387_I" deadCode="false" sourceNode="P_4F10387" targetNode="P_12F10387">
		<representations href="../../../cobol/LVVS3210.cbl.cobModel#S_14F10387"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10387_O" deadCode="false" sourceNode="P_4F10387" targetNode="P_13F10387">
		<representations href="../../../cobol/LVVS3210.cbl.cobModel#S_14F10387"/>
	</edges>
	<edges id="P_4F10387P_5F10387" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10387" targetNode="P_5F10387"/>
	<edges id="P_8F10387P_9F10387" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10387" targetNode="P_9F10387"/>
	<edges id="P_10F10387P_11F10387" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10387" targetNode="P_11F10387"/>
	<edges id="P_12F10387P_13F10387" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10387" targetNode="P_13F10387"/>
	<edges xsi:type="cbl:CallEdge" id="S_31F10387" deadCode="false" name="Dynamic WK-PGM-CALLED" sourceNode="P_12F10387" targetNode="LDBSF110">
		<representations href="../../../cobol/../importantStmts.cobModel#S_31F10387"></representations>
	</edges>
</Package>

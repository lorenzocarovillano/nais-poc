<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LCCS0017" cbl:id="LCCS0017" xsi:id="LCCS0017" packageRef="LCCS0017.igd#LCCS0017" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LCCS0017_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10123" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LCCS0017.cbl.cobModel#SC_1F10123"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10123" deadCode="false" name="INIZIO">
				<representations href="../../../cobol/LCCS0017.cbl.cobModel#P_1F10123"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10123" deadCode="false" name="FINE-TEST">
				<representations href="../../../cobol/LCCS0017.cbl.cobModel#P_2F10123"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10123" deadCode="false" name="TEST-ANNI">
				<representations href="../../../cobol/LCCS0017.cbl.cobModel#P_3F10123"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10123" deadCode="false" name="IMPOSTA-DATA-SUP">
				<representations href="../../../cobol/LCCS0017.cbl.cobModel#P_4F10123"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_4F10123" deadCode="false" name="CALCOLO-GIORNI">
			<representations href="../../../cobol/LCCS0017.cbl.cobModel#SC_4F10123"/>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10123" deadCode="false" name="CALCOLO-GIORNI_FIRST_SENTENCES">
				<representations href="../../../cobol/LCCS0017.cbl.cobModel#P_5F10123"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10123" deadCode="false" name="CALCOLO-GIORNI-999">
				<representations href="../../../cobol/LCCS0017.cbl.cobModel#P_6F10123"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_3F10123" deadCode="false" name="CALCOLO-MESI">
			<representations href="../../../cobol/LCCS0017.cbl.cobModel#SC_3F10123"/>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10123" deadCode="false" name="CALCOLO-MESI_FIRST_SENTENCES">
				<representations href="../../../cobol/LCCS0017.cbl.cobModel#P_7F10123"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10123" deadCode="false" name="CALCOLO-MESI-999">
				<representations href="../../../cobol/LCCS0017.cbl.cobModel#P_8F10123"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_2F10123" deadCode="false" name="FINE">
			<representations href="../../../cobol/LCCS0017.cbl.cobModel#SC_2F10123"/>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10123" deadCode="false" name="FINE_FIRST_SENTENCES">
				<representations href="../../../cobol/LCCS0017.cbl.cobModel#P_9F10123"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10123P_1F10123" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10123" targetNode="P_1F10123"/>
	<edges id="SC_4F10123P_5F10123" xsi:type="cbl:FallThroughEdge" sourceNode="SC_4F10123" targetNode="P_5F10123"/>
	<edges id="SC_3F10123P_7F10123" xsi:type="cbl:FallThroughEdge" sourceNode="SC_3F10123" targetNode="P_7F10123"/>
	<edges id="SC_2F10123P_9F10123" xsi:type="cbl:FallThroughEdge" sourceNode="SC_2F10123" targetNode="P_9F10123"/>
	<edges xsi:type="cbl:GotoEdge" id="S_4F10123_GTP_1" deadCode="false" sourceNode="P_1F10123" targetNode="SC_2F10123">
		<representations href="../../../cobol/LCCS0017.cbl.cobModel#S_4F10123"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_7F10123_GTP_1" deadCode="false" sourceNode="P_1F10123" targetNode="SC_2F10123">
		<representations href="../../../cobol/LCCS0017.cbl.cobModel#S_7F10123"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_19F10123_GTP_1" deadCode="false" sourceNode="P_1F10123" targetNode="SC_2F10123">
		<representations href="../../../cobol/LCCS0017.cbl.cobModel#S_19F10123"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_22F10123_GTP_1" deadCode="false" sourceNode="P_1F10123" targetNode="SC_2F10123">
		<representations href="../../../cobol/LCCS0017.cbl.cobModel#S_22F10123"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_24F10123_GTP_1" deadCode="false" sourceNode="P_1F10123" targetNode="SC_2F10123">
		<representations href="../../../cobol/LCCS0017.cbl.cobModel#S_24F10123"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_26F10123_GTP_1" deadCode="false" sourceNode="P_1F10123" targetNode="P_2F10123">
		<representations href="../../../cobol/LCCS0017.cbl.cobModel#S_26F10123"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_28F10123_GTP_1" deadCode="false" sourceNode="P_1F10123" targetNode="P_2F10123">
		<representations href="../../../cobol/LCCS0017.cbl.cobModel#S_28F10123"/>
	</edges>
	<edges id="P_1F10123P_2F10123" xsi:type="cbl:FallThroughEdge" sourceNode="P_1F10123" targetNode="P_2F10123"/>
	<edges xsi:type="cbl:GotoEdge" id="S_31F10123_GTP_1" deadCode="false" sourceNode="P_2F10123" targetNode="SC_2F10123">
		<representations href="../../../cobol/LCCS0017.cbl.cobModel#S_31F10123"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10123_I" deadCode="false" sourceNode="P_2F10123" targetNode="SC_3F10123">
		<representations href="../../../cobol/LCCS0017.cbl.cobModel#S_30F10123"/>
	</edges>
	<edges id="P_2F10123P_3F10123" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10123" targetNode="P_3F10123"/>
	<edges xsi:type="cbl:GotoEdge" id="S_33F10123_GTP_1" deadCode="false" sourceNode="P_3F10123" targetNode="P_4F10123">
		<representations href="../../../cobol/LCCS0017.cbl.cobModel#S_33F10123"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_40F10123_GTP_1" deadCode="false" sourceNode="P_3F10123" targetNode="P_3F10123">
		<representations href="../../../cobol/LCCS0017.cbl.cobModel#S_40F10123"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10123_I" deadCode="false" sourceNode="P_3F10123" targetNode="SC_4F10123">
		<representations href="../../../cobol/LCCS0017.cbl.cobModel#S_36F10123"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_43F10123_GTP_1" deadCode="false" sourceNode="P_4F10123" targetNode="SC_2F10123">
		<representations href="../../../cobol/LCCS0017.cbl.cobModel#S_43F10123"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10123_I" deadCode="false" sourceNode="P_4F10123" targetNode="SC_4F10123">
		<representations href="../../../cobol/LCCS0017.cbl.cobModel#S_42F10123"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_46F10123_GTP_1" deadCode="false" sourceNode="P_5F10123" targetNode="P_6F10123">
		<representations href="../../../cobol/LCCS0017.cbl.cobModel#S_46F10123"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_55F10123_GTP_1" deadCode="false" sourceNode="P_5F10123" targetNode="SC_4F10123">
		<representations href="../../../cobol/LCCS0017.cbl.cobModel#S_55F10123"/>
	</edges>
	<edges xsi:type="cbl:GotoEdge" id="S_60F10123_GTP_1" deadCode="false" sourceNode="P_7F10123" targetNode="SC_3F10123">
		<representations href="../../../cobol/LCCS0017.cbl.cobModel#S_60F10123"/>
	</edges>
	<edges id="P_7F10123P_8F10123" xsi:type="cbl:FallThroughEdge" sourceNode="P_7F10123" targetNode="P_8F10123"/>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS2760" cbl:id="LVVS2760" xsi:id="LVVS2760" packageRef="LVVS2760.igd#LVVS2760" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS2760_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10376" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS2760.cbl.cobModel#SC_1F10376"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10376" deadCode="false" name="PROGRAM_LVVS2760_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_1F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10376" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_2F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10376" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_3F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10376" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_4F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10376" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_5F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10376" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_8F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10376" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_9F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10376" deadCode="false" name="S1101-INIZIA-CONTRAENTE">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_12F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10376" deadCode="false" name="S1101-INIZIA-CONTRAENTE-EX">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_13F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10376" deadCode="false" name="S1100-LETTURA-CONTRAENTE">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_10F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10376" deadCode="false" name="S1100-LETTURA-CONTRAENTE-EX">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_11F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10376" deadCode="false" name="LETTURA-POLIZZA">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_14F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10376" deadCode="false" name="LETTURA-POLIZZA-EX">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_15F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10376" deadCode="false" name="LETTURA-STB">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_18F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10376" deadCode="false" name="LETTURA-STB-EX">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_19F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10376" deadCode="false" name="S1110-LETTURA-GARANZIA">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_20F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10376" deadCode="false" name="S1110-LETTURA-GARANZIA-EX">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_21F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10376" deadCode="false" name="S1135-CHIUSURA-CURS-GAR">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_22F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10376" deadCode="false" name="S1135-CHIUSURA-CURS-GAR-EX">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_23F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10376" deadCode="false" name="S1130-CHIUSURA-CURS">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_16F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10376" deadCode="false" name="S1130-CHIUSURA-CURS-EX">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_17F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10376" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_6F10376"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10376" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS2760.cbl.cobModel#P_7F10376"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS5910" name="LDBS5910">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10225"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSPOL0" name="IDBSPOL0">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10078"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSSTB0" name="IDBSSTB0">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10090"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS1420" name="LDBS1420">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10159"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10376P_1F10376" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10376" targetNode="P_1F10376"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10376_I" deadCode="false" sourceNode="P_1F10376" targetNode="P_2F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_1F10376"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10376_O" deadCode="false" sourceNode="P_1F10376" targetNode="P_3F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_1F10376"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10376_I" deadCode="false" sourceNode="P_1F10376" targetNode="P_4F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_2F10376"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10376_O" deadCode="false" sourceNode="P_1F10376" targetNode="P_5F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_2F10376"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10376_I" deadCode="false" sourceNode="P_1F10376" targetNode="P_6F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_3F10376"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10376_O" deadCode="false" sourceNode="P_1F10376" targetNode="P_7F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_3F10376"/>
	</edges>
	<edges id="P_2F10376P_3F10376" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10376" targetNode="P_3F10376"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10376_I" deadCode="false" sourceNode="P_4F10376" targetNode="P_8F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_10F10376"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10376_O" deadCode="false" sourceNode="P_4F10376" targetNode="P_9F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_10F10376"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10376_I" deadCode="false" sourceNode="P_4F10376" targetNode="P_10F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_11F10376"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10376_O" deadCode="false" sourceNode="P_4F10376" targetNode="P_11F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_11F10376"/>
	</edges>
	<edges id="P_4F10376P_5F10376" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10376" targetNode="P_5F10376"/>
	<edges id="P_8F10376P_9F10376" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10376" targetNode="P_9F10376"/>
	<edges id="P_12F10376P_13F10376" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10376" targetNode="P_13F10376"/>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10376_I" deadCode="false" sourceNode="P_10F10376" targetNode="P_12F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_28F10376"/>
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_29F10376"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10376_O" deadCode="false" sourceNode="P_10F10376" targetNode="P_13F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_28F10376"/>
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_29F10376"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10376_I" deadCode="false" sourceNode="P_10F10376" targetNode="P_14F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_28F10376"/>
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_37F10376"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10376_O" deadCode="false" sourceNode="P_10F10376" targetNode="P_15F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_28F10376"/>
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_37F10376"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10376_I" deadCode="false" sourceNode="P_10F10376" targetNode="P_12F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_28F10376"/>
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_39F10376"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10376_O" deadCode="false" sourceNode="P_10F10376" targetNode="P_13F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_28F10376"/>
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_39F10376"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10376_I" deadCode="false" sourceNode="P_10F10376" targetNode="P_16F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_28F10376"/>
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_45F10376"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10376_O" deadCode="false" sourceNode="P_10F10376" targetNode="P_17F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_28F10376"/>
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_45F10376"/>
	</edges>
	<edges id="P_10F10376P_11F10376" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10376" targetNode="P_11F10376"/>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10376_I" deadCode="false" sourceNode="P_14F10376" targetNode="P_18F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_70F10376"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10376_O" deadCode="false" sourceNode="P_14F10376" targetNode="P_19F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_70F10376"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10376_I" deadCode="false" sourceNode="P_14F10376" targetNode="P_20F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_72F10376"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10376_O" deadCode="false" sourceNode="P_14F10376" targetNode="P_21F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_72F10376"/>
	</edges>
	<edges id="P_14F10376P_15F10376" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10376" targetNode="P_15F10376"/>
	<edges id="P_18F10376P_19F10376" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10376" targetNode="P_19F10376"/>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10376_I" deadCode="false" sourceNode="P_20F10376" targetNode="P_22F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_104F10376"/>
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_120F10376"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10376_O" deadCode="false" sourceNode="P_20F10376" targetNode="P_23F10376">
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_104F10376"/>
		<representations href="../../../cobol/LVVS2760.cbl.cobModel#S_120F10376"/>
	</edges>
	<edges id="P_20F10376P_21F10376" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10376" targetNode="P_21F10376"/>
	<edges id="P_22F10376P_23F10376" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10376" targetNode="P_23F10376"/>
	<edges id="P_16F10376P_17F10376" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10376" targetNode="P_17F10376"/>
	<edges xsi:type="cbl:CallEdge" id="S_30F10376" deadCode="false" name="Dynamic LDBS5910" sourceNode="P_10F10376" targetNode="LDBS5910">
		<representations href="../../../cobol/../importantStmts.cobModel#S_30F10376"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_59F10376" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_14F10376" targetNode="IDBSPOL0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_59F10376"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_89F10376" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_18F10376" targetNode="IDBSSTB0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_89F10376"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_112F10376" deadCode="false" name="Dynamic LDBS1420" sourceNode="P_20F10376" targetNode="LDBS1420">
		<representations href="../../../cobol/../importantStmts.cobModel#S_112F10376"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_126F10376" deadCode="false" name="Dynamic LDBS1420" sourceNode="P_22F10376" targetNode="LDBS1420">
		<representations href="../../../cobol/../importantStmts.cobModel#S_126F10376"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_136F10376" deadCode="false" name="Dynamic LDBS5910" sourceNode="P_16F10376" targetNode="LDBS5910">
		<representations href="../../../cobol/../importantStmts.cobModel#S_136F10376"></representations>
	</edges>
</Package>

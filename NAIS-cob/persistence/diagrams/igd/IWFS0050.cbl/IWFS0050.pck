<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IWFS0050" cbl:id="IWFS0050" xsi:id="IWFS0050" packageRef="IWFS0050.igd#IWFS0050" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IWFS0050_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10118" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IWFS0050.cbl.cobModel#SC_1F10118"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10118" deadCode="false" name="PROGRAM_IWFS0050_FIRST_SENTENCES">
				<representations href="../../../cobol/IWFS0050.cbl.cobModel#P_1F10118"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10118" deadCode="false" name="A000-OPERAZ-INIZ">
				<representations href="../../../cobol/IWFS0050.cbl.cobModel#P_2F10118"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10118" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IWFS0050.cbl.cobModel#P_3F10118"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10118" deadCode="false" name="A050-INITIALIZE">
				<representations href="../../../cobol/IWFS0050.cbl.cobModel#P_6F10118"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10118" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/IWFS0050.cbl.cobModel#P_7F10118"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10118" deadCode="false" name="A060-CTRL-INPUT">
				<representations href="../../../cobol/IWFS0050.cbl.cobModel#P_8F10118"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10118" deadCode="false" name="A060-EX">
				<representations href="../../../cobol/IWFS0050.cbl.cobModel#P_9F10118"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10118" deadCode="false" name="B000-ELABORA">
				<representations href="../../../cobol/IWFS0050.cbl.cobModel#P_4F10118"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10118" deadCode="false" name="B000-EX">
				<representations href="../../../cobol/IWFS0050.cbl.cobModel#P_5F10118"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10118" deadCode="false" name="B050-RICERCA-VIRGOLA">
				<representations href="../../../cobol/IWFS0050.cbl.cobModel#P_10F10118"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10118" deadCode="false" name="B050-EX">
				<representations href="../../../cobol/IWFS0050.cbl.cobModel#P_11F10118"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10118" deadCode="false" name="B100-TRATTA-STRINGA">
				<representations href="../../../cobol/IWFS0050.cbl.cobModel#P_12F10118"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10118" deadCode="false" name="B100-EX">
				<representations href="../../../cobol/IWFS0050.cbl.cobModel#P_13F10118"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10118" deadCode="false" name="B101-CONTROLLA-SEGNO">
				<representations href="../../../cobol/IWFS0050.cbl.cobModel#P_14F10118"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10118" deadCode="false" name="B101-EX">
				<representations href="../../../cobol/IWFS0050.cbl.cobModel#P_15F10118"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10118" deadCode="false" name="B200-TRATTA-INTERI">
				<representations href="../../../cobol/IWFS0050.cbl.cobModel#P_18F10118"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10118" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/IWFS0050.cbl.cobModel#P_19F10118"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10118" deadCode="false" name="B300-TRATTA-DECIMALI">
				<representations href="../../../cobol/IWFS0050.cbl.cobModel#P_16F10118"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10118" deadCode="false" name="B300-EX">
				<representations href="../../../cobol/IWFS0050.cbl.cobModel#P_17F10118"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10118P_1F10118" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10118" targetNode="P_1F10118"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10118_I" deadCode="false" sourceNode="P_1F10118" targetNode="P_2F10118">
		<representations href="../../../cobol/IWFS0050.cbl.cobModel#S_1F10118"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10118_O" deadCode="false" sourceNode="P_1F10118" targetNode="P_3F10118">
		<representations href="../../../cobol/IWFS0050.cbl.cobModel#S_1F10118"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10118_I" deadCode="false" sourceNode="P_1F10118" targetNode="P_4F10118">
		<representations href="../../../cobol/IWFS0050.cbl.cobModel#S_3F10118"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10118_O" deadCode="false" sourceNode="P_1F10118" targetNode="P_5F10118">
		<representations href="../../../cobol/IWFS0050.cbl.cobModel#S_3F10118"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10118_I" deadCode="false" sourceNode="P_2F10118" targetNode="P_6F10118">
		<representations href="../../../cobol/IWFS0050.cbl.cobModel#S_5F10118"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10118_O" deadCode="false" sourceNode="P_2F10118" targetNode="P_7F10118">
		<representations href="../../../cobol/IWFS0050.cbl.cobModel#S_5F10118"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10118_I" deadCode="false" sourceNode="P_2F10118" targetNode="P_8F10118">
		<representations href="../../../cobol/IWFS0050.cbl.cobModel#S_6F10118"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10118_O" deadCode="false" sourceNode="P_2F10118" targetNode="P_9F10118">
		<representations href="../../../cobol/IWFS0050.cbl.cobModel#S_6F10118"/>
	</edges>
	<edges id="P_2F10118P_3F10118" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10118" targetNode="P_3F10118"/>
	<edges id="P_6F10118P_7F10118" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10118" targetNode="P_7F10118"/>
	<edges id="P_8F10118P_9F10118" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10118" targetNode="P_9F10118"/>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10118_I" deadCode="false" sourceNode="P_4F10118" targetNode="P_10F10118">
		<representations href="../../../cobol/IWFS0050.cbl.cobModel#S_31F10118"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10118_O" deadCode="false" sourceNode="P_4F10118" targetNode="P_11F10118">
		<representations href="../../../cobol/IWFS0050.cbl.cobModel#S_31F10118"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10118_I" deadCode="false" sourceNode="P_4F10118" targetNode="P_12F10118">
		<representations href="../../../cobol/IWFS0050.cbl.cobModel#S_32F10118"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10118_O" deadCode="false" sourceNode="P_4F10118" targetNode="P_13F10118">
		<representations href="../../../cobol/IWFS0050.cbl.cobModel#S_32F10118"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10118_I" deadCode="false" sourceNode="P_4F10118" targetNode="P_14F10118">
		<representations href="../../../cobol/IWFS0050.cbl.cobModel#S_34F10118"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10118_O" deadCode="false" sourceNode="P_4F10118" targetNode="P_15F10118">
		<representations href="../../../cobol/IWFS0050.cbl.cobModel#S_34F10118"/>
	</edges>
	<edges id="P_4F10118P_5F10118" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10118" targetNode="P_5F10118"/>
	<edges id="P_10F10118P_11F10118" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10118" targetNode="P_11F10118"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10118_I" deadCode="false" sourceNode="P_12F10118" targetNode="P_16F10118">
		<representations href="../../../cobol/IWFS0050.cbl.cobModel#S_43F10118"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10118_O" deadCode="false" sourceNode="P_12F10118" targetNode="P_17F10118">
		<representations href="../../../cobol/IWFS0050.cbl.cobModel#S_43F10118"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10118_I" deadCode="false" sourceNode="P_12F10118" targetNode="P_18F10118">
		<representations href="../../../cobol/IWFS0050.cbl.cobModel#S_44F10118"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10118_O" deadCode="false" sourceNode="P_12F10118" targetNode="P_19F10118">
		<representations href="../../../cobol/IWFS0050.cbl.cobModel#S_44F10118"/>
	</edges>
	<edges id="P_12F10118P_13F10118" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10118" targetNode="P_13F10118"/>
	<edges id="P_14F10118P_15F10118" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10118" targetNode="P_15F10118"/>
	<edges id="P_18F10118P_19F10118" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10118" targetNode="P_19F10118"/>
	<edges id="P_16F10118P_17F10118" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10118" targetNode="P_17F10118"/>
</Package>

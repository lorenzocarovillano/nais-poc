<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IABS0050" cbl:id="IABS0050" xsi:id="IABS0050" packageRef="IABS0050.igd#IABS0050" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IABS0050_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10004" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IABS0050.cbl.cobModel#SC_1F10004"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10004" deadCode="false" name="PROGRAM_IABS0050_FIRST_SENTENCES">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_1F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10004" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_2F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10004" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_3F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10004" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_8F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10004" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_9F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10004" deadCode="false" name="A300-ELABORA">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_4F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10004" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_5F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10004" deadCode="false" name="A301-ELABORA-PK">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_10F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10004" deadCode="false" name="A301-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_11F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10004" deadCode="false" name="A302-ELABORA-WC">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_12F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10004" deadCode="false" name="A302-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_13F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10004" deadCode="false" name="A310-SELECT-PK">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_14F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10004" deadCode="false" name="A310-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_15F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10004" deadCode="false" name="W000-SELECT-WC">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_16F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10004" deadCode="false" name="W000-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_17F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10004" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_18F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10004" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_19F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10004" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_20F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10004" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_21F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10004" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_22F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10004" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_23F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10004" deadCode="true" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_24F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10004" deadCode="true" name="Z950-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_25F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10004" deadCode="true" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_26F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10004" deadCode="true" name="Z960-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_27F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10004" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_6F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10004" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_7F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10004" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_28F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10004" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_29F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10004" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_30F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10004" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_31F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10004" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_32F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10004" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_33F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10004" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_34F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10004" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_35F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10004" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_36F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10004" deadCode="true" name="Z800-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_41F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10004" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_37F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10004" deadCode="true" name="Z810-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_38F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10004" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_39F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10004" deadCode="true" name="Z820-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_40F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10004" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_42F10004"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10004" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/IABS0050.cbl.cobModel#P_43F10004"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_BTC_BATCH_TYPE" name="BTC_BATCH_TYPE">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_BTC_BATCH_TYPE"/>
		</children>
	</packageNode>
	<edges id="SC_1F10004P_1F10004" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10004" targetNode="P_1F10004"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10004_I" deadCode="false" sourceNode="P_1F10004" targetNode="P_2F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_1F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10004_O" deadCode="false" sourceNode="P_1F10004" targetNode="P_3F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_1F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10004_I" deadCode="false" sourceNode="P_1F10004" targetNode="P_4F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_2F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10004_O" deadCode="false" sourceNode="P_1F10004" targetNode="P_5F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_2F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10004_I" deadCode="false" sourceNode="P_2F10004" targetNode="P_6F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_9F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10004_O" deadCode="false" sourceNode="P_2F10004" targetNode="P_7F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_9F10004"/>
	</edges>
	<edges id="P_2F10004P_3F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10004" targetNode="P_3F10004"/>
	<edges id="P_8F10004P_9F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10004" targetNode="P_9F10004"/>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10004_I" deadCode="false" sourceNode="P_4F10004" targetNode="P_10F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_22F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10004_O" deadCode="false" sourceNode="P_4F10004" targetNode="P_11F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_22F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10004_I" deadCode="false" sourceNode="P_4F10004" targetNode="P_12F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_23F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10004_O" deadCode="false" sourceNode="P_4F10004" targetNode="P_13F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_23F10004"/>
	</edges>
	<edges id="P_4F10004P_5F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10004" targetNode="P_5F10004"/>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10004_I" deadCode="false" sourceNode="P_10F10004" targetNode="P_14F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_27F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10004_O" deadCode="false" sourceNode="P_10F10004" targetNode="P_15F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_27F10004"/>
	</edges>
	<edges id="P_10F10004P_11F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10004" targetNode="P_11F10004"/>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10004_I" deadCode="false" sourceNode="P_12F10004" targetNode="P_16F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_31F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10004_O" deadCode="false" sourceNode="P_12F10004" targetNode="P_17F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_31F10004"/>
	</edges>
	<edges id="P_12F10004P_13F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10004" targetNode="P_13F10004"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10004_I" deadCode="false" sourceNode="P_14F10004" targetNode="P_8F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_35F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10004_O" deadCode="false" sourceNode="P_14F10004" targetNode="P_9F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_35F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10004_I" deadCode="false" sourceNode="P_14F10004" targetNode="P_18F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_37F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10004_O" deadCode="false" sourceNode="P_14F10004" targetNode="P_19F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_37F10004"/>
	</edges>
	<edges id="P_14F10004P_15F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10004" targetNode="P_15F10004"/>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10004_I" deadCode="false" sourceNode="P_16F10004" targetNode="P_20F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_39F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10004_O" deadCode="false" sourceNode="P_16F10004" targetNode="P_21F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_39F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10004_I" deadCode="false" sourceNode="P_16F10004" targetNode="P_8F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_41F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10004_O" deadCode="false" sourceNode="P_16F10004" targetNode="P_9F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_41F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10004_I" deadCode="false" sourceNode="P_16F10004" targetNode="P_18F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_43F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10004_O" deadCode="false" sourceNode="P_16F10004" targetNode="P_19F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_43F10004"/>
	</edges>
	<edges id="P_16F10004P_17F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10004" targetNode="P_17F10004"/>
	<edges id="P_18F10004P_19F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10004" targetNode="P_19F10004"/>
	<edges id="P_20F10004P_21F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10004" targetNode="P_21F10004"/>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10004_I" deadCode="false" sourceNode="P_6F10004" targetNode="P_28F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_88F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10004_O" deadCode="false" sourceNode="P_6F10004" targetNode="P_29F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_88F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10004_I" deadCode="false" sourceNode="P_6F10004" targetNode="P_30F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_90F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10004_O" deadCode="false" sourceNode="P_6F10004" targetNode="P_31F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_90F10004"/>
	</edges>
	<edges id="P_6F10004P_7F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10004" targetNode="P_7F10004"/>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10004_I" deadCode="true" sourceNode="P_28F10004" targetNode="P_32F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_95F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10004_O" deadCode="true" sourceNode="P_28F10004" targetNode="P_33F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_95F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10004_I" deadCode="true" sourceNode="P_28F10004" targetNode="P_32F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_100F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10004_O" deadCode="true" sourceNode="P_28F10004" targetNode="P_33F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_100F10004"/>
	</edges>
	<edges id="P_28F10004P_29F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10004" targetNode="P_29F10004"/>
	<edges id="P_30F10004P_31F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10004" targetNode="P_31F10004"/>
	<edges id="P_32F10004P_33F10004" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10004" targetNode="P_33F10004"/>
	<edges xsi:type="cbl:PerformEdge" id="S_129F10004_I" deadCode="true" sourceNode="P_36F10004" targetNode="P_37F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_129F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_129F10004_O" deadCode="true" sourceNode="P_36F10004" targetNode="P_38F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_129F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10004_I" deadCode="true" sourceNode="P_36F10004" targetNode="P_39F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_130F10004"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10004_O" deadCode="true" sourceNode="P_36F10004" targetNode="P_40F10004">
		<representations href="../../../cobol/IABS0050.cbl.cobModel#S_130F10004"/>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_34F10004_POS1" deadCode="false" targetNode="P_14F10004" sourceNode="DB2_BTC_BATCH_TYPE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_34F10004"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_40F10004_POS1" deadCode="false" targetNode="P_16F10004" sourceNode="DB2_BTC_BATCH_TYPE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_40F10004"></representations>
	</edges>
</Package>

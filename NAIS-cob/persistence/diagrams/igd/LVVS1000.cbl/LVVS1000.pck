<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS1000" cbl:id="LVVS1000" xsi:id="LVVS1000" packageRef="LVVS1000.igd#LVVS1000" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS1000_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10363" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS1000.cbl.cobModel#SC_1F10363"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10363" deadCode="false" name="PROGRAM_LVVS1000_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS1000.cbl.cobModel#P_1F10363"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10363" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS1000.cbl.cobModel#P_2F10363"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10363" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS1000.cbl.cobModel#P_3F10363"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10363" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS1000.cbl.cobModel#P_4F10363"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10363" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS1000.cbl.cobModel#P_5F10363"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10363" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS1000.cbl.cobModel#P_8F10363"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10363" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS1000.cbl.cobModel#P_9F10363"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10363" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS1000.cbl.cobModel#P_6F10363"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10363" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS1000.cbl.cobModel#P_7F10363"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10363P_1F10363" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10363" targetNode="P_1F10363"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10363_I" deadCode="false" sourceNode="P_1F10363" targetNode="P_2F10363">
		<representations href="../../../cobol/LVVS1000.cbl.cobModel#S_1F10363"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10363_O" deadCode="false" sourceNode="P_1F10363" targetNode="P_3F10363">
		<representations href="../../../cobol/LVVS1000.cbl.cobModel#S_1F10363"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10363_I" deadCode="false" sourceNode="P_1F10363" targetNode="P_4F10363">
		<representations href="../../../cobol/LVVS1000.cbl.cobModel#S_2F10363"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10363_O" deadCode="false" sourceNode="P_1F10363" targetNode="P_5F10363">
		<representations href="../../../cobol/LVVS1000.cbl.cobModel#S_2F10363"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10363_I" deadCode="false" sourceNode="P_1F10363" targetNode="P_6F10363">
		<representations href="../../../cobol/LVVS1000.cbl.cobModel#S_3F10363"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10363_O" deadCode="false" sourceNode="P_1F10363" targetNode="P_7F10363">
		<representations href="../../../cobol/LVVS1000.cbl.cobModel#S_3F10363"/>
	</edges>
	<edges id="P_2F10363P_3F10363" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10363" targetNode="P_3F10363"/>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10363_I" deadCode="false" sourceNode="P_4F10363" targetNode="P_8F10363">
		<representations href="../../../cobol/LVVS1000.cbl.cobModel#S_9F10363"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10363_O" deadCode="false" sourceNode="P_4F10363" targetNode="P_9F10363">
		<representations href="../../../cobol/LVVS1000.cbl.cobModel#S_9F10363"/>
	</edges>
	<edges id="P_4F10363P_5F10363" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10363" targetNode="P_5F10363"/>
	<edges id="P_8F10363P_9F10363" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10363" targetNode="P_9F10363"/>
</Package>

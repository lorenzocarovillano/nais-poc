<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS2750" cbl:id="LVVS2750" xsi:id="LVVS2750" packageRef="LVVS2750.igd#LVVS2750" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS2750_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10375" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS2750.cbl.cobModel#SC_1F10375"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10375" deadCode="false" name="PROGRAM_LVVS2750_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_1F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10375" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_2F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10375" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_3F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10375" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_4F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10375" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_5F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10375" deadCode="false" name="VERIFICA-MOVIMENTO">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_8F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10375" deadCode="false" name="VERIFICA-MOVIMENTO-EX">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_9F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10375" deadCode="false" name="RECUP-MOVI-COMUN">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_14F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10375" deadCode="false" name="RECUP-MOVI-COMUN-EX">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_15F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10375" deadCode="false" name="CLOSE-MOVI">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_24F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10375" deadCode="false" name="CLOSE-MOVI-EX">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_25F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10375" deadCode="false" name="LETTURA-IMPST-BOLLO">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_18F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10375" deadCode="false" name="LETTURA-IMPST-BOLLO-EX">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_19F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10375" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_22F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10375" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_23F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10375" deadCode="false" name="S1150-CALCOLO-ANNO">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_10F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10375" deadCode="false" name="S1150-CALCOLO-ANNO-EX">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_11F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10375" deadCode="false" name="S1200-CALCOLO-IMPORTO">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_12F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10375" deadCode="false" name="S1200-CALCOLO-IMPORTO-EX">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_13F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10375" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_6F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10375" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_7F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10375" deadCode="false" name="LETTURA-IMPST-BOLLO-2">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_20F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10375" deadCode="false" name="LETTURA-IMPST-BOLLO-2-EX">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_21F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10375" deadCode="false" name="VALORIZZA-OUTPUT-P58">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_26F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10375" deadCode="false" name="VALORIZZA-OUTPUT-P58-EX">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_27F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10375" deadCode="false" name="INIZIA-TOT-P58">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_16F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10375" deadCode="false" name="INIZIA-TOT-P58-EX">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_17F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10375" deadCode="true" name="INIZIA-NULL-P58">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_32F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10375" deadCode="false" name="INIZIA-NULL-P58-EX">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_33F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10375" deadCode="false" name="INIZIA-ZEROES-P58">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_28F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10375" deadCode="false" name="INIZIA-ZEROES-P58-EX">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_29F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10375" deadCode="false" name="INIZIA-SPACES-P58">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_30F10375"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10375" deadCode="false" name="INIZIA-SPACES-P58-EX">
				<representations href="../../../cobol/LVVS2750.cbl.cobModel#P_31F10375"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS6040" name="LDBS6040">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10230"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSE590" name="LDBSE590">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10264"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10375P_1F10375" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10375" targetNode="P_1F10375"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10375_I" deadCode="false" sourceNode="P_1F10375" targetNode="P_2F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_1F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10375_O" deadCode="false" sourceNode="P_1F10375" targetNode="P_3F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_1F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10375_I" deadCode="false" sourceNode="P_1F10375" targetNode="P_4F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_2F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10375_O" deadCode="false" sourceNode="P_1F10375" targetNode="P_5F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_2F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10375_I" deadCode="false" sourceNode="P_1F10375" targetNode="P_6F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_3F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10375_O" deadCode="false" sourceNode="P_1F10375" targetNode="P_7F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_3F10375"/>
	</edges>
	<edges id="P_2F10375P_3F10375" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10375" targetNode="P_3F10375"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10375_I" deadCode="false" sourceNode="P_4F10375" targetNode="P_8F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_11F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10375_O" deadCode="false" sourceNode="P_4F10375" targetNode="P_9F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_11F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10375_I" deadCode="false" sourceNode="P_4F10375" targetNode="P_10F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_13F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10375_O" deadCode="false" sourceNode="P_4F10375" targetNode="P_11F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_13F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10375_I" deadCode="false" sourceNode="P_4F10375" targetNode="P_12F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_15F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10375_O" deadCode="false" sourceNode="P_4F10375" targetNode="P_13F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_15F10375"/>
	</edges>
	<edges id="P_4F10375P_5F10375" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10375" targetNode="P_5F10375"/>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10375_I" deadCode="false" sourceNode="P_8F10375" targetNode="P_14F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_19F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10375_O" deadCode="false" sourceNode="P_8F10375" targetNode="P_15F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_19F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10375_I" deadCode="false" sourceNode="P_8F10375" targetNode="P_16F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_25F10375"/>
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_26F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10375_O" deadCode="false" sourceNode="P_8F10375" targetNode="P_17F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_25F10375"/>
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_26F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10375_I" deadCode="false" sourceNode="P_8F10375" targetNode="P_18F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_27F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10375_O" deadCode="false" sourceNode="P_8F10375" targetNode="P_19F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_27F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10375_I" deadCode="false" sourceNode="P_8F10375" targetNode="P_20F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_30F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10375_O" deadCode="false" sourceNode="P_8F10375" targetNode="P_21F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_30F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10375_I" deadCode="false" sourceNode="P_8F10375" targetNode="P_22F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_31F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10375_O" deadCode="false" sourceNode="P_8F10375" targetNode="P_23F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_31F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10375_I" deadCode="false" sourceNode="P_8F10375" targetNode="P_18F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_33F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10375_O" deadCode="false" sourceNode="P_8F10375" targetNode="P_19F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_33F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10375_I" deadCode="false" sourceNode="P_8F10375" targetNode="P_22F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_34F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10375_O" deadCode="false" sourceNode="P_8F10375" targetNode="P_23F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_34F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10375_I" deadCode="false" sourceNode="P_8F10375" targetNode="P_18F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_36F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10375_O" deadCode="false" sourceNode="P_8F10375" targetNode="P_19F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_36F10375"/>
	</edges>
	<edges id="P_8F10375P_9F10375" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10375" targetNode="P_9F10375"/>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10375_I" deadCode="false" sourceNode="P_14F10375" targetNode="P_24F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_45F10375"/>
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_67F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10375_O" deadCode="false" sourceNode="P_14F10375" targetNode="P_25F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_45F10375"/>
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_67F10375"/>
	</edges>
	<edges id="P_14F10375P_15F10375" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10375" targetNode="P_15F10375"/>
	<edges id="P_24F10375P_25F10375" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10375" targetNode="P_25F10375"/>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10375_I" deadCode="false" sourceNode="P_18F10375" targetNode="P_26F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_96F10375"/>
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_114F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10375_O" deadCode="false" sourceNode="P_18F10375" targetNode="P_27F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_96F10375"/>
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_114F10375"/>
	</edges>
	<edges id="P_18F10375P_19F10375" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10375" targetNode="P_19F10375"/>
	<edges id="P_22F10375P_23F10375" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10375" targetNode="P_23F10375"/>
	<edges id="P_10F10375P_11F10375" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10375" targetNode="P_11F10375"/>
	<edges id="P_12F10375P_13F10375" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10375" targetNode="P_13F10375"/>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10375_I" deadCode="false" sourceNode="P_20F10375" targetNode="P_26F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_148F10375"/>
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_165F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10375_O" deadCode="false" sourceNode="P_20F10375" targetNode="P_27F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_148F10375"/>
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_165F10375"/>
	</edges>
	<edges id="P_20F10375P_21F10375" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10375" targetNode="P_21F10375"/>
	<edges id="P_26F10375P_27F10375" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10375" targetNode="P_27F10375"/>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10375_I" deadCode="false" sourceNode="P_16F10375" targetNode="P_28F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_213F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10375_O" deadCode="false" sourceNode="P_16F10375" targetNode="P_29F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_213F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_214F10375_I" deadCode="false" sourceNode="P_16F10375" targetNode="P_30F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_214F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_214F10375_O" deadCode="false" sourceNode="P_16F10375" targetNode="P_31F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_214F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_215F10375_I" deadCode="true" sourceNode="P_16F10375" targetNode="P_32F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_215F10375"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_215F10375_O" deadCode="true" sourceNode="P_16F10375" targetNode="P_33F10375">
		<representations href="../../../cobol/LVVS2750.cbl.cobModel#S_215F10375"/>
	</edges>
	<edges id="P_16F10375P_17F10375" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10375" targetNode="P_17F10375"/>
	<edges id="P_32F10375P_33F10375" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10375" targetNode="P_33F10375"/>
	<edges id="P_28F10375P_29F10375" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10375" targetNode="P_29F10375"/>
	<edges id="P_30F10375P_31F10375" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10375" targetNode="P_31F10375"/>
	<edges xsi:type="cbl:CallEdge" id="S_53F10375" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_14F10375" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_53F10375"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_79F10375" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_24F10375" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_79F10375"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_103F10375" deadCode="false" name="Dynamic LDBSE590" sourceNode="P_18F10375" targetNode="LDBSE590">
		<representations href="../../../cobol/../importantStmts.cobModel#S_103F10375"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_155F10375" deadCode="false" name="Dynamic LDBSE590" sourceNode="P_20F10375" targetNode="LDBSE590">
		<representations href="../../../cobol/../importantStmts.cobModel#S_155F10375"></representations>
	</edges>
</Package>

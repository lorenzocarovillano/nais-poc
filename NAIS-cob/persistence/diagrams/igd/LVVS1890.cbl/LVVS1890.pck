<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS1890" cbl:id="LVVS1890" xsi:id="LVVS1890" packageRef="LVVS1890.igd#LVVS1890" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS1890_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10368" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS1890.cbl.cobModel#SC_1F10368"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10368" deadCode="false" name="PROGRAM_LVVS1890_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS1890.cbl.cobModel#P_1F10368"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10368" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS1890.cbl.cobModel#P_2F10368"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10368" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS1890.cbl.cobModel#P_3F10368"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10368" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS1890.cbl.cobModel#P_4F10368"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10368" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS1890.cbl.cobModel#P_5F10368"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10368" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS1890.cbl.cobModel#P_8F10368"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10368" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS1890.cbl.cobModel#P_9F10368"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10368" deadCode="false" name="S1200-CERCA-FRAZ-GAR">
				<representations href="../../../cobol/LVVS1890.cbl.cobModel#P_10F10368"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10368" deadCode="false" name="S1200-EX">
				<representations href="../../../cobol/LVVS1890.cbl.cobModel#P_11F10368"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10368" deadCode="false" name="S1300-CERCA-PARAM-MOVI">
				<representations href="../../../cobol/LVVS1890.cbl.cobModel#P_12F10368"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10368" deadCode="false" name="S1300-EX">
				<representations href="../../../cobol/LVVS1890.cbl.cobModel#P_13F10368"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10368" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS1890.cbl.cobModel#P_6F10368"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10368" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS1890.cbl.cobModel#P_7F10368"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10368P_1F10368" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10368" targetNode="P_1F10368"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10368_I" deadCode="false" sourceNode="P_1F10368" targetNode="P_2F10368">
		<representations href="../../../cobol/LVVS1890.cbl.cobModel#S_1F10368"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10368_O" deadCode="false" sourceNode="P_1F10368" targetNode="P_3F10368">
		<representations href="../../../cobol/LVVS1890.cbl.cobModel#S_1F10368"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10368_I" deadCode="false" sourceNode="P_1F10368" targetNode="P_4F10368">
		<representations href="../../../cobol/LVVS1890.cbl.cobModel#S_2F10368"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10368_O" deadCode="false" sourceNode="P_1F10368" targetNode="P_5F10368">
		<representations href="../../../cobol/LVVS1890.cbl.cobModel#S_2F10368"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10368_I" deadCode="false" sourceNode="P_1F10368" targetNode="P_6F10368">
		<representations href="../../../cobol/LVVS1890.cbl.cobModel#S_3F10368"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10368_O" deadCode="false" sourceNode="P_1F10368" targetNode="P_7F10368">
		<representations href="../../../cobol/LVVS1890.cbl.cobModel#S_3F10368"/>
	</edges>
	<edges id="P_2F10368P_3F10368" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10368" targetNode="P_3F10368"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10368_I" deadCode="false" sourceNode="P_4F10368" targetNode="P_8F10368">
		<representations href="../../../cobol/LVVS1890.cbl.cobModel#S_11F10368"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10368_O" deadCode="false" sourceNode="P_4F10368" targetNode="P_9F10368">
		<representations href="../../../cobol/LVVS1890.cbl.cobModel#S_11F10368"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10368_I" deadCode="false" sourceNode="P_4F10368" targetNode="P_10F10368">
		<representations href="../../../cobol/LVVS1890.cbl.cobModel#S_14F10368"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10368_O" deadCode="false" sourceNode="P_4F10368" targetNode="P_11F10368">
		<representations href="../../../cobol/LVVS1890.cbl.cobModel#S_14F10368"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10368_I" deadCode="false" sourceNode="P_4F10368" targetNode="P_12F10368">
		<representations href="../../../cobol/LVVS1890.cbl.cobModel#S_16F10368"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10368_O" deadCode="false" sourceNode="P_4F10368" targetNode="P_13F10368">
		<representations href="../../../cobol/LVVS1890.cbl.cobModel#S_16F10368"/>
	</edges>
	<edges id="P_4F10368P_5F10368" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10368" targetNode="P_5F10368"/>
	<edges id="P_8F10368P_9F10368" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10368" targetNode="P_9F10368"/>
	<edges id="P_10F10368P_11F10368" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10368" targetNode="P_11F10368"/>
	<edges id="P_12F10368P_13F10368" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10368" targetNode="P_13F10368"/>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0116" cbl:id="LVVS0116" xsi:id="LVVS0116" packageRef="LVVS0116.igd#LVVS0116" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0116_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10353" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0116.cbl.cobModel#SC_1F10353"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10353" deadCode="false" name="PROGRAM_LVVS0116_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_1F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10353" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_2F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10353" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_3F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10353" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_4F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10353" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_5F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10353" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_8F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10353" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_9F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10353" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_10F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10353" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_11F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10353" deadCode="false" name="S1230-RECUP-ID-COMUN">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_12F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10353" deadCode="false" name="S1230-EX">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_13F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10353" deadCode="false" name="RECUP-STAT-MOVI-COMUN">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_16F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10353" deadCode="false" name="RECUP-STAT-MOVI-COMUN-EX">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_17F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10353" deadCode="false" name="S1235-RECUP-MOVI-FINRIO">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_18F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10353" deadCode="false" name="S1235-EX">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_19F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10353" deadCode="false" name="S1250-CALCOLA-QUOTE">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_14F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10353" deadCode="false" name="S1250-EX">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_15F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10353" deadCode="false" name="S1252-CHIUDE-CURVAS">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_22F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10353" deadCode="false" name="S1252-EX">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_23F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10353" deadCode="false" name="S1251-CALCOLA-QUOTE">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_20F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10353" deadCode="false" name="S1251-EX">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_21F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10353" deadCode="false" name="S1253-ARROTONDA-IMP">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_24F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10353" deadCode="false" name="S1253-EX">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_25F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10353" deadCode="true" name="S1260-SOTTRAI-ANNULLO">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_26F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10353" deadCode="true" name="S1260-EX">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_29F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10353" deadCode="true" name="S1261-SOTTRAI-QUOTE">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_27F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10353" deadCode="true" name="S1261-EX">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_28F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10353" deadCode="true" name="S1262-CALCOLA-QUOTE">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_30F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10353" deadCode="true" name="S1262-EX">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_31F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10353" deadCode="true" name="S1263-CHIUDE-CURVAS">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_32F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10353" deadCode="true" name="S1263-EX">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_33F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10353" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_6F10353"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10353" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0116.cbl.cobModel#P_7F10353"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS1530" name="LDBS1530">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10162"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSSTW0" name="IDBSSTW0">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10091"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS5950" name="LDBS5950">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10226"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS4910" name="LDBS4910">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10214"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LVVS0116_LDBS5990" name="Dynamic LVVS0116 LDBS5990" missing="true">
			<representations href="../../../../missing.xmi#IDD2JFZ2FFGYVQB2UZTNBXEBX3PP2N4SQIT0GYQ2LJGKNKMVI30N1K"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LVVS0116_LDBS6000" name="Dynamic LVVS0116 LDBS6000" missing="true">
			<representations href="../../../../missing.xmi#ID1200EEC43HNSIVCKQVVM3I515OSBGE5TKNLYPWIMI1J1Y4OKNK2B"/>
		</children>
	</packageNode>
	<edges id="SC_1F10353P_1F10353" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10353" targetNode="P_1F10353"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10353_I" deadCode="false" sourceNode="P_1F10353" targetNode="P_2F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_1F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10353_O" deadCode="false" sourceNode="P_1F10353" targetNode="P_3F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_1F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10353_I" deadCode="false" sourceNode="P_1F10353" targetNode="P_4F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_2F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10353_O" deadCode="false" sourceNode="P_1F10353" targetNode="P_5F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_2F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10353_I" deadCode="false" sourceNode="P_1F10353" targetNode="P_6F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_3F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10353_O" deadCode="false" sourceNode="P_1F10353" targetNode="P_7F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_3F10353"/>
	</edges>
	<edges id="P_2F10353P_3F10353" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10353" targetNode="P_3F10353"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10353_I" deadCode="false" sourceNode="P_4F10353" targetNode="P_8F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_11F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10353_O" deadCode="false" sourceNode="P_4F10353" targetNode="P_9F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_11F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10353_I" deadCode="false" sourceNode="P_4F10353" targetNode="P_10F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_13F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10353_O" deadCode="false" sourceNode="P_4F10353" targetNode="P_11F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_13F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10353_I" deadCode="false" sourceNode="P_4F10353" targetNode="P_12F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_21F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10353_O" deadCode="false" sourceNode="P_4F10353" targetNode="P_13F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_21F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10353_I" deadCode="false" sourceNode="P_4F10353" targetNode="P_14F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_23F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10353_O" deadCode="false" sourceNode="P_4F10353" targetNode="P_15F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_23F10353"/>
	</edges>
	<edges id="P_4F10353P_5F10353" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10353" targetNode="P_5F10353"/>
	<edges id="P_8F10353P_9F10353" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10353" targetNode="P_9F10353"/>
	<edges id="P_10F10353P_11F10353" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10353" targetNode="P_11F10353"/>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10353_I" deadCode="false" sourceNode="P_12F10353" targetNode="P_16F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_83F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10353_O" deadCode="false" sourceNode="P_12F10353" targetNode="P_17F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_83F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10353_I" deadCode="false" sourceNode="P_12F10353" targetNode="P_18F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_85F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10353_O" deadCode="false" sourceNode="P_12F10353" targetNode="P_19F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_85F10353"/>
	</edges>
	<edges id="P_12F10353P_13F10353" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10353" targetNode="P_13F10353"/>
	<edges id="P_16F10353P_17F10353" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10353" targetNode="P_17F10353"/>
	<edges id="P_18F10353P_19F10353" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10353" targetNode="P_19F10353"/>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10353_I" deadCode="false" sourceNode="P_14F10353" targetNode="P_20F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_179F10353"/>
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_189F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10353_O" deadCode="false" sourceNode="P_14F10353" targetNode="P_21F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_179F10353"/>
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_189F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10353_I" deadCode="false" sourceNode="P_14F10353" targetNode="P_22F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_195F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10353_O" deadCode="false" sourceNode="P_14F10353" targetNode="P_23F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_195F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_201F10353_I" deadCode="false" sourceNode="P_14F10353" targetNode="P_24F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_201F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_201F10353_O" deadCode="false" sourceNode="P_14F10353" targetNode="P_25F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_201F10353"/>
	</edges>
	<edges id="P_14F10353P_15F10353" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10353" targetNode="P_15F10353"/>
	<edges id="P_22F10353P_23F10353" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10353" targetNode="P_23F10353"/>
	<edges id="P_20F10353P_21F10353" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10353" targetNode="P_21F10353"/>
	<edges id="P_24F10353P_25F10353" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10353" targetNode="P_25F10353"/>
	<edges xsi:type="cbl:PerformEdge" id="S_261F10353_I" deadCode="true" sourceNode="P_26F10353" targetNode="P_27F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_261F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_261F10353_O" deadCode="true" sourceNode="P_26F10353" targetNode="P_28F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_261F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10353_I" deadCode="true" sourceNode="P_27F10353" targetNode="P_30F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_277F10353"/>
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_284F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10353_O" deadCode="true" sourceNode="P_27F10353" targetNode="P_31F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_277F10353"/>
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_284F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_290F10353_I" deadCode="true" sourceNode="P_27F10353" targetNode="P_32F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_290F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_290F10353_O" deadCode="true" sourceNode="P_27F10353" targetNode="P_33F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_290F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_296F10353_I" deadCode="true" sourceNode="P_27F10353" targetNode="P_24F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_296F10353"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_296F10353_O" deadCode="true" sourceNode="P_27F10353" targetNode="P_25F10353">
		<representations href="../../../cobol/LVVS0116.cbl.cobModel#S_296F10353"/>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_78F10353" deadCode="false" name="Dynamic LDBS1530" sourceNode="P_12F10353" targetNode="LDBS1530">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10353"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_108F10353" deadCode="false" name="Dynamic IDBSSTW0" sourceNode="P_16F10353" targetNode="IDBSSTW0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_108F10353"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_153F10353" deadCode="false" name="Dynamic LDBS5950" sourceNode="P_18F10353" targetNode="LDBS5950">
		<representations href="../../../cobol/../importantStmts.cobModel#S_153F10353"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_180F10353" deadCode="false" name="Dynamic LDBS4910" sourceNode="P_14F10353" targetNode="LDBS4910">
		<representations href="../../../cobol/../importantStmts.cobModel#S_180F10353"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_204F10353" deadCode="false" name="Dynamic LDBS4910" sourceNode="P_22F10353" targetNode="LDBS4910">
		<representations href="../../../cobol/../importantStmts.cobModel#S_204F10353"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_256F10353" deadCode="true" name="Dynamic LDBS5990" sourceNode="P_26F10353" targetNode="Dynamic_LVVS0116_LDBS5990">
		<representations href="../../../cobol/../importantStmts.cobModel#S_256F10353"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_278F10353" deadCode="true" name="Dynamic LDBS6000" sourceNode="P_27F10353" targetNode="Dynamic_LVVS0116_LDBS6000">
		<representations href="../../../cobol/../importantStmts.cobModel#S_278F10353"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_313F10353" deadCode="true" name="Dynamic LDBS6000" sourceNode="P_32F10353" targetNode="Dynamic_LVVS0116_LDBS6000">
		<representations href="../../../cobol/../importantStmts.cobModel#S_313F10353"></representations>
	</edges>
</Package>

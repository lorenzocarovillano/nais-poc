<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IDBSODE0" cbl:id="IDBSODE0" xsi:id="IDBSODE0" packageRef="IDBSODE0.igd#IDBSODE0" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IDBSODE0_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10060" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IDBSODE0.cbl.cobModel#SC_1F10060"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10060" deadCode="false" name="PROGRAM_IDBSODE0_FIRST_SENTENCES">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_1F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10060" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_2F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10060" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_3F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10060" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_28F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10060" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_29F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10060" deadCode="false" name="A200-ELABORA-PK">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_24F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10060" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_25F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10060" deadCode="false" name="A300-ELABORA-ID-EFF">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_4F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10060" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_5F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10060" deadCode="false" name="A400-ELABORA-IDP-EFF">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_6F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10060" deadCode="false" name="A400-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_7F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10060" deadCode="false" name="A500-ELABORA-IBO">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_8F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10060" deadCode="false" name="A500-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_9F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10060" deadCode="false" name="A600-ELABORA-IBS">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_10F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10060" deadCode="false" name="A600-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_11F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10060" deadCode="false" name="A700-ELABORA-IDO">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_12F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10060" deadCode="false" name="A700-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_13F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10060" deadCode="false" name="B300-ELABORA-ID-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_14F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10060" deadCode="false" name="B300-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_15F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10060" deadCode="false" name="B400-ELABORA-IDP-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_16F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10060" deadCode="false" name="B400-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_17F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10060" deadCode="false" name="B500-ELABORA-IBO-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_18F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10060" deadCode="false" name="B500-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_19F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10060" deadCode="false" name="B600-ELABORA-IBS-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_20F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10060" deadCode="false" name="B600-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_21F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10060" deadCode="false" name="B700-ELABORA-IDO-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_22F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10060" deadCode="false" name="B700-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_23F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10060" deadCode="false" name="A210-SELECT-PK">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_30F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10060" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_31F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10060" deadCode="false" name="A220-INSERT-PK">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_32F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10060" deadCode="false" name="A220-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_33F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10060" deadCode="false" name="A230-UPDATE-PK">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_34F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10060" deadCode="false" name="A230-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_35F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10060" deadCode="false" name="A240-DELETE-PK">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_36F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10060" deadCode="false" name="A240-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_37F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_140F10060" deadCode="false" name="A305-DECLARE-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_140F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_141F10060" deadCode="false" name="A305-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_141F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10060" deadCode="false" name="A310-SELECT-ID-EFF">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_38F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10060" deadCode="false" name="A310-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_39F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_142F10060" deadCode="false" name="A330-UPDATE-ID-EFF">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_142F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_143F10060" deadCode="false" name="A330-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_143F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_144F10060" deadCode="false" name="A360-OPEN-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_144F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_145F10060" deadCode="false" name="A360-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_145F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_146F10060" deadCode="false" name="A370-CLOSE-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_146F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_147F10060" deadCode="false" name="A370-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_147F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_148F10060" deadCode="true" name="A380-FETCH-FIRST-ID-EFF">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_148F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_151F10060" deadCode="true" name="A380-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_151F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_149F10060" deadCode="false" name="A390-FETCH-NEXT-ID-EFF">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_149F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_150F10060" deadCode="false" name="A390-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_150F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_152F10060" deadCode="false" name="A405-DECLARE-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_152F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_153F10060" deadCode="false" name="A405-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_153F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10060" deadCode="false" name="A410-SELECT-IDP-EFF">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_42F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10060" deadCode="false" name="A410-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_43F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10060" deadCode="false" name="A460-OPEN-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_44F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10060" deadCode="false" name="A460-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_45F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10060" deadCode="false" name="A470-CLOSE-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_46F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10060" deadCode="false" name="A470-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_47F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10060" deadCode="false" name="A480-FETCH-FIRST-IDP-EFF">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_48F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10060" deadCode="false" name="A480-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_49F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10060" deadCode="false" name="A490-FETCH-NEXT-IDP-EFF">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_50F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10060" deadCode="false" name="A490-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_51F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_154F10060" deadCode="false" name="A505-DECLARE-CURSOR-IBO">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_154F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_155F10060" deadCode="false" name="A505-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_155F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10060" deadCode="false" name="A510-SELECT-IBO">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_52F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10060" deadCode="false" name="A510-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_53F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10060" deadCode="false" name="A560-OPEN-CURSOR-IBO">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_54F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10060" deadCode="false" name="A560-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_55F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10060" deadCode="false" name="A570-CLOSE-CURSOR-IBO">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_56F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10060" deadCode="false" name="A570-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_57F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10060" deadCode="false" name="A580-FETCH-FIRST-IBO">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_58F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10060" deadCode="false" name="A580-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_59F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10060" deadCode="false" name="A590-FETCH-NEXT-IBO">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_60F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10060" deadCode="false" name="A590-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_61F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_156F10060" deadCode="false" name="A605-DECLARE-CURSOR-IBS">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_156F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_157F10060" deadCode="false" name="A605-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_157F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10060" deadCode="false" name="A610-SELECT-IBS">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_62F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10060" deadCode="false" name="A610-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_63F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10060" deadCode="false" name="A660-OPEN-CURSOR-IBS">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_64F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10060" deadCode="false" name="A660-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_65F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10060" deadCode="false" name="A670-CLOSE-CURSOR-IBS">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_66F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10060" deadCode="false" name="A670-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_67F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10060" deadCode="false" name="A680-FETCH-FIRST-IBS">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_68F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10060" deadCode="false" name="A680-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_69F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10060" deadCode="false" name="A690-FETCH-NEXT-IBS">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_70F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10060" deadCode="false" name="A690-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_71F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_158F10060" deadCode="false" name="A705-DECLARE-CURSOR-IDO">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_158F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_159F10060" deadCode="false" name="A705-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_159F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10060" deadCode="false" name="A710-SELECT-IDO">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_72F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10060" deadCode="false" name="A710-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_73F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10060" deadCode="false" name="A760-OPEN-CURSOR-IDO">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_74F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10060" deadCode="false" name="A760-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_75F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10060" deadCode="false" name="A770-CLOSE-CURSOR-IDO">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_76F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10060" deadCode="false" name="A770-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_77F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_78F10060" deadCode="false" name="A780-FETCH-FIRST-IDO">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_78F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_79F10060" deadCode="false" name="A780-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_79F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_80F10060" deadCode="false" name="A790-FETCH-NEXT-IDO">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_80F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_81F10060" deadCode="false" name="A790-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_81F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_82F10060" deadCode="false" name="B310-SELECT-ID-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_82F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_83F10060" deadCode="false" name="B310-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_83F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_160F10060" deadCode="false" name="B405-DECLARE-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_160F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_161F10060" deadCode="false" name="B405-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_161F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_84F10060" deadCode="false" name="B410-SELECT-IDP-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_84F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_85F10060" deadCode="false" name="B410-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_85F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_86F10060" deadCode="false" name="B460-OPEN-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_86F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_87F10060" deadCode="false" name="B460-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_87F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_88F10060" deadCode="false" name="B470-CLOSE-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_88F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_89F10060" deadCode="false" name="B470-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_89F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_90F10060" deadCode="false" name="B480-FETCH-FIRST-IDP-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_90F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_91F10060" deadCode="false" name="B480-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_91F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_92F10060" deadCode="false" name="B490-FETCH-NEXT-IDP-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_92F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_93F10060" deadCode="false" name="B490-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_93F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_162F10060" deadCode="false" name="B505-DECLARE-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_162F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_163F10060" deadCode="false" name="B505-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_163F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_94F10060" deadCode="false" name="B510-SELECT-IBO-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_94F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_95F10060" deadCode="false" name="B510-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_95F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_96F10060" deadCode="false" name="B560-OPEN-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_96F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_97F10060" deadCode="false" name="B560-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_97F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_98F10060" deadCode="false" name="B570-CLOSE-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_98F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_99F10060" deadCode="false" name="B570-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_99F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_100F10060" deadCode="false" name="B580-FETCH-FIRST-IBO-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_100F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_101F10060" deadCode="false" name="B580-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_101F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_102F10060" deadCode="false" name="B590-FETCH-NEXT-IBO-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_102F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_103F10060" deadCode="false" name="B590-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_103F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_164F10060" deadCode="false" name="B605-DECLARE-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_164F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_165F10060" deadCode="false" name="B605-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_165F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_104F10060" deadCode="false" name="B610-SELECT-IBS-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_104F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_105F10060" deadCode="false" name="B610-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_105F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_106F10060" deadCode="false" name="B660-OPEN-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_106F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_107F10060" deadCode="false" name="B660-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_107F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_108F10060" deadCode="false" name="B670-CLOSE-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_108F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_109F10060" deadCode="false" name="B670-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_109F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_110F10060" deadCode="false" name="B680-FETCH-FIRST-IBS-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_110F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_111F10060" deadCode="false" name="B680-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_111F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_112F10060" deadCode="false" name="B690-FETCH-NEXT-IBS-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_112F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_113F10060" deadCode="false" name="B690-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_113F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_166F10060" deadCode="false" name="B705-DECLARE-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_166F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_167F10060" deadCode="false" name="B705-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_167F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_114F10060" deadCode="false" name="B710-SELECT-IDO-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_114F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_115F10060" deadCode="false" name="B710-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_115F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_116F10060" deadCode="false" name="B760-OPEN-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_116F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_117F10060" deadCode="false" name="B760-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_117F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_118F10060" deadCode="false" name="B770-CLOSE-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_118F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_119F10060" deadCode="false" name="B770-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_119F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_120F10060" deadCode="false" name="B780-FETCH-FIRST-IDO-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_120F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_121F10060" deadCode="false" name="B780-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_121F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_122F10060" deadCode="false" name="B790-FETCH-NEXT-IDO-CPZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_122F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_123F10060" deadCode="false" name="B790-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_123F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_126F10060" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_126F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_127F10060" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_127F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_132F10060" deadCode="false" name="Z150-VALORIZZA-DATA-SERVICES-I">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_132F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_133F10060" deadCode="false" name="Z150-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_133F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_138F10060" deadCode="false" name="Z160-VALORIZZA-DATA-SERVICES-U">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_138F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_139F10060" deadCode="false" name="Z160-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_139F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_134F10060" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_134F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_135F10060" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_135F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_130F10060" deadCode="false" name="Z400-SEQ-RIGA">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_130F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_131F10060" deadCode="false" name="Z400-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_131F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10060" deadCode="false" name="Z500-AGGIORNAMENTO-STORICO">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_40F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10060" deadCode="false" name="Z500-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_41F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_168F10060" deadCode="false" name="Z600-INSERT-NUOVA-RIGA-STORICA">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_168F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_169F10060" deadCode="false" name="Z600-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_169F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_136F10060" deadCode="false" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_136F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_137F10060" deadCode="false" name="Z900-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_137F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_128F10060" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_128F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_129F10060" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_129F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_124F10060" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_124F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_125F10060" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_125F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10060" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_26F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10060" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_27F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_174F10060" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_174F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_175F10060" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_175F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_176F10060" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_176F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_177F10060" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_177F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_170F10060" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_170F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_171F10060" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_171F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_178F10060" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_178F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_179F10060" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_179F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_172F10060" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_172F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_173F10060" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_173F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_180F10060" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_180F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_181F10060" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_181F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_182F10060" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_182F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_183F10060" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_183F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_184F10060" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_184F10060"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_185F10060" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/IDBSODE0.cbl.cobModel#P_185F10060"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_OGG_DEROGA" name="OGG_DEROGA">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_OGG_DEROGA"/>
		</children>
	</packageNode>
	<edges id="SC_1F10060P_1F10060" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10060" targetNode="P_1F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10060_I" deadCode="false" sourceNode="P_1F10060" targetNode="P_2F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_1F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10060_O" deadCode="false" sourceNode="P_1F10060" targetNode="P_3F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_1F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10060_I" deadCode="false" sourceNode="P_1F10060" targetNode="P_4F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_5F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10060_O" deadCode="false" sourceNode="P_1F10060" targetNode="P_5F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_5F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10060_I" deadCode="false" sourceNode="P_1F10060" targetNode="P_6F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_6F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10060_O" deadCode="false" sourceNode="P_1F10060" targetNode="P_7F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_6F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10060_I" deadCode="false" sourceNode="P_1F10060" targetNode="P_8F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_7F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10060_O" deadCode="false" sourceNode="P_1F10060" targetNode="P_9F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_7F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10060_I" deadCode="false" sourceNode="P_1F10060" targetNode="P_10F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_8F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10060_O" deadCode="false" sourceNode="P_1F10060" targetNode="P_11F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_8F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10060_I" deadCode="false" sourceNode="P_1F10060" targetNode="P_12F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_9F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10060_O" deadCode="false" sourceNode="P_1F10060" targetNode="P_13F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_9F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10060_I" deadCode="false" sourceNode="P_1F10060" targetNode="P_14F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_13F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10060_O" deadCode="false" sourceNode="P_1F10060" targetNode="P_15F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_13F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10060_I" deadCode="false" sourceNode="P_1F10060" targetNode="P_16F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_14F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10060_O" deadCode="false" sourceNode="P_1F10060" targetNode="P_17F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_14F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10060_I" deadCode="false" sourceNode="P_1F10060" targetNode="P_18F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_15F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10060_O" deadCode="false" sourceNode="P_1F10060" targetNode="P_19F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_15F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10060_I" deadCode="false" sourceNode="P_1F10060" targetNode="P_20F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_16F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10060_O" deadCode="false" sourceNode="P_1F10060" targetNode="P_21F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_16F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10060_I" deadCode="false" sourceNode="P_1F10060" targetNode="P_22F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_17F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10060_O" deadCode="false" sourceNode="P_1F10060" targetNode="P_23F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_17F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10060_I" deadCode="false" sourceNode="P_1F10060" targetNode="P_24F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_21F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10060_O" deadCode="false" sourceNode="P_1F10060" targetNode="P_25F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_21F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10060_I" deadCode="false" sourceNode="P_1F10060" targetNode="P_8F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_22F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10060_O" deadCode="false" sourceNode="P_1F10060" targetNode="P_9F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_22F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10060_I" deadCode="false" sourceNode="P_1F10060" targetNode="P_10F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_23F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10060_O" deadCode="false" sourceNode="P_1F10060" targetNode="P_11F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_23F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10060_I" deadCode="false" sourceNode="P_1F10060" targetNode="P_12F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_24F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10060_O" deadCode="false" sourceNode="P_1F10060" targetNode="P_13F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_24F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10060_I" deadCode="false" sourceNode="P_2F10060" targetNode="P_26F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_33F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10060_O" deadCode="false" sourceNode="P_2F10060" targetNode="P_27F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_33F10060"/>
	</edges>
	<edges id="P_2F10060P_3F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10060" targetNode="P_3F10060"/>
	<edges id="P_28F10060P_29F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10060" targetNode="P_29F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10060_I" deadCode="false" sourceNode="P_24F10060" targetNode="P_30F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_46F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10060_O" deadCode="false" sourceNode="P_24F10060" targetNode="P_31F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_46F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10060_I" deadCode="false" sourceNode="P_24F10060" targetNode="P_32F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_47F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10060_O" deadCode="false" sourceNode="P_24F10060" targetNode="P_33F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_47F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10060_I" deadCode="false" sourceNode="P_24F10060" targetNode="P_34F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_48F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10060_O" deadCode="false" sourceNode="P_24F10060" targetNode="P_35F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_48F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10060_I" deadCode="false" sourceNode="P_24F10060" targetNode="P_36F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_49F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10060_O" deadCode="false" sourceNode="P_24F10060" targetNode="P_37F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_49F10060"/>
	</edges>
	<edges id="P_24F10060P_25F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10060" targetNode="P_25F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10060_I" deadCode="false" sourceNode="P_4F10060" targetNode="P_38F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_53F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10060_O" deadCode="false" sourceNode="P_4F10060" targetNode="P_39F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_53F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10060_I" deadCode="false" sourceNode="P_4F10060" targetNode="P_40F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_54F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10060_O" deadCode="false" sourceNode="P_4F10060" targetNode="P_41F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_54F10060"/>
	</edges>
	<edges id="P_4F10060P_5F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10060" targetNode="P_5F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10060_I" deadCode="false" sourceNode="P_6F10060" targetNode="P_42F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_58F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10060_O" deadCode="false" sourceNode="P_6F10060" targetNode="P_43F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_58F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10060_I" deadCode="false" sourceNode="P_6F10060" targetNode="P_44F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_59F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10060_O" deadCode="false" sourceNode="P_6F10060" targetNode="P_45F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_59F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10060_I" deadCode="false" sourceNode="P_6F10060" targetNode="P_46F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_60F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10060_O" deadCode="false" sourceNode="P_6F10060" targetNode="P_47F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_60F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10060_I" deadCode="false" sourceNode="P_6F10060" targetNode="P_48F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_61F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10060_O" deadCode="false" sourceNode="P_6F10060" targetNode="P_49F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_61F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10060_I" deadCode="false" sourceNode="P_6F10060" targetNode="P_50F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_62F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10060_O" deadCode="false" sourceNode="P_6F10060" targetNode="P_51F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_62F10060"/>
	</edges>
	<edges id="P_6F10060P_7F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10060" targetNode="P_7F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10060_I" deadCode="false" sourceNode="P_8F10060" targetNode="P_52F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_66F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10060_O" deadCode="false" sourceNode="P_8F10060" targetNode="P_53F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_66F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10060_I" deadCode="false" sourceNode="P_8F10060" targetNode="P_54F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_67F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10060_O" deadCode="false" sourceNode="P_8F10060" targetNode="P_55F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_67F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10060_I" deadCode="false" sourceNode="P_8F10060" targetNode="P_56F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_68F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10060_O" deadCode="false" sourceNode="P_8F10060" targetNode="P_57F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_68F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10060_I" deadCode="false" sourceNode="P_8F10060" targetNode="P_58F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_69F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10060_O" deadCode="false" sourceNode="P_8F10060" targetNode="P_59F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_69F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10060_I" deadCode="false" sourceNode="P_8F10060" targetNode="P_60F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_70F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10060_O" deadCode="false" sourceNode="P_8F10060" targetNode="P_61F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_70F10060"/>
	</edges>
	<edges id="P_8F10060P_9F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10060" targetNode="P_9F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10060_I" deadCode="false" sourceNode="P_10F10060" targetNode="P_62F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_74F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10060_O" deadCode="false" sourceNode="P_10F10060" targetNode="P_63F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_74F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10060_I" deadCode="false" sourceNode="P_10F10060" targetNode="P_64F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_75F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10060_O" deadCode="false" sourceNode="P_10F10060" targetNode="P_65F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_75F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10060_I" deadCode="false" sourceNode="P_10F10060" targetNode="P_66F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_76F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10060_O" deadCode="false" sourceNode="P_10F10060" targetNode="P_67F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_76F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10060_I" deadCode="false" sourceNode="P_10F10060" targetNode="P_68F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_77F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10060_O" deadCode="false" sourceNode="P_10F10060" targetNode="P_69F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_77F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10060_I" deadCode="false" sourceNode="P_10F10060" targetNode="P_70F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_78F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10060_O" deadCode="false" sourceNode="P_10F10060" targetNode="P_71F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_78F10060"/>
	</edges>
	<edges id="P_10F10060P_11F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10060" targetNode="P_11F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10060_I" deadCode="false" sourceNode="P_12F10060" targetNode="P_72F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_82F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10060_O" deadCode="false" sourceNode="P_12F10060" targetNode="P_73F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_82F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10060_I" deadCode="false" sourceNode="P_12F10060" targetNode="P_74F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_83F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10060_O" deadCode="false" sourceNode="P_12F10060" targetNode="P_75F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_83F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10060_I" deadCode="false" sourceNode="P_12F10060" targetNode="P_76F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_84F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10060_O" deadCode="false" sourceNode="P_12F10060" targetNode="P_77F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_84F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10060_I" deadCode="false" sourceNode="P_12F10060" targetNode="P_78F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_85F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10060_O" deadCode="false" sourceNode="P_12F10060" targetNode="P_79F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_85F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10060_I" deadCode="false" sourceNode="P_12F10060" targetNode="P_80F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_86F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10060_O" deadCode="false" sourceNode="P_12F10060" targetNode="P_81F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_86F10060"/>
	</edges>
	<edges id="P_12F10060P_13F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10060" targetNode="P_13F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10060_I" deadCode="false" sourceNode="P_14F10060" targetNode="P_82F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_90F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10060_O" deadCode="false" sourceNode="P_14F10060" targetNode="P_83F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_90F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10060_I" deadCode="false" sourceNode="P_14F10060" targetNode="P_40F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_91F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10060_O" deadCode="false" sourceNode="P_14F10060" targetNode="P_41F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_91F10060"/>
	</edges>
	<edges id="P_14F10060P_15F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10060" targetNode="P_15F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10060_I" deadCode="false" sourceNode="P_16F10060" targetNode="P_84F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_95F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10060_O" deadCode="false" sourceNode="P_16F10060" targetNode="P_85F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_95F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10060_I" deadCode="false" sourceNode="P_16F10060" targetNode="P_86F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_96F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10060_O" deadCode="false" sourceNode="P_16F10060" targetNode="P_87F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_96F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10060_I" deadCode="false" sourceNode="P_16F10060" targetNode="P_88F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_97F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10060_O" deadCode="false" sourceNode="P_16F10060" targetNode="P_89F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_97F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10060_I" deadCode="false" sourceNode="P_16F10060" targetNode="P_90F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_98F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10060_O" deadCode="false" sourceNode="P_16F10060" targetNode="P_91F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_98F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10060_I" deadCode="false" sourceNode="P_16F10060" targetNode="P_92F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_99F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10060_O" deadCode="false" sourceNode="P_16F10060" targetNode="P_93F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_99F10060"/>
	</edges>
	<edges id="P_16F10060P_17F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10060" targetNode="P_17F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10060_I" deadCode="false" sourceNode="P_18F10060" targetNode="P_94F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_103F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10060_O" deadCode="false" sourceNode="P_18F10060" targetNode="P_95F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_103F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10060_I" deadCode="false" sourceNode="P_18F10060" targetNode="P_96F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_104F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10060_O" deadCode="false" sourceNode="P_18F10060" targetNode="P_97F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_104F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10060_I" deadCode="false" sourceNode="P_18F10060" targetNode="P_98F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_105F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10060_O" deadCode="false" sourceNode="P_18F10060" targetNode="P_99F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_105F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10060_I" deadCode="false" sourceNode="P_18F10060" targetNode="P_100F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_106F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10060_O" deadCode="false" sourceNode="P_18F10060" targetNode="P_101F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_106F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10060_I" deadCode="false" sourceNode="P_18F10060" targetNode="P_102F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_107F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10060_O" deadCode="false" sourceNode="P_18F10060" targetNode="P_103F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_107F10060"/>
	</edges>
	<edges id="P_18F10060P_19F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10060" targetNode="P_19F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10060_I" deadCode="false" sourceNode="P_20F10060" targetNode="P_104F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_111F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10060_O" deadCode="false" sourceNode="P_20F10060" targetNode="P_105F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_111F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10060_I" deadCode="false" sourceNode="P_20F10060" targetNode="P_106F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_112F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10060_O" deadCode="false" sourceNode="P_20F10060" targetNode="P_107F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_112F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10060_I" deadCode="false" sourceNode="P_20F10060" targetNode="P_108F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_113F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10060_O" deadCode="false" sourceNode="P_20F10060" targetNode="P_109F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_113F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10060_I" deadCode="false" sourceNode="P_20F10060" targetNode="P_110F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_114F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10060_O" deadCode="false" sourceNode="P_20F10060" targetNode="P_111F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_114F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10060_I" deadCode="false" sourceNode="P_20F10060" targetNode="P_112F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_115F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10060_O" deadCode="false" sourceNode="P_20F10060" targetNode="P_113F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_115F10060"/>
	</edges>
	<edges id="P_20F10060P_21F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10060" targetNode="P_21F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10060_I" deadCode="false" sourceNode="P_22F10060" targetNode="P_114F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_119F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10060_O" deadCode="false" sourceNode="P_22F10060" targetNode="P_115F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_119F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10060_I" deadCode="false" sourceNode="P_22F10060" targetNode="P_116F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_120F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10060_O" deadCode="false" sourceNode="P_22F10060" targetNode="P_117F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_120F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10060_I" deadCode="false" sourceNode="P_22F10060" targetNode="P_118F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_121F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10060_O" deadCode="false" sourceNode="P_22F10060" targetNode="P_119F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_121F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10060_I" deadCode="false" sourceNode="P_22F10060" targetNode="P_120F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_122F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10060_O" deadCode="false" sourceNode="P_22F10060" targetNode="P_121F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_122F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10060_I" deadCode="false" sourceNode="P_22F10060" targetNode="P_122F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_123F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10060_O" deadCode="false" sourceNode="P_22F10060" targetNode="P_123F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_123F10060"/>
	</edges>
	<edges id="P_22F10060P_23F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10060" targetNode="P_23F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10060_I" deadCode="false" sourceNode="P_30F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_126F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10060_O" deadCode="false" sourceNode="P_30F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_126F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10060_I" deadCode="false" sourceNode="P_30F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_128F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10060_O" deadCode="false" sourceNode="P_30F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_128F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10060_I" deadCode="false" sourceNode="P_30F10060" targetNode="P_126F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_130F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10060_O" deadCode="false" sourceNode="P_30F10060" targetNode="P_127F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_130F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10060_I" deadCode="false" sourceNode="P_30F10060" targetNode="P_128F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_131F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10060_O" deadCode="false" sourceNode="P_30F10060" targetNode="P_129F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_131F10060"/>
	</edges>
	<edges id="P_30F10060P_31F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10060" targetNode="P_31F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10060_I" deadCode="false" sourceNode="P_32F10060" targetNode="P_130F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_133F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10060_O" deadCode="false" sourceNode="P_32F10060" targetNode="P_131F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_133F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10060_I" deadCode="false" sourceNode="P_32F10060" targetNode="P_132F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_135F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10060_O" deadCode="false" sourceNode="P_32F10060" targetNode="P_133F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_135F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10060_I" deadCode="false" sourceNode="P_32F10060" targetNode="P_134F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_136F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10060_O" deadCode="false" sourceNode="P_32F10060" targetNode="P_135F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_136F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10060_I" deadCode="false" sourceNode="P_32F10060" targetNode="P_136F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_137F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10060_O" deadCode="false" sourceNode="P_32F10060" targetNode="P_137F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_137F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10060_I" deadCode="false" sourceNode="P_32F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_138F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10060_O" deadCode="false" sourceNode="P_32F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_138F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10060_I" deadCode="false" sourceNode="P_32F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_140F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10060_O" deadCode="false" sourceNode="P_32F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_140F10060"/>
	</edges>
	<edges id="P_32F10060P_33F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10060" targetNode="P_33F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10060_I" deadCode="false" sourceNode="P_34F10060" targetNode="P_138F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_142F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10060_O" deadCode="false" sourceNode="P_34F10060" targetNode="P_139F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_142F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10060_I" deadCode="false" sourceNode="P_34F10060" targetNode="P_134F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_143F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10060_O" deadCode="false" sourceNode="P_34F10060" targetNode="P_135F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_143F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10060_I" deadCode="false" sourceNode="P_34F10060" targetNode="P_136F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_144F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10060_O" deadCode="false" sourceNode="P_34F10060" targetNode="P_137F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_144F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10060_I" deadCode="false" sourceNode="P_34F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_145F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10060_O" deadCode="false" sourceNode="P_34F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_145F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10060_I" deadCode="false" sourceNode="P_34F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_147F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10060_O" deadCode="false" sourceNode="P_34F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_147F10060"/>
	</edges>
	<edges id="P_34F10060P_35F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10060" targetNode="P_35F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10060_I" deadCode="false" sourceNode="P_36F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_150F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10060_O" deadCode="false" sourceNode="P_36F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_150F10060"/>
	</edges>
	<edges id="P_36F10060P_37F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10060" targetNode="P_37F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10060_I" deadCode="false" sourceNode="P_140F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_152F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10060_O" deadCode="false" sourceNode="P_140F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_152F10060"/>
	</edges>
	<edges id="P_140F10060P_141F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_140F10060" targetNode="P_141F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_156F10060_I" deadCode="false" sourceNode="P_38F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_156F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_156F10060_O" deadCode="false" sourceNode="P_38F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_156F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10060_I" deadCode="false" sourceNode="P_38F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_158F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10060_O" deadCode="false" sourceNode="P_38F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_158F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10060_I" deadCode="false" sourceNode="P_38F10060" targetNode="P_126F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_160F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10060_O" deadCode="false" sourceNode="P_38F10060" targetNode="P_127F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_160F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10060_I" deadCode="false" sourceNode="P_38F10060" targetNode="P_128F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_161F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10060_O" deadCode="false" sourceNode="P_38F10060" targetNode="P_129F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_161F10060"/>
	</edges>
	<edges id="P_38F10060P_39F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10060" targetNode="P_39F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10060_I" deadCode="false" sourceNode="P_142F10060" targetNode="P_138F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_163F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10060_O" deadCode="false" sourceNode="P_142F10060" targetNode="P_139F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_163F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10060_I" deadCode="false" sourceNode="P_142F10060" targetNode="P_134F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_164F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10060_O" deadCode="false" sourceNode="P_142F10060" targetNode="P_135F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_164F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10060_I" deadCode="false" sourceNode="P_142F10060" targetNode="P_136F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_165F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10060_O" deadCode="false" sourceNode="P_142F10060" targetNode="P_137F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_165F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10060_I" deadCode="false" sourceNode="P_142F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_166F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10060_O" deadCode="false" sourceNode="P_142F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_166F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10060_I" deadCode="false" sourceNode="P_142F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_168F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10060_O" deadCode="false" sourceNode="P_142F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_168F10060"/>
	</edges>
	<edges id="P_142F10060P_143F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_142F10060" targetNode="P_143F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10060_I" deadCode="false" sourceNode="P_144F10060" targetNode="P_140F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_170F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10060_O" deadCode="false" sourceNode="P_144F10060" targetNode="P_141F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_170F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10060_I" deadCode="false" sourceNode="P_144F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_172F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10060_O" deadCode="false" sourceNode="P_144F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_172F10060"/>
	</edges>
	<edges id="P_144F10060P_145F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_144F10060" targetNode="P_145F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10060_I" deadCode="false" sourceNode="P_146F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_175F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10060_O" deadCode="false" sourceNode="P_146F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_175F10060"/>
	</edges>
	<edges id="P_146F10060P_147F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_146F10060" targetNode="P_147F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10060_I" deadCode="true" sourceNode="P_148F10060" targetNode="P_144F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_177F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10060_O" deadCode="true" sourceNode="P_148F10060" targetNode="P_145F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_177F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10060_I" deadCode="true" sourceNode="P_148F10060" targetNode="P_149F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_179F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10060_O" deadCode="true" sourceNode="P_148F10060" targetNode="P_150F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_179F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_182F10060_I" deadCode="false" sourceNode="P_149F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_182F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_182F10060_O" deadCode="false" sourceNode="P_149F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_182F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10060_I" deadCode="false" sourceNode="P_149F10060" targetNode="P_126F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_184F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10060_O" deadCode="false" sourceNode="P_149F10060" targetNode="P_127F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_184F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10060_I" deadCode="false" sourceNode="P_149F10060" targetNode="P_128F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_185F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10060_O" deadCode="false" sourceNode="P_149F10060" targetNode="P_129F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_185F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10060_I" deadCode="false" sourceNode="P_149F10060" targetNode="P_146F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_187F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10060_O" deadCode="false" sourceNode="P_149F10060" targetNode="P_147F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_187F10060"/>
	</edges>
	<edges id="P_149F10060P_150F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_149F10060" targetNode="P_150F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10060_I" deadCode="false" sourceNode="P_152F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_191F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10060_O" deadCode="false" sourceNode="P_152F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_191F10060"/>
	</edges>
	<edges id="P_152F10060P_153F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_152F10060" targetNode="P_153F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_194F10060_I" deadCode="false" sourceNode="P_42F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_194F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_194F10060_O" deadCode="false" sourceNode="P_42F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_194F10060"/>
	</edges>
	<edges id="P_42F10060P_43F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10060" targetNode="P_43F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_197F10060_I" deadCode="false" sourceNode="P_44F10060" targetNode="P_152F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_197F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_197F10060_O" deadCode="false" sourceNode="P_44F10060" targetNode="P_153F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_197F10060"/>
	</edges>
	<edges id="P_44F10060P_45F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10060" targetNode="P_45F10060"/>
	<edges id="P_46F10060P_47F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10060" targetNode="P_47F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10060_I" deadCode="false" sourceNode="P_48F10060" targetNode="P_44F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_202F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10060_O" deadCode="false" sourceNode="P_48F10060" targetNode="P_45F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_202F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_204F10060_I" deadCode="false" sourceNode="P_48F10060" targetNode="P_50F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_204F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_204F10060_O" deadCode="false" sourceNode="P_48F10060" targetNode="P_51F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_204F10060"/>
	</edges>
	<edges id="P_48F10060P_49F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10060" targetNode="P_49F10060"/>
	<edges id="P_50F10060P_51F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10060" targetNode="P_51F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_208F10060_I" deadCode="false" sourceNode="P_154F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_208F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_208F10060_O" deadCode="false" sourceNode="P_154F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_208F10060"/>
	</edges>
	<edges id="P_154F10060P_155F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_154F10060" targetNode="P_155F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_212F10060_I" deadCode="false" sourceNode="P_52F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_212F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_212F10060_O" deadCode="false" sourceNode="P_52F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_212F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_214F10060_I" deadCode="false" sourceNode="P_52F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_214F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_214F10060_O" deadCode="false" sourceNode="P_52F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_214F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10060_I" deadCode="false" sourceNode="P_52F10060" targetNode="P_126F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_216F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10060_O" deadCode="false" sourceNode="P_52F10060" targetNode="P_127F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_216F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_217F10060_I" deadCode="false" sourceNode="P_52F10060" targetNode="P_128F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_217F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_217F10060_O" deadCode="false" sourceNode="P_52F10060" targetNode="P_129F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_217F10060"/>
	</edges>
	<edges id="P_52F10060P_53F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10060" targetNode="P_53F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_219F10060_I" deadCode="false" sourceNode="P_54F10060" targetNode="P_154F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_219F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_219F10060_O" deadCode="false" sourceNode="P_54F10060" targetNode="P_155F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_219F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10060_I" deadCode="false" sourceNode="P_54F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_221F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10060_O" deadCode="false" sourceNode="P_54F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_221F10060"/>
	</edges>
	<edges id="P_54F10060P_55F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10060" targetNode="P_55F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_224F10060_I" deadCode="false" sourceNode="P_56F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_224F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_224F10060_O" deadCode="false" sourceNode="P_56F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_224F10060"/>
	</edges>
	<edges id="P_56F10060P_57F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10060" targetNode="P_57F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_226F10060_I" deadCode="false" sourceNode="P_58F10060" targetNode="P_54F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_226F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_226F10060_O" deadCode="false" sourceNode="P_58F10060" targetNode="P_55F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_226F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_228F10060_I" deadCode="false" sourceNode="P_58F10060" targetNode="P_60F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_228F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_228F10060_O" deadCode="false" sourceNode="P_58F10060" targetNode="P_61F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_228F10060"/>
	</edges>
	<edges id="P_58F10060P_59F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10060" targetNode="P_59F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10060_I" deadCode="false" sourceNode="P_60F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_231F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10060_O" deadCode="false" sourceNode="P_60F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_231F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_233F10060_I" deadCode="false" sourceNode="P_60F10060" targetNode="P_126F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_233F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_233F10060_O" deadCode="false" sourceNode="P_60F10060" targetNode="P_127F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_233F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_234F10060_I" deadCode="false" sourceNode="P_60F10060" targetNode="P_128F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_234F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_234F10060_O" deadCode="false" sourceNode="P_60F10060" targetNode="P_129F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_234F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_236F10060_I" deadCode="false" sourceNode="P_60F10060" targetNode="P_56F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_236F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_236F10060_O" deadCode="false" sourceNode="P_60F10060" targetNode="P_57F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_236F10060"/>
	</edges>
	<edges id="P_60F10060P_61F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_60F10060" targetNode="P_61F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_240F10060_I" deadCode="false" sourceNode="P_156F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_240F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_240F10060_O" deadCode="false" sourceNode="P_156F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_240F10060"/>
	</edges>
	<edges id="P_156F10060P_157F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_156F10060" targetNode="P_157F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_243F10060_I" deadCode="false" sourceNode="P_62F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_243F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_243F10060_O" deadCode="false" sourceNode="P_62F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_243F10060"/>
	</edges>
	<edges id="P_62F10060P_63F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10060" targetNode="P_63F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_246F10060_I" deadCode="false" sourceNode="P_64F10060" targetNode="P_156F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_246F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_246F10060_O" deadCode="false" sourceNode="P_64F10060" targetNode="P_157F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_246F10060"/>
	</edges>
	<edges id="P_64F10060P_65F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10060" targetNode="P_65F10060"/>
	<edges id="P_66F10060P_67F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10060" targetNode="P_67F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_251F10060_I" deadCode="false" sourceNode="P_68F10060" targetNode="P_64F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_251F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_251F10060_O" deadCode="false" sourceNode="P_68F10060" targetNode="P_65F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_251F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10060_I" deadCode="false" sourceNode="P_68F10060" targetNode="P_70F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_253F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10060_O" deadCode="false" sourceNode="P_68F10060" targetNode="P_71F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_253F10060"/>
	</edges>
	<edges id="P_68F10060P_69F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10060" targetNode="P_69F10060"/>
	<edges id="P_70F10060P_71F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_70F10060" targetNode="P_71F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_257F10060_I" deadCode="false" sourceNode="P_158F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_257F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_257F10060_O" deadCode="false" sourceNode="P_158F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_257F10060"/>
	</edges>
	<edges id="P_158F10060P_159F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_158F10060" targetNode="P_159F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_261F10060_I" deadCode="false" sourceNode="P_72F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_261F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_261F10060_O" deadCode="false" sourceNode="P_72F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_261F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_263F10060_I" deadCode="false" sourceNode="P_72F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_263F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_263F10060_O" deadCode="false" sourceNode="P_72F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_263F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10060_I" deadCode="false" sourceNode="P_72F10060" targetNode="P_126F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_265F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10060_O" deadCode="false" sourceNode="P_72F10060" targetNode="P_127F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_265F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_266F10060_I" deadCode="false" sourceNode="P_72F10060" targetNode="P_128F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_266F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_266F10060_O" deadCode="false" sourceNode="P_72F10060" targetNode="P_129F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_266F10060"/>
	</edges>
	<edges id="P_72F10060P_73F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10060" targetNode="P_73F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_268F10060_I" deadCode="false" sourceNode="P_74F10060" targetNode="P_158F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_268F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_268F10060_O" deadCode="false" sourceNode="P_74F10060" targetNode="P_159F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_268F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_270F10060_I" deadCode="false" sourceNode="P_74F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_270F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_270F10060_O" deadCode="false" sourceNode="P_74F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_270F10060"/>
	</edges>
	<edges id="P_74F10060P_75F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10060" targetNode="P_75F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_273F10060_I" deadCode="false" sourceNode="P_76F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_273F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_273F10060_O" deadCode="false" sourceNode="P_76F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_273F10060"/>
	</edges>
	<edges id="P_76F10060P_77F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_76F10060" targetNode="P_77F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_275F10060_I" deadCode="false" sourceNode="P_78F10060" targetNode="P_74F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_275F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_275F10060_O" deadCode="false" sourceNode="P_78F10060" targetNode="P_75F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_275F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_277F10060_I" deadCode="false" sourceNode="P_78F10060" targetNode="P_80F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_277F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_277F10060_O" deadCode="false" sourceNode="P_78F10060" targetNode="P_81F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_277F10060"/>
	</edges>
	<edges id="P_78F10060P_79F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_78F10060" targetNode="P_79F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10060_I" deadCode="false" sourceNode="P_80F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_280F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10060_O" deadCode="false" sourceNode="P_80F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_280F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_282F10060_I" deadCode="false" sourceNode="P_80F10060" targetNode="P_126F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_282F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_282F10060_O" deadCode="false" sourceNode="P_80F10060" targetNode="P_127F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_282F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10060_I" deadCode="false" sourceNode="P_80F10060" targetNode="P_128F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_283F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10060_O" deadCode="false" sourceNode="P_80F10060" targetNode="P_129F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_283F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_285F10060_I" deadCode="false" sourceNode="P_80F10060" targetNode="P_76F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_285F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_285F10060_O" deadCode="false" sourceNode="P_80F10060" targetNode="P_77F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_285F10060"/>
	</edges>
	<edges id="P_80F10060P_81F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_80F10060" targetNode="P_81F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10060_I" deadCode="false" sourceNode="P_82F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_289F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10060_O" deadCode="false" sourceNode="P_82F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_289F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_291F10060_I" deadCode="false" sourceNode="P_82F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_291F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_291F10060_O" deadCode="false" sourceNode="P_82F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_291F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_293F10060_I" deadCode="false" sourceNode="P_82F10060" targetNode="P_126F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_293F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_293F10060_O" deadCode="false" sourceNode="P_82F10060" targetNode="P_127F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_293F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10060_I" deadCode="false" sourceNode="P_82F10060" targetNode="P_128F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_294F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10060_O" deadCode="false" sourceNode="P_82F10060" targetNode="P_129F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_294F10060"/>
	</edges>
	<edges id="P_82F10060P_83F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_82F10060" targetNode="P_83F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_296F10060_I" deadCode="false" sourceNode="P_160F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_296F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_296F10060_O" deadCode="false" sourceNode="P_160F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_296F10060"/>
	</edges>
	<edges id="P_160F10060P_161F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_160F10060" targetNode="P_161F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10060_I" deadCode="false" sourceNode="P_84F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_299F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10060_O" deadCode="false" sourceNode="P_84F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_299F10060"/>
	</edges>
	<edges id="P_84F10060P_85F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_84F10060" targetNode="P_85F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_302F10060_I" deadCode="false" sourceNode="P_86F10060" targetNode="P_160F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_302F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_302F10060_O" deadCode="false" sourceNode="P_86F10060" targetNode="P_161F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_302F10060"/>
	</edges>
	<edges id="P_86F10060P_87F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_86F10060" targetNode="P_87F10060"/>
	<edges id="P_88F10060P_89F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_88F10060" targetNode="P_89F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_307F10060_I" deadCode="false" sourceNode="P_90F10060" targetNode="P_86F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_307F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_307F10060_O" deadCode="false" sourceNode="P_90F10060" targetNode="P_87F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_307F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10060_I" deadCode="false" sourceNode="P_90F10060" targetNode="P_92F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_309F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10060_O" deadCode="false" sourceNode="P_90F10060" targetNode="P_93F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_309F10060"/>
	</edges>
	<edges id="P_90F10060P_91F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_90F10060" targetNode="P_91F10060"/>
	<edges id="P_92F10060P_93F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_92F10060" targetNode="P_93F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_313F10060_I" deadCode="false" sourceNode="P_162F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_313F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_313F10060_O" deadCode="false" sourceNode="P_162F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_313F10060"/>
	</edges>
	<edges id="P_162F10060P_163F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_162F10060" targetNode="P_163F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_317F10060_I" deadCode="false" sourceNode="P_94F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_317F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_317F10060_O" deadCode="false" sourceNode="P_94F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_317F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_319F10060_I" deadCode="false" sourceNode="P_94F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_319F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_319F10060_O" deadCode="false" sourceNode="P_94F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_319F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10060_I" deadCode="false" sourceNode="P_94F10060" targetNode="P_126F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_321F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10060_O" deadCode="false" sourceNode="P_94F10060" targetNode="P_127F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_321F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_322F10060_I" deadCode="false" sourceNode="P_94F10060" targetNode="P_128F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_322F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_322F10060_O" deadCode="false" sourceNode="P_94F10060" targetNode="P_129F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_322F10060"/>
	</edges>
	<edges id="P_94F10060P_95F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_94F10060" targetNode="P_95F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_324F10060_I" deadCode="false" sourceNode="P_96F10060" targetNode="P_162F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_324F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_324F10060_O" deadCode="false" sourceNode="P_96F10060" targetNode="P_163F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_324F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_326F10060_I" deadCode="false" sourceNode="P_96F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_326F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_326F10060_O" deadCode="false" sourceNode="P_96F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_326F10060"/>
	</edges>
	<edges id="P_96F10060P_97F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_96F10060" targetNode="P_97F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_329F10060_I" deadCode="false" sourceNode="P_98F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_329F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_329F10060_O" deadCode="false" sourceNode="P_98F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_329F10060"/>
	</edges>
	<edges id="P_98F10060P_99F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_98F10060" targetNode="P_99F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_331F10060_I" deadCode="false" sourceNode="P_100F10060" targetNode="P_96F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_331F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_331F10060_O" deadCode="false" sourceNode="P_100F10060" targetNode="P_97F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_331F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_333F10060_I" deadCode="false" sourceNode="P_100F10060" targetNode="P_102F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_333F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_333F10060_O" deadCode="false" sourceNode="P_100F10060" targetNode="P_103F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_333F10060"/>
	</edges>
	<edges id="P_100F10060P_101F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_100F10060" targetNode="P_101F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10060_I" deadCode="false" sourceNode="P_102F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_336F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10060_O" deadCode="false" sourceNode="P_102F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_336F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10060_I" deadCode="false" sourceNode="P_102F10060" targetNode="P_126F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_338F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10060_O" deadCode="false" sourceNode="P_102F10060" targetNode="P_127F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_338F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_339F10060_I" deadCode="false" sourceNode="P_102F10060" targetNode="P_128F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_339F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_339F10060_O" deadCode="false" sourceNode="P_102F10060" targetNode="P_129F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_339F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_341F10060_I" deadCode="false" sourceNode="P_102F10060" targetNode="P_98F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_341F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_341F10060_O" deadCode="false" sourceNode="P_102F10060" targetNode="P_99F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_341F10060"/>
	</edges>
	<edges id="P_102F10060P_103F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_102F10060" targetNode="P_103F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_345F10060_I" deadCode="false" sourceNode="P_164F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_345F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_345F10060_O" deadCode="false" sourceNode="P_164F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_345F10060"/>
	</edges>
	<edges id="P_164F10060P_165F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_164F10060" targetNode="P_165F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_348F10060_I" deadCode="false" sourceNode="P_104F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_348F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_348F10060_O" deadCode="false" sourceNode="P_104F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_348F10060"/>
	</edges>
	<edges id="P_104F10060P_105F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_104F10060" targetNode="P_105F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_351F10060_I" deadCode="false" sourceNode="P_106F10060" targetNode="P_164F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_351F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_351F10060_O" deadCode="false" sourceNode="P_106F10060" targetNode="P_165F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_351F10060"/>
	</edges>
	<edges id="P_106F10060P_107F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_106F10060" targetNode="P_107F10060"/>
	<edges id="P_108F10060P_109F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_108F10060" targetNode="P_109F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_356F10060_I" deadCode="false" sourceNode="P_110F10060" targetNode="P_106F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_356F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_356F10060_O" deadCode="false" sourceNode="P_110F10060" targetNode="P_107F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_356F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_358F10060_I" deadCode="false" sourceNode="P_110F10060" targetNode="P_112F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_358F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_358F10060_O" deadCode="false" sourceNode="P_110F10060" targetNode="P_113F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_358F10060"/>
	</edges>
	<edges id="P_110F10060P_111F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_110F10060" targetNode="P_111F10060"/>
	<edges id="P_112F10060P_113F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_112F10060" targetNode="P_113F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_362F10060_I" deadCode="false" sourceNode="P_166F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_362F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_362F10060_O" deadCode="false" sourceNode="P_166F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_362F10060"/>
	</edges>
	<edges id="P_166F10060P_167F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_166F10060" targetNode="P_167F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_366F10060_I" deadCode="false" sourceNode="P_114F10060" targetNode="P_124F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_366F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_366F10060_O" deadCode="false" sourceNode="P_114F10060" targetNode="P_125F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_366F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_368F10060_I" deadCode="false" sourceNode="P_114F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_368F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_368F10060_O" deadCode="false" sourceNode="P_114F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_368F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_370F10060_I" deadCode="false" sourceNode="P_114F10060" targetNode="P_126F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_370F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_370F10060_O" deadCode="false" sourceNode="P_114F10060" targetNode="P_127F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_370F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_371F10060_I" deadCode="false" sourceNode="P_114F10060" targetNode="P_128F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_371F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_371F10060_O" deadCode="false" sourceNode="P_114F10060" targetNode="P_129F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_371F10060"/>
	</edges>
	<edges id="P_114F10060P_115F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_114F10060" targetNode="P_115F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_373F10060_I" deadCode="false" sourceNode="P_116F10060" targetNode="P_166F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_373F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_373F10060_O" deadCode="false" sourceNode="P_116F10060" targetNode="P_167F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_373F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_375F10060_I" deadCode="false" sourceNode="P_116F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_375F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_375F10060_O" deadCode="false" sourceNode="P_116F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_375F10060"/>
	</edges>
	<edges id="P_116F10060P_117F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_116F10060" targetNode="P_117F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_378F10060_I" deadCode="false" sourceNode="P_118F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_378F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_378F10060_O" deadCode="false" sourceNode="P_118F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_378F10060"/>
	</edges>
	<edges id="P_118F10060P_119F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_118F10060" targetNode="P_119F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_380F10060_I" deadCode="false" sourceNode="P_120F10060" targetNode="P_116F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_380F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_380F10060_O" deadCode="false" sourceNode="P_120F10060" targetNode="P_117F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_380F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_382F10060_I" deadCode="false" sourceNode="P_120F10060" targetNode="P_122F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_382F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_382F10060_O" deadCode="false" sourceNode="P_120F10060" targetNode="P_123F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_382F10060"/>
	</edges>
	<edges id="P_120F10060P_121F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_120F10060" targetNode="P_121F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_385F10060_I" deadCode="false" sourceNode="P_122F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_385F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_385F10060_O" deadCode="false" sourceNode="P_122F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_385F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_387F10060_I" deadCode="false" sourceNode="P_122F10060" targetNode="P_126F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_387F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_387F10060_O" deadCode="false" sourceNode="P_122F10060" targetNode="P_127F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_387F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_388F10060_I" deadCode="false" sourceNode="P_122F10060" targetNode="P_128F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_388F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_388F10060_O" deadCode="false" sourceNode="P_122F10060" targetNode="P_129F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_388F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_390F10060_I" deadCode="false" sourceNode="P_122F10060" targetNode="P_118F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_390F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_390F10060_O" deadCode="false" sourceNode="P_122F10060" targetNode="P_119F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_390F10060"/>
	</edges>
	<edges id="P_122F10060P_123F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_122F10060" targetNode="P_123F10060"/>
	<edges id="P_126F10060P_127F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_126F10060" targetNode="P_127F10060"/>
	<edges id="P_132F10060P_133F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_132F10060" targetNode="P_133F10060"/>
	<edges id="P_138F10060P_139F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_138F10060" targetNode="P_139F10060"/>
	<edges id="P_134F10060P_135F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_134F10060" targetNode="P_135F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_421F10060_I" deadCode="false" sourceNode="P_130F10060" targetNode="P_28F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_421F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_421F10060_O" deadCode="false" sourceNode="P_130F10060" targetNode="P_29F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_421F10060"/>
	</edges>
	<edges id="P_130F10060P_131F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_130F10060" targetNode="P_131F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_425F10060_I" deadCode="false" sourceNode="P_40F10060" targetNode="P_144F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_425F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_425F10060_O" deadCode="false" sourceNode="P_40F10060" targetNode="P_145F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_425F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_427F10060_I" deadCode="false" sourceNode="P_40F10060" targetNode="P_149F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_426F10060"/>
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_427F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_427F10060_O" deadCode="false" sourceNode="P_40F10060" targetNode="P_150F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_426F10060"/>
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_427F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_431F10060_I" deadCode="false" sourceNode="P_40F10060" targetNode="P_142F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_426F10060"/>
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_431F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_431F10060_O" deadCode="false" sourceNode="P_40F10060" targetNode="P_143F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_426F10060"/>
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_431F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_439F10060_I" deadCode="false" sourceNode="P_40F10060" targetNode="P_32F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_426F10060"/>
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_439F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_439F10060_O" deadCode="false" sourceNode="P_40F10060" targetNode="P_33F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_426F10060"/>
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_439F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_442F10060_I" deadCode="false" sourceNode="P_40F10060" targetNode="P_168F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_442F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_442F10060_O" deadCode="false" sourceNode="P_40F10060" targetNode="P_169F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_442F10060"/>
	</edges>
	<edges id="P_40F10060P_41F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10060" targetNode="P_41F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_453F10060_I" deadCode="false" sourceNode="P_168F10060" targetNode="P_32F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_453F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_453F10060_O" deadCode="false" sourceNode="P_168F10060" targetNode="P_33F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_453F10060"/>
	</edges>
	<edges id="P_168F10060P_169F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_168F10060" targetNode="P_169F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_456F10060_I" deadCode="false" sourceNode="P_136F10060" targetNode="P_170F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_456F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_456F10060_O" deadCode="false" sourceNode="P_136F10060" targetNode="P_171F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_456F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_459F10060_I" deadCode="false" sourceNode="P_136F10060" targetNode="P_170F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_459F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_459F10060_O" deadCode="false" sourceNode="P_136F10060" targetNode="P_171F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_459F10060"/>
	</edges>
	<edges id="P_136F10060P_137F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_136F10060" targetNode="P_137F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_463F10060_I" deadCode="false" sourceNode="P_128F10060" targetNode="P_172F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_463F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_463F10060_O" deadCode="false" sourceNode="P_128F10060" targetNode="P_173F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_463F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_466F10060_I" deadCode="false" sourceNode="P_128F10060" targetNode="P_172F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_466F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_466F10060_O" deadCode="false" sourceNode="P_128F10060" targetNode="P_173F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_466F10060"/>
	</edges>
	<edges id="P_128F10060P_129F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_128F10060" targetNode="P_129F10060"/>
	<edges id="P_124F10060P_125F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_124F10060" targetNode="P_125F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_471F10060_I" deadCode="false" sourceNode="P_26F10060" targetNode="P_174F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_471F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_471F10060_O" deadCode="false" sourceNode="P_26F10060" targetNode="P_175F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_471F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_473F10060_I" deadCode="false" sourceNode="P_26F10060" targetNode="P_176F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_473F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_473F10060_O" deadCode="false" sourceNode="P_26F10060" targetNode="P_177F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_473F10060"/>
	</edges>
	<edges id="P_26F10060P_27F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10060" targetNode="P_27F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_478F10060_I" deadCode="false" sourceNode="P_174F10060" targetNode="P_170F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_478F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_478F10060_O" deadCode="false" sourceNode="P_174F10060" targetNode="P_171F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_478F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_483F10060_I" deadCode="false" sourceNode="P_174F10060" targetNode="P_170F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_483F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_483F10060_O" deadCode="false" sourceNode="P_174F10060" targetNode="P_171F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_483F10060"/>
	</edges>
	<edges id="P_174F10060P_175F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_174F10060" targetNode="P_175F10060"/>
	<edges id="P_176F10060P_177F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_176F10060" targetNode="P_177F10060"/>
	<edges id="P_170F10060P_171F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_170F10060" targetNode="P_171F10060"/>
	<edges xsi:type="cbl:PerformEdge" id="S_512F10060_I" deadCode="false" sourceNode="P_172F10060" targetNode="P_180F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_512F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_512F10060_O" deadCode="false" sourceNode="P_172F10060" targetNode="P_181F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_512F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_513F10060_I" deadCode="false" sourceNode="P_172F10060" targetNode="P_182F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_513F10060"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_513F10060_O" deadCode="false" sourceNode="P_172F10060" targetNode="P_183F10060">
		<representations href="../../../cobol/IDBSODE0.cbl.cobModel#S_513F10060"/>
	</edges>
	<edges id="P_172F10060P_173F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_172F10060" targetNode="P_173F10060"/>
	<edges id="P_180F10060P_181F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_180F10060" targetNode="P_181F10060"/>
	<edges id="P_182F10060P_183F10060" xsi:type="cbl:FallThroughEdge" sourceNode="P_182F10060" targetNode="P_183F10060"/>
	<edges xsi:type="cbl:DataEdge" id="S_127F10060_POS1" deadCode="false" targetNode="P_30F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_127F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_139F10060_POS1" deadCode="false" sourceNode="P_32F10060" targetNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_139F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_146F10060_POS1" deadCode="false" sourceNode="P_34F10060" targetNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_146F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_149F10060_POS1" deadCode="false" sourceNode="P_36F10060" targetNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_149F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_157F10060_POS1" deadCode="false" targetNode="P_38F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_157F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_167F10060_POS1" deadCode="false" sourceNode="P_142F10060" targetNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_167F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_171F10060_POS1" deadCode="false" targetNode="P_144F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_171F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_174F10060_POS1" deadCode="false" targetNode="P_146F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_174F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_181F10060_POS1" deadCode="false" targetNode="P_149F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_181F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_213F10060_POS1" deadCode="false" targetNode="P_52F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_213F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_220F10060_POS1" deadCode="false" targetNode="P_54F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_220F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_223F10060_POS1" deadCode="false" targetNode="P_56F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_223F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_230F10060_POS1" deadCode="false" targetNode="P_60F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_230F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_262F10060_POS1" deadCode="false" targetNode="P_72F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_262F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_269F10060_POS1" deadCode="false" targetNode="P_74F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_269F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_272F10060_POS1" deadCode="false" targetNode="P_76F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_272F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_279F10060_POS1" deadCode="false" targetNode="P_80F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_279F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_290F10060_POS1" deadCode="false" targetNode="P_82F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_290F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_318F10060_POS1" deadCode="false" targetNode="P_94F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_318F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_325F10060_POS1" deadCode="false" targetNode="P_96F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_325F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_328F10060_POS1" deadCode="false" targetNode="P_98F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_328F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_335F10060_POS1" deadCode="false" targetNode="P_102F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_335F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_367F10060_POS1" deadCode="false" targetNode="P_114F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_367F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_374F10060_POS1" deadCode="false" targetNode="P_116F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_374F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_377F10060_POS1" deadCode="false" targetNode="P_118F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_377F10060"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_384F10060_POS1" deadCode="false" targetNode="P_122F10060" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_384F10060"></representations>
	</edges>
</Package>

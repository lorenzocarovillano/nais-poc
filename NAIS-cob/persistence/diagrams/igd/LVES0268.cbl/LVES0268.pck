<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVES0268" cbl:id="LVES0268" xsi:id="LVES0268" packageRef="LVES0268.igd#LVES0268" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVES0268_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10301" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVES0268.cbl.cobModel#SC_1F10301"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10301" deadCode="false" name="PROGRAM_LVES0268_FIRST_SENTENCES">
				<representations href="../../../cobol/LVES0268.cbl.cobModel#P_1F10301"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10301" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVES0268.cbl.cobModel#P_2F10301"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10301" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVES0268.cbl.cobModel#P_3F10301"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10301" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVES0268.cbl.cobModel#P_4F10301"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10301" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVES0268.cbl.cobModel#P_5F10301"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10301" deadCode="false" name="S1100-CALL-LVES0269">
				<representations href="../../../cobol/LVES0268.cbl.cobModel#P_8F10301"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10301" deadCode="false" name="EX-S1100">
				<representations href="../../../cobol/LVES0268.cbl.cobModel#P_9F10301"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10301" deadCode="false" name="S1101-CALL-LVES0270">
				<representations href="../../../cobol/LVES0268.cbl.cobModel#P_10F10301"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10301" deadCode="false" name="EX-S1101">
				<representations href="../../../cobol/LVES0268.cbl.cobModel#P_11F10301"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10301" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVES0268.cbl.cobModel#P_6F10301"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10301" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVES0268.cbl.cobModel#P_7F10301"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10301" deadCode="false" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LVES0268.cbl.cobModel#P_12F10301"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10301" deadCode="false" name="EX-S0290">
				<representations href="../../../cobol/LVES0268.cbl.cobModel#P_13F10301"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10301" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LVES0268.cbl.cobModel#P_14F10301"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10301" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LVES0268.cbl.cobModel#P_15F10301"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10301" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LVES0268.cbl.cobModel#P_18F10301"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10301" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LVES0268.cbl.cobModel#P_19F10301"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10301" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LVES0268.cbl.cobModel#P_16F10301"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10301" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LVES0268.cbl.cobModel#P_17F10301"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LVES0269" name="LVES0269">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10302"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LVES0270" name="LVES0270">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10303"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10301P_1F10301" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10301" targetNode="P_1F10301"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10301_I" deadCode="false" sourceNode="P_1F10301" targetNode="P_2F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_1F10301"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10301_O" deadCode="false" sourceNode="P_1F10301" targetNode="P_3F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_1F10301"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10301_I" deadCode="false" sourceNode="P_1F10301" targetNode="P_4F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_3F10301"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10301_O" deadCode="false" sourceNode="P_1F10301" targetNode="P_5F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_3F10301"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10301_I" deadCode="false" sourceNode="P_1F10301" targetNode="P_6F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_4F10301"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10301_O" deadCode="false" sourceNode="P_1F10301" targetNode="P_7F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_4F10301"/>
	</edges>
	<edges id="P_2F10301P_3F10301" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10301" targetNode="P_3F10301"/>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10301_I" deadCode="false" sourceNode="P_4F10301" targetNode="P_8F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_9F10301"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10301_O" deadCode="false" sourceNode="P_4F10301" targetNode="P_9F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_9F10301"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10301_I" deadCode="false" sourceNode="P_4F10301" targetNode="P_10F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_10F10301"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10301_O" deadCode="false" sourceNode="P_4F10301" targetNode="P_11F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_10F10301"/>
	</edges>
	<edges id="P_4F10301P_5F10301" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10301" targetNode="P_5F10301"/>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10301_I" deadCode="false" sourceNode="P_8F10301" targetNode="P_12F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_17F10301"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10301_O" deadCode="false" sourceNode="P_8F10301" targetNode="P_13F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_17F10301"/>
	</edges>
	<edges id="P_8F10301P_9F10301" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10301" targetNode="P_9F10301"/>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10301_I" deadCode="false" sourceNode="P_10F10301" targetNode="P_12F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_23F10301"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10301_O" deadCode="false" sourceNode="P_10F10301" targetNode="P_13F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_23F10301"/>
	</edges>
	<edges id="P_10F10301P_11F10301" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10301" targetNode="P_11F10301"/>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10301_I" deadCode="false" sourceNode="P_12F10301" targetNode="P_14F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_29F10301"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10301_O" deadCode="false" sourceNode="P_12F10301" targetNode="P_15F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_29F10301"/>
	</edges>
	<edges id="P_12F10301P_13F10301" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10301" targetNode="P_13F10301"/>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10301_I" deadCode="false" sourceNode="P_14F10301" targetNode="P_16F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_36F10301"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10301_O" deadCode="false" sourceNode="P_14F10301" targetNode="P_17F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_36F10301"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10301_I" deadCode="false" sourceNode="P_14F10301" targetNode="P_18F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_40F10301"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10301_O" deadCode="false" sourceNode="P_14F10301" targetNode="P_19F10301">
		<representations href="../../../cobol/LVES0268.cbl.cobModel#S_40F10301"/>
	</edges>
	<edges id="P_14F10301P_15F10301" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10301" targetNode="P_15F10301"/>
	<edges id="P_18F10301P_19F10301" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10301" targetNode="P_19F10301"/>
	<edges id="P_16F10301P_17F10301" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10301" targetNode="P_17F10301"/>
	<edges xsi:type="cbl:CallEdge" id="S_13F10301" deadCode="false" name="Dynamic LVES0269" sourceNode="P_8F10301" targetNode="LVES0269">
		<representations href="../../../cobol/../importantStmts.cobModel#S_13F10301"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_19F10301" deadCode="false" name="Dynamic LVES0270" sourceNode="P_10F10301" targetNode="LVES0270">
		<representations href="../../../cobol/../importantStmts.cobModel#S_19F10301"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_34F10301" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_14F10301" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_34F10301"></representations>
	</edges>
</Package>

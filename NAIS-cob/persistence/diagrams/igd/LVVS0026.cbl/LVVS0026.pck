<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0026" cbl:id="LVVS0026" xsi:id="LVVS0026" packageRef="LVVS0026.igd#LVVS0026" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0026_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10320" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0026.cbl.cobModel#SC_1F10320"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10320" deadCode="false" name="PROGRAM_LVVS0026_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0026.cbl.cobModel#P_1F10320"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10320" deadCode="false" name="L000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0026.cbl.cobModel#P_2F10320"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10320" deadCode="false" name="L000-EX">
				<representations href="../../../cobol/LVVS0026.cbl.cobModel#P_3F10320"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10320" deadCode="false" name="L100-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0026.cbl.cobModel#P_4F10320"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10320" deadCode="false" name="L100-EX">
				<representations href="../../../cobol/LVVS0026.cbl.cobModel#P_5F10320"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10320" deadCode="false" name="L500-PREPARA-CALL-LDBS1650">
				<representations href="../../../cobol/LVVS0026.cbl.cobModel#P_8F10320"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10320" deadCode="false" name="L500-EX">
				<representations href="../../../cobol/LVVS0026.cbl.cobModel#P_9F10320"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10320" deadCode="false" name="L700-CALL-LDBS1650">
				<representations href="../../../cobol/LVVS0026.cbl.cobModel#P_10F10320"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10320" deadCode="false" name="L700-EX">
				<representations href="../../../cobol/LVVS0026.cbl.cobModel#P_11F10320"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10320" deadCode="false" name="L900-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0026.cbl.cobModel#P_6F10320"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10320" deadCode="true" name="L900-EX">
				<representations href="../../../cobol/LVVS0026.cbl.cobModel#P_7F10320"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS1650" name="LDBS1650">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10164"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10320P_1F10320" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10320" targetNode="P_1F10320"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10320_I" deadCode="false" sourceNode="P_1F10320" targetNode="P_2F10320">
		<representations href="../../../cobol/LVVS0026.cbl.cobModel#S_1F10320"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10320_O" deadCode="false" sourceNode="P_1F10320" targetNode="P_3F10320">
		<representations href="../../../cobol/LVVS0026.cbl.cobModel#S_1F10320"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10320_I" deadCode="false" sourceNode="P_1F10320" targetNode="P_4F10320">
		<representations href="../../../cobol/LVVS0026.cbl.cobModel#S_2F10320"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10320_O" deadCode="false" sourceNode="P_1F10320" targetNode="P_5F10320">
		<representations href="../../../cobol/LVVS0026.cbl.cobModel#S_2F10320"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10320_I" deadCode="false" sourceNode="P_1F10320" targetNode="P_6F10320">
		<representations href="../../../cobol/LVVS0026.cbl.cobModel#S_3F10320"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10320_O" deadCode="false" sourceNode="P_1F10320" targetNode="P_7F10320">
		<representations href="../../../cobol/LVVS0026.cbl.cobModel#S_3F10320"/>
	</edges>
	<edges id="P_2F10320P_3F10320" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10320" targetNode="P_3F10320"/>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10320_I" deadCode="false" sourceNode="P_4F10320" targetNode="P_8F10320">
		<representations href="../../../cobol/LVVS0026.cbl.cobModel#S_9F10320"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10320_O" deadCode="false" sourceNode="P_4F10320" targetNode="P_9F10320">
		<representations href="../../../cobol/LVVS0026.cbl.cobModel#S_9F10320"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10320_I" deadCode="false" sourceNode="P_4F10320" targetNode="P_10F10320">
		<representations href="../../../cobol/LVVS0026.cbl.cobModel#S_10F10320"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10320_O" deadCode="false" sourceNode="P_4F10320" targetNode="P_11F10320">
		<representations href="../../../cobol/LVVS0026.cbl.cobModel#S_10F10320"/>
	</edges>
	<edges id="P_4F10320P_5F10320" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10320" targetNode="P_5F10320"/>
	<edges id="P_8F10320P_9F10320" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10320" targetNode="P_9F10320"/>
	<edges id="P_10F10320P_11F10320" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10320" targetNode="P_11F10320"/>
	<edges xsi:type="cbl:CallEdge" id="S_19F10320" deadCode="false" name="Dynamic WK-PGM-CALLED" sourceNode="P_10F10320" targetNode="LDBS1650">
		<representations href="../../../cobol/../importantStmts.cobModel#S_19F10320"></representations>
	</edges>
</Package>

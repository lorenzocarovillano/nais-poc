<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IABS0060" cbl:id="IABS0060" xsi:id="IABS0060" packageRef="IABS0060.igd#IABS0060" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IABS0060_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10005" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IABS0060.cbl.cobModel#SC_1F10005"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10005" deadCode="false" name="PROGRAM_IABS0060_FIRST_SENTENCES">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_1F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10005" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_2F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10005" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_3F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10005" deadCode="false" name="A060-CNTL-INPUT">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_4F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10005" deadCode="false" name="A060-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_5F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10005" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_12F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10005" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_13F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10005" deadCode="false" name="A300-ELABORA">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_6F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10005" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_7F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10005" deadCode="false" name="A400-FINE">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_8F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10005" deadCode="false" name="A400-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_9F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10005" deadCode="false" name="A310-SELECT">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_14F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10005" deadCode="false" name="A310-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_15F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10005" deadCode="false" name="A311-SELECT-STD">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_28F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10005" deadCode="false" name="A311-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_29F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10005" deadCode="false" name="A312-SELECT-MCRFNCT">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_30F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10005" deadCode="false" name="A312-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_31F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10005" deadCode="false" name="A313-SELECT-TPMV">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_32F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10005" deadCode="false" name="A313-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_33F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10005" deadCode="false" name="A314-SELECT-MCRFNCT-TPMV">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_34F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10005" deadCode="false" name="A314-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_35F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10005" deadCode="false" name="A315-SELECT-IB-OGG">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_36F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10005" deadCode="false" name="A315-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_37F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10005" deadCode="false" name="A318-INSERT">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_16F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10005" deadCode="false" name="A318-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_17F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10005" deadCode="false" name="A320-UPDATE">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_26F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10005" deadCode="false" name="A320-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_27F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10005" deadCode="false" name="A330-UPDATE-PK">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_54F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10005" deadCode="false" name="A330-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_55F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10005" deadCode="false" name="A340-UPDATE-FIRST-ACTION">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_56F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10005" deadCode="false" name="A340-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_57F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10005" deadCode="false" name="A341-UPDATE-F-ACT-STD">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_60F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10005" deadCode="false" name="A341-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_61F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10005" deadCode="false" name="A342-UPDATE-F-ACT-MCRFNCT">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_62F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10005" deadCode="false" name="A342-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_63F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10005" deadCode="false" name="A343-UPDATE-F-ACT-TPMV">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_64F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10005" deadCode="false" name="A343-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_65F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10005" deadCode="false" name="A344-UPDATE-F-ACT-MCRFNCT-TPMV">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_66F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10005" deadCode="false" name="A344-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_67F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10005" deadCode="false" name="A350-UPDATE-WHERE-COND">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_58F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10005" deadCode="false" name="A350-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_59F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10005" deadCode="false" name="A351-UPD-WH-COND-STD">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_68F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10005" deadCode="false" name="A351-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_69F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10005" deadCode="false" name="A352-UPD-WH-COND-MCRFNCT">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_70F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10005" deadCode="false" name="A352-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_71F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10005" deadCode="false" name="A353-UPD-WH-COND-TPMV">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_72F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10005" deadCode="false" name="A353-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_73F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10005" deadCode="false" name="A354-UPD-WH-COND-MCRFNCT-TPMV">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_74F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10005" deadCode="false" name="A354-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_75F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10005" deadCode="false" name="A360-OPEN-CURSOR">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_18F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10005" deadCode="false" name="A360-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_19F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10005" deadCode="false" name="A370-CLOSE-CURSOR">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_20F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10005" deadCode="false" name="A370-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_21F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10005" deadCode="false" name="A380-FETCH-FIRST">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_22F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10005" deadCode="false" name="A380-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_23F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10005" deadCode="false" name="A390-FETCH-NEXT">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_24F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10005" deadCode="false" name="A390-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_25F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_90F10005" deadCode="false" name="D101-DCL-CUR-IDJ-STD">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_90F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_91F10005" deadCode="false" name="D101-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_91F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_92F10005" deadCode="false" name="D102-DCL-CUR-IDJ-MCRFNCT">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_92F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_93F10005" deadCode="false" name="D102-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_93F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_94F10005" deadCode="false" name="D103-DCL-CUR-IDJ-TPMV">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_94F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_95F10005" deadCode="false" name="D103-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_95F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_96F10005" deadCode="false" name="D104-DCL-CUR-IDJ-MCRFNCT-TPMV">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_96F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_97F10005" deadCode="false" name="D104-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_97F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_98F10005" deadCode="false" name="D601-DCL-CUR-IBO-STD">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_98F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_99F10005" deadCode="false" name="D601-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_99F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_100F10005" deadCode="false" name="D602-DCL-CUR-IBO-MCRFNCT">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_100F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_101F10005" deadCode="false" name="D602-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_101F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_102F10005" deadCode="false" name="D603-DCL-CUR-IBO-TPMV">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_102F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_103F10005" deadCode="false" name="D603-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_103F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_104F10005" deadCode="false" name="D604-DCL-CUR-IBO-MCRFNCT-TPMV">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_104F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_105F10005" deadCode="false" name="D604-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_105F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_86F10005" deadCode="false" name="F100-FET-NEXT-IDJ">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_86F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_87F10005" deadCode="false" name="F100-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_87F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_106F10005" deadCode="false" name="F101-FET-NEXT-IDJ-STD">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_106F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_107F10005" deadCode="false" name="F101-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_107F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_108F10005" deadCode="false" name="F102-FET-NEXT-IDJ-MCRFNCT">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_108F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_109F10005" deadCode="false" name="F102-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_109F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_110F10005" deadCode="false" name="F103-FET-NEXT-IDJ-TPMV">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_110F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_111F10005" deadCode="false" name="F103-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_111F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_112F10005" deadCode="false" name="F104-FET-NEXT-IDJ-MCRFNCT-TPMV">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_112F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_113F10005" deadCode="false" name="F104-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_113F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_88F10005" deadCode="false" name="F600-FET-NEXT-IBO">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_88F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_89F10005" deadCode="false" name="F600-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_89F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_116F10005" deadCode="false" name="F601-FET-NEXT-IBO-STD">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_116F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_117F10005" deadCode="false" name="F601-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_117F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_118F10005" deadCode="false" name="F602-FET-NEXT-IBO-MCRFNCT">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_118F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_119F10005" deadCode="false" name="F602-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_119F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_120F10005" deadCode="false" name="F603-FET-NEXT-IBO-TPMV">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_120F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_121F10005" deadCode="false" name="F603-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_121F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_122F10005" deadCode="false" name="F604-FET-NEXT-IBO-MCRFNCT-TPMV">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_122F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_123F10005" deadCode="false" name="F604-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_123F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10005" deadCode="false" name="R311-SELECT-STD-X-RNG">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_38F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10005" deadCode="false" name="R311-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_39F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_124F10005" deadCode="false" name="S101-DCL-IDJ-STD-X-RNG">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_124F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_125F10005" deadCode="false" name="S101-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_125F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_114F10005" deadCode="false" name="T101-FET-NEXT-IDJ-STD-X-RNG">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_114F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_115F10005" deadCode="false" name="T101-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_115F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10005" deadCode="false" name="U351-UPD-WH-COND-STD-X-RNG">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_76F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10005" deadCode="false" name="U351-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_77F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10005" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_40F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10005" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_41F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10005" deadCode="false" name="Z150-VALORIZZA-DATA-SERVICES">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_46F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10005" deadCode="false" name="Z150-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_47F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10005" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_48F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10005" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_49F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10005" deadCode="false" name="Z400-SEQ">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_44F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10005" deadCode="false" name="Z400-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_45F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10005" deadCode="false" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_50F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10005" deadCode="false" name="Z900-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_51F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10005" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_42F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10005" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_43F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10005" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_52F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10005" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_53F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_82F10005" deadCode="false" name="C100-CLOSE-IDJ">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_82F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_83F10005" deadCode="false" name="C100-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_83F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_84F10005" deadCode="false" name="C600-CLOSE-IBO">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_84F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_85F10005" deadCode="false" name="C600-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_85F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_78F10005" deadCode="false" name="D100-DECLARE-CURSOR-IDJ">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_78F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_79F10005" deadCode="false" name="D100-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_79F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_80F10005" deadCode="false" name="D600-DECLARE-CURSOR-IBO">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_80F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_81F10005" deadCode="false" name="D600-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_81F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10005" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_10F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10005" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_11F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_130F10005" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_130F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_131F10005" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_131F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_132F10005" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_132F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_133F10005" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_133F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_134F10005" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_134F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_135F10005" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_135F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_126F10005" deadCode="false" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_126F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_127F10005" deadCode="false" name="Z701-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_127F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_136F10005" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_136F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_141F10005" deadCode="true" name="Z800-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_141F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_137F10005" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_137F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_138F10005" deadCode="true" name="Z810-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_138F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_139F10005" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_139F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_140F10005" deadCode="true" name="Z820-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_140F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_128F10005" deadCode="false" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_128F10005"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_129F10005" deadCode="false" name="Z801-EX">
				<representations href="../../../cobol/IABS0060.cbl.cobModel#P_129F10005"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_BTC_JOB_SCHEDULE" name="BTC_JOB_SCHEDULE">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_BTC_JOB_SCHEDULE"/>
		</children>
	</packageNode>
	<edges id="SC_1F10005P_1F10005" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10005" targetNode="P_1F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10005_I" deadCode="false" sourceNode="P_1F10005" targetNode="P_2F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_1F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10005_O" deadCode="false" sourceNode="P_1F10005" targetNode="P_3F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_1F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10005_I" deadCode="false" sourceNode="P_1F10005" targetNode="P_4F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_2F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10005_O" deadCode="false" sourceNode="P_1F10005" targetNode="P_5F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_2F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10005_I" deadCode="false" sourceNode="P_1F10005" targetNode="P_6F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_3F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10005_O" deadCode="false" sourceNode="P_1F10005" targetNode="P_7F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_3F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10005_I" deadCode="false" sourceNode="P_1F10005" targetNode="P_8F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_4F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10005_O" deadCode="false" sourceNode="P_1F10005" targetNode="P_9F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_4F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10005_I" deadCode="false" sourceNode="P_2F10005" targetNode="P_10F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_12F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10005_O" deadCode="false" sourceNode="P_2F10005" targetNode="P_11F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_12F10005"/>
	</edges>
	<edges id="P_2F10005P_3F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10005" targetNode="P_3F10005"/>
	<edges id="P_4F10005P_5F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10005" targetNode="P_5F10005"/>
	<edges id="P_12F10005P_13F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10005" targetNode="P_13F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10005_I" deadCode="false" sourceNode="P_6F10005" targetNode="P_14F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_40F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10005_O" deadCode="false" sourceNode="P_6F10005" targetNode="P_15F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_40F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10005_I" deadCode="false" sourceNode="P_6F10005" targetNode="P_16F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_41F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10005_O" deadCode="false" sourceNode="P_6F10005" targetNode="P_17F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_41F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10005_I" deadCode="false" sourceNode="P_6F10005" targetNode="P_18F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_42F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10005_O" deadCode="false" sourceNode="P_6F10005" targetNode="P_19F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_42F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10005_I" deadCode="false" sourceNode="P_6F10005" targetNode="P_20F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_43F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10005_O" deadCode="false" sourceNode="P_6F10005" targetNode="P_21F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_43F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10005_I" deadCode="false" sourceNode="P_6F10005" targetNode="P_22F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_44F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10005_O" deadCode="false" sourceNode="P_6F10005" targetNode="P_23F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_44F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10005_I" deadCode="false" sourceNode="P_6F10005" targetNode="P_24F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_45F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10005_O" deadCode="false" sourceNode="P_6F10005" targetNode="P_25F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_45F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10005_I" deadCode="false" sourceNode="P_6F10005" targetNode="P_26F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_46F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10005_O" deadCode="false" sourceNode="P_6F10005" targetNode="P_27F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_46F10005"/>
	</edges>
	<edges id="P_6F10005P_7F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10005" targetNode="P_7F10005"/>
	<edges id="P_8F10005P_9F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10005" targetNode="P_9F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10005_I" deadCode="false" sourceNode="P_14F10005" targetNode="P_28F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_53F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10005_O" deadCode="false" sourceNode="P_14F10005" targetNode="P_29F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_53F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10005_I" deadCode="false" sourceNode="P_14F10005" targetNode="P_30F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_54F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10005_O" deadCode="false" sourceNode="P_14F10005" targetNode="P_31F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_54F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10005_I" deadCode="false" sourceNode="P_14F10005" targetNode="P_32F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_55F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10005_O" deadCode="false" sourceNode="P_14F10005" targetNode="P_33F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_55F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10005_I" deadCode="false" sourceNode="P_14F10005" targetNode="P_34F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_56F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10005_O" deadCode="false" sourceNode="P_14F10005" targetNode="P_35F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_56F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10005_I" deadCode="false" sourceNode="P_14F10005" targetNode="P_36F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_57F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10005_O" deadCode="false" sourceNode="P_14F10005" targetNode="P_37F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_57F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10005_I" deadCode="false" sourceNode="P_14F10005" targetNode="P_38F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_59F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10005_O" deadCode="false" sourceNode="P_14F10005" targetNode="P_39F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_59F10005"/>
	</edges>
	<edges id="P_14F10005P_15F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10005" targetNode="P_15F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10005_I" deadCode="false" sourceNode="P_28F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_64F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10005_O" deadCode="false" sourceNode="P_28F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_64F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10005_I" deadCode="false" sourceNode="P_28F10005" targetNode="P_40F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_66F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10005_O" deadCode="false" sourceNode="P_28F10005" targetNode="P_41F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_66F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10005_I" deadCode="false" sourceNode="P_28F10005" targetNode="P_42F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_67F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10005_O" deadCode="false" sourceNode="P_28F10005" targetNode="P_43F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_67F10005"/>
	</edges>
	<edges id="P_28F10005P_29F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10005" targetNode="P_29F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10005_I" deadCode="false" sourceNode="P_30F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_70F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10005_O" deadCode="false" sourceNode="P_30F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_70F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10005_I" deadCode="false" sourceNode="P_30F10005" targetNode="P_40F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_72F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10005_O" deadCode="false" sourceNode="P_30F10005" targetNode="P_41F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_72F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10005_I" deadCode="false" sourceNode="P_30F10005" targetNode="P_42F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_73F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10005_O" deadCode="false" sourceNode="P_30F10005" targetNode="P_43F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_73F10005"/>
	</edges>
	<edges id="P_30F10005P_31F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10005" targetNode="P_31F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10005_I" deadCode="false" sourceNode="P_32F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_76F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10005_O" deadCode="false" sourceNode="P_32F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_76F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10005_I" deadCode="false" sourceNode="P_32F10005" targetNode="P_40F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_78F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10005_O" deadCode="false" sourceNode="P_32F10005" targetNode="P_41F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_78F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10005_I" deadCode="false" sourceNode="P_32F10005" targetNode="P_42F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_79F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10005_O" deadCode="false" sourceNode="P_32F10005" targetNode="P_43F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_79F10005"/>
	</edges>
	<edges id="P_32F10005P_33F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10005" targetNode="P_33F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10005_I" deadCode="false" sourceNode="P_34F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_82F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10005_O" deadCode="false" sourceNode="P_34F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_82F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10005_I" deadCode="false" sourceNode="P_34F10005" targetNode="P_40F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_84F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10005_O" deadCode="false" sourceNode="P_34F10005" targetNode="P_41F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_84F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10005_I" deadCode="false" sourceNode="P_34F10005" targetNode="P_42F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_85F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10005_O" deadCode="false" sourceNode="P_34F10005" targetNode="P_43F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_85F10005"/>
	</edges>
	<edges id="P_34F10005P_35F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10005" targetNode="P_35F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10005_I" deadCode="false" sourceNode="P_36F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_88F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10005_O" deadCode="false" sourceNode="P_36F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_88F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10005_I" deadCode="false" sourceNode="P_36F10005" targetNode="P_40F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_90F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10005_O" deadCode="false" sourceNode="P_36F10005" targetNode="P_41F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_90F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10005_I" deadCode="false" sourceNode="P_36F10005" targetNode="P_42F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_91F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10005_O" deadCode="false" sourceNode="P_36F10005" targetNode="P_43F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_91F10005"/>
	</edges>
	<edges id="P_36F10005P_37F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10005" targetNode="P_37F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10005_I" deadCode="false" sourceNode="P_16F10005" targetNode="P_44F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_97F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10005_O" deadCode="false" sourceNode="P_16F10005" targetNode="P_45F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_97F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10005_I" deadCode="false" sourceNode="P_16F10005" targetNode="P_46F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_99F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10005_O" deadCode="false" sourceNode="P_16F10005" targetNode="P_47F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_99F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10005_I" deadCode="false" sourceNode="P_16F10005" targetNode="P_48F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_100F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10005_O" deadCode="false" sourceNode="P_16F10005" targetNode="P_49F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_100F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10005_I" deadCode="false" sourceNode="P_16F10005" targetNode="P_50F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_101F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10005_O" deadCode="false" sourceNode="P_16F10005" targetNode="P_51F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_101F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10005_I" deadCode="false" sourceNode="P_16F10005" targetNode="P_52F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_102F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10005_O" deadCode="false" sourceNode="P_16F10005" targetNode="P_53F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_102F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10005_I" deadCode="false" sourceNode="P_16F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_104F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10005_O" deadCode="false" sourceNode="P_16F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_104F10005"/>
	</edges>
	<edges id="P_16F10005P_17F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10005" targetNode="P_17F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10005_I" deadCode="false" sourceNode="P_26F10005" targetNode="P_54F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_107F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10005_O" deadCode="false" sourceNode="P_26F10005" targetNode="P_55F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_107F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10005_I" deadCode="false" sourceNode="P_26F10005" targetNode="P_56F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_108F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10005_O" deadCode="false" sourceNode="P_26F10005" targetNode="P_57F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_108F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10005_I" deadCode="false" sourceNode="P_26F10005" targetNode="P_58F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_109F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10005_O" deadCode="false" sourceNode="P_26F10005" targetNode="P_59F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_109F10005"/>
	</edges>
	<edges id="P_26F10005P_27F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10005" targetNode="P_27F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10005_I" deadCode="false" sourceNode="P_54F10005" targetNode="P_48F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_112F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10005_O" deadCode="false" sourceNode="P_54F10005" targetNode="P_49F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_112F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10005_I" deadCode="false" sourceNode="P_54F10005" targetNode="P_50F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_113F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10005_O" deadCode="false" sourceNode="P_54F10005" targetNode="P_51F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_113F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10005_I" deadCode="false" sourceNode="P_54F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_115F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10005_O" deadCode="false" sourceNode="P_54F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_115F10005"/>
	</edges>
	<edges id="P_54F10005P_55F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10005" targetNode="P_55F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10005_I" deadCode="false" sourceNode="P_56F10005" targetNode="P_60F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_120F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10005_O" deadCode="false" sourceNode="P_56F10005" targetNode="P_61F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_120F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10005_I" deadCode="false" sourceNode="P_56F10005" targetNode="P_62F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_121F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10005_O" deadCode="false" sourceNode="P_56F10005" targetNode="P_63F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_121F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10005_I" deadCode="false" sourceNode="P_56F10005" targetNode="P_64F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_122F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10005_O" deadCode="false" sourceNode="P_56F10005" targetNode="P_65F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_122F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10005_I" deadCode="false" sourceNode="P_56F10005" targetNode="P_66F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_123F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10005_O" deadCode="false" sourceNode="P_56F10005" targetNode="P_67F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_123F10005"/>
	</edges>
	<edges id="P_56F10005P_57F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10005" targetNode="P_57F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10005_I" deadCode="false" sourceNode="P_60F10005" targetNode="P_48F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_125F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10005_O" deadCode="false" sourceNode="P_60F10005" targetNode="P_49F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_125F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10005_I" deadCode="false" sourceNode="P_60F10005" targetNode="P_50F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_126F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10005_O" deadCode="false" sourceNode="P_60F10005" targetNode="P_51F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_126F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10005_I" deadCode="false" sourceNode="P_60F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_128F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10005_O" deadCode="false" sourceNode="P_60F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_128F10005"/>
	</edges>
	<edges id="P_60F10005P_61F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_60F10005" targetNode="P_61F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10005_I" deadCode="false" sourceNode="P_62F10005" targetNode="P_48F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_132F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10005_O" deadCode="false" sourceNode="P_62F10005" targetNode="P_49F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_132F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10005_I" deadCode="false" sourceNode="P_62F10005" targetNode="P_50F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_133F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10005_O" deadCode="false" sourceNode="P_62F10005" targetNode="P_51F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_133F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10005_I" deadCode="false" sourceNode="P_62F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_135F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10005_O" deadCode="false" sourceNode="P_62F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_135F10005"/>
	</edges>
	<edges id="P_62F10005P_63F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10005" targetNode="P_63F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10005_I" deadCode="false" sourceNode="P_64F10005" targetNode="P_48F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_139F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10005_O" deadCode="false" sourceNode="P_64F10005" targetNode="P_49F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_139F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10005_I" deadCode="false" sourceNode="P_64F10005" targetNode="P_50F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_140F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10005_O" deadCode="false" sourceNode="P_64F10005" targetNode="P_51F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_140F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10005_I" deadCode="false" sourceNode="P_64F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_142F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10005_O" deadCode="false" sourceNode="P_64F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_142F10005"/>
	</edges>
	<edges id="P_64F10005P_65F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10005" targetNode="P_65F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10005_I" deadCode="false" sourceNode="P_66F10005" targetNode="P_48F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_146F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10005_O" deadCode="false" sourceNode="P_66F10005" targetNode="P_49F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_146F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10005_I" deadCode="false" sourceNode="P_66F10005" targetNode="P_50F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_147F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10005_O" deadCode="false" sourceNode="P_66F10005" targetNode="P_51F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_147F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10005_I" deadCode="false" sourceNode="P_66F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_149F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10005_O" deadCode="false" sourceNode="P_66F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_149F10005"/>
	</edges>
	<edges id="P_66F10005P_67F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10005" targetNode="P_67F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_155F10005_I" deadCode="false" sourceNode="P_58F10005" targetNode="P_68F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_155F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_155F10005_O" deadCode="false" sourceNode="P_58F10005" targetNode="P_69F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_155F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_156F10005_I" deadCode="false" sourceNode="P_58F10005" targetNode="P_70F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_156F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_156F10005_O" deadCode="false" sourceNode="P_58F10005" targetNode="P_71F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_156F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10005_I" deadCode="false" sourceNode="P_58F10005" targetNode="P_72F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_157F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10005_O" deadCode="false" sourceNode="P_58F10005" targetNode="P_73F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_157F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10005_I" deadCode="false" sourceNode="P_58F10005" targetNode="P_74F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_158F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10005_O" deadCode="false" sourceNode="P_58F10005" targetNode="P_75F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_158F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10005_I" deadCode="false" sourceNode="P_58F10005" targetNode="P_76F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_160F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10005_O" deadCode="false" sourceNode="P_58F10005" targetNode="P_77F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_160F10005"/>
	</edges>
	<edges id="P_58F10005P_59F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10005" targetNode="P_59F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10005_I" deadCode="false" sourceNode="P_68F10005" targetNode="P_48F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_164F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10005_O" deadCode="false" sourceNode="P_68F10005" targetNode="P_49F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_164F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10005_I" deadCode="false" sourceNode="P_68F10005" targetNode="P_50F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_165F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10005_O" deadCode="false" sourceNode="P_68F10005" targetNode="P_51F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_165F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10005_I" deadCode="false" sourceNode="P_68F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_167F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10005_O" deadCode="false" sourceNode="P_68F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_167F10005"/>
	</edges>
	<edges id="P_68F10005P_69F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10005" targetNode="P_69F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10005_I" deadCode="false" sourceNode="P_70F10005" targetNode="P_48F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_171F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10005_O" deadCode="false" sourceNode="P_70F10005" targetNode="P_49F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_171F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10005_I" deadCode="false" sourceNode="P_70F10005" targetNode="P_50F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_172F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10005_O" deadCode="false" sourceNode="P_70F10005" targetNode="P_51F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_172F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10005_I" deadCode="false" sourceNode="P_70F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_174F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10005_O" deadCode="false" sourceNode="P_70F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_174F10005"/>
	</edges>
	<edges id="P_70F10005P_71F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_70F10005" targetNode="P_71F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_178F10005_I" deadCode="false" sourceNode="P_72F10005" targetNode="P_48F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_178F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_178F10005_O" deadCode="false" sourceNode="P_72F10005" targetNode="P_49F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_178F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10005_I" deadCode="false" sourceNode="P_72F10005" targetNode="P_50F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_179F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10005_O" deadCode="false" sourceNode="P_72F10005" targetNode="P_51F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_179F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10005_I" deadCode="false" sourceNode="P_72F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_181F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10005_O" deadCode="false" sourceNode="P_72F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_181F10005"/>
	</edges>
	<edges id="P_72F10005P_73F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10005" targetNode="P_73F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10005_I" deadCode="false" sourceNode="P_74F10005" targetNode="P_48F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_185F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10005_O" deadCode="false" sourceNode="P_74F10005" targetNode="P_49F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_185F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10005_I" deadCode="false" sourceNode="P_74F10005" targetNode="P_50F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_186F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10005_O" deadCode="false" sourceNode="P_74F10005" targetNode="P_51F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_186F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_188F10005_I" deadCode="false" sourceNode="P_74F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_188F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_188F10005_O" deadCode="false" sourceNode="P_74F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_188F10005"/>
	</edges>
	<edges id="P_74F10005P_75F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10005" targetNode="P_75F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_193F10005_I" deadCode="false" sourceNode="P_18F10005" targetNode="P_78F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_193F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_193F10005_O" deadCode="false" sourceNode="P_18F10005" targetNode="P_79F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_193F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_194F10005_I" deadCode="false" sourceNode="P_18F10005" targetNode="P_80F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_194F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_194F10005_O" deadCode="false" sourceNode="P_18F10005" targetNode="P_81F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_194F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_197F10005_I" deadCode="false" sourceNode="P_18F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_197F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_197F10005_O" deadCode="false" sourceNode="P_18F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_197F10005"/>
	</edges>
	<edges id="P_18F10005P_19F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10005" targetNode="P_19F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_200F10005_I" deadCode="false" sourceNode="P_20F10005" targetNode="P_82F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_200F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_200F10005_O" deadCode="false" sourceNode="P_20F10005" targetNode="P_83F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_200F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_201F10005_I" deadCode="false" sourceNode="P_20F10005" targetNode="P_84F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_201F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_201F10005_O" deadCode="false" sourceNode="P_20F10005" targetNode="P_85F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_201F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_204F10005_I" deadCode="false" sourceNode="P_20F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_204F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_204F10005_O" deadCode="false" sourceNode="P_20F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_204F10005"/>
	</edges>
	<edges id="P_20F10005P_21F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10005" targetNode="P_21F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_206F10005_I" deadCode="false" sourceNode="P_22F10005" targetNode="P_18F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_206F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_206F10005_O" deadCode="false" sourceNode="P_22F10005" targetNode="P_19F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_206F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_208F10005_I" deadCode="false" sourceNode="P_22F10005" targetNode="P_24F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_208F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_208F10005_O" deadCode="false" sourceNode="P_22F10005" targetNode="P_25F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_208F10005"/>
	</edges>
	<edges id="P_22F10005P_23F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10005" targetNode="P_23F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10005_I" deadCode="false" sourceNode="P_24F10005" targetNode="P_86F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_211F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10005_O" deadCode="false" sourceNode="P_24F10005" targetNode="P_87F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_211F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_212F10005_I" deadCode="false" sourceNode="P_24F10005" targetNode="P_88F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_212F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_212F10005_O" deadCode="false" sourceNode="P_24F10005" targetNode="P_89F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_212F10005"/>
	</edges>
	<edges id="P_24F10005P_25F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10005" targetNode="P_25F10005"/>
	<edges id="P_90F10005P_91F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_90F10005" targetNode="P_91F10005"/>
	<edges id="P_92F10005P_93F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_92F10005" targetNode="P_93F10005"/>
	<edges id="P_94F10005P_95F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_94F10005" targetNode="P_95F10005"/>
	<edges id="P_96F10005P_97F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_96F10005" targetNode="P_97F10005"/>
	<edges id="P_98F10005P_99F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_98F10005" targetNode="P_99F10005"/>
	<edges id="P_100F10005P_101F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_100F10005" targetNode="P_101F10005"/>
	<edges id="P_102F10005P_103F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_102F10005" targetNode="P_103F10005"/>
	<edges id="P_104F10005P_105F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_104F10005" targetNode="P_105F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_249F10005_I" deadCode="false" sourceNode="P_86F10005" targetNode="P_106F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_249F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_249F10005_O" deadCode="false" sourceNode="P_86F10005" targetNode="P_107F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_249F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_250F10005_I" deadCode="false" sourceNode="P_86F10005" targetNode="P_108F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_250F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_250F10005_O" deadCode="false" sourceNode="P_86F10005" targetNode="P_109F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_250F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_251F10005_I" deadCode="false" sourceNode="P_86F10005" targetNode="P_110F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_251F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_251F10005_O" deadCode="false" sourceNode="P_86F10005" targetNode="P_111F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_251F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_252F10005_I" deadCode="false" sourceNode="P_86F10005" targetNode="P_112F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_252F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_252F10005_O" deadCode="false" sourceNode="P_86F10005" targetNode="P_113F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_252F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_254F10005_I" deadCode="false" sourceNode="P_86F10005" targetNode="P_114F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_254F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_254F10005_O" deadCode="false" sourceNode="P_86F10005" targetNode="P_115F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_254F10005"/>
	</edges>
	<edges id="P_86F10005P_87F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_86F10005" targetNode="P_87F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10005_I" deadCode="false" sourceNode="P_106F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_259F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10005_O" deadCode="false" sourceNode="P_106F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_259F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_261F10005_I" deadCode="false" sourceNode="P_106F10005" targetNode="P_40F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_261F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_261F10005_O" deadCode="false" sourceNode="P_106F10005" targetNode="P_41F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_261F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10005_I" deadCode="false" sourceNode="P_106F10005" targetNode="P_42F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_262F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10005_O" deadCode="false" sourceNode="P_106F10005" targetNode="P_43F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_262F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_264F10005_I" deadCode="false" sourceNode="P_106F10005" targetNode="P_20F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_264F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_264F10005_O" deadCode="false" sourceNode="P_106F10005" targetNode="P_21F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_264F10005"/>
	</edges>
	<edges id="P_106F10005P_107F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_106F10005" targetNode="P_107F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_269F10005_I" deadCode="false" sourceNode="P_108F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_269F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_269F10005_O" deadCode="false" sourceNode="P_108F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_269F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_271F10005_I" deadCode="false" sourceNode="P_108F10005" targetNode="P_40F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_271F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_271F10005_O" deadCode="false" sourceNode="P_108F10005" targetNode="P_41F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_271F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_272F10005_I" deadCode="false" sourceNode="P_108F10005" targetNode="P_42F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_272F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_272F10005_O" deadCode="false" sourceNode="P_108F10005" targetNode="P_43F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_272F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_274F10005_I" deadCode="false" sourceNode="P_108F10005" targetNode="P_20F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_274F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_274F10005_O" deadCode="false" sourceNode="P_108F10005" targetNode="P_21F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_274F10005"/>
	</edges>
	<edges id="P_108F10005P_109F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_108F10005" targetNode="P_109F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_279F10005_I" deadCode="false" sourceNode="P_110F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_279F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_279F10005_O" deadCode="false" sourceNode="P_110F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_279F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10005_I" deadCode="false" sourceNode="P_110F10005" targetNode="P_40F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_281F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10005_O" deadCode="false" sourceNode="P_110F10005" targetNode="P_41F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_281F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_282F10005_I" deadCode="false" sourceNode="P_110F10005" targetNode="P_42F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_282F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_282F10005_O" deadCode="false" sourceNode="P_110F10005" targetNode="P_43F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_282F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10005_I" deadCode="false" sourceNode="P_110F10005" targetNode="P_20F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_284F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10005_O" deadCode="false" sourceNode="P_110F10005" targetNode="P_21F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_284F10005"/>
	</edges>
	<edges id="P_110F10005P_111F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_110F10005" targetNode="P_111F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10005_I" deadCode="false" sourceNode="P_112F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_289F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10005_O" deadCode="false" sourceNode="P_112F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_289F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_291F10005_I" deadCode="false" sourceNode="P_112F10005" targetNode="P_40F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_291F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_291F10005_O" deadCode="false" sourceNode="P_112F10005" targetNode="P_41F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_291F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10005_I" deadCode="false" sourceNode="P_112F10005" targetNode="P_42F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_292F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10005_O" deadCode="false" sourceNode="P_112F10005" targetNode="P_43F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_292F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10005_I" deadCode="false" sourceNode="P_112F10005" targetNode="P_20F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_294F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10005_O" deadCode="false" sourceNode="P_112F10005" targetNode="P_21F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_294F10005"/>
	</edges>
	<edges id="P_112F10005P_113F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_112F10005" targetNode="P_113F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10005_I" deadCode="false" sourceNode="P_88F10005" targetNode="P_116F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_299F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10005_O" deadCode="false" sourceNode="P_88F10005" targetNode="P_117F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_299F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_300F10005_I" deadCode="false" sourceNode="P_88F10005" targetNode="P_118F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_300F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_300F10005_O" deadCode="false" sourceNode="P_88F10005" targetNode="P_119F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_300F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10005_I" deadCode="false" sourceNode="P_88F10005" targetNode="P_120F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_301F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10005_O" deadCode="false" sourceNode="P_88F10005" targetNode="P_121F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_301F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_302F10005_I" deadCode="false" sourceNode="P_88F10005" targetNode="P_122F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_302F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_302F10005_O" deadCode="false" sourceNode="P_88F10005" targetNode="P_123F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_302F10005"/>
	</edges>
	<edges id="P_88F10005P_89F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_88F10005" targetNode="P_89F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_305F10005_I" deadCode="false" sourceNode="P_116F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_305F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_305F10005_O" deadCode="false" sourceNode="P_116F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_305F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_307F10005_I" deadCode="false" sourceNode="P_116F10005" targetNode="P_40F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_307F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_307F10005_O" deadCode="false" sourceNode="P_116F10005" targetNode="P_41F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_307F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10005_I" deadCode="false" sourceNode="P_116F10005" targetNode="P_42F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_308F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10005_O" deadCode="false" sourceNode="P_116F10005" targetNode="P_43F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_308F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_310F10005_I" deadCode="false" sourceNode="P_116F10005" targetNode="P_20F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_310F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_310F10005_O" deadCode="false" sourceNode="P_116F10005" targetNode="P_21F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_310F10005"/>
	</edges>
	<edges id="P_116F10005P_117F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_116F10005" targetNode="P_117F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_315F10005_I" deadCode="false" sourceNode="P_118F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_315F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_315F10005_O" deadCode="false" sourceNode="P_118F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_315F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_317F10005_I" deadCode="false" sourceNode="P_118F10005" targetNode="P_40F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_317F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_317F10005_O" deadCode="false" sourceNode="P_118F10005" targetNode="P_41F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_317F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_318F10005_I" deadCode="false" sourceNode="P_118F10005" targetNode="P_42F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_318F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_318F10005_O" deadCode="false" sourceNode="P_118F10005" targetNode="P_43F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_318F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_320F10005_I" deadCode="false" sourceNode="P_118F10005" targetNode="P_20F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_320F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_320F10005_O" deadCode="false" sourceNode="P_118F10005" targetNode="P_21F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_320F10005"/>
	</edges>
	<edges id="P_118F10005P_119F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_118F10005" targetNode="P_119F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_325F10005_I" deadCode="false" sourceNode="P_120F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_325F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_325F10005_O" deadCode="false" sourceNode="P_120F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_325F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_327F10005_I" deadCode="false" sourceNode="P_120F10005" targetNode="P_40F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_327F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_327F10005_O" deadCode="false" sourceNode="P_120F10005" targetNode="P_41F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_327F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_328F10005_I" deadCode="false" sourceNode="P_120F10005" targetNode="P_42F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_328F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_328F10005_O" deadCode="false" sourceNode="P_120F10005" targetNode="P_43F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_328F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_330F10005_I" deadCode="false" sourceNode="P_120F10005" targetNode="P_20F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_330F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_330F10005_O" deadCode="false" sourceNode="P_120F10005" targetNode="P_21F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_330F10005"/>
	</edges>
	<edges id="P_120F10005P_121F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_120F10005" targetNode="P_121F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_335F10005_I" deadCode="false" sourceNode="P_122F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_335F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_335F10005_O" deadCode="false" sourceNode="P_122F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_335F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_337F10005_I" deadCode="false" sourceNode="P_122F10005" targetNode="P_40F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_337F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_337F10005_O" deadCode="false" sourceNode="P_122F10005" targetNode="P_41F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_337F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10005_I" deadCode="false" sourceNode="P_122F10005" targetNode="P_42F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_338F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10005_O" deadCode="false" sourceNode="P_122F10005" targetNode="P_43F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_338F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_340F10005_I" deadCode="false" sourceNode="P_122F10005" targetNode="P_20F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_340F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_340F10005_O" deadCode="false" sourceNode="P_122F10005" targetNode="P_21F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_340F10005"/>
	</edges>
	<edges id="P_122F10005P_123F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_122F10005" targetNode="P_123F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_345F10005_I" deadCode="false" sourceNode="P_38F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_345F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_345F10005_O" deadCode="false" sourceNode="P_38F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_345F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_347F10005_I" deadCode="false" sourceNode="P_38F10005" targetNode="P_40F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_347F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_347F10005_O" deadCode="false" sourceNode="P_38F10005" targetNode="P_41F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_347F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_348F10005_I" deadCode="false" sourceNode="P_38F10005" targetNode="P_42F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_348F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_348F10005_O" deadCode="false" sourceNode="P_38F10005" targetNode="P_43F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_348F10005"/>
	</edges>
	<edges id="P_38F10005P_39F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10005" targetNode="P_39F10005"/>
	<edges id="P_124F10005P_125F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_124F10005" targetNode="P_125F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_355F10005_I" deadCode="false" sourceNode="P_114F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_355F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_355F10005_O" deadCode="false" sourceNode="P_114F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_355F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_357F10005_I" deadCode="false" sourceNode="P_114F10005" targetNode="P_40F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_357F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_357F10005_O" deadCode="false" sourceNode="P_114F10005" targetNode="P_41F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_357F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_358F10005_I" deadCode="false" sourceNode="P_114F10005" targetNode="P_42F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_358F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_358F10005_O" deadCode="false" sourceNode="P_114F10005" targetNode="P_43F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_358F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_360F10005_I" deadCode="false" sourceNode="P_114F10005" targetNode="P_20F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_360F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_360F10005_O" deadCode="false" sourceNode="P_114F10005" targetNode="P_21F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_360F10005"/>
	</edges>
	<edges id="P_114F10005P_115F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_114F10005" targetNode="P_115F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_364F10005_I" deadCode="false" sourceNode="P_76F10005" targetNode="P_48F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_364F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_364F10005_O" deadCode="false" sourceNode="P_76F10005" targetNode="P_49F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_364F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_365F10005_I" deadCode="false" sourceNode="P_76F10005" targetNode="P_50F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_365F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_365F10005_O" deadCode="false" sourceNode="P_76F10005" targetNode="P_51F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_365F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_367F10005_I" deadCode="false" sourceNode="P_76F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_367F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_367F10005_O" deadCode="false" sourceNode="P_76F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_367F10005"/>
	</edges>
	<edges id="P_76F10005P_77F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_76F10005" targetNode="P_77F10005"/>
	<edges id="P_40F10005P_41F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10005" targetNode="P_41F10005"/>
	<edges id="P_46F10005P_47F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10005" targetNode="P_47F10005"/>
	<edges id="P_48F10005P_49F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10005" targetNode="P_49F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_432F10005_I" deadCode="false" sourceNode="P_44F10005" targetNode="P_12F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_432F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_432F10005_O" deadCode="false" sourceNode="P_44F10005" targetNode="P_13F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_432F10005"/>
	</edges>
	<edges id="P_44F10005P_45F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10005" targetNode="P_45F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_436F10005_I" deadCode="false" sourceNode="P_50F10005" targetNode="P_126F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_436F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_436F10005_O" deadCode="false" sourceNode="P_50F10005" targetNode="P_127F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_436F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_440F10005_I" deadCode="false" sourceNode="P_50F10005" targetNode="P_126F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_440F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_440F10005_O" deadCode="false" sourceNode="P_50F10005" targetNode="P_127F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_440F10005"/>
	</edges>
	<edges id="P_50F10005P_51F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10005" targetNode="P_51F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_445F10005_I" deadCode="false" sourceNode="P_42F10005" targetNode="P_128F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_445F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_445F10005_O" deadCode="false" sourceNode="P_42F10005" targetNode="P_129F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_445F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_449F10005_I" deadCode="false" sourceNode="P_42F10005" targetNode="P_128F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_449F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_449F10005_O" deadCode="false" sourceNode="P_42F10005" targetNode="P_129F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_449F10005"/>
	</edges>
	<edges id="P_42F10005P_43F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10005" targetNode="P_43F10005"/>
	<edges id="P_52F10005P_53F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10005" targetNode="P_53F10005"/>
	<edges id="P_82F10005P_83F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_82F10005" targetNode="P_83F10005"/>
	<edges id="P_84F10005P_85F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_84F10005" targetNode="P_85F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_474F10005_I" deadCode="false" sourceNode="P_78F10005" targetNode="P_90F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_474F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_474F10005_O" deadCode="false" sourceNode="P_78F10005" targetNode="P_91F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_474F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_476F10005_I" deadCode="false" sourceNode="P_78F10005" targetNode="P_92F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_476F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_476F10005_O" deadCode="false" sourceNode="P_78F10005" targetNode="P_93F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_476F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_478F10005_I" deadCode="false" sourceNode="P_78F10005" targetNode="P_94F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_478F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_478F10005_O" deadCode="false" sourceNode="P_78F10005" targetNode="P_95F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_478F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_480F10005_I" deadCode="false" sourceNode="P_78F10005" targetNode="P_96F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_480F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_480F10005_O" deadCode="false" sourceNode="P_78F10005" targetNode="P_97F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_480F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_483F10005_I" deadCode="false" sourceNode="P_78F10005" targetNode="P_124F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_483F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_483F10005_O" deadCode="false" sourceNode="P_78F10005" targetNode="P_125F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_483F10005"/>
	</edges>
	<edges id="P_78F10005P_79F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_78F10005" targetNode="P_79F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_489F10005_I" deadCode="false" sourceNode="P_80F10005" targetNode="P_98F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_489F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_489F10005_O" deadCode="false" sourceNode="P_80F10005" targetNode="P_99F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_489F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_491F10005_I" deadCode="false" sourceNode="P_80F10005" targetNode="P_100F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_491F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_491F10005_O" deadCode="false" sourceNode="P_80F10005" targetNode="P_101F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_491F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_493F10005_I" deadCode="false" sourceNode="P_80F10005" targetNode="P_102F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_493F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_493F10005_O" deadCode="false" sourceNode="P_80F10005" targetNode="P_103F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_493F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_495F10005_I" deadCode="false" sourceNode="P_80F10005" targetNode="P_104F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_495F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_495F10005_O" deadCode="false" sourceNode="P_80F10005" targetNode="P_105F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_495F10005"/>
	</edges>
	<edges id="P_80F10005P_81F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_80F10005" targetNode="P_81F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_498F10005_I" deadCode="false" sourceNode="P_10F10005" targetNode="P_130F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_498F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_498F10005_O" deadCode="false" sourceNode="P_10F10005" targetNode="P_131F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_498F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_500F10005_I" deadCode="false" sourceNode="P_10F10005" targetNode="P_132F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_500F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_500F10005_O" deadCode="false" sourceNode="P_10F10005" targetNode="P_133F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_500F10005"/>
	</edges>
	<edges id="P_10F10005P_11F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10005" targetNode="P_11F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_505F10005_I" deadCode="true" sourceNode="P_130F10005" targetNode="P_134F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_505F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_505F10005_O" deadCode="true" sourceNode="P_130F10005" targetNode="P_135F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_505F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_510F10005_I" deadCode="true" sourceNode="P_130F10005" targetNode="P_134F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_510F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_510F10005_O" deadCode="true" sourceNode="P_130F10005" targetNode="P_135F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_510F10005"/>
	</edges>
	<edges id="P_130F10005P_131F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_130F10005" targetNode="P_131F10005"/>
	<edges id="P_132F10005P_133F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_132F10005" targetNode="P_133F10005"/>
	<edges id="P_134F10005P_135F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_134F10005" targetNode="P_135F10005"/>
	<edges id="P_126F10005P_127F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_126F10005" targetNode="P_127F10005"/>
	<edges xsi:type="cbl:PerformEdge" id="S_539F10005_I" deadCode="true" sourceNode="P_136F10005" targetNode="P_137F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_539F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_539F10005_O" deadCode="true" sourceNode="P_136F10005" targetNode="P_138F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_539F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_540F10005_I" deadCode="true" sourceNode="P_136F10005" targetNode="P_139F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_540F10005"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_540F10005_O" deadCode="true" sourceNode="P_136F10005" targetNode="P_140F10005">
		<representations href="../../../cobol/IABS0060.cbl.cobModel#S_540F10005"/>
	</edges>
	<edges id="P_128F10005P_129F10005" xsi:type="cbl:FallThroughEdge" sourceNode="P_128F10005" targetNode="P_129F10005"/>
	<edges xsi:type="cbl:DataEdge" id="S_63F10005_POS1" deadCode="false" targetNode="P_28F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_63F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_69F10005_POS1" deadCode="false" targetNode="P_30F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_69F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_75F10005_POS1" deadCode="false" targetNode="P_32F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_75F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_81F10005_POS1" deadCode="false" targetNode="P_34F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_81F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_87F10005_POS1" deadCode="false" targetNode="P_36F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_87F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_103F10005_POS1" deadCode="false" sourceNode="P_16F10005" targetNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_103F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_114F10005_POS1" deadCode="false" sourceNode="P_54F10005" targetNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_127F10005_POS1" deadCode="false" sourceNode="P_60F10005" targetNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_127F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_134F10005_POS1" deadCode="false" sourceNode="P_62F10005" targetNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_134F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_141F10005_POS1" deadCode="false" sourceNode="P_64F10005" targetNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_141F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_148F10005_POS1" deadCode="false" sourceNode="P_66F10005" targetNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_148F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_166F10005_POS1" deadCode="false" sourceNode="P_68F10005" targetNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_166F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_173F10005_POS1" deadCode="false" sourceNode="P_70F10005" targetNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_173F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_180F10005_POS1" deadCode="false" sourceNode="P_72F10005" targetNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_180F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_187F10005_POS1" deadCode="false" sourceNode="P_74F10005" targetNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_187F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_258F10005_POS1" deadCode="false" targetNode="P_106F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_258F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_268F10005_POS1" deadCode="false" targetNode="P_108F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_268F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_278F10005_POS1" deadCode="false" targetNode="P_110F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_278F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_288F10005_POS1" deadCode="false" targetNode="P_112F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_288F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_304F10005_POS1" deadCode="false" targetNode="P_116F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_304F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_314F10005_POS1" deadCode="false" targetNode="P_118F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_314F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_324F10005_POS1" deadCode="false" targetNode="P_120F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_324F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_334F10005_POS1" deadCode="false" targetNode="P_122F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_334F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_344F10005_POS1" deadCode="false" targetNode="P_38F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_344F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_354F10005_POS1" deadCode="false" targetNode="P_114F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_354F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_366F10005_POS1" deadCode="false" sourceNode="P_76F10005" targetNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_366F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_457F10005_POS1" deadCode="false" targetNode="P_82F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_457F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_458F10005_POS1" deadCode="false" targetNode="P_82F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_458F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_459F10005_POS1" deadCode="false" targetNode="P_82F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_459F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_460F10005_POS1" deadCode="false" targetNode="P_82F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_460F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_462F10005_POS1" deadCode="false" targetNode="P_82F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_462F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_467F10005_POS1" deadCode="false" targetNode="P_84F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_467F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_468F10005_POS1" deadCode="false" targetNode="P_84F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_468F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_469F10005_POS1" deadCode="false" targetNode="P_84F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_469F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_470F10005_POS1" deadCode="false" targetNode="P_84F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_470F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_475F10005_POS1" deadCode="false" targetNode="P_78F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_475F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_477F10005_POS1" deadCode="false" targetNode="P_78F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_477F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_479F10005_POS1" deadCode="false" targetNode="P_78F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_479F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_481F10005_POS1" deadCode="false" targetNode="P_78F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_481F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_484F10005_POS1" deadCode="false" targetNode="P_78F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_484F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_490F10005_POS1" deadCode="false" targetNode="P_80F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_490F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_492F10005_POS1" deadCode="false" targetNode="P_80F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_492F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_494F10005_POS1" deadCode="false" targetNode="P_80F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_494F10005"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_496F10005_POS1" deadCode="false" targetNode="P_80F10005" sourceNode="DB2_BTC_JOB_SCHEDULE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_496F10005"></representations>
	</edges>
</Package>

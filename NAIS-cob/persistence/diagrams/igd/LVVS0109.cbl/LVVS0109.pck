<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0109" cbl:id="LVVS0109" xsi:id="LVVS0109" packageRef="LVVS0109.igd#LVVS0109" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0109_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10349" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0109.cbl.cobModel#SC_1F10349"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10349" deadCode="false" name="PROGRAM_LVVS0109_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0109.cbl.cobModel#P_1F10349"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10349" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0109.cbl.cobModel#P_2F10349"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10349" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0109.cbl.cobModel#P_3F10349"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10349" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0109.cbl.cobModel#P_4F10349"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10349" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0109.cbl.cobModel#P_5F10349"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10349" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0109.cbl.cobModel#P_8F10349"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10349" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0109.cbl.cobModel#P_9F10349"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10349" deadCode="false" name="S1200-CONTRLLO-DATI">
				<representations href="../../../cobol/LVVS0109.cbl.cobModel#P_10F10349"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10349" deadCode="false" name="S1200-EX">
				<representations href="../../../cobol/LVVS0109.cbl.cobModel#P_11F10349"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10349" deadCode="false" name="S1260-CALCOLA-DIFF">
				<representations href="../../../cobol/LVVS0109.cbl.cobModel#P_12F10349"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10349" deadCode="false" name="S1260-EX">
				<representations href="../../../cobol/LVVS0109.cbl.cobModel#P_13F10349"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10349" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0109.cbl.cobModel#P_6F10349"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10349" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0109.cbl.cobModel#P_7F10349"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS1750" name="LCCS1750">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10138"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10349P_1F10349" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10349" targetNode="P_1F10349"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10349_I" deadCode="false" sourceNode="P_1F10349" targetNode="P_2F10349">
		<representations href="../../../cobol/LVVS0109.cbl.cobModel#S_1F10349"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10349_O" deadCode="false" sourceNode="P_1F10349" targetNode="P_3F10349">
		<representations href="../../../cobol/LVVS0109.cbl.cobModel#S_1F10349"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10349_I" deadCode="false" sourceNode="P_1F10349" targetNode="P_4F10349">
		<representations href="../../../cobol/LVVS0109.cbl.cobModel#S_2F10349"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10349_O" deadCode="false" sourceNode="P_1F10349" targetNode="P_5F10349">
		<representations href="../../../cobol/LVVS0109.cbl.cobModel#S_2F10349"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10349_I" deadCode="false" sourceNode="P_1F10349" targetNode="P_6F10349">
		<representations href="../../../cobol/LVVS0109.cbl.cobModel#S_3F10349"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10349_O" deadCode="false" sourceNode="P_1F10349" targetNode="P_7F10349">
		<representations href="../../../cobol/LVVS0109.cbl.cobModel#S_3F10349"/>
	</edges>
	<edges id="P_2F10349P_3F10349" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10349" targetNode="P_3F10349"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10349_I" deadCode="false" sourceNode="P_4F10349" targetNode="P_8F10349">
		<representations href="../../../cobol/LVVS0109.cbl.cobModel#S_10F10349"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10349_O" deadCode="false" sourceNode="P_4F10349" targetNode="P_9F10349">
		<representations href="../../../cobol/LVVS0109.cbl.cobModel#S_10F10349"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10349_I" deadCode="false" sourceNode="P_4F10349" targetNode="P_10F10349">
		<representations href="../../../cobol/LVVS0109.cbl.cobModel#S_12F10349"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10349_O" deadCode="false" sourceNode="P_4F10349" targetNode="P_11F10349">
		<representations href="../../../cobol/LVVS0109.cbl.cobModel#S_12F10349"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10349_I" deadCode="false" sourceNode="P_4F10349" targetNode="P_12F10349">
		<representations href="../../../cobol/LVVS0109.cbl.cobModel#S_14F10349"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10349_O" deadCode="false" sourceNode="P_4F10349" targetNode="P_13F10349">
		<representations href="../../../cobol/LVVS0109.cbl.cobModel#S_14F10349"/>
	</edges>
	<edges id="P_4F10349P_5F10349" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10349" targetNode="P_5F10349"/>
	<edges id="P_8F10349P_9F10349" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10349" targetNode="P_9F10349"/>
	<edges id="P_10F10349P_11F10349" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10349" targetNode="P_11F10349"/>
	<edges id="P_12F10349P_13F10349" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10349" targetNode="P_13F10349"/>
	<edges xsi:type="cbl:CallEdge" id="S_30F10349" deadCode="false" name="Dynamic LCCS1750" sourceNode="P_12F10349" targetNode="LCCS1750">
		<representations href="../../../cobol/../importantStmts.cobModel#S_30F10349"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0007" cbl:id="LVVS0007" xsi:id="LVVS0007" packageRef="LVVS0007.igd#LVVS0007" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0007_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10309" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0007.cbl.cobModel#SC_1F10309"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10309" deadCode="false" name="PROGRAM_LVVS0007_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_1F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10309" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_2F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10309" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_3F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10309" deadCode="false" name="S00010-INIZIA-TABELLE">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_8F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10309" deadCode="false" name="S00010-INIZIA-TABELLE-EX">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_9F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10309" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_4F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10309" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_5F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10309" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_12F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10309" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_13F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10309" deadCode="false" name="S1250-CALCOLA-DATA1">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_14F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10309" deadCode="false" name="S1250-EX">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_15F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10309" deadCode="false" name="S1300-ELABORA-COLL">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_16F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10309" deadCode="false" name="S1300-EX">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_17F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10309" deadCode="false" name="S1310-LEGGI-TRCH">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_18F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10309" deadCode="false" name="S1310-EX">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_19F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10309" deadCode="false" name="S1320-LEGGI-DTC">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_20F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10309" deadCode="false" name="S1320-EX">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_21F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10309" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_6F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10309" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_7F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10309" deadCode="false" name="VALORIZZA-OUTPUT-TGA">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_22F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10309" deadCode="false" name="VALORIZZA-OUTPUT-TGA-EX">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_23F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10309" deadCode="false" name="INIZIA-TOT-TGA">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_10F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10309" deadCode="false" name="INIZIA-TOT-TGA-EX">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_11F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10309" deadCode="true" name="INIZIA-NULL-TGA">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_28F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10309" deadCode="false" name="INIZIA-NULL-TGA-EX">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_29F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10309" deadCode="false" name="INIZIA-ZEROES-TGA">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_24F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10309" deadCode="false" name="INIZIA-ZEROES-TGA-EX">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_25F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10309" deadCode="true" name="INIZIA-SPACES-TGA">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_26F10309"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10309" deadCode="false" name="INIZIA-SPACES-TGA-EX">
				<representations href="../../../cobol/LVVS0007.cbl.cobModel#P_27F10309"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS1590" name="LDBS1590">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10163"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS3020" name="LDBS3020">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10195"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSDTC0" name="IDBSDTC0">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10034"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10309P_1F10309" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10309" targetNode="P_1F10309"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10309_I" deadCode="false" sourceNode="P_1F10309" targetNode="P_2F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_1F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10309_O" deadCode="false" sourceNode="P_1F10309" targetNode="P_3F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_1F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10309_I" deadCode="false" sourceNode="P_1F10309" targetNode="P_4F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_2F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10309_O" deadCode="false" sourceNode="P_1F10309" targetNode="P_5F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_2F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10309_I" deadCode="false" sourceNode="P_1F10309" targetNode="P_6F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_3F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10309_O" deadCode="false" sourceNode="P_1F10309" targetNode="P_7F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_3F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10309_I" deadCode="false" sourceNode="P_2F10309" targetNode="P_8F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_10F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10309_O" deadCode="false" sourceNode="P_2F10309" targetNode="P_9F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_10F10309"/>
	</edges>
	<edges id="P_2F10309P_3F10309" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10309" targetNode="P_3F10309"/>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10309_I" deadCode="false" sourceNode="P_8F10309" targetNode="P_10F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_13F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10309_O" deadCode="false" sourceNode="P_8F10309" targetNode="P_11F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_13F10309"/>
	</edges>
	<edges id="P_8F10309P_9F10309" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10309" targetNode="P_9F10309"/>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10309_I" deadCode="false" sourceNode="P_4F10309" targetNode="P_12F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_16F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10309_O" deadCode="false" sourceNode="P_4F10309" targetNode="P_13F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_16F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10309_I" deadCode="false" sourceNode="P_4F10309" targetNode="P_14F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_25F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10309_O" deadCode="false" sourceNode="P_4F10309" targetNode="P_15F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_25F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10309_I" deadCode="false" sourceNode="P_4F10309" targetNode="P_14F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_31F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10309_O" deadCode="false" sourceNode="P_4F10309" targetNode="P_15F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_31F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10309_I" deadCode="false" sourceNode="P_4F10309" targetNode="P_16F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_32F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10309_O" deadCode="false" sourceNode="P_4F10309" targetNode="P_17F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_32F10309"/>
	</edges>
	<edges id="P_4F10309P_5F10309" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10309" targetNode="P_5F10309"/>
	<edges id="P_12F10309P_13F10309" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10309" targetNode="P_13F10309"/>
	<edges id="P_14F10309P_15F10309" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10309" targetNode="P_15F10309"/>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10309_I" deadCode="false" sourceNode="P_16F10309" targetNode="P_18F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_50F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10309_O" deadCode="false" sourceNode="P_16F10309" targetNode="P_19F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_50F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10309_I" deadCode="false" sourceNode="P_16F10309" targetNode="P_20F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_51F10309"/>
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_52F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10309_O" deadCode="false" sourceNode="P_16F10309" targetNode="P_21F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_51F10309"/>
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_52F10309"/>
	</edges>
	<edges id="P_16F10309P_17F10309" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10309" targetNode="P_17F10309"/>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10309_I" deadCode="false" sourceNode="P_18F10309" targetNode="P_22F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_75F10309"/>
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_84F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10309_O" deadCode="false" sourceNode="P_18F10309" targetNode="P_23F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_75F10309"/>
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_84F10309"/>
	</edges>
	<edges id="P_18F10309P_19F10309" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10309" targetNode="P_19F10309"/>
	<edges id="P_20F10309P_21F10309" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10309" targetNode="P_21F10309"/>
	<edges id="P_22F10309P_23F10309" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10309" targetNode="P_23F10309"/>
	<edges xsi:type="cbl:PerformEdge" id="S_493F10309_I" deadCode="false" sourceNode="P_10F10309" targetNode="P_24F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_493F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_493F10309_O" deadCode="false" sourceNode="P_10F10309" targetNode="P_25F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_493F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_494F10309_I" deadCode="true" sourceNode="P_10F10309" targetNode="P_26F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_494F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_494F10309_O" deadCode="true" sourceNode="P_10F10309" targetNode="P_27F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_494F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_495F10309_I" deadCode="true" sourceNode="P_10F10309" targetNode="P_28F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_495F10309"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_495F10309_O" deadCode="true" sourceNode="P_10F10309" targetNode="P_29F10309">
		<representations href="../../../cobol/LVVS0007.cbl.cobModel#S_495F10309"/>
	</edges>
	<edges id="P_10F10309P_11F10309" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10309" targetNode="P_11F10309"/>
	<edges id="P_28F10309P_29F10309" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10309" targetNode="P_29F10309"/>
	<edges id="P_24F10309P_25F10309" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10309" targetNode="P_25F10309"/>
	<edges id="P_26F10309P_27F10309" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10309" targetNode="P_27F10309"/>
	<edges xsi:type="cbl:CallEdge" id="S_38F10309" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_14F10309" targetNode="LDBS1590">
		<representations href="../../../cobol/../importantStmts.cobModel#S_38F10309"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_76F10309" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_18F10309" targetNode="LDBS3020">
		<representations href="../../../cobol/../importantStmts.cobModel#S_76F10309"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_104F10309" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_20F10309" targetNode="IDBSDTC0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_104F10309"></representations>
	</edges>
</Package>

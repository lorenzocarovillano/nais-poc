<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS5140" cbl:id="LDBS5140" xsi:id="LDBS5140" packageRef="LDBS5140.igd#LDBS5140" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS5140_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10219" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS5140.cbl.cobModel#SC_1F10219"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10219" deadCode="false" name="PROGRAM_LDBS5140_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_1F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10219" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_2F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10219" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_3F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10219" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_12F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10219" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_13F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10219" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_4F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10219" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_5F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10219" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_6F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10219" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_7F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10219" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_8F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10219" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_9F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10219" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_44F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10219" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_49F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10219" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_14F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10219" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_15F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10219" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_16F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10219" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_17F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10219" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_18F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10219" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_19F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10219" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_20F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10219" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_21F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10219" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_22F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10219" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_23F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10219" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_56F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10219" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_57F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10219" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_24F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10219" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_25F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10219" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_26F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10219" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_27F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10219" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_28F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10219" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_29F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10219" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_30F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10219" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_31F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10219" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_32F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10219" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_33F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10219" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_58F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10219" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_59F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10219" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_34F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10219" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_35F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10219" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_36F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10219" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_37F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10219" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_38F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10219" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_39F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10219" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_40F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10219" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_41F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10219" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_42F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10219" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_43F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10219" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_50F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10219" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_51F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10219" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_60F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10219" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_63F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10219" deadCode="true" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_52F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10219" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_53F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10219" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_45F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10219" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_46F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10219" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_47F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10219" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_48F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10219" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_54F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10219" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_55F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10219" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_10F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10219" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_11F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10219" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_66F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10219" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_67F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10219" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_68F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10219" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_69F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10219" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_61F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10219" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_62F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10219" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_70F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10219" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_71F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10219" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_64F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10219" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_65F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10219" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_72F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10219" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_73F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10219" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_74F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10219" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_75F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10219" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_76F10219"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10219" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS5140.cbl.cobModel#P_77F10219"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_EST_TRCH_DI_GAR" name="EST_TRCH_DI_GAR">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_EST_TRCH_DI_GAR"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_STAT_OGG_BUS" name="STAT_OGG_BUS">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_STAT_OGG_BUS"/>
		</children>
	</packageNode>
	<edges id="SC_1F10219P_1F10219" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10219" targetNode="P_1F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10219_I" deadCode="false" sourceNode="P_1F10219" targetNode="P_2F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_1F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10219_O" deadCode="false" sourceNode="P_1F10219" targetNode="P_3F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_1F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10219_I" deadCode="false" sourceNode="P_1F10219" targetNode="P_4F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_5F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10219_O" deadCode="false" sourceNode="P_1F10219" targetNode="P_5F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_5F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10219_I" deadCode="false" sourceNode="P_1F10219" targetNode="P_6F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_9F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10219_O" deadCode="false" sourceNode="P_1F10219" targetNode="P_7F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_9F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10219_I" deadCode="false" sourceNode="P_1F10219" targetNode="P_8F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_13F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10219_O" deadCode="false" sourceNode="P_1F10219" targetNode="P_9F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_13F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10219_I" deadCode="false" sourceNode="P_2F10219" targetNode="P_10F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_22F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10219_O" deadCode="false" sourceNode="P_2F10219" targetNode="P_11F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_22F10219"/>
	</edges>
	<edges id="P_2F10219P_3F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10219" targetNode="P_3F10219"/>
	<edges id="P_12F10219P_13F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10219" targetNode="P_13F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10219_I" deadCode="false" sourceNode="P_4F10219" targetNode="P_14F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_35F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10219_O" deadCode="false" sourceNode="P_4F10219" targetNode="P_15F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_35F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10219_I" deadCode="false" sourceNode="P_4F10219" targetNode="P_16F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_36F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10219_O" deadCode="false" sourceNode="P_4F10219" targetNode="P_17F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_36F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10219_I" deadCode="false" sourceNode="P_4F10219" targetNode="P_18F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_37F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10219_O" deadCode="false" sourceNode="P_4F10219" targetNode="P_19F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_37F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10219_I" deadCode="false" sourceNode="P_4F10219" targetNode="P_20F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_38F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10219_O" deadCode="false" sourceNode="P_4F10219" targetNode="P_21F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_38F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10219_I" deadCode="false" sourceNode="P_4F10219" targetNode="P_22F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_39F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10219_O" deadCode="false" sourceNode="P_4F10219" targetNode="P_23F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_39F10219"/>
	</edges>
	<edges id="P_4F10219P_5F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10219" targetNode="P_5F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10219_I" deadCode="false" sourceNode="P_6F10219" targetNode="P_24F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_43F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10219_O" deadCode="false" sourceNode="P_6F10219" targetNode="P_25F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_43F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10219_I" deadCode="false" sourceNode="P_6F10219" targetNode="P_26F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_44F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10219_O" deadCode="false" sourceNode="P_6F10219" targetNode="P_27F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_44F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10219_I" deadCode="false" sourceNode="P_6F10219" targetNode="P_28F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_45F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10219_O" deadCode="false" sourceNode="P_6F10219" targetNode="P_29F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_45F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10219_I" deadCode="false" sourceNode="P_6F10219" targetNode="P_30F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_46F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10219_O" deadCode="false" sourceNode="P_6F10219" targetNode="P_31F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_46F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10219_I" deadCode="false" sourceNode="P_6F10219" targetNode="P_32F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_47F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10219_O" deadCode="false" sourceNode="P_6F10219" targetNode="P_33F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_47F10219"/>
	</edges>
	<edges id="P_6F10219P_7F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10219" targetNode="P_7F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10219_I" deadCode="false" sourceNode="P_8F10219" targetNode="P_34F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_51F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10219_O" deadCode="false" sourceNode="P_8F10219" targetNode="P_35F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_51F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10219_I" deadCode="false" sourceNode="P_8F10219" targetNode="P_36F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_52F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10219_O" deadCode="false" sourceNode="P_8F10219" targetNode="P_37F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_52F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10219_I" deadCode="false" sourceNode="P_8F10219" targetNode="P_38F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_53F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10219_O" deadCode="false" sourceNode="P_8F10219" targetNode="P_39F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_53F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10219_I" deadCode="false" sourceNode="P_8F10219" targetNode="P_40F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_54F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10219_O" deadCode="false" sourceNode="P_8F10219" targetNode="P_41F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_54F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10219_I" deadCode="false" sourceNode="P_8F10219" targetNode="P_42F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_55F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10219_O" deadCode="false" sourceNode="P_8F10219" targetNode="P_43F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_55F10219"/>
	</edges>
	<edges id="P_8F10219P_9F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10219" targetNode="P_9F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10219_I" deadCode="false" sourceNode="P_44F10219" targetNode="P_45F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_58F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10219_O" deadCode="false" sourceNode="P_44F10219" targetNode="P_46F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_58F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10219_I" deadCode="false" sourceNode="P_44F10219" targetNode="P_47F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_59F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10219_O" deadCode="false" sourceNode="P_44F10219" targetNode="P_48F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_59F10219"/>
	</edges>
	<edges id="P_44F10219P_49F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10219" targetNode="P_49F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10219_I" deadCode="false" sourceNode="P_14F10219" targetNode="P_45F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_62F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10219_O" deadCode="false" sourceNode="P_14F10219" targetNode="P_46F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_62F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10219_I" deadCode="false" sourceNode="P_14F10219" targetNode="P_47F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_63F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10219_O" deadCode="false" sourceNode="P_14F10219" targetNode="P_48F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_63F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10219_I" deadCode="false" sourceNode="P_14F10219" targetNode="P_12F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_65F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10219_O" deadCode="false" sourceNode="P_14F10219" targetNode="P_13F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_65F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10219_I" deadCode="false" sourceNode="P_14F10219" targetNode="P_50F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_67F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10219_O" deadCode="false" sourceNode="P_14F10219" targetNode="P_51F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_67F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10219_I" deadCode="true" sourceNode="P_14F10219" targetNode="P_52F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_68F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10219_O" deadCode="true" sourceNode="P_14F10219" targetNode="P_53F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_68F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10219_I" deadCode="false" sourceNode="P_14F10219" targetNode="P_54F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_69F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10219_O" deadCode="false" sourceNode="P_14F10219" targetNode="P_55F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_69F10219"/>
	</edges>
	<edges id="P_14F10219P_15F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10219" targetNode="P_15F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10219_I" deadCode="false" sourceNode="P_16F10219" targetNode="P_44F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_71F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10219_O" deadCode="false" sourceNode="P_16F10219" targetNode="P_49F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_71F10219"/>
	</edges>
	<edges id="P_16F10219P_17F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10219" targetNode="P_17F10219"/>
	<edges id="P_18F10219P_19F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10219" targetNode="P_19F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10219_I" deadCode="false" sourceNode="P_20F10219" targetNode="P_16F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_76F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10219_O" deadCode="false" sourceNode="P_20F10219" targetNode="P_17F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_76F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10219_I" deadCode="false" sourceNode="P_20F10219" targetNode="P_22F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_78F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10219_O" deadCode="false" sourceNode="P_20F10219" targetNode="P_23F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_78F10219"/>
	</edges>
	<edges id="P_20F10219P_21F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10219" targetNode="P_21F10219"/>
	<edges id="P_22F10219P_23F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10219" targetNode="P_23F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10219_I" deadCode="false" sourceNode="P_56F10219" targetNode="P_45F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_82F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10219_O" deadCode="false" sourceNode="P_56F10219" targetNode="P_46F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_82F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10219_I" deadCode="false" sourceNode="P_56F10219" targetNode="P_47F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_83F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10219_O" deadCode="false" sourceNode="P_56F10219" targetNode="P_48F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_83F10219"/>
	</edges>
	<edges id="P_56F10219P_57F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10219" targetNode="P_57F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10219_I" deadCode="false" sourceNode="P_24F10219" targetNode="P_45F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_86F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10219_O" deadCode="false" sourceNode="P_24F10219" targetNode="P_46F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_86F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10219_I" deadCode="false" sourceNode="P_24F10219" targetNode="P_47F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_87F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10219_O" deadCode="false" sourceNode="P_24F10219" targetNode="P_48F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_87F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10219_I" deadCode="false" sourceNode="P_24F10219" targetNode="P_12F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_89F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10219_O" deadCode="false" sourceNode="P_24F10219" targetNode="P_13F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_89F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10219_I" deadCode="false" sourceNode="P_24F10219" targetNode="P_50F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_91F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10219_O" deadCode="false" sourceNode="P_24F10219" targetNode="P_51F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_91F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10219_I" deadCode="true" sourceNode="P_24F10219" targetNode="P_52F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_92F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10219_O" deadCode="true" sourceNode="P_24F10219" targetNode="P_53F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_92F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10219_I" deadCode="false" sourceNode="P_24F10219" targetNode="P_54F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_93F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10219_O" deadCode="false" sourceNode="P_24F10219" targetNode="P_55F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_93F10219"/>
	</edges>
	<edges id="P_24F10219P_25F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10219" targetNode="P_25F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10219_I" deadCode="false" sourceNode="P_26F10219" targetNode="P_56F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_95F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10219_O" deadCode="false" sourceNode="P_26F10219" targetNode="P_57F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_95F10219"/>
	</edges>
	<edges id="P_26F10219P_27F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10219" targetNode="P_27F10219"/>
	<edges id="P_28F10219P_29F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10219" targetNode="P_29F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10219_I" deadCode="false" sourceNode="P_30F10219" targetNode="P_26F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_100F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10219_O" deadCode="false" sourceNode="P_30F10219" targetNode="P_27F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_100F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10219_I" deadCode="false" sourceNode="P_30F10219" targetNode="P_32F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_102F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10219_O" deadCode="false" sourceNode="P_30F10219" targetNode="P_33F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_102F10219"/>
	</edges>
	<edges id="P_30F10219P_31F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10219" targetNode="P_31F10219"/>
	<edges id="P_32F10219P_33F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10219" targetNode="P_33F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10219_I" deadCode="false" sourceNode="P_58F10219" targetNode="P_45F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_106F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10219_O" deadCode="false" sourceNode="P_58F10219" targetNode="P_46F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_106F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10219_I" deadCode="false" sourceNode="P_58F10219" targetNode="P_47F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_107F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10219_O" deadCode="false" sourceNode="P_58F10219" targetNode="P_48F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_107F10219"/>
	</edges>
	<edges id="P_58F10219P_59F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10219" targetNode="P_59F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10219_I" deadCode="false" sourceNode="P_34F10219" targetNode="P_45F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_110F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10219_O" deadCode="false" sourceNode="P_34F10219" targetNode="P_46F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_110F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10219_I" deadCode="false" sourceNode="P_34F10219" targetNode="P_47F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_111F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10219_O" deadCode="false" sourceNode="P_34F10219" targetNode="P_48F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_111F10219"/>
	</edges>
	<edges id="P_34F10219P_35F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10219" targetNode="P_35F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10219_I" deadCode="false" sourceNode="P_36F10219" targetNode="P_58F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_114F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10219_O" deadCode="false" sourceNode="P_36F10219" targetNode="P_59F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_114F10219"/>
	</edges>
	<edges id="P_36F10219P_37F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10219" targetNode="P_37F10219"/>
	<edges id="P_38F10219P_39F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10219" targetNode="P_39F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10219_I" deadCode="false" sourceNode="P_40F10219" targetNode="P_36F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_119F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10219_O" deadCode="false" sourceNode="P_40F10219" targetNode="P_37F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_119F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10219_I" deadCode="false" sourceNode="P_40F10219" targetNode="P_42F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_121F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10219_O" deadCode="false" sourceNode="P_40F10219" targetNode="P_43F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_121F10219"/>
	</edges>
	<edges id="P_40F10219P_41F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10219" targetNode="P_41F10219"/>
	<edges id="P_42F10219P_43F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10219" targetNode="P_43F10219"/>
	<edges id="P_50F10219P_51F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10219" targetNode="P_51F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10219_I" deadCode="true" sourceNode="P_60F10219" targetNode="P_61F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_142F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10219_O" deadCode="true" sourceNode="P_60F10219" targetNode="P_62F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_142F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10219_I" deadCode="true" sourceNode="P_60F10219" targetNode="P_61F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_145F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10219_O" deadCode="true" sourceNode="P_60F10219" targetNode="P_62F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_145F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10219_I" deadCode="true" sourceNode="P_60F10219" targetNode="P_61F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_149F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10219_O" deadCode="true" sourceNode="P_60F10219" targetNode="P_62F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_149F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10219_I" deadCode="true" sourceNode="P_60F10219" targetNode="P_61F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_152F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10219_O" deadCode="true" sourceNode="P_60F10219" targetNode="P_62F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_152F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_155F10219_I" deadCode="true" sourceNode="P_60F10219" targetNode="P_61F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_155F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_155F10219_O" deadCode="true" sourceNode="P_60F10219" targetNode="P_62F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_155F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_159F10219_I" deadCode="true" sourceNode="P_52F10219" targetNode="P_64F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_159F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_159F10219_O" deadCode="true" sourceNode="P_52F10219" targetNode="P_65F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_159F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10219_I" deadCode="true" sourceNode="P_52F10219" targetNode="P_64F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_162F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10219_O" deadCode="true" sourceNode="P_52F10219" targetNode="P_65F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_162F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10219_I" deadCode="true" sourceNode="P_52F10219" targetNode="P_64F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_166F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10219_O" deadCode="true" sourceNode="P_52F10219" targetNode="P_65F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_166F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_169F10219_I" deadCode="true" sourceNode="P_52F10219" targetNode="P_64F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_169F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_169F10219_O" deadCode="true" sourceNode="P_52F10219" targetNode="P_65F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_169F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10219_I" deadCode="true" sourceNode="P_52F10219" targetNode="P_64F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_172F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10219_O" deadCode="true" sourceNode="P_52F10219" targetNode="P_65F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_172F10219"/>
	</edges>
	<edges id="P_52F10219P_53F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10219" targetNode="P_53F10219"/>
	<edges id="P_45F10219P_46F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10219" targetNode="P_46F10219"/>
	<edges id="P_47F10219P_48F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10219" targetNode="P_48F10219"/>
	<edges id="P_54F10219P_55F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10219" targetNode="P_55F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10219_I" deadCode="false" sourceNode="P_10F10219" targetNode="P_66F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_181F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10219_O" deadCode="false" sourceNode="P_10F10219" targetNode="P_67F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_181F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_183F10219_I" deadCode="false" sourceNode="P_10F10219" targetNode="P_68F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_183F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_183F10219_O" deadCode="false" sourceNode="P_10F10219" targetNode="P_69F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_183F10219"/>
	</edges>
	<edges id="P_10F10219P_11F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10219" targetNode="P_11F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_188F10219_I" deadCode="false" sourceNode="P_66F10219" targetNode="P_61F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_188F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_188F10219_O" deadCode="false" sourceNode="P_66F10219" targetNode="P_62F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_188F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_193F10219_I" deadCode="false" sourceNode="P_66F10219" targetNode="P_61F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_193F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_193F10219_O" deadCode="false" sourceNode="P_66F10219" targetNode="P_62F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_193F10219"/>
	</edges>
	<edges id="P_66F10219P_67F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10219" targetNode="P_67F10219"/>
	<edges id="P_68F10219P_69F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10219" targetNode="P_69F10219"/>
	<edges id="P_61F10219P_62F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10219" targetNode="P_62F10219"/>
	<edges xsi:type="cbl:PerformEdge" id="S_222F10219_I" deadCode="true" sourceNode="P_64F10219" targetNode="P_72F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_222F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_222F10219_O" deadCode="true" sourceNode="P_64F10219" targetNode="P_73F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_222F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_223F10219_I" deadCode="true" sourceNode="P_64F10219" targetNode="P_74F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_223F10219"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_223F10219_O" deadCode="true" sourceNode="P_64F10219" targetNode="P_75F10219">
		<representations href="../../../cobol/LDBS5140.cbl.cobModel#S_223F10219"/>
	</edges>
	<edges id="P_64F10219P_65F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10219" targetNode="P_65F10219"/>
	<edges id="P_72F10219P_73F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10219" targetNode="P_73F10219"/>
	<edges id="P_74F10219P_75F10219" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10219" targetNode="P_75F10219"/>
	<edges xsi:type="cbl:DataEdge" id="S_64F10219_POS1" deadCode="false" targetNode="P_14F10219" sourceNode="DB2_EST_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_64F10219"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_64F10219_POS2" deadCode="false" targetNode="P_14F10219" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_64F10219"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_88F10219_POS1" deadCode="false" targetNode="P_24F10219" sourceNode="DB2_EST_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_88F10219"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_88F10219_POS2" deadCode="false" targetNode="P_24F10219" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_88F10219"></representations>
	</edges>
</Package>

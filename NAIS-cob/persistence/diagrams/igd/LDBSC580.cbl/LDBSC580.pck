<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBSC580" cbl:id="LDBSC580" xsi:id="LDBSC580" packageRef="LDBSC580.igd#LDBSC580" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBSC580_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10249" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBSC580.cbl.cobModel#SC_1F10249"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10249" deadCode="false" name="PROGRAM_LDBSC580_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_1F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10249" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_2F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10249" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_3F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10249" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_12F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10249" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_13F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10249" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_4F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10249" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_5F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10249" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_6F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10249" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_7F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10249" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_8F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10249" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_9F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10249" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_44F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10249" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_49F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10249" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_14F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10249" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_15F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10249" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_16F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10249" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_17F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10249" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_18F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10249" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_19F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10249" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_20F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10249" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_21F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10249" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_22F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10249" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_23F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10249" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_56F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10249" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_57F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10249" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_24F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10249" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_25F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10249" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_26F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10249" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_27F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10249" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_28F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10249" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_29F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10249" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_30F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10249" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_31F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10249" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_32F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10249" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_33F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10249" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_58F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10249" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_59F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10249" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_34F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10249" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_35F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10249" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_36F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10249" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_37F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10249" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_38F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10249" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_39F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10249" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_40F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10249" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_41F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10249" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_42F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10249" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_43F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10249" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_50F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10249" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_51F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10249" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_60F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10249" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_63F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10249" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_52F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10249" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_53F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10249" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_45F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10249" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_46F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10249" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_47F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10249" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_48F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10249" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_54F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10249" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_55F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10249" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_10F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10249" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_11F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10249" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_66F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10249" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_67F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10249" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_68F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10249" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_69F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10249" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_61F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10249" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_62F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10249" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_70F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10249" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_71F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10249" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_64F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10249" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_65F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10249" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_72F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10249" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_73F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10249" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_74F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10249" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_75F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10249" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_76F10249"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10249" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBSC580.cbl.cobModel#P_77F10249"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_STAT_OGG_BUS" name="STAT_OGG_BUS">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_STAT_OGG_BUS"/>
		</children>
	</packageNode>
	<edges id="SC_1F10249P_1F10249" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10249" targetNode="P_1F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10249_I" deadCode="false" sourceNode="P_1F10249" targetNode="P_2F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_1F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10249_O" deadCode="false" sourceNode="P_1F10249" targetNode="P_3F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_1F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10249_I" deadCode="false" sourceNode="P_1F10249" targetNode="P_4F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_5F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10249_O" deadCode="false" sourceNode="P_1F10249" targetNode="P_5F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_5F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10249_I" deadCode="false" sourceNode="P_1F10249" targetNode="P_6F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_9F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10249_O" deadCode="false" sourceNode="P_1F10249" targetNode="P_7F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_9F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10249_I" deadCode="false" sourceNode="P_1F10249" targetNode="P_8F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_13F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10249_O" deadCode="false" sourceNode="P_1F10249" targetNode="P_9F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_13F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10249_I" deadCode="false" sourceNode="P_2F10249" targetNode="P_10F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_22F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10249_O" deadCode="false" sourceNode="P_2F10249" targetNode="P_11F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_22F10249"/>
	</edges>
	<edges id="P_2F10249P_3F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10249" targetNode="P_3F10249"/>
	<edges id="P_12F10249P_13F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10249" targetNode="P_13F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10249_I" deadCode="false" sourceNode="P_4F10249" targetNode="P_14F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_35F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10249_O" deadCode="false" sourceNode="P_4F10249" targetNode="P_15F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_35F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10249_I" deadCode="false" sourceNode="P_4F10249" targetNode="P_16F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_36F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10249_O" deadCode="false" sourceNode="P_4F10249" targetNode="P_17F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_36F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10249_I" deadCode="false" sourceNode="P_4F10249" targetNode="P_18F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_37F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10249_O" deadCode="false" sourceNode="P_4F10249" targetNode="P_19F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_37F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10249_I" deadCode="false" sourceNode="P_4F10249" targetNode="P_20F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_38F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10249_O" deadCode="false" sourceNode="P_4F10249" targetNode="P_21F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_38F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10249_I" deadCode="false" sourceNode="P_4F10249" targetNode="P_22F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_39F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10249_O" deadCode="false" sourceNode="P_4F10249" targetNode="P_23F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_39F10249"/>
	</edges>
	<edges id="P_4F10249P_5F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10249" targetNode="P_5F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10249_I" deadCode="false" sourceNode="P_6F10249" targetNode="P_24F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_43F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10249_O" deadCode="false" sourceNode="P_6F10249" targetNode="P_25F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_43F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10249_I" deadCode="false" sourceNode="P_6F10249" targetNode="P_26F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_44F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10249_O" deadCode="false" sourceNode="P_6F10249" targetNode="P_27F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_44F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10249_I" deadCode="false" sourceNode="P_6F10249" targetNode="P_28F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_45F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10249_O" deadCode="false" sourceNode="P_6F10249" targetNode="P_29F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_45F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10249_I" deadCode="false" sourceNode="P_6F10249" targetNode="P_30F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_46F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10249_O" deadCode="false" sourceNode="P_6F10249" targetNode="P_31F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_46F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10249_I" deadCode="false" sourceNode="P_6F10249" targetNode="P_32F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_47F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10249_O" deadCode="false" sourceNode="P_6F10249" targetNode="P_33F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_47F10249"/>
	</edges>
	<edges id="P_6F10249P_7F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10249" targetNode="P_7F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10249_I" deadCode="false" sourceNode="P_8F10249" targetNode="P_34F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_51F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10249_O" deadCode="false" sourceNode="P_8F10249" targetNode="P_35F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_51F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10249_I" deadCode="false" sourceNode="P_8F10249" targetNode="P_36F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_52F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10249_O" deadCode="false" sourceNode="P_8F10249" targetNode="P_37F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_52F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10249_I" deadCode="false" sourceNode="P_8F10249" targetNode="P_38F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_53F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10249_O" deadCode="false" sourceNode="P_8F10249" targetNode="P_39F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_53F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10249_I" deadCode="false" sourceNode="P_8F10249" targetNode="P_40F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_54F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10249_O" deadCode="false" sourceNode="P_8F10249" targetNode="P_41F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_54F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10249_I" deadCode="false" sourceNode="P_8F10249" targetNode="P_42F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_55F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10249_O" deadCode="false" sourceNode="P_8F10249" targetNode="P_43F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_55F10249"/>
	</edges>
	<edges id="P_8F10249P_9F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10249" targetNode="P_9F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10249_I" deadCode="false" sourceNode="P_44F10249" targetNode="P_45F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_58F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10249_O" deadCode="false" sourceNode="P_44F10249" targetNode="P_46F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_58F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10249_I" deadCode="false" sourceNode="P_44F10249" targetNode="P_47F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_59F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10249_O" deadCode="false" sourceNode="P_44F10249" targetNode="P_48F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_59F10249"/>
	</edges>
	<edges id="P_44F10249P_49F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10249" targetNode="P_49F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10249_I" deadCode="false" sourceNode="P_14F10249" targetNode="P_45F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_63F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10249_O" deadCode="false" sourceNode="P_14F10249" targetNode="P_46F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_63F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10249_I" deadCode="false" sourceNode="P_14F10249" targetNode="P_47F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_64F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10249_O" deadCode="false" sourceNode="P_14F10249" targetNode="P_48F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_64F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10249_I" deadCode="false" sourceNode="P_14F10249" targetNode="P_12F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_66F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10249_O" deadCode="false" sourceNode="P_14F10249" targetNode="P_13F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_66F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10249_I" deadCode="false" sourceNode="P_14F10249" targetNode="P_50F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_68F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10249_O" deadCode="false" sourceNode="P_14F10249" targetNode="P_51F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_68F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10249_I" deadCode="false" sourceNode="P_14F10249" targetNode="P_52F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_69F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10249_O" deadCode="false" sourceNode="P_14F10249" targetNode="P_53F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_69F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10249_I" deadCode="false" sourceNode="P_14F10249" targetNode="P_54F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_70F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10249_O" deadCode="false" sourceNode="P_14F10249" targetNode="P_55F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_70F10249"/>
	</edges>
	<edges id="P_14F10249P_15F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10249" targetNode="P_15F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10249_I" deadCode="false" sourceNode="P_16F10249" targetNode="P_44F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_72F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10249_O" deadCode="false" sourceNode="P_16F10249" targetNode="P_49F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_72F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10249_I" deadCode="false" sourceNode="P_16F10249" targetNode="P_12F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_74F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10249_O" deadCode="false" sourceNode="P_16F10249" targetNode="P_13F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_74F10249"/>
	</edges>
	<edges id="P_16F10249P_17F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10249" targetNode="P_17F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10249_I" deadCode="false" sourceNode="P_18F10249" targetNode="P_12F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_77F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10249_O" deadCode="false" sourceNode="P_18F10249" targetNode="P_13F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_77F10249"/>
	</edges>
	<edges id="P_18F10249P_19F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10249" targetNode="P_19F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10249_I" deadCode="false" sourceNode="P_20F10249" targetNode="P_16F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_79F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10249_O" deadCode="false" sourceNode="P_20F10249" targetNode="P_17F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_79F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10249_I" deadCode="false" sourceNode="P_20F10249" targetNode="P_22F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_81F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10249_O" deadCode="false" sourceNode="P_20F10249" targetNode="P_23F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_81F10249"/>
	</edges>
	<edges id="P_20F10249P_21F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10249" targetNode="P_21F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10249_I" deadCode="false" sourceNode="P_22F10249" targetNode="P_12F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_84F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10249_O" deadCode="false" sourceNode="P_22F10249" targetNode="P_13F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_84F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10249_I" deadCode="false" sourceNode="P_22F10249" targetNode="P_50F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_86F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10249_O" deadCode="false" sourceNode="P_22F10249" targetNode="P_51F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_86F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10249_I" deadCode="false" sourceNode="P_22F10249" targetNode="P_52F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_87F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10249_O" deadCode="false" sourceNode="P_22F10249" targetNode="P_53F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_87F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10249_I" deadCode="false" sourceNode="P_22F10249" targetNode="P_54F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_88F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10249_O" deadCode="false" sourceNode="P_22F10249" targetNode="P_55F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_88F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10249_I" deadCode="false" sourceNode="P_22F10249" targetNode="P_18F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_90F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10249_O" deadCode="false" sourceNode="P_22F10249" targetNode="P_19F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_90F10249"/>
	</edges>
	<edges id="P_22F10249P_23F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10249" targetNode="P_23F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10249_I" deadCode="false" sourceNode="P_56F10249" targetNode="P_45F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_94F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10249_O" deadCode="false" sourceNode="P_56F10249" targetNode="P_46F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_94F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10249_I" deadCode="false" sourceNode="P_56F10249" targetNode="P_47F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_95F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10249_O" deadCode="false" sourceNode="P_56F10249" targetNode="P_48F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_95F10249"/>
	</edges>
	<edges id="P_56F10249P_57F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10249" targetNode="P_57F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10249_I" deadCode="false" sourceNode="P_24F10249" targetNode="P_45F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_99F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10249_O" deadCode="false" sourceNode="P_24F10249" targetNode="P_46F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_99F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10249_I" deadCode="false" sourceNode="P_24F10249" targetNode="P_47F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_100F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10249_O" deadCode="false" sourceNode="P_24F10249" targetNode="P_48F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_100F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10249_I" deadCode="false" sourceNode="P_24F10249" targetNode="P_12F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_102F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10249_O" deadCode="false" sourceNode="P_24F10249" targetNode="P_13F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_102F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10249_I" deadCode="false" sourceNode="P_24F10249" targetNode="P_50F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_104F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10249_O" deadCode="false" sourceNode="P_24F10249" targetNode="P_51F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_104F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10249_I" deadCode="false" sourceNode="P_24F10249" targetNode="P_52F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_105F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10249_O" deadCode="false" sourceNode="P_24F10249" targetNode="P_53F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_105F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10249_I" deadCode="false" sourceNode="P_24F10249" targetNode="P_54F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_106F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10249_O" deadCode="false" sourceNode="P_24F10249" targetNode="P_55F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_106F10249"/>
	</edges>
	<edges id="P_24F10249P_25F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10249" targetNode="P_25F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10249_I" deadCode="false" sourceNode="P_26F10249" targetNode="P_56F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_108F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10249_O" deadCode="false" sourceNode="P_26F10249" targetNode="P_57F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_108F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10249_I" deadCode="false" sourceNode="P_26F10249" targetNode="P_12F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_110F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10249_O" deadCode="false" sourceNode="P_26F10249" targetNode="P_13F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_110F10249"/>
	</edges>
	<edges id="P_26F10249P_27F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10249" targetNode="P_27F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10249_I" deadCode="false" sourceNode="P_28F10249" targetNode="P_12F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_113F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10249_O" deadCode="false" sourceNode="P_28F10249" targetNode="P_13F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_113F10249"/>
	</edges>
	<edges id="P_28F10249P_29F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10249" targetNode="P_29F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10249_I" deadCode="false" sourceNode="P_30F10249" targetNode="P_26F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_115F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10249_O" deadCode="false" sourceNode="P_30F10249" targetNode="P_27F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_115F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10249_I" deadCode="false" sourceNode="P_30F10249" targetNode="P_32F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_117F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10249_O" deadCode="false" sourceNode="P_30F10249" targetNode="P_33F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_117F10249"/>
	</edges>
	<edges id="P_30F10249P_31F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10249" targetNode="P_31F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10249_I" deadCode="false" sourceNode="P_32F10249" targetNode="P_12F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_120F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10249_O" deadCode="false" sourceNode="P_32F10249" targetNode="P_13F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_120F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10249_I" deadCode="false" sourceNode="P_32F10249" targetNode="P_50F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_122F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10249_O" deadCode="false" sourceNode="P_32F10249" targetNode="P_51F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_122F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10249_I" deadCode="false" sourceNode="P_32F10249" targetNode="P_52F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_123F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10249_O" deadCode="false" sourceNode="P_32F10249" targetNode="P_53F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_123F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10249_I" deadCode="false" sourceNode="P_32F10249" targetNode="P_54F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_124F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10249_O" deadCode="false" sourceNode="P_32F10249" targetNode="P_55F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_124F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10249_I" deadCode="false" sourceNode="P_32F10249" targetNode="P_28F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_126F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10249_O" deadCode="false" sourceNode="P_32F10249" targetNode="P_29F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_126F10249"/>
	</edges>
	<edges id="P_32F10249P_33F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10249" targetNode="P_33F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10249_I" deadCode="false" sourceNode="P_58F10249" targetNode="P_45F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_130F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10249_O" deadCode="false" sourceNode="P_58F10249" targetNode="P_46F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_130F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10249_I" deadCode="false" sourceNode="P_58F10249" targetNode="P_47F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_131F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10249_O" deadCode="false" sourceNode="P_58F10249" targetNode="P_48F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_131F10249"/>
	</edges>
	<edges id="P_58F10249P_59F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10249" targetNode="P_59F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_134F10249_I" deadCode="false" sourceNode="P_34F10249" targetNode="P_45F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_134F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_134F10249_O" deadCode="false" sourceNode="P_34F10249" targetNode="P_46F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_134F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10249_I" deadCode="false" sourceNode="P_34F10249" targetNode="P_47F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_135F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10249_O" deadCode="false" sourceNode="P_34F10249" targetNode="P_48F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_135F10249"/>
	</edges>
	<edges id="P_34F10249P_35F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10249" targetNode="P_35F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10249_I" deadCode="false" sourceNode="P_36F10249" targetNode="P_58F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_138F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10249_O" deadCode="false" sourceNode="P_36F10249" targetNode="P_59F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_138F10249"/>
	</edges>
	<edges id="P_36F10249P_37F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10249" targetNode="P_37F10249"/>
	<edges id="P_38F10249P_39F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10249" targetNode="P_39F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10249_I" deadCode="false" sourceNode="P_40F10249" targetNode="P_36F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_143F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10249_O" deadCode="false" sourceNode="P_40F10249" targetNode="P_37F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_143F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10249_I" deadCode="false" sourceNode="P_40F10249" targetNode="P_42F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_145F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10249_O" deadCode="false" sourceNode="P_40F10249" targetNode="P_43F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_145F10249"/>
	</edges>
	<edges id="P_40F10249P_41F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10249" targetNode="P_41F10249"/>
	<edges id="P_42F10249P_43F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10249" targetNode="P_43F10249"/>
	<edges id="P_50F10249P_51F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10249" targetNode="P_51F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10249_I" deadCode="true" sourceNode="P_60F10249" targetNode="P_61F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_154F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10249_O" deadCode="true" sourceNode="P_60F10249" targetNode="P_62F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_154F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10249_I" deadCode="true" sourceNode="P_60F10249" targetNode="P_61F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_157F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10249_O" deadCode="true" sourceNode="P_60F10249" targetNode="P_62F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_157F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10249_I" deadCode="false" sourceNode="P_52F10249" targetNode="P_64F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_161F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10249_O" deadCode="false" sourceNode="P_52F10249" targetNode="P_65F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_161F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10249_I" deadCode="false" sourceNode="P_52F10249" targetNode="P_64F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_164F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10249_O" deadCode="false" sourceNode="P_52F10249" targetNode="P_65F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_164F10249"/>
	</edges>
	<edges id="P_52F10249P_53F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10249" targetNode="P_53F10249"/>
	<edges id="P_45F10249P_46F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10249" targetNode="P_46F10249"/>
	<edges id="P_47F10249P_48F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10249" targetNode="P_48F10249"/>
	<edges id="P_54F10249P_55F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10249" targetNode="P_55F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10249_I" deadCode="false" sourceNode="P_10F10249" targetNode="P_66F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_173F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10249_O" deadCode="false" sourceNode="P_10F10249" targetNode="P_67F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_173F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10249_I" deadCode="false" sourceNode="P_10F10249" targetNode="P_68F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_175F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10249_O" deadCode="false" sourceNode="P_10F10249" targetNode="P_69F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_175F10249"/>
	</edges>
	<edges id="P_10F10249P_11F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10249" targetNode="P_11F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_180F10249_I" deadCode="false" sourceNode="P_66F10249" targetNode="P_61F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_180F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_180F10249_O" deadCode="false" sourceNode="P_66F10249" targetNode="P_62F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_180F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10249_I" deadCode="false" sourceNode="P_66F10249" targetNode="P_61F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_185F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10249_O" deadCode="false" sourceNode="P_66F10249" targetNode="P_62F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_185F10249"/>
	</edges>
	<edges id="P_66F10249P_67F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10249" targetNode="P_67F10249"/>
	<edges id="P_68F10249P_69F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10249" targetNode="P_69F10249"/>
	<edges id="P_61F10249P_62F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10249" targetNode="P_62F10249"/>
	<edges xsi:type="cbl:PerformEdge" id="S_214F10249_I" deadCode="false" sourceNode="P_64F10249" targetNode="P_72F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_214F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_214F10249_O" deadCode="false" sourceNode="P_64F10249" targetNode="P_73F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_214F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_215F10249_I" deadCode="false" sourceNode="P_64F10249" targetNode="P_74F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_215F10249"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_215F10249_O" deadCode="false" sourceNode="P_64F10249" targetNode="P_75F10249">
		<representations href="../../../cobol/LDBSC580.cbl.cobModel#S_215F10249"/>
	</edges>
	<edges id="P_64F10249P_65F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10249" targetNode="P_65F10249"/>
	<edges id="P_72F10249P_73F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10249" targetNode="P_73F10249"/>
	<edges id="P_74F10249P_75F10249" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10249" targetNode="P_75F10249"/>
	<edges xsi:type="cbl:DataEdge" id="S_65F10249_POS1" deadCode="false" targetNode="P_14F10249" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_65F10249"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_73F10249_POS1" deadCode="false" targetNode="P_16F10249" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_73F10249"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_76F10249_POS1" deadCode="false" targetNode="P_18F10249" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_76F10249"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_83F10249_POS1" deadCode="false" targetNode="P_22F10249" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_83F10249"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_101F10249_POS1" deadCode="false" targetNode="P_24F10249" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_101F10249"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_109F10249_POS1" deadCode="false" targetNode="P_26F10249" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_109F10249"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_112F10249_POS1" deadCode="false" targetNode="P_28F10249" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_112F10249"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_119F10249_POS1" deadCode="false" targetNode="P_32F10249" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_119F10249"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0012" cbl:id="LVVS0012" xsi:id="LVVS0012" packageRef="LVVS0012.igd#LVVS0012" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0012_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10313" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0012.cbl.cobModel#SC_1F10313"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10313" deadCode="false" name="PROGRAM_LVVS0012_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0012.cbl.cobModel#P_1F10313"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10313" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0012.cbl.cobModel#P_2F10313"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10313" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0012.cbl.cobModel#P_3F10313"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10313" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0012.cbl.cobModel#P_4F10313"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10313" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0012.cbl.cobModel#P_5F10313"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10313" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0012.cbl.cobModel#P_8F10313"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10313" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0012.cbl.cobModel#P_9F10313"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10313" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS0012.cbl.cobModel#P_10F10313"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10313" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS0012.cbl.cobModel#P_11F10313"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10313" deadCode="false" name="S1300-CALL-LVVS0000">
				<representations href="../../../cobol/LVVS0012.cbl.cobModel#P_12F10313"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10313" deadCode="false" name="S1300-CALL-LVVS0000-EX">
				<representations href="../../../cobol/LVVS0012.cbl.cobModel#P_13F10313"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10313" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0012.cbl.cobModel#P_6F10313"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10313" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0012.cbl.cobModel#P_7F10313"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LVVS0000" name="LVVS0000">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10304"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10313P_1F10313" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10313" targetNode="P_1F10313"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10313_I" deadCode="false" sourceNode="P_1F10313" targetNode="P_2F10313">
		<representations href="../../../cobol/LVVS0012.cbl.cobModel#S_1F10313"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10313_O" deadCode="false" sourceNode="P_1F10313" targetNode="P_3F10313">
		<representations href="../../../cobol/LVVS0012.cbl.cobModel#S_1F10313"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10313_I" deadCode="false" sourceNode="P_1F10313" targetNode="P_4F10313">
		<representations href="../../../cobol/LVVS0012.cbl.cobModel#S_2F10313"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10313_O" deadCode="false" sourceNode="P_1F10313" targetNode="P_5F10313">
		<representations href="../../../cobol/LVVS0012.cbl.cobModel#S_2F10313"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10313_I" deadCode="false" sourceNode="P_1F10313" targetNode="P_6F10313">
		<representations href="../../../cobol/LVVS0012.cbl.cobModel#S_3F10313"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10313_O" deadCode="false" sourceNode="P_1F10313" targetNode="P_7F10313">
		<representations href="../../../cobol/LVVS0012.cbl.cobModel#S_3F10313"/>
	</edges>
	<edges id="P_2F10313P_3F10313" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10313" targetNode="P_3F10313"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10313_I" deadCode="false" sourceNode="P_4F10313" targetNode="P_8F10313">
		<representations href="../../../cobol/LVVS0012.cbl.cobModel#S_10F10313"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10313_O" deadCode="false" sourceNode="P_4F10313" targetNode="P_9F10313">
		<representations href="../../../cobol/LVVS0012.cbl.cobModel#S_10F10313"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10313_I" deadCode="false" sourceNode="P_4F10313" targetNode="P_10F10313">
		<representations href="../../../cobol/LVVS0012.cbl.cobModel#S_12F10313"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10313_O" deadCode="false" sourceNode="P_4F10313" targetNode="P_11F10313">
		<representations href="../../../cobol/LVVS0012.cbl.cobModel#S_12F10313"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10313_I" deadCode="false" sourceNode="P_4F10313" targetNode="P_12F10313">
		<representations href="../../../cobol/LVVS0012.cbl.cobModel#S_14F10313"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10313_O" deadCode="false" sourceNode="P_4F10313" targetNode="P_13F10313">
		<representations href="../../../cobol/LVVS0012.cbl.cobModel#S_14F10313"/>
	</edges>
	<edges id="P_4F10313P_5F10313" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10313" targetNode="P_5F10313"/>
	<edges id="P_8F10313P_9F10313" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10313" targetNode="P_9F10313"/>
	<edges id="P_10F10313P_11F10313" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10313" targetNode="P_11F10313"/>
	<edges id="P_12F10313P_13F10313" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10313" targetNode="P_13F10313"/>
	<edges xsi:type="cbl:CallEdge" id="S_42F10313" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_12F10313" targetNode="LVVS0000">
		<representations href="../../../cobol/../importantStmts.cobModel#S_42F10313"></representations>
	</edges>
</Package>

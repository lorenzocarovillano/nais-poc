<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS3420" cbl:id="LVVS3420" xsi:id="LVVS3420" packageRef="LVVS3420.igd#LVVS3420" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS3420_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10392" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS3420.cbl.cobModel#SC_1F10392"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10392" deadCode="false" name="PROGRAM_LVVS3420_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS3420.cbl.cobModel#P_1F10392"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10392" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS3420.cbl.cobModel#P_2F10392"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10392" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS3420.cbl.cobModel#P_3F10392"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10392" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS3420.cbl.cobModel#P_4F10392"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10392" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS3420.cbl.cobModel#P_5F10392"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10392" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS3420.cbl.cobModel#P_8F10392"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10392" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS3420.cbl.cobModel#P_9F10392"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10392" deadCode="false" name="DETERMINA-MOVI">
				<representations href="../../../cobol/LVVS3420.cbl.cobModel#P_10F10392"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10392" deadCode="false" name="DETERMINA-MOVI-EX">
				<representations href="../../../cobol/LVVS3420.cbl.cobModel#P_11F10392"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10392" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS3420.cbl.cobModel#P_6F10392"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10392" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS3420.cbl.cobModel#P_7F10392"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10392P_1F10392" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10392" targetNode="P_1F10392"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10392_I" deadCode="false" sourceNode="P_1F10392" targetNode="P_2F10392">
		<representations href="../../../cobol/LVVS3420.cbl.cobModel#S_1F10392"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10392_O" deadCode="false" sourceNode="P_1F10392" targetNode="P_3F10392">
		<representations href="../../../cobol/LVVS3420.cbl.cobModel#S_1F10392"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10392_I" deadCode="false" sourceNode="P_1F10392" targetNode="P_4F10392">
		<representations href="../../../cobol/LVVS3420.cbl.cobModel#S_2F10392"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10392_O" deadCode="false" sourceNode="P_1F10392" targetNode="P_5F10392">
		<representations href="../../../cobol/LVVS3420.cbl.cobModel#S_2F10392"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10392_I" deadCode="false" sourceNode="P_1F10392" targetNode="P_6F10392">
		<representations href="../../../cobol/LVVS3420.cbl.cobModel#S_3F10392"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10392_O" deadCode="false" sourceNode="P_1F10392" targetNode="P_7F10392">
		<representations href="../../../cobol/LVVS3420.cbl.cobModel#S_3F10392"/>
	</edges>
	<edges id="P_2F10392P_3F10392" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10392" targetNode="P_3F10392"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10392_I" deadCode="false" sourceNode="P_4F10392" targetNode="P_8F10392">
		<representations href="../../../cobol/LVVS3420.cbl.cobModel#S_10F10392"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10392_O" deadCode="false" sourceNode="P_4F10392" targetNode="P_9F10392">
		<representations href="../../../cobol/LVVS3420.cbl.cobModel#S_10F10392"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10392_I" deadCode="false" sourceNode="P_4F10392" targetNode="P_10F10392">
		<representations href="../../../cobol/LVVS3420.cbl.cobModel#S_11F10392"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10392_O" deadCode="false" sourceNode="P_4F10392" targetNode="P_11F10392">
		<representations href="../../../cobol/LVVS3420.cbl.cobModel#S_11F10392"/>
	</edges>
	<edges id="P_4F10392P_5F10392" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10392" targetNode="P_5F10392"/>
	<edges id="P_8F10392P_9F10392" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10392" targetNode="P_9F10392"/>
	<edges id="P_10F10392P_11F10392" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10392" targetNode="P_11F10392"/>
</Package>

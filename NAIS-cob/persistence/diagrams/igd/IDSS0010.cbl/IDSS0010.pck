<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IDSS0010" cbl:id="IDSS0010" xsi:id="IDSS0010" packageRef="IDSS0010.igd#IDSS0010" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IDSS0010_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10099" deadCode="false" name="MAIN">
			<representations href="../../../cobol/IDSS0010.cbl.cobModel#SC_1F10099"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10099" deadCode="false" name="MAIN_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0010.cbl.cobModel#P_1F10099"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_2F10099" deadCode="false" name="A000-INIZIALIZZA-OUTPUT">
			<representations href="../../../cobol/IDSS0010.cbl.cobModel#SC_2F10099"/>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10099" deadCode="false" name="A000-INIZIALIZZA-OUTPUT_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0010.cbl.cobModel#P_8F10099"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10099" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IDSS0010.cbl.cobModel#P_9F10099"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_3F10099" deadCode="false" name="A005-CONTROLLO-INPUT">
			<representations href="../../../cobol/IDSS0010.cbl.cobModel#SC_3F10099"/>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10099" deadCode="false" name="A005-CONTROLLO-INPUT_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0010.cbl.cobModel#P_10F10099"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10099" deadCode="false" name="A005-EX">
				<representations href="../../../cobol/IDSS0010.cbl.cobModel#P_11F10099"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_4F10099" deadCode="false" name="S2000-CERCA-NOME">
			<representations href="../../../cobol/IDSS0010.cbl.cobModel#SC_4F10099"/>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10099" deadCode="false" name="S2000-CERCA-NOME_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0010.cbl.cobModel#P_12F10099"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10099" deadCode="false" name="EX-S2000">
				<representations href="../../../cobol/IDSS0010.cbl.cobModel#P_4F10099"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_5F10099" deadCode="false" name="S3000-CALL-PGM">
			<representations href="../../../cobol/IDSS0010.cbl.cobModel#SC_5F10099"/>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10099" deadCode="false" name="S3000-CALL-PGM_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0010.cbl.cobModel#P_15F10099"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10099" deadCode="false" name="EX-S3000">
				<representations href="../../../cobol/IDSS0010.cbl.cobModel#P_7F10099"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10099" deadCode="false" name="S4000-SCRIVI-ERRORE">
				<representations href="../../../cobol/IDSS0010.cbl.cobModel#P_13F10099"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10099" deadCode="false" name="EX-S4000">
				<representations href="../../../cobol/IDSS0010.cbl.cobModel#P_14F10099"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10099" deadCode="false" name="S5000-VALOR-AREA">
				<representations href="../../../cobol/IDSS0010.cbl.cobModel#P_5F10099"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10099" deadCode="false" name="EX-S5000">
				<representations href="../../../cobol/IDSS0010.cbl.cobModel#P_6F10099"/>
			</children>
		</children>
		<children xsi:type="cbl:SectionNode" id="SC_6F10099" deadCode="false" name="4000-FINE">
			<representations href="../../../cobol/IDSS0010.cbl.cobModel#SC_6F10099"/>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10099" deadCode="false" name="4000-FINE_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0010.cbl.cobModel#P_16F10099"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10099" deadCode="true" name="4000-EX-FINE">
				<representations href="../../../cobol/IDSS0010.cbl.cobModel#P_17F10099"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10099" deadCode="true" name="4010-TIMESTAMP">
				<representations href="../../../cobol/IDSS0010.cbl.cobModel#P_18F10099"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10099" deadCode="true" name="4010-EX-FINE">
				<representations href="../../../cobol/IDSS0010.cbl.cobModel#P_19F10099"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10099" deadCode="false" name="ESEGUI-DISPLAY">
				<representations href="../../../cobol/IDSS0010.cbl.cobModel#P_2F10099"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10099" deadCode="false" name="ESEGUI-DISPLAY-EX">
				<representations href="../../../cobol/IDSS0010.cbl.cobModel#P_3F10099"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSA010" name="IDBSA010" missing="true">
			<representations href="../../../../missing.xmi#ID24KWVLYD1LV1D0AWEG4TRTEOXEVTCZONUEJ04SN1EDXFJ53ADEKM"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSA690" name="IDBSA690" missing="true">
			<representations href="../../../../missing.xmi#ID5DXMRBXQWGKBITZUMKMBPSKGWNGJB5L2URLGAIL0FP44XHHDXJAO"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSA700" name="IDBSA700" missing="true">
			<representations href="../../../../missing.xmi#ID0FCRM1EQYNUXDWFLOJMTJOZKTKYVOV1231LOSLO1C2GMKTX5BEDO"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSA710" name="IDBSA710" missing="true">
			<representations href="../../../../missing.xmi#IDXJPEERF24CLQMR3443SYNKU0IK103VA1SSW2VH20QROQTWIRSVN"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSA720" name="IDBSA720" missing="true">
			<representations href="../../../../missing.xmi#ID3SQB41VF3FW1D4Z0FLTAWFQVEFLLFAGUSRCBARF511D0BCJG40AO"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSA730" name="IDBSA730" missing="true">
			<representations href="../../../../missing.xmi#ID2BJYOM152SNDKKXZOZRBOK5SYCGUVSVBNQXHCJOYTWOEWJQICVFB"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSA740" name="IDBSA740" missing="true">
			<representations href="../../../../missing.xmi#IDDJAHJSKE0TGADMSRLF0MVJHH5M11WE4IKDR0VVHG4ESNZSCEGEKN"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSA750" name="IDBSA750" missing="true">
			<representations href="../../../../missing.xmi#IDOAIJDMDDATPOOOBENLU0ELGEGIOFIOMSKC1ELBMT3XXHJ31PPHLC"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSA760" name="IDBSA760" missing="true">
			<representations href="../../../../missing.xmi#IDLPO3MDSGNYXKEQIF3KGVJAFA4GDF2ADPXMZFZBJ53KORYV2PSLTO"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSAAZ0" name="IDBSAAZ0" missing="true">
			<representations href="../../../../missing.xmi#IDYA0DOM34YYS4P3CKRWHUQZM15IUXWSBJ1TVNK0IAZSV4EFSL5JCL"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSADE0" name="IDBSADE0">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10016"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSADI0" name="IDBSADI0" missing="true">
			<representations href="../../../../missing.xmi#IDUJXAUATXNROGBQOSMELTJHRPLKMEAFKGAJHE5UEJMODNGHKYKTHP"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSAMT0" name="IDBSAMT0" missing="true">
			<representations href="../../../../missing.xmi#ID3MZW4OXFNYRFSNBK4X1GH13QNSIWQFFFRBLCVDL33MWGXQ2I1OL"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSL030" name="IDBSL030" missing="true">
			<representations href="../../../../missing.xmi#IDJUNEKKMYJBLRNRI4OUOGP4O5EFJJ0UUEV01AP2RGQL5COKC1QT"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSL050" name="IDBSL050">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10042"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSL090" name="IDBSL090" missing="true">
			<representations href="../../../../missing.xmi#IDKULONQWJE1FGNTPFHOCVMVB5FCZE0XDCQDNEH3KXPYGMF4UMSS4N"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSL160" name="IDBSL160">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10044"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSM010" name="IDBSM010" missing="true">
			<representations href="../../../../missing.xmi#IDNJCV5KYQPFB3NKIYBZXNYEDKUJXGV4ZNTUOYSDF0QQQKPBEFBCWF"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSP630" name="IDBSP630">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10066"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSXAB0" name="IDBSXAB0" missing="true">
			<representations href="../../../../missing.xmi#IDACJZX3ZE1NIJGNDKYZBNNNHVQLECVFJZWB2HD4DRA0J1TULKQ1OI"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IJCSTMSP" name="IJCSTMSP" missing="true">
			<representations href="../../../../missing.xmi#ID4GAQVPM0Z12ED05MPMC3Q3PJLIIF0PGQOOXCDHDOPSGQEJV4ENDL"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS8880" name="IDSS8880">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10106"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10099P_1F10099" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10099" targetNode="P_1F10099"/>
	<edges id="SC_2F10099P_8F10099" xsi:type="cbl:FallThroughEdge" sourceNode="SC_2F10099" targetNode="P_8F10099"/>
	<edges id="SC_3F10099P_10F10099" xsi:type="cbl:FallThroughEdge" sourceNode="SC_3F10099" targetNode="P_10F10099"/>
	<edges id="SC_4F10099P_12F10099" xsi:type="cbl:FallThroughEdge" sourceNode="SC_4F10099" targetNode="P_12F10099"/>
	<edges id="SC_5F10099P_15F10099" xsi:type="cbl:FallThroughEdge" sourceNode="SC_5F10099" targetNode="P_15F10099"/>
	<edges id="SC_6F10099P_16F10099" xsi:type="cbl:FallThroughEdge" sourceNode="SC_6F10099" targetNode="P_16F10099"/>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10099_I" deadCode="false" sourceNode="P_1F10099" targetNode="P_2F10099">
		<representations href="../../../cobol/IDSS0010.cbl.cobModel#S_8F10099"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10099_O" deadCode="false" sourceNode="P_1F10099" targetNode="P_3F10099">
		<representations href="../../../cobol/IDSS0010.cbl.cobModel#S_8F10099"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10099_I" deadCode="false" sourceNode="P_1F10099" targetNode="SC_2F10099">
		<representations href="../../../cobol/IDSS0010.cbl.cobModel#S_9F10099"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10099_I" deadCode="false" sourceNode="P_1F10099" targetNode="SC_3F10099">
		<representations href="../../../cobol/IDSS0010.cbl.cobModel#S_10F10099"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10099_I" deadCode="false" sourceNode="P_1F10099" targetNode="SC_4F10099">
		<representations href="../../../cobol/IDSS0010.cbl.cobModel#S_12F10099"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10099_O" deadCode="false" sourceNode="P_1F10099" targetNode="P_4F10099">
		<representations href="../../../cobol/IDSS0010.cbl.cobModel#S_12F10099"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10099_I" deadCode="false" sourceNode="P_1F10099" targetNode="P_5F10099">
		<representations href="../../../cobol/IDSS0010.cbl.cobModel#S_14F10099"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10099_O" deadCode="false" sourceNode="P_1F10099" targetNode="P_6F10099">
		<representations href="../../../cobol/IDSS0010.cbl.cobModel#S_14F10099"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10099_I" deadCode="false" sourceNode="P_1F10099" targetNode="SC_5F10099">
		<representations href="../../../cobol/IDSS0010.cbl.cobModel#S_15F10099"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10099_O" deadCode="false" sourceNode="P_1F10099" targetNode="P_7F10099">
		<representations href="../../../cobol/IDSS0010.cbl.cobModel#S_15F10099"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10099_I" deadCode="false" sourceNode="P_1F10099" targetNode="P_2F10099">
		<representations href="../../../cobol/IDSS0010.cbl.cobModel#S_23F10099"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10099_O" deadCode="false" sourceNode="P_1F10099" targetNode="P_3F10099">
		<representations href="../../../cobol/IDSS0010.cbl.cobModel#S_23F10099"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10099_I" deadCode="false" sourceNode="P_1F10099" targetNode="SC_6F10099">
		<representations href="../../../cobol/IDSS0010.cbl.cobModel#S_24F10099"/>
	</edges>
	<edges id="P_8F10099P_9F10099" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10099" targetNode="P_9F10099"/>
	<edges id="P_10F10099P_11F10099" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10099" targetNode="P_11F10099"/>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10099_I" deadCode="false" sourceNode="P_12F10099" targetNode="P_13F10099">
		<representations href="../../../cobol/IDSS0010.cbl.cobModel#S_42F10099"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10099_O" deadCode="false" sourceNode="P_12F10099" targetNode="P_14F10099">
		<representations href="../../../cobol/IDSS0010.cbl.cobModel#S_42F10099"/>
	</edges>
	<edges id="P_12F10099P_4F10099" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10099" targetNode="P_4F10099"/>
	<edges id="P_15F10099P_7F10099" xsi:type="cbl:FallThroughEdge" sourceNode="P_15F10099" targetNode="P_7F10099"/>
	<edges id="P_13F10099P_14F10099" xsi:type="cbl:FallThroughEdge" sourceNode="P_13F10099" targetNode="P_14F10099"/>
	<edges id="P_5F10099P_6F10099" xsi:type="cbl:FallThroughEdge" sourceNode="P_5F10099" targetNode="P_6F10099"/>
	<edges id="P_2F10099P_3F10099" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10099" targetNode="P_3F10099"/>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="AGE-BANK            IDBSA010" sourceNode="P_15F10099" targetNode="IDBSA010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099_2" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="ANA-QAV-MSC         IDBSA690" sourceNode="P_15F10099" targetNode="IDBSA690">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099_3" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="ANA-QAV-REDDITO     IDBSA700" sourceNode="P_15F10099" targetNode="IDBSA700">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099_4" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="ANA-QAV-KYC         IDBSA710" sourceNode="P_15F10099" targetNode="IDBSA710">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099_5" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="ANA-QAV-ANAG        IDBSA720" sourceNode="P_15F10099" targetNode="IDBSA720">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099_6" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="ANA-QAV-FT-AEOI     IDBSA730" sourceNode="P_15F10099" targetNode="IDBSA730">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099_7" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="ANA-QAV-PAT         IDBSA740" sourceNode="P_15F10099" targetNode="IDBSA740">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099_8" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="ANA-QAV-LISTE       IDBSA750" sourceNode="P_15F10099" targetNode="IDBSA750">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099_9" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="ANA-QAV-PROF-RS     IDBSA760" sourceNode="P_15F10099" targetNode="IDBSA760">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099_10" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="ANA-AZ              IDBSAAZ0" sourceNode="P_15F10099" targetNode="IDBSAAZ0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099_11" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="ADES                IDBSADE0" sourceNode="P_15F10099" targetNode="IDBSADE0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099_12" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="ANA-DOCTI-ISTR      IDBSADI0" sourceNode="P_15F10099" targetNode="IDBSADI0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099_13" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="ANA-MATR            IDBSAMT0" sourceNode="P_15F10099" targetNode="IDBSAMT0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099_14" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="ANA-CNTRL-ADEGZ     IDBSL030" sourceNode="P_15F10099" targetNode="IDBSL030">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099_15" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="AMMB-FUNZ-FUNZ      IDBSL050" sourceNode="P_15F10099" targetNode="IDBSL050">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099_16" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="ANA-COMP-COAS-RIAS  IDBSL090" sourceNode="P_15F10099" targetNode="IDBSL090">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099_17" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="AMMB-FUNZ-BLOCCO    IDBSL160" sourceNode="P_15F10099" targetNode="IDBSL160">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099_18" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="ANA-INTERF-MON      IDBSM010" sourceNode="P_15F10099" targetNode="IDBSM010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099_19" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="ACC-COMM            IDBSP630" sourceNode="P_15F10099" targetNode="IDBSP630">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10099_20" deadCode="false" name="Dynamic WS-NOME-PROG" targetName="ANA-BLOCCO          IDBSXAB0" sourceNode="P_15F10099" targetNode="IDBSXAB0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_95F10099" deadCode="false" name="Dynamic IDSV8888-CALL-TIMESTAMP" sourceNode="P_2F10099" targetNode="IJCSTMSP">
		<representations href="../../../cobol/../importantStmts.cobModel#S_95F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_97F10099" deadCode="false" name="Dynamic IDSV8888-CALL-DISPLAY" sourceNode="P_2F10099" targetNode="IDSS8880">
		<representations href="../../../cobol/../importantStmts.cobModel#S_97F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_107F10099" deadCode="false" name="Dynamic IDSV8888-CALL-TIMESTAMP" sourceNode="P_2F10099" targetNode="IJCSTMSP">
		<representations href="../../../cobol/../importantStmts.cobModel#S_107F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_109F10099" deadCode="false" name="Dynamic IDSV8888-CALL-DISPLAY" sourceNode="P_2F10099" targetNode="IDSS8880">
		<representations href="../../../cobol/../importantStmts.cobModel#S_109F10099"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_114F10099" deadCode="false" name="Dynamic IDSV8888-CALL-DISPLAY" sourceNode="P_2F10099" targetNode="IDSS8880">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10099"></representations>
	</edges>
</Package>

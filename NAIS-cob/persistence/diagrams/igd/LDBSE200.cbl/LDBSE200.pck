<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBSE200" cbl:id="LDBSE200" xsi:id="LDBSE200" packageRef="LDBSE200.igd#LDBSE200" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBSE200_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10259" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBSE200.cbl.cobModel#SC_1F10259"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10259" deadCode="false" name="PROGRAM_LDBSE200_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_1F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10259" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_2F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10259" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_3F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10259" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_12F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10259" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_13F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10259" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_4F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10259" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_5F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10259" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_6F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10259" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_7F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10259" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_8F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10259" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_9F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10259" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_44F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10259" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_49F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10259" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_14F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10259" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_15F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10259" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_16F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10259" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_17F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10259" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_18F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10259" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_19F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10259" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_20F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10259" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_21F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10259" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_22F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10259" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_23F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10259" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_50F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10259" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_51F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10259" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_24F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10259" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_25F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10259" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_26F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10259" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_27F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10259" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_28F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10259" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_29F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10259" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_30F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10259" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_31F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10259" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_32F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10259" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_33F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10259" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_52F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10259" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_53F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10259" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_34F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10259" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_35F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10259" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_36F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10259" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_37F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10259" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_38F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10259" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_39F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10259" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_40F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10259" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_41F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10259" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_42F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10259" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_43F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10259" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_54F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10259" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_55F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10259" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_60F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10259" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_61F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10259" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_56F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10259" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_57F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10259" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_45F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10259" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_46F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10259" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_47F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10259" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_48F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10259" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_58F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10259" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_59F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10259" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_10F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10259" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_11F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10259" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_62F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10259" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_63F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10259" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_64F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10259" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_65F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10259" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_66F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10259" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_67F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10259" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_68F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10259" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_69F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10259" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_70F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10259" deadCode="true" name="Z800-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_75F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10259" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_71F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10259" deadCode="true" name="Z810-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_72F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10259" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_73F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10259" deadCode="true" name="Z820-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_74F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10259" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_76F10259"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10259" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBSE200.cbl.cobModel#P_77F10259"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_AMMB_FUNZ_BLOCCO" name="AMMB_FUNZ_BLOCCO">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_AMMB_FUNZ_BLOCCO"/>
		</children>
	</packageNode>
	<edges id="SC_1F10259P_1F10259" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10259" targetNode="P_1F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10259_I" deadCode="false" sourceNode="P_1F10259" targetNode="P_2F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_1F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10259_O" deadCode="false" sourceNode="P_1F10259" targetNode="P_3F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_1F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10259_I" deadCode="false" sourceNode="P_1F10259" targetNode="P_4F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_5F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10259_O" deadCode="false" sourceNode="P_1F10259" targetNode="P_5F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_5F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10259_I" deadCode="false" sourceNode="P_1F10259" targetNode="P_6F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_9F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10259_O" deadCode="false" sourceNode="P_1F10259" targetNode="P_7F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_9F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10259_I" deadCode="false" sourceNode="P_1F10259" targetNode="P_8F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_13F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10259_O" deadCode="false" sourceNode="P_1F10259" targetNode="P_9F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_13F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10259_I" deadCode="false" sourceNode="P_2F10259" targetNode="P_10F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_22F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10259_O" deadCode="false" sourceNode="P_2F10259" targetNode="P_11F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_22F10259"/>
	</edges>
	<edges id="P_2F10259P_3F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10259" targetNode="P_3F10259"/>
	<edges id="P_12F10259P_13F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10259" targetNode="P_13F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10259_I" deadCode="false" sourceNode="P_4F10259" targetNode="P_14F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_35F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10259_O" deadCode="false" sourceNode="P_4F10259" targetNode="P_15F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_35F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10259_I" deadCode="false" sourceNode="P_4F10259" targetNode="P_16F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_36F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10259_O" deadCode="false" sourceNode="P_4F10259" targetNode="P_17F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_36F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10259_I" deadCode="false" sourceNode="P_4F10259" targetNode="P_18F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_37F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10259_O" deadCode="false" sourceNode="P_4F10259" targetNode="P_19F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_37F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10259_I" deadCode="false" sourceNode="P_4F10259" targetNode="P_20F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_38F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10259_O" deadCode="false" sourceNode="P_4F10259" targetNode="P_21F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_38F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10259_I" deadCode="false" sourceNode="P_4F10259" targetNode="P_22F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_39F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10259_O" deadCode="false" sourceNode="P_4F10259" targetNode="P_23F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_39F10259"/>
	</edges>
	<edges id="P_4F10259P_5F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10259" targetNode="P_5F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10259_I" deadCode="false" sourceNode="P_6F10259" targetNode="P_24F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_43F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10259_O" deadCode="false" sourceNode="P_6F10259" targetNode="P_25F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_43F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10259_I" deadCode="false" sourceNode="P_6F10259" targetNode="P_26F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_44F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10259_O" deadCode="false" sourceNode="P_6F10259" targetNode="P_27F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_44F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10259_I" deadCode="false" sourceNode="P_6F10259" targetNode="P_28F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_45F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10259_O" deadCode="false" sourceNode="P_6F10259" targetNode="P_29F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_45F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10259_I" deadCode="false" sourceNode="P_6F10259" targetNode="P_30F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_46F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10259_O" deadCode="false" sourceNode="P_6F10259" targetNode="P_31F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_46F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10259_I" deadCode="false" sourceNode="P_6F10259" targetNode="P_32F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_47F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10259_O" deadCode="false" sourceNode="P_6F10259" targetNode="P_33F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_47F10259"/>
	</edges>
	<edges id="P_6F10259P_7F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10259" targetNode="P_7F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10259_I" deadCode="false" sourceNode="P_8F10259" targetNode="P_34F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_51F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10259_O" deadCode="false" sourceNode="P_8F10259" targetNode="P_35F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_51F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10259_I" deadCode="false" sourceNode="P_8F10259" targetNode="P_36F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_52F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10259_O" deadCode="false" sourceNode="P_8F10259" targetNode="P_37F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_52F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10259_I" deadCode="false" sourceNode="P_8F10259" targetNode="P_38F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_53F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10259_O" deadCode="false" sourceNode="P_8F10259" targetNode="P_39F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_53F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10259_I" deadCode="false" sourceNode="P_8F10259" targetNode="P_40F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_54F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10259_O" deadCode="false" sourceNode="P_8F10259" targetNode="P_41F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_54F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10259_I" deadCode="false" sourceNode="P_8F10259" targetNode="P_42F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_55F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10259_O" deadCode="false" sourceNode="P_8F10259" targetNode="P_43F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_55F10259"/>
	</edges>
	<edges id="P_8F10259P_9F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10259" targetNode="P_9F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10259_I" deadCode="false" sourceNode="P_44F10259" targetNode="P_45F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_58F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10259_O" deadCode="false" sourceNode="P_44F10259" targetNode="P_46F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_58F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10259_I" deadCode="false" sourceNode="P_44F10259" targetNode="P_47F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_59F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10259_O" deadCode="false" sourceNode="P_44F10259" targetNode="P_48F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_59F10259"/>
	</edges>
	<edges id="P_44F10259P_49F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10259" targetNode="P_49F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10259_I" deadCode="false" sourceNode="P_14F10259" targetNode="P_45F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_62F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10259_O" deadCode="false" sourceNode="P_14F10259" targetNode="P_46F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_62F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10259_I" deadCode="false" sourceNode="P_14F10259" targetNode="P_47F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_63F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10259_O" deadCode="false" sourceNode="P_14F10259" targetNode="P_48F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_63F10259"/>
	</edges>
	<edges id="P_14F10259P_15F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10259" targetNode="P_15F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10259_I" deadCode="false" sourceNode="P_16F10259" targetNode="P_44F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_66F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10259_O" deadCode="false" sourceNode="P_16F10259" targetNode="P_49F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_66F10259"/>
	</edges>
	<edges id="P_16F10259P_17F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10259" targetNode="P_17F10259"/>
	<edges id="P_18F10259P_19F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10259" targetNode="P_19F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10259_I" deadCode="false" sourceNode="P_20F10259" targetNode="P_16F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_71F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10259_O" deadCode="false" sourceNode="P_20F10259" targetNode="P_17F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_71F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10259_I" deadCode="false" sourceNode="P_20F10259" targetNode="P_22F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_73F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10259_O" deadCode="false" sourceNode="P_20F10259" targetNode="P_23F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_73F10259"/>
	</edges>
	<edges id="P_20F10259P_21F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10259" targetNode="P_21F10259"/>
	<edges id="P_22F10259P_23F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10259" targetNode="P_23F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10259_I" deadCode="false" sourceNode="P_50F10259" targetNode="P_45F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_77F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10259_O" deadCode="false" sourceNode="P_50F10259" targetNode="P_46F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_77F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10259_I" deadCode="false" sourceNode="P_50F10259" targetNode="P_47F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_78F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10259_O" deadCode="false" sourceNode="P_50F10259" targetNode="P_48F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_78F10259"/>
	</edges>
	<edges id="P_50F10259P_51F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10259" targetNode="P_51F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10259_I" deadCode="false" sourceNode="P_24F10259" targetNode="P_45F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_81F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10259_O" deadCode="false" sourceNode="P_24F10259" targetNode="P_46F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_81F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10259_I" deadCode="false" sourceNode="P_24F10259" targetNode="P_47F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_82F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10259_O" deadCode="false" sourceNode="P_24F10259" targetNode="P_48F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_82F10259"/>
	</edges>
	<edges id="P_24F10259P_25F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10259" targetNode="P_25F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10259_I" deadCode="false" sourceNode="P_26F10259" targetNode="P_50F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_85F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10259_O" deadCode="false" sourceNode="P_26F10259" targetNode="P_51F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_85F10259"/>
	</edges>
	<edges id="P_26F10259P_27F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10259" targetNode="P_27F10259"/>
	<edges id="P_28F10259P_29F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10259" targetNode="P_29F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10259_I" deadCode="false" sourceNode="P_30F10259" targetNode="P_26F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_90F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10259_O" deadCode="false" sourceNode="P_30F10259" targetNode="P_27F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_90F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10259_I" deadCode="false" sourceNode="P_30F10259" targetNode="P_32F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_92F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10259_O" deadCode="false" sourceNode="P_30F10259" targetNode="P_33F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_92F10259"/>
	</edges>
	<edges id="P_30F10259P_31F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10259" targetNode="P_31F10259"/>
	<edges id="P_32F10259P_33F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10259" targetNode="P_33F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10259_I" deadCode="false" sourceNode="P_52F10259" targetNode="P_45F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_96F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10259_O" deadCode="false" sourceNode="P_52F10259" targetNode="P_46F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_96F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10259_I" deadCode="false" sourceNode="P_52F10259" targetNode="P_47F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_97F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10259_O" deadCode="false" sourceNode="P_52F10259" targetNode="P_48F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_97F10259"/>
	</edges>
	<edges id="P_52F10259P_53F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10259" targetNode="P_53F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10259_I" deadCode="false" sourceNode="P_34F10259" targetNode="P_45F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_101F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10259_O" deadCode="false" sourceNode="P_34F10259" targetNode="P_46F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_101F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10259_I" deadCode="false" sourceNode="P_34F10259" targetNode="P_47F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_102F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10259_O" deadCode="false" sourceNode="P_34F10259" targetNode="P_48F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_102F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10259_I" deadCode="false" sourceNode="P_34F10259" targetNode="P_12F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_104F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10259_O" deadCode="false" sourceNode="P_34F10259" targetNode="P_13F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_104F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10259_I" deadCode="false" sourceNode="P_34F10259" targetNode="P_54F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_106F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10259_O" deadCode="false" sourceNode="P_34F10259" targetNode="P_55F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_106F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10259_I" deadCode="false" sourceNode="P_34F10259" targetNode="P_56F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_107F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10259_O" deadCode="false" sourceNode="P_34F10259" targetNode="P_57F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_107F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10259_I" deadCode="false" sourceNode="P_34F10259" targetNode="P_58F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_108F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10259_O" deadCode="false" sourceNode="P_34F10259" targetNode="P_59F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_108F10259"/>
	</edges>
	<edges id="P_34F10259P_35F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10259" targetNode="P_35F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10259_I" deadCode="false" sourceNode="P_36F10259" targetNode="P_52F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_110F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10259_O" deadCode="false" sourceNode="P_36F10259" targetNode="P_53F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_110F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10259_I" deadCode="false" sourceNode="P_36F10259" targetNode="P_12F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_112F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10259_O" deadCode="false" sourceNode="P_36F10259" targetNode="P_13F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_112F10259"/>
	</edges>
	<edges id="P_36F10259P_37F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10259" targetNode="P_37F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10259_I" deadCode="false" sourceNode="P_38F10259" targetNode="P_12F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_115F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10259_O" deadCode="false" sourceNode="P_38F10259" targetNode="P_13F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_115F10259"/>
	</edges>
	<edges id="P_38F10259P_39F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10259" targetNode="P_39F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10259_I" deadCode="false" sourceNode="P_40F10259" targetNode="P_36F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_117F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10259_O" deadCode="false" sourceNode="P_40F10259" targetNode="P_37F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_117F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10259_I" deadCode="false" sourceNode="P_40F10259" targetNode="P_42F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_119F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10259_O" deadCode="false" sourceNode="P_40F10259" targetNode="P_43F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_119F10259"/>
	</edges>
	<edges id="P_40F10259P_41F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10259" targetNode="P_41F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10259_I" deadCode="false" sourceNode="P_42F10259" targetNode="P_12F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_122F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10259_O" deadCode="false" sourceNode="P_42F10259" targetNode="P_13F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_122F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10259_I" deadCode="false" sourceNode="P_42F10259" targetNode="P_54F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_124F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10259_O" deadCode="false" sourceNode="P_42F10259" targetNode="P_55F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_124F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10259_I" deadCode="false" sourceNode="P_42F10259" targetNode="P_56F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_125F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10259_O" deadCode="false" sourceNode="P_42F10259" targetNode="P_57F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_125F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10259_I" deadCode="false" sourceNode="P_42F10259" targetNode="P_58F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_126F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10259_O" deadCode="false" sourceNode="P_42F10259" targetNode="P_59F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_126F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10259_I" deadCode="false" sourceNode="P_42F10259" targetNode="P_38F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_128F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10259_O" deadCode="false" sourceNode="P_42F10259" targetNode="P_39F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_128F10259"/>
	</edges>
	<edges id="P_42F10259P_43F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10259" targetNode="P_43F10259"/>
	<edges id="P_54F10259P_55F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10259" targetNode="P_55F10259"/>
	<edges id="P_56F10259P_57F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10259" targetNode="P_57F10259"/>
	<edges id="P_45F10259P_46F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10259" targetNode="P_46F10259"/>
	<edges id="P_47F10259P_48F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10259" targetNode="P_48F10259"/>
	<edges id="P_58F10259P_59F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10259" targetNode="P_59F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10259_I" deadCode="false" sourceNode="P_10F10259" targetNode="P_62F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_146F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10259_O" deadCode="false" sourceNode="P_10F10259" targetNode="P_63F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_146F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10259_I" deadCode="false" sourceNode="P_10F10259" targetNode="P_64F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_148F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10259_O" deadCode="false" sourceNode="P_10F10259" targetNode="P_65F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_148F10259"/>
	</edges>
	<edges id="P_10F10259P_11F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10259" targetNode="P_11F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10259_I" deadCode="true" sourceNode="P_62F10259" targetNode="P_66F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_153F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10259_O" deadCode="true" sourceNode="P_62F10259" targetNode="P_67F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_153F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10259_I" deadCode="true" sourceNode="P_62F10259" targetNode="P_66F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_158F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10259_O" deadCode="true" sourceNode="P_62F10259" targetNode="P_67F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_158F10259"/>
	</edges>
	<edges id="P_62F10259P_63F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10259" targetNode="P_63F10259"/>
	<edges id="P_64F10259P_65F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10259" targetNode="P_65F10259"/>
	<edges id="P_66F10259P_67F10259" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10259" targetNode="P_67F10259"/>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10259_I" deadCode="true" sourceNode="P_70F10259" targetNode="P_71F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_187F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10259_O" deadCode="true" sourceNode="P_70F10259" targetNode="P_72F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_187F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_188F10259_I" deadCode="true" sourceNode="P_70F10259" targetNode="P_73F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_188F10259"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_188F10259_O" deadCode="true" sourceNode="P_70F10259" targetNode="P_74F10259">
		<representations href="../../../cobol/LDBSE200.cbl.cobModel#S_188F10259"/>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_103F10259_POS1" deadCode="false" targetNode="P_34F10259" sourceNode="DB2_AMMB_FUNZ_BLOCCO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_103F10259"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10259_POS1" deadCode="false" targetNode="P_36F10259" sourceNode="DB2_AMMB_FUNZ_BLOCCO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10259"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_114F10259_POS1" deadCode="false" targetNode="P_38F10259" sourceNode="DB2_AMMB_FUNZ_BLOCCO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10259"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_121F10259_POS1" deadCode="false" targetNode="P_42F10259" sourceNode="DB2_AMMB_FUNZ_BLOCCO">
		<representations href="../../../cobol/../importantStmts.cobModel#S_121F10259"></representations>
	</edges>
</Package>

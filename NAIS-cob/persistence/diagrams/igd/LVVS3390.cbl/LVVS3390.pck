<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS3390" cbl:id="LVVS3390" xsi:id="LVVS3390" packageRef="LVVS3390.igd#LVVS3390" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS3390_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10391" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS3390.cbl.cobModel#SC_1F10391"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10391" deadCode="false" name="PROGRAM_LVVS3390_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS3390.cbl.cobModel#P_1F10391"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10391" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS3390.cbl.cobModel#P_2F10391"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10391" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS3390.cbl.cobModel#P_3F10391"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10391" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS3390.cbl.cobModel#P_4F10391"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10391" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS3390.cbl.cobModel#P_5F10391"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10391" deadCode="true" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS3390.cbl.cobModel#P_8F10391"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10391" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS3390.cbl.cobModel#P_9F10391"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10391" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS3390.cbl.cobModel#P_10F10391"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10391" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS3390.cbl.cobModel#P_11F10391"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10391" deadCode="false" name="S1400-CHIAMA-LDBSF220">
				<representations href="../../../cobol/LVVS3390.cbl.cobModel#P_12F10391"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10391" deadCode="false" name="S1400-CHIAMA-LDBSF220-EX">
				<representations href="../../../cobol/LVVS3390.cbl.cobModel#P_13F10391"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10391" deadCode="false" name="S1410-PREP-TIT-CONT">
				<representations href="../../../cobol/LVVS3390.cbl.cobModel#P_14F10391"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10391" deadCode="false" name="S1410-PREP-TIT-CONT-EX">
				<representations href="../../../cobol/LVVS3390.cbl.cobModel#P_15F10391"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10391" deadCode="false" name="S1450-CALL-TIT-CONT">
				<representations href="../../../cobol/LVVS3390.cbl.cobModel#P_16F10391"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10391" deadCode="false" name="S1450-CALL-TIT-CONT-EX">
				<representations href="../../../cobol/LVVS3390.cbl.cobModel#P_17F10391"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10391" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS3390.cbl.cobModel#P_6F10391"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10391" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS3390.cbl.cobModel#P_7F10391"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSF220" name="LDBSF220">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10266"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSTIT0" name="IDBSTIT0">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10095"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10391P_1F10391" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10391" targetNode="P_1F10391"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10391_I" deadCode="false" sourceNode="P_1F10391" targetNode="P_2F10391">
		<representations href="../../../cobol/LVVS3390.cbl.cobModel#S_1F10391"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10391_O" deadCode="false" sourceNode="P_1F10391" targetNode="P_3F10391">
		<representations href="../../../cobol/LVVS3390.cbl.cobModel#S_1F10391"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10391_I" deadCode="false" sourceNode="P_1F10391" targetNode="P_4F10391">
		<representations href="../../../cobol/LVVS3390.cbl.cobModel#S_2F10391"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10391_O" deadCode="false" sourceNode="P_1F10391" targetNode="P_5F10391">
		<representations href="../../../cobol/LVVS3390.cbl.cobModel#S_2F10391"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10391_I" deadCode="false" sourceNode="P_1F10391" targetNode="P_6F10391">
		<representations href="../../../cobol/LVVS3390.cbl.cobModel#S_3F10391"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10391_O" deadCode="false" sourceNode="P_1F10391" targetNode="P_7F10391">
		<representations href="../../../cobol/LVVS3390.cbl.cobModel#S_3F10391"/>
	</edges>
	<edges id="P_2F10391P_3F10391" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10391" targetNode="P_3F10391"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10391_I" deadCode="true" sourceNode="P_4F10391" targetNode="P_8F10391">
		<representations href="../../../cobol/LVVS3390.cbl.cobModel#S_10F10391"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10391_O" deadCode="true" sourceNode="P_4F10391" targetNode="P_9F10391">
		<representations href="../../../cobol/LVVS3390.cbl.cobModel#S_10F10391"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10391_I" deadCode="false" sourceNode="P_4F10391" targetNode="P_10F10391">
		<representations href="../../../cobol/LVVS3390.cbl.cobModel#S_12F10391"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10391_O" deadCode="false" sourceNode="P_4F10391" targetNode="P_11F10391">
		<representations href="../../../cobol/LVVS3390.cbl.cobModel#S_12F10391"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10391_I" deadCode="false" sourceNode="P_4F10391" targetNode="P_12F10391">
		<representations href="../../../cobol/LVVS3390.cbl.cobModel#S_14F10391"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10391_O" deadCode="false" sourceNode="P_4F10391" targetNode="P_13F10391">
		<representations href="../../../cobol/LVVS3390.cbl.cobModel#S_14F10391"/>
	</edges>
	<edges id="P_4F10391P_5F10391" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10391" targetNode="P_5F10391"/>
	<edges id="P_8F10391P_9F10391" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10391" targetNode="P_9F10391"/>
	<edges id="P_10F10391P_11F10391" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10391" targetNode="P_11F10391"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10391_I" deadCode="false" sourceNode="P_12F10391" targetNode="P_14F10391">
		<representations href="../../../cobol/LVVS3390.cbl.cobModel#S_43F10391"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10391_O" deadCode="false" sourceNode="P_12F10391" targetNode="P_15F10391">
		<representations href="../../../cobol/LVVS3390.cbl.cobModel#S_43F10391"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10391_I" deadCode="false" sourceNode="P_12F10391" targetNode="P_16F10391">
		<representations href="../../../cobol/LVVS3390.cbl.cobModel#S_44F10391"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10391_O" deadCode="false" sourceNode="P_12F10391" targetNode="P_17F10391">
		<representations href="../../../cobol/LVVS3390.cbl.cobModel#S_44F10391"/>
	</edges>
	<edges id="P_12F10391P_13F10391" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10391" targetNode="P_13F10391"/>
	<edges id="P_14F10391P_15F10391" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10391" targetNode="P_15F10391"/>
	<edges id="P_16F10391P_17F10391" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10391" targetNode="P_17F10391"/>
	<edges xsi:type="cbl:CallEdge" id="S_37F10391" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_12F10391" targetNode="LDBSF220">
		<representations href="../../../cobol/../importantStmts.cobModel#S_37F10391"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_59F10391" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_16F10391" targetNode="IDBSTIT0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_59F10391"></representations>
	</edges>
</Package>

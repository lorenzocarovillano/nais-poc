<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS5560" cbl:id="LDBS5560" xsi:id="LDBS5560" packageRef="LDBS5560.igd#LDBS5560" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS5560_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10221" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS5560.cbl.cobModel#SC_1F10221"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10221" deadCode="false" name="PROGRAM_LDBS5560_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_1F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10221" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_2F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10221" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_3F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10221" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_12F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10221" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_13F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10221" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_4F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10221" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_5F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10221" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_6F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10221" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_7F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10221" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_8F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10221" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_9F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10221" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_44F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10221" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_49F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10221" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_14F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10221" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_15F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10221" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_16F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10221" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_17F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10221" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_18F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10221" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_19F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10221" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_20F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10221" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_21F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10221" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_22F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10221" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_23F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10221" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_56F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10221" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_57F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10221" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_24F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10221" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_25F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10221" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_26F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10221" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_27F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10221" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_28F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10221" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_29F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10221" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_30F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10221" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_31F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10221" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_32F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10221" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_33F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10221" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_58F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10221" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_59F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10221" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_34F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10221" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_35F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10221" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_36F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10221" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_37F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10221" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_38F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10221" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_39F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10221" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_40F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10221" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_41F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10221" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_42F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10221" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_43F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10221" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_50F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10221" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_51F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10221" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_60F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10221" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_63F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10221" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_52F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10221" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_53F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10221" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_45F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10221" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_46F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10221" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_47F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10221" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_48F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10221" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_54F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10221" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_55F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10221" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_10F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10221" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_11F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10221" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_66F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10221" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_67F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10221" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_68F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10221" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_69F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10221" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_61F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10221" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_62F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10221" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_70F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10221" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_71F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10221" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_64F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10221" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_65F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10221" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_72F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10221" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_73F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10221" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_74F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10221" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_75F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10221" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_76F10221"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10221" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS5560.cbl.cobModel#P_77F10221"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_POLI" name="POLI">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_POLI"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_RAPP_ANA" name="RAPP_ANA" missing="true">
			<representations href="../../../../missing.xmi#"/>
		</children>
	</packageNode>
	<edges id="SC_1F10221P_1F10221" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10221" targetNode="P_1F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10221_I" deadCode="false" sourceNode="P_1F10221" targetNode="P_2F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_2F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10221_O" deadCode="false" sourceNode="P_1F10221" targetNode="P_3F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_2F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10221_I" deadCode="false" sourceNode="P_1F10221" targetNode="P_4F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_6F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10221_O" deadCode="false" sourceNode="P_1F10221" targetNode="P_5F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_6F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10221_I" deadCode="false" sourceNode="P_1F10221" targetNode="P_6F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_10F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10221_O" deadCode="false" sourceNode="P_1F10221" targetNode="P_7F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_10F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10221_I" deadCode="false" sourceNode="P_1F10221" targetNode="P_8F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_14F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10221_O" deadCode="false" sourceNode="P_1F10221" targetNode="P_9F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_14F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10221_I" deadCode="false" sourceNode="P_2F10221" targetNode="P_10F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_24F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10221_O" deadCode="false" sourceNode="P_2F10221" targetNode="P_11F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_24F10221"/>
	</edges>
	<edges id="P_2F10221P_3F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10221" targetNode="P_3F10221"/>
	<edges id="P_12F10221P_13F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10221" targetNode="P_13F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10221_I" deadCode="false" sourceNode="P_4F10221" targetNode="P_14F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_37F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10221_O" deadCode="false" sourceNode="P_4F10221" targetNode="P_15F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_37F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10221_I" deadCode="false" sourceNode="P_4F10221" targetNode="P_16F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_38F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10221_O" deadCode="false" sourceNode="P_4F10221" targetNode="P_17F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_38F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10221_I" deadCode="false" sourceNode="P_4F10221" targetNode="P_18F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_39F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10221_O" deadCode="false" sourceNode="P_4F10221" targetNode="P_19F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_39F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10221_I" deadCode="false" sourceNode="P_4F10221" targetNode="P_20F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_40F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10221_O" deadCode="false" sourceNode="P_4F10221" targetNode="P_21F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_40F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10221_I" deadCode="false" sourceNode="P_4F10221" targetNode="P_22F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_41F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10221_O" deadCode="false" sourceNode="P_4F10221" targetNode="P_23F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_41F10221"/>
	</edges>
	<edges id="P_4F10221P_5F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10221" targetNode="P_5F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10221_I" deadCode="false" sourceNode="P_6F10221" targetNode="P_24F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_45F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10221_O" deadCode="false" sourceNode="P_6F10221" targetNode="P_25F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_45F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10221_I" deadCode="false" sourceNode="P_6F10221" targetNode="P_26F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_46F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10221_O" deadCode="false" sourceNode="P_6F10221" targetNode="P_27F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_46F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10221_I" deadCode="false" sourceNode="P_6F10221" targetNode="P_28F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_47F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10221_O" deadCode="false" sourceNode="P_6F10221" targetNode="P_29F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_47F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10221_I" deadCode="false" sourceNode="P_6F10221" targetNode="P_30F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_48F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10221_O" deadCode="false" sourceNode="P_6F10221" targetNode="P_31F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_48F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10221_I" deadCode="false" sourceNode="P_6F10221" targetNode="P_32F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_49F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10221_O" deadCode="false" sourceNode="P_6F10221" targetNode="P_33F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_49F10221"/>
	</edges>
	<edges id="P_6F10221P_7F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10221" targetNode="P_7F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10221_I" deadCode="false" sourceNode="P_8F10221" targetNode="P_34F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_53F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10221_O" deadCode="false" sourceNode="P_8F10221" targetNode="P_35F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_53F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10221_I" deadCode="false" sourceNode="P_8F10221" targetNode="P_36F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_54F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10221_O" deadCode="false" sourceNode="P_8F10221" targetNode="P_37F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_54F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10221_I" deadCode="false" sourceNode="P_8F10221" targetNode="P_38F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_55F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10221_O" deadCode="false" sourceNode="P_8F10221" targetNode="P_39F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_55F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10221_I" deadCode="false" sourceNode="P_8F10221" targetNode="P_40F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_56F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10221_O" deadCode="false" sourceNode="P_8F10221" targetNode="P_41F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_56F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10221_I" deadCode="false" sourceNode="P_8F10221" targetNode="P_42F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_57F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10221_O" deadCode="false" sourceNode="P_8F10221" targetNode="P_43F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_57F10221"/>
	</edges>
	<edges id="P_8F10221P_9F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10221" targetNode="P_9F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10221_I" deadCode="false" sourceNode="P_44F10221" targetNode="P_45F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_60F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10221_O" deadCode="false" sourceNode="P_44F10221" targetNode="P_46F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_60F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10221_I" deadCode="false" sourceNode="P_44F10221" targetNode="P_47F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_61F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10221_O" deadCode="false" sourceNode="P_44F10221" targetNode="P_48F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_61F10221"/>
	</edges>
	<edges id="P_44F10221P_49F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10221" targetNode="P_49F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10221_I" deadCode="false" sourceNode="P_14F10221" targetNode="P_45F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_65F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10221_O" deadCode="false" sourceNode="P_14F10221" targetNode="P_46F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_65F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10221_I" deadCode="false" sourceNode="P_14F10221" targetNode="P_47F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_66F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10221_O" deadCode="false" sourceNode="P_14F10221" targetNode="P_48F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_66F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10221_I" deadCode="false" sourceNode="P_14F10221" targetNode="P_12F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_68F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10221_O" deadCode="false" sourceNode="P_14F10221" targetNode="P_13F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_68F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10221_I" deadCode="false" sourceNode="P_14F10221" targetNode="P_50F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_70F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10221_O" deadCode="false" sourceNode="P_14F10221" targetNode="P_51F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_70F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10221_I" deadCode="false" sourceNode="P_14F10221" targetNode="P_52F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_71F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10221_O" deadCode="false" sourceNode="P_14F10221" targetNode="P_53F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_71F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10221_I" deadCode="false" sourceNode="P_14F10221" targetNode="P_54F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_72F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10221_O" deadCode="false" sourceNode="P_14F10221" targetNode="P_55F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_72F10221"/>
	</edges>
	<edges id="P_14F10221P_15F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10221" targetNode="P_15F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10221_I" deadCode="false" sourceNode="P_16F10221" targetNode="P_44F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_74F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10221_O" deadCode="false" sourceNode="P_16F10221" targetNode="P_49F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_74F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10221_I" deadCode="false" sourceNode="P_16F10221" targetNode="P_12F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_76F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10221_O" deadCode="false" sourceNode="P_16F10221" targetNode="P_13F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_76F10221"/>
	</edges>
	<edges id="P_16F10221P_17F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10221" targetNode="P_17F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10221_I" deadCode="false" sourceNode="P_18F10221" targetNode="P_12F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_79F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10221_O" deadCode="false" sourceNode="P_18F10221" targetNode="P_13F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_79F10221"/>
	</edges>
	<edges id="P_18F10221P_19F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10221" targetNode="P_19F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10221_I" deadCode="false" sourceNode="P_20F10221" targetNode="P_16F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_81F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10221_O" deadCode="false" sourceNode="P_20F10221" targetNode="P_17F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_81F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10221_I" deadCode="false" sourceNode="P_20F10221" targetNode="P_22F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_83F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10221_O" deadCode="false" sourceNode="P_20F10221" targetNode="P_23F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_83F10221"/>
	</edges>
	<edges id="P_20F10221P_21F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10221" targetNode="P_21F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10221_I" deadCode="false" sourceNode="P_22F10221" targetNode="P_12F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_86F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10221_O" deadCode="false" sourceNode="P_22F10221" targetNode="P_13F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_86F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10221_I" deadCode="false" sourceNode="P_22F10221" targetNode="P_50F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_88F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10221_O" deadCode="false" sourceNode="P_22F10221" targetNode="P_51F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_88F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10221_I" deadCode="false" sourceNode="P_22F10221" targetNode="P_52F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_89F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10221_O" deadCode="false" sourceNode="P_22F10221" targetNode="P_53F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_89F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10221_I" deadCode="false" sourceNode="P_22F10221" targetNode="P_54F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_90F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10221_O" deadCode="false" sourceNode="P_22F10221" targetNode="P_55F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_90F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10221_I" deadCode="false" sourceNode="P_22F10221" targetNode="P_18F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_92F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10221_O" deadCode="false" sourceNode="P_22F10221" targetNode="P_19F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_92F10221"/>
	</edges>
	<edges id="P_22F10221P_23F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10221" targetNode="P_23F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10221_I" deadCode="false" sourceNode="P_56F10221" targetNode="P_45F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_96F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10221_O" deadCode="false" sourceNode="P_56F10221" targetNode="P_46F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_96F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10221_I" deadCode="false" sourceNode="P_56F10221" targetNode="P_47F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_97F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10221_O" deadCode="false" sourceNode="P_56F10221" targetNode="P_48F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_97F10221"/>
	</edges>
	<edges id="P_56F10221P_57F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10221" targetNode="P_57F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10221_I" deadCode="false" sourceNode="P_24F10221" targetNode="P_45F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_101F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10221_O" deadCode="false" sourceNode="P_24F10221" targetNode="P_46F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_101F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10221_I" deadCode="false" sourceNode="P_24F10221" targetNode="P_47F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_102F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10221_O" deadCode="false" sourceNode="P_24F10221" targetNode="P_48F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_102F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10221_I" deadCode="false" sourceNode="P_24F10221" targetNode="P_12F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_104F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10221_O" deadCode="false" sourceNode="P_24F10221" targetNode="P_13F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_104F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10221_I" deadCode="false" sourceNode="P_24F10221" targetNode="P_50F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_106F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10221_O" deadCode="false" sourceNode="P_24F10221" targetNode="P_51F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_106F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10221_I" deadCode="false" sourceNode="P_24F10221" targetNode="P_52F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_107F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10221_O" deadCode="false" sourceNode="P_24F10221" targetNode="P_53F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_107F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10221_I" deadCode="false" sourceNode="P_24F10221" targetNode="P_54F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_108F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10221_O" deadCode="false" sourceNode="P_24F10221" targetNode="P_55F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_108F10221"/>
	</edges>
	<edges id="P_24F10221P_25F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10221" targetNode="P_25F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10221_I" deadCode="false" sourceNode="P_26F10221" targetNode="P_56F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_110F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10221_O" deadCode="false" sourceNode="P_26F10221" targetNode="P_57F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_110F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10221_I" deadCode="false" sourceNode="P_26F10221" targetNode="P_12F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_112F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10221_O" deadCode="false" sourceNode="P_26F10221" targetNode="P_13F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_112F10221"/>
	</edges>
	<edges id="P_26F10221P_27F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10221" targetNode="P_27F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10221_I" deadCode="false" sourceNode="P_28F10221" targetNode="P_12F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_115F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10221_O" deadCode="false" sourceNode="P_28F10221" targetNode="P_13F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_115F10221"/>
	</edges>
	<edges id="P_28F10221P_29F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10221" targetNode="P_29F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10221_I" deadCode="false" sourceNode="P_30F10221" targetNode="P_26F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_117F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10221_O" deadCode="false" sourceNode="P_30F10221" targetNode="P_27F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_117F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10221_I" deadCode="false" sourceNode="P_30F10221" targetNode="P_32F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_119F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10221_O" deadCode="false" sourceNode="P_30F10221" targetNode="P_33F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_119F10221"/>
	</edges>
	<edges id="P_30F10221P_31F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10221" targetNode="P_31F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10221_I" deadCode="false" sourceNode="P_32F10221" targetNode="P_12F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_122F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10221_O" deadCode="false" sourceNode="P_32F10221" targetNode="P_13F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_122F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10221_I" deadCode="false" sourceNode="P_32F10221" targetNode="P_50F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_124F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10221_O" deadCode="false" sourceNode="P_32F10221" targetNode="P_51F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_124F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10221_I" deadCode="false" sourceNode="P_32F10221" targetNode="P_52F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_125F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10221_O" deadCode="false" sourceNode="P_32F10221" targetNode="P_53F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_125F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10221_I" deadCode="false" sourceNode="P_32F10221" targetNode="P_54F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_126F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10221_O" deadCode="false" sourceNode="P_32F10221" targetNode="P_55F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_126F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10221_I" deadCode="false" sourceNode="P_32F10221" targetNode="P_28F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_128F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10221_O" deadCode="false" sourceNode="P_32F10221" targetNode="P_29F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_128F10221"/>
	</edges>
	<edges id="P_32F10221P_33F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10221" targetNode="P_33F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10221_I" deadCode="false" sourceNode="P_58F10221" targetNode="P_45F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_132F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10221_O" deadCode="false" sourceNode="P_58F10221" targetNode="P_46F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_132F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10221_I" deadCode="false" sourceNode="P_58F10221" targetNode="P_47F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_133F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10221_O" deadCode="false" sourceNode="P_58F10221" targetNode="P_48F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_133F10221"/>
	</edges>
	<edges id="P_58F10221P_59F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10221" targetNode="P_59F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10221_I" deadCode="false" sourceNode="P_34F10221" targetNode="P_45F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_136F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10221_O" deadCode="false" sourceNode="P_34F10221" targetNode="P_46F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_136F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10221_I" deadCode="false" sourceNode="P_34F10221" targetNode="P_47F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_137F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10221_O" deadCode="false" sourceNode="P_34F10221" targetNode="P_48F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_137F10221"/>
	</edges>
	<edges id="P_34F10221P_35F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10221" targetNode="P_35F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10221_I" deadCode="false" sourceNode="P_36F10221" targetNode="P_58F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_140F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10221_O" deadCode="false" sourceNode="P_36F10221" targetNode="P_59F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_140F10221"/>
	</edges>
	<edges id="P_36F10221P_37F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10221" targetNode="P_37F10221"/>
	<edges id="P_38F10221P_39F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10221" targetNode="P_39F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10221_I" deadCode="false" sourceNode="P_40F10221" targetNode="P_36F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_145F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10221_O" deadCode="false" sourceNode="P_40F10221" targetNode="P_37F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_145F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10221_I" deadCode="false" sourceNode="P_40F10221" targetNode="P_42F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_147F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10221_O" deadCode="false" sourceNode="P_40F10221" targetNode="P_43F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_147F10221"/>
	</edges>
	<edges id="P_40F10221P_41F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10221" targetNode="P_41F10221"/>
	<edges id="P_42F10221P_43F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10221" targetNode="P_43F10221"/>
	<edges id="P_50F10221P_51F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10221" targetNode="P_51F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10221_I" deadCode="true" sourceNode="P_60F10221" targetNode="P_61F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_253F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10221_O" deadCode="true" sourceNode="P_60F10221" targetNode="P_62F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_253F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_256F10221_I" deadCode="true" sourceNode="P_60F10221" targetNode="P_61F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_256F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_256F10221_O" deadCode="true" sourceNode="P_60F10221" targetNode="P_62F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_256F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10221_I" deadCode="true" sourceNode="P_60F10221" targetNode="P_61F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_259F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10221_O" deadCode="true" sourceNode="P_60F10221" targetNode="P_62F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_259F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10221_I" deadCode="true" sourceNode="P_60F10221" targetNode="P_61F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_262F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10221_O" deadCode="true" sourceNode="P_60F10221" targetNode="P_62F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_262F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10221_I" deadCode="true" sourceNode="P_60F10221" targetNode="P_61F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_265F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10221_O" deadCode="true" sourceNode="P_60F10221" targetNode="P_62F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_265F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_269F10221_I" deadCode="true" sourceNode="P_60F10221" targetNode="P_61F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_269F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_269F10221_O" deadCode="true" sourceNode="P_60F10221" targetNode="P_62F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_269F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_272F10221_I" deadCode="true" sourceNode="P_60F10221" targetNode="P_61F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_272F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_272F10221_O" deadCode="true" sourceNode="P_60F10221" targetNode="P_62F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_272F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10221_I" deadCode="true" sourceNode="P_60F10221" targetNode="P_61F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_276F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10221_O" deadCode="true" sourceNode="P_60F10221" targetNode="P_62F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_276F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10221_I" deadCode="true" sourceNode="P_60F10221" targetNode="P_61F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_280F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10221_O" deadCode="true" sourceNode="P_60F10221" targetNode="P_62F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_280F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10221_I" deadCode="true" sourceNode="P_60F10221" targetNode="P_61F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_284F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10221_O" deadCode="true" sourceNode="P_60F10221" targetNode="P_62F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_284F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10221_I" deadCode="false" sourceNode="P_52F10221" targetNode="P_64F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_289F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10221_O" deadCode="false" sourceNode="P_52F10221" targetNode="P_65F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_289F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10221_I" deadCode="false" sourceNode="P_52F10221" targetNode="P_64F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_292F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10221_O" deadCode="false" sourceNode="P_52F10221" targetNode="P_65F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_292F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_295F10221_I" deadCode="false" sourceNode="P_52F10221" targetNode="P_64F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_295F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_295F10221_O" deadCode="false" sourceNode="P_52F10221" targetNode="P_65F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_295F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_298F10221_I" deadCode="false" sourceNode="P_52F10221" targetNode="P_64F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_298F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_298F10221_O" deadCode="false" sourceNode="P_52F10221" targetNode="P_65F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_298F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10221_I" deadCode="false" sourceNode="P_52F10221" targetNode="P_64F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_301F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10221_O" deadCode="false" sourceNode="P_52F10221" targetNode="P_65F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_301F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_305F10221_I" deadCode="false" sourceNode="P_52F10221" targetNode="P_64F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_305F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_305F10221_O" deadCode="false" sourceNode="P_52F10221" targetNode="P_65F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_305F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10221_I" deadCode="false" sourceNode="P_52F10221" targetNode="P_64F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_308F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10221_O" deadCode="false" sourceNode="P_52F10221" targetNode="P_65F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_308F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_312F10221_I" deadCode="false" sourceNode="P_52F10221" targetNode="P_64F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_312F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_312F10221_O" deadCode="false" sourceNode="P_52F10221" targetNode="P_65F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_312F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_316F10221_I" deadCode="false" sourceNode="P_52F10221" targetNode="P_64F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_316F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_316F10221_O" deadCode="false" sourceNode="P_52F10221" targetNode="P_65F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_316F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_320F10221_I" deadCode="false" sourceNode="P_52F10221" targetNode="P_64F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_320F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_320F10221_O" deadCode="false" sourceNode="P_52F10221" targetNode="P_65F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_320F10221"/>
	</edges>
	<edges id="P_52F10221P_53F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10221" targetNode="P_53F10221"/>
	<edges id="P_45F10221P_46F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10221" targetNode="P_46F10221"/>
	<edges id="P_47F10221P_48F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10221" targetNode="P_48F10221"/>
	<edges id="P_54F10221P_55F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10221" targetNode="P_55F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_329F10221_I" deadCode="false" sourceNode="P_10F10221" targetNode="P_66F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_329F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_329F10221_O" deadCode="false" sourceNode="P_10F10221" targetNode="P_67F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_329F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_331F10221_I" deadCode="false" sourceNode="P_10F10221" targetNode="P_68F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_331F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_331F10221_O" deadCode="false" sourceNode="P_10F10221" targetNode="P_69F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_331F10221"/>
	</edges>
	<edges id="P_10F10221P_11F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10221" targetNode="P_11F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10221_I" deadCode="false" sourceNode="P_66F10221" targetNode="P_61F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_336F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10221_O" deadCode="false" sourceNode="P_66F10221" targetNode="P_62F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_336F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_341F10221_I" deadCode="false" sourceNode="P_66F10221" targetNode="P_61F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_341F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_341F10221_O" deadCode="false" sourceNode="P_66F10221" targetNode="P_62F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_341F10221"/>
	</edges>
	<edges id="P_66F10221P_67F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10221" targetNode="P_67F10221"/>
	<edges id="P_68F10221P_69F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10221" targetNode="P_69F10221"/>
	<edges id="P_61F10221P_62F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10221" targetNode="P_62F10221"/>
	<edges xsi:type="cbl:PerformEdge" id="S_370F10221_I" deadCode="false" sourceNode="P_64F10221" targetNode="P_72F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_370F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_370F10221_O" deadCode="false" sourceNode="P_64F10221" targetNode="P_73F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_370F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_371F10221_I" deadCode="false" sourceNode="P_64F10221" targetNode="P_74F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_371F10221"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_371F10221_O" deadCode="false" sourceNode="P_64F10221" targetNode="P_75F10221">
		<representations href="../../../cobol/LDBS5560.cbl.cobModel#S_371F10221"/>
	</edges>
	<edges id="P_64F10221P_65F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10221" targetNode="P_65F10221"/>
	<edges id="P_72F10221P_73F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10221" targetNode="P_73F10221"/>
	<edges id="P_74F10221P_75F10221" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10221" targetNode="P_75F10221"/>
	<edges xsi:type="cbl:DataEdge" id="S_67F10221_POS1" deadCode="false" targetNode="P_14F10221" sourceNode="DB2_POLI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10221"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_67F10221_POS2" deadCode="false" targetNode="P_14F10221" sourceNode="DB2_RAPP_ANA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10221"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_75F10221_POS1" deadCode="false" targetNode="P_16F10221" sourceNode="DB2_POLI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_75F10221"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_75F10221_POS2" deadCode="false" targetNode="P_16F10221" sourceNode="DB2_RAPP_ANA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_75F10221"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_78F10221_POS1" deadCode="false" targetNode="P_18F10221" sourceNode="DB2_POLI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10221"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_78F10221_POS2" deadCode="false" targetNode="P_18F10221" sourceNode="DB2_RAPP_ANA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10221"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_85F10221_POS1" deadCode="false" targetNode="P_22F10221" sourceNode="DB2_POLI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_85F10221"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_85F10221_POS2" deadCode="false" targetNode="P_22F10221" sourceNode="DB2_RAPP_ANA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_85F10221"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_103F10221_POS1" deadCode="false" targetNode="P_24F10221" sourceNode="DB2_POLI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_103F10221"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_103F10221_POS2" deadCode="false" targetNode="P_24F10221" sourceNode="DB2_RAPP_ANA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_103F10221"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10221_POS1" deadCode="false" targetNode="P_26F10221" sourceNode="DB2_POLI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10221"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10221_POS2" deadCode="false" targetNode="P_26F10221" sourceNode="DB2_RAPP_ANA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10221"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_114F10221_POS1" deadCode="false" targetNode="P_28F10221" sourceNode="DB2_POLI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10221"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_114F10221_POS2" deadCode="false" targetNode="P_28F10221" sourceNode="DB2_RAPP_ANA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10221"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_121F10221_POS1" deadCode="false" targetNode="P_32F10221" sourceNode="DB2_POLI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_121F10221"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_121F10221_POS2" deadCode="false" targetNode="P_32F10221" sourceNode="DB2_RAPP_ANA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_121F10221"></representations>
	</edges>
</Package>

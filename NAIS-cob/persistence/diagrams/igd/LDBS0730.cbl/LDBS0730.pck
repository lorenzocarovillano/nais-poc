<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS0730" cbl:id="LDBS0730" xsi:id="LDBS0730" packageRef="LDBS0730.igd#LDBS0730" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS0730_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10149" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS0730.cbl.cobModel#SC_1F10149"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10149" deadCode="false" name="PROGRAM_LDBS0730_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_1F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10149" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_2F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10149" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_3F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10149" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_8F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10149" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_9F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10149" deadCode="false" name="A205-ELABORA-WC">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_4F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10149" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_5F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10149" deadCode="false" name="A206-ELABORA-WC01">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_10F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10149" deadCode="false" name="A206-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_11F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10149" deadCode="false" name="A207-ELABORA-WC02">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_14F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10149" deadCode="false" name="A207-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_15F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10149" deadCode="false" name="AA250-DECLARE-CURSOR-WC1">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_38F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10149" deadCode="false" name="AA250-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_39F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10149" deadCode="false" name="AA260-OPEN-CURSOR-WC1">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_18F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10149" deadCode="false" name="AA260-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_19F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10149" deadCode="false" name="AA270-CLOSE-CURSOR-WC1">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_20F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10149" deadCode="false" name="AA270-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_21F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10149" deadCode="false" name="AA280-FETCH-FIRST-WC1">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_22F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10149" deadCode="false" name="AA280-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_23F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10149" deadCode="false" name="AA290-FETCH-NEXT-WC1">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_24F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10149" deadCode="false" name="AA290-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_25F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10149" deadCode="false" name="AA295-SELECT-WC1">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_26F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10149" deadCode="false" name="AA295-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_27F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10149" deadCode="false" name="AB250-DECLARE-CURSOR-WC2">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_46F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10149" deadCode="false" name="AB250-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_47F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10149" deadCode="false" name="AB260-OPEN-CURSOR-WC2">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_28F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10149" deadCode="false" name="AB260-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_29F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10149" deadCode="false" name="AB270-CLOSE-CURSOR-WC2">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_30F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10149" deadCode="false" name="AB270-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_31F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10149" deadCode="false" name="AB280-FETCH-FIRST-WC2">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_32F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10149" deadCode="false" name="AB280-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_33F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10149" deadCode="false" name="AB290-FETCH-NEXT-WC2">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_34F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10149" deadCode="false" name="AB290-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_35F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10149" deadCode="false" name="AB295-SELECT-WC2">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_36F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10149" deadCode="false" name="AB295-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_37F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10149" deadCode="false" name="A208-ELABORA-WC03">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_12F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10149" deadCode="false" name="A208-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_13F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10149" deadCode="false" name="A209-ELABORA-WC04">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_16F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10149" deadCode="false" name="A209-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_17F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10149" deadCode="false" name="AA350-DECLARE-CURSOR-WC3">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_64F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10149" deadCode="false" name="AA350-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_65F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10149" deadCode="false" name="AA360-OPEN-CURSOR-WC3">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_48F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10149" deadCode="false" name="AA360-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_49F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10149" deadCode="false" name="AA370-CLOSE-CURSOR-WC3">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_50F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10149" deadCode="false" name="AA370-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_51F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10149" deadCode="false" name="AA380-FETCH-FIRST-WC3">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_52F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10149" deadCode="false" name="AA380-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_53F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10149" deadCode="false" name="AA390-FETCH-NEXT-WC3">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_54F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10149" deadCode="false" name="AA390-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_55F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10149" deadCode="false" name="AB350-DECLARE-CURSOR-WC4">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_66F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10149" deadCode="false" name="AB350-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_67F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10149" deadCode="false" name="AB360-OPEN-CURSOR-WC4">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_56F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10149" deadCode="false" name="AB360-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_57F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10149" deadCode="false" name="AB370-CLOSE-CURSOR-WC4">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_58F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10149" deadCode="false" name="AB370-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_59F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10149" deadCode="false" name="AB380-FETCH-FIRST-WC4">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_60F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10149" deadCode="false" name="AB380-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_61F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10149" deadCode="false" name="AB390-FETCH-NEXT-WC4">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_62F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10149" deadCode="false" name="AB390-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_63F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10149" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_44F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10149" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_45F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10149" deadCode="false" name="Z101-SET-NULL-TGA">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_68F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10149" deadCode="false" name="Z101-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_69F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10149" deadCode="false" name="Z102-VALORIZZA-TGA">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_70F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10149" deadCode="false" name="Z102-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_71F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10149" deadCode="true" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_72F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10149" deadCode="true" name="Z200-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_73F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10149" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_74F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10149" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_75F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10149" deadCode="false" name="Z950-CONVERTI-X-TO-N-TGA">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_40F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10149" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_41F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10149" deadCode="false" name="Z951-CONVERTI-X-TO-N-STB">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_42F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10149" deadCode="false" name="Z951-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_43F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10149" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_6F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10149" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_7F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_78F10149" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_78F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_79F10149" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_79F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_80F10149" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_80F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_81F10149" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_81F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_82F10149" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_82F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_83F10149" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_83F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_84F10149" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_84F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_85F10149" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_85F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10149" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_76F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10149" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_77F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_86F10149" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_86F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_87F10149" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_87F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_88F10149" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_88F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_89F10149" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_89F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_90F10149" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_90F10149"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_91F10149" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS0730.cbl.cobModel#P_91F10149"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_ADES" name="ADES">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_ADES"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_STAT_OGG_BUS" name="STAT_OGG_BUS">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_STAT_OGG_BUS"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_TRCH_DI_GAR" name="TRCH_DI_GAR">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_TRCH_DI_GAR"/>
		</children>
	</packageNode>
	<edges id="SC_1F10149P_1F10149" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10149" targetNode="P_1F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10149_I" deadCode="false" sourceNode="P_1F10149" targetNode="P_2F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_1F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10149_O" deadCode="false" sourceNode="P_1F10149" targetNode="P_3F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_1F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10149_I" deadCode="false" sourceNode="P_1F10149" targetNode="P_4F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_4F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10149_O" deadCode="false" sourceNode="P_1F10149" targetNode="P_5F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_4F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10149_I" deadCode="false" sourceNode="P_2F10149" targetNode="P_6F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_14F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10149_O" deadCode="false" sourceNode="P_2F10149" targetNode="P_7F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_14F10149"/>
	</edges>
	<edges id="P_2F10149P_3F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10149" targetNode="P_3F10149"/>
	<edges id="P_8F10149P_9F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10149" targetNode="P_9F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10149_I" deadCode="false" sourceNode="P_4F10149" targetNode="P_10F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_31F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10149_O" deadCode="false" sourceNode="P_4F10149" targetNode="P_11F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_31F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10149_I" deadCode="false" sourceNode="P_4F10149" targetNode="P_12F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_32F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10149_O" deadCode="false" sourceNode="P_4F10149" targetNode="P_13F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_32F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10149_I" deadCode="false" sourceNode="P_4F10149" targetNode="P_14F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_34F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10149_O" deadCode="false" sourceNode="P_4F10149" targetNode="P_15F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_34F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10149_I" deadCode="false" sourceNode="P_4F10149" targetNode="P_16F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_35F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10149_O" deadCode="false" sourceNode="P_4F10149" targetNode="P_17F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_35F10149"/>
	</edges>
	<edges id="P_4F10149P_5F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10149" targetNode="P_5F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10149_I" deadCode="false" sourceNode="P_10F10149" targetNode="P_18F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_38F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10149_O" deadCode="false" sourceNode="P_10F10149" targetNode="P_19F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_38F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10149_I" deadCode="false" sourceNode="P_10F10149" targetNode="P_20F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_39F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10149_O" deadCode="false" sourceNode="P_10F10149" targetNode="P_21F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_39F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10149_I" deadCode="false" sourceNode="P_10F10149" targetNode="P_22F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_40F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10149_O" deadCode="false" sourceNode="P_10F10149" targetNode="P_23F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_40F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10149_I" deadCode="false" sourceNode="P_10F10149" targetNode="P_24F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_41F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10149_O" deadCode="false" sourceNode="P_10F10149" targetNode="P_25F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_41F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10149_I" deadCode="false" sourceNode="P_10F10149" targetNode="P_26F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_42F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10149_O" deadCode="false" sourceNode="P_10F10149" targetNode="P_27F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_42F10149"/>
	</edges>
	<edges id="P_10F10149P_11F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10149" targetNode="P_11F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10149_I" deadCode="false" sourceNode="P_14F10149" targetNode="P_28F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_46F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10149_O" deadCode="false" sourceNode="P_14F10149" targetNode="P_29F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_46F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10149_I" deadCode="false" sourceNode="P_14F10149" targetNode="P_30F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_47F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10149_O" deadCode="false" sourceNode="P_14F10149" targetNode="P_31F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_47F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10149_I" deadCode="false" sourceNode="P_14F10149" targetNode="P_32F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_48F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10149_O" deadCode="false" sourceNode="P_14F10149" targetNode="P_33F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_48F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10149_I" deadCode="false" sourceNode="P_14F10149" targetNode="P_34F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_49F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10149_O" deadCode="false" sourceNode="P_14F10149" targetNode="P_35F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_49F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10149_I" deadCode="false" sourceNode="P_14F10149" targetNode="P_36F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_50F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10149_O" deadCode="false" sourceNode="P_14F10149" targetNode="P_37F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_50F10149"/>
	</edges>
	<edges id="P_14F10149P_15F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10149" targetNode="P_15F10149"/>
	<edges id="P_38F10149P_39F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10149" targetNode="P_39F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10149_I" deadCode="false" sourceNode="P_18F10149" targetNode="P_38F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_56F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10149_O" deadCode="false" sourceNode="P_18F10149" targetNode="P_39F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_56F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10149_I" deadCode="false" sourceNode="P_18F10149" targetNode="P_8F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_58F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10149_O" deadCode="false" sourceNode="P_18F10149" targetNode="P_9F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_58F10149"/>
	</edges>
	<edges id="P_18F10149P_19F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10149" targetNode="P_19F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10149_I" deadCode="false" sourceNode="P_20F10149" targetNode="P_8F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_61F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10149_O" deadCode="false" sourceNode="P_20F10149" targetNode="P_9F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_61F10149"/>
	</edges>
	<edges id="P_20F10149P_21F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10149" targetNode="P_21F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10149_I" deadCode="false" sourceNode="P_22F10149" targetNode="P_18F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_63F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10149_O" deadCode="false" sourceNode="P_22F10149" targetNode="P_19F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_63F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10149_I" deadCode="false" sourceNode="P_22F10149" targetNode="P_24F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_65F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10149_O" deadCode="false" sourceNode="P_22F10149" targetNode="P_25F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_65F10149"/>
	</edges>
	<edges id="P_22F10149P_23F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10149" targetNode="P_23F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10149_I" deadCode="false" sourceNode="P_24F10149" targetNode="P_8F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_68F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10149_O" deadCode="false" sourceNode="P_24F10149" targetNode="P_9F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_68F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10149_I" deadCode="false" sourceNode="P_24F10149" targetNode="P_40F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_70F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10149_O" deadCode="false" sourceNode="P_24F10149" targetNode="P_41F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_70F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10149_I" deadCode="false" sourceNode="P_24F10149" targetNode="P_42F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_71F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10149_O" deadCode="false" sourceNode="P_24F10149" targetNode="P_43F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_71F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10149_I" deadCode="false" sourceNode="P_24F10149" targetNode="P_44F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_72F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10149_O" deadCode="false" sourceNode="P_24F10149" targetNode="P_45F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_72F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10149_I" deadCode="false" sourceNode="P_24F10149" targetNode="P_20F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_74F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10149_O" deadCode="false" sourceNode="P_24F10149" targetNode="P_21F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_74F10149"/>
	</edges>
	<edges id="P_24F10149P_25F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10149" targetNode="P_25F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10149_I" deadCode="false" sourceNode="P_26F10149" targetNode="P_8F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_79F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10149_O" deadCode="false" sourceNode="P_26F10149" targetNode="P_9F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_79F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10149_I" deadCode="false" sourceNode="P_26F10149" targetNode="P_44F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_81F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10149_O" deadCode="false" sourceNode="P_26F10149" targetNode="P_45F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_81F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10149_I" deadCode="false" sourceNode="P_26F10149" targetNode="P_42F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_82F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10149_O" deadCode="false" sourceNode="P_26F10149" targetNode="P_43F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_82F10149"/>
	</edges>
	<edges id="P_26F10149P_27F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10149" targetNode="P_27F10149"/>
	<edges id="P_46F10149P_47F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10149" targetNode="P_47F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10149_I" deadCode="false" sourceNode="P_28F10149" targetNode="P_46F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_87F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10149_O" deadCode="false" sourceNode="P_28F10149" targetNode="P_47F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_87F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10149_I" deadCode="false" sourceNode="P_28F10149" targetNode="P_8F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_89F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10149_O" deadCode="false" sourceNode="P_28F10149" targetNode="P_9F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_89F10149"/>
	</edges>
	<edges id="P_28F10149P_29F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10149" targetNode="P_29F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10149_I" deadCode="false" sourceNode="P_30F10149" targetNode="P_8F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_92F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10149_O" deadCode="false" sourceNode="P_30F10149" targetNode="P_9F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_92F10149"/>
	</edges>
	<edges id="P_30F10149P_31F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10149" targetNode="P_31F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10149_I" deadCode="false" sourceNode="P_32F10149" targetNode="P_28F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_94F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10149_O" deadCode="false" sourceNode="P_32F10149" targetNode="P_29F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_94F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10149_I" deadCode="false" sourceNode="P_32F10149" targetNode="P_34F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_96F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10149_O" deadCode="false" sourceNode="P_32F10149" targetNode="P_35F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_96F10149"/>
	</edges>
	<edges id="P_32F10149P_33F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10149" targetNode="P_33F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10149_I" deadCode="false" sourceNode="P_34F10149" targetNode="P_8F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_99F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10149_O" deadCode="false" sourceNode="P_34F10149" targetNode="P_9F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_99F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10149_I" deadCode="false" sourceNode="P_34F10149" targetNode="P_40F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_101F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10149_O" deadCode="false" sourceNode="P_34F10149" targetNode="P_41F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_101F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10149_I" deadCode="false" sourceNode="P_34F10149" targetNode="P_42F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_102F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10149_O" deadCode="false" sourceNode="P_34F10149" targetNode="P_43F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_102F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10149_I" deadCode="false" sourceNode="P_34F10149" targetNode="P_44F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_103F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10149_O" deadCode="false" sourceNode="P_34F10149" targetNode="P_45F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_103F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10149_I" deadCode="false" sourceNode="P_34F10149" targetNode="P_30F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_105F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10149_O" deadCode="false" sourceNode="P_34F10149" targetNode="P_31F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_105F10149"/>
	</edges>
	<edges id="P_34F10149P_35F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10149" targetNode="P_35F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10149_I" deadCode="false" sourceNode="P_36F10149" targetNode="P_8F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_110F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10149_O" deadCode="false" sourceNode="P_36F10149" targetNode="P_9F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_110F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10149_I" deadCode="false" sourceNode="P_36F10149" targetNode="P_40F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_112F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10149_O" deadCode="false" sourceNode="P_36F10149" targetNode="P_41F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_112F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10149_I" deadCode="false" sourceNode="P_36F10149" targetNode="P_42F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_113F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10149_O" deadCode="false" sourceNode="P_36F10149" targetNode="P_43F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_113F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10149_I" deadCode="false" sourceNode="P_36F10149" targetNode="P_44F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_114F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10149_O" deadCode="false" sourceNode="P_36F10149" targetNode="P_45F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_114F10149"/>
	</edges>
	<edges id="P_36F10149P_37F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10149" targetNode="P_37F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10149_I" deadCode="false" sourceNode="P_12F10149" targetNode="P_48F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_117F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10149_O" deadCode="false" sourceNode="P_12F10149" targetNode="P_49F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_117F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10149_I" deadCode="false" sourceNode="P_12F10149" targetNode="P_50F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_118F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10149_O" deadCode="false" sourceNode="P_12F10149" targetNode="P_51F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_118F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10149_I" deadCode="false" sourceNode="P_12F10149" targetNode="P_52F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_119F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10149_O" deadCode="false" sourceNode="P_12F10149" targetNode="P_53F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_119F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10149_I" deadCode="false" sourceNode="P_12F10149" targetNode="P_54F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_120F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10149_O" deadCode="false" sourceNode="P_12F10149" targetNode="P_55F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_120F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10149_I" deadCode="false" sourceNode="P_12F10149" targetNode="P_26F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_121F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10149_O" deadCode="false" sourceNode="P_12F10149" targetNode="P_27F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_121F10149"/>
	</edges>
	<edges id="P_12F10149P_13F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10149" targetNode="P_13F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10149_I" deadCode="false" sourceNode="P_16F10149" targetNode="P_56F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_125F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10149_O" deadCode="false" sourceNode="P_16F10149" targetNode="P_57F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_125F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10149_I" deadCode="false" sourceNode="P_16F10149" targetNode="P_58F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_126F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10149_O" deadCode="false" sourceNode="P_16F10149" targetNode="P_59F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_126F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10149_I" deadCode="false" sourceNode="P_16F10149" targetNode="P_60F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_127F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10149_O" deadCode="false" sourceNode="P_16F10149" targetNode="P_61F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_127F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10149_I" deadCode="false" sourceNode="P_16F10149" targetNode="P_62F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_128F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10149_O" deadCode="false" sourceNode="P_16F10149" targetNode="P_63F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_128F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_129F10149_I" deadCode="false" sourceNode="P_16F10149" targetNode="P_36F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_129F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_129F10149_O" deadCode="false" sourceNode="P_16F10149" targetNode="P_37F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_129F10149"/>
	</edges>
	<edges id="P_16F10149P_17F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10149" targetNode="P_17F10149"/>
	<edges id="P_64F10149P_65F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10149" targetNode="P_65F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10149_I" deadCode="false" sourceNode="P_48F10149" targetNode="P_64F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_135F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10149_O" deadCode="false" sourceNode="P_48F10149" targetNode="P_65F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_135F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10149_I" deadCode="false" sourceNode="P_48F10149" targetNode="P_8F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_137F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10149_O" deadCode="false" sourceNode="P_48F10149" targetNode="P_9F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_137F10149"/>
	</edges>
	<edges id="P_48F10149P_49F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10149" targetNode="P_49F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10149_I" deadCode="false" sourceNode="P_50F10149" targetNode="P_8F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_140F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10149_O" deadCode="false" sourceNode="P_50F10149" targetNode="P_9F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_140F10149"/>
	</edges>
	<edges id="P_50F10149P_51F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10149" targetNode="P_51F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10149_I" deadCode="false" sourceNode="P_52F10149" targetNode="P_48F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_142F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10149_O" deadCode="false" sourceNode="P_52F10149" targetNode="P_49F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_142F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10149_I" deadCode="false" sourceNode="P_52F10149" targetNode="P_54F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_144F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10149_O" deadCode="false" sourceNode="P_52F10149" targetNode="P_55F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_144F10149"/>
	</edges>
	<edges id="P_52F10149P_53F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10149" targetNode="P_53F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10149_I" deadCode="false" sourceNode="P_54F10149" targetNode="P_8F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_147F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10149_O" deadCode="false" sourceNode="P_54F10149" targetNode="P_9F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_147F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10149_I" deadCode="false" sourceNode="P_54F10149" targetNode="P_44F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_149F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10149_O" deadCode="false" sourceNode="P_54F10149" targetNode="P_45F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_149F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10149_I" deadCode="false" sourceNode="P_54F10149" targetNode="P_42F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_150F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10149_O" deadCode="false" sourceNode="P_54F10149" targetNode="P_43F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_150F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10149_I" deadCode="false" sourceNode="P_54F10149" targetNode="P_50F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_152F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10149_O" deadCode="false" sourceNode="P_54F10149" targetNode="P_51F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_152F10149"/>
	</edges>
	<edges id="P_54F10149P_55F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10149" targetNode="P_55F10149"/>
	<edges id="P_66F10149P_67F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10149" targetNode="P_67F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_159F10149_I" deadCode="false" sourceNode="P_56F10149" targetNode="P_66F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_159F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_159F10149_O" deadCode="false" sourceNode="P_56F10149" targetNode="P_67F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_159F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10149_I" deadCode="false" sourceNode="P_56F10149" targetNode="P_8F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_161F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10149_O" deadCode="false" sourceNode="P_56F10149" targetNode="P_9F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_161F10149"/>
	</edges>
	<edges id="P_56F10149P_57F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10149" targetNode="P_57F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10149_I" deadCode="false" sourceNode="P_58F10149" targetNode="P_8F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_164F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10149_O" deadCode="false" sourceNode="P_58F10149" targetNode="P_9F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_164F10149"/>
	</edges>
	<edges id="P_58F10149P_59F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10149" targetNode="P_59F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10149_I" deadCode="false" sourceNode="P_60F10149" targetNode="P_56F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_166F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10149_O" deadCode="false" sourceNode="P_60F10149" targetNode="P_57F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_166F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10149_I" deadCode="false" sourceNode="P_60F10149" targetNode="P_62F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_168F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10149_O" deadCode="false" sourceNode="P_60F10149" targetNode="P_63F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_168F10149"/>
	</edges>
	<edges id="P_60F10149P_61F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_60F10149" targetNode="P_61F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10149_I" deadCode="false" sourceNode="P_62F10149" targetNode="P_8F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_171F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10149_O" deadCode="false" sourceNode="P_62F10149" targetNode="P_9F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_171F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10149_I" deadCode="false" sourceNode="P_62F10149" targetNode="P_40F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_173F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10149_O" deadCode="false" sourceNode="P_62F10149" targetNode="P_41F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_173F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10149_I" deadCode="false" sourceNode="P_62F10149" targetNode="P_42F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_174F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10149_O" deadCode="false" sourceNode="P_62F10149" targetNode="P_43F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_174F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10149_I" deadCode="false" sourceNode="P_62F10149" targetNode="P_44F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_175F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10149_O" deadCode="false" sourceNode="P_62F10149" targetNode="P_45F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_175F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10149_I" deadCode="false" sourceNode="P_62F10149" targetNode="P_58F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_177F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10149_O" deadCode="false" sourceNode="P_62F10149" targetNode="P_59F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_177F10149"/>
	</edges>
	<edges id="P_62F10149P_63F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10149" targetNode="P_63F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_188F10149_I" deadCode="false" sourceNode="P_44F10149" targetNode="P_68F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_188F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_188F10149_O" deadCode="false" sourceNode="P_44F10149" targetNode="P_69F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_188F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10149_I" deadCode="false" sourceNode="P_44F10149" targetNode="P_70F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_189F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10149_O" deadCode="false" sourceNode="P_44F10149" targetNode="P_71F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_189F10149"/>
	</edges>
	<edges id="P_44F10149P_45F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10149" targetNode="P_45F10149"/>
	<edges id="P_68F10149P_69F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10149" targetNode="P_69F10149"/>
	<edges id="P_70F10149P_71F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_70F10149" targetNode="P_71F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_435F10149_I" deadCode="false" sourceNode="P_40F10149" targetNode="P_76F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_435F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_435F10149_O" deadCode="false" sourceNode="P_40F10149" targetNode="P_77F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_435F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_438F10149_I" deadCode="false" sourceNode="P_40F10149" targetNode="P_76F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_438F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_438F10149_O" deadCode="false" sourceNode="P_40F10149" targetNode="P_77F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_438F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_441F10149_I" deadCode="false" sourceNode="P_40F10149" targetNode="P_76F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_441F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_441F10149_O" deadCode="false" sourceNode="P_40F10149" targetNode="P_77F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_441F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_445F10149_I" deadCode="false" sourceNode="P_40F10149" targetNode="P_76F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_445F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_445F10149_O" deadCode="false" sourceNode="P_40F10149" targetNode="P_77F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_445F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_449F10149_I" deadCode="false" sourceNode="P_40F10149" targetNode="P_76F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_449F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_449F10149_O" deadCode="false" sourceNode="P_40F10149" targetNode="P_77F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_449F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_453F10149_I" deadCode="false" sourceNode="P_40F10149" targetNode="P_76F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_453F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_453F10149_O" deadCode="false" sourceNode="P_40F10149" targetNode="P_77F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_453F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_457F10149_I" deadCode="false" sourceNode="P_40F10149" targetNode="P_76F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_457F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_457F10149_O" deadCode="false" sourceNode="P_40F10149" targetNode="P_77F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_457F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_461F10149_I" deadCode="false" sourceNode="P_40F10149" targetNode="P_76F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_461F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_461F10149_O" deadCode="false" sourceNode="P_40F10149" targetNode="P_77F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_461F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_465F10149_I" deadCode="false" sourceNode="P_40F10149" targetNode="P_76F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_465F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_465F10149_O" deadCode="false" sourceNode="P_40F10149" targetNode="P_77F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_465F10149"/>
	</edges>
	<edges id="P_40F10149P_41F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10149" targetNode="P_41F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_469F10149_I" deadCode="false" sourceNode="P_42F10149" targetNode="P_76F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_469F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_469F10149_O" deadCode="false" sourceNode="P_42F10149" targetNode="P_77F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_469F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_472F10149_I" deadCode="false" sourceNode="P_42F10149" targetNode="P_76F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_472F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_472F10149_O" deadCode="false" sourceNode="P_42F10149" targetNode="P_77F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_472F10149"/>
	</edges>
	<edges id="P_42F10149P_43F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10149" targetNode="P_43F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_475F10149_I" deadCode="false" sourceNode="P_6F10149" targetNode="P_78F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_475F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_475F10149_O" deadCode="false" sourceNode="P_6F10149" targetNode="P_79F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_475F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_477F10149_I" deadCode="false" sourceNode="P_6F10149" targetNode="P_80F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_477F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_477F10149_O" deadCode="false" sourceNode="P_6F10149" targetNode="P_81F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_477F10149"/>
	</edges>
	<edges id="P_6F10149P_7F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10149" targetNode="P_7F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_482F10149_I" deadCode="false" sourceNode="P_78F10149" targetNode="P_82F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_482F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_482F10149_O" deadCode="false" sourceNode="P_78F10149" targetNode="P_83F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_482F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_487F10149_I" deadCode="false" sourceNode="P_78F10149" targetNode="P_82F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_487F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_487F10149_O" deadCode="false" sourceNode="P_78F10149" targetNode="P_83F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_487F10149"/>
	</edges>
	<edges id="P_78F10149P_79F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_78F10149" targetNode="P_79F10149"/>
	<edges id="P_80F10149P_81F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_80F10149" targetNode="P_81F10149"/>
	<edges id="P_82F10149P_83F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_82F10149" targetNode="P_83F10149"/>
	<edges xsi:type="cbl:PerformEdge" id="S_516F10149_I" deadCode="false" sourceNode="P_76F10149" targetNode="P_86F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_516F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_516F10149_O" deadCode="false" sourceNode="P_76F10149" targetNode="P_87F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_516F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_517F10149_I" deadCode="false" sourceNode="P_76F10149" targetNode="P_88F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_517F10149"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_517F10149_O" deadCode="false" sourceNode="P_76F10149" targetNode="P_89F10149">
		<representations href="../../../cobol/LDBS0730.cbl.cobModel#S_517F10149"/>
	</edges>
	<edges id="P_76F10149P_77F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_76F10149" targetNode="P_77F10149"/>
	<edges id="P_86F10149P_87F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_86F10149" targetNode="P_87F10149"/>
	<edges id="P_88F10149P_89F10149" xsi:type="cbl:FallThroughEdge" sourceNode="P_88F10149" targetNode="P_89F10149"/>
	<edges xsi:type="cbl:DataEdge" id="S_57F10149_POS1" deadCode="false" targetNode="P_18F10149" sourceNode="DB2_ADES">
		<representations href="../../../cobol/../importantStmts.cobModel#S_57F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_57F10149_POS2" deadCode="false" targetNode="P_18F10149" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_57F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_60F10149_POS1" deadCode="false" targetNode="P_20F10149" sourceNode="DB2_ADES">
		<representations href="../../../cobol/../importantStmts.cobModel#S_60F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_60F10149_POS2" deadCode="false" targetNode="P_20F10149" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_60F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_67F10149_POS1" deadCode="false" targetNode="P_24F10149" sourceNode="DB2_ADES">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_67F10149_POS2" deadCode="false" targetNode="P_24F10149" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_78F10149_POS1" deadCode="false" targetNode="P_26F10149" sourceNode="DB2_ADES">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_78F10149_POS2" deadCode="false" targetNode="P_26F10149" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_88F10149_POS1" deadCode="false" targetNode="P_28F10149" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_88F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_88F10149_POS2" deadCode="false" targetNode="P_28F10149" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_88F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_91F10149_POS1" deadCode="false" targetNode="P_30F10149" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_91F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_91F10149_POS2" deadCode="false" targetNode="P_30F10149" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_91F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_98F10149_POS1" deadCode="false" targetNode="P_34F10149" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_98F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_98F10149_POS2" deadCode="false" targetNode="P_34F10149" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_98F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_109F10149_POS1" deadCode="false" targetNode="P_36F10149" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_109F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_109F10149_POS2" deadCode="false" targetNode="P_36F10149" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_109F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_136F10149_POS1" deadCode="false" targetNode="P_48F10149" sourceNode="DB2_ADES">
		<representations href="../../../cobol/../importantStmts.cobModel#S_136F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_136F10149_POS2" deadCode="false" targetNode="P_48F10149" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_136F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_139F10149_POS1" deadCode="false" targetNode="P_50F10149" sourceNode="DB2_ADES">
		<representations href="../../../cobol/../importantStmts.cobModel#S_139F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_139F10149_POS2" deadCode="false" targetNode="P_50F10149" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_139F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_146F10149_POS1" deadCode="false" targetNode="P_54F10149" sourceNode="DB2_ADES">
		<representations href="../../../cobol/../importantStmts.cobModel#S_146F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_146F10149_POS2" deadCode="false" targetNode="P_54F10149" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_146F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_160F10149_POS1" deadCode="false" targetNode="P_56F10149" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_160F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_160F10149_POS2" deadCode="false" targetNode="P_56F10149" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_160F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_163F10149_POS1" deadCode="false" targetNode="P_58F10149" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_163F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_163F10149_POS2" deadCode="false" targetNode="P_58F10149" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_163F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_170F10149_POS1" deadCode="false" targetNode="P_62F10149" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_170F10149"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_170F10149_POS2" deadCode="false" targetNode="P_62F10149" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_170F10149"></representations>
	</edges>
</Package>

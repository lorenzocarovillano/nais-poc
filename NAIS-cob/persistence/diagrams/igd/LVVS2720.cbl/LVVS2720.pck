<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS2720" cbl:id="LVVS2720" xsi:id="LVVS2720" packageRef="LVVS2720.igd#LVVS2720" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS2720_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10372" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS2720.cbl.cobModel#SC_1F10372"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10372" deadCode="false" name="PROGRAM_LVVS2720_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_1F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10372" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_2F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10372" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_3F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10372" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_4F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10372" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_5F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10372" deadCode="false" name="VERIFICA-MOVIMENTO">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_8F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10372" deadCode="false" name="VERIFICA-MOVIMENTO-EX">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_9F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10372" deadCode="false" name="RECUP-MOVI-COMUN">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_18F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10372" deadCode="false" name="RECUP-MOVI-COMUN-EX">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_19F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10372" deadCode="false" name="CLOSE-MOVI">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_26F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10372" deadCode="false" name="CLOSE-MOVI-EX">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_27F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10372" deadCode="false" name="LETTURA-IMPST-BOLLO">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_22F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10372" deadCode="false" name="LETTURA-IMPST-BOLLO-EX">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_23F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10372" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_24F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10372" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_25F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10372" deadCode="false" name="S1150-CALCOLO-ANNO">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_10F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10372" deadCode="false" name="S1150-CALCOLO-ANNO-EX">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_11F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10372" deadCode="false" name="S1300-CALCOLO-SOMMA">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_12F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10372" deadCode="false" name="S1300-CALCOLO-SOMMA-EX">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_13F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10372" deadCode="false" name="S1390-CALCOLO-SOMMA-2">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_16F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10372" deadCode="false" name="S1390-CALCOLO-SOMMA-2-EX">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_17F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10372" deadCode="false" name="S1350-RECUPERO-INFO">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_14F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10372" deadCode="false" name="S1350-RECUPERO-INFO-EX">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_15F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10372" deadCode="false" name="ACCESSO-PERS">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_30F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10372" deadCode="false" name="ACCESSO-PERS-EX">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_31F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10372" deadCode="false" name="E000-CONVERTI-CHAR">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_32F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10372" deadCode="false" name="EX-E000">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_33F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10372" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_6F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10372" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_7F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10372" deadCode="false" name="VALORIZZA-OUTPUT-P58">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_28F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10372" deadCode="false" name="VALORIZZA-OUTPUT-P58-EX">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_29F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10372" deadCode="false" name="INIZIA-TOT-P58">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_20F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10372" deadCode="false" name="INIZIA-TOT-P58-EX">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_21F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10372" deadCode="false" name="INIZIA-NULL-P58">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_38F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10372" deadCode="false" name="INIZIA-NULL-P58-EX">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_39F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10372" deadCode="true" name="INIZIA-ZEROES-P58">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_34F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10372" deadCode="false" name="INIZIA-ZEROES-P58-EX">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_35F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10372" deadCode="true" name="INIZIA-SPACES-P58">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_36F10372"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10372" deadCode="false" name="INIZIA-SPACES-P58-EX">
				<representations href="../../../cobol/LVVS2720.cbl.cobModel#P_37F10372"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS6040" name="LDBS6040">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10230"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSE590" name="LDBSE590">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10264"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSE420" name="LDBSE420">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10263"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSE390" name="LDBSE390">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10262"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS1240" name="LDBS1240">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10152"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS1300" name="LDBS1300">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10154"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IWFS0050" name="IWFS0050">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10118"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10372P_1F10372" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10372" targetNode="P_1F10372"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10372_I" deadCode="false" sourceNode="P_1F10372" targetNode="P_2F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_1F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10372_O" deadCode="false" sourceNode="P_1F10372" targetNode="P_3F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_1F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10372_I" deadCode="false" sourceNode="P_1F10372" targetNode="P_4F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_2F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10372_O" deadCode="false" sourceNode="P_1F10372" targetNode="P_5F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_2F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10372_I" deadCode="false" sourceNode="P_1F10372" targetNode="P_6F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_3F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10372_O" deadCode="false" sourceNode="P_1F10372" targetNode="P_7F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_3F10372"/>
	</edges>
	<edges id="P_2F10372P_3F10372" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10372" targetNode="P_3F10372"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10372_I" deadCode="false" sourceNode="P_4F10372" targetNode="P_8F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_11F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10372_O" deadCode="false" sourceNode="P_4F10372" targetNode="P_9F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_11F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10372_I" deadCode="false" sourceNode="P_4F10372" targetNode="P_10F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_13F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10372_O" deadCode="false" sourceNode="P_4F10372" targetNode="P_11F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_13F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10372_I" deadCode="false" sourceNode="P_4F10372" targetNode="P_12F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_16F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10372_O" deadCode="false" sourceNode="P_4F10372" targetNode="P_13F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_16F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10372_I" deadCode="false" sourceNode="P_4F10372" targetNode="P_14F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_17F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10372_O" deadCode="false" sourceNode="P_4F10372" targetNode="P_15F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_17F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10372_I" deadCode="false" sourceNode="P_4F10372" targetNode="P_16F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_19F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10372_O" deadCode="false" sourceNode="P_4F10372" targetNode="P_17F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_19F10372"/>
	</edges>
	<edges id="P_4F10372P_5F10372" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10372" targetNode="P_5F10372"/>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10372_I" deadCode="false" sourceNode="P_8F10372" targetNode="P_18F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_23F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10372_O" deadCode="false" sourceNode="P_8F10372" targetNode="P_19F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_23F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10372_I" deadCode="false" sourceNode="P_8F10372" targetNode="P_20F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_29F10372"/>
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_30F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10372_O" deadCode="false" sourceNode="P_8F10372" targetNode="P_21F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_29F10372"/>
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_30F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10372_I" deadCode="false" sourceNode="P_8F10372" targetNode="P_22F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_31F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10372_O" deadCode="false" sourceNode="P_8F10372" targetNode="P_23F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_31F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10372_I" deadCode="false" sourceNode="P_8F10372" targetNode="P_24F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_32F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10372_O" deadCode="false" sourceNode="P_8F10372" targetNode="P_25F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_32F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10372_I" deadCode="false" sourceNode="P_8F10372" targetNode="P_24F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_33F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10372_O" deadCode="false" sourceNode="P_8F10372" targetNode="P_25F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_33F10372"/>
	</edges>
	<edges id="P_8F10372P_9F10372" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10372" targetNode="P_9F10372"/>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10372_I" deadCode="false" sourceNode="P_18F10372" targetNode="P_26F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_42F10372"/>
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_64F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10372_O" deadCode="false" sourceNode="P_18F10372" targetNode="P_27F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_42F10372"/>
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_64F10372"/>
	</edges>
	<edges id="P_18F10372P_19F10372" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10372" targetNode="P_19F10372"/>
	<edges id="P_26F10372P_27F10372" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10372" targetNode="P_27F10372"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10372_I" deadCode="false" sourceNode="P_22F10372" targetNode="P_28F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_92F10372"/>
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_110F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10372_O" deadCode="false" sourceNode="P_22F10372" targetNode="P_29F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_92F10372"/>
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_110F10372"/>
	</edges>
	<edges id="P_22F10372P_23F10372" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10372" targetNode="P_23F10372"/>
	<edges id="P_24F10372P_25F10372" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10372" targetNode="P_25F10372"/>
	<edges id="P_10F10372P_11F10372" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10372" targetNode="P_11F10372"/>
	<edges id="P_12F10372P_13F10372" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10372" targetNode="P_13F10372"/>
	<edges id="P_16F10372P_17F10372" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10372" targetNode="P_17F10372"/>
	<edges xsi:type="cbl:PerformEdge" id="S_247F10372_I" deadCode="false" sourceNode="P_14F10372" targetNode="P_30F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_247F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_247F10372_O" deadCode="false" sourceNode="P_14F10372" targetNode="P_31F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_247F10372"/>
	</edges>
	<edges id="P_14F10372P_15F10372" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10372" targetNode="P_15F10372"/>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10372_I" deadCode="false" sourceNode="P_30F10372" targetNode="P_32F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_255F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10372_O" deadCode="false" sourceNode="P_30F10372" targetNode="P_33F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_255F10372"/>
	</edges>
	<edges id="P_30F10372P_31F10372" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10372" targetNode="P_31F10372"/>
	<edges id="P_32F10372P_33F10372" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10372" targetNode="P_33F10372"/>
	<edges id="P_28F10372P_29F10372" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10372" targetNode="P_29F10372"/>
	<edges xsi:type="cbl:PerformEdge" id="S_335F10372_I" deadCode="true" sourceNode="P_20F10372" targetNode="P_34F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_335F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_335F10372_O" deadCode="true" sourceNode="P_20F10372" targetNode="P_35F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_335F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10372_I" deadCode="true" sourceNode="P_20F10372" targetNode="P_36F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_336F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10372_O" deadCode="true" sourceNode="P_20F10372" targetNode="P_37F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_336F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_337F10372_I" deadCode="false" sourceNode="P_20F10372" targetNode="P_38F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_337F10372"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_337F10372_O" deadCode="false" sourceNode="P_20F10372" targetNode="P_39F10372">
		<representations href="../../../cobol/LVVS2720.cbl.cobModel#S_337F10372"/>
	</edges>
	<edges id="P_20F10372P_21F10372" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10372" targetNode="P_21F10372"/>
	<edges id="P_38F10372P_39F10372" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10372" targetNode="P_39F10372"/>
	<edges id="P_34F10372P_35F10372" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10372" targetNode="P_35F10372"/>
	<edges id="P_36F10372P_37F10372" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10372" targetNode="P_37F10372"/>
	<edges xsi:type="cbl:CallEdge" id="S_50F10372" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_18F10372" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_50F10372"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_76F10372" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_26F10372" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_76F10372"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_99F10372" deadCode="false" name="Dynamic LDBSE590" sourceNode="P_22F10372" targetNode="LDBSE590">
		<representations href="../../../cobol/../importantStmts.cobModel#S_99F10372"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_152F10372" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_12F10372" targetNode="LDBSE420">
		<representations href="../../../cobol/../importantStmts.cobModel#S_152F10372"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_173F10372" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_12F10372" targetNode="LDBSE390">
		<representations href="../../../cobol/../importantStmts.cobModel#S_173F10372"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_197F10372" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_16F10372" targetNode="LDBSE420">
		<representations href="../../../cobol/../importantStmts.cobModel#S_197F10372"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_214F10372" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_16F10372" targetNode="LDBSE390">
		<representations href="../../../cobol/../importantStmts.cobModel#S_214F10372"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_242F10372" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_14F10372" targetNode="LDBS1240">
		<representations href="../../../cobol/../importantStmts.cobModel#S_242F10372"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_263F10372" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_30F10372" targetNode="LDBS1300">
		<representations href="../../../cobol/../importantStmts.cobModel#S_263F10372"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_288F10372" deadCode="false" name="Dynamic IWFS0050" sourceNode="P_32F10372" targetNode="IWFS0050">
		<representations href="../../../cobol/../importantStmts.cobModel#S_288F10372"></representations>
	</edges>
</Package>

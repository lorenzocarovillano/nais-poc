<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IDSS0160" cbl:id="IDSS0160" xsi:id="IDSS0160" packageRef="IDSS0160.igd#IDSS0160" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IDSS0160_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10104" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IDSS0160.cbl.cobModel#SC_1F10104"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10104" deadCode="false" name="PROGRAM_IDSS0160_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_1F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10104" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_2F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10104" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_3F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10104" deadCode="false" name="S0005-CTRL-DATI-INPUT">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_14F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10104" deadCode="false" name="EX-S0005">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_15F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10104" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_4F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10104" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_5F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10104" deadCode="false" name="S1100-LEGGI-COMP-NUM-OGG">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_18F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10104" deadCode="false" name="EX-S1100">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_19F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10104" deadCode="false" name="S1150-PREPARA-CNO">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_20F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10104" deadCode="false" name="EX-S1150">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_21F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10104" deadCode="false" name="S1101-LETTURA-CNO-OK">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_24F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10104" deadCode="false" name="EX-S1101">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_25F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10104" deadCode="false" name="S1102-LETTURA-CNO-100">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_26F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10104" deadCode="false" name="EX-S1102">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_27F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10104" deadCode="false" name="S110A-GEST-OUTPUT-CNO">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_32F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10104" deadCode="false" name="EX-S110A">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_33F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10104" deadCode="false" name="S110B-GEST-OUTPUT-CNO-P">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_30F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10104" deadCode="false" name="EX-S110B">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_31F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10104" deadCode="false" name="S110C-VALORIZZA-DATO-STATICO">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_38F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10104" deadCode="false" name="EX-S110C">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_39F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10104" deadCode="false" name="S1115-GEST-NUM-OGG">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_36F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10104" deadCode="false" name="EX-S1115">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_37F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10104" deadCode="false" name="S115A-LEGGI-NUM-OGG">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_52F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10104" deadCode="false" name="EX-S115A">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_53F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10104" deadCode="false" name="S115B-PREPARA-NOG">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_54F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10104" deadCode="false" name="EX-S115B">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_55F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10104" deadCode="false" name="S115C-GEST-OUTPUT-NOG">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_56F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10104" deadCode="false" name="EX-S115C">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_57F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10104" deadCode="false" name="S1116-GEST-PROGR-NUM-OGG">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_34F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10104" deadCode="false" name="EX-S1116">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_35F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10104" deadCode="false" name="S1117-CONVERTI-CHAR">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_60F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10104" deadCode="false" name="EX-S1117">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_61F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10104" deadCode="false" name="S116A-LEGGI-PROGR-OGG-UPD">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_62F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10104" deadCode="false" name="EX-S116A">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_63F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10104" deadCode="false" name="S116B-PREPARA-D03">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_64F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10104" deadCode="false" name="EX-S116B">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_65F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10104" deadCode="false" name="S116C-GEST-OUTPUT-D03">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_66F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10104" deadCode="false" name="EX-S116C">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_67F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10104" deadCode="false" name="S116D-INSERT-D03">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_68F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10104" deadCode="false" name="EX-S116D">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_69F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10104" deadCode="false" name="S116E-LEGGI-PROGR-OGG">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_58F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10104" deadCode="false" name="EX-S116E">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_59F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10104" deadCode="false" name="S2000-VERIFICA-DATO">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_40F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10104" deadCode="false" name="EX-S2000">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_41F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10104" deadCode="false" name="S2100-CALL-IDSS0020">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_70F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10104" deadCode="false" name="EX-S2100">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_71F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10104" deadCode="false" name="S2110-VALORIZZA-AREAT">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_74F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10104" deadCode="false" name="EX-S2110">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_75F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10104" deadCode="false" name="S3000-VALORIZZA-DATO">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_42F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10104" deadCode="false" name="EX-S3000">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_43F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10104" deadCode="false" name="S3100-SWITCH-DCLGEN-AREA-APPO">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_76F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10104" deadCode="false" name="EX-S3100">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_77F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10104" deadCode="false" name="S4000-CALCOLA-LUNGHEZZA">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_44F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10104" deadCode="false" name="EX-S4000">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_45F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10104" deadCode="false" name="S5000-CONCATENA-IB-OGG">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_46F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10104" deadCode="false" name="EX-S5000">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_47F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10104" deadCode="false" name="S5001-CONCATENA-KEY">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_48F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10104" deadCode="false" name="EX-S5001">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_49F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10104" deadCode="false" name="S6000-AGGIORNA-IB-OGG">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_50F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10104" deadCode="false" name="EX-S6000">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_51F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_80F10104" deadCode="false" name="S7000-GESTIONE-RAPP-RETE">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_80F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_81F10104" deadCode="false" name="EX-S7000">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_81F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_82F10104" deadCode="false" name="S7100-LETTURA-RAPP-RETE">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_82F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_83F10104" deadCode="false" name="EX-S7100">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_83F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10104" deadCode="false" name="A0010-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_10F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10104" deadCode="false" name="A0010-EX">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_11F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_78F10104" deadCode="false" name="Y9999-CHIAMA-IDSS0100">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_78F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_79F10104" deadCode="false" name="Y9999-EX">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_79F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10104" deadCode="false" name="S9001-ERRORE-COMUNE">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_28F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10104" deadCode="false" name="EX-S9001">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_29F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10104" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_6F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10104" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_7F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_84F10104" deadCode="false" name="VALORIZZA-OUTPUT-RRE">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_84F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_85F10104" deadCode="false" name="VALORIZZA-OUTPUT-RRE-EX">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_85F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10104" deadCode="false" name="INIZIA-TOT-D03">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_12F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10104" deadCode="false" name="INIZIA-TOT-D03-EX">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_13F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_90F10104" deadCode="false" name="INIZIA-NULL-D03">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_90F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_91F10104" deadCode="false" name="INIZIA-NULL-D03-EX">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_91F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_86F10104" deadCode="false" name="INIZIA-ZEROES-D03">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_86F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_87F10104" deadCode="false" name="INIZIA-ZEROES-D03-EX">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_87F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_88F10104" deadCode="true" name="INIZIA-SPACES-D03">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_88F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_89F10104" deadCode="false" name="INIZIA-SPACES-D03-EX">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_89F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10104" deadCode="false" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_72F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10104" deadCode="false" name="EX-S0290">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_73F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10104" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_16F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10104" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_17F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_94F10104" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_94F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_95F10104" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_95F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_92F10104" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_92F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_93F10104" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_93F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10104" deadCode="false" name="CALL-DISPATCHER">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_22F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10104" deadCode="false" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_23F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10104" deadCode="false" name="ESEGUI-DISPLAY">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_8F10104"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10104" deadCode="false" name="ESEGUI-DISPLAY-EX">
				<representations href="../../../cobol/IDSS0160.cbl.cobModel#P_9F10104"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IWFS0050" name="IWFS0050">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10118"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0020" name="IDSS0020">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10100"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0140" name="IDSS0140">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10102"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0010" name="IDSS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10099"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IJCSTMSP" name="IJCSTMSP" missing="true">
			<representations href="../../../../missing.xmi#ID4GAQVPM0Z12ED05MPMC3Q3PJLIIF0PGQOOXCDHDOPSGQEJV4ENDL"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS8880" name="IDSS8880">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10106"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10104P_1F10104" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10104" targetNode="P_1F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10104_I" deadCode="false" sourceNode="P_1F10104" targetNode="P_2F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_1F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10104_O" deadCode="false" sourceNode="P_1F10104" targetNode="P_3F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_1F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10104_I" deadCode="false" sourceNode="P_1F10104" targetNode="P_4F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_3F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10104_O" deadCode="false" sourceNode="P_1F10104" targetNode="P_5F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_3F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10104_I" deadCode="false" sourceNode="P_1F10104" targetNode="P_6F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_4F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10104_O" deadCode="false" sourceNode="P_1F10104" targetNode="P_7F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_4F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10104_I" deadCode="false" sourceNode="P_2F10104" targetNode="P_8F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_9F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10104_O" deadCode="false" sourceNode="P_2F10104" targetNode="P_9F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_9F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10104_I" deadCode="false" sourceNode="P_2F10104" targetNode="P_10F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_14F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10104_O" deadCode="false" sourceNode="P_2F10104" targetNode="P_11F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_14F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10104_I" deadCode="false" sourceNode="P_2F10104" targetNode="P_12F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_15F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10104_O" deadCode="false" sourceNode="P_2F10104" targetNode="P_13F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_15F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10104_I" deadCode="false" sourceNode="P_2F10104" targetNode="P_8F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_20F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10104_O" deadCode="false" sourceNode="P_2F10104" targetNode="P_9F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_20F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10104_I" deadCode="false" sourceNode="P_2F10104" targetNode="P_14F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_21F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10104_O" deadCode="false" sourceNode="P_2F10104" targetNode="P_15F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_21F10104"/>
	</edges>
	<edges id="P_2F10104P_3F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10104" targetNode="P_3F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10104_I" deadCode="false" sourceNode="P_14F10104" targetNode="P_16F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_28F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10104_O" deadCode="false" sourceNode="P_14F10104" targetNode="P_17F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_28F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10104_I" deadCode="false" sourceNode="P_14F10104" targetNode="P_16F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_34F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10104_O" deadCode="false" sourceNode="P_14F10104" targetNode="P_17F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_34F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10104_I" deadCode="false" sourceNode="P_14F10104" targetNode="P_16F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_40F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10104_O" deadCode="false" sourceNode="P_14F10104" targetNode="P_17F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_40F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10104_I" deadCode="false" sourceNode="P_14F10104" targetNode="P_16F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_47F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10104_O" deadCode="false" sourceNode="P_14F10104" targetNode="P_17F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_47F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10104_I" deadCode="false" sourceNode="P_14F10104" targetNode="P_16F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_54F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10104_O" deadCode="false" sourceNode="P_14F10104" targetNode="P_17F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_54F10104"/>
	</edges>
	<edges id="P_14F10104P_15F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10104" targetNode="P_15F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10104_I" deadCode="false" sourceNode="P_4F10104" targetNode="P_18F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_58F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10104_O" deadCode="false" sourceNode="P_4F10104" targetNode="P_19F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_58F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10104_I" deadCode="false" sourceNode="P_4F10104" targetNode="P_18F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_60F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10104_O" deadCode="false" sourceNode="P_4F10104" targetNode="P_19F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_60F10104"/>
	</edges>
	<edges id="P_4F10104P_5F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10104" targetNode="P_5F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10104_I" deadCode="false" sourceNode="P_18F10104" targetNode="P_20F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_62F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10104_O" deadCode="false" sourceNode="P_18F10104" targetNode="P_21F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_62F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10104_I" deadCode="false" sourceNode="P_18F10104" targetNode="P_22F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_63F10104"/>
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_64F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10104_O" deadCode="false" sourceNode="P_18F10104" targetNode="P_23F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_63F10104"/>
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_64F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10104_I" deadCode="false" sourceNode="P_18F10104" targetNode="P_24F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_63F10104"/>
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_67F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10104_O" deadCode="false" sourceNode="P_18F10104" targetNode="P_25F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_63F10104"/>
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_67F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10104_I" deadCode="false" sourceNode="P_18F10104" targetNode="P_26F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_63F10104"/>
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_68F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10104_O" deadCode="false" sourceNode="P_18F10104" targetNode="P_27F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_63F10104"/>
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_68F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10104_I" deadCode="false" sourceNode="P_18F10104" targetNode="P_28F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_63F10104"/>
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_69F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10104_O" deadCode="false" sourceNode="P_18F10104" targetNode="P_29F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_63F10104"/>
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_69F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10104_I" deadCode="false" sourceNode="P_18F10104" targetNode="P_28F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_63F10104"/>
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_70F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10104_O" deadCode="false" sourceNode="P_18F10104" targetNode="P_29F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_63F10104"/>
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_70F10104"/>
	</edges>
	<edges id="P_18F10104P_19F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10104" targetNode="P_19F10104"/>
	<edges id="P_20F10104P_21F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10104" targetNode="P_21F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10104_I" deadCode="false" sourceNode="P_24F10104" targetNode="P_30F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_93F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10104_O" deadCode="false" sourceNode="P_24F10104" targetNode="P_31F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_93F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10104_I" deadCode="false" sourceNode="P_24F10104" targetNode="P_32F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_94F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10104_O" deadCode="false" sourceNode="P_24F10104" targetNode="P_33F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_94F10104"/>
	</edges>
	<edges id="P_24F10104P_25F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10104" targetNode="P_25F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10104_I" deadCode="false" sourceNode="P_26F10104" targetNode="P_16F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_105F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10104_O" deadCode="false" sourceNode="P_26F10104" targetNode="P_17F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_105F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10104_I" deadCode="false" sourceNode="P_26F10104" targetNode="P_16F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_115F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10104_O" deadCode="false" sourceNode="P_26F10104" targetNode="P_17F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_115F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10104_I" deadCode="false" sourceNode="P_26F10104" targetNode="P_34F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_118F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10104_O" deadCode="false" sourceNode="P_26F10104" targetNode="P_35F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_118F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10104_I" deadCode="false" sourceNode="P_26F10104" targetNode="P_36F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_119F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10104_O" deadCode="false" sourceNode="P_26F10104" targetNode="P_37F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_119F10104"/>
	</edges>
	<edges id="P_26F10104P_27F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10104" targetNode="P_27F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10104_I" deadCode="false" sourceNode="P_32F10104" targetNode="P_38F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_128F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10104_O" deadCode="false" sourceNode="P_32F10104" targetNode="P_39F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_128F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_129F10104_I" deadCode="false" sourceNode="P_32F10104" targetNode="P_40F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_129F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_129F10104_O" deadCode="false" sourceNode="P_32F10104" targetNode="P_41F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_129F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10104_I" deadCode="false" sourceNode="P_32F10104" targetNode="P_42F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_131F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10104_O" deadCode="false" sourceNode="P_32F10104" targetNode="P_43F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_131F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_134F10104_I" deadCode="false" sourceNode="P_32F10104" targetNode="P_44F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_134F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_134F10104_O" deadCode="false" sourceNode="P_32F10104" targetNode="P_45F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_134F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10104_I" deadCode="false" sourceNode="P_32F10104" targetNode="P_46F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_138F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10104_O" deadCode="false" sourceNode="P_32F10104" targetNode="P_47F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_138F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10104_I" deadCode="false" sourceNode="P_32F10104" targetNode="P_48F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_140F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10104_O" deadCode="false" sourceNode="P_32F10104" targetNode="P_49F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_140F10104"/>
	</edges>
	<edges id="P_32F10104P_33F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10104" targetNode="P_33F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10104_I" deadCode="false" sourceNode="P_30F10104" targetNode="P_44F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_152F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10104_O" deadCode="false" sourceNode="P_30F10104" targetNode="P_45F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_152F10104"/>
	</edges>
	<edges id="P_30F10104P_31F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10104" targetNode="P_31F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10104_I" deadCode="false" sourceNode="P_38F10104" targetNode="P_44F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_163F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10104_O" deadCode="false" sourceNode="P_38F10104" targetNode="P_45F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_163F10104"/>
	</edges>
	<edges id="P_38F10104P_39F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10104" targetNode="P_39F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10104_I" deadCode="false" sourceNode="P_36F10104" targetNode="P_50F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_168F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10104_O" deadCode="false" sourceNode="P_36F10104" targetNode="P_51F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_168F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_169F10104_I" deadCode="false" sourceNode="P_36F10104" targetNode="P_52F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_169F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_169F10104_O" deadCode="false" sourceNode="P_36F10104" targetNode="P_53F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_169F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10104_I" deadCode="false" sourceNode="P_36F10104" targetNode="P_50F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_171F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10104_O" deadCode="false" sourceNode="P_36F10104" targetNode="P_51F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_171F10104"/>
	</edges>
	<edges id="P_36F10104P_37F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10104" targetNode="P_37F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10104_I" deadCode="false" sourceNode="P_52F10104" targetNode="P_54F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_173F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10104_O" deadCode="false" sourceNode="P_52F10104" targetNode="P_55F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_173F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10104_I" deadCode="false" sourceNode="P_52F10104" targetNode="P_22F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_174F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10104_O" deadCode="false" sourceNode="P_52F10104" targetNode="P_23F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_174F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_178F10104_I" deadCode="false" sourceNode="P_52F10104" targetNode="P_56F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_178F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_178F10104_O" deadCode="false" sourceNode="P_52F10104" targetNode="P_57F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_178F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_183F10104_I" deadCode="false" sourceNode="P_52F10104" targetNode="P_16F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_183F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_183F10104_O" deadCode="false" sourceNode="P_52F10104" targetNode="P_17F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_183F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10104_I" deadCode="false" sourceNode="P_52F10104" targetNode="P_28F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_184F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10104_O" deadCode="false" sourceNode="P_52F10104" targetNode="P_29F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_184F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10104_I" deadCode="false" sourceNode="P_52F10104" targetNode="P_28F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_185F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10104_O" deadCode="false" sourceNode="P_52F10104" targetNode="P_29F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_185F10104"/>
	</edges>
	<edges id="P_52F10104P_53F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10104" targetNode="P_53F10104"/>
	<edges id="P_54F10104P_55F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10104" targetNode="P_55F10104"/>
	<edges id="P_56F10104P_57F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10104" targetNode="P_57F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_209F10104_I" deadCode="false" sourceNode="P_34F10104" targetNode="P_50F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_209F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_209F10104_O" deadCode="false" sourceNode="P_34F10104" targetNode="P_51F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_209F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_210F10104_I" deadCode="false" sourceNode="P_34F10104" targetNode="P_58F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_210F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_210F10104_O" deadCode="false" sourceNode="P_34F10104" targetNode="P_59F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_210F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10104_I" deadCode="false" sourceNode="P_34F10104" targetNode="P_60F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_213F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10104_O" deadCode="false" sourceNode="P_34F10104" targetNode="P_61F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_213F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10104_I" deadCode="false" sourceNode="P_34F10104" targetNode="P_16F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_221F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10104_O" deadCode="false" sourceNode="P_34F10104" targetNode="P_17F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_221F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_224F10104_I" deadCode="false" sourceNode="P_34F10104" targetNode="P_50F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_224F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_224F10104_O" deadCode="false" sourceNode="P_34F10104" targetNode="P_51F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_224F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_229F10104_I" deadCode="false" sourceNode="P_34F10104" targetNode="P_16F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_229F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_229F10104_O" deadCode="false" sourceNode="P_34F10104" targetNode="P_17F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_229F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_239F10104_I" deadCode="false" sourceNode="P_34F10104" targetNode="P_16F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_239F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_239F10104_O" deadCode="false" sourceNode="P_34F10104" targetNode="P_17F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_239F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_241F10104_I" deadCode="false" sourceNode="P_34F10104" targetNode="P_62F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_241F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_241F10104_O" deadCode="false" sourceNode="P_34F10104" targetNode="P_63F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_241F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_243F10104_I" deadCode="false" sourceNode="P_34F10104" targetNode="P_50F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_243F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_243F10104_O" deadCode="false" sourceNode="P_34F10104" targetNode="P_51F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_243F10104"/>
	</edges>
	<edges id="P_34F10104P_35F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10104" targetNode="P_35F10104"/>
	<edges id="P_60F10104P_61F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_60F10104" targetNode="P_61F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_252F10104_I" deadCode="false" sourceNode="P_62F10104" targetNode="P_64F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_252F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_252F10104_O" deadCode="false" sourceNode="P_62F10104" targetNode="P_65F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_252F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10104_I" deadCode="false" sourceNode="P_62F10104" targetNode="P_22F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_253F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10104_O" deadCode="false" sourceNode="P_62F10104" targetNode="P_23F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_253F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_257F10104_I" deadCode="false" sourceNode="P_62F10104" targetNode="P_66F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_257F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_257F10104_O" deadCode="false" sourceNode="P_62F10104" targetNode="P_67F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_257F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_260F10104_I" deadCode="false" sourceNode="P_62F10104" targetNode="P_68F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_260F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_260F10104_O" deadCode="false" sourceNode="P_62F10104" targetNode="P_69F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_260F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_261F10104_I" deadCode="false" sourceNode="P_62F10104" targetNode="P_28F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_261F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_261F10104_O" deadCode="false" sourceNode="P_62F10104" targetNode="P_29F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_261F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10104_I" deadCode="false" sourceNode="P_62F10104" targetNode="P_28F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_262F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10104_O" deadCode="false" sourceNode="P_62F10104" targetNode="P_29F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_262F10104"/>
	</edges>
	<edges id="P_62F10104P_63F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10104" targetNode="P_63F10104"/>
	<edges id="P_64F10104P_65F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10104" targetNode="P_65F10104"/>
	<edges id="P_66F10104P_67F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10104" targetNode="P_67F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10104_I" deadCode="false" sourceNode="P_68F10104" targetNode="P_22F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_294F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10104_O" deadCode="false" sourceNode="P_68F10104" targetNode="P_23F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_294F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_298F10104_I" deadCode="false" sourceNode="P_68F10104" targetNode="P_28F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_298F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_298F10104_O" deadCode="false" sourceNode="P_68F10104" targetNode="P_29F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_298F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10104_I" deadCode="false" sourceNode="P_68F10104" targetNode="P_28F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_299F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10104_O" deadCode="false" sourceNode="P_68F10104" targetNode="P_29F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_299F10104"/>
	</edges>
	<edges id="P_68F10104P_69F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10104" targetNode="P_69F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_317F10104_I" deadCode="false" sourceNode="P_58F10104" targetNode="P_22F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_317F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_317F10104_O" deadCode="false" sourceNode="P_58F10104" targetNode="P_23F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_317F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_322F10104_I" deadCode="false" sourceNode="P_58F10104" targetNode="P_28F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_322F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_322F10104_O" deadCode="false" sourceNode="P_58F10104" targetNode="P_29F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_322F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_323F10104_I" deadCode="false" sourceNode="P_58F10104" targetNode="P_28F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_323F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_323F10104_O" deadCode="false" sourceNode="P_58F10104" targetNode="P_29F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_323F10104"/>
	</edges>
	<edges id="P_58F10104P_59F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10104" targetNode="P_59F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10104_I" deadCode="false" sourceNode="P_40F10104" targetNode="P_70F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_338F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10104_O" deadCode="false" sourceNode="P_40F10104" targetNode="P_71F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_338F10104"/>
	</edges>
	<edges id="P_40F10104P_41F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10104" targetNode="P_41F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_353F10104_I" deadCode="false" sourceNode="P_70F10104" targetNode="P_8F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_353F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_353F10104_O" deadCode="false" sourceNode="P_70F10104" targetNode="P_9F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_353F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_358F10104_I" deadCode="false" sourceNode="P_70F10104" targetNode="P_72F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_358F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_358F10104_O" deadCode="false" sourceNode="P_70F10104" targetNode="P_73F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_358F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_363F10104_I" deadCode="false" sourceNode="P_70F10104" targetNode="P_8F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_363F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_363F10104_O" deadCode="false" sourceNode="P_70F10104" targetNode="P_9F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_363F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_368F10104_I" deadCode="false" sourceNode="P_70F10104" targetNode="P_74F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_368F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_368F10104_O" deadCode="false" sourceNode="P_70F10104" targetNode="P_75F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_368F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_372F10104_I" deadCode="false" sourceNode="P_70F10104" targetNode="P_72F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_372F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_372F10104_O" deadCode="false" sourceNode="P_70F10104" targetNode="P_73F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_372F10104"/>
	</edges>
	<edges id="P_70F10104P_71F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_70F10104" targetNode="P_71F10104"/>
	<edges id="P_74F10104P_75F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10104" targetNode="P_75F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_393F10104_I" deadCode="false" sourceNode="P_42F10104" targetNode="P_76F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_393F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_393F10104_O" deadCode="false" sourceNode="P_42F10104" targetNode="P_77F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_393F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_400F10104_I" deadCode="false" sourceNode="P_42F10104" targetNode="P_78F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_400F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_400F10104_O" deadCode="false" sourceNode="P_42F10104" targetNode="P_79F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_400F10104"/>
	</edges>
	<edges id="P_42F10104P_43F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10104" targetNode="P_43F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_414F10104_I" deadCode="false" sourceNode="P_76F10104" targetNode="P_80F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_414F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_414F10104_O" deadCode="false" sourceNode="P_76F10104" targetNode="P_81F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_414F10104"/>
	</edges>
	<edges id="P_76F10104P_77F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_76F10104" targetNode="P_77F10104"/>
	<edges id="P_44F10104P_45F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10104" targetNode="P_45F10104"/>
	<edges id="P_46F10104P_47F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10104" targetNode="P_47F10104"/>
	<edges id="P_48F10104P_49F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10104" targetNode="P_49F10104"/>
	<edges id="P_50F10104P_51F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10104" targetNode="P_51F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_446F10104_I" deadCode="false" sourceNode="P_80F10104" targetNode="P_82F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_446F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_446F10104_O" deadCode="false" sourceNode="P_80F10104" targetNode="P_83F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_446F10104"/>
	</edges>
	<edges id="P_80F10104P_81F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_80F10104" targetNode="P_81F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_462F10104_I" deadCode="false" sourceNode="P_82F10104" targetNode="P_22F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_462F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_462F10104_O" deadCode="false" sourceNode="P_82F10104" targetNode="P_23F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_462F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_467F10104_I" deadCode="false" sourceNode="P_82F10104" targetNode="P_84F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_467F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_467F10104_O" deadCode="false" sourceNode="P_82F10104" targetNode="P_85F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_467F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_473F10104_I" deadCode="false" sourceNode="P_82F10104" targetNode="P_16F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_473F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_473F10104_O" deadCode="false" sourceNode="P_82F10104" targetNode="P_17F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_473F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_478F10104_I" deadCode="false" sourceNode="P_82F10104" targetNode="P_16F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_478F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_478F10104_O" deadCode="false" sourceNode="P_82F10104" targetNode="P_17F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_478F10104"/>
	</edges>
	<edges id="P_82F10104P_83F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_82F10104" targetNode="P_83F10104"/>
	<edges id="P_10F10104P_11F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10104" targetNode="P_11F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_492F10104_I" deadCode="false" sourceNode="P_78F10104" targetNode="P_16F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_492F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_492F10104_O" deadCode="false" sourceNode="P_78F10104" targetNode="P_17F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_492F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_498F10104_I" deadCode="false" sourceNode="P_78F10104" targetNode="P_16F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_498F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_498F10104_O" deadCode="false" sourceNode="P_78F10104" targetNode="P_17F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_498F10104"/>
	</edges>
	<edges id="P_78F10104P_79F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_78F10104" targetNode="P_79F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_504F10104_I" deadCode="false" sourceNode="P_28F10104" targetNode="P_16F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_504F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_504F10104_O" deadCode="false" sourceNode="P_28F10104" targetNode="P_17F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_504F10104"/>
	</edges>
	<edges id="P_28F10104P_29F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10104" targetNode="P_29F10104"/>
	<edges id="P_84F10104P_85F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_84F10104" targetNode="P_85F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_554F10104_I" deadCode="false" sourceNode="P_12F10104" targetNode="P_86F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_554F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_554F10104_O" deadCode="false" sourceNode="P_12F10104" targetNode="P_87F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_554F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_555F10104_I" deadCode="true" sourceNode="P_12F10104" targetNode="P_88F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_555F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_555F10104_O" deadCode="true" sourceNode="P_12F10104" targetNode="P_89F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_555F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_556F10104_I" deadCode="false" sourceNode="P_12F10104" targetNode="P_90F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_556F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_556F10104_O" deadCode="false" sourceNode="P_12F10104" targetNode="P_91F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_556F10104"/>
	</edges>
	<edges id="P_12F10104P_13F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10104" targetNode="P_13F10104"/>
	<edges id="P_90F10104P_91F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_90F10104" targetNode="P_91F10104"/>
	<edges id="P_86F10104P_87F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_86F10104" targetNode="P_87F10104"/>
	<edges id="P_88F10104P_89F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_88F10104" targetNode="P_89F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_570F10104_I" deadCode="false" sourceNode="P_72F10104" targetNode="P_16F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_570F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_570F10104_O" deadCode="false" sourceNode="P_72F10104" targetNode="P_17F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_570F10104"/>
	</edges>
	<edges id="P_72F10104P_73F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10104" targetNode="P_73F10104"/>
	<edges xsi:type="cbl:PerformEdge" id="S_577F10104_I" deadCode="false" sourceNode="P_16F10104" targetNode="P_92F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_577F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_577F10104_O" deadCode="false" sourceNode="P_16F10104" targetNode="P_93F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_577F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_581F10104_I" deadCode="false" sourceNode="P_16F10104" targetNode="P_94F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_581F10104"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_581F10104_O" deadCode="false" sourceNode="P_16F10104" targetNode="P_95F10104">
		<representations href="../../../cobol/IDSS0160.cbl.cobModel#S_581F10104"/>
	</edges>
	<edges id="P_16F10104P_17F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10104" targetNode="P_17F10104"/>
	<edges id="P_94F10104P_95F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_94F10104" targetNode="P_95F10104"/>
	<edges id="P_92F10104P_93F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_92F10104" targetNode="P_93F10104"/>
	<edges id="P_22F10104P_23F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10104" targetNode="P_23F10104"/>
	<edges id="P_8F10104P_9F10104" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10104" targetNode="P_9F10104"/>
	<edges xsi:type="cbl:CallEdge" id="S_248F10104" deadCode="false" name="Dynamic IWFS0050" sourceNode="P_60F10104" targetNode="IWFS0050">
		<representations href="../../../cobol/../importantStmts.cobModel#S_248F10104"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_354F10104" deadCode="false" name="Dynamic IDSS0020" sourceNode="P_70F10104" targetNode="IDSS0020">
		<representations href="../../../cobol/../importantStmts.cobModel#S_354F10104"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_487F10104" deadCode="false" name="Dynamic IDSS0140" sourceNode="P_78F10104" targetNode="IDSS0140">
		<representations href="../../../cobol/../importantStmts.cobModel#S_487F10104"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_575F10104" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_16F10104" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_575F10104"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_641F10104" deadCode="false" name="Dynamic IDSI0011-PGM" sourceNode="P_22F10104" targetNode="IDSS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_641F10104"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_655F10104" deadCode="false" name="Dynamic IDSV8888-CALL-TIMESTAMP" sourceNode="P_8F10104" targetNode="IJCSTMSP">
		<representations href="../../../cobol/../importantStmts.cobModel#S_655F10104"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_657F10104" deadCode="false" name="Dynamic IDSV8888-CALL-DISPLAY" sourceNode="P_8F10104" targetNode="IDSS8880">
		<representations href="../../../cobol/../importantStmts.cobModel#S_657F10104"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_667F10104" deadCode="false" name="Dynamic IDSV8888-CALL-TIMESTAMP" sourceNode="P_8F10104" targetNode="IJCSTMSP">
		<representations href="../../../cobol/../importantStmts.cobModel#S_667F10104"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_669F10104" deadCode="false" name="Dynamic IDSV8888-CALL-DISPLAY" sourceNode="P_8F10104" targetNode="IDSS8880">
		<representations href="../../../cobol/../importantStmts.cobModel#S_669F10104"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_674F10104" deadCode="false" name="Dynamic IDSV8888-CALL-DISPLAY" sourceNode="P_8F10104" targetNode="IDSS8880">
		<representations href="../../../cobol/../importantStmts.cobModel#S_674F10104"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS2780" cbl:id="LDBS2780" xsi:id="LDBS2780" packageRef="LDBS2780.igd#LDBS2780" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS2780_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10186" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS2780.cbl.cobModel#SC_1F10186"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10186" deadCode="false" name="PROGRAM_LDBS2780_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_1F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10186" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_2F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10186" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_3F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10186" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_8F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10186" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_9F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10186" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_4F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10186" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_5F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10186" deadCode="false" name="C210-UPDATE-WC-NST">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_10F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10186" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_11F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10186" deadCode="true" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_20F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10186" deadCode="true" name="Z100-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_21F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10186" deadCode="false" name="Z150-VALORIZZA-DATA-SERVICES">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_12F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10186" deadCode="false" name="Z150-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_13F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10186" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_14F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10186" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_15F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10186" deadCode="true" name="Z400-SEQ-RIGA">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_22F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10186" deadCode="true" name="Z400-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_23F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10186" deadCode="true" name="Z500-AGGIORNAMENTO-STORICO">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_24F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10186" deadCode="true" name="Z500-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_25F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10186" deadCode="true" name="Z600-INSERT-NUOVA-RIGA-STORICA">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_26F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10186" deadCode="true" name="Z600-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_27F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10186" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_16F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10186" deadCode="false" name="Z900-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_17F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10186" deadCode="true" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_30F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10186" deadCode="true" name="Z950-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_31F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10186" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_18F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10186" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_19F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10186" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_6F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10186" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_7F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10186" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_32F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10186" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_33F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10186" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_34F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10186" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_35F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10186" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_28F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10186" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_29F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10186" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_36F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10186" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_37F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10186" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_38F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10186" deadCode="true" name="Z800-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_43F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10186" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_39F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10186" deadCode="true" name="Z810-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_40F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10186" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_41F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10186" deadCode="true" name="Z820-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_42F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10186" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_44F10186"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10186" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS2780.cbl.cobModel#P_45F10186"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_BILA_TRCH_ESTR" name="BILA_TRCH_ESTR">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_BILA_TRCH_ESTR"/>
		</children>
	</packageNode>
	<edges id="SC_1F10186P_1F10186" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10186" targetNode="P_1F10186"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10186_I" deadCode="false" sourceNode="P_1F10186" targetNode="P_2F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_1F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10186_O" deadCode="false" sourceNode="P_1F10186" targetNode="P_3F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_1F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10186_I" deadCode="false" sourceNode="P_1F10186" targetNode="P_4F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_5F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10186_O" deadCode="false" sourceNode="P_1F10186" targetNode="P_5F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_5F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10186_I" deadCode="false" sourceNode="P_2F10186" targetNode="P_6F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_14F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10186_O" deadCode="false" sourceNode="P_2F10186" targetNode="P_7F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_14F10186"/>
	</edges>
	<edges id="P_2F10186P_3F10186" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10186" targetNode="P_3F10186"/>
	<edges id="P_8F10186P_9F10186" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10186" targetNode="P_9F10186"/>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10186_I" deadCode="false" sourceNode="P_4F10186" targetNode="P_10F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_27F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10186_O" deadCode="false" sourceNode="P_4F10186" targetNode="P_11F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_27F10186"/>
	</edges>
	<edges id="P_4F10186P_5F10186" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10186" targetNode="P_5F10186"/>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10186_I" deadCode="false" sourceNode="P_10F10186" targetNode="P_12F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_30F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10186_O" deadCode="false" sourceNode="P_10F10186" targetNode="P_13F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_30F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10186_I" deadCode="false" sourceNode="P_10F10186" targetNode="P_14F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_31F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10186_O" deadCode="false" sourceNode="P_10F10186" targetNode="P_15F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_31F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10186_I" deadCode="true" sourceNode="P_10F10186" targetNode="P_16F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_32F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10186_O" deadCode="true" sourceNode="P_10F10186" targetNode="P_17F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_32F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10186_I" deadCode="false" sourceNode="P_10F10186" targetNode="P_18F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_33F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10186_O" deadCode="false" sourceNode="P_10F10186" targetNode="P_19F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_33F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10186_I" deadCode="false" sourceNode="P_10F10186" targetNode="P_8F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_35F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10186_O" deadCode="false" sourceNode="P_10F10186" targetNode="P_9F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_35F10186"/>
	</edges>
	<edges id="P_10F10186P_11F10186" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10186" targetNode="P_11F10186"/>
	<edges id="P_12F10186P_13F10186" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10186" targetNode="P_13F10186"/>
	<edges id="P_14F10186P_15F10186" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10186" targetNode="P_15F10186"/>
	<edges xsi:type="cbl:PerformEdge" id="S_578F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_578F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_578F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_578F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_581F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_581F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_581F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_581F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_585F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_585F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_585F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_585F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_588F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_588F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_588F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_588F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_591F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_591F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_591F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_591F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_595F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_595F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_595F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_595F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_598F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_598F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_598F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_598F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_601F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_601F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_601F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_601F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_605F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_605F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_605F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_605F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_609F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_609F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_609F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_609F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_613F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_613F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_613F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_613F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_617F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_617F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_617F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_617F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_621F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_621F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_621F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_621F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_625F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_625F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_625F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_625F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_629F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_629F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_629F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_629F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_633F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_633F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_633F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_633F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_637F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_637F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_637F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_637F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_641F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_641F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_641F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_641F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_645F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_645F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_645F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_645F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_649F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_649F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_649F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_649F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_653F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_653F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_653F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_653F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_657F10186_I" deadCode="true" sourceNode="P_16F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_657F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_657F10186_O" deadCode="true" sourceNode="P_16F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_657F10186"/>
	</edges>
	<edges id="P_16F10186P_17F10186" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10186" targetNode="P_17F10186"/>
	<edges id="P_18F10186P_19F10186" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10186" targetNode="P_19F10186"/>
	<edges xsi:type="cbl:PerformEdge" id="S_664F10186_I" deadCode="false" sourceNode="P_6F10186" targetNode="P_32F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_664F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_664F10186_O" deadCode="false" sourceNode="P_6F10186" targetNode="P_33F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_664F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_666F10186_I" deadCode="false" sourceNode="P_6F10186" targetNode="P_34F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_666F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_666F10186_O" deadCode="false" sourceNode="P_6F10186" targetNode="P_35F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_666F10186"/>
	</edges>
	<edges id="P_6F10186P_7F10186" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10186" targetNode="P_7F10186"/>
	<edges xsi:type="cbl:PerformEdge" id="S_671F10186_I" deadCode="true" sourceNode="P_32F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_671F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_671F10186_O" deadCode="true" sourceNode="P_32F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_671F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_676F10186_I" deadCode="true" sourceNode="P_32F10186" targetNode="P_28F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_676F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_676F10186_O" deadCode="true" sourceNode="P_32F10186" targetNode="P_29F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_676F10186"/>
	</edges>
	<edges id="P_32F10186P_33F10186" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10186" targetNode="P_33F10186"/>
	<edges id="P_34F10186P_35F10186" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10186" targetNode="P_35F10186"/>
	<edges id="P_28F10186P_29F10186" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10186" targetNode="P_29F10186"/>
	<edges xsi:type="cbl:PerformEdge" id="S_705F10186_I" deadCode="true" sourceNode="P_38F10186" targetNode="P_39F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_705F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_705F10186_O" deadCode="true" sourceNode="P_38F10186" targetNode="P_40F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_705F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_706F10186_I" deadCode="true" sourceNode="P_38F10186" targetNode="P_41F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_706F10186"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_706F10186_O" deadCode="true" sourceNode="P_38F10186" targetNode="P_42F10186">
		<representations href="../../../cobol/LDBS2780.cbl.cobModel#S_706F10186"/>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_34F10186_POS1" deadCode="false" sourceNode="P_10F10186" targetNode="DB2_BILA_TRCH_ESTR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_34F10186"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IABS0080" cbl:id="IABS0080" xsi:id="IABS0080" packageRef="IABS0080.igd#IABS0080" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IABS0080_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10007" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IABS0080.cbl.cobModel#SC_1F10007"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10007" deadCode="false" name="PROGRAM_IABS0080_FIRST_SENTENCES">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_1F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10007" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_2F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10007" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_3F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10007" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_6F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10007" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_7F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10007" deadCode="false" name="A200-ELABORA-PK">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_4F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10007" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_5F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10007" deadCode="false" name="A210-SELECT-PK">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_8F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10007" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_9F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10007" deadCode="false" name="A220-INSERT-PK">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_10F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10007" deadCode="false" name="A220-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_11F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10007" deadCode="false" name="A230-UPDATE-PK">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_12F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10007" deadCode="false" name="A230-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_13F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10007" deadCode="false" name="A240-DELETE-PK">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_14F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10007" deadCode="false" name="A240-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_15F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10007" deadCode="false" name="Z080-ESTRAI-ID-EXECUTION">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_20F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10007" deadCode="false" name="Z080-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_21F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10007" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_16F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10007" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_17F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10007" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_22F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10007" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_23F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10007" deadCode="true" name="Z400-SEQ-RIGA">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_28F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10007" deadCode="true" name="Z400-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_29F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10007" deadCode="true" name="Z500-AGGIORNAMENTO-STORICO">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_30F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10007" deadCode="true" name="Z500-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_31F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10007" deadCode="true" name="Z600-INSERT-NUOVA-RIGA-STORICA">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_32F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10007" deadCode="true" name="Z600-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_33F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10007" deadCode="false" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_24F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10007" deadCode="false" name="Z900-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_25F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10007" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_18F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10007" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_19F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10007" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_26F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10007" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_27F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10007" deadCode="true" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_38F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10007" deadCode="true" name="A001-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_43F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10007" deadCode="true" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_39F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10007" deadCode="true" name="A020-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_40F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10007" deadCode="true" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_41F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10007" deadCode="true" name="A050-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_42F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10007" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_44F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10007" deadCode="true" name="Z700-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_45F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10007" deadCode="false" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_34F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10007" deadCode="false" name="Z701-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_35F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10007" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_46F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10007" deadCode="true" name="Z800-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_51F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10007" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_47F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10007" deadCode="true" name="Z810-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_48F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10007" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_49F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10007" deadCode="true" name="Z820-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_50F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10007" deadCode="false" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_36F10007"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10007" deadCode="false" name="Z801-EX">
				<representations href="../../../cobol/IABS0080.cbl.cobModel#P_37F10007"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_BTC_JOB_EXECUTION" name="BTC_JOB_EXECUTION">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_BTC_JOB_EXECUTION"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0090" name="LCCS0090">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10133"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10007P_1F10007" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10007" targetNode="P_1F10007"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10007_I" deadCode="false" sourceNode="P_1F10007" targetNode="P_2F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_1F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10007_O" deadCode="false" sourceNode="P_1F10007" targetNode="P_3F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_1F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10007_I" deadCode="false" sourceNode="P_1F10007" targetNode="P_4F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_3F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10007_O" deadCode="false" sourceNode="P_1F10007" targetNode="P_5F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_3F10007"/>
	</edges>
	<edges id="P_2F10007P_3F10007" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10007" targetNode="P_3F10007"/>
	<edges id="P_6F10007P_7F10007" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10007" targetNode="P_7F10007"/>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10007_I" deadCode="false" sourceNode="P_4F10007" targetNode="P_8F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_24F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10007_O" deadCode="false" sourceNode="P_4F10007" targetNode="P_9F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_24F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10007_I" deadCode="false" sourceNode="P_4F10007" targetNode="P_10F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_25F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10007_O" deadCode="false" sourceNode="P_4F10007" targetNode="P_11F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_25F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10007_I" deadCode="false" sourceNode="P_4F10007" targetNode="P_12F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_26F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10007_O" deadCode="false" sourceNode="P_4F10007" targetNode="P_13F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_26F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10007_I" deadCode="false" sourceNode="P_4F10007" targetNode="P_14F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_27F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10007_O" deadCode="false" sourceNode="P_4F10007" targetNode="P_15F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_27F10007"/>
	</edges>
	<edges id="P_4F10007P_5F10007" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10007" targetNode="P_5F10007"/>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10007_I" deadCode="false" sourceNode="P_8F10007" targetNode="P_6F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_31F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10007_O" deadCode="false" sourceNode="P_8F10007" targetNode="P_7F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_31F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10007_I" deadCode="false" sourceNode="P_8F10007" targetNode="P_16F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_33F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10007_O" deadCode="false" sourceNode="P_8F10007" targetNode="P_17F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_33F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10007_I" deadCode="false" sourceNode="P_8F10007" targetNode="P_18F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_34F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10007_O" deadCode="false" sourceNode="P_8F10007" targetNode="P_19F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_34F10007"/>
	</edges>
	<edges id="P_8F10007P_9F10007" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10007" targetNode="P_9F10007"/>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10007_I" deadCode="false" sourceNode="P_10F10007" targetNode="P_20F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_36F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10007_O" deadCode="false" sourceNode="P_10F10007" targetNode="P_21F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_36F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10007_I" deadCode="false" sourceNode="P_10F10007" targetNode="P_22F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_38F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10007_O" deadCode="false" sourceNode="P_10F10007" targetNode="P_23F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_38F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10007_I" deadCode="false" sourceNode="P_10F10007" targetNode="P_24F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_39F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10007_O" deadCode="false" sourceNode="P_10F10007" targetNode="P_25F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_39F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10007_I" deadCode="false" sourceNode="P_10F10007" targetNode="P_26F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_40F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10007_O" deadCode="false" sourceNode="P_10F10007" targetNode="P_27F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_40F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10007_I" deadCode="false" sourceNode="P_10F10007" targetNode="P_6F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_43F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10007_O" deadCode="false" sourceNode="P_10F10007" targetNode="P_7F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_43F10007"/>
	</edges>
	<edges id="P_10F10007P_11F10007" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10007" targetNode="P_11F10007"/>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10007_I" deadCode="false" sourceNode="P_12F10007" targetNode="P_22F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_46F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10007_O" deadCode="false" sourceNode="P_12F10007" targetNode="P_23F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_46F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10007_I" deadCode="false" sourceNode="P_12F10007" targetNode="P_24F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_47F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10007_O" deadCode="false" sourceNode="P_12F10007" targetNode="P_25F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_47F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10007_I" deadCode="false" sourceNode="P_12F10007" targetNode="P_26F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_48F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10007_O" deadCode="false" sourceNode="P_12F10007" targetNode="P_27F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_48F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10007_I" deadCode="false" sourceNode="P_12F10007" targetNode="P_6F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_50F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10007_O" deadCode="false" sourceNode="P_12F10007" targetNode="P_7F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_50F10007"/>
	</edges>
	<edges id="P_12F10007P_13F10007" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10007" targetNode="P_13F10007"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10007_I" deadCode="false" sourceNode="P_14F10007" targetNode="P_6F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_53F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10007_O" deadCode="false" sourceNode="P_14F10007" targetNode="P_7F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_53F10007"/>
	</edges>
	<edges id="P_14F10007P_15F10007" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10007" targetNode="P_15F10007"/>
	<edges id="P_20F10007P_21F10007" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10007" targetNode="P_21F10007"/>
	<edges id="P_16F10007P_17F10007" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10007" targetNode="P_17F10007"/>
	<edges id="P_22F10007P_23F10007" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10007" targetNode="P_23F10007"/>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10007_I" deadCode="false" sourceNode="P_24F10007" targetNode="P_34F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_89F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10007_O" deadCode="false" sourceNode="P_24F10007" targetNode="P_35F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_89F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10007_I" deadCode="false" sourceNode="P_24F10007" targetNode="P_34F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_93F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10007_O" deadCode="false" sourceNode="P_24F10007" targetNode="P_35F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_93F10007"/>
	</edges>
	<edges id="P_24F10007P_25F10007" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10007" targetNode="P_25F10007"/>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10007_I" deadCode="false" sourceNode="P_18F10007" targetNode="P_36F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_97F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10007_O" deadCode="false" sourceNode="P_18F10007" targetNode="P_37F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_97F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10007_I" deadCode="false" sourceNode="P_18F10007" targetNode="P_36F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_101F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10007_O" deadCode="false" sourceNode="P_18F10007" targetNode="P_37F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_101F10007"/>
	</edges>
	<edges id="P_18F10007P_19F10007" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10007" targetNode="P_19F10007"/>
	<edges id="P_26F10007P_27F10007" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10007" targetNode="P_27F10007"/>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10007_I" deadCode="true" sourceNode="P_38F10007" targetNode="P_39F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_106F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10007_O" deadCode="true" sourceNode="P_38F10007" targetNode="P_40F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_106F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10007_I" deadCode="true" sourceNode="P_38F10007" targetNode="P_41F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_108F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10007_O" deadCode="true" sourceNode="P_38F10007" targetNode="P_42F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_108F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10007_I" deadCode="true" sourceNode="P_39F10007" targetNode="P_44F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_113F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10007_O" deadCode="true" sourceNode="P_39F10007" targetNode="P_45F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_113F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10007_I" deadCode="true" sourceNode="P_39F10007" targetNode="P_44F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_118F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10007_O" deadCode="true" sourceNode="P_39F10007" targetNode="P_45F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_118F10007"/>
	</edges>
	<edges id="P_34F10007P_35F10007" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10007" targetNode="P_35F10007"/>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10007_I" deadCode="true" sourceNode="P_46F10007" targetNode="P_47F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_147F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10007_O" deadCode="true" sourceNode="P_46F10007" targetNode="P_48F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_147F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10007_I" deadCode="true" sourceNode="P_46F10007" targetNode="P_49F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_148F10007"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10007_O" deadCode="true" sourceNode="P_46F10007" targetNode="P_50F10007">
		<representations href="../../../cobol/IABS0080.cbl.cobModel#S_148F10007"/>
	</edges>
	<edges id="P_36F10007P_37F10007" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10007" targetNode="P_37F10007"/>
	<edges xsi:type="cbl:DataEdge" id="S_30F10007_POS1" deadCode="false" targetNode="P_8F10007" sourceNode="DB2_BTC_JOB_EXECUTION">
		<representations href="../../../cobol/../importantStmts.cobModel#S_30F10007"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_42F10007_POS1" deadCode="false" sourceNode="P_10F10007" targetNode="DB2_BTC_JOB_EXECUTION">
		<representations href="../../../cobol/../importantStmts.cobModel#S_42F10007"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_49F10007_POS1" deadCode="false" sourceNode="P_12F10007" targetNode="DB2_BTC_JOB_EXECUTION">
		<representations href="../../../cobol/../importantStmts.cobModel#S_49F10007"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_52F10007_POS1" deadCode="false" sourceNode="P_14F10007" targetNode="DB2_BTC_JOB_EXECUTION">
		<representations href="../../../cobol/../importantStmts.cobModel#S_52F10007"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_57F10007" deadCode="false" name="Dynamic PGM-LCCS0090" sourceNode="P_20F10007" targetNode="LCCS0090">
		<representations href="../../../cobol/../importantStmts.cobModel#S_57F10007"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS3360" cbl:id="LDBS3360" xsi:id="LDBS3360" packageRef="LDBS3360.igd#LDBS3360" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS3360_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10197" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS3360.cbl.cobModel#SC_1F10197"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10197" deadCode="false" name="PROGRAM_LDBS3360_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_1F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10197" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_2F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10197" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_3F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10197" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_12F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10197" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_13F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10197" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_4F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10197" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_5F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10197" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_6F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10197" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_7F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10197" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_8F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10197" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_9F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10197" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_44F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10197" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_49F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10197" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_14F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10197" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_15F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10197" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_16F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10197" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_17F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10197" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_18F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10197" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_19F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10197" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_20F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10197" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_21F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10197" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_22F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10197" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_23F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10197" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_56F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10197" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_57F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10197" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_24F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10197" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_25F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10197" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_26F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10197" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_27F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10197" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_28F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10197" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_29F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10197" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_30F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10197" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_31F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10197" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_32F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10197" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_33F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10197" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_58F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10197" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_59F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10197" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_34F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10197" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_35F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10197" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_36F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10197" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_37F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10197" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_38F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10197" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_39F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10197" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_40F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10197" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_41F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10197" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_42F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10197" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_43F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10197" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_50F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10197" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_51F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10197" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_60F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10197" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_63F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10197" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_52F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10197" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_53F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10197" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_45F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10197" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_46F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10197" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_47F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10197" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_48F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10197" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_54F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10197" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_55F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10197" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_10F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10197" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_11F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10197" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_66F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10197" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_67F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10197" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_68F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10197" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_69F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10197" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_61F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10197" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_62F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10197" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_70F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10197" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_71F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10197" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_64F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10197" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_65F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10197" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_72F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10197" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_73F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10197" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_74F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10197" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_75F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10197" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_76F10197"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10197" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS3360.cbl.cobModel#P_77F10197"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_OGG_DEROGA" name="OGG_DEROGA">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_OGG_DEROGA"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_STAT_OGG_WF" name="STAT_OGG_WF" missing="true">
			<representations href="../../../../missing.xmi#"/>
		</children>
	</packageNode>
	<edges id="SC_1F10197P_1F10197" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10197" targetNode="P_1F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10197_I" deadCode="false" sourceNode="P_1F10197" targetNode="P_2F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_2F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10197_O" deadCode="false" sourceNode="P_1F10197" targetNode="P_3F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_2F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10197_I" deadCode="false" sourceNode="P_1F10197" targetNode="P_4F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_6F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10197_O" deadCode="false" sourceNode="P_1F10197" targetNode="P_5F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_6F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10197_I" deadCode="false" sourceNode="P_1F10197" targetNode="P_6F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_10F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10197_O" deadCode="false" sourceNode="P_1F10197" targetNode="P_7F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_10F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10197_I" deadCode="false" sourceNode="P_1F10197" targetNode="P_8F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_14F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10197_O" deadCode="false" sourceNode="P_1F10197" targetNode="P_9F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_14F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10197_I" deadCode="false" sourceNode="P_2F10197" targetNode="P_10F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_24F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10197_O" deadCode="false" sourceNode="P_2F10197" targetNode="P_11F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_24F10197"/>
	</edges>
	<edges id="P_2F10197P_3F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10197" targetNode="P_3F10197"/>
	<edges id="P_12F10197P_13F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10197" targetNode="P_13F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10197_I" deadCode="false" sourceNode="P_4F10197" targetNode="P_14F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_37F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10197_O" deadCode="false" sourceNode="P_4F10197" targetNode="P_15F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_37F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10197_I" deadCode="false" sourceNode="P_4F10197" targetNode="P_16F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_38F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10197_O" deadCode="false" sourceNode="P_4F10197" targetNode="P_17F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_38F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10197_I" deadCode="false" sourceNode="P_4F10197" targetNode="P_18F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_39F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10197_O" deadCode="false" sourceNode="P_4F10197" targetNode="P_19F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_39F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10197_I" deadCode="false" sourceNode="P_4F10197" targetNode="P_20F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_40F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10197_O" deadCode="false" sourceNode="P_4F10197" targetNode="P_21F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_40F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10197_I" deadCode="false" sourceNode="P_4F10197" targetNode="P_22F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_41F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10197_O" deadCode="false" sourceNode="P_4F10197" targetNode="P_23F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_41F10197"/>
	</edges>
	<edges id="P_4F10197P_5F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10197" targetNode="P_5F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10197_I" deadCode="false" sourceNode="P_6F10197" targetNode="P_24F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_45F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10197_O" deadCode="false" sourceNode="P_6F10197" targetNode="P_25F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_45F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10197_I" deadCode="false" sourceNode="P_6F10197" targetNode="P_26F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_46F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10197_O" deadCode="false" sourceNode="P_6F10197" targetNode="P_27F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_46F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10197_I" deadCode="false" sourceNode="P_6F10197" targetNode="P_28F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_47F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10197_O" deadCode="false" sourceNode="P_6F10197" targetNode="P_29F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_47F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10197_I" deadCode="false" sourceNode="P_6F10197" targetNode="P_30F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_48F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10197_O" deadCode="false" sourceNode="P_6F10197" targetNode="P_31F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_48F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10197_I" deadCode="false" sourceNode="P_6F10197" targetNode="P_32F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_49F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10197_O" deadCode="false" sourceNode="P_6F10197" targetNode="P_33F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_49F10197"/>
	</edges>
	<edges id="P_6F10197P_7F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10197" targetNode="P_7F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10197_I" deadCode="false" sourceNode="P_8F10197" targetNode="P_34F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_53F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10197_O" deadCode="false" sourceNode="P_8F10197" targetNode="P_35F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_53F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10197_I" deadCode="false" sourceNode="P_8F10197" targetNode="P_36F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_54F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10197_O" deadCode="false" sourceNode="P_8F10197" targetNode="P_37F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_54F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10197_I" deadCode="false" sourceNode="P_8F10197" targetNode="P_38F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_55F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10197_O" deadCode="false" sourceNode="P_8F10197" targetNode="P_39F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_55F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10197_I" deadCode="false" sourceNode="P_8F10197" targetNode="P_40F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_56F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10197_O" deadCode="false" sourceNode="P_8F10197" targetNode="P_41F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_56F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10197_I" deadCode="false" sourceNode="P_8F10197" targetNode="P_42F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_57F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10197_O" deadCode="false" sourceNode="P_8F10197" targetNode="P_43F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_57F10197"/>
	</edges>
	<edges id="P_8F10197P_9F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10197" targetNode="P_9F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10197_I" deadCode="false" sourceNode="P_44F10197" targetNode="P_45F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_60F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10197_O" deadCode="false" sourceNode="P_44F10197" targetNode="P_46F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_60F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10197_I" deadCode="false" sourceNode="P_44F10197" targetNode="P_47F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_61F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10197_O" deadCode="false" sourceNode="P_44F10197" targetNode="P_48F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_61F10197"/>
	</edges>
	<edges id="P_44F10197P_49F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10197" targetNode="P_49F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10197_I" deadCode="false" sourceNode="P_14F10197" targetNode="P_45F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_65F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10197_O" deadCode="false" sourceNode="P_14F10197" targetNode="P_46F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_65F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10197_I" deadCode="false" sourceNode="P_14F10197" targetNode="P_47F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_66F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10197_O" deadCode="false" sourceNode="P_14F10197" targetNode="P_48F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_66F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10197_I" deadCode="false" sourceNode="P_14F10197" targetNode="P_12F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_68F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10197_O" deadCode="false" sourceNode="P_14F10197" targetNode="P_13F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_68F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10197_I" deadCode="false" sourceNode="P_14F10197" targetNode="P_50F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_70F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10197_O" deadCode="false" sourceNode="P_14F10197" targetNode="P_51F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_70F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10197_I" deadCode="false" sourceNode="P_14F10197" targetNode="P_52F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_71F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10197_O" deadCode="false" sourceNode="P_14F10197" targetNode="P_53F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_71F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10197_I" deadCode="false" sourceNode="P_14F10197" targetNode="P_54F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_72F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10197_O" deadCode="false" sourceNode="P_14F10197" targetNode="P_55F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_72F10197"/>
	</edges>
	<edges id="P_14F10197P_15F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10197" targetNode="P_15F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10197_I" deadCode="false" sourceNode="P_16F10197" targetNode="P_44F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_74F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10197_O" deadCode="false" sourceNode="P_16F10197" targetNode="P_49F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_74F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10197_I" deadCode="false" sourceNode="P_16F10197" targetNode="P_12F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_76F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10197_O" deadCode="false" sourceNode="P_16F10197" targetNode="P_13F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_76F10197"/>
	</edges>
	<edges id="P_16F10197P_17F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10197" targetNode="P_17F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10197_I" deadCode="false" sourceNode="P_18F10197" targetNode="P_12F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_79F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10197_O" deadCode="false" sourceNode="P_18F10197" targetNode="P_13F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_79F10197"/>
	</edges>
	<edges id="P_18F10197P_19F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10197" targetNode="P_19F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10197_I" deadCode="false" sourceNode="P_20F10197" targetNode="P_16F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_81F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10197_O" deadCode="false" sourceNode="P_20F10197" targetNode="P_17F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_81F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10197_I" deadCode="false" sourceNode="P_20F10197" targetNode="P_22F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_83F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10197_O" deadCode="false" sourceNode="P_20F10197" targetNode="P_23F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_83F10197"/>
	</edges>
	<edges id="P_20F10197P_21F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10197" targetNode="P_21F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10197_I" deadCode="false" sourceNode="P_22F10197" targetNode="P_12F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_86F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10197_O" deadCode="false" sourceNode="P_22F10197" targetNode="P_13F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_86F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10197_I" deadCode="false" sourceNode="P_22F10197" targetNode="P_50F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_88F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10197_O" deadCode="false" sourceNode="P_22F10197" targetNode="P_51F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_88F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10197_I" deadCode="false" sourceNode="P_22F10197" targetNode="P_52F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_89F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10197_O" deadCode="false" sourceNode="P_22F10197" targetNode="P_53F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_89F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10197_I" deadCode="false" sourceNode="P_22F10197" targetNode="P_54F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_90F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10197_O" deadCode="false" sourceNode="P_22F10197" targetNode="P_55F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_90F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10197_I" deadCode="false" sourceNode="P_22F10197" targetNode="P_18F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_92F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10197_O" deadCode="false" sourceNode="P_22F10197" targetNode="P_19F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_92F10197"/>
	</edges>
	<edges id="P_22F10197P_23F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10197" targetNode="P_23F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10197_I" deadCode="false" sourceNode="P_56F10197" targetNode="P_45F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_96F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10197_O" deadCode="false" sourceNode="P_56F10197" targetNode="P_46F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_96F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10197_I" deadCode="false" sourceNode="P_56F10197" targetNode="P_47F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_97F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10197_O" deadCode="false" sourceNode="P_56F10197" targetNode="P_48F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_97F10197"/>
	</edges>
	<edges id="P_56F10197P_57F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10197" targetNode="P_57F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10197_I" deadCode="false" sourceNode="P_24F10197" targetNode="P_45F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_101F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10197_O" deadCode="false" sourceNode="P_24F10197" targetNode="P_46F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_101F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10197_I" deadCode="false" sourceNode="P_24F10197" targetNode="P_47F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_102F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10197_O" deadCode="false" sourceNode="P_24F10197" targetNode="P_48F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_102F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10197_I" deadCode="false" sourceNode="P_24F10197" targetNode="P_12F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_104F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10197_O" deadCode="false" sourceNode="P_24F10197" targetNode="P_13F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_104F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10197_I" deadCode="false" sourceNode="P_24F10197" targetNode="P_50F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_106F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10197_O" deadCode="false" sourceNode="P_24F10197" targetNode="P_51F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_106F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10197_I" deadCode="false" sourceNode="P_24F10197" targetNode="P_52F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_107F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10197_O" deadCode="false" sourceNode="P_24F10197" targetNode="P_53F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_107F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10197_I" deadCode="false" sourceNode="P_24F10197" targetNode="P_54F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_108F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10197_O" deadCode="false" sourceNode="P_24F10197" targetNode="P_55F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_108F10197"/>
	</edges>
	<edges id="P_24F10197P_25F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10197" targetNode="P_25F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10197_I" deadCode="false" sourceNode="P_26F10197" targetNode="P_56F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_110F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10197_O" deadCode="false" sourceNode="P_26F10197" targetNode="P_57F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_110F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10197_I" deadCode="false" sourceNode="P_26F10197" targetNode="P_12F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_112F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10197_O" deadCode="false" sourceNode="P_26F10197" targetNode="P_13F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_112F10197"/>
	</edges>
	<edges id="P_26F10197P_27F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10197" targetNode="P_27F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10197_I" deadCode="false" sourceNode="P_28F10197" targetNode="P_12F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_115F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10197_O" deadCode="false" sourceNode="P_28F10197" targetNode="P_13F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_115F10197"/>
	</edges>
	<edges id="P_28F10197P_29F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10197" targetNode="P_29F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10197_I" deadCode="false" sourceNode="P_30F10197" targetNode="P_26F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_117F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10197_O" deadCode="false" sourceNode="P_30F10197" targetNode="P_27F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_117F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10197_I" deadCode="false" sourceNode="P_30F10197" targetNode="P_32F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_119F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10197_O" deadCode="false" sourceNode="P_30F10197" targetNode="P_33F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_119F10197"/>
	</edges>
	<edges id="P_30F10197P_31F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10197" targetNode="P_31F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10197_I" deadCode="false" sourceNode="P_32F10197" targetNode="P_12F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_122F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10197_O" deadCode="false" sourceNode="P_32F10197" targetNode="P_13F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_122F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10197_I" deadCode="false" sourceNode="P_32F10197" targetNode="P_50F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_124F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10197_O" deadCode="false" sourceNode="P_32F10197" targetNode="P_51F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_124F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10197_I" deadCode="false" sourceNode="P_32F10197" targetNode="P_52F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_125F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10197_O" deadCode="false" sourceNode="P_32F10197" targetNode="P_53F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_125F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10197_I" deadCode="false" sourceNode="P_32F10197" targetNode="P_54F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_126F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10197_O" deadCode="false" sourceNode="P_32F10197" targetNode="P_55F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_126F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10197_I" deadCode="false" sourceNode="P_32F10197" targetNode="P_28F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_128F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10197_O" deadCode="false" sourceNode="P_32F10197" targetNode="P_29F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_128F10197"/>
	</edges>
	<edges id="P_32F10197P_33F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10197" targetNode="P_33F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10197_I" deadCode="false" sourceNode="P_58F10197" targetNode="P_45F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_132F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10197_O" deadCode="false" sourceNode="P_58F10197" targetNode="P_46F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_132F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10197_I" deadCode="false" sourceNode="P_58F10197" targetNode="P_47F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_133F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10197_O" deadCode="false" sourceNode="P_58F10197" targetNode="P_48F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_133F10197"/>
	</edges>
	<edges id="P_58F10197P_59F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10197" targetNode="P_59F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10197_I" deadCode="false" sourceNode="P_34F10197" targetNode="P_45F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_136F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10197_O" deadCode="false" sourceNode="P_34F10197" targetNode="P_46F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_136F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10197_I" deadCode="false" sourceNode="P_34F10197" targetNode="P_47F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_137F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10197_O" deadCode="false" sourceNode="P_34F10197" targetNode="P_48F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_137F10197"/>
	</edges>
	<edges id="P_34F10197P_35F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10197" targetNode="P_35F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10197_I" deadCode="false" sourceNode="P_36F10197" targetNode="P_58F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_140F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10197_O" deadCode="false" sourceNode="P_36F10197" targetNode="P_59F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_140F10197"/>
	</edges>
	<edges id="P_36F10197P_37F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10197" targetNode="P_37F10197"/>
	<edges id="P_38F10197P_39F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10197" targetNode="P_39F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10197_I" deadCode="false" sourceNode="P_40F10197" targetNode="P_36F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_145F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10197_O" deadCode="false" sourceNode="P_40F10197" targetNode="P_37F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_145F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10197_I" deadCode="false" sourceNode="P_40F10197" targetNode="P_42F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_147F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10197_O" deadCode="false" sourceNode="P_40F10197" targetNode="P_43F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_147F10197"/>
	</edges>
	<edges id="P_40F10197P_41F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10197" targetNode="P_41F10197"/>
	<edges id="P_42F10197P_43F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10197" targetNode="P_43F10197"/>
	<edges id="P_50F10197P_51F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10197" targetNode="P_51F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10197_I" deadCode="true" sourceNode="P_60F10197" targetNode="P_61F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_160F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10197_O" deadCode="true" sourceNode="P_60F10197" targetNode="P_62F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_160F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10197_I" deadCode="true" sourceNode="P_60F10197" targetNode="P_61F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_163F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10197_O" deadCode="true" sourceNode="P_60F10197" targetNode="P_62F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_163F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10197_I" deadCode="false" sourceNode="P_52F10197" targetNode="P_64F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_167F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10197_O" deadCode="false" sourceNode="P_52F10197" targetNode="P_65F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_167F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10197_I" deadCode="false" sourceNode="P_52F10197" targetNode="P_64F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_170F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10197_O" deadCode="false" sourceNode="P_52F10197" targetNode="P_65F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_170F10197"/>
	</edges>
	<edges id="P_52F10197P_53F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10197" targetNode="P_53F10197"/>
	<edges id="P_45F10197P_46F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10197" targetNode="P_46F10197"/>
	<edges id="P_47F10197P_48F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10197" targetNode="P_48F10197"/>
	<edges id="P_54F10197P_55F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10197" targetNode="P_55F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10197_I" deadCode="false" sourceNode="P_10F10197" targetNode="P_66F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_179F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10197_O" deadCode="false" sourceNode="P_10F10197" targetNode="P_67F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_179F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10197_I" deadCode="false" sourceNode="P_10F10197" targetNode="P_68F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_181F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10197_O" deadCode="false" sourceNode="P_10F10197" targetNode="P_69F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_181F10197"/>
	</edges>
	<edges id="P_10F10197P_11F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10197" targetNode="P_11F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10197_I" deadCode="false" sourceNode="P_66F10197" targetNode="P_61F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_186F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10197_O" deadCode="false" sourceNode="P_66F10197" targetNode="P_62F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_186F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10197_I" deadCode="false" sourceNode="P_66F10197" targetNode="P_61F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_191F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10197_O" deadCode="false" sourceNode="P_66F10197" targetNode="P_62F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_191F10197"/>
	</edges>
	<edges id="P_66F10197P_67F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10197" targetNode="P_67F10197"/>
	<edges id="P_68F10197P_69F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10197" targetNode="P_69F10197"/>
	<edges id="P_61F10197P_62F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10197" targetNode="P_62F10197"/>
	<edges xsi:type="cbl:PerformEdge" id="S_220F10197_I" deadCode="false" sourceNode="P_64F10197" targetNode="P_72F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_220F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_220F10197_O" deadCode="false" sourceNode="P_64F10197" targetNode="P_73F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_220F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10197_I" deadCode="false" sourceNode="P_64F10197" targetNode="P_74F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_221F10197"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10197_O" deadCode="false" sourceNode="P_64F10197" targetNode="P_75F10197">
		<representations href="../../../cobol/LDBS3360.cbl.cobModel#S_221F10197"/>
	</edges>
	<edges id="P_64F10197P_65F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10197" targetNode="P_65F10197"/>
	<edges id="P_72F10197P_73F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10197" targetNode="P_73F10197"/>
	<edges id="P_74F10197P_75F10197" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10197" targetNode="P_75F10197"/>
	<edges xsi:type="cbl:DataEdge" id="S_67F10197_POS1" deadCode="false" targetNode="P_14F10197" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10197"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_67F10197_POS2" deadCode="false" targetNode="P_14F10197" sourceNode="DB2_STAT_OGG_WF">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10197"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_75F10197_POS1" deadCode="false" targetNode="P_16F10197" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_75F10197"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_75F10197_POS2" deadCode="false" targetNode="P_16F10197" sourceNode="DB2_STAT_OGG_WF">
		<representations href="../../../cobol/../importantStmts.cobModel#S_75F10197"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_78F10197_POS1" deadCode="false" targetNode="P_18F10197" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10197"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_78F10197_POS2" deadCode="false" targetNode="P_18F10197" sourceNode="DB2_STAT_OGG_WF">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10197"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_85F10197_POS1" deadCode="false" targetNode="P_22F10197" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_85F10197"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_85F10197_POS2" deadCode="false" targetNode="P_22F10197" sourceNode="DB2_STAT_OGG_WF">
		<representations href="../../../cobol/../importantStmts.cobModel#S_85F10197"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_103F10197_POS1" deadCode="false" targetNode="P_24F10197" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_103F10197"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_103F10197_POS2" deadCode="false" targetNode="P_24F10197" sourceNode="DB2_STAT_OGG_WF">
		<representations href="../../../cobol/../importantStmts.cobModel#S_103F10197"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10197_POS1" deadCode="false" targetNode="P_26F10197" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10197"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10197_POS2" deadCode="false" targetNode="P_26F10197" sourceNode="DB2_STAT_OGG_WF">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10197"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_114F10197_POS1" deadCode="false" targetNode="P_28F10197" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10197"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_114F10197_POS2" deadCode="false" targetNode="P_28F10197" sourceNode="DB2_STAT_OGG_WF">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10197"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_121F10197_POS1" deadCode="false" targetNode="P_32F10197" sourceNode="DB2_OGG_DEROGA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_121F10197"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_121F10197_POS2" deadCode="false" targetNode="P_32F10197" sourceNode="DB2_STAT_OGG_WF">
		<representations href="../../../cobol/../importantStmts.cobModel#S_121F10197"></representations>
	</edges>
</Package>

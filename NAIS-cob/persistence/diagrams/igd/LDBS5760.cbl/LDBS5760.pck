<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS5760" cbl:id="LDBS5760" xsi:id="LDBS5760" packageRef="LDBS5760.igd#LDBS5760" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS5760_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10223" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS5760.cbl.cobModel#SC_1F10223"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10223" deadCode="false" name="PROGRAM_LDBS5760_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_1F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10223" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_2F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10223" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_3F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10223" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_12F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10223" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_13F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10223" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_4F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10223" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_5F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10223" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_6F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10223" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_7F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10223" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_8F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10223" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_9F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10223" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_44F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10223" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_49F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10223" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_14F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10223" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_15F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10223" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_16F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10223" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_17F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10223" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_18F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10223" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_19F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10223" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_20F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10223" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_21F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10223" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_22F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10223" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_23F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10223" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_56F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10223" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_57F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10223" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_24F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10223" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_25F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10223" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_26F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10223" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_27F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10223" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_28F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10223" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_29F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10223" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_30F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10223" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_31F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10223" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_32F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10223" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_33F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10223" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_58F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10223" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_59F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10223" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_34F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10223" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_35F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10223" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_36F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10223" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_37F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10223" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_38F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10223" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_39F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10223" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_40F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10223" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_41F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10223" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_42F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10223" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_43F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10223" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_50F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10223" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_51F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10223" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_60F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10223" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_63F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10223" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_52F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10223" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_53F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10223" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_45F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10223" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_46F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10223" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_47F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10223" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_48F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10223" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_54F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10223" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_55F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10223" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_10F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10223" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_11F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10223" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_66F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10223" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_67F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10223" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_68F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10223" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_69F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10223" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_61F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10223" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_62F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10223" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_70F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10223" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_71F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10223" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_64F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10223" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_65F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10223" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_72F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10223" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_73F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10223" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_74F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10223" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_75F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10223" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_76F10223"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10223" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS5760.cbl.cobModel#P_77F10223"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_PARAM_MOVI" name="PARAM_MOVI">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_PARAM_MOVI"/>
		</children>
	</packageNode>
	<edges id="SC_1F10223P_1F10223" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10223" targetNode="P_1F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10223_I" deadCode="false" sourceNode="P_1F10223" targetNode="P_2F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_1F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10223_O" deadCode="false" sourceNode="P_1F10223" targetNode="P_3F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_1F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10223_I" deadCode="false" sourceNode="P_1F10223" targetNode="P_4F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_5F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10223_O" deadCode="false" sourceNode="P_1F10223" targetNode="P_5F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_5F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10223_I" deadCode="false" sourceNode="P_1F10223" targetNode="P_6F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_9F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10223_O" deadCode="false" sourceNode="P_1F10223" targetNode="P_7F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_9F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10223_I" deadCode="false" sourceNode="P_1F10223" targetNode="P_8F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_13F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10223_O" deadCode="false" sourceNode="P_1F10223" targetNode="P_9F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_13F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10223_I" deadCode="false" sourceNode="P_2F10223" targetNode="P_10F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_22F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10223_O" deadCode="false" sourceNode="P_2F10223" targetNode="P_11F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_22F10223"/>
	</edges>
	<edges id="P_2F10223P_3F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10223" targetNode="P_3F10223"/>
	<edges id="P_12F10223P_13F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10223" targetNode="P_13F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10223_I" deadCode="false" sourceNode="P_4F10223" targetNode="P_14F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_35F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10223_O" deadCode="false" sourceNode="P_4F10223" targetNode="P_15F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_35F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10223_I" deadCode="false" sourceNode="P_4F10223" targetNode="P_16F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_36F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10223_O" deadCode="false" sourceNode="P_4F10223" targetNode="P_17F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_36F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10223_I" deadCode="false" sourceNode="P_4F10223" targetNode="P_18F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_37F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10223_O" deadCode="false" sourceNode="P_4F10223" targetNode="P_19F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_37F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10223_I" deadCode="false" sourceNode="P_4F10223" targetNode="P_20F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_38F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10223_O" deadCode="false" sourceNode="P_4F10223" targetNode="P_21F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_38F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10223_I" deadCode="false" sourceNode="P_4F10223" targetNode="P_22F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_39F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10223_O" deadCode="false" sourceNode="P_4F10223" targetNode="P_23F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_39F10223"/>
	</edges>
	<edges id="P_4F10223P_5F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10223" targetNode="P_5F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10223_I" deadCode="false" sourceNode="P_6F10223" targetNode="P_24F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_43F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10223_O" deadCode="false" sourceNode="P_6F10223" targetNode="P_25F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_43F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10223_I" deadCode="false" sourceNode="P_6F10223" targetNode="P_26F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_44F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10223_O" deadCode="false" sourceNode="P_6F10223" targetNode="P_27F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_44F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10223_I" deadCode="false" sourceNode="P_6F10223" targetNode="P_28F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_45F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10223_O" deadCode="false" sourceNode="P_6F10223" targetNode="P_29F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_45F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10223_I" deadCode="false" sourceNode="P_6F10223" targetNode="P_30F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_46F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10223_O" deadCode="false" sourceNode="P_6F10223" targetNode="P_31F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_46F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10223_I" deadCode="false" sourceNode="P_6F10223" targetNode="P_32F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_47F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10223_O" deadCode="false" sourceNode="P_6F10223" targetNode="P_33F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_47F10223"/>
	</edges>
	<edges id="P_6F10223P_7F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10223" targetNode="P_7F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10223_I" deadCode="false" sourceNode="P_8F10223" targetNode="P_34F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_51F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10223_O" deadCode="false" sourceNode="P_8F10223" targetNode="P_35F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_51F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10223_I" deadCode="false" sourceNode="P_8F10223" targetNode="P_36F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_52F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10223_O" deadCode="false" sourceNode="P_8F10223" targetNode="P_37F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_52F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10223_I" deadCode="false" sourceNode="P_8F10223" targetNode="P_38F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_53F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10223_O" deadCode="false" sourceNode="P_8F10223" targetNode="P_39F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_53F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10223_I" deadCode="false" sourceNode="P_8F10223" targetNode="P_40F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_54F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10223_O" deadCode="false" sourceNode="P_8F10223" targetNode="P_41F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_54F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10223_I" deadCode="false" sourceNode="P_8F10223" targetNode="P_42F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_55F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10223_O" deadCode="false" sourceNode="P_8F10223" targetNode="P_43F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_55F10223"/>
	</edges>
	<edges id="P_8F10223P_9F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10223" targetNode="P_9F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10223_I" deadCode="false" sourceNode="P_44F10223" targetNode="P_45F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_58F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10223_O" deadCode="false" sourceNode="P_44F10223" targetNode="P_46F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_58F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10223_I" deadCode="false" sourceNode="P_44F10223" targetNode="P_47F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_59F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10223_O" deadCode="false" sourceNode="P_44F10223" targetNode="P_48F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_59F10223"/>
	</edges>
	<edges id="P_44F10223P_49F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10223" targetNode="P_49F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10223_I" deadCode="false" sourceNode="P_14F10223" targetNode="P_45F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_63F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10223_O" deadCode="false" sourceNode="P_14F10223" targetNode="P_46F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_63F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10223_I" deadCode="false" sourceNode="P_14F10223" targetNode="P_47F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_64F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10223_O" deadCode="false" sourceNode="P_14F10223" targetNode="P_48F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_64F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10223_I" deadCode="false" sourceNode="P_14F10223" targetNode="P_12F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_66F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10223_O" deadCode="false" sourceNode="P_14F10223" targetNode="P_13F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_66F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10223_I" deadCode="false" sourceNode="P_14F10223" targetNode="P_50F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_68F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10223_O" deadCode="false" sourceNode="P_14F10223" targetNode="P_51F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_68F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10223_I" deadCode="false" sourceNode="P_14F10223" targetNode="P_52F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_69F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10223_O" deadCode="false" sourceNode="P_14F10223" targetNode="P_53F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_69F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10223_I" deadCode="false" sourceNode="P_14F10223" targetNode="P_54F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_70F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10223_O" deadCode="false" sourceNode="P_14F10223" targetNode="P_55F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_70F10223"/>
	</edges>
	<edges id="P_14F10223P_15F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10223" targetNode="P_15F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10223_I" deadCode="false" sourceNode="P_16F10223" targetNode="P_44F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_72F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10223_O" deadCode="false" sourceNode="P_16F10223" targetNode="P_49F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_72F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10223_I" deadCode="false" sourceNode="P_16F10223" targetNode="P_12F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_74F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10223_O" deadCode="false" sourceNode="P_16F10223" targetNode="P_13F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_74F10223"/>
	</edges>
	<edges id="P_16F10223P_17F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10223" targetNode="P_17F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10223_I" deadCode="false" sourceNode="P_18F10223" targetNode="P_12F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_77F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10223_O" deadCode="false" sourceNode="P_18F10223" targetNode="P_13F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_77F10223"/>
	</edges>
	<edges id="P_18F10223P_19F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10223" targetNode="P_19F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10223_I" deadCode="false" sourceNode="P_20F10223" targetNode="P_16F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_79F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10223_O" deadCode="false" sourceNode="P_20F10223" targetNode="P_17F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_79F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10223_I" deadCode="false" sourceNode="P_20F10223" targetNode="P_22F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_81F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10223_O" deadCode="false" sourceNode="P_20F10223" targetNode="P_23F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_81F10223"/>
	</edges>
	<edges id="P_20F10223P_21F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10223" targetNode="P_21F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10223_I" deadCode="false" sourceNode="P_22F10223" targetNode="P_12F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_84F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10223_O" deadCode="false" sourceNode="P_22F10223" targetNode="P_13F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_84F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10223_I" deadCode="false" sourceNode="P_22F10223" targetNode="P_50F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_86F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10223_O" deadCode="false" sourceNode="P_22F10223" targetNode="P_51F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_86F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10223_I" deadCode="false" sourceNode="P_22F10223" targetNode="P_52F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_87F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10223_O" deadCode="false" sourceNode="P_22F10223" targetNode="P_53F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_87F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10223_I" deadCode="false" sourceNode="P_22F10223" targetNode="P_54F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_88F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10223_O" deadCode="false" sourceNode="P_22F10223" targetNode="P_55F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_88F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10223_I" deadCode="false" sourceNode="P_22F10223" targetNode="P_18F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_90F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10223_O" deadCode="false" sourceNode="P_22F10223" targetNode="P_19F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_90F10223"/>
	</edges>
	<edges id="P_22F10223P_23F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10223" targetNode="P_23F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10223_I" deadCode="false" sourceNode="P_56F10223" targetNode="P_45F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_94F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10223_O" deadCode="false" sourceNode="P_56F10223" targetNode="P_46F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_94F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10223_I" deadCode="false" sourceNode="P_56F10223" targetNode="P_47F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_95F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10223_O" deadCode="false" sourceNode="P_56F10223" targetNode="P_48F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_95F10223"/>
	</edges>
	<edges id="P_56F10223P_57F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10223" targetNode="P_57F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10223_I" deadCode="false" sourceNode="P_24F10223" targetNode="P_45F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_99F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10223_O" deadCode="false" sourceNode="P_24F10223" targetNode="P_46F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_99F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10223_I" deadCode="false" sourceNode="P_24F10223" targetNode="P_47F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_100F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10223_O" deadCode="false" sourceNode="P_24F10223" targetNode="P_48F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_100F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10223_I" deadCode="false" sourceNode="P_24F10223" targetNode="P_12F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_102F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10223_O" deadCode="false" sourceNode="P_24F10223" targetNode="P_13F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_102F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10223_I" deadCode="false" sourceNode="P_24F10223" targetNode="P_50F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_104F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10223_O" deadCode="false" sourceNode="P_24F10223" targetNode="P_51F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_104F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10223_I" deadCode="false" sourceNode="P_24F10223" targetNode="P_52F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_105F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10223_O" deadCode="false" sourceNode="P_24F10223" targetNode="P_53F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_105F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10223_I" deadCode="false" sourceNode="P_24F10223" targetNode="P_54F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_106F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10223_O" deadCode="false" sourceNode="P_24F10223" targetNode="P_55F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_106F10223"/>
	</edges>
	<edges id="P_24F10223P_25F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10223" targetNode="P_25F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10223_I" deadCode="false" sourceNode="P_26F10223" targetNode="P_56F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_108F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10223_O" deadCode="false" sourceNode="P_26F10223" targetNode="P_57F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_108F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10223_I" deadCode="false" sourceNode="P_26F10223" targetNode="P_12F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_110F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10223_O" deadCode="false" sourceNode="P_26F10223" targetNode="P_13F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_110F10223"/>
	</edges>
	<edges id="P_26F10223P_27F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10223" targetNode="P_27F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10223_I" deadCode="false" sourceNode="P_28F10223" targetNode="P_12F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_113F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10223_O" deadCode="false" sourceNode="P_28F10223" targetNode="P_13F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_113F10223"/>
	</edges>
	<edges id="P_28F10223P_29F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10223" targetNode="P_29F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10223_I" deadCode="false" sourceNode="P_30F10223" targetNode="P_26F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_115F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10223_O" deadCode="false" sourceNode="P_30F10223" targetNode="P_27F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_115F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10223_I" deadCode="false" sourceNode="P_30F10223" targetNode="P_32F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_117F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10223_O" deadCode="false" sourceNode="P_30F10223" targetNode="P_33F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_117F10223"/>
	</edges>
	<edges id="P_30F10223P_31F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10223" targetNode="P_31F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10223_I" deadCode="false" sourceNode="P_32F10223" targetNode="P_12F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_120F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10223_O" deadCode="false" sourceNode="P_32F10223" targetNode="P_13F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_120F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10223_I" deadCode="false" sourceNode="P_32F10223" targetNode="P_50F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_122F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10223_O" deadCode="false" sourceNode="P_32F10223" targetNode="P_51F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_122F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10223_I" deadCode="false" sourceNode="P_32F10223" targetNode="P_52F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_123F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10223_O" deadCode="false" sourceNode="P_32F10223" targetNode="P_53F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_123F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10223_I" deadCode="false" sourceNode="P_32F10223" targetNode="P_54F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_124F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10223_O" deadCode="false" sourceNode="P_32F10223" targetNode="P_55F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_124F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10223_I" deadCode="false" sourceNode="P_32F10223" targetNode="P_28F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_126F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10223_O" deadCode="false" sourceNode="P_32F10223" targetNode="P_29F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_126F10223"/>
	</edges>
	<edges id="P_32F10223P_33F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10223" targetNode="P_33F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10223_I" deadCode="false" sourceNode="P_58F10223" targetNode="P_45F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_130F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10223_O" deadCode="false" sourceNode="P_58F10223" targetNode="P_46F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_130F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10223_I" deadCode="false" sourceNode="P_58F10223" targetNode="P_47F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_131F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10223_O" deadCode="false" sourceNode="P_58F10223" targetNode="P_48F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_131F10223"/>
	</edges>
	<edges id="P_58F10223P_59F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10223" targetNode="P_59F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_134F10223_I" deadCode="false" sourceNode="P_34F10223" targetNode="P_45F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_134F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_134F10223_O" deadCode="false" sourceNode="P_34F10223" targetNode="P_46F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_134F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10223_I" deadCode="false" sourceNode="P_34F10223" targetNode="P_47F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_135F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10223_O" deadCode="false" sourceNode="P_34F10223" targetNode="P_48F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_135F10223"/>
	</edges>
	<edges id="P_34F10223P_35F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10223" targetNode="P_35F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10223_I" deadCode="false" sourceNode="P_36F10223" targetNode="P_58F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_138F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10223_O" deadCode="false" sourceNode="P_36F10223" targetNode="P_59F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_138F10223"/>
	</edges>
	<edges id="P_36F10223P_37F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10223" targetNode="P_37F10223"/>
	<edges id="P_38F10223P_39F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10223" targetNode="P_39F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10223_I" deadCode="false" sourceNode="P_40F10223" targetNode="P_36F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_143F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10223_O" deadCode="false" sourceNode="P_40F10223" targetNode="P_37F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_143F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10223_I" deadCode="false" sourceNode="P_40F10223" targetNode="P_42F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_145F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10223_O" deadCode="false" sourceNode="P_40F10223" targetNode="P_43F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_145F10223"/>
	</edges>
	<edges id="P_40F10223P_41F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10223" targetNode="P_41F10223"/>
	<edges id="P_42F10223P_43F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10223" targetNode="P_43F10223"/>
	<edges id="P_50F10223P_51F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10223" targetNode="P_51F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_236F10223_I" deadCode="true" sourceNode="P_60F10223" targetNode="P_61F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_236F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_236F10223_O" deadCode="true" sourceNode="P_60F10223" targetNode="P_62F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_236F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_239F10223_I" deadCode="true" sourceNode="P_60F10223" targetNode="P_61F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_239F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_239F10223_O" deadCode="true" sourceNode="P_60F10223" targetNode="P_62F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_239F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_243F10223_I" deadCode="true" sourceNode="P_60F10223" targetNode="P_61F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_243F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_243F10223_O" deadCode="true" sourceNode="P_60F10223" targetNode="P_62F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_243F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_247F10223_I" deadCode="true" sourceNode="P_60F10223" targetNode="P_61F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_247F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_247F10223_O" deadCode="true" sourceNode="P_60F10223" targetNode="P_62F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_247F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_251F10223_I" deadCode="true" sourceNode="P_60F10223" targetNode="P_61F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_251F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_251F10223_O" deadCode="true" sourceNode="P_60F10223" targetNode="P_62F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_251F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10223_I" deadCode="false" sourceNode="P_52F10223" targetNode="P_64F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_255F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10223_O" deadCode="false" sourceNode="P_52F10223" targetNode="P_65F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_255F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_258F10223_I" deadCode="false" sourceNode="P_52F10223" targetNode="P_64F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_258F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_258F10223_O" deadCode="false" sourceNode="P_52F10223" targetNode="P_65F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_258F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10223_I" deadCode="false" sourceNode="P_52F10223" targetNode="P_64F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_262F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10223_O" deadCode="false" sourceNode="P_52F10223" targetNode="P_65F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_262F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_266F10223_I" deadCode="false" sourceNode="P_52F10223" targetNode="P_64F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_266F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_266F10223_O" deadCode="false" sourceNode="P_52F10223" targetNode="P_65F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_266F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_270F10223_I" deadCode="false" sourceNode="P_52F10223" targetNode="P_64F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_270F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_270F10223_O" deadCode="false" sourceNode="P_52F10223" targetNode="P_65F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_270F10223"/>
	</edges>
	<edges id="P_52F10223P_53F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10223" targetNode="P_53F10223"/>
	<edges id="P_45F10223P_46F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10223" targetNode="P_46F10223"/>
	<edges id="P_47F10223P_48F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10223" targetNode="P_48F10223"/>
	<edges id="P_54F10223P_55F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10223" targetNode="P_55F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_279F10223_I" deadCode="false" sourceNode="P_10F10223" targetNode="P_66F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_279F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_279F10223_O" deadCode="false" sourceNode="P_10F10223" targetNode="P_67F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_279F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10223_I" deadCode="false" sourceNode="P_10F10223" targetNode="P_68F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_281F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10223_O" deadCode="false" sourceNode="P_10F10223" targetNode="P_69F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_281F10223"/>
	</edges>
	<edges id="P_10F10223P_11F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10223" targetNode="P_11F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_286F10223_I" deadCode="false" sourceNode="P_66F10223" targetNode="P_61F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_286F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_286F10223_O" deadCode="false" sourceNode="P_66F10223" targetNode="P_62F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_286F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_291F10223_I" deadCode="false" sourceNode="P_66F10223" targetNode="P_61F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_291F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_291F10223_O" deadCode="false" sourceNode="P_66F10223" targetNode="P_62F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_291F10223"/>
	</edges>
	<edges id="P_66F10223P_67F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10223" targetNode="P_67F10223"/>
	<edges id="P_68F10223P_69F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10223" targetNode="P_69F10223"/>
	<edges id="P_61F10223P_62F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10223" targetNode="P_62F10223"/>
	<edges xsi:type="cbl:PerformEdge" id="S_320F10223_I" deadCode="false" sourceNode="P_64F10223" targetNode="P_72F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_320F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_320F10223_O" deadCode="false" sourceNode="P_64F10223" targetNode="P_73F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_320F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10223_I" deadCode="false" sourceNode="P_64F10223" targetNode="P_74F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_321F10223"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10223_O" deadCode="false" sourceNode="P_64F10223" targetNode="P_75F10223">
		<representations href="../../../cobol/LDBS5760.cbl.cobModel#S_321F10223"/>
	</edges>
	<edges id="P_64F10223P_65F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10223" targetNode="P_65F10223"/>
	<edges id="P_72F10223P_73F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10223" targetNode="P_73F10223"/>
	<edges id="P_74F10223P_75F10223" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10223" targetNode="P_75F10223"/>
	<edges xsi:type="cbl:DataEdge" id="S_65F10223_POS1" deadCode="false" targetNode="P_14F10223" sourceNode="DB2_PARAM_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_65F10223"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_73F10223_POS1" deadCode="false" targetNode="P_16F10223" sourceNode="DB2_PARAM_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_73F10223"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_76F10223_POS1" deadCode="false" targetNode="P_18F10223" sourceNode="DB2_PARAM_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_76F10223"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_83F10223_POS1" deadCode="false" targetNode="P_22F10223" sourceNode="DB2_PARAM_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_83F10223"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_101F10223_POS1" deadCode="false" targetNode="P_24F10223" sourceNode="DB2_PARAM_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_101F10223"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_109F10223_POS1" deadCode="false" targetNode="P_26F10223" sourceNode="DB2_PARAM_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_109F10223"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_112F10223_POS1" deadCode="false" targetNode="P_28F10223" sourceNode="DB2_PARAM_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_112F10223"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_119F10223_POS1" deadCode="false" targetNode="P_32F10223" sourceNode="DB2_PARAM_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_119F10223"></representations>
	</edges>
</Package>

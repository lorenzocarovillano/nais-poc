<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LCCS1750" cbl:id="LCCS1750" xsi:id="LCCS1750" packageRef="LCCS1750.igd#LCCS1750" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LCCS1750_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10138" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LCCS1750.cbl.cobModel#SC_1F10138"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10138" deadCode="false" name="INIZIO">
				<representations href="../../../cobol/LCCS1750.cbl.cobModel#P_1F10138"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0010" name="LCCS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10122"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10138P_1F10138" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10138" targetNode="P_1F10138"/>
	<edges xsi:type="cbl:CallEdge" id="S_5F10138" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_1F10138" targetNode="LCCS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_5F10138"></representations>
	</edges>
</Package>

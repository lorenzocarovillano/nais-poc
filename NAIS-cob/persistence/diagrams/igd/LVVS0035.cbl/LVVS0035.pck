<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0035" cbl:id="LVVS0035" xsi:id="LVVS0035" packageRef="LVVS0035.igd#LVVS0035" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0035_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10324" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0035.cbl.cobModel#SC_1F10324"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10324" deadCode="false" name="PROGRAM_LVVS0035_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0035.cbl.cobModel#P_1F10324"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10324" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0035.cbl.cobModel#P_2F10324"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10324" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0035.cbl.cobModel#P_3F10324"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10324" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0035.cbl.cobModel#P_4F10324"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10324" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0035.cbl.cobModel#P_5F10324"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10324" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0035.cbl.cobModel#P_8F10324"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10324" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0035.cbl.cobModel#P_9F10324"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10324" deadCode="false" name="S1250-CALCOLA-DATA1">
				<representations href="../../../cobol/LVVS0035.cbl.cobModel#P_12F10324"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10324" deadCode="false" name="S1250-EX">
				<representations href="../../../cobol/LVVS0035.cbl.cobModel#P_13F10324"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10324" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS0035.cbl.cobModel#P_10F10324"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10324" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS0035.cbl.cobModel#P_11F10324"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10324" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0035.cbl.cobModel#P_6F10324"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10324" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0035.cbl.cobModel#P_7F10324"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0010" name="LCCS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10122"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10324P_1F10324" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10324" targetNode="P_1F10324"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10324_I" deadCode="false" sourceNode="P_1F10324" targetNode="P_2F10324">
		<representations href="../../../cobol/LVVS0035.cbl.cobModel#S_1F10324"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10324_O" deadCode="false" sourceNode="P_1F10324" targetNode="P_3F10324">
		<representations href="../../../cobol/LVVS0035.cbl.cobModel#S_1F10324"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10324_I" deadCode="false" sourceNode="P_1F10324" targetNode="P_4F10324">
		<representations href="../../../cobol/LVVS0035.cbl.cobModel#S_2F10324"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10324_O" deadCode="false" sourceNode="P_1F10324" targetNode="P_5F10324">
		<representations href="../../../cobol/LVVS0035.cbl.cobModel#S_2F10324"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10324_I" deadCode="false" sourceNode="P_1F10324" targetNode="P_6F10324">
		<representations href="../../../cobol/LVVS0035.cbl.cobModel#S_3F10324"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10324_O" deadCode="false" sourceNode="P_1F10324" targetNode="P_7F10324">
		<representations href="../../../cobol/LVVS0035.cbl.cobModel#S_3F10324"/>
	</edges>
	<edges id="P_2F10324P_3F10324" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10324" targetNode="P_3F10324"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10324_I" deadCode="false" sourceNode="P_4F10324" targetNode="P_8F10324">
		<representations href="../../../cobol/LVVS0035.cbl.cobModel#S_11F10324"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10324_O" deadCode="false" sourceNode="P_4F10324" targetNode="P_9F10324">
		<representations href="../../../cobol/LVVS0035.cbl.cobModel#S_11F10324"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10324_I" deadCode="false" sourceNode="P_4F10324" targetNode="P_10F10324">
		<representations href="../../../cobol/LVVS0035.cbl.cobModel#S_13F10324"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10324_O" deadCode="false" sourceNode="P_4F10324" targetNode="P_11F10324">
		<representations href="../../../cobol/LVVS0035.cbl.cobModel#S_13F10324"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10324_I" deadCode="false" sourceNode="P_4F10324" targetNode="P_12F10324">
		<representations href="../../../cobol/LVVS0035.cbl.cobModel#S_15F10324"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10324_O" deadCode="false" sourceNode="P_4F10324" targetNode="P_13F10324">
		<representations href="../../../cobol/LVVS0035.cbl.cobModel#S_15F10324"/>
	</edges>
	<edges id="P_4F10324P_5F10324" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10324" targetNode="P_5F10324"/>
	<edges id="P_8F10324P_9F10324" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10324" targetNode="P_9F10324"/>
	<edges id="P_12F10324P_13F10324" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10324" targetNode="P_13F10324"/>
	<edges id="P_10F10324P_11F10324" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10324" targetNode="P_11F10324"/>
	<edges xsi:type="cbl:CallEdge" id="S_29F10324" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_12F10324" targetNode="LCCS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_29F10324"></representations>
	</edges>
</Package>

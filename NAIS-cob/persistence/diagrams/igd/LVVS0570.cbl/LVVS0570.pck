<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0570" cbl:id="LVVS0570" xsi:id="LVVS0570" packageRef="LVVS0570.igd#LVVS0570" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0570_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10359" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0570.cbl.cobModel#SC_1F10359"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10359" deadCode="false" name="PROGRAM_LVVS0570_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0570.cbl.cobModel#P_1F10359"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10359" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0570.cbl.cobModel#P_2F10359"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10359" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0570.cbl.cobModel#P_3F10359"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10359" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0570.cbl.cobModel#P_4F10359"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10359" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0570.cbl.cobModel#P_5F10359"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10359" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0570.cbl.cobModel#P_8F10359"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10359" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0570.cbl.cobModel#P_9F10359"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10359" deadCode="false" name="S1200-CONVERTI-CHAR-NUM">
				<representations href="../../../cobol/LVVS0570.cbl.cobModel#P_10F10359"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10359" deadCode="false" name="S1200-CONVERTI-CHAR-NUM-EX">
				<representations href="../../../cobol/LVVS0570.cbl.cobModel#P_11F10359"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10359" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0570.cbl.cobModel#P_6F10359"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10359" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0570.cbl.cobModel#P_7F10359"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IWFS0050" name="IWFS0050">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10118"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10359P_1F10359" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10359" targetNode="P_1F10359"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10359_I" deadCode="false" sourceNode="P_1F10359" targetNode="P_2F10359">
		<representations href="../../../cobol/LVVS0570.cbl.cobModel#S_1F10359"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10359_O" deadCode="false" sourceNode="P_1F10359" targetNode="P_3F10359">
		<representations href="../../../cobol/LVVS0570.cbl.cobModel#S_1F10359"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10359_I" deadCode="false" sourceNode="P_1F10359" targetNode="P_4F10359">
		<representations href="../../../cobol/LVVS0570.cbl.cobModel#S_2F10359"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10359_O" deadCode="false" sourceNode="P_1F10359" targetNode="P_5F10359">
		<representations href="../../../cobol/LVVS0570.cbl.cobModel#S_2F10359"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10359_I" deadCode="false" sourceNode="P_1F10359" targetNode="P_6F10359">
		<representations href="../../../cobol/LVVS0570.cbl.cobModel#S_3F10359"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10359_O" deadCode="false" sourceNode="P_1F10359" targetNode="P_7F10359">
		<representations href="../../../cobol/LVVS0570.cbl.cobModel#S_3F10359"/>
	</edges>
	<edges id="P_2F10359P_3F10359" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10359" targetNode="P_3F10359"/>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10359_I" deadCode="false" sourceNode="P_4F10359" targetNode="P_8F10359">
		<representations href="../../../cobol/LVVS0570.cbl.cobModel#S_9F10359"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10359_O" deadCode="false" sourceNode="P_4F10359" targetNode="P_9F10359">
		<representations href="../../../cobol/LVVS0570.cbl.cobModel#S_9F10359"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10359_I" deadCode="false" sourceNode="P_4F10359" targetNode="P_10F10359">
		<representations href="../../../cobol/LVVS0570.cbl.cobModel#S_10F10359"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10359_O" deadCode="false" sourceNode="P_4F10359" targetNode="P_11F10359">
		<representations href="../../../cobol/LVVS0570.cbl.cobModel#S_10F10359"/>
	</edges>
	<edges id="P_4F10359P_5F10359" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10359" targetNode="P_5F10359"/>
	<edges id="P_8F10359P_9F10359" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10359" targetNode="P_9F10359"/>
	<edges id="P_10F10359P_11F10359" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10359" targetNode="P_11F10359"/>
	<edges xsi:type="cbl:CallEdge" id="S_17F10359" deadCode="false" name="Dynamic IWFS0050" sourceNode="P_10F10359" targetNode="IWFS0050">
		<representations href="../../../cobol/../importantStmts.cobModel#S_17F10359"></representations>
	</edges>
</Package>

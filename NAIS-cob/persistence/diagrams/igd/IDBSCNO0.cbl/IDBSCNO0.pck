<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IDBSCNO0" cbl:id="IDBSCNO0" xsi:id="IDBSCNO0" packageRef="IDBSCNO0.igd#IDBSCNO0" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IDBSCNO0_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10026" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#SC_1F10026"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10026" deadCode="false" name="PROGRAM_IDBSCNO0_FIRST_SENTENCES">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_1F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10026" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_2F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10026" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_3F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10026" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_28F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10026" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_29F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10026" deadCode="false" name="A200-ELABORA-PK">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_24F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10026" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_25F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10026" deadCode="false" name="A300-ELABORA-ID-EFF">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_4F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10026" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_5F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10026" deadCode="false" name="A400-ELABORA-IDP-EFF">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_6F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10026" deadCode="false" name="A400-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_7F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10026" deadCode="false" name="A500-ELABORA-IBO">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_8F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10026" deadCode="false" name="A500-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_9F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10026" deadCode="false" name="A600-ELABORA-IBS">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_10F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10026" deadCode="false" name="A600-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_11F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10026" deadCode="false" name="A700-ELABORA-IDO">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_12F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10026" deadCode="false" name="A700-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_13F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10026" deadCode="false" name="B300-ELABORA-ID-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_14F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10026" deadCode="false" name="B300-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_15F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10026" deadCode="false" name="B400-ELABORA-IDP-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_16F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10026" deadCode="false" name="B400-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_17F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10026" deadCode="false" name="B500-ELABORA-IBO-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_18F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10026" deadCode="false" name="B500-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_19F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10026" deadCode="false" name="B600-ELABORA-IBS-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_20F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10026" deadCode="false" name="B600-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_21F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10026" deadCode="false" name="B700-ELABORA-IDO-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_22F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10026" deadCode="false" name="B700-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_23F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10026" deadCode="false" name="A210-SELECT-PK">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_30F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10026" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_31F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10026" deadCode="false" name="A220-INSERT-PK">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_32F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10026" deadCode="false" name="A220-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_33F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10026" deadCode="false" name="A230-UPDATE-PK">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_34F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10026" deadCode="false" name="A230-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_35F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10026" deadCode="false" name="A240-DELETE-PK">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_36F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10026" deadCode="false" name="A240-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_37F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_140F10026" deadCode="true" name="A305-DECLARE-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_140F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_141F10026" deadCode="true" name="A305-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_141F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10026" deadCode="false" name="A310-SELECT-ID-EFF">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_38F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10026" deadCode="false" name="A310-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_39F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_142F10026" deadCode="true" name="A330-UPDATE-ID-EFF">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_142F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_143F10026" deadCode="true" name="A330-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_143F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_144F10026" deadCode="true" name="A360-OPEN-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_144F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_145F10026" deadCode="true" name="A360-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_145F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_146F10026" deadCode="true" name="A370-CLOSE-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_146F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_147F10026" deadCode="true" name="A370-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_147F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_148F10026" deadCode="true" name="A380-FETCH-FIRST-ID-EFF">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_148F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_151F10026" deadCode="true" name="A380-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_151F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_149F10026" deadCode="true" name="A390-FETCH-NEXT-ID-EFF">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_149F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_150F10026" deadCode="true" name="A390-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_150F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_152F10026" deadCode="false" name="A405-DECLARE-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_152F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_153F10026" deadCode="false" name="A405-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_153F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10026" deadCode="false" name="A410-SELECT-IDP-EFF">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_42F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10026" deadCode="false" name="A410-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_43F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10026" deadCode="false" name="A460-OPEN-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_44F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10026" deadCode="false" name="A460-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_45F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10026" deadCode="false" name="A470-CLOSE-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_46F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10026" deadCode="false" name="A470-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_47F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10026" deadCode="false" name="A480-FETCH-FIRST-IDP-EFF">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_48F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10026" deadCode="false" name="A480-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_49F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10026" deadCode="false" name="A490-FETCH-NEXT-IDP-EFF">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_50F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10026" deadCode="false" name="A490-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_51F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_154F10026" deadCode="false" name="A505-DECLARE-CURSOR-IBO">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_154F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_155F10026" deadCode="false" name="A505-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_155F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10026" deadCode="false" name="A510-SELECT-IBO">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_52F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10026" deadCode="false" name="A510-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_53F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10026" deadCode="false" name="A560-OPEN-CURSOR-IBO">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_54F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10026" deadCode="false" name="A560-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_55F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10026" deadCode="false" name="A570-CLOSE-CURSOR-IBO">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_56F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10026" deadCode="false" name="A570-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_57F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10026" deadCode="false" name="A580-FETCH-FIRST-IBO">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_58F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10026" deadCode="false" name="A580-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_59F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10026" deadCode="false" name="A590-FETCH-NEXT-IBO">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_60F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10026" deadCode="false" name="A590-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_61F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_156F10026" deadCode="false" name="A605-DECLARE-CURSOR-IBS">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_156F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_157F10026" deadCode="false" name="A605-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_157F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10026" deadCode="false" name="A610-SELECT-IBS">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_62F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10026" deadCode="false" name="A610-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_63F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10026" deadCode="false" name="A660-OPEN-CURSOR-IBS">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_64F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10026" deadCode="false" name="A660-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_65F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10026" deadCode="false" name="A670-CLOSE-CURSOR-IBS">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_66F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10026" deadCode="false" name="A670-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_67F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10026" deadCode="false" name="A680-FETCH-FIRST-IBS">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_68F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10026" deadCode="false" name="A680-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_69F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10026" deadCode="false" name="A690-FETCH-NEXT-IBS">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_70F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10026" deadCode="false" name="A690-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_71F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_158F10026" deadCode="false" name="A705-DECLARE-CURSOR-IDO">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_158F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_159F10026" deadCode="false" name="A705-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_159F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10026" deadCode="false" name="A710-SELECT-IDO">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_72F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10026" deadCode="false" name="A710-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_73F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10026" deadCode="false" name="A760-OPEN-CURSOR-IDO">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_74F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10026" deadCode="false" name="A760-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_75F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10026" deadCode="false" name="A770-CLOSE-CURSOR-IDO">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_76F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10026" deadCode="false" name="A770-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_77F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_78F10026" deadCode="false" name="A780-FETCH-FIRST-IDO">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_78F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_79F10026" deadCode="false" name="A780-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_79F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_80F10026" deadCode="false" name="A790-FETCH-NEXT-IDO">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_80F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_81F10026" deadCode="false" name="A790-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_81F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_82F10026" deadCode="false" name="B310-SELECT-ID-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_82F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_83F10026" deadCode="false" name="B310-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_83F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_160F10026" deadCode="false" name="B405-DECLARE-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_160F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_161F10026" deadCode="false" name="B405-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_161F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_84F10026" deadCode="false" name="B410-SELECT-IDP-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_84F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_85F10026" deadCode="false" name="B410-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_85F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_86F10026" deadCode="false" name="B460-OPEN-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_86F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_87F10026" deadCode="false" name="B460-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_87F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_88F10026" deadCode="false" name="B470-CLOSE-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_88F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_89F10026" deadCode="false" name="B470-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_89F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_90F10026" deadCode="false" name="B480-FETCH-FIRST-IDP-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_90F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_91F10026" deadCode="false" name="B480-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_91F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_92F10026" deadCode="false" name="B490-FETCH-NEXT-IDP-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_92F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_93F10026" deadCode="false" name="B490-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_93F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_162F10026" deadCode="false" name="B505-DECLARE-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_162F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_163F10026" deadCode="false" name="B505-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_163F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_94F10026" deadCode="false" name="B510-SELECT-IBO-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_94F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_95F10026" deadCode="false" name="B510-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_95F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_96F10026" deadCode="false" name="B560-OPEN-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_96F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_97F10026" deadCode="false" name="B560-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_97F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_98F10026" deadCode="false" name="B570-CLOSE-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_98F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_99F10026" deadCode="false" name="B570-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_99F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_100F10026" deadCode="false" name="B580-FETCH-FIRST-IBO-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_100F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_101F10026" deadCode="false" name="B580-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_101F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_102F10026" deadCode="false" name="B590-FETCH-NEXT-IBO-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_102F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_103F10026" deadCode="false" name="B590-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_103F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_164F10026" deadCode="false" name="B605-DECLARE-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_164F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_165F10026" deadCode="false" name="B605-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_165F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_104F10026" deadCode="false" name="B610-SELECT-IBS-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_104F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_105F10026" deadCode="false" name="B610-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_105F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_106F10026" deadCode="false" name="B660-OPEN-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_106F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_107F10026" deadCode="false" name="B660-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_107F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_108F10026" deadCode="false" name="B670-CLOSE-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_108F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_109F10026" deadCode="false" name="B670-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_109F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_110F10026" deadCode="false" name="B680-FETCH-FIRST-IBS-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_110F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_111F10026" deadCode="false" name="B680-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_111F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_112F10026" deadCode="false" name="B690-FETCH-NEXT-IBS-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_112F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_113F10026" deadCode="false" name="B690-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_113F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_166F10026" deadCode="false" name="B705-DECLARE-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_166F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_167F10026" deadCode="false" name="B705-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_167F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_114F10026" deadCode="false" name="B710-SELECT-IDO-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_114F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_115F10026" deadCode="false" name="B710-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_115F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_116F10026" deadCode="false" name="B760-OPEN-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_116F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_117F10026" deadCode="false" name="B760-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_117F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_118F10026" deadCode="false" name="B770-CLOSE-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_118F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_119F10026" deadCode="false" name="B770-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_119F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_120F10026" deadCode="false" name="B780-FETCH-FIRST-IDO-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_120F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_121F10026" deadCode="false" name="B780-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_121F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_122F10026" deadCode="false" name="B790-FETCH-NEXT-IDO-CPZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_122F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_123F10026" deadCode="false" name="B790-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_123F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_126F10026" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_126F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_127F10026" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_127F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_132F10026" deadCode="false" name="Z150-VALORIZZA-DATA-SERVICES-I">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_132F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_133F10026" deadCode="false" name="Z150-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_133F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_138F10026" deadCode="false" name="Z160-VALORIZZA-DATA-SERVICES-U">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_138F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_139F10026" deadCode="false" name="Z160-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_139F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_134F10026" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_134F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_135F10026" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_135F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_130F10026" deadCode="false" name="Z400-SEQ-RIGA">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_130F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_131F10026" deadCode="false" name="Z400-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_131F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10026" deadCode="false" name="Z500-AGGIORNAMENTO-STORICO">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_40F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10026" deadCode="false" name="Z500-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_41F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_168F10026" deadCode="true" name="Z600-INSERT-NUOVA-RIGA-STORICA">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_168F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_169F10026" deadCode="true" name="Z600-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_169F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_136F10026" deadCode="false" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_136F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_137F10026" deadCode="false" name="Z900-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_137F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_128F10026" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_128F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_129F10026" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_129F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_124F10026" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_124F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_125F10026" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_125F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10026" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_26F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10026" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_27F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_170F10026" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_170F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_171F10026" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_171F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_172F10026" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_172F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_173F10026" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_173F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_174F10026" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_174F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_175F10026" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_175F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_176F10026" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_176F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_177F10026" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_177F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_178F10026" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_178F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_183F10026" deadCode="true" name="Z800-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_183F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_179F10026" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_179F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_180F10026" deadCode="true" name="Z810-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_180F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_181F10026" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_181F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_182F10026" deadCode="true" name="Z820-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_182F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_184F10026" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_184F10026"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_185F10026" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#P_185F10026"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_COMP_NUM_OGG" name="COMP_NUM_OGG">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_COMP_NUM_OGG"/>
		</children>
	</packageNode>
	<edges id="SC_1F10026P_1F10026" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10026" targetNode="P_1F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10026_I" deadCode="false" sourceNode="P_1F10026" targetNode="P_2F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_1F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10026_O" deadCode="false" sourceNode="P_1F10026" targetNode="P_3F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_1F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10026_I" deadCode="false" sourceNode="P_1F10026" targetNode="P_4F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_5F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10026_O" deadCode="false" sourceNode="P_1F10026" targetNode="P_5F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_5F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10026_I" deadCode="false" sourceNode="P_1F10026" targetNode="P_6F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_6F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10026_O" deadCode="false" sourceNode="P_1F10026" targetNode="P_7F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_6F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10026_I" deadCode="false" sourceNode="P_1F10026" targetNode="P_8F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_7F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10026_O" deadCode="false" sourceNode="P_1F10026" targetNode="P_9F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_7F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10026_I" deadCode="false" sourceNode="P_1F10026" targetNode="P_10F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_8F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10026_O" deadCode="false" sourceNode="P_1F10026" targetNode="P_11F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_8F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10026_I" deadCode="false" sourceNode="P_1F10026" targetNode="P_12F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_9F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10026_O" deadCode="false" sourceNode="P_1F10026" targetNode="P_13F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_9F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10026_I" deadCode="false" sourceNode="P_1F10026" targetNode="P_14F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_13F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10026_O" deadCode="false" sourceNode="P_1F10026" targetNode="P_15F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_13F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10026_I" deadCode="false" sourceNode="P_1F10026" targetNode="P_16F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_14F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10026_O" deadCode="false" sourceNode="P_1F10026" targetNode="P_17F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_14F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10026_I" deadCode="false" sourceNode="P_1F10026" targetNode="P_18F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_15F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10026_O" deadCode="false" sourceNode="P_1F10026" targetNode="P_19F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_15F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10026_I" deadCode="false" sourceNode="P_1F10026" targetNode="P_20F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_16F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10026_O" deadCode="false" sourceNode="P_1F10026" targetNode="P_21F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_16F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10026_I" deadCode="false" sourceNode="P_1F10026" targetNode="P_22F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_17F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10026_O" deadCode="false" sourceNode="P_1F10026" targetNode="P_23F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_17F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10026_I" deadCode="false" sourceNode="P_1F10026" targetNode="P_24F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_21F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10026_O" deadCode="false" sourceNode="P_1F10026" targetNode="P_25F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_21F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10026_I" deadCode="false" sourceNode="P_1F10026" targetNode="P_8F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_22F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10026_O" deadCode="false" sourceNode="P_1F10026" targetNode="P_9F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_22F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10026_I" deadCode="false" sourceNode="P_1F10026" targetNode="P_10F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_23F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10026_O" deadCode="false" sourceNode="P_1F10026" targetNode="P_11F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_23F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10026_I" deadCode="false" sourceNode="P_1F10026" targetNode="P_12F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_24F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10026_O" deadCode="false" sourceNode="P_1F10026" targetNode="P_13F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_24F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10026_I" deadCode="false" sourceNode="P_2F10026" targetNode="P_26F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_33F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10026_O" deadCode="false" sourceNode="P_2F10026" targetNode="P_27F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_33F10026"/>
	</edges>
	<edges id="P_2F10026P_3F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10026" targetNode="P_3F10026"/>
	<edges id="P_28F10026P_29F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10026" targetNode="P_29F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10026_I" deadCode="false" sourceNode="P_24F10026" targetNode="P_30F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_46F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10026_O" deadCode="false" sourceNode="P_24F10026" targetNode="P_31F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_46F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10026_I" deadCode="false" sourceNode="P_24F10026" targetNode="P_32F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_47F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10026_O" deadCode="false" sourceNode="P_24F10026" targetNode="P_33F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_47F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10026_I" deadCode="false" sourceNode="P_24F10026" targetNode="P_34F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_48F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10026_O" deadCode="false" sourceNode="P_24F10026" targetNode="P_35F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_48F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10026_I" deadCode="false" sourceNode="P_24F10026" targetNode="P_36F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_49F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10026_O" deadCode="false" sourceNode="P_24F10026" targetNode="P_37F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_49F10026"/>
	</edges>
	<edges id="P_24F10026P_25F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10026" targetNode="P_25F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10026_I" deadCode="false" sourceNode="P_4F10026" targetNode="P_38F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_53F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10026_O" deadCode="false" sourceNode="P_4F10026" targetNode="P_39F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_53F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10026_I" deadCode="false" sourceNode="P_4F10026" targetNode="P_40F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_54F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10026_O" deadCode="false" sourceNode="P_4F10026" targetNode="P_41F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_54F10026"/>
	</edges>
	<edges id="P_4F10026P_5F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10026" targetNode="P_5F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10026_I" deadCode="false" sourceNode="P_6F10026" targetNode="P_42F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_58F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10026_O" deadCode="false" sourceNode="P_6F10026" targetNode="P_43F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_58F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10026_I" deadCode="false" sourceNode="P_6F10026" targetNode="P_44F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_59F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10026_O" deadCode="false" sourceNode="P_6F10026" targetNode="P_45F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_59F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10026_I" deadCode="false" sourceNode="P_6F10026" targetNode="P_46F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_60F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10026_O" deadCode="false" sourceNode="P_6F10026" targetNode="P_47F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_60F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10026_I" deadCode="false" sourceNode="P_6F10026" targetNode="P_48F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_61F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10026_O" deadCode="false" sourceNode="P_6F10026" targetNode="P_49F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_61F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10026_I" deadCode="false" sourceNode="P_6F10026" targetNode="P_50F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_62F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10026_O" deadCode="false" sourceNode="P_6F10026" targetNode="P_51F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_62F10026"/>
	</edges>
	<edges id="P_6F10026P_7F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10026" targetNode="P_7F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10026_I" deadCode="false" sourceNode="P_8F10026" targetNode="P_52F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_66F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10026_O" deadCode="false" sourceNode="P_8F10026" targetNode="P_53F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_66F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10026_I" deadCode="false" sourceNode="P_8F10026" targetNode="P_54F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_67F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10026_O" deadCode="false" sourceNode="P_8F10026" targetNode="P_55F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_67F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10026_I" deadCode="false" sourceNode="P_8F10026" targetNode="P_56F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_68F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10026_O" deadCode="false" sourceNode="P_8F10026" targetNode="P_57F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_68F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10026_I" deadCode="false" sourceNode="P_8F10026" targetNode="P_58F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_69F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10026_O" deadCode="false" sourceNode="P_8F10026" targetNode="P_59F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_69F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10026_I" deadCode="false" sourceNode="P_8F10026" targetNode="P_60F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_70F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10026_O" deadCode="false" sourceNode="P_8F10026" targetNode="P_61F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_70F10026"/>
	</edges>
	<edges id="P_8F10026P_9F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10026" targetNode="P_9F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10026_I" deadCode="false" sourceNode="P_10F10026" targetNode="P_62F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_74F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10026_O" deadCode="false" sourceNode="P_10F10026" targetNode="P_63F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_74F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10026_I" deadCode="false" sourceNode="P_10F10026" targetNode="P_64F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_75F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10026_O" deadCode="false" sourceNode="P_10F10026" targetNode="P_65F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_75F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10026_I" deadCode="false" sourceNode="P_10F10026" targetNode="P_66F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_76F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10026_O" deadCode="false" sourceNode="P_10F10026" targetNode="P_67F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_76F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10026_I" deadCode="false" sourceNode="P_10F10026" targetNode="P_68F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_77F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10026_O" deadCode="false" sourceNode="P_10F10026" targetNode="P_69F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_77F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10026_I" deadCode="false" sourceNode="P_10F10026" targetNode="P_70F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_78F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10026_O" deadCode="false" sourceNode="P_10F10026" targetNode="P_71F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_78F10026"/>
	</edges>
	<edges id="P_10F10026P_11F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10026" targetNode="P_11F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10026_I" deadCode="false" sourceNode="P_12F10026" targetNode="P_72F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_82F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10026_O" deadCode="false" sourceNode="P_12F10026" targetNode="P_73F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_82F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10026_I" deadCode="false" sourceNode="P_12F10026" targetNode="P_74F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_83F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10026_O" deadCode="false" sourceNode="P_12F10026" targetNode="P_75F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_83F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10026_I" deadCode="false" sourceNode="P_12F10026" targetNode="P_76F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_84F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10026_O" deadCode="false" sourceNode="P_12F10026" targetNode="P_77F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_84F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10026_I" deadCode="false" sourceNode="P_12F10026" targetNode="P_78F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_85F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10026_O" deadCode="false" sourceNode="P_12F10026" targetNode="P_79F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_85F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10026_I" deadCode="false" sourceNode="P_12F10026" targetNode="P_80F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_86F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10026_O" deadCode="false" sourceNode="P_12F10026" targetNode="P_81F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_86F10026"/>
	</edges>
	<edges id="P_12F10026P_13F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10026" targetNode="P_13F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10026_I" deadCode="false" sourceNode="P_14F10026" targetNode="P_82F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_90F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10026_O" deadCode="false" sourceNode="P_14F10026" targetNode="P_83F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_90F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10026_I" deadCode="false" sourceNode="P_14F10026" targetNode="P_40F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_91F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10026_O" deadCode="false" sourceNode="P_14F10026" targetNode="P_41F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_91F10026"/>
	</edges>
	<edges id="P_14F10026P_15F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10026" targetNode="P_15F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10026_I" deadCode="false" sourceNode="P_16F10026" targetNode="P_84F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_95F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10026_O" deadCode="false" sourceNode="P_16F10026" targetNode="P_85F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_95F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10026_I" deadCode="false" sourceNode="P_16F10026" targetNode="P_86F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_96F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10026_O" deadCode="false" sourceNode="P_16F10026" targetNode="P_87F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_96F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10026_I" deadCode="false" sourceNode="P_16F10026" targetNode="P_88F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_97F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10026_O" deadCode="false" sourceNode="P_16F10026" targetNode="P_89F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_97F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10026_I" deadCode="false" sourceNode="P_16F10026" targetNode="P_90F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_98F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10026_O" deadCode="false" sourceNode="P_16F10026" targetNode="P_91F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_98F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10026_I" deadCode="false" sourceNode="P_16F10026" targetNode="P_92F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_99F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10026_O" deadCode="false" sourceNode="P_16F10026" targetNode="P_93F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_99F10026"/>
	</edges>
	<edges id="P_16F10026P_17F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10026" targetNode="P_17F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10026_I" deadCode="false" sourceNode="P_18F10026" targetNode="P_94F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_103F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10026_O" deadCode="false" sourceNode="P_18F10026" targetNode="P_95F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_103F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10026_I" deadCode="false" sourceNode="P_18F10026" targetNode="P_96F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_104F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10026_O" deadCode="false" sourceNode="P_18F10026" targetNode="P_97F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_104F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10026_I" deadCode="false" sourceNode="P_18F10026" targetNode="P_98F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_105F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10026_O" deadCode="false" sourceNode="P_18F10026" targetNode="P_99F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_105F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10026_I" deadCode="false" sourceNode="P_18F10026" targetNode="P_100F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_106F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10026_O" deadCode="false" sourceNode="P_18F10026" targetNode="P_101F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_106F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10026_I" deadCode="false" sourceNode="P_18F10026" targetNode="P_102F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_107F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10026_O" deadCode="false" sourceNode="P_18F10026" targetNode="P_103F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_107F10026"/>
	</edges>
	<edges id="P_18F10026P_19F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10026" targetNode="P_19F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10026_I" deadCode="false" sourceNode="P_20F10026" targetNode="P_104F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_111F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10026_O" deadCode="false" sourceNode="P_20F10026" targetNode="P_105F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_111F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10026_I" deadCode="false" sourceNode="P_20F10026" targetNode="P_106F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_112F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10026_O" deadCode="false" sourceNode="P_20F10026" targetNode="P_107F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_112F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10026_I" deadCode="false" sourceNode="P_20F10026" targetNode="P_108F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_113F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10026_O" deadCode="false" sourceNode="P_20F10026" targetNode="P_109F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_113F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10026_I" deadCode="false" sourceNode="P_20F10026" targetNode="P_110F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_114F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10026_O" deadCode="false" sourceNode="P_20F10026" targetNode="P_111F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_114F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10026_I" deadCode="false" sourceNode="P_20F10026" targetNode="P_112F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_115F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10026_O" deadCode="false" sourceNode="P_20F10026" targetNode="P_113F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_115F10026"/>
	</edges>
	<edges id="P_20F10026P_21F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10026" targetNode="P_21F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10026_I" deadCode="false" sourceNode="P_22F10026" targetNode="P_114F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_119F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10026_O" deadCode="false" sourceNode="P_22F10026" targetNode="P_115F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_119F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10026_I" deadCode="false" sourceNode="P_22F10026" targetNode="P_116F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_120F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10026_O" deadCode="false" sourceNode="P_22F10026" targetNode="P_117F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_120F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10026_I" deadCode="false" sourceNode="P_22F10026" targetNode="P_118F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_121F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10026_O" deadCode="false" sourceNode="P_22F10026" targetNode="P_119F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_121F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10026_I" deadCode="false" sourceNode="P_22F10026" targetNode="P_120F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_122F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10026_O" deadCode="false" sourceNode="P_22F10026" targetNode="P_121F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_122F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10026_I" deadCode="false" sourceNode="P_22F10026" targetNode="P_122F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_123F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10026_O" deadCode="false" sourceNode="P_22F10026" targetNode="P_123F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_123F10026"/>
	</edges>
	<edges id="P_22F10026P_23F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10026" targetNode="P_23F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10026_I" deadCode="false" sourceNode="P_30F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_126F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10026_O" deadCode="false" sourceNode="P_30F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_126F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10026_I" deadCode="false" sourceNode="P_30F10026" targetNode="P_28F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_128F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10026_O" deadCode="false" sourceNode="P_30F10026" targetNode="P_29F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_128F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10026_I" deadCode="false" sourceNode="P_30F10026" targetNode="P_126F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_130F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10026_O" deadCode="false" sourceNode="P_30F10026" targetNode="P_127F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_130F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10026_I" deadCode="false" sourceNode="P_30F10026" targetNode="P_128F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_131F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10026_O" deadCode="false" sourceNode="P_30F10026" targetNode="P_129F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_131F10026"/>
	</edges>
	<edges id="P_30F10026P_31F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10026" targetNode="P_31F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10026_I" deadCode="false" sourceNode="P_32F10026" targetNode="P_130F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_133F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10026_O" deadCode="false" sourceNode="P_32F10026" targetNode="P_131F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_133F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10026_I" deadCode="false" sourceNode="P_32F10026" targetNode="P_132F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_135F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10026_O" deadCode="false" sourceNode="P_32F10026" targetNode="P_133F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_135F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10026_I" deadCode="false" sourceNode="P_32F10026" targetNode="P_134F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_136F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10026_O" deadCode="false" sourceNode="P_32F10026" targetNode="P_135F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_136F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10026_I" deadCode="false" sourceNode="P_32F10026" targetNode="P_136F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_137F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10026_O" deadCode="false" sourceNode="P_32F10026" targetNode="P_137F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_137F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10026_I" deadCode="false" sourceNode="P_32F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_138F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10026_O" deadCode="false" sourceNode="P_32F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_138F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10026_I" deadCode="false" sourceNode="P_32F10026" targetNode="P_28F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_140F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10026_O" deadCode="false" sourceNode="P_32F10026" targetNode="P_29F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_140F10026"/>
	</edges>
	<edges id="P_32F10026P_33F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10026" targetNode="P_33F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10026_I" deadCode="false" sourceNode="P_34F10026" targetNode="P_138F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_142F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10026_O" deadCode="false" sourceNode="P_34F10026" targetNode="P_139F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_142F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10026_I" deadCode="false" sourceNode="P_34F10026" targetNode="P_134F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_143F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10026_O" deadCode="false" sourceNode="P_34F10026" targetNode="P_135F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_143F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10026_I" deadCode="false" sourceNode="P_34F10026" targetNode="P_136F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_144F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10026_O" deadCode="false" sourceNode="P_34F10026" targetNode="P_137F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_144F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10026_I" deadCode="false" sourceNode="P_34F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_145F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10026_O" deadCode="false" sourceNode="P_34F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_145F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10026_I" deadCode="false" sourceNode="P_34F10026" targetNode="P_28F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_147F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10026_O" deadCode="false" sourceNode="P_34F10026" targetNode="P_29F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_147F10026"/>
	</edges>
	<edges id="P_34F10026P_35F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10026" targetNode="P_35F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10026_I" deadCode="false" sourceNode="P_36F10026" targetNode="P_28F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_150F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10026_O" deadCode="false" sourceNode="P_36F10026" targetNode="P_29F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_150F10026"/>
	</edges>
	<edges id="P_36F10026P_37F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10026" targetNode="P_37F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10026_I" deadCode="true" sourceNode="P_140F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_152F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10026_O" deadCode="true" sourceNode="P_140F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_152F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_155F10026_I" deadCode="false" sourceNode="P_38F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_155F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_155F10026_O" deadCode="false" sourceNode="P_38F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_155F10026"/>
	</edges>
	<edges id="P_38F10026P_39F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10026" targetNode="P_39F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10026_I" deadCode="true" sourceNode="P_142F10026" targetNode="P_138F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_158F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10026_O" deadCode="true" sourceNode="P_142F10026" targetNode="P_139F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_158F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_159F10026_I" deadCode="true" sourceNode="P_142F10026" targetNode="P_134F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_159F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_159F10026_O" deadCode="true" sourceNode="P_142F10026" targetNode="P_135F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_159F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10026_I" deadCode="true" sourceNode="P_142F10026" targetNode="P_136F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_160F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10026_O" deadCode="true" sourceNode="P_142F10026" targetNode="P_137F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_160F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10026_I" deadCode="true" sourceNode="P_142F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_161F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10026_O" deadCode="true" sourceNode="P_142F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_161F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10026_I" deadCode="true" sourceNode="P_144F10026" targetNode="P_140F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_164F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10026_O" deadCode="true" sourceNode="P_144F10026" targetNode="P_141F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_164F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_169F10026_I" deadCode="true" sourceNode="P_148F10026" targetNode="P_144F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_169F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_169F10026_O" deadCode="true" sourceNode="P_148F10026" targetNode="P_145F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_169F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10026_I" deadCode="true" sourceNode="P_148F10026" targetNode="P_149F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_171F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10026_O" deadCode="true" sourceNode="P_148F10026" targetNode="P_150F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_171F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10026_I" deadCode="false" sourceNode="P_152F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_175F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10026_O" deadCode="false" sourceNode="P_152F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_175F10026"/>
	</edges>
	<edges id="P_152F10026P_153F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_152F10026" targetNode="P_153F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_178F10026_I" deadCode="false" sourceNode="P_42F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_178F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_178F10026_O" deadCode="false" sourceNode="P_42F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_178F10026"/>
	</edges>
	<edges id="P_42F10026P_43F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10026" targetNode="P_43F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10026_I" deadCode="false" sourceNode="P_44F10026" targetNode="P_152F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_181F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10026_O" deadCode="false" sourceNode="P_44F10026" targetNode="P_153F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_181F10026"/>
	</edges>
	<edges id="P_44F10026P_45F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10026" targetNode="P_45F10026"/>
	<edges id="P_46F10026P_47F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10026" targetNode="P_47F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10026_I" deadCode="false" sourceNode="P_48F10026" targetNode="P_44F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_186F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10026_O" deadCode="false" sourceNode="P_48F10026" targetNode="P_45F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_186F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_188F10026_I" deadCode="false" sourceNode="P_48F10026" targetNode="P_50F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_188F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_188F10026_O" deadCode="false" sourceNode="P_48F10026" targetNode="P_51F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_188F10026"/>
	</edges>
	<edges id="P_48F10026P_49F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10026" targetNode="P_49F10026"/>
	<edges id="P_50F10026P_51F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10026" targetNode="P_51F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_192F10026_I" deadCode="false" sourceNode="P_154F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_192F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_192F10026_O" deadCode="false" sourceNode="P_154F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_192F10026"/>
	</edges>
	<edges id="P_154F10026P_155F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_154F10026" targetNode="P_155F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10026_I" deadCode="false" sourceNode="P_52F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_195F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10026_O" deadCode="false" sourceNode="P_52F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_195F10026"/>
	</edges>
	<edges id="P_52F10026P_53F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10026" targetNode="P_53F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_198F10026_I" deadCode="false" sourceNode="P_54F10026" targetNode="P_154F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_198F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_198F10026_O" deadCode="false" sourceNode="P_54F10026" targetNode="P_155F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_198F10026"/>
	</edges>
	<edges id="P_54F10026P_55F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10026" targetNode="P_55F10026"/>
	<edges id="P_56F10026P_57F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10026" targetNode="P_57F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_203F10026_I" deadCode="false" sourceNode="P_58F10026" targetNode="P_54F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_203F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_203F10026_O" deadCode="false" sourceNode="P_58F10026" targetNode="P_55F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_203F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_205F10026_I" deadCode="false" sourceNode="P_58F10026" targetNode="P_60F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_205F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_205F10026_O" deadCode="false" sourceNode="P_58F10026" targetNode="P_61F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_205F10026"/>
	</edges>
	<edges id="P_58F10026P_59F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10026" targetNode="P_59F10026"/>
	<edges id="P_60F10026P_61F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_60F10026" targetNode="P_61F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_209F10026_I" deadCode="false" sourceNode="P_156F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_209F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_209F10026_O" deadCode="false" sourceNode="P_156F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_209F10026"/>
	</edges>
	<edges id="P_156F10026P_157F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_156F10026" targetNode="P_157F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_212F10026_I" deadCode="false" sourceNode="P_62F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_212F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_212F10026_O" deadCode="false" sourceNode="P_62F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_212F10026"/>
	</edges>
	<edges id="P_62F10026P_63F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10026" targetNode="P_63F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_215F10026_I" deadCode="false" sourceNode="P_64F10026" targetNode="P_156F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_215F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_215F10026_O" deadCode="false" sourceNode="P_64F10026" targetNode="P_157F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_215F10026"/>
	</edges>
	<edges id="P_64F10026P_65F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10026" targetNode="P_65F10026"/>
	<edges id="P_66F10026P_67F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10026" targetNode="P_67F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_220F10026_I" deadCode="false" sourceNode="P_68F10026" targetNode="P_64F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_220F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_220F10026_O" deadCode="false" sourceNode="P_68F10026" targetNode="P_65F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_220F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_222F10026_I" deadCode="false" sourceNode="P_68F10026" targetNode="P_70F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_222F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_222F10026_O" deadCode="false" sourceNode="P_68F10026" targetNode="P_71F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_222F10026"/>
	</edges>
	<edges id="P_68F10026P_69F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10026" targetNode="P_69F10026"/>
	<edges id="P_70F10026P_71F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_70F10026" targetNode="P_71F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_226F10026_I" deadCode="false" sourceNode="P_158F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_226F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_226F10026_O" deadCode="false" sourceNode="P_158F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_226F10026"/>
	</edges>
	<edges id="P_158F10026P_159F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_158F10026" targetNode="P_159F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_229F10026_I" deadCode="false" sourceNode="P_72F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_229F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_229F10026_O" deadCode="false" sourceNode="P_72F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_229F10026"/>
	</edges>
	<edges id="P_72F10026P_73F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10026" targetNode="P_73F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_232F10026_I" deadCode="false" sourceNode="P_74F10026" targetNode="P_158F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_232F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_232F10026_O" deadCode="false" sourceNode="P_74F10026" targetNode="P_159F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_232F10026"/>
	</edges>
	<edges id="P_74F10026P_75F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10026" targetNode="P_75F10026"/>
	<edges id="P_76F10026P_77F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_76F10026" targetNode="P_77F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_237F10026_I" deadCode="false" sourceNode="P_78F10026" targetNode="P_74F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_237F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_237F10026_O" deadCode="false" sourceNode="P_78F10026" targetNode="P_75F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_237F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_239F10026_I" deadCode="false" sourceNode="P_78F10026" targetNode="P_80F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_239F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_239F10026_O" deadCode="false" sourceNode="P_78F10026" targetNode="P_81F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_239F10026"/>
	</edges>
	<edges id="P_78F10026P_79F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_78F10026" targetNode="P_79F10026"/>
	<edges id="P_80F10026P_81F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_80F10026" targetNode="P_81F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_243F10026_I" deadCode="false" sourceNode="P_82F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_243F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_243F10026_O" deadCode="false" sourceNode="P_82F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_243F10026"/>
	</edges>
	<edges id="P_82F10026P_83F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_82F10026" targetNode="P_83F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_246F10026_I" deadCode="false" sourceNode="P_160F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_246F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_246F10026_O" deadCode="false" sourceNode="P_160F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_246F10026"/>
	</edges>
	<edges id="P_160F10026P_161F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_160F10026" targetNode="P_161F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_249F10026_I" deadCode="false" sourceNode="P_84F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_249F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_249F10026_O" deadCode="false" sourceNode="P_84F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_249F10026"/>
	</edges>
	<edges id="P_84F10026P_85F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_84F10026" targetNode="P_85F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_252F10026_I" deadCode="false" sourceNode="P_86F10026" targetNode="P_160F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_252F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_252F10026_O" deadCode="false" sourceNode="P_86F10026" targetNode="P_161F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_252F10026"/>
	</edges>
	<edges id="P_86F10026P_87F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_86F10026" targetNode="P_87F10026"/>
	<edges id="P_88F10026P_89F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_88F10026" targetNode="P_89F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_257F10026_I" deadCode="false" sourceNode="P_90F10026" targetNode="P_86F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_257F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_257F10026_O" deadCode="false" sourceNode="P_90F10026" targetNode="P_87F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_257F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10026_I" deadCode="false" sourceNode="P_90F10026" targetNode="P_92F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_259F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10026_O" deadCode="false" sourceNode="P_90F10026" targetNode="P_93F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_259F10026"/>
	</edges>
	<edges id="P_90F10026P_91F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_90F10026" targetNode="P_91F10026"/>
	<edges id="P_92F10026P_93F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_92F10026" targetNode="P_93F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_263F10026_I" deadCode="false" sourceNode="P_162F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_263F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_263F10026_O" deadCode="false" sourceNode="P_162F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_263F10026"/>
	</edges>
	<edges id="P_162F10026P_163F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_162F10026" targetNode="P_163F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_266F10026_I" deadCode="false" sourceNode="P_94F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_266F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_266F10026_O" deadCode="false" sourceNode="P_94F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_266F10026"/>
	</edges>
	<edges id="P_94F10026P_95F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_94F10026" targetNode="P_95F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_269F10026_I" deadCode="false" sourceNode="P_96F10026" targetNode="P_162F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_269F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_269F10026_O" deadCode="false" sourceNode="P_96F10026" targetNode="P_163F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_269F10026"/>
	</edges>
	<edges id="P_96F10026P_97F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_96F10026" targetNode="P_97F10026"/>
	<edges id="P_98F10026P_99F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_98F10026" targetNode="P_99F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_274F10026_I" deadCode="false" sourceNode="P_100F10026" targetNode="P_96F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_274F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_274F10026_O" deadCode="false" sourceNode="P_100F10026" targetNode="P_97F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_274F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10026_I" deadCode="false" sourceNode="P_100F10026" targetNode="P_102F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_276F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10026_O" deadCode="false" sourceNode="P_100F10026" targetNode="P_103F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_276F10026"/>
	</edges>
	<edges id="P_100F10026P_101F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_100F10026" targetNode="P_101F10026"/>
	<edges id="P_102F10026P_103F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_102F10026" targetNode="P_103F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10026_I" deadCode="false" sourceNode="P_164F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_280F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10026_O" deadCode="false" sourceNode="P_164F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_280F10026"/>
	</edges>
	<edges id="P_164F10026P_165F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_164F10026" targetNode="P_165F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10026_I" deadCode="false" sourceNode="P_104F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_283F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10026_O" deadCode="false" sourceNode="P_104F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_283F10026"/>
	</edges>
	<edges id="P_104F10026P_105F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_104F10026" targetNode="P_105F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_286F10026_I" deadCode="false" sourceNode="P_106F10026" targetNode="P_164F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_286F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_286F10026_O" deadCode="false" sourceNode="P_106F10026" targetNode="P_165F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_286F10026"/>
	</edges>
	<edges id="P_106F10026P_107F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_106F10026" targetNode="P_107F10026"/>
	<edges id="P_108F10026P_109F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_108F10026" targetNode="P_109F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_291F10026_I" deadCode="false" sourceNode="P_110F10026" targetNode="P_106F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_291F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_291F10026_O" deadCode="false" sourceNode="P_110F10026" targetNode="P_107F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_291F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_293F10026_I" deadCode="false" sourceNode="P_110F10026" targetNode="P_112F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_293F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_293F10026_O" deadCode="false" sourceNode="P_110F10026" targetNode="P_113F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_293F10026"/>
	</edges>
	<edges id="P_110F10026P_111F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_110F10026" targetNode="P_111F10026"/>
	<edges id="P_112F10026P_113F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_112F10026" targetNode="P_113F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_297F10026_I" deadCode="false" sourceNode="P_166F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_297F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_297F10026_O" deadCode="false" sourceNode="P_166F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_297F10026"/>
	</edges>
	<edges id="P_166F10026P_167F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_166F10026" targetNode="P_167F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_300F10026_I" deadCode="false" sourceNode="P_114F10026" targetNode="P_124F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_300F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_300F10026_O" deadCode="false" sourceNode="P_114F10026" targetNode="P_125F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_300F10026"/>
	</edges>
	<edges id="P_114F10026P_115F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_114F10026" targetNode="P_115F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_303F10026_I" deadCode="false" sourceNode="P_116F10026" targetNode="P_166F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_303F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_303F10026_O" deadCode="false" sourceNode="P_116F10026" targetNode="P_167F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_303F10026"/>
	</edges>
	<edges id="P_116F10026P_117F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_116F10026" targetNode="P_117F10026"/>
	<edges id="P_118F10026P_119F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_118F10026" targetNode="P_119F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10026_I" deadCode="false" sourceNode="P_120F10026" targetNode="P_116F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_308F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10026_O" deadCode="false" sourceNode="P_120F10026" targetNode="P_117F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_308F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_310F10026_I" deadCode="false" sourceNode="P_120F10026" targetNode="P_122F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_310F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_310F10026_O" deadCode="false" sourceNode="P_120F10026" targetNode="P_123F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_310F10026"/>
	</edges>
	<edges id="P_120F10026P_121F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_120F10026" targetNode="P_121F10026"/>
	<edges id="P_122F10026P_123F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_122F10026" targetNode="P_123F10026"/>
	<edges id="P_126F10026P_127F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_126F10026" targetNode="P_127F10026"/>
	<edges id="P_132F10026P_133F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_132F10026" targetNode="P_133F10026"/>
	<edges id="P_138F10026P_139F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_138F10026" targetNode="P_139F10026"/>
	<edges id="P_134F10026P_135F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_134F10026" targetNode="P_135F10026"/>
	<edges id="P_130F10026P_131F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_130F10026" targetNode="P_131F10026"/>
	<edges id="P_40F10026P_41F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10026" targetNode="P_41F10026"/>
	<edges id="P_136F10026P_137F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_136F10026" targetNode="P_137F10026"/>
	<edges id="P_128F10026P_129F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_128F10026" targetNode="P_129F10026"/>
	<edges id="P_124F10026P_125F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_124F10026" targetNode="P_125F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_358F10026_I" deadCode="false" sourceNode="P_26F10026" targetNode="P_170F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_358F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_358F10026_O" deadCode="false" sourceNode="P_26F10026" targetNode="P_171F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_358F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_360F10026_I" deadCode="false" sourceNode="P_26F10026" targetNode="P_172F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_360F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_360F10026_O" deadCode="false" sourceNode="P_26F10026" targetNode="P_173F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_360F10026"/>
	</edges>
	<edges id="P_26F10026P_27F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10026" targetNode="P_27F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_365F10026_I" deadCode="true" sourceNode="P_170F10026" targetNode="P_174F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_365F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_365F10026_O" deadCode="true" sourceNode="P_170F10026" targetNode="P_175F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_365F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_370F10026_I" deadCode="true" sourceNode="P_170F10026" targetNode="P_174F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_370F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_370F10026_O" deadCode="true" sourceNode="P_170F10026" targetNode="P_175F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_370F10026"/>
	</edges>
	<edges id="P_170F10026P_171F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_170F10026" targetNode="P_171F10026"/>
	<edges id="P_172F10026P_173F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_172F10026" targetNode="P_173F10026"/>
	<edges id="P_174F10026P_175F10026" xsi:type="cbl:FallThroughEdge" sourceNode="P_174F10026" targetNode="P_175F10026"/>
	<edges xsi:type="cbl:PerformEdge" id="S_399F10026_I" deadCode="true" sourceNode="P_178F10026" targetNode="P_179F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_399F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_399F10026_O" deadCode="true" sourceNode="P_178F10026" targetNode="P_180F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_399F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_400F10026_I" deadCode="true" sourceNode="P_178F10026" targetNode="P_181F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_400F10026"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_400F10026_O" deadCode="true" sourceNode="P_178F10026" targetNode="P_182F10026">
		<representations href="../../../cobol/IDBSCNO0.cbl.cobModel#S_400F10026"/>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_127F10026_POS1" deadCode="false" targetNode="P_30F10026" sourceNode="DB2_COMP_NUM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_127F10026"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_139F10026_POS1" deadCode="false" sourceNode="P_32F10026" targetNode="DB2_COMP_NUM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_139F10026"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_146F10026_POS1" deadCode="false" sourceNode="P_34F10026" targetNode="DB2_COMP_NUM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_146F10026"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_149F10026_POS1" deadCode="false" sourceNode="P_36F10026" targetNode="DB2_COMP_NUM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_149F10026"></representations>
	</edges>
</Package>

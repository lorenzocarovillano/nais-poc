<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IDBSRST0" cbl:id="IDBSRST0" xsi:id="IDBSRST0" packageRef="IDBSRST0.igd#IDBSRST0" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IDBSRST0_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10087" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IDBSRST0.cbl.cobModel#SC_1F10087"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10087" deadCode="false" name="PROGRAM_IDBSRST0_FIRST_SENTENCES">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_1F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10087" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_2F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10087" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_3F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10087" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_28F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10087" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_29F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10087" deadCode="false" name="A200-ELABORA-PK">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_24F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10087" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_25F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10087" deadCode="false" name="A300-ELABORA-ID-EFF">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_4F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10087" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_5F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10087" deadCode="false" name="A400-ELABORA-IDP-EFF">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_6F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10087" deadCode="false" name="A400-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_7F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10087" deadCode="false" name="A500-ELABORA-IBO">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_8F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10087" deadCode="false" name="A500-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_9F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10087" deadCode="false" name="A600-ELABORA-IBS">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_10F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10087" deadCode="false" name="A600-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_11F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10087" deadCode="false" name="A700-ELABORA-IDO">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_12F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10087" deadCode="false" name="A700-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_13F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10087" deadCode="false" name="B300-ELABORA-ID-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_14F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10087" deadCode="false" name="B300-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_15F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10087" deadCode="false" name="B400-ELABORA-IDP-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_16F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10087" deadCode="false" name="B400-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_17F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10087" deadCode="false" name="B500-ELABORA-IBO-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_18F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10087" deadCode="false" name="B500-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_19F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10087" deadCode="false" name="B600-ELABORA-IBS-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_20F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10087" deadCode="false" name="B600-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_21F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10087" deadCode="false" name="B700-ELABORA-IDO-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_22F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10087" deadCode="false" name="B700-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_23F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10087" deadCode="false" name="A210-SELECT-PK">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_30F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10087" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_31F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10087" deadCode="false" name="A220-INSERT-PK">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_32F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10087" deadCode="false" name="A220-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_33F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10087" deadCode="false" name="A230-UPDATE-PK">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_34F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10087" deadCode="false" name="A230-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_35F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10087" deadCode="false" name="A240-DELETE-PK">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_36F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10087" deadCode="false" name="A240-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_37F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_142F10087" deadCode="false" name="A305-DECLARE-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_142F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_143F10087" deadCode="false" name="A305-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_143F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10087" deadCode="false" name="A310-SELECT-ID-EFF">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_38F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10087" deadCode="false" name="A310-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_39F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_144F10087" deadCode="false" name="A330-UPDATE-ID-EFF">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_144F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_145F10087" deadCode="false" name="A330-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_145F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_146F10087" deadCode="false" name="A360-OPEN-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_146F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_147F10087" deadCode="false" name="A360-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_147F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_148F10087" deadCode="false" name="A370-CLOSE-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_148F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_149F10087" deadCode="false" name="A370-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_149F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_150F10087" deadCode="true" name="A380-FETCH-FIRST-ID-EFF">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_150F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_153F10087" deadCode="true" name="A380-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_153F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_151F10087" deadCode="false" name="A390-FETCH-NEXT-ID-EFF">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_151F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_152F10087" deadCode="false" name="A390-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_152F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_154F10087" deadCode="false" name="A405-DECLARE-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_154F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_155F10087" deadCode="false" name="A405-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_155F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10087" deadCode="false" name="A410-SELECT-IDP-EFF">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_44F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10087" deadCode="false" name="A410-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_45F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10087" deadCode="false" name="A460-OPEN-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_46F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10087" deadCode="false" name="A460-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_47F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10087" deadCode="false" name="A470-CLOSE-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_48F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10087" deadCode="false" name="A470-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_49F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10087" deadCode="false" name="A480-FETCH-FIRST-IDP-EFF">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_50F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10087" deadCode="false" name="A480-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_51F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10087" deadCode="false" name="A490-FETCH-NEXT-IDP-EFF">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_52F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10087" deadCode="false" name="A490-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_53F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_156F10087" deadCode="false" name="A505-DECLARE-CURSOR-IBO">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_156F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_157F10087" deadCode="false" name="A505-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_157F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10087" deadCode="false" name="A510-SELECT-IBO">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_54F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10087" deadCode="false" name="A510-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_55F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10087" deadCode="false" name="A560-OPEN-CURSOR-IBO">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_56F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10087" deadCode="false" name="A560-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_57F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10087" deadCode="false" name="A570-CLOSE-CURSOR-IBO">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_58F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10087" deadCode="false" name="A570-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_59F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10087" deadCode="false" name="A580-FETCH-FIRST-IBO">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_60F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10087" deadCode="false" name="A580-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_61F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10087" deadCode="false" name="A590-FETCH-NEXT-IBO">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_62F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10087" deadCode="false" name="A590-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_63F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_158F10087" deadCode="false" name="A605-DECLARE-CURSOR-IBS">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_158F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_159F10087" deadCode="false" name="A605-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_159F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10087" deadCode="false" name="A610-SELECT-IBS">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_64F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10087" deadCode="false" name="A610-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_65F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10087" deadCode="false" name="A660-OPEN-CURSOR-IBS">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_66F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10087" deadCode="false" name="A660-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_67F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10087" deadCode="false" name="A670-CLOSE-CURSOR-IBS">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_68F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10087" deadCode="false" name="A670-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_69F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10087" deadCode="false" name="A680-FETCH-FIRST-IBS">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_70F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10087" deadCode="false" name="A680-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_71F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10087" deadCode="false" name="A690-FETCH-NEXT-IBS">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_72F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10087" deadCode="false" name="A690-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_73F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_160F10087" deadCode="false" name="A705-DECLARE-CURSOR-IDO">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_160F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_161F10087" deadCode="false" name="A705-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_161F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10087" deadCode="false" name="A710-SELECT-IDO">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_74F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10087" deadCode="false" name="A710-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_75F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10087" deadCode="false" name="A760-OPEN-CURSOR-IDO">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_76F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10087" deadCode="false" name="A760-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_77F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_78F10087" deadCode="false" name="A770-CLOSE-CURSOR-IDO">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_78F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_79F10087" deadCode="false" name="A770-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_79F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_80F10087" deadCode="false" name="A780-FETCH-FIRST-IDO">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_80F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_81F10087" deadCode="false" name="A780-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_81F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_82F10087" deadCode="false" name="A790-FETCH-NEXT-IDO">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_82F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_83F10087" deadCode="false" name="A790-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_83F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_84F10087" deadCode="false" name="B310-SELECT-ID-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_84F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_85F10087" deadCode="false" name="B310-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_85F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_162F10087" deadCode="false" name="B405-DECLARE-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_162F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_163F10087" deadCode="false" name="B405-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_163F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_86F10087" deadCode="false" name="B410-SELECT-IDP-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_86F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_87F10087" deadCode="false" name="B410-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_87F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_88F10087" deadCode="false" name="B460-OPEN-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_88F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_89F10087" deadCode="false" name="B460-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_89F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_90F10087" deadCode="false" name="B470-CLOSE-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_90F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_91F10087" deadCode="false" name="B470-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_91F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_92F10087" deadCode="false" name="B480-FETCH-FIRST-IDP-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_92F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_93F10087" deadCode="false" name="B480-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_93F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_94F10087" deadCode="false" name="B490-FETCH-NEXT-IDP-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_94F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_95F10087" deadCode="false" name="B490-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_95F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_164F10087" deadCode="false" name="B505-DECLARE-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_164F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_165F10087" deadCode="false" name="B505-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_165F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_96F10087" deadCode="false" name="B510-SELECT-IBO-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_96F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_97F10087" deadCode="false" name="B510-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_97F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_98F10087" deadCode="false" name="B560-OPEN-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_98F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_99F10087" deadCode="false" name="B560-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_99F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_100F10087" deadCode="false" name="B570-CLOSE-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_100F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_101F10087" deadCode="false" name="B570-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_101F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_102F10087" deadCode="false" name="B580-FETCH-FIRST-IBO-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_102F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_103F10087" deadCode="false" name="B580-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_103F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_104F10087" deadCode="false" name="B590-FETCH-NEXT-IBO-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_104F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_105F10087" deadCode="false" name="B590-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_105F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_166F10087" deadCode="false" name="B605-DECLARE-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_166F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_167F10087" deadCode="false" name="B605-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_167F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_106F10087" deadCode="false" name="B610-SELECT-IBS-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_106F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_107F10087" deadCode="false" name="B610-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_107F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_108F10087" deadCode="false" name="B660-OPEN-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_108F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_109F10087" deadCode="false" name="B660-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_109F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_110F10087" deadCode="false" name="B670-CLOSE-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_110F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_111F10087" deadCode="false" name="B670-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_111F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_112F10087" deadCode="false" name="B680-FETCH-FIRST-IBS-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_112F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_113F10087" deadCode="false" name="B680-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_113F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_114F10087" deadCode="false" name="B690-FETCH-NEXT-IBS-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_114F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_115F10087" deadCode="false" name="B690-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_115F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_168F10087" deadCode="false" name="B705-DECLARE-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_168F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_169F10087" deadCode="false" name="B705-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_169F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_116F10087" deadCode="false" name="B710-SELECT-IDO-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_116F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_117F10087" deadCode="false" name="B710-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_117F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_118F10087" deadCode="false" name="B760-OPEN-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_118F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_119F10087" deadCode="false" name="B760-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_119F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_120F10087" deadCode="false" name="B770-CLOSE-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_120F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_121F10087" deadCode="false" name="B770-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_121F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_122F10087" deadCode="false" name="B780-FETCH-FIRST-IDO-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_122F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_123F10087" deadCode="false" name="B780-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_123F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_124F10087" deadCode="false" name="B790-FETCH-NEXT-IDO-CPZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_124F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_125F10087" deadCode="false" name="B790-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_125F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_128F10087" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_128F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_129F10087" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_129F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_134F10087" deadCode="false" name="Z150-VALORIZZA-DATA-SERVICES-I">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_134F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_135F10087" deadCode="false" name="Z150-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_135F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_140F10087" deadCode="false" name="Z160-VALORIZZA-DATA-SERVICES-U">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_140F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_141F10087" deadCode="false" name="Z160-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_141F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_136F10087" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_136F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_137F10087" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_137F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_132F10087" deadCode="false" name="Z400-SEQ-RIGA">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_132F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_133F10087" deadCode="false" name="Z400-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_133F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10087" deadCode="false" name="Z500-AGGIORNAMENTO-STORICO">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_40F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10087" deadCode="false" name="Z500-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_41F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10087" deadCode="false" name="Z550-AGG-STORICO-SOLO-INS">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_42F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10087" deadCode="false" name="Z550-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_43F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_170F10087" deadCode="false" name="Z600-INSERT-NUOVA-RIGA-STORICA">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_170F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_171F10087" deadCode="false" name="Z600-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_171F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_138F10087" deadCode="false" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_138F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_139F10087" deadCode="false" name="Z900-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_139F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_130F10087" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_130F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_131F10087" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_131F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_126F10087" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_126F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_127F10087" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_127F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10087" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_26F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10087" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_27F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_176F10087" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_176F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_177F10087" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_177F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_178F10087" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_178F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_179F10087" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_179F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_172F10087" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_172F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_173F10087" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_173F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_180F10087" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_180F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_181F10087" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_181F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_174F10087" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_174F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_175F10087" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_175F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_182F10087" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_182F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_183F10087" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_183F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_184F10087" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_184F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_185F10087" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_185F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_186F10087" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_186F10087"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_187F10087" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/IDBSRST0.cbl.cobModel#P_187F10087"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_RIS_DI_TRCH" name="RIS_DI_TRCH">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_RIS_DI_TRCH"/>
		</children>
	</packageNode>
	<edges id="SC_1F10087P_1F10087" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10087" targetNode="P_1F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10087_I" deadCode="false" sourceNode="P_1F10087" targetNode="P_2F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_1F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10087_O" deadCode="false" sourceNode="P_1F10087" targetNode="P_3F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_1F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10087_I" deadCode="false" sourceNode="P_1F10087" targetNode="P_4F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_5F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10087_O" deadCode="false" sourceNode="P_1F10087" targetNode="P_5F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_5F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10087_I" deadCode="false" sourceNode="P_1F10087" targetNode="P_6F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_6F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10087_O" deadCode="false" sourceNode="P_1F10087" targetNode="P_7F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_6F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10087_I" deadCode="false" sourceNode="P_1F10087" targetNode="P_8F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_7F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10087_O" deadCode="false" sourceNode="P_1F10087" targetNode="P_9F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_7F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10087_I" deadCode="false" sourceNode="P_1F10087" targetNode="P_10F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_8F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10087_O" deadCode="false" sourceNode="P_1F10087" targetNode="P_11F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_8F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10087_I" deadCode="false" sourceNode="P_1F10087" targetNode="P_12F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_9F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10087_O" deadCode="false" sourceNode="P_1F10087" targetNode="P_13F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_9F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10087_I" deadCode="false" sourceNode="P_1F10087" targetNode="P_14F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_13F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10087_O" deadCode="false" sourceNode="P_1F10087" targetNode="P_15F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_13F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10087_I" deadCode="false" sourceNode="P_1F10087" targetNode="P_16F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_14F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10087_O" deadCode="false" sourceNode="P_1F10087" targetNode="P_17F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_14F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10087_I" deadCode="false" sourceNode="P_1F10087" targetNode="P_18F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_15F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10087_O" deadCode="false" sourceNode="P_1F10087" targetNode="P_19F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_15F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10087_I" deadCode="false" sourceNode="P_1F10087" targetNode="P_20F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_16F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10087_O" deadCode="false" sourceNode="P_1F10087" targetNode="P_21F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_16F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10087_I" deadCode="false" sourceNode="P_1F10087" targetNode="P_22F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_17F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10087_O" deadCode="false" sourceNode="P_1F10087" targetNode="P_23F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_17F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10087_I" deadCode="false" sourceNode="P_1F10087" targetNode="P_24F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_21F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10087_O" deadCode="false" sourceNode="P_1F10087" targetNode="P_25F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_21F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10087_I" deadCode="false" sourceNode="P_1F10087" targetNode="P_8F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_22F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10087_O" deadCode="false" sourceNode="P_1F10087" targetNode="P_9F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_22F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10087_I" deadCode="false" sourceNode="P_1F10087" targetNode="P_10F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_23F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10087_O" deadCode="false" sourceNode="P_1F10087" targetNode="P_11F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_23F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10087_I" deadCode="false" sourceNode="P_1F10087" targetNode="P_12F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_24F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10087_O" deadCode="false" sourceNode="P_1F10087" targetNode="P_13F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_24F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10087_I" deadCode="false" sourceNode="P_2F10087" targetNode="P_26F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_33F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10087_O" deadCode="false" sourceNode="P_2F10087" targetNode="P_27F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_33F10087"/>
	</edges>
	<edges id="P_2F10087P_3F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10087" targetNode="P_3F10087"/>
	<edges id="P_28F10087P_29F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10087" targetNode="P_29F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10087_I" deadCode="false" sourceNode="P_24F10087" targetNode="P_30F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_46F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10087_O" deadCode="false" sourceNode="P_24F10087" targetNode="P_31F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_46F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10087_I" deadCode="false" sourceNode="P_24F10087" targetNode="P_32F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_47F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10087_O" deadCode="false" sourceNode="P_24F10087" targetNode="P_33F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_47F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10087_I" deadCode="false" sourceNode="P_24F10087" targetNode="P_34F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_48F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10087_O" deadCode="false" sourceNode="P_24F10087" targetNode="P_35F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_48F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10087_I" deadCode="false" sourceNode="P_24F10087" targetNode="P_36F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_49F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10087_O" deadCode="false" sourceNode="P_24F10087" targetNode="P_37F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_49F10087"/>
	</edges>
	<edges id="P_24F10087P_25F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10087" targetNode="P_25F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10087_I" deadCode="false" sourceNode="P_4F10087" targetNode="P_38F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_53F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10087_O" deadCode="false" sourceNode="P_4F10087" targetNode="P_39F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_53F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10087_I" deadCode="false" sourceNode="P_4F10087" targetNode="P_40F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_54F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10087_O" deadCode="false" sourceNode="P_4F10087" targetNode="P_41F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_54F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10087_I" deadCode="false" sourceNode="P_4F10087" targetNode="P_42F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_55F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10087_O" deadCode="false" sourceNode="P_4F10087" targetNode="P_43F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_55F10087"/>
	</edges>
	<edges id="P_4F10087P_5F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10087" targetNode="P_5F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10087_I" deadCode="false" sourceNode="P_6F10087" targetNode="P_44F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_59F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10087_O" deadCode="false" sourceNode="P_6F10087" targetNode="P_45F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_59F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10087_I" deadCode="false" sourceNode="P_6F10087" targetNode="P_46F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_60F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10087_O" deadCode="false" sourceNode="P_6F10087" targetNode="P_47F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_60F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10087_I" deadCode="false" sourceNode="P_6F10087" targetNode="P_48F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_61F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10087_O" deadCode="false" sourceNode="P_6F10087" targetNode="P_49F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_61F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10087_I" deadCode="false" sourceNode="P_6F10087" targetNode="P_50F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_62F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10087_O" deadCode="false" sourceNode="P_6F10087" targetNode="P_51F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_62F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10087_I" deadCode="false" sourceNode="P_6F10087" targetNode="P_52F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_63F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10087_O" deadCode="false" sourceNode="P_6F10087" targetNode="P_53F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_63F10087"/>
	</edges>
	<edges id="P_6F10087P_7F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10087" targetNode="P_7F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10087_I" deadCode="false" sourceNode="P_8F10087" targetNode="P_54F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_67F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10087_O" deadCode="false" sourceNode="P_8F10087" targetNode="P_55F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_67F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10087_I" deadCode="false" sourceNode="P_8F10087" targetNode="P_56F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_68F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10087_O" deadCode="false" sourceNode="P_8F10087" targetNode="P_57F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_68F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10087_I" deadCode="false" sourceNode="P_8F10087" targetNode="P_58F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_69F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10087_O" deadCode="false" sourceNode="P_8F10087" targetNode="P_59F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_69F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10087_I" deadCode="false" sourceNode="P_8F10087" targetNode="P_60F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_70F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10087_O" deadCode="false" sourceNode="P_8F10087" targetNode="P_61F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_70F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10087_I" deadCode="false" sourceNode="P_8F10087" targetNode="P_62F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_71F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10087_O" deadCode="false" sourceNode="P_8F10087" targetNode="P_63F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_71F10087"/>
	</edges>
	<edges id="P_8F10087P_9F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10087" targetNode="P_9F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10087_I" deadCode="false" sourceNode="P_10F10087" targetNode="P_64F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_75F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10087_O" deadCode="false" sourceNode="P_10F10087" targetNode="P_65F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_75F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10087_I" deadCode="false" sourceNode="P_10F10087" targetNode="P_66F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_76F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10087_O" deadCode="false" sourceNode="P_10F10087" targetNode="P_67F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_76F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10087_I" deadCode="false" sourceNode="P_10F10087" targetNode="P_68F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_77F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10087_O" deadCode="false" sourceNode="P_10F10087" targetNode="P_69F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_77F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10087_I" deadCode="false" sourceNode="P_10F10087" targetNode="P_70F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_78F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10087_O" deadCode="false" sourceNode="P_10F10087" targetNode="P_71F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_78F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10087_I" deadCode="false" sourceNode="P_10F10087" targetNode="P_72F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_79F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10087_O" deadCode="false" sourceNode="P_10F10087" targetNode="P_73F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_79F10087"/>
	</edges>
	<edges id="P_10F10087P_11F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10087" targetNode="P_11F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10087_I" deadCode="false" sourceNode="P_12F10087" targetNode="P_74F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_83F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10087_O" deadCode="false" sourceNode="P_12F10087" targetNode="P_75F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_83F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10087_I" deadCode="false" sourceNode="P_12F10087" targetNode="P_76F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_84F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10087_O" deadCode="false" sourceNode="P_12F10087" targetNode="P_77F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_84F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10087_I" deadCode="false" sourceNode="P_12F10087" targetNode="P_78F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_85F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10087_O" deadCode="false" sourceNode="P_12F10087" targetNode="P_79F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_85F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10087_I" deadCode="false" sourceNode="P_12F10087" targetNode="P_80F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_86F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10087_O" deadCode="false" sourceNode="P_12F10087" targetNode="P_81F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_86F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10087_I" deadCode="false" sourceNode="P_12F10087" targetNode="P_82F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_87F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10087_O" deadCode="false" sourceNode="P_12F10087" targetNode="P_83F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_87F10087"/>
	</edges>
	<edges id="P_12F10087P_13F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10087" targetNode="P_13F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10087_I" deadCode="false" sourceNode="P_14F10087" targetNode="P_84F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_91F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10087_O" deadCode="false" sourceNode="P_14F10087" targetNode="P_85F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_91F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10087_I" deadCode="false" sourceNode="P_14F10087" targetNode="P_40F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_92F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10087_O" deadCode="false" sourceNode="P_14F10087" targetNode="P_41F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_92F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10087_I" deadCode="false" sourceNode="P_14F10087" targetNode="P_42F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_93F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10087_O" deadCode="false" sourceNode="P_14F10087" targetNode="P_43F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_93F10087"/>
	</edges>
	<edges id="P_14F10087P_15F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10087" targetNode="P_15F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10087_I" deadCode="false" sourceNode="P_16F10087" targetNode="P_86F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_97F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10087_O" deadCode="false" sourceNode="P_16F10087" targetNode="P_87F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_97F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10087_I" deadCode="false" sourceNode="P_16F10087" targetNode="P_88F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_98F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10087_O" deadCode="false" sourceNode="P_16F10087" targetNode="P_89F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_98F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10087_I" deadCode="false" sourceNode="P_16F10087" targetNode="P_90F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_99F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10087_O" deadCode="false" sourceNode="P_16F10087" targetNode="P_91F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_99F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10087_I" deadCode="false" sourceNode="P_16F10087" targetNode="P_92F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_100F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10087_O" deadCode="false" sourceNode="P_16F10087" targetNode="P_93F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_100F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10087_I" deadCode="false" sourceNode="P_16F10087" targetNode="P_94F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_101F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10087_O" deadCode="false" sourceNode="P_16F10087" targetNode="P_95F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_101F10087"/>
	</edges>
	<edges id="P_16F10087P_17F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10087" targetNode="P_17F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10087_I" deadCode="false" sourceNode="P_18F10087" targetNode="P_96F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_105F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10087_O" deadCode="false" sourceNode="P_18F10087" targetNode="P_97F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_105F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10087_I" deadCode="false" sourceNode="P_18F10087" targetNode="P_98F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_106F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10087_O" deadCode="false" sourceNode="P_18F10087" targetNode="P_99F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_106F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10087_I" deadCode="false" sourceNode="P_18F10087" targetNode="P_100F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_107F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10087_O" deadCode="false" sourceNode="P_18F10087" targetNode="P_101F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_107F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10087_I" deadCode="false" sourceNode="P_18F10087" targetNode="P_102F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_108F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10087_O" deadCode="false" sourceNode="P_18F10087" targetNode="P_103F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_108F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10087_I" deadCode="false" sourceNode="P_18F10087" targetNode="P_104F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_109F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10087_O" deadCode="false" sourceNode="P_18F10087" targetNode="P_105F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_109F10087"/>
	</edges>
	<edges id="P_18F10087P_19F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10087" targetNode="P_19F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10087_I" deadCode="false" sourceNode="P_20F10087" targetNode="P_106F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_113F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10087_O" deadCode="false" sourceNode="P_20F10087" targetNode="P_107F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_113F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10087_I" deadCode="false" sourceNode="P_20F10087" targetNode="P_108F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_114F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10087_O" deadCode="false" sourceNode="P_20F10087" targetNode="P_109F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_114F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10087_I" deadCode="false" sourceNode="P_20F10087" targetNode="P_110F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_115F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10087_O" deadCode="false" sourceNode="P_20F10087" targetNode="P_111F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_115F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10087_I" deadCode="false" sourceNode="P_20F10087" targetNode="P_112F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_116F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10087_O" deadCode="false" sourceNode="P_20F10087" targetNode="P_113F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_116F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10087_I" deadCode="false" sourceNode="P_20F10087" targetNode="P_114F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_117F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10087_O" deadCode="false" sourceNode="P_20F10087" targetNode="P_115F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_117F10087"/>
	</edges>
	<edges id="P_20F10087P_21F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10087" targetNode="P_21F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10087_I" deadCode="false" sourceNode="P_22F10087" targetNode="P_116F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_121F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10087_O" deadCode="false" sourceNode="P_22F10087" targetNode="P_117F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_121F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10087_I" deadCode="false" sourceNode="P_22F10087" targetNode="P_118F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_122F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10087_O" deadCode="false" sourceNode="P_22F10087" targetNode="P_119F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_122F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10087_I" deadCode="false" sourceNode="P_22F10087" targetNode="P_120F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_123F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10087_O" deadCode="false" sourceNode="P_22F10087" targetNode="P_121F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_123F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10087_I" deadCode="false" sourceNode="P_22F10087" targetNode="P_122F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_124F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10087_O" deadCode="false" sourceNode="P_22F10087" targetNode="P_123F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_124F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10087_I" deadCode="false" sourceNode="P_22F10087" targetNode="P_124F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_125F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10087_O" deadCode="false" sourceNode="P_22F10087" targetNode="P_125F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_125F10087"/>
	</edges>
	<edges id="P_22F10087P_23F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10087" targetNode="P_23F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10087_I" deadCode="false" sourceNode="P_30F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_128F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10087_O" deadCode="false" sourceNode="P_30F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_128F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10087_I" deadCode="false" sourceNode="P_30F10087" targetNode="P_28F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_130F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10087_O" deadCode="false" sourceNode="P_30F10087" targetNode="P_29F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_130F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10087_I" deadCode="false" sourceNode="P_30F10087" targetNode="P_128F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_132F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10087_O" deadCode="false" sourceNode="P_30F10087" targetNode="P_129F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_132F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10087_I" deadCode="false" sourceNode="P_30F10087" targetNode="P_130F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_133F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10087_O" deadCode="false" sourceNode="P_30F10087" targetNode="P_131F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_133F10087"/>
	</edges>
	<edges id="P_30F10087P_31F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10087" targetNode="P_31F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10087_I" deadCode="false" sourceNode="P_32F10087" targetNode="P_132F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_135F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10087_O" deadCode="false" sourceNode="P_32F10087" targetNode="P_133F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_135F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10087_I" deadCode="false" sourceNode="P_32F10087" targetNode="P_134F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_137F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10087_O" deadCode="false" sourceNode="P_32F10087" targetNode="P_135F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_137F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10087_I" deadCode="false" sourceNode="P_32F10087" targetNode="P_136F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_138F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10087_O" deadCode="false" sourceNode="P_32F10087" targetNode="P_137F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_138F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10087_I" deadCode="false" sourceNode="P_32F10087" targetNode="P_138F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_139F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10087_O" deadCode="false" sourceNode="P_32F10087" targetNode="P_139F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_139F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10087_I" deadCode="false" sourceNode="P_32F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_140F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10087_O" deadCode="false" sourceNode="P_32F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_140F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10087_I" deadCode="false" sourceNode="P_32F10087" targetNode="P_28F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_142F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10087_O" deadCode="false" sourceNode="P_32F10087" targetNode="P_29F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_142F10087"/>
	</edges>
	<edges id="P_32F10087P_33F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10087" targetNode="P_33F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10087_I" deadCode="false" sourceNode="P_34F10087" targetNode="P_140F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_144F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10087_O" deadCode="false" sourceNode="P_34F10087" targetNode="P_141F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_144F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10087_I" deadCode="false" sourceNode="P_34F10087" targetNode="P_136F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_145F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10087_O" deadCode="false" sourceNode="P_34F10087" targetNode="P_137F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_145F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10087_I" deadCode="false" sourceNode="P_34F10087" targetNode="P_138F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_146F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10087_O" deadCode="false" sourceNode="P_34F10087" targetNode="P_139F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_146F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10087_I" deadCode="false" sourceNode="P_34F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_147F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10087_O" deadCode="false" sourceNode="P_34F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_147F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10087_I" deadCode="false" sourceNode="P_34F10087" targetNode="P_28F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_149F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10087_O" deadCode="false" sourceNode="P_34F10087" targetNode="P_29F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_149F10087"/>
	</edges>
	<edges id="P_34F10087P_35F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10087" targetNode="P_35F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10087_I" deadCode="false" sourceNode="P_36F10087" targetNode="P_28F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_152F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10087_O" deadCode="false" sourceNode="P_36F10087" targetNode="P_29F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_152F10087"/>
	</edges>
	<edges id="P_36F10087P_37F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10087" targetNode="P_37F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10087_I" deadCode="false" sourceNode="P_142F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_154F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10087_O" deadCode="false" sourceNode="P_142F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_154F10087"/>
	</edges>
	<edges id="P_142F10087P_143F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_142F10087" targetNode="P_143F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10087_I" deadCode="false" sourceNode="P_38F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_158F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10087_O" deadCode="false" sourceNode="P_38F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_158F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10087_I" deadCode="false" sourceNode="P_38F10087" targetNode="P_28F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_160F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10087_O" deadCode="false" sourceNode="P_38F10087" targetNode="P_29F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_160F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10087_I" deadCode="false" sourceNode="P_38F10087" targetNode="P_128F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_162F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10087_O" deadCode="false" sourceNode="P_38F10087" targetNode="P_129F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_162F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10087_I" deadCode="false" sourceNode="P_38F10087" targetNode="P_130F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_163F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10087_O" deadCode="false" sourceNode="P_38F10087" targetNode="P_131F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_163F10087"/>
	</edges>
	<edges id="P_38F10087P_39F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10087" targetNode="P_39F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10087_I" deadCode="false" sourceNode="P_144F10087" targetNode="P_140F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_165F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10087_O" deadCode="false" sourceNode="P_144F10087" targetNode="P_141F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_165F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10087_I" deadCode="false" sourceNode="P_144F10087" targetNode="P_136F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_166F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10087_O" deadCode="false" sourceNode="P_144F10087" targetNode="P_137F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_166F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10087_I" deadCode="false" sourceNode="P_144F10087" targetNode="P_138F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_167F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10087_O" deadCode="false" sourceNode="P_144F10087" targetNode="P_139F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_167F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10087_I" deadCode="false" sourceNode="P_144F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_168F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10087_O" deadCode="false" sourceNode="P_144F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_168F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10087_I" deadCode="false" sourceNode="P_144F10087" targetNode="P_28F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_170F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10087_O" deadCode="false" sourceNode="P_144F10087" targetNode="P_29F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_170F10087"/>
	</edges>
	<edges id="P_144F10087P_145F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_144F10087" targetNode="P_145F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10087_I" deadCode="false" sourceNode="P_146F10087" targetNode="P_142F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_172F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10087_O" deadCode="false" sourceNode="P_146F10087" targetNode="P_143F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_172F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10087_I" deadCode="false" sourceNode="P_146F10087" targetNode="P_28F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_174F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10087_O" deadCode="false" sourceNode="P_146F10087" targetNode="P_29F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_174F10087"/>
	</edges>
	<edges id="P_146F10087P_147F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_146F10087" targetNode="P_147F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10087_I" deadCode="false" sourceNode="P_148F10087" targetNode="P_28F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_177F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10087_O" deadCode="false" sourceNode="P_148F10087" targetNode="P_29F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_177F10087"/>
	</edges>
	<edges id="P_148F10087P_149F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_148F10087" targetNode="P_149F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10087_I" deadCode="true" sourceNode="P_150F10087" targetNode="P_146F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_179F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10087_O" deadCode="true" sourceNode="P_150F10087" targetNode="P_147F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_179F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10087_I" deadCode="true" sourceNode="P_150F10087" targetNode="P_151F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_181F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10087_O" deadCode="true" sourceNode="P_150F10087" targetNode="P_152F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_181F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10087_I" deadCode="false" sourceNode="P_151F10087" targetNode="P_28F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_184F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10087_O" deadCode="false" sourceNode="P_151F10087" targetNode="P_29F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_184F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10087_I" deadCode="false" sourceNode="P_151F10087" targetNode="P_128F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_186F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10087_O" deadCode="false" sourceNode="P_151F10087" targetNode="P_129F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_186F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10087_I" deadCode="false" sourceNode="P_151F10087" targetNode="P_130F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_187F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10087_O" deadCode="false" sourceNode="P_151F10087" targetNode="P_131F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_187F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10087_I" deadCode="false" sourceNode="P_151F10087" targetNode="P_148F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_189F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10087_O" deadCode="false" sourceNode="P_151F10087" targetNode="P_149F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_189F10087"/>
	</edges>
	<edges id="P_151F10087P_152F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_151F10087" targetNode="P_152F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_193F10087_I" deadCode="false" sourceNode="P_154F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_193F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_193F10087_O" deadCode="false" sourceNode="P_154F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_193F10087"/>
	</edges>
	<edges id="P_154F10087P_155F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_154F10087" targetNode="P_155F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_197F10087_I" deadCode="false" sourceNode="P_44F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_197F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_197F10087_O" deadCode="false" sourceNode="P_44F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_197F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_199F10087_I" deadCode="false" sourceNode="P_44F10087" targetNode="P_28F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_199F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_199F10087_O" deadCode="false" sourceNode="P_44F10087" targetNode="P_29F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_199F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_201F10087_I" deadCode="false" sourceNode="P_44F10087" targetNode="P_128F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_201F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_201F10087_O" deadCode="false" sourceNode="P_44F10087" targetNode="P_129F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_201F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10087_I" deadCode="false" sourceNode="P_44F10087" targetNode="P_130F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_202F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10087_O" deadCode="false" sourceNode="P_44F10087" targetNode="P_131F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_202F10087"/>
	</edges>
	<edges id="P_44F10087P_45F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10087" targetNode="P_45F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_204F10087_I" deadCode="false" sourceNode="P_46F10087" targetNode="P_154F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_204F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_204F10087_O" deadCode="false" sourceNode="P_46F10087" targetNode="P_155F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_204F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_206F10087_I" deadCode="false" sourceNode="P_46F10087" targetNode="P_28F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_206F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_206F10087_O" deadCode="false" sourceNode="P_46F10087" targetNode="P_29F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_206F10087"/>
	</edges>
	<edges id="P_46F10087P_47F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10087" targetNode="P_47F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_209F10087_I" deadCode="false" sourceNode="P_48F10087" targetNode="P_28F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_209F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_209F10087_O" deadCode="false" sourceNode="P_48F10087" targetNode="P_29F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_209F10087"/>
	</edges>
	<edges id="P_48F10087P_49F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10087" targetNode="P_49F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10087_I" deadCode="false" sourceNode="P_50F10087" targetNode="P_46F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_211F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10087_O" deadCode="false" sourceNode="P_50F10087" targetNode="P_47F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_211F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10087_I" deadCode="false" sourceNode="P_50F10087" targetNode="P_52F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_213F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10087_O" deadCode="false" sourceNode="P_50F10087" targetNode="P_53F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_213F10087"/>
	</edges>
	<edges id="P_50F10087P_51F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10087" targetNode="P_51F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10087_I" deadCode="false" sourceNode="P_52F10087" targetNode="P_28F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_216F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10087_O" deadCode="false" sourceNode="P_52F10087" targetNode="P_29F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_216F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_218F10087_I" deadCode="false" sourceNode="P_52F10087" targetNode="P_128F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_218F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_218F10087_O" deadCode="false" sourceNode="P_52F10087" targetNode="P_129F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_218F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_219F10087_I" deadCode="false" sourceNode="P_52F10087" targetNode="P_130F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_219F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_219F10087_O" deadCode="false" sourceNode="P_52F10087" targetNode="P_131F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_219F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10087_I" deadCode="false" sourceNode="P_52F10087" targetNode="P_48F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_221F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10087_O" deadCode="false" sourceNode="P_52F10087" targetNode="P_49F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_221F10087"/>
	</edges>
	<edges id="P_52F10087P_53F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10087" targetNode="P_53F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_225F10087_I" deadCode="false" sourceNode="P_156F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_225F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_225F10087_O" deadCode="false" sourceNode="P_156F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_225F10087"/>
	</edges>
	<edges id="P_156F10087P_157F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_156F10087" targetNode="P_157F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_228F10087_I" deadCode="false" sourceNode="P_54F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_228F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_228F10087_O" deadCode="false" sourceNode="P_54F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_228F10087"/>
	</edges>
	<edges id="P_54F10087P_55F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10087" targetNode="P_55F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10087_I" deadCode="false" sourceNode="P_56F10087" targetNode="P_156F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_231F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10087_O" deadCode="false" sourceNode="P_56F10087" targetNode="P_157F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_231F10087"/>
	</edges>
	<edges id="P_56F10087P_57F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10087" targetNode="P_57F10087"/>
	<edges id="P_58F10087P_59F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10087" targetNode="P_59F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_236F10087_I" deadCode="false" sourceNode="P_60F10087" targetNode="P_56F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_236F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_236F10087_O" deadCode="false" sourceNode="P_60F10087" targetNode="P_57F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_236F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_238F10087_I" deadCode="false" sourceNode="P_60F10087" targetNode="P_62F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_238F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_238F10087_O" deadCode="false" sourceNode="P_60F10087" targetNode="P_63F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_238F10087"/>
	</edges>
	<edges id="P_60F10087P_61F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_60F10087" targetNode="P_61F10087"/>
	<edges id="P_62F10087P_63F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10087" targetNode="P_63F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10087_I" deadCode="false" sourceNode="P_158F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_242F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10087_O" deadCode="false" sourceNode="P_158F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_242F10087"/>
	</edges>
	<edges id="P_158F10087P_159F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_158F10087" targetNode="P_159F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_245F10087_I" deadCode="false" sourceNode="P_64F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_245F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_245F10087_O" deadCode="false" sourceNode="P_64F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_245F10087"/>
	</edges>
	<edges id="P_64F10087P_65F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10087" targetNode="P_65F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_248F10087_I" deadCode="false" sourceNode="P_66F10087" targetNode="P_158F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_248F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_248F10087_O" deadCode="false" sourceNode="P_66F10087" targetNode="P_159F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_248F10087"/>
	</edges>
	<edges id="P_66F10087P_67F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10087" targetNode="P_67F10087"/>
	<edges id="P_68F10087P_69F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10087" targetNode="P_69F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10087_I" deadCode="false" sourceNode="P_70F10087" targetNode="P_66F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_253F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10087_O" deadCode="false" sourceNode="P_70F10087" targetNode="P_67F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_253F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10087_I" deadCode="false" sourceNode="P_70F10087" targetNode="P_72F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_255F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10087_O" deadCode="false" sourceNode="P_70F10087" targetNode="P_73F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_255F10087"/>
	</edges>
	<edges id="P_70F10087P_71F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_70F10087" targetNode="P_71F10087"/>
	<edges id="P_72F10087P_73F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10087" targetNode="P_73F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10087_I" deadCode="false" sourceNode="P_160F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_259F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10087_O" deadCode="false" sourceNode="P_160F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_259F10087"/>
	</edges>
	<edges id="P_160F10087P_161F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_160F10087" targetNode="P_161F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10087_I" deadCode="false" sourceNode="P_74F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_262F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10087_O" deadCode="false" sourceNode="P_74F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_262F10087"/>
	</edges>
	<edges id="P_74F10087P_75F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10087" targetNode="P_75F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10087_I" deadCode="false" sourceNode="P_76F10087" targetNode="P_160F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_265F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10087_O" deadCode="false" sourceNode="P_76F10087" targetNode="P_161F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_265F10087"/>
	</edges>
	<edges id="P_76F10087P_77F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_76F10087" targetNode="P_77F10087"/>
	<edges id="P_78F10087P_79F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_78F10087" targetNode="P_79F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_270F10087_I" deadCode="false" sourceNode="P_80F10087" targetNode="P_76F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_270F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_270F10087_O" deadCode="false" sourceNode="P_80F10087" targetNode="P_77F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_270F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_272F10087_I" deadCode="false" sourceNode="P_80F10087" targetNode="P_82F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_272F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_272F10087_O" deadCode="false" sourceNode="P_80F10087" targetNode="P_83F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_272F10087"/>
	</edges>
	<edges id="P_80F10087P_81F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_80F10087" targetNode="P_81F10087"/>
	<edges id="P_82F10087P_83F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_82F10087" targetNode="P_83F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10087_I" deadCode="false" sourceNode="P_84F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_276F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10087_O" deadCode="false" sourceNode="P_84F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_276F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10087_I" deadCode="false" sourceNode="P_84F10087" targetNode="P_28F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_278F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10087_O" deadCode="false" sourceNode="P_84F10087" targetNode="P_29F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_278F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10087_I" deadCode="false" sourceNode="P_84F10087" targetNode="P_128F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_280F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10087_O" deadCode="false" sourceNode="P_84F10087" targetNode="P_129F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_280F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10087_I" deadCode="false" sourceNode="P_84F10087" targetNode="P_130F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_281F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10087_O" deadCode="false" sourceNode="P_84F10087" targetNode="P_131F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_281F10087"/>
	</edges>
	<edges id="P_84F10087P_85F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_84F10087" targetNode="P_85F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10087_I" deadCode="false" sourceNode="P_162F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_283F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10087_O" deadCode="false" sourceNode="P_162F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_283F10087"/>
	</edges>
	<edges id="P_162F10087P_163F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_162F10087" targetNode="P_163F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_287F10087_I" deadCode="false" sourceNode="P_86F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_287F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_287F10087_O" deadCode="false" sourceNode="P_86F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_287F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10087_I" deadCode="false" sourceNode="P_86F10087" targetNode="P_28F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_289F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10087_O" deadCode="false" sourceNode="P_86F10087" targetNode="P_29F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_289F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_291F10087_I" deadCode="false" sourceNode="P_86F10087" targetNode="P_128F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_291F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_291F10087_O" deadCode="false" sourceNode="P_86F10087" targetNode="P_129F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_291F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10087_I" deadCode="false" sourceNode="P_86F10087" targetNode="P_130F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_292F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10087_O" deadCode="false" sourceNode="P_86F10087" targetNode="P_131F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_292F10087"/>
	</edges>
	<edges id="P_86F10087P_87F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_86F10087" targetNode="P_87F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10087_I" deadCode="false" sourceNode="P_88F10087" targetNode="P_162F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_294F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10087_O" deadCode="false" sourceNode="P_88F10087" targetNode="P_163F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_294F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_296F10087_I" deadCode="false" sourceNode="P_88F10087" targetNode="P_28F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_296F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_296F10087_O" deadCode="false" sourceNode="P_88F10087" targetNode="P_29F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_296F10087"/>
	</edges>
	<edges id="P_88F10087P_89F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_88F10087" targetNode="P_89F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10087_I" deadCode="false" sourceNode="P_90F10087" targetNode="P_28F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_299F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10087_O" deadCode="false" sourceNode="P_90F10087" targetNode="P_29F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_299F10087"/>
	</edges>
	<edges id="P_90F10087P_91F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_90F10087" targetNode="P_91F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10087_I" deadCode="false" sourceNode="P_92F10087" targetNode="P_88F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_301F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10087_O" deadCode="false" sourceNode="P_92F10087" targetNode="P_89F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_301F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_303F10087_I" deadCode="false" sourceNode="P_92F10087" targetNode="P_94F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_303F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_303F10087_O" deadCode="false" sourceNode="P_92F10087" targetNode="P_95F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_303F10087"/>
	</edges>
	<edges id="P_92F10087P_93F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_92F10087" targetNode="P_93F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_306F10087_I" deadCode="false" sourceNode="P_94F10087" targetNode="P_28F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_306F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_306F10087_O" deadCode="false" sourceNode="P_94F10087" targetNode="P_29F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_306F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10087_I" deadCode="false" sourceNode="P_94F10087" targetNode="P_128F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_308F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10087_O" deadCode="false" sourceNode="P_94F10087" targetNode="P_129F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_308F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10087_I" deadCode="false" sourceNode="P_94F10087" targetNode="P_130F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_309F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10087_O" deadCode="false" sourceNode="P_94F10087" targetNode="P_131F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_309F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10087_I" deadCode="false" sourceNode="P_94F10087" targetNode="P_90F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_311F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10087_O" deadCode="false" sourceNode="P_94F10087" targetNode="P_91F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_311F10087"/>
	</edges>
	<edges id="P_94F10087P_95F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_94F10087" targetNode="P_95F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_315F10087_I" deadCode="false" sourceNode="P_164F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_315F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_315F10087_O" deadCode="false" sourceNode="P_164F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_315F10087"/>
	</edges>
	<edges id="P_164F10087P_165F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_164F10087" targetNode="P_165F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_318F10087_I" deadCode="false" sourceNode="P_96F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_318F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_318F10087_O" deadCode="false" sourceNode="P_96F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_318F10087"/>
	</edges>
	<edges id="P_96F10087P_97F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_96F10087" targetNode="P_97F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10087_I" deadCode="false" sourceNode="P_98F10087" targetNode="P_164F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_321F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10087_O" deadCode="false" sourceNode="P_98F10087" targetNode="P_165F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_321F10087"/>
	</edges>
	<edges id="P_98F10087P_99F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_98F10087" targetNode="P_99F10087"/>
	<edges id="P_100F10087P_101F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_100F10087" targetNode="P_101F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_326F10087_I" deadCode="false" sourceNode="P_102F10087" targetNode="P_98F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_326F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_326F10087_O" deadCode="false" sourceNode="P_102F10087" targetNode="P_99F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_326F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_328F10087_I" deadCode="false" sourceNode="P_102F10087" targetNode="P_104F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_328F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_328F10087_O" deadCode="false" sourceNode="P_102F10087" targetNode="P_105F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_328F10087"/>
	</edges>
	<edges id="P_102F10087P_103F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_102F10087" targetNode="P_103F10087"/>
	<edges id="P_104F10087P_105F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_104F10087" targetNode="P_105F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_332F10087_I" deadCode="false" sourceNode="P_166F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_332F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_332F10087_O" deadCode="false" sourceNode="P_166F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_332F10087"/>
	</edges>
	<edges id="P_166F10087P_167F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_166F10087" targetNode="P_167F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_335F10087_I" deadCode="false" sourceNode="P_106F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_335F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_335F10087_O" deadCode="false" sourceNode="P_106F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_335F10087"/>
	</edges>
	<edges id="P_106F10087P_107F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_106F10087" targetNode="P_107F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10087_I" deadCode="false" sourceNode="P_108F10087" targetNode="P_166F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_338F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10087_O" deadCode="false" sourceNode="P_108F10087" targetNode="P_167F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_338F10087"/>
	</edges>
	<edges id="P_108F10087P_109F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_108F10087" targetNode="P_109F10087"/>
	<edges id="P_110F10087P_111F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_110F10087" targetNode="P_111F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_343F10087_I" deadCode="false" sourceNode="P_112F10087" targetNode="P_108F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_343F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_343F10087_O" deadCode="false" sourceNode="P_112F10087" targetNode="P_109F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_343F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_345F10087_I" deadCode="false" sourceNode="P_112F10087" targetNode="P_114F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_345F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_345F10087_O" deadCode="false" sourceNode="P_112F10087" targetNode="P_115F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_345F10087"/>
	</edges>
	<edges id="P_112F10087P_113F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_112F10087" targetNode="P_113F10087"/>
	<edges id="P_114F10087P_115F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_114F10087" targetNode="P_115F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_349F10087_I" deadCode="false" sourceNode="P_168F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_349F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_349F10087_O" deadCode="false" sourceNode="P_168F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_349F10087"/>
	</edges>
	<edges id="P_168F10087P_169F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_168F10087" targetNode="P_169F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_352F10087_I" deadCode="false" sourceNode="P_116F10087" targetNode="P_126F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_352F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_352F10087_O" deadCode="false" sourceNode="P_116F10087" targetNode="P_127F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_352F10087"/>
	</edges>
	<edges id="P_116F10087P_117F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_116F10087" targetNode="P_117F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_355F10087_I" deadCode="false" sourceNode="P_118F10087" targetNode="P_168F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_355F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_355F10087_O" deadCode="false" sourceNode="P_118F10087" targetNode="P_169F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_355F10087"/>
	</edges>
	<edges id="P_118F10087P_119F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_118F10087" targetNode="P_119F10087"/>
	<edges id="P_120F10087P_121F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_120F10087" targetNode="P_121F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_360F10087_I" deadCode="false" sourceNode="P_122F10087" targetNode="P_118F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_360F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_360F10087_O" deadCode="false" sourceNode="P_122F10087" targetNode="P_119F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_360F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_362F10087_I" deadCode="false" sourceNode="P_122F10087" targetNode="P_124F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_362F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_362F10087_O" deadCode="false" sourceNode="P_122F10087" targetNode="P_125F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_362F10087"/>
	</edges>
	<edges id="P_122F10087P_123F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_122F10087" targetNode="P_123F10087"/>
	<edges id="P_124F10087P_125F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_124F10087" targetNode="P_125F10087"/>
	<edges id="P_128F10087P_129F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_128F10087" targetNode="P_129F10087"/>
	<edges id="P_134F10087P_135F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_134F10087" targetNode="P_135F10087"/>
	<edges id="P_140F10087P_141F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_140F10087" targetNode="P_141F10087"/>
	<edges id="P_136F10087P_137F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_136F10087" targetNode="P_137F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_558F10087_I" deadCode="false" sourceNode="P_132F10087" targetNode="P_28F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_558F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_558F10087_O" deadCode="false" sourceNode="P_132F10087" targetNode="P_29F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_558F10087"/>
	</edges>
	<edges id="P_132F10087P_133F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_132F10087" targetNode="P_133F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_562F10087_I" deadCode="false" sourceNode="P_40F10087" targetNode="P_146F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_562F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_562F10087_O" deadCode="false" sourceNode="P_40F10087" targetNode="P_147F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_562F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_564F10087_I" deadCode="false" sourceNode="P_40F10087" targetNode="P_151F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_563F10087"/>
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_564F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_564F10087_O" deadCode="false" sourceNode="P_40F10087" targetNode="P_152F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_563F10087"/>
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_564F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_568F10087_I" deadCode="false" sourceNode="P_40F10087" targetNode="P_144F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_563F10087"/>
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_568F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_568F10087_O" deadCode="false" sourceNode="P_40F10087" targetNode="P_145F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_563F10087"/>
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_568F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_576F10087_I" deadCode="false" sourceNode="P_40F10087" targetNode="P_32F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_563F10087"/>
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_576F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_576F10087_O" deadCode="false" sourceNode="P_40F10087" targetNode="P_33F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_563F10087"/>
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_576F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_579F10087_I" deadCode="false" sourceNode="P_40F10087" targetNode="P_170F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_579F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_579F10087_O" deadCode="false" sourceNode="P_40F10087" targetNode="P_171F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_579F10087"/>
	</edges>
	<edges id="P_40F10087P_41F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10087" targetNode="P_41F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_584F10087_I" deadCode="false" sourceNode="P_42F10087" targetNode="P_170F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_584F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_584F10087_O" deadCode="false" sourceNode="P_42F10087" targetNode="P_171F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_584F10087"/>
	</edges>
	<edges id="P_42F10087P_43F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10087" targetNode="P_43F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_594F10087_I" deadCode="false" sourceNode="P_170F10087" targetNode="P_32F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_594F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_594F10087_O" deadCode="false" sourceNode="P_170F10087" targetNode="P_33F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_594F10087"/>
	</edges>
	<edges id="P_170F10087P_171F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_170F10087" targetNode="P_171F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_597F10087_I" deadCode="false" sourceNode="P_138F10087" targetNode="P_172F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_597F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_597F10087_O" deadCode="false" sourceNode="P_138F10087" targetNode="P_173F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_597F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_600F10087_I" deadCode="false" sourceNode="P_138F10087" targetNode="P_172F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_600F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_600F10087_O" deadCode="false" sourceNode="P_138F10087" targetNode="P_173F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_600F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_604F10087_I" deadCode="false" sourceNode="P_138F10087" targetNode="P_172F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_604F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_604F10087_O" deadCode="false" sourceNode="P_138F10087" targetNode="P_173F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_604F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_608F10087_I" deadCode="false" sourceNode="P_138F10087" targetNode="P_172F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_608F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_608F10087_O" deadCode="false" sourceNode="P_138F10087" targetNode="P_173F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_608F10087"/>
	</edges>
	<edges id="P_138F10087P_139F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_138F10087" targetNode="P_139F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_612F10087_I" deadCode="false" sourceNode="P_130F10087" targetNode="P_174F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_612F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_612F10087_O" deadCode="false" sourceNode="P_130F10087" targetNode="P_175F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_612F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_615F10087_I" deadCode="false" sourceNode="P_130F10087" targetNode="P_174F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_615F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_615F10087_O" deadCode="false" sourceNode="P_130F10087" targetNode="P_175F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_615F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_619F10087_I" deadCode="false" sourceNode="P_130F10087" targetNode="P_174F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_619F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_619F10087_O" deadCode="false" sourceNode="P_130F10087" targetNode="P_175F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_619F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_623F10087_I" deadCode="false" sourceNode="P_130F10087" targetNode="P_174F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_623F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_623F10087_O" deadCode="false" sourceNode="P_130F10087" targetNode="P_175F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_623F10087"/>
	</edges>
	<edges id="P_130F10087P_131F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_130F10087" targetNode="P_131F10087"/>
	<edges id="P_126F10087P_127F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_126F10087" targetNode="P_127F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_628F10087_I" deadCode="false" sourceNode="P_26F10087" targetNode="P_176F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_628F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_628F10087_O" deadCode="false" sourceNode="P_26F10087" targetNode="P_177F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_628F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_630F10087_I" deadCode="false" sourceNode="P_26F10087" targetNode="P_178F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_630F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_630F10087_O" deadCode="false" sourceNode="P_26F10087" targetNode="P_179F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_630F10087"/>
	</edges>
	<edges id="P_26F10087P_27F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10087" targetNode="P_27F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_635F10087_I" deadCode="false" sourceNode="P_176F10087" targetNode="P_172F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_635F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_635F10087_O" deadCode="false" sourceNode="P_176F10087" targetNode="P_173F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_635F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_640F10087_I" deadCode="false" sourceNode="P_176F10087" targetNode="P_172F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_640F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_640F10087_O" deadCode="false" sourceNode="P_176F10087" targetNode="P_173F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_640F10087"/>
	</edges>
	<edges id="P_176F10087P_177F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_176F10087" targetNode="P_177F10087"/>
	<edges id="P_178F10087P_179F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_178F10087" targetNode="P_179F10087"/>
	<edges id="P_172F10087P_173F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_172F10087" targetNode="P_173F10087"/>
	<edges xsi:type="cbl:PerformEdge" id="S_669F10087_I" deadCode="false" sourceNode="P_174F10087" targetNode="P_182F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_669F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_669F10087_O" deadCode="false" sourceNode="P_174F10087" targetNode="P_183F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_669F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_670F10087_I" deadCode="false" sourceNode="P_174F10087" targetNode="P_184F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_670F10087"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_670F10087_O" deadCode="false" sourceNode="P_174F10087" targetNode="P_185F10087">
		<representations href="../../../cobol/IDBSRST0.cbl.cobModel#S_670F10087"/>
	</edges>
	<edges id="P_174F10087P_175F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_174F10087" targetNode="P_175F10087"/>
	<edges id="P_182F10087P_183F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_182F10087" targetNode="P_183F10087"/>
	<edges id="P_184F10087P_185F10087" xsi:type="cbl:FallThroughEdge" sourceNode="P_184F10087" targetNode="P_185F10087"/>
	<edges xsi:type="cbl:DataEdge" id="S_129F10087_POS1" deadCode="false" targetNode="P_30F10087" sourceNode="DB2_RIS_DI_TRCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_129F10087"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_141F10087_POS1" deadCode="false" sourceNode="P_32F10087" targetNode="DB2_RIS_DI_TRCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_141F10087"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_148F10087_POS1" deadCode="false" sourceNode="P_34F10087" targetNode="DB2_RIS_DI_TRCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_148F10087"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_151F10087_POS1" deadCode="false" sourceNode="P_36F10087" targetNode="DB2_RIS_DI_TRCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_151F10087"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_159F10087_POS1" deadCode="false" targetNode="P_38F10087" sourceNode="DB2_RIS_DI_TRCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_159F10087"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_169F10087_POS1" deadCode="false" sourceNode="P_144F10087" targetNode="DB2_RIS_DI_TRCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_169F10087"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_173F10087_POS1" deadCode="false" targetNode="P_146F10087" sourceNode="DB2_RIS_DI_TRCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_173F10087"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_176F10087_POS1" deadCode="false" targetNode="P_148F10087" sourceNode="DB2_RIS_DI_TRCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_176F10087"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_183F10087_POS1" deadCode="false" targetNode="P_151F10087" sourceNode="DB2_RIS_DI_TRCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_183F10087"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_198F10087_POS1" deadCode="false" targetNode="P_44F10087" sourceNode="DB2_RIS_DI_TRCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_198F10087"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_205F10087_POS1" deadCode="false" targetNode="P_46F10087" sourceNode="DB2_RIS_DI_TRCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_205F10087"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_208F10087_POS1" deadCode="false" targetNode="P_48F10087" sourceNode="DB2_RIS_DI_TRCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_208F10087"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_215F10087_POS1" deadCode="false" targetNode="P_52F10087" sourceNode="DB2_RIS_DI_TRCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_215F10087"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_277F10087_POS1" deadCode="false" targetNode="P_84F10087" sourceNode="DB2_RIS_DI_TRCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_277F10087"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_288F10087_POS1" deadCode="false" targetNode="P_86F10087" sourceNode="DB2_RIS_DI_TRCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_288F10087"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_295F10087_POS1" deadCode="false" targetNode="P_88F10087" sourceNode="DB2_RIS_DI_TRCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_295F10087"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_298F10087_POS1" deadCode="false" targetNode="P_90F10087" sourceNode="DB2_RIS_DI_TRCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_298F10087"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_305F10087_POS1" deadCode="false" targetNode="P_94F10087" sourceNode="DB2_RIS_DI_TRCH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_305F10087"></representations>
	</edges>
</Package>

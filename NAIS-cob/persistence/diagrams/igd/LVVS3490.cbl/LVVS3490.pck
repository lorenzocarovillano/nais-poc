<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS3490" cbl:id="LVVS3490" xsi:id="LVVS3490" packageRef="LVVS3490.igd#LVVS3490" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS3490_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10394" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS3490.cbl.cobModel#SC_1F10394"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10394" deadCode="false" name="PROGRAM_LVVS3490_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS3490.cbl.cobModel#P_1F10394"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10394" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS3490.cbl.cobModel#P_2F10394"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10394" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS3490.cbl.cobModel#P_3F10394"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10394" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS3490.cbl.cobModel#P_4F10394"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10394" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS3490.cbl.cobModel#P_5F10394"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10394" deadCode="false" name="LETTURA-D-CRIST">
				<representations href="../../../cobol/LVVS3490.cbl.cobModel#P_10F10394"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10394" deadCode="false" name="LETTURA-D-CRIST-EX">
				<representations href="../../../cobol/LVVS3490.cbl.cobModel#P_11F10394"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10394" deadCode="true" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS3490.cbl.cobModel#P_8F10394"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10394" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS3490.cbl.cobModel#P_9F10394"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10394" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS3490.cbl.cobModel#P_6F10394"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10394" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS3490.cbl.cobModel#P_7F10394"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSH580" name="LDBSH580">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10276"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10394P_1F10394" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10394" targetNode="P_1F10394"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10394_I" deadCode="false" sourceNode="P_1F10394" targetNode="P_2F10394">
		<representations href="../../../cobol/LVVS3490.cbl.cobModel#S_1F10394"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10394_O" deadCode="false" sourceNode="P_1F10394" targetNode="P_3F10394">
		<representations href="../../../cobol/LVVS3490.cbl.cobModel#S_1F10394"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10394_I" deadCode="false" sourceNode="P_1F10394" targetNode="P_4F10394">
		<representations href="../../../cobol/LVVS3490.cbl.cobModel#S_2F10394"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10394_O" deadCode="false" sourceNode="P_1F10394" targetNode="P_5F10394">
		<representations href="../../../cobol/LVVS3490.cbl.cobModel#S_2F10394"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10394_I" deadCode="false" sourceNode="P_1F10394" targetNode="P_6F10394">
		<representations href="../../../cobol/LVVS3490.cbl.cobModel#S_3F10394"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10394_O" deadCode="false" sourceNode="P_1F10394" targetNode="P_7F10394">
		<representations href="../../../cobol/LVVS3490.cbl.cobModel#S_3F10394"/>
	</edges>
	<edges id="P_2F10394P_3F10394" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10394" targetNode="P_3F10394"/>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10394_I" deadCode="true" sourceNode="P_4F10394" targetNode="P_8F10394">
		<representations href="../../../cobol/LVVS3490.cbl.cobModel#S_9F10394"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10394_O" deadCode="true" sourceNode="P_4F10394" targetNode="P_9F10394">
		<representations href="../../../cobol/LVVS3490.cbl.cobModel#S_9F10394"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10394_I" deadCode="false" sourceNode="P_4F10394" targetNode="P_10F10394">
		<representations href="../../../cobol/LVVS3490.cbl.cobModel#S_10F10394"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10394_O" deadCode="false" sourceNode="P_4F10394" targetNode="P_11F10394">
		<representations href="../../../cobol/LVVS3490.cbl.cobModel#S_10F10394"/>
	</edges>
	<edges id="P_4F10394P_5F10394" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10394" targetNode="P_5F10394"/>
	<edges id="P_10F10394P_11F10394" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10394" targetNode="P_11F10394"/>
	<edges id="P_8F10394P_9F10394" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10394" targetNode="P_9F10394"/>
	<edges xsi:type="cbl:CallEdge" id="S_24F10394" deadCode="false" name="Dynamic WK-PGM-CALL" sourceNode="P_10F10394" targetNode="LDBSH580">
		<representations href="../../../cobol/../importantStmts.cobModel#S_24F10394"></representations>
	</edges>
</Package>

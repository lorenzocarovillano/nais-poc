<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IDBSPOG0" cbl:id="IDBSPOG0" xsi:id="IDBSPOG0" packageRef="IDBSPOG0.igd#IDBSPOG0" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IDBSPOG0_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10077" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#SC_1F10077"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10077" deadCode="false" name="PROGRAM_IDBSPOG0_FIRST_SENTENCES">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_1F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10077" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_2F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10077" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_3F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10077" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_28F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10077" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_29F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10077" deadCode="false" name="A200-ELABORA-PK">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_24F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10077" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_25F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10077" deadCode="false" name="A300-ELABORA-ID-EFF">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_4F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10077" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_5F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10077" deadCode="false" name="A400-ELABORA-IDP-EFF">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_6F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10077" deadCode="false" name="A400-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_7F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10077" deadCode="false" name="A500-ELABORA-IBO">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_8F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10077" deadCode="false" name="A500-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_9F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10077" deadCode="false" name="A600-ELABORA-IBS">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_10F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10077" deadCode="false" name="A600-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_11F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10077" deadCode="false" name="A700-ELABORA-IDO">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_12F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10077" deadCode="false" name="A700-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_13F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10077" deadCode="false" name="B300-ELABORA-ID-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_14F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10077" deadCode="false" name="B300-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_15F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10077" deadCode="false" name="B400-ELABORA-IDP-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_16F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10077" deadCode="false" name="B400-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_17F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10077" deadCode="false" name="B500-ELABORA-IBO-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_18F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10077" deadCode="false" name="B500-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_19F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10077" deadCode="false" name="B600-ELABORA-IBS-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_20F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10077" deadCode="false" name="B600-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_21F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10077" deadCode="false" name="B700-ELABORA-IDO-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_22F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10077" deadCode="false" name="B700-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_23F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10077" deadCode="false" name="A210-SELECT-PK">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_30F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10077" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_31F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10077" deadCode="false" name="A220-INSERT-PK">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_32F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10077" deadCode="false" name="A220-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_33F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10077" deadCode="false" name="A230-UPDATE-PK">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_34F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10077" deadCode="false" name="A230-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_35F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10077" deadCode="false" name="A240-DELETE-PK">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_36F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10077" deadCode="false" name="A240-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_37F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_140F10077" deadCode="false" name="A305-DECLARE-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_140F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_141F10077" deadCode="false" name="A305-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_141F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10077" deadCode="false" name="A310-SELECT-ID-EFF">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_38F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10077" deadCode="false" name="A310-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_39F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_142F10077" deadCode="false" name="A330-UPDATE-ID-EFF">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_142F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_143F10077" deadCode="false" name="A330-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_143F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_144F10077" deadCode="false" name="A360-OPEN-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_144F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_145F10077" deadCode="false" name="A360-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_145F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_146F10077" deadCode="false" name="A370-CLOSE-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_146F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_147F10077" deadCode="false" name="A370-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_147F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_148F10077" deadCode="true" name="A380-FETCH-FIRST-ID-EFF">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_148F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_151F10077" deadCode="true" name="A380-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_151F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_149F10077" deadCode="false" name="A390-FETCH-NEXT-ID-EFF">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_149F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_150F10077" deadCode="false" name="A390-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_150F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_152F10077" deadCode="false" name="A405-DECLARE-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_152F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_153F10077" deadCode="false" name="A405-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_153F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10077" deadCode="false" name="A410-SELECT-IDP-EFF">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_42F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10077" deadCode="false" name="A410-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_43F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10077" deadCode="false" name="A460-OPEN-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_44F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10077" deadCode="false" name="A460-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_45F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10077" deadCode="false" name="A470-CLOSE-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_46F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10077" deadCode="false" name="A470-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_47F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10077" deadCode="false" name="A480-FETCH-FIRST-IDP-EFF">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_48F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10077" deadCode="false" name="A480-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_49F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10077" deadCode="false" name="A490-FETCH-NEXT-IDP-EFF">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_50F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10077" deadCode="false" name="A490-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_51F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_154F10077" deadCode="false" name="A505-DECLARE-CURSOR-IBO">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_154F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_155F10077" deadCode="false" name="A505-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_155F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10077" deadCode="false" name="A510-SELECT-IBO">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_52F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10077" deadCode="false" name="A510-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_53F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10077" deadCode="false" name="A560-OPEN-CURSOR-IBO">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_54F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10077" deadCode="false" name="A560-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_55F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10077" deadCode="false" name="A570-CLOSE-CURSOR-IBO">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_56F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10077" deadCode="false" name="A570-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_57F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10077" deadCode="false" name="A580-FETCH-FIRST-IBO">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_58F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10077" deadCode="false" name="A580-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_59F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10077" deadCode="false" name="A590-FETCH-NEXT-IBO">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_60F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10077" deadCode="false" name="A590-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_61F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_156F10077" deadCode="false" name="A605-DECLARE-CURSOR-IBS">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_156F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_157F10077" deadCode="false" name="A605-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_157F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10077" deadCode="false" name="A610-SELECT-IBS">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_62F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10077" deadCode="false" name="A610-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_63F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10077" deadCode="false" name="A660-OPEN-CURSOR-IBS">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_64F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10077" deadCode="false" name="A660-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_65F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10077" deadCode="false" name="A670-CLOSE-CURSOR-IBS">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_66F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10077" deadCode="false" name="A670-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_67F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10077" deadCode="false" name="A680-FETCH-FIRST-IBS">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_68F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10077" deadCode="false" name="A680-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_69F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10077" deadCode="false" name="A690-FETCH-NEXT-IBS">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_70F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10077" deadCode="false" name="A690-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_71F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_158F10077" deadCode="false" name="A705-DECLARE-CURSOR-IDO">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_158F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_159F10077" deadCode="false" name="A705-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_159F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10077" deadCode="false" name="A710-SELECT-IDO">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_72F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10077" deadCode="false" name="A710-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_73F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10077" deadCode="false" name="A760-OPEN-CURSOR-IDO">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_74F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10077" deadCode="false" name="A760-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_75F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10077" deadCode="false" name="A770-CLOSE-CURSOR-IDO">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_76F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10077" deadCode="false" name="A770-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_77F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_78F10077" deadCode="false" name="A780-FETCH-FIRST-IDO">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_78F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_79F10077" deadCode="false" name="A780-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_79F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_80F10077" deadCode="false" name="A790-FETCH-NEXT-IDO">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_80F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_81F10077" deadCode="false" name="A790-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_81F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_82F10077" deadCode="false" name="B310-SELECT-ID-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_82F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_83F10077" deadCode="false" name="B310-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_83F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_160F10077" deadCode="false" name="B405-DECLARE-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_160F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_161F10077" deadCode="false" name="B405-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_161F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_84F10077" deadCode="false" name="B410-SELECT-IDP-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_84F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_85F10077" deadCode="false" name="B410-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_85F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_86F10077" deadCode="false" name="B460-OPEN-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_86F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_87F10077" deadCode="false" name="B460-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_87F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_88F10077" deadCode="false" name="B470-CLOSE-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_88F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_89F10077" deadCode="false" name="B470-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_89F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_90F10077" deadCode="false" name="B480-FETCH-FIRST-IDP-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_90F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_91F10077" deadCode="false" name="B480-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_91F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_92F10077" deadCode="false" name="B490-FETCH-NEXT-IDP-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_92F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_93F10077" deadCode="false" name="B490-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_93F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_162F10077" deadCode="false" name="B505-DECLARE-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_162F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_163F10077" deadCode="false" name="B505-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_163F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_94F10077" deadCode="false" name="B510-SELECT-IBO-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_94F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_95F10077" deadCode="false" name="B510-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_95F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_96F10077" deadCode="false" name="B560-OPEN-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_96F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_97F10077" deadCode="false" name="B560-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_97F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_98F10077" deadCode="false" name="B570-CLOSE-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_98F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_99F10077" deadCode="false" name="B570-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_99F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_100F10077" deadCode="false" name="B580-FETCH-FIRST-IBO-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_100F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_101F10077" deadCode="false" name="B580-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_101F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_102F10077" deadCode="false" name="B590-FETCH-NEXT-IBO-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_102F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_103F10077" deadCode="false" name="B590-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_103F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_164F10077" deadCode="false" name="B605-DECLARE-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_164F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_165F10077" deadCode="false" name="B605-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_165F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_104F10077" deadCode="false" name="B610-SELECT-IBS-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_104F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_105F10077" deadCode="false" name="B610-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_105F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_106F10077" deadCode="false" name="B660-OPEN-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_106F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_107F10077" deadCode="false" name="B660-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_107F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_108F10077" deadCode="false" name="B670-CLOSE-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_108F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_109F10077" deadCode="false" name="B670-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_109F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_110F10077" deadCode="false" name="B680-FETCH-FIRST-IBS-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_110F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_111F10077" deadCode="false" name="B680-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_111F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_112F10077" deadCode="false" name="B690-FETCH-NEXT-IBS-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_112F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_113F10077" deadCode="false" name="B690-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_113F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_166F10077" deadCode="false" name="B705-DECLARE-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_166F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_167F10077" deadCode="false" name="B705-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_167F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_114F10077" deadCode="false" name="B710-SELECT-IDO-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_114F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_115F10077" deadCode="false" name="B710-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_115F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_116F10077" deadCode="false" name="B760-OPEN-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_116F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_117F10077" deadCode="false" name="B760-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_117F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_118F10077" deadCode="false" name="B770-CLOSE-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_118F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_119F10077" deadCode="false" name="B770-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_119F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_120F10077" deadCode="false" name="B780-FETCH-FIRST-IDO-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_120F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_121F10077" deadCode="false" name="B780-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_121F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_122F10077" deadCode="false" name="B790-FETCH-NEXT-IDO-CPZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_122F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_123F10077" deadCode="false" name="B790-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_123F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_126F10077" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_126F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_127F10077" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_127F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_132F10077" deadCode="false" name="Z150-VALORIZZA-DATA-SERVICES-I">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_132F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_133F10077" deadCode="false" name="Z150-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_133F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_138F10077" deadCode="false" name="Z160-VALORIZZA-DATA-SERVICES-U">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_138F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_139F10077" deadCode="false" name="Z160-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_139F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_134F10077" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_134F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_135F10077" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_135F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_130F10077" deadCode="false" name="Z400-SEQ-RIGA">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_130F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_131F10077" deadCode="false" name="Z400-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_131F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10077" deadCode="false" name="Z500-AGGIORNAMENTO-STORICO">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_40F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10077" deadCode="false" name="Z500-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_41F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_168F10077" deadCode="false" name="Z600-INSERT-NUOVA-RIGA-STORICA">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_168F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_169F10077" deadCode="false" name="Z600-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_169F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_136F10077" deadCode="false" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_136F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_137F10077" deadCode="false" name="Z900-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_137F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_128F10077" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_128F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_129F10077" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_129F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_124F10077" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_124F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_125F10077" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_125F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10077" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_26F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10077" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_27F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_174F10077" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_174F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_175F10077" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_175F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_176F10077" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_176F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_177F10077" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_177F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_170F10077" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_170F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_171F10077" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_171F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_178F10077" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_178F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_179F10077" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_179F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_172F10077" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_172F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_173F10077" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_173F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_180F10077" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_180F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_181F10077" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_181F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_182F10077" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_182F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_183F10077" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_183F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_184F10077" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_184F10077"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_185F10077" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#P_185F10077"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_PARAM_OGG" name="PARAM_OGG">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_PARAM_OGG"/>
		</children>
	</packageNode>
	<edges id="SC_1F10077P_1F10077" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10077" targetNode="P_1F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10077_I" deadCode="false" sourceNode="P_1F10077" targetNode="P_2F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_1F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10077_O" deadCode="false" sourceNode="P_1F10077" targetNode="P_3F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_1F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10077_I" deadCode="false" sourceNode="P_1F10077" targetNode="P_4F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_5F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10077_O" deadCode="false" sourceNode="P_1F10077" targetNode="P_5F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_5F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10077_I" deadCode="false" sourceNode="P_1F10077" targetNode="P_6F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_6F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10077_O" deadCode="false" sourceNode="P_1F10077" targetNode="P_7F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_6F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10077_I" deadCode="false" sourceNode="P_1F10077" targetNode="P_8F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_7F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10077_O" deadCode="false" sourceNode="P_1F10077" targetNode="P_9F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_7F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10077_I" deadCode="false" sourceNode="P_1F10077" targetNode="P_10F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_8F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10077_O" deadCode="false" sourceNode="P_1F10077" targetNode="P_11F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_8F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10077_I" deadCode="false" sourceNode="P_1F10077" targetNode="P_12F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_9F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10077_O" deadCode="false" sourceNode="P_1F10077" targetNode="P_13F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_9F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10077_I" deadCode="false" sourceNode="P_1F10077" targetNode="P_14F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_13F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10077_O" deadCode="false" sourceNode="P_1F10077" targetNode="P_15F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_13F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10077_I" deadCode="false" sourceNode="P_1F10077" targetNode="P_16F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_14F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10077_O" deadCode="false" sourceNode="P_1F10077" targetNode="P_17F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_14F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10077_I" deadCode="false" sourceNode="P_1F10077" targetNode="P_18F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_15F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10077_O" deadCode="false" sourceNode="P_1F10077" targetNode="P_19F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_15F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10077_I" deadCode="false" sourceNode="P_1F10077" targetNode="P_20F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_16F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10077_O" deadCode="false" sourceNode="P_1F10077" targetNode="P_21F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_16F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10077_I" deadCode="false" sourceNode="P_1F10077" targetNode="P_22F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_17F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10077_O" deadCode="false" sourceNode="P_1F10077" targetNode="P_23F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_17F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10077_I" deadCode="false" sourceNode="P_1F10077" targetNode="P_24F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_21F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10077_O" deadCode="false" sourceNode="P_1F10077" targetNode="P_25F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_21F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10077_I" deadCode="false" sourceNode="P_1F10077" targetNode="P_8F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_22F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10077_O" deadCode="false" sourceNode="P_1F10077" targetNode="P_9F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_22F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10077_I" deadCode="false" sourceNode="P_1F10077" targetNode="P_10F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_23F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10077_O" deadCode="false" sourceNode="P_1F10077" targetNode="P_11F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_23F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10077_I" deadCode="false" sourceNode="P_1F10077" targetNode="P_12F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_24F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10077_O" deadCode="false" sourceNode="P_1F10077" targetNode="P_13F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_24F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10077_I" deadCode="false" sourceNode="P_2F10077" targetNode="P_26F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_33F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10077_O" deadCode="false" sourceNode="P_2F10077" targetNode="P_27F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_33F10077"/>
	</edges>
	<edges id="P_2F10077P_3F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10077" targetNode="P_3F10077"/>
	<edges id="P_28F10077P_29F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10077" targetNode="P_29F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10077_I" deadCode="false" sourceNode="P_24F10077" targetNode="P_30F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_46F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10077_O" deadCode="false" sourceNode="P_24F10077" targetNode="P_31F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_46F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10077_I" deadCode="false" sourceNode="P_24F10077" targetNode="P_32F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_47F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10077_O" deadCode="false" sourceNode="P_24F10077" targetNode="P_33F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_47F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10077_I" deadCode="false" sourceNode="P_24F10077" targetNode="P_34F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_48F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10077_O" deadCode="false" sourceNode="P_24F10077" targetNode="P_35F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_48F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10077_I" deadCode="false" sourceNode="P_24F10077" targetNode="P_36F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_49F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10077_O" deadCode="false" sourceNode="P_24F10077" targetNode="P_37F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_49F10077"/>
	</edges>
	<edges id="P_24F10077P_25F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10077" targetNode="P_25F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10077_I" deadCode="false" sourceNode="P_4F10077" targetNode="P_38F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_53F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10077_O" deadCode="false" sourceNode="P_4F10077" targetNode="P_39F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_53F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10077_I" deadCode="false" sourceNode="P_4F10077" targetNode="P_40F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_54F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10077_O" deadCode="false" sourceNode="P_4F10077" targetNode="P_41F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_54F10077"/>
	</edges>
	<edges id="P_4F10077P_5F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10077" targetNode="P_5F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10077_I" deadCode="false" sourceNode="P_6F10077" targetNode="P_42F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_58F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10077_O" deadCode="false" sourceNode="P_6F10077" targetNode="P_43F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_58F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10077_I" deadCode="false" sourceNode="P_6F10077" targetNode="P_44F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_59F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10077_O" deadCode="false" sourceNode="P_6F10077" targetNode="P_45F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_59F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10077_I" deadCode="false" sourceNode="P_6F10077" targetNode="P_46F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_60F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10077_O" deadCode="false" sourceNode="P_6F10077" targetNode="P_47F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_60F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10077_I" deadCode="false" sourceNode="P_6F10077" targetNode="P_48F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_61F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10077_O" deadCode="false" sourceNode="P_6F10077" targetNode="P_49F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_61F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10077_I" deadCode="false" sourceNode="P_6F10077" targetNode="P_50F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_62F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10077_O" deadCode="false" sourceNode="P_6F10077" targetNode="P_51F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_62F10077"/>
	</edges>
	<edges id="P_6F10077P_7F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10077" targetNode="P_7F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10077_I" deadCode="false" sourceNode="P_8F10077" targetNode="P_52F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_66F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10077_O" deadCode="false" sourceNode="P_8F10077" targetNode="P_53F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_66F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10077_I" deadCode="false" sourceNode="P_8F10077" targetNode="P_54F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_67F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10077_O" deadCode="false" sourceNode="P_8F10077" targetNode="P_55F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_67F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10077_I" deadCode="false" sourceNode="P_8F10077" targetNode="P_56F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_68F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10077_O" deadCode="false" sourceNode="P_8F10077" targetNode="P_57F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_68F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10077_I" deadCode="false" sourceNode="P_8F10077" targetNode="P_58F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_69F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10077_O" deadCode="false" sourceNode="P_8F10077" targetNode="P_59F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_69F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10077_I" deadCode="false" sourceNode="P_8F10077" targetNode="P_60F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_70F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10077_O" deadCode="false" sourceNode="P_8F10077" targetNode="P_61F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_70F10077"/>
	</edges>
	<edges id="P_8F10077P_9F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10077" targetNode="P_9F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10077_I" deadCode="false" sourceNode="P_10F10077" targetNode="P_62F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_74F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10077_O" deadCode="false" sourceNode="P_10F10077" targetNode="P_63F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_74F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10077_I" deadCode="false" sourceNode="P_10F10077" targetNode="P_64F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_75F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10077_O" deadCode="false" sourceNode="P_10F10077" targetNode="P_65F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_75F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10077_I" deadCode="false" sourceNode="P_10F10077" targetNode="P_66F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_76F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10077_O" deadCode="false" sourceNode="P_10F10077" targetNode="P_67F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_76F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10077_I" deadCode="false" sourceNode="P_10F10077" targetNode="P_68F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_77F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10077_O" deadCode="false" sourceNode="P_10F10077" targetNode="P_69F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_77F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10077_I" deadCode="false" sourceNode="P_10F10077" targetNode="P_70F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_78F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10077_O" deadCode="false" sourceNode="P_10F10077" targetNode="P_71F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_78F10077"/>
	</edges>
	<edges id="P_10F10077P_11F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10077" targetNode="P_11F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10077_I" deadCode="false" sourceNode="P_12F10077" targetNode="P_72F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_82F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10077_O" deadCode="false" sourceNode="P_12F10077" targetNode="P_73F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_82F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10077_I" deadCode="false" sourceNode="P_12F10077" targetNode="P_74F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_83F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10077_O" deadCode="false" sourceNode="P_12F10077" targetNode="P_75F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_83F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10077_I" deadCode="false" sourceNode="P_12F10077" targetNode="P_76F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_84F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10077_O" deadCode="false" sourceNode="P_12F10077" targetNode="P_77F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_84F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10077_I" deadCode="false" sourceNode="P_12F10077" targetNode="P_78F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_85F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10077_O" deadCode="false" sourceNode="P_12F10077" targetNode="P_79F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_85F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10077_I" deadCode="false" sourceNode="P_12F10077" targetNode="P_80F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_86F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10077_O" deadCode="false" sourceNode="P_12F10077" targetNode="P_81F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_86F10077"/>
	</edges>
	<edges id="P_12F10077P_13F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10077" targetNode="P_13F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10077_I" deadCode="false" sourceNode="P_14F10077" targetNode="P_82F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_90F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10077_O" deadCode="false" sourceNode="P_14F10077" targetNode="P_83F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_90F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10077_I" deadCode="false" sourceNode="P_14F10077" targetNode="P_40F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_91F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10077_O" deadCode="false" sourceNode="P_14F10077" targetNode="P_41F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_91F10077"/>
	</edges>
	<edges id="P_14F10077P_15F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10077" targetNode="P_15F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10077_I" deadCode="false" sourceNode="P_16F10077" targetNode="P_84F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_95F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10077_O" deadCode="false" sourceNode="P_16F10077" targetNode="P_85F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_95F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10077_I" deadCode="false" sourceNode="P_16F10077" targetNode="P_86F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_96F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10077_O" deadCode="false" sourceNode="P_16F10077" targetNode="P_87F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_96F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10077_I" deadCode="false" sourceNode="P_16F10077" targetNode="P_88F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_97F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10077_O" deadCode="false" sourceNode="P_16F10077" targetNode="P_89F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_97F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10077_I" deadCode="false" sourceNode="P_16F10077" targetNode="P_90F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_98F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10077_O" deadCode="false" sourceNode="P_16F10077" targetNode="P_91F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_98F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10077_I" deadCode="false" sourceNode="P_16F10077" targetNode="P_92F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_99F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10077_O" deadCode="false" sourceNode="P_16F10077" targetNode="P_93F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_99F10077"/>
	</edges>
	<edges id="P_16F10077P_17F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10077" targetNode="P_17F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10077_I" deadCode="false" sourceNode="P_18F10077" targetNode="P_94F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_103F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10077_O" deadCode="false" sourceNode="P_18F10077" targetNode="P_95F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_103F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10077_I" deadCode="false" sourceNode="P_18F10077" targetNode="P_96F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_104F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10077_O" deadCode="false" sourceNode="P_18F10077" targetNode="P_97F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_104F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10077_I" deadCode="false" sourceNode="P_18F10077" targetNode="P_98F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_105F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10077_O" deadCode="false" sourceNode="P_18F10077" targetNode="P_99F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_105F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10077_I" deadCode="false" sourceNode="P_18F10077" targetNode="P_100F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_106F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10077_O" deadCode="false" sourceNode="P_18F10077" targetNode="P_101F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_106F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10077_I" deadCode="false" sourceNode="P_18F10077" targetNode="P_102F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_107F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10077_O" deadCode="false" sourceNode="P_18F10077" targetNode="P_103F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_107F10077"/>
	</edges>
	<edges id="P_18F10077P_19F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10077" targetNode="P_19F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10077_I" deadCode="false" sourceNode="P_20F10077" targetNode="P_104F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_111F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10077_O" deadCode="false" sourceNode="P_20F10077" targetNode="P_105F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_111F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10077_I" deadCode="false" sourceNode="P_20F10077" targetNode="P_106F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_112F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10077_O" deadCode="false" sourceNode="P_20F10077" targetNode="P_107F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_112F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10077_I" deadCode="false" sourceNode="P_20F10077" targetNode="P_108F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_113F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10077_O" deadCode="false" sourceNode="P_20F10077" targetNode="P_109F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_113F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10077_I" deadCode="false" sourceNode="P_20F10077" targetNode="P_110F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_114F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10077_O" deadCode="false" sourceNode="P_20F10077" targetNode="P_111F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_114F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10077_I" deadCode="false" sourceNode="P_20F10077" targetNode="P_112F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_115F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10077_O" deadCode="false" sourceNode="P_20F10077" targetNode="P_113F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_115F10077"/>
	</edges>
	<edges id="P_20F10077P_21F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10077" targetNode="P_21F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10077_I" deadCode="false" sourceNode="P_22F10077" targetNode="P_114F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_119F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10077_O" deadCode="false" sourceNode="P_22F10077" targetNode="P_115F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_119F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10077_I" deadCode="false" sourceNode="P_22F10077" targetNode="P_116F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_120F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10077_O" deadCode="false" sourceNode="P_22F10077" targetNode="P_117F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_120F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10077_I" deadCode="false" sourceNode="P_22F10077" targetNode="P_118F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_121F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10077_O" deadCode="false" sourceNode="P_22F10077" targetNode="P_119F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_121F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10077_I" deadCode="false" sourceNode="P_22F10077" targetNode="P_120F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_122F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10077_O" deadCode="false" sourceNode="P_22F10077" targetNode="P_121F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_122F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10077_I" deadCode="false" sourceNode="P_22F10077" targetNode="P_122F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_123F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10077_O" deadCode="false" sourceNode="P_22F10077" targetNode="P_123F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_123F10077"/>
	</edges>
	<edges id="P_22F10077P_23F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10077" targetNode="P_23F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10077_I" deadCode="false" sourceNode="P_30F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_126F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10077_O" deadCode="false" sourceNode="P_30F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_126F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10077_I" deadCode="false" sourceNode="P_30F10077" targetNode="P_28F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_128F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10077_O" deadCode="false" sourceNode="P_30F10077" targetNode="P_29F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_128F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10077_I" deadCode="false" sourceNode="P_30F10077" targetNode="P_126F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_130F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10077_O" deadCode="false" sourceNode="P_30F10077" targetNode="P_127F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_130F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10077_I" deadCode="false" sourceNode="P_30F10077" targetNode="P_128F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_131F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10077_O" deadCode="false" sourceNode="P_30F10077" targetNode="P_129F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_131F10077"/>
	</edges>
	<edges id="P_30F10077P_31F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10077" targetNode="P_31F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10077_I" deadCode="false" sourceNode="P_32F10077" targetNode="P_130F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_133F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10077_O" deadCode="false" sourceNode="P_32F10077" targetNode="P_131F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_133F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10077_I" deadCode="false" sourceNode="P_32F10077" targetNode="P_132F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_135F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10077_O" deadCode="false" sourceNode="P_32F10077" targetNode="P_133F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_135F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10077_I" deadCode="false" sourceNode="P_32F10077" targetNode="P_134F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_136F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10077_O" deadCode="false" sourceNode="P_32F10077" targetNode="P_135F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_136F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10077_I" deadCode="false" sourceNode="P_32F10077" targetNode="P_136F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_137F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10077_O" deadCode="false" sourceNode="P_32F10077" targetNode="P_137F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_137F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10077_I" deadCode="false" sourceNode="P_32F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_138F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10077_O" deadCode="false" sourceNode="P_32F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_138F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10077_I" deadCode="false" sourceNode="P_32F10077" targetNode="P_28F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_140F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10077_O" deadCode="false" sourceNode="P_32F10077" targetNode="P_29F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_140F10077"/>
	</edges>
	<edges id="P_32F10077P_33F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10077" targetNode="P_33F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10077_I" deadCode="false" sourceNode="P_34F10077" targetNode="P_138F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_142F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10077_O" deadCode="false" sourceNode="P_34F10077" targetNode="P_139F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_142F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10077_I" deadCode="false" sourceNode="P_34F10077" targetNode="P_134F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_143F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10077_O" deadCode="false" sourceNode="P_34F10077" targetNode="P_135F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_143F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10077_I" deadCode="false" sourceNode="P_34F10077" targetNode="P_136F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_144F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10077_O" deadCode="false" sourceNode="P_34F10077" targetNode="P_137F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_144F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10077_I" deadCode="false" sourceNode="P_34F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_145F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10077_O" deadCode="false" sourceNode="P_34F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_145F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10077_I" deadCode="false" sourceNode="P_34F10077" targetNode="P_28F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_147F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10077_O" deadCode="false" sourceNode="P_34F10077" targetNode="P_29F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_147F10077"/>
	</edges>
	<edges id="P_34F10077P_35F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10077" targetNode="P_35F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10077_I" deadCode="false" sourceNode="P_36F10077" targetNode="P_28F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_150F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10077_O" deadCode="false" sourceNode="P_36F10077" targetNode="P_29F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_150F10077"/>
	</edges>
	<edges id="P_36F10077P_37F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10077" targetNode="P_37F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10077_I" deadCode="false" sourceNode="P_140F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_152F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10077_O" deadCode="false" sourceNode="P_140F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_152F10077"/>
	</edges>
	<edges id="P_140F10077P_141F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_140F10077" targetNode="P_141F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_156F10077_I" deadCode="false" sourceNode="P_38F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_156F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_156F10077_O" deadCode="false" sourceNode="P_38F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_156F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10077_I" deadCode="false" sourceNode="P_38F10077" targetNode="P_28F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_158F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10077_O" deadCode="false" sourceNode="P_38F10077" targetNode="P_29F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_158F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10077_I" deadCode="false" sourceNode="P_38F10077" targetNode="P_126F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_160F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10077_O" deadCode="false" sourceNode="P_38F10077" targetNode="P_127F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_160F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10077_I" deadCode="false" sourceNode="P_38F10077" targetNode="P_128F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_161F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10077_O" deadCode="false" sourceNode="P_38F10077" targetNode="P_129F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_161F10077"/>
	</edges>
	<edges id="P_38F10077P_39F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10077" targetNode="P_39F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10077_I" deadCode="false" sourceNode="P_142F10077" targetNode="P_138F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_163F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10077_O" deadCode="false" sourceNode="P_142F10077" targetNode="P_139F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_163F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10077_I" deadCode="false" sourceNode="P_142F10077" targetNode="P_134F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_164F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10077_O" deadCode="false" sourceNode="P_142F10077" targetNode="P_135F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_164F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10077_I" deadCode="false" sourceNode="P_142F10077" targetNode="P_136F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_165F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10077_O" deadCode="false" sourceNode="P_142F10077" targetNode="P_137F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_165F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10077_I" deadCode="false" sourceNode="P_142F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_166F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10077_O" deadCode="false" sourceNode="P_142F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_166F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10077_I" deadCode="false" sourceNode="P_142F10077" targetNode="P_28F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_168F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10077_O" deadCode="false" sourceNode="P_142F10077" targetNode="P_29F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_168F10077"/>
	</edges>
	<edges id="P_142F10077P_143F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_142F10077" targetNode="P_143F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10077_I" deadCode="false" sourceNode="P_144F10077" targetNode="P_140F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_170F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10077_O" deadCode="false" sourceNode="P_144F10077" targetNode="P_141F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_170F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10077_I" deadCode="false" sourceNode="P_144F10077" targetNode="P_28F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_172F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10077_O" deadCode="false" sourceNode="P_144F10077" targetNode="P_29F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_172F10077"/>
	</edges>
	<edges id="P_144F10077P_145F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_144F10077" targetNode="P_145F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10077_I" deadCode="false" sourceNode="P_146F10077" targetNode="P_28F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_175F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10077_O" deadCode="false" sourceNode="P_146F10077" targetNode="P_29F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_175F10077"/>
	</edges>
	<edges id="P_146F10077P_147F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_146F10077" targetNode="P_147F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10077_I" deadCode="true" sourceNode="P_148F10077" targetNode="P_144F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_177F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10077_O" deadCode="true" sourceNode="P_148F10077" targetNode="P_145F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_177F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10077_I" deadCode="true" sourceNode="P_148F10077" targetNode="P_149F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_179F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10077_O" deadCode="true" sourceNode="P_148F10077" targetNode="P_150F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_179F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_182F10077_I" deadCode="false" sourceNode="P_149F10077" targetNode="P_28F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_182F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_182F10077_O" deadCode="false" sourceNode="P_149F10077" targetNode="P_29F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_182F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10077_I" deadCode="false" sourceNode="P_149F10077" targetNode="P_126F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_184F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10077_O" deadCode="false" sourceNode="P_149F10077" targetNode="P_127F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_184F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10077_I" deadCode="false" sourceNode="P_149F10077" targetNode="P_128F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_185F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10077_O" deadCode="false" sourceNode="P_149F10077" targetNode="P_129F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_185F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10077_I" deadCode="false" sourceNode="P_149F10077" targetNode="P_146F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_187F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10077_O" deadCode="false" sourceNode="P_149F10077" targetNode="P_147F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_187F10077"/>
	</edges>
	<edges id="P_149F10077P_150F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_149F10077" targetNode="P_150F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10077_I" deadCode="false" sourceNode="P_152F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_191F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10077_O" deadCode="false" sourceNode="P_152F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_191F10077"/>
	</edges>
	<edges id="P_152F10077P_153F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_152F10077" targetNode="P_153F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_194F10077_I" deadCode="false" sourceNode="P_42F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_194F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_194F10077_O" deadCode="false" sourceNode="P_42F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_194F10077"/>
	</edges>
	<edges id="P_42F10077P_43F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10077" targetNode="P_43F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_197F10077_I" deadCode="false" sourceNode="P_44F10077" targetNode="P_152F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_197F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_197F10077_O" deadCode="false" sourceNode="P_44F10077" targetNode="P_153F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_197F10077"/>
	</edges>
	<edges id="P_44F10077P_45F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10077" targetNode="P_45F10077"/>
	<edges id="P_46F10077P_47F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10077" targetNode="P_47F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10077_I" deadCode="false" sourceNode="P_48F10077" targetNode="P_44F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_202F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10077_O" deadCode="false" sourceNode="P_48F10077" targetNode="P_45F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_202F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_204F10077_I" deadCode="false" sourceNode="P_48F10077" targetNode="P_50F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_204F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_204F10077_O" deadCode="false" sourceNode="P_48F10077" targetNode="P_51F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_204F10077"/>
	</edges>
	<edges id="P_48F10077P_49F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10077" targetNode="P_49F10077"/>
	<edges id="P_50F10077P_51F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10077" targetNode="P_51F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_208F10077_I" deadCode="false" sourceNode="P_154F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_208F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_208F10077_O" deadCode="false" sourceNode="P_154F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_208F10077"/>
	</edges>
	<edges id="P_154F10077P_155F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_154F10077" targetNode="P_155F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10077_I" deadCode="false" sourceNode="P_52F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_211F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10077_O" deadCode="false" sourceNode="P_52F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_211F10077"/>
	</edges>
	<edges id="P_52F10077P_53F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10077" targetNode="P_53F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_214F10077_I" deadCode="false" sourceNode="P_54F10077" targetNode="P_154F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_214F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_214F10077_O" deadCode="false" sourceNode="P_54F10077" targetNode="P_155F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_214F10077"/>
	</edges>
	<edges id="P_54F10077P_55F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10077" targetNode="P_55F10077"/>
	<edges id="P_56F10077P_57F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10077" targetNode="P_57F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_219F10077_I" deadCode="false" sourceNode="P_58F10077" targetNode="P_54F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_219F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_219F10077_O" deadCode="false" sourceNode="P_58F10077" targetNode="P_55F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_219F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10077_I" deadCode="false" sourceNode="P_58F10077" targetNode="P_60F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_221F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10077_O" deadCode="false" sourceNode="P_58F10077" targetNode="P_61F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_221F10077"/>
	</edges>
	<edges id="P_58F10077P_59F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10077" targetNode="P_59F10077"/>
	<edges id="P_60F10077P_61F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_60F10077" targetNode="P_61F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_225F10077_I" deadCode="false" sourceNode="P_156F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_225F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_225F10077_O" deadCode="false" sourceNode="P_156F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_225F10077"/>
	</edges>
	<edges id="P_156F10077P_157F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_156F10077" targetNode="P_157F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_228F10077_I" deadCode="false" sourceNode="P_62F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_228F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_228F10077_O" deadCode="false" sourceNode="P_62F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_228F10077"/>
	</edges>
	<edges id="P_62F10077P_63F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10077" targetNode="P_63F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10077_I" deadCode="false" sourceNode="P_64F10077" targetNode="P_156F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_231F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10077_O" deadCode="false" sourceNode="P_64F10077" targetNode="P_157F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_231F10077"/>
	</edges>
	<edges id="P_64F10077P_65F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10077" targetNode="P_65F10077"/>
	<edges id="P_66F10077P_67F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10077" targetNode="P_67F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_236F10077_I" deadCode="false" sourceNode="P_68F10077" targetNode="P_64F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_236F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_236F10077_O" deadCode="false" sourceNode="P_68F10077" targetNode="P_65F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_236F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_238F10077_I" deadCode="false" sourceNode="P_68F10077" targetNode="P_70F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_238F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_238F10077_O" deadCode="false" sourceNode="P_68F10077" targetNode="P_71F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_238F10077"/>
	</edges>
	<edges id="P_68F10077P_69F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10077" targetNode="P_69F10077"/>
	<edges id="P_70F10077P_71F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_70F10077" targetNode="P_71F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10077_I" deadCode="false" sourceNode="P_158F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_242F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10077_O" deadCode="false" sourceNode="P_158F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_242F10077"/>
	</edges>
	<edges id="P_158F10077P_159F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_158F10077" targetNode="P_159F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_246F10077_I" deadCode="false" sourceNode="P_72F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_246F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_246F10077_O" deadCode="false" sourceNode="P_72F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_246F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_248F10077_I" deadCode="false" sourceNode="P_72F10077" targetNode="P_28F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_248F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_248F10077_O" deadCode="false" sourceNode="P_72F10077" targetNode="P_29F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_248F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_250F10077_I" deadCode="false" sourceNode="P_72F10077" targetNode="P_126F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_250F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_250F10077_O" deadCode="false" sourceNode="P_72F10077" targetNode="P_127F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_250F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_251F10077_I" deadCode="false" sourceNode="P_72F10077" targetNode="P_128F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_251F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_251F10077_O" deadCode="false" sourceNode="P_72F10077" targetNode="P_129F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_251F10077"/>
	</edges>
	<edges id="P_72F10077P_73F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10077" targetNode="P_73F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10077_I" deadCode="false" sourceNode="P_74F10077" targetNode="P_158F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_253F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10077_O" deadCode="false" sourceNode="P_74F10077" targetNode="P_159F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_253F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10077_I" deadCode="false" sourceNode="P_74F10077" targetNode="P_28F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_255F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10077_O" deadCode="false" sourceNode="P_74F10077" targetNode="P_29F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_255F10077"/>
	</edges>
	<edges id="P_74F10077P_75F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10077" targetNode="P_75F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_258F10077_I" deadCode="false" sourceNode="P_76F10077" targetNode="P_28F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_258F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_258F10077_O" deadCode="false" sourceNode="P_76F10077" targetNode="P_29F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_258F10077"/>
	</edges>
	<edges id="P_76F10077P_77F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_76F10077" targetNode="P_77F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_260F10077_I" deadCode="false" sourceNode="P_78F10077" targetNode="P_74F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_260F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_260F10077_O" deadCode="false" sourceNode="P_78F10077" targetNode="P_75F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_260F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10077_I" deadCode="false" sourceNode="P_78F10077" targetNode="P_80F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_262F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10077_O" deadCode="false" sourceNode="P_78F10077" targetNode="P_81F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_262F10077"/>
	</edges>
	<edges id="P_78F10077P_79F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_78F10077" targetNode="P_79F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10077_I" deadCode="false" sourceNode="P_80F10077" targetNode="P_28F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_265F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10077_O" deadCode="false" sourceNode="P_80F10077" targetNode="P_29F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_265F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_267F10077_I" deadCode="false" sourceNode="P_80F10077" targetNode="P_126F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_267F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_267F10077_O" deadCode="false" sourceNode="P_80F10077" targetNode="P_127F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_267F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_268F10077_I" deadCode="false" sourceNode="P_80F10077" targetNode="P_128F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_268F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_268F10077_O" deadCode="false" sourceNode="P_80F10077" targetNode="P_129F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_268F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_270F10077_I" deadCode="false" sourceNode="P_80F10077" targetNode="P_76F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_270F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_270F10077_O" deadCode="false" sourceNode="P_80F10077" targetNode="P_77F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_270F10077"/>
	</edges>
	<edges id="P_80F10077P_81F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_80F10077" targetNode="P_81F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_274F10077_I" deadCode="false" sourceNode="P_82F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_274F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_274F10077_O" deadCode="false" sourceNode="P_82F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_274F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10077_I" deadCode="false" sourceNode="P_82F10077" targetNode="P_28F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_276F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10077_O" deadCode="false" sourceNode="P_82F10077" targetNode="P_29F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_276F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10077_I" deadCode="false" sourceNode="P_82F10077" targetNode="P_126F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_278F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10077_O" deadCode="false" sourceNode="P_82F10077" targetNode="P_127F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_278F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_279F10077_I" deadCode="false" sourceNode="P_82F10077" targetNode="P_128F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_279F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_279F10077_O" deadCode="false" sourceNode="P_82F10077" targetNode="P_129F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_279F10077"/>
	</edges>
	<edges id="P_82F10077P_83F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_82F10077" targetNode="P_83F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10077_I" deadCode="false" sourceNode="P_160F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_281F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10077_O" deadCode="false" sourceNode="P_160F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_281F10077"/>
	</edges>
	<edges id="P_160F10077P_161F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_160F10077" targetNode="P_161F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10077_I" deadCode="false" sourceNode="P_84F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_284F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10077_O" deadCode="false" sourceNode="P_84F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_284F10077"/>
	</edges>
	<edges id="P_84F10077P_85F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_84F10077" targetNode="P_85F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_287F10077_I" deadCode="false" sourceNode="P_86F10077" targetNode="P_160F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_287F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_287F10077_O" deadCode="false" sourceNode="P_86F10077" targetNode="P_161F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_287F10077"/>
	</edges>
	<edges id="P_86F10077P_87F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_86F10077" targetNode="P_87F10077"/>
	<edges id="P_88F10077P_89F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_88F10077" targetNode="P_89F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10077_I" deadCode="false" sourceNode="P_90F10077" targetNode="P_86F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_292F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10077_O" deadCode="false" sourceNode="P_90F10077" targetNode="P_87F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_292F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10077_I" deadCode="false" sourceNode="P_90F10077" targetNode="P_92F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_294F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10077_O" deadCode="false" sourceNode="P_90F10077" targetNode="P_93F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_294F10077"/>
	</edges>
	<edges id="P_90F10077P_91F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_90F10077" targetNode="P_91F10077"/>
	<edges id="P_92F10077P_93F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_92F10077" targetNode="P_93F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_298F10077_I" deadCode="false" sourceNode="P_162F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_298F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_298F10077_O" deadCode="false" sourceNode="P_162F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_298F10077"/>
	</edges>
	<edges id="P_162F10077P_163F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_162F10077" targetNode="P_163F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10077_I" deadCode="false" sourceNode="P_94F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_301F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10077_O" deadCode="false" sourceNode="P_94F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_301F10077"/>
	</edges>
	<edges id="P_94F10077P_95F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_94F10077" targetNode="P_95F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_304F10077_I" deadCode="false" sourceNode="P_96F10077" targetNode="P_162F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_304F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_304F10077_O" deadCode="false" sourceNode="P_96F10077" targetNode="P_163F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_304F10077"/>
	</edges>
	<edges id="P_96F10077P_97F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_96F10077" targetNode="P_97F10077"/>
	<edges id="P_98F10077P_99F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_98F10077" targetNode="P_99F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10077_I" deadCode="false" sourceNode="P_100F10077" targetNode="P_96F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_309F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10077_O" deadCode="false" sourceNode="P_100F10077" targetNode="P_97F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_309F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10077_I" deadCode="false" sourceNode="P_100F10077" targetNode="P_102F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_311F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10077_O" deadCode="false" sourceNode="P_100F10077" targetNode="P_103F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_311F10077"/>
	</edges>
	<edges id="P_100F10077P_101F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_100F10077" targetNode="P_101F10077"/>
	<edges id="P_102F10077P_103F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_102F10077" targetNode="P_103F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_315F10077_I" deadCode="false" sourceNode="P_164F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_315F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_315F10077_O" deadCode="false" sourceNode="P_164F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_315F10077"/>
	</edges>
	<edges id="P_164F10077P_165F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_164F10077" targetNode="P_165F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_318F10077_I" deadCode="false" sourceNode="P_104F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_318F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_318F10077_O" deadCode="false" sourceNode="P_104F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_318F10077"/>
	</edges>
	<edges id="P_104F10077P_105F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_104F10077" targetNode="P_105F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10077_I" deadCode="false" sourceNode="P_106F10077" targetNode="P_164F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_321F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10077_O" deadCode="false" sourceNode="P_106F10077" targetNode="P_165F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_321F10077"/>
	</edges>
	<edges id="P_106F10077P_107F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_106F10077" targetNode="P_107F10077"/>
	<edges id="P_108F10077P_109F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_108F10077" targetNode="P_109F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_326F10077_I" deadCode="false" sourceNode="P_110F10077" targetNode="P_106F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_326F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_326F10077_O" deadCode="false" sourceNode="P_110F10077" targetNode="P_107F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_326F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_328F10077_I" deadCode="false" sourceNode="P_110F10077" targetNode="P_112F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_328F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_328F10077_O" deadCode="false" sourceNode="P_110F10077" targetNode="P_113F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_328F10077"/>
	</edges>
	<edges id="P_110F10077P_111F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_110F10077" targetNode="P_111F10077"/>
	<edges id="P_112F10077P_113F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_112F10077" targetNode="P_113F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_332F10077_I" deadCode="false" sourceNode="P_166F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_332F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_332F10077_O" deadCode="false" sourceNode="P_166F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_332F10077"/>
	</edges>
	<edges id="P_166F10077P_167F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_166F10077" targetNode="P_167F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10077_I" deadCode="false" sourceNode="P_114F10077" targetNode="P_124F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_336F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10077_O" deadCode="false" sourceNode="P_114F10077" targetNode="P_125F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_336F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10077_I" deadCode="false" sourceNode="P_114F10077" targetNode="P_28F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_338F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10077_O" deadCode="false" sourceNode="P_114F10077" targetNode="P_29F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_338F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_340F10077_I" deadCode="false" sourceNode="P_114F10077" targetNode="P_126F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_340F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_340F10077_O" deadCode="false" sourceNode="P_114F10077" targetNode="P_127F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_340F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_341F10077_I" deadCode="false" sourceNode="P_114F10077" targetNode="P_128F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_341F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_341F10077_O" deadCode="false" sourceNode="P_114F10077" targetNode="P_129F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_341F10077"/>
	</edges>
	<edges id="P_114F10077P_115F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_114F10077" targetNode="P_115F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_343F10077_I" deadCode="false" sourceNode="P_116F10077" targetNode="P_166F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_343F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_343F10077_O" deadCode="false" sourceNode="P_116F10077" targetNode="P_167F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_343F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_345F10077_I" deadCode="false" sourceNode="P_116F10077" targetNode="P_28F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_345F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_345F10077_O" deadCode="false" sourceNode="P_116F10077" targetNode="P_29F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_345F10077"/>
	</edges>
	<edges id="P_116F10077P_117F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_116F10077" targetNode="P_117F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_348F10077_I" deadCode="false" sourceNode="P_118F10077" targetNode="P_28F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_348F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_348F10077_O" deadCode="false" sourceNode="P_118F10077" targetNode="P_29F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_348F10077"/>
	</edges>
	<edges id="P_118F10077P_119F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_118F10077" targetNode="P_119F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_350F10077_I" deadCode="false" sourceNode="P_120F10077" targetNode="P_116F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_350F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_350F10077_O" deadCode="false" sourceNode="P_120F10077" targetNode="P_117F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_350F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_352F10077_I" deadCode="false" sourceNode="P_120F10077" targetNode="P_122F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_352F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_352F10077_O" deadCode="false" sourceNode="P_120F10077" targetNode="P_123F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_352F10077"/>
	</edges>
	<edges id="P_120F10077P_121F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_120F10077" targetNode="P_121F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_355F10077_I" deadCode="false" sourceNode="P_122F10077" targetNode="P_28F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_355F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_355F10077_O" deadCode="false" sourceNode="P_122F10077" targetNode="P_29F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_355F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_357F10077_I" deadCode="false" sourceNode="P_122F10077" targetNode="P_126F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_357F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_357F10077_O" deadCode="false" sourceNode="P_122F10077" targetNode="P_127F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_357F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_358F10077_I" deadCode="false" sourceNode="P_122F10077" targetNode="P_128F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_358F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_358F10077_O" deadCode="false" sourceNode="P_122F10077" targetNode="P_129F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_358F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_360F10077_I" deadCode="false" sourceNode="P_122F10077" targetNode="P_118F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_360F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_360F10077_O" deadCode="false" sourceNode="P_122F10077" targetNode="P_119F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_360F10077"/>
	</edges>
	<edges id="P_122F10077P_123F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_122F10077" targetNode="P_123F10077"/>
	<edges id="P_126F10077P_127F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_126F10077" targetNode="P_127F10077"/>
	<edges id="P_132F10077P_133F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_132F10077" targetNode="P_133F10077"/>
	<edges id="P_138F10077P_139F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_138F10077" targetNode="P_139F10077"/>
	<edges id="P_134F10077P_135F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_134F10077" targetNode="P_135F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_431F10077_I" deadCode="false" sourceNode="P_130F10077" targetNode="P_28F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_431F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_431F10077_O" deadCode="false" sourceNode="P_130F10077" targetNode="P_29F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_431F10077"/>
	</edges>
	<edges id="P_130F10077P_131F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_130F10077" targetNode="P_131F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_435F10077_I" deadCode="false" sourceNode="P_40F10077" targetNode="P_144F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_435F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_435F10077_O" deadCode="false" sourceNode="P_40F10077" targetNode="P_145F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_435F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_437F10077_I" deadCode="false" sourceNode="P_40F10077" targetNode="P_149F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_436F10077"/>
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_437F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_437F10077_O" deadCode="false" sourceNode="P_40F10077" targetNode="P_150F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_436F10077"/>
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_437F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_441F10077_I" deadCode="false" sourceNode="P_40F10077" targetNode="P_142F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_436F10077"/>
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_441F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_441F10077_O" deadCode="false" sourceNode="P_40F10077" targetNode="P_143F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_436F10077"/>
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_441F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_449F10077_I" deadCode="false" sourceNode="P_40F10077" targetNode="P_32F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_436F10077"/>
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_449F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_449F10077_O" deadCode="false" sourceNode="P_40F10077" targetNode="P_33F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_436F10077"/>
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_449F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_452F10077_I" deadCode="false" sourceNode="P_40F10077" targetNode="P_168F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_452F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_452F10077_O" deadCode="false" sourceNode="P_40F10077" targetNode="P_169F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_452F10077"/>
	</edges>
	<edges id="P_40F10077P_41F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10077" targetNode="P_41F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_463F10077_I" deadCode="false" sourceNode="P_168F10077" targetNode="P_32F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_463F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_463F10077_O" deadCode="false" sourceNode="P_168F10077" targetNode="P_33F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_463F10077"/>
	</edges>
	<edges id="P_168F10077P_169F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_168F10077" targetNode="P_169F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_466F10077_I" deadCode="false" sourceNode="P_136F10077" targetNode="P_170F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_466F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_466F10077_O" deadCode="false" sourceNode="P_136F10077" targetNode="P_171F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_466F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_469F10077_I" deadCode="false" sourceNode="P_136F10077" targetNode="P_170F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_469F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_469F10077_O" deadCode="false" sourceNode="P_136F10077" targetNode="P_171F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_469F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_473F10077_I" deadCode="false" sourceNode="P_136F10077" targetNode="P_170F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_473F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_473F10077_O" deadCode="false" sourceNode="P_136F10077" targetNode="P_171F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_473F10077"/>
	</edges>
	<edges id="P_136F10077P_137F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_136F10077" targetNode="P_137F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_477F10077_I" deadCode="false" sourceNode="P_128F10077" targetNode="P_172F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_477F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_477F10077_O" deadCode="false" sourceNode="P_128F10077" targetNode="P_173F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_477F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_480F10077_I" deadCode="false" sourceNode="P_128F10077" targetNode="P_172F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_480F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_480F10077_O" deadCode="false" sourceNode="P_128F10077" targetNode="P_173F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_480F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_484F10077_I" deadCode="false" sourceNode="P_128F10077" targetNode="P_172F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_484F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_484F10077_O" deadCode="false" sourceNode="P_128F10077" targetNode="P_173F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_484F10077"/>
	</edges>
	<edges id="P_128F10077P_129F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_128F10077" targetNode="P_129F10077"/>
	<edges id="P_124F10077P_125F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_124F10077" targetNode="P_125F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_489F10077_I" deadCode="false" sourceNode="P_26F10077" targetNode="P_174F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_489F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_489F10077_O" deadCode="false" sourceNode="P_26F10077" targetNode="P_175F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_489F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_491F10077_I" deadCode="false" sourceNode="P_26F10077" targetNode="P_176F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_491F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_491F10077_O" deadCode="false" sourceNode="P_26F10077" targetNode="P_177F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_491F10077"/>
	</edges>
	<edges id="P_26F10077P_27F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10077" targetNode="P_27F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_496F10077_I" deadCode="false" sourceNode="P_174F10077" targetNode="P_170F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_496F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_496F10077_O" deadCode="false" sourceNode="P_174F10077" targetNode="P_171F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_496F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_501F10077_I" deadCode="false" sourceNode="P_174F10077" targetNode="P_170F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_501F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_501F10077_O" deadCode="false" sourceNode="P_174F10077" targetNode="P_171F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_501F10077"/>
	</edges>
	<edges id="P_174F10077P_175F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_174F10077" targetNode="P_175F10077"/>
	<edges id="P_176F10077P_177F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_176F10077" targetNode="P_177F10077"/>
	<edges id="P_170F10077P_171F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_170F10077" targetNode="P_171F10077"/>
	<edges xsi:type="cbl:PerformEdge" id="S_530F10077_I" deadCode="false" sourceNode="P_172F10077" targetNode="P_180F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_530F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_530F10077_O" deadCode="false" sourceNode="P_172F10077" targetNode="P_181F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_530F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_531F10077_I" deadCode="false" sourceNode="P_172F10077" targetNode="P_182F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_531F10077"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_531F10077_O" deadCode="false" sourceNode="P_172F10077" targetNode="P_183F10077">
		<representations href="../../../cobol/IDBSPOG0.cbl.cobModel#S_531F10077"/>
	</edges>
	<edges id="P_172F10077P_173F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_172F10077" targetNode="P_173F10077"/>
	<edges id="P_180F10077P_181F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_180F10077" targetNode="P_181F10077"/>
	<edges id="P_182F10077P_183F10077" xsi:type="cbl:FallThroughEdge" sourceNode="P_182F10077" targetNode="P_183F10077"/>
	<edges xsi:type="cbl:DataEdge" id="S_127F10077_POS1" deadCode="false" targetNode="P_30F10077" sourceNode="DB2_PARAM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_127F10077"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_139F10077_POS1" deadCode="false" sourceNode="P_32F10077" targetNode="DB2_PARAM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_139F10077"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_146F10077_POS1" deadCode="false" sourceNode="P_34F10077" targetNode="DB2_PARAM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_146F10077"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_149F10077_POS1" deadCode="false" sourceNode="P_36F10077" targetNode="DB2_PARAM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_149F10077"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_157F10077_POS1" deadCode="false" targetNode="P_38F10077" sourceNode="DB2_PARAM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_157F10077"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_167F10077_POS1" deadCode="false" sourceNode="P_142F10077" targetNode="DB2_PARAM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_167F10077"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_171F10077_POS1" deadCode="false" targetNode="P_144F10077" sourceNode="DB2_PARAM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_171F10077"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_174F10077_POS1" deadCode="false" targetNode="P_146F10077" sourceNode="DB2_PARAM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_174F10077"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_181F10077_POS1" deadCode="false" targetNode="P_149F10077" sourceNode="DB2_PARAM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_181F10077"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_247F10077_POS1" deadCode="false" targetNode="P_72F10077" sourceNode="DB2_PARAM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_247F10077"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_254F10077_POS1" deadCode="false" targetNode="P_74F10077" sourceNode="DB2_PARAM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_254F10077"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_257F10077_POS1" deadCode="false" targetNode="P_76F10077" sourceNode="DB2_PARAM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_257F10077"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_264F10077_POS1" deadCode="false" targetNode="P_80F10077" sourceNode="DB2_PARAM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_264F10077"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_275F10077_POS1" deadCode="false" targetNode="P_82F10077" sourceNode="DB2_PARAM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_275F10077"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_337F10077_POS1" deadCode="false" targetNode="P_114F10077" sourceNode="DB2_PARAM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_337F10077"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_344F10077_POS1" deadCode="false" targetNode="P_116F10077" sourceNode="DB2_PARAM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_344F10077"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_347F10077_POS1" deadCode="false" targetNode="P_118F10077" sourceNode="DB2_PARAM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_347F10077"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_354F10077_POS1" deadCode="false" targetNode="P_122F10077" sourceNode="DB2_PARAM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_354F10077"></representations>
	</edges>
</Package>

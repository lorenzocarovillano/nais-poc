<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBSD600" cbl:id="LDBSD600" xsi:id="LDBSD600" packageRef="LDBSD600.igd#LDBSD600" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBSD600_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10254" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBSD600.cbl.cobModel#SC_1F10254"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10254" deadCode="false" name="PROGRAM_LDBSD600_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_1F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10254" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_2F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10254" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_3F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10254" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_12F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10254" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_13F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10254" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_4F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10254" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_5F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10254" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_6F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10254" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_7F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10254" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_8F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10254" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_9F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10254" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_44F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10254" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_49F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10254" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_14F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10254" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_15F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10254" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_16F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10254" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_17F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10254" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_18F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10254" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_19F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10254" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_20F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10254" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_21F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10254" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_22F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10254" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_23F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10254" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_56F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10254" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_57F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10254" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_24F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10254" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_25F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10254" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_26F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10254" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_27F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10254" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_28F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10254" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_29F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10254" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_30F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10254" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_31F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10254" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_32F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10254" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_33F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10254" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_58F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10254" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_59F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10254" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_34F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10254" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_35F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10254" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_36F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10254" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_37F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10254" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_38F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10254" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_39F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10254" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_40F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10254" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_41F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10254" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_42F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10254" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_43F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10254" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_50F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10254" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_51F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10254" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_60F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10254" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_63F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10254" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_52F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10254" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_53F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10254" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_45F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10254" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_46F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10254" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_47F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10254" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_48F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10254" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_54F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10254" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_55F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10254" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_10F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10254" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_11F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10254" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_66F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10254" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_67F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10254" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_68F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10254" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_69F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10254" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_61F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10254" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_62F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10254" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_70F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10254" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_71F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10254" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_64F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10254" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_65F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10254" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_72F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10254" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_73F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10254" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_74F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10254" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_75F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10254" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_76F10254"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10254" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBSD600.cbl.cobModel#P_77F10254"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_TIT_CONT" name="TIT_CONT">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_TIT_CONT"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_DETT_TIT_CONT" name="DETT_TIT_CONT">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_DETT_TIT_CONT"/>
		</children>
	</packageNode>
	<edges id="SC_1F10254P_1F10254" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10254" targetNode="P_1F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10254_I" deadCode="false" sourceNode="P_1F10254" targetNode="P_2F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_2F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10254_O" deadCode="false" sourceNode="P_1F10254" targetNode="P_3F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_2F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10254_I" deadCode="false" sourceNode="P_1F10254" targetNode="P_4F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_6F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10254_O" deadCode="false" sourceNode="P_1F10254" targetNode="P_5F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_6F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10254_I" deadCode="false" sourceNode="P_1F10254" targetNode="P_6F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_10F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10254_O" deadCode="false" sourceNode="P_1F10254" targetNode="P_7F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_10F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10254_I" deadCode="false" sourceNode="P_1F10254" targetNode="P_8F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_14F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10254_O" deadCode="false" sourceNode="P_1F10254" targetNode="P_9F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_14F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10254_I" deadCode="false" sourceNode="P_2F10254" targetNode="P_10F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_24F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10254_O" deadCode="false" sourceNode="P_2F10254" targetNode="P_11F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_24F10254"/>
	</edges>
	<edges id="P_2F10254P_3F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10254" targetNode="P_3F10254"/>
	<edges id="P_12F10254P_13F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10254" targetNode="P_13F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10254_I" deadCode="false" sourceNode="P_4F10254" targetNode="P_14F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_37F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10254_O" deadCode="false" sourceNode="P_4F10254" targetNode="P_15F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_37F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10254_I" deadCode="false" sourceNode="P_4F10254" targetNode="P_16F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_38F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10254_O" deadCode="false" sourceNode="P_4F10254" targetNode="P_17F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_38F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10254_I" deadCode="false" sourceNode="P_4F10254" targetNode="P_18F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_39F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10254_O" deadCode="false" sourceNode="P_4F10254" targetNode="P_19F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_39F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10254_I" deadCode="false" sourceNode="P_4F10254" targetNode="P_20F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_40F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10254_O" deadCode="false" sourceNode="P_4F10254" targetNode="P_21F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_40F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10254_I" deadCode="false" sourceNode="P_4F10254" targetNode="P_22F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_41F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10254_O" deadCode="false" sourceNode="P_4F10254" targetNode="P_23F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_41F10254"/>
	</edges>
	<edges id="P_4F10254P_5F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10254" targetNode="P_5F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10254_I" deadCode="false" sourceNode="P_6F10254" targetNode="P_24F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_45F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10254_O" deadCode="false" sourceNode="P_6F10254" targetNode="P_25F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_45F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10254_I" deadCode="false" sourceNode="P_6F10254" targetNode="P_26F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_46F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10254_O" deadCode="false" sourceNode="P_6F10254" targetNode="P_27F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_46F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10254_I" deadCode="false" sourceNode="P_6F10254" targetNode="P_28F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_47F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10254_O" deadCode="false" sourceNode="P_6F10254" targetNode="P_29F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_47F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10254_I" deadCode="false" sourceNode="P_6F10254" targetNode="P_30F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_48F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10254_O" deadCode="false" sourceNode="P_6F10254" targetNode="P_31F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_48F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10254_I" deadCode="false" sourceNode="P_6F10254" targetNode="P_32F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_49F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10254_O" deadCode="false" sourceNode="P_6F10254" targetNode="P_33F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_49F10254"/>
	</edges>
	<edges id="P_6F10254P_7F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10254" targetNode="P_7F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10254_I" deadCode="false" sourceNode="P_8F10254" targetNode="P_34F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_53F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10254_O" deadCode="false" sourceNode="P_8F10254" targetNode="P_35F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_53F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10254_I" deadCode="false" sourceNode="P_8F10254" targetNode="P_36F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_54F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10254_O" deadCode="false" sourceNode="P_8F10254" targetNode="P_37F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_54F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10254_I" deadCode="false" sourceNode="P_8F10254" targetNode="P_38F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_55F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10254_O" deadCode="false" sourceNode="P_8F10254" targetNode="P_39F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_55F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10254_I" deadCode="false" sourceNode="P_8F10254" targetNode="P_40F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_56F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10254_O" deadCode="false" sourceNode="P_8F10254" targetNode="P_41F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_56F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10254_I" deadCode="false" sourceNode="P_8F10254" targetNode="P_42F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_57F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10254_O" deadCode="false" sourceNode="P_8F10254" targetNode="P_43F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_57F10254"/>
	</edges>
	<edges id="P_8F10254P_9F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10254" targetNode="P_9F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10254_I" deadCode="false" sourceNode="P_44F10254" targetNode="P_45F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_60F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10254_O" deadCode="false" sourceNode="P_44F10254" targetNode="P_46F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_60F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10254_I" deadCode="false" sourceNode="P_44F10254" targetNode="P_47F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_61F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10254_O" deadCode="false" sourceNode="P_44F10254" targetNode="P_48F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_61F10254"/>
	</edges>
	<edges id="P_44F10254P_49F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10254" targetNode="P_49F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10254_I" deadCode="false" sourceNode="P_14F10254" targetNode="P_45F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_65F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10254_O" deadCode="false" sourceNode="P_14F10254" targetNode="P_46F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_65F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10254_I" deadCode="false" sourceNode="P_14F10254" targetNode="P_47F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_66F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10254_O" deadCode="false" sourceNode="P_14F10254" targetNode="P_48F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_66F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10254_I" deadCode="false" sourceNode="P_14F10254" targetNode="P_12F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_68F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10254_O" deadCode="false" sourceNode="P_14F10254" targetNode="P_13F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_68F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10254_I" deadCode="false" sourceNode="P_14F10254" targetNode="P_50F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_70F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10254_O" deadCode="false" sourceNode="P_14F10254" targetNode="P_51F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_70F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10254_I" deadCode="false" sourceNode="P_14F10254" targetNode="P_52F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_71F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10254_O" deadCode="false" sourceNode="P_14F10254" targetNode="P_53F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_71F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10254_I" deadCode="false" sourceNode="P_14F10254" targetNode="P_54F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_72F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10254_O" deadCode="false" sourceNode="P_14F10254" targetNode="P_55F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_72F10254"/>
	</edges>
	<edges id="P_14F10254P_15F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10254" targetNode="P_15F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10254_I" deadCode="false" sourceNode="P_16F10254" targetNode="P_44F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_74F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10254_O" deadCode="false" sourceNode="P_16F10254" targetNode="P_49F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_74F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10254_I" deadCode="false" sourceNode="P_16F10254" targetNode="P_12F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_76F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10254_O" deadCode="false" sourceNode="P_16F10254" targetNode="P_13F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_76F10254"/>
	</edges>
	<edges id="P_16F10254P_17F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10254" targetNode="P_17F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10254_I" deadCode="false" sourceNode="P_18F10254" targetNode="P_12F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_79F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10254_O" deadCode="false" sourceNode="P_18F10254" targetNode="P_13F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_79F10254"/>
	</edges>
	<edges id="P_18F10254P_19F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10254" targetNode="P_19F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10254_I" deadCode="false" sourceNode="P_20F10254" targetNode="P_16F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_81F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10254_O" deadCode="false" sourceNode="P_20F10254" targetNode="P_17F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_81F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10254_I" deadCode="false" sourceNode="P_20F10254" targetNode="P_22F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_83F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10254_O" deadCode="false" sourceNode="P_20F10254" targetNode="P_23F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_83F10254"/>
	</edges>
	<edges id="P_20F10254P_21F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10254" targetNode="P_21F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10254_I" deadCode="false" sourceNode="P_22F10254" targetNode="P_12F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_86F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10254_O" deadCode="false" sourceNode="P_22F10254" targetNode="P_13F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_86F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10254_I" deadCode="false" sourceNode="P_22F10254" targetNode="P_50F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_88F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10254_O" deadCode="false" sourceNode="P_22F10254" targetNode="P_51F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_88F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10254_I" deadCode="false" sourceNode="P_22F10254" targetNode="P_52F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_89F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10254_O" deadCode="false" sourceNode="P_22F10254" targetNode="P_53F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_89F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10254_I" deadCode="false" sourceNode="P_22F10254" targetNode="P_54F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_90F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10254_O" deadCode="false" sourceNode="P_22F10254" targetNode="P_55F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_90F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10254_I" deadCode="false" sourceNode="P_22F10254" targetNode="P_18F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_92F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10254_O" deadCode="false" sourceNode="P_22F10254" targetNode="P_19F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_92F10254"/>
	</edges>
	<edges id="P_22F10254P_23F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10254" targetNode="P_23F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10254_I" deadCode="false" sourceNode="P_56F10254" targetNode="P_45F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_96F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10254_O" deadCode="false" sourceNode="P_56F10254" targetNode="P_46F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_96F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10254_I" deadCode="false" sourceNode="P_56F10254" targetNode="P_47F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_97F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10254_O" deadCode="false" sourceNode="P_56F10254" targetNode="P_48F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_97F10254"/>
	</edges>
	<edges id="P_56F10254P_57F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10254" targetNode="P_57F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10254_I" deadCode="false" sourceNode="P_24F10254" targetNode="P_45F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_101F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10254_O" deadCode="false" sourceNode="P_24F10254" targetNode="P_46F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_101F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10254_I" deadCode="false" sourceNode="P_24F10254" targetNode="P_47F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_102F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10254_O" deadCode="false" sourceNode="P_24F10254" targetNode="P_48F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_102F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10254_I" deadCode="false" sourceNode="P_24F10254" targetNode="P_12F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_104F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10254_O" deadCode="false" sourceNode="P_24F10254" targetNode="P_13F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_104F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10254_I" deadCode="false" sourceNode="P_24F10254" targetNode="P_50F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_106F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10254_O" deadCode="false" sourceNode="P_24F10254" targetNode="P_51F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_106F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10254_I" deadCode="false" sourceNode="P_24F10254" targetNode="P_52F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_107F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10254_O" deadCode="false" sourceNode="P_24F10254" targetNode="P_53F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_107F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10254_I" deadCode="false" sourceNode="P_24F10254" targetNode="P_54F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_108F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10254_O" deadCode="false" sourceNode="P_24F10254" targetNode="P_55F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_108F10254"/>
	</edges>
	<edges id="P_24F10254P_25F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10254" targetNode="P_25F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10254_I" deadCode="false" sourceNode="P_26F10254" targetNode="P_56F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_110F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10254_O" deadCode="false" sourceNode="P_26F10254" targetNode="P_57F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_110F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10254_I" deadCode="false" sourceNode="P_26F10254" targetNode="P_12F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_112F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10254_O" deadCode="false" sourceNode="P_26F10254" targetNode="P_13F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_112F10254"/>
	</edges>
	<edges id="P_26F10254P_27F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10254" targetNode="P_27F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10254_I" deadCode="false" sourceNode="P_28F10254" targetNode="P_12F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_115F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10254_O" deadCode="false" sourceNode="P_28F10254" targetNode="P_13F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_115F10254"/>
	</edges>
	<edges id="P_28F10254P_29F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10254" targetNode="P_29F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10254_I" deadCode="false" sourceNode="P_30F10254" targetNode="P_26F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_117F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10254_O" deadCode="false" sourceNode="P_30F10254" targetNode="P_27F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_117F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10254_I" deadCode="false" sourceNode="P_30F10254" targetNode="P_32F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_119F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10254_O" deadCode="false" sourceNode="P_30F10254" targetNode="P_33F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_119F10254"/>
	</edges>
	<edges id="P_30F10254P_31F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10254" targetNode="P_31F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10254_I" deadCode="false" sourceNode="P_32F10254" targetNode="P_12F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_122F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10254_O" deadCode="false" sourceNode="P_32F10254" targetNode="P_13F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_122F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10254_I" deadCode="false" sourceNode="P_32F10254" targetNode="P_50F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_124F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10254_O" deadCode="false" sourceNode="P_32F10254" targetNode="P_51F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_124F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10254_I" deadCode="false" sourceNode="P_32F10254" targetNode="P_52F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_125F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10254_O" deadCode="false" sourceNode="P_32F10254" targetNode="P_53F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_125F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10254_I" deadCode="false" sourceNode="P_32F10254" targetNode="P_54F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_126F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10254_O" deadCode="false" sourceNode="P_32F10254" targetNode="P_55F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_126F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10254_I" deadCode="false" sourceNode="P_32F10254" targetNode="P_28F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_128F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10254_O" deadCode="false" sourceNode="P_32F10254" targetNode="P_29F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_128F10254"/>
	</edges>
	<edges id="P_32F10254P_33F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10254" targetNode="P_33F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10254_I" deadCode="false" sourceNode="P_58F10254" targetNode="P_45F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_132F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10254_O" deadCode="false" sourceNode="P_58F10254" targetNode="P_46F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_132F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10254_I" deadCode="false" sourceNode="P_58F10254" targetNode="P_47F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_133F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10254_O" deadCode="false" sourceNode="P_58F10254" targetNode="P_48F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_133F10254"/>
	</edges>
	<edges id="P_58F10254P_59F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10254" targetNode="P_59F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10254_I" deadCode="false" sourceNode="P_34F10254" targetNode="P_45F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_136F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10254_O" deadCode="false" sourceNode="P_34F10254" targetNode="P_46F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_136F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10254_I" deadCode="false" sourceNode="P_34F10254" targetNode="P_47F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_137F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10254_O" deadCode="false" sourceNode="P_34F10254" targetNode="P_48F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_137F10254"/>
	</edges>
	<edges id="P_34F10254P_35F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10254" targetNode="P_35F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10254_I" deadCode="false" sourceNode="P_36F10254" targetNode="P_58F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_140F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10254_O" deadCode="false" sourceNode="P_36F10254" targetNode="P_59F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_140F10254"/>
	</edges>
	<edges id="P_36F10254P_37F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10254" targetNode="P_37F10254"/>
	<edges id="P_38F10254P_39F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10254" targetNode="P_39F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10254_I" deadCode="false" sourceNode="P_40F10254" targetNode="P_36F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_145F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10254_O" deadCode="false" sourceNode="P_40F10254" targetNode="P_37F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_145F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10254_I" deadCode="false" sourceNode="P_40F10254" targetNode="P_42F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_147F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10254_O" deadCode="false" sourceNode="P_40F10254" targetNode="P_43F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_147F10254"/>
	</edges>
	<edges id="P_40F10254P_41F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10254" targetNode="P_41F10254"/>
	<edges id="P_42F10254P_43F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10254" targetNode="P_43F10254"/>
	<edges id="P_50F10254P_51F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10254" targetNode="P_51F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_296F10254_I" deadCode="true" sourceNode="P_60F10254" targetNode="P_61F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_296F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_296F10254_O" deadCode="true" sourceNode="P_60F10254" targetNode="P_62F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_296F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10254_I" deadCode="true" sourceNode="P_60F10254" targetNode="P_61F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_299F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10254_O" deadCode="true" sourceNode="P_60F10254" targetNode="P_62F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_299F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_303F10254_I" deadCode="true" sourceNode="P_60F10254" targetNode="P_61F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_303F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_303F10254_O" deadCode="true" sourceNode="P_60F10254" targetNode="P_62F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_303F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_307F10254_I" deadCode="true" sourceNode="P_60F10254" targetNode="P_61F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_307F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_307F10254_O" deadCode="true" sourceNode="P_60F10254" targetNode="P_62F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_307F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10254_I" deadCode="true" sourceNode="P_60F10254" targetNode="P_61F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_311F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10254_O" deadCode="true" sourceNode="P_60F10254" targetNode="P_62F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_311F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_315F10254_I" deadCode="true" sourceNode="P_60F10254" targetNode="P_61F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_315F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_315F10254_O" deadCode="true" sourceNode="P_60F10254" targetNode="P_62F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_315F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_319F10254_I" deadCode="true" sourceNode="P_60F10254" targetNode="P_61F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_319F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_319F10254_O" deadCode="true" sourceNode="P_60F10254" targetNode="P_62F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_319F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_323F10254_I" deadCode="true" sourceNode="P_60F10254" targetNode="P_61F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_323F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_323F10254_O" deadCode="true" sourceNode="P_60F10254" targetNode="P_62F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_323F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_327F10254_I" deadCode="true" sourceNode="P_60F10254" targetNode="P_61F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_327F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_327F10254_O" deadCode="true" sourceNode="P_60F10254" targetNode="P_62F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_327F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_331F10254_I" deadCode="true" sourceNode="P_60F10254" targetNode="P_61F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_331F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_331F10254_O" deadCode="true" sourceNode="P_60F10254" targetNode="P_62F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_331F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_335F10254_I" deadCode="true" sourceNode="P_60F10254" targetNode="P_61F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_335F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_335F10254_O" deadCode="true" sourceNode="P_60F10254" targetNode="P_62F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_335F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_339F10254_I" deadCode="false" sourceNode="P_52F10254" targetNode="P_64F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_339F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_339F10254_O" deadCode="false" sourceNode="P_52F10254" targetNode="P_65F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_339F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_342F10254_I" deadCode="false" sourceNode="P_52F10254" targetNode="P_64F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_342F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_342F10254_O" deadCode="false" sourceNode="P_52F10254" targetNode="P_65F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_342F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_346F10254_I" deadCode="false" sourceNode="P_52F10254" targetNode="P_64F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_346F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_346F10254_O" deadCode="false" sourceNode="P_52F10254" targetNode="P_65F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_346F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_350F10254_I" deadCode="false" sourceNode="P_52F10254" targetNode="P_64F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_350F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_350F10254_O" deadCode="false" sourceNode="P_52F10254" targetNode="P_65F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_350F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_354F10254_I" deadCode="false" sourceNode="P_52F10254" targetNode="P_64F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_354F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_354F10254_O" deadCode="false" sourceNode="P_52F10254" targetNode="P_65F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_354F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_358F10254_I" deadCode="false" sourceNode="P_52F10254" targetNode="P_64F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_358F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_358F10254_O" deadCode="false" sourceNode="P_52F10254" targetNode="P_65F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_358F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_362F10254_I" deadCode="false" sourceNode="P_52F10254" targetNode="P_64F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_362F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_362F10254_O" deadCode="false" sourceNode="P_52F10254" targetNode="P_65F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_362F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_366F10254_I" deadCode="false" sourceNode="P_52F10254" targetNode="P_64F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_366F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_366F10254_O" deadCode="false" sourceNode="P_52F10254" targetNode="P_65F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_366F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_370F10254_I" deadCode="false" sourceNode="P_52F10254" targetNode="P_64F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_370F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_370F10254_O" deadCode="false" sourceNode="P_52F10254" targetNode="P_65F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_370F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_374F10254_I" deadCode="false" sourceNode="P_52F10254" targetNode="P_64F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_374F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_374F10254_O" deadCode="false" sourceNode="P_52F10254" targetNode="P_65F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_374F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_378F10254_I" deadCode="false" sourceNode="P_52F10254" targetNode="P_64F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_378F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_378F10254_O" deadCode="false" sourceNode="P_52F10254" targetNode="P_65F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_378F10254"/>
	</edges>
	<edges id="P_52F10254P_53F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10254" targetNode="P_53F10254"/>
	<edges id="P_45F10254P_46F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10254" targetNode="P_46F10254"/>
	<edges id="P_47F10254P_48F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10254" targetNode="P_48F10254"/>
	<edges id="P_54F10254P_55F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10254" targetNode="P_55F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_387F10254_I" deadCode="false" sourceNode="P_10F10254" targetNode="P_66F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_387F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_387F10254_O" deadCode="false" sourceNode="P_10F10254" targetNode="P_67F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_387F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_389F10254_I" deadCode="false" sourceNode="P_10F10254" targetNode="P_68F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_389F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_389F10254_O" deadCode="false" sourceNode="P_10F10254" targetNode="P_69F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_389F10254"/>
	</edges>
	<edges id="P_10F10254P_11F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10254" targetNode="P_11F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_394F10254_I" deadCode="false" sourceNode="P_66F10254" targetNode="P_61F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_394F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_394F10254_O" deadCode="false" sourceNode="P_66F10254" targetNode="P_62F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_394F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_399F10254_I" deadCode="false" sourceNode="P_66F10254" targetNode="P_61F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_399F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_399F10254_O" deadCode="false" sourceNode="P_66F10254" targetNode="P_62F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_399F10254"/>
	</edges>
	<edges id="P_66F10254P_67F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10254" targetNode="P_67F10254"/>
	<edges id="P_68F10254P_69F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10254" targetNode="P_69F10254"/>
	<edges id="P_61F10254P_62F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10254" targetNode="P_62F10254"/>
	<edges xsi:type="cbl:PerformEdge" id="S_428F10254_I" deadCode="false" sourceNode="P_64F10254" targetNode="P_72F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_428F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_428F10254_O" deadCode="false" sourceNode="P_64F10254" targetNode="P_73F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_428F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_429F10254_I" deadCode="false" sourceNode="P_64F10254" targetNode="P_74F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_429F10254"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_429F10254_O" deadCode="false" sourceNode="P_64F10254" targetNode="P_75F10254">
		<representations href="../../../cobol/LDBSD600.cbl.cobModel#S_429F10254"/>
	</edges>
	<edges id="P_64F10254P_65F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10254" targetNode="P_65F10254"/>
	<edges id="P_72F10254P_73F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10254" targetNode="P_73F10254"/>
	<edges id="P_74F10254P_75F10254" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10254" targetNode="P_75F10254"/>
	<edges xsi:type="cbl:DataEdge" id="S_67F10254_POS1" deadCode="false" targetNode="P_14F10254" sourceNode="DB2_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10254"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_67F10254_POS2" deadCode="false" targetNode="P_14F10254" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10254"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_75F10254_POS1" deadCode="false" targetNode="P_16F10254" sourceNode="DB2_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_75F10254"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_75F10254_POS2" deadCode="false" targetNode="P_16F10254" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_75F10254"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_78F10254_POS1" deadCode="false" targetNode="P_18F10254" sourceNode="DB2_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10254"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_78F10254_POS2" deadCode="false" targetNode="P_18F10254" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10254"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_85F10254_POS1" deadCode="false" targetNode="P_22F10254" sourceNode="DB2_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_85F10254"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_85F10254_POS2" deadCode="false" targetNode="P_22F10254" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_85F10254"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_103F10254_POS1" deadCode="false" targetNode="P_24F10254" sourceNode="DB2_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_103F10254"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_103F10254_POS2" deadCode="false" targetNode="P_24F10254" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_103F10254"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10254_POS1" deadCode="false" targetNode="P_26F10254" sourceNode="DB2_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10254"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10254_POS2" deadCode="false" targetNode="P_26F10254" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10254"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_114F10254_POS1" deadCode="false" targetNode="P_28F10254" sourceNode="DB2_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10254"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_114F10254_POS2" deadCode="false" targetNode="P_28F10254" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10254"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_121F10254_POS1" deadCode="false" targetNode="P_32F10254" sourceNode="DB2_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_121F10254"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_121F10254_POS2" deadCode="false" targetNode="P_32F10254" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_121F10254"></representations>
	</edges>
</Package>

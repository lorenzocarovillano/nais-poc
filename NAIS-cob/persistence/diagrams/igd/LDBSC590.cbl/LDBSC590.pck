<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBSC590" cbl:id="LDBSC590" xsi:id="LDBSC590" packageRef="LDBSC590.igd#LDBSC590" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBSC590_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10250" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBSC590.cbl.cobModel#SC_1F10250"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10250" deadCode="false" name="PROGRAM_LDBSC590_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_1F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10250" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_2F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10250" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_3F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10250" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_12F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10250" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_13F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10250" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_4F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10250" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_5F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10250" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_6F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10250" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_7F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10250" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_8F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10250" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_9F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10250" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_44F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10250" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_49F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10250" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_14F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10250" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_15F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10250" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_16F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10250" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_17F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10250" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_18F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10250" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_19F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10250" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_20F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10250" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_21F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10250" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_22F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10250" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_23F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10250" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_50F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10250" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_51F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10250" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_24F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10250" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_25F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10250" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_26F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10250" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_27F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10250" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_28F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10250" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_29F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10250" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_30F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10250" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_31F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10250" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_32F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10250" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_33F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10250" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_52F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10250" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_53F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10250" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_34F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10250" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_35F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10250" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_36F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10250" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_37F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10250" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_38F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10250" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_39F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10250" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_40F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10250" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_41F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10250" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_42F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10250" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_43F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10250" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_54F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10250" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_55F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10250" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_60F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10250" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_63F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10250" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_56F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10250" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_57F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10250" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_45F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10250" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_46F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10250" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_47F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10250" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_48F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10250" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_58F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10250" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_59F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10250" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_10F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10250" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_11F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10250" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_66F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10250" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_67F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10250" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_68F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10250" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_69F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10250" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_61F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10250" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_62F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10250" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_70F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10250" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_71F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10250" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_64F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10250" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_65F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10250" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_72F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10250" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_73F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10250" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_74F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10250" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_75F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10250" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_76F10250"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10250" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBSC590.cbl.cobModel#P_77F10250"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_MOVI" name="MOVI">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_MOVI"/>
		</children>
	</packageNode>
	<edges id="SC_1F10250P_1F10250" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10250" targetNode="P_1F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10250_I" deadCode="false" sourceNode="P_1F10250" targetNode="P_2F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_2F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10250_O" deadCode="false" sourceNode="P_1F10250" targetNode="P_3F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_2F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10250_I" deadCode="false" sourceNode="P_1F10250" targetNode="P_4F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_6F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10250_O" deadCode="false" sourceNode="P_1F10250" targetNode="P_5F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_6F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10250_I" deadCode="false" sourceNode="P_1F10250" targetNode="P_6F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_10F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10250_O" deadCode="false" sourceNode="P_1F10250" targetNode="P_7F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_10F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10250_I" deadCode="false" sourceNode="P_1F10250" targetNode="P_8F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_14F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10250_O" deadCode="false" sourceNode="P_1F10250" targetNode="P_9F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_14F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10250_I" deadCode="false" sourceNode="P_2F10250" targetNode="P_10F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_24F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10250_O" deadCode="false" sourceNode="P_2F10250" targetNode="P_11F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_24F10250"/>
	</edges>
	<edges id="P_2F10250P_3F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10250" targetNode="P_3F10250"/>
	<edges id="P_12F10250P_13F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10250" targetNode="P_13F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10250_I" deadCode="false" sourceNode="P_4F10250" targetNode="P_14F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_37F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10250_O" deadCode="false" sourceNode="P_4F10250" targetNode="P_15F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_37F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10250_I" deadCode="false" sourceNode="P_4F10250" targetNode="P_16F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_38F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10250_O" deadCode="false" sourceNode="P_4F10250" targetNode="P_17F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_38F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10250_I" deadCode="false" sourceNode="P_4F10250" targetNode="P_18F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_39F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10250_O" deadCode="false" sourceNode="P_4F10250" targetNode="P_19F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_39F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10250_I" deadCode="false" sourceNode="P_4F10250" targetNode="P_20F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_40F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10250_O" deadCode="false" sourceNode="P_4F10250" targetNode="P_21F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_40F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10250_I" deadCode="false" sourceNode="P_4F10250" targetNode="P_22F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_41F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10250_O" deadCode="false" sourceNode="P_4F10250" targetNode="P_23F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_41F10250"/>
	</edges>
	<edges id="P_4F10250P_5F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10250" targetNode="P_5F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10250_I" deadCode="false" sourceNode="P_6F10250" targetNode="P_24F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_45F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10250_O" deadCode="false" sourceNode="P_6F10250" targetNode="P_25F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_45F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10250_I" deadCode="false" sourceNode="P_6F10250" targetNode="P_26F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_46F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10250_O" deadCode="false" sourceNode="P_6F10250" targetNode="P_27F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_46F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10250_I" deadCode="false" sourceNode="P_6F10250" targetNode="P_28F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_47F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10250_O" deadCode="false" sourceNode="P_6F10250" targetNode="P_29F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_47F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10250_I" deadCode="false" sourceNode="P_6F10250" targetNode="P_30F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_48F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10250_O" deadCode="false" sourceNode="P_6F10250" targetNode="P_31F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_48F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10250_I" deadCode="false" sourceNode="P_6F10250" targetNode="P_32F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_49F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10250_O" deadCode="false" sourceNode="P_6F10250" targetNode="P_33F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_49F10250"/>
	</edges>
	<edges id="P_6F10250P_7F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10250" targetNode="P_7F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10250_I" deadCode="false" sourceNode="P_8F10250" targetNode="P_34F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_53F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10250_O" deadCode="false" sourceNode="P_8F10250" targetNode="P_35F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_53F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10250_I" deadCode="false" sourceNode="P_8F10250" targetNode="P_36F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_54F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10250_O" deadCode="false" sourceNode="P_8F10250" targetNode="P_37F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_54F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10250_I" deadCode="false" sourceNode="P_8F10250" targetNode="P_38F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_55F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10250_O" deadCode="false" sourceNode="P_8F10250" targetNode="P_39F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_55F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10250_I" deadCode="false" sourceNode="P_8F10250" targetNode="P_40F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_56F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10250_O" deadCode="false" sourceNode="P_8F10250" targetNode="P_41F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_56F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10250_I" deadCode="false" sourceNode="P_8F10250" targetNode="P_42F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_57F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10250_O" deadCode="false" sourceNode="P_8F10250" targetNode="P_43F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_57F10250"/>
	</edges>
	<edges id="P_8F10250P_9F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10250" targetNode="P_9F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10250_I" deadCode="false" sourceNode="P_44F10250" targetNode="P_45F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_60F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10250_O" deadCode="false" sourceNode="P_44F10250" targetNode="P_46F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_60F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10250_I" deadCode="false" sourceNode="P_44F10250" targetNode="P_47F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_61F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10250_O" deadCode="false" sourceNode="P_44F10250" targetNode="P_48F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_61F10250"/>
	</edges>
	<edges id="P_44F10250P_49F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10250" targetNode="P_49F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10250_I" deadCode="false" sourceNode="P_14F10250" targetNode="P_45F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_64F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10250_O" deadCode="false" sourceNode="P_14F10250" targetNode="P_46F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_64F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10250_I" deadCode="false" sourceNode="P_14F10250" targetNode="P_47F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_65F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10250_O" deadCode="false" sourceNode="P_14F10250" targetNode="P_48F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_65F10250"/>
	</edges>
	<edges id="P_14F10250P_15F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10250" targetNode="P_15F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10250_I" deadCode="false" sourceNode="P_16F10250" targetNode="P_44F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_68F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10250_O" deadCode="false" sourceNode="P_16F10250" targetNode="P_49F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_68F10250"/>
	</edges>
	<edges id="P_16F10250P_17F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10250" targetNode="P_17F10250"/>
	<edges id="P_18F10250P_19F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10250" targetNode="P_19F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10250_I" deadCode="false" sourceNode="P_20F10250" targetNode="P_16F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_73F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10250_O" deadCode="false" sourceNode="P_20F10250" targetNode="P_17F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_73F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10250_I" deadCode="false" sourceNode="P_20F10250" targetNode="P_22F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_75F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10250_O" deadCode="false" sourceNode="P_20F10250" targetNode="P_23F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_75F10250"/>
	</edges>
	<edges id="P_20F10250P_21F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10250" targetNode="P_21F10250"/>
	<edges id="P_22F10250P_23F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10250" targetNode="P_23F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10250_I" deadCode="false" sourceNode="P_50F10250" targetNode="P_45F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_79F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10250_O" deadCode="false" sourceNode="P_50F10250" targetNode="P_46F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_79F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10250_I" deadCode="false" sourceNode="P_50F10250" targetNode="P_47F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_80F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10250_O" deadCode="false" sourceNode="P_50F10250" targetNode="P_48F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_80F10250"/>
	</edges>
	<edges id="P_50F10250P_51F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10250" targetNode="P_51F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10250_I" deadCode="false" sourceNode="P_24F10250" targetNode="P_45F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_83F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10250_O" deadCode="false" sourceNode="P_24F10250" targetNode="P_46F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_83F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10250_I" deadCode="false" sourceNode="P_24F10250" targetNode="P_47F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_84F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10250_O" deadCode="false" sourceNode="P_24F10250" targetNode="P_48F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_84F10250"/>
	</edges>
	<edges id="P_24F10250P_25F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10250" targetNode="P_25F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10250_I" deadCode="false" sourceNode="P_26F10250" targetNode="P_50F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_87F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10250_O" deadCode="false" sourceNode="P_26F10250" targetNode="P_51F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_87F10250"/>
	</edges>
	<edges id="P_26F10250P_27F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10250" targetNode="P_27F10250"/>
	<edges id="P_28F10250P_29F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10250" targetNode="P_29F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10250_I" deadCode="false" sourceNode="P_30F10250" targetNode="P_26F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_92F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10250_O" deadCode="false" sourceNode="P_30F10250" targetNode="P_27F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_92F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10250_I" deadCode="false" sourceNode="P_30F10250" targetNode="P_32F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_94F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10250_O" deadCode="false" sourceNode="P_30F10250" targetNode="P_33F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_94F10250"/>
	</edges>
	<edges id="P_30F10250P_31F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10250" targetNode="P_31F10250"/>
	<edges id="P_32F10250P_33F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10250" targetNode="P_33F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10250_I" deadCode="false" sourceNode="P_52F10250" targetNode="P_45F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_98F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10250_O" deadCode="false" sourceNode="P_52F10250" targetNode="P_46F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_98F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10250_I" deadCode="false" sourceNode="P_52F10250" targetNode="P_47F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_99F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10250_O" deadCode="false" sourceNode="P_52F10250" targetNode="P_48F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_99F10250"/>
	</edges>
	<edges id="P_52F10250P_53F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10250" targetNode="P_53F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10250_I" deadCode="false" sourceNode="P_34F10250" targetNode="P_45F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_103F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10250_O" deadCode="false" sourceNode="P_34F10250" targetNode="P_46F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_103F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10250_I" deadCode="false" sourceNode="P_34F10250" targetNode="P_47F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_104F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10250_O" deadCode="false" sourceNode="P_34F10250" targetNode="P_48F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_104F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10250_I" deadCode="false" sourceNode="P_34F10250" targetNode="P_12F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_106F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10250_O" deadCode="false" sourceNode="P_34F10250" targetNode="P_13F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_106F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10250_I" deadCode="false" sourceNode="P_34F10250" targetNode="P_54F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_108F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10250_O" deadCode="false" sourceNode="P_34F10250" targetNode="P_55F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_108F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10250_I" deadCode="false" sourceNode="P_34F10250" targetNode="P_56F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_109F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10250_O" deadCode="false" sourceNode="P_34F10250" targetNode="P_57F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_109F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10250_I" deadCode="false" sourceNode="P_34F10250" targetNode="P_58F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_110F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10250_O" deadCode="false" sourceNode="P_34F10250" targetNode="P_59F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_110F10250"/>
	</edges>
	<edges id="P_34F10250P_35F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10250" targetNode="P_35F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10250_I" deadCode="false" sourceNode="P_36F10250" targetNode="P_52F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_112F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10250_O" deadCode="false" sourceNode="P_36F10250" targetNode="P_53F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_112F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10250_I" deadCode="false" sourceNode="P_36F10250" targetNode="P_12F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_114F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10250_O" deadCode="false" sourceNode="P_36F10250" targetNode="P_13F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_114F10250"/>
	</edges>
	<edges id="P_36F10250P_37F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10250" targetNode="P_37F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10250_I" deadCode="false" sourceNode="P_38F10250" targetNode="P_12F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_117F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10250_O" deadCode="false" sourceNode="P_38F10250" targetNode="P_13F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_117F10250"/>
	</edges>
	<edges id="P_38F10250P_39F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10250" targetNode="P_39F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10250_I" deadCode="false" sourceNode="P_40F10250" targetNode="P_36F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_119F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10250_O" deadCode="false" sourceNode="P_40F10250" targetNode="P_37F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_119F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10250_I" deadCode="false" sourceNode="P_40F10250" targetNode="P_42F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_121F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10250_O" deadCode="false" sourceNode="P_40F10250" targetNode="P_43F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_121F10250"/>
	</edges>
	<edges id="P_40F10250P_41F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10250" targetNode="P_41F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10250_I" deadCode="false" sourceNode="P_42F10250" targetNode="P_12F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_124F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10250_O" deadCode="false" sourceNode="P_42F10250" targetNode="P_13F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_124F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10250_I" deadCode="false" sourceNode="P_42F10250" targetNode="P_54F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_126F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10250_O" deadCode="false" sourceNode="P_42F10250" targetNode="P_55F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_126F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10250_I" deadCode="false" sourceNode="P_42F10250" targetNode="P_56F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_127F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10250_O" deadCode="false" sourceNode="P_42F10250" targetNode="P_57F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_127F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10250_I" deadCode="false" sourceNode="P_42F10250" targetNode="P_58F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_128F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10250_O" deadCode="false" sourceNode="P_42F10250" targetNode="P_59F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_128F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10250_I" deadCode="false" sourceNode="P_42F10250" targetNode="P_38F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_130F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10250_O" deadCode="false" sourceNode="P_42F10250" targetNode="P_39F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_130F10250"/>
	</edges>
	<edges id="P_42F10250P_43F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10250" targetNode="P_43F10250"/>
	<edges id="P_54F10250P_55F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10250" targetNode="P_55F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10250_I" deadCode="true" sourceNode="P_60F10250" targetNode="P_61F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_153F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10250_O" deadCode="true" sourceNode="P_60F10250" targetNode="P_62F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_153F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10250_I" deadCode="false" sourceNode="P_56F10250" targetNode="P_64F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_157F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10250_O" deadCode="false" sourceNode="P_56F10250" targetNode="P_65F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_157F10250"/>
	</edges>
	<edges id="P_56F10250P_57F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10250" targetNode="P_57F10250"/>
	<edges id="P_45F10250P_46F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10250" targetNode="P_46F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10250_I" deadCode="false" sourceNode="P_47F10250" targetNode="P_61F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_163F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10250_O" deadCode="false" sourceNode="P_47F10250" targetNode="P_62F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_163F10250"/>
	</edges>
	<edges id="P_47F10250P_48F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10250" targetNode="P_48F10250"/>
	<edges id="P_58F10250P_59F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10250" targetNode="P_59F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10250_I" deadCode="false" sourceNode="P_10F10250" targetNode="P_66F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_168F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10250_O" deadCode="false" sourceNode="P_10F10250" targetNode="P_67F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_168F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10250_I" deadCode="false" sourceNode="P_10F10250" targetNode="P_68F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_170F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10250_O" deadCode="false" sourceNode="P_10F10250" targetNode="P_69F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_170F10250"/>
	</edges>
	<edges id="P_10F10250P_11F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10250" targetNode="P_11F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10250_I" deadCode="false" sourceNode="P_66F10250" targetNode="P_61F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_175F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10250_O" deadCode="false" sourceNode="P_66F10250" targetNode="P_62F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_175F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_180F10250_I" deadCode="false" sourceNode="P_66F10250" targetNode="P_61F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_180F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_180F10250_O" deadCode="false" sourceNode="P_66F10250" targetNode="P_62F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_180F10250"/>
	</edges>
	<edges id="P_66F10250P_67F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10250" targetNode="P_67F10250"/>
	<edges id="P_68F10250P_69F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10250" targetNode="P_69F10250"/>
	<edges id="P_61F10250P_62F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10250" targetNode="P_62F10250"/>
	<edges xsi:type="cbl:PerformEdge" id="S_209F10250_I" deadCode="false" sourceNode="P_64F10250" targetNode="P_72F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_209F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_209F10250_O" deadCode="false" sourceNode="P_64F10250" targetNode="P_73F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_209F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_210F10250_I" deadCode="false" sourceNode="P_64F10250" targetNode="P_74F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_210F10250"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_210F10250_O" deadCode="false" sourceNode="P_64F10250" targetNode="P_75F10250">
		<representations href="../../../cobol/LDBSC590.cbl.cobModel#S_210F10250"/>
	</edges>
	<edges id="P_64F10250P_65F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10250" targetNode="P_65F10250"/>
	<edges id="P_72F10250P_73F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10250" targetNode="P_73F10250"/>
	<edges id="P_74F10250P_75F10250" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10250" targetNode="P_75F10250"/>
	<edges xsi:type="cbl:DataEdge" id="S_105F10250_POS1" deadCode="false" targetNode="P_34F10250" sourceNode="DB2_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_105F10250"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_113F10250_POS1" deadCode="false" targetNode="P_36F10250" sourceNode="DB2_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_113F10250"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_116F10250_POS1" deadCode="false" targetNode="P_38F10250" sourceNode="DB2_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_116F10250"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_123F10250_POS1" deadCode="false" targetNode="P_42F10250" sourceNode="DB2_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_123F10250"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0000" cbl:id="LVVS0000" xsi:id="LVVS0000" packageRef="LVVS0000.igd#LVVS0000" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0000_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10304" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0000.cbl.cobModel#SC_1F10304"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10304" deadCode="false" name="PROGRAM_LVVS0000_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_1F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10304" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_2F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10304" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_3F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10304" deadCode="false" name="S0005-CTRL-DATI-INPUT-1">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_8F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10304" deadCode="false" name="EX-S0005">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_9F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10304" deadCode="false" name="S0010-CTRL-DATI-INPUT-2">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_10F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10304" deadCode="false" name="EX-S0010">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_11F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10304" deadCode="false" name="S0015-CTRL-DATI-INPUT-3">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_12F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10304" deadCode="false" name="EX-S0015">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_13F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10304" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_4F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10304" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_5F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10304" deadCode="false" name="C100-CONVERTI-FORMAT-1">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_14F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10304" deadCode="false" name="C100-EX">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_15F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10304" deadCode="false" name="C200-CONVERTI-FORMAT-2">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_16F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10304" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_17F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10304" deadCode="false" name="C300-CONVERTI-FORMAT-3">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_18F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10304" deadCode="false" name="C300-EX">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_19F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10304" deadCode="true" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_20F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10304" deadCode="true" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_21F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10304" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_6F10304"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10304" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0000.cbl.cobModel#P_7F10304"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10304P_1F10304" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10304" targetNode="P_1F10304"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10304_I" deadCode="false" sourceNode="P_1F10304" targetNode="P_2F10304">
		<representations href="../../../cobol/LVVS0000.cbl.cobModel#S_1F10304"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10304_O" deadCode="false" sourceNode="P_1F10304" targetNode="P_3F10304">
		<representations href="../../../cobol/LVVS0000.cbl.cobModel#S_1F10304"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10304_I" deadCode="false" sourceNode="P_1F10304" targetNode="P_4F10304">
		<representations href="../../../cobol/LVVS0000.cbl.cobModel#S_3F10304"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10304_O" deadCode="false" sourceNode="P_1F10304" targetNode="P_5F10304">
		<representations href="../../../cobol/LVVS0000.cbl.cobModel#S_3F10304"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10304_I" deadCode="false" sourceNode="P_1F10304" targetNode="P_6F10304">
		<representations href="../../../cobol/LVVS0000.cbl.cobModel#S_4F10304"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10304_O" deadCode="false" sourceNode="P_1F10304" targetNode="P_7F10304">
		<representations href="../../../cobol/LVVS0000.cbl.cobModel#S_4F10304"/>
	</edges>
	<edges id="P_2F10304P_3F10304" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10304" targetNode="P_3F10304"/>
	<edges id="P_8F10304P_9F10304" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10304" targetNode="P_9F10304"/>
	<edges id="P_10F10304P_11F10304" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10304" targetNode="P_11F10304"/>
	<edges id="P_12F10304P_13F10304" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10304" targetNode="P_13F10304"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10304_I" deadCode="false" sourceNode="P_4F10304" targetNode="P_8F10304">
		<representations href="../../../cobol/LVVS0000.cbl.cobModel#S_45F10304"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10304_O" deadCode="false" sourceNode="P_4F10304" targetNode="P_9F10304">
		<representations href="../../../cobol/LVVS0000.cbl.cobModel#S_45F10304"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10304_I" deadCode="false" sourceNode="P_4F10304" targetNode="P_14F10304">
		<representations href="../../../cobol/LVVS0000.cbl.cobModel#S_47F10304"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10304_O" deadCode="false" sourceNode="P_4F10304" targetNode="P_15F10304">
		<representations href="../../../cobol/LVVS0000.cbl.cobModel#S_47F10304"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10304_I" deadCode="false" sourceNode="P_4F10304" targetNode="P_10F10304">
		<representations href="../../../cobol/LVVS0000.cbl.cobModel#S_48F10304"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10304_O" deadCode="false" sourceNode="P_4F10304" targetNode="P_11F10304">
		<representations href="../../../cobol/LVVS0000.cbl.cobModel#S_48F10304"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10304_I" deadCode="false" sourceNode="P_4F10304" targetNode="P_16F10304">
		<representations href="../../../cobol/LVVS0000.cbl.cobModel#S_50F10304"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10304_O" deadCode="false" sourceNode="P_4F10304" targetNode="P_17F10304">
		<representations href="../../../cobol/LVVS0000.cbl.cobModel#S_50F10304"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10304_I" deadCode="false" sourceNode="P_4F10304" targetNode="P_12F10304">
		<representations href="../../../cobol/LVVS0000.cbl.cobModel#S_51F10304"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10304_O" deadCode="false" sourceNode="P_4F10304" targetNode="P_13F10304">
		<representations href="../../../cobol/LVVS0000.cbl.cobModel#S_51F10304"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10304_I" deadCode="false" sourceNode="P_4F10304" targetNode="P_18F10304">
		<representations href="../../../cobol/LVVS0000.cbl.cobModel#S_53F10304"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10304_O" deadCode="false" sourceNode="P_4F10304" targetNode="P_19F10304">
		<representations href="../../../cobol/LVVS0000.cbl.cobModel#S_53F10304"/>
	</edges>
	<edges id="P_4F10304P_5F10304" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10304" targetNode="P_5F10304"/>
	<edges id="P_14F10304P_15F10304" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10304" targetNode="P_15F10304"/>
	<edges id="P_16F10304P_17F10304" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10304" targetNode="P_17F10304"/>
	<edges id="P_18F10304P_19F10304" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10304" targetNode="P_19F10304"/>
</Package>

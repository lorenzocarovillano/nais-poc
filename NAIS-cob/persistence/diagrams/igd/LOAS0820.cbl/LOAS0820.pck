<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LOAS0820" cbl:id="LOAS0820" xsi:id="LOAS0820" packageRef="LOAS0820.igd#LOAS0820" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LOAS0820_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10294" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LOAS0820.cbl.cobModel#SC_1F10294"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10294" deadCode="false" name="PROGRAM_LOAS0820_FIRST_SENTENCES">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_1F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10294" deadCode="false" name="S00000-OPERAZ-INIZIALI">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_2F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10294" deadCode="false" name="S00000-OPERAZ-INIZIALI-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_3F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10294" deadCode="false" name="S00100-OPEN-OUT">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_10F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10294" deadCode="false" name="S00100-OPEN-OUT-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_11F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10294" deadCode="false" name="S00200-CTRL-INPUT">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_12F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10294" deadCode="false" name="S00200-CTRL-INPUT-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_13F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10294" deadCode="false" name="S10000-ELABORAZIONE">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_4F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10294" deadCode="false" name="S10000-ELABORAZIONE-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_5F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10294" deadCode="false" name="S11000-RECORD-TESTATA">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_16F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10294" deadCode="false" name="S11000-RECORD-TESTATA-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_17F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10294" deadCode="false" name="S11999-WRITE-REC-T">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_20F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10294" deadCode="false" name="S11999-WRITE-REC-T-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_21F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10294" deadCode="false" name="S12000-RECORD-DATI">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_18F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10294" deadCode="false" name="S12000-RECORD-DATI-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_19F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10294" deadCode="false" name="S12100-DATI-PARAM-MOVI">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_24F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10294" deadCode="false" name="S12100-DATI-PARAM-MOVI-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_25F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10294" deadCode="false" name="S12200-DATI-TRANCHE">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_26F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10294" deadCode="false" name="S12200-DATI-TRANCHE-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_27F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10294" deadCode="false" name="S12300-DATI-RAPP-RETE">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_28F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10294" deadCode="false" name="S12300-DATI-RAPP-RETE-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_29F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10294" deadCode="false" name="S12310-IMPOSTA-RRE">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_34F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10294" deadCode="false" name="S12310-IMPOSTA-RRE-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_35F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10294" deadCode="false" name="S12320-LEGGI-RRE">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_36F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10294" deadCode="false" name="S12320-LEGGI-RRE-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_37F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10294" deadCode="false" name="S12400-DATI-MOVI">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_30F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10294" deadCode="false" name="S12400-DATI-MOVI-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_31F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10294" deadCode="false" name="S12410-IMPOSTA-MOVI">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_40F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10294" deadCode="false" name="S12410-IMPOSTA-MOVI-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_41F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10294" deadCode="false" name="S12420-LEGGI-MOVI">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_42F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10294" deadCode="false" name="S12420-LEGGI-MOVI-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_43F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10294" deadCode="false" name="S12430-TEST-MOVI">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_44F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10294" deadCode="false" name="S12430-TEST-MOVI-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_45F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10294" deadCode="false" name="S12440-CERCA-IMP-RISC-PARZ">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_46F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10294" deadCode="false" name="S12440-CERCA-IMP-RISC-PARZ-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_47F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10294" deadCode="false" name="S12450-IMPOSTA-TRANCHE-LIQ">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_48F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10294" deadCode="false" name="S12450-IMPOSTA-TRANCHE-LIQ-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_49F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10294" deadCode="false" name="S12460-LEGGI-TRANCHE-LIQ">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_50F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10294" deadCode="false" name="S12460-LEGGI-TRANCHE-LIQ-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_51F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10294" deadCode="false" name="S12465-CONTROLLA-MOVIMENTO">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_52F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10294" deadCode="false" name="S12465-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_53F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10294" deadCode="false" name="S12466-MOV-TROVATI">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_54F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10294" deadCode="false" name="S12466-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_55F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10294" deadCode="false" name="S12470-TEST-LEGAME-GAR">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_56F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10294" deadCode="false" name="S12470-TEST-LEGAME-GAR-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_57F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10294" deadCode="false" name="S12999-WRITE-REC-D">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_32F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10294" deadCode="false" name="S12999-WRITE-REC-D-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_33F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10294" deadCode="false" name="S90000-OPERAZ-FINALI">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_6F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10294" deadCode="false" name="S90000-OPERAZ-FINALI-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_7F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10294" deadCode="false" name="S90100-CLOSE-OUT">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_58F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10294" deadCode="false" name="S90100-CLOSE-OUT-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_59F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10294" deadCode="false" name="S99999-CONV-N-TO-X">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_22F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10294" deadCode="false" name="S99999-CONV-N-TO-X-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_23F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10294" deadCode="false" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_38F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10294" deadCode="false" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_39F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10294" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_14F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10294" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_15F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10294" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_62F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10294" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_63F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10294" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_60F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10294" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_61F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10294" deadCode="true" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_64F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10294" deadCode="true" name="EX-S0290">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_65F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10294" deadCode="true" name="S0320-OUTPUT-PRODOTTO">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_66F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10294" deadCode="true" name="EX-S0320-OUTPUT-PRODOTTO">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_71F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10294" deadCode="true" name="S0322-SALVA-ERRORE">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_67F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10294" deadCode="true" name="EX-S0322">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_68F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10294" deadCode="true" name="S0321-ERRORI-DEROGA">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_69F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10294" deadCode="true" name="EX-S0321">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_70F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10294" deadCode="true" name="S0323-SALVA-DEROGA">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_72F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10294" deadCode="true" name="EX-S0323">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_73F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10294" deadCode="false" name="DISPLAY-LABEL">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_8F10294"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10294" deadCode="false" name="DISPLAY-LABEL-EX">
				<representations href="../../../cobol/LOAS0820.cbl.cobModel#P_9F10294"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0010" name="IDSS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10099"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
		<children xsi:type="cbl:VsamDataNode" id="V_OUMANFEE_LOAS0820" name="OUMANFEE[LOAS0820]">
			<representations href="../../../explorer/storage-explorer.xml.storage#OUMANFEE_LOAS0820"/>
		</children>
	</packageNode>
	<edges id="SC_1F10294P_1F10294" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10294" targetNode="P_1F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10294_I" deadCode="false" sourceNode="P_1F10294" targetNode="P_2F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_1F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10294_O" deadCode="false" sourceNode="P_1F10294" targetNode="P_3F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_1F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10294_I" deadCode="false" sourceNode="P_1F10294" targetNode="P_4F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_3F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10294_O" deadCode="false" sourceNode="P_1F10294" targetNode="P_5F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_3F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10294_I" deadCode="false" sourceNode="P_1F10294" targetNode="P_6F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_4F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10294_O" deadCode="false" sourceNode="P_1F10294" targetNode="P_7F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_4F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10294_I" deadCode="false" sourceNode="P_2F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_8F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10294_O" deadCode="false" sourceNode="P_2F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_8F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10294_I" deadCode="false" sourceNode="P_2F10294" targetNode="P_10F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_9F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10294_O" deadCode="false" sourceNode="P_2F10294" targetNode="P_11F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_9F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10294_I" deadCode="false" sourceNode="P_2F10294" targetNode="P_12F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_10F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10294_O" deadCode="false" sourceNode="P_2F10294" targetNode="P_13F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_10F10294"/>
	</edges>
	<edges id="P_2F10294P_3F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10294" targetNode="P_3F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10294_I" deadCode="false" sourceNode="P_10F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_14F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10294_O" deadCode="false" sourceNode="P_10F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_14F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10294_I" deadCode="false" sourceNode="P_10F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_21F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10294_O" deadCode="false" sourceNode="P_10F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_21F10294"/>
	</edges>
	<edges id="P_10F10294P_11F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10294" targetNode="P_11F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10294_I" deadCode="false" sourceNode="P_12F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_25F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10294_O" deadCode="false" sourceNode="P_12F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_25F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10294_I" deadCode="false" sourceNode="P_12F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_32F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10294_O" deadCode="false" sourceNode="P_12F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_32F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10294_I" deadCode="false" sourceNode="P_12F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_38F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10294_O" deadCode="false" sourceNode="P_12F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_38F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10294_I" deadCode="false" sourceNode="P_12F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_44F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10294_O" deadCode="false" sourceNode="P_12F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_44F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10294_I" deadCode="false" sourceNode="P_12F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_50F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10294_O" deadCode="false" sourceNode="P_12F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_50F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10294_I" deadCode="false" sourceNode="P_12F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_56F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10294_O" deadCode="false" sourceNode="P_12F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_56F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10294_I" deadCode="false" sourceNode="P_12F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_62F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10294_O" deadCode="false" sourceNode="P_12F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_62F10294"/>
	</edges>
	<edges id="P_12F10294P_13F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10294" targetNode="P_13F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10294_I" deadCode="false" sourceNode="P_4F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_65F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10294_O" deadCode="false" sourceNode="P_4F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_65F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10294_I" deadCode="false" sourceNode="P_4F10294" targetNode="P_16F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_67F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10294_O" deadCode="false" sourceNode="P_4F10294" targetNode="P_17F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_67F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10294_I" deadCode="false" sourceNode="P_4F10294" targetNode="P_18F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_69F10294"/>
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_70F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10294_O" deadCode="false" sourceNode="P_4F10294" targetNode="P_19F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_69F10294"/>
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_70F10294"/>
	</edges>
	<edges id="P_4F10294P_5F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10294" targetNode="P_5F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10294_I" deadCode="false" sourceNode="P_16F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_73F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10294_O" deadCode="false" sourceNode="P_16F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_73F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10294_I" deadCode="false" sourceNode="P_16F10294" targetNode="P_20F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_74F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10294_O" deadCode="false" sourceNode="P_16F10294" targetNode="P_21F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_74F10294"/>
	</edges>
	<edges id="P_16F10294P_17F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10294" targetNode="P_17F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10294_I" deadCode="false" sourceNode="P_20F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_77F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10294_O" deadCode="false" sourceNode="P_20F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_77F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10294_I" deadCode="false" sourceNode="P_20F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_84F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10294_O" deadCode="false" sourceNode="P_20F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_84F10294"/>
	</edges>
	<edges id="P_20F10294P_21F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10294" targetNode="P_21F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10294_I" deadCode="false" sourceNode="P_18F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_87F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10294_O" deadCode="false" sourceNode="P_18F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_87F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10294_I" deadCode="false" sourceNode="P_18F10294" targetNode="P_22F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_91F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10294_O" deadCode="false" sourceNode="P_18F10294" targetNode="P_23F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_91F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10294_I" deadCode="false" sourceNode="P_18F10294" targetNode="P_22F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_101F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10294_O" deadCode="false" sourceNode="P_18F10294" targetNode="P_23F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_101F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10294_I" deadCode="false" sourceNode="P_18F10294" targetNode="P_22F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_105F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10294_O" deadCode="false" sourceNode="P_18F10294" targetNode="P_23F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_105F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10294_I" deadCode="false" sourceNode="P_18F10294" targetNode="P_22F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_109F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10294_O" deadCode="false" sourceNode="P_18F10294" targetNode="P_23F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_109F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10294_I" deadCode="false" sourceNode="P_18F10294" targetNode="P_24F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_119F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10294_O" deadCode="false" sourceNode="P_18F10294" targetNode="P_25F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_119F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10294_I" deadCode="false" sourceNode="P_18F10294" targetNode="P_26F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_121F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10294_O" deadCode="false" sourceNode="P_18F10294" targetNode="P_27F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_121F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10294_I" deadCode="false" sourceNode="P_18F10294" targetNode="P_28F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_123F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10294_O" deadCode="false" sourceNode="P_18F10294" targetNode="P_29F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_123F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10294_I" deadCode="false" sourceNode="P_18F10294" targetNode="P_30F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_125F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10294_O" deadCode="false" sourceNode="P_18F10294" targetNode="P_31F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_125F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10294_I" deadCode="false" sourceNode="P_18F10294" targetNode="P_32F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_127F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10294_O" deadCode="false" sourceNode="P_18F10294" targetNode="P_33F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_127F10294"/>
	</edges>
	<edges id="P_18F10294P_19F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10294" targetNode="P_19F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10294_I" deadCode="false" sourceNode="P_24F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_130F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10294_O" deadCode="false" sourceNode="P_24F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_130F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10294_I" deadCode="false" sourceNode="P_24F10294" targetNode="P_22F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_131F10294"/>
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_137F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10294_O" deadCode="false" sourceNode="P_24F10294" targetNode="P_23F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_131F10294"/>
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_137F10294"/>
	</edges>
	<edges id="P_24F10294P_25F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10294" targetNode="P_25F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10294_I" deadCode="false" sourceNode="P_26F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_143F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10294_O" deadCode="false" sourceNode="P_26F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_143F10294"/>
	</edges>
	<edges id="P_26F10294P_27F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10294" targetNode="P_27F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10294_I" deadCode="false" sourceNode="P_28F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_160F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10294_O" deadCode="false" sourceNode="P_28F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_160F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10294_I" deadCode="false" sourceNode="P_28F10294" targetNode="P_34F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_161F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10294_O" deadCode="false" sourceNode="P_28F10294" targetNode="P_35F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_161F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10294_I" deadCode="false" sourceNode="P_28F10294" targetNode="P_36F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_162F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10294_O" deadCode="false" sourceNode="P_28F10294" targetNode="P_37F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_162F10294"/>
	</edges>
	<edges id="P_28F10294P_29F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10294" targetNode="P_29F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10294_I" deadCode="false" sourceNode="P_34F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_168F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10294_O" deadCode="false" sourceNode="P_34F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_168F10294"/>
	</edges>
	<edges id="P_34F10294P_35F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10294" targetNode="P_35F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10294_I" deadCode="false" sourceNode="P_36F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_185F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_185F10294_O" deadCode="false" sourceNode="P_36F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_185F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10294_I" deadCode="false" sourceNode="P_36F10294" targetNode="P_38F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_186F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10294_O" deadCode="false" sourceNode="P_36F10294" targetNode="P_39F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_186F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_193F10294_I" deadCode="false" sourceNode="P_36F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_193F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_193F10294_O" deadCode="false" sourceNode="P_36F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_193F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_199F10294_I" deadCode="false" sourceNode="P_36F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_199F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_199F10294_O" deadCode="false" sourceNode="P_36F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_199F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_204F10294_I" deadCode="false" sourceNode="P_36F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_204F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_204F10294_O" deadCode="false" sourceNode="P_36F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_204F10294"/>
	</edges>
	<edges id="P_36F10294P_37F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10294" targetNode="P_37F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_207F10294_I" deadCode="false" sourceNode="P_30F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_207F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_207F10294_O" deadCode="false" sourceNode="P_30F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_207F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10294_I" deadCode="false" sourceNode="P_30F10294" targetNode="P_40F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_213F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10294_O" deadCode="false" sourceNode="P_30F10294" targetNode="P_41F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_213F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_214F10294_I" deadCode="false" sourceNode="P_30F10294" targetNode="P_42F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_214F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_214F10294_O" deadCode="false" sourceNode="P_30F10294" targetNode="P_43F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_214F10294"/>
	</edges>
	<edges id="P_30F10294P_31F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10294" targetNode="P_31F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_223F10294_I" deadCode="false" sourceNode="P_40F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_223F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_223F10294_O" deadCode="false" sourceNode="P_40F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_223F10294"/>
	</edges>
	<edges id="P_40F10294P_41F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10294" targetNode="P_41F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_257F10294_I" deadCode="false" sourceNode="P_42F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_257F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_257F10294_O" deadCode="false" sourceNode="P_42F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_257F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_258F10294_I" deadCode="false" sourceNode="P_42F10294" targetNode="P_38F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_258F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_258F10294_O" deadCode="false" sourceNode="P_42F10294" targetNode="P_39F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_258F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_264F10294_I" deadCode="false" sourceNode="P_42F10294" targetNode="P_44F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_264F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_264F10294_O" deadCode="false" sourceNode="P_42F10294" targetNode="P_45F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_264F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_270F10294_I" deadCode="false" sourceNode="P_42F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_270F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_270F10294_O" deadCode="false" sourceNode="P_42F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_270F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_275F10294_I" deadCode="false" sourceNode="P_42F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_275F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_275F10294_O" deadCode="false" sourceNode="P_42F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_275F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10294_I" deadCode="false" sourceNode="P_42F10294" targetNode="P_40F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_276F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10294_O" deadCode="false" sourceNode="P_42F10294" targetNode="P_41F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_276F10294"/>
	</edges>
	<edges id="P_42F10294P_43F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10294" targetNode="P_43F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_279F10294_I" deadCode="false" sourceNode="P_44F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_279F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_279F10294_O" deadCode="false" sourceNode="P_44F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_279F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_282F10294_I" deadCode="false" sourceNode="P_44F10294" targetNode="P_46F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_282F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_282F10294_O" deadCode="false" sourceNode="P_44F10294" targetNode="P_47F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_282F10294"/>
	</edges>
	<edges id="P_44F10294P_45F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10294" targetNode="P_45F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_285F10294_I" deadCode="false" sourceNode="P_46F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_285F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_285F10294_O" deadCode="false" sourceNode="P_46F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_285F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_288F10294_I" deadCode="false" sourceNode="P_46F10294" targetNode="P_48F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_288F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_288F10294_O" deadCode="false" sourceNode="P_46F10294" targetNode="P_49F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_288F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10294_I" deadCode="false" sourceNode="P_46F10294" targetNode="P_50F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_289F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10294_O" deadCode="false" sourceNode="P_46F10294" targetNode="P_51F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_289F10294"/>
	</edges>
	<edges id="P_46F10294P_47F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10294" targetNode="P_47F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10294_I" deadCode="false" sourceNode="P_48F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_292F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10294_O" deadCode="false" sourceNode="P_48F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_292F10294"/>
	</edges>
	<edges id="P_48F10294P_49F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10294" targetNode="P_49F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_305F10294_I" deadCode="false" sourceNode="P_50F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_305F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_305F10294_O" deadCode="false" sourceNode="P_50F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_305F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_306F10294_I" deadCode="false" sourceNode="P_50F10294" targetNode="P_38F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_306F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_306F10294_O" deadCode="false" sourceNode="P_50F10294" targetNode="P_39F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_306F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10294_I" deadCode="false" sourceNode="P_50F10294" targetNode="P_52F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_311F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10294_O" deadCode="false" sourceNode="P_50F10294" targetNode="P_53F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_311F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_317F10294_I" deadCode="false" sourceNode="P_50F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_317F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_317F10294_O" deadCode="false" sourceNode="P_50F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_317F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_319F10294_I" deadCode="false" sourceNode="P_50F10294" targetNode="P_54F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_319F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_319F10294_O" deadCode="false" sourceNode="P_50F10294" targetNode="P_55F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_319F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_322F10294_I" deadCode="false" sourceNode="P_50F10294" targetNode="P_56F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_322F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_322F10294_O" deadCode="false" sourceNode="P_50F10294" targetNode="P_57F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_322F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_327F10294_I" deadCode="false" sourceNode="P_50F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_327F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_327F10294_O" deadCode="false" sourceNode="P_50F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_327F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_332F10294_I" deadCode="false" sourceNode="P_50F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_332F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_332F10294_O" deadCode="false" sourceNode="P_50F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_332F10294"/>
	</edges>
	<edges id="P_50F10294P_51F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10294" targetNode="P_51F10294"/>
	<edges id="P_52F10294P_53F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10294" targetNode="P_53F10294"/>
	<edges id="P_54F10294P_55F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10294" targetNode="P_55F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_355F10294_I" deadCode="false" sourceNode="P_56F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_355F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_355F10294_O" deadCode="false" sourceNode="P_56F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_355F10294"/>
	</edges>
	<edges id="P_56F10294P_57F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10294" targetNode="P_57F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_363F10294_I" deadCode="false" sourceNode="P_32F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_363F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_363F10294_O" deadCode="false" sourceNode="P_32F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_363F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_370F10294_I" deadCode="false" sourceNode="P_32F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_370F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_370F10294_O" deadCode="false" sourceNode="P_32F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_370F10294"/>
	</edges>
	<edges id="P_32F10294P_33F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10294" targetNode="P_33F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_373F10294_I" deadCode="false" sourceNode="P_6F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_373F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_373F10294_O" deadCode="false" sourceNode="P_6F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_373F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_374F10294_I" deadCode="false" sourceNode="P_6F10294" targetNode="P_58F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_374F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_374F10294_O" deadCode="false" sourceNode="P_6F10294" targetNode="P_59F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_374F10294"/>
	</edges>
	<edges id="P_6F10294P_7F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10294" targetNode="P_7F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_377F10294_I" deadCode="false" sourceNode="P_58F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_377F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_377F10294_O" deadCode="false" sourceNode="P_58F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_377F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_385F10294_I" deadCode="false" sourceNode="P_58F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_385F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_385F10294_O" deadCode="false" sourceNode="P_58F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_385F10294"/>
	</edges>
	<edges id="P_58F10294P_59F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10294" targetNode="P_59F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_388F10294_I" deadCode="false" sourceNode="P_22F10294" targetNode="P_8F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_388F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_388F10294_O" deadCode="false" sourceNode="P_22F10294" targetNode="P_9F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_388F10294"/>
	</edges>
	<edges id="P_22F10294P_23F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10294" targetNode="P_23F10294"/>
	<edges id="P_38F10294P_39F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10294" targetNode="P_39F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_427F10294_I" deadCode="false" sourceNode="P_14F10294" targetNode="P_60F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_427F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_427F10294_O" deadCode="false" sourceNode="P_14F10294" targetNode="P_61F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_427F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_431F10294_I" deadCode="false" sourceNode="P_14F10294" targetNode="P_62F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_431F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_431F10294_O" deadCode="false" sourceNode="P_14F10294" targetNode="P_63F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_431F10294"/>
	</edges>
	<edges id="P_14F10294P_15F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10294" targetNode="P_15F10294"/>
	<edges id="P_62F10294P_63F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10294" targetNode="P_63F10294"/>
	<edges id="P_60F10294P_61F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_60F10294" targetNode="P_61F10294"/>
	<edges xsi:type="cbl:PerformEdge" id="S_467F10294_I" deadCode="true" sourceNode="P_64F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_467F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_467F10294_O" deadCode="true" sourceNode="P_64F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_467F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_483F10294_I" deadCode="true" sourceNode="P_66F10294" targetNode="P_67F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_481F10294"/>
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_483F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_483F10294_O" deadCode="true" sourceNode="P_66F10294" targetNode="P_68F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_481F10294"/>
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_483F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_484F10294_I" deadCode="true" sourceNode="P_66F10294" targetNode="P_69F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_484F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_484F10294_O" deadCode="true" sourceNode="P_66F10294" targetNode="P_70F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_484F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_489F10294_I" deadCode="true" sourceNode="P_67F10294" targetNode="P_14F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_489F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_489F10294_O" deadCode="true" sourceNode="P_67F10294" targetNode="P_15F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_489F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_493F10294_I" deadCode="true" sourceNode="P_69F10294" targetNode="P_67F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_493F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_493F10294_O" deadCode="true" sourceNode="P_69F10294" targetNode="P_68F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_493F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_495F10294_I" deadCode="true" sourceNode="P_69F10294" targetNode="P_67F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_495F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_495F10294_O" deadCode="true" sourceNode="P_69F10294" targetNode="P_68F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_495F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_497F10294_I" deadCode="true" sourceNode="P_69F10294" targetNode="P_67F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_497F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_497F10294_O" deadCode="true" sourceNode="P_69F10294" targetNode="P_68F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_497F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_500F10294_I" deadCode="true" sourceNode="P_69F10294" targetNode="P_67F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_500F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_500F10294_O" deadCode="true" sourceNode="P_69F10294" targetNode="P_68F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_500F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_501F10294_I" deadCode="true" sourceNode="P_69F10294" targetNode="P_72F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_501F10294"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_501F10294_O" deadCode="true" sourceNode="P_69F10294" targetNode="P_73F10294">
		<representations href="../../../cobol/LOAS0820.cbl.cobModel#S_501F10294"/>
	</edges>
	<edges id="P_8F10294P_9F10294" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10294" targetNode="P_9F10294"/>
	<edges xsi:type="cbl:DataEdge" id="LOAS0820_S_15F10294OUMANFEE_LOAS0820" deadCode="false" sourceNode="P_10F10294" targetNode="V_OUMANFEE_LOAS0820">
		<representations href="../../../cobol/../importantStmts.cobModel#S_15F10294"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="LOAS0820_S_379F10294OUMANFEE_LOAS0820" deadCode="false" sourceNode="P_58F10294" targetNode="V_OUMANFEE_LOAS0820">
		<representations href="../../../cobol/../importantStmts.cobModel#S_379F10294"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="LOAS0820_S_78F10294OUMANFEE_LOAS0820" deadCode="false" sourceNode="P_20F10294" targetNode="V_OUMANFEE_LOAS0820">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10294"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="LOAS0820_S_364F10294OUMANFEE_LOAS0820" deadCode="false" sourceNode="P_32F10294" targetNode="V_OUMANFEE_LOAS0820">
		<representations href="../../../cobol/../importantStmts.cobModel#S_364F10294"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_419F10294" deadCode="false" name="Dynamic IDSI0011-PGM" sourceNode="P_38F10294" targetNode="IDSS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_419F10294"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_425F10294" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_14F10294" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_425F10294"></representations>
	</edges>
</Package>

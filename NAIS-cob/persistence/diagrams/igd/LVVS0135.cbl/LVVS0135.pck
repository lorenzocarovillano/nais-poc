<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0135" cbl:id="LVVS0135" xsi:id="LVVS0135" packageRef="LVVS0135.igd#LVVS0135" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0135_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10355" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0135.cbl.cobModel#SC_1F10355"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10355" deadCode="false" name="PROGRAM_LVVS0135_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_1F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10355" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_2F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10355" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_3F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10355" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_4F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10355" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_5F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10355" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_8F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10355" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_9F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10355" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_10F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10355" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_11F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10355" deadCode="false" name="S1250-CALCOLA-VARIABILE">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_18F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10355" deadCode="false" name="S1250-EX">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_19F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10355" deadCode="false" name="S1253-ARROTONDA-IMP">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_20F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10355" deadCode="false" name="S1253-EX">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_21F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10355" deadCode="false" name="A191-CALCOLA-DT-RICOR-TRANCHE">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_12F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10355" deadCode="false" name="A191-EX">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_13F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10355" deadCode="false" name="CALL-ROUTINE-DATE">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_22F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10355" deadCode="false" name="CALL-ROUTINE-DATE-EX">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_23F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10355" deadCode="false" name="A140-RECUP-VALORE-ASSET">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_14F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10355" deadCode="false" name="A140-EX">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_15F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10355" deadCode="false" name="S1252-CHIUDE-CURVAS">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_28F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10355" deadCode="false" name="S1252-EX">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_29F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10355" deadCode="false" name="A160-RECUP-QUOTAZ-FONDO">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_16F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10355" deadCode="false" name="A160-EX">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_17F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10355" deadCode="false" name="VALORIZZA-OUTPUT-L19">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_32F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10355" deadCode="false" name="VALORIZZA-OUTPUT-L19-EX">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_33F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10355" deadCode="false" name="INIZIA-TOT-L19">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_30F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10355" deadCode="false" name="INIZIA-TOT-L19-EX">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_31F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10355" deadCode="false" name="INIZIA-NULL-L19">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_38F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10355" deadCode="false" name="INIZIA-NULL-L19-EX">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_39F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10355" deadCode="true" name="INIZIA-ZEROES-L19">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_34F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10355" deadCode="false" name="INIZIA-ZEROES-L19-EX">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_35F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10355" deadCode="false" name="INIZIA-SPACES-L19">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_36F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10355" deadCode="false" name="INIZIA-SPACES-L19-EX">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_37F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10355" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_6F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10355" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_7F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10355" deadCode="false" name="VALORIZZA-OUTPUT-VAS">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_26F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10355" deadCode="false" name="VALORIZZA-OUTPUT-VAS-EX">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_27F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10355" deadCode="false" name="INIZIA-TOT-VAS">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_24F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10355" deadCode="false" name="INIZIA-TOT-VAS-EX">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_25F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10355" deadCode="false" name="INIZIA-NULL-VAS">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_44F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10355" deadCode="false" name="INIZIA-NULL-VAS-EX">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_45F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10355" deadCode="true" name="INIZIA-ZEROES-VAS">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_40F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10355" deadCode="false" name="INIZIA-ZEROES-VAS-EX">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_41F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10355" deadCode="true" name="INIZIA-SPACES-VAS">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_42F10355"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10355" deadCode="false" name="INIZIA-SPACES-VAS-EX">
				<representations href="../../../cobol/LVVS0135.cbl.cobModel#P_43F10355"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0003" name="LCCS0003">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10119"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS4910" name="LDBS4910">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10214"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS2080" name="LDBS2080">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10167"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10355P_1F10355" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10355" targetNode="P_1F10355"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10355_I" deadCode="false" sourceNode="P_1F10355" targetNode="P_2F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_1F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10355_O" deadCode="false" sourceNode="P_1F10355" targetNode="P_3F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_1F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10355_I" deadCode="false" sourceNode="P_1F10355" targetNode="P_4F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_2F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10355_O" deadCode="false" sourceNode="P_1F10355" targetNode="P_5F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_2F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10355_I" deadCode="false" sourceNode="P_1F10355" targetNode="P_6F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_3F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10355_O" deadCode="false" sourceNode="P_1F10355" targetNode="P_7F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_3F10355"/>
	</edges>
	<edges id="P_2F10355P_3F10355" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10355" targetNode="P_3F10355"/>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10355_I" deadCode="false" sourceNode="P_4F10355" targetNode="P_8F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_12F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10355_O" deadCode="false" sourceNode="P_4F10355" targetNode="P_9F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_12F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10355_I" deadCode="false" sourceNode="P_4F10355" targetNode="P_10F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_14F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10355_O" deadCode="false" sourceNode="P_4F10355" targetNode="P_11F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_14F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10355_I" deadCode="false" sourceNode="P_4F10355" targetNode="P_12F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_17F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10355_O" deadCode="false" sourceNode="P_4F10355" targetNode="P_13F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_17F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10355_I" deadCode="false" sourceNode="P_4F10355" targetNode="P_14F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_19F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10355_O" deadCode="false" sourceNode="P_4F10355" targetNode="P_15F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_19F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10355_I" deadCode="false" sourceNode="P_4F10355" targetNode="P_16F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_21F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10355_O" deadCode="false" sourceNode="P_4F10355" targetNode="P_17F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_21F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10355_I" deadCode="false" sourceNode="P_4F10355" targetNode="P_18F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_23F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10355_O" deadCode="false" sourceNode="P_4F10355" targetNode="P_19F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_23F10355"/>
	</edges>
	<edges id="P_4F10355P_5F10355" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10355" targetNode="P_5F10355"/>
	<edges id="P_8F10355P_9F10355" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10355" targetNode="P_9F10355"/>
	<edges id="P_10F10355P_11F10355" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10355" targetNode="P_11F10355"/>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10355_I" deadCode="false" sourceNode="P_18F10355" targetNode="P_20F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_63F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10355_O" deadCode="false" sourceNode="P_18F10355" targetNode="P_21F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_63F10355"/>
	</edges>
	<edges id="P_18F10355P_19F10355" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10355" targetNode="P_19F10355"/>
	<edges id="P_20F10355P_21F10355" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10355" targetNode="P_21F10355"/>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10355_I" deadCode="false" sourceNode="P_12F10355" targetNode="P_22F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_85F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10355_O" deadCode="false" sourceNode="P_12F10355" targetNode="P_23F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_85F10355"/>
	</edges>
	<edges id="P_12F10355P_13F10355" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10355" targetNode="P_13F10355"/>
	<edges id="P_22F10355P_23F10355" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10355" targetNode="P_23F10355"/>
	<edges xsi:type="cbl:PerformEdge" id="S_129F10355_I" deadCode="false" sourceNode="P_14F10355" targetNode="P_24F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_115F10355"/>
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_129F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10355_I" deadCode="false" sourceNode="P_14F10355" targetNode="P_25F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_115F10355"/>
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_130F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10355_I" deadCode="false" sourceNode="P_14F10355" targetNode="P_26F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_115F10355"/>
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_131F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10355_O" deadCode="false" sourceNode="P_14F10355" targetNode="P_27F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_115F10355"/>
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_131F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10355_I" deadCode="false" sourceNode="P_14F10355" targetNode="P_28F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_139F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10355_O" deadCode="false" sourceNode="P_14F10355" targetNode="P_29F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_139F10355"/>
	</edges>
	<edges id="P_14F10355P_15F10355" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10355" targetNode="P_15F10355"/>
	<edges id="P_28F10355P_29F10355" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10355" targetNode="P_29F10355"/>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10355_I" deadCode="false" sourceNode="P_16F10355" targetNode="P_30F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_168F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_169F10355_I" deadCode="false" sourceNode="P_16F10355" targetNode="P_31F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_169F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10355_I" deadCode="false" sourceNode="P_16F10355" targetNode="P_32F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_170F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10355_O" deadCode="false" sourceNode="P_16F10355" targetNode="P_33F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_170F10355"/>
	</edges>
	<edges id="P_16F10355P_17F10355" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10355" targetNode="P_17F10355"/>
	<edges id="P_32F10355P_33F10355" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10355" targetNode="P_33F10355"/>
	<edges xsi:type="cbl:PerformEdge" id="S_196F10355_I" deadCode="true" sourceNode="P_30F10355" targetNode="P_34F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_196F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_196F10355_O" deadCode="true" sourceNode="P_30F10355" targetNode="P_35F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_196F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_197F10355_I" deadCode="false" sourceNode="P_30F10355" targetNode="P_36F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_197F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_197F10355_O" deadCode="false" sourceNode="P_30F10355" targetNode="P_37F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_197F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_198F10355_I" deadCode="false" sourceNode="P_30F10355" targetNode="P_38F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_198F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_198F10355_O" deadCode="false" sourceNode="P_30F10355" targetNode="P_39F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_198F10355"/>
	</edges>
	<edges id="P_38F10355P_39F10355" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10355" targetNode="P_39F10355"/>
	<edges id="P_34F10355P_35F10355" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10355" targetNode="P_35F10355"/>
	<edges id="P_36F10355P_37F10355" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10355" targetNode="P_37F10355"/>
	<edges id="P_26F10355P_27F10355" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10355" targetNode="P_27F10355"/>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10355_I" deadCode="true" sourceNode="P_24F10355" targetNode="P_40F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_278F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10355_O" deadCode="true" sourceNode="P_24F10355" targetNode="P_41F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_278F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_279F10355_I" deadCode="true" sourceNode="P_24F10355" targetNode="P_42F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_279F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_279F10355_O" deadCode="true" sourceNode="P_24F10355" targetNode="P_43F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_279F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10355_I" deadCode="false" sourceNode="P_24F10355" targetNode="P_44F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_280F10355"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10355_O" deadCode="false" sourceNode="P_24F10355" targetNode="P_45F10355">
		<representations href="../../../cobol/LVVS0135.cbl.cobModel#S_280F10355"/>
	</edges>
	<edges id="P_44F10355P_45F10355" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10355" targetNode="P_45F10355"/>
	<edges id="P_40F10355P_41F10355" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10355" targetNode="P_41F10355"/>
	<edges id="P_42F10355P_43F10355" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10355" targetNode="P_43F10355"/>
	<edges xsi:type="cbl:CallEdge" id="S_100F10355" deadCode="false" name="Dynamic LCCS0003" sourceNode="P_22F10355" targetNode="LCCS0003">
		<representations href="../../../cobol/../importantStmts.cobModel#S_100F10355"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_122F10355" deadCode="false" name="Dynamic LDBS4910" sourceNode="P_14F10355" targetNode="LDBS4910">
		<representations href="../../../cobol/../importantStmts.cobModel#S_122F10355"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_142F10355" deadCode="false" name="Dynamic LDBS4910" sourceNode="P_28F10355" targetNode="LDBS4910">
		<representations href="../../../cobol/../importantStmts.cobModel#S_142F10355"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_162F10355" deadCode="false" name="Dynamic LDBS2080" sourceNode="P_16F10355" targetNode="LDBS2080">
		<representations href="../../../cobol/../importantStmts.cobModel#S_162F10355"></representations>
	</edges>
</Package>

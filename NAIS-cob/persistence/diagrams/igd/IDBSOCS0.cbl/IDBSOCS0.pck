<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IDBSOCS0" cbl:id="IDBSOCS0" xsi:id="IDBSOCS0" packageRef="IDBSOCS0.igd#IDBSOCS0" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IDBSOCS0_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10059" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#SC_1F10059"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10059" deadCode="false" name="PROGRAM_IDBSOCS0_FIRST_SENTENCES">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_1F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10059" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_2F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10059" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_3F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10059" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_28F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10059" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_29F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10059" deadCode="false" name="A200-ELABORA-PK">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_24F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10059" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_25F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10059" deadCode="false" name="A300-ELABORA-ID-EFF">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_4F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10059" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_5F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10059" deadCode="false" name="A400-ELABORA-IDP-EFF">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_6F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10059" deadCode="false" name="A400-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_7F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10059" deadCode="false" name="A500-ELABORA-IBO">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_8F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10059" deadCode="false" name="A500-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_9F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10059" deadCode="false" name="A600-ELABORA-IBS">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_10F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10059" deadCode="false" name="A600-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_11F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10059" deadCode="false" name="A700-ELABORA-IDO">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_12F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10059" deadCode="false" name="A700-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_13F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10059" deadCode="false" name="B300-ELABORA-ID-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_14F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10059" deadCode="false" name="B300-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_15F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10059" deadCode="false" name="B400-ELABORA-IDP-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_16F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10059" deadCode="false" name="B400-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_17F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10059" deadCode="false" name="B500-ELABORA-IBO-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_18F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10059" deadCode="false" name="B500-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_19F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10059" deadCode="false" name="B600-ELABORA-IBS-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_20F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10059" deadCode="false" name="B600-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_21F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10059" deadCode="false" name="B700-ELABORA-IDO-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_22F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10059" deadCode="false" name="B700-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_23F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10059" deadCode="false" name="A210-SELECT-PK">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_30F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10059" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_31F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10059" deadCode="false" name="A220-INSERT-PK">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_32F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10059" deadCode="false" name="A220-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_33F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10059" deadCode="false" name="A230-UPDATE-PK">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_34F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10059" deadCode="false" name="A230-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_35F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10059" deadCode="false" name="A240-DELETE-PK">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_36F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10059" deadCode="false" name="A240-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_37F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_142F10059" deadCode="false" name="A305-DECLARE-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_142F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_143F10059" deadCode="false" name="A305-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_143F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10059" deadCode="false" name="A310-SELECT-ID-EFF">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_38F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10059" deadCode="false" name="A310-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_39F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_144F10059" deadCode="false" name="A330-UPDATE-ID-EFF">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_144F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_145F10059" deadCode="false" name="A330-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_145F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_146F10059" deadCode="false" name="A360-OPEN-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_146F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_147F10059" deadCode="false" name="A360-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_147F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_148F10059" deadCode="false" name="A370-CLOSE-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_148F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_149F10059" deadCode="false" name="A370-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_149F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_150F10059" deadCode="true" name="A380-FETCH-FIRST-ID-EFF">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_150F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_153F10059" deadCode="true" name="A380-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_153F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_151F10059" deadCode="false" name="A390-FETCH-NEXT-ID-EFF">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_151F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_152F10059" deadCode="false" name="A390-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_152F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_154F10059" deadCode="false" name="A405-DECLARE-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_154F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_155F10059" deadCode="false" name="A405-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_155F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10059" deadCode="false" name="A410-SELECT-IDP-EFF">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_44F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10059" deadCode="false" name="A410-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_45F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10059" deadCode="false" name="A460-OPEN-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_46F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10059" deadCode="false" name="A460-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_47F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10059" deadCode="false" name="A470-CLOSE-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_48F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10059" deadCode="false" name="A470-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_49F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10059" deadCode="false" name="A480-FETCH-FIRST-IDP-EFF">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_50F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10059" deadCode="false" name="A480-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_51F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10059" deadCode="false" name="A490-FETCH-NEXT-IDP-EFF">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_52F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10059" deadCode="false" name="A490-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_53F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_156F10059" deadCode="false" name="A505-DECLARE-CURSOR-IBO">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_156F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_157F10059" deadCode="false" name="A505-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_157F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10059" deadCode="false" name="A510-SELECT-IBO">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_54F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10059" deadCode="false" name="A510-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_55F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10059" deadCode="false" name="A560-OPEN-CURSOR-IBO">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_56F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10059" deadCode="false" name="A560-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_57F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10059" deadCode="false" name="A570-CLOSE-CURSOR-IBO">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_58F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10059" deadCode="false" name="A570-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_59F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10059" deadCode="false" name="A580-FETCH-FIRST-IBO">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_60F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10059" deadCode="false" name="A580-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_61F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10059" deadCode="false" name="A590-FETCH-NEXT-IBO">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_62F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10059" deadCode="false" name="A590-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_63F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_158F10059" deadCode="false" name="A605-DECLARE-CURSOR-IBS">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_158F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_159F10059" deadCode="false" name="A605-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_159F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10059" deadCode="false" name="A610-SELECT-IBS">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_64F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10059" deadCode="false" name="A610-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_65F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10059" deadCode="false" name="A660-OPEN-CURSOR-IBS">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_66F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10059" deadCode="false" name="A660-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_67F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10059" deadCode="false" name="A670-CLOSE-CURSOR-IBS">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_68F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10059" deadCode="false" name="A670-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_69F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10059" deadCode="false" name="A680-FETCH-FIRST-IBS">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_70F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10059" deadCode="false" name="A680-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_71F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10059" deadCode="false" name="A690-FETCH-NEXT-IBS">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_72F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10059" deadCode="false" name="A690-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_73F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_160F10059" deadCode="false" name="A705-DECLARE-CURSOR-IDO">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_160F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_161F10059" deadCode="false" name="A705-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_161F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10059" deadCode="false" name="A710-SELECT-IDO">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_74F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10059" deadCode="false" name="A710-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_75F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10059" deadCode="false" name="A760-OPEN-CURSOR-IDO">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_76F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10059" deadCode="false" name="A760-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_77F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_78F10059" deadCode="false" name="A770-CLOSE-CURSOR-IDO">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_78F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_79F10059" deadCode="false" name="A770-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_79F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_80F10059" deadCode="false" name="A780-FETCH-FIRST-IDO">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_80F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_81F10059" deadCode="false" name="A780-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_81F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_82F10059" deadCode="false" name="A790-FETCH-NEXT-IDO">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_82F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_83F10059" deadCode="false" name="A790-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_83F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_84F10059" deadCode="false" name="B310-SELECT-ID-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_84F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_85F10059" deadCode="false" name="B310-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_85F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_162F10059" deadCode="false" name="B405-DECLARE-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_162F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_163F10059" deadCode="false" name="B405-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_163F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_86F10059" deadCode="false" name="B410-SELECT-IDP-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_86F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_87F10059" deadCode="false" name="B410-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_87F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_88F10059" deadCode="false" name="B460-OPEN-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_88F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_89F10059" deadCode="false" name="B460-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_89F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_90F10059" deadCode="false" name="B470-CLOSE-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_90F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_91F10059" deadCode="false" name="B470-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_91F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_92F10059" deadCode="false" name="B480-FETCH-FIRST-IDP-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_92F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_93F10059" deadCode="false" name="B480-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_93F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_94F10059" deadCode="false" name="B490-FETCH-NEXT-IDP-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_94F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_95F10059" deadCode="false" name="B490-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_95F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_164F10059" deadCode="false" name="B505-DECLARE-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_164F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_165F10059" deadCode="false" name="B505-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_165F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_96F10059" deadCode="false" name="B510-SELECT-IBO-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_96F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_97F10059" deadCode="false" name="B510-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_97F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_98F10059" deadCode="false" name="B560-OPEN-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_98F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_99F10059" deadCode="false" name="B560-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_99F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_100F10059" deadCode="false" name="B570-CLOSE-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_100F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_101F10059" deadCode="false" name="B570-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_101F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_102F10059" deadCode="false" name="B580-FETCH-FIRST-IBO-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_102F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_103F10059" deadCode="false" name="B580-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_103F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_104F10059" deadCode="false" name="B590-FETCH-NEXT-IBO-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_104F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_105F10059" deadCode="false" name="B590-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_105F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_166F10059" deadCode="false" name="B605-DECLARE-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_166F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_167F10059" deadCode="false" name="B605-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_167F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_106F10059" deadCode="false" name="B610-SELECT-IBS-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_106F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_107F10059" deadCode="false" name="B610-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_107F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_108F10059" deadCode="false" name="B660-OPEN-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_108F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_109F10059" deadCode="false" name="B660-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_109F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_110F10059" deadCode="false" name="B670-CLOSE-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_110F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_111F10059" deadCode="false" name="B670-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_111F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_112F10059" deadCode="false" name="B680-FETCH-FIRST-IBS-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_112F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_113F10059" deadCode="false" name="B680-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_113F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_114F10059" deadCode="false" name="B690-FETCH-NEXT-IBS-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_114F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_115F10059" deadCode="false" name="B690-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_115F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_168F10059" deadCode="false" name="B705-DECLARE-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_168F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_169F10059" deadCode="false" name="B705-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_169F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_116F10059" deadCode="false" name="B710-SELECT-IDO-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_116F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_117F10059" deadCode="false" name="B710-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_117F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_118F10059" deadCode="false" name="B760-OPEN-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_118F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_119F10059" deadCode="false" name="B760-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_119F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_120F10059" deadCode="false" name="B770-CLOSE-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_120F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_121F10059" deadCode="false" name="B770-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_121F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_122F10059" deadCode="false" name="B780-FETCH-FIRST-IDO-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_122F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_123F10059" deadCode="false" name="B780-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_123F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_124F10059" deadCode="false" name="B790-FETCH-NEXT-IDO-CPZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_124F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_125F10059" deadCode="false" name="B790-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_125F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_128F10059" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_128F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_129F10059" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_129F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_134F10059" deadCode="false" name="Z150-VALORIZZA-DATA-SERVICES-I">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_134F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_135F10059" deadCode="false" name="Z150-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_135F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_140F10059" deadCode="false" name="Z160-VALORIZZA-DATA-SERVICES-U">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_140F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_141F10059" deadCode="false" name="Z160-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_141F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_136F10059" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_136F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_137F10059" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_137F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_132F10059" deadCode="false" name="Z400-SEQ-RIGA">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_132F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_133F10059" deadCode="false" name="Z400-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_133F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10059" deadCode="false" name="Z500-AGGIORNAMENTO-STORICO">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_40F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10059" deadCode="false" name="Z500-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_41F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10059" deadCode="false" name="Z550-AGG-STORICO-SOLO-INS">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_42F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10059" deadCode="false" name="Z550-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_43F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_170F10059" deadCode="false" name="Z600-INSERT-NUOVA-RIGA-STORICA">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_170F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_171F10059" deadCode="false" name="Z600-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_171F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_138F10059" deadCode="false" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_138F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_139F10059" deadCode="false" name="Z900-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_139F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_130F10059" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_130F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_131F10059" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_131F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_126F10059" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_126F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_127F10059" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_127F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10059" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_26F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10059" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_27F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_176F10059" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_176F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_177F10059" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_177F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_178F10059" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_178F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_179F10059" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_179F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_172F10059" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_172F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_173F10059" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_173F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_180F10059" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_180F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_181F10059" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_181F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_174F10059" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_174F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_175F10059" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_175F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_182F10059" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_182F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_183F10059" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_183F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_184F10059" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_184F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_185F10059" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_185F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_186F10059" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_186F10059"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_187F10059" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#P_187F10059"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_OGG_IN_COASS" name="OGG_IN_COASS">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_OGG_IN_COASS"/>
		</children>
	</packageNode>
	<edges id="SC_1F10059P_1F10059" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10059" targetNode="P_1F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10059_I" deadCode="false" sourceNode="P_1F10059" targetNode="P_2F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_1F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10059_O" deadCode="false" sourceNode="P_1F10059" targetNode="P_3F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_1F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10059_I" deadCode="false" sourceNode="P_1F10059" targetNode="P_4F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_5F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10059_O" deadCode="false" sourceNode="P_1F10059" targetNode="P_5F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_5F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10059_I" deadCode="false" sourceNode="P_1F10059" targetNode="P_6F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_6F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10059_O" deadCode="false" sourceNode="P_1F10059" targetNode="P_7F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_6F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10059_I" deadCode="false" sourceNode="P_1F10059" targetNode="P_8F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_7F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10059_O" deadCode="false" sourceNode="P_1F10059" targetNode="P_9F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_7F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10059_I" deadCode="false" sourceNode="P_1F10059" targetNode="P_10F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_8F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10059_O" deadCode="false" sourceNode="P_1F10059" targetNode="P_11F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_8F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10059_I" deadCode="false" sourceNode="P_1F10059" targetNode="P_12F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_9F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10059_O" deadCode="false" sourceNode="P_1F10059" targetNode="P_13F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_9F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10059_I" deadCode="false" sourceNode="P_1F10059" targetNode="P_14F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_13F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10059_O" deadCode="false" sourceNode="P_1F10059" targetNode="P_15F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_13F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10059_I" deadCode="false" sourceNode="P_1F10059" targetNode="P_16F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_14F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10059_O" deadCode="false" sourceNode="P_1F10059" targetNode="P_17F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_14F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10059_I" deadCode="false" sourceNode="P_1F10059" targetNode="P_18F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_15F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10059_O" deadCode="false" sourceNode="P_1F10059" targetNode="P_19F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_15F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10059_I" deadCode="false" sourceNode="P_1F10059" targetNode="P_20F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_16F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10059_O" deadCode="false" sourceNode="P_1F10059" targetNode="P_21F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_16F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10059_I" deadCode="false" sourceNode="P_1F10059" targetNode="P_22F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_17F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10059_O" deadCode="false" sourceNode="P_1F10059" targetNode="P_23F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_17F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10059_I" deadCode="false" sourceNode="P_1F10059" targetNode="P_24F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_21F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10059_O" deadCode="false" sourceNode="P_1F10059" targetNode="P_25F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_21F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10059_I" deadCode="false" sourceNode="P_1F10059" targetNode="P_8F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_22F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10059_O" deadCode="false" sourceNode="P_1F10059" targetNode="P_9F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_22F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10059_I" deadCode="false" sourceNode="P_1F10059" targetNode="P_10F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_23F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10059_O" deadCode="false" sourceNode="P_1F10059" targetNode="P_11F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_23F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10059_I" deadCode="false" sourceNode="P_1F10059" targetNode="P_12F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_24F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10059_O" deadCode="false" sourceNode="P_1F10059" targetNode="P_13F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_24F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10059_I" deadCode="false" sourceNode="P_2F10059" targetNode="P_26F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_33F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10059_O" deadCode="false" sourceNode="P_2F10059" targetNode="P_27F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_33F10059"/>
	</edges>
	<edges id="P_2F10059P_3F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10059" targetNode="P_3F10059"/>
	<edges id="P_28F10059P_29F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10059" targetNode="P_29F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10059_I" deadCode="false" sourceNode="P_24F10059" targetNode="P_30F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_46F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10059_O" deadCode="false" sourceNode="P_24F10059" targetNode="P_31F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_46F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10059_I" deadCode="false" sourceNode="P_24F10059" targetNode="P_32F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_47F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10059_O" deadCode="false" sourceNode="P_24F10059" targetNode="P_33F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_47F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10059_I" deadCode="false" sourceNode="P_24F10059" targetNode="P_34F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_48F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10059_O" deadCode="false" sourceNode="P_24F10059" targetNode="P_35F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_48F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10059_I" deadCode="false" sourceNode="P_24F10059" targetNode="P_36F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_49F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10059_O" deadCode="false" sourceNode="P_24F10059" targetNode="P_37F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_49F10059"/>
	</edges>
	<edges id="P_24F10059P_25F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10059" targetNode="P_25F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10059_I" deadCode="false" sourceNode="P_4F10059" targetNode="P_38F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_53F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10059_O" deadCode="false" sourceNode="P_4F10059" targetNode="P_39F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_53F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10059_I" deadCode="false" sourceNode="P_4F10059" targetNode="P_40F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_54F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10059_O" deadCode="false" sourceNode="P_4F10059" targetNode="P_41F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_54F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10059_I" deadCode="false" sourceNode="P_4F10059" targetNode="P_42F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_55F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10059_O" deadCode="false" sourceNode="P_4F10059" targetNode="P_43F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_55F10059"/>
	</edges>
	<edges id="P_4F10059P_5F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10059" targetNode="P_5F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10059_I" deadCode="false" sourceNode="P_6F10059" targetNode="P_44F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_59F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10059_O" deadCode="false" sourceNode="P_6F10059" targetNode="P_45F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_59F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10059_I" deadCode="false" sourceNode="P_6F10059" targetNode="P_46F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_60F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10059_O" deadCode="false" sourceNode="P_6F10059" targetNode="P_47F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_60F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10059_I" deadCode="false" sourceNode="P_6F10059" targetNode="P_48F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_61F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10059_O" deadCode="false" sourceNode="P_6F10059" targetNode="P_49F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_61F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10059_I" deadCode="false" sourceNode="P_6F10059" targetNode="P_50F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_62F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10059_O" deadCode="false" sourceNode="P_6F10059" targetNode="P_51F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_62F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10059_I" deadCode="false" sourceNode="P_6F10059" targetNode="P_52F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_63F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10059_O" deadCode="false" sourceNode="P_6F10059" targetNode="P_53F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_63F10059"/>
	</edges>
	<edges id="P_6F10059P_7F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10059" targetNode="P_7F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10059_I" deadCode="false" sourceNode="P_8F10059" targetNode="P_54F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_67F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10059_O" deadCode="false" sourceNode="P_8F10059" targetNode="P_55F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_67F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10059_I" deadCode="false" sourceNode="P_8F10059" targetNode="P_56F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_68F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10059_O" deadCode="false" sourceNode="P_8F10059" targetNode="P_57F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_68F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10059_I" deadCode="false" sourceNode="P_8F10059" targetNode="P_58F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_69F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10059_O" deadCode="false" sourceNode="P_8F10059" targetNode="P_59F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_69F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10059_I" deadCode="false" sourceNode="P_8F10059" targetNode="P_60F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_70F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10059_O" deadCode="false" sourceNode="P_8F10059" targetNode="P_61F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_70F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10059_I" deadCode="false" sourceNode="P_8F10059" targetNode="P_62F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_71F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10059_O" deadCode="false" sourceNode="P_8F10059" targetNode="P_63F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_71F10059"/>
	</edges>
	<edges id="P_8F10059P_9F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10059" targetNode="P_9F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10059_I" deadCode="false" sourceNode="P_10F10059" targetNode="P_64F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_75F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10059_O" deadCode="false" sourceNode="P_10F10059" targetNode="P_65F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_75F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10059_I" deadCode="false" sourceNode="P_10F10059" targetNode="P_66F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_76F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10059_O" deadCode="false" sourceNode="P_10F10059" targetNode="P_67F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_76F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10059_I" deadCode="false" sourceNode="P_10F10059" targetNode="P_68F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_77F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10059_O" deadCode="false" sourceNode="P_10F10059" targetNode="P_69F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_77F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10059_I" deadCode="false" sourceNode="P_10F10059" targetNode="P_70F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_78F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10059_O" deadCode="false" sourceNode="P_10F10059" targetNode="P_71F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_78F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10059_I" deadCode="false" sourceNode="P_10F10059" targetNode="P_72F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_79F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10059_O" deadCode="false" sourceNode="P_10F10059" targetNode="P_73F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_79F10059"/>
	</edges>
	<edges id="P_10F10059P_11F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10059" targetNode="P_11F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10059_I" deadCode="false" sourceNode="P_12F10059" targetNode="P_74F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_83F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10059_O" deadCode="false" sourceNode="P_12F10059" targetNode="P_75F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_83F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10059_I" deadCode="false" sourceNode="P_12F10059" targetNode="P_76F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_84F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10059_O" deadCode="false" sourceNode="P_12F10059" targetNode="P_77F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_84F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10059_I" deadCode="false" sourceNode="P_12F10059" targetNode="P_78F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_85F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10059_O" deadCode="false" sourceNode="P_12F10059" targetNode="P_79F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_85F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10059_I" deadCode="false" sourceNode="P_12F10059" targetNode="P_80F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_86F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10059_O" deadCode="false" sourceNode="P_12F10059" targetNode="P_81F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_86F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10059_I" deadCode="false" sourceNode="P_12F10059" targetNode="P_82F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_87F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10059_O" deadCode="false" sourceNode="P_12F10059" targetNode="P_83F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_87F10059"/>
	</edges>
	<edges id="P_12F10059P_13F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10059" targetNode="P_13F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10059_I" deadCode="false" sourceNode="P_14F10059" targetNode="P_84F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_91F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10059_O" deadCode="false" sourceNode="P_14F10059" targetNode="P_85F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_91F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10059_I" deadCode="false" sourceNode="P_14F10059" targetNode="P_40F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_92F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10059_O" deadCode="false" sourceNode="P_14F10059" targetNode="P_41F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_92F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10059_I" deadCode="false" sourceNode="P_14F10059" targetNode="P_42F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_93F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10059_O" deadCode="false" sourceNode="P_14F10059" targetNode="P_43F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_93F10059"/>
	</edges>
	<edges id="P_14F10059P_15F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10059" targetNode="P_15F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10059_I" deadCode="false" sourceNode="P_16F10059" targetNode="P_86F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_97F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10059_O" deadCode="false" sourceNode="P_16F10059" targetNode="P_87F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_97F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10059_I" deadCode="false" sourceNode="P_16F10059" targetNode="P_88F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_98F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10059_O" deadCode="false" sourceNode="P_16F10059" targetNode="P_89F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_98F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10059_I" deadCode="false" sourceNode="P_16F10059" targetNode="P_90F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_99F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10059_O" deadCode="false" sourceNode="P_16F10059" targetNode="P_91F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_99F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10059_I" deadCode="false" sourceNode="P_16F10059" targetNode="P_92F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_100F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10059_O" deadCode="false" sourceNode="P_16F10059" targetNode="P_93F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_100F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10059_I" deadCode="false" sourceNode="P_16F10059" targetNode="P_94F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_101F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10059_O" deadCode="false" sourceNode="P_16F10059" targetNode="P_95F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_101F10059"/>
	</edges>
	<edges id="P_16F10059P_17F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10059" targetNode="P_17F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10059_I" deadCode="false" sourceNode="P_18F10059" targetNode="P_96F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_105F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10059_O" deadCode="false" sourceNode="P_18F10059" targetNode="P_97F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_105F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10059_I" deadCode="false" sourceNode="P_18F10059" targetNode="P_98F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_106F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10059_O" deadCode="false" sourceNode="P_18F10059" targetNode="P_99F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_106F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10059_I" deadCode="false" sourceNode="P_18F10059" targetNode="P_100F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_107F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10059_O" deadCode="false" sourceNode="P_18F10059" targetNode="P_101F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_107F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10059_I" deadCode="false" sourceNode="P_18F10059" targetNode="P_102F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_108F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10059_O" deadCode="false" sourceNode="P_18F10059" targetNode="P_103F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_108F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10059_I" deadCode="false" sourceNode="P_18F10059" targetNode="P_104F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_109F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10059_O" deadCode="false" sourceNode="P_18F10059" targetNode="P_105F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_109F10059"/>
	</edges>
	<edges id="P_18F10059P_19F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10059" targetNode="P_19F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10059_I" deadCode="false" sourceNode="P_20F10059" targetNode="P_106F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_113F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10059_O" deadCode="false" sourceNode="P_20F10059" targetNode="P_107F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_113F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10059_I" deadCode="false" sourceNode="P_20F10059" targetNode="P_108F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_114F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10059_O" deadCode="false" sourceNode="P_20F10059" targetNode="P_109F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_114F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10059_I" deadCode="false" sourceNode="P_20F10059" targetNode="P_110F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_115F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10059_O" deadCode="false" sourceNode="P_20F10059" targetNode="P_111F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_115F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10059_I" deadCode="false" sourceNode="P_20F10059" targetNode="P_112F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_116F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10059_O" deadCode="false" sourceNode="P_20F10059" targetNode="P_113F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_116F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10059_I" deadCode="false" sourceNode="P_20F10059" targetNode="P_114F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_117F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10059_O" deadCode="false" sourceNode="P_20F10059" targetNode="P_115F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_117F10059"/>
	</edges>
	<edges id="P_20F10059P_21F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10059" targetNode="P_21F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10059_I" deadCode="false" sourceNode="P_22F10059" targetNode="P_116F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_121F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10059_O" deadCode="false" sourceNode="P_22F10059" targetNode="P_117F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_121F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10059_I" deadCode="false" sourceNode="P_22F10059" targetNode="P_118F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_122F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10059_O" deadCode="false" sourceNode="P_22F10059" targetNode="P_119F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_122F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10059_I" deadCode="false" sourceNode="P_22F10059" targetNode="P_120F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_123F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10059_O" deadCode="false" sourceNode="P_22F10059" targetNode="P_121F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_123F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10059_I" deadCode="false" sourceNode="P_22F10059" targetNode="P_122F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_124F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10059_O" deadCode="false" sourceNode="P_22F10059" targetNode="P_123F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_124F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10059_I" deadCode="false" sourceNode="P_22F10059" targetNode="P_124F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_125F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10059_O" deadCode="false" sourceNode="P_22F10059" targetNode="P_125F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_125F10059"/>
	</edges>
	<edges id="P_22F10059P_23F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10059" targetNode="P_23F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10059_I" deadCode="false" sourceNode="P_30F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_128F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10059_O" deadCode="false" sourceNode="P_30F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_128F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10059_I" deadCode="false" sourceNode="P_30F10059" targetNode="P_28F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_130F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10059_O" deadCode="false" sourceNode="P_30F10059" targetNode="P_29F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_130F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10059_I" deadCode="false" sourceNode="P_30F10059" targetNode="P_128F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_132F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10059_O" deadCode="false" sourceNode="P_30F10059" targetNode="P_129F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_132F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10059_I" deadCode="false" sourceNode="P_30F10059" targetNode="P_130F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_133F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10059_O" deadCode="false" sourceNode="P_30F10059" targetNode="P_131F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_133F10059"/>
	</edges>
	<edges id="P_30F10059P_31F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10059" targetNode="P_31F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10059_I" deadCode="false" sourceNode="P_32F10059" targetNode="P_132F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_135F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10059_O" deadCode="false" sourceNode="P_32F10059" targetNode="P_133F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_135F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10059_I" deadCode="false" sourceNode="P_32F10059" targetNode="P_134F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_137F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10059_O" deadCode="false" sourceNode="P_32F10059" targetNode="P_135F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_137F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10059_I" deadCode="false" sourceNode="P_32F10059" targetNode="P_136F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_138F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10059_O" deadCode="false" sourceNode="P_32F10059" targetNode="P_137F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_138F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10059_I" deadCode="false" sourceNode="P_32F10059" targetNode="P_138F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_139F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10059_O" deadCode="false" sourceNode="P_32F10059" targetNode="P_139F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_139F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10059_I" deadCode="false" sourceNode="P_32F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_140F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10059_O" deadCode="false" sourceNode="P_32F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_140F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10059_I" deadCode="false" sourceNode="P_32F10059" targetNode="P_28F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_142F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10059_O" deadCode="false" sourceNode="P_32F10059" targetNode="P_29F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_142F10059"/>
	</edges>
	<edges id="P_32F10059P_33F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10059" targetNode="P_33F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10059_I" deadCode="false" sourceNode="P_34F10059" targetNode="P_140F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_144F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10059_O" deadCode="false" sourceNode="P_34F10059" targetNode="P_141F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_144F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10059_I" deadCode="false" sourceNode="P_34F10059" targetNode="P_136F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_145F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10059_O" deadCode="false" sourceNode="P_34F10059" targetNode="P_137F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_145F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10059_I" deadCode="false" sourceNode="P_34F10059" targetNode="P_138F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_146F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10059_O" deadCode="false" sourceNode="P_34F10059" targetNode="P_139F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_146F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10059_I" deadCode="false" sourceNode="P_34F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_147F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10059_O" deadCode="false" sourceNode="P_34F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_147F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10059_I" deadCode="false" sourceNode="P_34F10059" targetNode="P_28F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_149F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10059_O" deadCode="false" sourceNode="P_34F10059" targetNode="P_29F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_149F10059"/>
	</edges>
	<edges id="P_34F10059P_35F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10059" targetNode="P_35F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10059_I" deadCode="false" sourceNode="P_36F10059" targetNode="P_28F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_152F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10059_O" deadCode="false" sourceNode="P_36F10059" targetNode="P_29F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_152F10059"/>
	</edges>
	<edges id="P_36F10059P_37F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10059" targetNode="P_37F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10059_I" deadCode="false" sourceNode="P_142F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_154F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10059_O" deadCode="false" sourceNode="P_142F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_154F10059"/>
	</edges>
	<edges id="P_142F10059P_143F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_142F10059" targetNode="P_143F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10059_I" deadCode="false" sourceNode="P_38F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_158F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10059_O" deadCode="false" sourceNode="P_38F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_158F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10059_I" deadCode="false" sourceNode="P_38F10059" targetNode="P_28F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_160F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10059_O" deadCode="false" sourceNode="P_38F10059" targetNode="P_29F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_160F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10059_I" deadCode="false" sourceNode="P_38F10059" targetNode="P_128F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_162F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10059_O" deadCode="false" sourceNode="P_38F10059" targetNode="P_129F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_162F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10059_I" deadCode="false" sourceNode="P_38F10059" targetNode="P_130F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_163F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10059_O" deadCode="false" sourceNode="P_38F10059" targetNode="P_131F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_163F10059"/>
	</edges>
	<edges id="P_38F10059P_39F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10059" targetNode="P_39F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10059_I" deadCode="false" sourceNode="P_144F10059" targetNode="P_140F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_165F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10059_O" deadCode="false" sourceNode="P_144F10059" targetNode="P_141F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_165F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10059_I" deadCode="false" sourceNode="P_144F10059" targetNode="P_136F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_166F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10059_O" deadCode="false" sourceNode="P_144F10059" targetNode="P_137F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_166F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10059_I" deadCode="false" sourceNode="P_144F10059" targetNode="P_138F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_167F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10059_O" deadCode="false" sourceNode="P_144F10059" targetNode="P_139F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_167F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10059_I" deadCode="false" sourceNode="P_144F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_168F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10059_O" deadCode="false" sourceNode="P_144F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_168F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10059_I" deadCode="false" sourceNode="P_144F10059" targetNode="P_28F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_170F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10059_O" deadCode="false" sourceNode="P_144F10059" targetNode="P_29F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_170F10059"/>
	</edges>
	<edges id="P_144F10059P_145F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_144F10059" targetNode="P_145F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10059_I" deadCode="false" sourceNode="P_146F10059" targetNode="P_142F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_172F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10059_O" deadCode="false" sourceNode="P_146F10059" targetNode="P_143F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_172F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10059_I" deadCode="false" sourceNode="P_146F10059" targetNode="P_28F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_174F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10059_O" deadCode="false" sourceNode="P_146F10059" targetNode="P_29F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_174F10059"/>
	</edges>
	<edges id="P_146F10059P_147F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_146F10059" targetNode="P_147F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10059_I" deadCode="false" sourceNode="P_148F10059" targetNode="P_28F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_177F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10059_O" deadCode="false" sourceNode="P_148F10059" targetNode="P_29F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_177F10059"/>
	</edges>
	<edges id="P_148F10059P_149F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_148F10059" targetNode="P_149F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10059_I" deadCode="true" sourceNode="P_150F10059" targetNode="P_146F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_179F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10059_O" deadCode="true" sourceNode="P_150F10059" targetNode="P_147F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_179F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10059_I" deadCode="true" sourceNode="P_150F10059" targetNode="P_151F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_181F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10059_O" deadCode="true" sourceNode="P_150F10059" targetNode="P_152F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_181F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10059_I" deadCode="false" sourceNode="P_151F10059" targetNode="P_28F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_184F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10059_O" deadCode="false" sourceNode="P_151F10059" targetNode="P_29F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_184F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10059_I" deadCode="false" sourceNode="P_151F10059" targetNode="P_128F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_186F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10059_O" deadCode="false" sourceNode="P_151F10059" targetNode="P_129F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_186F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10059_I" deadCode="false" sourceNode="P_151F10059" targetNode="P_130F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_187F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10059_O" deadCode="false" sourceNode="P_151F10059" targetNode="P_131F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_187F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10059_I" deadCode="false" sourceNode="P_151F10059" targetNode="P_148F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_189F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10059_O" deadCode="false" sourceNode="P_151F10059" targetNode="P_149F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_189F10059"/>
	</edges>
	<edges id="P_151F10059P_152F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_151F10059" targetNode="P_152F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_193F10059_I" deadCode="false" sourceNode="P_154F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_193F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_193F10059_O" deadCode="false" sourceNode="P_154F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_193F10059"/>
	</edges>
	<edges id="P_154F10059P_155F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_154F10059" targetNode="P_155F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_197F10059_I" deadCode="false" sourceNode="P_44F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_197F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_197F10059_O" deadCode="false" sourceNode="P_44F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_197F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_199F10059_I" deadCode="false" sourceNode="P_44F10059" targetNode="P_28F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_199F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_199F10059_O" deadCode="false" sourceNode="P_44F10059" targetNode="P_29F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_199F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_201F10059_I" deadCode="false" sourceNode="P_44F10059" targetNode="P_128F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_201F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_201F10059_O" deadCode="false" sourceNode="P_44F10059" targetNode="P_129F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_201F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10059_I" deadCode="false" sourceNode="P_44F10059" targetNode="P_130F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_202F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10059_O" deadCode="false" sourceNode="P_44F10059" targetNode="P_131F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_202F10059"/>
	</edges>
	<edges id="P_44F10059P_45F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10059" targetNode="P_45F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_204F10059_I" deadCode="false" sourceNode="P_46F10059" targetNode="P_154F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_204F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_204F10059_O" deadCode="false" sourceNode="P_46F10059" targetNode="P_155F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_204F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_206F10059_I" deadCode="false" sourceNode="P_46F10059" targetNode="P_28F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_206F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_206F10059_O" deadCode="false" sourceNode="P_46F10059" targetNode="P_29F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_206F10059"/>
	</edges>
	<edges id="P_46F10059P_47F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10059" targetNode="P_47F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_209F10059_I" deadCode="false" sourceNode="P_48F10059" targetNode="P_28F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_209F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_209F10059_O" deadCode="false" sourceNode="P_48F10059" targetNode="P_29F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_209F10059"/>
	</edges>
	<edges id="P_48F10059P_49F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10059" targetNode="P_49F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10059_I" deadCode="false" sourceNode="P_50F10059" targetNode="P_46F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_211F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10059_O" deadCode="false" sourceNode="P_50F10059" targetNode="P_47F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_211F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10059_I" deadCode="false" sourceNode="P_50F10059" targetNode="P_52F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_213F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10059_O" deadCode="false" sourceNode="P_50F10059" targetNode="P_53F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_213F10059"/>
	</edges>
	<edges id="P_50F10059P_51F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10059" targetNode="P_51F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10059_I" deadCode="false" sourceNode="P_52F10059" targetNode="P_28F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_216F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10059_O" deadCode="false" sourceNode="P_52F10059" targetNode="P_29F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_216F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_218F10059_I" deadCode="false" sourceNode="P_52F10059" targetNode="P_128F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_218F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_218F10059_O" deadCode="false" sourceNode="P_52F10059" targetNode="P_129F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_218F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_219F10059_I" deadCode="false" sourceNode="P_52F10059" targetNode="P_130F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_219F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_219F10059_O" deadCode="false" sourceNode="P_52F10059" targetNode="P_131F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_219F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10059_I" deadCode="false" sourceNode="P_52F10059" targetNode="P_48F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_221F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10059_O" deadCode="false" sourceNode="P_52F10059" targetNode="P_49F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_221F10059"/>
	</edges>
	<edges id="P_52F10059P_53F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10059" targetNode="P_53F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_225F10059_I" deadCode="false" sourceNode="P_156F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_225F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_225F10059_O" deadCode="false" sourceNode="P_156F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_225F10059"/>
	</edges>
	<edges id="P_156F10059P_157F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_156F10059" targetNode="P_157F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_228F10059_I" deadCode="false" sourceNode="P_54F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_228F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_228F10059_O" deadCode="false" sourceNode="P_54F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_228F10059"/>
	</edges>
	<edges id="P_54F10059P_55F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10059" targetNode="P_55F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10059_I" deadCode="false" sourceNode="P_56F10059" targetNode="P_156F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_231F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10059_O" deadCode="false" sourceNode="P_56F10059" targetNode="P_157F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_231F10059"/>
	</edges>
	<edges id="P_56F10059P_57F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10059" targetNode="P_57F10059"/>
	<edges id="P_58F10059P_59F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10059" targetNode="P_59F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_236F10059_I" deadCode="false" sourceNode="P_60F10059" targetNode="P_56F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_236F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_236F10059_O" deadCode="false" sourceNode="P_60F10059" targetNode="P_57F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_236F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_238F10059_I" deadCode="false" sourceNode="P_60F10059" targetNode="P_62F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_238F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_238F10059_O" deadCode="false" sourceNode="P_60F10059" targetNode="P_63F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_238F10059"/>
	</edges>
	<edges id="P_60F10059P_61F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_60F10059" targetNode="P_61F10059"/>
	<edges id="P_62F10059P_63F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10059" targetNode="P_63F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10059_I" deadCode="false" sourceNode="P_158F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_242F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10059_O" deadCode="false" sourceNode="P_158F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_242F10059"/>
	</edges>
	<edges id="P_158F10059P_159F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_158F10059" targetNode="P_159F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_245F10059_I" deadCode="false" sourceNode="P_64F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_245F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_245F10059_O" deadCode="false" sourceNode="P_64F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_245F10059"/>
	</edges>
	<edges id="P_64F10059P_65F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10059" targetNode="P_65F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_248F10059_I" deadCode="false" sourceNode="P_66F10059" targetNode="P_158F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_248F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_248F10059_O" deadCode="false" sourceNode="P_66F10059" targetNode="P_159F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_248F10059"/>
	</edges>
	<edges id="P_66F10059P_67F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10059" targetNode="P_67F10059"/>
	<edges id="P_68F10059P_69F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10059" targetNode="P_69F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10059_I" deadCode="false" sourceNode="P_70F10059" targetNode="P_66F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_253F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10059_O" deadCode="false" sourceNode="P_70F10059" targetNode="P_67F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_253F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10059_I" deadCode="false" sourceNode="P_70F10059" targetNode="P_72F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_255F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10059_O" deadCode="false" sourceNode="P_70F10059" targetNode="P_73F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_255F10059"/>
	</edges>
	<edges id="P_70F10059P_71F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_70F10059" targetNode="P_71F10059"/>
	<edges id="P_72F10059P_73F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10059" targetNode="P_73F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10059_I" deadCode="false" sourceNode="P_160F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_259F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10059_O" deadCode="false" sourceNode="P_160F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_259F10059"/>
	</edges>
	<edges id="P_160F10059P_161F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_160F10059" targetNode="P_161F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10059_I" deadCode="false" sourceNode="P_74F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_262F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10059_O" deadCode="false" sourceNode="P_74F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_262F10059"/>
	</edges>
	<edges id="P_74F10059P_75F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10059" targetNode="P_75F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10059_I" deadCode="false" sourceNode="P_76F10059" targetNode="P_160F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_265F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10059_O" deadCode="false" sourceNode="P_76F10059" targetNode="P_161F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_265F10059"/>
	</edges>
	<edges id="P_76F10059P_77F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_76F10059" targetNode="P_77F10059"/>
	<edges id="P_78F10059P_79F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_78F10059" targetNode="P_79F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_270F10059_I" deadCode="false" sourceNode="P_80F10059" targetNode="P_76F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_270F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_270F10059_O" deadCode="false" sourceNode="P_80F10059" targetNode="P_77F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_270F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_272F10059_I" deadCode="false" sourceNode="P_80F10059" targetNode="P_82F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_272F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_272F10059_O" deadCode="false" sourceNode="P_80F10059" targetNode="P_83F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_272F10059"/>
	</edges>
	<edges id="P_80F10059P_81F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_80F10059" targetNode="P_81F10059"/>
	<edges id="P_82F10059P_83F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_82F10059" targetNode="P_83F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10059_I" deadCode="false" sourceNode="P_84F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_276F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10059_O" deadCode="false" sourceNode="P_84F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_276F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10059_I" deadCode="false" sourceNode="P_84F10059" targetNode="P_28F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_278F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10059_O" deadCode="false" sourceNode="P_84F10059" targetNode="P_29F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_278F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10059_I" deadCode="false" sourceNode="P_84F10059" targetNode="P_128F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_280F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10059_O" deadCode="false" sourceNode="P_84F10059" targetNode="P_129F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_280F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10059_I" deadCode="false" sourceNode="P_84F10059" targetNode="P_130F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_281F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10059_O" deadCode="false" sourceNode="P_84F10059" targetNode="P_131F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_281F10059"/>
	</edges>
	<edges id="P_84F10059P_85F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_84F10059" targetNode="P_85F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10059_I" deadCode="false" sourceNode="P_162F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_283F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10059_O" deadCode="false" sourceNode="P_162F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_283F10059"/>
	</edges>
	<edges id="P_162F10059P_163F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_162F10059" targetNode="P_163F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_287F10059_I" deadCode="false" sourceNode="P_86F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_287F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_287F10059_O" deadCode="false" sourceNode="P_86F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_287F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10059_I" deadCode="false" sourceNode="P_86F10059" targetNode="P_28F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_289F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10059_O" deadCode="false" sourceNode="P_86F10059" targetNode="P_29F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_289F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_291F10059_I" deadCode="false" sourceNode="P_86F10059" targetNode="P_128F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_291F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_291F10059_O" deadCode="false" sourceNode="P_86F10059" targetNode="P_129F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_291F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10059_I" deadCode="false" sourceNode="P_86F10059" targetNode="P_130F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_292F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10059_O" deadCode="false" sourceNode="P_86F10059" targetNode="P_131F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_292F10059"/>
	</edges>
	<edges id="P_86F10059P_87F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_86F10059" targetNode="P_87F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10059_I" deadCode="false" sourceNode="P_88F10059" targetNode="P_162F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_294F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10059_O" deadCode="false" sourceNode="P_88F10059" targetNode="P_163F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_294F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_296F10059_I" deadCode="false" sourceNode="P_88F10059" targetNode="P_28F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_296F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_296F10059_O" deadCode="false" sourceNode="P_88F10059" targetNode="P_29F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_296F10059"/>
	</edges>
	<edges id="P_88F10059P_89F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_88F10059" targetNode="P_89F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10059_I" deadCode="false" sourceNode="P_90F10059" targetNode="P_28F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_299F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10059_O" deadCode="false" sourceNode="P_90F10059" targetNode="P_29F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_299F10059"/>
	</edges>
	<edges id="P_90F10059P_91F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_90F10059" targetNode="P_91F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10059_I" deadCode="false" sourceNode="P_92F10059" targetNode="P_88F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_301F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10059_O" deadCode="false" sourceNode="P_92F10059" targetNode="P_89F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_301F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_303F10059_I" deadCode="false" sourceNode="P_92F10059" targetNode="P_94F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_303F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_303F10059_O" deadCode="false" sourceNode="P_92F10059" targetNode="P_95F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_303F10059"/>
	</edges>
	<edges id="P_92F10059P_93F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_92F10059" targetNode="P_93F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_306F10059_I" deadCode="false" sourceNode="P_94F10059" targetNode="P_28F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_306F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_306F10059_O" deadCode="false" sourceNode="P_94F10059" targetNode="P_29F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_306F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10059_I" deadCode="false" sourceNode="P_94F10059" targetNode="P_128F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_308F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10059_O" deadCode="false" sourceNode="P_94F10059" targetNode="P_129F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_308F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10059_I" deadCode="false" sourceNode="P_94F10059" targetNode="P_130F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_309F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10059_O" deadCode="false" sourceNode="P_94F10059" targetNode="P_131F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_309F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10059_I" deadCode="false" sourceNode="P_94F10059" targetNode="P_90F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_311F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10059_O" deadCode="false" sourceNode="P_94F10059" targetNode="P_91F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_311F10059"/>
	</edges>
	<edges id="P_94F10059P_95F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_94F10059" targetNode="P_95F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_315F10059_I" deadCode="false" sourceNode="P_164F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_315F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_315F10059_O" deadCode="false" sourceNode="P_164F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_315F10059"/>
	</edges>
	<edges id="P_164F10059P_165F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_164F10059" targetNode="P_165F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_318F10059_I" deadCode="false" sourceNode="P_96F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_318F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_318F10059_O" deadCode="false" sourceNode="P_96F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_318F10059"/>
	</edges>
	<edges id="P_96F10059P_97F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_96F10059" targetNode="P_97F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10059_I" deadCode="false" sourceNode="P_98F10059" targetNode="P_164F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_321F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10059_O" deadCode="false" sourceNode="P_98F10059" targetNode="P_165F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_321F10059"/>
	</edges>
	<edges id="P_98F10059P_99F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_98F10059" targetNode="P_99F10059"/>
	<edges id="P_100F10059P_101F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_100F10059" targetNode="P_101F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_326F10059_I" deadCode="false" sourceNode="P_102F10059" targetNode="P_98F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_326F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_326F10059_O" deadCode="false" sourceNode="P_102F10059" targetNode="P_99F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_326F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_328F10059_I" deadCode="false" sourceNode="P_102F10059" targetNode="P_104F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_328F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_328F10059_O" deadCode="false" sourceNode="P_102F10059" targetNode="P_105F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_328F10059"/>
	</edges>
	<edges id="P_102F10059P_103F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_102F10059" targetNode="P_103F10059"/>
	<edges id="P_104F10059P_105F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_104F10059" targetNode="P_105F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_332F10059_I" deadCode="false" sourceNode="P_166F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_332F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_332F10059_O" deadCode="false" sourceNode="P_166F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_332F10059"/>
	</edges>
	<edges id="P_166F10059P_167F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_166F10059" targetNode="P_167F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_335F10059_I" deadCode="false" sourceNode="P_106F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_335F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_335F10059_O" deadCode="false" sourceNode="P_106F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_335F10059"/>
	</edges>
	<edges id="P_106F10059P_107F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_106F10059" targetNode="P_107F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10059_I" deadCode="false" sourceNode="P_108F10059" targetNode="P_166F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_338F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10059_O" deadCode="false" sourceNode="P_108F10059" targetNode="P_167F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_338F10059"/>
	</edges>
	<edges id="P_108F10059P_109F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_108F10059" targetNode="P_109F10059"/>
	<edges id="P_110F10059P_111F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_110F10059" targetNode="P_111F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_343F10059_I" deadCode="false" sourceNode="P_112F10059" targetNode="P_108F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_343F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_343F10059_O" deadCode="false" sourceNode="P_112F10059" targetNode="P_109F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_343F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_345F10059_I" deadCode="false" sourceNode="P_112F10059" targetNode="P_114F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_345F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_345F10059_O" deadCode="false" sourceNode="P_112F10059" targetNode="P_115F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_345F10059"/>
	</edges>
	<edges id="P_112F10059P_113F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_112F10059" targetNode="P_113F10059"/>
	<edges id="P_114F10059P_115F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_114F10059" targetNode="P_115F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_349F10059_I" deadCode="false" sourceNode="P_168F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_349F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_349F10059_O" deadCode="false" sourceNode="P_168F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_349F10059"/>
	</edges>
	<edges id="P_168F10059P_169F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_168F10059" targetNode="P_169F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_352F10059_I" deadCode="false" sourceNode="P_116F10059" targetNode="P_126F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_352F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_352F10059_O" deadCode="false" sourceNode="P_116F10059" targetNode="P_127F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_352F10059"/>
	</edges>
	<edges id="P_116F10059P_117F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_116F10059" targetNode="P_117F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_355F10059_I" deadCode="false" sourceNode="P_118F10059" targetNode="P_168F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_355F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_355F10059_O" deadCode="false" sourceNode="P_118F10059" targetNode="P_169F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_355F10059"/>
	</edges>
	<edges id="P_118F10059P_119F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_118F10059" targetNode="P_119F10059"/>
	<edges id="P_120F10059P_121F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_120F10059" targetNode="P_121F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_360F10059_I" deadCode="false" sourceNode="P_122F10059" targetNode="P_118F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_360F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_360F10059_O" deadCode="false" sourceNode="P_122F10059" targetNode="P_119F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_360F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_362F10059_I" deadCode="false" sourceNode="P_122F10059" targetNode="P_124F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_362F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_362F10059_O" deadCode="false" sourceNode="P_122F10059" targetNode="P_125F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_362F10059"/>
	</edges>
	<edges id="P_122F10059P_123F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_122F10059" targetNode="P_123F10059"/>
	<edges id="P_124F10059P_125F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_124F10059" targetNode="P_125F10059"/>
	<edges id="P_128F10059P_129F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_128F10059" targetNode="P_129F10059"/>
	<edges id="P_134F10059P_135F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_134F10059" targetNode="P_135F10059"/>
	<edges id="P_140F10059P_141F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_140F10059" targetNode="P_141F10059"/>
	<edges id="P_136F10059P_137F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_136F10059" targetNode="P_137F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_398F10059_I" deadCode="false" sourceNode="P_132F10059" targetNode="P_28F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_398F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_398F10059_O" deadCode="false" sourceNode="P_132F10059" targetNode="P_29F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_398F10059"/>
	</edges>
	<edges id="P_132F10059P_133F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_132F10059" targetNode="P_133F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_402F10059_I" deadCode="false" sourceNode="P_40F10059" targetNode="P_146F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_402F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_402F10059_O" deadCode="false" sourceNode="P_40F10059" targetNode="P_147F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_402F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_404F10059_I" deadCode="false" sourceNode="P_40F10059" targetNode="P_151F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_403F10059"/>
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_404F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_404F10059_O" deadCode="false" sourceNode="P_40F10059" targetNode="P_152F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_403F10059"/>
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_404F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_408F10059_I" deadCode="false" sourceNode="P_40F10059" targetNode="P_144F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_403F10059"/>
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_408F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_408F10059_O" deadCode="false" sourceNode="P_40F10059" targetNode="P_145F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_403F10059"/>
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_408F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_416F10059_I" deadCode="false" sourceNode="P_40F10059" targetNode="P_32F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_403F10059"/>
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_416F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_416F10059_O" deadCode="false" sourceNode="P_40F10059" targetNode="P_33F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_403F10059"/>
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_416F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_419F10059_I" deadCode="false" sourceNode="P_40F10059" targetNode="P_170F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_419F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_419F10059_O" deadCode="false" sourceNode="P_40F10059" targetNode="P_171F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_419F10059"/>
	</edges>
	<edges id="P_40F10059P_41F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10059" targetNode="P_41F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_424F10059_I" deadCode="false" sourceNode="P_42F10059" targetNode="P_170F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_424F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_424F10059_O" deadCode="false" sourceNode="P_42F10059" targetNode="P_171F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_424F10059"/>
	</edges>
	<edges id="P_42F10059P_43F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10059" targetNode="P_43F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_434F10059_I" deadCode="false" sourceNode="P_170F10059" targetNode="P_32F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_434F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_434F10059_O" deadCode="false" sourceNode="P_170F10059" targetNode="P_33F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_434F10059"/>
	</edges>
	<edges id="P_170F10059P_171F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_170F10059" targetNode="P_171F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_437F10059_I" deadCode="false" sourceNode="P_138F10059" targetNode="P_172F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_437F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_437F10059_O" deadCode="false" sourceNode="P_138F10059" targetNode="P_173F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_437F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_440F10059_I" deadCode="false" sourceNode="P_138F10059" targetNode="P_172F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_440F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_440F10059_O" deadCode="false" sourceNode="P_138F10059" targetNode="P_173F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_440F10059"/>
	</edges>
	<edges id="P_138F10059P_139F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_138F10059" targetNode="P_139F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_444F10059_I" deadCode="false" sourceNode="P_130F10059" targetNode="P_174F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_444F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_444F10059_O" deadCode="false" sourceNode="P_130F10059" targetNode="P_175F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_444F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_447F10059_I" deadCode="false" sourceNode="P_130F10059" targetNode="P_174F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_447F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_447F10059_O" deadCode="false" sourceNode="P_130F10059" targetNode="P_175F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_447F10059"/>
	</edges>
	<edges id="P_130F10059P_131F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_130F10059" targetNode="P_131F10059"/>
	<edges id="P_126F10059P_127F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_126F10059" targetNode="P_127F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_452F10059_I" deadCode="false" sourceNode="P_26F10059" targetNode="P_176F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_452F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_452F10059_O" deadCode="false" sourceNode="P_26F10059" targetNode="P_177F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_452F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_454F10059_I" deadCode="false" sourceNode="P_26F10059" targetNode="P_178F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_454F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_454F10059_O" deadCode="false" sourceNode="P_26F10059" targetNode="P_179F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_454F10059"/>
	</edges>
	<edges id="P_26F10059P_27F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10059" targetNode="P_27F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_459F10059_I" deadCode="false" sourceNode="P_176F10059" targetNode="P_172F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_459F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_459F10059_O" deadCode="false" sourceNode="P_176F10059" targetNode="P_173F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_459F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_464F10059_I" deadCode="false" sourceNode="P_176F10059" targetNode="P_172F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_464F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_464F10059_O" deadCode="false" sourceNode="P_176F10059" targetNode="P_173F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_464F10059"/>
	</edges>
	<edges id="P_176F10059P_177F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_176F10059" targetNode="P_177F10059"/>
	<edges id="P_178F10059P_179F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_178F10059" targetNode="P_179F10059"/>
	<edges id="P_172F10059P_173F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_172F10059" targetNode="P_173F10059"/>
	<edges xsi:type="cbl:PerformEdge" id="S_493F10059_I" deadCode="false" sourceNode="P_174F10059" targetNode="P_182F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_493F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_493F10059_O" deadCode="false" sourceNode="P_174F10059" targetNode="P_183F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_493F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_494F10059_I" deadCode="false" sourceNode="P_174F10059" targetNode="P_184F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_494F10059"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_494F10059_O" deadCode="false" sourceNode="P_174F10059" targetNode="P_185F10059">
		<representations href="../../../cobol/IDBSOCS0.cbl.cobModel#S_494F10059"/>
	</edges>
	<edges id="P_174F10059P_175F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_174F10059" targetNode="P_175F10059"/>
	<edges id="P_182F10059P_183F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_182F10059" targetNode="P_183F10059"/>
	<edges id="P_184F10059P_185F10059" xsi:type="cbl:FallThroughEdge" sourceNode="P_184F10059" targetNode="P_185F10059"/>
	<edges xsi:type="cbl:DataEdge" id="S_129F10059_POS1" deadCode="false" targetNode="P_30F10059" sourceNode="DB2_OGG_IN_COASS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_129F10059"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_141F10059_POS1" deadCode="false" sourceNode="P_32F10059" targetNode="DB2_OGG_IN_COASS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_141F10059"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_148F10059_POS1" deadCode="false" sourceNode="P_34F10059" targetNode="DB2_OGG_IN_COASS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_148F10059"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_151F10059_POS1" deadCode="false" sourceNode="P_36F10059" targetNode="DB2_OGG_IN_COASS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_151F10059"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_159F10059_POS1" deadCode="false" targetNode="P_38F10059" sourceNode="DB2_OGG_IN_COASS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_159F10059"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_169F10059_POS1" deadCode="false" sourceNode="P_144F10059" targetNode="DB2_OGG_IN_COASS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_169F10059"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_173F10059_POS1" deadCode="false" targetNode="P_146F10059" sourceNode="DB2_OGG_IN_COASS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_173F10059"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_176F10059_POS1" deadCode="false" targetNode="P_148F10059" sourceNode="DB2_OGG_IN_COASS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_176F10059"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_183F10059_POS1" deadCode="false" targetNode="P_151F10059" sourceNode="DB2_OGG_IN_COASS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_183F10059"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_198F10059_POS1" deadCode="false" targetNode="P_44F10059" sourceNode="DB2_OGG_IN_COASS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_198F10059"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_205F10059_POS1" deadCode="false" targetNode="P_46F10059" sourceNode="DB2_OGG_IN_COASS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_205F10059"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_208F10059_POS1" deadCode="false" targetNode="P_48F10059" sourceNode="DB2_OGG_IN_COASS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_208F10059"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_215F10059_POS1" deadCode="false" targetNode="P_52F10059" sourceNode="DB2_OGG_IN_COASS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_215F10059"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_277F10059_POS1" deadCode="false" targetNode="P_84F10059" sourceNode="DB2_OGG_IN_COASS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_277F10059"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_288F10059_POS1" deadCode="false" targetNode="P_86F10059" sourceNode="DB2_OGG_IN_COASS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_288F10059"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_295F10059_POS1" deadCode="false" targetNode="P_88F10059" sourceNode="DB2_OGG_IN_COASS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_295F10059"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_298F10059_POS1" deadCode="false" targetNode="P_90F10059" sourceNode="DB2_OGG_IN_COASS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_298F10059"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_305F10059_POS1" deadCode="false" targetNode="P_94F10059" sourceNode="DB2_OGG_IN_COASS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_305F10059"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS1400" cbl:id="LDBS1400" xsi:id="LDBS1400" packageRef="LDBS1400.igd#LDBS1400" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS1400_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10158" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS1400.cbl.cobModel#SC_1F10158"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10158" deadCode="false" name="PROGRAM_LDBS1400_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_1F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10158" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_2F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10158" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_3F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10158" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_8F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10158" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_9F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10158" deadCode="false" name="A200-ELABORA">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_4F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10158" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_5F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10158" deadCode="false" name="A210-SELECT">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_10F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10158" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_11F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10158" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_12F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10158" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_13F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10158" deadCode="true" name="Z150-VALORIZZA-DATA-SERVICES">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_16F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10158" deadCode="true" name="Z150-EX">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_17F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10158" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_14F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10158" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_15F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10158" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_6F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10158" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_7F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10158" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_18F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10158" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_19F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10158" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_20F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10158" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_21F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10158" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_22F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10158" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_23F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10158" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_24F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10158" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_25F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10158" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_26F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10158" deadCode="true" name="Z800-EX">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_31F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10158" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_27F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10158" deadCode="true" name="Z810-EX">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_28F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10158" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_29F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10158" deadCode="true" name="Z820-EX">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_30F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10158" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_32F10158"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10158" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS1400.cbl.cobModel#P_33F10158"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_MATR_VAL_VAR" name="MATR_VAL_VAR">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_MATR_VAL_VAR"/>
		</children>
	</packageNode>
	<edges id="SC_1F10158P_1F10158" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10158" targetNode="P_1F10158"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10158_I" deadCode="false" sourceNode="P_1F10158" targetNode="P_2F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_1F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10158_O" deadCode="false" sourceNode="P_1F10158" targetNode="P_3F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_1F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10158_I" deadCode="false" sourceNode="P_1F10158" targetNode="P_4F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_3F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10158_O" deadCode="false" sourceNode="P_1F10158" targetNode="P_5F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_3F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10158_I" deadCode="false" sourceNode="P_2F10158" targetNode="P_6F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_10F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10158_O" deadCode="false" sourceNode="P_2F10158" targetNode="P_7F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_10F10158"/>
	</edges>
	<edges id="P_2F10158P_3F10158" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10158" targetNode="P_3F10158"/>
	<edges id="P_8F10158P_9F10158" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10158" targetNode="P_9F10158"/>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10158_I" deadCode="false" sourceNode="P_4F10158" targetNode="P_10F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_23F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10158_O" deadCode="false" sourceNode="P_4F10158" targetNode="P_11F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_23F10158"/>
	</edges>
	<edges id="P_4F10158P_5F10158" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10158" targetNode="P_5F10158"/>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10158_I" deadCode="false" sourceNode="P_10F10158" targetNode="P_8F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_26F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10158_O" deadCode="false" sourceNode="P_10F10158" targetNode="P_9F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_26F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10158_I" deadCode="false" sourceNode="P_10F10158" targetNode="P_12F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_28F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10158_O" deadCode="false" sourceNode="P_10F10158" targetNode="P_13F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_28F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10158_I" deadCode="false" sourceNode="P_10F10158" targetNode="P_14F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_29F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10158_O" deadCode="false" sourceNode="P_10F10158" targetNode="P_15F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_29F10158"/>
	</edges>
	<edges id="P_10F10158P_11F10158" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10158" targetNode="P_11F10158"/>
	<edges id="P_12F10158P_13F10158" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10158" targetNode="P_13F10158"/>
	<edges id="P_14F10158P_15F10158" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10158" targetNode="P_15F10158"/>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10158_I" deadCode="false" sourceNode="P_6F10158" targetNode="P_18F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_77F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10158_O" deadCode="false" sourceNode="P_6F10158" targetNode="P_19F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_77F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10158_I" deadCode="false" sourceNode="P_6F10158" targetNode="P_20F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_79F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10158_O" deadCode="false" sourceNode="P_6F10158" targetNode="P_21F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_79F10158"/>
	</edges>
	<edges id="P_6F10158P_7F10158" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10158" targetNode="P_7F10158"/>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10158_I" deadCode="true" sourceNode="P_18F10158" targetNode="P_22F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_84F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10158_O" deadCode="true" sourceNode="P_18F10158" targetNode="P_23F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_84F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10158_I" deadCode="true" sourceNode="P_18F10158" targetNode="P_22F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_89F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10158_O" deadCode="true" sourceNode="P_18F10158" targetNode="P_23F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_89F10158"/>
	</edges>
	<edges id="P_18F10158P_19F10158" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10158" targetNode="P_19F10158"/>
	<edges id="P_20F10158P_21F10158" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10158" targetNode="P_21F10158"/>
	<edges id="P_22F10158P_23F10158" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10158" targetNode="P_23F10158"/>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10158_I" deadCode="true" sourceNode="P_26F10158" targetNode="P_27F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_118F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10158_O" deadCode="true" sourceNode="P_26F10158" targetNode="P_28F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_118F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10158_I" deadCode="true" sourceNode="P_26F10158" targetNode="P_29F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_119F10158"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10158_O" deadCode="true" sourceNode="P_26F10158" targetNode="P_30F10158">
		<representations href="../../../cobol/LDBS1400.cbl.cobModel#S_119F10158"/>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_25F10158_POS1" deadCode="false" targetNode="P_10F10158" sourceNode="DB2_MATR_VAL_VAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_25F10158"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IABS0070" cbl:id="IABS0070" xsi:id="IABS0070" packageRef="IABS0070.igd#IABS0070" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IABS0070_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10006" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IABS0070.cbl.cobModel#SC_1F10006"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10006" deadCode="false" name="PROGRAM_IABS0070_FIRST_SENTENCES">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_1F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10006" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_2F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10006" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_3F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10006" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_8F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10006" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_9F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10006" deadCode="false" name="A300-ELABORA">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_4F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10006" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_5F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10006" deadCode="false" name="A305-DECLARE-CURSOR">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_20F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10006" deadCode="false" name="A305-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_21F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10006" deadCode="false" name="A310-SELECT">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_10F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10006" deadCode="false" name="A310-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_11F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10006" deadCode="false" name="A360-OPEN-CURSOR">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_12F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10006" deadCode="false" name="A360-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_13F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10006" deadCode="false" name="A370-CLOSE-CURSOR">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_14F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10006" deadCode="false" name="A370-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_15F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10006" deadCode="false" name="A380-FETCH-FIRST">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_16F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10006" deadCode="false" name="A380-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_17F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10006" deadCode="false" name="A390-FETCH-NEXT">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_18F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10006" deadCode="false" name="A390-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_19F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10006" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_22F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10006" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_23F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10006" deadCode="true" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_26F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10006" deadCode="true" name="Z200-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_27F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10006" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_28F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10006" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_29F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10006" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_24F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10006" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_25F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10006" deadCode="true" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_30F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10006" deadCode="true" name="Z960-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_31F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10006" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_6F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10006" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_7F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10006" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_32F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10006" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_33F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10006" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_34F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10006" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_35F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10006" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_36F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10006" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_37F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10006" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_38F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10006" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_39F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10006" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_40F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10006" deadCode="true" name="Z800-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_45F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10006" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_41F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10006" deadCode="true" name="Z810-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_42F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10006" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_43F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10006" deadCode="true" name="Z820-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_44F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10006" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_46F10006"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10006" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/IABS0070.cbl.cobModel#P_47F10006"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_BTC_BATCH_STATE" name="BTC_BATCH_STATE">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_BTC_BATCH_STATE"/>
		</children>
	</packageNode>
	<edges id="SC_1F10006P_1F10006" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10006" targetNode="P_1F10006"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10006_I" deadCode="false" sourceNode="P_1F10006" targetNode="P_2F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_1F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10006_O" deadCode="false" sourceNode="P_1F10006" targetNode="P_3F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_1F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10006_I" deadCode="false" sourceNode="P_1F10006" targetNode="P_4F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_2F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10006_O" deadCode="false" sourceNode="P_1F10006" targetNode="P_5F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_2F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10006_I" deadCode="false" sourceNode="P_2F10006" targetNode="P_6F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_9F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10006_O" deadCode="false" sourceNode="P_2F10006" targetNode="P_7F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_9F10006"/>
	</edges>
	<edges id="P_2F10006P_3F10006" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10006" targetNode="P_3F10006"/>
	<edges id="P_8F10006P_9F10006" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10006" targetNode="P_9F10006"/>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10006_I" deadCode="false" sourceNode="P_4F10006" targetNode="P_10F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_22F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10006_O" deadCode="false" sourceNode="P_4F10006" targetNode="P_11F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_22F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10006_I" deadCode="false" sourceNode="P_4F10006" targetNode="P_12F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_23F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10006_O" deadCode="false" sourceNode="P_4F10006" targetNode="P_13F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_23F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10006_I" deadCode="false" sourceNode="P_4F10006" targetNode="P_14F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_24F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10006_O" deadCode="false" sourceNode="P_4F10006" targetNode="P_15F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_24F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10006_I" deadCode="false" sourceNode="P_4F10006" targetNode="P_16F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_25F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10006_O" deadCode="false" sourceNode="P_4F10006" targetNode="P_17F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_25F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10006_I" deadCode="false" sourceNode="P_4F10006" targetNode="P_18F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_26F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10006_O" deadCode="false" sourceNode="P_4F10006" targetNode="P_19F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_26F10006"/>
	</edges>
	<edges id="P_4F10006P_5F10006" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10006" targetNode="P_5F10006"/>
	<edges id="P_20F10006P_21F10006" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10006" targetNode="P_21F10006"/>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10006_I" deadCode="false" sourceNode="P_10F10006" targetNode="P_8F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_33F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10006_O" deadCode="false" sourceNode="P_10F10006" targetNode="P_9F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_33F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10006_I" deadCode="false" sourceNode="P_10F10006" targetNode="P_22F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_35F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10006_O" deadCode="false" sourceNode="P_10F10006" targetNode="P_23F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_35F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10006_I" deadCode="false" sourceNode="P_10F10006" targetNode="P_24F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_36F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10006_O" deadCode="false" sourceNode="P_10F10006" targetNode="P_25F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_36F10006"/>
	</edges>
	<edges id="P_10F10006P_11F10006" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10006" targetNode="P_11F10006"/>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10006_I" deadCode="false" sourceNode="P_12F10006" targetNode="P_20F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_38F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10006_O" deadCode="false" sourceNode="P_12F10006" targetNode="P_21F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_38F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10006_I" deadCode="false" sourceNode="P_12F10006" targetNode="P_8F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_40F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10006_O" deadCode="false" sourceNode="P_12F10006" targetNode="P_9F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_40F10006"/>
	</edges>
	<edges id="P_12F10006P_13F10006" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10006" targetNode="P_13F10006"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10006_I" deadCode="false" sourceNode="P_14F10006" targetNode="P_8F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_43F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10006_O" deadCode="false" sourceNode="P_14F10006" targetNode="P_9F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_43F10006"/>
	</edges>
	<edges id="P_14F10006P_15F10006" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10006" targetNode="P_15F10006"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10006_I" deadCode="false" sourceNode="P_16F10006" targetNode="P_12F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_45F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10006_O" deadCode="false" sourceNode="P_16F10006" targetNode="P_13F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_45F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10006_I" deadCode="false" sourceNode="P_16F10006" targetNode="P_18F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_47F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10006_O" deadCode="false" sourceNode="P_16F10006" targetNode="P_19F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_47F10006"/>
	</edges>
	<edges id="P_16F10006P_17F10006" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10006" targetNode="P_17F10006"/>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10006_I" deadCode="false" sourceNode="P_18F10006" targetNode="P_8F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_50F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10006_O" deadCode="false" sourceNode="P_18F10006" targetNode="P_9F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_50F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10006_I" deadCode="false" sourceNode="P_18F10006" targetNode="P_22F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_52F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10006_O" deadCode="false" sourceNode="P_18F10006" targetNode="P_23F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_52F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10006_I" deadCode="false" sourceNode="P_18F10006" targetNode="P_24F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_53F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10006_O" deadCode="false" sourceNode="P_18F10006" targetNode="P_25F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_53F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10006_I" deadCode="false" sourceNode="P_18F10006" targetNode="P_14F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_55F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10006_O" deadCode="false" sourceNode="P_18F10006" targetNode="P_15F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_55F10006"/>
	</edges>
	<edges id="P_18F10006P_19F10006" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10006" targetNode="P_19F10006"/>
	<edges id="P_22F10006P_23F10006" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10006" targetNode="P_23F10006"/>
	<edges id="P_24F10006P_25F10006" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10006" targetNode="P_25F10006"/>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10006_I" deadCode="false" sourceNode="P_6F10006" targetNode="P_32F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_78F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10006_O" deadCode="false" sourceNode="P_6F10006" targetNode="P_33F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_78F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10006_I" deadCode="false" sourceNode="P_6F10006" targetNode="P_34F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_80F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10006_O" deadCode="false" sourceNode="P_6F10006" targetNode="P_35F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_80F10006"/>
	</edges>
	<edges id="P_6F10006P_7F10006" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10006" targetNode="P_7F10006"/>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10006_I" deadCode="true" sourceNode="P_32F10006" targetNode="P_36F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_85F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10006_O" deadCode="true" sourceNode="P_32F10006" targetNode="P_37F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_85F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10006_I" deadCode="true" sourceNode="P_32F10006" targetNode="P_36F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_90F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10006_O" deadCode="true" sourceNode="P_32F10006" targetNode="P_37F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_90F10006"/>
	</edges>
	<edges id="P_32F10006P_33F10006" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10006" targetNode="P_33F10006"/>
	<edges id="P_34F10006P_35F10006" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10006" targetNode="P_35F10006"/>
	<edges id="P_36F10006P_37F10006" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10006" targetNode="P_37F10006"/>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10006_I" deadCode="true" sourceNode="P_40F10006" targetNode="P_41F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_119F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10006_O" deadCode="true" sourceNode="P_40F10006" targetNode="P_42F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_119F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10006_I" deadCode="true" sourceNode="P_40F10006" targetNode="P_43F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_120F10006"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10006_O" deadCode="true" sourceNode="P_40F10006" targetNode="P_44F10006">
		<representations href="../../../cobol/IABS0070.cbl.cobModel#S_120F10006"/>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_32F10006_POS1" deadCode="false" targetNode="P_10F10006" sourceNode="DB2_BTC_BATCH_STATE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_32F10006"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_39F10006_POS1" deadCode="false" targetNode="P_12F10006" sourceNode="DB2_BTC_BATCH_STATE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_39F10006"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_42F10006_POS1" deadCode="false" targetNode="P_14F10006" sourceNode="DB2_BTC_BATCH_STATE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_42F10006"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_49F10006_POS1" deadCode="false" targetNode="P_18F10006" sourceNode="DB2_BTC_BATCH_STATE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_49F10006"></representations>
	</edges>
</Package>

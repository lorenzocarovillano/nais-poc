<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS1010" cbl:id="LVVS1010" xsi:id="LVVS1010" packageRef="LVVS1010.igd#LVVS1010" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS1010_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10364" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS1010.cbl.cobModel#SC_1F10364"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10364" deadCode="false" name="PROGRAM_LVVS1010_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS1010.cbl.cobModel#P_1F10364"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10364" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS1010.cbl.cobModel#P_2F10364"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10364" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS1010.cbl.cobModel#P_3F10364"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10364" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS1010.cbl.cobModel#P_4F10364"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10364" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS1010.cbl.cobModel#P_5F10364"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10364" deadCode="false" name="RECUP-MOVI-COMUN">
				<representations href="../../../cobol/LVVS1010.cbl.cobModel#P_8F10364"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10364" deadCode="false" name="RECUP-MOVI-COMUN-EX">
				<representations href="../../../cobol/LVVS1010.cbl.cobModel#P_9F10364"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10364" deadCode="false" name="CLOSE-MOVI">
				<representations href="../../../cobol/LVVS1010.cbl.cobModel#P_12F10364"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10364" deadCode="false" name="CLOSE-MOVI-EX">
				<representations href="../../../cobol/LVVS1010.cbl.cobModel#P_13F10364"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10364" deadCode="false" name="RECUP-CUM-PRE-ATT-TRA">
				<representations href="../../../cobol/LVVS1010.cbl.cobModel#P_10F10364"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10364" deadCode="false" name="RECUP-CUM-PRE-ATT-TRA-EX">
				<representations href="../../../cobol/LVVS1010.cbl.cobModel#P_11F10364"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10364" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS1010.cbl.cobModel#P_6F10364"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10364" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS1010.cbl.cobModel#P_7F10364"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS6040" name="LDBS6040">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10230"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSE120" name="IDBSE120">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10037"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10364P_1F10364" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10364" targetNode="P_1F10364"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10364_I" deadCode="false" sourceNode="P_1F10364" targetNode="P_2F10364">
		<representations href="../../../cobol/LVVS1010.cbl.cobModel#S_1F10364"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10364_O" deadCode="false" sourceNode="P_1F10364" targetNode="P_3F10364">
		<representations href="../../../cobol/LVVS1010.cbl.cobModel#S_1F10364"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10364_I" deadCode="false" sourceNode="P_1F10364" targetNode="P_4F10364">
		<representations href="../../../cobol/LVVS1010.cbl.cobModel#S_3F10364"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10364_O" deadCode="false" sourceNode="P_1F10364" targetNode="P_5F10364">
		<representations href="../../../cobol/LVVS1010.cbl.cobModel#S_3F10364"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10364_I" deadCode="false" sourceNode="P_1F10364" targetNode="P_6F10364">
		<representations href="../../../cobol/LVVS1010.cbl.cobModel#S_4F10364"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10364_O" deadCode="false" sourceNode="P_1F10364" targetNode="P_7F10364">
		<representations href="../../../cobol/LVVS1010.cbl.cobModel#S_4F10364"/>
	</edges>
	<edges id="P_2F10364P_3F10364" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10364" targetNode="P_3F10364"/>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10364_I" deadCode="false" sourceNode="P_4F10364" targetNode="P_8F10364">
		<representations href="../../../cobol/LVVS1010.cbl.cobModel#S_12F10364"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10364_O" deadCode="false" sourceNode="P_4F10364" targetNode="P_9F10364">
		<representations href="../../../cobol/LVVS1010.cbl.cobModel#S_12F10364"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10364_I" deadCode="false" sourceNode="P_4F10364" targetNode="P_10F10364">
		<representations href="../../../cobol/LVVS1010.cbl.cobModel#S_20F10364"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10364_O" deadCode="false" sourceNode="P_4F10364" targetNode="P_11F10364">
		<representations href="../../../cobol/LVVS1010.cbl.cobModel#S_20F10364"/>
	</edges>
	<edges id="P_4F10364P_5F10364" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10364" targetNode="P_5F10364"/>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10364_I" deadCode="false" sourceNode="P_8F10364" targetNode="P_12F10364">
		<representations href="../../../cobol/LVVS1010.cbl.cobModel#S_31F10364"/>
		<representations href="../../../cobol/LVVS1010.cbl.cobModel#S_54F10364"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10364_O" deadCode="false" sourceNode="P_8F10364" targetNode="P_13F10364">
		<representations href="../../../cobol/LVVS1010.cbl.cobModel#S_31F10364"/>
		<representations href="../../../cobol/LVVS1010.cbl.cobModel#S_54F10364"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10364_I" deadCode="false" sourceNode="P_8F10364" targetNode="P_12F10364">
		<representations href="../../../cobol/LVVS1010.cbl.cobModel#S_31F10364"/>
		<representations href="../../../cobol/LVVS1010.cbl.cobModel#S_63F10364"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10364_O" deadCode="false" sourceNode="P_8F10364" targetNode="P_13F10364">
		<representations href="../../../cobol/LVVS1010.cbl.cobModel#S_31F10364"/>
		<representations href="../../../cobol/LVVS1010.cbl.cobModel#S_63F10364"/>
	</edges>
	<edges id="P_8F10364P_9F10364" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10364" targetNode="P_9F10364"/>
	<edges id="P_12F10364P_13F10364" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10364" targetNode="P_13F10364"/>
	<edges id="P_10F10364P_11F10364" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10364" targetNode="P_11F10364"/>
	<edges xsi:type="cbl:CallEdge" id="S_40F10364" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_8F10364" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_40F10364"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_81F10364" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_12F10364" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_81F10364"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_107F10364" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_10F10364" targetNode="IDBSE120">
		<representations href="../../../cobol/../importantStmts.cobModel#S_107F10364"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LCCS0029" cbl:id="LCCS0029" xsi:id="LCCS0029" packageRef="LCCS0029.igd#LCCS0029" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LCCS0029_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10129" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LCCS0029.cbl.cobModel#SC_1F10129"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10129" deadCode="false" name="PROGRAM_LCCS0029_FIRST_SENTENCES">
				<representations href="../../../cobol/LCCS0029.cbl.cobModel#P_1F10129"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10129" deadCode="false" name="S00000-OPERAZ-INIZIALI">
				<representations href="../../../cobol/LCCS0029.cbl.cobModel#P_2F10129"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10129" deadCode="false" name="S00000-OPERAZ-INIZIALI-EX">
				<representations href="../../../cobol/LCCS0029.cbl.cobModel#P_3F10129"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10129" deadCode="false" name="S10000-ELABORAZIONE">
				<representations href="../../../cobol/LCCS0029.cbl.cobModel#P_4F10129"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10129" deadCode="false" name="S10000-ELABORAZIONE-EX">
				<representations href="../../../cobol/LCCS0029.cbl.cobModel#P_5F10129"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10129" deadCode="false" name="S10100-VALIDA-FINE-MESE">
				<representations href="../../../cobol/LCCS0029.cbl.cobModel#P_10F10129"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10129" deadCode="false" name="S10100-VALIDA-FINE-MESE-EX">
				<representations href="../../../cobol/LCCS0029.cbl.cobModel#P_11F10129"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10129" deadCode="false" name="S90000-OPERAZ-FINALI">
				<representations href="../../../cobol/LCCS0029.cbl.cobModel#P_6F10129"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10129" deadCode="true" name="S90000-OPERAZ-FINALI-EX">
				<representations href="../../../cobol/LCCS0029.cbl.cobModel#P_7F10129"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10129" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LCCS0029.cbl.cobModel#P_8F10129"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10129" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LCCS0029.cbl.cobModel#P_9F10129"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10129" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LCCS0029.cbl.cobModel#P_16F10129"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10129" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LCCS0029.cbl.cobModel#P_17F10129"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10129" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LCCS0029.cbl.cobModel#P_14F10129"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10129" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LCCS0029.cbl.cobModel#P_15F10129"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10129" deadCode="false" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LCCS0029.cbl.cobModel#P_12F10129"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10129" deadCode="false" name="EX-S0290">
				<representations href="../../../cobol/LCCS0029.cbl.cobModel#P_13F10129"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0004" name="LCCS0004">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10120"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10129P_1F10129" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10129" targetNode="P_1F10129"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10129_I" deadCode="false" sourceNode="P_1F10129" targetNode="P_2F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_1F10129"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10129_O" deadCode="false" sourceNode="P_1F10129" targetNode="P_3F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_1F10129"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10129_I" deadCode="false" sourceNode="P_1F10129" targetNode="P_4F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_3F10129"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10129_O" deadCode="false" sourceNode="P_1F10129" targetNode="P_5F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_3F10129"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10129_I" deadCode="false" sourceNode="P_1F10129" targetNode="P_6F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_4F10129"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10129_O" deadCode="false" sourceNode="P_1F10129" targetNode="P_7F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_4F10129"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10129_I" deadCode="false" sourceNode="P_2F10129" targetNode="P_8F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_12F10129"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10129_O" deadCode="false" sourceNode="P_2F10129" targetNode="P_9F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_12F10129"/>
	</edges>
	<edges id="P_2F10129P_3F10129" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10129" targetNode="P_3F10129"/>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10129_I" deadCode="false" sourceNode="P_4F10129" targetNode="P_10F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_18F10129"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10129_O" deadCode="false" sourceNode="P_4F10129" targetNode="P_11F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_18F10129"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10129_I" deadCode="false" sourceNode="P_4F10129" targetNode="P_8F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_25F10129"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10129_O" deadCode="false" sourceNode="P_4F10129" targetNode="P_9F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_25F10129"/>
	</edges>
	<edges id="P_4F10129P_5F10129" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10129" targetNode="P_5F10129"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10129_I" deadCode="false" sourceNode="P_10F10129" targetNode="P_12F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_37F10129"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10129_O" deadCode="false" sourceNode="P_10F10129" targetNode="P_13F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_37F10129"/>
	</edges>
	<edges id="P_10F10129P_11F10129" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10129" targetNode="P_11F10129"/>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10129_I" deadCode="false" sourceNode="P_8F10129" targetNode="P_14F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_49F10129"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10129_O" deadCode="false" sourceNode="P_8F10129" targetNode="P_15F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_49F10129"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10129_I" deadCode="false" sourceNode="P_8F10129" targetNode="P_16F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_53F10129"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10129_O" deadCode="false" sourceNode="P_8F10129" targetNode="P_17F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_53F10129"/>
	</edges>
	<edges id="P_8F10129P_9F10129" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10129" targetNode="P_9F10129"/>
	<edges id="P_16F10129P_17F10129" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10129" targetNode="P_17F10129"/>
	<edges id="P_14F10129P_15F10129" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10129" targetNode="P_15F10129"/>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10129_I" deadCode="false" sourceNode="P_12F10129" targetNode="P_8F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_89F10129"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10129_O" deadCode="false" sourceNode="P_12F10129" targetNode="P_9F10129">
		<representations href="../../../cobol/LCCS0029.cbl.cobModel#S_89F10129"/>
	</edges>
	<edges id="P_12F10129P_13F10129" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10129" targetNode="P_13F10129"/>
	<edges xsi:type="cbl:CallEdge" id="S_33F10129" deadCode="false" name="Dynamic LCCS0004" sourceNode="P_10F10129" targetNode="LCCS0004">
		<representations href="../../../cobol/../importantStmts.cobModel#S_33F10129"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_47F10129" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_8F10129" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_47F10129"></representations>
	</edges>
</Package>

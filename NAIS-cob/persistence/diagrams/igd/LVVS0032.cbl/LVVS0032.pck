<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0032" cbl:id="LVVS0032" xsi:id="LVVS0032" packageRef="LVVS0032.igd#LVVS0032" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0032_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10323" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0032.cbl.cobModel#SC_1F10323"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10323" deadCode="false" name="PROGRAM_LVVS0032_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0032.cbl.cobModel#P_1F10323"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10323" deadCode="false" name="L000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0032.cbl.cobModel#P_2F10323"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10323" deadCode="false" name="L000-EX">
				<representations href="../../../cobol/LVVS0032.cbl.cobModel#P_3F10323"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10323" deadCode="false" name="L100-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0032.cbl.cobModel#P_4F10323"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10323" deadCode="false" name="L100-EX">
				<representations href="../../../cobol/LVVS0032.cbl.cobModel#P_5F10323"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10323" deadCode="false" name="L500-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0032.cbl.cobModel#P_8F10323"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10323" deadCode="false" name="L500-EX">
				<representations href="../../../cobol/LVVS0032.cbl.cobModel#P_9F10323"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10323" deadCode="false" name="L600-PREPARA-CALL">
				<representations href="../../../cobol/LVVS0032.cbl.cobModel#P_12F10323"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10323" deadCode="false" name="L600-EX">
				<representations href="../../../cobol/LVVS0032.cbl.cobModel#P_13F10323"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10323" deadCode="false" name="L700-CALL-LVVS0000">
				<representations href="../../../cobol/LVVS0032.cbl.cobModel#P_10F10323"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10323" deadCode="false" name="L700-EX">
				<representations href="../../../cobol/LVVS0032.cbl.cobModel#P_11F10323"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10323" deadCode="false" name="L900-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0032.cbl.cobModel#P_6F10323"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10323" deadCode="true" name="L900-EX">
				<representations href="../../../cobol/LVVS0032.cbl.cobModel#P_7F10323"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LVVS0000" name="LVVS0000">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10304"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10323P_1F10323" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10323" targetNode="P_1F10323"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10323_I" deadCode="false" sourceNode="P_1F10323" targetNode="P_2F10323">
		<representations href="../../../cobol/LVVS0032.cbl.cobModel#S_1F10323"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10323_O" deadCode="false" sourceNode="P_1F10323" targetNode="P_3F10323">
		<representations href="../../../cobol/LVVS0032.cbl.cobModel#S_1F10323"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10323_I" deadCode="false" sourceNode="P_1F10323" targetNode="P_4F10323">
		<representations href="../../../cobol/LVVS0032.cbl.cobModel#S_2F10323"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10323_O" deadCode="false" sourceNode="P_1F10323" targetNode="P_5F10323">
		<representations href="../../../cobol/LVVS0032.cbl.cobModel#S_2F10323"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10323_I" deadCode="false" sourceNode="P_1F10323" targetNode="P_6F10323">
		<representations href="../../../cobol/LVVS0032.cbl.cobModel#S_3F10323"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10323_O" deadCode="false" sourceNode="P_1F10323" targetNode="P_7F10323">
		<representations href="../../../cobol/LVVS0032.cbl.cobModel#S_3F10323"/>
	</edges>
	<edges id="P_2F10323P_3F10323" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10323" targetNode="P_3F10323"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10323_I" deadCode="false" sourceNode="P_4F10323" targetNode="P_8F10323">
		<representations href="../../../cobol/LVVS0032.cbl.cobModel#S_10F10323"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10323_O" deadCode="false" sourceNode="P_4F10323" targetNode="P_9F10323">
		<representations href="../../../cobol/LVVS0032.cbl.cobModel#S_10F10323"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10323_I" deadCode="false" sourceNode="P_4F10323" targetNode="P_10F10323">
		<representations href="../../../cobol/LVVS0032.cbl.cobModel#S_12F10323"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10323_O" deadCode="false" sourceNode="P_4F10323" targetNode="P_11F10323">
		<representations href="../../../cobol/LVVS0032.cbl.cobModel#S_12F10323"/>
	</edges>
	<edges id="P_4F10323P_5F10323" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10323" targetNode="P_5F10323"/>
	<edges id="P_8F10323P_9F10323" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10323" targetNode="P_9F10323"/>
	<edges id="P_12F10323P_13F10323" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10323" targetNode="P_13F10323"/>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10323_I" deadCode="false" sourceNode="P_10F10323" targetNode="P_12F10323">
		<representations href="../../../cobol/LVVS0032.cbl.cobModel#S_36F10323"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10323_O" deadCode="false" sourceNode="P_10F10323" targetNode="P_13F10323">
		<representations href="../../../cobol/LVVS0032.cbl.cobModel#S_36F10323"/>
	</edges>
	<edges id="P_10F10323P_11F10323" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10323" targetNode="P_11F10323"/>
	<edges xsi:type="cbl:CallEdge" id="S_37F10323" deadCode="false" name="Dynamic PGM-LVVS0000" sourceNode="P_10F10323" targetNode="LVVS0000">
		<representations href="../../../cobol/../importantStmts.cobModel#S_37F10323"></representations>
	</edges>
</Package>

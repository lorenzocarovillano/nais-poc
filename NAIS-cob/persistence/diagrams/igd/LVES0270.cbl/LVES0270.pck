<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVES0270" cbl:id="LVES0270" xsi:id="LVES0270" packageRef="LVES0270.igd#LVES0270" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVES0270_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10303" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVES0270.cbl.cobModel#SC_1F10303"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10303" deadCode="false" name="PROGRAM_LVES0270_FIRST_SENTENCES">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_1F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10303" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_2F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10303" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_3F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10303" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_4F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10303" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_5F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10303" deadCode="false" name="S1100-CONTROLLI-BUSINESS">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_8F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10303" deadCode="false" name="EX-S1100">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_9F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10303" deadCode="false" name="S1170-CTRL-BENEFICIARI">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_14F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10303" deadCode="false" name="S1170-EX">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_15F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10303" deadCode="true" name="S1160-IMPOSTA-TEMP">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_16F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10303" deadCode="true" name="EX-S1160">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_17F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10303" deadCode="false" name="S1200-CALCOLA-PREST-INI">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_12F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10303" deadCode="false" name="S1200-EX">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_13F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10303" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_6F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10303" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_7F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10303" deadCode="true" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_18F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10303" deadCode="true" name="EX-S0290">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_19F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10303" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_10F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10303" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_11F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10303" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_22F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10303" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_23F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10303" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_20F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10303" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_21F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10303" deadCode="true" name="VERIFICA-PLATFOND">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_24F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10303" deadCode="true" name="VERIFICA-PLATFOND-EX">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_27F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10303" deadCode="true" name="VERIFICA-POG-PLTFOND">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_25F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10303" deadCode="true" name="VERIFICA-POG-PLTFOND-EX">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_26F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10303" deadCode="true" name="GESTIONE-TEMP-TABLE">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_28F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10303" deadCode="true" name="GESTIONE-TEMP-TABLE-EX">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_29F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10303" deadCode="true" name="GESTIONE-ELE-MAX-TEMP">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_30F10303"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10303" deadCode="true" name="GESTIONE-ELE-MAX-TEMP-EX">
				<representations href="../../../cobol/LVES0270.cbl.cobModel#P_31F10303"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LVES0270_LCCS0019" name="Dynamic LVES0270 LCCS0019" missing="true">
			<representations href="../../../../missing.xmi#IDJYPVOVUDVURJOLEDMK3NFR4R2HBEBDNA3YEVEEFZUX4XLVL3RJVJ"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LVES0270_IDSS0300" name="Dynamic LVES0270 IDSS0300" missing="true">
			<representations href="../../../../missing.xmi#IDFPD45GEZMLFXFIUE0W54BF2VXM0XA1MFVAA5REKRENWZ3214XW4I"/>
		</children>
	</packageNode>
	<edges id="SC_1F10303P_1F10303" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10303" targetNode="P_1F10303"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10303_I" deadCode="false" sourceNode="P_1F10303" targetNode="P_2F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_1F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10303_O" deadCode="false" sourceNode="P_1F10303" targetNode="P_3F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_1F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10303_I" deadCode="false" sourceNode="P_1F10303" targetNode="P_4F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_3F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10303_O" deadCode="false" sourceNode="P_1F10303" targetNode="P_5F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_3F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10303_I" deadCode="false" sourceNode="P_1F10303" targetNode="P_6F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_4F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10303_O" deadCode="false" sourceNode="P_1F10303" targetNode="P_7F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_4F10303"/>
	</edges>
	<edges id="P_2F10303P_3F10303" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10303" targetNode="P_3F10303"/>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10303_I" deadCode="false" sourceNode="P_4F10303" targetNode="P_8F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_9F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10303_O" deadCode="false" sourceNode="P_4F10303" targetNode="P_9F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_9F10303"/>
	</edges>
	<edges id="P_4F10303P_5F10303" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10303" targetNode="P_5F10303"/>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10303_I" deadCode="false" sourceNode="P_8F10303" targetNode="P_10F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_18F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10303_O" deadCode="false" sourceNode="P_8F10303" targetNode="P_11F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_18F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10303_I" deadCode="false" sourceNode="P_8F10303" targetNode="P_10F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_25F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10303_O" deadCode="false" sourceNode="P_8F10303" targetNode="P_11F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_25F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10303_I" deadCode="false" sourceNode="P_8F10303" targetNode="P_12F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_27F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10303_O" deadCode="false" sourceNode="P_8F10303" targetNode="P_13F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_27F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10303_I" deadCode="false" sourceNode="P_8F10303" targetNode="P_10F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_34F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10303_O" deadCode="false" sourceNode="P_8F10303" targetNode="P_11F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_34F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10303_I" deadCode="false" sourceNode="P_8F10303" targetNode="P_14F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_36F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10303_O" deadCode="false" sourceNode="P_8F10303" targetNode="P_15F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_36F10303"/>
	</edges>
	<edges id="P_8F10303P_9F10303" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10303" targetNode="P_9F10303"/>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10303_I" deadCode="false" sourceNode="P_14F10303" targetNode="P_10F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_52F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10303_O" deadCode="false" sourceNode="P_14F10303" targetNode="P_11F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_52F10303"/>
	</edges>
	<edges id="P_14F10303P_15F10303" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10303" targetNode="P_15F10303"/>
	<edges id="P_12F10303P_13F10303" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10303" targetNode="P_13F10303"/>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10303_I" deadCode="true" sourceNode="P_18F10303" targetNode="P_10F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_70F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10303_O" deadCode="true" sourceNode="P_18F10303" targetNode="P_11F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_70F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10303_I" deadCode="false" sourceNode="P_10F10303" targetNode="P_20F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_77F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10303_O" deadCode="false" sourceNode="P_10F10303" targetNode="P_21F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_77F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10303_I" deadCode="false" sourceNode="P_10F10303" targetNode="P_22F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_81F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10303_O" deadCode="false" sourceNode="P_10F10303" targetNode="P_23F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_81F10303"/>
	</edges>
	<edges id="P_10F10303P_11F10303" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10303" targetNode="P_11F10303"/>
	<edges id="P_22F10303P_23F10303" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10303" targetNode="P_23F10303"/>
	<edges id="P_20F10303P_21F10303" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10303" targetNode="P_21F10303"/>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10303_I" deadCode="true" sourceNode="P_24F10303" targetNode="P_25F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_115F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10303_O" deadCode="true" sourceNode="P_24F10303" targetNode="P_26F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_115F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10303_I" deadCode="true" sourceNode="P_24F10303" targetNode="P_18F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_146F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10303_O" deadCode="true" sourceNode="P_24F10303" targetNode="P_19F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_146F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10303_I" deadCode="true" sourceNode="P_28F10303" targetNode="P_18F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_164F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10303_O" deadCode="true" sourceNode="P_28F10303" targetNode="P_19F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_164F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10303_I" deadCode="true" sourceNode="P_28F10303" targetNode="P_10F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_172F10303"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10303_O" deadCode="true" sourceNode="P_28F10303" targetNode="P_11F10303">
		<representations href="../../../cobol/LVES0270.cbl.cobModel#S_172F10303"/>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_75F10303" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_10F10303" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_75F10303"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_142F10303" deadCode="true" name="Dynamic LCCS0019" sourceNode="P_24F10303" targetNode="Dynamic_LVES0270_LCCS0019">
		<representations href="../../../cobol/../importantStmts.cobModel#S_142F10303"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_160F10303" deadCode="true" name="Dynamic IDSS0300" sourceNode="P_28F10303" targetNode="Dynamic_LVES0270_IDSS0300">
		<representations href="../../../cobol/../importantStmts.cobModel#S_160F10303"></representations>
	</edges>
</Package>

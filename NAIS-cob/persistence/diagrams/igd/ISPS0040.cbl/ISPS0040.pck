<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="ISPS0040" cbl:id="ISPS0040" xsi:id="ISPS0040" packageRef="ISPS0040.igd#ISPS0040" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="ISPS0040_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10110" deadCode="false" name="FIRST">
			<representations href="../../../cobol/ISPS0040.cbl.cobModel#SC_1F10110"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10110" deadCode="false" name="PROGRAM_ISPS0040_FIRST_SENTENCES">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_1F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10110" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_2F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10110" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_3F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10110" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_4F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10110" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_5F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10110" deadCode="false" name="S1050-PREP-AREA-ISPS0040">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_8F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10110" deadCode="false" name="EX-S1050">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_9F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10110" deadCode="false" name="S1100-CALL-ISPS0040">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_10F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10110" deadCode="false" name="EX-S1100">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_11F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10110" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_6F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10110" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_7F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10110" deadCode="false" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_12F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10110" deadCode="false" name="EX-S0290">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_13F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10110" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_16F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10110" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_17F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10110" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_20F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10110" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_21F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10110" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_18F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10110" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_19F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10110" deadCode="false" name="S0320-OUTPUT-PRODOTTO">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_14F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10110" deadCode="false" name="EX-S0320-OUTPUT-PRODOTTO">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_15F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10110" deadCode="false" name="S0322-SALVA-ERRORE">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_22F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10110" deadCode="false" name="EX-S0322">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_23F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10110" deadCode="false" name="S0321-ERRORI-DEROGA">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_24F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10110" deadCode="false" name="EX-S0321">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_25F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10110" deadCode="false" name="S0323-SALVA-DEROGA">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_26F10110"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10110" deadCode="false" name="EX-S0323">
				<representations href="../../../cobol/ISPS0040.cbl.cobModel#P_27F10110"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IJCSMQ01" name="IJCSMQ01" missing="true">
			<representations href="../../../../missing.xmi#ID5ATRMDNFB0YCCMGWHJU5JJS0SG4JNFAX4FQPUOM0TGMAVTOXJ3AJ"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10110P_1F10110" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10110" targetNode="P_1F10110"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10110_I" deadCode="false" sourceNode="P_1F10110" targetNode="P_2F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_1F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10110_O" deadCode="false" sourceNode="P_1F10110" targetNode="P_3F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_1F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10110_I" deadCode="false" sourceNode="P_1F10110" targetNode="P_4F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_3F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10110_O" deadCode="false" sourceNode="P_1F10110" targetNode="P_5F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_3F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10110_I" deadCode="false" sourceNode="P_1F10110" targetNode="P_6F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_4F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10110_O" deadCode="false" sourceNode="P_1F10110" targetNode="P_7F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_4F10110"/>
	</edges>
	<edges id="P_2F10110P_3F10110" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10110" targetNode="P_3F10110"/>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10110_I" deadCode="false" sourceNode="P_4F10110" targetNode="P_8F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_7F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10110_O" deadCode="false" sourceNode="P_4F10110" targetNode="P_9F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_7F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10110_I" deadCode="false" sourceNode="P_4F10110" targetNode="P_10F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_8F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10110_O" deadCode="false" sourceNode="P_4F10110" targetNode="P_11F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_8F10110"/>
	</edges>
	<edges id="P_4F10110P_5F10110" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10110" targetNode="P_5F10110"/>
	<edges id="P_8F10110P_9F10110" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10110" targetNode="P_9F10110"/>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10110_I" deadCode="false" sourceNode="P_10F10110" targetNode="P_12F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_25F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10110_O" deadCode="false" sourceNode="P_10F10110" targetNode="P_13F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_25F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10110_I" deadCode="false" sourceNode="P_10F10110" targetNode="P_14F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_29F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10110_O" deadCode="false" sourceNode="P_10F10110" targetNode="P_15F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_29F10110"/>
	</edges>
	<edges id="P_10F10110P_11F10110" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10110" targetNode="P_11F10110"/>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10110_I" deadCode="false" sourceNode="P_12F10110" targetNode="P_16F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_41F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10110_O" deadCode="false" sourceNode="P_12F10110" targetNode="P_17F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_41F10110"/>
	</edges>
	<edges id="P_12F10110P_13F10110" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10110" targetNode="P_13F10110"/>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10110_I" deadCode="false" sourceNode="P_16F10110" targetNode="P_18F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_48F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10110_O" deadCode="false" sourceNode="P_16F10110" targetNode="P_19F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_48F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10110_I" deadCode="false" sourceNode="P_16F10110" targetNode="P_20F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_52F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10110_O" deadCode="false" sourceNode="P_16F10110" targetNode="P_21F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_52F10110"/>
	</edges>
	<edges id="P_16F10110P_17F10110" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10110" targetNode="P_17F10110"/>
	<edges id="P_20F10110P_21F10110" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10110" targetNode="P_21F10110"/>
	<edges id="P_18F10110P_19F10110" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10110" targetNode="P_19F10110"/>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10110_I" deadCode="false" sourceNode="P_14F10110" targetNode="P_22F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_98F10110"/>
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_100F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10110_O" deadCode="false" sourceNode="P_14F10110" targetNode="P_23F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_98F10110"/>
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_100F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10110_I" deadCode="false" sourceNode="P_14F10110" targetNode="P_24F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_101F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10110_O" deadCode="false" sourceNode="P_14F10110" targetNode="P_25F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_101F10110"/>
	</edges>
	<edges id="P_14F10110P_15F10110" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10110" targetNode="P_15F10110"/>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10110_I" deadCode="false" sourceNode="P_22F10110" targetNode="P_16F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_106F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10110_O" deadCode="false" sourceNode="P_22F10110" targetNode="P_17F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_106F10110"/>
	</edges>
	<edges id="P_22F10110P_23F10110" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10110" targetNode="P_23F10110"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10110_I" deadCode="false" sourceNode="P_24F10110" targetNode="P_22F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_110F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10110_O" deadCode="false" sourceNode="P_24F10110" targetNode="P_23F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_110F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10110_I" deadCode="false" sourceNode="P_24F10110" targetNode="P_22F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_112F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10110_O" deadCode="false" sourceNode="P_24F10110" targetNode="P_23F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_112F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10110_I" deadCode="false" sourceNode="P_24F10110" targetNode="P_22F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_114F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10110_O" deadCode="false" sourceNode="P_24F10110" targetNode="P_23F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_114F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10110_I" deadCode="false" sourceNode="P_24F10110" targetNode="P_22F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_117F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10110_O" deadCode="false" sourceNode="P_24F10110" targetNode="P_23F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_117F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10110_I" deadCode="false" sourceNode="P_24F10110" targetNode="P_26F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_118F10110"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10110_O" deadCode="false" sourceNode="P_24F10110" targetNode="P_27F10110">
		<representations href="../../../cobol/ISPS0040.cbl.cobModel#S_118F10110"/>
	</edges>
	<edges id="P_24F10110P_25F10110" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10110" targetNode="P_25F10110"/>
	<edges id="P_26F10110P_27F10110" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10110" targetNode="P_27F10110"/>
	<edges xsi:type="cbl:CallEdge" id="S_21F10110" deadCode="false" name="Dynamic INTERF-MQSERIES" sourceNode="P_10F10110" targetNode="IJCSMQ01">
		<representations href="../../../cobol/../importantStmts.cobModel#S_21F10110"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_46F10110" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_16F10110" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_46F10110"></representations>
	</edges>
</Package>

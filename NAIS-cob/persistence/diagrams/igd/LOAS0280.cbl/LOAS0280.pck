<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LOAS0280" cbl:id="LOAS0280" xsi:id="LOAS0280" packageRef="LOAS0280.igd#LOAS0280" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LOAS0280_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10287" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LOAS0280.cbl.cobModel#SC_1F10287"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10287" deadCode="false" name="PROGRAM_LOAS0280_FIRST_SENTENCES">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_1F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10287" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_2F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10287" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_3F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10287" deadCode="false" name="S0050-CONTROLLI">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_8F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10287" deadCode="false" name="EX-S0050">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_9F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10287" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_4F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10287" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_5F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10287" deadCode="false" name="S4000-JOIN-STW-ODE-POL">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_14F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10287" deadCode="false" name="EX-S4000">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_15F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10287" deadCode="false" name="S4110-PREPARA-AREA-LDBS3360">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_16F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10287" deadCode="false" name="EX-S4110">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_17F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10287" deadCode="false" name="S4120-CALL-LDBS3360">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_18F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10287" deadCode="false" name="EX-S4120">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_19F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10287" deadCode="false" name="S5000-JOIN-STW-ODE-ADE">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_12F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10287" deadCode="false" name="EX-S5000">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_13F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10287" deadCode="false" name="S6000-SET-MESSAGGIO">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_20F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10287" deadCode="false" name="EX-S6000">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_21F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10287" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_6F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10287" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_7F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10287" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_10F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10287" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_11F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10287" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_26F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10287" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_27F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10287" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_24F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10287" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_25F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10287" deadCode="true" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_28F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10287" deadCode="true" name="EX-S0290">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_29F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10287" deadCode="true" name="S0320-OUTPUT-PRODOTTO">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_30F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10287" deadCode="true" name="EX-S0320-OUTPUT-PRODOTTO">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_35F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10287" deadCode="true" name="S0322-SALVA-ERRORE">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_31F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10287" deadCode="true" name="EX-S0322">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_32F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10287" deadCode="true" name="S0321-ERRORI-DEROGA">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_33F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10287" deadCode="true" name="EX-S0321">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_34F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10287" deadCode="true" name="S0323-SALVA-DEROGA">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_36F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10287" deadCode="true" name="EX-S0323">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_37F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10287" deadCode="false" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_22F10287"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10287" deadCode="false" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LOAS0280.cbl.cobModel#P_23F10287"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0010" name="IDSS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10099"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10287P_1F10287" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10287" targetNode="P_1F10287"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10287_I" deadCode="false" sourceNode="P_1F10287" targetNode="P_2F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_1F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10287_O" deadCode="false" sourceNode="P_1F10287" targetNode="P_3F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_1F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10287_I" deadCode="false" sourceNode="P_1F10287" targetNode="P_4F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_3F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10287_O" deadCode="false" sourceNode="P_1F10287" targetNode="P_5F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_3F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10287_I" deadCode="false" sourceNode="P_1F10287" targetNode="P_6F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_4F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10287_O" deadCode="false" sourceNode="P_1F10287" targetNode="P_7F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_4F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10287_I" deadCode="false" sourceNode="P_2F10287" targetNode="P_8F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_9F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10287_O" deadCode="false" sourceNode="P_2F10287" targetNode="P_9F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_9F10287"/>
	</edges>
	<edges id="P_2F10287P_3F10287" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10287" targetNode="P_3F10287"/>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10287_I" deadCode="false" sourceNode="P_8F10287" targetNode="P_10F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_18F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10287_O" deadCode="false" sourceNode="P_8F10287" targetNode="P_11F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_18F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10287_I" deadCode="false" sourceNode="P_8F10287" targetNode="P_10F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_25F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10287_O" deadCode="false" sourceNode="P_8F10287" targetNode="P_11F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_25F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10287_I" deadCode="false" sourceNode="P_8F10287" targetNode="P_10F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_31F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10287_O" deadCode="false" sourceNode="P_8F10287" targetNode="P_11F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_31F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10287_I" deadCode="false" sourceNode="P_8F10287" targetNode="P_10F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_39F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10287_O" deadCode="false" sourceNode="P_8F10287" targetNode="P_11F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_39F10287"/>
	</edges>
	<edges id="P_8F10287P_9F10287" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10287" targetNode="P_9F10287"/>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10287_I" deadCode="false" sourceNode="P_4F10287" targetNode="P_12F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_42F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10287_O" deadCode="false" sourceNode="P_4F10287" targetNode="P_13F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_42F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10287_I" deadCode="false" sourceNode="P_4F10287" targetNode="P_14F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_44F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10287_O" deadCode="false" sourceNode="P_4F10287" targetNode="P_15F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_44F10287"/>
	</edges>
	<edges id="P_4F10287P_5F10287" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10287" targetNode="P_5F10287"/>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10287_I" deadCode="false" sourceNode="P_14F10287" targetNode="P_16F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_50F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10287_O" deadCode="false" sourceNode="P_14F10287" targetNode="P_17F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_50F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10287_I" deadCode="false" sourceNode="P_14F10287" targetNode="P_18F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_52F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10287_O" deadCode="false" sourceNode="P_14F10287" targetNode="P_19F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_52F10287"/>
	</edges>
	<edges id="P_14F10287P_15F10287" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10287" targetNode="P_15F10287"/>
	<edges id="P_16F10287P_17F10287" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10287" targetNode="P_17F10287"/>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10287_I" deadCode="false" sourceNode="P_18F10287" targetNode="P_20F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_64F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10287_O" deadCode="false" sourceNode="P_18F10287" targetNode="P_21F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_64F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10287_I" deadCode="false" sourceNode="P_18F10287" targetNode="P_22F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_65F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10287_O" deadCode="false" sourceNode="P_18F10287" targetNode="P_23F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_65F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10287_I" deadCode="false" sourceNode="P_18F10287" targetNode="P_10F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_78F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10287_O" deadCode="false" sourceNode="P_18F10287" targetNode="P_11F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_78F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10287_I" deadCode="false" sourceNode="P_18F10287" targetNode="P_10F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_83F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10287_O" deadCode="false" sourceNode="P_18F10287" targetNode="P_11F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_83F10287"/>
	</edges>
	<edges id="P_18F10287P_19F10287" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10287" targetNode="P_19F10287"/>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10287_I" deadCode="false" sourceNode="P_12F10287" targetNode="P_16F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_88F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10287_O" deadCode="false" sourceNode="P_12F10287" targetNode="P_17F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_88F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10287_I" deadCode="false" sourceNode="P_12F10287" targetNode="P_18F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_90F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10287_O" deadCode="false" sourceNode="P_12F10287" targetNode="P_19F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_90F10287"/>
	</edges>
	<edges id="P_12F10287P_13F10287" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10287" targetNode="P_13F10287"/>
	<edges id="P_20F10287P_21F10287" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10287" targetNode="P_21F10287"/>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10287_I" deadCode="false" sourceNode="P_10F10287" targetNode="P_24F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_103F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10287_O" deadCode="false" sourceNode="P_10F10287" targetNode="P_25F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_103F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10287_I" deadCode="false" sourceNode="P_10F10287" targetNode="P_26F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_107F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10287_O" deadCode="false" sourceNode="P_10F10287" targetNode="P_27F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_107F10287"/>
	</edges>
	<edges id="P_10F10287P_11F10287" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10287" targetNode="P_11F10287"/>
	<edges id="P_26F10287P_27F10287" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10287" targetNode="P_27F10287"/>
	<edges id="P_24F10287P_25F10287" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10287" targetNode="P_25F10287"/>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10287_I" deadCode="true" sourceNode="P_28F10287" targetNode="P_10F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_143F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10287_O" deadCode="true" sourceNode="P_28F10287" targetNode="P_11F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_143F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_159F10287_I" deadCode="true" sourceNode="P_30F10287" targetNode="P_31F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_157F10287"/>
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_159F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_159F10287_O" deadCode="true" sourceNode="P_30F10287" targetNode="P_32F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_157F10287"/>
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_159F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10287_I" deadCode="true" sourceNode="P_30F10287" targetNode="P_33F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_160F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10287_O" deadCode="true" sourceNode="P_30F10287" targetNode="P_34F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_160F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10287_I" deadCode="true" sourceNode="P_31F10287" targetNode="P_10F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_165F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10287_O" deadCode="true" sourceNode="P_31F10287" targetNode="P_11F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_165F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_169F10287_I" deadCode="true" sourceNode="P_33F10287" targetNode="P_31F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_169F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_169F10287_O" deadCode="true" sourceNode="P_33F10287" targetNode="P_32F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_169F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10287_I" deadCode="true" sourceNode="P_33F10287" targetNode="P_31F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_171F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10287_O" deadCode="true" sourceNode="P_33F10287" targetNode="P_32F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_171F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10287_I" deadCode="true" sourceNode="P_33F10287" targetNode="P_31F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_173F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10287_O" deadCode="true" sourceNode="P_33F10287" targetNode="P_32F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_173F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_176F10287_I" deadCode="true" sourceNode="P_33F10287" targetNode="P_31F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_176F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_176F10287_O" deadCode="true" sourceNode="P_33F10287" targetNode="P_32F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_176F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10287_I" deadCode="true" sourceNode="P_33F10287" targetNode="P_36F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_177F10287"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10287_O" deadCode="true" sourceNode="P_33F10287" targetNode="P_37F10287">
		<representations href="../../../cobol/LOAS0280.cbl.cobModel#S_177F10287"/>
	</edges>
	<edges id="P_22F10287P_23F10287" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10287" targetNode="P_23F10287"/>
	<edges xsi:type="cbl:CallEdge" id="S_101F10287" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_10F10287" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_101F10287"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_220F10287" deadCode="false" name="Dynamic IDSI0011-PGM" sourceNode="P_22F10287" targetNode="IDSS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_220F10287"></representations>
	</edges>
</Package>

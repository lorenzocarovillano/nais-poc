<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS6190" cbl:id="LDBS6190" xsi:id="LDBS6190" packageRef="LDBS6190.igd#LDBS6190" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS6190_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10233" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS6190.cbl.cobModel#SC_1F10233"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10233" deadCode="false" name="PROGRAM_LDBS6190_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_1F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10233" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_2F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10233" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_3F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10233" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_12F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10233" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_13F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10233" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_4F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10233" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_5F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10233" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_6F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10233" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_7F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10233" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_8F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10233" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_9F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10233" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_44F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10233" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_49F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10233" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_14F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10233" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_15F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10233" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_16F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10233" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_17F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10233" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_18F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10233" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_19F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10233" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_20F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10233" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_21F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10233" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_22F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10233" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_23F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10233" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_56F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10233" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_57F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10233" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_24F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10233" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_25F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10233" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_26F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10233" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_27F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10233" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_28F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10233" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_29F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10233" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_30F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10233" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_31F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10233" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_32F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10233" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_33F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10233" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_58F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10233" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_59F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10233" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_34F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10233" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_35F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10233" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_36F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10233" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_37F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10233" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_38F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10233" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_39F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10233" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_40F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10233" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_41F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10233" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_42F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10233" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_43F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10233" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_50F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10233" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_51F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10233" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_60F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10233" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_63F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10233" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_52F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10233" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_53F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10233" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_45F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10233" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_46F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10233" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_47F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10233" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_48F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10233" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_54F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10233" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_55F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10233" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_10F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10233" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_11F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10233" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_66F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10233" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_67F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10233" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_68F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10233" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_69F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10233" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_61F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10233" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_62F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10233" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_70F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10233" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_71F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10233" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_64F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10233" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_65F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10233" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_72F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10233" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_73F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10233" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_74F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10233" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_75F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10233" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_76F10233"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10233" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS6190.cbl.cobModel#P_77F10233"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_IMPST_SOST" name="IMPST_SOST">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_IMPST_SOST"/>
		</children>
	</packageNode>
	<edges id="SC_1F10233P_1F10233" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10233" targetNode="P_1F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10233_I" deadCode="false" sourceNode="P_1F10233" targetNode="P_2F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_2F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10233_O" deadCode="false" sourceNode="P_1F10233" targetNode="P_3F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_2F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10233_I" deadCode="false" sourceNode="P_1F10233" targetNode="P_4F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_6F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10233_O" deadCode="false" sourceNode="P_1F10233" targetNode="P_5F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_6F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10233_I" deadCode="false" sourceNode="P_1F10233" targetNode="P_6F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_10F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10233_O" deadCode="false" sourceNode="P_1F10233" targetNode="P_7F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_10F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10233_I" deadCode="false" sourceNode="P_1F10233" targetNode="P_8F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_14F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10233_O" deadCode="false" sourceNode="P_1F10233" targetNode="P_9F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_14F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10233_I" deadCode="false" sourceNode="P_2F10233" targetNode="P_10F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_24F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10233_O" deadCode="false" sourceNode="P_2F10233" targetNode="P_11F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_24F10233"/>
	</edges>
	<edges id="P_2F10233P_3F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10233" targetNode="P_3F10233"/>
	<edges id="P_12F10233P_13F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10233" targetNode="P_13F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10233_I" deadCode="false" sourceNode="P_4F10233" targetNode="P_14F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_37F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10233_O" deadCode="false" sourceNode="P_4F10233" targetNode="P_15F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_37F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10233_I" deadCode="false" sourceNode="P_4F10233" targetNode="P_16F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_38F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10233_O" deadCode="false" sourceNode="P_4F10233" targetNode="P_17F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_38F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10233_I" deadCode="false" sourceNode="P_4F10233" targetNode="P_18F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_39F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10233_O" deadCode="false" sourceNode="P_4F10233" targetNode="P_19F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_39F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10233_I" deadCode="false" sourceNode="P_4F10233" targetNode="P_20F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_40F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10233_O" deadCode="false" sourceNode="P_4F10233" targetNode="P_21F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_40F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10233_I" deadCode="false" sourceNode="P_4F10233" targetNode="P_22F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_41F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10233_O" deadCode="false" sourceNode="P_4F10233" targetNode="P_23F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_41F10233"/>
	</edges>
	<edges id="P_4F10233P_5F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10233" targetNode="P_5F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10233_I" deadCode="false" sourceNode="P_6F10233" targetNode="P_24F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_45F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10233_O" deadCode="false" sourceNode="P_6F10233" targetNode="P_25F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_45F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10233_I" deadCode="false" sourceNode="P_6F10233" targetNode="P_26F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_46F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10233_O" deadCode="false" sourceNode="P_6F10233" targetNode="P_27F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_46F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10233_I" deadCode="false" sourceNode="P_6F10233" targetNode="P_28F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_47F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10233_O" deadCode="false" sourceNode="P_6F10233" targetNode="P_29F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_47F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10233_I" deadCode="false" sourceNode="P_6F10233" targetNode="P_30F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_48F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10233_O" deadCode="false" sourceNode="P_6F10233" targetNode="P_31F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_48F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10233_I" deadCode="false" sourceNode="P_6F10233" targetNode="P_32F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_49F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10233_O" deadCode="false" sourceNode="P_6F10233" targetNode="P_33F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_49F10233"/>
	</edges>
	<edges id="P_6F10233P_7F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10233" targetNode="P_7F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10233_I" deadCode="false" sourceNode="P_8F10233" targetNode="P_34F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_53F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10233_O" deadCode="false" sourceNode="P_8F10233" targetNode="P_35F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_53F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10233_I" deadCode="false" sourceNode="P_8F10233" targetNode="P_36F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_54F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10233_O" deadCode="false" sourceNode="P_8F10233" targetNode="P_37F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_54F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10233_I" deadCode="false" sourceNode="P_8F10233" targetNode="P_38F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_55F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10233_O" deadCode="false" sourceNode="P_8F10233" targetNode="P_39F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_55F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10233_I" deadCode="false" sourceNode="P_8F10233" targetNode="P_40F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_56F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10233_O" deadCode="false" sourceNode="P_8F10233" targetNode="P_41F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_56F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10233_I" deadCode="false" sourceNode="P_8F10233" targetNode="P_42F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_57F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10233_O" deadCode="false" sourceNode="P_8F10233" targetNode="P_43F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_57F10233"/>
	</edges>
	<edges id="P_8F10233P_9F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10233" targetNode="P_9F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10233_I" deadCode="false" sourceNode="P_44F10233" targetNode="P_45F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_60F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10233_O" deadCode="false" sourceNode="P_44F10233" targetNode="P_46F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_60F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10233_I" deadCode="false" sourceNode="P_44F10233" targetNode="P_47F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_61F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10233_O" deadCode="false" sourceNode="P_44F10233" targetNode="P_48F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_61F10233"/>
	</edges>
	<edges id="P_44F10233P_49F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10233" targetNode="P_49F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10233_I" deadCode="false" sourceNode="P_14F10233" targetNode="P_45F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_64F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10233_O" deadCode="false" sourceNode="P_14F10233" targetNode="P_46F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_64F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10233_I" deadCode="false" sourceNode="P_14F10233" targetNode="P_47F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_65F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10233_O" deadCode="false" sourceNode="P_14F10233" targetNode="P_48F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_65F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10233_I" deadCode="false" sourceNode="P_14F10233" targetNode="P_12F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_67F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10233_O" deadCode="false" sourceNode="P_14F10233" targetNode="P_13F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_67F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10233_I" deadCode="false" sourceNode="P_14F10233" targetNode="P_50F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_69F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10233_O" deadCode="false" sourceNode="P_14F10233" targetNode="P_51F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_69F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10233_I" deadCode="false" sourceNode="P_14F10233" targetNode="P_52F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_70F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10233_O" deadCode="false" sourceNode="P_14F10233" targetNode="P_53F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_70F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10233_I" deadCode="false" sourceNode="P_14F10233" targetNode="P_54F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_71F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10233_O" deadCode="false" sourceNode="P_14F10233" targetNode="P_55F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_71F10233"/>
	</edges>
	<edges id="P_14F10233P_15F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10233" targetNode="P_15F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10233_I" deadCode="false" sourceNode="P_16F10233" targetNode="P_44F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_73F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10233_O" deadCode="false" sourceNode="P_16F10233" targetNode="P_49F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_73F10233"/>
	</edges>
	<edges id="P_16F10233P_17F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10233" targetNode="P_17F10233"/>
	<edges id="P_18F10233P_19F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10233" targetNode="P_19F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10233_I" deadCode="false" sourceNode="P_20F10233" targetNode="P_16F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_78F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10233_O" deadCode="false" sourceNode="P_20F10233" targetNode="P_17F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_78F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10233_I" deadCode="false" sourceNode="P_20F10233" targetNode="P_22F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_80F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10233_O" deadCode="false" sourceNode="P_20F10233" targetNode="P_23F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_80F10233"/>
	</edges>
	<edges id="P_20F10233P_21F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10233" targetNode="P_21F10233"/>
	<edges id="P_22F10233P_23F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10233" targetNode="P_23F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10233_I" deadCode="false" sourceNode="P_56F10233" targetNode="P_45F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_84F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10233_O" deadCode="false" sourceNode="P_56F10233" targetNode="P_46F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_84F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10233_I" deadCode="false" sourceNode="P_56F10233" targetNode="P_47F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_85F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10233_O" deadCode="false" sourceNode="P_56F10233" targetNode="P_48F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_85F10233"/>
	</edges>
	<edges id="P_56F10233P_57F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10233" targetNode="P_57F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10233_I" deadCode="false" sourceNode="P_24F10233" targetNode="P_45F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_88F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10233_O" deadCode="false" sourceNode="P_24F10233" targetNode="P_46F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_88F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10233_I" deadCode="false" sourceNode="P_24F10233" targetNode="P_47F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_89F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10233_O" deadCode="false" sourceNode="P_24F10233" targetNode="P_48F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_89F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10233_I" deadCode="false" sourceNode="P_24F10233" targetNode="P_12F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_91F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10233_O" deadCode="false" sourceNode="P_24F10233" targetNode="P_13F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_91F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10233_I" deadCode="false" sourceNode="P_24F10233" targetNode="P_50F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_93F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10233_O" deadCode="false" sourceNode="P_24F10233" targetNode="P_51F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_93F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10233_I" deadCode="false" sourceNode="P_24F10233" targetNode="P_52F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_94F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10233_O" deadCode="false" sourceNode="P_24F10233" targetNode="P_53F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_94F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10233_I" deadCode="false" sourceNode="P_24F10233" targetNode="P_54F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_95F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10233_O" deadCode="false" sourceNode="P_24F10233" targetNode="P_55F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_95F10233"/>
	</edges>
	<edges id="P_24F10233P_25F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10233" targetNode="P_25F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10233_I" deadCode="false" sourceNode="P_26F10233" targetNode="P_56F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_97F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10233_O" deadCode="false" sourceNode="P_26F10233" targetNode="P_57F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_97F10233"/>
	</edges>
	<edges id="P_26F10233P_27F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10233" targetNode="P_27F10233"/>
	<edges id="P_28F10233P_29F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10233" targetNode="P_29F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10233_I" deadCode="false" sourceNode="P_30F10233" targetNode="P_26F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_102F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10233_O" deadCode="false" sourceNode="P_30F10233" targetNode="P_27F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_102F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10233_I" deadCode="false" sourceNode="P_30F10233" targetNode="P_32F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_104F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10233_O" deadCode="false" sourceNode="P_30F10233" targetNode="P_33F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_104F10233"/>
	</edges>
	<edges id="P_30F10233P_31F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10233" targetNode="P_31F10233"/>
	<edges id="P_32F10233P_33F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10233" targetNode="P_33F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10233_I" deadCode="false" sourceNode="P_58F10233" targetNode="P_45F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_108F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10233_O" deadCode="false" sourceNode="P_58F10233" targetNode="P_46F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_108F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10233_I" deadCode="false" sourceNode="P_58F10233" targetNode="P_47F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_109F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10233_O" deadCode="false" sourceNode="P_58F10233" targetNode="P_48F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_109F10233"/>
	</edges>
	<edges id="P_58F10233P_59F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10233" targetNode="P_59F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10233_I" deadCode="false" sourceNode="P_34F10233" targetNode="P_45F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_112F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10233_O" deadCode="false" sourceNode="P_34F10233" targetNode="P_46F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_112F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10233_I" deadCode="false" sourceNode="P_34F10233" targetNode="P_47F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_113F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10233_O" deadCode="false" sourceNode="P_34F10233" targetNode="P_48F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_113F10233"/>
	</edges>
	<edges id="P_34F10233P_35F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10233" targetNode="P_35F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10233_I" deadCode="false" sourceNode="P_36F10233" targetNode="P_58F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_116F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10233_O" deadCode="false" sourceNode="P_36F10233" targetNode="P_59F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_116F10233"/>
	</edges>
	<edges id="P_36F10233P_37F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10233" targetNode="P_37F10233"/>
	<edges id="P_38F10233P_39F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10233" targetNode="P_39F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10233_I" deadCode="false" sourceNode="P_40F10233" targetNode="P_36F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_121F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10233_O" deadCode="false" sourceNode="P_40F10233" targetNode="P_37F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_121F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10233_I" deadCode="false" sourceNode="P_40F10233" targetNode="P_42F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_123F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10233_O" deadCode="false" sourceNode="P_40F10233" targetNode="P_43F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_123F10233"/>
	</edges>
	<edges id="P_40F10233P_41F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10233" targetNode="P_41F10233"/>
	<edges id="P_42F10233P_43F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10233" targetNode="P_43F10233"/>
	<edges id="P_50F10233P_51F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10233" targetNode="P_51F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10233_I" deadCode="true" sourceNode="P_60F10233" targetNode="P_61F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_160F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10233_O" deadCode="true" sourceNode="P_60F10233" targetNode="P_62F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_160F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10233_I" deadCode="true" sourceNode="P_60F10233" targetNode="P_61F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_163F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10233_O" deadCode="true" sourceNode="P_60F10233" targetNode="P_62F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_163F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10233_I" deadCode="true" sourceNode="P_60F10233" targetNode="P_61F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_167F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10233_O" deadCode="true" sourceNode="P_60F10233" targetNode="P_62F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_167F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10233_I" deadCode="true" sourceNode="P_60F10233" targetNode="P_61F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_171F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10233_O" deadCode="true" sourceNode="P_60F10233" targetNode="P_62F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_171F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10233_I" deadCode="false" sourceNode="P_52F10233" targetNode="P_64F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_175F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10233_O" deadCode="false" sourceNode="P_52F10233" targetNode="P_65F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_175F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_178F10233_I" deadCode="false" sourceNode="P_52F10233" targetNode="P_64F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_178F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_178F10233_O" deadCode="false" sourceNode="P_52F10233" targetNode="P_65F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_178F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_182F10233_I" deadCode="false" sourceNode="P_52F10233" targetNode="P_64F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_182F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_182F10233_O" deadCode="false" sourceNode="P_52F10233" targetNode="P_65F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_182F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10233_I" deadCode="false" sourceNode="P_52F10233" targetNode="P_64F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_186F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10233_O" deadCode="false" sourceNode="P_52F10233" targetNode="P_65F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_186F10233"/>
	</edges>
	<edges id="P_52F10233P_53F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10233" targetNode="P_53F10233"/>
	<edges id="P_45F10233P_46F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10233" targetNode="P_46F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_192F10233_I" deadCode="false" sourceNode="P_47F10233" targetNode="P_61F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_192F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_192F10233_O" deadCode="false" sourceNode="P_47F10233" targetNode="P_62F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_192F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10233_I" deadCode="false" sourceNode="P_47F10233" targetNode="P_61F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_195F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10233_O" deadCode="false" sourceNode="P_47F10233" targetNode="P_62F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_195F10233"/>
	</edges>
	<edges id="P_47F10233P_48F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10233" targetNode="P_48F10233"/>
	<edges id="P_54F10233P_55F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10233" targetNode="P_55F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_200F10233_I" deadCode="false" sourceNode="P_10F10233" targetNode="P_66F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_200F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_200F10233_O" deadCode="false" sourceNode="P_10F10233" targetNode="P_67F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_200F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10233_I" deadCode="false" sourceNode="P_10F10233" targetNode="P_68F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_202F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10233_O" deadCode="false" sourceNode="P_10F10233" targetNode="P_69F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_202F10233"/>
	</edges>
	<edges id="P_10F10233P_11F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10233" targetNode="P_11F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_207F10233_I" deadCode="false" sourceNode="P_66F10233" targetNode="P_61F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_207F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_207F10233_O" deadCode="false" sourceNode="P_66F10233" targetNode="P_62F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_207F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_212F10233_I" deadCode="false" sourceNode="P_66F10233" targetNode="P_61F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_212F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_212F10233_O" deadCode="false" sourceNode="P_66F10233" targetNode="P_62F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_212F10233"/>
	</edges>
	<edges id="P_66F10233P_67F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10233" targetNode="P_67F10233"/>
	<edges id="P_68F10233P_69F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10233" targetNode="P_69F10233"/>
	<edges id="P_61F10233P_62F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10233" targetNode="P_62F10233"/>
	<edges xsi:type="cbl:PerformEdge" id="S_241F10233_I" deadCode="false" sourceNode="P_64F10233" targetNode="P_72F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_241F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_241F10233_O" deadCode="false" sourceNode="P_64F10233" targetNode="P_73F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_241F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10233_I" deadCode="false" sourceNode="P_64F10233" targetNode="P_74F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_242F10233"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10233_O" deadCode="false" sourceNode="P_64F10233" targetNode="P_75F10233">
		<representations href="../../../cobol/LDBS6190.cbl.cobModel#S_242F10233"/>
	</edges>
	<edges id="P_64F10233P_65F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10233" targetNode="P_65F10233"/>
	<edges id="P_72F10233P_73F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10233" targetNode="P_73F10233"/>
	<edges id="P_74F10233P_75F10233" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10233" targetNode="P_75F10233"/>
	<edges xsi:type="cbl:DataEdge" id="S_66F10233_POS1" deadCode="false" targetNode="P_14F10233" sourceNode="DB2_IMPST_SOST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_66F10233"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_90F10233_POS1" deadCode="false" targetNode="P_24F10233" sourceNode="DB2_IMPST_SOST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_90F10233"></representations>
	</edges>
</Package>

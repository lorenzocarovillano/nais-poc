<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0740" cbl:id="LVVS0740" xsi:id="LVVS0740" packageRef="LVVS0740.igd#LVVS0740" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0740_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10362" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0740.cbl.cobModel#SC_1F10362"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10362" deadCode="false" name="PROGRAM_LVVS0740_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_1F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10362" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_2F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10362" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_3F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10362" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_4F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10362" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_5F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10362" deadCode="false" name="S1110-LEGGI-PRESTITO">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_10F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10362" deadCode="false" name="S1110-EX">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_11F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10362" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_8F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10362" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_9F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10362" deadCode="false" name="S1255-LEGGI-TIT">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_12F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10362" deadCode="false" name="S1255-EX">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_13F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10362" deadCode="false" name="S1260-VALORIZZA">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_14F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10362" deadCode="false" name="S1260-EX">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_15F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10362" deadCode="false" name="VALORIZZA-OUTPUT-TIT">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_16F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10362" deadCode="false" name="VALORIZZA-OUTPUT-TIT-EX">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_17F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10362" deadCode="false" name="INIZIA-TOT-TIT">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_6F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10362" deadCode="false" name="INIZIA-TOT-TIT-EX">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_7F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10362" deadCode="false" name="INIZIA-NULL-TIT">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_22F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10362" deadCode="false" name="INIZIA-NULL-TIT-EX">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_23F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10362" deadCode="true" name="INIZIA-ZEROES-TIT">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_18F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10362" deadCode="false" name="INIZIA-ZEROES-TIT-EX">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_19F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10362" deadCode="true" name="INIZIA-SPACES-TIT">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_20F10362"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10362" deadCode="false" name="INIZIA-SPACES-TIT-EX">
				<representations href="../../../cobol/LVVS0740.cbl.cobModel#P_21F10362"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS6160" name="LDBS6160">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10232"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS8570" name="LDBS8570">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10242"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10362P_1F10362" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10362" targetNode="P_1F10362"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10362_I" deadCode="false" sourceNode="P_1F10362" targetNode="P_2F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_1F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10362_O" deadCode="false" sourceNode="P_1F10362" targetNode="P_3F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_1F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10362_I" deadCode="false" sourceNode="P_1F10362" targetNode="P_4F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_2F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10362_O" deadCode="false" sourceNode="P_1F10362" targetNode="P_5F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_2F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10362_I" deadCode="false" sourceNode="P_2F10362" targetNode="P_6F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_8F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10362_O" deadCode="false" sourceNode="P_2F10362" targetNode="P_7F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_8F10362"/>
	</edges>
	<edges id="P_2F10362P_3F10362" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10362" targetNode="P_3F10362"/>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10362_I" deadCode="false" sourceNode="P_4F10362" targetNode="P_8F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_12F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10362_O" deadCode="false" sourceNode="P_4F10362" targetNode="P_9F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_12F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10362_I" deadCode="false" sourceNode="P_4F10362" targetNode="P_10F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_14F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10362_O" deadCode="false" sourceNode="P_4F10362" targetNode="P_11F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_14F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10362_I" deadCode="false" sourceNode="P_4F10362" targetNode="P_12F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_18F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10362_O" deadCode="false" sourceNode="P_4F10362" targetNode="P_13F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_18F10362"/>
	</edges>
	<edges id="P_4F10362P_5F10362" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10362" targetNode="P_5F10362"/>
	<edges id="P_10F10362P_11F10362" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10362" targetNode="P_11F10362"/>
	<edges id="P_8F10362P_9F10362" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10362" targetNode="P_9F10362"/>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10362_I" deadCode="false" sourceNode="P_12F10362" targetNode="P_14F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_68F10362"/>
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_76F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10362_O" deadCode="false" sourceNode="P_12F10362" targetNode="P_15F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_68F10362"/>
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_76F10362"/>
	</edges>
	<edges id="P_12F10362P_13F10362" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10362" targetNode="P_13F10362"/>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10362_I" deadCode="false" sourceNode="P_14F10362" targetNode="P_16F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_93F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10362_O" deadCode="false" sourceNode="P_14F10362" targetNode="P_17F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_93F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10362_I" deadCode="false" sourceNode="P_14F10362" targetNode="P_16F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_95F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10362_O" deadCode="false" sourceNode="P_14F10362" targetNode="P_17F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_95F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10362_I" deadCode="false" sourceNode="P_14F10362" targetNode="P_16F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_102F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10362_O" deadCode="false" sourceNode="P_14F10362" targetNode="P_17F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_102F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10362_I" deadCode="false" sourceNode="P_14F10362" targetNode="P_16F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_104F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10362_O" deadCode="false" sourceNode="P_14F10362" targetNode="P_17F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_104F10362"/>
	</edges>
	<edges id="P_14F10362P_15F10362" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10362" targetNode="P_15F10362"/>
	<edges id="P_16F10362P_17F10362" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10362" targetNode="P_17F10362"/>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10362_I" deadCode="true" sourceNode="P_6F10362" targetNode="P_18F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_338F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10362_O" deadCode="true" sourceNode="P_6F10362" targetNode="P_19F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_338F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_339F10362_I" deadCode="true" sourceNode="P_6F10362" targetNode="P_20F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_339F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_339F10362_O" deadCode="true" sourceNode="P_6F10362" targetNode="P_21F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_339F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_340F10362_I" deadCode="false" sourceNode="P_6F10362" targetNode="P_22F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_340F10362"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_340F10362_O" deadCode="false" sourceNode="P_6F10362" targetNode="P_23F10362">
		<representations href="../../../cobol/LVVS0740.cbl.cobModel#S_340F10362"/>
	</edges>
	<edges id="P_6F10362P_7F10362" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10362" targetNode="P_7F10362"/>
	<edges id="P_22F10362P_23F10362" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10362" targetNode="P_23F10362"/>
	<edges id="P_18F10362P_19F10362" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10362" targetNode="P_19F10362"/>
	<edges id="P_20F10362P_21F10362" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10362" targetNode="P_21F10362"/>
	<edges xsi:type="cbl:CallEdge" id="S_38F10362" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_10F10362" targetNode="LDBS6160">
		<representations href="../../../cobol/../importantStmts.cobModel#S_38F10362"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_69F10362" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_12F10362" targetNode="LDBS8570">
		<representations href="../../../cobol/../importantStmts.cobModel#S_69F10362"></representations>
	</edges>
</Package>

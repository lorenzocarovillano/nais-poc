<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0039" cbl:id="LVVS0039" xsi:id="LVVS0039" packageRef="LVVS0039.igd#LVVS0039" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0039_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10326" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0039.cbl.cobModel#SC_1F10326"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10326" deadCode="false" name="PROGRAM_LVVS0039_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_1F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10326" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_2F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10326" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_3F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10326" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_4F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10326" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_5F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10326" deadCode="false" name="S1240-CONTROLLA-GAR">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_12F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10326" deadCode="false" name="EX-S1250">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_13F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10326" deadCode="false" name="S1260-CARICA-AREA-GAR">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_16F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10326" deadCode="false" name="EX-S1260">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_17F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10326" deadCode="true" name="S1200-CARICA-DCLGEN">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_18F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10326" deadCode="true" name="S1200-CARICA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_19F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10326" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_8F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10326" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_9F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10326" deadCode="false" name="S1250-CALCOLA-DATA1">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_14F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10326" deadCode="false" name="S1250-EX">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_15F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10326" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_10F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10326" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_11F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10326" deadCode="false" name="Z000-CALCOLA">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_20F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10326" deadCode="false" name="Z000-EX">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_21F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10326" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_6F10326"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10326" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0039.cbl.cobModel#P_7F10326"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSF980" name="LDBSF980">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10273"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10326P_1F10326" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10326" targetNode="P_1F10326"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10326_I" deadCode="false" sourceNode="P_1F10326" targetNode="P_2F10326">
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_1F10326"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10326_O" deadCode="false" sourceNode="P_1F10326" targetNode="P_3F10326">
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_1F10326"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10326_I" deadCode="false" sourceNode="P_1F10326" targetNode="P_4F10326">
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_2F10326"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10326_O" deadCode="false" sourceNode="P_1F10326" targetNode="P_5F10326">
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_2F10326"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10326_I" deadCode="false" sourceNode="P_1F10326" targetNode="P_6F10326">
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_3F10326"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10326_O" deadCode="false" sourceNode="P_1F10326" targetNode="P_7F10326">
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_3F10326"/>
	</edges>
	<edges id="P_2F10326P_3F10326" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10326" targetNode="P_3F10326"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10326_I" deadCode="false" sourceNode="P_4F10326" targetNode="P_8F10326">
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_10F10326"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10326_O" deadCode="false" sourceNode="P_4F10326" targetNode="P_9F10326">
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_10F10326"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10326_I" deadCode="false" sourceNode="P_4F10326" targetNode="P_10F10326">
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_13F10326"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10326_O" deadCode="false" sourceNode="P_4F10326" targetNode="P_11F10326">
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_13F10326"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10326_I" deadCode="false" sourceNode="P_4F10326" targetNode="P_12F10326">
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_18F10326"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10326_O" deadCode="false" sourceNode="P_4F10326" targetNode="P_13F10326">
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_18F10326"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10326_I" deadCode="false" sourceNode="P_4F10326" targetNode="P_14F10326">
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_20F10326"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10326_O" deadCode="false" sourceNode="P_4F10326" targetNode="P_15F10326">
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_20F10326"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10326_I" deadCode="false" sourceNode="P_4F10326" targetNode="P_16F10326">
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_22F10326"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10326_O" deadCode="false" sourceNode="P_4F10326" targetNode="P_17F10326">
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_22F10326"/>
	</edges>
	<edges id="P_4F10326P_5F10326" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10326" targetNode="P_5F10326"/>
	<edges id="P_12F10326P_13F10326" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10326" targetNode="P_13F10326"/>
	<edges id="P_16F10326P_17F10326" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10326" targetNode="P_17F10326"/>
	<edges id="P_8F10326P_9F10326" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10326" targetNode="P_9F10326"/>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10326_I" deadCode="false" sourceNode="P_14F10326" targetNode="P_20F10326">
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_70F10326"/>
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_77F10326"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10326_O" deadCode="false" sourceNode="P_14F10326" targetNode="P_21F10326">
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_70F10326"/>
		<representations href="../../../cobol/LVVS0039.cbl.cobModel#S_77F10326"/>
	</edges>
	<edges id="P_14F10326P_15F10326" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10326" targetNode="P_15F10326"/>
	<edges id="P_10F10326P_11F10326" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10326" targetNode="P_11F10326"/>
	<edges id="P_20F10326P_21F10326" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10326" targetNode="P_21F10326"/>
	<edges xsi:type="cbl:CallEdge" id="S_71F10326" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_14F10326" targetNode="LDBSF980">
		<representations href="../../../cobol/../importantStmts.cobModel#S_71F10326"></representations>
	</edges>
</Package>

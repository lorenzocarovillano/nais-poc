<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS4510" cbl:id="LDBS4510" xsi:id="LDBS4510" packageRef="LDBS4510.igd#LDBS4510" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS4510_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10210" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS4510.cbl.cobModel#SC_1F10210"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10210" deadCode="false" name="PROGRAM_LDBS4510_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_1F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10210" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_2F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10210" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_3F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10210" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_12F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10210" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_13F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10210" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_4F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10210" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_5F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10210" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_6F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10210" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_7F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10210" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_8F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10210" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_9F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10210" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_44F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10210" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_49F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10210" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_14F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10210" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_15F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10210" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_16F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10210" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_17F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10210" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_18F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10210" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_19F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10210" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_20F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10210" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_21F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10210" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_22F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10210" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_23F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10210" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_50F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10210" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_51F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10210" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_24F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10210" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_25F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10210" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_26F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10210" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_27F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10210" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_28F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10210" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_29F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10210" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_30F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10210" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_31F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10210" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_32F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10210" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_33F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10210" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_52F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10210" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_53F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10210" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_34F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10210" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_35F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10210" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_36F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10210" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_37F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10210" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_38F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10210" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_39F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10210" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_40F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10210" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_41F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10210" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_42F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10210" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_43F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10210" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_54F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10210" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_55F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10210" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_60F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10210" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_63F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10210" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_56F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10210" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_57F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10210" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_45F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10210" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_46F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10210" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_47F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10210" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_48F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10210" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_58F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10210" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_59F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10210" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_10F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10210" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_11F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10210" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_66F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10210" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_67F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10210" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_68F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10210" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_69F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10210" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_61F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10210" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_62F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10210" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_70F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10210" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_71F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10210" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_64F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10210" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_65F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10210" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_72F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10210" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_73F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10210" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_74F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10210" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_75F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10210" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_76F10210"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10210" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS4510.cbl.cobModel#P_77F10210"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_RICH" name="RICH">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_RICH"/>
		</children>
	</packageNode>
	<edges id="SC_1F10210P_1F10210" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10210" targetNode="P_1F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10210_I" deadCode="false" sourceNode="P_1F10210" targetNode="P_2F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_2F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10210_O" deadCode="false" sourceNode="P_1F10210" targetNode="P_3F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_2F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10210_I" deadCode="false" sourceNode="P_1F10210" targetNode="P_4F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_6F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10210_O" deadCode="false" sourceNode="P_1F10210" targetNode="P_5F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_6F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10210_I" deadCode="false" sourceNode="P_1F10210" targetNode="P_6F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_10F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10210_O" deadCode="false" sourceNode="P_1F10210" targetNode="P_7F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_10F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10210_I" deadCode="false" sourceNode="P_1F10210" targetNode="P_8F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_14F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10210_O" deadCode="false" sourceNode="P_1F10210" targetNode="P_9F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_14F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10210_I" deadCode="false" sourceNode="P_2F10210" targetNode="P_10F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_23F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10210_O" deadCode="false" sourceNode="P_2F10210" targetNode="P_11F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_23F10210"/>
	</edges>
	<edges id="P_2F10210P_3F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10210" targetNode="P_3F10210"/>
	<edges id="P_12F10210P_13F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10210" targetNode="P_13F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10210_I" deadCode="false" sourceNode="P_4F10210" targetNode="P_14F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_36F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10210_O" deadCode="false" sourceNode="P_4F10210" targetNode="P_15F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_36F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10210_I" deadCode="false" sourceNode="P_4F10210" targetNode="P_16F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_37F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10210_O" deadCode="false" sourceNode="P_4F10210" targetNode="P_17F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_37F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10210_I" deadCode="false" sourceNode="P_4F10210" targetNode="P_18F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_38F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10210_O" deadCode="false" sourceNode="P_4F10210" targetNode="P_19F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_38F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10210_I" deadCode="false" sourceNode="P_4F10210" targetNode="P_20F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_39F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10210_O" deadCode="false" sourceNode="P_4F10210" targetNode="P_21F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_39F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10210_I" deadCode="false" sourceNode="P_4F10210" targetNode="P_22F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_40F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10210_O" deadCode="false" sourceNode="P_4F10210" targetNode="P_23F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_40F10210"/>
	</edges>
	<edges id="P_4F10210P_5F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10210" targetNode="P_5F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10210_I" deadCode="false" sourceNode="P_6F10210" targetNode="P_24F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_44F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10210_O" deadCode="false" sourceNode="P_6F10210" targetNode="P_25F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_44F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10210_I" deadCode="false" sourceNode="P_6F10210" targetNode="P_26F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_45F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10210_O" deadCode="false" sourceNode="P_6F10210" targetNode="P_27F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_45F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10210_I" deadCode="false" sourceNode="P_6F10210" targetNode="P_28F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_46F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10210_O" deadCode="false" sourceNode="P_6F10210" targetNode="P_29F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_46F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10210_I" deadCode="false" sourceNode="P_6F10210" targetNode="P_30F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_47F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10210_O" deadCode="false" sourceNode="P_6F10210" targetNode="P_31F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_47F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10210_I" deadCode="false" sourceNode="P_6F10210" targetNode="P_32F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_48F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10210_O" deadCode="false" sourceNode="P_6F10210" targetNode="P_33F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_48F10210"/>
	</edges>
	<edges id="P_6F10210P_7F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10210" targetNode="P_7F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10210_I" deadCode="false" sourceNode="P_8F10210" targetNode="P_34F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_52F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10210_O" deadCode="false" sourceNode="P_8F10210" targetNode="P_35F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_52F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10210_I" deadCode="false" sourceNode="P_8F10210" targetNode="P_36F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_53F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10210_O" deadCode="false" sourceNode="P_8F10210" targetNode="P_37F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_53F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10210_I" deadCode="false" sourceNode="P_8F10210" targetNode="P_38F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_54F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10210_O" deadCode="false" sourceNode="P_8F10210" targetNode="P_39F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_54F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10210_I" deadCode="false" sourceNode="P_8F10210" targetNode="P_40F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_55F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10210_O" deadCode="false" sourceNode="P_8F10210" targetNode="P_41F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_55F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10210_I" deadCode="false" sourceNode="P_8F10210" targetNode="P_42F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_56F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10210_O" deadCode="false" sourceNode="P_8F10210" targetNode="P_43F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_56F10210"/>
	</edges>
	<edges id="P_8F10210P_9F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10210" targetNode="P_9F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10210_I" deadCode="false" sourceNode="P_44F10210" targetNode="P_45F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_59F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10210_O" deadCode="false" sourceNode="P_44F10210" targetNode="P_46F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_59F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10210_I" deadCode="false" sourceNode="P_44F10210" targetNode="P_47F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_60F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10210_O" deadCode="false" sourceNode="P_44F10210" targetNode="P_48F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_60F10210"/>
	</edges>
	<edges id="P_44F10210P_49F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10210" targetNode="P_49F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10210_I" deadCode="false" sourceNode="P_14F10210" targetNode="P_45F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_63F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10210_O" deadCode="false" sourceNode="P_14F10210" targetNode="P_46F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_63F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10210_I" deadCode="false" sourceNode="P_14F10210" targetNode="P_47F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_64F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10210_O" deadCode="false" sourceNode="P_14F10210" targetNode="P_48F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_64F10210"/>
	</edges>
	<edges id="P_14F10210P_15F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10210" targetNode="P_15F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10210_I" deadCode="false" sourceNode="P_16F10210" targetNode="P_44F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_67F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10210_O" deadCode="false" sourceNode="P_16F10210" targetNode="P_49F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_67F10210"/>
	</edges>
	<edges id="P_16F10210P_17F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10210" targetNode="P_17F10210"/>
	<edges id="P_18F10210P_19F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10210" targetNode="P_19F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10210_I" deadCode="false" sourceNode="P_20F10210" targetNode="P_16F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_72F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10210_O" deadCode="false" sourceNode="P_20F10210" targetNode="P_17F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_72F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10210_I" deadCode="false" sourceNode="P_20F10210" targetNode="P_22F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_74F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10210_O" deadCode="false" sourceNode="P_20F10210" targetNode="P_23F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_74F10210"/>
	</edges>
	<edges id="P_20F10210P_21F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10210" targetNode="P_21F10210"/>
	<edges id="P_22F10210P_23F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10210" targetNode="P_23F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10210_I" deadCode="false" sourceNode="P_50F10210" targetNode="P_45F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_78F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10210_O" deadCode="false" sourceNode="P_50F10210" targetNode="P_46F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_78F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10210_I" deadCode="false" sourceNode="P_50F10210" targetNode="P_47F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_79F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10210_O" deadCode="false" sourceNode="P_50F10210" targetNode="P_48F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_79F10210"/>
	</edges>
	<edges id="P_50F10210P_51F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10210" targetNode="P_51F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10210_I" deadCode="false" sourceNode="P_24F10210" targetNode="P_45F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_82F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10210_O" deadCode="false" sourceNode="P_24F10210" targetNode="P_46F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_82F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10210_I" deadCode="false" sourceNode="P_24F10210" targetNode="P_47F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_83F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10210_O" deadCode="false" sourceNode="P_24F10210" targetNode="P_48F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_83F10210"/>
	</edges>
	<edges id="P_24F10210P_25F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10210" targetNode="P_25F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10210_I" deadCode="false" sourceNode="P_26F10210" targetNode="P_50F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_86F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10210_O" deadCode="false" sourceNode="P_26F10210" targetNode="P_51F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_86F10210"/>
	</edges>
	<edges id="P_26F10210P_27F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10210" targetNode="P_27F10210"/>
	<edges id="P_28F10210P_29F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10210" targetNode="P_29F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10210_I" deadCode="false" sourceNode="P_30F10210" targetNode="P_26F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_91F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10210_O" deadCode="false" sourceNode="P_30F10210" targetNode="P_27F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_91F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10210_I" deadCode="false" sourceNode="P_30F10210" targetNode="P_32F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_93F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10210_O" deadCode="false" sourceNode="P_30F10210" targetNode="P_33F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_93F10210"/>
	</edges>
	<edges id="P_30F10210P_31F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10210" targetNode="P_31F10210"/>
	<edges id="P_32F10210P_33F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10210" targetNode="P_33F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10210_I" deadCode="false" sourceNode="P_52F10210" targetNode="P_45F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_97F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10210_O" deadCode="false" sourceNode="P_52F10210" targetNode="P_46F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_97F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10210_I" deadCode="false" sourceNode="P_52F10210" targetNode="P_47F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_98F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10210_O" deadCode="false" sourceNode="P_52F10210" targetNode="P_48F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_98F10210"/>
	</edges>
	<edges id="P_52F10210P_53F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10210" targetNode="P_53F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10210_I" deadCode="false" sourceNode="P_34F10210" targetNode="P_45F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_102F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10210_O" deadCode="false" sourceNode="P_34F10210" targetNode="P_46F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_102F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10210_I" deadCode="false" sourceNode="P_34F10210" targetNode="P_47F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_103F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10210_O" deadCode="false" sourceNode="P_34F10210" targetNode="P_48F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_103F10210"/>
	</edges>
	<edges id="P_34F10210P_35F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10210" targetNode="P_35F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10210_I" deadCode="false" sourceNode="P_36F10210" targetNode="P_52F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_106F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10210_O" deadCode="false" sourceNode="P_36F10210" targetNode="P_53F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_106F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10210_I" deadCode="false" sourceNode="P_36F10210" targetNode="P_12F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_108F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10210_O" deadCode="false" sourceNode="P_36F10210" targetNode="P_13F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_108F10210"/>
	</edges>
	<edges id="P_36F10210P_37F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10210" targetNode="P_37F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10210_I" deadCode="false" sourceNode="P_38F10210" targetNode="P_12F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_111F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10210_O" deadCode="false" sourceNode="P_38F10210" targetNode="P_13F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_111F10210"/>
	</edges>
	<edges id="P_38F10210P_39F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10210" targetNode="P_39F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10210_I" deadCode="false" sourceNode="P_40F10210" targetNode="P_36F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_113F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10210_O" deadCode="false" sourceNode="P_40F10210" targetNode="P_37F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_113F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10210_I" deadCode="false" sourceNode="P_40F10210" targetNode="P_42F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_115F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10210_O" deadCode="false" sourceNode="P_40F10210" targetNode="P_43F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_115F10210"/>
	</edges>
	<edges id="P_40F10210P_41F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10210" targetNode="P_41F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10210_I" deadCode="false" sourceNode="P_42F10210" targetNode="P_12F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_118F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10210_O" deadCode="false" sourceNode="P_42F10210" targetNode="P_13F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_118F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10210_I" deadCode="false" sourceNode="P_42F10210" targetNode="P_54F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_120F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10210_O" deadCode="false" sourceNode="P_42F10210" targetNode="P_55F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_120F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10210_I" deadCode="false" sourceNode="P_42F10210" targetNode="P_56F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_121F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10210_O" deadCode="false" sourceNode="P_42F10210" targetNode="P_57F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_121F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10210_I" deadCode="false" sourceNode="P_42F10210" targetNode="P_58F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_122F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10210_O" deadCode="false" sourceNode="P_42F10210" targetNode="P_59F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_122F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10210_I" deadCode="false" sourceNode="P_42F10210" targetNode="P_38F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_124F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10210_O" deadCode="false" sourceNode="P_42F10210" targetNode="P_39F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_124F10210"/>
	</edges>
	<edges id="P_42F10210P_43F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10210" targetNode="P_43F10210"/>
	<edges id="P_54F10210P_55F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10210" targetNode="P_55F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10210_I" deadCode="true" sourceNode="P_60F10210" targetNode="P_61F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_165F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10210_O" deadCode="true" sourceNode="P_60F10210" targetNode="P_62F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_165F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10210_I" deadCode="true" sourceNode="P_60F10210" targetNode="P_61F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_168F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10210_O" deadCode="true" sourceNode="P_60F10210" targetNode="P_62F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_168F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10210_I" deadCode="true" sourceNode="P_60F10210" targetNode="P_61F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_171F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10210_O" deadCode="true" sourceNode="P_60F10210" targetNode="P_62F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_171F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10210_I" deadCode="true" sourceNode="P_60F10210" targetNode="P_61F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_174F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10210_O" deadCode="true" sourceNode="P_60F10210" targetNode="P_62F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_174F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_178F10210_I" deadCode="false" sourceNode="P_56F10210" targetNode="P_64F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_178F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_178F10210_O" deadCode="false" sourceNode="P_56F10210" targetNode="P_65F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_178F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10210_I" deadCode="false" sourceNode="P_56F10210" targetNode="P_64F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_181F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10210_O" deadCode="false" sourceNode="P_56F10210" targetNode="P_65F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_181F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10210_I" deadCode="false" sourceNode="P_56F10210" targetNode="P_64F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_184F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10210_O" deadCode="false" sourceNode="P_56F10210" targetNode="P_65F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_184F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10210_I" deadCode="false" sourceNode="P_56F10210" targetNode="P_64F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_187F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10210_O" deadCode="false" sourceNode="P_56F10210" targetNode="P_65F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_187F10210"/>
	</edges>
	<edges id="P_56F10210P_57F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10210" targetNode="P_57F10210"/>
	<edges id="P_45F10210P_46F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10210" targetNode="P_46F10210"/>
	<edges id="P_47F10210P_48F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10210" targetNode="P_48F10210"/>
	<edges id="P_58F10210P_59F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10210" targetNode="P_59F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_196F10210_I" deadCode="false" sourceNode="P_10F10210" targetNode="P_66F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_196F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_196F10210_O" deadCode="false" sourceNode="P_10F10210" targetNode="P_67F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_196F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_198F10210_I" deadCode="false" sourceNode="P_10F10210" targetNode="P_68F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_198F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_198F10210_O" deadCode="false" sourceNode="P_10F10210" targetNode="P_69F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_198F10210"/>
	</edges>
	<edges id="P_10F10210P_11F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10210" targetNode="P_11F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_203F10210_I" deadCode="true" sourceNode="P_66F10210" targetNode="P_61F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_203F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_203F10210_O" deadCode="true" sourceNode="P_66F10210" targetNode="P_62F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_203F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_208F10210_I" deadCode="true" sourceNode="P_66F10210" targetNode="P_61F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_208F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_208F10210_O" deadCode="true" sourceNode="P_66F10210" targetNode="P_62F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_208F10210"/>
	</edges>
	<edges id="P_66F10210P_67F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10210" targetNode="P_67F10210"/>
	<edges id="P_68F10210P_69F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10210" targetNode="P_69F10210"/>
	<edges id="P_61F10210P_62F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10210" targetNode="P_62F10210"/>
	<edges xsi:type="cbl:PerformEdge" id="S_237F10210_I" deadCode="false" sourceNode="P_64F10210" targetNode="P_72F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_237F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_237F10210_O" deadCode="false" sourceNode="P_64F10210" targetNode="P_73F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_237F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_238F10210_I" deadCode="false" sourceNode="P_64F10210" targetNode="P_74F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_238F10210"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_238F10210_O" deadCode="false" sourceNode="P_64F10210" targetNode="P_75F10210">
		<representations href="../../../cobol/LDBS4510.cbl.cobModel#S_238F10210"/>
	</edges>
	<edges id="P_64F10210P_65F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10210" targetNode="P_65F10210"/>
	<edges id="P_72F10210P_73F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10210" targetNode="P_73F10210"/>
	<edges id="P_74F10210P_75F10210" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10210" targetNode="P_75F10210"/>
	<edges xsi:type="cbl:DataEdge" id="S_107F10210_POS1" deadCode="false" targetNode="P_36F10210" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_107F10210"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_107F10210_POS2" deadCode="false" targetNode="P_36F10210" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_107F10210"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_110F10210_POS1" deadCode="false" targetNode="P_38F10210" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_110F10210"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_110F10210_POS2" deadCode="false" targetNode="P_38F10210" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_110F10210"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_117F10210_POS1" deadCode="false" targetNode="P_42F10210" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_117F10210"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_117F10210_POS2" deadCode="false" targetNode="P_42F10210" sourceNode="DB2_RICH">
		<representations href="../../../cobol/../importantStmts.cobModel#S_117F10210"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LCCS0234" cbl:id="LCCS0234" xsi:id="LCCS0234" packageRef="LCCS0234.igd#LCCS0234" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LCCS0234_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10134" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LCCS0234.cbl.cobModel#SC_1F10134"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10134" deadCode="false" name="PROGRAM_LCCS0234_FIRST_SENTENCES">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_1F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10134" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_2F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10134" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_3F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10134" deadCode="false" name="S0005-CTRL-DATI-INPUT">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_8F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10134" deadCode="false" name="EX-S0005">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_9F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10134" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_4F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10134" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_5F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10134" deadCode="false" name="S1100-ELABORA-POLIZZA">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_10F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10134" deadCode="false" name="EX-S1100">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_11F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10134" deadCode="false" name="S1200-ELABORA-ADESIONE">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_12F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10134" deadCode="false" name="EX-S1200">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_13F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10134" deadCode="false" name="S1300-ELABORA-GARANZIE">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_14F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10134" deadCode="false" name="EX-S1300">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_15F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10134" deadCode="false" name="S1400-ELABORA-TRANCHE">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_16F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10134" deadCode="false" name="EX-S1400">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_17F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10134" deadCode="false" name="S1500-ELABORA-LIQ">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_18F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10134" deadCode="false" name="EX-S1500">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_19F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10134" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_6F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10134" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_7F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10134" deadCode="true" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_22F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10134" deadCode="true" name="EX-S0290">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_23F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10134" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_20F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10134" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_21F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10134" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_26F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10134" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_27F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10134" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_24F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10134" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_25F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10134" deadCode="true" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_28F10134"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10134" deadCode="true" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LCCS0234.cbl.cobModel#P_29F10134"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LCCS0234_IDSI0011-PGM" name="Dynamic LCCS0234 IDSI0011-PGM" missing="true">
			<representations href="../../../../missing.xmi#IDNZUEJVPX1D32EMVWRJCPNI4VLNDB2RCBUGP0NJHYZMVMMXRVLEJE"/>
		</children>
	</packageNode>
	<edges id="SC_1F10134P_1F10134" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10134" targetNode="P_1F10134"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10134_I" deadCode="false" sourceNode="P_1F10134" targetNode="P_2F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_1F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10134_O" deadCode="false" sourceNode="P_1F10134" targetNode="P_3F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_1F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10134_I" deadCode="false" sourceNode="P_1F10134" targetNode="P_4F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_3F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10134_O" deadCode="false" sourceNode="P_1F10134" targetNode="P_5F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_3F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10134_I" deadCode="false" sourceNode="P_1F10134" targetNode="P_6F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_4F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10134_O" deadCode="false" sourceNode="P_1F10134" targetNode="P_7F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_4F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10134_I" deadCode="false" sourceNode="P_2F10134" targetNode="P_8F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_7F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10134_O" deadCode="false" sourceNode="P_2F10134" targetNode="P_9F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_7F10134"/>
	</edges>
	<edges id="P_2F10134P_3F10134" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10134" targetNode="P_3F10134"/>
	<edges id="P_8F10134P_9F10134" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10134" targetNode="P_9F10134"/>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10134_I" deadCode="false" sourceNode="P_4F10134" targetNode="P_10F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_12F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10134_O" deadCode="false" sourceNode="P_4F10134" targetNode="P_11F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_12F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10134_I" deadCode="false" sourceNode="P_4F10134" targetNode="P_12F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_13F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10134_O" deadCode="false" sourceNode="P_4F10134" targetNode="P_13F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_13F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10134_I" deadCode="false" sourceNode="P_4F10134" targetNode="P_14F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_14F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10134_O" deadCode="false" sourceNode="P_4F10134" targetNode="P_15F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_14F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10134_I" deadCode="false" sourceNode="P_4F10134" targetNode="P_16F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_15F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10134_O" deadCode="false" sourceNode="P_4F10134" targetNode="P_17F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_15F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10134_I" deadCode="false" sourceNode="P_4F10134" targetNode="P_18F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_16F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10134_O" deadCode="false" sourceNode="P_4F10134" targetNode="P_19F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_16F10134"/>
	</edges>
	<edges id="P_4F10134P_5F10134" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10134" targetNode="P_5F10134"/>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10134_I" deadCode="false" sourceNode="P_10F10134" targetNode="P_20F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_26F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10134_O" deadCode="false" sourceNode="P_10F10134" targetNode="P_21F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_26F10134"/>
	</edges>
	<edges id="P_10F10134P_11F10134" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10134" targetNode="P_11F10134"/>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10134_I" deadCode="false" sourceNode="P_12F10134" targetNode="P_20F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_38F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10134_O" deadCode="false" sourceNode="P_12F10134" targetNode="P_21F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_38F10134"/>
	</edges>
	<edges id="P_12F10134P_13F10134" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10134" targetNode="P_13F10134"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10134_I" deadCode="false" sourceNode="P_14F10134" targetNode="P_20F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_53F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10134_O" deadCode="false" sourceNode="P_14F10134" targetNode="P_21F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_53F10134"/>
	</edges>
	<edges id="P_14F10134P_15F10134" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10134" targetNode="P_15F10134"/>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10134_I" deadCode="false" sourceNode="P_16F10134" targetNode="P_20F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_68F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10134_O" deadCode="false" sourceNode="P_16F10134" targetNode="P_21F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_68F10134"/>
	</edges>
	<edges id="P_16F10134P_17F10134" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10134" targetNode="P_17F10134"/>
	<edges id="P_18F10134P_19F10134" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10134" targetNode="P_19F10134"/>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10134_I" deadCode="true" sourceNode="P_22F10134" targetNode="P_20F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_78F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10134_O" deadCode="true" sourceNode="P_22F10134" targetNode="P_21F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_78F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10134_I" deadCode="false" sourceNode="P_20F10134" targetNode="P_24F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_85F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10134_O" deadCode="false" sourceNode="P_20F10134" targetNode="P_25F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_85F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10134_I" deadCode="false" sourceNode="P_20F10134" targetNode="P_26F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_89F10134"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10134_O" deadCode="false" sourceNode="P_20F10134" targetNode="P_27F10134">
		<representations href="../../../cobol/LCCS0234.cbl.cobModel#S_89F10134"/>
	</edges>
	<edges id="P_20F10134P_21F10134" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10134" targetNode="P_21F10134"/>
	<edges id="P_26F10134P_27F10134" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10134" targetNode="P_27F10134"/>
	<edges id="P_24F10134P_25F10134" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10134" targetNode="P_25F10134"/>
	<edges xsi:type="cbl:CallEdge" id="S_83F10134" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_20F10134" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_83F10134"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_149F10134" deadCode="true" name="Dynamic IDSI0011-PGM" sourceNode="P_28F10134" targetNode="Dynamic_LCCS0234_IDSI0011-PGM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_149F10134"></representations>
	</edges>
</Package>

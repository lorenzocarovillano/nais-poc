<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LLBS0240" cbl:id="LLBS0240" xsi:id="LLBS0240" packageRef="LLBS0240.igd#LLBS0240" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LLBS0240_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10281" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LLBS0240.cbl.cobModel#SC_1F10281"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10281" deadCode="false" name="PROGRAM_LLBS0240_FIRST_SENTENCES">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_1F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10281" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_2F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10281" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_3F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10281" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_4F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10281" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_5F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10281" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_6F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10281" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_7F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10281" deadCode="false" name="VAL-DCLGEN-B03">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_10F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10281" deadCode="false" name="VAL-DCLGEN-B03-EX">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_11F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10281" deadCode="false" name="SCRIVI-BIL-ESTRATTI">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_8F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10281" deadCode="false" name="SCRIVI-BIL-ESTRATTI-EX">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_9F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10281" deadCode="false" name="VALORIZZA-AREA-DSH-B03">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_14F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10281" deadCode="false" name="VALORIZZA-AREA-DSH-B03-EX">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_15F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10281" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_18F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10281" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_23F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10281" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_21F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10281" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_22F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10281" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_19F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10281" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_20F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10281" deadCode="false" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_24F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10281" deadCode="false" name="EX-S0290">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_25F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10281" deadCode="false" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_26F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10281" deadCode="false" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_27F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10281" deadCode="false" name="AGGIORNA-TABELLA">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_16F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10281" deadCode="false" name="AGGIORNA-TABELLA-EX">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_17F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10281" deadCode="false" name="ESTR-SEQUENCE">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_12F10281"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10281" deadCode="false" name="ESTR-SEQUENCE-EX">
				<representations href="../../../cobol/LLBS0240.cbl.cobModel#P_13F10281"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0010" name="IDSS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10099"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0090" name="LCCS0090">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10133"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10281P_1F10281" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10281" targetNode="P_1F10281"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10281_I" deadCode="false" sourceNode="P_1F10281" targetNode="P_2F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_1F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10281_O" deadCode="false" sourceNode="P_1F10281" targetNode="P_3F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_1F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10281_I" deadCode="false" sourceNode="P_1F10281" targetNode="P_4F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_3F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10281_O" deadCode="false" sourceNode="P_1F10281" targetNode="P_5F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_3F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10281_I" deadCode="false" sourceNode="P_1F10281" targetNode="P_6F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_4F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10281_O" deadCode="false" sourceNode="P_1F10281" targetNode="P_7F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_4F10281"/>
	</edges>
	<edges id="P_2F10281P_3F10281" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10281" targetNode="P_3F10281"/>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10281_I" deadCode="false" sourceNode="P_4F10281" targetNode="P_8F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_9F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10281_O" deadCode="false" sourceNode="P_4F10281" targetNode="P_9F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_9F10281"/>
	</edges>
	<edges id="P_4F10281P_5F10281" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10281" targetNode="P_5F10281"/>
	<edges id="P_10F10281P_11F10281" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10281" targetNode="P_11F10281"/>
	<edges xsi:type="cbl:PerformEdge" id="S_604F10281_I" deadCode="false" sourceNode="P_8F10281" targetNode="P_12F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_604F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_604F10281_O" deadCode="false" sourceNode="P_8F10281" targetNode="P_13F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_604F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_612F10281_I" deadCode="false" sourceNode="P_8F10281" targetNode="P_10F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_612F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_612F10281_O" deadCode="false" sourceNode="P_8F10281" targetNode="P_11F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_612F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_613F10281_I" deadCode="false" sourceNode="P_8F10281" targetNode="P_14F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_613F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_613F10281_O" deadCode="false" sourceNode="P_8F10281" targetNode="P_15F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_613F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_614F10281_I" deadCode="false" sourceNode="P_8F10281" targetNode="P_16F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_614F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_614F10281_O" deadCode="false" sourceNode="P_8F10281" targetNode="P_17F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_614F10281"/>
	</edges>
	<edges id="P_8F10281P_9F10281" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10281" targetNode="P_9F10281"/>
	<edges id="P_14F10281P_15F10281" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10281" targetNode="P_15F10281"/>
	<edges xsi:type="cbl:PerformEdge" id="S_629F10281_I" deadCode="false" sourceNode="P_18F10281" targetNode="P_19F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_629F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_629F10281_O" deadCode="false" sourceNode="P_18F10281" targetNode="P_20F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_629F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_633F10281_I" deadCode="false" sourceNode="P_18F10281" targetNode="P_21F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_633F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_633F10281_O" deadCode="false" sourceNode="P_18F10281" targetNode="P_22F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_633F10281"/>
	</edges>
	<edges id="P_18F10281P_23F10281" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10281" targetNode="P_23F10281"/>
	<edges id="P_21F10281P_22F10281" xsi:type="cbl:FallThroughEdge" sourceNode="P_21F10281" targetNode="P_22F10281"/>
	<edges id="P_19F10281P_20F10281" xsi:type="cbl:FallThroughEdge" sourceNode="P_19F10281" targetNode="P_20F10281"/>
	<edges xsi:type="cbl:PerformEdge" id="S_669F10281_I" deadCode="false" sourceNode="P_24F10281" targetNode="P_18F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_669F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_669F10281_O" deadCode="false" sourceNode="P_24F10281" targetNode="P_23F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_669F10281"/>
	</edges>
	<edges id="P_24F10281P_25F10281" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10281" targetNode="P_25F10281"/>
	<edges id="P_26F10281P_27F10281" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10281" targetNode="P_27F10281"/>
	<edges xsi:type="cbl:PerformEdge" id="S_701F10281_I" deadCode="false" sourceNode="P_16F10281" targetNode="P_26F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_701F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_701F10281_O" deadCode="false" sourceNode="P_16F10281" targetNode="P_27F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_701F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_709F10281_I" deadCode="false" sourceNode="P_16F10281" targetNode="P_18F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_709F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_709F10281_O" deadCode="false" sourceNode="P_16F10281" targetNode="P_23F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_709F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_714F10281_I" deadCode="false" sourceNode="P_16F10281" targetNode="P_18F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_714F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_714F10281_O" deadCode="false" sourceNode="P_16F10281" targetNode="P_23F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_714F10281"/>
	</edges>
	<edges id="P_16F10281P_17F10281" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10281" targetNode="P_17F10281"/>
	<edges xsi:type="cbl:PerformEdge" id="S_721F10281_I" deadCode="false" sourceNode="P_12F10281" targetNode="P_24F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_721F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_721F10281_O" deadCode="false" sourceNode="P_12F10281" targetNode="P_25F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_721F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_729F10281_I" deadCode="false" sourceNode="P_12F10281" targetNode="P_18F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_729F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_729F10281_O" deadCode="false" sourceNode="P_12F10281" targetNode="P_23F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_729F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_734F10281_I" deadCode="false" sourceNode="P_12F10281" targetNode="P_18F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_734F10281"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_734F10281_O" deadCode="false" sourceNode="P_12F10281" targetNode="P_23F10281">
		<representations href="../../../cobol/LLBS0240.cbl.cobModel#S_734F10281"/>
	</edges>
	<edges id="P_12F10281P_13F10281" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10281" targetNode="P_13F10281"/>
	<edges xsi:type="cbl:CallEdge" id="S_627F10281" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_18F10281" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_627F10281"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_697F10281" deadCode="false" name="Dynamic IDSI0011-PGM" sourceNode="P_26F10281" targetNode="IDSS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_697F10281"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_717F10281" deadCode="false" name="Dynamic LCCS0090" sourceNode="P_12F10281" targetNode="LCCS0090">
		<representations href="../../../cobol/../importantStmts.cobModel#S_717F10281"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS3540" cbl:id="LDBS3540" xsi:id="LDBS3540" packageRef="LDBS3540.igd#LDBS3540" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS3540_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10202" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS3540.cbl.cobModel#SC_1F10202"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10202" deadCode="false" name="PROGRAM_LDBS3540_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_1F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10202" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_2F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10202" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_3F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10202" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_12F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10202" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_13F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10202" deadCode="false" name="A300-ELABORA">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_4F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10202" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_5F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10202" deadCode="false" name="A305-DECLARE-CURSOR-SC01">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_16F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10202" deadCode="false" name="A305-SC01-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_23F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10202" deadCode="false" name="A320-UPDATE-SC01">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_24F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10202" deadCode="false" name="A320-SC01-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_27F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10202" deadCode="false" name="A330-UPDATE-PK-SC01">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_25F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10202" deadCode="false" name="A330-SC01-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_26F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10202" deadCode="false" name="A350-CTRL-COMMIT">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_6F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10202" deadCode="false" name="A350-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_7F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10202" deadCode="false" name="A360-OPEN-CURSOR-SC01">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_30F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10202" deadCode="false" name="A360-SC01-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_31F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10202" deadCode="false" name="A370-CLOSE-CURSOR-SC01">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_32F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10202" deadCode="false" name="A370-SC01-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_33F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10202" deadCode="false" name="A380-FETCH-FIRST-SC01">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_34F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10202" deadCode="false" name="A380-SC01-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_37F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10202" deadCode="false" name="A390-FETCH-NEXT-SC01">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_35F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10202" deadCode="false" name="A390-SC01-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_36F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10202" deadCode="false" name="A400-FINE">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_8F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10202" deadCode="false" name="A400-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_9F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10202" deadCode="false" name="SC01-SELECTION-CURSOR-01">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_14F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10202" deadCode="false" name="SC01-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_15F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10202" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_38F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10202" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_39F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10202" deadCode="false" name="Z150-VALORIZZA-DATA-SERVICES">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_28F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10202" deadCode="false" name="Z150-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_29F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10202" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_17F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10202" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_18F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10202" deadCode="false" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_19F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10202" deadCode="false" name="Z900-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_20F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10202" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_40F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10202" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_41F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10202" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_21F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10202" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_22F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10202" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_10F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10202" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_11F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10202" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_46F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10202" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_47F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10202" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_48F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10202" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_49F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10202" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_42F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10202" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_43F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10202" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_50F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10202" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_51F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10202" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_44F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10202" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_45F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10202" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_52F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10202" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_53F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10202" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_54F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10202" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_55F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10202" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_56F10202"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10202" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS3540.cbl.cobModel#P_57F10202"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_PARAM_MOVI" name="PARAM_MOVI">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_PARAM_MOVI"/>
		</children>
	</packageNode>
	<edges id="SC_1F10202P_1F10202" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10202" targetNode="P_1F10202"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10202_I" deadCode="false" sourceNode="P_1F10202" targetNode="P_2F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_1F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10202_O" deadCode="false" sourceNode="P_1F10202" targetNode="P_3F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_1F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10202_I" deadCode="false" sourceNode="P_1F10202" targetNode="P_4F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_2F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10202_O" deadCode="false" sourceNode="P_1F10202" targetNode="P_5F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_2F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10202_I" deadCode="false" sourceNode="P_1F10202" targetNode="P_6F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_3F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10202_O" deadCode="false" sourceNode="P_1F10202" targetNode="P_7F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_3F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10202_I" deadCode="false" sourceNode="P_1F10202" targetNode="P_8F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_4F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10202_O" deadCode="false" sourceNode="P_1F10202" targetNode="P_9F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_4F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10202_I" deadCode="false" sourceNode="P_2F10202" targetNode="P_10F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_12F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10202_O" deadCode="false" sourceNode="P_2F10202" targetNode="P_11F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_12F10202"/>
	</edges>
	<edges id="P_2F10202P_3F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10202" targetNode="P_3F10202"/>
	<edges id="P_12F10202P_13F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10202" targetNode="P_13F10202"/>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10202_I" deadCode="false" sourceNode="P_4F10202" targetNode="P_14F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_25F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10202_O" deadCode="false" sourceNode="P_4F10202" targetNode="P_15F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_25F10202"/>
	</edges>
	<edges id="P_4F10202P_5F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10202" targetNode="P_5F10202"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10202_I" deadCode="false" sourceNode="P_16F10202" targetNode="P_17F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_37F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10202_O" deadCode="false" sourceNode="P_16F10202" targetNode="P_18F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_37F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10202_I" deadCode="false" sourceNode="P_16F10202" targetNode="P_19F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_38F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10202_O" deadCode="false" sourceNode="P_16F10202" targetNode="P_20F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_38F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10202_I" deadCode="false" sourceNode="P_16F10202" targetNode="P_21F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_39F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10202_O" deadCode="false" sourceNode="P_16F10202" targetNode="P_22F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_39F10202"/>
	</edges>
	<edges id="P_16F10202P_23F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10202" targetNode="P_23F10202"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10202_I" deadCode="false" sourceNode="P_24F10202" targetNode="P_25F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_43F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10202_O" deadCode="false" sourceNode="P_24F10202" targetNode="P_26F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_43F10202"/>
	</edges>
	<edges id="P_24F10202P_27F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10202" targetNode="P_27F10202"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10202_I" deadCode="false" sourceNode="P_25F10202" targetNode="P_28F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_45F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10202_O" deadCode="false" sourceNode="P_25F10202" targetNode="P_29F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_45F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10202_I" deadCode="false" sourceNode="P_25F10202" targetNode="P_17F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_46F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10202_O" deadCode="false" sourceNode="P_25F10202" targetNode="P_18F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_46F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10202_I" deadCode="false" sourceNode="P_25F10202" targetNode="P_19F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_47F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10202_O" deadCode="false" sourceNode="P_25F10202" targetNode="P_20F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_47F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10202_I" deadCode="false" sourceNode="P_25F10202" targetNode="P_21F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_48F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10202_O" deadCode="false" sourceNode="P_25F10202" targetNode="P_22F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_48F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10202_I" deadCode="false" sourceNode="P_25F10202" targetNode="P_12F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_50F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10202_O" deadCode="false" sourceNode="P_25F10202" targetNode="P_13F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_50F10202"/>
	</edges>
	<edges id="P_25F10202P_26F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_25F10202" targetNode="P_26F10202"/>
	<edges id="P_6F10202P_7F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10202" targetNode="P_7F10202"/>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10202_I" deadCode="false" sourceNode="P_30F10202" targetNode="P_16F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_54F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10202_O" deadCode="false" sourceNode="P_30F10202" targetNode="P_23F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_54F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10202_I" deadCode="false" sourceNode="P_30F10202" targetNode="P_12F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_56F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10202_O" deadCode="false" sourceNode="P_30F10202" targetNode="P_13F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_56F10202"/>
	</edges>
	<edges id="P_30F10202P_31F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10202" targetNode="P_31F10202"/>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10202_I" deadCode="false" sourceNode="P_32F10202" targetNode="P_12F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_59F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10202_O" deadCode="false" sourceNode="P_32F10202" targetNode="P_13F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_59F10202"/>
	</edges>
	<edges id="P_32F10202P_33F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10202" targetNode="P_33F10202"/>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10202_I" deadCode="false" sourceNode="P_34F10202" targetNode="P_30F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_61F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10202_O" deadCode="false" sourceNode="P_34F10202" targetNode="P_31F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_61F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10202_I" deadCode="false" sourceNode="P_34F10202" targetNode="P_35F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_63F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10202_O" deadCode="false" sourceNode="P_34F10202" targetNode="P_36F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_63F10202"/>
	</edges>
	<edges id="P_34F10202P_37F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10202" targetNode="P_37F10202"/>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10202_I" deadCode="false" sourceNode="P_35F10202" targetNode="P_12F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_66F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10202_O" deadCode="false" sourceNode="P_35F10202" targetNode="P_13F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_66F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10202_I" deadCode="false" sourceNode="P_35F10202" targetNode="P_38F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_68F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10202_O" deadCode="false" sourceNode="P_35F10202" targetNode="P_39F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_68F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10202_I" deadCode="false" sourceNode="P_35F10202" targetNode="P_40F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_69F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10202_O" deadCode="false" sourceNode="P_35F10202" targetNode="P_41F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_69F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10202_I" deadCode="false" sourceNode="P_35F10202" targetNode="P_32F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_71F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10202_O" deadCode="false" sourceNode="P_35F10202" targetNode="P_33F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_71F10202"/>
	</edges>
	<edges id="P_35F10202P_36F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_35F10202" targetNode="P_36F10202"/>
	<edges id="P_8F10202P_9F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10202" targetNode="P_9F10202"/>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10202_I" deadCode="false" sourceNode="P_14F10202" targetNode="P_30F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_78F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10202_O" deadCode="false" sourceNode="P_14F10202" targetNode="P_31F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_78F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10202_I" deadCode="false" sourceNode="P_14F10202" targetNode="P_32F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_79F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10202_O" deadCode="false" sourceNode="P_14F10202" targetNode="P_33F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_79F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10202_I" deadCode="false" sourceNode="P_14F10202" targetNode="P_34F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_80F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10202_O" deadCode="false" sourceNode="P_14F10202" targetNode="P_37F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_80F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10202_I" deadCode="false" sourceNode="P_14F10202" targetNode="P_35F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_81F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10202_O" deadCode="false" sourceNode="P_14F10202" targetNode="P_36F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_81F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10202_I" deadCode="false" sourceNode="P_14F10202" targetNode="P_24F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_82F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10202_O" deadCode="false" sourceNode="P_14F10202" targetNode="P_27F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_82F10202"/>
	</edges>
	<edges id="P_14F10202P_15F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10202" targetNode="P_15F10202"/>
	<edges id="P_38F10202P_39F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10202" targetNode="P_39F10202"/>
	<edges id="P_28F10202P_29F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10202" targetNode="P_29F10202"/>
	<edges id="P_17F10202P_18F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_17F10202" targetNode="P_18F10202"/>
	<edges xsi:type="cbl:PerformEdge" id="S_302F10202_I" deadCode="false" sourceNode="P_19F10202" targetNode="P_42F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_302F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_302F10202_O" deadCode="false" sourceNode="P_19F10202" targetNode="P_43F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_302F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_305F10202_I" deadCode="false" sourceNode="P_19F10202" targetNode="P_42F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_305F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_305F10202_O" deadCode="false" sourceNode="P_19F10202" targetNode="P_43F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_305F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10202_I" deadCode="false" sourceNode="P_19F10202" targetNode="P_42F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_309F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10202_O" deadCode="false" sourceNode="P_19F10202" targetNode="P_43F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_309F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_313F10202_I" deadCode="false" sourceNode="P_19F10202" targetNode="P_42F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_313F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_313F10202_O" deadCode="false" sourceNode="P_19F10202" targetNode="P_43F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_313F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_317F10202_I" deadCode="false" sourceNode="P_19F10202" targetNode="P_42F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_317F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_317F10202_O" deadCode="false" sourceNode="P_19F10202" targetNode="P_43F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_317F10202"/>
	</edges>
	<edges id="P_19F10202P_20F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_19F10202" targetNode="P_20F10202"/>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10202_I" deadCode="false" sourceNode="P_40F10202" targetNode="P_44F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_321F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10202_O" deadCode="false" sourceNode="P_40F10202" targetNode="P_45F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_321F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_324F10202_I" deadCode="false" sourceNode="P_40F10202" targetNode="P_44F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_324F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_324F10202_O" deadCode="false" sourceNode="P_40F10202" targetNode="P_45F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_324F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_328F10202_I" deadCode="false" sourceNode="P_40F10202" targetNode="P_44F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_328F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_328F10202_O" deadCode="false" sourceNode="P_40F10202" targetNode="P_45F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_328F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_332F10202_I" deadCode="false" sourceNode="P_40F10202" targetNode="P_44F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_332F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_332F10202_O" deadCode="false" sourceNode="P_40F10202" targetNode="P_45F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_332F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10202_I" deadCode="false" sourceNode="P_40F10202" targetNode="P_44F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_336F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_336F10202_O" deadCode="false" sourceNode="P_40F10202" targetNode="P_45F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_336F10202"/>
	</edges>
	<edges id="P_40F10202P_41F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10202" targetNode="P_41F10202"/>
	<edges id="P_21F10202P_22F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_21F10202" targetNode="P_22F10202"/>
	<edges xsi:type="cbl:PerformEdge" id="S_341F10202_I" deadCode="false" sourceNode="P_10F10202" targetNode="P_46F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_341F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_341F10202_O" deadCode="false" sourceNode="P_10F10202" targetNode="P_47F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_341F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_343F10202_I" deadCode="false" sourceNode="P_10F10202" targetNode="P_48F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_343F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_343F10202_O" deadCode="false" sourceNode="P_10F10202" targetNode="P_49F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_343F10202"/>
	</edges>
	<edges id="P_10F10202P_11F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10202" targetNode="P_11F10202"/>
	<edges xsi:type="cbl:PerformEdge" id="S_348F10202_I" deadCode="false" sourceNode="P_46F10202" targetNode="P_42F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_348F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_348F10202_O" deadCode="false" sourceNode="P_46F10202" targetNode="P_43F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_348F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_353F10202_I" deadCode="false" sourceNode="P_46F10202" targetNode="P_42F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_353F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_353F10202_O" deadCode="false" sourceNode="P_46F10202" targetNode="P_43F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_353F10202"/>
	</edges>
	<edges id="P_46F10202P_47F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10202" targetNode="P_47F10202"/>
	<edges id="P_48F10202P_49F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10202" targetNode="P_49F10202"/>
	<edges id="P_42F10202P_43F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10202" targetNode="P_43F10202"/>
	<edges xsi:type="cbl:PerformEdge" id="S_382F10202_I" deadCode="false" sourceNode="P_44F10202" targetNode="P_52F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_382F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_382F10202_O" deadCode="false" sourceNode="P_44F10202" targetNode="P_53F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_382F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_383F10202_I" deadCode="false" sourceNode="P_44F10202" targetNode="P_54F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_383F10202"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_383F10202_O" deadCode="false" sourceNode="P_44F10202" targetNode="P_55F10202">
		<representations href="../../../cobol/LDBS3540.cbl.cobModel#S_383F10202"/>
	</edges>
	<edges id="P_44F10202P_45F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10202" targetNode="P_45F10202"/>
	<edges id="P_52F10202P_53F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10202" targetNode="P_53F10202"/>
	<edges id="P_54F10202P_55F10202" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10202" targetNode="P_55F10202"/>
	<edges xsi:type="cbl:DataEdge" id="S_49F10202_POS1" deadCode="false" sourceNode="P_25F10202" targetNode="DB2_PARAM_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_49F10202"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_55F10202_POS1" deadCode="false" targetNode="P_30F10202" sourceNode="DB2_PARAM_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_55F10202"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_58F10202_POS1" deadCode="false" targetNode="P_32F10202" sourceNode="DB2_PARAM_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_58F10202"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_65F10202_POS1" deadCode="false" targetNode="P_35F10202" sourceNode="DB2_PARAM_MOVI">
		<representations href="../../../cobol/../importantStmts.cobModel#S_65F10202"></representations>
	</edges>
</Package>

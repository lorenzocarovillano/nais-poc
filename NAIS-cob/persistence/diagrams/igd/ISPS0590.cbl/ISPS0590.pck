<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="ISPS0590" cbl:id="ISPS0590" xsi:id="ISPS0590" packageRef="ISPS0590.igd#ISPS0590" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="ISPS0590_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10114" deadCode="false" name="FIRST">
			<representations href="../../../cobol/ISPS0590.cbl.cobModel#SC_1F10114"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10114" deadCode="false" name="PROGRAM_ISPS0590_FIRST_SENTENCES">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_1F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10114" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_2F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10114" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_3F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10114" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_4F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10114" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_5F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10114" deadCode="false" name="S1050-PREP-AREA-ISPS0590">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_8F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10114" deadCode="false" name="EX-S1050">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_9F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10114" deadCode="false" name="S1100-CALL-ISPS0590">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_10F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10114" deadCode="false" name="EX-S1100">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_11F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10114" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_6F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10114" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_7F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10114" deadCode="false" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_12F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10114" deadCode="false" name="EX-S0290">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_13F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10114" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_16F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10114" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_17F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10114" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_20F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10114" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_21F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10114" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_18F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10114" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_19F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10114" deadCode="false" name="S0320-OUTPUT-PRODOTTO">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_14F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10114" deadCode="false" name="EX-S0320-OUTPUT-PRODOTTO">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_15F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10114" deadCode="false" name="S0322-SALVA-ERRORE">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_22F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10114" deadCode="false" name="EX-S0322">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_23F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10114" deadCode="false" name="S0321-ERRORI-DEROGA">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_24F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10114" deadCode="false" name="EX-S0321">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_25F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10114" deadCode="false" name="S0323-SALVA-DEROGA">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_26F10114"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10114" deadCode="false" name="EX-S0323">
				<representations href="../../../cobol/ISPS0590.cbl.cobModel#P_27F10114"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IJCSMQ01" name="IJCSMQ01" missing="true">
			<representations href="../../../../missing.xmi#ID5ATRMDNFB0YCCMGWHJU5JJS0SG4JNFAX4FQPUOM0TGMAVTOXJ3AJ"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10114P_1F10114" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10114" targetNode="P_1F10114"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10114_I" deadCode="false" sourceNode="P_1F10114" targetNode="P_2F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_1F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10114_O" deadCode="false" sourceNode="P_1F10114" targetNode="P_3F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_1F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10114_I" deadCode="false" sourceNode="P_1F10114" targetNode="P_4F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_3F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10114_O" deadCode="false" sourceNode="P_1F10114" targetNode="P_5F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_3F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10114_I" deadCode="false" sourceNode="P_1F10114" targetNode="P_6F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_4F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10114_O" deadCode="false" sourceNode="P_1F10114" targetNode="P_7F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_4F10114"/>
	</edges>
	<edges id="P_2F10114P_3F10114" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10114" targetNode="P_3F10114"/>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10114_I" deadCode="false" sourceNode="P_4F10114" targetNode="P_8F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_7F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10114_O" deadCode="false" sourceNode="P_4F10114" targetNode="P_9F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_7F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10114_I" deadCode="false" sourceNode="P_4F10114" targetNode="P_10F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_8F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10114_O" deadCode="false" sourceNode="P_4F10114" targetNode="P_11F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_8F10114"/>
	</edges>
	<edges id="P_4F10114P_5F10114" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10114" targetNode="P_5F10114"/>
	<edges id="P_8F10114P_9F10114" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10114" targetNode="P_9F10114"/>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10114_I" deadCode="false" sourceNode="P_10F10114" targetNode="P_12F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_26F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10114_O" deadCode="false" sourceNode="P_10F10114" targetNode="P_13F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_26F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10114_I" deadCode="false" sourceNode="P_10F10114" targetNode="P_14F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_30F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10114_O" deadCode="false" sourceNode="P_10F10114" targetNode="P_15F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_30F10114"/>
	</edges>
	<edges id="P_10F10114P_11F10114" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10114" targetNode="P_11F10114"/>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10114_I" deadCode="false" sourceNode="P_12F10114" targetNode="P_16F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_42F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10114_O" deadCode="false" sourceNode="P_12F10114" targetNode="P_17F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_42F10114"/>
	</edges>
	<edges id="P_12F10114P_13F10114" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10114" targetNode="P_13F10114"/>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10114_I" deadCode="false" sourceNode="P_16F10114" targetNode="P_18F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_49F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10114_O" deadCode="false" sourceNode="P_16F10114" targetNode="P_19F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_49F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10114_I" deadCode="false" sourceNode="P_16F10114" targetNode="P_20F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_53F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10114_O" deadCode="false" sourceNode="P_16F10114" targetNode="P_21F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_53F10114"/>
	</edges>
	<edges id="P_16F10114P_17F10114" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10114" targetNode="P_17F10114"/>
	<edges id="P_20F10114P_21F10114" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10114" targetNode="P_21F10114"/>
	<edges id="P_18F10114P_19F10114" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10114" targetNode="P_19F10114"/>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10114_I" deadCode="false" sourceNode="P_14F10114" targetNode="P_22F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_99F10114"/>
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_101F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10114_O" deadCode="false" sourceNode="P_14F10114" targetNode="P_23F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_99F10114"/>
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_101F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10114_I" deadCode="false" sourceNode="P_14F10114" targetNode="P_24F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_102F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10114_O" deadCode="false" sourceNode="P_14F10114" targetNode="P_25F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_102F10114"/>
	</edges>
	<edges id="P_14F10114P_15F10114" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10114" targetNode="P_15F10114"/>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10114_I" deadCode="false" sourceNode="P_22F10114" targetNode="P_16F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_107F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10114_O" deadCode="false" sourceNode="P_22F10114" targetNode="P_17F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_107F10114"/>
	</edges>
	<edges id="P_22F10114P_23F10114" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10114" targetNode="P_23F10114"/>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10114_I" deadCode="false" sourceNode="P_24F10114" targetNode="P_22F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_111F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10114_O" deadCode="false" sourceNode="P_24F10114" targetNode="P_23F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_111F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10114_I" deadCode="false" sourceNode="P_24F10114" targetNode="P_22F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_113F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10114_O" deadCode="false" sourceNode="P_24F10114" targetNode="P_23F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_113F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10114_I" deadCode="false" sourceNode="P_24F10114" targetNode="P_22F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_115F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10114_O" deadCode="false" sourceNode="P_24F10114" targetNode="P_23F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_115F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10114_I" deadCode="false" sourceNode="P_24F10114" targetNode="P_22F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_118F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10114_O" deadCode="false" sourceNode="P_24F10114" targetNode="P_23F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_118F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10114_I" deadCode="false" sourceNode="P_24F10114" targetNode="P_26F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_119F10114"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10114_O" deadCode="false" sourceNode="P_24F10114" targetNode="P_27F10114">
		<representations href="../../../cobol/ISPS0590.cbl.cobModel#S_119F10114"/>
	</edges>
	<edges id="P_24F10114P_25F10114" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10114" targetNode="P_25F10114"/>
	<edges id="P_26F10114P_27F10114" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10114" targetNode="P_27F10114"/>
	<edges xsi:type="cbl:CallEdge" id="S_22F10114" deadCode="false" name="Dynamic INTERF-MQSERIES" sourceNode="P_10F10114" targetNode="IJCSMQ01">
		<representations href="../../../cobol/../importantStmts.cobModel#S_22F10114"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_47F10114" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_16F10114" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_47F10114"></representations>
	</edges>
</Package>

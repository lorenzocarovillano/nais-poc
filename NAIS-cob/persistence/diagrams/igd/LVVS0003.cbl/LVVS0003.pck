<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0003" cbl:id="LVVS0003" xsi:id="LVVS0003" packageRef="LVVS0003.igd#LVVS0003" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0003_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10307" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0003.cbl.cobModel#SC_1F10307"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10307" deadCode="false" name="PROGRAM_LVVS0003_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_1F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10307" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_2F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10307" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_3F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10307" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_4F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10307" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_5F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10307" deadCode="false" name="L450-LEGGI-POLIZZA">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_10F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10307" deadCode="false" name="L450-LEGGI-POLIZZA-EX">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_11F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10307" deadCode="false" name="L460-LEGGI-ADESIONE">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_12F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10307" deadCode="false" name="L460-LEGGI-ADESIONE-EX">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_13F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10307" deadCode="false" name="LEGGI-PMO">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_16F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10307" deadCode="false" name="LEGGI-PMO-EX">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_17F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10307" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_8F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10307" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_9F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10307" deadCode="false" name="S1253-RECUP-MOVI">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_14F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10307" deadCode="false" name="S1253-EX">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_15F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10307" deadCode="false" name="S1300-CALL-LVVS0000">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_20F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10307" deadCode="false" name="S1300-EX">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_21F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10307" deadCode="false" name="S1400-CALC-DATA">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_18F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10307" deadCode="false" name="EX-S1400">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_19F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10307" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_6F10307"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10307" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0003.cbl.cobModel#P_7F10307"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSPOL0" name="IDBSPOL0">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10078"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSADE0" name="IDBSADE0">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10016"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS7850" name="LDBS7850">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10239"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSE060" name="LDBSE060">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10257"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LVVS0000" name="LVVS0000">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10304"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10307P_1F10307" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10307" targetNode="P_1F10307"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10307_I" deadCode="false" sourceNode="P_1F10307" targetNode="P_2F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_1F10307"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10307_O" deadCode="false" sourceNode="P_1F10307" targetNode="P_3F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_1F10307"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10307_I" deadCode="false" sourceNode="P_1F10307" targetNode="P_4F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_2F10307"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10307_O" deadCode="false" sourceNode="P_1F10307" targetNode="P_5F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_2F10307"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10307_I" deadCode="false" sourceNode="P_1F10307" targetNode="P_6F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_3F10307"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10307_O" deadCode="false" sourceNode="P_1F10307" targetNode="P_7F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_3F10307"/>
	</edges>
	<edges id="P_2F10307P_3F10307" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10307" targetNode="P_3F10307"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10307_I" deadCode="false" sourceNode="P_4F10307" targetNode="P_8F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_10F10307"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10307_O" deadCode="false" sourceNode="P_4F10307" targetNode="P_9F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_10F10307"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10307_I" deadCode="false" sourceNode="P_4F10307" targetNode="P_10F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_11F10307"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10307_O" deadCode="false" sourceNode="P_4F10307" targetNode="P_11F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_11F10307"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10307_I" deadCode="false" sourceNode="P_4F10307" targetNode="P_12F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_14F10307"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10307_O" deadCode="false" sourceNode="P_4F10307" targetNode="P_13F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_14F10307"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10307_I" deadCode="false" sourceNode="P_4F10307" targetNode="P_14F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_16F10307"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10307_O" deadCode="false" sourceNode="P_4F10307" targetNode="P_15F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_16F10307"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10307_I" deadCode="false" sourceNode="P_4F10307" targetNode="P_16F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_19F10307"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10307_O" deadCode="false" sourceNode="P_4F10307" targetNode="P_17F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_19F10307"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10307_I" deadCode="false" sourceNode="P_4F10307" targetNode="P_18F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_22F10307"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10307_O" deadCode="false" sourceNode="P_4F10307" targetNode="P_19F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_22F10307"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10307_I" deadCode="false" sourceNode="P_4F10307" targetNode="P_20F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_29F10307"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10307_O" deadCode="false" sourceNode="P_4F10307" targetNode="P_21F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_29F10307"/>
	</edges>
	<edges id="P_4F10307P_5F10307" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10307" targetNode="P_5F10307"/>
	<edges id="P_10F10307P_11F10307" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10307" targetNode="P_11F10307"/>
	<edges id="P_12F10307P_13F10307" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10307" targetNode="P_13F10307"/>
	<edges id="P_16F10307P_17F10307" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10307" targetNode="P_17F10307"/>
	<edges id="P_8F10307P_9F10307" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10307" targetNode="P_9F10307"/>
	<edges id="P_14F10307P_15F10307" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10307" targetNode="P_15F10307"/>
	<edges id="P_20F10307P_21F10307" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10307" targetNode="P_21F10307"/>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10307_I" deadCode="false" sourceNode="P_18F10307" targetNode="P_20F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_148F10307"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10307_O" deadCode="false" sourceNode="P_18F10307" targetNode="P_21F10307">
		<representations href="../../../cobol/LVVS0003.cbl.cobModel#S_148F10307"/>
	</edges>
	<edges id="P_18F10307P_19F10307" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10307" targetNode="P_19F10307"/>
	<edges xsi:type="cbl:CallEdge" id="S_36F10307" deadCode="false" name="Dynamic WK-PGM-CALLED" sourceNode="P_10F10307" targetNode="IDBSPOL0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_36F10307"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_53F10307" deadCode="false" name="Dynamic WK-PGM-CALLED" sourceNode="P_12F10307" targetNode="IDBSADE0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_53F10307"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_79F10307" deadCode="false" name="Dynamic WK-PGM-CALLED" sourceNode="P_16F10307" targetNode="LDBS7850">
		<representations href="../../../cobol/../importantStmts.cobModel#S_79F10307"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_114F10307" deadCode="false" name="Dynamic PGM-LDBSE060" sourceNode="P_14F10307" targetNode="LDBSE060">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10307"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_134F10307" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_20F10307" targetNode="LVVS0000">
		<representations href="../../../cobol/../importantStmts.cobModel#S_134F10307"></representations>
	</edges>
</Package>

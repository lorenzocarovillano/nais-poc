<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LOAS9000" cbl:id="LOAS9000" xsi:id="LOAS9000" packageRef="LOAS9000.igd#LOAS9000" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LOAS9000_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10297" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LOAS9000.cbl.cobModel#SC_1F10297"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10297" deadCode="false" name="PROGRAM_LOAS9000_FIRST_SENTENCES">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_1F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10297" deadCode="false" name="A000-OPERAZ-INIZ">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_2F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10297" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_3F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10297" deadCode="false" name="A020-VALORIZZA-IDSV0003">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_4F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10297" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_5F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10297" deadCode="false" name="A030-IDENTIFICA-TIPOLOGIE">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_10F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10297" deadCode="false" name="A030-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_11F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10297" deadCode="false" name="A050-INITIALIZE">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_8F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10297" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_9F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10297" deadCode="false" name="B000-ELABORA">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_6F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10297" deadCode="false" name="B000-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_7F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10297" deadCode="false" name="E000-ESTRAI-DATI">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_12F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10297" deadCode="false" name="E000-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_13F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10297" deadCode="false" name="E100-PREPARA-GUIDE">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_16F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10297" deadCode="false" name="E100-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_17F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10297" deadCode="false" name="E200-ACCEDI-GUIDE">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_18F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10297" deadCode="false" name="E200-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_19F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10297" deadCode="false" name="E300-CARICA-DATI-ESTRATTI">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_22F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10297" deadCode="false" name="E300-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_23F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10297" deadCode="false" name="E500-ELABORA-BUSINESS">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_14F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10297" deadCode="false" name="E500-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_15F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10297" deadCode="false" name="E600-PRE-BUSINESS">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_28F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10297" deadCode="false" name="E600-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_29F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10297" deadCode="false" name="E700-POST-BUSINESS">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_32F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10297" deadCode="false" name="E700-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_33F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10297" deadCode="false" name="E800-AGGIORNA-JOB">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_34F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10297" deadCode="false" name="E800-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_35F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10297" deadCode="false" name="CALL-PGM-GUIDA">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_20F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10297" deadCode="false" name="CALL-PGM-GUIDA-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_21F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10297" deadCode="false" name="CALL-PGM-BUSINESS">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_30F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10297" deadCode="false" name="CALL-PGM-BUSINESS-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_31F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10297" deadCode="false" name="VALORIZZA-OUTPUT-PMO">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_26F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10297" deadCode="false" name="VALORIZZA-OUTPUT-PMO-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_27F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10297" deadCode="true" name="INIZIA-TOT-PMO">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_36F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10297" deadCode="true" name="INIZIA-TOT-PMO-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_43F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10297" deadCode="true" name="INIZIA-NULL-PMO">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_41F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10297" deadCode="true" name="INIZIA-NULL-PMO-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_42F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10297" deadCode="true" name="INIZIA-ZEROES-PMO">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_37F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10297" deadCode="true" name="INIZIA-ZEROES-PMO-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_38F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10297" deadCode="true" name="INIZIA-SPACES-PMO">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_39F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10297" deadCode="true" name="INIZIA-SPACES-PMO-EX">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_40F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10297" deadCode="true" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_44F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10297" deadCode="true" name="EX-S0290">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_45F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10297" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_24F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10297" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_25F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10297" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_48F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10297" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_49F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10297" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_46F10297"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10297" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LOAS9000.cbl.cobModel#P_47F10297"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS3540" name="LDBS3540">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10202"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LOAS0310" name="LOAS0310">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10288"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10297P_1F10297" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10297" targetNode="P_1F10297"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10297_I" deadCode="false" sourceNode="P_1F10297" targetNode="P_2F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_2F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10297_O" deadCode="false" sourceNode="P_1F10297" targetNode="P_3F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_2F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10297_I" deadCode="false" sourceNode="P_1F10297" targetNode="P_4F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_3F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10297_O" deadCode="false" sourceNode="P_1F10297" targetNode="P_5F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_3F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10297_I" deadCode="false" sourceNode="P_1F10297" targetNode="P_6F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_4F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10297_O" deadCode="false" sourceNode="P_1F10297" targetNode="P_7F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_4F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10297_I" deadCode="false" sourceNode="P_2F10297" targetNode="P_8F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_7F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10297_O" deadCode="false" sourceNode="P_2F10297" targetNode="P_9F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_7F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10297_I" deadCode="false" sourceNode="P_2F10297" targetNode="P_10F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_8F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10297_O" deadCode="false" sourceNode="P_2F10297" targetNode="P_11F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_8F10297"/>
	</edges>
	<edges id="P_2F10297P_3F10297" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10297" targetNode="P_3F10297"/>
	<edges id="P_4F10297P_5F10297" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10297" targetNode="P_5F10297"/>
	<edges id="P_10F10297P_11F10297" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10297" targetNode="P_11F10297"/>
	<edges id="P_8F10297P_9F10297" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10297" targetNode="P_9F10297"/>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10297_I" deadCode="false" sourceNode="P_6F10297" targetNode="P_12F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_46F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10297_O" deadCode="false" sourceNode="P_6F10297" targetNode="P_13F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_46F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10297_I" deadCode="false" sourceNode="P_6F10297" targetNode="P_14F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_48F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10297_O" deadCode="false" sourceNode="P_6F10297" targetNode="P_15F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_48F10297"/>
	</edges>
	<edges id="P_6F10297P_7F10297" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10297" targetNode="P_7F10297"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10297_I" deadCode="false" sourceNode="P_12F10297" targetNode="P_16F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_51F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10297_O" deadCode="false" sourceNode="P_12F10297" targetNode="P_17F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_51F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10297_I" deadCode="false" sourceNode="P_12F10297" targetNode="P_18F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_52F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10297_O" deadCode="false" sourceNode="P_12F10297" targetNode="P_19F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_52F10297"/>
	</edges>
	<edges id="P_12F10297P_13F10297" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10297" targetNode="P_13F10297"/>
	<edges id="P_16F10297P_17F10297" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10297" targetNode="P_17F10297"/>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10297_I" deadCode="false" sourceNode="P_18F10297" targetNode="P_20F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_73F10297"/>
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_74F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10297_O" deadCode="false" sourceNode="P_18F10297" targetNode="P_21F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_73F10297"/>
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_74F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10297_I" deadCode="false" sourceNode="P_18F10297" targetNode="P_22F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_73F10297"/>
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_77F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10297_O" deadCode="false" sourceNode="P_18F10297" targetNode="P_23F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_73F10297"/>
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_77F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10297_I" deadCode="false" sourceNode="P_18F10297" targetNode="P_24F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_73F10297"/>
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_84F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10297_O" deadCode="false" sourceNode="P_18F10297" targetNode="P_25F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_73F10297"/>
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_84F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10297_I" deadCode="false" sourceNode="P_18F10297" targetNode="P_24F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_73F10297"/>
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_90F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10297_O" deadCode="false" sourceNode="P_18F10297" targetNode="P_25F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_73F10297"/>
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_90F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10297_I" deadCode="false" sourceNode="P_18F10297" targetNode="P_24F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_73F10297"/>
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_95F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10297_O" deadCode="false" sourceNode="P_18F10297" targetNode="P_25F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_73F10297"/>
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_95F10297"/>
	</edges>
	<edges id="P_18F10297P_19F10297" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10297" targetNode="P_19F10297"/>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10297_I" deadCode="false" sourceNode="P_22F10297" targetNode="P_26F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_102F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10297_O" deadCode="false" sourceNode="P_22F10297" targetNode="P_27F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_102F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10297_I" deadCode="false" sourceNode="P_22F10297" targetNode="P_24F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_107F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10297_O" deadCode="false" sourceNode="P_22F10297" targetNode="P_25F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_107F10297"/>
	</edges>
	<edges id="P_22F10297P_23F10297" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10297" targetNode="P_23F10297"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10297_I" deadCode="false" sourceNode="P_14F10297" targetNode="P_28F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_110F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10297_O" deadCode="false" sourceNode="P_14F10297" targetNode="P_29F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_110F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10297_I" deadCode="false" sourceNode="P_14F10297" targetNode="P_30F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_111F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10297_O" deadCode="false" sourceNode="P_14F10297" targetNode="P_31F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_111F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10297_I" deadCode="false" sourceNode="P_14F10297" targetNode="P_32F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_112F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10297_O" deadCode="false" sourceNode="P_14F10297" targetNode="P_33F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_112F10297"/>
	</edges>
	<edges id="P_14F10297P_15F10297" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10297" targetNode="P_15F10297"/>
	<edges id="P_28F10297P_29F10297" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10297" targetNode="P_29F10297"/>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10297_I" deadCode="false" sourceNode="P_32F10297" targetNode="P_34F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_127F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10297_O" deadCode="false" sourceNode="P_32F10297" targetNode="P_35F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_127F10297"/>
	</edges>
	<edges id="P_32F10297P_33F10297" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10297" targetNode="P_33F10297"/>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10297_I" deadCode="false" sourceNode="P_34F10297" targetNode="P_20F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_133F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10297_O" deadCode="false" sourceNode="P_34F10297" targetNode="P_21F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_133F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_141F10297_I" deadCode="false" sourceNode="P_34F10297" targetNode="P_24F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_141F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_141F10297_O" deadCode="false" sourceNode="P_34F10297" targetNode="P_25F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_141F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10297_I" deadCode="false" sourceNode="P_34F10297" targetNode="P_24F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_146F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10297_O" deadCode="false" sourceNode="P_34F10297" targetNode="P_25F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_146F10297"/>
	</edges>
	<edges id="P_34F10297P_35F10297" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10297" targetNode="P_35F10297"/>
	<edges xsi:type="cbl:PerformEdge" id="S_156F10297_I" deadCode="false" sourceNode="P_20F10297" targetNode="P_24F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_156F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_156F10297_O" deadCode="false" sourceNode="P_20F10297" targetNode="P_25F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_156F10297"/>
	</edges>
	<edges id="P_20F10297P_21F10297" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10297" targetNode="P_21F10297"/>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10297_I" deadCode="false" sourceNode="P_30F10297" targetNode="P_24F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_162F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10297_O" deadCode="false" sourceNode="P_30F10297" targetNode="P_25F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_162F10297"/>
	</edges>
	<edges id="P_30F10297P_31F10297" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10297" targetNode="P_31F10297"/>
	<edges id="P_26F10297P_27F10297" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10297" targetNode="P_27F10297"/>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10297_I" deadCode="true" sourceNode="P_36F10297" targetNode="P_37F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_308F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10297_O" deadCode="true" sourceNode="P_36F10297" targetNode="P_38F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_308F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10297_I" deadCode="true" sourceNode="P_36F10297" targetNode="P_39F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_309F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10297_O" deadCode="true" sourceNode="P_36F10297" targetNode="P_40F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_309F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_310F10297_I" deadCode="true" sourceNode="P_36F10297" targetNode="P_41F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_310F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_310F10297_O" deadCode="true" sourceNode="P_36F10297" targetNode="P_42F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_310F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_375F10297_I" deadCode="true" sourceNode="P_44F10297" targetNode="P_24F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_375F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_375F10297_O" deadCode="true" sourceNode="P_44F10297" targetNode="P_25F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_375F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_382F10297_I" deadCode="false" sourceNode="P_24F10297" targetNode="P_46F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_382F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_382F10297_O" deadCode="false" sourceNode="P_24F10297" targetNode="P_47F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_382F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_386F10297_I" deadCode="false" sourceNode="P_24F10297" targetNode="P_48F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_386F10297"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_386F10297_O" deadCode="false" sourceNode="P_24F10297" targetNode="P_49F10297">
		<representations href="../../../cobol/LOAS9000.cbl.cobModel#S_386F10297"/>
	</edges>
	<edges id="P_24F10297P_25F10297" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10297" targetNode="P_25F10297"/>
	<edges id="P_48F10297P_49F10297" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10297" targetNode="P_49F10297"/>
	<edges id="P_46F10297P_47F10297" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10297" targetNode="P_47F10297"/>
	<edges xsi:type="cbl:CallEdge" id="S_151F10297" deadCode="false" name="Dynamic WK-PGM-GUIDA" sourceNode="P_20F10297" targetNode="LDBS3540">
		<representations href="../../../cobol/../importantStmts.cobModel#S_151F10297"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_160F10297" deadCode="false" name="Dynamic WK-PGM-BUSINESS" sourceNode="P_30F10297" targetNode="LOAS0310">
		<representations href="../../../cobol/../importantStmts.cobModel#S_160F10297"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_380F10297" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_24F10297" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_380F10297"></representations>
	</edges>
</Package>

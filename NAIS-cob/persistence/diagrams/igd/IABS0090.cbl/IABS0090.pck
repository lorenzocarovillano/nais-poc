<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IABS0090" cbl:id="IABS0090" xsi:id="IABS0090" packageRef="IABS0090.igd#IABS0090" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IABS0090_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10008" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IABS0090.cbl.cobModel#SC_1F10008"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10008" deadCode="false" name="PROGRAM_IABS0090_FIRST_SENTENCES">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_1F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10008" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_2F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10008" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_3F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10008" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_6F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10008" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_7F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10008" deadCode="false" name="A200-ELABORA-PK">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_4F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10008" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_5F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10008" deadCode="false" name="A210-SELECT-PK">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_8F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10008" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_9F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10008" deadCode="false" name="A220-INSERT-PK">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_10F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10008" deadCode="false" name="A220-EX">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_11F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10008" deadCode="false" name="A230-UPDATE-PK">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_12F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10008" deadCode="false" name="A230-EX">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_13F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10008" deadCode="false" name="A240-DELETE-PK">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_14F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10008" deadCode="false" name="A240-EX">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_15F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10008" deadCode="false" name="Z080-ESTRAI-ID-MESSAGE">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_18F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10008" deadCode="false" name="Z080-EX">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_19F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10008" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_16F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10008" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_17F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10008" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_20F10008"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10008" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/IABS0090.cbl.cobModel#P_21F10008"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_BTC_EXE_MESSAGE" name="BTC_EXE_MESSAGE">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_BTC_EXE_MESSAGE"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0090" name="LCCS0090">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10133"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10008P_1F10008" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10008" targetNode="P_1F10008"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10008_I" deadCode="false" sourceNode="P_1F10008" targetNode="P_2F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_1F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10008_O" deadCode="false" sourceNode="P_1F10008" targetNode="P_3F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_1F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10008_I" deadCode="false" sourceNode="P_1F10008" targetNode="P_4F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_3F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10008_O" deadCode="false" sourceNode="P_1F10008" targetNode="P_5F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_3F10008"/>
	</edges>
	<edges id="P_2F10008P_3F10008" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10008" targetNode="P_3F10008"/>
	<edges id="P_6F10008P_7F10008" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10008" targetNode="P_7F10008"/>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10008_I" deadCode="false" sourceNode="P_4F10008" targetNode="P_8F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_26F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10008_O" deadCode="false" sourceNode="P_4F10008" targetNode="P_9F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_26F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10008_I" deadCode="false" sourceNode="P_4F10008" targetNode="P_10F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_27F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10008_O" deadCode="false" sourceNode="P_4F10008" targetNode="P_11F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_27F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10008_I" deadCode="false" sourceNode="P_4F10008" targetNode="P_12F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_28F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10008_O" deadCode="false" sourceNode="P_4F10008" targetNode="P_13F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_28F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10008_I" deadCode="false" sourceNode="P_4F10008" targetNode="P_14F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_29F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10008_O" deadCode="false" sourceNode="P_4F10008" targetNode="P_15F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_29F10008"/>
	</edges>
	<edges id="P_4F10008P_5F10008" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10008" targetNode="P_5F10008"/>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10008_I" deadCode="false" sourceNode="P_8F10008" targetNode="P_6F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_33F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10008_O" deadCode="false" sourceNode="P_8F10008" targetNode="P_7F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_33F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10008_I" deadCode="false" sourceNode="P_8F10008" targetNode="P_16F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_35F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10008_O" deadCode="false" sourceNode="P_8F10008" targetNode="P_17F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_35F10008"/>
	</edges>
	<edges id="P_8F10008P_9F10008" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10008" targetNode="P_9F10008"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10008_I" deadCode="false" sourceNode="P_10F10008" targetNode="P_18F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_37F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10008_O" deadCode="false" sourceNode="P_10F10008" targetNode="P_19F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_37F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10008_I" deadCode="false" sourceNode="P_10F10008" targetNode="P_20F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_39F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10008_O" deadCode="false" sourceNode="P_10F10008" targetNode="P_21F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_39F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10008_I" deadCode="false" sourceNode="P_10F10008" targetNode="P_6F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_41F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10008_O" deadCode="false" sourceNode="P_10F10008" targetNode="P_7F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_41F10008"/>
	</edges>
	<edges id="P_10F10008P_11F10008" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10008" targetNode="P_11F10008"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10008_I" deadCode="false" sourceNode="P_12F10008" targetNode="P_20F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_43F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10008_O" deadCode="false" sourceNode="P_12F10008" targetNode="P_21F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_43F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10008_I" deadCode="false" sourceNode="P_12F10008" targetNode="P_6F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_45F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10008_O" deadCode="false" sourceNode="P_12F10008" targetNode="P_7F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_45F10008"/>
	</edges>
	<edges id="P_12F10008P_13F10008" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10008" targetNode="P_13F10008"/>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10008_I" deadCode="false" sourceNode="P_14F10008" targetNode="P_6F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_48F10008"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10008_O" deadCode="false" sourceNode="P_14F10008" targetNode="P_7F10008">
		<representations href="../../../cobol/IABS0090.cbl.cobModel#S_48F10008"/>
	</edges>
	<edges id="P_14F10008P_15F10008" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10008" targetNode="P_15F10008"/>
	<edges id="P_18F10008P_19F10008" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10008" targetNode="P_19F10008"/>
	<edges id="P_16F10008P_17F10008" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10008" targetNode="P_17F10008"/>
	<edges id="P_20F10008P_21F10008" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10008" targetNode="P_21F10008"/>
	<edges xsi:type="cbl:DataEdge" id="S_32F10008_POS1" deadCode="false" targetNode="P_8F10008" sourceNode="DB2_BTC_EXE_MESSAGE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_32F10008"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_40F10008_POS1" deadCode="false" sourceNode="P_10F10008" targetNode="DB2_BTC_EXE_MESSAGE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_40F10008"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_44F10008_POS1" deadCode="false" sourceNode="P_12F10008" targetNode="DB2_BTC_EXE_MESSAGE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_44F10008"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_47F10008_POS1" deadCode="false" sourceNode="P_14F10008" targetNode="DB2_BTC_EXE_MESSAGE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_47F10008"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_52F10008" deadCode="false" name="Dynamic PGM-LCCS0090" sourceNode="P_18F10008" targetNode="LCCS0090">
		<representations href="../../../cobol/../importantStmts.cobModel#S_52F10008"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LLBS0266" cbl:id="LLBS0266" xsi:id="LLBS0266" packageRef="LLBS0266.igd#LLBS0266" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LLBS0266_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10283" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LLBS0266.cbl.cobModel#SC_1F10283"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10283" deadCode="false" name="PROGRAM_LLBS0266_FIRST_SENTENCES">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_1F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10283" deadCode="true" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_2F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10283" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_3F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10283" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_4F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10283" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_5F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10283" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_6F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10283" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_7F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10283" deadCode="false" name="VAL-DCLGEN-B04">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_10F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10283" deadCode="false" name="VAL-DCLGEN-B04-EX">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_11F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10283" deadCode="false" name="SCRIVI-BIL-VAR-DI-CALC-P">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_8F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10283" deadCode="false" name="SCRIVI-BIL-VAR-DI-CALC-P-EX">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_9F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10283" deadCode="false" name="VALORIZZA-AREA-DSH-B04">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_12F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10283" deadCode="false" name="VALORIZZA-AREA-DSH-B04-EX">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_13F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10283" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_16F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10283" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_21F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10283" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_19F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10283" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_20F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10283" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_17F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10283" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_18F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10283" deadCode="true" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_22F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10283" deadCode="true" name="EX-S0290">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_23F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10283" deadCode="false" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_24F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10283" deadCode="false" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_25F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10283" deadCode="false" name="AGGIORNA-TABELLA">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_14F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10283" deadCode="false" name="AGGIORNA-TABELLA-EX">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_15F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10283" deadCode="true" name="ESTR-SEQUENCE">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_26F10283"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10283" deadCode="true" name="ESTR-SEQUENCE-EX">
				<representations href="../../../cobol/LLBS0266.cbl.cobModel#P_27F10283"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0010" name="IDSS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10099"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LLBS0266_LCCS0090" name="Dynamic LLBS0266 LCCS0090" missing="true">
			<representations href="../../../../missing.xmi#IDNWFTTSGVDQ05M1Z3Q4EXIOBPBF24XBX3DMXSTGPIQZLDB2XNBYGD"/>
		</children>
	</packageNode>
	<edges id="SC_1F10283P_1F10283" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10283" targetNode="P_1F10283"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10283_I" deadCode="true" sourceNode="P_1F10283" targetNode="P_2F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_1F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10283_O" deadCode="true" sourceNode="P_1F10283" targetNode="P_3F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_1F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10283_I" deadCode="false" sourceNode="P_1F10283" targetNode="P_4F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_3F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10283_O" deadCode="false" sourceNode="P_1F10283" targetNode="P_5F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_3F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10283_I" deadCode="false" sourceNode="P_1F10283" targetNode="P_6F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_4F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10283_O" deadCode="false" sourceNode="P_1F10283" targetNode="P_7F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_4F10283"/>
	</edges>
	<edges id="P_2F10283P_3F10283" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10283" targetNode="P_3F10283"/>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10283_I" deadCode="false" sourceNode="P_4F10283" targetNode="P_8F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_9F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10283_O" deadCode="false" sourceNode="P_4F10283" targetNode="P_9F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_9F10283"/>
	</edges>
	<edges id="P_4F10283P_5F10283" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10283" targetNode="P_5F10283"/>
	<edges id="P_10F10283P_11F10283" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10283" targetNode="P_11F10283"/>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10283_I" deadCode="false" sourceNode="P_8F10283" targetNode="P_10F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_52F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10283_O" deadCode="false" sourceNode="P_8F10283" targetNode="P_11F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_52F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10283_I" deadCode="false" sourceNode="P_8F10283" targetNode="P_12F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_54F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10283_O" deadCode="false" sourceNode="P_8F10283" targetNode="P_13F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_54F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10283_I" deadCode="false" sourceNode="P_8F10283" targetNode="P_14F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_55F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10283_O" deadCode="false" sourceNode="P_8F10283" targetNode="P_15F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_55F10283"/>
	</edges>
	<edges id="P_8F10283P_9F10283" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10283" targetNode="P_9F10283"/>
	<edges id="P_12F10283P_13F10283" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10283" targetNode="P_13F10283"/>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10283_I" deadCode="false" sourceNode="P_16F10283" targetNode="P_17F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_70F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10283_O" deadCode="false" sourceNode="P_16F10283" targetNode="P_18F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_70F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10283_I" deadCode="false" sourceNode="P_16F10283" targetNode="P_19F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_74F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10283_O" deadCode="false" sourceNode="P_16F10283" targetNode="P_20F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_74F10283"/>
	</edges>
	<edges id="P_16F10283P_21F10283" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10283" targetNode="P_21F10283"/>
	<edges id="P_19F10283P_20F10283" xsi:type="cbl:FallThroughEdge" sourceNode="P_19F10283" targetNode="P_20F10283"/>
	<edges id="P_17F10283P_18F10283" xsi:type="cbl:FallThroughEdge" sourceNode="P_17F10283" targetNode="P_18F10283"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10283_I" deadCode="true" sourceNode="P_22F10283" targetNode="P_16F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_110F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10283_O" deadCode="true" sourceNode="P_22F10283" targetNode="P_21F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_110F10283"/>
	</edges>
	<edges id="P_24F10283P_25F10283" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10283" targetNode="P_25F10283"/>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10283_I" deadCode="false" sourceNode="P_14F10283" targetNode="P_24F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_142F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10283_O" deadCode="false" sourceNode="P_14F10283" targetNode="P_25F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_142F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10283_I" deadCode="false" sourceNode="P_14F10283" targetNode="P_16F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_150F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10283_O" deadCode="false" sourceNode="P_14F10283" targetNode="P_21F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_150F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_155F10283_I" deadCode="false" sourceNode="P_14F10283" targetNode="P_16F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_155F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_155F10283_O" deadCode="false" sourceNode="P_14F10283" targetNode="P_21F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_155F10283"/>
	</edges>
	<edges id="P_14F10283P_15F10283" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10283" targetNode="P_15F10283"/>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10283_I" deadCode="true" sourceNode="P_26F10283" targetNode="P_22F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_162F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10283_O" deadCode="true" sourceNode="P_26F10283" targetNode="P_23F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_162F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10283_I" deadCode="true" sourceNode="P_26F10283" targetNode="P_16F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_170F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10283_O" deadCode="true" sourceNode="P_26F10283" targetNode="P_21F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_170F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10283_I" deadCode="true" sourceNode="P_26F10283" targetNode="P_16F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_175F10283"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_175F10283_O" deadCode="true" sourceNode="P_26F10283" targetNode="P_21F10283">
		<representations href="../../../cobol/LLBS0266.cbl.cobModel#S_175F10283"/>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_68F10283" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_16F10283" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_68F10283"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_138F10283" deadCode="false" name="Dynamic IDSI0011-PGM" sourceNode="P_24F10283" targetNode="IDSS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_138F10283"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_158F10283" deadCode="true" name="Dynamic LCCS0090" sourceNode="P_26F10283" targetNode="Dynamic_LLBS0266_LCCS0090">
		<representations href="../../../cobol/../importantStmts.cobModel#S_158F10283"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS1240" cbl:id="LDBS1240" xsi:id="LDBS1240" packageRef="LDBS1240.igd#LDBS1240" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS1240_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10152" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS1240.cbl.cobModel#SC_1F10152"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10152" deadCode="false" name="PROGRAM_LDBS1240_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_1F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10152" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_2F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10152" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_3F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10152" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_12F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10152" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_13F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10152" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_4F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10152" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_5F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10152" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_6F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10152" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_7F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10152" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_8F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10152" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_9F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10152" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_44F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10152" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_49F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10152" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_14F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10152" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_15F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10152" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_16F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10152" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_17F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10152" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_18F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10152" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_19F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10152" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_20F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10152" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_21F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10152" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_22F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10152" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_23F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10152" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_56F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10152" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_57F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10152" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_24F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10152" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_25F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10152" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_26F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10152" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_27F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10152" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_28F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10152" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_29F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10152" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_30F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10152" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_31F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10152" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_32F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10152" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_33F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10152" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_58F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10152" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_59F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10152" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_34F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10152" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_35F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10152" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_36F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10152" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_37F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10152" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_38F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10152" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_39F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10152" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_40F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10152" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_41F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10152" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_42F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10152" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_43F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10152" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_50F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10152" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_51F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10152" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_60F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10152" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_63F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10152" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_52F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10152" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_53F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10152" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_45F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10152" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_46F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10152" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_47F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10152" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_48F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10152" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_54F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10152" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_55F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10152" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_10F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10152" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_11F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10152" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_66F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10152" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_67F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10152" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_68F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10152" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_69F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10152" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_61F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10152" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_62F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10152" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_70F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10152" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_71F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10152" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_64F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10152" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_65F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10152" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_72F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10152" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_73F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10152" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_74F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10152" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_75F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10152" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_76F10152"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10152" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS1240.cbl.cobModel#P_77F10152"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_RAPP_ANA" name="RAPP_ANA">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_RAPP_ANA"/>
		</children>
	</packageNode>
	<edges id="SC_1F10152P_1F10152" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10152" targetNode="P_1F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10152_I" deadCode="false" sourceNode="P_1F10152" targetNode="P_2F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_2F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10152_O" deadCode="false" sourceNode="P_1F10152" targetNode="P_3F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_2F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10152_I" deadCode="false" sourceNode="P_1F10152" targetNode="P_4F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_6F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10152_O" deadCode="false" sourceNode="P_1F10152" targetNode="P_5F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_6F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10152_I" deadCode="false" sourceNode="P_1F10152" targetNode="P_6F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_10F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10152_O" deadCode="false" sourceNode="P_1F10152" targetNode="P_7F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_10F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10152_I" deadCode="false" sourceNode="P_1F10152" targetNode="P_8F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_14F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10152_O" deadCode="false" sourceNode="P_1F10152" targetNode="P_9F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_14F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10152_I" deadCode="false" sourceNode="P_2F10152" targetNode="P_10F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_24F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10152_O" deadCode="false" sourceNode="P_2F10152" targetNode="P_11F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_24F10152"/>
	</edges>
	<edges id="P_2F10152P_3F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10152" targetNode="P_3F10152"/>
	<edges id="P_12F10152P_13F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10152" targetNode="P_13F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10152_I" deadCode="false" sourceNode="P_4F10152" targetNode="P_14F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_37F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10152_O" deadCode="false" sourceNode="P_4F10152" targetNode="P_15F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_37F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10152_I" deadCode="false" sourceNode="P_4F10152" targetNode="P_16F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_38F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10152_O" deadCode="false" sourceNode="P_4F10152" targetNode="P_17F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_38F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10152_I" deadCode="false" sourceNode="P_4F10152" targetNode="P_18F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_39F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10152_O" deadCode="false" sourceNode="P_4F10152" targetNode="P_19F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_39F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10152_I" deadCode="false" sourceNode="P_4F10152" targetNode="P_20F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_40F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10152_O" deadCode="false" sourceNode="P_4F10152" targetNode="P_21F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_40F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10152_I" deadCode="false" sourceNode="P_4F10152" targetNode="P_22F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_41F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10152_O" deadCode="false" sourceNode="P_4F10152" targetNode="P_23F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_41F10152"/>
	</edges>
	<edges id="P_4F10152P_5F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10152" targetNode="P_5F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10152_I" deadCode="false" sourceNode="P_6F10152" targetNode="P_24F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_45F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10152_O" deadCode="false" sourceNode="P_6F10152" targetNode="P_25F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_45F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10152_I" deadCode="false" sourceNode="P_6F10152" targetNode="P_26F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_46F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10152_O" deadCode="false" sourceNode="P_6F10152" targetNode="P_27F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_46F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10152_I" deadCode="false" sourceNode="P_6F10152" targetNode="P_28F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_47F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10152_O" deadCode="false" sourceNode="P_6F10152" targetNode="P_29F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_47F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10152_I" deadCode="false" sourceNode="P_6F10152" targetNode="P_30F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_48F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10152_O" deadCode="false" sourceNode="P_6F10152" targetNode="P_31F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_48F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10152_I" deadCode="false" sourceNode="P_6F10152" targetNode="P_32F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_49F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10152_O" deadCode="false" sourceNode="P_6F10152" targetNode="P_33F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_49F10152"/>
	</edges>
	<edges id="P_6F10152P_7F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10152" targetNode="P_7F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10152_I" deadCode="false" sourceNode="P_8F10152" targetNode="P_34F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_53F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10152_O" deadCode="false" sourceNode="P_8F10152" targetNode="P_35F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_53F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10152_I" deadCode="false" sourceNode="P_8F10152" targetNode="P_36F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_54F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10152_O" deadCode="false" sourceNode="P_8F10152" targetNode="P_37F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_54F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10152_I" deadCode="false" sourceNode="P_8F10152" targetNode="P_38F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_55F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10152_O" deadCode="false" sourceNode="P_8F10152" targetNode="P_39F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_55F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10152_I" deadCode="false" sourceNode="P_8F10152" targetNode="P_40F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_56F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10152_O" deadCode="false" sourceNode="P_8F10152" targetNode="P_41F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_56F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10152_I" deadCode="false" sourceNode="P_8F10152" targetNode="P_42F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_57F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10152_O" deadCode="false" sourceNode="P_8F10152" targetNode="P_43F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_57F10152"/>
	</edges>
	<edges id="P_8F10152P_9F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10152" targetNode="P_9F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10152_I" deadCode="false" sourceNode="P_44F10152" targetNode="P_45F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_60F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10152_O" deadCode="false" sourceNode="P_44F10152" targetNode="P_46F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_60F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10152_I" deadCode="false" sourceNode="P_44F10152" targetNode="P_47F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_61F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10152_O" deadCode="false" sourceNode="P_44F10152" targetNode="P_48F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_61F10152"/>
	</edges>
	<edges id="P_44F10152P_49F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10152" targetNode="P_49F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10152_I" deadCode="false" sourceNode="P_14F10152" targetNode="P_45F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_65F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10152_O" deadCode="false" sourceNode="P_14F10152" targetNode="P_46F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_65F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10152_I" deadCode="false" sourceNode="P_14F10152" targetNode="P_47F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_66F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10152_O" deadCode="false" sourceNode="P_14F10152" targetNode="P_48F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_66F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10152_I" deadCode="false" sourceNode="P_14F10152" targetNode="P_12F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_68F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10152_O" deadCode="false" sourceNode="P_14F10152" targetNode="P_13F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_68F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10152_I" deadCode="false" sourceNode="P_14F10152" targetNode="P_50F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_70F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10152_O" deadCode="false" sourceNode="P_14F10152" targetNode="P_51F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_70F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10152_I" deadCode="false" sourceNode="P_14F10152" targetNode="P_52F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_71F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10152_O" deadCode="false" sourceNode="P_14F10152" targetNode="P_53F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_71F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10152_I" deadCode="false" sourceNode="P_14F10152" targetNode="P_54F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_72F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10152_O" deadCode="false" sourceNode="P_14F10152" targetNode="P_55F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_72F10152"/>
	</edges>
	<edges id="P_14F10152P_15F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10152" targetNode="P_15F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10152_I" deadCode="false" sourceNode="P_16F10152" targetNode="P_44F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_74F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10152_O" deadCode="false" sourceNode="P_16F10152" targetNode="P_49F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_74F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10152_I" deadCode="false" sourceNode="P_16F10152" targetNode="P_12F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_76F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10152_O" deadCode="false" sourceNode="P_16F10152" targetNode="P_13F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_76F10152"/>
	</edges>
	<edges id="P_16F10152P_17F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10152" targetNode="P_17F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10152_I" deadCode="false" sourceNode="P_18F10152" targetNode="P_12F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_79F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10152_O" deadCode="false" sourceNode="P_18F10152" targetNode="P_13F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_79F10152"/>
	</edges>
	<edges id="P_18F10152P_19F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10152" targetNode="P_19F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10152_I" deadCode="false" sourceNode="P_20F10152" targetNode="P_16F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_81F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10152_O" deadCode="false" sourceNode="P_20F10152" targetNode="P_17F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_81F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10152_I" deadCode="false" sourceNode="P_20F10152" targetNode="P_22F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_83F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10152_O" deadCode="false" sourceNode="P_20F10152" targetNode="P_23F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_83F10152"/>
	</edges>
	<edges id="P_20F10152P_21F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10152" targetNode="P_21F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10152_I" deadCode="false" sourceNode="P_22F10152" targetNode="P_12F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_86F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10152_O" deadCode="false" sourceNode="P_22F10152" targetNode="P_13F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_86F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10152_I" deadCode="false" sourceNode="P_22F10152" targetNode="P_50F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_88F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10152_O" deadCode="false" sourceNode="P_22F10152" targetNode="P_51F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_88F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10152_I" deadCode="false" sourceNode="P_22F10152" targetNode="P_52F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_89F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10152_O" deadCode="false" sourceNode="P_22F10152" targetNode="P_53F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_89F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10152_I" deadCode="false" sourceNode="P_22F10152" targetNode="P_54F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_90F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10152_O" deadCode="false" sourceNode="P_22F10152" targetNode="P_55F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_90F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10152_I" deadCode="false" sourceNode="P_22F10152" targetNode="P_18F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_92F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10152_O" deadCode="false" sourceNode="P_22F10152" targetNode="P_19F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_92F10152"/>
	</edges>
	<edges id="P_22F10152P_23F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10152" targetNode="P_23F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10152_I" deadCode="false" sourceNode="P_56F10152" targetNode="P_45F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_96F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10152_O" deadCode="false" sourceNode="P_56F10152" targetNode="P_46F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_96F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10152_I" deadCode="false" sourceNode="P_56F10152" targetNode="P_47F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_97F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10152_O" deadCode="false" sourceNode="P_56F10152" targetNode="P_48F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_97F10152"/>
	</edges>
	<edges id="P_56F10152P_57F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10152" targetNode="P_57F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10152_I" deadCode="false" sourceNode="P_24F10152" targetNode="P_45F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_101F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10152_O" deadCode="false" sourceNode="P_24F10152" targetNode="P_46F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_101F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10152_I" deadCode="false" sourceNode="P_24F10152" targetNode="P_47F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_102F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10152_O" deadCode="false" sourceNode="P_24F10152" targetNode="P_48F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_102F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10152_I" deadCode="false" sourceNode="P_24F10152" targetNode="P_12F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_104F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10152_O" deadCode="false" sourceNode="P_24F10152" targetNode="P_13F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_104F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10152_I" deadCode="false" sourceNode="P_24F10152" targetNode="P_50F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_106F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10152_O" deadCode="false" sourceNode="P_24F10152" targetNode="P_51F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_106F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10152_I" deadCode="false" sourceNode="P_24F10152" targetNode="P_52F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_107F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10152_O" deadCode="false" sourceNode="P_24F10152" targetNode="P_53F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_107F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10152_I" deadCode="false" sourceNode="P_24F10152" targetNode="P_54F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_108F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10152_O" deadCode="false" sourceNode="P_24F10152" targetNode="P_55F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_108F10152"/>
	</edges>
	<edges id="P_24F10152P_25F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10152" targetNode="P_25F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10152_I" deadCode="false" sourceNode="P_26F10152" targetNode="P_56F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_110F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10152_O" deadCode="false" sourceNode="P_26F10152" targetNode="P_57F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_110F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10152_I" deadCode="false" sourceNode="P_26F10152" targetNode="P_12F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_112F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10152_O" deadCode="false" sourceNode="P_26F10152" targetNode="P_13F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_112F10152"/>
	</edges>
	<edges id="P_26F10152P_27F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10152" targetNode="P_27F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10152_I" deadCode="false" sourceNode="P_28F10152" targetNode="P_12F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_115F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10152_O" deadCode="false" sourceNode="P_28F10152" targetNode="P_13F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_115F10152"/>
	</edges>
	<edges id="P_28F10152P_29F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10152" targetNode="P_29F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10152_I" deadCode="false" sourceNode="P_30F10152" targetNode="P_26F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_117F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10152_O" deadCode="false" sourceNode="P_30F10152" targetNode="P_27F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_117F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10152_I" deadCode="false" sourceNode="P_30F10152" targetNode="P_32F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_119F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10152_O" deadCode="false" sourceNode="P_30F10152" targetNode="P_33F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_119F10152"/>
	</edges>
	<edges id="P_30F10152P_31F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10152" targetNode="P_31F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10152_I" deadCode="false" sourceNode="P_32F10152" targetNode="P_12F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_122F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10152_O" deadCode="false" sourceNode="P_32F10152" targetNode="P_13F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_122F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10152_I" deadCode="false" sourceNode="P_32F10152" targetNode="P_50F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_124F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10152_O" deadCode="false" sourceNode="P_32F10152" targetNode="P_51F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_124F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10152_I" deadCode="false" sourceNode="P_32F10152" targetNode="P_52F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_125F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10152_O" deadCode="false" sourceNode="P_32F10152" targetNode="P_53F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_125F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10152_I" deadCode="false" sourceNode="P_32F10152" targetNode="P_54F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_126F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10152_O" deadCode="false" sourceNode="P_32F10152" targetNode="P_55F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_126F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10152_I" deadCode="false" sourceNode="P_32F10152" targetNode="P_28F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_128F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10152_O" deadCode="false" sourceNode="P_32F10152" targetNode="P_29F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_128F10152"/>
	</edges>
	<edges id="P_32F10152P_33F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10152" targetNode="P_33F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10152_I" deadCode="false" sourceNode="P_58F10152" targetNode="P_45F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_132F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10152_O" deadCode="false" sourceNode="P_58F10152" targetNode="P_46F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_132F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10152_I" deadCode="false" sourceNode="P_58F10152" targetNode="P_47F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_133F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10152_O" deadCode="false" sourceNode="P_58F10152" targetNode="P_48F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_133F10152"/>
	</edges>
	<edges id="P_58F10152P_59F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10152" targetNode="P_59F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10152_I" deadCode="false" sourceNode="P_34F10152" targetNode="P_45F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_136F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10152_O" deadCode="false" sourceNode="P_34F10152" targetNode="P_46F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_136F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10152_I" deadCode="false" sourceNode="P_34F10152" targetNode="P_47F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_137F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10152_O" deadCode="false" sourceNode="P_34F10152" targetNode="P_48F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_137F10152"/>
	</edges>
	<edges id="P_34F10152P_35F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10152" targetNode="P_35F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10152_I" deadCode="false" sourceNode="P_36F10152" targetNode="P_58F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_140F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10152_O" deadCode="false" sourceNode="P_36F10152" targetNode="P_59F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_140F10152"/>
	</edges>
	<edges id="P_36F10152P_37F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10152" targetNode="P_37F10152"/>
	<edges id="P_38F10152P_39F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10152" targetNode="P_39F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10152_I" deadCode="false" sourceNode="P_40F10152" targetNode="P_36F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_145F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10152_O" deadCode="false" sourceNode="P_40F10152" targetNode="P_37F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_145F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10152_I" deadCode="false" sourceNode="P_40F10152" targetNode="P_42F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_147F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10152_O" deadCode="false" sourceNode="P_40F10152" targetNode="P_43F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_147F10152"/>
	</edges>
	<edges id="P_40F10152P_41F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10152" targetNode="P_41F10152"/>
	<edges id="P_42F10152P_43F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10152" targetNode="P_43F10152"/>
	<edges id="P_50F10152P_51F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10152" targetNode="P_51F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_244F10152_I" deadCode="true" sourceNode="P_60F10152" targetNode="P_61F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_244F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_244F10152_O" deadCode="true" sourceNode="P_60F10152" targetNode="P_62F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_244F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_247F10152_I" deadCode="true" sourceNode="P_60F10152" targetNode="P_61F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_247F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_247F10152_O" deadCode="true" sourceNode="P_60F10152" targetNode="P_62F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_247F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_251F10152_I" deadCode="true" sourceNode="P_60F10152" targetNode="P_61F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_251F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_251F10152_O" deadCode="true" sourceNode="P_60F10152" targetNode="P_62F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_251F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10152_I" deadCode="true" sourceNode="P_60F10152" targetNode="P_61F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_255F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10152_O" deadCode="true" sourceNode="P_60F10152" targetNode="P_62F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_255F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10152_I" deadCode="true" sourceNode="P_60F10152" targetNode="P_61F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_259F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10152_O" deadCode="true" sourceNode="P_60F10152" targetNode="P_62F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_259F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_263F10152_I" deadCode="false" sourceNode="P_52F10152" targetNode="P_64F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_263F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_263F10152_O" deadCode="false" sourceNode="P_52F10152" targetNode="P_65F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_263F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_266F10152_I" deadCode="false" sourceNode="P_52F10152" targetNode="P_64F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_266F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_266F10152_O" deadCode="false" sourceNode="P_52F10152" targetNode="P_65F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_266F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_270F10152_I" deadCode="false" sourceNode="P_52F10152" targetNode="P_64F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_270F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_270F10152_O" deadCode="false" sourceNode="P_52F10152" targetNode="P_65F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_270F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_274F10152_I" deadCode="false" sourceNode="P_52F10152" targetNode="P_64F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_274F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_274F10152_O" deadCode="false" sourceNode="P_52F10152" targetNode="P_65F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_274F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10152_I" deadCode="false" sourceNode="P_52F10152" targetNode="P_64F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_278F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10152_O" deadCode="false" sourceNode="P_52F10152" targetNode="P_65F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_278F10152"/>
	</edges>
	<edges id="P_52F10152P_53F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10152" targetNode="P_53F10152"/>
	<edges id="P_45F10152P_46F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10152" targetNode="P_46F10152"/>
	<edges id="P_47F10152P_48F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10152" targetNode="P_48F10152"/>
	<edges id="P_54F10152P_55F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10152" targetNode="P_55F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_290F10152_I" deadCode="false" sourceNode="P_10F10152" targetNode="P_66F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_290F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_290F10152_O" deadCode="false" sourceNode="P_10F10152" targetNode="P_67F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_290F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10152_I" deadCode="false" sourceNode="P_10F10152" targetNode="P_68F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_292F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10152_O" deadCode="false" sourceNode="P_10F10152" targetNode="P_69F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_292F10152"/>
	</edges>
	<edges id="P_10F10152P_11F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10152" targetNode="P_11F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_297F10152_I" deadCode="false" sourceNode="P_66F10152" targetNode="P_61F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_297F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_297F10152_O" deadCode="false" sourceNode="P_66F10152" targetNode="P_62F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_297F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_302F10152_I" deadCode="false" sourceNode="P_66F10152" targetNode="P_61F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_302F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_302F10152_O" deadCode="false" sourceNode="P_66F10152" targetNode="P_62F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_302F10152"/>
	</edges>
	<edges id="P_66F10152P_67F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10152" targetNode="P_67F10152"/>
	<edges id="P_68F10152P_69F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10152" targetNode="P_69F10152"/>
	<edges id="P_61F10152P_62F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10152" targetNode="P_62F10152"/>
	<edges xsi:type="cbl:PerformEdge" id="S_331F10152_I" deadCode="false" sourceNode="P_64F10152" targetNode="P_72F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_331F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_331F10152_O" deadCode="false" sourceNode="P_64F10152" targetNode="P_73F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_331F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_332F10152_I" deadCode="false" sourceNode="P_64F10152" targetNode="P_74F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_332F10152"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_332F10152_O" deadCode="false" sourceNode="P_64F10152" targetNode="P_75F10152">
		<representations href="../../../cobol/LDBS1240.cbl.cobModel#S_332F10152"/>
	</edges>
	<edges id="P_64F10152P_65F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10152" targetNode="P_65F10152"/>
	<edges id="P_72F10152P_73F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10152" targetNode="P_73F10152"/>
	<edges id="P_74F10152P_75F10152" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10152" targetNode="P_75F10152"/>
	<edges xsi:type="cbl:DataEdge" id="S_67F10152_POS1" deadCode="false" targetNode="P_14F10152" sourceNode="DB2_RAPP_ANA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10152"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_75F10152_POS1" deadCode="false" targetNode="P_16F10152" sourceNode="DB2_RAPP_ANA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_75F10152"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_78F10152_POS1" deadCode="false" targetNode="P_18F10152" sourceNode="DB2_RAPP_ANA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10152"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_85F10152_POS1" deadCode="false" targetNode="P_22F10152" sourceNode="DB2_RAPP_ANA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_85F10152"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_103F10152_POS1" deadCode="false" targetNode="P_24F10152" sourceNode="DB2_RAPP_ANA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_103F10152"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10152_POS1" deadCode="false" targetNode="P_26F10152" sourceNode="DB2_RAPP_ANA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10152"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_114F10152_POS1" deadCode="false" targetNode="P_28F10152" sourceNode="DB2_RAPP_ANA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10152"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_121F10152_POS1" deadCode="false" targetNode="P_32F10152" sourceNode="DB2_RAPP_ANA">
		<representations href="../../../cobol/../importantStmts.cobModel#S_121F10152"></representations>
	</edges>
</Package>

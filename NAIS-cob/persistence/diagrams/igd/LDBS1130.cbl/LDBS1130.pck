<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS1130" cbl:id="LDBS1130" xsi:id="LDBS1130" packageRef="LDBS1130.igd#LDBS1130" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS1130_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10151" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS1130.cbl.cobModel#SC_1F10151"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10151" deadCode="false" name="PROGRAM_LDBS1130_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_1F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10151" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_2F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10151" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_3F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10151" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_8F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10151" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_9F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10151" deadCode="false" name="A200-ELABORA-WC">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_4F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10151" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_5F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10151" deadCode="false" name="A245-SELECT-WC">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_10F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10151" deadCode="false" name="A245-EX">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_11F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10151" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_12F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10151" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_13F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10151" deadCode="true" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_16F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10151" deadCode="true" name="Z200-EX">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_17F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10151" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_18F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10151" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_21F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10151" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_14F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10151" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_15F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10151" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_6F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10151" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_7F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10151" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_24F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10151" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_25F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10151" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_26F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10151" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_27F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10151" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_19F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10151" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_20F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10151" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_28F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10151" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_29F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10151" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_22F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10151" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_23F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10151" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_30F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10151" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_31F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10151" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_32F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10151" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_33F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10151" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_34F10151"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10151" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS1130.cbl.cobModel#P_35F10151"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_PARAM_OGG" name="PARAM_OGG">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_PARAM_OGG"/>
		</children>
	</packageNode>
	<edges id="SC_1F10151P_1F10151" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10151" targetNode="P_1F10151"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10151_I" deadCode="false" sourceNode="P_1F10151" targetNode="P_2F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_2F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10151_O" deadCode="false" sourceNode="P_1F10151" targetNode="P_3F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_2F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10151_I" deadCode="false" sourceNode="P_1F10151" targetNode="P_4F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_6F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10151_O" deadCode="false" sourceNode="P_1F10151" targetNode="P_5F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_6F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10151_I" deadCode="false" sourceNode="P_2F10151" targetNode="P_6F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_16F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10151_O" deadCode="false" sourceNode="P_2F10151" targetNode="P_7F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_16F10151"/>
	</edges>
	<edges id="P_2F10151P_3F10151" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10151" targetNode="P_3F10151"/>
	<edges id="P_8F10151P_9F10151" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10151" targetNode="P_9F10151"/>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10151_I" deadCode="false" sourceNode="P_4F10151" targetNode="P_10F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_31F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10151_O" deadCode="false" sourceNode="P_4F10151" targetNode="P_11F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_31F10151"/>
	</edges>
	<edges id="P_4F10151P_5F10151" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10151" targetNode="P_5F10151"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10151_I" deadCode="false" sourceNode="P_10F10151" targetNode="P_8F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_35F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10151_O" deadCode="false" sourceNode="P_10F10151" targetNode="P_9F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_35F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10151_I" deadCode="false" sourceNode="P_10F10151" targetNode="P_12F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_37F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10151_O" deadCode="false" sourceNode="P_10F10151" targetNode="P_13F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_37F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10151_I" deadCode="false" sourceNode="P_10F10151" targetNode="P_14F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_38F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10151_O" deadCode="false" sourceNode="P_10F10151" targetNode="P_15F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_38F10151"/>
	</edges>
	<edges id="P_10F10151P_11F10151" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10151" targetNode="P_11F10151"/>
	<edges id="P_12F10151P_13F10151" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10151" targetNode="P_13F10151"/>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10151_I" deadCode="true" sourceNode="P_18F10151" targetNode="P_19F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_70F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10151_O" deadCode="true" sourceNode="P_18F10151" targetNode="P_20F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_70F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10151_I" deadCode="true" sourceNode="P_18F10151" targetNode="P_19F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_76F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10151_O" deadCode="true" sourceNode="P_18F10151" targetNode="P_20F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_76F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10151_I" deadCode="true" sourceNode="P_18F10151" targetNode="P_19F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_83F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10151_O" deadCode="true" sourceNode="P_18F10151" targetNode="P_20F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_83F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10151_I" deadCode="false" sourceNode="P_14F10151" targetNode="P_22F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_89F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10151_O" deadCode="false" sourceNode="P_14F10151" targetNode="P_23F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_89F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10151_I" deadCode="false" sourceNode="P_14F10151" targetNode="P_22F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_92F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10151_O" deadCode="false" sourceNode="P_14F10151" targetNode="P_23F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_92F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10151_I" deadCode="false" sourceNode="P_14F10151" targetNode="P_22F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_97F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10151_O" deadCode="false" sourceNode="P_14F10151" targetNode="P_23F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_97F10151"/>
	</edges>
	<edges id="P_14F10151P_15F10151" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10151" targetNode="P_15F10151"/>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10151_I" deadCode="false" sourceNode="P_6F10151" targetNode="P_24F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_100F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10151_O" deadCode="false" sourceNode="P_6F10151" targetNode="P_25F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_100F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10151_I" deadCode="false" sourceNode="P_6F10151" targetNode="P_26F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_102F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10151_O" deadCode="false" sourceNode="P_6F10151" targetNode="P_27F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_102F10151"/>
	</edges>
	<edges id="P_6F10151P_7F10151" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10151" targetNode="P_7F10151"/>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10151_I" deadCode="false" sourceNode="P_24F10151" targetNode="P_19F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_107F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10151_O" deadCode="false" sourceNode="P_24F10151" targetNode="P_20F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_107F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10151_I" deadCode="false" sourceNode="P_24F10151" targetNode="P_19F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_112F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10151_O" deadCode="false" sourceNode="P_24F10151" targetNode="P_20F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_112F10151"/>
	</edges>
	<edges id="P_24F10151P_25F10151" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10151" targetNode="P_25F10151"/>
	<edges id="P_26F10151P_27F10151" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10151" targetNode="P_27F10151"/>
	<edges id="P_19F10151P_20F10151" xsi:type="cbl:FallThroughEdge" sourceNode="P_19F10151" targetNode="P_20F10151"/>
	<edges xsi:type="cbl:PerformEdge" id="S_141F10151_I" deadCode="false" sourceNode="P_22F10151" targetNode="P_30F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_141F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_141F10151_O" deadCode="false" sourceNode="P_22F10151" targetNode="P_31F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_141F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10151_I" deadCode="false" sourceNode="P_22F10151" targetNode="P_32F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_142F10151"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10151_O" deadCode="false" sourceNode="P_22F10151" targetNode="P_33F10151">
		<representations href="../../../cobol/LDBS1130.cbl.cobModel#S_142F10151"/>
	</edges>
	<edges id="P_22F10151P_23F10151" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10151" targetNode="P_23F10151"/>
	<edges id="P_30F10151P_31F10151" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10151" targetNode="P_31F10151"/>
	<edges id="P_32F10151P_33F10151" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10151" targetNode="P_33F10151"/>
	<edges xsi:type="cbl:DataEdge" id="S_34F10151_POS1" deadCode="false" targetNode="P_10F10151" sourceNode="DB2_PARAM_OGG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_34F10151"></representations>
	</edges>
</Package>

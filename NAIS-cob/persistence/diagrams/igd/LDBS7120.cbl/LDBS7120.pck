<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS7120" cbl:id="LDBS7120" xsi:id="LDBS7120" packageRef="LDBS7120.igd#LDBS7120" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS7120_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10236" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS7120.cbl.cobModel#SC_1F10236"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10236" deadCode="false" name="PROGRAM_LDBS7120_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_1F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10236" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_2F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10236" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_3F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10236" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_12F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10236" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_13F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10236" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_4F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10236" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_5F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10236" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_6F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10236" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_7F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10236" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_8F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10236" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_9F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10236" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_44F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10236" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_49F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10236" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_14F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10236" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_15F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10236" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_16F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10236" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_17F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10236" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_18F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10236" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_19F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10236" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_20F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10236" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_21F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10236" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_22F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10236" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_23F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10236" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_56F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10236" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_57F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10236" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_24F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10236" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_25F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10236" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_26F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10236" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_27F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10236" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_28F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10236" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_29F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10236" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_30F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10236" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_31F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10236" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_32F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10236" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_33F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10236" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_58F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10236" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_59F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10236" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_34F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10236" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_35F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10236" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_36F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10236" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_37F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10236" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_38F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10236" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_39F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10236" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_40F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10236" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_41F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10236" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_42F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10236" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_43F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10236" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_50F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10236" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_51F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10236" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_60F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10236" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_63F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10236" deadCode="true" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_52F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10236" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_53F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10236" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_45F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10236" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_46F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10236" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_47F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10236" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_48F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10236" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_54F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10236" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_55F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10236" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_10F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10236" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_11F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10236" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_66F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10236" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_67F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10236" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_68F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10236" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_69F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10236" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_61F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10236" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_62F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10236" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_70F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10236" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_71F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10236" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_64F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10236" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_65F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10236" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_72F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10236" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_73F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10236" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_74F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10236" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_75F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10236" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_76F10236"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10236" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS7120.cbl.cobModel#P_77F10236"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_EST_TRCH_DI_GAR" name="EST_TRCH_DI_GAR">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_EST_TRCH_DI_GAR"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_STAT_OGG_BUS" name="STAT_OGG_BUS">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_STAT_OGG_BUS"/>
		</children>
	</packageNode>
	<edges id="SC_1F10236P_1F10236" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10236" targetNode="P_1F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10236_I" deadCode="false" sourceNode="P_1F10236" targetNode="P_2F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_1F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10236_O" deadCode="false" sourceNode="P_1F10236" targetNode="P_3F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_1F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10236_I" deadCode="false" sourceNode="P_1F10236" targetNode="P_4F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_5F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10236_O" deadCode="false" sourceNode="P_1F10236" targetNode="P_5F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_5F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10236_I" deadCode="false" sourceNode="P_1F10236" targetNode="P_6F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_9F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10236_O" deadCode="false" sourceNode="P_1F10236" targetNode="P_7F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_9F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10236_I" deadCode="false" sourceNode="P_1F10236" targetNode="P_8F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_13F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10236_O" deadCode="false" sourceNode="P_1F10236" targetNode="P_9F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_13F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10236_I" deadCode="false" sourceNode="P_2F10236" targetNode="P_10F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_22F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10236_O" deadCode="false" sourceNode="P_2F10236" targetNode="P_11F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_22F10236"/>
	</edges>
	<edges id="P_2F10236P_3F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10236" targetNode="P_3F10236"/>
	<edges id="P_12F10236P_13F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10236" targetNode="P_13F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10236_I" deadCode="false" sourceNode="P_4F10236" targetNode="P_14F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_35F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10236_O" deadCode="false" sourceNode="P_4F10236" targetNode="P_15F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_35F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10236_I" deadCode="false" sourceNode="P_4F10236" targetNode="P_16F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_36F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10236_O" deadCode="false" sourceNode="P_4F10236" targetNode="P_17F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_36F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10236_I" deadCode="false" sourceNode="P_4F10236" targetNode="P_18F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_37F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10236_O" deadCode="false" sourceNode="P_4F10236" targetNode="P_19F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_37F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10236_I" deadCode="false" sourceNode="P_4F10236" targetNode="P_20F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_38F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10236_O" deadCode="false" sourceNode="P_4F10236" targetNode="P_21F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_38F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10236_I" deadCode="false" sourceNode="P_4F10236" targetNode="P_22F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_39F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10236_O" deadCode="false" sourceNode="P_4F10236" targetNode="P_23F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_39F10236"/>
	</edges>
	<edges id="P_4F10236P_5F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10236" targetNode="P_5F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10236_I" deadCode="false" sourceNode="P_6F10236" targetNode="P_24F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_43F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10236_O" deadCode="false" sourceNode="P_6F10236" targetNode="P_25F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_43F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10236_I" deadCode="false" sourceNode="P_6F10236" targetNode="P_26F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_44F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10236_O" deadCode="false" sourceNode="P_6F10236" targetNode="P_27F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_44F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10236_I" deadCode="false" sourceNode="P_6F10236" targetNode="P_28F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_45F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10236_O" deadCode="false" sourceNode="P_6F10236" targetNode="P_29F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_45F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10236_I" deadCode="false" sourceNode="P_6F10236" targetNode="P_30F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_46F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10236_O" deadCode="false" sourceNode="P_6F10236" targetNode="P_31F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_46F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10236_I" deadCode="false" sourceNode="P_6F10236" targetNode="P_32F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_47F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10236_O" deadCode="false" sourceNode="P_6F10236" targetNode="P_33F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_47F10236"/>
	</edges>
	<edges id="P_6F10236P_7F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10236" targetNode="P_7F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10236_I" deadCode="false" sourceNode="P_8F10236" targetNode="P_34F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_51F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10236_O" deadCode="false" sourceNode="P_8F10236" targetNode="P_35F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_51F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10236_I" deadCode="false" sourceNode="P_8F10236" targetNode="P_36F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_52F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10236_O" deadCode="false" sourceNode="P_8F10236" targetNode="P_37F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_52F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10236_I" deadCode="false" sourceNode="P_8F10236" targetNode="P_38F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_53F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10236_O" deadCode="false" sourceNode="P_8F10236" targetNode="P_39F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_53F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10236_I" deadCode="false" sourceNode="P_8F10236" targetNode="P_40F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_54F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10236_O" deadCode="false" sourceNode="P_8F10236" targetNode="P_41F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_54F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10236_I" deadCode="false" sourceNode="P_8F10236" targetNode="P_42F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_55F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10236_O" deadCode="false" sourceNode="P_8F10236" targetNode="P_43F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_55F10236"/>
	</edges>
	<edges id="P_8F10236P_9F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10236" targetNode="P_9F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10236_I" deadCode="false" sourceNode="P_44F10236" targetNode="P_45F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_58F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10236_O" deadCode="false" sourceNode="P_44F10236" targetNode="P_46F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_58F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10236_I" deadCode="false" sourceNode="P_44F10236" targetNode="P_47F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_59F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10236_O" deadCode="false" sourceNode="P_44F10236" targetNode="P_48F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_59F10236"/>
	</edges>
	<edges id="P_44F10236P_49F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10236" targetNode="P_49F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10236_I" deadCode="false" sourceNode="P_14F10236" targetNode="P_45F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_62F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10236_O" deadCode="false" sourceNode="P_14F10236" targetNode="P_46F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_62F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10236_I" deadCode="false" sourceNode="P_14F10236" targetNode="P_47F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_63F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10236_O" deadCode="false" sourceNode="P_14F10236" targetNode="P_48F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_63F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10236_I" deadCode="false" sourceNode="P_14F10236" targetNode="P_12F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_65F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10236_O" deadCode="false" sourceNode="P_14F10236" targetNode="P_13F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_65F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10236_I" deadCode="false" sourceNode="P_14F10236" targetNode="P_50F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_67F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10236_O" deadCode="false" sourceNode="P_14F10236" targetNode="P_51F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_67F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10236_I" deadCode="true" sourceNode="P_14F10236" targetNode="P_52F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_68F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10236_O" deadCode="true" sourceNode="P_14F10236" targetNode="P_53F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_68F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10236_I" deadCode="false" sourceNode="P_14F10236" targetNode="P_54F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_69F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10236_O" deadCode="false" sourceNode="P_14F10236" targetNode="P_55F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_69F10236"/>
	</edges>
	<edges id="P_14F10236P_15F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10236" targetNode="P_15F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10236_I" deadCode="false" sourceNode="P_16F10236" targetNode="P_44F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_71F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10236_O" deadCode="false" sourceNode="P_16F10236" targetNode="P_49F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_71F10236"/>
	</edges>
	<edges id="P_16F10236P_17F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10236" targetNode="P_17F10236"/>
	<edges id="P_18F10236P_19F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10236" targetNode="P_19F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10236_I" deadCode="false" sourceNode="P_20F10236" targetNode="P_16F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_76F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10236_O" deadCode="false" sourceNode="P_20F10236" targetNode="P_17F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_76F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10236_I" deadCode="false" sourceNode="P_20F10236" targetNode="P_22F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_78F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10236_O" deadCode="false" sourceNode="P_20F10236" targetNode="P_23F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_78F10236"/>
	</edges>
	<edges id="P_20F10236P_21F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10236" targetNode="P_21F10236"/>
	<edges id="P_22F10236P_23F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10236" targetNode="P_23F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10236_I" deadCode="false" sourceNode="P_56F10236" targetNode="P_45F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_82F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10236_O" deadCode="false" sourceNode="P_56F10236" targetNode="P_46F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_82F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10236_I" deadCode="false" sourceNode="P_56F10236" targetNode="P_47F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_83F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10236_O" deadCode="false" sourceNode="P_56F10236" targetNode="P_48F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_83F10236"/>
	</edges>
	<edges id="P_56F10236P_57F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10236" targetNode="P_57F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10236_I" deadCode="false" sourceNode="P_24F10236" targetNode="P_45F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_86F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10236_O" deadCode="false" sourceNode="P_24F10236" targetNode="P_46F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_86F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10236_I" deadCode="false" sourceNode="P_24F10236" targetNode="P_47F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_87F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10236_O" deadCode="false" sourceNode="P_24F10236" targetNode="P_48F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_87F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10236_I" deadCode="false" sourceNode="P_24F10236" targetNode="P_12F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_89F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10236_O" deadCode="false" sourceNode="P_24F10236" targetNode="P_13F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_89F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10236_I" deadCode="false" sourceNode="P_24F10236" targetNode="P_50F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_91F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10236_O" deadCode="false" sourceNode="P_24F10236" targetNode="P_51F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_91F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10236_I" deadCode="true" sourceNode="P_24F10236" targetNode="P_52F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_92F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10236_O" deadCode="true" sourceNode="P_24F10236" targetNode="P_53F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_92F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10236_I" deadCode="false" sourceNode="P_24F10236" targetNode="P_54F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_93F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10236_O" deadCode="false" sourceNode="P_24F10236" targetNode="P_55F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_93F10236"/>
	</edges>
	<edges id="P_24F10236P_25F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10236" targetNode="P_25F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10236_I" deadCode="false" sourceNode="P_26F10236" targetNode="P_56F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_95F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10236_O" deadCode="false" sourceNode="P_26F10236" targetNode="P_57F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_95F10236"/>
	</edges>
	<edges id="P_26F10236P_27F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10236" targetNode="P_27F10236"/>
	<edges id="P_28F10236P_29F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10236" targetNode="P_29F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10236_I" deadCode="false" sourceNode="P_30F10236" targetNode="P_26F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_100F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10236_O" deadCode="false" sourceNode="P_30F10236" targetNode="P_27F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_100F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10236_I" deadCode="false" sourceNode="P_30F10236" targetNode="P_32F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_102F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10236_O" deadCode="false" sourceNode="P_30F10236" targetNode="P_33F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_102F10236"/>
	</edges>
	<edges id="P_30F10236P_31F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10236" targetNode="P_31F10236"/>
	<edges id="P_32F10236P_33F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10236" targetNode="P_33F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10236_I" deadCode="false" sourceNode="P_58F10236" targetNode="P_45F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_106F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10236_O" deadCode="false" sourceNode="P_58F10236" targetNode="P_46F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_106F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10236_I" deadCode="false" sourceNode="P_58F10236" targetNode="P_47F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_107F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10236_O" deadCode="false" sourceNode="P_58F10236" targetNode="P_48F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_107F10236"/>
	</edges>
	<edges id="P_58F10236P_59F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10236" targetNode="P_59F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10236_I" deadCode="false" sourceNode="P_34F10236" targetNode="P_45F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_110F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10236_O" deadCode="false" sourceNode="P_34F10236" targetNode="P_46F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_110F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10236_I" deadCode="false" sourceNode="P_34F10236" targetNode="P_47F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_111F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_111F10236_O" deadCode="false" sourceNode="P_34F10236" targetNode="P_48F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_111F10236"/>
	</edges>
	<edges id="P_34F10236P_35F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10236" targetNode="P_35F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10236_I" deadCode="false" sourceNode="P_36F10236" targetNode="P_58F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_114F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10236_O" deadCode="false" sourceNode="P_36F10236" targetNode="P_59F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_114F10236"/>
	</edges>
	<edges id="P_36F10236P_37F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10236" targetNode="P_37F10236"/>
	<edges id="P_38F10236P_39F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10236" targetNode="P_39F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10236_I" deadCode="false" sourceNode="P_40F10236" targetNode="P_36F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_119F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10236_O" deadCode="false" sourceNode="P_40F10236" targetNode="P_37F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_119F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10236_I" deadCode="false" sourceNode="P_40F10236" targetNode="P_42F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_121F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10236_O" deadCode="false" sourceNode="P_40F10236" targetNode="P_43F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_121F10236"/>
	</edges>
	<edges id="P_40F10236P_41F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10236" targetNode="P_41F10236"/>
	<edges id="P_42F10236P_43F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10236" targetNode="P_43F10236"/>
	<edges id="P_50F10236P_51F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10236" targetNode="P_51F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10236_I" deadCode="true" sourceNode="P_60F10236" targetNode="P_61F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_140F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10236_O" deadCode="true" sourceNode="P_60F10236" targetNode="P_62F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_140F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10236_I" deadCode="true" sourceNode="P_60F10236" targetNode="P_61F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_143F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10236_O" deadCode="true" sourceNode="P_60F10236" targetNode="P_62F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_143F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10236_I" deadCode="true" sourceNode="P_60F10236" targetNode="P_61F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_147F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10236_O" deadCode="true" sourceNode="P_60F10236" targetNode="P_62F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_147F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10236_I" deadCode="true" sourceNode="P_60F10236" targetNode="P_61F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_150F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_150F10236_O" deadCode="true" sourceNode="P_60F10236" targetNode="P_62F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_150F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10236_I" deadCode="true" sourceNode="P_60F10236" targetNode="P_61F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_153F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10236_O" deadCode="true" sourceNode="P_60F10236" targetNode="P_62F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_153F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10236_I" deadCode="true" sourceNode="P_52F10236" targetNode="P_64F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_157F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_157F10236_O" deadCode="true" sourceNode="P_52F10236" targetNode="P_65F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_157F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10236_I" deadCode="true" sourceNode="P_52F10236" targetNode="P_64F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_160F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10236_O" deadCode="true" sourceNode="P_52F10236" targetNode="P_65F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_160F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10236_I" deadCode="true" sourceNode="P_52F10236" targetNode="P_64F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_164F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10236_O" deadCode="true" sourceNode="P_52F10236" targetNode="P_65F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_164F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10236_I" deadCode="true" sourceNode="P_52F10236" targetNode="P_64F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_167F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10236_O" deadCode="true" sourceNode="P_52F10236" targetNode="P_65F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_167F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10236_I" deadCode="true" sourceNode="P_52F10236" targetNode="P_64F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_170F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10236_O" deadCode="true" sourceNode="P_52F10236" targetNode="P_65F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_170F10236"/>
	</edges>
	<edges id="P_52F10236P_53F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10236" targetNode="P_53F10236"/>
	<edges id="P_45F10236P_46F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10236" targetNode="P_46F10236"/>
	<edges id="P_47F10236P_48F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10236" targetNode="P_48F10236"/>
	<edges id="P_54F10236P_55F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10236" targetNode="P_55F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10236_I" deadCode="false" sourceNode="P_10F10236" targetNode="P_66F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_179F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10236_O" deadCode="false" sourceNode="P_10F10236" targetNode="P_67F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_179F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10236_I" deadCode="false" sourceNode="P_10F10236" targetNode="P_68F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_181F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10236_O" deadCode="false" sourceNode="P_10F10236" targetNode="P_69F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_181F10236"/>
	</edges>
	<edges id="P_10F10236P_11F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10236" targetNode="P_11F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10236_I" deadCode="false" sourceNode="P_66F10236" targetNode="P_61F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_186F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10236_O" deadCode="false" sourceNode="P_66F10236" targetNode="P_62F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_186F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10236_I" deadCode="false" sourceNode="P_66F10236" targetNode="P_61F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_191F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10236_O" deadCode="false" sourceNode="P_66F10236" targetNode="P_62F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_191F10236"/>
	</edges>
	<edges id="P_66F10236P_67F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10236" targetNode="P_67F10236"/>
	<edges id="P_68F10236P_69F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10236" targetNode="P_69F10236"/>
	<edges id="P_61F10236P_62F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10236" targetNode="P_62F10236"/>
	<edges xsi:type="cbl:PerformEdge" id="S_220F10236_I" deadCode="true" sourceNode="P_64F10236" targetNode="P_72F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_220F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_220F10236_O" deadCode="true" sourceNode="P_64F10236" targetNode="P_73F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_220F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10236_I" deadCode="true" sourceNode="P_64F10236" targetNode="P_74F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_221F10236"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10236_O" deadCode="true" sourceNode="P_64F10236" targetNode="P_75F10236">
		<representations href="../../../cobol/LDBS7120.cbl.cobModel#S_221F10236"/>
	</edges>
	<edges id="P_64F10236P_65F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10236" targetNode="P_65F10236"/>
	<edges id="P_72F10236P_73F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10236" targetNode="P_73F10236"/>
	<edges id="P_74F10236P_75F10236" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10236" targetNode="P_75F10236"/>
	<edges xsi:type="cbl:DataEdge" id="S_64F10236_POS1" deadCode="false" targetNode="P_14F10236" sourceNode="DB2_EST_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_64F10236"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_64F10236_POS2" deadCode="false" targetNode="P_14F10236" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_64F10236"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_88F10236_POS1" deadCode="false" targetNode="P_24F10236" sourceNode="DB2_EST_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_88F10236"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_88F10236_POS2" deadCode="false" targetNode="P_24F10236" sourceNode="DB2_STAT_OGG_BUS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_88F10236"></representations>
	</edges>
</Package>

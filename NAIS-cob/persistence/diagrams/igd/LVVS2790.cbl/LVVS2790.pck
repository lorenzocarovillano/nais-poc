<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS2790" cbl:id="LVVS2790" xsi:id="LVVS2790" packageRef="LVVS2790.igd#LVVS2790" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS2790_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10378" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS2790.cbl.cobModel#SC_1F10378"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10378" deadCode="false" name="PROGRAM_LVVS2790_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_1F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10378" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_2F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10378" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_3F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10378" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_4F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10378" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_5F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10378" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_8F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10378" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_9F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10378" deadCode="false" name="S1110-LETTURA-LIQ">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_12F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10378" deadCode="false" name="S1110-EX">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_13F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10378" deadCode="false" name="S1111-LETTURA-TRANCHE-LIQ">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_14F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10378" deadCode="false" name="S1111-EX">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_15F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10378" deadCode="true" name="RECUPERA-TRCH-DI-GAR">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_20F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10378" deadCode="true" name="RECUPERA-TRCH-DI-GAR-EX">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_23F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10378" deadCode="true" name="S1120-VERIFICA-INDEX">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_21F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10378" deadCode="true" name="S1120-EX">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_22F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10378" deadCode="false" name="S1150-CALCOLO-ANNO">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_16F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10378" deadCode="false" name="S1150-CALCOLO-ANNO-EX">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_17F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10378" deadCode="false" name="S1200-CALCOLO-IMPORTO">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_18F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10378" deadCode="false" name="S1200-CALCOLO-IMPORTO-EX">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_19F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10378" deadCode="false" name="VERIFICA-MOVIMENTO">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_10F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10378" deadCode="false" name="VERIFICA-MOVIMENTO-EX">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_11F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10378" deadCode="false" name="RECUP-MOVI-COMUN">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_24F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10378" deadCode="false" name="RECUP-MOVI-COMUN-EX">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_25F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10378" deadCode="false" name="CLOSE-MOVI">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_26F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10378" deadCode="false" name="CLOSE-MOVI-EX">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_27F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10378" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_6F10378"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10378" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS2790.cbl.cobModel#P_7F10378"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSLQU0" name="IDBSLQU0">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10051"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LVVS2790_IDBSTGA0" name="Dynamic LVVS2790 IDBSTGA0" missing="true">
			<representations href="../../../../missing.xmi#IDDFETWXFBFLZ4HW0AAPTUCLERILGNFNMSOLFBG2MD3KV2MKWS04WG"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LVVS2790_IDBSGRZ0" name="Dynamic LVVS2790 IDBSGRZ0" missing="true">
			<representations href="../../../../missing.xmi#IDGS5FOSW15U5EDMUBZRYM3A3VWIRQCDDBRZZCGDMCBINQUAAUYO4J"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS6040" name="LDBS6040">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10230"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10378P_1F10378" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10378" targetNode="P_1F10378"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10378_I" deadCode="false" sourceNode="P_1F10378" targetNode="P_2F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_1F10378"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10378_O" deadCode="false" sourceNode="P_1F10378" targetNode="P_3F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_1F10378"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10378_I" deadCode="false" sourceNode="P_1F10378" targetNode="P_4F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_2F10378"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10378_O" deadCode="false" sourceNode="P_1F10378" targetNode="P_5F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_2F10378"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10378_I" deadCode="false" sourceNode="P_1F10378" targetNode="P_6F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_3F10378"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10378_O" deadCode="false" sourceNode="P_1F10378" targetNode="P_7F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_3F10378"/>
	</edges>
	<edges id="P_2F10378P_3F10378" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10378" targetNode="P_3F10378"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10378_I" deadCode="false" sourceNode="P_4F10378" targetNode="P_8F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_11F10378"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10378_O" deadCode="false" sourceNode="P_4F10378" targetNode="P_9F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_11F10378"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10378_I" deadCode="false" sourceNode="P_4F10378" targetNode="P_10F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_12F10378"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10378_O" deadCode="false" sourceNode="P_4F10378" targetNode="P_11F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_12F10378"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10378_I" deadCode="false" sourceNode="P_4F10378" targetNode="P_12F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_14F10378"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10378_O" deadCode="false" sourceNode="P_4F10378" targetNode="P_13F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_14F10378"/>
	</edges>
	<edges id="P_4F10378P_5F10378" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10378" targetNode="P_5F10378"/>
	<edges id="P_8F10378P_9F10378" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10378" targetNode="P_9F10378"/>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10378_I" deadCode="false" sourceNode="P_12F10378" targetNode="P_14F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_29F10378"/>
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_42F10378"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10378_O" deadCode="false" sourceNode="P_12F10378" targetNode="P_15F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_29F10378"/>
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_42F10378"/>
	</edges>
	<edges id="P_12F10378P_13F10378" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10378" targetNode="P_13F10378"/>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10378_I" deadCode="false" sourceNode="P_14F10378" targetNode="P_16F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_54F10378"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10378_O" deadCode="false" sourceNode="P_14F10378" targetNode="P_17F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_54F10378"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10378_I" deadCode="false" sourceNode="P_14F10378" targetNode="P_18F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_56F10378"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10378_O" deadCode="false" sourceNode="P_14F10378" targetNode="P_19F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_56F10378"/>
	</edges>
	<edges id="P_14F10378P_15F10378" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10378" targetNode="P_15F10378"/>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10378_I" deadCode="true" sourceNode="P_20F10378" targetNode="P_21F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_72F10378"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10378_O" deadCode="true" sourceNode="P_20F10378" targetNode="P_22F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_72F10378"/>
	</edges>
	<edges id="P_16F10378P_17F10378" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10378" targetNode="P_17F10378"/>
	<edges id="P_18F10378P_19F10378" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10378" targetNode="P_19F10378"/>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10378_I" deadCode="false" sourceNode="P_10F10378" targetNode="P_24F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_112F10378"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10378_O" deadCode="false" sourceNode="P_10F10378" targetNode="P_25F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_112F10378"/>
	</edges>
	<edges id="P_10F10378P_11F10378" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10378" targetNode="P_11F10378"/>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10378_I" deadCode="false" sourceNode="P_24F10378" targetNode="P_26F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_126F10378"/>
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_148F10378"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10378_O" deadCode="false" sourceNode="P_24F10378" targetNode="P_27F10378">
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_126F10378"/>
		<representations href="../../../cobol/LVVS2790.cbl.cobModel#S_148F10378"/>
	</edges>
	<edges id="P_24F10378P_25F10378" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10378" targetNode="P_25F10378"/>
	<edges id="P_26F10378P_27F10378" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10378" targetNode="P_27F10378"/>
	<edges xsi:type="cbl:CallEdge" id="S_30F10378" deadCode="false" name="Dynamic IDBSLQU0" sourceNode="P_12F10378" targetNode="IDBSLQU0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_30F10378"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_66F10378" deadCode="true" name="Dynamic IDBSTGA0" sourceNode="P_20F10378" targetNode="Dynamic_LVVS2790_IDBSTGA0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_66F10378"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_85F10378" deadCode="true" name="Dynamic IDBSGRZ0" sourceNode="P_21F10378" targetNode="Dynamic_LVVS2790_IDBSGRZ0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_85F10378"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_134F10378" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_24F10378" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_134F10378"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_160F10378" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_26F10378" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_160F10378"></representations>
	</edges>
</Package>

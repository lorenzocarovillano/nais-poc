<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IDSS8880" cbl:id="IDSS8880" xsi:id="IDSS8880" packageRef="IDSS8880.igd#IDSS8880" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IDSS8880_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10106" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IDSS8880.cbl.cobModel#SC_1F10106"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10106" deadCode="false" name="PROGRAM_IDSS8880_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS8880.cbl.cobModel#P_1F10106"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10106P_1F10106" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10106" targetNode="P_1F10106"/>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LCCS0024" cbl:id="LCCS0024" xsi:id="LCCS0024" packageRef="LCCS0024.igd#LCCS0024" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LCCS0024_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10127" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LCCS0024.cbl.cobModel#SC_1F10127"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10127" deadCode="false" name="PROGRAM_LCCS0024_FIRST_SENTENCES">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_1F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10127" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_2F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10127" deadCode="false" name="S0000-EX">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_3F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10127" deadCode="false" name="S0005-CTRL-DATI-INPUT">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_8F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10127" deadCode="false" name="S0005-EX">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_9F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10127" deadCode="false" name="S0006-CTRL-COD-BLOCCO">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_12F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10127" deadCode="false" name="S0006-EX">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_13F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10127" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_4F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10127" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_5F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10127" deadCode="true" name="S1005-VERIF-PRESENZA-BLOCCO">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_22F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10127" deadCode="true" name="S1005-EX">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_23F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10127" deadCode="false" name="S1010-CENSIMENTO-BLOCCO">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_16F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10127" deadCode="false" name="S1010-EX">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_17F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10127" deadCode="false" name="S1011-VAL-OGG-BLOCCO">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_24F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10127" deadCode="false" name="S1011-EX">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_25F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10127" deadCode="false" name="S1012-INSERT-OGG-BLOCCO">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_26F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10127" deadCode="false" name="S1012-EX">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_27F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10127" deadCode="false" name="S1015-GEST-MOVI-SOSPESO">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_18F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10127" deadCode="false" name="S1015-EX">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_19F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10127" deadCode="false" name="S1016-VAL-MOVI-SOSPESO">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_30F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10127" deadCode="false" name="S1016-EX">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_31F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10127" deadCode="false" name="S1017-INSERT-MOVI-SOSPESO">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_32F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10127" deadCode="false" name="S1017-EX">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_33F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10127" deadCode="false" name="S1020-VALORIZZA-OUTPUT">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_20F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10127" deadCode="false" name="S1020-EX">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_21F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10127" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_6F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10127" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_7F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10127" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_10F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10127" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_11F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10127" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_36F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10127" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_37F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10127" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_34F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10127" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_35F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10127" deadCode="false" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_38F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10127" deadCode="false" name="EX-S0290">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_39F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10127" deadCode="true" name="S0320-OUTPUT-PRODOTTO">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_40F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10127" deadCode="true" name="EX-S0320-OUTPUT-PRODOTTO">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_45F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10127" deadCode="true" name="S0322-SALVA-ERRORE">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_41F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10127" deadCode="true" name="EX-S0322">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_42F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10127" deadCode="true" name="S0321-ERRORI-DEROGA">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_43F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10127" deadCode="true" name="EX-S0321">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_44F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10127" deadCode="true" name="S0323-SALVA-DEROGA">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_46F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10127" deadCode="true" name="EX-S0323">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_47F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10127" deadCode="false" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_14F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10127" deadCode="false" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_15F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10127" deadCode="false" name="ESTR-SEQUENCE">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_28F10127"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10127" deadCode="false" name="ESTR-SEQUENCE-EX">
				<representations href="../../../cobol/LCCS0024.cbl.cobModel#P_29F10127"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0010" name="IDSS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10099"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0090" name="LCCS0090">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10133"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10127P_1F10127" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10127" targetNode="P_1F10127"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10127_I" deadCode="false" sourceNode="P_1F10127" targetNode="P_2F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_1F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10127_O" deadCode="false" sourceNode="P_1F10127" targetNode="P_3F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_1F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10127_I" deadCode="false" sourceNode="P_1F10127" targetNode="P_4F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_3F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10127_O" deadCode="false" sourceNode="P_1F10127" targetNode="P_5F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_3F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10127_I" deadCode="false" sourceNode="P_1F10127" targetNode="P_6F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_4F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10127_O" deadCode="false" sourceNode="P_1F10127" targetNode="P_7F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_4F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10127_I" deadCode="false" sourceNode="P_2F10127" targetNode="P_8F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_7F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10127_O" deadCode="false" sourceNode="P_2F10127" targetNode="P_9F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_7F10127"/>
	</edges>
	<edges id="P_2F10127P_3F10127" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10127" targetNode="P_3F10127"/>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10127_I" deadCode="false" sourceNode="P_8F10127" targetNode="P_10F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_14F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10127_O" deadCode="false" sourceNode="P_8F10127" targetNode="P_11F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_14F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10127_I" deadCode="false" sourceNode="P_8F10127" targetNode="P_10F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_21F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10127_O" deadCode="false" sourceNode="P_8F10127" targetNode="P_11F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_21F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10127_I" deadCode="false" sourceNode="P_8F10127" targetNode="P_10F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_28F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10127_O" deadCode="false" sourceNode="P_8F10127" targetNode="P_11F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_28F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10127_I" deadCode="false" sourceNode="P_8F10127" targetNode="P_10F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_35F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10127_O" deadCode="false" sourceNode="P_8F10127" targetNode="P_11F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_35F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10127_I" deadCode="false" sourceNode="P_8F10127" targetNode="P_10F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_42F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_42F10127_O" deadCode="false" sourceNode="P_8F10127" targetNode="P_11F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_42F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10127_I" deadCode="false" sourceNode="P_8F10127" targetNode="P_10F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_49F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10127_O" deadCode="false" sourceNode="P_8F10127" targetNode="P_11F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_49F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10127_I" deadCode="false" sourceNode="P_8F10127" targetNode="P_10F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_56F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10127_O" deadCode="false" sourceNode="P_8F10127" targetNode="P_11F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_56F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10127_I" deadCode="false" sourceNode="P_8F10127" targetNode="P_12F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_58F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10127_O" deadCode="false" sourceNode="P_8F10127" targetNode="P_13F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_58F10127"/>
	</edges>
	<edges id="P_8F10127P_9F10127" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10127" targetNode="P_9F10127"/>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10127_I" deadCode="false" sourceNode="P_12F10127" targetNode="P_14F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_69F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10127_O" deadCode="false" sourceNode="P_12F10127" targetNode="P_15F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_69F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10127_I" deadCode="false" sourceNode="P_12F10127" targetNode="P_10F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_76F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10127_O" deadCode="false" sourceNode="P_12F10127" targetNode="P_11F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_76F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10127_I" deadCode="false" sourceNode="P_12F10127" targetNode="P_10F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_82F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_82F10127_O" deadCode="false" sourceNode="P_12F10127" targetNode="P_11F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_82F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10127_I" deadCode="false" sourceNode="P_12F10127" targetNode="P_10F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_87F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10127_O" deadCode="false" sourceNode="P_12F10127" targetNode="P_11F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_87F10127"/>
	</edges>
	<edges id="P_12F10127P_13F10127" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10127" targetNode="P_13F10127"/>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10127_I" deadCode="false" sourceNode="P_4F10127" targetNode="P_16F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_90F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10127_O" deadCode="false" sourceNode="P_4F10127" targetNode="P_17F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_90F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10127_I" deadCode="false" sourceNode="P_4F10127" targetNode="P_18F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_93F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10127_O" deadCode="false" sourceNode="P_4F10127" targetNode="P_19F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_93F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10127_I" deadCode="false" sourceNode="P_4F10127" targetNode="P_20F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_95F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10127_O" deadCode="false" sourceNode="P_4F10127" targetNode="P_21F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_95F10127"/>
	</edges>
	<edges id="P_4F10127P_5F10127" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10127" targetNode="P_5F10127"/>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10127_I" deadCode="true" sourceNode="P_22F10127" targetNode="P_14F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_113F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10127_O" deadCode="true" sourceNode="P_22F10127" targetNode="P_15F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_113F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10127_I" deadCode="true" sourceNode="P_22F10127" targetNode="P_10F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_123F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10127_O" deadCode="true" sourceNode="P_22F10127" targetNode="P_11F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_123F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10127_I" deadCode="false" sourceNode="P_16F10127" targetNode="P_24F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_125F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10127_O" deadCode="false" sourceNode="P_16F10127" targetNode="P_25F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_125F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10127_I" deadCode="false" sourceNode="P_16F10127" targetNode="P_26F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_126F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10127_O" deadCode="false" sourceNode="P_16F10127" targetNode="P_27F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_126F10127"/>
	</edges>
	<edges id="P_16F10127P_17F10127" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10127" targetNode="P_17F10127"/>
	<edges xsi:type="cbl:PerformEdge" id="S_129F10127_I" deadCode="false" sourceNode="P_24F10127" targetNode="P_28F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_129F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_129F10127_O" deadCode="false" sourceNode="P_24F10127" targetNode="P_29F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_129F10127"/>
	</edges>
	<edges id="P_24F10127P_25F10127" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10127" targetNode="P_25F10127"/>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10127_I" deadCode="false" sourceNode="P_26F10127" targetNode="P_14F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_153F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10127_O" deadCode="false" sourceNode="P_26F10127" targetNode="P_15F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_153F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10127_I" deadCode="false" sourceNode="P_26F10127" targetNode="P_10F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_161F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10127_O" deadCode="false" sourceNode="P_26F10127" targetNode="P_11F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_161F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10127_I" deadCode="false" sourceNode="P_26F10127" targetNode="P_10F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_166F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10127_O" deadCode="false" sourceNode="P_26F10127" targetNode="P_11F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_166F10127"/>
	</edges>
	<edges id="P_26F10127P_27F10127" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10127" targetNode="P_27F10127"/>
	<edges xsi:type="cbl:PerformEdge" id="S_169F10127_I" deadCode="false" sourceNode="P_18F10127" targetNode="P_30F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_168F10127"/>
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_169F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_169F10127_O" deadCode="false" sourceNode="P_18F10127" targetNode="P_31F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_168F10127"/>
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_169F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10127_I" deadCode="false" sourceNode="P_18F10127" targetNode="P_32F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_168F10127"/>
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_170F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10127_O" deadCode="false" sourceNode="P_18F10127" targetNode="P_33F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_168F10127"/>
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_170F10127"/>
	</edges>
	<edges id="P_18F10127P_19F10127" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10127" targetNode="P_19F10127"/>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10127_I" deadCode="false" sourceNode="P_30F10127" targetNode="P_28F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_173F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_173F10127_O" deadCode="false" sourceNode="P_30F10127" targetNode="P_29F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_173F10127"/>
	</edges>
	<edges id="P_30F10127P_31F10127" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10127" targetNode="P_31F10127"/>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10127_I" deadCode="false" sourceNode="P_32F10127" targetNode="P_14F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_202F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10127_O" deadCode="false" sourceNode="P_32F10127" targetNode="P_15F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_202F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_210F10127_I" deadCode="false" sourceNode="P_32F10127" targetNode="P_10F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_210F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_210F10127_O" deadCode="false" sourceNode="P_32F10127" targetNode="P_11F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_210F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_215F10127_I" deadCode="false" sourceNode="P_32F10127" targetNode="P_10F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_215F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_215F10127_O" deadCode="false" sourceNode="P_32F10127" targetNode="P_11F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_215F10127"/>
	</edges>
	<edges id="P_32F10127P_33F10127" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10127" targetNode="P_33F10127"/>
	<edges id="P_20F10127P_21F10127" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10127" targetNode="P_21F10127"/>
	<edges xsi:type="cbl:PerformEdge" id="S_227F10127_I" deadCode="false" sourceNode="P_10F10127" targetNode="P_34F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_227F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_227F10127_O" deadCode="false" sourceNode="P_10F10127" targetNode="P_35F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_227F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10127_I" deadCode="false" sourceNode="P_10F10127" targetNode="P_36F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_231F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10127_O" deadCode="false" sourceNode="P_10F10127" targetNode="P_37F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_231F10127"/>
	</edges>
	<edges id="P_10F10127P_11F10127" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10127" targetNode="P_11F10127"/>
	<edges id="P_36F10127P_37F10127" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10127" targetNode="P_37F10127"/>
	<edges id="P_34F10127P_35F10127" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10127" targetNode="P_35F10127"/>
	<edges xsi:type="cbl:PerformEdge" id="S_267F10127_I" deadCode="false" sourceNode="P_38F10127" targetNode="P_10F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_267F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_267F10127_O" deadCode="false" sourceNode="P_38F10127" targetNode="P_11F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_267F10127"/>
	</edges>
	<edges id="P_38F10127P_39F10127" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10127" targetNode="P_39F10127"/>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10127_I" deadCode="true" sourceNode="P_40F10127" targetNode="P_41F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_281F10127"/>
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_283F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10127_O" deadCode="true" sourceNode="P_40F10127" targetNode="P_42F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_281F10127"/>
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_283F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10127_I" deadCode="true" sourceNode="P_40F10127" targetNode="P_43F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_284F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_284F10127_O" deadCode="true" sourceNode="P_40F10127" targetNode="P_44F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_284F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10127_I" deadCode="true" sourceNode="P_41F10127" targetNode="P_10F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_289F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10127_O" deadCode="true" sourceNode="P_41F10127" targetNode="P_11F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_289F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_293F10127_I" deadCode="true" sourceNode="P_43F10127" targetNode="P_41F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_293F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_293F10127_O" deadCode="true" sourceNode="P_43F10127" targetNode="P_42F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_293F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_295F10127_I" deadCode="true" sourceNode="P_43F10127" targetNode="P_41F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_295F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_295F10127_O" deadCode="true" sourceNode="P_43F10127" targetNode="P_42F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_295F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_297F10127_I" deadCode="true" sourceNode="P_43F10127" targetNode="P_41F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_297F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_297F10127_O" deadCode="true" sourceNode="P_43F10127" targetNode="P_42F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_297F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_300F10127_I" deadCode="true" sourceNode="P_43F10127" targetNode="P_41F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_300F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_300F10127_O" deadCode="true" sourceNode="P_43F10127" targetNode="P_42F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_300F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10127_I" deadCode="true" sourceNode="P_43F10127" targetNode="P_46F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_301F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10127_O" deadCode="true" sourceNode="P_43F10127" targetNode="P_47F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_301F10127"/>
	</edges>
	<edges id="P_14F10127P_15F10127" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10127" targetNode="P_15F10127"/>
	<edges xsi:type="cbl:PerformEdge" id="S_352F10127_I" deadCode="false" sourceNode="P_28F10127" targetNode="P_38F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_352F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_352F10127_O" deadCode="false" sourceNode="P_28F10127" targetNode="P_39F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_352F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_360F10127_I" deadCode="false" sourceNode="P_28F10127" targetNode="P_10F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_360F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_360F10127_O" deadCode="false" sourceNode="P_28F10127" targetNode="P_11F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_360F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_365F10127_I" deadCode="false" sourceNode="P_28F10127" targetNode="P_10F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_365F10127"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_365F10127_O" deadCode="false" sourceNode="P_28F10127" targetNode="P_11F10127">
		<representations href="../../../cobol/LCCS0024.cbl.cobModel#S_365F10127"/>
	</edges>
	<edges id="P_28F10127P_29F10127" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10127" targetNode="P_29F10127"/>
	<edges xsi:type="cbl:CallEdge" id="S_225F10127" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_10F10127" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_225F10127"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_344F10127" deadCode="false" name="Dynamic IDSI0011-PGM" sourceNode="P_14F10127" targetNode="IDSS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_344F10127"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_348F10127" deadCode="false" name="Dynamic LCCS0090" sourceNode="P_28F10127" targetNode="LCCS0090">
		<representations href="../../../cobol/../importantStmts.cobModel#S_348F10127"></representations>
	</edges>
</Package>

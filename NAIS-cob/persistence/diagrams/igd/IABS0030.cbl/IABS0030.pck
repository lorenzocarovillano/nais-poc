<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IABS0030" cbl:id="IABS0030" xsi:id="IABS0030" packageRef="IABS0030.igd#IABS0030" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IABS0030_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10002" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IABS0030.cbl.cobModel#SC_1F10002"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10002" deadCode="false" name="PROGRAM_IABS0030_FIRST_SENTENCES">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_1F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10002" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_2F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10002" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_3F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10002" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_6F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10002" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_7F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10002" deadCode="false" name="A300-ELABORA">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_4F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10002" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_5F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10002" deadCode="false" name="A305-DECLARE-CURSOR">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_20F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10002" deadCode="false" name="A305-EX">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_21F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10002" deadCode="false" name="A310-SELECT">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_8F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10002" deadCode="false" name="A310-EX">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_9F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10002" deadCode="false" name="A330-UPDATE">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_18F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10002" deadCode="false" name="A330-EX">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_19F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10002" deadCode="false" name="A360-OPEN-CURSOR">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_10F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10002" deadCode="false" name="A360-EX">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_11F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10002" deadCode="false" name="A370-CLOSE-CURSOR">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_12F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10002" deadCode="false" name="A370-EX">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_13F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10002" deadCode="false" name="A380-FETCH-FIRST">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_14F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10002" deadCode="false" name="A380-EX">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_15F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10002" deadCode="false" name="A390-FETCH-NEXT">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_16F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10002" deadCode="false" name="A390-EX">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_17F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10002" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_22F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10002" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_23F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10002" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_24F10002"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10002" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/IABS0030.cbl.cobModel#P_25F10002"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_BTC_ELAB_STATE" name="BTC_ELAB_STATE">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_BTC_ELAB_STATE"/>
		</children>
	</packageNode>
	<edges id="SC_1F10002P_1F10002" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10002" targetNode="P_1F10002"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10002_I" deadCode="false" sourceNode="P_1F10002" targetNode="P_2F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_1F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10002_O" deadCode="false" sourceNode="P_1F10002" targetNode="P_3F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_1F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10002_I" deadCode="false" sourceNode="P_1F10002" targetNode="P_4F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_2F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10002_O" deadCode="false" sourceNode="P_1F10002" targetNode="P_5F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_2F10002"/>
	</edges>
	<edges id="P_2F10002P_3F10002" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10002" targetNode="P_3F10002"/>
	<edges id="P_6F10002P_7F10002" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10002" targetNode="P_7F10002"/>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10002_I" deadCode="false" sourceNode="P_4F10002" targetNode="P_8F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_21F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10002_O" deadCode="false" sourceNode="P_4F10002" targetNode="P_9F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_21F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10002_I" deadCode="false" sourceNode="P_4F10002" targetNode="P_10F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_22F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10002_O" deadCode="false" sourceNode="P_4F10002" targetNode="P_11F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_22F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10002_I" deadCode="false" sourceNode="P_4F10002" targetNode="P_12F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_23F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10002_O" deadCode="false" sourceNode="P_4F10002" targetNode="P_13F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_23F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10002_I" deadCode="false" sourceNode="P_4F10002" targetNode="P_14F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_24F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10002_O" deadCode="false" sourceNode="P_4F10002" targetNode="P_15F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_24F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10002_I" deadCode="false" sourceNode="P_4F10002" targetNode="P_16F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_25F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10002_O" deadCode="false" sourceNode="P_4F10002" targetNode="P_17F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_25F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10002_I" deadCode="false" sourceNode="P_4F10002" targetNode="P_18F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_26F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10002_O" deadCode="false" sourceNode="P_4F10002" targetNode="P_19F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_26F10002"/>
	</edges>
	<edges id="P_4F10002P_5F10002" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10002" targetNode="P_5F10002"/>
	<edges id="P_20F10002P_21F10002" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10002" targetNode="P_21F10002"/>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10002_I" deadCode="false" sourceNode="P_8F10002" targetNode="P_6F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_33F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10002_O" deadCode="false" sourceNode="P_8F10002" targetNode="P_7F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_33F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10002_I" deadCode="false" sourceNode="P_8F10002" targetNode="P_22F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_35F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10002_O" deadCode="false" sourceNode="P_8F10002" targetNode="P_23F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_35F10002"/>
	</edges>
	<edges id="P_8F10002P_9F10002" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10002" targetNode="P_9F10002"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10002_I" deadCode="false" sourceNode="P_18F10002" targetNode="P_24F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_37F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10002_O" deadCode="false" sourceNode="P_18F10002" targetNode="P_25F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_37F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10002_I" deadCode="false" sourceNode="P_18F10002" targetNode="P_6F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_39F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10002_O" deadCode="false" sourceNode="P_18F10002" targetNode="P_7F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_39F10002"/>
	</edges>
	<edges id="P_18F10002P_19F10002" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10002" targetNode="P_19F10002"/>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10002_I" deadCode="false" sourceNode="P_10F10002" targetNode="P_20F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_41F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10002_O" deadCode="false" sourceNode="P_10F10002" targetNode="P_21F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_41F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10002_I" deadCode="false" sourceNode="P_10F10002" targetNode="P_6F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_43F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10002_O" deadCode="false" sourceNode="P_10F10002" targetNode="P_7F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_43F10002"/>
	</edges>
	<edges id="P_10F10002P_11F10002" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10002" targetNode="P_11F10002"/>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10002_I" deadCode="false" sourceNode="P_12F10002" targetNode="P_6F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_46F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10002_O" deadCode="false" sourceNode="P_12F10002" targetNode="P_7F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_46F10002"/>
	</edges>
	<edges id="P_12F10002P_13F10002" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10002" targetNode="P_13F10002"/>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10002_I" deadCode="false" sourceNode="P_14F10002" targetNode="P_10F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_48F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10002_O" deadCode="false" sourceNode="P_14F10002" targetNode="P_11F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_48F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10002_I" deadCode="false" sourceNode="P_14F10002" targetNode="P_16F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_50F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10002_O" deadCode="false" sourceNode="P_14F10002" targetNode="P_17F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_50F10002"/>
	</edges>
	<edges id="P_14F10002P_15F10002" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10002" targetNode="P_15F10002"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10002_I" deadCode="false" sourceNode="P_16F10002" targetNode="P_6F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_53F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10002_O" deadCode="false" sourceNode="P_16F10002" targetNode="P_7F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_53F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10002_I" deadCode="false" sourceNode="P_16F10002" targetNode="P_22F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_55F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10002_O" deadCode="false" sourceNode="P_16F10002" targetNode="P_23F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_55F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10002_I" deadCode="false" sourceNode="P_16F10002" targetNode="P_12F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_57F10002"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10002_O" deadCode="false" sourceNode="P_16F10002" targetNode="P_13F10002">
		<representations href="../../../cobol/IABS0030.cbl.cobModel#S_57F10002"/>
	</edges>
	<edges id="P_16F10002P_17F10002" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10002" targetNode="P_17F10002"/>
	<edges id="P_22F10002P_23F10002" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10002" targetNode="P_23F10002"/>
	<edges id="P_24F10002P_25F10002" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10002" targetNode="P_25F10002"/>
	<edges xsi:type="cbl:DataEdge" id="S_32F10002_POS1" deadCode="false" targetNode="P_8F10002" sourceNode="DB2_BTC_ELAB_STATE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_32F10002"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_38F10002_POS1" deadCode="false" sourceNode="P_18F10002" targetNode="DB2_BTC_ELAB_STATE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_38F10002"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_42F10002_POS1" deadCode="false" targetNode="P_10F10002" sourceNode="DB2_BTC_ELAB_STATE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_42F10002"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_45F10002_POS1" deadCode="false" targetNode="P_12F10002" sourceNode="DB2_BTC_ELAB_STATE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_45F10002"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_52F10002_POS1" deadCode="false" targetNode="P_16F10002" sourceNode="DB2_BTC_ELAB_STATE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_52F10002"></representations>
	</edges>
</Package>

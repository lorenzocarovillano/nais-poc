<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS2800" cbl:id="LVVS2800" xsi:id="LVVS2800" packageRef="LVVS2800.igd#LVVS2800" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS2800_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10379" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS2800.cbl.cobModel#SC_1F10379"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10379" deadCode="false" name="PROGRAM_LVVS2800_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_1F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10379" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_2F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10379" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_3F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10379" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_4F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10379" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_5F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10379" deadCode="false" name="L450-LEGGI-POLIZZA">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_8F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10379" deadCode="false" name="L450-LEGGI-POLIZZA-EX">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_9F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10379" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_20F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10379" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_21F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10379" deadCode="false" name="S1150-CALCOLO-ANNO">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_14F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10379" deadCode="false" name="S1150-CALCOLO-ANNO-EX">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_15F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10379" deadCode="false" name="S1200-CALCOLO-IMPORTO">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_16F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10379" deadCode="false" name="S1200-CALCOLO-IMPORTO-EX">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_17F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10379" deadCode="false" name="S1201-CALC-IMP-COLL">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_18F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10379" deadCode="false" name="S1201-CALC-IMP-COLL-EX">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_19F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10379" deadCode="false" name="VERIFICA-MOVIMENTO">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_10F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10379" deadCode="false" name="VERIFICA-MOVIMENTO-EX">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_11F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10379" deadCode="false" name="RECUP-MOVI-COMUN">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_22F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10379" deadCode="false" name="RECUP-MOVI-COMUN-EX">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_23F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10379" deadCode="false" name="LETTURA-D-CRIST">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_26F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10379" deadCode="false" name="LETTURA-D-CRIST-EX">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_27F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10379" deadCode="false" name="LEGGI-D-CRIST-X-ADE">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_12F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10379" deadCode="false" name="LEGGI-D-CRIST-X-ADE-EX">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_13F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10379" deadCode="false" name="CLOSE-MOVI">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_28F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10379" deadCode="false" name="CLOSE-MOVI-EX">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_29F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10379" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_6F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10379" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_7F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10379" deadCode="false" name="VALORIZZA-OUTPUT-P61">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_30F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10379" deadCode="false" name="VALORIZZA-OUTPUT-P61-EX">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_31F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10379" deadCode="false" name="INIZIA-TOT-P61">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_24F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10379" deadCode="false" name="INIZIA-TOT-P61-EX">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_25F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10379" deadCode="false" name="INIZIA-NULL-P61">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_36F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10379" deadCode="false" name="INIZIA-NULL-P61-EX">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_37F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10379" deadCode="true" name="INIZIA-ZEROES-P61">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_32F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10379" deadCode="false" name="INIZIA-ZEROES-P61-EX">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_33F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10379" deadCode="true" name="INIZIA-SPACES-P61">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_34F10379"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10379" deadCode="false" name="INIZIA-SPACES-P61-EX">
				<representations href="../../../cobol/LVVS2800.cbl.cobModel#P_35F10379"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSPOL0" name="IDBSPOL0">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10078"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS6040" name="LDBS6040">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10230"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSP610" name="IDBSP610">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10065"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSH650" name="LDBSH650">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10278"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10379P_1F10379" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10379" targetNode="P_1F10379"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10379_I" deadCode="false" sourceNode="P_1F10379" targetNode="P_2F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_1F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10379_O" deadCode="false" sourceNode="P_1F10379" targetNode="P_3F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_1F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10379_I" deadCode="false" sourceNode="P_1F10379" targetNode="P_4F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_2F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10379_O" deadCode="false" sourceNode="P_1F10379" targetNode="P_5F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_2F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10379_I" deadCode="false" sourceNode="P_1F10379" targetNode="P_6F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_3F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10379_O" deadCode="false" sourceNode="P_1F10379" targetNode="P_7F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_3F10379"/>
	</edges>
	<edges id="P_2F10379P_3F10379" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10379" targetNode="P_3F10379"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10379_I" deadCode="false" sourceNode="P_4F10379" targetNode="P_8F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_11F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10379_O" deadCode="false" sourceNode="P_4F10379" targetNode="P_9F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_11F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10379_I" deadCode="false" sourceNode="P_4F10379" targetNode="P_10F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_14F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10379_O" deadCode="false" sourceNode="P_4F10379" targetNode="P_11F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_14F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10379_I" deadCode="false" sourceNode="P_4F10379" targetNode="P_12F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_15F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10379_O" deadCode="false" sourceNode="P_4F10379" targetNode="P_13F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_15F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10379_I" deadCode="false" sourceNode="P_4F10379" targetNode="P_14F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_17F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10379_O" deadCode="false" sourceNode="P_4F10379" targetNode="P_15F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_17F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10379_I" deadCode="false" sourceNode="P_4F10379" targetNode="P_16F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_20F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10379_O" deadCode="false" sourceNode="P_4F10379" targetNode="P_17F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_20F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10379_I" deadCode="false" sourceNode="P_4F10379" targetNode="P_18F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_21F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10379_O" deadCode="false" sourceNode="P_4F10379" targetNode="P_19F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_21F10379"/>
	</edges>
	<edges id="P_4F10379P_5F10379" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10379" targetNode="P_5F10379"/>
	<edges id="P_8F10379P_9F10379" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10379" targetNode="P_9F10379"/>
	<edges id="P_20F10379P_21F10379" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10379" targetNode="P_21F10379"/>
	<edges id="P_14F10379P_15F10379" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10379" targetNode="P_15F10379"/>
	<edges id="P_16F10379P_17F10379" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10379" targetNode="P_17F10379"/>
	<edges id="P_18F10379P_19F10379" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10379" targetNode="P_19F10379"/>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10379_I" deadCode="false" sourceNode="P_10F10379" targetNode="P_22F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_65F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10379_O" deadCode="false" sourceNode="P_10F10379" targetNode="P_23F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_65F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10379_I" deadCode="false" sourceNode="P_10F10379" targetNode="P_24F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_71F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10379_O" deadCode="false" sourceNode="P_10F10379" targetNode="P_25F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_71F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10379_I" deadCode="false" sourceNode="P_10F10379" targetNode="P_26F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_72F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10379_O" deadCode="false" sourceNode="P_10F10379" targetNode="P_27F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_72F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10379_I" deadCode="false" sourceNode="P_10F10379" targetNode="P_20F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_73F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10379_O" deadCode="false" sourceNode="P_10F10379" targetNode="P_21F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_73F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10379_I" deadCode="false" sourceNode="P_10F10379" targetNode="P_20F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_74F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10379_O" deadCode="false" sourceNode="P_10F10379" targetNode="P_21F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_74F10379"/>
	</edges>
	<edges id="P_10F10379P_11F10379" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10379" targetNode="P_11F10379"/>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10379_I" deadCode="false" sourceNode="P_22F10379" targetNode="P_28F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_83F10379"/>
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_105F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10379_O" deadCode="false" sourceNode="P_22F10379" targetNode="P_29F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_83F10379"/>
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_105F10379"/>
	</edges>
	<edges id="P_22F10379P_23F10379" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10379" targetNode="P_23F10379"/>
	<edges xsi:type="cbl:PerformEdge" id="S_141F10379_I" deadCode="false" sourceNode="P_26F10379" targetNode="P_30F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_123F10379"/>
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_141F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_141F10379_O" deadCode="false" sourceNode="P_26F10379" targetNode="P_31F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_123F10379"/>
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_141F10379"/>
	</edges>
	<edges id="P_26F10379P_27F10379" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10379" targetNode="P_27F10379"/>
	<edges id="P_12F10379P_13F10379" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10379" targetNode="P_13F10379"/>
	<edges id="P_28F10379P_29F10379" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10379" targetNode="P_29F10379"/>
	<edges id="P_30F10379P_31F10379" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10379" targetNode="P_31F10379"/>
	<edges xsi:type="cbl:PerformEdge" id="S_305F10379_I" deadCode="true" sourceNode="P_24F10379" targetNode="P_32F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_305F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_305F10379_O" deadCode="true" sourceNode="P_24F10379" targetNode="P_33F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_305F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_306F10379_I" deadCode="true" sourceNode="P_24F10379" targetNode="P_34F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_306F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_306F10379_O" deadCode="true" sourceNode="P_24F10379" targetNode="P_35F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_306F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_307F10379_I" deadCode="false" sourceNode="P_24F10379" targetNode="P_36F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_307F10379"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_307F10379_O" deadCode="false" sourceNode="P_24F10379" targetNode="P_37F10379">
		<representations href="../../../cobol/LVVS2800.cbl.cobModel#S_307F10379"/>
	</edges>
	<edges id="P_24F10379P_25F10379" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10379" targetNode="P_25F10379"/>
	<edges id="P_36F10379P_37F10379" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10379" targetNode="P_37F10379"/>
	<edges id="P_32F10379P_33F10379" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10379" targetNode="P_33F10379"/>
	<edges id="P_34F10379P_35F10379" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10379" targetNode="P_35F10379"/>
	<edges xsi:type="cbl:CallEdge" id="S_29F10379" deadCode="false" name="Dynamic WK-PGM-CALLED" sourceNode="P_8F10379" targetNode="IDBSPOL0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_29F10379"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_91F10379" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_22F10379" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_91F10379"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_130F10379" deadCode="false" name="Dynamic IDBSP610" sourceNode="P_26F10379" targetNode="IDBSP610">
		<representations href="../../../cobol/../importantStmts.cobModel#S_130F10379"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_165F10379" deadCode="false" name="Dynamic LDBSH650" sourceNode="P_12F10379" targetNode="LDBSH650">
		<representations href="../../../cobol/../importantStmts.cobModel#S_165F10379"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_188F10379" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_28F10379" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_188F10379"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS3270" cbl:id="LVVS3270" xsi:id="LVVS3270" packageRef="LVVS3270.igd#LVVS3270" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS3270_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10390" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS3270.cbl.cobModel#SC_1F10390"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10390" deadCode="false" name="PROGRAM_LVVS3270_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_1F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10390" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_2F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10390" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_3F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10390" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_4F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10390" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_5F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10390" deadCode="false" name="A400-LEGGI-SDI">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_10F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10390" deadCode="false" name="A400-LEGGI-SDI-EX">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_11F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10390" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_8F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10390" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_9F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10390" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_6F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10390" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_7F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10390" deadCode="true" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_12F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10390" deadCode="true" name="EX-S0290">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_15F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10390" deadCode="true" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_13F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10390" deadCode="true" name="EX-S0300">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_14F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10390" deadCode="true" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_18F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10390" deadCode="true" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_19F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10390" deadCode="true" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_16F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10390" deadCode="true" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_17F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10390" deadCode="true" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_20F10390"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10390" deadCode="true" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LVVS3270.cbl.cobModel#P_21F10390"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSSDI0" name="IDBSSDI0">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10088"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LVVS3270_CALL-PGM" name="Dynamic LVVS3270 CALL-PGM" missing="true">
			<representations href="../../../../missing.xmi#ID0S2WDEV4FZZZFFUIQUHG0SJSYHBDD52JAIZ1TDDIEXDAANILV1WB"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LVVS3270_IDSI0011-PGM" name="Dynamic LVVS3270 IDSI0011-PGM" missing="true">
			<representations href="../../../../missing.xmi#IDSY5O2ADTLXFUCQZ4Q5UXXEVRSLGNAFFSRA343KSRITW35SL431K"/>
		</children>
	</packageNode>
	<edges id="SC_1F10390P_1F10390" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10390" targetNode="P_1F10390"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10390_I" deadCode="false" sourceNode="P_1F10390" targetNode="P_2F10390">
		<representations href="../../../cobol/LVVS3270.cbl.cobModel#S_1F10390"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10390_O" deadCode="false" sourceNode="P_1F10390" targetNode="P_3F10390">
		<representations href="../../../cobol/LVVS3270.cbl.cobModel#S_1F10390"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10390_I" deadCode="false" sourceNode="P_1F10390" targetNode="P_4F10390">
		<representations href="../../../cobol/LVVS3270.cbl.cobModel#S_2F10390"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10390_O" deadCode="false" sourceNode="P_1F10390" targetNode="P_5F10390">
		<representations href="../../../cobol/LVVS3270.cbl.cobModel#S_2F10390"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10390_I" deadCode="false" sourceNode="P_1F10390" targetNode="P_6F10390">
		<representations href="../../../cobol/LVVS3270.cbl.cobModel#S_3F10390"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10390_O" deadCode="false" sourceNode="P_1F10390" targetNode="P_7F10390">
		<representations href="../../../cobol/LVVS3270.cbl.cobModel#S_3F10390"/>
	</edges>
	<edges id="P_2F10390P_3F10390" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10390" targetNode="P_3F10390"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10390_I" deadCode="false" sourceNode="P_4F10390" targetNode="P_8F10390">
		<representations href="../../../cobol/LVVS3270.cbl.cobModel#S_10F10390"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10390_O" deadCode="false" sourceNode="P_4F10390" targetNode="P_9F10390">
		<representations href="../../../cobol/LVVS3270.cbl.cobModel#S_10F10390"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10390_I" deadCode="false" sourceNode="P_4F10390" targetNode="P_10F10390">
		<representations href="../../../cobol/LVVS3270.cbl.cobModel#S_12F10390"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10390_O" deadCode="false" sourceNode="P_4F10390" targetNode="P_11F10390">
		<representations href="../../../cobol/LVVS3270.cbl.cobModel#S_12F10390"/>
	</edges>
	<edges id="P_4F10390P_5F10390" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10390" targetNode="P_5F10390"/>
	<edges id="P_10F10390P_11F10390" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10390" targetNode="P_11F10390"/>
	<edges id="P_8F10390P_9F10390" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10390" targetNode="P_9F10390"/>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10390_I" deadCode="true" sourceNode="P_12F10390" targetNode="P_13F10390">
		<representations href="../../../cobol/LVVS3270.cbl.cobModel#S_47F10390"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10390_O" deadCode="true" sourceNode="P_12F10390" targetNode="P_14F10390">
		<representations href="../../../cobol/LVVS3270.cbl.cobModel#S_47F10390"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10390_I" deadCode="true" sourceNode="P_13F10390" targetNode="P_16F10390">
		<representations href="../../../cobol/LVVS3270.cbl.cobModel#S_54F10390"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10390_O" deadCode="true" sourceNode="P_13F10390" targetNode="P_17F10390">
		<representations href="../../../cobol/LVVS3270.cbl.cobModel#S_54F10390"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10390_I" deadCode="true" sourceNode="P_13F10390" targetNode="P_18F10390">
		<representations href="../../../cobol/LVVS3270.cbl.cobModel#S_58F10390"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10390_O" deadCode="true" sourceNode="P_13F10390" targetNode="P_19F10390">
		<representations href="../../../cobol/LVVS3270.cbl.cobModel#S_58F10390"/>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_26F10390" deadCode="false" name="Dynamic PGM-IDBSSDI0" sourceNode="P_10F10390" targetNode="IDBSSDI0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_26F10390"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_52F10390" deadCode="true" name="Dynamic CALL-PGM" sourceNode="P_13F10390" targetNode="Dynamic_LVVS3270_CALL-PGM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_52F10390"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_118F10390" deadCode="true" name="Dynamic IDSI0011-PGM" sourceNode="P_20F10390" targetNode="Dynamic_LVVS3270_IDSI0011-PGM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_118F10390"></representations>
	</edges>
</Package>

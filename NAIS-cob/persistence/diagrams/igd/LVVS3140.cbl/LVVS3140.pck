<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS3140" cbl:id="LVVS3140" xsi:id="LVVS3140" packageRef="LVVS3140.igd#LVVS3140" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS3140_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10383" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS3140.cbl.cobModel#SC_1F10383"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10383" deadCode="false" name="PROGRAM_LVVS3140_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_1F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10383" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_2F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10383" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_3F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10383" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_4F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10383" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_5F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10383" deadCode="false" name="L450-LEGGI-POLIZZA">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_8F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10383" deadCode="false" name="L450-LEGGI-POLIZZA-EX">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_9F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10383" deadCode="false" name="LEGGI-D-CRIST-X-ADE">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_12F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10383" deadCode="false" name="LEGGI-D-CRIST-X-ADE-EX">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_13F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10383" deadCode="false" name="S1201-CALC-IMP-COLL">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_18F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10383" deadCode="false" name="S1201-CALC-IMP-COLL-EX">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_19F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10383" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_20F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10383" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_21F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10383" deadCode="false" name="S1150-CALCOLO-ANNO">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_14F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10383" deadCode="false" name="S1150-CALCOLO-ANNO-EX">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_15F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10383" deadCode="false" name="S1200-CALCOLO-IMPORTO">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_16F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10383" deadCode="false" name="S1200-CALCOLO-IMPORTO-EX">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_17F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10383" deadCode="false" name="VERIFICA-MOVIMENTO">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_10F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10383" deadCode="false" name="VERIFICA-MOVIMENTO-EX">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_11F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10383" deadCode="false" name="RECUP-MOVI-COMUN">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_22F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10383" deadCode="false" name="RECUP-MOVI-COMUN-EX">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_23F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10383" deadCode="false" name="LETTURA-D-CRIST">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_26F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10383" deadCode="false" name="LETTURA-D-CRIST-EX">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_27F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10383" deadCode="false" name="CLOSE-MOVI">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_28F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10383" deadCode="false" name="CLOSE-MOVI-EX">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_29F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10383" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_6F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10383" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_7F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10383" deadCode="false" name="VALORIZZA-OUTPUT-P61">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_30F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10383" deadCode="false" name="VALORIZZA-OUTPUT-P61-EX">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_31F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10383" deadCode="false" name="INIZIA-TOT-P61">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_24F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10383" deadCode="false" name="INIZIA-TOT-P61-EX">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_25F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10383" deadCode="false" name="INIZIA-NULL-P61">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_36F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10383" deadCode="false" name="INIZIA-NULL-P61-EX">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_37F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10383" deadCode="true" name="INIZIA-ZEROES-P61">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_32F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10383" deadCode="false" name="INIZIA-ZEROES-P61-EX">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_33F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10383" deadCode="true" name="INIZIA-SPACES-P61">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_34F10383"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10383" deadCode="false" name="INIZIA-SPACES-P61-EX">
				<representations href="../../../cobol/LVVS3140.cbl.cobModel#P_35F10383"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSPOL0" name="IDBSPOL0">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10078"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSH650" name="LDBSH650">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10278"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS6040" name="LDBS6040">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10230"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDBSP610" name="IDBSP610">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10065"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10383P_1F10383" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10383" targetNode="P_1F10383"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10383_I" deadCode="false" sourceNode="P_1F10383" targetNode="P_2F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_1F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10383_O" deadCode="false" sourceNode="P_1F10383" targetNode="P_3F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_1F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10383_I" deadCode="false" sourceNode="P_1F10383" targetNode="P_4F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_2F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10383_O" deadCode="false" sourceNode="P_1F10383" targetNode="P_5F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_2F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10383_I" deadCode="false" sourceNode="P_1F10383" targetNode="P_6F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_3F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10383_O" deadCode="false" sourceNode="P_1F10383" targetNode="P_7F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_3F10383"/>
	</edges>
	<edges id="P_2F10383P_3F10383" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10383" targetNode="P_3F10383"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10383_I" deadCode="false" sourceNode="P_4F10383" targetNode="P_8F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_11F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10383_O" deadCode="false" sourceNode="P_4F10383" targetNode="P_9F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_11F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10383_I" deadCode="false" sourceNode="P_4F10383" targetNode="P_10F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_14F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10383_O" deadCode="false" sourceNode="P_4F10383" targetNode="P_11F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_14F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10383_I" deadCode="false" sourceNode="P_4F10383" targetNode="P_12F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_15F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10383_O" deadCode="false" sourceNode="P_4F10383" targetNode="P_13F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_15F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10383_I" deadCode="false" sourceNode="P_4F10383" targetNode="P_14F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_17F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10383_O" deadCode="false" sourceNode="P_4F10383" targetNode="P_15F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_17F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10383_I" deadCode="false" sourceNode="P_4F10383" targetNode="P_16F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_20F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10383_O" deadCode="false" sourceNode="P_4F10383" targetNode="P_17F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_20F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10383_I" deadCode="false" sourceNode="P_4F10383" targetNode="P_18F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_22F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10383_O" deadCode="false" sourceNode="P_4F10383" targetNode="P_19F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_22F10383"/>
	</edges>
	<edges id="P_4F10383P_5F10383" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10383" targetNode="P_5F10383"/>
	<edges id="P_8F10383P_9F10383" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10383" targetNode="P_9F10383"/>
	<edges id="P_12F10383P_13F10383" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10383" targetNode="P_13F10383"/>
	<edges id="P_18F10383P_19F10383" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10383" targetNode="P_19F10383"/>
	<edges id="P_20F10383P_21F10383" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10383" targetNode="P_21F10383"/>
	<edges id="P_14F10383P_15F10383" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10383" targetNode="P_15F10383"/>
	<edges id="P_16F10383P_17F10383" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10383" targetNode="P_17F10383"/>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10383_I" deadCode="false" sourceNode="P_10F10383" targetNode="P_22F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_98F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10383_O" deadCode="false" sourceNode="P_10F10383" targetNode="P_23F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_98F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10383_I" deadCode="false" sourceNode="P_10F10383" targetNode="P_24F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_104F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10383_O" deadCode="false" sourceNode="P_10F10383" targetNode="P_25F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_104F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10383_I" deadCode="false" sourceNode="P_10F10383" targetNode="P_26F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_105F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10383_O" deadCode="false" sourceNode="P_10F10383" targetNode="P_27F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_105F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10383_I" deadCode="false" sourceNode="P_10F10383" targetNode="P_20F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_106F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10383_O" deadCode="false" sourceNode="P_10F10383" targetNode="P_21F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_106F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10383_I" deadCode="false" sourceNode="P_10F10383" targetNode="P_20F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_107F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10383_O" deadCode="false" sourceNode="P_10F10383" targetNode="P_21F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_107F10383"/>
	</edges>
	<edges id="P_10F10383P_11F10383" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10383" targetNode="P_11F10383"/>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10383_I" deadCode="false" sourceNode="P_22F10383" targetNode="P_28F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_115F10383"/>
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_135F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10383_O" deadCode="false" sourceNode="P_22F10383" targetNode="P_29F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_115F10383"/>
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_135F10383"/>
	</edges>
	<edges id="P_22F10383P_23F10383" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10383" targetNode="P_23F10383"/>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10383_I" deadCode="false" sourceNode="P_26F10383" targetNode="P_30F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_153F10383"/>
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_167F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10383_O" deadCode="false" sourceNode="P_26F10383" targetNode="P_31F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_153F10383"/>
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_167F10383"/>
	</edges>
	<edges id="P_26F10383P_27F10383" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10383" targetNode="P_27F10383"/>
	<edges id="P_28F10383P_29F10383" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10383" targetNode="P_29F10383"/>
	<edges id="P_30F10383P_31F10383" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10383" targetNode="P_31F10383"/>
	<edges xsi:type="cbl:PerformEdge" id="S_300F10383_I" deadCode="true" sourceNode="P_24F10383" targetNode="P_32F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_300F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_300F10383_O" deadCode="true" sourceNode="P_24F10383" targetNode="P_33F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_300F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10383_I" deadCode="true" sourceNode="P_24F10383" targetNode="P_34F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_301F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10383_O" deadCode="true" sourceNode="P_24F10383" targetNode="P_35F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_301F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_302F10383_I" deadCode="false" sourceNode="P_24F10383" targetNode="P_36F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_302F10383"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_302F10383_O" deadCode="false" sourceNode="P_24F10383" targetNode="P_37F10383">
		<representations href="../../../cobol/LVVS3140.cbl.cobModel#S_302F10383"/>
	</edges>
	<edges id="P_24F10383P_25F10383" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10383" targetNode="P_25F10383"/>
	<edges id="P_36F10383P_37F10383" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10383" targetNode="P_37F10383"/>
	<edges id="P_32F10383P_33F10383" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10383" targetNode="P_33F10383"/>
	<edges id="P_34F10383P_35F10383" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10383" targetNode="P_35F10383"/>
	<edges xsi:type="cbl:CallEdge" id="S_30F10383" deadCode="false" name="Dynamic WK-PGM-CALLED" sourceNode="P_8F10383" targetNode="IDBSPOL0">
		<representations href="../../../cobol/../importantStmts.cobModel#S_30F10383"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_57F10383" deadCode="false" name="Dynamic LDBSH650" sourceNode="P_12F10383" targetNode="LDBSH650">
		<representations href="../../../cobol/../importantStmts.cobModel#S_57F10383"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_123F10383" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_22F10383" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_123F10383"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_160F10383" deadCode="false" name="Dynamic IDBSP610" sourceNode="P_26F10383" targetNode="IDBSP610">
		<representations href="../../../cobol/../importantStmts.cobModel#S_160F10383"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_183F10383" deadCode="false" name="Dynamic LDBS6040" sourceNode="P_28F10383" targetNode="LDBS6040">
		<representations href="../../../cobol/../importantStmts.cobModel#S_183F10383"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IDBSPLI0" cbl:id="IDBSPLI0" xsi:id="IDBSPLI0" packageRef="IDBSPLI0.igd#IDBSPLI0" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IDBSPLI0_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10075" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#SC_1F10075"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10075" deadCode="false" name="PROGRAM_IDBSPLI0_FIRST_SENTENCES">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_1F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10075" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_2F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10075" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_3F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10075" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_28F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10075" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_29F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10075" deadCode="false" name="A200-ELABORA-PK">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_24F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10075" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_25F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10075" deadCode="false" name="A300-ELABORA-ID-EFF">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_4F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10075" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_5F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10075" deadCode="false" name="A400-ELABORA-IDP-EFF">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_6F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10075" deadCode="false" name="A400-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_7F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10075" deadCode="false" name="A500-ELABORA-IBO">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_8F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10075" deadCode="false" name="A500-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_9F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10075" deadCode="false" name="A600-ELABORA-IBS">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_10F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10075" deadCode="false" name="A600-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_11F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10075" deadCode="false" name="A700-ELABORA-IDO">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_12F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10075" deadCode="false" name="A700-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_13F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10075" deadCode="false" name="B300-ELABORA-ID-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_14F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10075" deadCode="false" name="B300-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_15F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10075" deadCode="false" name="B400-ELABORA-IDP-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_16F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10075" deadCode="false" name="B400-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_17F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10075" deadCode="false" name="B500-ELABORA-IBO-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_18F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10075" deadCode="false" name="B500-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_19F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10075" deadCode="false" name="B600-ELABORA-IBS-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_20F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10075" deadCode="false" name="B600-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_21F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10075" deadCode="false" name="B700-ELABORA-IDO-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_22F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10075" deadCode="false" name="B700-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_23F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10075" deadCode="false" name="A210-SELECT-PK">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_30F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10075" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_31F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10075" deadCode="false" name="A220-INSERT-PK">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_32F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10075" deadCode="false" name="A220-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_33F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10075" deadCode="false" name="A230-UPDATE-PK">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_34F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10075" deadCode="false" name="A230-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_35F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10075" deadCode="false" name="A240-DELETE-PK">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_36F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10075" deadCode="false" name="A240-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_37F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_142F10075" deadCode="false" name="A305-DECLARE-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_142F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_143F10075" deadCode="false" name="A305-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_143F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10075" deadCode="false" name="A310-SELECT-ID-EFF">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_38F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10075" deadCode="false" name="A310-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_39F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_144F10075" deadCode="false" name="A330-UPDATE-ID-EFF">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_144F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_145F10075" deadCode="false" name="A330-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_145F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_146F10075" deadCode="false" name="A360-OPEN-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_146F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_147F10075" deadCode="false" name="A360-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_147F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_148F10075" deadCode="false" name="A370-CLOSE-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_148F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_149F10075" deadCode="false" name="A370-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_149F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_150F10075" deadCode="true" name="A380-FETCH-FIRST-ID-EFF">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_150F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_153F10075" deadCode="true" name="A380-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_153F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_151F10075" deadCode="false" name="A390-FETCH-NEXT-ID-EFF">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_151F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_152F10075" deadCode="false" name="A390-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_152F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_154F10075" deadCode="false" name="A405-DECLARE-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_154F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_155F10075" deadCode="false" name="A405-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_155F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10075" deadCode="false" name="A410-SELECT-IDP-EFF">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_44F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10075" deadCode="false" name="A410-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_45F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10075" deadCode="false" name="A460-OPEN-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_46F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10075" deadCode="false" name="A460-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_47F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10075" deadCode="false" name="A470-CLOSE-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_48F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10075" deadCode="false" name="A470-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_49F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10075" deadCode="false" name="A480-FETCH-FIRST-IDP-EFF">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_50F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10075" deadCode="false" name="A480-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_51F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10075" deadCode="false" name="A490-FETCH-NEXT-IDP-EFF">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_52F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10075" deadCode="false" name="A490-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_53F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_156F10075" deadCode="false" name="A505-DECLARE-CURSOR-IBO">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_156F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_157F10075" deadCode="false" name="A505-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_157F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10075" deadCode="false" name="A510-SELECT-IBO">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_54F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10075" deadCode="false" name="A510-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_55F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10075" deadCode="false" name="A560-OPEN-CURSOR-IBO">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_56F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10075" deadCode="false" name="A560-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_57F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10075" deadCode="false" name="A570-CLOSE-CURSOR-IBO">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_58F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10075" deadCode="false" name="A570-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_59F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10075" deadCode="false" name="A580-FETCH-FIRST-IBO">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_60F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10075" deadCode="false" name="A580-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_61F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10075" deadCode="false" name="A590-FETCH-NEXT-IBO">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_62F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10075" deadCode="false" name="A590-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_63F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_158F10075" deadCode="false" name="A605-DECLARE-CURSOR-IBS">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_158F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_159F10075" deadCode="false" name="A605-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_159F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10075" deadCode="false" name="A610-SELECT-IBS">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_64F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10075" deadCode="false" name="A610-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_65F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10075" deadCode="false" name="A660-OPEN-CURSOR-IBS">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_66F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10075" deadCode="false" name="A660-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_67F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10075" deadCode="false" name="A670-CLOSE-CURSOR-IBS">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_68F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10075" deadCode="false" name="A670-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_69F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10075" deadCode="false" name="A680-FETCH-FIRST-IBS">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_70F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10075" deadCode="false" name="A680-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_71F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10075" deadCode="false" name="A690-FETCH-NEXT-IBS">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_72F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10075" deadCode="false" name="A690-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_73F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_160F10075" deadCode="false" name="A705-DECLARE-CURSOR-IDO">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_160F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_161F10075" deadCode="false" name="A705-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_161F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10075" deadCode="false" name="A710-SELECT-IDO">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_74F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10075" deadCode="false" name="A710-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_75F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10075" deadCode="false" name="A760-OPEN-CURSOR-IDO">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_76F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10075" deadCode="false" name="A760-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_77F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_78F10075" deadCode="false" name="A770-CLOSE-CURSOR-IDO">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_78F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_79F10075" deadCode="false" name="A770-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_79F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_80F10075" deadCode="false" name="A780-FETCH-FIRST-IDO">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_80F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_81F10075" deadCode="false" name="A780-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_81F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_82F10075" deadCode="false" name="A790-FETCH-NEXT-IDO">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_82F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_83F10075" deadCode="false" name="A790-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_83F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_84F10075" deadCode="false" name="B310-SELECT-ID-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_84F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_85F10075" deadCode="false" name="B310-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_85F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_162F10075" deadCode="false" name="B405-DECLARE-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_162F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_163F10075" deadCode="false" name="B405-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_163F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_86F10075" deadCode="false" name="B410-SELECT-IDP-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_86F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_87F10075" deadCode="false" name="B410-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_87F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_88F10075" deadCode="false" name="B460-OPEN-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_88F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_89F10075" deadCode="false" name="B460-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_89F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_90F10075" deadCode="false" name="B470-CLOSE-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_90F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_91F10075" deadCode="false" name="B470-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_91F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_92F10075" deadCode="false" name="B480-FETCH-FIRST-IDP-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_92F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_93F10075" deadCode="false" name="B480-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_93F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_94F10075" deadCode="false" name="B490-FETCH-NEXT-IDP-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_94F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_95F10075" deadCode="false" name="B490-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_95F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_164F10075" deadCode="false" name="B505-DECLARE-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_164F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_165F10075" deadCode="false" name="B505-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_165F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_96F10075" deadCode="false" name="B510-SELECT-IBO-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_96F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_97F10075" deadCode="false" name="B510-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_97F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_98F10075" deadCode="false" name="B560-OPEN-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_98F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_99F10075" deadCode="false" name="B560-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_99F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_100F10075" deadCode="false" name="B570-CLOSE-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_100F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_101F10075" deadCode="false" name="B570-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_101F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_102F10075" deadCode="false" name="B580-FETCH-FIRST-IBO-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_102F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_103F10075" deadCode="false" name="B580-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_103F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_104F10075" deadCode="false" name="B590-FETCH-NEXT-IBO-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_104F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_105F10075" deadCode="false" name="B590-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_105F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_166F10075" deadCode="false" name="B605-DECLARE-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_166F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_167F10075" deadCode="false" name="B605-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_167F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_106F10075" deadCode="false" name="B610-SELECT-IBS-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_106F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_107F10075" deadCode="false" name="B610-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_107F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_108F10075" deadCode="false" name="B660-OPEN-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_108F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_109F10075" deadCode="false" name="B660-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_109F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_110F10075" deadCode="false" name="B670-CLOSE-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_110F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_111F10075" deadCode="false" name="B670-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_111F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_112F10075" deadCode="false" name="B680-FETCH-FIRST-IBS-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_112F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_113F10075" deadCode="false" name="B680-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_113F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_114F10075" deadCode="false" name="B690-FETCH-NEXT-IBS-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_114F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_115F10075" deadCode="false" name="B690-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_115F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_168F10075" deadCode="false" name="B705-DECLARE-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_168F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_169F10075" deadCode="false" name="B705-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_169F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_116F10075" deadCode="false" name="B710-SELECT-IDO-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_116F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_117F10075" deadCode="false" name="B710-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_117F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_118F10075" deadCode="false" name="B760-OPEN-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_118F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_119F10075" deadCode="false" name="B760-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_119F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_120F10075" deadCode="false" name="B770-CLOSE-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_120F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_121F10075" deadCode="false" name="B770-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_121F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_122F10075" deadCode="false" name="B780-FETCH-FIRST-IDO-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_122F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_123F10075" deadCode="false" name="B780-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_123F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_124F10075" deadCode="false" name="B790-FETCH-NEXT-IDO-CPZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_124F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_125F10075" deadCode="false" name="B790-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_125F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_128F10075" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_128F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_129F10075" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_129F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_134F10075" deadCode="false" name="Z150-VALORIZZA-DATA-SERVICES-I">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_134F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_135F10075" deadCode="false" name="Z150-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_135F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_140F10075" deadCode="false" name="Z160-VALORIZZA-DATA-SERVICES-U">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_140F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_141F10075" deadCode="false" name="Z160-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_141F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_136F10075" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_136F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_137F10075" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_137F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_132F10075" deadCode="false" name="Z400-SEQ-RIGA">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_132F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_133F10075" deadCode="false" name="Z400-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_133F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10075" deadCode="false" name="Z500-AGGIORNAMENTO-STORICO">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_40F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10075" deadCode="false" name="Z500-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_41F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10075" deadCode="false" name="Z550-AGG-STORICO-SOLO-INS">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_42F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10075" deadCode="false" name="Z550-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_43F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_170F10075" deadCode="false" name="Z600-INSERT-NUOVA-RIGA-STORICA">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_170F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_171F10075" deadCode="false" name="Z600-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_171F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_138F10075" deadCode="false" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_138F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_139F10075" deadCode="false" name="Z900-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_139F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_130F10075" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_130F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_131F10075" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_131F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_126F10075" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_126F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_127F10075" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_127F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10075" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_26F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10075" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_27F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_176F10075" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_176F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_177F10075" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_177F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_178F10075" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_178F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_179F10075" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_179F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_172F10075" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_172F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_173F10075" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_173F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_180F10075" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_180F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_181F10075" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_181F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_174F10075" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_174F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_175F10075" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_175F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_182F10075" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_182F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_183F10075" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_183F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_184F10075" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_184F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_185F10075" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_185F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_186F10075" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_186F10075"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_187F10075" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#P_187F10075"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_PERC_LIQ" name="PERC_LIQ">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_PERC_LIQ"/>
		</children>
	</packageNode>
	<edges id="SC_1F10075P_1F10075" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10075" targetNode="P_1F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10075_I" deadCode="false" sourceNode="P_1F10075" targetNode="P_2F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_1F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10075_O" deadCode="false" sourceNode="P_1F10075" targetNode="P_3F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_1F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10075_I" deadCode="false" sourceNode="P_1F10075" targetNode="P_4F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_5F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10075_O" deadCode="false" sourceNode="P_1F10075" targetNode="P_5F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_5F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10075_I" deadCode="false" sourceNode="P_1F10075" targetNode="P_6F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_6F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10075_O" deadCode="false" sourceNode="P_1F10075" targetNode="P_7F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_6F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10075_I" deadCode="false" sourceNode="P_1F10075" targetNode="P_8F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_7F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10075_O" deadCode="false" sourceNode="P_1F10075" targetNode="P_9F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_7F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10075_I" deadCode="false" sourceNode="P_1F10075" targetNode="P_10F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_8F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10075_O" deadCode="false" sourceNode="P_1F10075" targetNode="P_11F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_8F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10075_I" deadCode="false" sourceNode="P_1F10075" targetNode="P_12F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_9F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10075_O" deadCode="false" sourceNode="P_1F10075" targetNode="P_13F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_9F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10075_I" deadCode="false" sourceNode="P_1F10075" targetNode="P_14F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_13F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10075_O" deadCode="false" sourceNode="P_1F10075" targetNode="P_15F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_13F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10075_I" deadCode="false" sourceNode="P_1F10075" targetNode="P_16F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_14F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10075_O" deadCode="false" sourceNode="P_1F10075" targetNode="P_17F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_14F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10075_I" deadCode="false" sourceNode="P_1F10075" targetNode="P_18F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_15F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10075_O" deadCode="false" sourceNode="P_1F10075" targetNode="P_19F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_15F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10075_I" deadCode="false" sourceNode="P_1F10075" targetNode="P_20F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_16F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10075_O" deadCode="false" sourceNode="P_1F10075" targetNode="P_21F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_16F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10075_I" deadCode="false" sourceNode="P_1F10075" targetNode="P_22F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_17F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10075_O" deadCode="false" sourceNode="P_1F10075" targetNode="P_23F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_17F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10075_I" deadCode="false" sourceNode="P_1F10075" targetNode="P_24F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_21F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10075_O" deadCode="false" sourceNode="P_1F10075" targetNode="P_25F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_21F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10075_I" deadCode="false" sourceNode="P_1F10075" targetNode="P_8F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_22F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10075_O" deadCode="false" sourceNode="P_1F10075" targetNode="P_9F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_22F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10075_I" deadCode="false" sourceNode="P_1F10075" targetNode="P_10F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_23F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10075_O" deadCode="false" sourceNode="P_1F10075" targetNode="P_11F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_23F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10075_I" deadCode="false" sourceNode="P_1F10075" targetNode="P_12F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_24F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10075_O" deadCode="false" sourceNode="P_1F10075" targetNode="P_13F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_24F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10075_I" deadCode="false" sourceNode="P_2F10075" targetNode="P_26F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_33F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10075_O" deadCode="false" sourceNode="P_2F10075" targetNode="P_27F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_33F10075"/>
	</edges>
	<edges id="P_2F10075P_3F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10075" targetNode="P_3F10075"/>
	<edges id="P_28F10075P_29F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10075" targetNode="P_29F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10075_I" deadCode="false" sourceNode="P_24F10075" targetNode="P_30F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_46F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10075_O" deadCode="false" sourceNode="P_24F10075" targetNode="P_31F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_46F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10075_I" deadCode="false" sourceNode="P_24F10075" targetNode="P_32F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_47F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10075_O" deadCode="false" sourceNode="P_24F10075" targetNode="P_33F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_47F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10075_I" deadCode="false" sourceNode="P_24F10075" targetNode="P_34F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_48F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10075_O" deadCode="false" sourceNode="P_24F10075" targetNode="P_35F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_48F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10075_I" deadCode="false" sourceNode="P_24F10075" targetNode="P_36F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_49F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10075_O" deadCode="false" sourceNode="P_24F10075" targetNode="P_37F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_49F10075"/>
	</edges>
	<edges id="P_24F10075P_25F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10075" targetNode="P_25F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10075_I" deadCode="false" sourceNode="P_4F10075" targetNode="P_38F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_53F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10075_O" deadCode="false" sourceNode="P_4F10075" targetNode="P_39F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_53F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10075_I" deadCode="false" sourceNode="P_4F10075" targetNode="P_40F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_54F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10075_O" deadCode="false" sourceNode="P_4F10075" targetNode="P_41F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_54F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10075_I" deadCode="false" sourceNode="P_4F10075" targetNode="P_42F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_55F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10075_O" deadCode="false" sourceNode="P_4F10075" targetNode="P_43F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_55F10075"/>
	</edges>
	<edges id="P_4F10075P_5F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10075" targetNode="P_5F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10075_I" deadCode="false" sourceNode="P_6F10075" targetNode="P_44F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_59F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10075_O" deadCode="false" sourceNode="P_6F10075" targetNode="P_45F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_59F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10075_I" deadCode="false" sourceNode="P_6F10075" targetNode="P_46F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_60F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10075_O" deadCode="false" sourceNode="P_6F10075" targetNode="P_47F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_60F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10075_I" deadCode="false" sourceNode="P_6F10075" targetNode="P_48F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_61F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10075_O" deadCode="false" sourceNode="P_6F10075" targetNode="P_49F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_61F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10075_I" deadCode="false" sourceNode="P_6F10075" targetNode="P_50F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_62F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10075_O" deadCode="false" sourceNode="P_6F10075" targetNode="P_51F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_62F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10075_I" deadCode="false" sourceNode="P_6F10075" targetNode="P_52F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_63F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10075_O" deadCode="false" sourceNode="P_6F10075" targetNode="P_53F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_63F10075"/>
	</edges>
	<edges id="P_6F10075P_7F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10075" targetNode="P_7F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10075_I" deadCode="false" sourceNode="P_8F10075" targetNode="P_54F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_67F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10075_O" deadCode="false" sourceNode="P_8F10075" targetNode="P_55F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_67F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10075_I" deadCode="false" sourceNode="P_8F10075" targetNode="P_56F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_68F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10075_O" deadCode="false" sourceNode="P_8F10075" targetNode="P_57F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_68F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10075_I" deadCode="false" sourceNode="P_8F10075" targetNode="P_58F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_69F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10075_O" deadCode="false" sourceNode="P_8F10075" targetNode="P_59F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_69F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10075_I" deadCode="false" sourceNode="P_8F10075" targetNode="P_60F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_70F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10075_O" deadCode="false" sourceNode="P_8F10075" targetNode="P_61F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_70F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10075_I" deadCode="false" sourceNode="P_8F10075" targetNode="P_62F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_71F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10075_O" deadCode="false" sourceNode="P_8F10075" targetNode="P_63F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_71F10075"/>
	</edges>
	<edges id="P_8F10075P_9F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10075" targetNode="P_9F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10075_I" deadCode="false" sourceNode="P_10F10075" targetNode="P_64F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_75F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10075_O" deadCode="false" sourceNode="P_10F10075" targetNode="P_65F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_75F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10075_I" deadCode="false" sourceNode="P_10F10075" targetNode="P_66F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_76F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10075_O" deadCode="false" sourceNode="P_10F10075" targetNode="P_67F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_76F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10075_I" deadCode="false" sourceNode="P_10F10075" targetNode="P_68F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_77F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10075_O" deadCode="false" sourceNode="P_10F10075" targetNode="P_69F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_77F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10075_I" deadCode="false" sourceNode="P_10F10075" targetNode="P_70F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_78F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10075_O" deadCode="false" sourceNode="P_10F10075" targetNode="P_71F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_78F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10075_I" deadCode="false" sourceNode="P_10F10075" targetNode="P_72F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_79F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10075_O" deadCode="false" sourceNode="P_10F10075" targetNode="P_73F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_79F10075"/>
	</edges>
	<edges id="P_10F10075P_11F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10075" targetNode="P_11F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10075_I" deadCode="false" sourceNode="P_12F10075" targetNode="P_74F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_83F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10075_O" deadCode="false" sourceNode="P_12F10075" targetNode="P_75F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_83F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10075_I" deadCode="false" sourceNode="P_12F10075" targetNode="P_76F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_84F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10075_O" deadCode="false" sourceNode="P_12F10075" targetNode="P_77F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_84F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10075_I" deadCode="false" sourceNode="P_12F10075" targetNode="P_78F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_85F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10075_O" deadCode="false" sourceNode="P_12F10075" targetNode="P_79F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_85F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10075_I" deadCode="false" sourceNode="P_12F10075" targetNode="P_80F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_86F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10075_O" deadCode="false" sourceNode="P_12F10075" targetNode="P_81F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_86F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10075_I" deadCode="false" sourceNode="P_12F10075" targetNode="P_82F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_87F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10075_O" deadCode="false" sourceNode="P_12F10075" targetNode="P_83F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_87F10075"/>
	</edges>
	<edges id="P_12F10075P_13F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10075" targetNode="P_13F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10075_I" deadCode="false" sourceNode="P_14F10075" targetNode="P_84F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_91F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10075_O" deadCode="false" sourceNode="P_14F10075" targetNode="P_85F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_91F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10075_I" deadCode="false" sourceNode="P_14F10075" targetNode="P_40F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_92F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10075_O" deadCode="false" sourceNode="P_14F10075" targetNode="P_41F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_92F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10075_I" deadCode="false" sourceNode="P_14F10075" targetNode="P_42F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_93F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10075_O" deadCode="false" sourceNode="P_14F10075" targetNode="P_43F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_93F10075"/>
	</edges>
	<edges id="P_14F10075P_15F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10075" targetNode="P_15F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10075_I" deadCode="false" sourceNode="P_16F10075" targetNode="P_86F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_97F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10075_O" deadCode="false" sourceNode="P_16F10075" targetNode="P_87F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_97F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10075_I" deadCode="false" sourceNode="P_16F10075" targetNode="P_88F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_98F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10075_O" deadCode="false" sourceNode="P_16F10075" targetNode="P_89F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_98F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10075_I" deadCode="false" sourceNode="P_16F10075" targetNode="P_90F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_99F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10075_O" deadCode="false" sourceNode="P_16F10075" targetNode="P_91F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_99F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10075_I" deadCode="false" sourceNode="P_16F10075" targetNode="P_92F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_100F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10075_O" deadCode="false" sourceNode="P_16F10075" targetNode="P_93F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_100F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10075_I" deadCode="false" sourceNode="P_16F10075" targetNode="P_94F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_101F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10075_O" deadCode="false" sourceNode="P_16F10075" targetNode="P_95F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_101F10075"/>
	</edges>
	<edges id="P_16F10075P_17F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10075" targetNode="P_17F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10075_I" deadCode="false" sourceNode="P_18F10075" targetNode="P_96F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_105F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10075_O" deadCode="false" sourceNode="P_18F10075" targetNode="P_97F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_105F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10075_I" deadCode="false" sourceNode="P_18F10075" targetNode="P_98F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_106F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10075_O" deadCode="false" sourceNode="P_18F10075" targetNode="P_99F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_106F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10075_I" deadCode="false" sourceNode="P_18F10075" targetNode="P_100F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_107F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10075_O" deadCode="false" sourceNode="P_18F10075" targetNode="P_101F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_107F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10075_I" deadCode="false" sourceNode="P_18F10075" targetNode="P_102F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_108F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10075_O" deadCode="false" sourceNode="P_18F10075" targetNode="P_103F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_108F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10075_I" deadCode="false" sourceNode="P_18F10075" targetNode="P_104F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_109F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10075_O" deadCode="false" sourceNode="P_18F10075" targetNode="P_105F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_109F10075"/>
	</edges>
	<edges id="P_18F10075P_19F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10075" targetNode="P_19F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10075_I" deadCode="false" sourceNode="P_20F10075" targetNode="P_106F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_113F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10075_O" deadCode="false" sourceNode="P_20F10075" targetNode="P_107F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_113F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10075_I" deadCode="false" sourceNode="P_20F10075" targetNode="P_108F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_114F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10075_O" deadCode="false" sourceNode="P_20F10075" targetNode="P_109F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_114F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10075_I" deadCode="false" sourceNode="P_20F10075" targetNode="P_110F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_115F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10075_O" deadCode="false" sourceNode="P_20F10075" targetNode="P_111F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_115F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10075_I" deadCode="false" sourceNode="P_20F10075" targetNode="P_112F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_116F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10075_O" deadCode="false" sourceNode="P_20F10075" targetNode="P_113F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_116F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10075_I" deadCode="false" sourceNode="P_20F10075" targetNode="P_114F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_117F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10075_O" deadCode="false" sourceNode="P_20F10075" targetNode="P_115F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_117F10075"/>
	</edges>
	<edges id="P_20F10075P_21F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10075" targetNode="P_21F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10075_I" deadCode="false" sourceNode="P_22F10075" targetNode="P_116F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_121F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10075_O" deadCode="false" sourceNode="P_22F10075" targetNode="P_117F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_121F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10075_I" deadCode="false" sourceNode="P_22F10075" targetNode="P_118F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_122F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10075_O" deadCode="false" sourceNode="P_22F10075" targetNode="P_119F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_122F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10075_I" deadCode="false" sourceNode="P_22F10075" targetNode="P_120F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_123F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10075_O" deadCode="false" sourceNode="P_22F10075" targetNode="P_121F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_123F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10075_I" deadCode="false" sourceNode="P_22F10075" targetNode="P_122F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_124F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10075_O" deadCode="false" sourceNode="P_22F10075" targetNode="P_123F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_124F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10075_I" deadCode="false" sourceNode="P_22F10075" targetNode="P_124F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_125F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10075_O" deadCode="false" sourceNode="P_22F10075" targetNode="P_125F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_125F10075"/>
	</edges>
	<edges id="P_22F10075P_23F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10075" targetNode="P_23F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10075_I" deadCode="false" sourceNode="P_30F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_128F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10075_O" deadCode="false" sourceNode="P_30F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_128F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10075_I" deadCode="false" sourceNode="P_30F10075" targetNode="P_28F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_130F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10075_O" deadCode="false" sourceNode="P_30F10075" targetNode="P_29F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_130F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10075_I" deadCode="false" sourceNode="P_30F10075" targetNode="P_128F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_132F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10075_O" deadCode="false" sourceNode="P_30F10075" targetNode="P_129F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_132F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10075_I" deadCode="false" sourceNode="P_30F10075" targetNode="P_130F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_133F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10075_O" deadCode="false" sourceNode="P_30F10075" targetNode="P_131F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_133F10075"/>
	</edges>
	<edges id="P_30F10075P_31F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10075" targetNode="P_31F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10075_I" deadCode="false" sourceNode="P_32F10075" targetNode="P_132F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_135F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10075_O" deadCode="false" sourceNode="P_32F10075" targetNode="P_133F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_135F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10075_I" deadCode="false" sourceNode="P_32F10075" targetNode="P_134F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_137F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10075_O" deadCode="false" sourceNode="P_32F10075" targetNode="P_135F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_137F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10075_I" deadCode="false" sourceNode="P_32F10075" targetNode="P_136F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_138F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10075_O" deadCode="false" sourceNode="P_32F10075" targetNode="P_137F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_138F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10075_I" deadCode="false" sourceNode="P_32F10075" targetNode="P_138F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_139F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10075_O" deadCode="false" sourceNode="P_32F10075" targetNode="P_139F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_139F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10075_I" deadCode="false" sourceNode="P_32F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_140F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10075_O" deadCode="false" sourceNode="P_32F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_140F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10075_I" deadCode="false" sourceNode="P_32F10075" targetNode="P_28F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_142F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10075_O" deadCode="false" sourceNode="P_32F10075" targetNode="P_29F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_142F10075"/>
	</edges>
	<edges id="P_32F10075P_33F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10075" targetNode="P_33F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10075_I" deadCode="false" sourceNode="P_34F10075" targetNode="P_140F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_144F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10075_O" deadCode="false" sourceNode="P_34F10075" targetNode="P_141F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_144F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10075_I" deadCode="false" sourceNode="P_34F10075" targetNode="P_136F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_145F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10075_O" deadCode="false" sourceNode="P_34F10075" targetNode="P_137F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_145F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10075_I" deadCode="false" sourceNode="P_34F10075" targetNode="P_138F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_146F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10075_O" deadCode="false" sourceNode="P_34F10075" targetNode="P_139F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_146F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10075_I" deadCode="false" sourceNode="P_34F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_147F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10075_O" deadCode="false" sourceNode="P_34F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_147F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10075_I" deadCode="false" sourceNode="P_34F10075" targetNode="P_28F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_149F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10075_O" deadCode="false" sourceNode="P_34F10075" targetNode="P_29F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_149F10075"/>
	</edges>
	<edges id="P_34F10075P_35F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10075" targetNode="P_35F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10075_I" deadCode="false" sourceNode="P_36F10075" targetNode="P_28F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_152F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10075_O" deadCode="false" sourceNode="P_36F10075" targetNode="P_29F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_152F10075"/>
	</edges>
	<edges id="P_36F10075P_37F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10075" targetNode="P_37F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10075_I" deadCode="false" sourceNode="P_142F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_154F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10075_O" deadCode="false" sourceNode="P_142F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_154F10075"/>
	</edges>
	<edges id="P_142F10075P_143F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_142F10075" targetNode="P_143F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10075_I" deadCode="false" sourceNode="P_38F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_158F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10075_O" deadCode="false" sourceNode="P_38F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_158F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10075_I" deadCode="false" sourceNode="P_38F10075" targetNode="P_28F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_160F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10075_O" deadCode="false" sourceNode="P_38F10075" targetNode="P_29F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_160F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10075_I" deadCode="false" sourceNode="P_38F10075" targetNode="P_128F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_162F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10075_O" deadCode="false" sourceNode="P_38F10075" targetNode="P_129F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_162F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10075_I" deadCode="false" sourceNode="P_38F10075" targetNode="P_130F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_163F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10075_O" deadCode="false" sourceNode="P_38F10075" targetNode="P_131F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_163F10075"/>
	</edges>
	<edges id="P_38F10075P_39F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10075" targetNode="P_39F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10075_I" deadCode="false" sourceNode="P_144F10075" targetNode="P_140F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_165F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10075_O" deadCode="false" sourceNode="P_144F10075" targetNode="P_141F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_165F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10075_I" deadCode="false" sourceNode="P_144F10075" targetNode="P_136F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_166F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10075_O" deadCode="false" sourceNode="P_144F10075" targetNode="P_137F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_166F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10075_I" deadCode="false" sourceNode="P_144F10075" targetNode="P_138F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_167F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10075_O" deadCode="false" sourceNode="P_144F10075" targetNode="P_139F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_167F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10075_I" deadCode="false" sourceNode="P_144F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_168F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10075_O" deadCode="false" sourceNode="P_144F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_168F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10075_I" deadCode="false" sourceNode="P_144F10075" targetNode="P_28F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_170F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10075_O" deadCode="false" sourceNode="P_144F10075" targetNode="P_29F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_170F10075"/>
	</edges>
	<edges id="P_144F10075P_145F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_144F10075" targetNode="P_145F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10075_I" deadCode="false" sourceNode="P_146F10075" targetNode="P_142F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_172F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10075_O" deadCode="false" sourceNode="P_146F10075" targetNode="P_143F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_172F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10075_I" deadCode="false" sourceNode="P_146F10075" targetNode="P_28F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_174F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10075_O" deadCode="false" sourceNode="P_146F10075" targetNode="P_29F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_174F10075"/>
	</edges>
	<edges id="P_146F10075P_147F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_146F10075" targetNode="P_147F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10075_I" deadCode="false" sourceNode="P_148F10075" targetNode="P_28F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_177F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10075_O" deadCode="false" sourceNode="P_148F10075" targetNode="P_29F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_177F10075"/>
	</edges>
	<edges id="P_148F10075P_149F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_148F10075" targetNode="P_149F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10075_I" deadCode="true" sourceNode="P_150F10075" targetNode="P_146F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_179F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10075_O" deadCode="true" sourceNode="P_150F10075" targetNode="P_147F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_179F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10075_I" deadCode="true" sourceNode="P_150F10075" targetNode="P_151F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_181F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10075_O" deadCode="true" sourceNode="P_150F10075" targetNode="P_152F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_181F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10075_I" deadCode="false" sourceNode="P_151F10075" targetNode="P_28F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_184F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10075_O" deadCode="false" sourceNode="P_151F10075" targetNode="P_29F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_184F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10075_I" deadCode="false" sourceNode="P_151F10075" targetNode="P_128F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_186F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10075_O" deadCode="false" sourceNode="P_151F10075" targetNode="P_129F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_186F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10075_I" deadCode="false" sourceNode="P_151F10075" targetNode="P_130F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_187F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10075_O" deadCode="false" sourceNode="P_151F10075" targetNode="P_131F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_187F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10075_I" deadCode="false" sourceNode="P_151F10075" targetNode="P_148F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_189F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10075_O" deadCode="false" sourceNode="P_151F10075" targetNode="P_149F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_189F10075"/>
	</edges>
	<edges id="P_151F10075P_152F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_151F10075" targetNode="P_152F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_193F10075_I" deadCode="false" sourceNode="P_154F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_193F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_193F10075_O" deadCode="false" sourceNode="P_154F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_193F10075"/>
	</edges>
	<edges id="P_154F10075P_155F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_154F10075" targetNode="P_155F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_197F10075_I" deadCode="false" sourceNode="P_44F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_197F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_197F10075_O" deadCode="false" sourceNode="P_44F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_197F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_199F10075_I" deadCode="false" sourceNode="P_44F10075" targetNode="P_28F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_199F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_199F10075_O" deadCode="false" sourceNode="P_44F10075" targetNode="P_29F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_199F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_201F10075_I" deadCode="false" sourceNode="P_44F10075" targetNode="P_128F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_201F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_201F10075_O" deadCode="false" sourceNode="P_44F10075" targetNode="P_129F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_201F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10075_I" deadCode="false" sourceNode="P_44F10075" targetNode="P_130F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_202F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10075_O" deadCode="false" sourceNode="P_44F10075" targetNode="P_131F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_202F10075"/>
	</edges>
	<edges id="P_44F10075P_45F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10075" targetNode="P_45F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_204F10075_I" deadCode="false" sourceNode="P_46F10075" targetNode="P_154F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_204F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_204F10075_O" deadCode="false" sourceNode="P_46F10075" targetNode="P_155F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_204F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_206F10075_I" deadCode="false" sourceNode="P_46F10075" targetNode="P_28F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_206F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_206F10075_O" deadCode="false" sourceNode="P_46F10075" targetNode="P_29F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_206F10075"/>
	</edges>
	<edges id="P_46F10075P_47F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10075" targetNode="P_47F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_209F10075_I" deadCode="false" sourceNode="P_48F10075" targetNode="P_28F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_209F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_209F10075_O" deadCode="false" sourceNode="P_48F10075" targetNode="P_29F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_209F10075"/>
	</edges>
	<edges id="P_48F10075P_49F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10075" targetNode="P_49F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10075_I" deadCode="false" sourceNode="P_50F10075" targetNode="P_46F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_211F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10075_O" deadCode="false" sourceNode="P_50F10075" targetNode="P_47F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_211F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10075_I" deadCode="false" sourceNode="P_50F10075" targetNode="P_52F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_213F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10075_O" deadCode="false" sourceNode="P_50F10075" targetNode="P_53F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_213F10075"/>
	</edges>
	<edges id="P_50F10075P_51F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10075" targetNode="P_51F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10075_I" deadCode="false" sourceNode="P_52F10075" targetNode="P_28F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_216F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10075_O" deadCode="false" sourceNode="P_52F10075" targetNode="P_29F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_216F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_218F10075_I" deadCode="false" sourceNode="P_52F10075" targetNode="P_128F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_218F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_218F10075_O" deadCode="false" sourceNode="P_52F10075" targetNode="P_129F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_218F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_219F10075_I" deadCode="false" sourceNode="P_52F10075" targetNode="P_130F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_219F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_219F10075_O" deadCode="false" sourceNode="P_52F10075" targetNode="P_131F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_219F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10075_I" deadCode="false" sourceNode="P_52F10075" targetNode="P_48F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_221F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10075_O" deadCode="false" sourceNode="P_52F10075" targetNode="P_49F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_221F10075"/>
	</edges>
	<edges id="P_52F10075P_53F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10075" targetNode="P_53F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_225F10075_I" deadCode="false" sourceNode="P_156F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_225F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_225F10075_O" deadCode="false" sourceNode="P_156F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_225F10075"/>
	</edges>
	<edges id="P_156F10075P_157F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_156F10075" targetNode="P_157F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_228F10075_I" deadCode="false" sourceNode="P_54F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_228F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_228F10075_O" deadCode="false" sourceNode="P_54F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_228F10075"/>
	</edges>
	<edges id="P_54F10075P_55F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10075" targetNode="P_55F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10075_I" deadCode="false" sourceNode="P_56F10075" targetNode="P_156F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_231F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_231F10075_O" deadCode="false" sourceNode="P_56F10075" targetNode="P_157F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_231F10075"/>
	</edges>
	<edges id="P_56F10075P_57F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10075" targetNode="P_57F10075"/>
	<edges id="P_58F10075P_59F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10075" targetNode="P_59F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_236F10075_I" deadCode="false" sourceNode="P_60F10075" targetNode="P_56F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_236F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_236F10075_O" deadCode="false" sourceNode="P_60F10075" targetNode="P_57F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_236F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_238F10075_I" deadCode="false" sourceNode="P_60F10075" targetNode="P_62F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_238F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_238F10075_O" deadCode="false" sourceNode="P_60F10075" targetNode="P_63F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_238F10075"/>
	</edges>
	<edges id="P_60F10075P_61F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_60F10075" targetNode="P_61F10075"/>
	<edges id="P_62F10075P_63F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10075" targetNode="P_63F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10075_I" deadCode="false" sourceNode="P_158F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_242F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10075_O" deadCode="false" sourceNode="P_158F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_242F10075"/>
	</edges>
	<edges id="P_158F10075P_159F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_158F10075" targetNode="P_159F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_245F10075_I" deadCode="false" sourceNode="P_64F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_245F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_245F10075_O" deadCode="false" sourceNode="P_64F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_245F10075"/>
	</edges>
	<edges id="P_64F10075P_65F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10075" targetNode="P_65F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_248F10075_I" deadCode="false" sourceNode="P_66F10075" targetNode="P_158F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_248F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_248F10075_O" deadCode="false" sourceNode="P_66F10075" targetNode="P_159F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_248F10075"/>
	</edges>
	<edges id="P_66F10075P_67F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10075" targetNode="P_67F10075"/>
	<edges id="P_68F10075P_69F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10075" targetNode="P_69F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10075_I" deadCode="false" sourceNode="P_70F10075" targetNode="P_66F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_253F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10075_O" deadCode="false" sourceNode="P_70F10075" targetNode="P_67F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_253F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10075_I" deadCode="false" sourceNode="P_70F10075" targetNode="P_72F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_255F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10075_O" deadCode="false" sourceNode="P_70F10075" targetNode="P_73F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_255F10075"/>
	</edges>
	<edges id="P_70F10075P_71F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_70F10075" targetNode="P_71F10075"/>
	<edges id="P_72F10075P_73F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10075" targetNode="P_73F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10075_I" deadCode="false" sourceNode="P_160F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_259F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10075_O" deadCode="false" sourceNode="P_160F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_259F10075"/>
	</edges>
	<edges id="P_160F10075P_161F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_160F10075" targetNode="P_161F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10075_I" deadCode="false" sourceNode="P_74F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_262F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_262F10075_O" deadCode="false" sourceNode="P_74F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_262F10075"/>
	</edges>
	<edges id="P_74F10075P_75F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10075" targetNode="P_75F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10075_I" deadCode="false" sourceNode="P_76F10075" targetNode="P_160F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_265F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_265F10075_O" deadCode="false" sourceNode="P_76F10075" targetNode="P_161F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_265F10075"/>
	</edges>
	<edges id="P_76F10075P_77F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_76F10075" targetNode="P_77F10075"/>
	<edges id="P_78F10075P_79F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_78F10075" targetNode="P_79F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_270F10075_I" deadCode="false" sourceNode="P_80F10075" targetNode="P_76F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_270F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_270F10075_O" deadCode="false" sourceNode="P_80F10075" targetNode="P_77F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_270F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_272F10075_I" deadCode="false" sourceNode="P_80F10075" targetNode="P_82F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_272F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_272F10075_O" deadCode="false" sourceNode="P_80F10075" targetNode="P_83F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_272F10075"/>
	</edges>
	<edges id="P_80F10075P_81F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_80F10075" targetNode="P_81F10075"/>
	<edges id="P_82F10075P_83F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_82F10075" targetNode="P_83F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10075_I" deadCode="false" sourceNode="P_84F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_276F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10075_O" deadCode="false" sourceNode="P_84F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_276F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10075_I" deadCode="false" sourceNode="P_84F10075" targetNode="P_28F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_278F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10075_O" deadCode="false" sourceNode="P_84F10075" targetNode="P_29F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_278F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10075_I" deadCode="false" sourceNode="P_84F10075" targetNode="P_128F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_280F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10075_O" deadCode="false" sourceNode="P_84F10075" targetNode="P_129F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_280F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10075_I" deadCode="false" sourceNode="P_84F10075" targetNode="P_130F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_281F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10075_O" deadCode="false" sourceNode="P_84F10075" targetNode="P_131F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_281F10075"/>
	</edges>
	<edges id="P_84F10075P_85F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_84F10075" targetNode="P_85F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10075_I" deadCode="false" sourceNode="P_162F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_283F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10075_O" deadCode="false" sourceNode="P_162F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_283F10075"/>
	</edges>
	<edges id="P_162F10075P_163F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_162F10075" targetNode="P_163F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_287F10075_I" deadCode="false" sourceNode="P_86F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_287F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_287F10075_O" deadCode="false" sourceNode="P_86F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_287F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10075_I" deadCode="false" sourceNode="P_86F10075" targetNode="P_28F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_289F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_289F10075_O" deadCode="false" sourceNode="P_86F10075" targetNode="P_29F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_289F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_291F10075_I" deadCode="false" sourceNode="P_86F10075" targetNode="P_128F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_291F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_291F10075_O" deadCode="false" sourceNode="P_86F10075" targetNode="P_129F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_291F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10075_I" deadCode="false" sourceNode="P_86F10075" targetNode="P_130F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_292F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10075_O" deadCode="false" sourceNode="P_86F10075" targetNode="P_131F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_292F10075"/>
	</edges>
	<edges id="P_86F10075P_87F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_86F10075" targetNode="P_87F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10075_I" deadCode="false" sourceNode="P_88F10075" targetNode="P_162F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_294F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10075_O" deadCode="false" sourceNode="P_88F10075" targetNode="P_163F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_294F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_296F10075_I" deadCode="false" sourceNode="P_88F10075" targetNode="P_28F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_296F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_296F10075_O" deadCode="false" sourceNode="P_88F10075" targetNode="P_29F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_296F10075"/>
	</edges>
	<edges id="P_88F10075P_89F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_88F10075" targetNode="P_89F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10075_I" deadCode="false" sourceNode="P_90F10075" targetNode="P_28F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_299F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10075_O" deadCode="false" sourceNode="P_90F10075" targetNode="P_29F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_299F10075"/>
	</edges>
	<edges id="P_90F10075P_91F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_90F10075" targetNode="P_91F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10075_I" deadCode="false" sourceNode="P_92F10075" targetNode="P_88F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_301F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_301F10075_O" deadCode="false" sourceNode="P_92F10075" targetNode="P_89F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_301F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_303F10075_I" deadCode="false" sourceNode="P_92F10075" targetNode="P_94F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_303F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_303F10075_O" deadCode="false" sourceNode="P_92F10075" targetNode="P_95F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_303F10075"/>
	</edges>
	<edges id="P_92F10075P_93F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_92F10075" targetNode="P_93F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_306F10075_I" deadCode="false" sourceNode="P_94F10075" targetNode="P_28F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_306F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_306F10075_O" deadCode="false" sourceNode="P_94F10075" targetNode="P_29F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_306F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10075_I" deadCode="false" sourceNode="P_94F10075" targetNode="P_128F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_308F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10075_O" deadCode="false" sourceNode="P_94F10075" targetNode="P_129F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_308F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10075_I" deadCode="false" sourceNode="P_94F10075" targetNode="P_130F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_309F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10075_O" deadCode="false" sourceNode="P_94F10075" targetNode="P_131F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_309F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10075_I" deadCode="false" sourceNode="P_94F10075" targetNode="P_90F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_311F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10075_O" deadCode="false" sourceNode="P_94F10075" targetNode="P_91F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_311F10075"/>
	</edges>
	<edges id="P_94F10075P_95F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_94F10075" targetNode="P_95F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_315F10075_I" deadCode="false" sourceNode="P_164F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_315F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_315F10075_O" deadCode="false" sourceNode="P_164F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_315F10075"/>
	</edges>
	<edges id="P_164F10075P_165F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_164F10075" targetNode="P_165F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_318F10075_I" deadCode="false" sourceNode="P_96F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_318F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_318F10075_O" deadCode="false" sourceNode="P_96F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_318F10075"/>
	</edges>
	<edges id="P_96F10075P_97F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_96F10075" targetNode="P_97F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10075_I" deadCode="false" sourceNode="P_98F10075" targetNode="P_164F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_321F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_321F10075_O" deadCode="false" sourceNode="P_98F10075" targetNode="P_165F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_321F10075"/>
	</edges>
	<edges id="P_98F10075P_99F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_98F10075" targetNode="P_99F10075"/>
	<edges id="P_100F10075P_101F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_100F10075" targetNode="P_101F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_326F10075_I" deadCode="false" sourceNode="P_102F10075" targetNode="P_98F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_326F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_326F10075_O" deadCode="false" sourceNode="P_102F10075" targetNode="P_99F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_326F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_328F10075_I" deadCode="false" sourceNode="P_102F10075" targetNode="P_104F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_328F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_328F10075_O" deadCode="false" sourceNode="P_102F10075" targetNode="P_105F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_328F10075"/>
	</edges>
	<edges id="P_102F10075P_103F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_102F10075" targetNode="P_103F10075"/>
	<edges id="P_104F10075P_105F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_104F10075" targetNode="P_105F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_332F10075_I" deadCode="false" sourceNode="P_166F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_332F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_332F10075_O" deadCode="false" sourceNode="P_166F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_332F10075"/>
	</edges>
	<edges id="P_166F10075P_167F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_166F10075" targetNode="P_167F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_335F10075_I" deadCode="false" sourceNode="P_106F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_335F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_335F10075_O" deadCode="false" sourceNode="P_106F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_335F10075"/>
	</edges>
	<edges id="P_106F10075P_107F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_106F10075" targetNode="P_107F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10075_I" deadCode="false" sourceNode="P_108F10075" targetNode="P_166F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_338F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10075_O" deadCode="false" sourceNode="P_108F10075" targetNode="P_167F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_338F10075"/>
	</edges>
	<edges id="P_108F10075P_109F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_108F10075" targetNode="P_109F10075"/>
	<edges id="P_110F10075P_111F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_110F10075" targetNode="P_111F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_343F10075_I" deadCode="false" sourceNode="P_112F10075" targetNode="P_108F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_343F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_343F10075_O" deadCode="false" sourceNode="P_112F10075" targetNode="P_109F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_343F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_345F10075_I" deadCode="false" sourceNode="P_112F10075" targetNode="P_114F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_345F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_345F10075_O" deadCode="false" sourceNode="P_112F10075" targetNode="P_115F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_345F10075"/>
	</edges>
	<edges id="P_112F10075P_113F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_112F10075" targetNode="P_113F10075"/>
	<edges id="P_114F10075P_115F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_114F10075" targetNode="P_115F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_349F10075_I" deadCode="false" sourceNode="P_168F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_349F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_349F10075_O" deadCode="false" sourceNode="P_168F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_349F10075"/>
	</edges>
	<edges id="P_168F10075P_169F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_168F10075" targetNode="P_169F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_352F10075_I" deadCode="false" sourceNode="P_116F10075" targetNode="P_126F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_352F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_352F10075_O" deadCode="false" sourceNode="P_116F10075" targetNode="P_127F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_352F10075"/>
	</edges>
	<edges id="P_116F10075P_117F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_116F10075" targetNode="P_117F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_355F10075_I" deadCode="false" sourceNode="P_118F10075" targetNode="P_168F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_355F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_355F10075_O" deadCode="false" sourceNode="P_118F10075" targetNode="P_169F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_355F10075"/>
	</edges>
	<edges id="P_118F10075P_119F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_118F10075" targetNode="P_119F10075"/>
	<edges id="P_120F10075P_121F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_120F10075" targetNode="P_121F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_360F10075_I" deadCode="false" sourceNode="P_122F10075" targetNode="P_118F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_360F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_360F10075_O" deadCode="false" sourceNode="P_122F10075" targetNode="P_119F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_360F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_362F10075_I" deadCode="false" sourceNode="P_122F10075" targetNode="P_124F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_362F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_362F10075_O" deadCode="false" sourceNode="P_122F10075" targetNode="P_125F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_362F10075"/>
	</edges>
	<edges id="P_122F10075P_123F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_122F10075" targetNode="P_123F10075"/>
	<edges id="P_124F10075P_125F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_124F10075" targetNode="P_125F10075"/>
	<edges id="P_128F10075P_129F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_128F10075" targetNode="P_129F10075"/>
	<edges id="P_134F10075P_135F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_134F10075" targetNode="P_135F10075"/>
	<edges id="P_140F10075P_141F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_140F10075" targetNode="P_141F10075"/>
	<edges id="P_136F10075P_137F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_136F10075" targetNode="P_137F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_423F10075_I" deadCode="false" sourceNode="P_132F10075" targetNode="P_28F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_423F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_423F10075_O" deadCode="false" sourceNode="P_132F10075" targetNode="P_29F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_423F10075"/>
	</edges>
	<edges id="P_132F10075P_133F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_132F10075" targetNode="P_133F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_427F10075_I" deadCode="false" sourceNode="P_40F10075" targetNode="P_146F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_427F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_427F10075_O" deadCode="false" sourceNode="P_40F10075" targetNode="P_147F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_427F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_429F10075_I" deadCode="false" sourceNode="P_40F10075" targetNode="P_151F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_428F10075"/>
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_429F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_429F10075_O" deadCode="false" sourceNode="P_40F10075" targetNode="P_152F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_428F10075"/>
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_429F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_433F10075_I" deadCode="false" sourceNode="P_40F10075" targetNode="P_144F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_428F10075"/>
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_433F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_433F10075_O" deadCode="false" sourceNode="P_40F10075" targetNode="P_145F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_428F10075"/>
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_433F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_441F10075_I" deadCode="false" sourceNode="P_40F10075" targetNode="P_32F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_428F10075"/>
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_441F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_441F10075_O" deadCode="false" sourceNode="P_40F10075" targetNode="P_33F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_428F10075"/>
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_441F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_444F10075_I" deadCode="false" sourceNode="P_40F10075" targetNode="P_170F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_444F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_444F10075_O" deadCode="false" sourceNode="P_40F10075" targetNode="P_171F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_444F10075"/>
	</edges>
	<edges id="P_40F10075P_41F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10075" targetNode="P_41F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_449F10075_I" deadCode="false" sourceNode="P_42F10075" targetNode="P_170F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_449F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_449F10075_O" deadCode="false" sourceNode="P_42F10075" targetNode="P_171F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_449F10075"/>
	</edges>
	<edges id="P_42F10075P_43F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10075" targetNode="P_43F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_459F10075_I" deadCode="false" sourceNode="P_170F10075" targetNode="P_32F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_459F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_459F10075_O" deadCode="false" sourceNode="P_170F10075" targetNode="P_33F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_459F10075"/>
	</edges>
	<edges id="P_170F10075P_171F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_170F10075" targetNode="P_171F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_462F10075_I" deadCode="false" sourceNode="P_138F10075" targetNode="P_172F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_462F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_462F10075_O" deadCode="false" sourceNode="P_138F10075" targetNode="P_173F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_462F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_465F10075_I" deadCode="false" sourceNode="P_138F10075" targetNode="P_172F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_465F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_465F10075_O" deadCode="false" sourceNode="P_138F10075" targetNode="P_173F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_465F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_469F10075_I" deadCode="false" sourceNode="P_138F10075" targetNode="P_172F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_469F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_469F10075_O" deadCode="false" sourceNode="P_138F10075" targetNode="P_173F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_469F10075"/>
	</edges>
	<edges id="P_138F10075P_139F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_138F10075" targetNode="P_139F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_473F10075_I" deadCode="false" sourceNode="P_130F10075" targetNode="P_174F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_473F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_473F10075_O" deadCode="false" sourceNode="P_130F10075" targetNode="P_175F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_473F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_476F10075_I" deadCode="false" sourceNode="P_130F10075" targetNode="P_174F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_476F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_476F10075_O" deadCode="false" sourceNode="P_130F10075" targetNode="P_175F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_476F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_480F10075_I" deadCode="false" sourceNode="P_130F10075" targetNode="P_174F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_480F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_480F10075_O" deadCode="false" sourceNode="P_130F10075" targetNode="P_175F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_480F10075"/>
	</edges>
	<edges id="P_130F10075P_131F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_130F10075" targetNode="P_131F10075"/>
	<edges id="P_126F10075P_127F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_126F10075" targetNode="P_127F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_485F10075_I" deadCode="false" sourceNode="P_26F10075" targetNode="P_176F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_485F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_485F10075_O" deadCode="false" sourceNode="P_26F10075" targetNode="P_177F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_485F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_487F10075_I" deadCode="false" sourceNode="P_26F10075" targetNode="P_178F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_487F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_487F10075_O" deadCode="false" sourceNode="P_26F10075" targetNode="P_179F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_487F10075"/>
	</edges>
	<edges id="P_26F10075P_27F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10075" targetNode="P_27F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_492F10075_I" deadCode="false" sourceNode="P_176F10075" targetNode="P_172F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_492F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_492F10075_O" deadCode="false" sourceNode="P_176F10075" targetNode="P_173F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_492F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_497F10075_I" deadCode="false" sourceNode="P_176F10075" targetNode="P_172F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_497F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_497F10075_O" deadCode="false" sourceNode="P_176F10075" targetNode="P_173F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_497F10075"/>
	</edges>
	<edges id="P_176F10075P_177F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_176F10075" targetNode="P_177F10075"/>
	<edges id="P_178F10075P_179F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_178F10075" targetNode="P_179F10075"/>
	<edges id="P_172F10075P_173F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_172F10075" targetNode="P_173F10075"/>
	<edges xsi:type="cbl:PerformEdge" id="S_526F10075_I" deadCode="false" sourceNode="P_174F10075" targetNode="P_182F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_526F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_526F10075_O" deadCode="false" sourceNode="P_174F10075" targetNode="P_183F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_526F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_527F10075_I" deadCode="false" sourceNode="P_174F10075" targetNode="P_184F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_527F10075"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_527F10075_O" deadCode="false" sourceNode="P_174F10075" targetNode="P_185F10075">
		<representations href="../../../cobol/IDBSPLI0.cbl.cobModel#S_527F10075"/>
	</edges>
	<edges id="P_174F10075P_175F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_174F10075" targetNode="P_175F10075"/>
	<edges id="P_182F10075P_183F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_182F10075" targetNode="P_183F10075"/>
	<edges id="P_184F10075P_185F10075" xsi:type="cbl:FallThroughEdge" sourceNode="P_184F10075" targetNode="P_185F10075"/>
	<edges xsi:type="cbl:DataEdge" id="S_129F10075_POS1" deadCode="false" targetNode="P_30F10075" sourceNode="DB2_PERC_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_129F10075"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_141F10075_POS1" deadCode="false" sourceNode="P_32F10075" targetNode="DB2_PERC_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_141F10075"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_148F10075_POS1" deadCode="false" sourceNode="P_34F10075" targetNode="DB2_PERC_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_148F10075"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_151F10075_POS1" deadCode="false" sourceNode="P_36F10075" targetNode="DB2_PERC_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_151F10075"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_159F10075_POS1" deadCode="false" targetNode="P_38F10075" sourceNode="DB2_PERC_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_159F10075"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_169F10075_POS1" deadCode="false" sourceNode="P_144F10075" targetNode="DB2_PERC_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_169F10075"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_173F10075_POS1" deadCode="false" targetNode="P_146F10075" sourceNode="DB2_PERC_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_173F10075"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_176F10075_POS1" deadCode="false" targetNode="P_148F10075" sourceNode="DB2_PERC_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_176F10075"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_183F10075_POS1" deadCode="false" targetNode="P_151F10075" sourceNode="DB2_PERC_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_183F10075"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_198F10075_POS1" deadCode="false" targetNode="P_44F10075" sourceNode="DB2_PERC_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_198F10075"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_205F10075_POS1" deadCode="false" targetNode="P_46F10075" sourceNode="DB2_PERC_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_205F10075"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_208F10075_POS1" deadCode="false" targetNode="P_48F10075" sourceNode="DB2_PERC_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_208F10075"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_215F10075_POS1" deadCode="false" targetNode="P_52F10075" sourceNode="DB2_PERC_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_215F10075"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_277F10075_POS1" deadCode="false" targetNode="P_84F10075" sourceNode="DB2_PERC_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_277F10075"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_288F10075_POS1" deadCode="false" targetNode="P_86F10075" sourceNode="DB2_PERC_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_288F10075"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_295F10075_POS1" deadCode="false" targetNode="P_88F10075" sourceNode="DB2_PERC_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_295F10075"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_298F10075_POS1" deadCode="false" targetNode="P_90F10075" sourceNode="DB2_PERC_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_298F10075"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_305F10075_POS1" deadCode="false" targetNode="P_94F10075" sourceNode="DB2_PERC_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_305F10075"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0093" cbl:id="LVVS0093" xsi:id="LVVS0093" packageRef="LVVS0093.igd#LVVS0093" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0093_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10342" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0093.cbl.cobModel#SC_1F10342"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10342" deadCode="false" name="PROGRAM_LVVS0093_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0093.cbl.cobModel#P_1F10342"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10342" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0093.cbl.cobModel#P_2F10342"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10342" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0093.cbl.cobModel#P_3F10342"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10342" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0093.cbl.cobModel#P_4F10342"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10342" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0093.cbl.cobModel#P_5F10342"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10342" deadCode="false" name="S60120-PREPARA-LCCS0003">
				<representations href="../../../cobol/LVVS0093.cbl.cobModel#P_10F10342"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10342" deadCode="false" name="S60120-PREPARA-LCCS0003-EX">
				<representations href="../../../cobol/LVVS0093.cbl.cobModel#P_11F10342"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10342" deadCode="false" name="S60130-CALL-LCCS0003">
				<representations href="../../../cobol/LVVS0093.cbl.cobModel#P_12F10342"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10342" deadCode="false" name="S60130-CALL-LCCS0003-EX">
				<representations href="../../../cobol/LVVS0093.cbl.cobModel#P_13F10342"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10342" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0093.cbl.cobModel#P_8F10342"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10342" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0093.cbl.cobModel#P_9F10342"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10342" deadCode="false" name="S1250-CALCOLA-DATA1">
				<representations href="../../../cobol/LVVS0093.cbl.cobModel#P_14F10342"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10342" deadCode="false" name="S1250-EX">
				<representations href="../../../cobol/LVVS0093.cbl.cobModel#P_15F10342"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10342" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0093.cbl.cobModel#P_6F10342"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10342" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0093.cbl.cobModel#P_7F10342"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0003" name="LCCS0003">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10119"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS2920" name="LDBS2920">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10191"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10342P_1F10342" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10342" targetNode="P_1F10342"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10342_I" deadCode="false" sourceNode="P_1F10342" targetNode="P_2F10342">
		<representations href="../../../cobol/LVVS0093.cbl.cobModel#S_1F10342"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10342_O" deadCode="false" sourceNode="P_1F10342" targetNode="P_3F10342">
		<representations href="../../../cobol/LVVS0093.cbl.cobModel#S_1F10342"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10342_I" deadCode="false" sourceNode="P_1F10342" targetNode="P_4F10342">
		<representations href="../../../cobol/LVVS0093.cbl.cobModel#S_2F10342"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10342_O" deadCode="false" sourceNode="P_1F10342" targetNode="P_5F10342">
		<representations href="../../../cobol/LVVS0093.cbl.cobModel#S_2F10342"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10342_I" deadCode="false" sourceNode="P_1F10342" targetNode="P_6F10342">
		<representations href="../../../cobol/LVVS0093.cbl.cobModel#S_3F10342"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10342_O" deadCode="false" sourceNode="P_1F10342" targetNode="P_7F10342">
		<representations href="../../../cobol/LVVS0093.cbl.cobModel#S_3F10342"/>
	</edges>
	<edges id="P_2F10342P_3F10342" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10342" targetNode="P_3F10342"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10342_I" deadCode="false" sourceNode="P_4F10342" targetNode="P_8F10342">
		<representations href="../../../cobol/LVVS0093.cbl.cobModel#S_10F10342"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10342_O" deadCode="false" sourceNode="P_4F10342" targetNode="P_9F10342">
		<representations href="../../../cobol/LVVS0093.cbl.cobModel#S_10F10342"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10342_I" deadCode="false" sourceNode="P_4F10342" targetNode="P_10F10342">
		<representations href="../../../cobol/LVVS0093.cbl.cobModel#S_14F10342"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10342_O" deadCode="false" sourceNode="P_4F10342" targetNode="P_11F10342">
		<representations href="../../../cobol/LVVS0093.cbl.cobModel#S_14F10342"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10342_I" deadCode="false" sourceNode="P_4F10342" targetNode="P_12F10342">
		<representations href="../../../cobol/LVVS0093.cbl.cobModel#S_15F10342"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10342_O" deadCode="false" sourceNode="P_4F10342" targetNode="P_13F10342">
		<representations href="../../../cobol/LVVS0093.cbl.cobModel#S_15F10342"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10342_I" deadCode="false" sourceNode="P_4F10342" targetNode="P_14F10342">
		<representations href="../../../cobol/LVVS0093.cbl.cobModel#S_19F10342"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_19F10342_O" deadCode="false" sourceNode="P_4F10342" targetNode="P_15F10342">
		<representations href="../../../cobol/LVVS0093.cbl.cobModel#S_19F10342"/>
	</edges>
	<edges id="P_4F10342P_5F10342" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10342" targetNode="P_5F10342"/>
	<edges id="P_10F10342P_11F10342" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10342" targetNode="P_11F10342"/>
	<edges id="P_12F10342P_13F10342" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10342" targetNode="P_13F10342"/>
	<edges id="P_8F10342P_9F10342" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10342" targetNode="P_9F10342"/>
	<edges id="P_14F10342P_15F10342" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10342" targetNode="P_15F10342"/>
	<edges xsi:type="cbl:CallEdge" id="S_29F10342" deadCode="false" name="Dynamic LCCS0003" sourceNode="P_12F10342" targetNode="LCCS0003">
		<representations href="../../../cobol/../importantStmts.cobModel#S_29F10342"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_43F10342" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_14F10342" targetNode="LDBS2920">
		<representations href="../../../cobol/../importantStmts.cobModel#S_43F10342"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IVVS0212" cbl:id="IVVS0212" xsi:id="IVVS0212" packageRef="IVVS0212.igd#IVVS0212" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IVVS0212_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10116" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IVVS0212.cbl.cobModel#SC_1F10116"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10116" deadCode="false" name="1000-PRINCIPALE">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_1F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10116" deadCode="false" name="1000-PRINCIPALE-FINE">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_4F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10116" deadCode="false" name="1100-RICERCA-AUT-OPER">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_2F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10116" deadCode="false" name="1100-RICERCA-AUT-OPER-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_3F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10116" deadCode="true" name="S1130-LENGTH-VCHAR-GER">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_5F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10116" deadCode="false" name="EX-S1130">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_6F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10116" deadCode="false" name="S1100-OPEN-CURSOR">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_7F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10116" deadCode="false" name="EX-S1100">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_8F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10116" deadCode="false" name="S1110-FETCH-NEXT">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_9F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10116" deadCode="false" name="EX-S1110">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_10F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10116" deadCode="false" name="S1120-CLOSE-CURSOR">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_13F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10116" deadCode="false" name="EX-S1120">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_14F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10116" deadCode="false" name="S2000-VALORIZZA-OUTPUT">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_11F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10116" deadCode="false" name="EX-S2000">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_12F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10116" deadCode="false" name="E501-PREPARA-UNZIP-PARAM">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_53F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10116" deadCode="false" name="E501-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_54F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10116" deadCode="false" name="U999-UNZIP-STRING">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_55F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10116" deadCode="false" name="U999-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_56F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10116" deadCode="false" name="E502-CARICA-PARAM-UNZIPPED">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_57F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10116" deadCode="false" name="E502-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_58F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10116" deadCode="false" name="OPEN-CURSORE-1">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_15F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10116" deadCode="false" name="OPEN-CURSORE-1-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_16F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10116" deadCode="false" name="OPEN-CURSORE-2">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_17F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10116" deadCode="false" name="OPEN-CURSORE-2-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_18F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10116" deadCode="false" name="OPEN-CURSORE-3">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_19F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10116" deadCode="false" name="OPEN-CURSORE-3-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_20F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10116" deadCode="false" name="OPEN-CURSORE-4">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_21F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10116" deadCode="false" name="OPEN-CURSORE-4-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_22F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10116" deadCode="false" name="OPEN-CURSORE-5">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_23F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10116" deadCode="false" name="OPEN-CURSORE-5-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_24F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10116" deadCode="false" name="OPEN-CURSORE-6">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_25F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10116" deadCode="false" name="OPEN-CURSORE-6-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_26F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10116" deadCode="false" name="CAO-1-FETCH-NEXT">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_27F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10116" deadCode="false" name="CAO-1-FETCH-NEXT-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_28F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10116" deadCode="false" name="CAO-2-FETCH-NEXT">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_29F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10116" deadCode="false" name="CAO-2-FETCH-NEXT-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_30F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10116" deadCode="false" name="CAO-3-FETCH-NEXT">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_31F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10116" deadCode="false" name="CAO-3-FETCH-NEXT-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_32F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10116" deadCode="false" name="CAO-4-FETCH-NEXT">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_33F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10116" deadCode="false" name="CAO-4-FETCH-NEXT-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_34F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10116" deadCode="false" name="CAO-5-FETCH-NEXT">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_35F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10116" deadCode="false" name="CAO-5-FETCH-NEXT-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_36F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10116" deadCode="false" name="CAO-6-FETCH-NEXT">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_37F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10116" deadCode="false" name="CAO-6-FETCH-NEXT-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_38F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10116" deadCode="false" name="CAO-1-CLOSE-CURSOR">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_39F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10116" deadCode="false" name="CAO-1-CLOSE-CURSOR-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_40F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10116" deadCode="false" name="CAO-2-CLOSE-CURSOR">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_41F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10116" deadCode="false" name="CAO-2-CLOSE-CURSOR-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_42F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10116" deadCode="false" name="CAO-3-CLOSE-CURSOR">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_43F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10116" deadCode="false" name="CAO-3-CLOSE-CURSOR-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_44F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10116" deadCode="false" name="CAO-4-CLOSE-CURSOR">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_45F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10116" deadCode="false" name="CAO-4-CLOSE-CURSOR-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_46F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10116" deadCode="false" name="CAO-5-CLOSE-CURSOR">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_47F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10116" deadCode="false" name="CAO-5-CLOSE-CURSOR-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_48F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10116" deadCode="false" name="CAO-6-CLOSE-CURSOR">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_49F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10116" deadCode="false" name="CAO-6-CLOSE-CURSOR-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_50F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10116" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_51F10116"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10116" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IVVS0212.cbl.cobModel#P_52F10116"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_CTRL_AUT_OPER" name="CTRL_AUT_OPER">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_CTRL_AUT_OPER"/>
		</children>
	</packageNode>
	<edges id="SC_1F10116P_1F10116" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10116" targetNode="P_1F10116"/>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10116_I" deadCode="false" sourceNode="P_1F10116" targetNode="P_2F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_18F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_18F10116_O" deadCode="false" sourceNode="P_1F10116" targetNode="P_3F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_18F10116"/>
	</edges>
	<edges id="P_1F10116P_4F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_1F10116" targetNode="P_4F10116"/>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10116_I" deadCode="true" sourceNode="P_2F10116" targetNode="P_5F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_32F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10116_O" deadCode="true" sourceNode="P_2F10116" targetNode="P_6F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_32F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10116_I" deadCode="false" sourceNode="P_2F10116" targetNode="P_7F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_33F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10116_O" deadCode="false" sourceNode="P_2F10116" targetNode="P_8F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_33F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10116_I" deadCode="false" sourceNode="P_2F10116" targetNode="P_9F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_35F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10116_O" deadCode="false" sourceNode="P_2F10116" targetNode="P_10F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_35F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10116_I" deadCode="false" sourceNode="P_2F10116" targetNode="P_11F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_37F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10116_O" deadCode="false" sourceNode="P_2F10116" targetNode="P_12F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_37F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10116_I" deadCode="false" sourceNode="P_2F10116" targetNode="P_13F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_39F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10116_O" deadCode="false" sourceNode="P_2F10116" targetNode="P_14F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_39F10116"/>
	</edges>
	<edges id="P_2F10116P_3F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10116" targetNode="P_3F10116"/>
	<edges id="P_5F10116P_6F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_5F10116" targetNode="P_6F10116"/>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10116_I" deadCode="false" sourceNode="P_7F10116" targetNode="P_15F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_48F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10116_O" deadCode="false" sourceNode="P_7F10116" targetNode="P_16F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_48F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10116_I" deadCode="false" sourceNode="P_7F10116" targetNode="P_17F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_49F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10116_O" deadCode="false" sourceNode="P_7F10116" targetNode="P_18F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_49F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10116_I" deadCode="false" sourceNode="P_7F10116" targetNode="P_19F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_50F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10116_O" deadCode="false" sourceNode="P_7F10116" targetNode="P_20F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_50F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10116_I" deadCode="false" sourceNode="P_7F10116" targetNode="P_21F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_51F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10116_O" deadCode="false" sourceNode="P_7F10116" targetNode="P_22F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_51F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10116_I" deadCode="false" sourceNode="P_7F10116" targetNode="P_23F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_52F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10116_O" deadCode="false" sourceNode="P_7F10116" targetNode="P_24F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_52F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10116_I" deadCode="false" sourceNode="P_7F10116" targetNode="P_25F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_53F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10116_O" deadCode="false" sourceNode="P_7F10116" targetNode="P_26F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_53F10116"/>
	</edges>
	<edges id="P_7F10116P_8F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_7F10116" targetNode="P_8F10116"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10116_I" deadCode="false" sourceNode="P_9F10116" targetNode="P_27F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_58F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10116_O" deadCode="false" sourceNode="P_9F10116" targetNode="P_28F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_58F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10116_I" deadCode="false" sourceNode="P_9F10116" targetNode="P_29F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_59F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10116_O" deadCode="false" sourceNode="P_9F10116" targetNode="P_30F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_59F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10116_I" deadCode="false" sourceNode="P_9F10116" targetNode="P_31F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_60F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10116_O" deadCode="false" sourceNode="P_9F10116" targetNode="P_32F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_60F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10116_I" deadCode="false" sourceNode="P_9F10116" targetNode="P_33F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_61F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10116_O" deadCode="false" sourceNode="P_9F10116" targetNode="P_34F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_61F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10116_I" deadCode="false" sourceNode="P_9F10116" targetNode="P_35F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_62F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10116_O" deadCode="false" sourceNode="P_9F10116" targetNode="P_36F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_62F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10116_I" deadCode="false" sourceNode="P_9F10116" targetNode="P_37F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_63F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10116_O" deadCode="false" sourceNode="P_9F10116" targetNode="P_38F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_63F10116"/>
	</edges>
	<edges id="P_9F10116P_10F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_9F10116" targetNode="P_10F10116"/>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10116_I" deadCode="false" sourceNode="P_13F10116" targetNode="P_39F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_68F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10116_O" deadCode="false" sourceNode="P_13F10116" targetNode="P_40F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_68F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10116_I" deadCode="false" sourceNode="P_13F10116" targetNode="P_41F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_69F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10116_O" deadCode="false" sourceNode="P_13F10116" targetNode="P_42F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_69F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10116_I" deadCode="false" sourceNode="P_13F10116" targetNode="P_43F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_70F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10116_O" deadCode="false" sourceNode="P_13F10116" targetNode="P_44F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_70F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10116_I" deadCode="false" sourceNode="P_13F10116" targetNode="P_45F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_71F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10116_O" deadCode="false" sourceNode="P_13F10116" targetNode="P_46F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_71F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10116_I" deadCode="false" sourceNode="P_13F10116" targetNode="P_47F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_72F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10116_O" deadCode="false" sourceNode="P_13F10116" targetNode="P_48F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_72F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10116_I" deadCode="false" sourceNode="P_13F10116" targetNode="P_49F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_73F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10116_O" deadCode="false" sourceNode="P_13F10116" targetNode="P_50F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_73F10116"/>
	</edges>
	<edges id="P_13F10116P_14F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_13F10116" targetNode="P_14F10116"/>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10116_I" deadCode="false" sourceNode="P_11F10116" targetNode="P_51F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_77F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10116_O" deadCode="false" sourceNode="P_11F10116" targetNode="P_52F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_77F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10116_I" deadCode="false" sourceNode="P_11F10116" targetNode="P_53F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_88F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10116_O" deadCode="false" sourceNode="P_11F10116" targetNode="P_54F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_88F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10116_I" deadCode="false" sourceNode="P_11F10116" targetNode="P_55F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_89F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10116_O" deadCode="false" sourceNode="P_11F10116" targetNode="P_56F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_89F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10116_I" deadCode="false" sourceNode="P_11F10116" targetNode="P_57F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_90F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10116_O" deadCode="false" sourceNode="P_11F10116" targetNode="P_58F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_90F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10116_I" deadCode="false" sourceNode="P_11F10116" targetNode="P_9F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_92F10116"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10116_O" deadCode="false" sourceNode="P_11F10116" targetNode="P_10F10116">
		<representations href="../../../cobol/IVVS0212.cbl.cobModel#S_92F10116"/>
	</edges>
	<edges id="P_11F10116P_12F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_11F10116" targetNode="P_12F10116"/>
	<edges id="P_53F10116P_54F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_53F10116" targetNode="P_54F10116"/>
	<edges id="P_55F10116P_56F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_55F10116" targetNode="P_56F10116"/>
	<edges id="P_57F10116P_58F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_57F10116" targetNode="P_58F10116"/>
	<edges id="P_15F10116P_16F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_15F10116" targetNode="P_16F10116"/>
	<edges id="P_17F10116P_18F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_17F10116" targetNode="P_18F10116"/>
	<edges id="P_19F10116P_20F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_19F10116" targetNode="P_20F10116"/>
	<edges id="P_21F10116P_22F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_21F10116" targetNode="P_22F10116"/>
	<edges id="P_23F10116P_24F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_23F10116" targetNode="P_24F10116"/>
	<edges id="P_25F10116P_26F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_25F10116" targetNode="P_26F10116"/>
	<edges id="P_27F10116P_28F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_27F10116" targetNode="P_28F10116"/>
	<edges id="P_29F10116P_30F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_29F10116" targetNode="P_30F10116"/>
	<edges id="P_31F10116P_32F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_31F10116" targetNode="P_32F10116"/>
	<edges id="P_33F10116P_34F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_33F10116" targetNode="P_34F10116"/>
	<edges id="P_35F10116P_36F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_35F10116" targetNode="P_36F10116"/>
	<edges id="P_37F10116P_38F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_37F10116" targetNode="P_38F10116"/>
	<edges id="P_39F10116P_40F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_39F10116" targetNode="P_40F10116"/>
	<edges id="P_41F10116P_42F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_41F10116" targetNode="P_42F10116"/>
	<edges id="P_43F10116P_44F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_43F10116" targetNode="P_44F10116"/>
	<edges id="P_45F10116P_46F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10116" targetNode="P_46F10116"/>
	<edges id="P_47F10116P_48F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10116" targetNode="P_48F10116"/>
	<edges id="P_49F10116P_50F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_49F10116" targetNode="P_50F10116"/>
	<edges id="P_51F10116P_52F10116" xsi:type="cbl:FallThroughEdge" sourceNode="P_51F10116" targetNode="P_52F10116"/>
	<edges xsi:type="cbl:DataEdge" id="S_126F10116_POS1" deadCode="false" targetNode="P_15F10116" sourceNode="DB2_CTRL_AUT_OPER">
		<representations href="../../../cobol/../importantStmts.cobModel#S_126F10116"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_128F10116_POS1" deadCode="false" targetNode="P_17F10116" sourceNode="DB2_CTRL_AUT_OPER">
		<representations href="../../../cobol/../importantStmts.cobModel#S_128F10116"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_130F10116_POS1" deadCode="false" targetNode="P_19F10116" sourceNode="DB2_CTRL_AUT_OPER">
		<representations href="../../../cobol/../importantStmts.cobModel#S_130F10116"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_132F10116_POS1" deadCode="false" targetNode="P_21F10116" sourceNode="DB2_CTRL_AUT_OPER">
		<representations href="../../../cobol/../importantStmts.cobModel#S_132F10116"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_134F10116_POS1" deadCode="false" targetNode="P_23F10116" sourceNode="DB2_CTRL_AUT_OPER">
		<representations href="../../../cobol/../importantStmts.cobModel#S_134F10116"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_136F10116_POS1" deadCode="false" targetNode="P_25F10116" sourceNode="DB2_CTRL_AUT_OPER">
		<representations href="../../../cobol/../importantStmts.cobModel#S_136F10116"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_138F10116_POS1" deadCode="false" targetNode="P_27F10116" sourceNode="DB2_CTRL_AUT_OPER">
		<representations href="../../../cobol/../importantStmts.cobModel#S_138F10116"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_140F10116_POS1" deadCode="false" targetNode="P_29F10116" sourceNode="DB2_CTRL_AUT_OPER">
		<representations href="../../../cobol/../importantStmts.cobModel#S_140F10116"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_143F10116_POS1" deadCode="false" targetNode="P_31F10116" sourceNode="DB2_CTRL_AUT_OPER">
		<representations href="../../../cobol/../importantStmts.cobModel#S_143F10116"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_146F10116_POS1" deadCode="false" targetNode="P_33F10116" sourceNode="DB2_CTRL_AUT_OPER">
		<representations href="../../../cobol/../importantStmts.cobModel#S_146F10116"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_149F10116_POS1" deadCode="false" targetNode="P_35F10116" sourceNode="DB2_CTRL_AUT_OPER">
		<representations href="../../../cobol/../importantStmts.cobModel#S_149F10116"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_152F10116_POS1" deadCode="false" targetNode="P_37F10116" sourceNode="DB2_CTRL_AUT_OPER">
		<representations href="../../../cobol/../importantStmts.cobModel#S_152F10116"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_155F10116_POS1" deadCode="false" targetNode="P_39F10116" sourceNode="DB2_CTRL_AUT_OPER">
		<representations href="../../../cobol/../importantStmts.cobModel#S_155F10116"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_157F10116_POS1" deadCode="false" targetNode="P_41F10116" sourceNode="DB2_CTRL_AUT_OPER">
		<representations href="../../../cobol/../importantStmts.cobModel#S_157F10116"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_159F10116_POS1" deadCode="false" targetNode="P_43F10116" sourceNode="DB2_CTRL_AUT_OPER">
		<representations href="../../../cobol/../importantStmts.cobModel#S_159F10116"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_161F10116_POS1" deadCode="false" targetNode="P_45F10116" sourceNode="DB2_CTRL_AUT_OPER">
		<representations href="../../../cobol/../importantStmts.cobModel#S_161F10116"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_163F10116_POS1" deadCode="false" targetNode="P_47F10116" sourceNode="DB2_CTRL_AUT_OPER">
		<representations href="../../../cobol/../importantStmts.cobModel#S_163F10116"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_165F10116_POS1" deadCode="false" targetNode="P_49F10116" sourceNode="DB2_CTRL_AUT_OPER">
		<representations href="../../../cobol/../importantStmts.cobModel#S_165F10116"></representations>
	</edges>
</Package>

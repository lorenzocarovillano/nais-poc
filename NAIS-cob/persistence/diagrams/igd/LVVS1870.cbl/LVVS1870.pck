<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS1870" cbl:id="LVVS1870" xsi:id="LVVS1870" packageRef="LVVS1870.igd#LVVS1870" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS1870_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10366" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS1870.cbl.cobModel#SC_1F10366"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10366" deadCode="false" name="PROGRAM_LVVS1870_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS1870.cbl.cobModel#P_1F10366"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10366" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS1870.cbl.cobModel#P_2F10366"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10366" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS1870.cbl.cobModel#P_3F10366"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10366" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS1870.cbl.cobModel#P_4F10366"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10366" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS1870.cbl.cobModel#P_5F10366"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10366" deadCode="true" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS1870.cbl.cobModel#P_8F10366"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10366" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS1870.cbl.cobModel#P_9F10366"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10366" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS1870.cbl.cobModel#P_10F10366"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10366" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS1870.cbl.cobModel#P_11F10366"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10366" deadCode="false" name="S1250-CALCOLA-ULTRISMATNET">
				<representations href="../../../cobol/LVVS1870.cbl.cobModel#P_12F10366"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10366" deadCode="false" name="S1250-EX">
				<representations href="../../../cobol/LVVS1870.cbl.cobModel#P_13F10366"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10366" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS1870.cbl.cobModel#P_6F10366"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10366" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS1870.cbl.cobModel#P_7F10366"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS3990" name="LDBS3990">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10205"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10366P_1F10366" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10366" targetNode="P_1F10366"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10366_I" deadCode="false" sourceNode="P_1F10366" targetNode="P_2F10366">
		<representations href="../../../cobol/LVVS1870.cbl.cobModel#S_1F10366"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10366_O" deadCode="false" sourceNode="P_1F10366" targetNode="P_3F10366">
		<representations href="../../../cobol/LVVS1870.cbl.cobModel#S_1F10366"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10366_I" deadCode="false" sourceNode="P_1F10366" targetNode="P_4F10366">
		<representations href="../../../cobol/LVVS1870.cbl.cobModel#S_2F10366"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10366_O" deadCode="false" sourceNode="P_1F10366" targetNode="P_5F10366">
		<representations href="../../../cobol/LVVS1870.cbl.cobModel#S_2F10366"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10366_I" deadCode="false" sourceNode="P_1F10366" targetNode="P_6F10366">
		<representations href="../../../cobol/LVVS1870.cbl.cobModel#S_3F10366"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10366_O" deadCode="false" sourceNode="P_1F10366" targetNode="P_7F10366">
		<representations href="../../../cobol/LVVS1870.cbl.cobModel#S_3F10366"/>
	</edges>
	<edges id="P_2F10366P_3F10366" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10366" targetNode="P_3F10366"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10366_I" deadCode="true" sourceNode="P_4F10366" targetNode="P_8F10366">
		<representations href="../../../cobol/LVVS1870.cbl.cobModel#S_10F10366"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10366_O" deadCode="true" sourceNode="P_4F10366" targetNode="P_9F10366">
		<representations href="../../../cobol/LVVS1870.cbl.cobModel#S_10F10366"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10366_I" deadCode="false" sourceNode="P_4F10366" targetNode="P_10F10366">
		<representations href="../../../cobol/LVVS1870.cbl.cobModel#S_12F10366"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10366_O" deadCode="false" sourceNode="P_4F10366" targetNode="P_11F10366">
		<representations href="../../../cobol/LVVS1870.cbl.cobModel#S_12F10366"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10366_I" deadCode="false" sourceNode="P_4F10366" targetNode="P_12F10366">
		<representations href="../../../cobol/LVVS1870.cbl.cobModel#S_14F10366"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10366_O" deadCode="false" sourceNode="P_4F10366" targetNode="P_13F10366">
		<representations href="../../../cobol/LVVS1870.cbl.cobModel#S_14F10366"/>
	</edges>
	<edges id="P_4F10366P_5F10366" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10366" targetNode="P_5F10366"/>
	<edges id="P_8F10366P_9F10366" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10366" targetNode="P_9F10366"/>
	<edges id="P_10F10366P_11F10366" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10366" targetNode="P_11F10366"/>
	<edges id="P_12F10366P_13F10366" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10366" targetNode="P_13F10366"/>
	<edges xsi:type="cbl:CallEdge" id="S_25F10366" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_12F10366" targetNode="LDBS3990">
		<representations href="../../../cobol/../importantStmts.cobModel#S_25F10366"></representations>
	</edges>
</Package>

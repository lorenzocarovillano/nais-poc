<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0030" cbl:id="LVVS0030" xsi:id="LVVS0030" packageRef="LVVS0030.igd#LVVS0030" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0030_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10322" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0030.cbl.cobModel#SC_1F10322"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10322" deadCode="false" name="PROGRAM_LVVS0030_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0030.cbl.cobModel#P_1F10322"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10322" deadCode="false" name="L000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0030.cbl.cobModel#P_2F10322"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10322" deadCode="false" name="L000-EX">
				<representations href="../../../cobol/LVVS0030.cbl.cobModel#P_3F10322"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10322" deadCode="false" name="L100-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0030.cbl.cobModel#P_4F10322"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10322" deadCode="false" name="L100-EX">
				<representations href="../../../cobol/LVVS0030.cbl.cobModel#P_5F10322"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10322" deadCode="false" name="L500-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0030.cbl.cobModel#P_8F10322"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10322" deadCode="false" name="L500-EX">
				<representations href="../../../cobol/LVVS0030.cbl.cobModel#P_9F10322"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10322" deadCode="false" name="L550-CERCA-TRANCHE">
				<representations href="../../../cobol/LVVS0030.cbl.cobModel#P_10F10322"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10322" deadCode="false" name="L550-EX">
				<representations href="../../../cobol/LVVS0030.cbl.cobModel#P_11F10322"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10322" deadCode="false" name="L600-PREPARA-CALL">
				<representations href="../../../cobol/LVVS0030.cbl.cobModel#P_14F10322"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10322" deadCode="false" name="L600-EX">
				<representations href="../../../cobol/LVVS0030.cbl.cobModel#P_15F10322"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10322" deadCode="false" name="L700-CALL-LVVS0000">
				<representations href="../../../cobol/LVVS0030.cbl.cobModel#P_12F10322"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10322" deadCode="false" name="L700-EX">
				<representations href="../../../cobol/LVVS0030.cbl.cobModel#P_13F10322"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10322" deadCode="false" name="L900-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0030.cbl.cobModel#P_6F10322"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10322" deadCode="true" name="L900-EX">
				<representations href="../../../cobol/LVVS0030.cbl.cobModel#P_7F10322"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LVVS0000" name="LVVS0000">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10304"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10322P_1F10322" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10322" targetNode="P_1F10322"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10322_I" deadCode="false" sourceNode="P_1F10322" targetNode="P_2F10322">
		<representations href="../../../cobol/LVVS0030.cbl.cobModel#S_1F10322"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10322_O" deadCode="false" sourceNode="P_1F10322" targetNode="P_3F10322">
		<representations href="../../../cobol/LVVS0030.cbl.cobModel#S_1F10322"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10322_I" deadCode="false" sourceNode="P_1F10322" targetNode="P_4F10322">
		<representations href="../../../cobol/LVVS0030.cbl.cobModel#S_2F10322"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10322_O" deadCode="false" sourceNode="P_1F10322" targetNode="P_5F10322">
		<representations href="../../../cobol/LVVS0030.cbl.cobModel#S_2F10322"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10322_I" deadCode="false" sourceNode="P_1F10322" targetNode="P_6F10322">
		<representations href="../../../cobol/LVVS0030.cbl.cobModel#S_3F10322"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10322_O" deadCode="false" sourceNode="P_1F10322" targetNode="P_7F10322">
		<representations href="../../../cobol/LVVS0030.cbl.cobModel#S_3F10322"/>
	</edges>
	<edges id="P_2F10322P_3F10322" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10322" targetNode="P_3F10322"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10322_I" deadCode="false" sourceNode="P_4F10322" targetNode="P_8F10322">
		<representations href="../../../cobol/LVVS0030.cbl.cobModel#S_11F10322"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10322_O" deadCode="false" sourceNode="P_4F10322" targetNode="P_9F10322">
		<representations href="../../../cobol/LVVS0030.cbl.cobModel#S_11F10322"/>
	</edges>
	<edges id="P_4F10322P_5F10322" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10322" targetNode="P_5F10322"/>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10322_I" deadCode="false" sourceNode="P_8F10322" targetNode="P_10F10322">
		<representations href="../../../cobol/LVVS0030.cbl.cobModel#S_24F10322"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10322_O" deadCode="false" sourceNode="P_8F10322" targetNode="P_11F10322">
		<representations href="../../../cobol/LVVS0030.cbl.cobModel#S_24F10322"/>
	</edges>
	<edges id="P_8F10322P_9F10322" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10322" targetNode="P_9F10322"/>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10322_I" deadCode="false" sourceNode="P_10F10322" targetNode="P_12F10322">
		<representations href="../../../cobol/LVVS0030.cbl.cobModel#S_26F10322"/>
		<representations href="../../../cobol/LVVS0030.cbl.cobModel#S_29F10322"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_29F10322_O" deadCode="false" sourceNode="P_10F10322" targetNode="P_13F10322">
		<representations href="../../../cobol/LVVS0030.cbl.cobModel#S_26F10322"/>
		<representations href="../../../cobol/LVVS0030.cbl.cobModel#S_29F10322"/>
	</edges>
	<edges id="P_10F10322P_11F10322" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10322" targetNode="P_11F10322"/>
	<edges id="P_14F10322P_15F10322" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10322" targetNode="P_15F10322"/>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10322_I" deadCode="false" sourceNode="P_12F10322" targetNode="P_14F10322">
		<representations href="../../../cobol/LVVS0030.cbl.cobModel#S_39F10322"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10322_O" deadCode="false" sourceNode="P_12F10322" targetNode="P_15F10322">
		<representations href="../../../cobol/LVVS0030.cbl.cobModel#S_39F10322"/>
	</edges>
	<edges id="P_12F10322P_13F10322" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10322" targetNode="P_13F10322"/>
	<edges xsi:type="cbl:CallEdge" id="S_40F10322" deadCode="false" name="Dynamic PGM-LVVS0000" sourceNode="P_12F10322" targetNode="LVVS0000">
		<representations href="../../../cobol/../importantStmts.cobModel#S_40F10322"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS1300" cbl:id="LDBS1300" xsi:id="LDBS1300" packageRef="LDBS1300.igd#LDBS1300" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS1300_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10154" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS1300.cbl.cobModel#SC_1F10154"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10154" deadCode="false" name="PROGRAM_LDBS1300_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_1F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10154" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_2F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10154" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_3F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10154" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_8F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10154" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_9F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10154" deadCode="false" name="A200-ELABORA-PK">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_4F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10154" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_5F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10154" deadCode="false" name="A210-SELECT-PK">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_10F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10154" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_11F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10154" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_14F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10154" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_15F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10154" deadCode="true" name="Z150-VALORIZZA-DATA-SERVICES">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_18F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10154" deadCode="true" name="Z150-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_19F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10154" deadCode="true" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_20F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10154" deadCode="true" name="Z200-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_21F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10154" deadCode="true" name="Z400-SEQ-RIGA">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_22F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10154" deadCode="true" name="Z400-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_23F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10154" deadCode="true" name="Z500-AGGIORNAMENTO-STORICO">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_24F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10154" deadCode="true" name="Z500-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_25F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10154" deadCode="true" name="Z600-INSERT-NUOVA-RIGA-STORICA">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_26F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10154" deadCode="true" name="Z600-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_27F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10154" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_28F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10154" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_31F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10154" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_16F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10154" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_17F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10154" deadCode="true" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_12F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10154" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_13F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10154" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_6F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10154" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_7F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10154" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_34F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10154" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_35F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10154" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_36F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10154" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_37F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10154" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_29F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10154" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_30F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10154" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_38F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10154" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_39F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10154" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_32F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10154" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_33F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10154" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_40F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10154" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_41F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10154" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_42F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10154" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_43F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10154" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_44F10154"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10154" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS1300.cbl.cobModel#P_45F10154"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_PERS" name="PERS">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_PERS"/>
		</children>
	</packageNode>
	<edges id="SC_1F10154P_1F10154" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10154" targetNode="P_1F10154"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10154_I" deadCode="false" sourceNode="P_1F10154" targetNode="P_2F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_2F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10154_O" deadCode="false" sourceNode="P_1F10154" targetNode="P_3F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_2F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10154_I" deadCode="false" sourceNode="P_1F10154" targetNode="P_4F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_5F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10154_O" deadCode="false" sourceNode="P_1F10154" targetNode="P_5F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_5F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10154_I" deadCode="false" sourceNode="P_2F10154" targetNode="P_6F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_14F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10154_O" deadCode="false" sourceNode="P_2F10154" targetNode="P_7F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_14F10154"/>
	</edges>
	<edges id="P_2F10154P_3F10154" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10154" targetNode="P_3F10154"/>
	<edges id="P_8F10154P_9F10154" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10154" targetNode="P_9F10154"/>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10154_I" deadCode="false" sourceNode="P_4F10154" targetNode="P_10F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_27F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10154_O" deadCode="false" sourceNode="P_4F10154" targetNode="P_11F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_27F10154"/>
	</edges>
	<edges id="P_4F10154P_5F10154" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10154" targetNode="P_5F10154"/>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10154_I" deadCode="true" sourceNode="P_10F10154" targetNode="P_12F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_30F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_30F10154_O" deadCode="true" sourceNode="P_10F10154" targetNode="P_13F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_30F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10154_I" deadCode="false" sourceNode="P_10F10154" targetNode="P_8F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_32F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_32F10154_O" deadCode="false" sourceNode="P_10F10154" targetNode="P_9F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_32F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10154_I" deadCode="false" sourceNode="P_10F10154" targetNode="P_14F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_34F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_34F10154_O" deadCode="false" sourceNode="P_10F10154" targetNode="P_15F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_34F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10154_I" deadCode="false" sourceNode="P_10F10154" targetNode="P_16F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_35F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10154_O" deadCode="false" sourceNode="P_10F10154" targetNode="P_17F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_35F10154"/>
	</edges>
	<edges id="P_10F10154P_11F10154" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10154" targetNode="P_11F10154"/>
	<edges id="P_14F10154P_15F10154" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10154" targetNode="P_15F10154"/>
	<edges xsi:type="cbl:PerformEdge" id="S_230F10154_I" deadCode="true" sourceNode="P_28F10154" targetNode="P_29F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_230F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_230F10154_O" deadCode="true" sourceNode="P_28F10154" targetNode="P_30F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_230F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_234F10154_I" deadCode="true" sourceNode="P_28F10154" targetNode="P_29F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_234F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_234F10154_O" deadCode="true" sourceNode="P_28F10154" targetNode="P_30F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_234F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_238F10154_I" deadCode="true" sourceNode="P_28F10154" targetNode="P_29F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_238F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_238F10154_O" deadCode="true" sourceNode="P_28F10154" targetNode="P_30F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_238F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10154_I" deadCode="true" sourceNode="P_28F10154" targetNode="P_29F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_242F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_242F10154_O" deadCode="true" sourceNode="P_28F10154" targetNode="P_30F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_242F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_246F10154_I" deadCode="true" sourceNode="P_28F10154" targetNode="P_29F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_246F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_246F10154_O" deadCode="true" sourceNode="P_28F10154" targetNode="P_30F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_246F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_250F10154_I" deadCode="true" sourceNode="P_28F10154" targetNode="P_29F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_250F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_250F10154_O" deadCode="true" sourceNode="P_28F10154" targetNode="P_30F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_250F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_254F10154_I" deadCode="true" sourceNode="P_28F10154" targetNode="P_29F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_254F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_254F10154_O" deadCode="true" sourceNode="P_28F10154" targetNode="P_30F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_254F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10154_I" deadCode="false" sourceNode="P_16F10154" targetNode="P_32F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_259F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10154_O" deadCode="false" sourceNode="P_16F10154" targetNode="P_33F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_259F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_263F10154_I" deadCode="false" sourceNode="P_16F10154" targetNode="P_32F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_263F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_263F10154_O" deadCode="false" sourceNode="P_16F10154" targetNode="P_33F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_263F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_267F10154_I" deadCode="false" sourceNode="P_16F10154" targetNode="P_32F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_267F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_267F10154_O" deadCode="false" sourceNode="P_16F10154" targetNode="P_33F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_267F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_271F10154_I" deadCode="false" sourceNode="P_16F10154" targetNode="P_32F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_271F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_271F10154_O" deadCode="false" sourceNode="P_16F10154" targetNode="P_33F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_271F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_275F10154_I" deadCode="false" sourceNode="P_16F10154" targetNode="P_32F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_275F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_275F10154_O" deadCode="false" sourceNode="P_16F10154" targetNode="P_33F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_275F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_279F10154_I" deadCode="false" sourceNode="P_16F10154" targetNode="P_32F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_279F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_279F10154_O" deadCode="false" sourceNode="P_16F10154" targetNode="P_33F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_279F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10154_I" deadCode="false" sourceNode="P_16F10154" targetNode="P_32F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_283F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10154_O" deadCode="false" sourceNode="P_16F10154" targetNode="P_33F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_283F10154"/>
	</edges>
	<edges id="P_16F10154P_17F10154" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10154" targetNode="P_17F10154"/>
	<edges id="P_12F10154P_13F10154" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10154" targetNode="P_13F10154"/>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10154_I" deadCode="false" sourceNode="P_6F10154" targetNode="P_34F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_292F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10154_O" deadCode="false" sourceNode="P_6F10154" targetNode="P_35F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_292F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10154_I" deadCode="false" sourceNode="P_6F10154" targetNode="P_36F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_294F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_294F10154_O" deadCode="false" sourceNode="P_6F10154" targetNode="P_37F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_294F10154"/>
	</edges>
	<edges id="P_6F10154P_7F10154" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10154" targetNode="P_7F10154"/>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10154_I" deadCode="true" sourceNode="P_34F10154" targetNode="P_29F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_299F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10154_O" deadCode="true" sourceNode="P_34F10154" targetNode="P_30F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_299F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_304F10154_I" deadCode="true" sourceNode="P_34F10154" targetNode="P_29F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_304F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_304F10154_O" deadCode="true" sourceNode="P_34F10154" targetNode="P_30F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_304F10154"/>
	</edges>
	<edges id="P_34F10154P_35F10154" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10154" targetNode="P_35F10154"/>
	<edges id="P_36F10154P_37F10154" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10154" targetNode="P_37F10154"/>
	<edges id="P_29F10154P_30F10154" xsi:type="cbl:FallThroughEdge" sourceNode="P_29F10154" targetNode="P_30F10154"/>
	<edges xsi:type="cbl:PerformEdge" id="S_333F10154_I" deadCode="false" sourceNode="P_32F10154" targetNode="P_40F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_333F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_333F10154_O" deadCode="false" sourceNode="P_32F10154" targetNode="P_41F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_333F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_334F10154_I" deadCode="false" sourceNode="P_32F10154" targetNode="P_42F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_334F10154"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_334F10154_O" deadCode="false" sourceNode="P_32F10154" targetNode="P_43F10154">
		<representations href="../../../cobol/LDBS1300.cbl.cobModel#S_334F10154"/>
	</edges>
	<edges id="P_32F10154P_33F10154" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10154" targetNode="P_33F10154"/>
	<edges id="P_40F10154P_41F10154" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10154" targetNode="P_41F10154"/>
	<edges id="P_42F10154P_43F10154" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10154" targetNode="P_43F10154"/>
	<edges xsi:type="cbl:DataEdge" id="S_31F10154_POS1" deadCode="false" targetNode="P_10F10154" sourceNode="DB2_PERS">
		<representations href="../../../cobol/../importantStmts.cobModel#S_31F10154"></representations>
	</edges>
</Package>

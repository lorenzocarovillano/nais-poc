<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0089" cbl:id="LVVS0089" xsi:id="LVVS0089" packageRef="LVVS0089.igd#LVVS0089" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0089_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10338" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0089.cbl.cobModel#SC_1F10338"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10338" deadCode="false" name="PROGRAM_LVVS0089_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_1F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10338" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_2F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10338" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_3F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10338" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_4F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10338" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_5F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10338" deadCode="true" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_16F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10338" deadCode="true" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_17F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10338" deadCode="false" name="S1200-RICERCA-MOVI-ORIG">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_8F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10338" deadCode="false" name="S1200-RICERCA-MOVI-ORIG-EX">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_9F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10338" deadCode="false" name="RECUP-MOVI-COMUN">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_10F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10338" deadCode="false" name="RECUP-MOVI-COMUN-EX">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_11F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10338" deadCode="false" name="CLOSE-MOVI">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_18F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10338" deadCode="false" name="CLOSE-MOVI-EX">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_19F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10338" deadCode="true" name="S1250-CALCOLA-DATA1">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_20F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10338" deadCode="true" name="S1250-EX">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_21F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10338" deadCode="false" name="S1303-LEGGI-TRCH-POS">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_12F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10338" deadCode="false" name="S1303-EX">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_13F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10338" deadCode="false" name="S1304-LEGGI-TRCH-NEG">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_14F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10338" deadCode="false" name="S1304-EX">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_15F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10338" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_6F10338"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10338" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0089.cbl.cobModel#P_7F10338"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS8850" name="LDBS8850">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10244"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="Dynamic_LVVS0089_WK-CALL-PGM" name="Dynamic LVVS0089 WK-CALL-PGM" missing="true">
			<representations href="../../../../missing.xmi#IDLRCVM5U2ZNOSBBJYCUOA0DBJZDONQLAUX2AXA4DSQPG3Z3KHZBZN"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBSE260" name="LDBSE260">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10261"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10338P_1F10338" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10338" targetNode="P_1F10338"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10338_I" deadCode="false" sourceNode="P_1F10338" targetNode="P_2F10338">
		<representations href="../../../cobol/LVVS0089.cbl.cobModel#S_1F10338"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10338_O" deadCode="false" sourceNode="P_1F10338" targetNode="P_3F10338">
		<representations href="../../../cobol/LVVS0089.cbl.cobModel#S_1F10338"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10338_I" deadCode="false" sourceNode="P_1F10338" targetNode="P_4F10338">
		<representations href="../../../cobol/LVVS0089.cbl.cobModel#S_2F10338"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10338_O" deadCode="false" sourceNode="P_1F10338" targetNode="P_5F10338">
		<representations href="../../../cobol/LVVS0089.cbl.cobModel#S_2F10338"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10338_I" deadCode="false" sourceNode="P_1F10338" targetNode="P_6F10338">
		<representations href="../../../cobol/LVVS0089.cbl.cobModel#S_3F10338"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10338_O" deadCode="false" sourceNode="P_1F10338" targetNode="P_7F10338">
		<representations href="../../../cobol/LVVS0089.cbl.cobModel#S_3F10338"/>
	</edges>
	<edges id="P_2F10338P_3F10338" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10338" targetNode="P_3F10338"/>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10338_I" deadCode="false" sourceNode="P_4F10338" targetNode="P_8F10338">
		<representations href="../../../cobol/LVVS0089.cbl.cobModel#S_16F10338"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10338_O" deadCode="false" sourceNode="P_4F10338" targetNode="P_9F10338">
		<representations href="../../../cobol/LVVS0089.cbl.cobModel#S_16F10338"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10338_I" deadCode="false" sourceNode="P_4F10338" targetNode="P_10F10338">
		<representations href="../../../cobol/LVVS0089.cbl.cobModel#S_21F10338"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10338_O" deadCode="false" sourceNode="P_4F10338" targetNode="P_11F10338">
		<representations href="../../../cobol/LVVS0089.cbl.cobModel#S_21F10338"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10338_I" deadCode="false" sourceNode="P_4F10338" targetNode="P_12F10338">
		<representations href="../../../cobol/LVVS0089.cbl.cobModel#S_25F10338"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_25F10338_O" deadCode="false" sourceNode="P_4F10338" targetNode="P_13F10338">
		<representations href="../../../cobol/LVVS0089.cbl.cobModel#S_25F10338"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10338_I" deadCode="false" sourceNode="P_4F10338" targetNode="P_14F10338">
		<representations href="../../../cobol/LVVS0089.cbl.cobModel#S_27F10338"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10338_O" deadCode="false" sourceNode="P_4F10338" targetNode="P_15F10338">
		<representations href="../../../cobol/LVVS0089.cbl.cobModel#S_27F10338"/>
	</edges>
	<edges id="P_4F10338P_5F10338" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10338" targetNode="P_5F10338"/>
	<edges id="P_8F10338P_9F10338" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10338" targetNode="P_9F10338"/>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10338_I" deadCode="false" sourceNode="P_10F10338" targetNode="P_18F10338">
		<representations href="../../../cobol/LVVS0089.cbl.cobModel#S_44F10338"/>
		<representations href="../../../cobol/LVVS0089.cbl.cobModel#S_67F10338"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10338_O" deadCode="false" sourceNode="P_10F10338" targetNode="P_19F10338">
		<representations href="../../../cobol/LVVS0089.cbl.cobModel#S_44F10338"/>
		<representations href="../../../cobol/LVVS0089.cbl.cobModel#S_67F10338"/>
	</edges>
	<edges id="P_10F10338P_11F10338" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10338" targetNode="P_11F10338"/>
	<edges id="P_18F10338P_19F10338" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10338" targetNode="P_19F10338"/>
	<edges id="P_12F10338P_13F10338" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10338" targetNode="P_13F10338"/>
	<edges id="P_14F10338P_15F10338" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10338" targetNode="P_15F10338"/>
	<edges xsi:type="cbl:CallEdge" id="S_54F10338" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_10F10338" targetNode="LDBS8850">
		<representations href="../../../cobol/../importantStmts.cobModel#S_54F10338"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_85F10338" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_18F10338" targetNode="LDBS8850">
		<representations href="../../../cobol/../importantStmts.cobModel#S_85F10338"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_104F10338" deadCode="true" name="Dynamic WK-CALL-PGM" sourceNode="P_20F10338" targetNode="Dynamic_LVVS0089_WK-CALL-PGM">
		<representations href="../../../cobol/../importantStmts.cobModel#S_104F10338"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_124F10338" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_12F10338" targetNode="LDBSE260">
		<representations href="../../../cobol/../importantStmts.cobModel#S_124F10338"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_145F10338" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_14F10338" targetNode="LDBSE260">
		<representations href="../../../cobol/../importantStmts.cobModel#S_145F10338"></representations>
	</edges>
</Package>

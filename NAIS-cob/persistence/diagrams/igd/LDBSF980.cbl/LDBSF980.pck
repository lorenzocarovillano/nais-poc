<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBSF980" cbl:id="LDBSF980" xsi:id="LDBSF980" packageRef="LDBSF980.igd#LDBSF980" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBSF980_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10273" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBSF980.cbl.cobModel#SC_1F10273"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10273" deadCode="false" name="PROGRAM_LDBSF980_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_1F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10273" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_2F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10273" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_3F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10273" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_12F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10273" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_13F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10273" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_4F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10273" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_5F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10273" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_6F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10273" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_7F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10273" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_8F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10273" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_9F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10273" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_44F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10273" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_49F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10273" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_14F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10273" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_15F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10273" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_16F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10273" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_17F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10273" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_18F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10273" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_19F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10273" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_20F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10273" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_21F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10273" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_22F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10273" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_23F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10273" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_56F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10273" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_57F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10273" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_24F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10273" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_25F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10273" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_26F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10273" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_27F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10273" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_28F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10273" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_29F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10273" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_30F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10273" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_31F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10273" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_32F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10273" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_33F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10273" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_58F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10273" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_59F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10273" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_34F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10273" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_35F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10273" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_36F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10273" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_37F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10273" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_38F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10273" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_39F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10273" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_40F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10273" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_41F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10273" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_42F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10273" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_43F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10273" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_50F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10273" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_51F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10273" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_60F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10273" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_63F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10273" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_52F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10273" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_53F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10273" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_45F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10273" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_46F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10273" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_47F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10273" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_48F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10273" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_54F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10273" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_55F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10273" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_10F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10273" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_11F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10273" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_66F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10273" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_67F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10273" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_68F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10273" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_69F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10273" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_61F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10273" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_62F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10273" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_70F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10273" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_71F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10273" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_64F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10273" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_65F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10273" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_72F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10273" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_73F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10273" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_74F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10273" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_75F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10273" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_76F10273"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10273" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBSF980.cbl.cobModel#P_77F10273"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_DETT_TIT_CONT" name="DETT_TIT_CONT">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_DETT_TIT_CONT"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_TRCH_DI_GAR" name="TRCH_DI_GAR">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_TRCH_DI_GAR"/>
		</children>
	</packageNode>
	<edges id="SC_1F10273P_1F10273" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10273" targetNode="P_1F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10273_I" deadCode="false" sourceNode="P_1F10273" targetNode="P_2F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_2F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10273_O" deadCode="false" sourceNode="P_1F10273" targetNode="P_3F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_2F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10273_I" deadCode="false" sourceNode="P_1F10273" targetNode="P_4F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_6F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10273_O" deadCode="false" sourceNode="P_1F10273" targetNode="P_5F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_6F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10273_I" deadCode="false" sourceNode="P_1F10273" targetNode="P_6F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_10F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10273_O" deadCode="false" sourceNode="P_1F10273" targetNode="P_7F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_10F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10273_I" deadCode="false" sourceNode="P_1F10273" targetNode="P_8F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_14F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10273_O" deadCode="false" sourceNode="P_1F10273" targetNode="P_9F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_14F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10273_I" deadCode="false" sourceNode="P_2F10273" targetNode="P_10F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_24F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10273_O" deadCode="false" sourceNode="P_2F10273" targetNode="P_11F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_24F10273"/>
	</edges>
	<edges id="P_2F10273P_3F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10273" targetNode="P_3F10273"/>
	<edges id="P_12F10273P_13F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10273" targetNode="P_13F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10273_I" deadCode="false" sourceNode="P_4F10273" targetNode="P_14F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_37F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10273_O" deadCode="false" sourceNode="P_4F10273" targetNode="P_15F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_37F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10273_I" deadCode="false" sourceNode="P_4F10273" targetNode="P_16F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_38F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10273_O" deadCode="false" sourceNode="P_4F10273" targetNode="P_17F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_38F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10273_I" deadCode="false" sourceNode="P_4F10273" targetNode="P_18F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_39F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10273_O" deadCode="false" sourceNode="P_4F10273" targetNode="P_19F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_39F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10273_I" deadCode="false" sourceNode="P_4F10273" targetNode="P_20F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_40F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10273_O" deadCode="false" sourceNode="P_4F10273" targetNode="P_21F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_40F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10273_I" deadCode="false" sourceNode="P_4F10273" targetNode="P_22F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_41F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10273_O" deadCode="false" sourceNode="P_4F10273" targetNode="P_23F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_41F10273"/>
	</edges>
	<edges id="P_4F10273P_5F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10273" targetNode="P_5F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10273_I" deadCode="false" sourceNode="P_6F10273" targetNode="P_24F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_45F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10273_O" deadCode="false" sourceNode="P_6F10273" targetNode="P_25F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_45F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10273_I" deadCode="false" sourceNode="P_6F10273" targetNode="P_26F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_46F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10273_O" deadCode="false" sourceNode="P_6F10273" targetNode="P_27F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_46F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10273_I" deadCode="false" sourceNode="P_6F10273" targetNode="P_28F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_47F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10273_O" deadCode="false" sourceNode="P_6F10273" targetNode="P_29F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_47F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10273_I" deadCode="false" sourceNode="P_6F10273" targetNode="P_30F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_48F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10273_O" deadCode="false" sourceNode="P_6F10273" targetNode="P_31F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_48F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10273_I" deadCode="false" sourceNode="P_6F10273" targetNode="P_32F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_49F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10273_O" deadCode="false" sourceNode="P_6F10273" targetNode="P_33F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_49F10273"/>
	</edges>
	<edges id="P_6F10273P_7F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10273" targetNode="P_7F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10273_I" deadCode="false" sourceNode="P_8F10273" targetNode="P_34F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_53F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10273_O" deadCode="false" sourceNode="P_8F10273" targetNode="P_35F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_53F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10273_I" deadCode="false" sourceNode="P_8F10273" targetNode="P_36F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_54F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10273_O" deadCode="false" sourceNode="P_8F10273" targetNode="P_37F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_54F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10273_I" deadCode="false" sourceNode="P_8F10273" targetNode="P_38F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_55F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10273_O" deadCode="false" sourceNode="P_8F10273" targetNode="P_39F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_55F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10273_I" deadCode="false" sourceNode="P_8F10273" targetNode="P_40F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_56F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10273_O" deadCode="false" sourceNode="P_8F10273" targetNode="P_41F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_56F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10273_I" deadCode="false" sourceNode="P_8F10273" targetNode="P_42F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_57F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10273_O" deadCode="false" sourceNode="P_8F10273" targetNode="P_43F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_57F10273"/>
	</edges>
	<edges id="P_8F10273P_9F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10273" targetNode="P_9F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10273_I" deadCode="false" sourceNode="P_44F10273" targetNode="P_45F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_60F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10273_O" deadCode="false" sourceNode="P_44F10273" targetNode="P_46F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_60F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10273_I" deadCode="false" sourceNode="P_44F10273" targetNode="P_47F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_61F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10273_O" deadCode="false" sourceNode="P_44F10273" targetNode="P_48F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_61F10273"/>
	</edges>
	<edges id="P_44F10273P_49F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10273" targetNode="P_49F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10273_I" deadCode="false" sourceNode="P_14F10273" targetNode="P_45F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_65F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10273_O" deadCode="false" sourceNode="P_14F10273" targetNode="P_46F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_65F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10273_I" deadCode="false" sourceNode="P_14F10273" targetNode="P_47F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_66F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10273_O" deadCode="false" sourceNode="P_14F10273" targetNode="P_48F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_66F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10273_I" deadCode="false" sourceNode="P_14F10273" targetNode="P_12F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_68F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10273_O" deadCode="false" sourceNode="P_14F10273" targetNode="P_13F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_68F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10273_I" deadCode="false" sourceNode="P_14F10273" targetNode="P_50F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_70F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10273_O" deadCode="false" sourceNode="P_14F10273" targetNode="P_51F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_70F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10273_I" deadCode="false" sourceNode="P_14F10273" targetNode="P_52F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_71F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10273_O" deadCode="false" sourceNode="P_14F10273" targetNode="P_53F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_71F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10273_I" deadCode="false" sourceNode="P_14F10273" targetNode="P_54F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_72F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10273_O" deadCode="false" sourceNode="P_14F10273" targetNode="P_55F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_72F10273"/>
	</edges>
	<edges id="P_14F10273P_15F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10273" targetNode="P_15F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10273_I" deadCode="false" sourceNode="P_16F10273" targetNode="P_44F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_74F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10273_O" deadCode="false" sourceNode="P_16F10273" targetNode="P_49F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_74F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10273_I" deadCode="false" sourceNode="P_16F10273" targetNode="P_12F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_76F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10273_O" deadCode="false" sourceNode="P_16F10273" targetNode="P_13F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_76F10273"/>
	</edges>
	<edges id="P_16F10273P_17F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10273" targetNode="P_17F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10273_I" deadCode="false" sourceNode="P_18F10273" targetNode="P_12F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_79F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10273_O" deadCode="false" sourceNode="P_18F10273" targetNode="P_13F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_79F10273"/>
	</edges>
	<edges id="P_18F10273P_19F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10273" targetNode="P_19F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10273_I" deadCode="false" sourceNode="P_20F10273" targetNode="P_16F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_81F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10273_O" deadCode="false" sourceNode="P_20F10273" targetNode="P_17F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_81F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10273_I" deadCode="false" sourceNode="P_20F10273" targetNode="P_22F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_83F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10273_O" deadCode="false" sourceNode="P_20F10273" targetNode="P_23F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_83F10273"/>
	</edges>
	<edges id="P_20F10273P_21F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10273" targetNode="P_21F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10273_I" deadCode="false" sourceNode="P_22F10273" targetNode="P_12F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_86F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10273_O" deadCode="false" sourceNode="P_22F10273" targetNode="P_13F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_86F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10273_I" deadCode="false" sourceNode="P_22F10273" targetNode="P_50F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_88F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10273_O" deadCode="false" sourceNode="P_22F10273" targetNode="P_51F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_88F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10273_I" deadCode="false" sourceNode="P_22F10273" targetNode="P_52F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_89F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10273_O" deadCode="false" sourceNode="P_22F10273" targetNode="P_53F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_89F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10273_I" deadCode="false" sourceNode="P_22F10273" targetNode="P_54F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_90F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10273_O" deadCode="false" sourceNode="P_22F10273" targetNode="P_55F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_90F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10273_I" deadCode="false" sourceNode="P_22F10273" targetNode="P_18F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_92F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10273_O" deadCode="false" sourceNode="P_22F10273" targetNode="P_19F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_92F10273"/>
	</edges>
	<edges id="P_22F10273P_23F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10273" targetNode="P_23F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10273_I" deadCode="false" sourceNode="P_56F10273" targetNode="P_45F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_96F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10273_O" deadCode="false" sourceNode="P_56F10273" targetNode="P_46F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_96F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10273_I" deadCode="false" sourceNode="P_56F10273" targetNode="P_47F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_97F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10273_O" deadCode="false" sourceNode="P_56F10273" targetNode="P_48F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_97F10273"/>
	</edges>
	<edges id="P_56F10273P_57F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10273" targetNode="P_57F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10273_I" deadCode="false" sourceNode="P_24F10273" targetNode="P_45F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_101F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10273_O" deadCode="false" sourceNode="P_24F10273" targetNode="P_46F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_101F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10273_I" deadCode="false" sourceNode="P_24F10273" targetNode="P_47F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_102F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10273_O" deadCode="false" sourceNode="P_24F10273" targetNode="P_48F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_102F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10273_I" deadCode="false" sourceNode="P_24F10273" targetNode="P_12F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_104F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10273_O" deadCode="false" sourceNode="P_24F10273" targetNode="P_13F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_104F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10273_I" deadCode="false" sourceNode="P_24F10273" targetNode="P_50F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_106F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10273_O" deadCode="false" sourceNode="P_24F10273" targetNode="P_51F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_106F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10273_I" deadCode="false" sourceNode="P_24F10273" targetNode="P_52F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_107F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10273_O" deadCode="false" sourceNode="P_24F10273" targetNode="P_53F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_107F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10273_I" deadCode="false" sourceNode="P_24F10273" targetNode="P_54F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_108F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10273_O" deadCode="false" sourceNode="P_24F10273" targetNode="P_55F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_108F10273"/>
	</edges>
	<edges id="P_24F10273P_25F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10273" targetNode="P_25F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10273_I" deadCode="false" sourceNode="P_26F10273" targetNode="P_56F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_110F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10273_O" deadCode="false" sourceNode="P_26F10273" targetNode="P_57F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_110F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10273_I" deadCode="false" sourceNode="P_26F10273" targetNode="P_12F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_112F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10273_O" deadCode="false" sourceNode="P_26F10273" targetNode="P_13F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_112F10273"/>
	</edges>
	<edges id="P_26F10273P_27F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10273" targetNode="P_27F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10273_I" deadCode="false" sourceNode="P_28F10273" targetNode="P_12F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_115F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10273_O" deadCode="false" sourceNode="P_28F10273" targetNode="P_13F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_115F10273"/>
	</edges>
	<edges id="P_28F10273P_29F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10273" targetNode="P_29F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10273_I" deadCode="false" sourceNode="P_30F10273" targetNode="P_26F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_117F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10273_O" deadCode="false" sourceNode="P_30F10273" targetNode="P_27F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_117F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10273_I" deadCode="false" sourceNode="P_30F10273" targetNode="P_32F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_119F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10273_O" deadCode="false" sourceNode="P_30F10273" targetNode="P_33F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_119F10273"/>
	</edges>
	<edges id="P_30F10273P_31F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10273" targetNode="P_31F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10273_I" deadCode="false" sourceNode="P_32F10273" targetNode="P_12F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_122F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10273_O" deadCode="false" sourceNode="P_32F10273" targetNode="P_13F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_122F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10273_I" deadCode="false" sourceNode="P_32F10273" targetNode="P_50F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_124F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10273_O" deadCode="false" sourceNode="P_32F10273" targetNode="P_51F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_124F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10273_I" deadCode="false" sourceNode="P_32F10273" targetNode="P_52F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_125F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10273_O" deadCode="false" sourceNode="P_32F10273" targetNode="P_53F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_125F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10273_I" deadCode="false" sourceNode="P_32F10273" targetNode="P_54F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_126F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10273_O" deadCode="false" sourceNode="P_32F10273" targetNode="P_55F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_126F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10273_I" deadCode="false" sourceNode="P_32F10273" targetNode="P_28F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_128F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10273_O" deadCode="false" sourceNode="P_32F10273" targetNode="P_29F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_128F10273"/>
	</edges>
	<edges id="P_32F10273P_33F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10273" targetNode="P_33F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10273_I" deadCode="false" sourceNode="P_58F10273" targetNode="P_45F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_132F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10273_O" deadCode="false" sourceNode="P_58F10273" targetNode="P_46F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_132F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10273_I" deadCode="false" sourceNode="P_58F10273" targetNode="P_47F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_133F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10273_O" deadCode="false" sourceNode="P_58F10273" targetNode="P_48F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_133F10273"/>
	</edges>
	<edges id="P_58F10273P_59F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10273" targetNode="P_59F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10273_I" deadCode="false" sourceNode="P_34F10273" targetNode="P_45F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_136F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10273_O" deadCode="false" sourceNode="P_34F10273" targetNode="P_46F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_136F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10273_I" deadCode="false" sourceNode="P_34F10273" targetNode="P_47F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_137F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10273_O" deadCode="false" sourceNode="P_34F10273" targetNode="P_48F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_137F10273"/>
	</edges>
	<edges id="P_34F10273P_35F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10273" targetNode="P_35F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10273_I" deadCode="false" sourceNode="P_36F10273" targetNode="P_58F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_140F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10273_O" deadCode="false" sourceNode="P_36F10273" targetNode="P_59F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_140F10273"/>
	</edges>
	<edges id="P_36F10273P_37F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10273" targetNode="P_37F10273"/>
	<edges id="P_38F10273P_39F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10273" targetNode="P_39F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10273_I" deadCode="false" sourceNode="P_40F10273" targetNode="P_36F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_145F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10273_O" deadCode="false" sourceNode="P_40F10273" targetNode="P_37F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_145F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10273_I" deadCode="false" sourceNode="P_40F10273" targetNode="P_42F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_147F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10273_O" deadCode="false" sourceNode="P_40F10273" targetNode="P_43F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_147F10273"/>
	</edges>
	<edges id="P_40F10273P_41F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10273" targetNode="P_41F10273"/>
	<edges id="P_42F10273P_43F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10273" targetNode="P_43F10273"/>
	<edges id="P_50F10273P_51F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10273" targetNode="P_51F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_252F10273_I" deadCode="true" sourceNode="P_60F10273" targetNode="P_61F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_252F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_252F10273_O" deadCode="true" sourceNode="P_60F10273" targetNode="P_62F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_252F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10273_I" deadCode="true" sourceNode="P_60F10273" targetNode="P_61F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_255F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10273_O" deadCode="true" sourceNode="P_60F10273" targetNode="P_62F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_255F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10273_I" deadCode="true" sourceNode="P_60F10273" targetNode="P_61F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_259F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_259F10273_O" deadCode="true" sourceNode="P_60F10273" targetNode="P_62F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_259F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_263F10273_I" deadCode="true" sourceNode="P_60F10273" targetNode="P_61F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_263F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_263F10273_O" deadCode="true" sourceNode="P_60F10273" targetNode="P_62F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_263F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_267F10273_I" deadCode="true" sourceNode="P_60F10273" targetNode="P_61F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_267F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_267F10273_O" deadCode="true" sourceNode="P_60F10273" targetNode="P_62F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_267F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_271F10273_I" deadCode="false" sourceNode="P_52F10273" targetNode="P_64F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_271F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_271F10273_O" deadCode="false" sourceNode="P_52F10273" targetNode="P_65F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_271F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_274F10273_I" deadCode="false" sourceNode="P_52F10273" targetNode="P_64F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_274F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_274F10273_O" deadCode="false" sourceNode="P_52F10273" targetNode="P_65F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_274F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10273_I" deadCode="false" sourceNode="P_52F10273" targetNode="P_64F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_278F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10273_O" deadCode="false" sourceNode="P_52F10273" targetNode="P_65F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_278F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_282F10273_I" deadCode="false" sourceNode="P_52F10273" targetNode="P_64F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_282F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_282F10273_O" deadCode="false" sourceNode="P_52F10273" targetNode="P_65F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_282F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_286F10273_I" deadCode="false" sourceNode="P_52F10273" targetNode="P_64F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_286F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_286F10273_O" deadCode="false" sourceNode="P_52F10273" targetNode="P_65F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_286F10273"/>
	</edges>
	<edges id="P_52F10273P_53F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10273" targetNode="P_53F10273"/>
	<edges id="P_45F10273P_46F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10273" targetNode="P_46F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10273_I" deadCode="false" sourceNode="P_47F10273" targetNode="P_61F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_292F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_292F10273_O" deadCode="false" sourceNode="P_47F10273" targetNode="P_62F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_292F10273"/>
	</edges>
	<edges id="P_47F10273P_48F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10273" targetNode="P_48F10273"/>
	<edges id="P_54F10273P_55F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10273" targetNode="P_55F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_297F10273_I" deadCode="false" sourceNode="P_10F10273" targetNode="P_66F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_297F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_297F10273_O" deadCode="false" sourceNode="P_10F10273" targetNode="P_67F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_297F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10273_I" deadCode="false" sourceNode="P_10F10273" targetNode="P_68F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_299F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_299F10273_O" deadCode="false" sourceNode="P_10F10273" targetNode="P_69F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_299F10273"/>
	</edges>
	<edges id="P_10F10273P_11F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10273" targetNode="P_11F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_304F10273_I" deadCode="false" sourceNode="P_66F10273" targetNode="P_61F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_304F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_304F10273_O" deadCode="false" sourceNode="P_66F10273" targetNode="P_62F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_304F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10273_I" deadCode="false" sourceNode="P_66F10273" targetNode="P_61F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_309F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10273_O" deadCode="false" sourceNode="P_66F10273" targetNode="P_62F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_309F10273"/>
	</edges>
	<edges id="P_66F10273P_67F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10273" targetNode="P_67F10273"/>
	<edges id="P_68F10273P_69F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10273" targetNode="P_69F10273"/>
	<edges id="P_61F10273P_62F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10273" targetNode="P_62F10273"/>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10273_I" deadCode="false" sourceNode="P_64F10273" targetNode="P_72F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_338F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_338F10273_O" deadCode="false" sourceNode="P_64F10273" targetNode="P_73F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_338F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_339F10273_I" deadCode="false" sourceNode="P_64F10273" targetNode="P_74F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_339F10273"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_339F10273_O" deadCode="false" sourceNode="P_64F10273" targetNode="P_75F10273">
		<representations href="../../../cobol/LDBSF980.cbl.cobModel#S_339F10273"/>
	</edges>
	<edges id="P_64F10273P_65F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10273" targetNode="P_65F10273"/>
	<edges id="P_72F10273P_73F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10273" targetNode="P_73F10273"/>
	<edges id="P_74F10273P_75F10273" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10273" targetNode="P_75F10273"/>
	<edges xsi:type="cbl:DataEdge" id="S_67F10273_POS1" deadCode="false" targetNode="P_14F10273" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10273"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_67F10273_POS2" deadCode="false" targetNode="P_14F10273" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10273"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_75F10273_POS1" deadCode="false" targetNode="P_16F10273" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_75F10273"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_75F10273_POS2" deadCode="false" targetNode="P_16F10273" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_75F10273"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_78F10273_POS1" deadCode="false" targetNode="P_18F10273" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10273"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_78F10273_POS2" deadCode="false" targetNode="P_18F10273" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10273"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_85F10273_POS1" deadCode="false" targetNode="P_22F10273" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_85F10273"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_85F10273_POS2" deadCode="false" targetNode="P_22F10273" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_85F10273"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_103F10273_POS1" deadCode="false" targetNode="P_24F10273" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_103F10273"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_103F10273_POS2" deadCode="false" targetNode="P_24F10273" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_103F10273"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10273_POS1" deadCode="false" targetNode="P_26F10273" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10273"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10273_POS2" deadCode="false" targetNode="P_26F10273" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10273"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_114F10273_POS1" deadCode="false" targetNode="P_28F10273" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10273"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_114F10273_POS2" deadCode="false" targetNode="P_28F10273" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10273"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_121F10273_POS1" deadCode="false" targetNode="P_32F10273" sourceNode="DB2_DETT_TIT_CONT">
		<representations href="../../../cobol/../importantStmts.cobModel#S_121F10273"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_121F10273_POS2" deadCode="false" targetNode="P_32F10273" sourceNode="DB2_TRCH_DI_GAR">
		<representations href="../../../cobol/../importantStmts.cobModel#S_121F10273"></representations>
	</edges>
</Package>

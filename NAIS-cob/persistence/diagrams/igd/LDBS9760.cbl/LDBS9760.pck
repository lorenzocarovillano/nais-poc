<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS9760" cbl:id="LDBS9760" xsi:id="LDBS9760" packageRef="LDBS9760.igd#LDBS9760" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS9760_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10246" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS9760.cbl.cobModel#SC_1F10246"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10246" deadCode="false" name="PROGRAM_LDBS9760_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_1F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10246" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_2F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10246" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_3F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10246" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_12F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10246" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_13F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10246" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_4F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10246" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_5F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10246" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_6F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10246" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_7F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10246" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_8F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10246" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_9F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10246" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_44F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10246" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_49F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10246" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_14F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10246" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_15F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10246" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_16F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10246" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_17F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10246" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_18F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10246" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_19F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10246" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_20F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10246" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_21F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10246" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_22F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10246" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_23F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10246" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_56F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10246" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_57F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10246" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_24F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10246" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_25F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10246" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_26F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10246" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_27F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10246" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_28F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10246" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_29F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10246" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_30F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10246" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_31F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10246" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_32F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10246" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_33F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10246" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_58F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10246" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_59F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10246" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_34F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10246" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_35F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10246" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_36F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10246" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_37F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10246" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_38F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10246" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_39F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10246" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_40F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10246" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_41F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10246" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_42F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10246" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_43F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10246" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_50F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10246" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_51F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10246" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_60F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10246" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_63F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10246" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_52F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10246" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_53F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10246" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_45F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10246" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_46F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10246" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_47F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10246" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_48F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10246" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_54F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10246" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_55F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10246" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_10F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10246" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_11F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10246" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_66F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10246" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_67F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10246" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_68F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10246" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_69F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10246" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_61F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10246" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_62F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10246" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_70F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10246" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_71F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10246" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_64F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10246" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_65F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10246" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_72F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10246" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_73F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10246" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_74F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10246" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_75F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10246" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_76F10246"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10246" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS9760.cbl.cobModel#P_77F10246"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_LIQ" name="LIQ">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_LIQ"/>
		</children>
	</packageNode>
	<edges id="SC_1F10246P_1F10246" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10246" targetNode="P_1F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10246_I" deadCode="false" sourceNode="P_1F10246" targetNode="P_2F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_1F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10246_O" deadCode="false" sourceNode="P_1F10246" targetNode="P_3F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_1F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10246_I" deadCode="false" sourceNode="P_1F10246" targetNode="P_4F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_5F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10246_O" deadCode="false" sourceNode="P_1F10246" targetNode="P_5F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_5F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10246_I" deadCode="false" sourceNode="P_1F10246" targetNode="P_6F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_9F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10246_O" deadCode="false" sourceNode="P_1F10246" targetNode="P_7F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_9F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10246_I" deadCode="false" sourceNode="P_1F10246" targetNode="P_8F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_13F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10246_O" deadCode="false" sourceNode="P_1F10246" targetNode="P_9F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_13F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10246_I" deadCode="false" sourceNode="P_2F10246" targetNode="P_10F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_22F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10246_O" deadCode="false" sourceNode="P_2F10246" targetNode="P_11F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_22F10246"/>
	</edges>
	<edges id="P_2F10246P_3F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10246" targetNode="P_3F10246"/>
	<edges id="P_12F10246P_13F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10246" targetNode="P_13F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10246_I" deadCode="false" sourceNode="P_4F10246" targetNode="P_14F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_35F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_35F10246_O" deadCode="false" sourceNode="P_4F10246" targetNode="P_15F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_35F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10246_I" deadCode="false" sourceNode="P_4F10246" targetNode="P_16F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_36F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10246_O" deadCode="false" sourceNode="P_4F10246" targetNode="P_17F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_36F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10246_I" deadCode="false" sourceNode="P_4F10246" targetNode="P_18F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_37F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10246_O" deadCode="false" sourceNode="P_4F10246" targetNode="P_19F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_37F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10246_I" deadCode="false" sourceNode="P_4F10246" targetNode="P_20F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_38F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10246_O" deadCode="false" sourceNode="P_4F10246" targetNode="P_21F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_38F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10246_I" deadCode="false" sourceNode="P_4F10246" targetNode="P_22F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_39F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10246_O" deadCode="false" sourceNode="P_4F10246" targetNode="P_23F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_39F10246"/>
	</edges>
	<edges id="P_4F10246P_5F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10246" targetNode="P_5F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10246_I" deadCode="false" sourceNode="P_6F10246" targetNode="P_24F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_43F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_43F10246_O" deadCode="false" sourceNode="P_6F10246" targetNode="P_25F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_43F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10246_I" deadCode="false" sourceNode="P_6F10246" targetNode="P_26F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_44F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_44F10246_O" deadCode="false" sourceNode="P_6F10246" targetNode="P_27F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_44F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10246_I" deadCode="false" sourceNode="P_6F10246" targetNode="P_28F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_45F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10246_O" deadCode="false" sourceNode="P_6F10246" targetNode="P_29F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_45F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10246_I" deadCode="false" sourceNode="P_6F10246" targetNode="P_30F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_46F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10246_O" deadCode="false" sourceNode="P_6F10246" targetNode="P_31F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_46F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10246_I" deadCode="false" sourceNode="P_6F10246" targetNode="P_32F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_47F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10246_O" deadCode="false" sourceNode="P_6F10246" targetNode="P_33F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_47F10246"/>
	</edges>
	<edges id="P_6F10246P_7F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10246" targetNode="P_7F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10246_I" deadCode="false" sourceNode="P_8F10246" targetNode="P_34F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_51F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10246_O" deadCode="false" sourceNode="P_8F10246" targetNode="P_35F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_51F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10246_I" deadCode="false" sourceNode="P_8F10246" targetNode="P_36F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_52F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_52F10246_O" deadCode="false" sourceNode="P_8F10246" targetNode="P_37F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_52F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10246_I" deadCode="false" sourceNode="P_8F10246" targetNode="P_38F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_53F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10246_O" deadCode="false" sourceNode="P_8F10246" targetNode="P_39F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_53F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10246_I" deadCode="false" sourceNode="P_8F10246" targetNode="P_40F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_54F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10246_O" deadCode="false" sourceNode="P_8F10246" targetNode="P_41F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_54F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10246_I" deadCode="false" sourceNode="P_8F10246" targetNode="P_42F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_55F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10246_O" deadCode="false" sourceNode="P_8F10246" targetNode="P_43F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_55F10246"/>
	</edges>
	<edges id="P_8F10246P_9F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10246" targetNode="P_9F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10246_I" deadCode="false" sourceNode="P_44F10246" targetNode="P_45F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_58F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10246_O" deadCode="false" sourceNode="P_44F10246" targetNode="P_46F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_58F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10246_I" deadCode="false" sourceNode="P_44F10246" targetNode="P_47F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_59F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10246_O" deadCode="false" sourceNode="P_44F10246" targetNode="P_48F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_59F10246"/>
	</edges>
	<edges id="P_44F10246P_49F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10246" targetNode="P_49F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10246_I" deadCode="false" sourceNode="P_14F10246" targetNode="P_45F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_63F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10246_O" deadCode="false" sourceNode="P_14F10246" targetNode="P_46F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_63F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10246_I" deadCode="false" sourceNode="P_14F10246" targetNode="P_47F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_64F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10246_O" deadCode="false" sourceNode="P_14F10246" targetNode="P_48F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_64F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10246_I" deadCode="false" sourceNode="P_14F10246" targetNode="P_12F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_66F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10246_O" deadCode="false" sourceNode="P_14F10246" targetNode="P_13F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_66F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10246_I" deadCode="false" sourceNode="P_14F10246" targetNode="P_50F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_68F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10246_O" deadCode="false" sourceNode="P_14F10246" targetNode="P_51F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_68F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10246_I" deadCode="false" sourceNode="P_14F10246" targetNode="P_52F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_69F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10246_O" deadCode="false" sourceNode="P_14F10246" targetNode="P_53F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_69F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10246_I" deadCode="false" sourceNode="P_14F10246" targetNode="P_54F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_70F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10246_O" deadCode="false" sourceNode="P_14F10246" targetNode="P_55F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_70F10246"/>
	</edges>
	<edges id="P_14F10246P_15F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10246" targetNode="P_15F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10246_I" deadCode="false" sourceNode="P_16F10246" targetNode="P_44F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_72F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10246_O" deadCode="false" sourceNode="P_16F10246" targetNode="P_49F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_72F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10246_I" deadCode="false" sourceNode="P_16F10246" targetNode="P_12F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_74F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10246_O" deadCode="false" sourceNode="P_16F10246" targetNode="P_13F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_74F10246"/>
	</edges>
	<edges id="P_16F10246P_17F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10246" targetNode="P_17F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10246_I" deadCode="false" sourceNode="P_18F10246" targetNode="P_12F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_77F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10246_O" deadCode="false" sourceNode="P_18F10246" targetNode="P_13F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_77F10246"/>
	</edges>
	<edges id="P_18F10246P_19F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10246" targetNode="P_19F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10246_I" deadCode="false" sourceNode="P_20F10246" targetNode="P_16F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_79F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10246_O" deadCode="false" sourceNode="P_20F10246" targetNode="P_17F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_79F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10246_I" deadCode="false" sourceNode="P_20F10246" targetNode="P_22F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_81F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10246_O" deadCode="false" sourceNode="P_20F10246" targetNode="P_23F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_81F10246"/>
	</edges>
	<edges id="P_20F10246P_21F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10246" targetNode="P_21F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10246_I" deadCode="false" sourceNode="P_22F10246" targetNode="P_12F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_84F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10246_O" deadCode="false" sourceNode="P_22F10246" targetNode="P_13F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_84F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10246_I" deadCode="false" sourceNode="P_22F10246" targetNode="P_50F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_86F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10246_O" deadCode="false" sourceNode="P_22F10246" targetNode="P_51F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_86F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10246_I" deadCode="false" sourceNode="P_22F10246" targetNode="P_52F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_87F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10246_O" deadCode="false" sourceNode="P_22F10246" targetNode="P_53F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_87F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10246_I" deadCode="false" sourceNode="P_22F10246" targetNode="P_54F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_88F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10246_O" deadCode="false" sourceNode="P_22F10246" targetNode="P_55F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_88F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10246_I" deadCode="false" sourceNode="P_22F10246" targetNode="P_18F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_90F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10246_O" deadCode="false" sourceNode="P_22F10246" targetNode="P_19F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_90F10246"/>
	</edges>
	<edges id="P_22F10246P_23F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10246" targetNode="P_23F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10246_I" deadCode="false" sourceNode="P_56F10246" targetNode="P_45F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_94F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10246_O" deadCode="false" sourceNode="P_56F10246" targetNode="P_46F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_94F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10246_I" deadCode="false" sourceNode="P_56F10246" targetNode="P_47F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_95F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10246_O" deadCode="false" sourceNode="P_56F10246" targetNode="P_48F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_95F10246"/>
	</edges>
	<edges id="P_56F10246P_57F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10246" targetNode="P_57F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10246_I" deadCode="false" sourceNode="P_24F10246" targetNode="P_45F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_99F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10246_O" deadCode="false" sourceNode="P_24F10246" targetNode="P_46F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_99F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10246_I" deadCode="false" sourceNode="P_24F10246" targetNode="P_47F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_100F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10246_O" deadCode="false" sourceNode="P_24F10246" targetNode="P_48F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_100F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10246_I" deadCode="false" sourceNode="P_24F10246" targetNode="P_12F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_102F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10246_O" deadCode="false" sourceNode="P_24F10246" targetNode="P_13F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_102F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10246_I" deadCode="false" sourceNode="P_24F10246" targetNode="P_50F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_104F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10246_O" deadCode="false" sourceNode="P_24F10246" targetNode="P_51F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_104F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10246_I" deadCode="false" sourceNode="P_24F10246" targetNode="P_52F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_105F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10246_O" deadCode="false" sourceNode="P_24F10246" targetNode="P_53F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_105F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10246_I" deadCode="false" sourceNode="P_24F10246" targetNode="P_54F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_106F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10246_O" deadCode="false" sourceNode="P_24F10246" targetNode="P_55F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_106F10246"/>
	</edges>
	<edges id="P_24F10246P_25F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10246" targetNode="P_25F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10246_I" deadCode="false" sourceNode="P_26F10246" targetNode="P_56F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_108F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10246_O" deadCode="false" sourceNode="P_26F10246" targetNode="P_57F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_108F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10246_I" deadCode="false" sourceNode="P_26F10246" targetNode="P_12F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_110F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10246_O" deadCode="false" sourceNode="P_26F10246" targetNode="P_13F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_110F10246"/>
	</edges>
	<edges id="P_26F10246P_27F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10246" targetNode="P_27F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10246_I" deadCode="false" sourceNode="P_28F10246" targetNode="P_12F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_113F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10246_O" deadCode="false" sourceNode="P_28F10246" targetNode="P_13F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_113F10246"/>
	</edges>
	<edges id="P_28F10246P_29F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10246" targetNode="P_29F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10246_I" deadCode="false" sourceNode="P_30F10246" targetNode="P_26F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_115F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10246_O" deadCode="false" sourceNode="P_30F10246" targetNode="P_27F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_115F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10246_I" deadCode="false" sourceNode="P_30F10246" targetNode="P_32F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_117F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10246_O" deadCode="false" sourceNode="P_30F10246" targetNode="P_33F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_117F10246"/>
	</edges>
	<edges id="P_30F10246P_31F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10246" targetNode="P_31F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10246_I" deadCode="false" sourceNode="P_32F10246" targetNode="P_12F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_120F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_120F10246_O" deadCode="false" sourceNode="P_32F10246" targetNode="P_13F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_120F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10246_I" deadCode="false" sourceNode="P_32F10246" targetNode="P_50F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_122F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10246_O" deadCode="false" sourceNode="P_32F10246" targetNode="P_51F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_122F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10246_I" deadCode="false" sourceNode="P_32F10246" targetNode="P_52F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_123F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10246_O" deadCode="false" sourceNode="P_32F10246" targetNode="P_53F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_123F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10246_I" deadCode="false" sourceNode="P_32F10246" targetNode="P_54F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_124F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10246_O" deadCode="false" sourceNode="P_32F10246" targetNode="P_55F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_124F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10246_I" deadCode="false" sourceNode="P_32F10246" targetNode="P_28F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_126F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10246_O" deadCode="false" sourceNode="P_32F10246" targetNode="P_29F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_126F10246"/>
	</edges>
	<edges id="P_32F10246P_33F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10246" targetNode="P_33F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10246_I" deadCode="false" sourceNode="P_58F10246" targetNode="P_45F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_130F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10246_O" deadCode="false" sourceNode="P_58F10246" targetNode="P_46F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_130F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10246_I" deadCode="false" sourceNode="P_58F10246" targetNode="P_47F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_131F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_131F10246_O" deadCode="false" sourceNode="P_58F10246" targetNode="P_48F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_131F10246"/>
	</edges>
	<edges id="P_58F10246P_59F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10246" targetNode="P_59F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_134F10246_I" deadCode="false" sourceNode="P_34F10246" targetNode="P_45F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_134F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_134F10246_O" deadCode="false" sourceNode="P_34F10246" targetNode="P_46F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_134F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10246_I" deadCode="false" sourceNode="P_34F10246" targetNode="P_47F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_135F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10246_O" deadCode="false" sourceNode="P_34F10246" targetNode="P_48F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_135F10246"/>
	</edges>
	<edges id="P_34F10246P_35F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10246" targetNode="P_35F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10246_I" deadCode="false" sourceNode="P_36F10246" targetNode="P_58F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_138F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10246_O" deadCode="false" sourceNode="P_36F10246" targetNode="P_59F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_138F10246"/>
	</edges>
	<edges id="P_36F10246P_37F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10246" targetNode="P_37F10246"/>
	<edges id="P_38F10246P_39F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10246" targetNode="P_39F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10246_I" deadCode="false" sourceNode="P_40F10246" targetNode="P_36F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_143F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_143F10246_O" deadCode="false" sourceNode="P_40F10246" targetNode="P_37F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_143F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10246_I" deadCode="false" sourceNode="P_40F10246" targetNode="P_42F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_145F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10246_O" deadCode="false" sourceNode="P_40F10246" targetNode="P_43F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_145F10246"/>
	</edges>
	<edges id="P_40F10246P_41F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10246" targetNode="P_41F10246"/>
	<edges id="P_42F10246P_43F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10246" targetNode="P_43F10246"/>
	<edges id="P_50F10246P_51F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10246" targetNode="P_51F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_346F10246_I" deadCode="true" sourceNode="P_60F10246" targetNode="P_61F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_346F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_346F10246_O" deadCode="true" sourceNode="P_60F10246" targetNode="P_62F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_346F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_349F10246_I" deadCode="true" sourceNode="P_60F10246" targetNode="P_61F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_349F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_349F10246_O" deadCode="true" sourceNode="P_60F10246" targetNode="P_62F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_349F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_353F10246_I" deadCode="true" sourceNode="P_60F10246" targetNode="P_61F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_353F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_353F10246_O" deadCode="true" sourceNode="P_60F10246" targetNode="P_62F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_353F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_357F10246_I" deadCode="true" sourceNode="P_60F10246" targetNode="P_61F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_357F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_357F10246_O" deadCode="true" sourceNode="P_60F10246" targetNode="P_62F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_357F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_361F10246_I" deadCode="true" sourceNode="P_60F10246" targetNode="P_61F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_361F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_361F10246_O" deadCode="true" sourceNode="P_60F10246" targetNode="P_62F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_361F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_365F10246_I" deadCode="true" sourceNode="P_60F10246" targetNode="P_61F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_365F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_365F10246_O" deadCode="true" sourceNode="P_60F10246" targetNode="P_62F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_365F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_369F10246_I" deadCode="true" sourceNode="P_60F10246" targetNode="P_61F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_369F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_369F10246_O" deadCode="true" sourceNode="P_60F10246" targetNode="P_62F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_369F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_373F10246_I" deadCode="true" sourceNode="P_60F10246" targetNode="P_61F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_373F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_373F10246_O" deadCode="true" sourceNode="P_60F10246" targetNode="P_62F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_373F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_377F10246_I" deadCode="true" sourceNode="P_60F10246" targetNode="P_61F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_377F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_377F10246_O" deadCode="true" sourceNode="P_60F10246" targetNode="P_62F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_377F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_381F10246_I" deadCode="false" sourceNode="P_52F10246" targetNode="P_64F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_381F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_381F10246_O" deadCode="false" sourceNode="P_52F10246" targetNode="P_65F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_381F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_384F10246_I" deadCode="false" sourceNode="P_52F10246" targetNode="P_64F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_384F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_384F10246_O" deadCode="false" sourceNode="P_52F10246" targetNode="P_65F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_384F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_388F10246_I" deadCode="false" sourceNode="P_52F10246" targetNode="P_64F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_388F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_388F10246_O" deadCode="false" sourceNode="P_52F10246" targetNode="P_65F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_388F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_392F10246_I" deadCode="false" sourceNode="P_52F10246" targetNode="P_64F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_392F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_392F10246_O" deadCode="false" sourceNode="P_52F10246" targetNode="P_65F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_392F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_396F10246_I" deadCode="false" sourceNode="P_52F10246" targetNode="P_64F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_396F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_396F10246_O" deadCode="false" sourceNode="P_52F10246" targetNode="P_65F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_396F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_400F10246_I" deadCode="false" sourceNode="P_52F10246" targetNode="P_64F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_400F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_400F10246_O" deadCode="false" sourceNode="P_52F10246" targetNode="P_65F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_400F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_404F10246_I" deadCode="false" sourceNode="P_52F10246" targetNode="P_64F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_404F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_404F10246_O" deadCode="false" sourceNode="P_52F10246" targetNode="P_65F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_404F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_408F10246_I" deadCode="false" sourceNode="P_52F10246" targetNode="P_64F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_408F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_408F10246_O" deadCode="false" sourceNode="P_52F10246" targetNode="P_65F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_408F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_412F10246_I" deadCode="false" sourceNode="P_52F10246" targetNode="P_64F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_412F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_412F10246_O" deadCode="false" sourceNode="P_52F10246" targetNode="P_65F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_412F10246"/>
	</edges>
	<edges id="P_52F10246P_53F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10246" targetNode="P_53F10246"/>
	<edges id="P_45F10246P_46F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10246" targetNode="P_46F10246"/>
	<edges id="P_47F10246P_48F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10246" targetNode="P_48F10246"/>
	<edges id="P_54F10246P_55F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10246" targetNode="P_55F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_421F10246_I" deadCode="false" sourceNode="P_10F10246" targetNode="P_66F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_421F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_421F10246_O" deadCode="false" sourceNode="P_10F10246" targetNode="P_67F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_421F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_423F10246_I" deadCode="false" sourceNode="P_10F10246" targetNode="P_68F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_423F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_423F10246_O" deadCode="false" sourceNode="P_10F10246" targetNode="P_69F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_423F10246"/>
	</edges>
	<edges id="P_10F10246P_11F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10246" targetNode="P_11F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_428F10246_I" deadCode="false" sourceNode="P_66F10246" targetNode="P_61F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_428F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_428F10246_O" deadCode="false" sourceNode="P_66F10246" targetNode="P_62F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_428F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_433F10246_I" deadCode="false" sourceNode="P_66F10246" targetNode="P_61F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_433F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_433F10246_O" deadCode="false" sourceNode="P_66F10246" targetNode="P_62F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_433F10246"/>
	</edges>
	<edges id="P_66F10246P_67F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10246" targetNode="P_67F10246"/>
	<edges id="P_68F10246P_69F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10246" targetNode="P_69F10246"/>
	<edges id="P_61F10246P_62F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10246" targetNode="P_62F10246"/>
	<edges xsi:type="cbl:PerformEdge" id="S_462F10246_I" deadCode="false" sourceNode="P_64F10246" targetNode="P_72F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_462F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_462F10246_O" deadCode="false" sourceNode="P_64F10246" targetNode="P_73F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_462F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_463F10246_I" deadCode="false" sourceNode="P_64F10246" targetNode="P_74F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_463F10246"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_463F10246_O" deadCode="false" sourceNode="P_64F10246" targetNode="P_75F10246">
		<representations href="../../../cobol/LDBS9760.cbl.cobModel#S_463F10246"/>
	</edges>
	<edges id="P_64F10246P_65F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10246" targetNode="P_65F10246"/>
	<edges id="P_72F10246P_73F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10246" targetNode="P_73F10246"/>
	<edges id="P_74F10246P_75F10246" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10246" targetNode="P_75F10246"/>
	<edges xsi:type="cbl:DataEdge" id="S_65F10246_POS1" deadCode="false" targetNode="P_14F10246" sourceNode="DB2_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_65F10246"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_73F10246_POS1" deadCode="false" targetNode="P_16F10246" sourceNode="DB2_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_73F10246"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_76F10246_POS1" deadCode="false" targetNode="P_18F10246" sourceNode="DB2_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_76F10246"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_83F10246_POS1" deadCode="false" targetNode="P_22F10246" sourceNode="DB2_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_83F10246"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_101F10246_POS1" deadCode="false" targetNode="P_24F10246" sourceNode="DB2_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_101F10246"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_109F10246_POS1" deadCode="false" targetNode="P_26F10246" sourceNode="DB2_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_109F10246"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_112F10246_POS1" deadCode="false" targetNode="P_28F10246" sourceNode="DB2_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_112F10246"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_119F10246_POS1" deadCode="false" targetNode="P_32F10246" sourceNode="DB2_LIQ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_119F10246"></representations>
	</edges>
</Package>

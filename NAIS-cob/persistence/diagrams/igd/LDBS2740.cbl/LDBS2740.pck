<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS2740" cbl:id="LDBS2740" xsi:id="LDBS2740" packageRef="LDBS2740.igd#LDBS2740" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS2740_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10184" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS2740.cbl.cobModel#SC_1F10184"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10184" deadCode="false" name="PROGRAM_LDBS2740_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_1F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10184" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_2F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10184" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_3F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10184" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_8F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10184" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_9F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10184" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_4F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10184" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_5F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10184" deadCode="false" name="C210-DELETE-WC-NST">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_10F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10184" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_11F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10184" deadCode="true" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_12F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10184" deadCode="true" name="Z100-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_13F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10184" deadCode="true" name="Z150-VALORIZZA-DATA-SERVICES">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_14F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10184" deadCode="true" name="Z150-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_15F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10184" deadCode="true" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_16F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10184" deadCode="true" name="Z200-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_17F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10184" deadCode="true" name="Z400-SEQ-RIGA">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_18F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10184" deadCode="true" name="Z400-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_19F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10184" deadCode="true" name="Z500-AGGIORNAMENTO-STORICO">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_20F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10184" deadCode="true" name="Z500-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_21F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10184" deadCode="true" name="Z600-INSERT-NUOVA-RIGA-STORICA">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_22F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10184" deadCode="true" name="Z600-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_23F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10184" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_24F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10184" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_25F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10184" deadCode="true" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_26F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10184" deadCode="true" name="Z950-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_27F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10184" deadCode="true" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_28F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10184" deadCode="true" name="Z960-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_29F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10184" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_6F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10184" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_7F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10184" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_30F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10184" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_31F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10184" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_32F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10184" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_33F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10184" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_34F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10184" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_35F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10184" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_36F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10184" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_37F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10184" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_38F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10184" deadCode="true" name="Z800-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_43F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10184" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_39F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10184" deadCode="true" name="Z810-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_40F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10184" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_41F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10184" deadCode="true" name="Z820-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_42F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10184" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_44F10184"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10184" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS2740.cbl.cobModel#P_45F10184"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_BILA_VAR_CALC_P" name="BILA_VAR_CALC_P">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_BILA_VAR_CALC_P"/>
		</children>
	</packageNode>
	<edges id="SC_1F10184P_1F10184" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10184" targetNode="P_1F10184"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10184_I" deadCode="false" sourceNode="P_1F10184" targetNode="P_2F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_1F10184"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10184_O" deadCode="false" sourceNode="P_1F10184" targetNode="P_3F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_1F10184"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10184_I" deadCode="false" sourceNode="P_1F10184" targetNode="P_4F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_5F10184"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10184_O" deadCode="false" sourceNode="P_1F10184" targetNode="P_5F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_5F10184"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10184_I" deadCode="false" sourceNode="P_2F10184" targetNode="P_6F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_14F10184"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10184_O" deadCode="false" sourceNode="P_2F10184" targetNode="P_7F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_14F10184"/>
	</edges>
	<edges id="P_2F10184P_3F10184" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10184" targetNode="P_3F10184"/>
	<edges id="P_8F10184P_9F10184" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10184" targetNode="P_9F10184"/>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10184_I" deadCode="false" sourceNode="P_4F10184" targetNode="P_10F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_27F10184"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_27F10184_O" deadCode="false" sourceNode="P_4F10184" targetNode="P_11F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_27F10184"/>
	</edges>
	<edges id="P_4F10184P_5F10184" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10184" targetNode="P_5F10184"/>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10184_I" deadCode="false" sourceNode="P_10F10184" targetNode="P_8F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_31F10184"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10184_O" deadCode="false" sourceNode="P_10F10184" targetNode="P_9F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_31F10184"/>
	</edges>
	<edges id="P_10F10184P_11F10184" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10184" targetNode="P_11F10184"/>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10184_I" deadCode="false" sourceNode="P_6F10184" targetNode="P_30F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_51F10184"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_51F10184_O" deadCode="false" sourceNode="P_6F10184" targetNode="P_31F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_51F10184"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10184_I" deadCode="false" sourceNode="P_6F10184" targetNode="P_32F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_53F10184"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10184_O" deadCode="false" sourceNode="P_6F10184" targetNode="P_33F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_53F10184"/>
	</edges>
	<edges id="P_6F10184P_7F10184" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10184" targetNode="P_7F10184"/>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10184_I" deadCode="true" sourceNode="P_30F10184" targetNode="P_34F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_58F10184"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_58F10184_O" deadCode="true" sourceNode="P_30F10184" targetNode="P_35F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_58F10184"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10184_I" deadCode="true" sourceNode="P_30F10184" targetNode="P_34F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_63F10184"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10184_O" deadCode="true" sourceNode="P_30F10184" targetNode="P_35F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_63F10184"/>
	</edges>
	<edges id="P_30F10184P_31F10184" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10184" targetNode="P_31F10184"/>
	<edges id="P_32F10184P_33F10184" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10184" targetNode="P_33F10184"/>
	<edges id="P_34F10184P_35F10184" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10184" targetNode="P_35F10184"/>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10184_I" deadCode="true" sourceNode="P_38F10184" targetNode="P_39F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_92F10184"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10184_O" deadCode="true" sourceNode="P_38F10184" targetNode="P_40F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_92F10184"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10184_I" deadCode="true" sourceNode="P_38F10184" targetNode="P_41F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_93F10184"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10184_O" deadCode="true" sourceNode="P_38F10184" targetNode="P_42F10184">
		<representations href="../../../cobol/LDBS2740.cbl.cobModel#S_93F10184"/>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_30F10184_POS1" deadCode="false" sourceNode="P_10F10184" targetNode="DB2_BILA_VAR_CALC_P">
		<representations href="../../../cobol/../importantStmts.cobModel#S_30F10184"></representations>
	</edges>
</Package>

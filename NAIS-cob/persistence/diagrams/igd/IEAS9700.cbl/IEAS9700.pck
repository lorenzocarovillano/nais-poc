<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IEAS9700" cbl:id="IEAS9700" xsi:id="IEAS9700" packageRef="IEAS9700.igd#IEAS9700" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IEAS9700_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10107" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IEAS9700.cbl.cobModel#SC_1F10107"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10107" deadCode="false" name="1000-PRINCIPALE">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_1F10107"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10107" deadCode="false" name="1000-PRINCIPALE-FINE">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_12F10107"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10107" deadCode="false" name="1100-CHK-FORMALI-INPUT">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_2F10107"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10107" deadCode="false" name="1100-CHK-FORMALI-INPUT-EXIT">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_3F10107"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10107" deadCode="false" name="1200-ESTRAI-PROGRESSIVO">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_4F10107"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10107" deadCode="false" name="1200-ESTRAI-PROGRESSIVO-EXIT">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_5F10107"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10107" deadCode="false" name="1300-VALORIZZAZIONE-CAMPI">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_6F10107"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10107" deadCode="false" name="1300-VALORIZZAZIONE-CAMPI-EXIT">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_7F10107"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10107" deadCode="false" name="1305-CURRENT-TIMESTAMP">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_13F10107"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10107" deadCode="false" name="1305-CURRENT-TIMESTAMP-EXIT">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_14F10107"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10107" deadCode="false" name="1400-INSERIMENTO-LOG">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_8F10107"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10107" deadCode="false" name="1400-INSERIMENTO-LOG-EXIT">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_9F10107"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10107" deadCode="false" name="1401-SET-LENGTH-VCHAR">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_15F10107"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10107" deadCode="false" name="1401-EX">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_16F10107"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10107" deadCode="true" name="1410-COMMIT-LOG">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_17F10107"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10107" deadCode="true" name="1410-COMMIT-LOG-EXIT">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_18F10107"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10107" deadCode="false" name="1500-VALORIZZA-OUTPUT">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_10F10107"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10107" deadCode="false" name="1500-VALORIZZA-OUTPUT-EXIT">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_11F10107"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10107" deadCode="true" name="1600-CONTA-VALORE">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_19F10107"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10107" deadCode="true" name="1600-CONTA-VALORE-EXIT">
				<representations href="../../../cobol/IEAS9700.cbl.cobModel#P_20F10107"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_LOG_ERRORE" name="LOG_ERRORE">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_LOG_ERRORE"/>
		</children>
	</packageNode>
	<edges id="SC_1F10107P_1F10107" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10107" targetNode="P_1F10107"/>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10107_I" deadCode="false" sourceNode="P_1F10107" targetNode="P_2F10107">
		<representations href="../../../cobol/IEAS9700.cbl.cobModel#S_3F10107"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10107_O" deadCode="false" sourceNode="P_1F10107" targetNode="P_3F10107">
		<representations href="../../../cobol/IEAS9700.cbl.cobModel#S_3F10107"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10107_I" deadCode="false" sourceNode="P_1F10107" targetNode="P_4F10107">
		<representations href="../../../cobol/IEAS9700.cbl.cobModel#S_5F10107"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10107_O" deadCode="false" sourceNode="P_1F10107" targetNode="P_5F10107">
		<representations href="../../../cobol/IEAS9700.cbl.cobModel#S_5F10107"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10107_I" deadCode="false" sourceNode="P_1F10107" targetNode="P_6F10107">
		<representations href="../../../cobol/IEAS9700.cbl.cobModel#S_7F10107"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10107_O" deadCode="false" sourceNode="P_1F10107" targetNode="P_7F10107">
		<representations href="../../../cobol/IEAS9700.cbl.cobModel#S_7F10107"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10107_I" deadCode="false" sourceNode="P_1F10107" targetNode="P_8F10107">
		<representations href="../../../cobol/IEAS9700.cbl.cobModel#S_9F10107"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10107_O" deadCode="false" sourceNode="P_1F10107" targetNode="P_9F10107">
		<representations href="../../../cobol/IEAS9700.cbl.cobModel#S_9F10107"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10107_I" deadCode="false" sourceNode="P_1F10107" targetNode="P_10F10107">
		<representations href="../../../cobol/IEAS9700.cbl.cobModel#S_11F10107"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10107_O" deadCode="false" sourceNode="P_1F10107" targetNode="P_11F10107">
		<representations href="../../../cobol/IEAS9700.cbl.cobModel#S_11F10107"/>
	</edges>
	<edges id="P_1F10107P_12F10107" xsi:type="cbl:FallThroughEdge" sourceNode="P_1F10107" targetNode="P_12F10107"/>
	<edges id="P_2F10107P_3F10107" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10107" targetNode="P_3F10107"/>
	<edges id="P_4F10107P_5F10107" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10107" targetNode="P_5F10107"/>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10107_I" deadCode="false" sourceNode="P_6F10107" targetNode="P_13F10107">
		<representations href="../../../cobol/IEAS9700.cbl.cobModel#S_78F10107"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10107_O" deadCode="false" sourceNode="P_6F10107" targetNode="P_14F10107">
		<representations href="../../../cobol/IEAS9700.cbl.cobModel#S_78F10107"/>
	</edges>
	<edges id="P_6F10107P_7F10107" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10107" targetNode="P_7F10107"/>
	<edges id="P_13F10107P_14F10107" xsi:type="cbl:FallThroughEdge" sourceNode="P_13F10107" targetNode="P_14F10107"/>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10107_I" deadCode="false" sourceNode="P_8F10107" targetNode="P_15F10107">
		<representations href="../../../cobol/IEAS9700.cbl.cobModel#S_94F10107"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10107_O" deadCode="false" sourceNode="P_8F10107" targetNode="P_16F10107">
		<representations href="../../../cobol/IEAS9700.cbl.cobModel#S_94F10107"/>
	</edges>
	<edges id="P_8F10107P_9F10107" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10107" targetNode="P_9F10107"/>
	<edges id="P_15F10107P_16F10107" xsi:type="cbl:FallThroughEdge" sourceNode="P_15F10107" targetNode="P_16F10107"/>
	<edges id="P_10F10107P_11F10107" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10107" targetNode="P_11F10107"/>
	<edges xsi:type="cbl:DataEdge" id="S_50F10107_POS1" deadCode="false" targetNode="P_4F10107" sourceNode="DB2_LOG_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_50F10107"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_95F10107_POS1" deadCode="false" sourceNode="P_8F10107" targetNode="DB2_LOG_ERRORE">
		<representations href="../../../cobol/../importantStmts.cobModel#S_95F10107"></representations>
	</edges>
</Package>

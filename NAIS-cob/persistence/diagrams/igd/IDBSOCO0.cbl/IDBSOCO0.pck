<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IDBSOCO0" cbl:id="IDBSOCO0" xsi:id="IDBSOCO0" packageRef="IDBSOCO0.igd#IDBSOCO0" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IDBSOCO0_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10058" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#SC_1F10058"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10058" deadCode="false" name="PROGRAM_IDBSOCO0_FIRST_SENTENCES">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_1F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10058" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_2F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10058" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_3F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10058" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_28F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10058" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_29F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10058" deadCode="false" name="A200-ELABORA-PK">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_24F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10058" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_25F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10058" deadCode="false" name="A300-ELABORA-ID-EFF">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_4F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10058" deadCode="false" name="A300-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_5F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10058" deadCode="false" name="A400-ELABORA-IDP-EFF">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_6F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10058" deadCode="false" name="A400-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_7F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10058" deadCode="false" name="A500-ELABORA-IBO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_8F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10058" deadCode="false" name="A500-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_9F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10058" deadCode="false" name="A600-ELABORA-IBS">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_10F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10058" deadCode="false" name="A600-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_11F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10058" deadCode="false" name="A700-ELABORA-IDO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_12F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10058" deadCode="false" name="A700-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_13F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10058" deadCode="false" name="B300-ELABORA-ID-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_14F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10058" deadCode="false" name="B300-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_15F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10058" deadCode="false" name="B400-ELABORA-IDP-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_16F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10058" deadCode="false" name="B400-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_17F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10058" deadCode="false" name="B500-ELABORA-IBO-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_18F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10058" deadCode="false" name="B500-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_19F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10058" deadCode="false" name="B600-ELABORA-IBS-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_20F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10058" deadCode="false" name="B600-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_21F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10058" deadCode="false" name="B700-ELABORA-IDO-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_22F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10058" deadCode="false" name="B700-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_23F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10058" deadCode="false" name="A210-SELECT-PK">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_30F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10058" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_31F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10058" deadCode="false" name="A220-INSERT-PK">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_32F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10058" deadCode="false" name="A220-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_33F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10058" deadCode="false" name="A230-UPDATE-PK">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_34F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10058" deadCode="false" name="A230-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_35F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10058" deadCode="false" name="A240-DELETE-PK">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_36F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10058" deadCode="false" name="A240-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_37F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_142F10058" deadCode="false" name="A305-DECLARE-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_142F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_143F10058" deadCode="false" name="A305-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_143F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10058" deadCode="false" name="A310-SELECT-ID-EFF">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_38F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10058" deadCode="false" name="A310-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_39F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_144F10058" deadCode="false" name="A330-UPDATE-ID-EFF">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_144F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_145F10058" deadCode="false" name="A330-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_145F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_146F10058" deadCode="false" name="A360-OPEN-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_146F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_147F10058" deadCode="false" name="A360-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_147F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_148F10058" deadCode="false" name="A370-CLOSE-CURSOR-ID-EFF">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_148F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_149F10058" deadCode="false" name="A370-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_149F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_150F10058" deadCode="true" name="A380-FETCH-FIRST-ID-EFF">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_150F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_153F10058" deadCode="true" name="A380-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_153F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_151F10058" deadCode="false" name="A390-FETCH-NEXT-ID-EFF">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_151F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_152F10058" deadCode="false" name="A390-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_152F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_154F10058" deadCode="false" name="A405-DECLARE-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_154F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_155F10058" deadCode="false" name="A405-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_155F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10058" deadCode="false" name="A410-SELECT-IDP-EFF">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_44F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10058" deadCode="false" name="A410-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_45F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10058" deadCode="false" name="A460-OPEN-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_46F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10058" deadCode="false" name="A460-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_47F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10058" deadCode="false" name="A470-CLOSE-CURSOR-IDP-EFF">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_48F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10058" deadCode="false" name="A470-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_49F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10058" deadCode="false" name="A480-FETCH-FIRST-IDP-EFF">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_50F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10058" deadCode="false" name="A480-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_51F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10058" deadCode="false" name="A490-FETCH-NEXT-IDP-EFF">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_52F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10058" deadCode="false" name="A490-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_53F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_156F10058" deadCode="false" name="A505-DECLARE-CURSOR-IBO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_156F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_157F10058" deadCode="false" name="A505-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_157F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10058" deadCode="false" name="A510-SELECT-IBO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_54F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10058" deadCode="false" name="A510-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_55F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10058" deadCode="false" name="A560-OPEN-CURSOR-IBO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_56F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10058" deadCode="false" name="A560-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_57F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10058" deadCode="false" name="A570-CLOSE-CURSOR-IBO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_58F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10058" deadCode="false" name="A570-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_59F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10058" deadCode="false" name="A580-FETCH-FIRST-IBO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_60F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10058" deadCode="false" name="A580-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_61F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10058" deadCode="false" name="A590-FETCH-NEXT-IBO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_62F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10058" deadCode="false" name="A590-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_63F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_158F10058" deadCode="false" name="A605-DCL-CUR-IBS-RIFTO-ESTNO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_158F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_159F10058" deadCode="false" name="A605-RIFTO-ESTNO-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_159F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_160F10058" deadCode="false" name="A605-DCL-CUR-2O-RIFTO-ESTNO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_160F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_161F10058" deadCode="false" name="A605-2O-RIFTO-ESTNO-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_161F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_162F10058" deadCode="false" name="A605-DECLARE-CURSOR-IBS">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_162F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_163F10058" deadCode="false" name="A605-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_163F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_164F10058" deadCode="false" name="A610-SELECT-IBS-RIFTO-ESTNO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_164F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_165F10058" deadCode="false" name="A610-RIFTO-ESTNO-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_165F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_166F10058" deadCode="false" name="A610-SELECT-IBS-2O-RIFTO-ESTNO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_166F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_167F10058" deadCode="false" name="A610-2O-RIFTO-ESTNO-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_167F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10058" deadCode="false" name="A610-SELECT-IBS">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_64F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10058" deadCode="false" name="A610-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_65F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10058" deadCode="false" name="A660-OPEN-CURSOR-IBS">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_66F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10058" deadCode="false" name="A660-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_67F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10058" deadCode="false" name="A670-CLOSE-CURSOR-IBS">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_68F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10058" deadCode="false" name="A670-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_69F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10058" deadCode="false" name="A680-FETCH-FIRST-IBS">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_70F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10058" deadCode="false" name="A680-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_71F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_168F10058" deadCode="false" name="A690-FN-IBS-RIFTO-ESTNO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_168F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_169F10058" deadCode="false" name="A690-RIFTO-ESTNO-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_169F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_170F10058" deadCode="false" name="A690-FN-IBS-2O-RIFTO-ESTNO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_170F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_171F10058" deadCode="false" name="A690-2O-RIFTO-ESTNO-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_171F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10058" deadCode="false" name="A690-FETCH-NEXT-IBS">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_72F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10058" deadCode="false" name="A690-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_73F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_172F10058" deadCode="false" name="A705-DECLARE-CURSOR-IDO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_172F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_173F10058" deadCode="false" name="A705-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_173F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10058" deadCode="false" name="A710-SELECT-IDO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_74F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10058" deadCode="false" name="A710-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_75F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10058" deadCode="false" name="A760-OPEN-CURSOR-IDO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_76F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10058" deadCode="false" name="A760-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_77F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_78F10058" deadCode="false" name="A770-CLOSE-CURSOR-IDO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_78F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_79F10058" deadCode="false" name="A770-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_79F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_80F10058" deadCode="false" name="A780-FETCH-FIRST-IDO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_80F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_81F10058" deadCode="false" name="A780-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_81F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_82F10058" deadCode="false" name="A790-FETCH-NEXT-IDO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_82F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_83F10058" deadCode="false" name="A790-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_83F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_84F10058" deadCode="false" name="B310-SELECT-ID-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_84F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_85F10058" deadCode="false" name="B310-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_85F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_174F10058" deadCode="false" name="B405-DECLARE-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_174F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_175F10058" deadCode="false" name="B405-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_175F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_86F10058" deadCode="false" name="B410-SELECT-IDP-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_86F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_87F10058" deadCode="false" name="B410-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_87F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_88F10058" deadCode="false" name="B460-OPEN-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_88F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_89F10058" deadCode="false" name="B460-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_89F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_90F10058" deadCode="false" name="B470-CLOSE-CURSOR-IDP-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_90F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_91F10058" deadCode="false" name="B470-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_91F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_92F10058" deadCode="false" name="B480-FETCH-FIRST-IDP-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_92F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_93F10058" deadCode="false" name="B480-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_93F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_94F10058" deadCode="false" name="B490-FETCH-NEXT-IDP-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_94F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_95F10058" deadCode="false" name="B490-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_95F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_176F10058" deadCode="false" name="B505-DECLARE-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_176F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_177F10058" deadCode="false" name="B505-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_177F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_96F10058" deadCode="false" name="B510-SELECT-IBO-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_96F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_97F10058" deadCode="false" name="B510-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_97F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_98F10058" deadCode="false" name="B560-OPEN-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_98F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_99F10058" deadCode="false" name="B560-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_99F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_100F10058" deadCode="false" name="B570-CLOSE-CURSOR-IBO-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_100F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_101F10058" deadCode="false" name="B570-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_101F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_102F10058" deadCode="false" name="B580-FETCH-FIRST-IBO-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_102F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_103F10058" deadCode="false" name="B580-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_103F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_104F10058" deadCode="false" name="B590-FETCH-NEXT-IBO-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_104F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_105F10058" deadCode="false" name="B590-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_105F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_178F10058" deadCode="false" name="B605-DCL-CUR-IBS-RIFTO-ESTNO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_178F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_179F10058" deadCode="false" name="B605-RIFTO-ESTNO-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_179F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_180F10058" deadCode="false" name="B605-DCL-CUR-2O-RIFTO-ESTNO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_180F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_181F10058" deadCode="false" name="B605-2O-RIFTO-ESTNO-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_181F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_182F10058" deadCode="false" name="B605-DECLARE-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_182F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_183F10058" deadCode="false" name="B605-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_183F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_184F10058" deadCode="false" name="B610-SELECT-IBS-RIFTO-ESTNO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_184F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_185F10058" deadCode="false" name="B610-RIFTO-ESTNO-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_185F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_186F10058" deadCode="false" name="B610-SELECT-IBS-2O-RIFTO-ESTNO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_186F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_187F10058" deadCode="false" name="B610-2O-RIFTO-ESTNO-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_187F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_106F10058" deadCode="false" name="B610-SELECT-IBS-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_106F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_107F10058" deadCode="false" name="B610-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_107F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_108F10058" deadCode="false" name="B660-OPEN-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_108F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_109F10058" deadCode="false" name="B660-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_109F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_110F10058" deadCode="false" name="B670-CLOSE-CURSOR-IBS-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_110F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_111F10058" deadCode="false" name="B670-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_111F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_112F10058" deadCode="false" name="B680-FETCH-FIRST-IBS-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_112F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_113F10058" deadCode="false" name="B680-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_113F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_188F10058" deadCode="false" name="B690-FN-IBS-RIFTO-ESTNO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_188F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_189F10058" deadCode="false" name="B690-RIFTO-ESTNO-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_189F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_190F10058" deadCode="false" name="B690-FN-IBS-2O-RIFTO-ESTNO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_190F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_191F10058" deadCode="false" name="B690-2O-RIFTO-ESTNO-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_191F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_114F10058" deadCode="false" name="B690-FETCH-NEXT-IBS-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_114F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_115F10058" deadCode="false" name="B690-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_115F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_192F10058" deadCode="false" name="B705-DECLARE-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_192F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_193F10058" deadCode="false" name="B705-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_193F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_116F10058" deadCode="false" name="B710-SELECT-IDO-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_116F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_117F10058" deadCode="false" name="B710-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_117F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_118F10058" deadCode="false" name="B760-OPEN-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_118F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_119F10058" deadCode="false" name="B760-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_119F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_120F10058" deadCode="false" name="B770-CLOSE-CURSOR-IDO-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_120F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_121F10058" deadCode="false" name="B770-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_121F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_122F10058" deadCode="false" name="B780-FETCH-FIRST-IDO-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_122F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_123F10058" deadCode="false" name="B780-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_123F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_124F10058" deadCode="false" name="B790-FETCH-NEXT-IDO-CPZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_124F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_125F10058" deadCode="false" name="B790-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_125F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_128F10058" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_128F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_129F10058" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_129F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_134F10058" deadCode="false" name="Z150-VALORIZZA-DATA-SERVICES-I">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_134F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_135F10058" deadCode="false" name="Z150-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_135F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_140F10058" deadCode="false" name="Z160-VALORIZZA-DATA-SERVICES-U">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_140F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_141F10058" deadCode="false" name="Z160-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_141F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_136F10058" deadCode="false" name="Z200-SET-INDICATORI-NULL">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_136F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_137F10058" deadCode="false" name="Z200-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_137F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_132F10058" deadCode="false" name="Z400-SEQ-RIGA">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_132F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_133F10058" deadCode="false" name="Z400-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_133F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10058" deadCode="false" name="Z500-AGGIORNAMENTO-STORICO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_40F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10058" deadCode="false" name="Z500-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_41F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10058" deadCode="false" name="Z550-AGG-STORICO-SOLO-INS">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_42F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10058" deadCode="false" name="Z550-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_43F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_194F10058" deadCode="false" name="Z600-INSERT-NUOVA-RIGA-STORICA">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_194F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_195F10058" deadCode="false" name="Z600-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_195F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_138F10058" deadCode="false" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_138F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_139F10058" deadCode="false" name="Z900-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_139F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_130F10058" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_130F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_131F10058" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_131F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_126F10058" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_126F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_127F10058" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_127F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10058" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_26F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10058" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_27F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_200F10058" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_200F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_201F10058" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_201F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_202F10058" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_202F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_203F10058" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_203F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_196F10058" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_196F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_197F10058" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_197F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_204F10058" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_204F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_205F10058" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_205F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_198F10058" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_198F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_199F10058" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_199F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_206F10058" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_206F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_207F10058" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_207F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_208F10058" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_208F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_209F10058" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_209F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_210F10058" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_210F10058"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_211F10058" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#P_211F10058"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_OGG_COLLG" name="OGG_COLLG">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_OGG_COLLG"/>
		</children>
	</packageNode>
	<edges id="SC_1F10058P_1F10058" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10058" targetNode="P_1F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10058_I" deadCode="false" sourceNode="P_1F10058" targetNode="P_2F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_1F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10058_O" deadCode="false" sourceNode="P_1F10058" targetNode="P_3F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_1F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10058_I" deadCode="false" sourceNode="P_1F10058" targetNode="P_4F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_5F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10058_O" deadCode="false" sourceNode="P_1F10058" targetNode="P_5F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_5F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10058_I" deadCode="false" sourceNode="P_1F10058" targetNode="P_6F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_6F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10058_O" deadCode="false" sourceNode="P_1F10058" targetNode="P_7F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_6F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10058_I" deadCode="false" sourceNode="P_1F10058" targetNode="P_8F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_7F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10058_O" deadCode="false" sourceNode="P_1F10058" targetNode="P_9F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_7F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10058_I" deadCode="false" sourceNode="P_1F10058" targetNode="P_10F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_8F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_8F10058_O" deadCode="false" sourceNode="P_1F10058" targetNode="P_11F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_8F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10058_I" deadCode="false" sourceNode="P_1F10058" targetNode="P_12F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_9F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_9F10058_O" deadCode="false" sourceNode="P_1F10058" targetNode="P_13F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_9F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10058_I" deadCode="false" sourceNode="P_1F10058" targetNode="P_14F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_13F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10058_O" deadCode="false" sourceNode="P_1F10058" targetNode="P_15F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_13F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10058_I" deadCode="false" sourceNode="P_1F10058" targetNode="P_16F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_14F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10058_O" deadCode="false" sourceNode="P_1F10058" targetNode="P_17F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_14F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10058_I" deadCode="false" sourceNode="P_1F10058" targetNode="P_18F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_15F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10058_O" deadCode="false" sourceNode="P_1F10058" targetNode="P_19F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_15F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10058_I" deadCode="false" sourceNode="P_1F10058" targetNode="P_20F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_16F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_16F10058_O" deadCode="false" sourceNode="P_1F10058" targetNode="P_21F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_16F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10058_I" deadCode="false" sourceNode="P_1F10058" targetNode="P_22F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_17F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_17F10058_O" deadCode="false" sourceNode="P_1F10058" targetNode="P_23F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_17F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10058_I" deadCode="false" sourceNode="P_1F10058" targetNode="P_24F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_21F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_21F10058_O" deadCode="false" sourceNode="P_1F10058" targetNode="P_25F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_21F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10058_I" deadCode="false" sourceNode="P_1F10058" targetNode="P_8F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_22F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_22F10058_O" deadCode="false" sourceNode="P_1F10058" targetNode="P_9F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_22F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10058_I" deadCode="false" sourceNode="P_1F10058" targetNode="P_10F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_23F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_23F10058_O" deadCode="false" sourceNode="P_1F10058" targetNode="P_11F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_23F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10058_I" deadCode="false" sourceNode="P_1F10058" targetNode="P_12F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_24F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10058_O" deadCode="false" sourceNode="P_1F10058" targetNode="P_13F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_24F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10058_I" deadCode="false" sourceNode="P_2F10058" targetNode="P_26F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_33F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_33F10058_O" deadCode="false" sourceNode="P_2F10058" targetNode="P_27F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_33F10058"/>
	</edges>
	<edges id="P_2F10058P_3F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10058" targetNode="P_3F10058"/>
	<edges id="P_28F10058P_29F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10058" targetNode="P_29F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10058_I" deadCode="false" sourceNode="P_24F10058" targetNode="P_30F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_46F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10058_O" deadCode="false" sourceNode="P_24F10058" targetNode="P_31F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_46F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10058_I" deadCode="false" sourceNode="P_24F10058" targetNode="P_32F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_47F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10058_O" deadCode="false" sourceNode="P_24F10058" targetNode="P_33F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_47F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10058_I" deadCode="false" sourceNode="P_24F10058" targetNode="P_34F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_48F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10058_O" deadCode="false" sourceNode="P_24F10058" targetNode="P_35F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_48F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10058_I" deadCode="false" sourceNode="P_24F10058" targetNode="P_36F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_49F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10058_O" deadCode="false" sourceNode="P_24F10058" targetNode="P_37F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_49F10058"/>
	</edges>
	<edges id="P_24F10058P_25F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10058" targetNode="P_25F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10058_I" deadCode="false" sourceNode="P_4F10058" targetNode="P_38F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_53F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10058_O" deadCode="false" sourceNode="P_4F10058" targetNode="P_39F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_53F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10058_I" deadCode="false" sourceNode="P_4F10058" targetNode="P_40F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_54F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10058_O" deadCode="false" sourceNode="P_4F10058" targetNode="P_41F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_54F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10058_I" deadCode="false" sourceNode="P_4F10058" targetNode="P_42F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_55F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10058_O" deadCode="false" sourceNode="P_4F10058" targetNode="P_43F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_55F10058"/>
	</edges>
	<edges id="P_4F10058P_5F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10058" targetNode="P_5F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10058_I" deadCode="false" sourceNode="P_6F10058" targetNode="P_44F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_59F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_59F10058_O" deadCode="false" sourceNode="P_6F10058" targetNode="P_45F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_59F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10058_I" deadCode="false" sourceNode="P_6F10058" targetNode="P_46F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_60F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10058_O" deadCode="false" sourceNode="P_6F10058" targetNode="P_47F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_60F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10058_I" deadCode="false" sourceNode="P_6F10058" targetNode="P_48F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_61F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10058_O" deadCode="false" sourceNode="P_6F10058" targetNode="P_49F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_61F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10058_I" deadCode="false" sourceNode="P_6F10058" targetNode="P_50F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_62F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_62F10058_O" deadCode="false" sourceNode="P_6F10058" targetNode="P_51F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_62F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10058_I" deadCode="false" sourceNode="P_6F10058" targetNode="P_52F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_63F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10058_O" deadCode="false" sourceNode="P_6F10058" targetNode="P_53F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_63F10058"/>
	</edges>
	<edges id="P_6F10058P_7F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10058" targetNode="P_7F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10058_I" deadCode="false" sourceNode="P_8F10058" targetNode="P_54F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_67F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_67F10058_O" deadCode="false" sourceNode="P_8F10058" targetNode="P_55F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_67F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10058_I" deadCode="false" sourceNode="P_8F10058" targetNode="P_56F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_68F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10058_O" deadCode="false" sourceNode="P_8F10058" targetNode="P_57F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_68F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10058_I" deadCode="false" sourceNode="P_8F10058" targetNode="P_58F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_69F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_69F10058_O" deadCode="false" sourceNode="P_8F10058" targetNode="P_59F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_69F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10058_I" deadCode="false" sourceNode="P_8F10058" targetNode="P_60F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_70F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10058_O" deadCode="false" sourceNode="P_8F10058" targetNode="P_61F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_70F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10058_I" deadCode="false" sourceNode="P_8F10058" targetNode="P_62F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_71F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10058_O" deadCode="false" sourceNode="P_8F10058" targetNode="P_63F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_71F10058"/>
	</edges>
	<edges id="P_8F10058P_9F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10058" targetNode="P_9F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10058_I" deadCode="false" sourceNode="P_10F10058" targetNode="P_64F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_75F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10058_O" deadCode="false" sourceNode="P_10F10058" targetNode="P_65F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_75F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10058_I" deadCode="false" sourceNode="P_10F10058" targetNode="P_66F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_76F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10058_O" deadCode="false" sourceNode="P_10F10058" targetNode="P_67F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_76F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10058_I" deadCode="false" sourceNode="P_10F10058" targetNode="P_68F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_77F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10058_O" deadCode="false" sourceNode="P_10F10058" targetNode="P_69F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_77F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10058_I" deadCode="false" sourceNode="P_10F10058" targetNode="P_70F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_78F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_78F10058_O" deadCode="false" sourceNode="P_10F10058" targetNode="P_71F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_78F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10058_I" deadCode="false" sourceNode="P_10F10058" targetNode="P_72F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_79F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10058_O" deadCode="false" sourceNode="P_10F10058" targetNode="P_73F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_79F10058"/>
	</edges>
	<edges id="P_10F10058P_11F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10058" targetNode="P_11F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10058_I" deadCode="false" sourceNode="P_12F10058" targetNode="P_74F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_83F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10058_O" deadCode="false" sourceNode="P_12F10058" targetNode="P_75F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_83F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10058_I" deadCode="false" sourceNode="P_12F10058" targetNode="P_76F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_84F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10058_O" deadCode="false" sourceNode="P_12F10058" targetNode="P_77F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_84F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10058_I" deadCode="false" sourceNode="P_12F10058" targetNode="P_78F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_85F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10058_O" deadCode="false" sourceNode="P_12F10058" targetNode="P_79F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_85F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10058_I" deadCode="false" sourceNode="P_12F10058" targetNode="P_80F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_86F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10058_O" deadCode="false" sourceNode="P_12F10058" targetNode="P_81F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_86F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10058_I" deadCode="false" sourceNode="P_12F10058" targetNode="P_82F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_87F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10058_O" deadCode="false" sourceNode="P_12F10058" targetNode="P_83F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_87F10058"/>
	</edges>
	<edges id="P_12F10058P_13F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10058" targetNode="P_13F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10058_I" deadCode="false" sourceNode="P_14F10058" targetNode="P_84F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_91F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_91F10058_O" deadCode="false" sourceNode="P_14F10058" targetNode="P_85F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_91F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10058_I" deadCode="false" sourceNode="P_14F10058" targetNode="P_40F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_92F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10058_O" deadCode="false" sourceNode="P_14F10058" targetNode="P_41F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_92F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10058_I" deadCode="false" sourceNode="P_14F10058" targetNode="P_42F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_93F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10058_O" deadCode="false" sourceNode="P_14F10058" targetNode="P_43F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_93F10058"/>
	</edges>
	<edges id="P_14F10058P_15F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10058" targetNode="P_15F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10058_I" deadCode="false" sourceNode="P_16F10058" targetNode="P_86F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_97F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10058_O" deadCode="false" sourceNode="P_16F10058" targetNode="P_87F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_97F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10058_I" deadCode="false" sourceNode="P_16F10058" targetNode="P_88F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_98F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10058_O" deadCode="false" sourceNode="P_16F10058" targetNode="P_89F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_98F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10058_I" deadCode="false" sourceNode="P_16F10058" targetNode="P_90F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_99F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10058_O" deadCode="false" sourceNode="P_16F10058" targetNode="P_91F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_99F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10058_I" deadCode="false" sourceNode="P_16F10058" targetNode="P_92F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_100F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_100F10058_O" deadCode="false" sourceNode="P_16F10058" targetNode="P_93F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_100F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10058_I" deadCode="false" sourceNode="P_16F10058" targetNode="P_94F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_101F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10058_O" deadCode="false" sourceNode="P_16F10058" targetNode="P_95F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_101F10058"/>
	</edges>
	<edges id="P_16F10058P_17F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10058" targetNode="P_17F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10058_I" deadCode="false" sourceNode="P_18F10058" targetNode="P_96F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_105F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10058_O" deadCode="false" sourceNode="P_18F10058" targetNode="P_97F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_105F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10058_I" deadCode="false" sourceNode="P_18F10058" targetNode="P_98F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_106F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10058_O" deadCode="false" sourceNode="P_18F10058" targetNode="P_99F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_106F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10058_I" deadCode="false" sourceNode="P_18F10058" targetNode="P_100F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_107F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10058_O" deadCode="false" sourceNode="P_18F10058" targetNode="P_101F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_107F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10058_I" deadCode="false" sourceNode="P_18F10058" targetNode="P_102F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_108F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10058_O" deadCode="false" sourceNode="P_18F10058" targetNode="P_103F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_108F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10058_I" deadCode="false" sourceNode="P_18F10058" targetNode="P_104F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_109F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10058_O" deadCode="false" sourceNode="P_18F10058" targetNode="P_105F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_109F10058"/>
	</edges>
	<edges id="P_18F10058P_19F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10058" targetNode="P_19F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10058_I" deadCode="false" sourceNode="P_20F10058" targetNode="P_106F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_113F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_113F10058_O" deadCode="false" sourceNode="P_20F10058" targetNode="P_107F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_113F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10058_I" deadCode="false" sourceNode="P_20F10058" targetNode="P_108F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_114F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10058_O" deadCode="false" sourceNode="P_20F10058" targetNode="P_109F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_114F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10058_I" deadCode="false" sourceNode="P_20F10058" targetNode="P_110F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_115F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10058_O" deadCode="false" sourceNode="P_20F10058" targetNode="P_111F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_115F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10058_I" deadCode="false" sourceNode="P_20F10058" targetNode="P_112F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_116F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_116F10058_O" deadCode="false" sourceNode="P_20F10058" targetNode="P_113F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_116F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10058_I" deadCode="false" sourceNode="P_20F10058" targetNode="P_114F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_117F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10058_O" deadCode="false" sourceNode="P_20F10058" targetNode="P_115F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_117F10058"/>
	</edges>
	<edges id="P_20F10058P_21F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10058" targetNode="P_21F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10058_I" deadCode="false" sourceNode="P_22F10058" targetNode="P_116F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_121F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10058_O" deadCode="false" sourceNode="P_22F10058" targetNode="P_117F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_121F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10058_I" deadCode="false" sourceNode="P_22F10058" targetNode="P_118F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_122F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10058_O" deadCode="false" sourceNode="P_22F10058" targetNode="P_119F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_122F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10058_I" deadCode="false" sourceNode="P_22F10058" targetNode="P_120F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_123F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_123F10058_O" deadCode="false" sourceNode="P_22F10058" targetNode="P_121F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_123F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10058_I" deadCode="false" sourceNode="P_22F10058" targetNode="P_122F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_124F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10058_O" deadCode="false" sourceNode="P_22F10058" targetNode="P_123F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_124F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10058_I" deadCode="false" sourceNode="P_22F10058" targetNode="P_124F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_125F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10058_O" deadCode="false" sourceNode="P_22F10058" targetNode="P_125F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_125F10058"/>
	</edges>
	<edges id="P_22F10058P_23F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10058" targetNode="P_23F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10058_I" deadCode="false" sourceNode="P_30F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_128F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10058_O" deadCode="false" sourceNode="P_30F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_128F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10058_I" deadCode="false" sourceNode="P_30F10058" targetNode="P_28F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_130F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10058_O" deadCode="false" sourceNode="P_30F10058" targetNode="P_29F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_130F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10058_I" deadCode="false" sourceNode="P_30F10058" targetNode="P_128F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_132F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10058_O" deadCode="false" sourceNode="P_30F10058" targetNode="P_129F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_132F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10058_I" deadCode="false" sourceNode="P_30F10058" targetNode="P_130F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_133F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10058_O" deadCode="false" sourceNode="P_30F10058" targetNode="P_131F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_133F10058"/>
	</edges>
	<edges id="P_30F10058P_31F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10058" targetNode="P_31F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10058_I" deadCode="false" sourceNode="P_32F10058" targetNode="P_132F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_135F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_135F10058_O" deadCode="false" sourceNode="P_32F10058" targetNode="P_133F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_135F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10058_I" deadCode="false" sourceNode="P_32F10058" targetNode="P_134F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_137F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10058_O" deadCode="false" sourceNode="P_32F10058" targetNode="P_135F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_137F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10058_I" deadCode="false" sourceNode="P_32F10058" targetNode="P_136F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_138F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_138F10058_O" deadCode="false" sourceNode="P_32F10058" targetNode="P_137F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_138F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10058_I" deadCode="false" sourceNode="P_32F10058" targetNode="P_138F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_139F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_139F10058_O" deadCode="false" sourceNode="P_32F10058" targetNode="P_139F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_139F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10058_I" deadCode="false" sourceNode="P_32F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_140F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10058_O" deadCode="false" sourceNode="P_32F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_140F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10058_I" deadCode="false" sourceNode="P_32F10058" targetNode="P_28F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_142F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10058_O" deadCode="false" sourceNode="P_32F10058" targetNode="P_29F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_142F10058"/>
	</edges>
	<edges id="P_32F10058P_33F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10058" targetNode="P_33F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10058_I" deadCode="false" sourceNode="P_34F10058" targetNode="P_140F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_144F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_144F10058_O" deadCode="false" sourceNode="P_34F10058" targetNode="P_141F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_144F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10058_I" deadCode="false" sourceNode="P_34F10058" targetNode="P_136F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_145F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10058_O" deadCode="false" sourceNode="P_34F10058" targetNode="P_137F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_145F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10058_I" deadCode="false" sourceNode="P_34F10058" targetNode="P_138F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_146F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_146F10058_O" deadCode="false" sourceNode="P_34F10058" targetNode="P_139F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_146F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10058_I" deadCode="false" sourceNode="P_34F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_147F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10058_O" deadCode="false" sourceNode="P_34F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_147F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10058_I" deadCode="false" sourceNode="P_34F10058" targetNode="P_28F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_149F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_149F10058_O" deadCode="false" sourceNode="P_34F10058" targetNode="P_29F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_149F10058"/>
	</edges>
	<edges id="P_34F10058P_35F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10058" targetNode="P_35F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10058_I" deadCode="false" sourceNode="P_36F10058" targetNode="P_28F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_152F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10058_O" deadCode="false" sourceNode="P_36F10058" targetNode="P_29F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_152F10058"/>
	</edges>
	<edges id="P_36F10058P_37F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10058" targetNode="P_37F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10058_I" deadCode="false" sourceNode="P_142F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_154F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10058_O" deadCode="false" sourceNode="P_142F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_154F10058"/>
	</edges>
	<edges id="P_142F10058P_143F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_142F10058" targetNode="P_143F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10058_I" deadCode="false" sourceNode="P_38F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_158F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_158F10058_O" deadCode="false" sourceNode="P_38F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_158F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10058_I" deadCode="false" sourceNode="P_38F10058" targetNode="P_28F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_160F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_160F10058_O" deadCode="false" sourceNode="P_38F10058" targetNode="P_29F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_160F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10058_I" deadCode="false" sourceNode="P_38F10058" targetNode="P_128F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_162F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_162F10058_O" deadCode="false" sourceNode="P_38F10058" targetNode="P_129F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_162F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10058_I" deadCode="false" sourceNode="P_38F10058" targetNode="P_130F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_163F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10058_O" deadCode="false" sourceNode="P_38F10058" targetNode="P_131F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_163F10058"/>
	</edges>
	<edges id="P_38F10058P_39F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10058" targetNode="P_39F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10058_I" deadCode="false" sourceNode="P_144F10058" targetNode="P_140F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_165F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10058_O" deadCode="false" sourceNode="P_144F10058" targetNode="P_141F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_165F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10058_I" deadCode="false" sourceNode="P_144F10058" targetNode="P_136F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_166F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10058_O" deadCode="false" sourceNode="P_144F10058" targetNode="P_137F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_166F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10058_I" deadCode="false" sourceNode="P_144F10058" targetNode="P_138F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_167F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_167F10058_O" deadCode="false" sourceNode="P_144F10058" targetNode="P_139F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_167F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10058_I" deadCode="false" sourceNode="P_144F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_168F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_168F10058_O" deadCode="false" sourceNode="P_144F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_168F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10058_I" deadCode="false" sourceNode="P_144F10058" targetNode="P_28F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_170F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10058_O" deadCode="false" sourceNode="P_144F10058" targetNode="P_29F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_170F10058"/>
	</edges>
	<edges id="P_144F10058P_145F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_144F10058" targetNode="P_145F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10058_I" deadCode="false" sourceNode="P_146F10058" targetNode="P_142F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_172F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_172F10058_O" deadCode="false" sourceNode="P_146F10058" targetNode="P_143F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_172F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10058_I" deadCode="false" sourceNode="P_146F10058" targetNode="P_28F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_174F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10058_O" deadCode="false" sourceNode="P_146F10058" targetNode="P_29F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_174F10058"/>
	</edges>
	<edges id="P_146F10058P_147F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_146F10058" targetNode="P_147F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10058_I" deadCode="false" sourceNode="P_148F10058" targetNode="P_28F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_177F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_177F10058_O" deadCode="false" sourceNode="P_148F10058" targetNode="P_29F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_177F10058"/>
	</edges>
	<edges id="P_148F10058P_149F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_148F10058" targetNode="P_149F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10058_I" deadCode="true" sourceNode="P_150F10058" targetNode="P_146F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_179F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_179F10058_O" deadCode="true" sourceNode="P_150F10058" targetNode="P_147F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_179F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10058_I" deadCode="true" sourceNode="P_150F10058" targetNode="P_151F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_181F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10058_O" deadCode="true" sourceNode="P_150F10058" targetNode="P_152F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_181F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10058_I" deadCode="false" sourceNode="P_151F10058" targetNode="P_28F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_184F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10058_O" deadCode="false" sourceNode="P_151F10058" targetNode="P_29F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_184F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10058_I" deadCode="false" sourceNode="P_151F10058" targetNode="P_128F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_186F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_186F10058_O" deadCode="false" sourceNode="P_151F10058" targetNode="P_129F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_186F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10058_I" deadCode="false" sourceNode="P_151F10058" targetNode="P_130F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_187F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10058_O" deadCode="false" sourceNode="P_151F10058" targetNode="P_131F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_187F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10058_I" deadCode="false" sourceNode="P_151F10058" targetNode="P_148F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_189F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_189F10058_O" deadCode="false" sourceNode="P_151F10058" targetNode="P_149F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_189F10058"/>
	</edges>
	<edges id="P_151F10058P_152F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_151F10058" targetNode="P_152F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_193F10058_I" deadCode="false" sourceNode="P_154F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_193F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_193F10058_O" deadCode="false" sourceNode="P_154F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_193F10058"/>
	</edges>
	<edges id="P_154F10058P_155F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_154F10058" targetNode="P_155F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_196F10058_I" deadCode="false" sourceNode="P_44F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_196F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_196F10058_O" deadCode="false" sourceNode="P_44F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_196F10058"/>
	</edges>
	<edges id="P_44F10058P_45F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10058" targetNode="P_45F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_199F10058_I" deadCode="false" sourceNode="P_46F10058" targetNode="P_154F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_199F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_199F10058_O" deadCode="false" sourceNode="P_46F10058" targetNode="P_155F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_199F10058"/>
	</edges>
	<edges id="P_46F10058P_47F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10058" targetNode="P_47F10058"/>
	<edges id="P_48F10058P_49F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10058" targetNode="P_49F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_204F10058_I" deadCode="false" sourceNode="P_50F10058" targetNode="P_46F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_204F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_204F10058_O" deadCode="false" sourceNode="P_50F10058" targetNode="P_47F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_204F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_206F10058_I" deadCode="false" sourceNode="P_50F10058" targetNode="P_52F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_206F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_206F10058_O" deadCode="false" sourceNode="P_50F10058" targetNode="P_53F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_206F10058"/>
	</edges>
	<edges id="P_50F10058P_51F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10058" targetNode="P_51F10058"/>
	<edges id="P_52F10058P_53F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10058" targetNode="P_53F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_210F10058_I" deadCode="false" sourceNode="P_156F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_210F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_210F10058_O" deadCode="false" sourceNode="P_156F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_210F10058"/>
	</edges>
	<edges id="P_156F10058P_157F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_156F10058" targetNode="P_157F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10058_I" deadCode="false" sourceNode="P_54F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_213F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_213F10058_O" deadCode="false" sourceNode="P_54F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_213F10058"/>
	</edges>
	<edges id="P_54F10058P_55F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10058" targetNode="P_55F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10058_I" deadCode="false" sourceNode="P_56F10058" targetNode="P_156F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_216F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_216F10058_O" deadCode="false" sourceNode="P_56F10058" targetNode="P_157F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_216F10058"/>
	</edges>
	<edges id="P_56F10058P_57F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10058" targetNode="P_57F10058"/>
	<edges id="P_58F10058P_59F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10058" targetNode="P_59F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10058_I" deadCode="false" sourceNode="P_60F10058" targetNode="P_56F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_221F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_221F10058_O" deadCode="false" sourceNode="P_60F10058" targetNode="P_57F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_221F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_223F10058_I" deadCode="false" sourceNode="P_60F10058" targetNode="P_62F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_223F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_223F10058_O" deadCode="false" sourceNode="P_60F10058" targetNode="P_63F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_223F10058"/>
	</edges>
	<edges id="P_60F10058P_61F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_60F10058" targetNode="P_61F10058"/>
	<edges id="P_62F10058P_63F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10058" targetNode="P_63F10058"/>
	<edges id="P_158F10058P_159F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_158F10058" targetNode="P_159F10058"/>
	<edges id="P_160F10058P_161F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_160F10058" targetNode="P_161F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_233F10058_I" deadCode="false" sourceNode="P_162F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_233F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_233F10058_O" deadCode="false" sourceNode="P_162F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_233F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_235F10058_I" deadCode="false" sourceNode="P_162F10058" targetNode="P_158F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_235F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_235F10058_O" deadCode="false" sourceNode="P_162F10058" targetNode="P_159F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_235F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_237F10058_I" deadCode="false" sourceNode="P_162F10058" targetNode="P_160F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_237F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_237F10058_O" deadCode="false" sourceNode="P_162F10058" targetNode="P_161F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_237F10058"/>
	</edges>
	<edges id="P_162F10058P_163F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_162F10058" targetNode="P_163F10058"/>
	<edges id="P_164F10058P_165F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_164F10058" targetNode="P_165F10058"/>
	<edges id="P_166F10058P_167F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_166F10058" targetNode="P_167F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_243F10058_I" deadCode="false" sourceNode="P_64F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_243F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_243F10058_O" deadCode="false" sourceNode="P_64F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_243F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_245F10058_I" deadCode="false" sourceNode="P_64F10058" targetNode="P_164F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_245F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_245F10058_O" deadCode="false" sourceNode="P_64F10058" targetNode="P_165F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_245F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_247F10058_I" deadCode="false" sourceNode="P_64F10058" targetNode="P_166F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_247F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_247F10058_O" deadCode="false" sourceNode="P_64F10058" targetNode="P_167F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_247F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_248F10058_I" deadCode="false" sourceNode="P_64F10058" targetNode="P_28F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_248F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_248F10058_O" deadCode="false" sourceNode="P_64F10058" targetNode="P_29F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_248F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_250F10058_I" deadCode="false" sourceNode="P_64F10058" targetNode="P_128F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_250F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_250F10058_O" deadCode="false" sourceNode="P_64F10058" targetNode="P_129F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_250F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_251F10058_I" deadCode="false" sourceNode="P_64F10058" targetNode="P_130F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_251F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_251F10058_O" deadCode="false" sourceNode="P_64F10058" targetNode="P_131F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_251F10058"/>
	</edges>
	<edges id="P_64F10058P_65F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10058" targetNode="P_65F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10058_I" deadCode="false" sourceNode="P_66F10058" targetNode="P_162F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_253F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10058_O" deadCode="false" sourceNode="P_66F10058" targetNode="P_163F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_253F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_258F10058_I" deadCode="false" sourceNode="P_66F10058" targetNode="P_28F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_258F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_258F10058_O" deadCode="false" sourceNode="P_66F10058" targetNode="P_29F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_258F10058"/>
	</edges>
	<edges id="P_66F10058P_67F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10058" targetNode="P_67F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_264F10058_I" deadCode="false" sourceNode="P_68F10058" targetNode="P_28F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_264F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_264F10058_O" deadCode="false" sourceNode="P_68F10058" targetNode="P_29F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_264F10058"/>
	</edges>
	<edges id="P_68F10058P_69F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10058" targetNode="P_69F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_266F10058_I" deadCode="false" sourceNode="P_70F10058" targetNode="P_66F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_266F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_266F10058_O" deadCode="false" sourceNode="P_70F10058" targetNode="P_67F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_266F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_268F10058_I" deadCode="false" sourceNode="P_70F10058" targetNode="P_72F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_268F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_268F10058_O" deadCode="false" sourceNode="P_70F10058" targetNode="P_73F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_268F10058"/>
	</edges>
	<edges id="P_70F10058P_71F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_70F10058" targetNode="P_71F10058"/>
	<edges id="P_168F10058P_169F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_168F10058" targetNode="P_169F10058"/>
	<edges id="P_170F10058P_171F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_170F10058" targetNode="P_171F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_275F10058_I" deadCode="false" sourceNode="P_72F10058" targetNode="P_168F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_275F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_275F10058_O" deadCode="false" sourceNode="P_72F10058" targetNode="P_169F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_275F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_277F10058_I" deadCode="false" sourceNode="P_72F10058" targetNode="P_170F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_277F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_277F10058_O" deadCode="false" sourceNode="P_72F10058" targetNode="P_171F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_277F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10058_I" deadCode="false" sourceNode="P_72F10058" targetNode="P_28F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_278F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_278F10058_O" deadCode="false" sourceNode="P_72F10058" targetNode="P_29F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_278F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10058_I" deadCode="false" sourceNode="P_72F10058" targetNode="P_128F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_280F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_280F10058_O" deadCode="false" sourceNode="P_72F10058" targetNode="P_129F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_280F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10058_I" deadCode="false" sourceNode="P_72F10058" targetNode="P_130F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_281F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_281F10058_O" deadCode="false" sourceNode="P_72F10058" targetNode="P_131F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_281F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10058_I" deadCode="false" sourceNode="P_72F10058" targetNode="P_68F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_283F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_283F10058_O" deadCode="false" sourceNode="P_72F10058" targetNode="P_69F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_283F10058"/>
	</edges>
	<edges id="P_72F10058P_73F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10058" targetNode="P_73F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_287F10058_I" deadCode="false" sourceNode="P_172F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_287F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_287F10058_O" deadCode="false" sourceNode="P_172F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_287F10058"/>
	</edges>
	<edges id="P_172F10058P_173F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_172F10058" targetNode="P_173F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_290F10058_I" deadCode="false" sourceNode="P_74F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_290F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_290F10058_O" deadCode="false" sourceNode="P_74F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_290F10058"/>
	</edges>
	<edges id="P_74F10058P_75F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10058" targetNode="P_75F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_293F10058_I" deadCode="false" sourceNode="P_76F10058" targetNode="P_172F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_293F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_293F10058_O" deadCode="false" sourceNode="P_76F10058" targetNode="P_173F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_293F10058"/>
	</edges>
	<edges id="P_76F10058P_77F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_76F10058" targetNode="P_77F10058"/>
	<edges id="P_78F10058P_79F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_78F10058" targetNode="P_79F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_298F10058_I" deadCode="false" sourceNode="P_80F10058" targetNode="P_76F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_298F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_298F10058_O" deadCode="false" sourceNode="P_80F10058" targetNode="P_77F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_298F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_300F10058_I" deadCode="false" sourceNode="P_80F10058" targetNode="P_82F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_300F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_300F10058_O" deadCode="false" sourceNode="P_80F10058" targetNode="P_83F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_300F10058"/>
	</edges>
	<edges id="P_80F10058P_81F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_80F10058" targetNode="P_81F10058"/>
	<edges id="P_82F10058P_83F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_82F10058" targetNode="P_83F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_304F10058_I" deadCode="false" sourceNode="P_84F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_304F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_304F10058_O" deadCode="false" sourceNode="P_84F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_304F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_306F10058_I" deadCode="false" sourceNode="P_84F10058" targetNode="P_28F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_306F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_306F10058_O" deadCode="false" sourceNode="P_84F10058" targetNode="P_29F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_306F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10058_I" deadCode="false" sourceNode="P_84F10058" targetNode="P_128F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_308F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_308F10058_O" deadCode="false" sourceNode="P_84F10058" targetNode="P_129F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_308F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10058_I" deadCode="false" sourceNode="P_84F10058" targetNode="P_130F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_309F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_309F10058_O" deadCode="false" sourceNode="P_84F10058" targetNode="P_131F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_309F10058"/>
	</edges>
	<edges id="P_84F10058P_85F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_84F10058" targetNode="P_85F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10058_I" deadCode="false" sourceNode="P_174F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_311F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_311F10058_O" deadCode="false" sourceNode="P_174F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_311F10058"/>
	</edges>
	<edges id="P_174F10058P_175F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_174F10058" targetNode="P_175F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_314F10058_I" deadCode="false" sourceNode="P_86F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_314F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_314F10058_O" deadCode="false" sourceNode="P_86F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_314F10058"/>
	</edges>
	<edges id="P_86F10058P_87F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_86F10058" targetNode="P_87F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_317F10058_I" deadCode="false" sourceNode="P_88F10058" targetNode="P_174F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_317F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_317F10058_O" deadCode="false" sourceNode="P_88F10058" targetNode="P_175F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_317F10058"/>
	</edges>
	<edges id="P_88F10058P_89F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_88F10058" targetNode="P_89F10058"/>
	<edges id="P_90F10058P_91F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_90F10058" targetNode="P_91F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_322F10058_I" deadCode="false" sourceNode="P_92F10058" targetNode="P_88F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_322F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_322F10058_O" deadCode="false" sourceNode="P_92F10058" targetNode="P_89F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_322F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_324F10058_I" deadCode="false" sourceNode="P_92F10058" targetNode="P_94F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_324F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_324F10058_O" deadCode="false" sourceNode="P_92F10058" targetNode="P_95F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_324F10058"/>
	</edges>
	<edges id="P_92F10058P_93F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_92F10058" targetNode="P_93F10058"/>
	<edges id="P_94F10058P_95F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_94F10058" targetNode="P_95F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_328F10058_I" deadCode="false" sourceNode="P_176F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_328F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_328F10058_O" deadCode="false" sourceNode="P_176F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_328F10058"/>
	</edges>
	<edges id="P_176F10058P_177F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_176F10058" targetNode="P_177F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_331F10058_I" deadCode="false" sourceNode="P_96F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_331F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_331F10058_O" deadCode="false" sourceNode="P_96F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_331F10058"/>
	</edges>
	<edges id="P_96F10058P_97F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_96F10058" targetNode="P_97F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_334F10058_I" deadCode="false" sourceNode="P_98F10058" targetNode="P_176F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_334F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_334F10058_O" deadCode="false" sourceNode="P_98F10058" targetNode="P_177F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_334F10058"/>
	</edges>
	<edges id="P_98F10058P_99F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_98F10058" targetNode="P_99F10058"/>
	<edges id="P_100F10058P_101F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_100F10058" targetNode="P_101F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_339F10058_I" deadCode="false" sourceNode="P_102F10058" targetNode="P_98F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_339F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_339F10058_O" deadCode="false" sourceNode="P_102F10058" targetNode="P_99F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_339F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_341F10058_I" deadCode="false" sourceNode="P_102F10058" targetNode="P_104F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_341F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_341F10058_O" deadCode="false" sourceNode="P_102F10058" targetNode="P_105F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_341F10058"/>
	</edges>
	<edges id="P_102F10058P_103F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_102F10058" targetNode="P_103F10058"/>
	<edges id="P_104F10058P_105F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_104F10058" targetNode="P_105F10058"/>
	<edges id="P_178F10058P_179F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_178F10058" targetNode="P_179F10058"/>
	<edges id="P_180F10058P_181F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_180F10058" targetNode="P_181F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_351F10058_I" deadCode="false" sourceNode="P_182F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_351F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_351F10058_O" deadCode="false" sourceNode="P_182F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_351F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_353F10058_I" deadCode="false" sourceNode="P_182F10058" targetNode="P_178F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_353F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_353F10058_O" deadCode="false" sourceNode="P_182F10058" targetNode="P_179F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_353F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_355F10058_I" deadCode="false" sourceNode="P_182F10058" targetNode="P_180F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_355F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_355F10058_O" deadCode="false" sourceNode="P_182F10058" targetNode="P_181F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_355F10058"/>
	</edges>
	<edges id="P_182F10058P_183F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_182F10058" targetNode="P_183F10058"/>
	<edges id="P_184F10058P_185F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_184F10058" targetNode="P_185F10058"/>
	<edges id="P_186F10058P_187F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_186F10058" targetNode="P_187F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_361F10058_I" deadCode="false" sourceNode="P_106F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_361F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_361F10058_O" deadCode="false" sourceNode="P_106F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_361F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_363F10058_I" deadCode="false" sourceNode="P_106F10058" targetNode="P_184F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_363F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_363F10058_O" deadCode="false" sourceNode="P_106F10058" targetNode="P_185F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_363F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_365F10058_I" deadCode="false" sourceNode="P_106F10058" targetNode="P_186F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_365F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_365F10058_O" deadCode="false" sourceNode="P_106F10058" targetNode="P_187F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_365F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_366F10058_I" deadCode="false" sourceNode="P_106F10058" targetNode="P_28F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_366F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_366F10058_O" deadCode="false" sourceNode="P_106F10058" targetNode="P_29F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_366F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_368F10058_I" deadCode="false" sourceNode="P_106F10058" targetNode="P_128F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_368F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_368F10058_O" deadCode="false" sourceNode="P_106F10058" targetNode="P_129F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_368F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_369F10058_I" deadCode="false" sourceNode="P_106F10058" targetNode="P_130F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_369F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_369F10058_O" deadCode="false" sourceNode="P_106F10058" targetNode="P_131F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_369F10058"/>
	</edges>
	<edges id="P_106F10058P_107F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_106F10058" targetNode="P_107F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_371F10058_I" deadCode="false" sourceNode="P_108F10058" targetNode="P_182F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_371F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_371F10058_O" deadCode="false" sourceNode="P_108F10058" targetNode="P_183F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_371F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_376F10058_I" deadCode="false" sourceNode="P_108F10058" targetNode="P_28F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_376F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_376F10058_O" deadCode="false" sourceNode="P_108F10058" targetNode="P_29F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_376F10058"/>
	</edges>
	<edges id="P_108F10058P_109F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_108F10058" targetNode="P_109F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_382F10058_I" deadCode="false" sourceNode="P_110F10058" targetNode="P_28F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_382F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_382F10058_O" deadCode="false" sourceNode="P_110F10058" targetNode="P_29F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_382F10058"/>
	</edges>
	<edges id="P_110F10058P_111F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_110F10058" targetNode="P_111F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_384F10058_I" deadCode="false" sourceNode="P_112F10058" targetNode="P_108F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_384F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_384F10058_O" deadCode="false" sourceNode="P_112F10058" targetNode="P_109F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_384F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_386F10058_I" deadCode="false" sourceNode="P_112F10058" targetNode="P_114F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_386F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_386F10058_O" deadCode="false" sourceNode="P_112F10058" targetNode="P_115F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_386F10058"/>
	</edges>
	<edges id="P_112F10058P_113F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_112F10058" targetNode="P_113F10058"/>
	<edges id="P_188F10058P_189F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_188F10058" targetNode="P_189F10058"/>
	<edges id="P_190F10058P_191F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_190F10058" targetNode="P_191F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_393F10058_I" deadCode="false" sourceNode="P_114F10058" targetNode="P_188F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_393F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_393F10058_O" deadCode="false" sourceNode="P_114F10058" targetNode="P_189F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_393F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_395F10058_I" deadCode="false" sourceNode="P_114F10058" targetNode="P_190F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_395F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_395F10058_O" deadCode="false" sourceNode="P_114F10058" targetNode="P_191F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_395F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_396F10058_I" deadCode="false" sourceNode="P_114F10058" targetNode="P_28F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_396F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_396F10058_O" deadCode="false" sourceNode="P_114F10058" targetNode="P_29F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_396F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_398F10058_I" deadCode="false" sourceNode="P_114F10058" targetNode="P_128F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_398F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_398F10058_O" deadCode="false" sourceNode="P_114F10058" targetNode="P_129F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_398F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_399F10058_I" deadCode="false" sourceNode="P_114F10058" targetNode="P_130F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_399F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_399F10058_O" deadCode="false" sourceNode="P_114F10058" targetNode="P_131F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_399F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_401F10058_I" deadCode="false" sourceNode="P_114F10058" targetNode="P_110F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_401F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_401F10058_O" deadCode="false" sourceNode="P_114F10058" targetNode="P_111F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_401F10058"/>
	</edges>
	<edges id="P_114F10058P_115F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_114F10058" targetNode="P_115F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_405F10058_I" deadCode="false" sourceNode="P_192F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_405F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_405F10058_O" deadCode="false" sourceNode="P_192F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_405F10058"/>
	</edges>
	<edges id="P_192F10058P_193F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_192F10058" targetNode="P_193F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_408F10058_I" deadCode="false" sourceNode="P_116F10058" targetNode="P_126F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_408F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_408F10058_O" deadCode="false" sourceNode="P_116F10058" targetNode="P_127F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_408F10058"/>
	</edges>
	<edges id="P_116F10058P_117F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_116F10058" targetNode="P_117F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_411F10058_I" deadCode="false" sourceNode="P_118F10058" targetNode="P_192F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_411F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_411F10058_O" deadCode="false" sourceNode="P_118F10058" targetNode="P_193F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_411F10058"/>
	</edges>
	<edges id="P_118F10058P_119F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_118F10058" targetNode="P_119F10058"/>
	<edges id="P_120F10058P_121F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_120F10058" targetNode="P_121F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_416F10058_I" deadCode="false" sourceNode="P_122F10058" targetNode="P_118F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_416F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_416F10058_O" deadCode="false" sourceNode="P_122F10058" targetNode="P_119F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_416F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_418F10058_I" deadCode="false" sourceNode="P_122F10058" targetNode="P_124F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_418F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_418F10058_O" deadCode="false" sourceNode="P_122F10058" targetNode="P_125F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_418F10058"/>
	</edges>
	<edges id="P_122F10058P_123F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_122F10058" targetNode="P_123F10058"/>
	<edges id="P_124F10058P_125F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_124F10058" targetNode="P_125F10058"/>
	<edges id="P_128F10058P_129F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_128F10058" targetNode="P_129F10058"/>
	<edges id="P_134F10058P_135F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_134F10058" targetNode="P_135F10058"/>
	<edges id="P_140F10058P_141F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_140F10058" targetNode="P_141F10058"/>
	<edges id="P_136F10058P_137F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_136F10058" targetNode="P_137F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_544F10058_I" deadCode="false" sourceNode="P_132F10058" targetNode="P_28F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_544F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_544F10058_O" deadCode="false" sourceNode="P_132F10058" targetNode="P_29F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_544F10058"/>
	</edges>
	<edges id="P_132F10058P_133F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_132F10058" targetNode="P_133F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_548F10058_I" deadCode="false" sourceNode="P_40F10058" targetNode="P_146F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_548F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_548F10058_O" deadCode="false" sourceNode="P_40F10058" targetNode="P_147F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_548F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_550F10058_I" deadCode="false" sourceNode="P_40F10058" targetNode="P_151F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_549F10058"/>
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_550F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_550F10058_O" deadCode="false" sourceNode="P_40F10058" targetNode="P_152F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_549F10058"/>
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_550F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_554F10058_I" deadCode="false" sourceNode="P_40F10058" targetNode="P_144F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_549F10058"/>
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_554F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_554F10058_O" deadCode="false" sourceNode="P_40F10058" targetNode="P_145F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_549F10058"/>
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_554F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_562F10058_I" deadCode="false" sourceNode="P_40F10058" targetNode="P_32F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_549F10058"/>
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_562F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_562F10058_O" deadCode="false" sourceNode="P_40F10058" targetNode="P_33F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_549F10058"/>
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_562F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_565F10058_I" deadCode="false" sourceNode="P_40F10058" targetNode="P_194F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_565F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_565F10058_O" deadCode="false" sourceNode="P_40F10058" targetNode="P_195F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_565F10058"/>
	</edges>
	<edges id="P_40F10058P_41F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10058" targetNode="P_41F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_570F10058_I" deadCode="false" sourceNode="P_42F10058" targetNode="P_194F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_570F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_570F10058_O" deadCode="false" sourceNode="P_42F10058" targetNode="P_195F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_570F10058"/>
	</edges>
	<edges id="P_42F10058P_43F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10058" targetNode="P_43F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_580F10058_I" deadCode="false" sourceNode="P_194F10058" targetNode="P_32F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_580F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_580F10058_O" deadCode="false" sourceNode="P_194F10058" targetNode="P_33F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_580F10058"/>
	</edges>
	<edges id="P_194F10058P_195F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_194F10058" targetNode="P_195F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_583F10058_I" deadCode="false" sourceNode="P_138F10058" targetNode="P_196F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_583F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_583F10058_O" deadCode="false" sourceNode="P_138F10058" targetNode="P_197F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_583F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_586F10058_I" deadCode="false" sourceNode="P_138F10058" targetNode="P_196F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_586F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_586F10058_O" deadCode="false" sourceNode="P_138F10058" targetNode="P_197F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_586F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_590F10058_I" deadCode="false" sourceNode="P_138F10058" targetNode="P_196F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_590F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_590F10058_O" deadCode="false" sourceNode="P_138F10058" targetNode="P_197F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_590F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_594F10058_I" deadCode="false" sourceNode="P_138F10058" targetNode="P_196F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_594F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_594F10058_O" deadCode="false" sourceNode="P_138F10058" targetNode="P_197F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_594F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_598F10058_I" deadCode="false" sourceNode="P_138F10058" targetNode="P_196F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_598F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_598F10058_O" deadCode="false" sourceNode="P_138F10058" targetNode="P_197F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_598F10058"/>
	</edges>
	<edges id="P_138F10058P_139F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_138F10058" targetNode="P_139F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_602F10058_I" deadCode="false" sourceNode="P_130F10058" targetNode="P_198F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_602F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_602F10058_O" deadCode="false" sourceNode="P_130F10058" targetNode="P_199F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_602F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_605F10058_I" deadCode="false" sourceNode="P_130F10058" targetNode="P_198F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_605F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_605F10058_O" deadCode="false" sourceNode="P_130F10058" targetNode="P_199F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_605F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_609F10058_I" deadCode="false" sourceNode="P_130F10058" targetNode="P_198F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_609F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_609F10058_O" deadCode="false" sourceNode="P_130F10058" targetNode="P_199F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_609F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_613F10058_I" deadCode="false" sourceNode="P_130F10058" targetNode="P_198F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_613F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_613F10058_O" deadCode="false" sourceNode="P_130F10058" targetNode="P_199F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_613F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_617F10058_I" deadCode="false" sourceNode="P_130F10058" targetNode="P_198F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_617F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_617F10058_O" deadCode="false" sourceNode="P_130F10058" targetNode="P_199F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_617F10058"/>
	</edges>
	<edges id="P_130F10058P_131F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_130F10058" targetNode="P_131F10058"/>
	<edges id="P_126F10058P_127F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_126F10058" targetNode="P_127F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_622F10058_I" deadCode="false" sourceNode="P_26F10058" targetNode="P_200F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_622F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_622F10058_O" deadCode="false" sourceNode="P_26F10058" targetNode="P_201F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_622F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_624F10058_I" deadCode="false" sourceNode="P_26F10058" targetNode="P_202F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_624F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_624F10058_O" deadCode="false" sourceNode="P_26F10058" targetNode="P_203F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_624F10058"/>
	</edges>
	<edges id="P_26F10058P_27F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10058" targetNode="P_27F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_629F10058_I" deadCode="false" sourceNode="P_200F10058" targetNode="P_196F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_629F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_629F10058_O" deadCode="false" sourceNode="P_200F10058" targetNode="P_197F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_629F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_634F10058_I" deadCode="false" sourceNode="P_200F10058" targetNode="P_196F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_634F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_634F10058_O" deadCode="false" sourceNode="P_200F10058" targetNode="P_197F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_634F10058"/>
	</edges>
	<edges id="P_200F10058P_201F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_200F10058" targetNode="P_201F10058"/>
	<edges id="P_202F10058P_203F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_202F10058" targetNode="P_203F10058"/>
	<edges id="P_196F10058P_197F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_196F10058" targetNode="P_197F10058"/>
	<edges xsi:type="cbl:PerformEdge" id="S_663F10058_I" deadCode="false" sourceNode="P_198F10058" targetNode="P_206F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_663F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_663F10058_O" deadCode="false" sourceNode="P_198F10058" targetNode="P_207F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_663F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_664F10058_I" deadCode="false" sourceNode="P_198F10058" targetNode="P_208F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_664F10058"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_664F10058_O" deadCode="false" sourceNode="P_198F10058" targetNode="P_209F10058">
		<representations href="../../../cobol/IDBSOCO0.cbl.cobModel#S_664F10058"/>
	</edges>
	<edges id="P_198F10058P_199F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_198F10058" targetNode="P_199F10058"/>
	<edges id="P_206F10058P_207F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_206F10058" targetNode="P_207F10058"/>
	<edges id="P_208F10058P_209F10058" xsi:type="cbl:FallThroughEdge" sourceNode="P_208F10058" targetNode="P_209F10058"/>
	<edges xsi:type="cbl:DataEdge" id="S_129F10058_POS1" deadCode="false" targetNode="P_30F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_129F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_141F10058_POS1" deadCode="false" sourceNode="P_32F10058" targetNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_141F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_148F10058_POS1" deadCode="false" sourceNode="P_34F10058" targetNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_148F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_151F10058_POS1" deadCode="false" sourceNode="P_36F10058" targetNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_151F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_159F10058_POS1" deadCode="false" targetNode="P_38F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_159F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_169F10058_POS1" deadCode="false" sourceNode="P_144F10058" targetNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_169F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_173F10058_POS1" deadCode="false" targetNode="P_146F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_173F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_176F10058_POS1" deadCode="false" targetNode="P_148F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_176F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_183F10058_POS1" deadCode="false" targetNode="P_151F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_183F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_239F10058_POS1" deadCode="false" targetNode="P_164F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_239F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_241F10058_POS1" deadCode="false" targetNode="P_166F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_241F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_255F10058_POS1" deadCode="false" targetNode="P_66F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_255F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_257F10058_POS1" deadCode="false" targetNode="P_66F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_257F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_261F10058_POS1" deadCode="false" targetNode="P_68F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_261F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_263F10058_POS1" deadCode="false" targetNode="P_68F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_263F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_270F10058_POS1" deadCode="false" targetNode="P_168F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_270F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_272F10058_POS1" deadCode="false" targetNode="P_170F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_272F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_305F10058_POS1" deadCode="false" targetNode="P_84F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_305F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_357F10058_POS1" deadCode="false" targetNode="P_184F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_357F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_359F10058_POS1" deadCode="false" targetNode="P_186F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_359F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_373F10058_POS1" deadCode="false" targetNode="P_108F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_373F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_375F10058_POS1" deadCode="false" targetNode="P_108F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_375F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_379F10058_POS1" deadCode="false" targetNode="P_110F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_379F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_381F10058_POS1" deadCode="false" targetNode="P_110F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_381F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_388F10058_POS1" deadCode="false" targetNode="P_188F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_388F10058"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_390F10058_POS1" deadCode="false" targetNode="P_190F10058" sourceNode="DB2_OGG_COLLG">
		<representations href="../../../cobol/../importantStmts.cobModel#S_390F10058"></representations>
	</edges>
</Package>

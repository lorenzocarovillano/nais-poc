<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0018" cbl:id="LVVS0018" xsi:id="LVVS0018" packageRef="LVVS0018.igd#LVVS0018" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0018_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10317" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0018.cbl.cobModel#SC_1F10317"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10317" deadCode="false" name="PROGRAM_LVVS0018_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0018.cbl.cobModel#P_1F10317"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10317" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0018.cbl.cobModel#P_2F10317"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10317" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0018.cbl.cobModel#P_3F10317"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10317" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0018.cbl.cobModel#P_4F10317"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10317" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0018.cbl.cobModel#P_5F10317"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10317" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0018.cbl.cobModel#P_8F10317"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10317" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0018.cbl.cobModel#P_9F10317"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10317" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS0018.cbl.cobModel#P_10F10317"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10317" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS0018.cbl.cobModel#P_11F10317"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10317" deadCode="false" name="S1250-CONVERTI-DATA">
				<representations href="../../../cobol/LVVS0018.cbl.cobModel#P_12F10317"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10317" deadCode="false" name="S1250-CONVERTI-DATA-EX">
				<representations href="../../../cobol/LVVS0018.cbl.cobModel#P_13F10317"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10317" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0018.cbl.cobModel#P_6F10317"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10317" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0018.cbl.cobModel#P_7F10317"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LVVS0000" name="LVVS0000">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10304"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10317P_1F10317" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10317" targetNode="P_1F10317"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10317_I" deadCode="false" sourceNode="P_1F10317" targetNode="P_2F10317">
		<representations href="../../../cobol/LVVS0018.cbl.cobModel#S_1F10317"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10317_O" deadCode="false" sourceNode="P_1F10317" targetNode="P_3F10317">
		<representations href="../../../cobol/LVVS0018.cbl.cobModel#S_1F10317"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10317_I" deadCode="false" sourceNode="P_1F10317" targetNode="P_4F10317">
		<representations href="../../../cobol/LVVS0018.cbl.cobModel#S_2F10317"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10317_O" deadCode="false" sourceNode="P_1F10317" targetNode="P_5F10317">
		<representations href="../../../cobol/LVVS0018.cbl.cobModel#S_2F10317"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10317_I" deadCode="false" sourceNode="P_1F10317" targetNode="P_6F10317">
		<representations href="../../../cobol/LVVS0018.cbl.cobModel#S_3F10317"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10317_O" deadCode="false" sourceNode="P_1F10317" targetNode="P_7F10317">
		<representations href="../../../cobol/LVVS0018.cbl.cobModel#S_3F10317"/>
	</edges>
	<edges id="P_2F10317P_3F10317" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10317" targetNode="P_3F10317"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10317_I" deadCode="false" sourceNode="P_4F10317" targetNode="P_8F10317">
		<representations href="../../../cobol/LVVS0018.cbl.cobModel#S_10F10317"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10317_O" deadCode="false" sourceNode="P_4F10317" targetNode="P_9F10317">
		<representations href="../../../cobol/LVVS0018.cbl.cobModel#S_10F10317"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10317_I" deadCode="false" sourceNode="P_4F10317" targetNode="P_10F10317">
		<representations href="../../../cobol/LVVS0018.cbl.cobModel#S_12F10317"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10317_O" deadCode="false" sourceNode="P_4F10317" targetNode="P_11F10317">
		<representations href="../../../cobol/LVVS0018.cbl.cobModel#S_12F10317"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10317_I" deadCode="false" sourceNode="P_4F10317" targetNode="P_12F10317">
		<representations href="../../../cobol/LVVS0018.cbl.cobModel#S_15F10317"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_15F10317_O" deadCode="false" sourceNode="P_4F10317" targetNode="P_13F10317">
		<representations href="../../../cobol/LVVS0018.cbl.cobModel#S_15F10317"/>
	</edges>
	<edges id="P_4F10317P_5F10317" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10317" targetNode="P_5F10317"/>
	<edges id="P_8F10317P_9F10317" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10317" targetNode="P_9F10317"/>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10317_I" deadCode="false" sourceNode="P_10F10317" targetNode="P_12F10317">
		<representations href="../../../cobol/LVVS0018.cbl.cobModel#S_28F10317"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_28F10317_O" deadCode="false" sourceNode="P_10F10317" targetNode="P_13F10317">
		<representations href="../../../cobol/LVVS0018.cbl.cobModel#S_28F10317"/>
	</edges>
	<edges id="P_10F10317P_11F10317" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10317" targetNode="P_11F10317"/>
	<edges id="P_12F10317P_13F10317" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10317" targetNode="P_13F10317"/>
	<edges xsi:type="cbl:CallEdge" id="S_32F10317" deadCode="false" name="Dynamic WK-LVVS0000" sourceNode="P_12F10317" targetNode="LVVS0000">
		<representations href="../../../cobol/../importantStmts.cobModel#S_32F10317"></representations>
	</edges>
</Package>

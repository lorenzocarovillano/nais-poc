<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBSF320" cbl:id="LDBSF320" xsi:id="LDBSF320" packageRef="LDBSF320.igd#LDBSF320" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBSF320_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10269" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBSF320.cbl.cobModel#SC_1F10269"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10269" deadCode="false" name="PROGRAM_LDBSF320_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_1F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10269" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_2F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10269" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_3F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10269" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_12F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10269" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_13F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10269" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_4F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10269" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_5F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10269" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_6F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10269" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_7F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10269" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_8F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10269" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_9F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10269" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_44F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10269" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_49F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10269" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_14F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10269" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_15F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10269" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_16F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10269" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_17F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10269" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_18F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10269" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_19F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10269" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_20F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10269" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_21F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10269" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_22F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10269" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_23F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10269" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_50F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10269" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_51F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10269" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_24F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10269" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_25F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10269" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_26F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10269" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_27F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10269" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_28F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10269" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_29F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10269" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_30F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10269" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_31F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10269" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_32F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10269" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_33F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10269" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_52F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10269" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_53F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10269" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_34F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10269" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_35F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10269" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_36F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10269" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_37F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10269" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_38F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10269" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_39F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10269" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_40F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10269" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_41F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10269" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_42F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10269" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_43F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10269" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_54F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10269" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_55F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10269" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_60F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10269" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_63F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10269" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_56F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10269" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_57F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10269" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_45F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10269" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_46F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10269" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_47F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10269" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_48F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10269" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_58F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10269" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_59F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10269" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_10F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10269" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_11F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10269" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_66F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10269" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_67F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10269" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_68F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10269" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_69F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10269" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_61F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10269" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_62F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10269" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_70F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10269" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_71F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10269" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_64F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10269" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_65F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10269" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_72F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10269" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_73F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10269" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_74F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10269" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_75F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10269" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_76F10269"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10269" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBSF320.cbl.cobModel#P_77F10269"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_ESTR_CNT_DIAGN_RIV" name="ESTR_CNT_DIAGN_RIV">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_ESTR_CNT_DIAGN_RIV"/>
		</children>
	</packageNode>
	<edges id="SC_1F10269P_1F10269" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10269" targetNode="P_1F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10269_I" deadCode="false" sourceNode="P_1F10269" targetNode="P_2F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_2F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10269_O" deadCode="false" sourceNode="P_1F10269" targetNode="P_3F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_2F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10269_I" deadCode="false" sourceNode="P_1F10269" targetNode="P_4F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_6F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10269_O" deadCode="false" sourceNode="P_1F10269" targetNode="P_5F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_6F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10269_I" deadCode="false" sourceNode="P_1F10269" targetNode="P_6F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_10F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10269_O" deadCode="false" sourceNode="P_1F10269" targetNode="P_7F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_10F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10269_I" deadCode="false" sourceNode="P_1F10269" targetNode="P_8F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_14F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10269_O" deadCode="false" sourceNode="P_1F10269" targetNode="P_9F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_14F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10269_I" deadCode="false" sourceNode="P_2F10269" targetNode="P_10F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_24F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10269_O" deadCode="false" sourceNode="P_2F10269" targetNode="P_11F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_24F10269"/>
	</edges>
	<edges id="P_2F10269P_3F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10269" targetNode="P_3F10269"/>
	<edges id="P_12F10269P_13F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10269" targetNode="P_13F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10269_I" deadCode="false" sourceNode="P_4F10269" targetNode="P_14F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_37F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10269_O" deadCode="false" sourceNode="P_4F10269" targetNode="P_15F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_37F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10269_I" deadCode="false" sourceNode="P_4F10269" targetNode="P_16F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_38F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10269_O" deadCode="false" sourceNode="P_4F10269" targetNode="P_17F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_38F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10269_I" deadCode="false" sourceNode="P_4F10269" targetNode="P_18F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_39F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10269_O" deadCode="false" sourceNode="P_4F10269" targetNode="P_19F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_39F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10269_I" deadCode="false" sourceNode="P_4F10269" targetNode="P_20F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_40F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10269_O" deadCode="false" sourceNode="P_4F10269" targetNode="P_21F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_40F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10269_I" deadCode="false" sourceNode="P_4F10269" targetNode="P_22F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_41F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10269_O" deadCode="false" sourceNode="P_4F10269" targetNode="P_23F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_41F10269"/>
	</edges>
	<edges id="P_4F10269P_5F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10269" targetNode="P_5F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10269_I" deadCode="false" sourceNode="P_6F10269" targetNode="P_24F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_45F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10269_O" deadCode="false" sourceNode="P_6F10269" targetNode="P_25F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_45F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10269_I" deadCode="false" sourceNode="P_6F10269" targetNode="P_26F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_46F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10269_O" deadCode="false" sourceNode="P_6F10269" targetNode="P_27F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_46F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10269_I" deadCode="false" sourceNode="P_6F10269" targetNode="P_28F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_47F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10269_O" deadCode="false" sourceNode="P_6F10269" targetNode="P_29F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_47F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10269_I" deadCode="false" sourceNode="P_6F10269" targetNode="P_30F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_48F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10269_O" deadCode="false" sourceNode="P_6F10269" targetNode="P_31F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_48F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10269_I" deadCode="false" sourceNode="P_6F10269" targetNode="P_32F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_49F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10269_O" deadCode="false" sourceNode="P_6F10269" targetNode="P_33F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_49F10269"/>
	</edges>
	<edges id="P_6F10269P_7F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10269" targetNode="P_7F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10269_I" deadCode="false" sourceNode="P_8F10269" targetNode="P_34F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_53F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10269_O" deadCode="false" sourceNode="P_8F10269" targetNode="P_35F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_53F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10269_I" deadCode="false" sourceNode="P_8F10269" targetNode="P_36F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_54F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10269_O" deadCode="false" sourceNode="P_8F10269" targetNode="P_37F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_54F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10269_I" deadCode="false" sourceNode="P_8F10269" targetNode="P_38F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_55F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10269_O" deadCode="false" sourceNode="P_8F10269" targetNode="P_39F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_55F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10269_I" deadCode="false" sourceNode="P_8F10269" targetNode="P_40F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_56F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10269_O" deadCode="false" sourceNode="P_8F10269" targetNode="P_41F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_56F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10269_I" deadCode="false" sourceNode="P_8F10269" targetNode="P_42F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_57F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10269_O" deadCode="false" sourceNode="P_8F10269" targetNode="P_43F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_57F10269"/>
	</edges>
	<edges id="P_8F10269P_9F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10269" targetNode="P_9F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10269_I" deadCode="false" sourceNode="P_44F10269" targetNode="P_45F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_60F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10269_O" deadCode="false" sourceNode="P_44F10269" targetNode="P_46F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_60F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10269_I" deadCode="false" sourceNode="P_44F10269" targetNode="P_47F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_61F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10269_O" deadCode="false" sourceNode="P_44F10269" targetNode="P_48F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_61F10269"/>
	</edges>
	<edges id="P_44F10269P_49F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10269" targetNode="P_49F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10269_I" deadCode="false" sourceNode="P_14F10269" targetNode="P_45F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_64F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10269_O" deadCode="false" sourceNode="P_14F10269" targetNode="P_46F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_64F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10269_I" deadCode="false" sourceNode="P_14F10269" targetNode="P_47F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_65F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10269_O" deadCode="false" sourceNode="P_14F10269" targetNode="P_48F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_65F10269"/>
	</edges>
	<edges id="P_14F10269P_15F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10269" targetNode="P_15F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10269_I" deadCode="false" sourceNode="P_16F10269" targetNode="P_44F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_68F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10269_O" deadCode="false" sourceNode="P_16F10269" targetNode="P_49F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_68F10269"/>
	</edges>
	<edges id="P_16F10269P_17F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10269" targetNode="P_17F10269"/>
	<edges id="P_18F10269P_19F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10269" targetNode="P_19F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10269_I" deadCode="false" sourceNode="P_20F10269" targetNode="P_16F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_73F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10269_O" deadCode="false" sourceNode="P_20F10269" targetNode="P_17F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_73F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10269_I" deadCode="false" sourceNode="P_20F10269" targetNode="P_22F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_75F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10269_O" deadCode="false" sourceNode="P_20F10269" targetNode="P_23F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_75F10269"/>
	</edges>
	<edges id="P_20F10269P_21F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10269" targetNode="P_21F10269"/>
	<edges id="P_22F10269P_23F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10269" targetNode="P_23F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10269_I" deadCode="false" sourceNode="P_50F10269" targetNode="P_45F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_79F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10269_O" deadCode="false" sourceNode="P_50F10269" targetNode="P_46F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_79F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10269_I" deadCode="false" sourceNode="P_50F10269" targetNode="P_47F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_80F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10269_O" deadCode="false" sourceNode="P_50F10269" targetNode="P_48F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_80F10269"/>
	</edges>
	<edges id="P_50F10269P_51F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10269" targetNode="P_51F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10269_I" deadCode="false" sourceNode="P_24F10269" targetNode="P_45F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_83F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10269_O" deadCode="false" sourceNode="P_24F10269" targetNode="P_46F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_83F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10269_I" deadCode="false" sourceNode="P_24F10269" targetNode="P_47F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_84F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10269_O" deadCode="false" sourceNode="P_24F10269" targetNode="P_48F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_84F10269"/>
	</edges>
	<edges id="P_24F10269P_25F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10269" targetNode="P_25F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10269_I" deadCode="false" sourceNode="P_26F10269" targetNode="P_50F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_87F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10269_O" deadCode="false" sourceNode="P_26F10269" targetNode="P_51F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_87F10269"/>
	</edges>
	<edges id="P_26F10269P_27F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10269" targetNode="P_27F10269"/>
	<edges id="P_28F10269P_29F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10269" targetNode="P_29F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10269_I" deadCode="false" sourceNode="P_30F10269" targetNode="P_26F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_92F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10269_O" deadCode="false" sourceNode="P_30F10269" targetNode="P_27F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_92F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10269_I" deadCode="false" sourceNode="P_30F10269" targetNode="P_32F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_94F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10269_O" deadCode="false" sourceNode="P_30F10269" targetNode="P_33F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_94F10269"/>
	</edges>
	<edges id="P_30F10269P_31F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10269" targetNode="P_31F10269"/>
	<edges id="P_32F10269P_33F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10269" targetNode="P_33F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10269_I" deadCode="false" sourceNode="P_52F10269" targetNode="P_45F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_98F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10269_O" deadCode="false" sourceNode="P_52F10269" targetNode="P_46F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_98F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10269_I" deadCode="false" sourceNode="P_52F10269" targetNode="P_47F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_99F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10269_O" deadCode="false" sourceNode="P_52F10269" targetNode="P_48F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_99F10269"/>
	</edges>
	<edges id="P_52F10269P_53F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10269" targetNode="P_53F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10269_I" deadCode="false" sourceNode="P_34F10269" targetNode="P_45F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_103F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10269_O" deadCode="false" sourceNode="P_34F10269" targetNode="P_46F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_103F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10269_I" deadCode="false" sourceNode="P_34F10269" targetNode="P_47F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_104F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10269_O" deadCode="false" sourceNode="P_34F10269" targetNode="P_48F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_104F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10269_I" deadCode="false" sourceNode="P_34F10269" targetNode="P_12F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_106F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10269_O" deadCode="false" sourceNode="P_34F10269" targetNode="P_13F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_106F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10269_I" deadCode="false" sourceNode="P_34F10269" targetNode="P_54F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_108F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10269_O" deadCode="false" sourceNode="P_34F10269" targetNode="P_55F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_108F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10269_I" deadCode="false" sourceNode="P_34F10269" targetNode="P_56F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_109F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10269_O" deadCode="false" sourceNode="P_34F10269" targetNode="P_57F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_109F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10269_I" deadCode="false" sourceNode="P_34F10269" targetNode="P_58F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_110F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10269_O" deadCode="false" sourceNode="P_34F10269" targetNode="P_59F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_110F10269"/>
	</edges>
	<edges id="P_34F10269P_35F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10269" targetNode="P_35F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10269_I" deadCode="false" sourceNode="P_36F10269" targetNode="P_52F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_112F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10269_O" deadCode="false" sourceNode="P_36F10269" targetNode="P_53F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_112F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10269_I" deadCode="false" sourceNode="P_36F10269" targetNode="P_12F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_114F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10269_O" deadCode="false" sourceNode="P_36F10269" targetNode="P_13F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_114F10269"/>
	</edges>
	<edges id="P_36F10269P_37F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10269" targetNode="P_37F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10269_I" deadCode="false" sourceNode="P_38F10269" targetNode="P_12F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_117F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10269_O" deadCode="false" sourceNode="P_38F10269" targetNode="P_13F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_117F10269"/>
	</edges>
	<edges id="P_38F10269P_39F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10269" targetNode="P_39F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10269_I" deadCode="false" sourceNode="P_40F10269" targetNode="P_36F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_119F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10269_O" deadCode="false" sourceNode="P_40F10269" targetNode="P_37F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_119F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10269_I" deadCode="false" sourceNode="P_40F10269" targetNode="P_42F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_121F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10269_O" deadCode="false" sourceNode="P_40F10269" targetNode="P_43F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_121F10269"/>
	</edges>
	<edges id="P_40F10269P_41F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10269" targetNode="P_41F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10269_I" deadCode="false" sourceNode="P_42F10269" targetNode="P_12F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_124F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10269_O" deadCode="false" sourceNode="P_42F10269" targetNode="P_13F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_124F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10269_I" deadCode="false" sourceNode="P_42F10269" targetNode="P_54F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_126F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10269_O" deadCode="false" sourceNode="P_42F10269" targetNode="P_55F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_126F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10269_I" deadCode="false" sourceNode="P_42F10269" targetNode="P_56F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_127F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10269_O" deadCode="false" sourceNode="P_42F10269" targetNode="P_57F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_127F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10269_I" deadCode="false" sourceNode="P_42F10269" targetNode="P_58F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_128F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10269_O" deadCode="false" sourceNode="P_42F10269" targetNode="P_59F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_128F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10269_I" deadCode="false" sourceNode="P_42F10269" targetNode="P_38F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_130F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10269_O" deadCode="false" sourceNode="P_42F10269" targetNode="P_39F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_130F10269"/>
	</edges>
	<edges id="P_42F10269P_43F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10269" targetNode="P_43F10269"/>
	<edges id="P_54F10269P_55F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10269" targetNode="P_55F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_151F10269_I" deadCode="true" sourceNode="P_60F10269" targetNode="P_61F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_151F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_151F10269_O" deadCode="true" sourceNode="P_60F10269" targetNode="P_62F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_151F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_155F10269_I" deadCode="false" sourceNode="P_56F10269" targetNode="P_64F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_155F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_155F10269_O" deadCode="false" sourceNode="P_56F10269" targetNode="P_65F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_155F10269"/>
	</edges>
	<edges id="P_56F10269P_57F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10269" targetNode="P_57F10269"/>
	<edges id="P_45F10269P_46F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10269" targetNode="P_46F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10269_I" deadCode="false" sourceNode="P_47F10269" targetNode="P_61F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_161F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10269_O" deadCode="false" sourceNode="P_47F10269" targetNode="P_62F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_161F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10269_I" deadCode="false" sourceNode="P_47F10269" targetNode="P_61F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_164F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_164F10269_O" deadCode="false" sourceNode="P_47F10269" targetNode="P_62F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_164F10269"/>
	</edges>
	<edges id="P_47F10269P_48F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10269" targetNode="P_48F10269"/>
	<edges id="P_58F10269P_59F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10269" targetNode="P_59F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_169F10269_I" deadCode="false" sourceNode="P_10F10269" targetNode="P_66F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_169F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_169F10269_O" deadCode="false" sourceNode="P_10F10269" targetNode="P_67F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_169F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10269_I" deadCode="false" sourceNode="P_10F10269" targetNode="P_68F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_171F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_171F10269_O" deadCode="false" sourceNode="P_10F10269" targetNode="P_69F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_171F10269"/>
	</edges>
	<edges id="P_10F10269P_11F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10269" targetNode="P_11F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_176F10269_I" deadCode="false" sourceNode="P_66F10269" targetNode="P_61F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_176F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_176F10269_O" deadCode="false" sourceNode="P_66F10269" targetNode="P_62F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_176F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10269_I" deadCode="false" sourceNode="P_66F10269" targetNode="P_61F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_181F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_181F10269_O" deadCode="false" sourceNode="P_66F10269" targetNode="P_62F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_181F10269"/>
	</edges>
	<edges id="P_66F10269P_67F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10269" targetNode="P_67F10269"/>
	<edges id="P_68F10269P_69F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10269" targetNode="P_69F10269"/>
	<edges id="P_61F10269P_62F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10269" targetNode="P_62F10269"/>
	<edges xsi:type="cbl:PerformEdge" id="S_210F10269_I" deadCode="false" sourceNode="P_64F10269" targetNode="P_72F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_210F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_210F10269_O" deadCode="false" sourceNode="P_64F10269" targetNode="P_73F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_210F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10269_I" deadCode="false" sourceNode="P_64F10269" targetNode="P_74F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_211F10269"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_211F10269_O" deadCode="false" sourceNode="P_64F10269" targetNode="P_75F10269">
		<representations href="../../../cobol/LDBSF320.cbl.cobModel#S_211F10269"/>
	</edges>
	<edges id="P_64F10269P_65F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10269" targetNode="P_65F10269"/>
	<edges id="P_72F10269P_73F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10269" targetNode="P_73F10269"/>
	<edges id="P_74F10269P_75F10269" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10269" targetNode="P_75F10269"/>
	<edges xsi:type="cbl:DataEdge" id="S_105F10269_POS1" deadCode="false" targetNode="P_34F10269" sourceNode="DB2_ESTR_CNT_DIAGN_RIV">
		<representations href="../../../cobol/../importantStmts.cobModel#S_105F10269"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_113F10269_POS1" deadCode="false" targetNode="P_36F10269" sourceNode="DB2_ESTR_CNT_DIAGN_RIV">
		<representations href="../../../cobol/../importantStmts.cobModel#S_113F10269"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_116F10269_POS1" deadCode="false" targetNode="P_38F10269" sourceNode="DB2_ESTR_CNT_DIAGN_RIV">
		<representations href="../../../cobol/../importantStmts.cobModel#S_116F10269"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_123F10269_POS1" deadCode="false" targetNode="P_42F10269" sourceNode="DB2_ESTR_CNT_DIAGN_RIV">
		<representations href="../../../cobol/../importantStmts.cobModel#S_123F10269"></representations>
	</edges>
</Package>

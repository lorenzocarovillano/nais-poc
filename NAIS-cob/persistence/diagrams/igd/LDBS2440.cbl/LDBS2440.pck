<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS2440" cbl:id="LDBS2440" xsi:id="LDBS2440" packageRef="LDBS2440.igd#LDBS2440" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS2440_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10175" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS2440.cbl.cobModel#SC_1F10175"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10175" deadCode="false" name="PROGRAM_LDBS2440_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_1F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10175" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_2F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10175" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_3F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10175" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_12F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10175" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_13F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10175" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_4F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10175" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_5F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10175" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_6F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10175" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_7F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10175" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_8F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10175" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_9F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10175" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_44F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10175" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_49F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10175" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_14F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10175" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_15F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10175" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_16F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10175" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_17F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10175" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_18F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10175" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_19F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10175" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_20F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10175" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_21F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10175" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_22F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10175" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_23F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10175" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_50F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10175" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_51F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10175" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_24F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10175" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_25F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10175" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_26F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10175" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_27F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10175" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_28F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10175" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_29F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10175" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_30F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10175" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_31F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10175" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_32F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10175" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_33F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10175" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_52F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10175" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_53F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10175" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_34F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10175" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_35F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10175" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_36F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10175" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_37F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10175" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_38F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10175" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_39F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10175" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_40F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10175" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_41F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10175" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_42F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10175" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_43F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10175" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_54F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10175" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_55F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10175" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_60F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10175" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_61F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10175" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_56F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10175" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_57F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10175" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_45F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10175" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_46F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10175" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_47F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10175" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_48F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10175" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_58F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10175" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_59F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10175" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_10F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10175" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_11F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10175" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_62F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10175" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_63F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10175" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_64F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10175" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_65F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10175" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_66F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10175" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_67F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10175" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_68F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10175" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_69F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10175" deadCode="true" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_70F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10175" deadCode="true" name="Z800-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_75F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10175" deadCode="true" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_71F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10175" deadCode="true" name="Z810-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_72F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10175" deadCode="true" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_73F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10175" deadCode="true" name="Z820-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_74F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10175" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_76F10175"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10175" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS2440.cbl.cobModel#P_77F10175"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_AMMB_FUNZ_FUNZ" name="AMMB_FUNZ_FUNZ">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_AMMB_FUNZ_FUNZ"/>
		</children>
	</packageNode>
	<edges id="SC_1F10175P_1F10175" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10175" targetNode="P_1F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10175_I" deadCode="false" sourceNode="P_1F10175" targetNode="P_2F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_2F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10175_O" deadCode="false" sourceNode="P_1F10175" targetNode="P_3F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_2F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10175_I" deadCode="false" sourceNode="P_1F10175" targetNode="P_4F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_6F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10175_O" deadCode="false" sourceNode="P_1F10175" targetNode="P_5F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_6F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10175_I" deadCode="false" sourceNode="P_1F10175" targetNode="P_6F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_10F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10175_O" deadCode="false" sourceNode="P_1F10175" targetNode="P_7F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_10F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10175_I" deadCode="false" sourceNode="P_1F10175" targetNode="P_8F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_14F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10175_O" deadCode="false" sourceNode="P_1F10175" targetNode="P_9F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_14F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10175_I" deadCode="false" sourceNode="P_2F10175" targetNode="P_10F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_24F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10175_O" deadCode="false" sourceNode="P_2F10175" targetNode="P_11F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_24F10175"/>
	</edges>
	<edges id="P_2F10175P_3F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10175" targetNode="P_3F10175"/>
	<edges id="P_12F10175P_13F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10175" targetNode="P_13F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10175_I" deadCode="false" sourceNode="P_4F10175" targetNode="P_14F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_37F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10175_O" deadCode="false" sourceNode="P_4F10175" targetNode="P_15F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_37F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10175_I" deadCode="false" sourceNode="P_4F10175" targetNode="P_16F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_38F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10175_O" deadCode="false" sourceNode="P_4F10175" targetNode="P_17F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_38F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10175_I" deadCode="false" sourceNode="P_4F10175" targetNode="P_18F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_39F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10175_O" deadCode="false" sourceNode="P_4F10175" targetNode="P_19F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_39F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10175_I" deadCode="false" sourceNode="P_4F10175" targetNode="P_20F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_40F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10175_O" deadCode="false" sourceNode="P_4F10175" targetNode="P_21F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_40F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10175_I" deadCode="false" sourceNode="P_4F10175" targetNode="P_22F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_41F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10175_O" deadCode="false" sourceNode="P_4F10175" targetNode="P_23F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_41F10175"/>
	</edges>
	<edges id="P_4F10175P_5F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10175" targetNode="P_5F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10175_I" deadCode="false" sourceNode="P_6F10175" targetNode="P_24F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_45F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10175_O" deadCode="false" sourceNode="P_6F10175" targetNode="P_25F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_45F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10175_I" deadCode="false" sourceNode="P_6F10175" targetNode="P_26F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_46F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10175_O" deadCode="false" sourceNode="P_6F10175" targetNode="P_27F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_46F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10175_I" deadCode="false" sourceNode="P_6F10175" targetNode="P_28F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_47F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10175_O" deadCode="false" sourceNode="P_6F10175" targetNode="P_29F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_47F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10175_I" deadCode="false" sourceNode="P_6F10175" targetNode="P_30F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_48F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10175_O" deadCode="false" sourceNode="P_6F10175" targetNode="P_31F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_48F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10175_I" deadCode="false" sourceNode="P_6F10175" targetNode="P_32F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_49F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10175_O" deadCode="false" sourceNode="P_6F10175" targetNode="P_33F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_49F10175"/>
	</edges>
	<edges id="P_6F10175P_7F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10175" targetNode="P_7F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10175_I" deadCode="false" sourceNode="P_8F10175" targetNode="P_34F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_53F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10175_O" deadCode="false" sourceNode="P_8F10175" targetNode="P_35F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_53F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10175_I" deadCode="false" sourceNode="P_8F10175" targetNode="P_36F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_54F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10175_O" deadCode="false" sourceNode="P_8F10175" targetNode="P_37F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_54F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10175_I" deadCode="false" sourceNode="P_8F10175" targetNode="P_38F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_55F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10175_O" deadCode="false" sourceNode="P_8F10175" targetNode="P_39F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_55F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10175_I" deadCode="false" sourceNode="P_8F10175" targetNode="P_40F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_56F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10175_O" deadCode="false" sourceNode="P_8F10175" targetNode="P_41F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_56F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10175_I" deadCode="false" sourceNode="P_8F10175" targetNode="P_42F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_57F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10175_O" deadCode="false" sourceNode="P_8F10175" targetNode="P_43F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_57F10175"/>
	</edges>
	<edges id="P_8F10175P_9F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10175" targetNode="P_9F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10175_I" deadCode="false" sourceNode="P_44F10175" targetNode="P_45F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_60F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10175_O" deadCode="false" sourceNode="P_44F10175" targetNode="P_46F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_60F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10175_I" deadCode="false" sourceNode="P_44F10175" targetNode="P_47F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_61F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10175_O" deadCode="false" sourceNode="P_44F10175" targetNode="P_48F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_61F10175"/>
	</edges>
	<edges id="P_44F10175P_49F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10175" targetNode="P_49F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10175_I" deadCode="false" sourceNode="P_14F10175" targetNode="P_45F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_64F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10175_O" deadCode="false" sourceNode="P_14F10175" targetNode="P_46F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_64F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10175_I" deadCode="false" sourceNode="P_14F10175" targetNode="P_47F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_65F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10175_O" deadCode="false" sourceNode="P_14F10175" targetNode="P_48F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_65F10175"/>
	</edges>
	<edges id="P_14F10175P_15F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10175" targetNode="P_15F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10175_I" deadCode="false" sourceNode="P_16F10175" targetNode="P_44F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_68F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10175_O" deadCode="false" sourceNode="P_16F10175" targetNode="P_49F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_68F10175"/>
	</edges>
	<edges id="P_16F10175P_17F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10175" targetNode="P_17F10175"/>
	<edges id="P_18F10175P_19F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10175" targetNode="P_19F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10175_I" deadCode="false" sourceNode="P_20F10175" targetNode="P_16F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_73F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_73F10175_O" deadCode="false" sourceNode="P_20F10175" targetNode="P_17F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_73F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10175_I" deadCode="false" sourceNode="P_20F10175" targetNode="P_22F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_75F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10175_O" deadCode="false" sourceNode="P_20F10175" targetNode="P_23F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_75F10175"/>
	</edges>
	<edges id="P_20F10175P_21F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10175" targetNode="P_21F10175"/>
	<edges id="P_22F10175P_23F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10175" targetNode="P_23F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10175_I" deadCode="false" sourceNode="P_50F10175" targetNode="P_45F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_79F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10175_O" deadCode="false" sourceNode="P_50F10175" targetNode="P_46F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_79F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10175_I" deadCode="false" sourceNode="P_50F10175" targetNode="P_47F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_80F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10175_O" deadCode="false" sourceNode="P_50F10175" targetNode="P_48F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_80F10175"/>
	</edges>
	<edges id="P_50F10175P_51F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10175" targetNode="P_51F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10175_I" deadCode="false" sourceNode="P_24F10175" targetNode="P_45F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_83F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10175_O" deadCode="false" sourceNode="P_24F10175" targetNode="P_46F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_83F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10175_I" deadCode="false" sourceNode="P_24F10175" targetNode="P_47F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_84F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_84F10175_O" deadCode="false" sourceNode="P_24F10175" targetNode="P_48F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_84F10175"/>
	</edges>
	<edges id="P_24F10175P_25F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10175" targetNode="P_25F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10175_I" deadCode="false" sourceNode="P_26F10175" targetNode="P_50F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_87F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_87F10175_O" deadCode="false" sourceNode="P_26F10175" targetNode="P_51F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_87F10175"/>
	</edges>
	<edges id="P_26F10175P_27F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10175" targetNode="P_27F10175"/>
	<edges id="P_28F10175P_29F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10175" targetNode="P_29F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10175_I" deadCode="false" sourceNode="P_30F10175" targetNode="P_26F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_92F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10175_O" deadCode="false" sourceNode="P_30F10175" targetNode="P_27F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_92F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10175_I" deadCode="false" sourceNode="P_30F10175" targetNode="P_32F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_94F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_94F10175_O" deadCode="false" sourceNode="P_30F10175" targetNode="P_33F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_94F10175"/>
	</edges>
	<edges id="P_30F10175P_31F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10175" targetNode="P_31F10175"/>
	<edges id="P_32F10175P_33F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10175" targetNode="P_33F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10175_I" deadCode="false" sourceNode="P_52F10175" targetNode="P_45F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_98F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_98F10175_O" deadCode="false" sourceNode="P_52F10175" targetNode="P_46F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_98F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10175_I" deadCode="false" sourceNode="P_52F10175" targetNode="P_47F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_99F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_99F10175_O" deadCode="false" sourceNode="P_52F10175" targetNode="P_48F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_99F10175"/>
	</edges>
	<edges id="P_52F10175P_53F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10175" targetNode="P_53F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10175_I" deadCode="false" sourceNode="P_34F10175" targetNode="P_45F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_103F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_103F10175_O" deadCode="false" sourceNode="P_34F10175" targetNode="P_46F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_103F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10175_I" deadCode="false" sourceNode="P_34F10175" targetNode="P_47F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_104F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10175_O" deadCode="false" sourceNode="P_34F10175" targetNode="P_48F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_104F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10175_I" deadCode="false" sourceNode="P_34F10175" targetNode="P_12F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_106F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10175_O" deadCode="false" sourceNode="P_34F10175" targetNode="P_13F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_106F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10175_I" deadCode="false" sourceNode="P_34F10175" targetNode="P_54F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_108F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10175_O" deadCode="false" sourceNode="P_34F10175" targetNode="P_55F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_108F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10175_I" deadCode="false" sourceNode="P_34F10175" targetNode="P_56F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_109F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_109F10175_O" deadCode="false" sourceNode="P_34F10175" targetNode="P_57F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_109F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10175_I" deadCode="false" sourceNode="P_34F10175" targetNode="P_58F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_110F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10175_O" deadCode="false" sourceNode="P_34F10175" targetNode="P_59F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_110F10175"/>
	</edges>
	<edges id="P_34F10175P_35F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10175" targetNode="P_35F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10175_I" deadCode="false" sourceNode="P_36F10175" targetNode="P_52F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_112F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10175_O" deadCode="false" sourceNode="P_36F10175" targetNode="P_53F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_112F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10175_I" deadCode="false" sourceNode="P_36F10175" targetNode="P_12F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_114F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_114F10175_O" deadCode="false" sourceNode="P_36F10175" targetNode="P_13F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_114F10175"/>
	</edges>
	<edges id="P_36F10175P_37F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10175" targetNode="P_37F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10175_I" deadCode="false" sourceNode="P_38F10175" targetNode="P_12F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_117F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10175_O" deadCode="false" sourceNode="P_38F10175" targetNode="P_13F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_117F10175"/>
	</edges>
	<edges id="P_38F10175P_39F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10175" targetNode="P_39F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10175_I" deadCode="false" sourceNode="P_40F10175" targetNode="P_36F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_119F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10175_O" deadCode="false" sourceNode="P_40F10175" targetNode="P_37F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_119F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10175_I" deadCode="false" sourceNode="P_40F10175" targetNode="P_42F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_121F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_121F10175_O" deadCode="false" sourceNode="P_40F10175" targetNode="P_43F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_121F10175"/>
	</edges>
	<edges id="P_40F10175P_41F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10175" targetNode="P_41F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10175_I" deadCode="false" sourceNode="P_42F10175" targetNode="P_12F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_124F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10175_O" deadCode="false" sourceNode="P_42F10175" targetNode="P_13F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_124F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10175_I" deadCode="false" sourceNode="P_42F10175" targetNode="P_54F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_126F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10175_O" deadCode="false" sourceNode="P_42F10175" targetNode="P_55F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_126F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10175_I" deadCode="false" sourceNode="P_42F10175" targetNode="P_56F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_127F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_127F10175_O" deadCode="false" sourceNode="P_42F10175" targetNode="P_57F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_127F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10175_I" deadCode="false" sourceNode="P_42F10175" targetNode="P_58F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_128F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10175_O" deadCode="false" sourceNode="P_42F10175" targetNode="P_59F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_128F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10175_I" deadCode="false" sourceNode="P_42F10175" targetNode="P_38F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_130F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_130F10175_O" deadCode="false" sourceNode="P_42F10175" targetNode="P_39F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_130F10175"/>
	</edges>
	<edges id="P_42F10175P_43F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10175" targetNode="P_43F10175"/>
	<edges id="P_54F10175P_55F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10175" targetNode="P_55F10175"/>
	<edges id="P_56F10175P_57F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10175" targetNode="P_57F10175"/>
	<edges id="P_45F10175P_46F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10175" targetNode="P_46F10175"/>
	<edges id="P_47F10175P_48F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10175" targetNode="P_48F10175"/>
	<edges id="P_58F10175P_59F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10175" targetNode="P_59F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10175_I" deadCode="false" sourceNode="P_10F10175" targetNode="P_62F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_154F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10175_O" deadCode="false" sourceNode="P_10F10175" targetNode="P_63F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_154F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_156F10175_I" deadCode="false" sourceNode="P_10F10175" targetNode="P_64F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_156F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_156F10175_O" deadCode="false" sourceNode="P_10F10175" targetNode="P_65F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_156F10175"/>
	</edges>
	<edges id="P_10F10175P_11F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10175" targetNode="P_11F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10175_I" deadCode="true" sourceNode="P_62F10175" targetNode="P_66F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_161F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_161F10175_O" deadCode="true" sourceNode="P_62F10175" targetNode="P_67F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_161F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10175_I" deadCode="true" sourceNode="P_62F10175" targetNode="P_66F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_166F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_166F10175_O" deadCode="true" sourceNode="P_62F10175" targetNode="P_67F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_166F10175"/>
	</edges>
	<edges id="P_62F10175P_63F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_62F10175" targetNode="P_63F10175"/>
	<edges id="P_64F10175P_65F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10175" targetNode="P_65F10175"/>
	<edges id="P_66F10175P_67F10175" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10175" targetNode="P_67F10175"/>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10175_I" deadCode="true" sourceNode="P_70F10175" targetNode="P_71F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_195F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10175_O" deadCode="true" sourceNode="P_70F10175" targetNode="P_72F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_195F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_196F10175_I" deadCode="true" sourceNode="P_70F10175" targetNode="P_73F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_196F10175"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_196F10175_O" deadCode="true" sourceNode="P_70F10175" targetNode="P_74F10175">
		<representations href="../../../cobol/LDBS2440.cbl.cobModel#S_196F10175"/>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_105F10175_POS1" deadCode="false" targetNode="P_34F10175" sourceNode="DB2_AMMB_FUNZ_FUNZ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_105F10175"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_113F10175_POS1" deadCode="false" targetNode="P_36F10175" sourceNode="DB2_AMMB_FUNZ_FUNZ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_113F10175"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_116F10175_POS1" deadCode="false" targetNode="P_38F10175" sourceNode="DB2_AMMB_FUNZ_FUNZ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_116F10175"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_123F10175_POS1" deadCode="false" targetNode="P_42F10175" sourceNode="DB2_AMMB_FUNZ_FUNZ">
		<representations href="../../../cobol/../importantStmts.cobModel#S_123F10175"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0145" cbl:id="LVVS0145" xsi:id="LVVS0145" packageRef="LVVS0145.igd#LVVS0145" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0145_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10356" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0145.cbl.cobModel#SC_1F10356"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10356" deadCode="false" name="PROGRAM_LVVS0145_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0145.cbl.cobModel#P_1F10356"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10356" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0145.cbl.cobModel#P_2F10356"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10356" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0145.cbl.cobModel#P_3F10356"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10356" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0145.cbl.cobModel#P_4F10356"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10356" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0145.cbl.cobModel#P_5F10356"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10356" deadCode="true" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0145.cbl.cobModel#P_8F10356"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10356" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0145.cbl.cobModel#P_9F10356"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10356" deadCode="false" name="S1251-CALCOLA-TOT">
				<representations href="../../../cobol/LVVS0145.cbl.cobModel#P_10F10356"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10356" deadCode="false" name="S1251-EX">
				<representations href="../../../cobol/LVVS0145.cbl.cobModel#P_11F10356"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10356" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0145.cbl.cobModel#P_6F10356"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10356" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0145.cbl.cobModel#P_7F10356"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LDBS2270" name="LDBS2270">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10171"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10356P_1F10356" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10356" targetNode="P_1F10356"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10356_I" deadCode="false" sourceNode="P_1F10356" targetNode="P_2F10356">
		<representations href="../../../cobol/LVVS0145.cbl.cobModel#S_1F10356"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10356_O" deadCode="false" sourceNode="P_1F10356" targetNode="P_3F10356">
		<representations href="../../../cobol/LVVS0145.cbl.cobModel#S_1F10356"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10356_I" deadCode="false" sourceNode="P_1F10356" targetNode="P_4F10356">
		<representations href="../../../cobol/LVVS0145.cbl.cobModel#S_2F10356"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10356_O" deadCode="false" sourceNode="P_1F10356" targetNode="P_5F10356">
		<representations href="../../../cobol/LVVS0145.cbl.cobModel#S_2F10356"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10356_I" deadCode="false" sourceNode="P_1F10356" targetNode="P_6F10356">
		<representations href="../../../cobol/LVVS0145.cbl.cobModel#S_3F10356"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10356_O" deadCode="false" sourceNode="P_1F10356" targetNode="P_7F10356">
		<representations href="../../../cobol/LVVS0145.cbl.cobModel#S_3F10356"/>
	</edges>
	<edges id="P_2F10356P_3F10356" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10356" targetNode="P_3F10356"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10356_I" deadCode="true" sourceNode="P_4F10356" targetNode="P_8F10356">
		<representations href="../../../cobol/LVVS0145.cbl.cobModel#S_10F10356"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10356_O" deadCode="true" sourceNode="P_4F10356" targetNode="P_9F10356">
		<representations href="../../../cobol/LVVS0145.cbl.cobModel#S_10F10356"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10356_I" deadCode="false" sourceNode="P_4F10356" targetNode="P_10F10356">
		<representations href="../../../cobol/LVVS0145.cbl.cobModel#S_12F10356"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10356_O" deadCode="false" sourceNode="P_4F10356" targetNode="P_11F10356">
		<representations href="../../../cobol/LVVS0145.cbl.cobModel#S_12F10356"/>
	</edges>
	<edges id="P_4F10356P_5F10356" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10356" targetNode="P_5F10356"/>
	<edges id="P_8F10356P_9F10356" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10356" targetNode="P_9F10356"/>
	<edges id="P_10F10356P_11F10356" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10356" targetNode="P_11F10356"/>
	<edges xsi:type="cbl:CallEdge" id="S_22F10356" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_10F10356" targetNode="LDBS2270">
		<representations href="../../../cobol/../importantStmts.cobModel#S_22F10356"></representations>
	</edges>
</Package>

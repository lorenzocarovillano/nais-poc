<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LDBS5000" cbl:id="LDBS5000" xsi:id="LDBS5000" packageRef="LDBS5000.igd#LDBS5000" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LDBS5000_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10216" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LDBS5000.cbl.cobModel#SC_1F10216"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10216" deadCode="false" name="PROGRAM_LDBS5000_FIRST_SENTENCES">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_1F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10216" deadCode="false" name="A000-INIZIO">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_2F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10216" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_3F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10216" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_12F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10216" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_13F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10216" deadCode="false" name="A200-ELABORA-WC-EFF">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_4F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10216" deadCode="false" name="A200-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_5F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10216" deadCode="false" name="B200-ELABORA-WC-CPZ">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_6F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10216" deadCode="false" name="B200-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_7F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10216" deadCode="false" name="C200-ELABORA-WC-NST">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_8F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10216" deadCode="false" name="C200-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_9F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10216" deadCode="false" name="A205-DECLARE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_44F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10216" deadCode="false" name="A205-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_49F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10216" deadCode="false" name="A210-SELECT-WC-EFF">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_14F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10216" deadCode="false" name="A210-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_15F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10216" deadCode="false" name="A260-OPEN-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_16F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10216" deadCode="false" name="A260-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_17F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10216" deadCode="false" name="A270-CLOSE-CURSOR-WC-EFF">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_18F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10216" deadCode="false" name="A270-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_19F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10216" deadCode="false" name="A280-FETCH-FIRST-WC-EFF">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_20F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10216" deadCode="false" name="A280-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_21F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10216" deadCode="false" name="A290-FETCH-NEXT-WC-EFF">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_22F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10216" deadCode="false" name="A290-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_23F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_56F10216" deadCode="false" name="B205-DECLARE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_56F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_57F10216" deadCode="false" name="B205-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_57F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10216" deadCode="false" name="B210-SELECT-WC-CPZ">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_24F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10216" deadCode="false" name="B210-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_25F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10216" deadCode="false" name="B260-OPEN-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_26F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10216" deadCode="false" name="B260-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_27F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10216" deadCode="false" name="B270-CLOSE-CURSOR-WC-CPZ">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_28F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10216" deadCode="false" name="B270-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_29F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10216" deadCode="false" name="B280-FETCH-FIRST-WC-CPZ">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_30F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10216" deadCode="false" name="B280-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_31F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10216" deadCode="false" name="B290-FETCH-NEXT-WC-CPZ">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_32F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10216" deadCode="false" name="B290-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_33F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_58F10216" deadCode="false" name="C205-DECLARE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_58F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_59F10216" deadCode="false" name="C205-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_59F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10216" deadCode="false" name="C210-SELECT-WC-NST">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_34F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10216" deadCode="false" name="C210-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_35F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10216" deadCode="false" name="C260-OPEN-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_36F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10216" deadCode="false" name="C260-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_37F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10216" deadCode="false" name="C270-CLOSE-CURSOR-WC-NST">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_38F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10216" deadCode="false" name="C270-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_39F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10216" deadCode="false" name="C280-FETCH-FIRST-WC-NST">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_40F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10216" deadCode="false" name="C280-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_41F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10216" deadCode="false" name="C290-FETCH-NEXT-WC-NST">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_42F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10216" deadCode="false" name="C290-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_43F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10216" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_50F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10216" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_51F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_60F10216" deadCode="true" name="Z900-CONVERTI-N-TO-X">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_60F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_63F10216" deadCode="true" name="Z900-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_63F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10216" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_52F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10216" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_53F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10216" deadCode="false" name="Z960-LENGTH-VCHAR">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_45F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10216" deadCode="false" name="Z960-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_46F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10216" deadCode="false" name="Z970-CODICE-ADHOC-PRE">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_47F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10216" deadCode="false" name="Z970-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_48F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10216" deadCode="false" name="Z980-CODICE-ADHOC-POST">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_54F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10216" deadCode="false" name="Z980-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_55F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10216" deadCode="false" name="A001-TRATTA-DATE-TIMESTAMP">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_10F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10216" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_11F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_66F10216" deadCode="false" name="A020-CONVERTI-DT-EFFETTO">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_66F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_67F10216" deadCode="false" name="A020-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_67F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_68F10216" deadCode="false" name="A050-VALORIZZA-CPTZ">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_68F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_69F10216" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_69F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_61F10216" deadCode="false" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_61F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_62F10216" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_62F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_70F10216" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_70F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_71F10216" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_71F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_64F10216" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_64F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_65F10216" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_65F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_72F10216" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_72F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_73F10216" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_73F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_74F10216" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_74F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_75F10216" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_75F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_76F10216" deadCode="true" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_76F10216"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_77F10216" deadCode="true" name="Z801-EX">
				<representations href="../../../cobol/LDBS5000.cbl.cobModel#P_77F10216"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_DETT_QUEST" name="DETT_QUEST">
			<representations href="../../../explorer/storage-explorer.xml.storage#SQLD_DETT_QUEST"/>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_QUEST" name="QUEST" missing="true">
			<representations href="../../../../missing.xmi#"/>
		</children>
	</packageNode>
	<edges id="SC_1F10216P_1F10216" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10216" targetNode="P_1F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10216_I" deadCode="false" sourceNode="P_1F10216" targetNode="P_2F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_2F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10216_O" deadCode="false" sourceNode="P_1F10216" targetNode="P_3F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_2F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10216_I" deadCode="false" sourceNode="P_1F10216" targetNode="P_4F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_6F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_6F10216_O" deadCode="false" sourceNode="P_1F10216" targetNode="P_5F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_6F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10216_I" deadCode="false" sourceNode="P_1F10216" targetNode="P_6F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_10F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10216_O" deadCode="false" sourceNode="P_1F10216" targetNode="P_7F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_10F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10216_I" deadCode="false" sourceNode="P_1F10216" targetNode="P_8F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_14F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10216_O" deadCode="false" sourceNode="P_1F10216" targetNode="P_9F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_14F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10216_I" deadCode="false" sourceNode="P_2F10216" targetNode="P_10F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_24F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_24F10216_O" deadCode="false" sourceNode="P_2F10216" targetNode="P_11F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_24F10216"/>
	</edges>
	<edges id="P_2F10216P_3F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10216" targetNode="P_3F10216"/>
	<edges id="P_12F10216P_13F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10216" targetNode="P_13F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10216_I" deadCode="false" sourceNode="P_4F10216" targetNode="P_14F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_37F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_37F10216_O" deadCode="false" sourceNode="P_4F10216" targetNode="P_15F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_37F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10216_I" deadCode="false" sourceNode="P_4F10216" targetNode="P_16F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_38F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10216_O" deadCode="false" sourceNode="P_4F10216" targetNode="P_17F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_38F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10216_I" deadCode="false" sourceNode="P_4F10216" targetNode="P_18F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_39F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10216_O" deadCode="false" sourceNode="P_4F10216" targetNode="P_19F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_39F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10216_I" deadCode="false" sourceNode="P_4F10216" targetNode="P_20F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_40F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_40F10216_O" deadCode="false" sourceNode="P_4F10216" targetNode="P_21F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_40F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10216_I" deadCode="false" sourceNode="P_4F10216" targetNode="P_22F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_41F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_41F10216_O" deadCode="false" sourceNode="P_4F10216" targetNode="P_23F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_41F10216"/>
	</edges>
	<edges id="P_4F10216P_5F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10216" targetNode="P_5F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10216_I" deadCode="false" sourceNode="P_6F10216" targetNode="P_24F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_45F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10216_O" deadCode="false" sourceNode="P_6F10216" targetNode="P_25F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_45F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10216_I" deadCode="false" sourceNode="P_6F10216" targetNode="P_26F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_46F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_46F10216_O" deadCode="false" sourceNode="P_6F10216" targetNode="P_27F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_46F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10216_I" deadCode="false" sourceNode="P_6F10216" targetNode="P_28F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_47F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_47F10216_O" deadCode="false" sourceNode="P_6F10216" targetNode="P_29F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_47F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10216_I" deadCode="false" sourceNode="P_6F10216" targetNode="P_30F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_48F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_48F10216_O" deadCode="false" sourceNode="P_6F10216" targetNode="P_31F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_48F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10216_I" deadCode="false" sourceNode="P_6F10216" targetNode="P_32F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_49F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_49F10216_O" deadCode="false" sourceNode="P_6F10216" targetNode="P_33F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_49F10216"/>
	</edges>
	<edges id="P_6F10216P_7F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10216" targetNode="P_7F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10216_I" deadCode="false" sourceNode="P_8F10216" targetNode="P_34F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_53F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_53F10216_O" deadCode="false" sourceNode="P_8F10216" targetNode="P_35F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_53F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10216_I" deadCode="false" sourceNode="P_8F10216" targetNode="P_36F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_54F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_54F10216_O" deadCode="false" sourceNode="P_8F10216" targetNode="P_37F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_54F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10216_I" deadCode="false" sourceNode="P_8F10216" targetNode="P_38F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_55F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10216_O" deadCode="false" sourceNode="P_8F10216" targetNode="P_39F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_55F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10216_I" deadCode="false" sourceNode="P_8F10216" targetNode="P_40F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_56F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10216_O" deadCode="false" sourceNode="P_8F10216" targetNode="P_41F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_56F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10216_I" deadCode="false" sourceNode="P_8F10216" targetNode="P_42F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_57F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10216_O" deadCode="false" sourceNode="P_8F10216" targetNode="P_43F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_57F10216"/>
	</edges>
	<edges id="P_8F10216P_9F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10216" targetNode="P_9F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10216_I" deadCode="false" sourceNode="P_44F10216" targetNode="P_45F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_60F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10216_O" deadCode="false" sourceNode="P_44F10216" targetNode="P_46F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_60F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10216_I" deadCode="false" sourceNode="P_44F10216" targetNode="P_47F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_61F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_61F10216_O" deadCode="false" sourceNode="P_44F10216" targetNode="P_48F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_61F10216"/>
	</edges>
	<edges id="P_44F10216P_49F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_44F10216" targetNode="P_49F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10216_I" deadCode="false" sourceNode="P_14F10216" targetNode="P_45F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_65F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10216_O" deadCode="false" sourceNode="P_14F10216" targetNode="P_46F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_65F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10216_I" deadCode="false" sourceNode="P_14F10216" targetNode="P_47F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_66F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_66F10216_O" deadCode="false" sourceNode="P_14F10216" targetNode="P_48F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_66F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10216_I" deadCode="false" sourceNode="P_14F10216" targetNode="P_12F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_68F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10216_O" deadCode="false" sourceNode="P_14F10216" targetNode="P_13F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_68F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10216_I" deadCode="false" sourceNode="P_14F10216" targetNode="P_50F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_70F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10216_O" deadCode="false" sourceNode="P_14F10216" targetNode="P_51F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_70F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10216_I" deadCode="false" sourceNode="P_14F10216" targetNode="P_52F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_71F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10216_O" deadCode="false" sourceNode="P_14F10216" targetNode="P_53F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_71F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10216_I" deadCode="false" sourceNode="P_14F10216" targetNode="P_54F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_72F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_72F10216_O" deadCode="false" sourceNode="P_14F10216" targetNode="P_55F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_72F10216"/>
	</edges>
	<edges id="P_14F10216P_15F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10216" targetNode="P_15F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10216_I" deadCode="false" sourceNode="P_16F10216" targetNode="P_44F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_74F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10216_O" deadCode="false" sourceNode="P_16F10216" targetNode="P_49F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_74F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10216_I" deadCode="false" sourceNode="P_16F10216" targetNode="P_12F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_76F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_76F10216_O" deadCode="false" sourceNode="P_16F10216" targetNode="P_13F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_76F10216"/>
	</edges>
	<edges id="P_16F10216P_17F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10216" targetNode="P_17F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10216_I" deadCode="false" sourceNode="P_18F10216" targetNode="P_12F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_79F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10216_O" deadCode="false" sourceNode="P_18F10216" targetNode="P_13F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_79F10216"/>
	</edges>
	<edges id="P_18F10216P_19F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10216" targetNode="P_19F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10216_I" deadCode="false" sourceNode="P_20F10216" targetNode="P_16F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_81F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_81F10216_O" deadCode="false" sourceNode="P_20F10216" targetNode="P_17F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_81F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10216_I" deadCode="false" sourceNode="P_20F10216" targetNode="P_22F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_83F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10216_O" deadCode="false" sourceNode="P_20F10216" targetNode="P_23F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_83F10216"/>
	</edges>
	<edges id="P_20F10216P_21F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10216" targetNode="P_21F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10216_I" deadCode="false" sourceNode="P_22F10216" targetNode="P_12F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_86F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10216_O" deadCode="false" sourceNode="P_22F10216" targetNode="P_13F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_86F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10216_I" deadCode="false" sourceNode="P_22F10216" targetNode="P_50F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_88F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10216_O" deadCode="false" sourceNode="P_22F10216" targetNode="P_51F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_88F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10216_I" deadCode="false" sourceNode="P_22F10216" targetNode="P_52F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_89F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_89F10216_O" deadCode="false" sourceNode="P_22F10216" targetNode="P_53F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_89F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10216_I" deadCode="false" sourceNode="P_22F10216" targetNode="P_54F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_90F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_90F10216_O" deadCode="false" sourceNode="P_22F10216" targetNode="P_55F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_90F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10216_I" deadCode="false" sourceNode="P_22F10216" targetNode="P_18F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_92F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_92F10216_O" deadCode="false" sourceNode="P_22F10216" targetNode="P_19F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_92F10216"/>
	</edges>
	<edges id="P_22F10216P_23F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10216" targetNode="P_23F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10216_I" deadCode="false" sourceNode="P_56F10216" targetNode="P_45F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_96F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10216_O" deadCode="false" sourceNode="P_56F10216" targetNode="P_46F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_96F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10216_I" deadCode="false" sourceNode="P_56F10216" targetNode="P_47F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_97F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10216_O" deadCode="false" sourceNode="P_56F10216" targetNode="P_48F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_97F10216"/>
	</edges>
	<edges id="P_56F10216P_57F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_56F10216" targetNode="P_57F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10216_I" deadCode="false" sourceNode="P_24F10216" targetNode="P_45F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_101F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_101F10216_O" deadCode="false" sourceNode="P_24F10216" targetNode="P_46F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_101F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10216_I" deadCode="false" sourceNode="P_24F10216" targetNode="P_47F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_102F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_102F10216_O" deadCode="false" sourceNode="P_24F10216" targetNode="P_48F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_102F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10216_I" deadCode="false" sourceNode="P_24F10216" targetNode="P_12F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_104F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_104F10216_O" deadCode="false" sourceNode="P_24F10216" targetNode="P_13F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_104F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10216_I" deadCode="false" sourceNode="P_24F10216" targetNode="P_50F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_106F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10216_O" deadCode="false" sourceNode="P_24F10216" targetNode="P_51F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_106F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10216_I" deadCode="false" sourceNode="P_24F10216" targetNode="P_52F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_107F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10216_O" deadCode="false" sourceNode="P_24F10216" targetNode="P_53F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_107F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10216_I" deadCode="false" sourceNode="P_24F10216" targetNode="P_54F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_108F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_108F10216_O" deadCode="false" sourceNode="P_24F10216" targetNode="P_55F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_108F10216"/>
	</edges>
	<edges id="P_24F10216P_25F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10216" targetNode="P_25F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10216_I" deadCode="false" sourceNode="P_26F10216" targetNode="P_56F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_110F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_110F10216_O" deadCode="false" sourceNode="P_26F10216" targetNode="P_57F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_110F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10216_I" deadCode="false" sourceNode="P_26F10216" targetNode="P_12F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_112F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_112F10216_O" deadCode="false" sourceNode="P_26F10216" targetNode="P_13F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_112F10216"/>
	</edges>
	<edges id="P_26F10216P_27F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10216" targetNode="P_27F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10216_I" deadCode="false" sourceNode="P_28F10216" targetNode="P_12F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_115F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_115F10216_O" deadCode="false" sourceNode="P_28F10216" targetNode="P_13F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_115F10216"/>
	</edges>
	<edges id="P_28F10216P_29F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10216" targetNode="P_29F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10216_I" deadCode="false" sourceNode="P_30F10216" targetNode="P_26F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_117F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_117F10216_O" deadCode="false" sourceNode="P_30F10216" targetNode="P_27F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_117F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10216_I" deadCode="false" sourceNode="P_30F10216" targetNode="P_32F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_119F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10216_O" deadCode="false" sourceNode="P_30F10216" targetNode="P_33F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_119F10216"/>
	</edges>
	<edges id="P_30F10216P_31F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10216" targetNode="P_31F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10216_I" deadCode="false" sourceNode="P_32F10216" targetNode="P_12F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_122F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_122F10216_O" deadCode="false" sourceNode="P_32F10216" targetNode="P_13F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_122F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10216_I" deadCode="false" sourceNode="P_32F10216" targetNode="P_50F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_124F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_124F10216_O" deadCode="false" sourceNode="P_32F10216" targetNode="P_51F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_124F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10216_I" deadCode="false" sourceNode="P_32F10216" targetNode="P_52F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_125F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_125F10216_O" deadCode="false" sourceNode="P_32F10216" targetNode="P_53F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_125F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10216_I" deadCode="false" sourceNode="P_32F10216" targetNode="P_54F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_126F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_126F10216_O" deadCode="false" sourceNode="P_32F10216" targetNode="P_55F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_126F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10216_I" deadCode="false" sourceNode="P_32F10216" targetNode="P_28F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_128F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_128F10216_O" deadCode="false" sourceNode="P_32F10216" targetNode="P_29F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_128F10216"/>
	</edges>
	<edges id="P_32F10216P_33F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10216" targetNode="P_33F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10216_I" deadCode="false" sourceNode="P_58F10216" targetNode="P_45F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_132F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_132F10216_O" deadCode="false" sourceNode="P_58F10216" targetNode="P_46F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_132F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10216_I" deadCode="false" sourceNode="P_58F10216" targetNode="P_47F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_133F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_133F10216_O" deadCode="false" sourceNode="P_58F10216" targetNode="P_48F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_133F10216"/>
	</edges>
	<edges id="P_58F10216P_59F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_58F10216" targetNode="P_59F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10216_I" deadCode="false" sourceNode="P_34F10216" targetNode="P_45F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_136F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_136F10216_O" deadCode="false" sourceNode="P_34F10216" targetNode="P_46F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_136F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10216_I" deadCode="false" sourceNode="P_34F10216" targetNode="P_47F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_137F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_137F10216_O" deadCode="false" sourceNode="P_34F10216" targetNode="P_48F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_137F10216"/>
	</edges>
	<edges id="P_34F10216P_35F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10216" targetNode="P_35F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10216_I" deadCode="false" sourceNode="P_36F10216" targetNode="P_58F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_140F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_140F10216_O" deadCode="false" sourceNode="P_36F10216" targetNode="P_59F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_140F10216"/>
	</edges>
	<edges id="P_36F10216P_37F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10216" targetNode="P_37F10216"/>
	<edges id="P_38F10216P_39F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10216" targetNode="P_39F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10216_I" deadCode="false" sourceNode="P_40F10216" targetNode="P_36F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_145F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10216_O" deadCode="false" sourceNode="P_40F10216" targetNode="P_37F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_145F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10216_I" deadCode="false" sourceNode="P_40F10216" targetNode="P_42F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_147F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_147F10216_O" deadCode="false" sourceNode="P_40F10216" targetNode="P_43F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_147F10216"/>
	</edges>
	<edges id="P_40F10216P_41F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10216" targetNode="P_41F10216"/>
	<edges id="P_42F10216P_43F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10216" targetNode="P_43F10216"/>
	<edges id="P_50F10216P_51F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10216" targetNode="P_51F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10216_I" deadCode="true" sourceNode="P_60F10216" targetNode="P_61F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_184F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_184F10216_O" deadCode="true" sourceNode="P_60F10216" targetNode="P_62F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_184F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10216_I" deadCode="true" sourceNode="P_60F10216" targetNode="P_61F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_187F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10216_O" deadCode="true" sourceNode="P_60F10216" targetNode="P_62F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_187F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10216_I" deadCode="true" sourceNode="P_60F10216" targetNode="P_61F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_191F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_191F10216_O" deadCode="true" sourceNode="P_60F10216" targetNode="P_62F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_191F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10216_I" deadCode="false" sourceNode="P_52F10216" targetNode="P_64F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_195F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10216_O" deadCode="false" sourceNode="P_52F10216" targetNode="P_65F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_195F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_198F10216_I" deadCode="false" sourceNode="P_52F10216" targetNode="P_64F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_198F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_198F10216_O" deadCode="false" sourceNode="P_52F10216" targetNode="P_65F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_198F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10216_I" deadCode="false" sourceNode="P_52F10216" targetNode="P_64F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_202F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_202F10216_O" deadCode="false" sourceNode="P_52F10216" targetNode="P_65F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_202F10216"/>
	</edges>
	<edges id="P_52F10216P_53F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10216" targetNode="P_53F10216"/>
	<edges id="P_45F10216P_46F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_45F10216" targetNode="P_46F10216"/>
	<edges id="P_47F10216P_48F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_47F10216" targetNode="P_48F10216"/>
	<edges id="P_54F10216P_55F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10216" targetNode="P_55F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_212F10216_I" deadCode="false" sourceNode="P_10F10216" targetNode="P_66F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_212F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_212F10216_O" deadCode="false" sourceNode="P_10F10216" targetNode="P_67F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_212F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_214F10216_I" deadCode="false" sourceNode="P_10F10216" targetNode="P_68F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_214F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_214F10216_O" deadCode="false" sourceNode="P_10F10216" targetNode="P_69F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_214F10216"/>
	</edges>
	<edges id="P_10F10216P_11F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10216" targetNode="P_11F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_219F10216_I" deadCode="false" sourceNode="P_66F10216" targetNode="P_61F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_219F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_219F10216_O" deadCode="false" sourceNode="P_66F10216" targetNode="P_62F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_219F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_224F10216_I" deadCode="false" sourceNode="P_66F10216" targetNode="P_61F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_224F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_224F10216_O" deadCode="false" sourceNode="P_66F10216" targetNode="P_62F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_224F10216"/>
	</edges>
	<edges id="P_66F10216P_67F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_66F10216" targetNode="P_67F10216"/>
	<edges id="P_68F10216P_69F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_68F10216" targetNode="P_69F10216"/>
	<edges id="P_61F10216P_62F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_61F10216" targetNode="P_62F10216"/>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10216_I" deadCode="false" sourceNode="P_64F10216" targetNode="P_72F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_253F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_253F10216_O" deadCode="false" sourceNode="P_64F10216" targetNode="P_73F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_253F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_254F10216_I" deadCode="false" sourceNode="P_64F10216" targetNode="P_74F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_254F10216"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_254F10216_O" deadCode="false" sourceNode="P_64F10216" targetNode="P_75F10216">
		<representations href="../../../cobol/LDBS5000.cbl.cobModel#S_254F10216"/>
	</edges>
	<edges id="P_64F10216P_65F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_64F10216" targetNode="P_65F10216"/>
	<edges id="P_72F10216P_73F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_72F10216" targetNode="P_73F10216"/>
	<edges id="P_74F10216P_75F10216" xsi:type="cbl:FallThroughEdge" sourceNode="P_74F10216" targetNode="P_75F10216"/>
	<edges xsi:type="cbl:DataEdge" id="S_67F10216_POS1" deadCode="false" targetNode="P_14F10216" sourceNode="DB2_DETT_QUEST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10216"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_67F10216_POS2" deadCode="false" targetNode="P_14F10216" sourceNode="DB2_QUEST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_67F10216"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_75F10216_POS1" deadCode="false" targetNode="P_16F10216" sourceNode="DB2_DETT_QUEST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_75F10216"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_75F10216_POS2" deadCode="false" targetNode="P_16F10216" sourceNode="DB2_QUEST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_75F10216"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_78F10216_POS1" deadCode="false" targetNode="P_18F10216" sourceNode="DB2_DETT_QUEST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10216"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_78F10216_POS2" deadCode="false" targetNode="P_18F10216" sourceNode="DB2_QUEST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_78F10216"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_85F10216_POS1" deadCode="false" targetNode="P_22F10216" sourceNode="DB2_DETT_QUEST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_85F10216"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_85F10216_POS2" deadCode="false" targetNode="P_22F10216" sourceNode="DB2_QUEST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_85F10216"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_103F10216_POS1" deadCode="false" targetNode="P_24F10216" sourceNode="DB2_DETT_QUEST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_103F10216"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_103F10216_POS2" deadCode="false" targetNode="P_24F10216" sourceNode="DB2_QUEST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_103F10216"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10216_POS1" deadCode="false" targetNode="P_26F10216" sourceNode="DB2_DETT_QUEST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10216"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_111F10216_POS2" deadCode="false" targetNode="P_26F10216" sourceNode="DB2_QUEST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10216"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_114F10216_POS1" deadCode="false" targetNode="P_28F10216" sourceNode="DB2_DETT_QUEST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10216"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_114F10216_POS2" deadCode="false" targetNode="P_28F10216" sourceNode="DB2_QUEST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_114F10216"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_121F10216_POS1" deadCode="false" targetNode="P_32F10216" sourceNode="DB2_DETT_QUEST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_121F10216"></representations>
	</edges>
	<edges xsi:type="cbl:DataEdge" id="S_121F10216_POS2" deadCode="false" targetNode="P_32F10216" sourceNode="DB2_QUEST">
		<representations href="../../../cobol/../importantStmts.cobModel#S_121F10216"></representations>
	</edges>
</Package>

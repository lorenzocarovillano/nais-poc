<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LOAS1050" cbl:id="LOAS1050" xsi:id="LOAS1050" packageRef="LOAS1050.igd#LOAS1050" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LOAS1050_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10296" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LOAS1050.cbl.cobModel#SC_1F10296"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10296" deadCode="false" name="PROGRAM_LOAS1050_FIRST_SENTENCES">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_1F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10296" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_2F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10296" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_3F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10296" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_4F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10296" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_5F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10296" deadCode="false" name="S2100-LEGGI-MATR-ELAB">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_10F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10296" deadCode="false" name="EX-S2100">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_11F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10296" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_6F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10296" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_7F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10296" deadCode="false" name="AGGIORNA-TABELLA">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_20F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10296" deadCode="false" name="AGGIORNA-TABELLA-EX">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_21F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10296" deadCode="false" name="ESTR-SEQUENCE">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_22F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10296" deadCode="false" name="ESTR-SEQUENCE-EX">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_25F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10296" deadCode="false" name="VALORIZZA-OUTPUT-L71">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_16F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10296" deadCode="false" name="VALORIZZA-OUTPUT-L71-EX">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_17F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10296" deadCode="false" name="INIZIA-TOT-L71">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_8F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10296" deadCode="false" name="INIZIA-TOT-L71-EX">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_9F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10296" deadCode="false" name="INIZIA-NULL-L71">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_30F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10296" deadCode="false" name="INIZIA-NULL-L71-EX">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_31F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10296" deadCode="false" name="INIZIA-ZEROES-L71">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_26F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10296" deadCode="false" name="INIZIA-ZEROES-L71-EX">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_27F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10296" deadCode="false" name="INIZIA-SPACES-L71">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_28F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10296" deadCode="false" name="INIZIA-SPACES-L71-EX">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_29F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10296" deadCode="false" name="VAL-DCLGEN-L71">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_32F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10296" deadCode="false" name="VAL-DCLGEN-L71-EX">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_33F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10296" deadCode="false" name="AGGIORNA-MATR-ELAB-BATCH">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_12F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10296" deadCode="false" name="AGGIORNA-MATR-ELAB-BATCH-EX">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_13F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10296" deadCode="false" name="VALORIZZA-AREA-DSH-L71">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_34F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10296" deadCode="false" name="VALORIZZA-AREA-DSH-L71-EX">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_35F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10296" deadCode="false" name="S0290-ERRORE-DI-SISTEMA">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_23F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10296" deadCode="false" name="EX-S0290">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_24F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10296" deadCode="false" name="S0300-RICERCA-GRAVITA-ERRORE">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_18F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10296" deadCode="false" name="EX-S0300">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_19F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10296" deadCode="false" name="S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_38F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10296" deadCode="false" name="EX-S0300-IMPOSTA-ERRORE">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_39F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10296" deadCode="false" name="S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_36F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10296" deadCode="false" name="EX-S0310-ERRORE-FATALE">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_37F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10296" deadCode="false" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_14F10296"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10296" deadCode="false" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LOAS1050.cbl.cobModel#P_15F10296"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0090" name="LCCS0090">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10133"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IEAS9900" name="IEAS9900">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10109"></representations>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0010" name="IDSS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10099"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10296P_1F10296" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10296" targetNode="P_1F10296"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10296_I" deadCode="false" sourceNode="P_1F10296" targetNode="P_2F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_1F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10296_O" deadCode="false" sourceNode="P_1F10296" targetNode="P_3F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_1F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10296_I" deadCode="false" sourceNode="P_1F10296" targetNode="P_4F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_3F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10296_O" deadCode="false" sourceNode="P_1F10296" targetNode="P_5F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_3F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10296_I" deadCode="false" sourceNode="P_1F10296" targetNode="P_6F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_4F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10296_O" deadCode="false" sourceNode="P_1F10296" targetNode="P_7F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_4F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10296_I" deadCode="false" sourceNode="P_2F10296" targetNode="P_8F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_5F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10296_O" deadCode="false" sourceNode="P_2F10296" targetNode="P_9F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_5F10296"/>
	</edges>
	<edges id="P_2F10296P_3F10296" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10296" targetNode="P_3F10296"/>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10296_I" deadCode="false" sourceNode="P_4F10296" targetNode="P_10F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_7F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10296_O" deadCode="false" sourceNode="P_4F10296" targetNode="P_11F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_7F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10296_I" deadCode="false" sourceNode="P_4F10296" targetNode="P_12F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_10F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10296_O" deadCode="false" sourceNode="P_4F10296" targetNode="P_13F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_10F10296"/>
	</edges>
	<edges id="P_4F10296P_5F10296" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10296" targetNode="P_5F10296"/>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10296_I" deadCode="false" sourceNode="P_10F10296" targetNode="P_14F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_26F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_26F10296_O" deadCode="false" sourceNode="P_10F10296" targetNode="P_15F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_26F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10296_I" deadCode="false" sourceNode="P_10F10296" targetNode="P_16F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_31F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_31F10296_O" deadCode="false" sourceNode="P_10F10296" targetNode="P_17F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_31F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10296_I" deadCode="false" sourceNode="P_10F10296" targetNode="P_18F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_45F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10296_O" deadCode="false" sourceNode="P_10F10296" targetNode="P_19F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_45F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10296_I" deadCode="false" sourceNode="P_10F10296" targetNode="P_18F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_50F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10296_O" deadCode="false" sourceNode="P_10F10296" targetNode="P_19F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_50F10296"/>
	</edges>
	<edges id="P_10F10296P_11F10296" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10296" targetNode="P_11F10296"/>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10296_I" deadCode="false" sourceNode="P_20F10296" targetNode="P_14F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_55F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_55F10296_O" deadCode="false" sourceNode="P_20F10296" targetNode="P_15F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_55F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10296_I" deadCode="false" sourceNode="P_20F10296" targetNode="P_18F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_63F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_63F10296_O" deadCode="false" sourceNode="P_20F10296" targetNode="P_19F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_63F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10296_I" deadCode="false" sourceNode="P_20F10296" targetNode="P_18F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_68F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_68F10296_O" deadCode="false" sourceNode="P_20F10296" targetNode="P_19F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_68F10296"/>
	</edges>
	<edges id="P_20F10296P_21F10296" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10296" targetNode="P_21F10296"/>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10296_I" deadCode="false" sourceNode="P_22F10296" targetNode="P_23F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_75F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10296_O" deadCode="false" sourceNode="P_22F10296" targetNode="P_24F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_75F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10296_I" deadCode="false" sourceNode="P_22F10296" targetNode="P_18F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_83F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_83F10296_O" deadCode="false" sourceNode="P_22F10296" targetNode="P_19F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_83F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10296_I" deadCode="false" sourceNode="P_22F10296" targetNode="P_18F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_88F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_88F10296_O" deadCode="false" sourceNode="P_22F10296" targetNode="P_19F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_88F10296"/>
	</edges>
	<edges id="P_22F10296P_25F10296" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10296" targetNode="P_25F10296"/>
	<edges id="P_16F10296P_17F10296" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10296" targetNode="P_17F10296"/>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10296_I" deadCode="false" sourceNode="P_8F10296" targetNode="P_26F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_105F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_105F10296_O" deadCode="false" sourceNode="P_8F10296" targetNode="P_27F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_105F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10296_I" deadCode="false" sourceNode="P_8F10296" targetNode="P_28F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_106F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_106F10296_O" deadCode="false" sourceNode="P_8F10296" targetNode="P_29F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_106F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10296_I" deadCode="false" sourceNode="P_8F10296" targetNode="P_30F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_107F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_107F10296_O" deadCode="false" sourceNode="P_8F10296" targetNode="P_31F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_107F10296"/>
	</edges>
	<edges id="P_8F10296P_9F10296" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10296" targetNode="P_9F10296"/>
	<edges id="P_30F10296P_31F10296" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10296" targetNode="P_31F10296"/>
	<edges id="P_26F10296P_27F10296" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10296" targetNode="P_27F10296"/>
	<edges id="P_28F10296P_29F10296" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10296" targetNode="P_29F10296"/>
	<edges id="P_32F10296P_33F10296" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10296" targetNode="P_33F10296"/>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10296_I" deadCode="false" sourceNode="P_12F10296" targetNode="P_22F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_145F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_145F10296_O" deadCode="false" sourceNode="P_12F10296" targetNode="P_25F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_145F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10296_I" deadCode="false" sourceNode="P_12F10296" targetNode="P_32F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_152F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_152F10296_O" deadCode="false" sourceNode="P_12F10296" targetNode="P_33F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_152F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10296_I" deadCode="false" sourceNode="P_12F10296" targetNode="P_34F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_153F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_153F10296_O" deadCode="false" sourceNode="P_12F10296" targetNode="P_35F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_153F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10296_I" deadCode="false" sourceNode="P_12F10296" targetNode="P_20F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_154F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_154F10296_O" deadCode="false" sourceNode="P_12F10296" targetNode="P_21F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_154F10296"/>
	</edges>
	<edges id="P_12F10296P_13F10296" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10296" targetNode="P_13F10296"/>
	<edges id="P_34F10296P_35F10296" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10296" targetNode="P_35F10296"/>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10296_I" deadCode="false" sourceNode="P_23F10296" targetNode="P_18F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_163F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_163F10296_O" deadCode="false" sourceNode="P_23F10296" targetNode="P_19F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_163F10296"/>
	</edges>
	<edges id="P_23F10296P_24F10296" xsi:type="cbl:FallThroughEdge" sourceNode="P_23F10296" targetNode="P_24F10296"/>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10296_I" deadCode="false" sourceNode="P_18F10296" targetNode="P_36F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_170F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_170F10296_O" deadCode="false" sourceNode="P_18F10296" targetNode="P_37F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_170F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10296_I" deadCode="false" sourceNode="P_18F10296" targetNode="P_38F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_174F10296"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_174F10296_O" deadCode="false" sourceNode="P_18F10296" targetNode="P_39F10296">
		<representations href="../../../cobol/LOAS1050.cbl.cobModel#S_174F10296"/>
	</edges>
	<edges id="P_18F10296P_19F10296" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10296" targetNode="P_19F10296"/>
	<edges id="P_38F10296P_39F10296" xsi:type="cbl:FallThroughEdge" sourceNode="P_38F10296" targetNode="P_39F10296"/>
	<edges id="P_36F10296P_37F10296" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10296" targetNode="P_37F10296"/>
	<edges id="P_14F10296P_15F10296" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10296" targetNode="P_15F10296"/>
	<edges xsi:type="cbl:CallEdge" id="S_71F10296" deadCode="false" name="Dynamic LCCS0090" sourceNode="P_22F10296" targetNode="LCCS0090">
		<representations href="../../../cobol/../importantStmts.cobModel#S_71F10296"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_168F10296" deadCode="false" name="Dynamic CALL-PGM" sourceNode="P_18F10296" targetNode="IEAS9900">
		<representations href="../../../cobol/../importantStmts.cobModel#S_168F10296"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_234F10296" deadCode="false" name="Dynamic IDSI0011-PGM" sourceNode="P_14F10296" targetNode="IDSS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_234F10296"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0112" cbl:id="LVVS0112" xsi:id="LVVS0112" packageRef="LVVS0112.igd#LVVS0112" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0112_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10350" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0112.cbl.cobModel#SC_1F10350"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10350" deadCode="false" name="PROGRAM_LVVS0112_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0112.cbl.cobModel#P_1F10350"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10350" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0112.cbl.cobModel#P_2F10350"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10350" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0112.cbl.cobModel#P_3F10350"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10350" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0112.cbl.cobModel#P_4F10350"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10350" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0112.cbl.cobModel#P_5F10350"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10350" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0112.cbl.cobModel#P_8F10350"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10350" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0112.cbl.cobModel#P_9F10350"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10350" deadCode="false" name="S1260-CALCOLA-VAL">
				<representations href="../../../cobol/LVVS0112.cbl.cobModel#P_10F10350"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10350" deadCode="false" name="S1260-EX">
				<representations href="../../../cobol/LVVS0112.cbl.cobModel#P_11F10350"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10350" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0112.cbl.cobModel#P_6F10350"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10350" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0112.cbl.cobModel#P_7F10350"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10350P_1F10350" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10350" targetNode="P_1F10350"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10350_I" deadCode="false" sourceNode="P_1F10350" targetNode="P_2F10350">
		<representations href="../../../cobol/LVVS0112.cbl.cobModel#S_1F10350"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10350_O" deadCode="false" sourceNode="P_1F10350" targetNode="P_3F10350">
		<representations href="../../../cobol/LVVS0112.cbl.cobModel#S_1F10350"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10350_I" deadCode="false" sourceNode="P_1F10350" targetNode="P_4F10350">
		<representations href="../../../cobol/LVVS0112.cbl.cobModel#S_2F10350"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10350_O" deadCode="false" sourceNode="P_1F10350" targetNode="P_5F10350">
		<representations href="../../../cobol/LVVS0112.cbl.cobModel#S_2F10350"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10350_I" deadCode="false" sourceNode="P_1F10350" targetNode="P_6F10350">
		<representations href="../../../cobol/LVVS0112.cbl.cobModel#S_3F10350"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10350_O" deadCode="false" sourceNode="P_1F10350" targetNode="P_7F10350">
		<representations href="../../../cobol/LVVS0112.cbl.cobModel#S_3F10350"/>
	</edges>
	<edges id="P_2F10350P_3F10350" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10350" targetNode="P_3F10350"/>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10350_I" deadCode="false" sourceNode="P_4F10350" targetNode="P_8F10350">
		<representations href="../../../cobol/LVVS0112.cbl.cobModel#S_10F10350"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10350_O" deadCode="false" sourceNode="P_4F10350" targetNode="P_9F10350">
		<representations href="../../../cobol/LVVS0112.cbl.cobModel#S_10F10350"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10350_I" deadCode="false" sourceNode="P_4F10350" targetNode="P_10F10350">
		<representations href="../../../cobol/LVVS0112.cbl.cobModel#S_12F10350"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_12F10350_O" deadCode="false" sourceNode="P_4F10350" targetNode="P_11F10350">
		<representations href="../../../cobol/LVVS0112.cbl.cobModel#S_12F10350"/>
	</edges>
	<edges id="P_4F10350P_5F10350" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10350" targetNode="P_5F10350"/>
	<edges id="P_8F10350P_9F10350" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10350" targetNode="P_9F10350"/>
	<edges id="P_10F10350P_11F10350" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10350" targetNode="P_11F10350"/>
</Package>

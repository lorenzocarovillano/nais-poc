<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0001" cbl:id="LVVS0001" xsi:id="LVVS0001" packageRef="LVVS0001.igd#LVVS0001" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0001_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10305" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0001.cbl.cobModel#SC_1F10305"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10305" deadCode="false" name="PROGRAM_LVVS0001_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_1F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10305" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_2F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10305" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_3F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10305" deadCode="false" name="S0005-CTRL-DATI-INPUT">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_8F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10305" deadCode="false" name="EX-S0005">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_9F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10305" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_4F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10305" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_5F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10305" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_10F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10305" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_11F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10305" deadCode="false" name="S1200-CONTROLLO-DATI">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_12F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10305" deadCode="false" name="S1200-CONTROLLO-DATI-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_13F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10305" deadCode="false" name="S1210-LEGGI-DATI-TAB">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_14F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10305" deadCode="false" name="S1210-LEGGI-DATI-TAB-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_15F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10305" deadCode="false" name="S1211-VALORIZZA-INPUT-DCO">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_18F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10305" deadCode="false" name="S1211-VALORIZZA-INPUT-DCO-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_19F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10305" deadCode="false" name="S1212-VALORIZZA-INPUT-ADE">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_22F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10305" deadCode="false" name="S1212-VALORIZZA-INPUT-ADE-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_23F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10305" deadCode="false" name="S1213-VALORIZZA-INPUT-GRZ">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_26F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10305" deadCode="false" name="S1213-VALORIZZA-INPUT-GRZ-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_27F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10305" deadCode="false" name="S1214-VALORIZZA-INPUT-PMO">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_30F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10305" deadCode="false" name="S1214-VALORIZZA-INPUT-PMO-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_31F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10305" deadCode="false" name="S1215-VALORIZZA-INPUT-TGA">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_34F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10305" deadCode="false" name="S1215-VALORIZZA-INPUT-TGA-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_35F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10305" deadCode="false" name="S1220-VALORIZZA-OUT">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_16F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10305" deadCode="false" name="S1220-VALORIZZA-OUT-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_17F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10305" deadCode="true" name="S1300-VALORIZZA-INPUT-POG">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_38F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10305" deadCode="true" name="S1300-VALORIZZA-INPUT-POG-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_39F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10305" deadCode="true" name="LETTURA-POG">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_40F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_45F10305" deadCode="true" name="LETTURA-POG-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_45F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10305" deadCode="false" name="LETTURA-DCO">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_20F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10305" deadCode="false" name="LETTURA-DCO-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_21F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10305" deadCode="false" name="LETTURA-ADE">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_24F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10305" deadCode="false" name="LETTURA-ADE-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_25F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10305" deadCode="false" name="LETTURA-PMO">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_32F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10305" deadCode="false" name="LETTURA-PMO-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_33F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10305" deadCode="false" name="LETTURA-GRZ">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_28F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10305" deadCode="false" name="LETTURA-GRZ-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_29F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10305" deadCode="false" name="LETTURA-TGA">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_36F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10305" deadCode="false" name="LETTURA-TGA-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_37F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10305" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_6F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10305" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_7F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10305" deadCode="true" name="VALORIZZA-OUTPUT-POG">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_43F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_44F10305" deadCode="true" name="VALORIZZA-OUTPUT-POG-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_44F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_46F10305" deadCode="false" name="VALORIZZA-OUTPUT-DCO">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_46F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_47F10305" deadCode="false" name="VALORIZZA-OUTPUT-DCO-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_47F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_48F10305" deadCode="true" name="VALORIZZA-OUTPUT-ADE">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_48F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_49F10305" deadCode="false" name="VALORIZZA-OUTPUT-ADE-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_49F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_50F10305" deadCode="false" name="VALORIZZA-OUTPUT-PMO">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_50F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_51F10305" deadCode="false" name="VALORIZZA-OUTPUT-PMO-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_51F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_52F10305" deadCode="false" name="VALORIZZA-OUTPUT-GRZ">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_52F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_53F10305" deadCode="false" name="VALORIZZA-OUTPUT-GRZ-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_53F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_54F10305" deadCode="false" name="VALORIZZA-OUTPUT-TGA">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_54F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_55F10305" deadCode="false" name="VALORIZZA-OUTPUT-TGA-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_55F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10305" deadCode="false" name="CALL-DISPATCHER">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_41F10305"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10305" deadCode="false" name="CALL-DISPATCHER-EX">
				<representations href="../../../cobol/LVVS0001.cbl.cobModel#P_42F10305"/>
			</children>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="IDSS0010" name="IDSS0010">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10099"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10305P_1F10305" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10305" targetNode="P_1F10305"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10305_I" deadCode="false" sourceNode="P_1F10305" targetNode="P_2F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_1F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10305_O" deadCode="false" sourceNode="P_1F10305" targetNode="P_3F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_1F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10305_I" deadCode="false" sourceNode="P_1F10305" targetNode="P_4F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_3F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10305_O" deadCode="false" sourceNode="P_1F10305" targetNode="P_5F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_3F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10305_I" deadCode="false" sourceNode="P_1F10305" targetNode="P_6F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_4F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_4F10305_O" deadCode="false" sourceNode="P_1F10305" targetNode="P_7F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_4F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10305_I" deadCode="false" sourceNode="P_2F10305" targetNode="P_8F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_11F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10305_O" deadCode="false" sourceNode="P_2F10305" targetNode="P_9F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_11F10305"/>
	</edges>
	<edges id="P_2F10305P_3F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10305" targetNode="P_3F10305"/>
	<edges id="P_8F10305P_9F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10305" targetNode="P_9F10305"/>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10305_I" deadCode="false" sourceNode="P_4F10305" targetNode="P_10F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_38F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_38F10305_O" deadCode="false" sourceNode="P_4F10305" targetNode="P_11F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_38F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10305_I" deadCode="false" sourceNode="P_4F10305" targetNode="P_12F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_39F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10305_O" deadCode="false" sourceNode="P_4F10305" targetNode="P_13F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_39F10305"/>
	</edges>
	<edges id="P_4F10305P_5F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10305" targetNode="P_5F10305"/>
	<edges id="P_10F10305P_11F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10305" targetNode="P_11F10305"/>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10305_I" deadCode="false" sourceNode="P_12F10305" targetNode="P_14F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_56F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_56F10305_O" deadCode="false" sourceNode="P_12F10305" targetNode="P_15F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_56F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10305_I" deadCode="false" sourceNode="P_12F10305" targetNode="P_16F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_57F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10305_O" deadCode="false" sourceNode="P_12F10305" targetNode="P_17F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_57F10305"/>
	</edges>
	<edges id="P_12F10305P_13F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10305" targetNode="P_13F10305"/>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10305_I" deadCode="false" sourceNode="P_14F10305" targetNode="P_18F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_64F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_64F10305_O" deadCode="false" sourceNode="P_14F10305" targetNode="P_19F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_64F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10305_I" deadCode="false" sourceNode="P_14F10305" targetNode="P_20F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_65F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_65F10305_O" deadCode="false" sourceNode="P_14F10305" targetNode="P_21F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_65F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10305_I" deadCode="false" sourceNode="P_14F10305" targetNode="P_22F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_70F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_70F10305_O" deadCode="false" sourceNode="P_14F10305" targetNode="P_23F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_70F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10305_I" deadCode="false" sourceNode="P_14F10305" targetNode="P_24F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_71F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_71F10305_O" deadCode="false" sourceNode="P_14F10305" targetNode="P_25F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_71F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10305_I" deadCode="false" sourceNode="P_14F10305" targetNode="P_26F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_74F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_74F10305_O" deadCode="false" sourceNode="P_14F10305" targetNode="P_27F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_74F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10305_I" deadCode="false" sourceNode="P_14F10305" targetNode="P_28F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_75F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10305_O" deadCode="false" sourceNode="P_14F10305" targetNode="P_29F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_75F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10305_I" deadCode="false" sourceNode="P_14F10305" targetNode="P_30F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_79F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_79F10305_O" deadCode="false" sourceNode="P_14F10305" targetNode="P_31F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_79F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10305_I" deadCode="false" sourceNode="P_14F10305" targetNode="P_32F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_80F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_80F10305_O" deadCode="false" sourceNode="P_14F10305" targetNode="P_33F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_80F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10305_I" deadCode="false" sourceNode="P_14F10305" targetNode="P_34F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_85F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_85F10305_O" deadCode="false" sourceNode="P_14F10305" targetNode="P_35F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_85F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10305_I" deadCode="false" sourceNode="P_14F10305" targetNode="P_36F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_86F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_86F10305_O" deadCode="false" sourceNode="P_14F10305" targetNode="P_37F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_86F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10305_I" deadCode="false" sourceNode="P_14F10305" targetNode="P_16F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_93F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_93F10305_O" deadCode="false" sourceNode="P_14F10305" targetNode="P_17F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_93F10305"/>
	</edges>
	<edges id="P_14F10305P_15F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10305" targetNode="P_15F10305"/>
	<edges id="P_18F10305P_19F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10305" targetNode="P_19F10305"/>
	<edges id="P_22F10305P_23F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10305" targetNode="P_23F10305"/>
	<edges id="P_26F10305P_27F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10305" targetNode="P_27F10305"/>
	<edges id="P_30F10305P_31F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10305" targetNode="P_31F10305"/>
	<edges id="P_34F10305P_35F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10305" targetNode="P_35F10305"/>
	<edges id="P_16F10305P_17F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10305" targetNode="P_17F10305"/>
	<edges xsi:type="cbl:PerformEdge" id="S_188F10305_I" deadCode="true" sourceNode="P_40F10305" targetNode="P_41F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_187F10305"/>
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_188F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_188F10305_O" deadCode="true" sourceNode="P_40F10305" targetNode="P_42F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_187F10305"/>
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_188F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10305_I" deadCode="true" sourceNode="P_40F10305" targetNode="P_43F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_187F10305"/>
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_195F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_195F10305_O" deadCode="true" sourceNode="P_40F10305" targetNode="P_44F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_187F10305"/>
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_195F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_208F10305_I" deadCode="false" sourceNode="P_20F10305" targetNode="P_41F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_208F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_208F10305_O" deadCode="false" sourceNode="P_20F10305" targetNode="P_42F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_208F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_212F10305_I" deadCode="false" sourceNode="P_20F10305" targetNode="P_46F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_212F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_212F10305_O" deadCode="false" sourceNode="P_20F10305" targetNode="P_47F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_212F10305"/>
	</edges>
	<edges id="P_20F10305P_21F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10305" targetNode="P_21F10305"/>
	<edges xsi:type="cbl:PerformEdge" id="S_223F10305_I" deadCode="false" sourceNode="P_24F10305" targetNode="P_41F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_223F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_223F10305_O" deadCode="false" sourceNode="P_24F10305" targetNode="P_42F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_223F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_228F10305_I" deadCode="true" sourceNode="P_24F10305" targetNode="P_48F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_228F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_228F10305_O" deadCode="true" sourceNode="P_24F10305" targetNode="P_49F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_228F10305"/>
	</edges>
	<edges id="P_24F10305P_25F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10305" targetNode="P_25F10305"/>
	<edges xsi:type="cbl:PerformEdge" id="S_239F10305_I" deadCode="false" sourceNode="P_32F10305" targetNode="P_41F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_239F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_239F10305_O" deadCode="false" sourceNode="P_32F10305" targetNode="P_42F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_239F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_244F10305_I" deadCode="false" sourceNode="P_32F10305" targetNode="P_50F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_244F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_244F10305_O" deadCode="false" sourceNode="P_32F10305" targetNode="P_51F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_244F10305"/>
	</edges>
	<edges id="P_32F10305P_33F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10305" targetNode="P_33F10305"/>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10305_I" deadCode="false" sourceNode="P_28F10305" targetNode="P_41F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_255F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_255F10305_O" deadCode="false" sourceNode="P_28F10305" targetNode="P_42F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_255F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_260F10305_I" deadCode="false" sourceNode="P_28F10305" targetNode="P_52F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_260F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_260F10305_O" deadCode="false" sourceNode="P_28F10305" targetNode="P_53F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_260F10305"/>
	</edges>
	<edges id="P_28F10305P_29F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10305" targetNode="P_29F10305"/>
	<edges xsi:type="cbl:PerformEdge" id="S_271F10305_I" deadCode="false" sourceNode="P_36F10305" targetNode="P_41F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_271F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_271F10305_O" deadCode="false" sourceNode="P_36F10305" targetNode="P_42F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_271F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10305_I" deadCode="false" sourceNode="P_36F10305" targetNode="P_54F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_276F10305"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_276F10305_O" deadCode="false" sourceNode="P_36F10305" targetNode="P_55F10305">
		<representations href="../../../cobol/LVVS0001.cbl.cobModel#S_276F10305"/>
	</edges>
	<edges id="P_36F10305P_37F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10305" targetNode="P_37F10305"/>
	<edges id="P_46F10305P_47F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_46F10305" targetNode="P_47F10305"/>
	<edges id="P_48F10305P_49F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_48F10305" targetNode="P_49F10305"/>
	<edges id="P_50F10305P_51F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_50F10305" targetNode="P_51F10305"/>
	<edges id="P_52F10305P_53F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_52F10305" targetNode="P_53F10305"/>
	<edges id="P_54F10305P_55F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_54F10305" targetNode="P_55F10305"/>
	<edges id="P_41F10305P_42F10305" xsi:type="cbl:FallThroughEdge" sourceNode="P_41F10305" targetNode="P_42F10305"/>
	<edges xsi:type="cbl:CallEdge" id="S_1295F10305" deadCode="false" name="Dynamic WK-CALL-PGM" sourceNode="P_41F10305" targetNode="IDSS0010">
		<representations href="../../../cobol/../importantStmts.cobModel#S_1295F10305"></representations>
	</edges>
</Package>

<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS2200" cbl:id="LVVS2200" xsi:id="LVVS2200" packageRef="LVVS2200.igd#LVVS2200" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS2200_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10370" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS2200.cbl.cobModel#SC_1F10370"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10370" deadCode="false" name="PROGRAM_LVVS2200_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS2200.cbl.cobModel#P_1F10370"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10370" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS2200.cbl.cobModel#P_2F10370"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10370" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS2200.cbl.cobModel#P_3F10370"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10370" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS2200.cbl.cobModel#P_4F10370"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10370" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS2200.cbl.cobModel#P_5F10370"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10370" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS2200.cbl.cobModel#P_8F10370"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10370" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS2200.cbl.cobModel#P_9F10370"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10370" deadCode="false" name="S1200-CALCOLA-PRATA">
				<representations href="../../../cobol/LVVS2200.cbl.cobModel#P_10F10370"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10370" deadCode="false" name="S1200-EX">
				<representations href="../../../cobol/LVVS2200.cbl.cobModel#P_11F10370"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10370" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS2200.cbl.cobModel#P_6F10370"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10370" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS2200.cbl.cobModel#P_7F10370"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10370P_1F10370" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10370" targetNode="P_1F10370"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10370_I" deadCode="false" sourceNode="P_1F10370" targetNode="P_2F10370">
		<representations href="../../../cobol/LVVS2200.cbl.cobModel#S_1F10370"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10370_O" deadCode="false" sourceNode="P_1F10370" targetNode="P_3F10370">
		<representations href="../../../cobol/LVVS2200.cbl.cobModel#S_1F10370"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10370_I" deadCode="false" sourceNode="P_1F10370" targetNode="P_4F10370">
		<representations href="../../../cobol/LVVS2200.cbl.cobModel#S_2F10370"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10370_O" deadCode="false" sourceNode="P_1F10370" targetNode="P_5F10370">
		<representations href="../../../cobol/LVVS2200.cbl.cobModel#S_2F10370"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10370_I" deadCode="false" sourceNode="P_1F10370" targetNode="P_6F10370">
		<representations href="../../../cobol/LVVS2200.cbl.cobModel#S_3F10370"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10370_O" deadCode="false" sourceNode="P_1F10370" targetNode="P_7F10370">
		<representations href="../../../cobol/LVVS2200.cbl.cobModel#S_3F10370"/>
	</edges>
	<edges id="P_2F10370P_3F10370" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10370" targetNode="P_3F10370"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10370_I" deadCode="false" sourceNode="P_4F10370" targetNode="P_8F10370">
		<representations href="../../../cobol/LVVS2200.cbl.cobModel#S_11F10370"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10370_O" deadCode="false" sourceNode="P_4F10370" targetNode="P_9F10370">
		<representations href="../../../cobol/LVVS2200.cbl.cobModel#S_11F10370"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10370_I" deadCode="false" sourceNode="P_4F10370" targetNode="P_10F10370">
		<representations href="../../../cobol/LVVS2200.cbl.cobModel#S_13F10370"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10370_O" deadCode="false" sourceNode="P_4F10370" targetNode="P_11F10370">
		<representations href="../../../cobol/LVVS2200.cbl.cobModel#S_13F10370"/>
	</edges>
	<edges id="P_4F10370P_5F10370" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10370" targetNode="P_5F10370"/>
	<edges id="P_8F10370P_9F10370" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10370" targetNode="P_9F10370"/>
	<edges id="P_10F10370P_11F10370" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10370" targetNode="P_11F10370"/>
</Package>

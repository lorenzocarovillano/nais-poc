<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="LVVS0005" cbl:id="LVVS0005" xsi:id="LVVS0005" packageRef="LVVS0005.igd#LVVS0005" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="LVVS0005_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10308" deadCode="false" name="FIRST">
			<representations href="../../../cobol/LVVS0005.cbl.cobModel#SC_1F10308"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10308" deadCode="false" name="PROGRAM_LVVS0005_FIRST_SENTENCES">
				<representations href="../../../cobol/LVVS0005.cbl.cobModel#P_1F10308"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10308" deadCode="false" name="S0000-OPERAZIONI-INIZIALI">
				<representations href="../../../cobol/LVVS0005.cbl.cobModel#P_2F10308"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10308" deadCode="false" name="EX-S0000">
				<representations href="../../../cobol/LVVS0005.cbl.cobModel#P_3F10308"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10308" deadCode="false" name="S1000-ELABORAZIONE">
				<representations href="../../../cobol/LVVS0005.cbl.cobModel#P_4F10308"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10308" deadCode="false" name="EX-S1000">
				<representations href="../../../cobol/LVVS0005.cbl.cobModel#P_5F10308"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10308" deadCode="false" name="S2027-RAN-GAR">
				<representations href="../../../cobol/LVVS0005.cbl.cobModel#P_10F10308"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10308" deadCode="false" name="S2027-EX">
				<representations href="../../../cobol/LVVS0005.cbl.cobModel#P_11F10308"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10308" deadCode="false" name="S3027-RAN-POL">
				<representations href="../../../cobol/LVVS0005.cbl.cobModel#P_12F10308"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10308" deadCode="false" name="S3027-EX">
				<representations href="../../../cobol/LVVS0005.cbl.cobModel#P_13F10308"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10308" deadCode="false" name="S1100-VALORIZZA-DCLGEN">
				<representations href="../../../cobol/LVVS0005.cbl.cobModel#P_8F10308"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10308" deadCode="false" name="S1100-VALORIZZA-DCLGEN-EX">
				<representations href="../../../cobol/LVVS0005.cbl.cobModel#P_9F10308"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10308" deadCode="false" name="S9000-OPERAZIONI-FINALI">
				<representations href="../../../cobol/LVVS0005.cbl.cobModel#P_6F10308"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10308" deadCode="true" name="EX-S9000">
				<representations href="../../../cobol/LVVS0005.cbl.cobModel#P_7F10308"/>
			</children>
		</children>
	</packageNode>
	<edges id="SC_1F10308P_1F10308" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10308" targetNode="P_1F10308"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10308_I" deadCode="false" sourceNode="P_1F10308" targetNode="P_2F10308">
		<representations href="../../../cobol/LVVS0005.cbl.cobModel#S_1F10308"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10308_O" deadCode="false" sourceNode="P_1F10308" targetNode="P_3F10308">
		<representations href="../../../cobol/LVVS0005.cbl.cobModel#S_1F10308"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10308_I" deadCode="false" sourceNode="P_1F10308" targetNode="P_4F10308">
		<representations href="../../../cobol/LVVS0005.cbl.cobModel#S_2F10308"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_2F10308_O" deadCode="false" sourceNode="P_1F10308" targetNode="P_5F10308">
		<representations href="../../../cobol/LVVS0005.cbl.cobModel#S_2F10308"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10308_I" deadCode="false" sourceNode="P_1F10308" targetNode="P_6F10308">
		<representations href="../../../cobol/LVVS0005.cbl.cobModel#S_3F10308"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10308_O" deadCode="false" sourceNode="P_1F10308" targetNode="P_7F10308">
		<representations href="../../../cobol/LVVS0005.cbl.cobModel#S_3F10308"/>
	</edges>
	<edges id="P_2F10308P_3F10308" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10308" targetNode="P_3F10308"/>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10308_I" deadCode="false" sourceNode="P_4F10308" targetNode="P_8F10308">
		<representations href="../../../cobol/LVVS0005.cbl.cobModel#S_11F10308"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_11F10308_O" deadCode="false" sourceNode="P_4F10308" targetNode="P_9F10308">
		<representations href="../../../cobol/LVVS0005.cbl.cobModel#S_11F10308"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10308_I" deadCode="false" sourceNode="P_4F10308" targetNode="P_10F10308">
		<representations href="../../../cobol/LVVS0005.cbl.cobModel#S_13F10308"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_13F10308_O" deadCode="false" sourceNode="P_4F10308" targetNode="P_11F10308">
		<representations href="../../../cobol/LVVS0005.cbl.cobModel#S_13F10308"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10308_I" deadCode="false" sourceNode="P_4F10308" targetNode="P_12F10308">
		<representations href="../../../cobol/LVVS0005.cbl.cobModel#S_14F10308"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10308_O" deadCode="false" sourceNode="P_4F10308" targetNode="P_13F10308">
		<representations href="../../../cobol/LVVS0005.cbl.cobModel#S_14F10308"/>
	</edges>
	<edges id="P_4F10308P_5F10308" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10308" targetNode="P_5F10308"/>
	<edges id="P_10F10308P_11F10308" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10308" targetNode="P_11F10308"/>
	<edges id="P_12F10308P_13F10308" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10308" targetNode="P_13F10308"/>
	<edges id="P_8F10308P_9F10308" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10308" targetNode="P_9F10308"/>
</Package>

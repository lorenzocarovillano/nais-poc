<Package xmlns="http:///com.bphx.logicminer.model.diagrams.ecore" name="Main" id="IDSS0150" cbl:id="IDSS0150" xsi:id="IDSS0150" packageRef="IDSS0150.igd#IDSS0150" xmlns:cbl="http:///com.bphx.logicminer.model.diagrams.cobol.ecore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<packageNode id="IDSS0150_NODE" name="Main">
		<children xsi:type="cbl:SectionNode" id="SC_1F10103" deadCode="false" name="FIRST">
			<representations href="../../../cobol/IDSS0150.cbl.cobModel#SC_1F10103"/>
			<children xsi:type="cbl:ParagraphNode" id="P_1F10103" deadCode="false" name="PROGRAM_IDSS0150_FIRST_SENTENCES">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_1F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_2F10103" deadCode="false" name="A000-FASE-INIZIALE">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_2F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_3F10103" deadCode="false" name="A000-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_3F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_14F10103" deadCode="false" name="A001-CONTROLLA-INPUT">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_14F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_15F10103" deadCode="false" name="A001-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_15F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_4F10103" deadCode="false" name="A010-ESTRAI-TIMESTAMP-DB">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_4F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_5F10103" deadCode="false" name="A010-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_5F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_6F10103" deadCode="false" name="A011-ESTRAI-DATE-DB">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_6F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_7F10103" deadCode="false" name="A011-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_7F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_8F10103" deadCode="false" name="A050-CALCOLA-CPTZ">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_8F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_9F10103" deadCode="false" name="A050-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_9F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_20F10103" deadCode="false" name="A053-ESTRAI-DT-COMPETENZA">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_20F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_21F10103" deadCode="false" name="A053-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_21F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_22F10103" deadCode="false" name="A060-CTRL-DATA">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_22F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_23F10103" deadCode="false" name="A060-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_23F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_24F10103" deadCode="false" name="A070-GESTISCI-DIFF">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_24F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_25F10103" deadCode="false" name="A070-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_25F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_16F10103" deadCode="false" name="A100-CHECK-RETURN-CODE">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_16F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_17F10103" deadCode="false" name="A100-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_17F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_32F10103" deadCode="false" name="D000-TRATTA-DEBUG">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_32F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_33F10103" deadCode="false" name="D000-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_33F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_10F10103" deadCode="false" name="E000-ESTRAI-ID-TMPRY-DATA">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_10F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_11F10103" deadCode="false" name="E000-ESTRAI-ID-TMPRY-DATA-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_11F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_36F10103" deadCode="false" name="Y000-CALCOLA-DIFFERENZA">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_36F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_37F10103" deadCode="false" name="Y000-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_37F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_12F10103" deadCode="false" name="Z000-FASE-FINALE">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_12F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_13F10103" deadCode="false" name="Z000-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_13F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_28F10103" deadCode="false" name="Z100-SET-COLONNE-NULL">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_28F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_29F10103" deadCode="false" name="Z100-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_29F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_30F10103" deadCode="false" name="Z950-CONVERTI-X-TO-N">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_30F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_31F10103" deadCode="false" name="Z950-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_31F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_34F10103" deadCode="true" name="Z700-DT-N-TO-X">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_34F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_35F10103" deadCode="false" name="Z700-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_35F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_38F10103" deadCode="true" name="Z701-TS-N-TO-X">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_38F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_39F10103" deadCode="true" name="Z701-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_39F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_26F10103" deadCode="false" name="Z800-DT-X-TO-N">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_26F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_27F10103" deadCode="false" name="Z800-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_27F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_40F10103" deadCode="false" name="Z810-DT-X-TO-N-ISO">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_40F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_41F10103" deadCode="false" name="Z810-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_41F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_42F10103" deadCode="false" name="Z820-DT-X-TO-N-EUR">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_42F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_43F10103" deadCode="false" name="Z820-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_43F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_18F10103" deadCode="false" name="Z801-TS-X-TO-N">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_18F10103"/>
			</children>
			<children xsi:type="cbl:ParagraphNode" id="P_19F10103" deadCode="false" name="Z801-EX">
				<representations href="../../../cobol/IDSS0150.cbl.cobModel#P_19F10103"/>
			</children>
		</children>
		<children xsi:type="cbl:Db2DataNode" id="DB2_PARAM_COMP" name="PARAM_COMP" missing="true">
			<representations href="../../../../missing.xmi#"/>
		</children>
		<children xsi:type="cbl:CobolProgramNode" id="LCCS0004" name="LCCS0004">
			<representations href="../../../cobol/../importantStmts.cobModel#PR_1F10120"></representations>
		</children>
	</packageNode>
	<edges id="SC_1F10103P_1F10103" xsi:type="cbl:FallThroughEdge" sourceNode="SC_1F10103" targetNode="P_1F10103"/>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10103_I" deadCode="false" sourceNode="P_1F10103" targetNode="P_2F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_1F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_1F10103_O" deadCode="false" sourceNode="P_1F10103" targetNode="P_3F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_1F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10103_I" deadCode="false" sourceNode="P_1F10103" targetNode="P_4F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_3F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_3F10103_O" deadCode="false" sourceNode="P_1F10103" targetNode="P_5F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_3F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10103_I" deadCode="false" sourceNode="P_1F10103" targetNode="P_6F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_5F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_5F10103_O" deadCode="false" sourceNode="P_1F10103" targetNode="P_7F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_5F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10103_I" deadCode="false" sourceNode="P_1F10103" targetNode="P_8F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_7F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_7F10103_O" deadCode="false" sourceNode="P_1F10103" targetNode="P_9F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_7F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10103_I" deadCode="false" sourceNode="P_1F10103" targetNode="P_10F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_10F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_10F10103_O" deadCode="false" sourceNode="P_1F10103" targetNode="P_11F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_10F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10103_I" deadCode="false" sourceNode="P_1F10103" targetNode="P_12F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_14F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_14F10103_O" deadCode="false" sourceNode="P_1F10103" targetNode="P_13F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_14F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10103_I" deadCode="false" sourceNode="P_2F10103" targetNode="P_14F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_20F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_20F10103_O" deadCode="false" sourceNode="P_2F10103" targetNode="P_15F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_20F10103"/>
	</edges>
	<edges id="P_2F10103P_3F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_2F10103" targetNode="P_3F10103"/>
	<edges id="P_14F10103P_15F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_14F10103" targetNode="P_15F10103"/>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10103_I" deadCode="false" sourceNode="P_4F10103" targetNode="P_16F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_36F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_36F10103_O" deadCode="false" sourceNode="P_4F10103" targetNode="P_17F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_36F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10103_I" deadCode="false" sourceNode="P_4F10103" targetNode="P_18F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_39F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_39F10103_O" deadCode="false" sourceNode="P_4F10103" targetNode="P_19F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_39F10103"/>
	</edges>
	<edges id="P_4F10103P_5F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_4F10103" targetNode="P_5F10103"/>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10103_I" deadCode="false" sourceNode="P_6F10103" targetNode="P_16F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_45F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_45F10103_O" deadCode="false" sourceNode="P_6F10103" targetNode="P_17F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_45F10103"/>
	</edges>
	<edges id="P_6F10103P_7F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_6F10103" targetNode="P_7F10103"/>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10103_I" deadCode="false" sourceNode="P_8F10103" targetNode="P_20F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_50F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_50F10103_O" deadCode="false" sourceNode="P_8F10103" targetNode="P_21F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_50F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10103_I" deadCode="false" sourceNode="P_8F10103" targetNode="P_22F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_57F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_57F10103_O" deadCode="false" sourceNode="P_8F10103" targetNode="P_23F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_57F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10103_I" deadCode="false" sourceNode="P_8F10103" targetNode="P_24F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_60F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_60F10103_O" deadCode="false" sourceNode="P_8F10103" targetNode="P_25F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_60F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10103_I" deadCode="false" sourceNode="P_8F10103" targetNode="P_26F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_75F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_75F10103_O" deadCode="false" sourceNode="P_8F10103" targetNode="P_27F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_75F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10103_I" deadCode="false" sourceNode="P_8F10103" targetNode="P_24F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_77F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_77F10103_O" deadCode="false" sourceNode="P_8F10103" targetNode="P_25F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_77F10103"/>
	</edges>
	<edges id="P_8F10103P_9F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_8F10103" targetNode="P_9F10103"/>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10103_I" deadCode="false" sourceNode="P_20F10103" targetNode="P_28F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_95F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_95F10103_O" deadCode="false" sourceNode="P_20F10103" targetNode="P_29F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_95F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10103_I" deadCode="false" sourceNode="P_20F10103" targetNode="P_30F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_96F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_96F10103_O" deadCode="false" sourceNode="P_20F10103" targetNode="P_31F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_96F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10103_I" deadCode="false" sourceNode="P_20F10103" targetNode="P_32F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_97F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_97F10103_O" deadCode="false" sourceNode="P_20F10103" targetNode="P_33F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_97F10103"/>
	</edges>
	<edges id="P_20F10103P_21F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_20F10103" targetNode="P_21F10103"/>
	<edges id="P_22F10103P_23F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_22F10103" targetNode="P_23F10103"/>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10103_I" deadCode="true" sourceNode="P_24F10103" targetNode="P_34F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_118F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_118F10103_O" deadCode="true" sourceNode="P_24F10103" targetNode="P_35F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_118F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10103_I" deadCode="false" sourceNode="P_24F10103" targetNode="P_36F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_119F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_119F10103_O" deadCode="false" sourceNode="P_24F10103" targetNode="P_37F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_119F10103"/>
	</edges>
	<edges id="P_24F10103P_25F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_24F10103" targetNode="P_25F10103"/>
	<edges id="P_16F10103P_17F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_16F10103" targetNode="P_17F10103"/>
	<edges id="P_32F10103P_33F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_32F10103" targetNode="P_33F10103"/>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10103_I" deadCode="false" sourceNode="P_10F10103" targetNode="P_16F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_142F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_142F10103_O" deadCode="false" sourceNode="P_10F10103" targetNode="P_17F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_142F10103"/>
	</edges>
	<edges id="P_10F10103P_11F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_10F10103" targetNode="P_11F10103"/>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10103_I" deadCode="false" sourceNode="P_36F10103" targetNode="P_16F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_148F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_148F10103_O" deadCode="false" sourceNode="P_36F10103" targetNode="P_17F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_148F10103"/>
	</edges>
	<edges id="P_36F10103P_37F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_36F10103" targetNode="P_37F10103"/>
	<edges id="P_12F10103P_13F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_12F10103" targetNode="P_13F10103"/>
	<edges id="P_28F10103P_29F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_28F10103" targetNode="P_29F10103"/>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10103_I" deadCode="false" sourceNode="P_30F10103" targetNode="P_26F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_165F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_165F10103_O" deadCode="false" sourceNode="P_30F10103" targetNode="P_27F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_165F10103"/>
	</edges>
	<edges id="P_30F10103P_31F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_30F10103" targetNode="P_31F10103"/>
	<edges id="P_34F10103P_35F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_34F10103" targetNode="P_35F10103"/>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10103_I" deadCode="false" sourceNode="P_26F10103" targetNode="P_40F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_187F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_187F10103_O" deadCode="false" sourceNode="P_26F10103" targetNode="P_41F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_187F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_188F10103_I" deadCode="false" sourceNode="P_26F10103" targetNode="P_42F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_188F10103"/>
	</edges>
	<edges xsi:type="cbl:PerformEdge" id="S_188F10103_O" deadCode="false" sourceNode="P_26F10103" targetNode="P_43F10103">
		<representations href="../../../cobol/IDSS0150.cbl.cobModel#S_188F10103"/>
	</edges>
	<edges id="P_26F10103P_27F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_26F10103" targetNode="P_27F10103"/>
	<edges id="P_40F10103P_41F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_40F10103" targetNode="P_41F10103"/>
	<edges id="P_42F10103P_43F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_42F10103" targetNode="P_43F10103"/>
	<edges id="P_18F10103P_19F10103" xsi:type="cbl:FallThroughEdge" sourceNode="P_18F10103" targetNode="P_19F10103"/>
	<edges xsi:type="cbl:DataEdge" id="S_92F10103_POS1" deadCode="false" targetNode="P_20F10103" sourceNode="DB2_PARAM_COMP">
		<representations href="../../../cobol/../importantStmts.cobModel#S_92F10103"></representations>
	</edges>
	<edges xsi:type="cbl:CallEdge" id="S_111F10103" deadCode="false" name="Dynamic LCCS0004" sourceNode="P_22F10103" targetNode="LCCS0004">
		<representations href="../../../cobol/../importantStmts.cobModel#S_111F10103"></representations>
	</edges>
</Package>

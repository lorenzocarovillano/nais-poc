//****************************************************************
//*       DB2GO   - PROC TO SUBMIT DB2 BATCH
//****************************************************************
//DB2GO    PROC
//*
//DB2GO    EXEC PGM=IKJEFT01,DYNAMNBR=20
//SYSTSPRT  DD SYSOUT=*
//SYSPRINT  DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=*
//SYSOUT    DD SYSOUT=*
//ABENDAID  DD SYSOUT=*
//SEQGUIDE  DD DUMMY
//SYSIN     DD DUMMY
//SYSTSIN   DD DUMMY
CREATE TABLE TIT_RAT (
		ID_TIT_RAT DECIMAL(9 , 0) NOT NULL, 
		ID_OGG DECIMAL(9 , 0) NOT NULL, 
		TP_OGG CHAR(2) FOR SBCS DATA NOT NULL, 
		ID_MOVI_CRZ DECIMAL(9 , 0) NOT NULL, 
		ID_MOVI_CHIU DECIMAL(9 , 0) WITH DEFAULT NULL, 
		DT_INI_EFF DATE NOT NULL, 
		DT_END_EFF DATE NOT NULL, 
		COD_COMP_ANIA DECIMAL(5 , 0) NOT NULL, 
		TP_TIT CHAR(2) FOR SBCS DATA NOT NULL, 
		PROG_TIT DECIMAL(5 , 0) WITH DEFAULT NULL, 
		TP_PRE_TIT CHAR(2) FOR SBCS DATA NOT NULL, 
		TP_STAT_TIT CHAR(2) FOR SBCS DATA NOT NULL, 
		DT_INI_COP DATE WITH DEFAULT NULL, 
		DT_END_COP DATE WITH DEFAULT NULL, 
		IMP_PAG DECIMAL(15 , 3) WITH DEFAULT NULL, 
		FL_SOLL CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		FRAZ DECIMAL(5 , 0) WITH DEFAULT NULL, 
		DT_APPLZ_MORA DATE WITH DEFAULT NULL, 
		FL_MORA CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		ID_RAPP_RETE DECIMAL(9 , 0) WITH DEFAULT NULL, 
		ID_RAPP_ANA DECIMAL(9 , 0) WITH DEFAULT NULL, 
		COD_DVS CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		DT_EMIS_TIT DATE NOT NULL, 
		DT_ESI_TIT DATE WITH DEFAULT NULL, 
		TOT_PRE_NET DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_INTR_FRAZ DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_INTR_MORA DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_INTR_PREST DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_INTR_RETDT DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_INTR_RIAT DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_DIR DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_SPE_MED DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_SPE_AGE DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_TAX DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_SOPR_SAN DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_SOPR_TEC DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_SOPR_SPO DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_SOPR_PROF DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_SOPR_ALT DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_PRE_TOT DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_PRE_PP_IAS DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_CAR_IAS DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_PRE_SOLO_RSH DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_PROV_ACQ_1AA DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_PROV_ACQ_2AA DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_PROV_RICOR DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_PROV_INC DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_PROV_DA_REC DECIMAL(15 , 3) WITH DEFAULT NULL, 
		IMP_AZ DECIMAL(15 , 3) WITH DEFAULT NULL, 
		IMP_ADER DECIMAL(15 , 3) WITH DEFAULT NULL, 
		IMP_TFR DECIMAL(15 , 3) WITH DEFAULT NULL, 
		IMP_VOLO DECIMAL(15 , 3) WITH DEFAULT NULL, 
		FL_VLDT_TIT CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		TOT_CAR_ACQ DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_CAR_GEST DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_CAR_INC DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_MANFEE_ANTIC DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_MANFEE_RICOR DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_MANFEE_REC DECIMAL(15 , 3) WITH DEFAULT NULL, 
		DS_RIGA DECIMAL(10 , 0) NOT NULL WITH DEFAULT, 
		DS_OPER_SQL CHAR(1) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		DS_VER DECIMAL(9 , 0) NOT NULL WITH DEFAULT, 
		DS_TS_INI_CPTZ DECIMAL(18 , 0) NOT NULL WITH DEFAULT, 
		DS_TS_END_CPTZ DECIMAL(18 , 0) NOT NULL WITH DEFAULT, 
		DS_UTENTE CHAR(20) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		DS_STATO_ELAB CHAR(1) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		IMP_TRASFE DECIMAL(15 , 3) WITH DEFAULT NULL, 
		IMP_TFR_STRC DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_ACQ_EXP DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_REMUN_ASS DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TOT_COMMIS_INTER DECIMAL(15 , 3) WITH DEFAULT NULL, 
		FL_INC_AUTOGEN CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		TOT_CNBT_ANTIRAC DECIMAL(15 , 3) WITH DEFAULT NULL, 
		CONSTRAINT TDR_PK PRIMARY KEY
		(DS_RIGA)
	)
	AUDIT NONE
	DATA CAPTURE NONE 
	CCSID EBCDIC;
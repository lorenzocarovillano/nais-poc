CREATE TABLE CTRL_AUT_OPER (
		ID_CTRL_AUT_OPER DECIMAL(9 , 0) NOT NULL, 
		COD_COMPAGNIA_ANIA DECIMAL(5 , 0) NOT NULL, 
		TP_MOVI DECIMAL(5 , 0) WITH DEFAULT NULL, 
		COD_ERRORE INTEGER NOT NULL, 
		KEY_AUT_OPER1 CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		KEY_AUT_OPER2 CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		KEY_AUT_OPER3 CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		KEY_AUT_OPER4 CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		KEY_AUT_OPER5 CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_LIV_AUT DECIMAL(5 , 0) NOT NULL, 
		TP_MOT_DEROGA CHAR(2) FOR SBCS DATA NOT NULL, 
		MODULO_VERIFICA CHAR(8) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_COND CHAR(50) FOR SBCS DATA WITH DEFAULT NULL, 
		PROG_COND DECIMAL(3 , 0) WITH DEFAULT NULL, 
		RISULT_COND CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		PARAM_AUT_OPER VARCHAR(2000) FOR SBCS DATA WITH DEFAULT NULL, 
		STATO_ATTIVAZIONE CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		CONSTRAINT CAO_PK PRIMARY KEY
		(ID_CTRL_AUT_OPER)
	)
	AUDIT NONE
	DATA CAPTURE NONE 
	CCSID EBCDIC;
CREATE TABLE QUEST (
		ID_QUEST DECIMAL(9 , 0) NOT NULL, 
		ID_RAPP_ANA DECIMAL(9 , 0) NOT NULL, 
		ID_MOVI_CRZ DECIMAL(9 , 0) NOT NULL, 
		ID_MOVI_CHIU DECIMAL(9 , 0) WITH DEFAULT NULL, 
		DT_INI_EFF DATE NOT NULL, 
		DT_END_EFF DATE NOT NULL, 
		COD_COMP_ANIA DECIMAL(5 , 0) NOT NULL, 
		COD_QUEST CHAR(20) FOR SBCS DATA NOT NULL, 
		TP_QUEST CHAR(20) FOR SBCS DATA NOT NULL, 
		FL_VST_MED CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_STAT_BUON_SAL CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		DS_RIGA DECIMAL(10 , 0) NOT NULL WITH DEFAULT, 
		DS_OPER_SQL CHAR(1) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		DS_VER DECIMAL(9 , 0) NOT NULL WITH DEFAULT, 
		DS_TS_INI_CPTZ DECIMAL(18 , 0) NOT NULL WITH DEFAULT, 
		DS_TS_END_CPTZ DECIMAL(18 , 0) NOT NULL WITH DEFAULT, 
		DS_UTENTE CHAR(20) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		DS_STATO_ELAB CHAR(1) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		TP_ADEGZ CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		CONSTRAINT QUE_PK PRIMARY KEY
		(DS_RIGA)
	)
	AUDIT NONE
	DATA CAPTURE NONE 
	CCSID EBCDIC;
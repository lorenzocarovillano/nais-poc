CREATE TABLE BILA_VAR_CALC_T (
		ID_BILA_VAR_CALC_T DECIMAL(9 , 0) NOT NULL, 
		COD_COMP_ANIA DECIMAL(5 , 0) NOT NULL, 
		ID_BILA_TRCH_ESTR DECIMAL(9 , 0) NOT NULL, 
		ID_RICH_ESTRAZ_MAS DECIMAL(9 , 0) NOT NULL, 
		ID_RICH_ESTRAZ_AGG DECIMAL(9 , 0) WITH DEFAULT NULL, 
		DT_RIS DATE WITH DEFAULT NULL, 
		ID_POLI DECIMAL(9 , 0) NOT NULL, 
		ID_ADES DECIMAL(9 , 0) NOT NULL, 
		ID_TRCH_DI_GAR DECIMAL(9 , 0) NOT NULL, 
		PROG_SCHEDA_VALOR DECIMAL(9 , 0) WITH DEFAULT NULL, 
		DT_INI_VLDT_TARI DATE NOT NULL, 
		TP_RGM_FISC CHAR(2) FOR SBCS DATA NOT NULL, 
		DT_INI_VLDT_PROD DATE NOT NULL, 
		DT_DECOR_TRCH DATE NOT NULL, 
		COD_VAR CHAR(30) FOR SBCS DATA NOT NULL, 
		TP_D CHAR(1) FOR SBCS DATA NOT NULL, 
		VAL_IMP DECIMAL(18 , 7) WITH DEFAULT NULL, 
		VAL_PC DECIMAL(14 , 9) WITH DEFAULT NULL, 
		VAL_STRINGA CHAR(60) FOR SBCS DATA WITH DEFAULT NULL, 
		DS_OPER_SQL CHAR(1) FOR SBCS DATA NOT NULL, 
		DS_VER DECIMAL(9 , 0) NOT NULL, 
		DS_TS_CPTZ DECIMAL(18 , 0) NOT NULL, 
		DS_UTENTE CHAR(20) FOR SBCS DATA NOT NULL, 
		DS_STATO_ELAB CHAR(1) FOR SBCS DATA NOT NULL, 
		AREA_D_VALOR_VAR VARCHAR(4000) FOR SBCS DATA WITH DEFAULT NULL
	)
	AUDIT NONE
	DATA CAPTURE NONE 
	CCSID EBCDIC;
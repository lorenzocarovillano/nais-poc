CREATE TABLE QUEST_ADEG_VER (
		ID_QUEST_ADEG_VER DECIMAL(9 , 0) NOT NULL, 
		COD_COMP_ANIA DECIMAL(5 , 0) NOT NULL, 
		ID_MOVI_CRZ DECIMAL(9 , 0) NOT NULL, 
		ID_RAPP_ANA DECIMAL(9 , 0) WITH DEFAULT NULL, 
		ID_POLI DECIMAL(9 , 0) NOT NULL, 
		NATURA_OPRZ CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		ORGN_FND CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_QLFC_PROF CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_NAZ_QLFC_PROF CHAR(4) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_PRV_QLFC_PROF CHAR(4) FOR SBCS DATA WITH DEFAULT NULL, 
		FNT_REDD CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		REDD_FATT_ANNU DECIMAL(15 , 3) WITH DEFAULT NULL, 
		COD_ATECO CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		VALUT_COLL CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		DS_OPER_SQL CHAR(1) FOR SBCS DATA NOT NULL, 
		DS_VER DECIMAL(9 , 0) NOT NULL, 
		DS_TS_CPTZ DECIMAL(18 , 0) NOT NULL, 
		DS_UTENTE CHAR(20) FOR SBCS DATA NOT NULL, 
		DS_STATO_ELAB CHAR(1) FOR SBCS DATA NOT NULL, 
		LUOGO_COSTITUZIONE VARCHAR(250) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_MOVI DECIMAL(5 , 0) WITH DEFAULT 0, 
		FL_RAG_RAPP CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_PRSZ_TIT_EFF CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_MOT_RISC CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_PNT_VND CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_ADEG_VER CHAR(2) FOR SBCS DATA WITH DEFAULT '00', 
		TP_RELA_ESEC CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_SCO_FIN_RAPP CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_PRSZ_3O_PAGAT CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		AREA_GEO_PROV_FND CHAR(4) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_DEST_FND CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_PAESE_RESID_AUT CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_PAESE_CIT_AUT CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_PAESE_NAZ_AUT CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_PROF_PREC CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_AUT_PEP CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_IMP_CAR_PUB CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_LIS_TERR_SORV CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_SIT_FIN_PAT CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_SIT_FIN_PAT_CON CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		IMP_TOT_AFF_UTIL DECIMAL(15 , 3) WITH DEFAULT NULL, 
		IMP_TOT_FIN_UTIL DECIMAL(15 , 3) WITH DEFAULT NULL, 
		IMP_TOT_AFF_ACC DECIMAL(15 , 3) WITH DEFAULT NULL, 
		IMP_TOT_FIN_ACC DECIMAL(15 , 3) WITH DEFAULT NULL, 
		TP_FRM_GIUR_SAV CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		REG_COLL_POLI CHAR(4) FOR SBCS DATA WITH DEFAULT NULL, 
		NUM_TEL CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		NUM_DIP DECIMAL(5 , 0) WITH DEFAULT NULL, 
		TP_SIT_FAM_CONV CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_PROF_CON CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_ES_PROC_PEN CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_COND_CLIENTE CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_SAE CHAR(4) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_OPER_ESTERO CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		STAT_OPER_ESTERO CHAR(4) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_PRV_SVOL_ATT CHAR(4) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_STAT_SVOL_ATT CHAR(4) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_SOC CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_IND_SOC_QUOT CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_SIT_GIUR CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		PC_QUO_DET_TIT_EFF DECIMAL(6 , 3) WITH DEFAULT NULL, 
		TP_PRFL_RSH_PEP CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_PEP CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_NOT_PREG CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		DT_INI_FNT_REDD DATE WITH DEFAULT NULL, 
		FNT_REDD_2 CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		DT_INI_FNT_REDD_2 DATE WITH DEFAULT NULL, 
		FNT_REDD_3 CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		DT_INI_FNT_REDD_3 DATE WITH DEFAULT NULL, 
		MOT_ASS_TIT_EFF VARCHAR(250) FOR SBCS DATA WITH DEFAULT NULL, 
		FIN_COSTITUZIONE VARCHAR(250) FOR SBCS DATA WITH DEFAULT NULL, 
		DESC_IMP_CAR_PUB VARCHAR(250) FOR SBCS DATA WITH DEFAULT NULL, 
		DESC_SCO_FIN_RAPP VARCHAR(250) FOR SBCS DATA WITH DEFAULT NULL, 
		DESC_PROC_PNL VARCHAR(250) FOR SBCS DATA WITH DEFAULT NULL, 
		DESC_NOT_PREG VARCHAR(250) FOR SBCS DATA WITH DEFAULT NULL, 
		ID_ASSICURATI DECIMAL(9 , 0) WITH DEFAULT NULL, 
		REDD_CON DECIMAL(15 , 3) WITH DEFAULT NULL, 
		DESC_LIB_MOT_RISC VARCHAR(250) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_MOT_ASS_TIT_EFF CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_RAG_RAPP CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_CAN DECIMAL(5 , 0) WITH DEFAULT NULL, 
		TP_FIN_COST CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		NAZ_DEST_FND CHAR(4) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_AU_FATCA_AEOI CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_CAR_FIN_GIUR CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_CAR_FIN_GIUR_AT CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_CAR_FIN_GIUR_PA CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_ISTITUZ_FIN CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_ORI_FND_TIT_EFF CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		PC_ESP_AG_PA_MSC DECIMAL(6 , 3) WITH DEFAULT NULL, 
		FL_PR_TR_USA CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_PR_TR_NO_USA CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		PC_RIP_PAT_AS_VITA DECIMAL(6 , 3) WITH DEFAULT NULL, 
		PC_RIP_PAT_IM DECIMAL(6 , 3) WITH DEFAULT NULL, 
		PC_RIP_PAT_SET_IM DECIMAL(6 , 3) WITH DEFAULT NULL, 
		TP_STATUS_AEOI CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_STATUS_FATCA CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_RAPP_PA_MSC CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_COMUN_SVOL_ATT CHAR(4) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_DT_1O_CON_CLI CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_MOD_EN_RELA_INT CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_REDD_ANNU_LRD CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_REDD_CON CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_OPER_SOC_FID CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_PA_ESP_MSC_1 CHAR(4) FOR SBCS DATA WITH DEFAULT NULL, 
		IMP_PA_ESP_MSC_1 DECIMAL(15 , 3) WITH DEFAULT NULL, 
		COD_PA_ESP_MSC_2 CHAR(4) FOR SBCS DATA WITH DEFAULT NULL, 
		IMP_PA_ESP_MSC_2 DECIMAL(15 , 3) WITH DEFAULT NULL, 
		COD_PA_ESP_MSC_3 CHAR(4) FOR SBCS DATA WITH DEFAULT NULL, 
		IMP_PA_ESP_MSC_3 DECIMAL(15 , 3) WITH DEFAULT NULL, 
		COD_PA_ESP_MSC_4 CHAR(4) FOR SBCS DATA WITH DEFAULT NULL, 
		IMP_PA_ESP_MSC_4 DECIMAL(15 , 3) WITH DEFAULT NULL, 
		COD_PA_ESP_MSC_5 CHAR(4) FOR SBCS DATA WITH DEFAULT NULL, 
		IMP_PA_ESP_MSC_5 DECIMAL(15 , 3) WITH DEFAULT NULL, 
		DESC_ORGN_FND VARCHAR(250) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_AUT_DUE_DIL CHAR(10) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_PR_QUEST_FATCA CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_PR_QUEST_AEOI CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_PR_QUEST_OFAC CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_PR_QUEST_KYC CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_PR_QUEST_MSCQ CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_NOT_PREG CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_PROC_PNL CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_IMP_CAR_PUB CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		OPRZ_SOSPETTE CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		ULT_FATT_ANNU DECIMAL(15 , 3) WITH DEFAULT NULL, 
		DESC_PEP VARCHAR(100) FOR SBCS DATA WITH DEFAULT NULL, 
		NUM_TEL_2 CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		IMP_AFI DECIMAL(15 , 3) WITH DEFAULT NULL, 
		FL_NEW_PRO CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_MOT_CAMBIO_CNTR CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		DESC_MOT_CAMBIO_CN VARCHAR(250) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_SOGG CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		CONSTRAINT P56_PK PRIMARY KEY
		(ID_QUEST_ADEG_VER)
	)
	AUDIT NONE
	DATA CAPTURE NONE 
	CCSID EBCDIC;
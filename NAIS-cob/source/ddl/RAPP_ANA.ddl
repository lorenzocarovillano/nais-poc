CREATE TABLE RAPP_ANA (
		ID_RAPP_ANA DECIMAL(9 , 0) NOT NULL, 
		ID_RAPP_ANA_COLLG DECIMAL(9 , 0) WITH DEFAULT NULL, 
		ID_OGG DECIMAL(9 , 0) NOT NULL, 
		TP_OGG CHAR(2) FOR SBCS DATA NOT NULL, 
		ID_MOVI_CRZ DECIMAL(9 , 0) NOT NULL, 
		ID_MOVI_CHIU DECIMAL(9 , 0) WITH DEFAULT NULL, 
		DT_INI_EFF DATE NOT NULL, 
		DT_END_EFF DATE NOT NULL, 
		COD_COMP_ANIA DECIMAL(5 , 0) NOT NULL, 
		COD_SOGG CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_RAPP_ANA CHAR(2) FOR SBCS DATA NOT NULL, 
		TP_PERS CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		SEX CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		DT_NASC DATE WITH DEFAULT NULL, 
		FL_ESTAS CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		INDIR_1 CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		INDIR_2 CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		INDIR_3 CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_UTLZ_INDIR_1 CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_UTLZ_INDIR_2 CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_UTLZ_INDIR_3 CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		ESTR_CNT_CORR_ACCR CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		ESTR_CNT_CORR_ADD CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		ESTR_DOCTO CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		PC_NEL_RAPP DECIMAL(6 , 3) WITH DEFAULT NULL, 
		TP_MEZ_PAG_ADD CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_MEZ_PAG_ACCR CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_MATR CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_ADEGZ CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_TST_RSH CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_AZ CHAR(30) FOR SBCS DATA WITH DEFAULT NULL, 
		IND_PRINC CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		DT_DELIBERA_CDA DATE WITH DEFAULT NULL, 
		DLG_AL_RISC CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		LEGALE_RAPPR_PRINC CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_LEGALE_RAPPR CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_IND_PRINC CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_STAT_RID CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		NOME_INT_RID VARCHAR(100) FOR SBCS DATA WITH DEFAULT NULL, 
		COGN_INT_RID VARCHAR(100) FOR SBCS DATA WITH DEFAULT NULL, 
		COGN_INT_TRATT VARCHAR(100) FOR SBCS DATA WITH DEFAULT NULL, 
		NOME_INT_TRATT VARCHAR(100) FOR SBCS DATA WITH DEFAULT NULL, 
		CF_INT_RID CHAR(16) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_COINC_DIP_CNTR CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_COINC_INT_CNTR CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		DT_DECES DATE WITH DEFAULT NULL, 
		FL_FUMATORE CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		DS_RIGA DECIMAL(10 , 0) NOT NULL WITH DEFAULT, 
		DS_OPER_SQL CHAR(1) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		DS_VER DECIMAL(9 , 0) NOT NULL WITH DEFAULT, 
		DS_TS_INI_CPTZ DECIMAL(18 , 0) NOT NULL WITH DEFAULT, 
		DS_TS_END_CPTZ DECIMAL(18 , 0) NOT NULL WITH DEFAULT, 
		DS_UTENTE CHAR(20) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		DS_STATO_ELAB CHAR(1) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		FL_LAV_DIP CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_VARZ_PAGAT CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_RID CHAR(11) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_CAUS_RID CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		IND_MASSA_CORP CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		CAT_RSH_PROF CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		CONSTRAINT RAN_PK PRIMARY KEY
		(DS_RIGA)
	)
	AUDIT NONE
	DATA CAPTURE NONE 
	CCSID EBCDIC;
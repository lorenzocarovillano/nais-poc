CREATE TABLE MOVI_FINRIO (
		ID_MOVI_FINRIO DECIMAL(9 , 0) NOT NULL, 
		ID_ADES DECIMAL(9 , 0) WITH DEFAULT NULL, 
		ID_LIQ DECIMAL(9 , 0) WITH DEFAULT NULL, 
		ID_TIT_CONT DECIMAL(9 , 0) WITH DEFAULT NULL, 
		ID_MOVI_CRZ DECIMAL(9 , 0) NOT NULL, 
		ID_MOVI_CHIU DECIMAL(9 , 0) WITH DEFAULT NULL, 
		DT_INI_EFF DATE NOT NULL, 
		DT_END_EFF DATE NOT NULL, 
		COD_COMP_ANIA DECIMAL(5 , 0) NOT NULL, 
		TP_MOVI_FINRIO CHAR(2) FOR SBCS DATA NOT NULL, 
		DT_EFF_MOVI_FINRIO DATE WITH DEFAULT NULL, 
		DT_ELAB DATE WITH DEFAULT NULL, 
		DT_RICH_MOVI DATE WITH DEFAULT NULL, 
		COS_OPRZ DECIMAL(15 , 3) WITH DEFAULT NULL, 
		STAT_MOVI CHAR(2) FOR SBCS DATA NOT NULL, 
		DS_RIGA DECIMAL(10 , 0) NOT NULL WITH DEFAULT, 
		DS_OPER_SQL CHAR(1) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		DS_VER DECIMAL(9 , 0) NOT NULL WITH DEFAULT, 
		DS_TS_INI_CPTZ DECIMAL(18 , 0) NOT NULL WITH DEFAULT, 
		DS_TS_END_CPTZ DECIMAL(18 , 0) NOT NULL WITH DEFAULT, 
		DS_UTENTE CHAR(20) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		DS_STATO_ELAB CHAR(1) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		TP_CAUS_RETTIFICA CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		CONSTRAINT MFZ_PK PRIMARY KEY
		(DS_RIGA)
	)
	AUDIT NONE
	DATA CAPTURE NONE 
	CCSID EBCDIC;
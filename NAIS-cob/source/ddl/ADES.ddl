CREATE TABLE ADES (
		ID_ADES DECIMAL(9 , 0) NOT NULL, 
		ID_POLI DECIMAL(9 , 0) NOT NULL, 
		ID_MOVI_CRZ DECIMAL(9 , 0) NOT NULL, 
		ID_MOVI_CHIU DECIMAL(9 , 0) WITH DEFAULT NULL, 
		DT_INI_EFF DATE NOT NULL, 
		DT_END_EFF DATE NOT NULL, 
		IB_PREV CHAR(40) FOR SBCS DATA WITH DEFAULT NULL, 
		IB_OGG CHAR(40) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_COMP_ANIA DECIMAL(5 , 0) NOT NULL, 
		DT_DECOR DATE WITH DEFAULT NULL, 
		DT_SCAD DATE WITH DEFAULT NULL, 
		ETA_A_SCAD DECIMAL(5 , 0) WITH DEFAULT NULL, 
		DUR_AA DECIMAL(5 , 0) WITH DEFAULT NULL, 
		DUR_MM DECIMAL(5 , 0) WITH DEFAULT NULL, 
		DUR_GG DECIMAL(5 , 0) WITH DEFAULT NULL, 
		TP_RGM_FISC CHAR(2) FOR SBCS DATA NOT NULL, 
		TP_RIAT CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_MOD_PAG_TIT CHAR(2) FOR SBCS DATA NOT NULL, 
		TP_IAS CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		DT_VARZ_TP_IAS DATE WITH DEFAULT NULL, 
		PRE_NET_IND DECIMAL(15 , 3) WITH DEFAULT NULL, 
		PRE_LRD_IND DECIMAL(15 , 3) WITH DEFAULT NULL, 
		RAT_LRD_IND DECIMAL(15 , 3) WITH DEFAULT NULL, 
		PRSTZ_INI_IND DECIMAL(15 , 3) WITH DEFAULT NULL, 
		FL_COINC_ASSTO CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		IB_DFLT CHAR(40) FOR SBCS DATA WITH DEFAULT NULL, 
		MOD_CALC CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_FNT_CNBTVA CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		IMP_AZ DECIMAL(15 , 3) WITH DEFAULT NULL, 
		IMP_ADER DECIMAL(15 , 3) WITH DEFAULT NULL, 
		IMP_TFR DECIMAL(15 , 3) WITH DEFAULT NULL, 
		IMP_VOLO DECIMAL(15 , 3) WITH DEFAULT NULL, 
		PC_AZ DECIMAL(6 , 3) WITH DEFAULT NULL, 
		PC_ADER DECIMAL(6 , 3) WITH DEFAULT NULL, 
		PC_TFR DECIMAL(6 , 3) WITH DEFAULT NULL, 
		PC_VOLO DECIMAL(6 , 3) WITH DEFAULT NULL, 
		DT_NOVA_RGM_FISC DATE WITH DEFAULT NULL, 
		FL_ATTIV CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		IMP_REC_RIT_VIS DECIMAL(15 , 3) WITH DEFAULT NULL, 
		IMP_REC_RIT_ACC DECIMAL(15 , 3) WITH DEFAULT NULL, 
		FL_VARZ_STAT_TBGC CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_PROVZA_MIGRAZ CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		IMPB_VIS_DA_REC DECIMAL(15 , 3) WITH DEFAULT NULL, 
		DT_DECOR_PREST_BAN DATE WITH DEFAULT NULL, 
		DT_EFF_VARZ_STAT_T DATE WITH DEFAULT NULL, 
		DS_RIGA DECIMAL(10 , 0) NOT NULL WITH DEFAULT, 
		DS_OPER_SQL CHAR(1) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		DS_VER DECIMAL(9 , 0) NOT NULL WITH DEFAULT, 
		DS_TS_INI_CPTZ DECIMAL(18 , 0) NOT NULL WITH DEFAULT, 
		DS_TS_END_CPTZ DECIMAL(18 , 0) NOT NULL WITH DEFAULT, 
		DS_UTENTE CHAR(20) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		DS_STATO_ELAB CHAR(1) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		CUM_CNBT_CAP DECIMAL(15 , 3) WITH DEFAULT NULL, 
		IMP_GAR_CNBT DECIMAL(15 , 3) WITH DEFAULT NULL, 
		DT_ULT_CONS_CNBT DATE WITH DEFAULT NULL, 
		IDEN_ISC_FND CHAR(40) FOR SBCS DATA WITH DEFAULT NULL, 
		NUM_RAT_PIAN DECIMAL(12 , 5) WITH DEFAULT NULL, 
		DT_PRESC DATE WITH DEFAULT NULL, 
		CONCS_PREST CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		CONSTRAINT ADE_PK PRIMARY KEY
		(DS_RIGA)
	)
	AUDIT NONE
	DATA CAPTURE NONE 
	CCSID EBCDIC;
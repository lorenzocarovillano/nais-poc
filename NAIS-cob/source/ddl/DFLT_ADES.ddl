CREATE TABLE DFLT_ADES (
		ID_DFLT_ADES DECIMAL(9 , 0) NOT NULL, 
		ID_POLI DECIMAL(9 , 0) NOT NULL, 
		IB_POLI CHAR(40) FOR SBCS DATA WITH DEFAULT NULL, 
		IB_DFLT CHAR(40) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_GAR_1 CHAR(12) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_GAR_2 CHAR(12) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_GAR_3 CHAR(12) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_GAR_4 CHAR(12) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_GAR_5 CHAR(12) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_GAR_6 CHAR(12) FOR SBCS DATA WITH DEFAULT NULL, 
		DT_DECOR_DFLT DATE WITH DEFAULT NULL, 
		ETA_SCAD_MASC_DFLT DECIMAL(5 , 0) WITH DEFAULT NULL, 
		ETA_SCAD_FEMM_DFLT DECIMAL(5 , 0) WITH DEFAULT NULL, 
		DUR_AA_ADES_DFLT DECIMAL(5 , 0) WITH DEFAULT NULL, 
		DUR_MM_ADES_DFLT DECIMAL(5 , 0) WITH DEFAULT NULL, 
		DUR_GG_ADES_DFLT DECIMAL(5 , 0) WITH DEFAULT NULL, 
		DT_SCAD_ADES_DFLT DATE WITH DEFAULT NULL, 
		FRAZ_DFLT DECIMAL(5 , 0) WITH DEFAULT NULL, 
		PC_PROV_INC_DFLT DECIMAL(6 , 3) WITH DEFAULT NULL, 
		IMP_PROV_INC_DFLT DECIMAL(15 , 3) WITH DEFAULT NULL, 
		IMP_AZ DECIMAL(15 , 3) WITH DEFAULT NULL, 
		IMP_ADER DECIMAL(15 , 3) WITH DEFAULT NULL, 
		IMP_TFR DECIMAL(15 , 3) WITH DEFAULT NULL, 
		IMP_VOLO DECIMAL(15 , 3) WITH DEFAULT NULL, 
		PC_AZ DECIMAL(6 , 3) WITH DEFAULT NULL, 
		PC_ADER DECIMAL(6 , 3) WITH DEFAULT NULL, 
		PC_TFR DECIMAL(6 , 3) WITH DEFAULT NULL, 
		PC_VOLO DECIMAL(6 , 3) WITH DEFAULT NULL, 
		TP_FNT_CNBTVA CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		IMP_PRE_DFLT DECIMAL(15 , 3) WITH DEFAULT NULL, 
		COD_COMP_ANIA DECIMAL(5 , 0) NOT NULL, 
		TP_PRE CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		DS_OPER_SQL CHAR(1) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		DS_VER DECIMAL(9 , 0) NOT NULL WITH DEFAULT, 
		DS_TS_CPTZ DECIMAL(18 , 0) NOT NULL WITH DEFAULT, 
		DS_UTENTE CHAR(20) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		DS_STATO_ELAB CHAR(1) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		CONSTRAINT DAD_PK PRIMARY KEY
		(ID_DFLT_ADES)
	)
	AUDIT NONE
	DATA CAPTURE NONE 
	CCSID EBCDIC;
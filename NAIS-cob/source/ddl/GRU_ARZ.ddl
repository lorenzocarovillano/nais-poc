CREATE TABLE GRU_ARZ (
		COD_GRU_ARZ DECIMAL(10 , 0) NOT NULL, 
		COD_COMP_ANIA DECIMAL(5 , 0) NOT NULL, 
		COD_LIV_OGZ DECIMAL(5 , 0) NOT NULL, 
		DSC_GRU_ARZ VARCHAR(255) FOR SBCS DATA NOT NULL, 
		DAT_INI_GRU_ARZ DATE NOT NULL, 
		DAT_FINE_GRU_ARZ DATE NOT NULL, 
		DEN_RESP_GRU_ARZ VARCHAR(255) FOR SBCS DATA NOT NULL, 
		IND_TLM_RESP VARCHAR(255) FOR SBCS DATA NOT NULL, 
		COD_UTE_INS CHAR(8) FOR SBCS DATA NOT NULL, 
		TMST_INS_RIG TIMESTAMP NOT NULL, 
		COD_UTE_AGR CHAR(8) FOR SBCS DATA NOT NULL, 
		TMST_AGR_RIG TIMESTAMP NOT NULL, 
		CONSTRAINT GRU_PK PRIMARY KEY
		(COD_GRU_ARZ)
	)
	AUDIT NONE
	DATA CAPTURE NONE 
	CCSID EBCDIC;
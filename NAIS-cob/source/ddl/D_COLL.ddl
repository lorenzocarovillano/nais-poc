CREATE TABLE D_COLL (
		ID_D_COLL DECIMAL(9 , 0) NOT NULL, 
		ID_POLI DECIMAL(9 , 0) NOT NULL, 
		ID_MOVI_CRZ DECIMAL(9 , 0) NOT NULL, 
		ID_MOVI_CHIU DECIMAL(9 , 0) WITH DEFAULT NULL, 
		DT_INI_EFF DATE NOT NULL, 
		DT_END_EFF DATE NOT NULL, 
		COD_COMP_ANIA DECIMAL(5 , 0) NOT NULL, 
		IMP_ARROT_PRE DECIMAL(15 , 3) WITH DEFAULT NULL, 
		PC_SCON DECIMAL(6 , 3) WITH DEFAULT NULL, 
		FL_ADES_SING CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_IMP CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_RICL_PRE_DA_CPT CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_ADES CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		DT_ULT_RINN_TAC DATE WITH DEFAULT NULL, 
		IMP_SCON DECIMAL(15 , 3) WITH DEFAULT NULL, 
		FRAZ_DFLT DECIMAL(5 , 0) WITH DEFAULT NULL, 
		ETA_SCAD_MASC_DFLT DECIMAL(5 , 0) WITH DEFAULT NULL, 
		ETA_SCAD_FEMM_DFLT DECIMAL(5 , 0) WITH DEFAULT NULL, 
		TP_DFLT_DUR CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		DUR_AA_ADES_DFLT DECIMAL(5 , 0) WITH DEFAULT NULL, 
		DUR_MM_ADES_DFLT DECIMAL(5 , 0) WITH DEFAULT NULL, 
		DUR_GG_ADES_DFLT DECIMAL(5 , 0) WITH DEFAULT NULL, 
		DT_SCAD_ADES_DFLT DATE WITH DEFAULT NULL, 
		COD_FND_DFLT CHAR(12) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_DUR CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_CALC_DUR CHAR(12) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_NO_ADERENTI CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_DISTINTA_CNBTVA CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_CNBT_AUTES CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_QTZ_POST_EMIS CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		FL_COMNZ_FND_IS CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		DS_RIGA DECIMAL(10 , 0) NOT NULL WITH DEFAULT, 
		DS_OPER_SQL CHAR(1) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		DS_VER DECIMAL(9 , 0) NOT NULL WITH DEFAULT, 
		DS_TS_INI_CPTZ DECIMAL(18 , 0) NOT NULL WITH DEFAULT, 
		DS_TS_END_CPTZ DECIMAL(18 , 0) NOT NULL WITH DEFAULT, 
		DS_UTENTE CHAR(20) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		DS_STATO_ELAB CHAR(1) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		CONSTRAINT DCO_PK PRIMARY KEY
		(DS_RIGA)
	)
	AUDIT NONE
	DATA CAPTURE NONE 
	CCSID EBCDIC;
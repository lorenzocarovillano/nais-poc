CREATE TABLE RICH (
		ID_RICH DECIMAL(9 , 0) NOT NULL, 
		COD_COMP_ANIA DECIMAL(5 , 0) NOT NULL, 
		TP_RICH CHAR(2) FOR SBCS DATA NOT NULL, 
		COD_MACROFUNCT CHAR(2) FOR SBCS DATA NOT NULL, 
		TP_MOVI DECIMAL(5 , 0) NOT NULL, 
		IB_RICH CHAR(40) FOR SBCS DATA WITH DEFAULT NULL, 
		DT_EFF DATE NOT NULL, 
		DT_RGSTRZ_RICH DATE NOT NULL, 
		DT_PERV_RICH DATE NOT NULL, 
		DT_ESEC_RICH DATE NOT NULL, 
		TS_EFF_ESEC_RICH DECIMAL(18 , 0) WITH DEFAULT NULL, 
		ID_OGG DECIMAL(9 , 0) WITH DEFAULT NULL, 
		TP_OGG CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		IB_POLI CHAR(40) FOR SBCS DATA WITH DEFAULT NULL, 
		IB_ADES CHAR(40) FOR SBCS DATA WITH DEFAULT NULL, 
		IB_GAR CHAR(40) FOR SBCS DATA WITH DEFAULT NULL, 
		IB_TRCH_DI_GAR CHAR(40) FOR SBCS DATA WITH DEFAULT NULL, 
		ID_BATCH DECIMAL(9 , 0) WITH DEFAULT NULL, 
		ID_JOB DECIMAL(9 , 0) WITH DEFAULT NULL, 
		FL_SIMULAZIONE CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		KEY_ORDINAMENTO VARCHAR(300) FOR SBCS DATA WITH DEFAULT NULL, 
		ID_RICH_COLLG DECIMAL(9 , 0) WITH DEFAULT NULL, 
		TP_RAMO_BILA CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_FRM_ASSVA CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_CALC_RIS CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		DS_OPER_SQL CHAR(1) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		DS_VER DECIMAL(9 , 0) NOT NULL WITH DEFAULT, 
		DS_TS_CPTZ DECIMAL(18 , 0) NOT NULL WITH DEFAULT, 
		DS_UTENTE CHAR(20) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		DS_STATO_ELAB CHAR(1) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		RAMO_BILA CHAR(12) FOR SBCS DATA WITH DEFAULT NULL, 
		CONSTRAINT RIC_PK PRIMARY KEY
		(ID_RICH)
	)
	AUDIT NONE
	DATA CAPTURE NONE 
	CCSID EBCDIC;
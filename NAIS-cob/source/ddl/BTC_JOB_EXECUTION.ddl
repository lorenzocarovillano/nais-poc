CREATE TABLE BTC_JOB_EXECUTION (
		ID_BATCH INTEGER NOT NULL, 
		ID_JOB INTEGER NOT NULL, 
		ID_EXECUTION INTEGER NOT NULL, 
		COD_ELAB_STATE CHAR(1) FOR SBCS DATA NOT NULL, 
		DATA VARCHAR(32000) FOR SBCS DATA WITH DEFAULT NULL, 
		FLAG_WARNINGS CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		DT_START TIMESTAMP NOT NULL, 
		DT_END TIMESTAMP WITH DEFAULT NULL, 
		USER_START CHAR(30) FOR SBCS DATA NOT NULL, 
		CONSTRAINT BJE_PK PRIMARY KEY
		(ID_BATCH, 
		 ID_JOB, 
		 ID_EXECUTION)
	)
	AUDIT NONE
	DATA CAPTURE NONE 
	CCSID EBCDIC;
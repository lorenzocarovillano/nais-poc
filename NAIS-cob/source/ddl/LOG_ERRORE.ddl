CREATE TABLE LOG_ERRORE (
		ID_LOG_ERRORE DECIMAL(9 , 0) NOT NULL, 
		PROG_LOG_ERRORE DECIMAL(5 , 0) NOT NULL, 
		ID_GRAVITA_ERRORE DECIMAL(9 , 0) NOT NULL, 
		DESC_ERRORE_ESTESA VARCHAR(200) FOR SBCS DATA NOT NULL, 
		COD_MAIN_BATCH CHAR(8) FOR SBCS DATA NOT NULL, 
		COD_SERVIZIO_BE CHAR(8) FOR SBCS DATA NOT NULL, 
		LABEL_ERR CHAR(30) FOR SBCS DATA WITH DEFAULT NULL, 
		OPER_TABELLA CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		NOME_TABELLA CHAR(18) FOR SBCS DATA WITH DEFAULT NULL, 
		STATUS_TABELLA CHAR(10) FOR SBCS DATA WITH DEFAULT NULL, 
		KEY_TABELLA VARCHAR(100) FOR SBCS DATA WITH DEFAULT NULL, 
		TIMESTAMP_REG DECIMAL(18 , 0) NOT NULL, 
		TIPO_OGGETTO DECIMAL(2 , 0) WITH DEFAULT NULL, 
		IB_OGGETTO CHAR(20) FOR SBCS DATA WITH DEFAULT NULL, 
		CONSTRAINT LOR_PK PRIMARY KEY
		(ID_LOG_ERRORE, 
		 PROG_LOG_ERRORE)
	)
	AUDIT NONE
	DATA CAPTURE NONE 
	CCSID EBCDIC;
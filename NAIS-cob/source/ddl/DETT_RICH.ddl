CREATE TABLE DETT_RICH (
		ID_RICH DECIMAL(9 , 0) NOT NULL, 
		PROG_DETT_RICH DECIMAL(5 , 0) NOT NULL, 
		COD_COMP_ANIA DECIMAL(5 , 0) NOT NULL, 
		TP_AREA_D_RICH CHAR(3) FOR SBCS DATA WITH DEFAULT NULL, 
		AREA_D_RICH VARCHAR(10000) FOR SBCS DATA WITH DEFAULT NULL, 
		DS_OPER_SQL CHAR(1) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		DS_VER DECIMAL(9 , 0) NOT NULL WITH DEFAULT, 
		DS_TS_CPTZ DECIMAL(18 , 0) NOT NULL WITH DEFAULT, 
		DS_UTENTE CHAR(20) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		DS_STATO_ELAB CHAR(1) FOR SBCS DATA NOT NULL WITH DEFAULT, 
		CONSTRAINT DER_PK PRIMARY KEY
		(ID_RICH, 
		 PROG_DETT_RICH)
	)
	AUDIT NONE
	DATA CAPTURE NONE 
	CCSID EBCDIC;
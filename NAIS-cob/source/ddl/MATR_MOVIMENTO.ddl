CREATE TABLE MATR_MOVIMENTO (
		ID_MATR_MOVIMENTO INTEGER NOT NULL, 
		COD_COMP_ANIA DECIMAL(5 , 0) NOT NULL, 
		TP_MOVI_PTF DECIMAL(5 , 0) NOT NULL, 
		TP_OGG CHAR(2) FOR SBCS DATA NOT NULL, 
		TP_FRM_ASSVA CHAR(2) FOR SBCS DATA WITH DEFAULT NULL, 
		TP_MOVI_ACT CHAR(5) FOR SBCS DATA NOT NULL, 
		AMMISSIBILITA_MOVI CHAR(1) FOR SBCS DATA WITH DEFAULT NULL, 
		SERVIZIO_CONTROLLO CHAR(8) FOR SBCS DATA WITH DEFAULT NULL, 
		COD_PROCESSO_WF CHAR(3) FOR SBCS DATA WITH DEFAULT NULL, 
		CONSTRAINT MMO_PK PRIMARY KEY
		(ID_MATR_MOVIMENTO)
	)
	AUDIT NONE
	DATA CAPTURE NONE 
	CCSID EBCDIC;
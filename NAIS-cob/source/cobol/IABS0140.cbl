       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IABS0140 IS INITIAL.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE
      * F A S E         : GESTIONE CONNECT / DISCONNECT
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.
       77  WK-NOME-TABELLA                PIC X(015).
       77  WK-LABEL                       PIC X(030).

       01 WK-DATABASE-NAME               PIC X(010).
       01 WK-USER                        PIC X(008).
       01 WK-PASSWORD                    PIC X(008).

       01 AREA-X-IABS0140.
         10 AREA0140-DATABASE-NAME     PIC X(010).
         10 AREA0140-USER              PIC X(008).
         10 AREA0140-PASSWORD          PIC X(008).

      * -- CAMPI OPERAZIONE
         10 AREA0140-OPERAZIONE        PIC X(015).
            88 AREA0140-CONNECT        VALUE 'CONNECT'.
            88 AREA0140-DISCONNECT     VALUE 'DISCONNECT'.
            88 AREA0140-CONNECT-RESET  VALUE 'CONNECT RESET'.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.


      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.



      *****************************************************************

       PROCEDURE DIVISION USING IDSV0003.

           PERFORM A000-INIZIO              THRU A000-EX.

           PERFORM B000-VALORIZZA-BUFFER    THRU B000-EX.

           PERFORM C000-CTRL-INPUT          THRU C000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              PERFORM A300-ELABORA          THRU A300-EX
           END-IF

           GOBACK.

      ******************************************************************
       A000-INIZIO.

           MOVE 'IABS0140'              TO   IDSV0003-COD-SERVIZIO-BE.
           MOVE SPACES                  TO   IDSV0003-NOME-TABELLA.

           MOVE '00'                    TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                  TO   IDSV0003-SQLCODE
                                             IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                  TO   IDSV0003-DESCRIZ-ERR-DB2.


       A000-EX.
           EXIT.

      ******************************************************************
       A100-VALORIZZA-RITORNO.

           MOVE SQLCODE               TO   IDSV0003-SQLCODE
           MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2.

       A100-EX.
           EXIT.

      ******************************************************************
       B000-VALORIZZA-BUFFER.

           MOVE IDSV0003-BUFFER-WHERE-COND TO AREA-X-IABS0140.

       B000-EX.
           EXIT.

      ******************************************************************
       A300-ELABORA.

           EVALUATE TRUE

              WHEN AREA0140-CONNECT
                 PERFORM A310-CONNECT       THRU A310-EX

              WHEN AREA0140-DISCONNECT
                 PERFORM A320-DISCONNECT    THRU A320-EX

              WHEN AREA0140-CONNECT-RESET
                 PERFORM A330-CONNECT-RESET THRU A330-EX

              WHEN OTHER
                 SET IDSV0003-INVALID-OPER  TO TRUE

           END-EVALUATE.

       A300-EX.
           EXIT.

      *----------------------------------------------------------------*
      * ESEGUI CONNECT
      *----------------------------------------------------------------*
       A310-CONNECT.
      *
           MOVE 'A310-CONNECT'               TO WK-LABEL.



           IF IDSV0003-SUCCESSFUL-RC

              MOVE AREA0140-DATABASE-NAME       TO WK-DATABASE-NAME
              MOVE AREA0140-USER                TO WK-USER
              MOVE AREA0140-PASSWORD            TO WK-PASSWORD

               EXEC SQL
                    CONNECT TO    :WK-DATABASE-NAME
                            USER  :WK-USER
                            USING :WK-PASSWORD
               END-EXEC

              PERFORM A100-VALORIZZA-RITORNO THRU A100-EX

           END-IF.
      *
       A310-EX.
           EXIT.



      *----------------------------------------------------------------*
      * ESEGUI DISCONNECT
      *----------------------------------------------------------------*
       A320-DISCONNECT.
      *
           MOVE 'A320-DISCONNECT'        TO WK-LABEL.

           MOVE AREA0140-DATABASE-NAME       TO WK-DATABASE-NAME

           EXEC SQL
                DISCONNECT    :WK-DATABASE-NAME
           END-EXEC.

           PERFORM A100-VALORIZZA-RITORNO THRU A100-EX.

      *
       A320-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ESEGUI CONNECT RESET
      *----------------------------------------------------------------*
       A330-CONNECT-RESET.
      *
           MOVE 'A330-CONNECT-RESET '        TO WK-LABEL.

           EXEC SQL
                CONNECT RESET
           END-EXEC.

           PERFORM A100-VALORIZZA-RITORNO    THRU A100-EX.

      *
       A330-EX.
           EXIT.
      *----------------------------------------------------------------*
      * controlli input
      *----------------------------------------------------------------*
       C000-CTRL-INPUT.
      *
           MOVE 'C000-CTRL-INPUT'               TO WK-LABEL.

           IF AREA0140-DATABASE-NAME = LOW-VALUE  OR
                                       HIGH-VALUE OR
                                       SPACES

              SET IDSV0003-GENERIC-ERROR TO TRUE

              MOVE SPACES TO IDSV0003-DESCRIZ-ERR-DB2
              STRING 'DATABASE NAME DI CONNESSIONE NON VALIDA'
                     DELIMITED BY SIZE
                     INTO
                     IDSV0003-DESCRIZ-ERR-DB2
              END-STRING

              MOVE 'IABS0140'  TO IDSV0003-COD-SERVIZIO-BE

           END-IF.

           IF IDSV0003-SUCCESSFUL-RC
              IF AREA0140-USER          = LOW-VALUE  OR
                                          HIGH-VALUE OR
                                          SPACES

                 SET IDSV0003-GENERIC-ERROR TO TRUE

                 MOVE SPACES TO IDSV0003-DESCRIZ-ERR-DB2
                 STRING 'USER DI CONNESSIONE NON VALIDA'
                        DELIMITED BY SIZE
                        INTO
                        IDSV0003-DESCRIZ-ERR-DB2
                 END-STRING

                 MOVE 'IABS0140'  TO IDSV0003-COD-SERVIZIO-BE

              END-IF

              IF IDSV0003-SUCCESSFUL-RC
                 IF AREA0140-PASSWORD      = LOW-VALUE  OR
                                             HIGH-VALUE OR
                                             SPACES

                    SET IDSV0003-GENERIC-ERROR TO TRUE

                    MOVE SPACES TO IDSV0003-DESCRIZ-ERR-DB2
                    STRING 'PASSWORD DI CONNESSIONE NON VALIDA'
                           DELIMITED BY SIZE
                           INTO
                           IDSV0003-DESCRIZ-ERR-DB2
                    END-STRING

                    MOVE 'IABS0140'  TO IDSV0003-COD-SERVIZIO-BE

                 END-IF

              END-IF

           END-IF.
      *
       C000-EX.
           EXIT.

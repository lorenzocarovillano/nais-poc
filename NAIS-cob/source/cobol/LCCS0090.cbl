      ******************************************************************
      **                                                        ********
      **    PORTAFOGLIO VITA ITALIA  VER 1.0                    ********
      **                                                        ********
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LCCS0090.
       AUTHOR.             AISS.
       DATE-WRITTEN.       7 NOV 2006.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *    PROGRAMMA ..... LCCS0090
      *    TIPOLOGIA...... SERVIZIO TRASVERSALE
      *    DESCRIZIONE.... ESTRAZIONE SEQUENCE TABELLA DB2
      *----------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    VARIABILI DI WORKING
      *----------------------------------------------------------------*

       77 DESCRIZ-ERR-DB2 PIC X(300) VALUE SPACES.

       01 WK-PGM                            PIC X(008) VALUE 'LCCS0090'.

       01 WS-CTRL-TABELLA                   PIC X(020).
          88 TAB-VALIDA                     VALUE
                                                  'ADES',
                                                  'ACC-COMM',
                                                  'ANA-SOPR-AUT',
                                                  'ANTIRICIC',
                                                  'ASSICURATI',
                                                  'AST-ALLOC',
                                                  'ATT-SERV-VAL',
                                                  'D-ATT-SERV-VAL',
                                                  'BILA-FND-CALC',
                                                  'BILA-FND-ESTR',
                                                  'BILA-TRCH-CALC',
                                                  'BILA-TRCH-ESTR',
                                                  'BILA-TRCH-SOM-P',
                                                  'BILA-VAR-CALC-P',
                                                  'BILA-VAR-CALC-T',
                                                  'BNFIC',
                                                  'BNFICR-LIQ',
                                                  'CLAU-TXT',
                                                  'CNBT-NDED',
                                                  'CONV-AZ',
                                                  'D-CRIST',
                                                  'DETT-ANTIC',
                                                  'DETT-DEROGA',
                                                  'DETT-QUEST',
                                                  'DETT-TCONT-LIQ',
                                                  'DETT-TIT-CONT',
                                                  'DETT-TIT-DI-RAT',
                                                  'DETT-TIT-COA-AG',
                                                  'DETT-TRASFE',
                                                  'DFLT-ADES',
                                                  'DFLT-RINN-TRCH',
                                                  'DOCTO-LIQ',
                                                  'D-COLL',
                                                  'D-FISC-ADES',
                                                  'D-FORZ-LIQ',
                                                  'EST-MOVI',
                                                  'ESTR-CNT-COASS',
                                                  'ESTENZ-CLAU-TXT',
                                                  'EVE-SERV-VAL',
                                                  'FNZ-F03-ACCOUNT',
                                                  'FNZ-F03-ORDER',
                                                  'FNZ-F05-ACCOUNT',
                                                  'FNZ-F5A-ACC-PF',
                                                  'FNZ-F5A-FUND-PF',
                                                  'GAR',
                                                  'GAR-LIQ',
                                                  'IDEN-ESTNI',
                                                  'IMPST-BOLLO',
                                                  'IMPST-SOST',
                                                  'LEG-ADES',
                                                  'LIQ',
                                                  'MATR-ELAB-BATCH',
                                                  'MATR-OBBL-ATTB',
                                                  'MOT-DEROGA',
                                                  'MOVI',
                                                  'MOVI-BATCH-SOSP',
                                                  'MOVI-FINRIO',
                                                  'NOTE-OGG',
                                                  'OGG-BLOCCO',
                                                  'OGG-COLLG',
                                                  'OGG-DEROGA',
                                                  'OGG-IN-COASS',
                                                  'OGG-RIASS',
                                                  'PARAM-CLAU-TXT',
                                                  'PARAM-DI-CALC',
                                                  'PARAM-MOVI',
                                                  'PARAM-OGG',
                                                  'PERC-LIQ',
                                                  'POLI',
                                                  'PREST',
                                                  'PREV',
                                                  'PROV-DI-GAR',
                                                  'PROG-PROD-IVASS',
                                                  'QUEST',
                                                  'RAPP-ANA',
                                                  'RAPP-RETE',
                                                  'REGOLE-VEND',
                                                  'REINVST-POLI-LQ',
                                                  'RICH',
                                                  'RICH-DIS-FND',
                                                  'RICH-INVST-FND',
                                                  'RIP-COASS-AGE',
                                                  'RIS-DI-TRCH',
                                                  'SOGG-ANTIRICIC',
                                                  'SOPR-DETT-QUEST',
                                                  'SOPR-DI-GAR',
                                                  'STAT-OGG-BUS',
                                                  'STAT-OGG-WF',
                                                  'STRA-DI-INVST',
                                                  'STRC-MOVI-SOSP',
                                                  'TCONT-LIQ',
                                                  'TIT-CONT',
                                                  'TRANS-POLI',
                                                  'TRANS-RAPP-ANA',
                                                  'TRANS-REN',
                                                  'TIT-RAT',
                                                  'TRCH-DI-GAR',
                                                  'TRCH-LIQ',
                                                  'TS-BNS-SIN-PRE',
                                                  'VAL-AST',
                                                  'VAL-PLFD',
                                                  'VAL-PMC',
                                                  'VINC-PEG',
                                                  'RICH-EST',
                                                  'STAT-RICH-EST',
                                                  'SEQ_ID_BATCH',
                                                  'SEQ_ID_EXECUTION',
                                                  'SEQ_ID_JOB',
                                                  'SEQ_ID_MESSAGE',
                                                  'SEQ_IVASS_TARI',
                                                  'SEQ_IVASS_PROD',
                                                  'SEQ_RIGA',
                                                  'SEQ_SESSIONE',
                                                  'SHARED_MEMORY',
                                                  'TRA-COND-MODMIN',
                                                  'TRA-VAL-MODMIN',
                                                  'PRE-LRD-CONT',
                                                  'SEQ-BILA-PROV-AMM-D',
                                                  'SEQ-BILA-PROV-AMM-T',
                                                  'D-CALC-SWITCH',
                                                  'RIP-DI-RIASS',
                                                  'TRCH-RIASTA',
                                                  'QUEST-ADEG-VER',
                                                  'RIBIL',
                                                  'SERV-INV-MIFID',
                                                  'PRESTAZ-PRE-ASS',
                                                  'MOT-LIQ',
                                                  'PDM-POLI-FORZ',
                                                  'PDM-CONF-SCARTO',
                                                  'PDM-CONF-GAP-IM',
                                                  'PDM-CONF-ELAB',
                                                  'AUT-DUE-DIL',
                                                  'RIV-TRCH-ESTR',
                                                  'RIV-VAR-CALC-P',
                                                  'RIV-VAR-CALC-T',
FNZF2                                             'FNZ-STANDBY',
FNZF2                                             'FNZ-EXT-ACC-ID',
                                                  'COSTI-IDD',
                                                  'DETT-COSTI-IDD',
AMLF3                                             'TERZO-REFERENTE',
16913                                             'CNTR-PTF-DIFF',
                                                  'FNZ-MON-FL-QTD',
PDLST                                             'PDM-CONF-ELAB-B',
PDLST                                             'PDM-STRC-ELAB',
PDLST                                             'PDM-STAT-ELAB',
                                                  'CNTSTR-CNT-CORR',
                                                  'STRA-ACTV-PTF',
                                                  'D-STRA-ACTV-PTF',
                                                  'STAT-STRA-AP',
                                                  'FNZ-EXT-ORDER-ID'.



           COPY LCCV0090.

      *----------------------------------------------------------------*
      *    MODULI CHIAMATI
      *----------------------------------------------------------------*

      *----------------------------------------------------------------*
      *    AREE MODULI CHIAMATI
      *----------------------------------------------------------------*

      *----------------------------------------------------------------*
      *    VARIABILI HOST
      *----------------------------------------------------------------*
       01 VARIABILI-HOST.
          02 WS-ID-TABELLA                            PIC S9(09) COMP-3.

      *----------------------------------------------------------------*
      *    INCLUDE DELLE TABELLE DB2
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE SQLCA    END-EXEC.

      *----------------------------------------------------------------*
      *    COMMAREA SERVIZIO
      *----------------------------------------------------------------*
       01  WORK-AREA.
           COPY LCCC0090                 REPLACING ==(SF)== BY ==WCOM==.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
       01  LINK-AREA.
           COPY LCCC0090                 REPLACING ==(SF)== BY ==LINK==.

      *---------------------------------------------------------------*
       PROCEDURE DIVISION USING LINK-AREA.
      *---------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI THRU EX-S0000.

           IF WCOM-RETURN-CODE = '00'
              PERFORM S1000-ELABORAZIONE     THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI   THRU EX-S9000.

      *----------------------------------------------------------------*
      * OPERAZIONI INIZIALI                                            *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           MOVE LINK-AREA                    TO WORK-AREA.

           MOVE '00'                         TO WCOM-RETURN-CODE.
           MOVE ZEROES                       TO WCOM-SEQ-TABELLA
                                                WCOM-SQLCODE.
           MOVE SPACES                       TO WCOM-DESCRIZ-ERR-DB2.

           PERFORM S0005-CTRL-DATI-INPUT     THRU EX-S0005.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    CONTROLLO DATI INPUT
      *----------------------------------------------------------------*
       S0005-CTRL-DATI-INPUT.

           MOVE WCOM-NOME-TABELLA  TO WS-CTRL-TABELLA
                                    WS-CTRL-TABELLA-CLIENT.

           IF NOT TAB-VALIDA AND NOT TAB-CLIENT-VALIDA
              MOVE 'S1'                       TO WCOM-RETURN-CODE
              MOVE 'NOME SEQUENCE NON VALIDO' TO WCOM-DESCRIZ-ERR-DB2
           END-IF.

       EX-S0005.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           PERFORM S1100-ESTRAZIONE-SEQ       THRU EX-S1100.

       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    ESTRAZIONE SEQUENCE
      *----------------------------------------------------------------*
       S1100-ESTRAZIONE-SEQ.

           IF WCOM-NOME-TABELLA = 'ADES'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_ADES
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'ACC-COMM'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_ACC_COMM
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'ANA-SOPR-AUT'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_ANA_SOPR_AUT
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'ANTIRICIC'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_ANTIRICIC
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'ASSICURATI'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_ASSICURATI
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'AST-ALLOC'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_AST_ALLOC
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'ATT-SERV-VAL'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_ATT_SERV_VAL
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'D-ATT-SERV-VAL'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_D_ATT_SERV_VAL
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'BNFIC'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_BNFIC
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'BILA-FND-CALC'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_BILA_FND_CALC
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'BILA-FND-ESTR'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_BILA_FND_ESTR
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'BILA-TRCH-CALC'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_BILA_TRCH_CALC
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'BILA-TRCH-ESTR'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_BILA_TRCH_ESTR
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'BILA-TRCH-SOM-P'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_BILA_TRCH_SOM_P
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'BILA-VAR-CALC-P'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_BILA_VAR_CALC_P
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'BILA-VAR-CALC-T'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_BILA_VAR_CALC_T
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'BNFICR-LIQ'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_BNFICR_LIQ
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'CLAU-TXT'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_CLAU_TXT
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'CNBT-NDED'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_CNBT_NDED
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'CONV-AZ'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_CONV_AZ
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'D-CRIST'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_D_CRIST
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'DETT-ANTIC'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_DETT_ANTIC
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'DETT-DEROGA'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_DETT_DEROGA
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'DETT-QUEST'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_DETT_QUEST
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'DETT-TCONT-LIQ'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_DETT_TCONT_LIQ
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'DETT-TIT-CONT'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_DETT_TIT_CONT
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'DETT-TIT-DI-RAT'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_DETT_TIT_DI_RAT
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'DETT-TIT-COA-AG'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_DETT_TIT_COA_AG
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'DETT-TRASFE'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_DETT_TRASFE
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'DFLT-ADES'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_DFLT_ADES
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'DFLT-RINN-TRCH'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_DFLT_RINN_TRCH
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'DOCTO-LIQ'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_DOCTO_LIQ
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'D-COLL'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_D_COLL
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'D-FISC-ADES'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_D_FISC_ADES
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'D-FORZ-LIQ'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_D_FORZ_LIQ
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'ESTENZ-CLAU-TXT'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_ESTENZ_CLAU_TXT
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'EST-MOVI'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_EST_MOVI
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'ESTR-CNT-COASS'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_ESTR_CNT_COASS
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'EVE-SERV-VAL'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_EVE_SERV_VAL
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'FNZ-F03-ACCOUNT'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_FNZ_F03_ACCOUNT
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'FNZ-F03-ORDER'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_FNZ_F03_ORDER
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'FNZ-F05-ACCOUNT'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_FNZ_F05_ACCOUNT
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'FNZ-F5A-ACC-PF'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_FNZ_F5A_ACC_PF
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'FNZ-F5A-FUND-PF'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_FNZ_F5A_FUND_PF
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'GAR'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_GAR
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'GAR-LIQ'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_GAR_LIQ
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'IDEN-ESTNI'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_IDEN_ESTNI
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'IMPST-BOLLO'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_IMPST_BOLLO
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'IMPST-SOST'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_IMPST_SOST
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'LEG-ADES'
           EXEC SQL
                 VALUES NEXTVAL FOR SEQ_LEG_ADES
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'LIQ'
           EXEC SQL
                 VALUES NEXTVAL FOR SEQ_LIQ
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'MATR-ELAB-BATCH'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_MATR_ELAB_BATCH
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'MATR-OBBL-ATTB'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_MATR_OBBL_ATTB
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'MOT-DEROGA'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_MOT_DEROGA
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'MOVI'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_MOVI
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'MOVI-BATCH-SOSP'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_MOVI_BATCH_SOSP
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'MOVI-FINRIO'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_MOVI_FINRIO
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'NOTE-OGG'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_NOTE_OGG
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'OGG-BLOCCO'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_OGG_BLOCCO
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'OGG-COLLG'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_OGG_COLLG
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'OGG-DEROGA'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_OGG_DEROGA
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'OGG-IN-COASS'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_OGG_IN_COASS
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'PARAM-CLAU-TXT'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_PARAM_CLAU_TXT
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'PARAM-DI-CALC'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_PARAM_DI_CALC
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'PARAM-MOVI'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_PARAM_MOVI
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'PARAM-OGG'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_PARAM_OGG
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'PERC-LIQ'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_PERC_LIQ
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'POLI'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_POLI
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'PREST'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_PREST
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'PREV'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_PREV
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'PROG-PROD-IVASS'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_PROG_PROD_IVASS
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'PROV-DI-GAR'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_PROV_DI_GAR
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'QUEST'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_QUEST
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'RAPP-ANA'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_RAPP_ANA
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'RAPP-RETE'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_RAPP_RETE
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'REINVST-POLI-LQ'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_REINVST_POLI_LQ
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'RICH'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_RICH
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'RICH-DIS-FND'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_RICH_DIS_FND
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'RICH-INVST-FND'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_RICH_INVST_FND
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'RIP-COASS-AGE'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_RIP_COASS_AGE
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'RIP-DI-RIASS'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_RIP_DI_RIASS
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'RIS-DI-TRCH'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_RIS_DI_TRCH
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'RIV-TRCH-ESTR'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_RIV_TRCH_ESTR
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'RIV-VAR-CALC-P'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_RIV_VAR_CALC_P
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'RIV-VAR-CALC-T'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_RIV_VAR_CALC_T
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'SOGG-ANTIRICIC'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_SOGG_ANTIRICIC
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'SOPR-DETT-QUEST'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_SOPR_DETT_QUEST
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'SOPR-DI-GAR'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_SOPR_DI_GAR
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'STAT-OGG-BUS'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_STAT_OGG_BUS
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'STAT-OGG-WF'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_STAT_OGG_WF
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'STRA-DI-INVST'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_STRA_DI_INVST
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'STRC-MOVI-SOSP'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_STRC_MOVI_SOSP
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'TCONT-LIQ'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_TCONT_LIQ
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'TIT-CONT'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_TIT_CONT
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'TIT-RAT'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_TIT_RAT
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'TRANS-POLI'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_TRANS_POLI
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'TRANS-RAPP-ANA'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_TRANS_RAPP_ANA
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'TRANS-REN'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_TRANS_REN
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'TRCH-DI-GAR'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_TRCH_DI_GAR
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'TRCH-LIQ'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_TRCH_LIQ
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'TRCH-RIASTA'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_TRCH_RIASTA
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'QUEST-ADEG-VER'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_QUEST_ADEG_VER
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'TS-BNS-SIN-PRE'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_TS_BNS_SIN_PRE
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'VAL-AST'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_VAL_AST
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'VAL-PLFD'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_VAL_PLFD
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'VAL-PMC'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_VAL_PMC
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'VINC-PEG'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_VINC_PEG
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'RICH-EST'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_RICH_EST
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'STAT-RICH-EST'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_STAT_RICH_EST
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'TRA-COND-MODMIN'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_TRA_COND_MODMIN
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'TRA-VAL-MODMIN'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_TRA_VAL_MODMIN
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'PRE-LRD-CONT'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_PRE_LRD_CONT
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'D-CALC-SWITCH'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_D_CALC_SWITCH
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'RIBIL'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_RIBIL
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'SERV-INV-MIFID'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_SERV_INV_MIFID
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'PRESTAZ-PRE-ASS'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_PRESTAZ_PRE_ASS
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'MOT-LIQ'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_MOT_LIQ
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'PDM-POLI-FORZ'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_PDM_POLI_FORZ
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'PDM-CONF-SCARTO'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_PDM_CONF_SCARTO
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'PDM-CONF-GAP-IM'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_PDM_CONF_GAP_IM
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'PDM-CONF-ELAB'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_PDM_CONF_ELAB
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'AUT-DUE-DIL'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_AUT_DUE_DIL
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

      ************************************************************
      *  SEQUENCE DI UTILIZZO INFRASTRUTTURALE
      *  BATCH EXECUTOR
      ************************************************************

           IF WCOM-NOME-TABELLA = 'SEQ_ID_BATCH'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_ID_BATCH
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'SEQ_ID_EXECUTION'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_ID_EXECUTION
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'SEQ_ID_JOB'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_ID_JOB
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'SEQ_ID_MESSAGE'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_ID_MESSAGE
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

      ************************************************************
      *  SEQUENCE DI UTILIZZO INFRASTRUTTURALE
      *  DATA DISPATCHER
      ************************************************************

           IF WCOM-NOME-TABELLA = 'SEQ_RIGA'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_RIGA
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'SEQ_SESSIONE'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_SESSIONE
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'SHARED_MEMORY'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_SHARED_MEMORY
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'REGOLE-VEND'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_REGOLE_VEND
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'BILA-PROV-AMM-D'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_BILA_PROV_AMM_D
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'BILA-PROV-AMM-T'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_BILA_PROV_AMM_T
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'SEQ_IVASS_TARI'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_IVASS_TARI
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'SEQ_IVASS_PROD'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_IVASS_PROD
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.


FNZF2      IF WCOM-NOME-TABELLA = 'FNZ-STANDBY'
FNZF2         EXEC SQL
FNZF2            VALUES NEXTVAL FOR SEQ_FNZ_STANDBY
FNZF2              INTO :WS-ID-TABELLA
FNZF2         END-EXEC
           END-IF.

FNZF2      IF WCOM-NOME-TABELLA = 'FNZ-EXT-ACC-ID'
FNZF2         EXEC SQL
FNZF2            VALUES NEXTVAL FOR SEQ_FNZ_EXT_ACC_ID
FNZF2              INTO :WS-ID-TABELLA
FNZF2         END-EXEC
           END-IF.


           IF WCOM-NOME-TABELLA = 'COSTI-IDD'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_COSTI_IDD
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'DETT-COSTI-IDD'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_DETT_COSTI_IDD
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

AMLF3      IF WCOM-NOME-TABELLA = 'TERZO-REFERENTE'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_TERZO_REFERENTE
                   INTO :WS-ID-TABELLA
              END-EXEC
AMLF3      END-IF.

16913      IF WCOM-NOME-TABELLA = 'CNTR-PTF-DIFF'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_CNTR_PTF_DIFF
                   INTO :WS-ID-TABELLA
              END-EXEC
16913      END-IF.

           IF WCOM-NOME-TABELLA = 'FNZ-MON-FL-QTD'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_FNZ_MON_FL_QTD
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

PDLST      IF WCOM-NOME-TABELLA = 'PDM-CONF-ELAB-B'
PDLST         EXEC SQL
PDLST            VALUES NEXTVAL FOR SEQ_PDM_CONF_ELAB_B
PDLST              INTO :WS-ID-TABELLA
PDLST         END-EXEC
PDLST      END-IF.

PDLST      IF WCOM-NOME-TABELLA = 'PDM-STRC-ELAB'
PDLST         EXEC SQL
PDLST            VALUES NEXTVAL FOR SEQ_PDM_STRC_ELAB
PDLST              INTO :WS-ID-TABELLA
PDLST         END-EXEC
PDLST      END-IF.

PDLST      IF WCOM-NOME-TABELLA = 'PDM-STAT-ELAB'
PDLST         EXEC SQL
PDLST            VALUES NEXTVAL FOR SEQ_PDM_STAT_ELAB
PDLST              INTO :WS-ID-TABELLA
PDLST         END-EXEC
PDLST      END-IF.

           IF WCOM-NOME-TABELLA = 'CNTSTR-CNT-CORR'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_CNTSTR_CNT_CORR
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'STRA-ACTV-PTF'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_STRA_ACTV_PTF
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'D-STRA-ACTV-PTF'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_D_STRA_ACTV_PTF
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'STAT-STRA-AP'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_STAT_STRA_AP
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'FNZ-EXT-ORDER-ID'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_FNZ_EXT_ORDER_ID
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           PERFORM S1101-ESTRAZIONE-SEQ-CLIENT THRU EX-S1101.

      ************************************************************
      *
      ************************************************************

           IF SQLCODE NOT = 0
              MOVE 'D3'             TO WCOM-RETURN-CODE
              MOVE SQLCODE          TO WCOM-SQLCODE
              MOVE DESCRIZ-ERR-DB2  TO WCOM-DESCRIZ-ERR-DB2
           ELSE
              MOVE WS-ID-TABELLA    TO WCOM-SEQ-TABELLA
           END-IF.

       EX-S1100.
           EXIT.

      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           MOVE WORK-AREA           TO LINK-AREA.

           GOBACK.

       EX-S9000.
           EXIT.


           EXEC SQL INCLUDE LCCP9999    END-EXEC.


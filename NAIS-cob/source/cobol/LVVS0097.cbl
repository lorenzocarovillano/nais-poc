      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0097.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0097
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0097'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
      *-- AREA ROUTINE PER IL CALCOLO

       01  DATA-INFERIORE.
           05 AAAA-INF             PIC 9(04) VALUE ZEROES.
           05 MM-INF               PIC 9(02) VALUE 01.
           05 GG-INF               PIC 9(02) VALUE 01.
       01  DATA-INFERIORE-N REDEFINES DATA-INFERIORE PIC 9(8).

       01  DATA-SUPERIORE.
           05 AAAA-SUP             PIC 9(04) VALUE ZEROES.
           05 MM-SUP               PIC 9(02) VALUE 12.
           05 GG-SUP               PIC 9(02) VALUE 31.
       01  DATA-SUPERIORE-N REDEFINES DATA-SUPERIORE PIC 9(8).

      *----------------------------------------------------------------*
      *--  Routine date
31403  01  LCCS0003                         PIC X(008) VALUE 'LCCS0003'.
      *--  COPY PER SERVIZIO VERIFICA CALCOLI SU DATE
31403      COPY LCCC0003.
31403  01  IN-RCODE                       PIC 9(2) VALUE ZEROES.

      *---------------------------------------------------------------*
      *  COPY PER CHIAMATA LDBS1780
      *---------------------------------------------------------------*
           COPY LDBV1471.
           COPY LDBV4021.
31403      COPY LDBVE091.
31403      COPY LCCVXMV0.
           COPY IDBVPMO1.
      *---------------------------------------------------------------*
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-TGA.
          03 DTGA-AREA-TGA.
             04 DTGA-ELE-TGA-MAX        PIC S9(04) COMP.
                COPY LCCVTGAC REPLACING   ==(SF)==  BY ==DTGA==.
             COPY LCCVTGA1              REPLACING ==(SF)== BY ==DTGA==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
           03 IX-TAB-TGA                     PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0007.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0007.
      *----------------------------------------------------------------*
      *
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.
      *
           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000
      *
           GOBACK.
      *
      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.

           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           INITIALIZE LDBV4021.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE AREA-IO-TGA.

      *
      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL

31403          MOVE  IDSV0003-TIPO-MOVIMENTO TO WS-MOVIMENTO

               PERFORM S1250-VERIFICA-DATE         THRU S1250-EX

31403          IF  CALC-IMPOSTA-SOSTIT
31403              IF IN-RCODE  = '00'
31403                 PERFORM S1256-CALCOLA-IMPORTO   THRU S1256-EX
31403              END-IF
31403          ELSE
                   PERFORM S1255-CALCOLA-IMPORTO   THRU S1255-EX
31403          END-IF

           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-TRCH-GAR
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DTGA-AREA-TGA
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA DATAULTADEGP
      *----------------------------------------------------------------*
       S1250-VERIFICA-DATE.
      *
           INITIALIZE PARAM-MOVI
                      LDBV1471.
      *
           MOVE IVVC0213-ID-ADESIONE             TO PMO-ID-OGG.
           MOVE 'AD'                             TO PMO-TP-OGG.
           MOVE 6010                             TO LDBV1471-TP-MOVI-01.
           SET IDSV0003-SELECT                   TO TRUE.
           SET IDSV0003-WHERE-CONDITION          TO TRUE.

           MOVE LDBV1471                 TO IDSV0003-BUFFER-WHERE-COND.
           PERFORM S1251-CALL-LDBS1470           THRU S1251-EX.

           IF IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
              IF PMO-DT-RICOR-PREC-NULL = HIGH-VALUE
                 MOVE IVVC0213-DATA-EFFETTO(1:4) TO AAAA-INF
                 MOVE IVVC0213-DATA-EFFETTO(1:4) TO AAAA-SUP
              ELSE
31403            IF CALC-IMPOSTA-SOSTIT
31403               PERFORM S1257-CALC-DT-INFERIORE
31403                  THRU S1257-EX
31403            ELSE
                   MOVE PMO-DT-INI-EFF           TO DATA-INFERIORE-N
                   MOVE PMO-DT-RICOR-PREC        TO DATA-SUPERIORE-N
31403            END-IF
              END-IF
           ELSE
              MOVE IVVC0213-DATA-EFFETTO(1:4)    TO AAAA-INF
              MOVE IVVC0213-DATA-EFFETTO(1:4)    TO AAAA-SUP
           END-IF.
      *
       S1250-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURA PMO
      *----------------------------------------------------------------*
       S1251-CALL-LDBS1470.
      *
           SET IDSV0003-SELECT               TO TRUE
           SET IDSV0003-WHERE-CONDITION      TO TRUE
           SET IDSV0003-TRATT-X-COMPETENZA   TO TRUE

           MOVE 'LDBS1470'                     TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 PARAM-MOVI
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS1470 ERRORE CHIAMATA - LDBS1470'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF NOT IDSV0003-SUCCESSFUL-SQL

              IF IDSV0003-NOT-FOUND
                 CONTINUE
              ELSE
                 IF IDSV0003-SQLCODE = -811
                    SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                 ELSE
                    SET IDSV0003-INVALID-OPER            TO TRUE
                    MOVE WK-CALL-PGM
                      TO IDSV0003-COD-SERVIZIO-BE
                    STRING 'CHIAMATA LDBS1470 ;'
                            IDSV0003-RETURN-CODE ';'
                            IDSV0003-SQLCODE
                        DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    END-STRING
                 END-IF
              END-IF
      *
           END-IF.
      *
       S1251-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA
      *----------------------------------------------------------------*
       S1255-CALCOLA-IMPORTO.
      *
           INITIALIZE LDBV4021.
      *
           SET IDSV0003-SELECT               TO TRUE
           SET IDSV0003-WHERE-CONDITION      TO TRUE
           SET IDSV0003-TRATT-X-COMPETENZA   TO TRUE


           MOVE DTGA-ID-TRCH-DI-GAR(IVVC0213-IX-TABB)
                                               TO LDBV4021-ID-TGA.
           MOVE DATA-INFERIORE-N               TO LDBV4021-DATA-INIZIO.
           MOVE DATA-SUPERIORE-N               TO LDBV4021-DATA-FINE.
      *
           MOVE 'LDBS4020'                     TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 LDBV4021
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS4020 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE LDBV4021-PRE-LRD      TO IVVC0213-VAL-IMP-O
           ELSE
              IF IDSV0003-NOT-FOUND
              OR IDSV0003-SQLCODE = -305
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF
      *
              MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA LDBS4020 ;'
                  IDSV0003-RETURN-CODE ';'
                  IDSV0003-SQLCODE
                  DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
      *
           END-IF.
      *
       S1255-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA PREPAGANNO IN CASO DI CALCOLO IMPOSTA SOSTITUTIVA
      *----------------------------------------------------------------*
       S1256-CALCOLA-IMPORTO.
      *
           INITIALIZE LDBVE091.
      *
           SET IDSV0003-SELECT               TO TRUE
           SET IDSV0003-WHERE-CONDITION      TO TRUE
           SET IDSV0003-TRATT-X-COMPETENZA   TO TRUE

           MOVE DTGA-ID-TRCH-DI-GAR(IVVC0213-IX-TABB)
                                               TO LDBVE091-ID-TGA.
           MOVE DATA-INFERIORE-N               TO LDBVE091-DATA-INIZIO.
           MOVE DATA-SUPERIORE-N               TO LDBVE091-DATA-FINE.
      *
           MOVE 'LDBSE090'                     TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 LDBVE091
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS4020 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE LDBVE091-PRE-LRD      TO IVVC0213-VAL-IMP-O
           ELSE
              IF IDSV0003-NOT-FOUND
              OR IDSV0003-SQLCODE = -305
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF
      *
              MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA LDBSE090 ;'
                  IDSV0003-RETURN-CODE ';'
                  IDSV0003-SQLCODE
                  DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
      *
           END-IF.
      *
       S1256-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CALCOLA IL VALORE DELLA DATA INIZIO VERIFICA
      *----------------------------------------------------------------*
31403  S1257-CALC-DT-INFERIORE.

           INITIALIZE           IO-A2K-LCCC0003
                                IN-RCODE

      *    sottrarre DELTA alla data di input
           MOVE '02'                        TO A2K-FUNZ

      *    FORMATO AAAAMMGG
           MOVE '03'                        TO A2K-INFO

      *    NUMERO GIORNI
           MOVE 1                           TO A2K-DELTA

      *    GIORNI
           MOVE SPACE                       TO A2K-TDELTA

      *    GIORNI FISSI
           MOVE '0'                         TO A2K-FISLAV

      *    INIZIO CONTEGGIO DA STESSO GIORNO
           MOVE 0                           TO A2K-INICON

      *    DATA INPUT : DATA RICORRENZA PRECEDENTE
           MOVE PMO-DT-RICOR-PREC           TO DATA-INFERIORE-N
           MOVE DATA-INFERIORE              TO A2K-INDATA

           PERFORM CALL-ROUTINE-DATE
              THRU CALL-ROUTINE-DATE-EX

           IF IDSV0003-SUCCESSFUL-RC
      *       DATA OUTPUT (CALCOLATA)
              MOVE A2K-OUAMG                  TO DATA-INFERIORE-N
              MOVE PMO-DT-RICOR-SUCC          TO DATA-SUPERIORE-N
           END-IF.

       S1257-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CHIAMATA AL SERVIZIO CALCOLA DATA
      *----------------------------------------------------------------*
31403  CALL-ROUTINE-DATE.

           CALL LCCS0003      USING IO-A2K-LCCC0003
                                    IN-RCODE

           ON EXCEPTION
              SET  IDSV0003-INVALID-OPER                  TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'LVVS0144 - ERRORE CALL LCCS0003'
                TO IDSV0003-DESCRIZ-ERR-DB2
           END-CALL.

           IF IN-RCODE  = '00'
              CONTINUE
           ELSE
              SET  IDSV0003-INVALID-OPER                  TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING 'LVVS0144 - ERRORE CALL LCCS0003 - RC: '
                     IN-RCODE
                     DELIMITED BY SIZE
                INTO IDSV0003-DESCRIZ-ERR-DB2
           END-IF.

       CALL-ROUTINE-DATE-EX.
           EXIT.



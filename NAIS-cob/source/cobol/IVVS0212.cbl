
      *****************************************************************
      *                                                               *
      *                   IDENTIFICATION DIVISION                     *
      *                                                               *
      *****************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IVVS0212.
       AUTHOR.        ACCENTURE.
       DATE-WRITTEN.  21/11/2007.

      *****************************************************************
      *                                                               *
      *    NOME :           IVVS0212                                  *
      *    TIPO :                                                     *
      *    DESCRIZIONE :    RICERCA AUTONOMIA OPERATIVA               *
      *                                                               *
      *    AREE DI PASSAGGIO DATI                                     *
      *                                                               *
      *    DATI DI INPUT/OUTPUT : IVVV0212                            *
      *                                                               *
      *****************************************************************
      *                      LOG MODIFICHE                            *
      *****************************************************************
      *                                                               *
      * CODICE MODIF.            AUTORE          DATA                 *
      * ---------------  **   -----------   **   --/--/----           *
      *                                                               *
      * DESCRZIONE: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX           *
      *             XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX           *
      *                                                               *
      *****************************************************************
      *****************************************************************
      *                                                               *
      *                   ENVIRONMENT  DIVISION                       *
      *                                                               *
      *****************************************************************
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
      *    NULL-IND     Carattere usato per indicare campi NULL;
           SYMBOLIC CHARACTERS
              NULL-IND         IS 256
           DECIMAL-POINT IS COMMA.


      *****************************************************************
      *                                                               *
      *                   DATA  DIVISION                              *
      *                                                               *
      *****************************************************************
       DATA DIVISION.

      *****************************************************************
      *                                                               *
      *                   WORKING STORAGE SECTION                     *
      *                                                               *
      *****************************************************************
       WORKING-STORAGE SECTION.
      ***************************************************************
      * NOMI PGM
      ***************************************************************
       01  WK-PGM                        PIC  X(08) VALUE 'IVVS0212'.

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       77 DESCRIZ-ERR-DB2                PIC X(300)  VALUE SPACES.

      *-- DCLGEN TABELLA CTRL_AUT_OPER
           EXEC SQL INCLUDE IDBDCAO0 END-EXEC.
           EXEC SQL INCLUDE IDBVCAO1 END-EXEC.
           EXEC SQL INCLUDE IDBVCAO2 END-EXEC.

           EXEC SQL
             DECLARE CAO-1 CURSOR FOR
             SELECT
                ID_CTRL_AUT_OPER
                ,COD_COMPAGNIA_ANIA
                ,TP_MOVI
                ,COD_ERRORE
                ,KEY_AUT_OPER1
                ,KEY_AUT_OPER2
                ,KEY_AUT_OPER3
                ,KEY_AUT_OPER4
                ,KEY_AUT_OPER5
                ,COD_LIV_AUT
                ,TP_MOT_DEROGA
                ,MODULO_VERIFICA
                ,COD_COND
                ,PROG_COND
                ,RISULT_COND
                ,PARAM_AUT_OPER
                ,STATO_ATTIVAZIONE
             FROM CTRL_AUT_OPER
             WHERE COD_COMPAGNIA_ANIA = :CAO-COD-COMPAGNIA-ANIA
               AND TP_MOVI            = :CAO-TP-MOVI
               AND KEY_AUT_OPER1      = :CAO-KEY-AUT-OPER1
               AND KEY_AUT_OPER2      = :CAO-KEY-AUT-OPER2
               AND KEY_AUT_OPER3      = :CAO-KEY-AUT-OPER3
               AND KEY_AUT_OPER4      = :CAO-KEY-AUT-OPER4
               AND KEY_AUT_OPER5      = :CAO-KEY-AUT-OPER5
           END-EXEC.

           EXEC SQL
             DECLARE CAO-2 CURSOR FOR
             SELECT
                ID_CTRL_AUT_OPER
                ,COD_COMPAGNIA_ANIA
                ,TP_MOVI
                ,COD_ERRORE
                ,KEY_AUT_OPER1
                ,KEY_AUT_OPER2
                ,KEY_AUT_OPER3
                ,KEY_AUT_OPER4
                ,KEY_AUT_OPER5
                ,COD_LIV_AUT
                ,TP_MOT_DEROGA
                ,MODULO_VERIFICA
                ,COD_COND
                ,PROG_COND
                ,RISULT_COND
                ,PARAM_AUT_OPER
                ,STATO_ATTIVAZIONE
             FROM CTRL_AUT_OPER
             WHERE COD_COMPAGNIA_ANIA = :CAO-COD-COMPAGNIA-ANIA
               AND TP_MOVI            = :CAO-TP-MOVI
               AND KEY_AUT_OPER1      = :CAO-KEY-AUT-OPER1
               AND KEY_AUT_OPER2      = :CAO-KEY-AUT-OPER2
               AND KEY_AUT_OPER3      = :CAO-KEY-AUT-OPER3
               AND KEY_AUT_OPER4      = :CAO-KEY-AUT-OPER4
               AND KEY_AUT_OPER5     IS NULL
           END-EXEC.

           EXEC SQL
             DECLARE CAO-3 CURSOR FOR
             SELECT
                ID_CTRL_AUT_OPER
                ,COD_COMPAGNIA_ANIA
                ,TP_MOVI
                ,COD_ERRORE
                ,KEY_AUT_OPER1
                ,KEY_AUT_OPER2
                ,KEY_AUT_OPER3
                ,KEY_AUT_OPER4
                ,KEY_AUT_OPER5
                ,COD_LIV_AUT
                ,TP_MOT_DEROGA
                ,MODULO_VERIFICA
                ,COD_COND
                ,PROG_COND
                ,RISULT_COND
                ,PARAM_AUT_OPER
                ,STATO_ATTIVAZIONE
             FROM CTRL_AUT_OPER
             WHERE COD_COMPAGNIA_ANIA = :CAO-COD-COMPAGNIA-ANIA
               AND TP_MOVI            = :CAO-TP-MOVI
               AND KEY_AUT_OPER1      = :CAO-KEY-AUT-OPER1
               AND KEY_AUT_OPER2      = :CAO-KEY-AUT-OPER2
               AND KEY_AUT_OPER3      = :CAO-KEY-AUT-OPER3
               AND KEY_AUT_OPER4     IS NULL
               AND KEY_AUT_OPER5     IS NULL
           END-EXEC.

           EXEC SQL
             DECLARE CAO-4 CURSOR FOR
             SELECT
                ID_CTRL_AUT_OPER
                ,COD_COMPAGNIA_ANIA
                ,TP_MOVI
                ,COD_ERRORE
                ,KEY_AUT_OPER1
                ,KEY_AUT_OPER2
                ,KEY_AUT_OPER3
                ,KEY_AUT_OPER4
                ,KEY_AUT_OPER5
                ,COD_LIV_AUT
                ,TP_MOT_DEROGA
                ,MODULO_VERIFICA
                ,COD_COND
                ,PROG_COND
                ,RISULT_COND
                ,PARAM_AUT_OPER
                ,STATO_ATTIVAZIONE
             FROM CTRL_AUT_OPER
             WHERE COD_COMPAGNIA_ANIA = :CAO-COD-COMPAGNIA-ANIA
               AND TP_MOVI            = :CAO-TP-MOVI
               AND KEY_AUT_OPER1      = :CAO-KEY-AUT-OPER1
               AND KEY_AUT_OPER2      = :CAO-KEY-AUT-OPER2
               AND KEY_AUT_OPER3     IS NULL
               AND KEY_AUT_OPER4     IS NULL
               AND KEY_AUT_OPER5     IS NULL
           END-EXEC.

           EXEC SQL
             DECLARE CAO-5 CURSOR FOR
             SELECT
                ID_CTRL_AUT_OPER
                ,COD_COMPAGNIA_ANIA
                ,TP_MOVI
                ,COD_ERRORE
                ,KEY_AUT_OPER1
                ,KEY_AUT_OPER2
                ,KEY_AUT_OPER3
                ,KEY_AUT_OPER4
                ,KEY_AUT_OPER5
                ,COD_LIV_AUT
                ,TP_MOT_DEROGA
                ,MODULO_VERIFICA
                ,COD_COND
                ,PROG_COND
                ,RISULT_COND
                ,PARAM_AUT_OPER
                ,STATO_ATTIVAZIONE
             FROM CTRL_AUT_OPER
             WHERE COD_COMPAGNIA_ANIA = :CAO-COD-COMPAGNIA-ANIA
               AND TP_MOVI            = :CAO-TP-MOVI
               AND KEY_AUT_OPER1      = :CAO-KEY-AUT-OPER1
               AND KEY_AUT_OPER2     IS NULL
               AND KEY_AUT_OPER3     IS NULL
               AND KEY_AUT_OPER4     IS NULL
               AND KEY_AUT_OPER5     IS NULL
           END-EXEC.

           EXEC SQL
             DECLARE CAO-6 CURSOR FOR
             SELECT
                 ID_CTRL_AUT_OPER
                ,COD_COMPAGNIA_ANIA
                ,TP_MOVI
                ,COD_ERRORE
                ,KEY_AUT_OPER1
                ,KEY_AUT_OPER2
                ,KEY_AUT_OPER3
                ,KEY_AUT_OPER4
                ,KEY_AUT_OPER5
                ,COD_LIV_AUT
                ,TP_MOT_DEROGA
                ,MODULO_VERIFICA
                ,COD_COND
                ,PROG_COND
                ,RISULT_COND
                ,PARAM_AUT_OPER
                ,STATO_ATTIVAZIONE
             FROM CTRL_AUT_OPER
             WHERE COD_COMPAGNIA_ANIA = :CAO-COD-COMPAGNIA-ANIA
               AND TP_MOVI            = :CAO-TP-MOVI
               AND KEY_AUT_OPER1     IS NULL
               AND KEY_AUT_OPER2     IS NULL
               AND KEY_AUT_OPER3     IS NULL
               AND KEY_AUT_OPER4     IS NULL
               AND KEY_AUT_OPER5     IS NULL
           END-EXEC.

       01 UNZIP-STRUCTURE.
          05 UNZIP-LENGTH-STR-MAX           PIC 9(04).
          05 UNZIP-LENGTH-FIELD             PIC 9(02).
          05 UNZIP-STRING-ZIPPED            PIC X(4000).
          05 UNZIP-IDENTIFICATORE           PIC X(01).
          05 UNZIP-AREA-VARIABILI.
             10 UNZIP-ELE-VARIABILI-MAX     PIC S9(04) COMP-3.
             10 UNZIP-TAB.
                15 UNZIP-TAB-VARIABILI      OCCURS 100.
                   20 UNZIP-COD-VARIABILE   PIC  X(30).
             10 UNZIP-TAB-R REDEFINES UNZIP-TAB.
                15 FILLER                   PIC  X(30).
                15 UNZIP-RESTO-TAB          PIC  X(2970).

      *---------------------------------------------------------------*
      *                   FLAGS AND SWITCHES                          *
      *---------------------------------------------------------------*

       01 SW-SWITCH.
      *--SWITCH PER ITER ELABORAZIONE
          05 SW-CORRETTO                   PIC X(01) VALUE 'N'.
              88 SI-CORRETTO                         VALUE 'S'.
              88 NO-CORRETTO                         VALUE 'N'.

          05 SW-TROVATO                    PIC X(01) VALUE 'N'.
              88 SI-TROVATO                          VALUE 'S'.
              88 NO-TROVATO                          VALUE 'N'.

          05 WK-VARLIST                   PIC X(01).
              88 SI-FINE-VARLIST                     VALUE 'S'.
              88 NO-FINE-VARLIST                     VALUE 'N'.

          05 SW-FINE-ELEMENTI             PIC X(01).
              88 LEGGI-ELEMENTI                      VALUE 'N'.
              88 FINE-ELEMENTI                       VALUE 'S'.


      *---------------------------------------------------------------*
      *                         CONSTANTI                             *
      *---------------------------------------------------------------*

      *---------------------------------------------------------------*
      *                            INDICI                             *
      *---------------------------------------------------------------*
       01 INDICI.
           03 IND-UNZIP                  PIC S9(04) COMP.
           03 IND-START-VAR              PIC S9(04) COMP.
           03 IND-END-VAR                PIC S9(04) COMP.
           03 IND-CHAR                   PIC S9(04) COMP.
           03 IX-TAB-CAO                 PIC S9(04) COMP.
           03 IX-AUT-OPER                PIC S9(04) COMP.
           03 IX-PARAM                   PIC S9(04) COMP.

       01 LIMITE-VARIABILI-UNZIPPED      PIC 9(03) VALUE 100.

      *---------------------------------------------------------------*
      *                         VARIABILI                             *
      *---------------------------------------------------------------*
       01 WS-VARIABILI.
           05 WS-COMPAGNIA                 PIC  9(005).
           05 WS-MOVIMENTO                 PIC  9(005).
           05 WS-NUM-ACCESSO               PIC  9(002).

       01 WS-SQLCODE                       PIC -9(004).

      *****************************************************************
      *                                                               *
      *                   LINKAGE SECTION                             *
      *                                                               *
      *****************************************************************
       LINKAGE SECTION.
      *01 DFHCOMMAREA.
             COPY IDSV0003.

       01 AREA-IO-IVVS0212.
             COPY IVVV0212           REPLACING ==(SF)== BY ==IVVV0212==.

      *****************************************************************
      *                                                               *
      *                   PROCEDURE DIVISION                          *
      *                                                               *
      *****************************************************************
      *

       PROCEDURE DIVISION USING IDSV0003 AREA-IO-IVVS0212.

      *****************************************************************
      *                                                               *
      *                   1000-PRINCIPALE                             *
      *                                                               *
      *****************************************************************
       1000-PRINCIPALE.

           SET  NO-TROVATO                      TO TRUE
           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA  TO WS-COMPAGNIA
           MOVE IDSV0003-TIPO-MOVIMENTO         TO WS-MOVIMENTO
           SET  LEGGI-ELEMENTI                  TO TRUE
           SET  IDSV0003-SUCCESSFUL-RC          TO TRUE

           PERFORM 1100-RICERCA-AUT-OPER
              THRU 1100-RICERCA-AUT-OPER-EX
           VARYING IX-TAB-CAO FROM 1 BY 1
             UNTIL IX-TAB-CAO > 6
                OR IDSV0003-SQL-ERROR
                OR FINE-ELEMENTI

           IF NOT IDSV0003-SUCCESSFUL-RC
               MOVE WK-PGM             TO IDSV0003-COD-SERVIZIO-BE
               STRING 'ERRORE ESTRAZIONE DATI TABELLA CTRL-AUT-OPER '
                      '- CURSORE CAO-' WS-NUM-ACCESSO
                      '(' DESCRIZ-ERR-DB2 ')'
                   DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
               END-STRING
            END-IF.

       1000-PRINCIPALE-FINE.
           GOBACK.

      ******************************************************************
      *                                                                *
      *              RICERCA AUTONOMIA OPERATIVA                       *
      *                                                                *
      ******************************************************************
       1100-RICERCA-AUT-OPER.

           INITIALIZE CTRL-AUT-OPER

           MOVE IX-TAB-CAO                   TO WS-NUM-ACCESSO

           MOVE WS-COMPAGNIA                 TO CAO-COD-COMPAGNIA-ANIA
           MOVE WS-MOVIMENTO                 TO CAO-TP-MOVI

           MOVE IVVV0212-KEY-AUT-OPER1       TO CAO-KEY-AUT-OPER1
           MOVE IVVV0212-KEY-AUT-OPER2       TO CAO-KEY-AUT-OPER2
           MOVE IVVV0212-KEY-AUT-OPER3       TO CAO-KEY-AUT-OPER3
           MOVE IVVV0212-KEY-AUT-OPER4       TO CAO-KEY-AUT-OPER4
           MOVE IVVV0212-KEY-AUT-OPER5       TO CAO-KEY-AUT-OPER5

           PERFORM S1130-LENGTH-VCHAR-GER    THRU EX-S1130

           PERFORM S1100-OPEN-CURSOR         THRU EX-S1100

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM S1110-FETCH-NEXT       THRU EX-S1110
           END-IF.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM S2000-VALORIZZA-OUTPUT
                 THRU EX-S2000
                UNTIL FINE-ELEMENTI
                   OR IDSV0003-SQL-ERROR

           ELSE
             IF IDSV0003-NOT-FOUND
                PERFORM S1120-CLOSE-CURSOR   THRU EX-S1120
                IF IDSV0003-SUCCESSFUL-SQL
                   SET IDSV0003-NOT-FOUND    TO TRUE
                ELSE
                   SET IDSV0003-SQL-ERROR    TO TRUE
                END-IF
             ELSE
                SET IDSV0003-SQL-ERROR       TO TRUE
             END-IF
           END-IF.

       1100-RICERCA-AUT-OPER-EX.
           EXIT.

       S1130-LENGTH-VCHAR-GER.

           MOVE LENGTH OF CAO-PARAM-AUT-OPER
                       TO CAO-PARAM-AUT-OPER-LEN.

       EX-S1130.
           EXIT.


       S1100-OPEN-CURSOR.

           EVALUATE WS-NUM-ACCESSO

               WHEN 1
                   PERFORM OPEN-CURSORE-1
                      THRU OPEN-CURSORE-1-EX

               WHEN 2
                   PERFORM OPEN-CURSORE-2
                      THRU OPEN-CURSORE-2-EX

               WHEN 3
                   PERFORM OPEN-CURSORE-3
                      THRU OPEN-CURSORE-3-EX

               WHEN 4
                   PERFORM OPEN-CURSORE-4
                      THRU OPEN-CURSORE-4-EX

               WHEN 5
                   PERFORM OPEN-CURSORE-5
                      THRU OPEN-CURSORE-5-EX

               WHEN 6
                   PERFORM OPEN-CURSORE-6
                      THRU OPEN-CURSORE-6-EX

               WHEN OTHER
                   CONTINUE

           END-EVALUATE.

           MOVE SQLCODE               TO   IDSV0003-SQLCODE.

       EX-S1100.
           EXIT.


       S1110-FETCH-NEXT.
           EVALUATE WS-NUM-ACCESSO

               WHEN 1
                   PERFORM CAO-1-FETCH-NEXT
                      THRU CAO-1-FETCH-NEXT-EX

               WHEN 2
                   PERFORM CAO-2-FETCH-NEXT
                      THRU CAO-2-FETCH-NEXT-EX

               WHEN 3
                   PERFORM CAO-3-FETCH-NEXT
                      THRU CAO-3-FETCH-NEXT-EX

               WHEN 4
                   PERFORM CAO-4-FETCH-NEXT
                      THRU CAO-4-FETCH-NEXT-EX

               WHEN 5
                   PERFORM CAO-5-FETCH-NEXT
                      THRU CAO-5-FETCH-NEXT-EX

               WHEN 6
                   PERFORM CAO-6-FETCH-NEXT
                      THRU CAO-6-FETCH-NEXT-EX

               WHEN OTHER
                   CONTINUE

           END-EVALUATE.

           MOVE SQLCODE               TO   IDSV0003-SQLCODE.

       EX-S1110.
           EXIT.


       S1120-CLOSE-CURSOR.
           EVALUATE WS-NUM-ACCESSO

               WHEN 1
                   PERFORM CAO-1-CLOSE-CURSOR
                      THRU CAO-1-CLOSE-CURSOR-EX

               WHEN 2
                   PERFORM CAO-2-CLOSE-CURSOR
                      THRU CAO-2-CLOSE-CURSOR-EX

               WHEN 3
                   PERFORM CAO-3-CLOSE-CURSOR
                      THRU CAO-3-CLOSE-CURSOR-EX

               WHEN 4
                   PERFORM CAO-4-CLOSE-CURSOR
                      THRU CAO-4-CLOSE-CURSOR-EX

               WHEN 5
                   PERFORM CAO-5-CLOSE-CURSOR
                      THRU CAO-5-CLOSE-CURSOR-EX

               WHEN 6
                   PERFORM CAO-6-CLOSE-CURSOR
                      THRU CAO-6-CLOSE-CURSOR-EX

               WHEN OTHER
                   CONTINUE

           END-EVALUATE.

           MOVE SQLCODE               TO   IDSV0003-SQLCODE.

       EX-S1120.
           EXIT.

       S2000-VALORIZZA-OUTPUT.
           PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

           ADD 1  TO IX-AUT-OPER
           MOVE IX-AUT-OPER
              TO IVVV0212-ELE-CTRL-AUT-OPER-MAX
           MOVE CAO-COD-ERRORE
              TO IVVV0212-COD-ERRORE(IX-AUT-OPER)
           MOVE CAO-COD-LIV-AUT
              TO IVVV0212-COD-LIV-AUT(IX-AUT-OPER)
           MOVE CAO-TP-MOT-DEROGA
              TO IVVV0212-TP-MOT-DEROGA(IX-AUT-OPER)
           MOVE CAO-MODULO-VERIFICA
              TO IVVV0212-MOD-VERIFICA(IX-AUT-OPER)
           MOVE CAO-COD-COND
              TO IVVV0212-CODICE-CONDIZIONE(IX-AUT-OPER)
           MOVE CAO-PROG-COND
              TO IVVV0212-PROGRESS-CONDITION(IX-AUT-OPER)
           MOVE CAO-RISULT-COND
              TO IVVV0212-RISULTATO-CONDIZIONE(IX-AUT-OPER)

           ADD 1 TO IX-PARAM
           PERFORM E501-PREPARA-UNZIP-PARAM    THRU E501-EX
           PERFORM U999-UNZIP-STRING           THRU U999-EX
           PERFORM E502-CARICA-PARAM-UNZIPPED  THRU E502-EX.

           MOVE IX-PARAM
              TO IVVV0212-ELE-PARAM-MAX(IX-AUT-OPER)

           PERFORM S1110-FETCH-NEXT THRU EX-S1110

           IF IDSV0003-SUCCESSFUL-SQL
              SET LEGGI-ELEMENTI             TO TRUE
           ELSE
              IF IDSV0003-NOT-FOUND
                 SET FINE-ELEMENTI  TO TRUE
              ELSE
                SET IDSV0003-SQL-ERROR       TO TRUE
              END-IF
           END-IF.

       EX-S2000.
           EXIT.

      *----------------------------------------------------------------*
      *   PREPARA STRINGA DA UNZIPPARE
      *----------------------------------------------------------------*
       E501-PREPARA-UNZIP-PARAM.

ALEX  *     INITIALIZE UNZIP-STRUCTURE

           INITIALIZE              UNZIP-LENGTH-STR-MAX
                                   UNZIP-LENGTH-FIELD
                                   UNZIP-STRING-ZIPPED
                                   UNZIP-IDENTIFICATORE
                                   UNZIP-ELE-VARIABILI-MAX
                                   UNZIP-COD-VARIABILE(1).

           MOVE UNZIP-TAB       TO  UNZIP-RESTO-TAB.

           MOVE LENGTH OF CAO-PARAM-AUT-OPER TO UNZIP-LENGTH-STR-MAX
           MOVE LENGTH OF IVVV0212-COD-PARAM(IX-AUT-OPER, IX-PARAM)
                                              TO UNZIP-LENGTH-FIELD
           MOVE CAO-PARAM-AUT-OPER            TO UNZIP-STRING-ZIPPED
           MOVE ','                           TO UNZIP-IDENTIFICATORE.

       E501-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   ESTRAZIONE NOME VARIABILE DALLA STRINGA GLOBALVARLIST
      *----------------------------------------------------------------*
       U999-UNZIP-STRING.

      *--> IND-START-VAR  = INDICA IL PRIMO CHAR DI UNA VARIABILE(FISSO)
      *--> IND-END-VAR    = INDICA L'ULTIMO CHAR DI UNA VARIABILE(VAR)
      *--> IND-UNZIP      = E' IL CONTATORE DI VARIABILI TROVATE DA USARE
      *                     PER LA OCCURS 100 DI OUTPUT(AREA VARIABILI)
      *--> IND-CHAR       = INDICA LA LUNGHEZZA DI UNA VARIABILE
      *--> WK-GLOVAR-MAX

           MOVE 1                             TO IND-START-VAR.
           MOVE ZEROES                        TO IND-UNZIP
                                                 IND-CHAR.

           SET NO-FINE-VARLIST                TO TRUE.


           PERFORM VARYING IND-END-VAR FROM 1 BY 1
                     UNTIL IND-END-VAR > UNZIP-LENGTH-STR-MAX
                        OR SI-FINE-VARLIST

             IF UNZIP-STRING-ZIPPED(IND-END-VAR:1) =
                                             UNZIP-IDENTIFICATORE OR
                                             SPACES               OR
                                             LOW-VALUE            OR
                                             HIGH-VALUE

      *-->   E' STATA TROVATA UNA VARIABILE E VIENE CARICATA IN
      *-->   OUTPUT
                IF IND-CHAR > 0
                   ADD  1                         TO IND-UNZIP

                   MOVE UNZIP-STRING-ZIPPED(IND-START-VAR:IND-CHAR)
                     TO UNZIP-COD-VARIABILE(IND-UNZIP)
                                           (1:UNZIP-LENGTH-FIELD)

                   COMPUTE IND-START-VAR = IND-END-VAR + 1

                   MOVE ZEROES                          TO IND-CHAR

                   IF UNZIP-STRING-ZIPPED(IND-END-VAR:1) = SPACES   OR
                                                        LOW-VALUE   OR
                                                        HIGH-VALUE
                      SET SI-FINE-VARLIST               TO TRUE
                   END-IF
                END-IF

             ELSE
                ADD  1                     TO IND-CHAR
             END-IF

           END-PERFORM.

           MOVE IND-UNZIP                  TO UNZIP-ELE-VARIABILI-MAX.

       U999-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CARICA CAMPI UNZIPPED
      *----------------------------------------------------------------*
       E502-CARICA-PARAM-UNZIPPED.

           PERFORM VARYING IND-UNZIP FROM 1 BY 1
                   UNTIL IND-UNZIP  >= LIMITE-VARIABILI-UNZIPPED OR
                         IND-UNZIP  >  UNZIP-ELE-VARIABILI-MAX   OR
                         UNZIP-COD-VARIABILE(IND-UNZIP) =
                         SPACES OR LOW-VALUE OR HIGH-VALUE

                   MOVE IND-UNZIP  TO IX-PARAM
                   MOVE UNZIP-COD-VARIABILE(IND-UNZIP)
                                           (1:UNZIP-LENGTH-FIELD)
                     TO IVVV0212-COD-PARAM(IX-AUT-OPER, IX-PARAM)
                   MOVE SPACES
                     TO IVVV0212-VAL-PARAM(IX-AUT-OPER, IX-PARAM)

           END-PERFORM.

       E502-EX.
           EXIT.


       OPEN-CURSORE-1.
           EXEC SQL
                OPEN CAO-1
           END-EXEC.
       OPEN-CURSORE-1-EX.
           EXIT.

       OPEN-CURSORE-2.
           EXEC SQL
                OPEN CAO-2
           END-EXEC.
       OPEN-CURSORE-2-EX.
           EXIT.

       OPEN-CURSORE-3.
           EXEC SQL
                OPEN CAO-3
           END-EXEC.
       OPEN-CURSORE-3-EX.
           EXIT.

       OPEN-CURSORE-4.
           EXEC SQL
                OPEN CAO-4
           END-EXEC.
       OPEN-CURSORE-4-EX.
           EXIT.

       OPEN-CURSORE-5.
           EXEC SQL
                OPEN CAO-5
           END-EXEC.
       OPEN-CURSORE-5-EX.
           EXIT.

       OPEN-CURSORE-6.
           EXEC SQL
                OPEN CAO-6
           END-EXEC.
       OPEN-CURSORE-6-EX.
           EXIT.

       CAO-1-FETCH-NEXT.
           EXEC SQL
                FETCH CAO-1
             INTO
                :CAO-ID-CTRL-AUT-OPER
               ,:CAO-COD-COMPAGNIA-ANIA
               ,:CAO-TP-MOVI
                :IND-CAO-TP-MOVI
               ,:CAO-COD-ERRORE
               ,:CAO-KEY-AUT-OPER1
                :IND-CAO-KEY-AUT-OPER1
               ,:CAO-KEY-AUT-OPER2
                :IND-CAO-KEY-AUT-OPER2
               ,:CAO-KEY-AUT-OPER3
                :IND-CAO-KEY-AUT-OPER3
               ,:CAO-KEY-AUT-OPER4
                :IND-CAO-KEY-AUT-OPER4
               ,:CAO-KEY-AUT-OPER5
                :IND-CAO-KEY-AUT-OPER5
               ,:CAO-COD-LIV-AUT
               ,:CAO-TP-MOT-DEROGA
               ,:CAO-MODULO-VERIFICA
                :IND-CAO-MODULO-VERIFICA
               ,:CAO-COD-COND
                :IND-CAO-COD-COND
               ,:CAO-PROG-COND
                :IND-CAO-PROG-COND
               ,:CAO-RISULT-COND
                :IND-CAO-RISULT-COND
               ,:CAO-PARAM-AUT-OPER-VCHAR
                :IND-CAO-PARAM-AUT-OPER
               ,:CAO-STATO-ATTIVAZIONE
                :IND-CAO-STATO-ATTIVAZIONE
           END-EXEC.

       CAO-1-FETCH-NEXT-EX.
           EXIT.

       CAO-2-FETCH-NEXT.
           EXEC SQL
                FETCH CAO-2
             INTO
                :CAO-ID-CTRL-AUT-OPER
               ,:CAO-COD-COMPAGNIA-ANIA
               ,:CAO-TP-MOVI
                :IND-CAO-TP-MOVI
               ,:CAO-COD-ERRORE
               ,:CAO-KEY-AUT-OPER1
                :IND-CAO-KEY-AUT-OPER1
               ,:CAO-KEY-AUT-OPER2
                :IND-CAO-KEY-AUT-OPER2
               ,:CAO-KEY-AUT-OPER3
                :IND-CAO-KEY-AUT-OPER3
               ,:CAO-KEY-AUT-OPER4
                :IND-CAO-KEY-AUT-OPER4
               ,:CAO-KEY-AUT-OPER5
                :IND-CAO-KEY-AUT-OPER5
               ,:CAO-COD-LIV-AUT
               ,:CAO-TP-MOT-DEROGA
               ,:CAO-MODULO-VERIFICA
                :IND-CAO-MODULO-VERIFICA
               ,:CAO-COD-COND
                :IND-CAO-COD-COND
               ,:CAO-PROG-COND
                :IND-CAO-PROG-COND
               ,:CAO-RISULT-COND
                :IND-CAO-RISULT-COND
               ,:CAO-PARAM-AUT-OPER-VCHAR
                :IND-CAO-PARAM-AUT-OPER
               ,:CAO-STATO-ATTIVAZIONE
                :IND-CAO-STATO-ATTIVAZIONE
           END-EXEC.

           MOVE SQLCODE               TO   IDSV0003-SQLCODE.

       CAO-2-FETCH-NEXT-EX.
           EXIT.

       CAO-3-FETCH-NEXT.
           EXEC SQL
                FETCH CAO-3
             INTO
                :CAO-ID-CTRL-AUT-OPER
               ,:CAO-COD-COMPAGNIA-ANIA
               ,:CAO-TP-MOVI
                :IND-CAO-TP-MOVI
               ,:CAO-COD-ERRORE
               ,:CAO-KEY-AUT-OPER1
                :IND-CAO-KEY-AUT-OPER1
               ,:CAO-KEY-AUT-OPER2
                :IND-CAO-KEY-AUT-OPER2
               ,:CAO-KEY-AUT-OPER3
                :IND-CAO-KEY-AUT-OPER3
               ,:CAO-KEY-AUT-OPER4
                :IND-CAO-KEY-AUT-OPER4
               ,:CAO-KEY-AUT-OPER5
                :IND-CAO-KEY-AUT-OPER5
               ,:CAO-COD-LIV-AUT
               ,:CAO-TP-MOT-DEROGA
               ,:CAO-MODULO-VERIFICA
                :IND-CAO-MODULO-VERIFICA
               ,:CAO-COD-COND
                :IND-CAO-COD-COND
               ,:CAO-PROG-COND
                :IND-CAO-PROG-COND
               ,:CAO-RISULT-COND
                :IND-CAO-RISULT-COND
               ,:CAO-PARAM-AUT-OPER-VCHAR
                :IND-CAO-PARAM-AUT-OPER
               ,:CAO-STATO-ATTIVAZIONE
                :IND-CAO-STATO-ATTIVAZIONE
           END-EXEC.

           MOVE SQLCODE               TO   IDSV0003-SQLCODE.

       CAO-3-FETCH-NEXT-EX.
           EXIT.

       CAO-4-FETCH-NEXT.
           EXEC SQL
                FETCH CAO-4
             INTO
                :CAO-ID-CTRL-AUT-OPER
               ,:CAO-COD-COMPAGNIA-ANIA
               ,:CAO-TP-MOVI
                :IND-CAO-TP-MOVI
               ,:CAO-COD-ERRORE
               ,:CAO-KEY-AUT-OPER1
                :IND-CAO-KEY-AUT-OPER1
               ,:CAO-KEY-AUT-OPER2
                :IND-CAO-KEY-AUT-OPER2
               ,:CAO-KEY-AUT-OPER3
                :IND-CAO-KEY-AUT-OPER3
               ,:CAO-KEY-AUT-OPER4
                :IND-CAO-KEY-AUT-OPER4
               ,:CAO-KEY-AUT-OPER5
                :IND-CAO-KEY-AUT-OPER5
               ,:CAO-COD-LIV-AUT
               ,:CAO-TP-MOT-DEROGA
               ,:CAO-MODULO-VERIFICA
                :IND-CAO-MODULO-VERIFICA
               ,:CAO-COD-COND
                :IND-CAO-COD-COND
               ,:CAO-PROG-COND
                :IND-CAO-PROG-COND
               ,:CAO-RISULT-COND
                :IND-CAO-RISULT-COND
               ,:CAO-PARAM-AUT-OPER-VCHAR
                :IND-CAO-PARAM-AUT-OPER
               ,:CAO-STATO-ATTIVAZIONE
                :IND-CAO-STATO-ATTIVAZIONE
           END-EXEC.

           MOVE SQLCODE               TO   IDSV0003-SQLCODE.

       CAO-4-FETCH-NEXT-EX.
           EXIT.

       CAO-5-FETCH-NEXT.
           EXEC SQL
                FETCH CAO-5
             INTO
                :CAO-ID-CTRL-AUT-OPER
               ,:CAO-COD-COMPAGNIA-ANIA
               ,:CAO-TP-MOVI
                :IND-CAO-TP-MOVI
               ,:CAO-COD-ERRORE
               ,:CAO-KEY-AUT-OPER1
                :IND-CAO-KEY-AUT-OPER1
               ,:CAO-KEY-AUT-OPER2
                :IND-CAO-KEY-AUT-OPER2
               ,:CAO-KEY-AUT-OPER3
                :IND-CAO-KEY-AUT-OPER3
               ,:CAO-KEY-AUT-OPER4
                :IND-CAO-KEY-AUT-OPER4
               ,:CAO-KEY-AUT-OPER5
                :IND-CAO-KEY-AUT-OPER5
               ,:CAO-COD-LIV-AUT
               ,:CAO-TP-MOT-DEROGA
               ,:CAO-MODULO-VERIFICA
                :IND-CAO-MODULO-VERIFICA
               ,:CAO-COD-COND
                :IND-CAO-COD-COND
               ,:CAO-PROG-COND
                :IND-CAO-PROG-COND
               ,:CAO-RISULT-COND
                :IND-CAO-RISULT-COND
               ,:CAO-PARAM-AUT-OPER-VCHAR
                :IND-CAO-PARAM-AUT-OPER
               ,:CAO-STATO-ATTIVAZIONE
                :IND-CAO-STATO-ATTIVAZIONE
           END-EXEC.

           MOVE SQLCODE               TO   IDSV0003-SQLCODE.

       CAO-5-FETCH-NEXT-EX.
           EXIT.

       CAO-6-FETCH-NEXT.
           EXEC SQL
                FETCH CAO-6
             INTO
                :CAO-ID-CTRL-AUT-OPER
               ,:CAO-COD-COMPAGNIA-ANIA
               ,:CAO-TP-MOVI
                :IND-CAO-TP-MOVI
               ,:CAO-COD-ERRORE
               ,:CAO-KEY-AUT-OPER1
                :IND-CAO-KEY-AUT-OPER1
               ,:CAO-KEY-AUT-OPER2
                :IND-CAO-KEY-AUT-OPER2
               ,:CAO-KEY-AUT-OPER3
                :IND-CAO-KEY-AUT-OPER3
               ,:CAO-KEY-AUT-OPER4
                :IND-CAO-KEY-AUT-OPER4
               ,:CAO-KEY-AUT-OPER5
                :IND-CAO-KEY-AUT-OPER5
               ,:CAO-COD-LIV-AUT
               ,:CAO-TP-MOT-DEROGA
               ,:CAO-MODULO-VERIFICA
                :IND-CAO-MODULO-VERIFICA
               ,:CAO-COD-COND
                :IND-CAO-COD-COND
               ,:CAO-PROG-COND
                :IND-CAO-PROG-COND
               ,:CAO-RISULT-COND
                :IND-CAO-RISULT-COND
               ,:CAO-PARAM-AUT-OPER-VCHAR
                :IND-CAO-PARAM-AUT-OPER
               ,:CAO-STATO-ATTIVAZIONE
                :IND-CAO-STATO-ATTIVAZIONE
           END-EXEC.

           MOVE SQLCODE               TO   IDSV0003-SQLCODE.

       CAO-6-FETCH-NEXT-EX.
           EXIT.


       CAO-1-CLOSE-CURSOR.
           EXEC SQL
                CLOSE CAO-1
           END-EXEC.
       CAO-1-CLOSE-CURSOR-EX.
           EXIT.

       CAO-2-CLOSE-CURSOR.
           EXEC SQL
                CLOSE CAO-2
           END-EXEC.
       CAO-2-CLOSE-CURSOR-EX.
           EXIT.

       CAO-3-CLOSE-CURSOR.
           EXEC SQL
                CLOSE CAO-3
           END-EXEC.
       CAO-3-CLOSE-CURSOR-EX.
           EXIT.

       CAO-4-CLOSE-CURSOR.
           EXEC SQL
                CLOSE CAO-4
           END-EXEC.
       CAO-4-CLOSE-CURSOR-EX.
           EXIT.

       CAO-5-CLOSE-CURSOR.
           EXEC SQL
                CLOSE CAO-5
           END-EXEC.
       CAO-5-CLOSE-CURSOR-EX.
           EXIT.

       CAO-6-CLOSE-CURSOR.
           EXEC SQL
                CLOSE CAO-6
           END-EXEC.
       CAO-6-CLOSE-CURSOR-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-CAO-TP-MOVI = -1
              MOVE HIGH-VALUES TO CAO-TP-MOVI-NULL
           END-IF
           IF IND-CAO-KEY-AUT-OPER1 = -1
              MOVE HIGH-VALUES TO CAO-KEY-AUT-OPER1-NULL
           END-IF
           IF IND-CAO-KEY-AUT-OPER2 = -1
              MOVE HIGH-VALUES TO CAO-KEY-AUT-OPER2-NULL
           END-IF
           IF IND-CAO-KEY-AUT-OPER3 = -1
              MOVE HIGH-VALUES TO CAO-KEY-AUT-OPER3-NULL
           END-IF
           IF IND-CAO-KEY-AUT-OPER4 = -1
              MOVE HIGH-VALUES TO CAO-KEY-AUT-OPER4-NULL
           END-IF
           IF IND-CAO-KEY-AUT-OPER5 = -1
              MOVE HIGH-VALUES TO CAO-KEY-AUT-OPER5-NULL
           END-IF
           IF IND-CAO-MODULO-VERIFICA = -1
              MOVE HIGH-VALUES TO CAO-MODULO-VERIFICA-NULL
           END-IF
           IF IND-CAO-COD-COND = -1
              MOVE HIGH-VALUES TO CAO-COD-COND-NULL
           END-IF
           IF IND-CAO-PROG-COND = -1
              MOVE HIGH-VALUES TO CAO-PROG-COND-NULL
           END-IF
           IF IND-CAO-RISULT-COND = -1
              MOVE HIGH-VALUES TO CAO-RISULT-COND-NULL
           END-IF
           IF IND-CAO-PARAM-AUT-OPER = -1
              MOVE HIGH-VALUES TO CAO-PARAM-AUT-OPER
           END-IF
           IF IND-CAO-STATO-ATTIVAZIONE = -1
              MOVE HIGH-VALUES TO CAO-STATO-ATTIVAZIONE-NULL
           END-IF.

       Z100-EX.
           EXIT.


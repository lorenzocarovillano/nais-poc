       IDENTIFICATION DIVISION.
       PROGRAM-ID.        LCCS0010.
       AUTHOR.            SYNTAX VITA (TP).
       DATE-WRITTEN.           30 NOVEMBRE 1988.
      *� INIZIO MODIFICA 09/02/89
      *� MODIFICATA GESTIONE DIFFERENZA GIORNI (I MESI ANNO TUTTI 30 GG)
      *****************************************************************
      *            F U N Z I O N I   D E L   P R O G R A M M A        *
      *****************************************************************
      *   CALCOLO DELLA DIFFERENZA IN GIORNI O IN MESI TRA DUE DATE   *
      *                                                               *
      *      PARAMETRI DI INPUT :                                     *
      *      - FORMATO DELLE DATE  ---- 'G' (GGMMAAAA)                *
      *                            ---- 'A' (AAAAMMGG)                *
      *                            ---- 'M' (AAAAMMGG PER CALCOLO     *
      *                                      DIFFERENZA IN MESI)      *
      *      - DATA INFERIORE   E  DATA SUPERIORE                     *
      *                                                               *
      *      VALORI DI OUTPUT :                                       *
      *      - DIFFERNZA IN GIORNI                                    *
      *        O IN MESI                                              *
      *      - CODICE RITORNO      ---- '0' (ELABORAZIONE O.K.)       *
      *                            ---- '1' (ERRATO 'FORMATO')        *
      *                            ---- '2' (DATE NON NUMERICHE)      *
      *                            ---- '3' (DATE NON IN SEQUENZA)    *
      *                            ---- '4' (MESE INESISTENTE)        *
      *****************************************************************
      *                                                               *
      *  INFINE,DOPO AVER AGGIORNATO L'AREA COMUNE,IL CONTROLLO       *
      *  VIENE CEDUTO AL PROGRAMMA CHIAMANTE.                         *
      *                                                               *
      *****************************************************************
      *                                                               *
      *            R O U T I N E         B A T C H                    *
      *                                                               *
      *     C A L C O L O   D I F F E R E N Z A  T R A  D A T E       *
      *                                                               *
      *            ( PROGRAMMA COBOL CHIAMATO CON UNA CALL)           *
      *                                                               *
      *****************************************************************
       EJECT
       ENVIRONMENT             DIVISION.
       CONFIGURATION           SECTION.
       SOURCE-COMPUTER.        IBM-4341.
       OBJECT-COMPUTER.        IBM-4341.
       SKIP3
       DATA DIVISION.
       SKIP3
      *****************************************************************
      *                                                               *
      *      W O R K I N G - S T O R A G E   S E C T I O N            *
      *                                                               *
      *****************************************************************
       SKIP3
       WORKING-STORAGE SECTION.
       SKIP3
       01  FILLER           PIC X(30)         VALUE
           '***INIZIO WORKING-STORAGE***'.
       SKIP3
       77  RISUL               PIC 9(3).
       77  RESTO               PIC 9.
      *�   MODIFICA 09/02/89
       01  TAB-GIORNI          PIC X(24)      VALUE
           '303030303030303030303030'.
       01  FILLER              REDEFINES TAB-GIORNI.
           05 T-GG                  PIC 9(02)      OCCURS   12 TIMES.
       01  AMGINF.
           05 AAAA-I              PIC 9(04).
           05 MM-I                PIC 9(02).
           05 GG-I                PIC 9(02).
       01  AMGSUP.
           05 AAAA-S              PIC 9(04).
           05 MM-S                PIC 9(02).
           05 GG-S                PIC 9(02).
       01  AAAAMMGG.
           05 AAAA                PIC 9(04).
           05 MM                  PIC 9(02).
           05 GG                  PIC 9(02).
       EJECT
      *****************************************************************
      **                L I N K A G E    S E C T I O N               **
      *****************************************************************
       SKIP3
       LINKAGE SECTION.
       01  FORMATO                 PIC X(01).
       01  DATA-INFERIORE.
           05 GG-INF               PIC 9(02).
           05 MM-INF               PIC 9(02).
           05 AAAA-INF             PIC 9(04).
       01  DATA-SUPERIORE.
           05 GG-SUP               PIC 9(02).
           05 MM-SUP               PIC 9(02).
           05 AAAA-SUP             PIC 9(04).
       01  GG-DIFF                 PIC 9(05).
       01  CODICE-RITORNO          PIC X(01).
           EJECT
      *****************************************************************
      *                                                               *
      *             P R O C E D U R E   D I V I S I O N               *
      *                                                               *
      *****************************************************************
       SKIP3
       PROCEDURE DIVISION USING
                               FORMATO,
                               DATA-INFERIORE,
                               DATA-SUPERIORE,
                               GG-DIFF,
                               CODICE-RITORNO.
       INIZIO.
           MOVE        ZERO        TO  GG-DIFF
                                       CODICE-RITORNO.
           IF          FORMATO     NOT EQUAL   'G' AND 'A' AND 'M'
                                       MOVE    '1'  TO CODICE-RITORNO
                                       GO TO FINE.
           IF         (DATA-INFERIORE  NOT NUMERIC)
                OR    (DATA-SUPERIORE  NOT NUMERIC)
                                       MOVE     '2' TO CODICE-RITORNO
                                       GO TO FINE.
           IF          FORMATO         EQUAL     'A' OR 'M'
                       MOVE      DATA-INFERIORE  TO AMGINF
                       MOVE      DATA-SUPERIORE  TO AMGSUP
                 ELSE
                       MOVE      AAAA-INF        TO AAAA-I
                       MOVE      MM-INF          TO MM-I
                       MOVE      GG-INF          TO GG-I
                       MOVE      AAAA-SUP        TO AAAA-S
                       MOVE      MM-SUP          TO MM-S
                       MOVE      GG-SUP          TO GG-S.
           IF          AMGINF          GREATER   AMGSUP
                                       MOVE    '3'   TO CODICE-RITORNO
                                       GO TO FINE.
           IF          MM-I              LESS     1
                OR     MM-I              GREATER 12
                OR     MM-S              LESS     1
                OR     MM-S              GREATER 12
                                       MOVE    '4' TO CODICE-RITORNO
                                       GO TO FINE.
           IF          AMGINF          EQUAL   AMGSUP
                                       GO TO     FINE.
      *� INIZIO MODIFICA 09/02/89
RAVA       IF   GG-I    =   28           AND
 "              GG-S    =   28
 "              GO TO FINE-TEST.
 "         IF   GG-I    =   29           AND
 "              GG-S    =   29
RAVA            GO TO FINE-TEST.
           IF   (GG-I    =   28 OR 29)   AND   MM-I   =  02
                  MOVE  30     TO GG-I.
           IF    GG-I    =       31
                  MOVE  30     TO GG-I.
           IF   (GG-S    =   28 OR 29)   AND   MM-S   =  02
                  MOVE  30     TO GG-S.
           IF    GG-S    =       31
                  MOVE  30     TO GG-S.
      *� FINE MODIFICA 09/02/89
       FINE-TEST.
           IF FORMATO  EQUAL  'M'
             PERFORM  CALCOLO-MESI
             GO TO FINE.
           EJECT
       TEST-ANNI.
           IF          AAAA-I              EQUAL   AAAA-S
             GO TO IMPOSTA-DATA-SUP.
           MOVE        12          TO  MM.
           MOVE        30          TO  GG.
           PERFORM     CALCOLO-GIORNI.
           ADD         1           TO  AAAA-I.
           MOVE        01          TO  MM-I.
           MOVE        ZERO        TO  GG-I.
           GO TO       TEST-ANNI.
           SKIP3
       IMPOSTA-DATA-SUP.
           MOVE        AMGSUP      TO  AAAAMMGG.
           PERFORM     CALCOLO-GIORNI.
           GO TO FINE.
           SKIP3
       CALCOLO-GIORNI SECTION.
           IF          MM-I              EQUAL   MM
              COMPUTE GG-DIFF = GG-DIFF + (GG - GG-I)
              GO TO   CALCOLO-GIORNI-999.
           COMPUTE     GG-DIFF  =  GG-DIFF + (T-GG (MM-I) - GG-I).
           ADD         1            TO  MM-I.
           MOVE        ZERO         TO  GG-I.
           GO TO       CALCOLO-GIORNI.
       CALCOLO-GIORNI-999.
           EXIT.
           SKIP3
       CALCOLO-MESI   SECTION.
           IF          AAAA-S            GREATER AAAA-I
              COMPUTE GG-DIFF = GG-DIFF + 12
              COMPUTE AAAA-S = AAAA-S - 1
              GO TO   CALCOLO-MESI.
           COMPUTE     GG-DIFF      =  GG-DIFF + (MM-S - MM-I).
       CALCOLO-MESI-999.
           EXIT.
           SKIP3
       FINE SECTION.
           GOBACK.


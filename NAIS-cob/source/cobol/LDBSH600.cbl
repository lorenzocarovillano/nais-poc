       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LDBSH600 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  12 NOV 2019.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2     PIC X(300)  VALUE SPACES.

       77 WS-BUFFER-TABLE     PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ      PIC 9(009)  VALUE ZEROES.

           EXEC SQL INCLUDE IDSV0010 END-EXEC.

      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDPMO0 END-EXEC.
           EXEC SQL INCLUDE IDBVPMO2 END-EXEC.
           EXEC SQL INCLUDE IDBVPMO3 END-EXEC.
      *
      *
           EXEC SQL INCLUDE LDBVH601 END-EXEC.
      *
      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVPMO1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 PARAM-MOVI.

           MOVE IDSV0003-BUFFER-WHERE-COND TO LDBVH601.

           PERFORM A000-INIZIO              THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-WHERE-CONDITION
                       PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-WHERE-CONDITION
                         PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-WHERE-CONDITION
                           PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                      SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           MOVE LDBVH601 TO IDSV0003-BUFFER-WHERE-COND.

           GOBACK.

       A000-INIZIO.
           MOVE 'LDBSH600'              TO   IDSV0003-COD-SERVIZIO-BE.
           MOVE 'PARAM-MOVI' TO   IDSV0003-NOME-TABELLA.

           MOVE '00'                      TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                    TO   IDSV0003-SQLCODE
                                               IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
                                               IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.

       A200-ELABORA-WC-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.

       B200-ELABORA-WC-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B200-EX.
           EXIT.

       C200-ELABORA-WC-NST.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       C200-EX.
           EXIT.
      *----
      *----  gestione WC Effetto
      *----
       A205-DECLARE-CURSOR-WC-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.


           EXEC SQL
              DECLARE C-EFF CURSOR FOR
              SELECT
                     ID_PARAM_MOVI
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_MOVI
                    ,FRQ_MOVI
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,DT_RICOR_PREC
                    ,DT_RICOR_SUCC
                    ,PC_INTR_FRAZ
                    ,IMP_BNS_DA_SCO_TOT
                    ,IMP_BNS_DA_SCO
                    ,PC_ANTIC_BNS
                    ,TP_RINN_COLL
                    ,TP_RIVAL_PRE
                    ,TP_RIVAL_PRSTZ
                    ,FL_EVID_RIVAL
                    ,ULT_PC_PERD
                    ,TOT_AA_GIA_PROR
                    ,TP_OPZ
                    ,AA_REN_CER
                    ,PC_REVRSB
                    ,IMP_RISC_PARZ_PRGT
                    ,IMP_LRD_DI_RAT
                    ,IB_OGG
                    ,COS_ONER
                    ,SPE_PC
                    ,FL_ATTIV_GAR
                    ,CAMBIO_VER_PROD
                    ,MM_DIFF
                    ,IMP_RAT_MANFEE
                    ,DT_ULT_EROG_MANFEE
                    ,TP_OGG_RIVAL
                    ,SOM_ASSTA_GARAC
                    ,PC_APPLZ_OPZ
                    ,ID_ADES
                    ,ID_POLI
                    ,TP_FRM_ASSVA
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,TP_ESTR_CNT
                    ,COD_RAMO
                    ,GEN_DA_SIN
                    ,COD_TARI
                    ,NUM_RAT_PAG_PRE
                    ,PC_SERV_VAL
                    ,ETA_AA_SOGL_BNFICR
              FROM PARAM_MOVI
              WHERE      ID_ADES = :LDBVH601-ID-ADES
                    AND TP_MOVI IN
                    (:LDBVH601-TP-MOVI-1
                     ,:LDBVH601-TP-MOVI-2
                     ,:LDBVH601-TP-MOVI-3
                     ,:LDBVH601-TP-MOVI-4
                     ,:LDBVH601-TP-MOVI-5
                     ,:LDBVH601-TP-MOVI-6
                     ,:LDBVH601-TP-MOVI-7
                     ,:LDBVH601-TP-MOVI-8
                     ,:LDBVH601-TP-MOVI-9
                     ,:LDBVH601-TP-MOVI-10
                     ,:LDBVH601-TP-MOVI-11
                     ,:LDBVH601-TP-MOVI-12
                     ,:LDBVH601-TP-MOVI-13
                     ,:LDBVH601-TP-MOVI-14
                     ,:LDBVH601-TP-MOVI-15)
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

       A205-EX.
           EXIT.

       A210-SELECT-WC-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A210-EX.
           EXIT.

       A260-OPEN-CURSOR-WC-EFF.
           PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.

           EXEC SQL
                OPEN C-EFF
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A260-EX.
           EXIT.

       A270-CLOSE-CURSOR-WC-EFF.
           EXEC SQL
                CLOSE C-EFF
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A270-EX.
           EXIT.

       A280-FETCH-FIRST-WC-EFF.
           PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
           END-IF.
       A280-EX.
           EXIT.

       A290-FETCH-NEXT-WC-EFF.
           EXEC SQL
                FETCH C-EFF
           INTO
                :PMO-ID-PARAM-MOVI
               ,:PMO-ID-OGG
               ,:PMO-TP-OGG
               ,:PMO-ID-MOVI-CRZ
               ,:PMO-ID-MOVI-CHIU
                :IND-PMO-ID-MOVI-CHIU
               ,:PMO-DT-INI-EFF-DB
               ,:PMO-DT-END-EFF-DB
               ,:PMO-COD-COMP-ANIA
               ,:PMO-TP-MOVI
                :IND-PMO-TP-MOVI
               ,:PMO-FRQ-MOVI
                :IND-PMO-FRQ-MOVI
               ,:PMO-DUR-AA
                :IND-PMO-DUR-AA
               ,:PMO-DUR-MM
                :IND-PMO-DUR-MM
               ,:PMO-DUR-GG
                :IND-PMO-DUR-GG
               ,:PMO-DT-RICOR-PREC-DB
                :IND-PMO-DT-RICOR-PREC
               ,:PMO-DT-RICOR-SUCC-DB
                :IND-PMO-DT-RICOR-SUCC
               ,:PMO-PC-INTR-FRAZ
                :IND-PMO-PC-INTR-FRAZ
               ,:PMO-IMP-BNS-DA-SCO-TOT
                :IND-PMO-IMP-BNS-DA-SCO-TOT
               ,:PMO-IMP-BNS-DA-SCO
                :IND-PMO-IMP-BNS-DA-SCO
               ,:PMO-PC-ANTIC-BNS
                :IND-PMO-PC-ANTIC-BNS
               ,:PMO-TP-RINN-COLL
                :IND-PMO-TP-RINN-COLL
               ,:PMO-TP-RIVAL-PRE
                :IND-PMO-TP-RIVAL-PRE
               ,:PMO-TP-RIVAL-PRSTZ
                :IND-PMO-TP-RIVAL-PRSTZ
               ,:PMO-FL-EVID-RIVAL
                :IND-PMO-FL-EVID-RIVAL
               ,:PMO-ULT-PC-PERD
                :IND-PMO-ULT-PC-PERD
               ,:PMO-TOT-AA-GIA-PROR
                :IND-PMO-TOT-AA-GIA-PROR
               ,:PMO-TP-OPZ
                :IND-PMO-TP-OPZ
               ,:PMO-AA-REN-CER
                :IND-PMO-AA-REN-CER
               ,:PMO-PC-REVRSB
                :IND-PMO-PC-REVRSB
               ,:PMO-IMP-RISC-PARZ-PRGT
                :IND-PMO-IMP-RISC-PARZ-PRGT
               ,:PMO-IMP-LRD-DI-RAT
                :IND-PMO-IMP-LRD-DI-RAT
               ,:PMO-IB-OGG
                :IND-PMO-IB-OGG
               ,:PMO-COS-ONER
                :IND-PMO-COS-ONER
               ,:PMO-SPE-PC
                :IND-PMO-SPE-PC
               ,:PMO-FL-ATTIV-GAR
                :IND-PMO-FL-ATTIV-GAR
               ,:PMO-CAMBIO-VER-PROD
                :IND-PMO-CAMBIO-VER-PROD
               ,:PMO-MM-DIFF
                :IND-PMO-MM-DIFF
               ,:PMO-IMP-RAT-MANFEE
                :IND-PMO-IMP-RAT-MANFEE
               ,:PMO-DT-ULT-EROG-MANFEE-DB
                :IND-PMO-DT-ULT-EROG-MANFEE
               ,:PMO-TP-OGG-RIVAL
                :IND-PMO-TP-OGG-RIVAL
               ,:PMO-SOM-ASSTA-GARAC
                :IND-PMO-SOM-ASSTA-GARAC
               ,:PMO-PC-APPLZ-OPZ
                :IND-PMO-PC-APPLZ-OPZ
               ,:PMO-ID-ADES
                :IND-PMO-ID-ADES
               ,:PMO-ID-POLI
               ,:PMO-TP-FRM-ASSVA
               ,:PMO-DS-RIGA
               ,:PMO-DS-OPER-SQL
               ,:PMO-DS-VER
               ,:PMO-DS-TS-INI-CPTZ
               ,:PMO-DS-TS-END-CPTZ
               ,:PMO-DS-UTENTE
               ,:PMO-DS-STATO-ELAB
               ,:PMO-TP-ESTR-CNT
                :IND-PMO-TP-ESTR-CNT
               ,:PMO-COD-RAMO
                :IND-PMO-COD-RAMO
               ,:PMO-GEN-DA-SIN
                :IND-PMO-GEN-DA-SIN
               ,:PMO-COD-TARI
                :IND-PMO-COD-TARI
               ,:PMO-NUM-RAT-PAG-PRE
                :IND-PMO-NUM-RAT-PAG-PRE
               ,:PMO-PC-SERV-VAL
                :IND-PMO-PC-SERV-VAL
               ,:PMO-ETA-AA-SOGL-BNFICR
                :IND-PMO-ETA-AA-SOGL-BNFICR

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A290-EX.
           EXIT.
      *----
      *----  gestione WC Competenza
      *----
       B205-DECLARE-CURSOR-WC-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           EXEC SQL
              DECLARE C-CPZ CURSOR FOR
              SELECT
                     ID_PARAM_MOVI
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_MOVI
                    ,FRQ_MOVI
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,DT_RICOR_PREC
                    ,DT_RICOR_SUCC
                    ,PC_INTR_FRAZ
                    ,IMP_BNS_DA_SCO_TOT
                    ,IMP_BNS_DA_SCO
                    ,PC_ANTIC_BNS
                    ,TP_RINN_COLL
                    ,TP_RIVAL_PRE
                    ,TP_RIVAL_PRSTZ
                    ,FL_EVID_RIVAL
                    ,ULT_PC_PERD
                    ,TOT_AA_GIA_PROR
                    ,TP_OPZ
                    ,AA_REN_CER
                    ,PC_REVRSB
                    ,IMP_RISC_PARZ_PRGT
                    ,IMP_LRD_DI_RAT
                    ,IB_OGG
                    ,COS_ONER
                    ,SPE_PC
                    ,FL_ATTIV_GAR
                    ,CAMBIO_VER_PROD
                    ,MM_DIFF
                    ,IMP_RAT_MANFEE
                    ,DT_ULT_EROG_MANFEE
                    ,TP_OGG_RIVAL
                    ,SOM_ASSTA_GARAC
                    ,PC_APPLZ_OPZ
                    ,ID_ADES
                    ,ID_POLI
                    ,TP_FRM_ASSVA
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,TP_ESTR_CNT
                    ,COD_RAMO
                    ,GEN_DA_SIN
                    ,COD_TARI
                    ,NUM_RAT_PAG_PRE
                    ,PC_SERV_VAL
                    ,ETA_AA_SOGL_BNFICR
              FROM PARAM_MOVI
              WHERE      ID_ADES = :LDBVH601-ID-ADES
                        AND TP_MOVI IN
                        (:LDBVH601-TP-MOVI-1
                         ,:LDBVH601-TP-MOVI-2
                         ,:LDBVH601-TP-MOVI-3
                         ,:LDBVH601-TP-MOVI-4
                         ,:LDBVH601-TP-MOVI-5
                         ,:LDBVH601-TP-MOVI-6
                         ,:LDBVH601-TP-MOVI-7
                         ,:LDBVH601-TP-MOVI-8
                         ,:LDBVH601-TP-MOVI-9
                         ,:LDBVH601-TP-MOVI-10
                         ,:LDBVH601-TP-MOVI-11
                         ,:LDBVH601-TP-MOVI-12
                         ,:LDBVH601-TP-MOVI-13
                         ,:LDBVH601-TP-MOVI-14
                         ,:LDBVH601-TP-MOVI-15)
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

       B205-EX.
           EXIT.

       B210-SELECT-WC-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B210-EX.
           EXIT.

       B260-OPEN-CURSOR-WC-CPZ.
           PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.

           EXEC SQL
                OPEN C-CPZ
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B260-EX.
           EXIT.

       B270-CLOSE-CURSOR-WC-CPZ.
           EXEC SQL
                CLOSE C-CPZ
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B270-EX.
           EXIT.

       B280-FETCH-FIRST-WC-CPZ.
           PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
           END-IF.
       B280-EX.
           EXIT.

       B290-FETCH-NEXT-WC-CPZ.
           EXEC SQL
                FETCH C-CPZ
           INTO
                :PMO-ID-PARAM-MOVI
               ,:PMO-ID-OGG
               ,:PMO-TP-OGG
               ,:PMO-ID-MOVI-CRZ
               ,:PMO-ID-MOVI-CHIU
                :IND-PMO-ID-MOVI-CHIU
               ,:PMO-DT-INI-EFF-DB
               ,:PMO-DT-END-EFF-DB
               ,:PMO-COD-COMP-ANIA
               ,:PMO-TP-MOVI
                :IND-PMO-TP-MOVI
               ,:PMO-FRQ-MOVI
                :IND-PMO-FRQ-MOVI
               ,:PMO-DUR-AA
                :IND-PMO-DUR-AA
               ,:PMO-DUR-MM
                :IND-PMO-DUR-MM
               ,:PMO-DUR-GG
                :IND-PMO-DUR-GG
               ,:PMO-DT-RICOR-PREC-DB
                :IND-PMO-DT-RICOR-PREC
               ,:PMO-DT-RICOR-SUCC-DB
                :IND-PMO-DT-RICOR-SUCC
               ,:PMO-PC-INTR-FRAZ
                :IND-PMO-PC-INTR-FRAZ
               ,:PMO-IMP-BNS-DA-SCO-TOT
                :IND-PMO-IMP-BNS-DA-SCO-TOT
               ,:PMO-IMP-BNS-DA-SCO
                :IND-PMO-IMP-BNS-DA-SCO
               ,:PMO-PC-ANTIC-BNS
                :IND-PMO-PC-ANTIC-BNS
               ,:PMO-TP-RINN-COLL
                :IND-PMO-TP-RINN-COLL
               ,:PMO-TP-RIVAL-PRE
                :IND-PMO-TP-RIVAL-PRE
               ,:PMO-TP-RIVAL-PRSTZ
                :IND-PMO-TP-RIVAL-PRSTZ
               ,:PMO-FL-EVID-RIVAL
                :IND-PMO-FL-EVID-RIVAL
               ,:PMO-ULT-PC-PERD
                :IND-PMO-ULT-PC-PERD
               ,:PMO-TOT-AA-GIA-PROR
                :IND-PMO-TOT-AA-GIA-PROR
               ,:PMO-TP-OPZ
                :IND-PMO-TP-OPZ
               ,:PMO-AA-REN-CER
                :IND-PMO-AA-REN-CER
               ,:PMO-PC-REVRSB
                :IND-PMO-PC-REVRSB
               ,:PMO-IMP-RISC-PARZ-PRGT
                :IND-PMO-IMP-RISC-PARZ-PRGT
               ,:PMO-IMP-LRD-DI-RAT
                :IND-PMO-IMP-LRD-DI-RAT
               ,:PMO-IB-OGG
                :IND-PMO-IB-OGG
               ,:PMO-COS-ONER
                :IND-PMO-COS-ONER
               ,:PMO-SPE-PC
                :IND-PMO-SPE-PC
               ,:PMO-FL-ATTIV-GAR
                :IND-PMO-FL-ATTIV-GAR
               ,:PMO-CAMBIO-VER-PROD
                :IND-PMO-CAMBIO-VER-PROD
               ,:PMO-MM-DIFF
                :IND-PMO-MM-DIFF
               ,:PMO-IMP-RAT-MANFEE
                :IND-PMO-IMP-RAT-MANFEE
               ,:PMO-DT-ULT-EROG-MANFEE-DB
                :IND-PMO-DT-ULT-EROG-MANFEE
               ,:PMO-TP-OGG-RIVAL
                :IND-PMO-TP-OGG-RIVAL
               ,:PMO-SOM-ASSTA-GARAC
                :IND-PMO-SOM-ASSTA-GARAC
               ,:PMO-PC-APPLZ-OPZ
                :IND-PMO-PC-APPLZ-OPZ
               ,:PMO-ID-ADES
                :IND-PMO-ID-ADES
               ,:PMO-ID-POLI
               ,:PMO-TP-FRM-ASSVA
               ,:PMO-DS-RIGA
               ,:PMO-DS-OPER-SQL
               ,:PMO-DS-VER
               ,:PMO-DS-TS-INI-CPTZ
               ,:PMO-DS-TS-END-CPTZ
               ,:PMO-DS-UTENTE
               ,:PMO-DS-STATO-ELAB
               ,:PMO-TP-ESTR-CNT
                :IND-PMO-TP-ESTR-CNT
               ,:PMO-COD-RAMO
                :IND-PMO-COD-RAMO
               ,:PMO-GEN-DA-SIN
                :IND-PMO-GEN-DA-SIN
               ,:PMO-COD-TARI
                :IND-PMO-COD-TARI
               ,:PMO-NUM-RAT-PAG-PRE
                :IND-PMO-NUM-RAT-PAG-PRE
               ,:PMO-PC-SERV-VAL
                :IND-PMO-PC-SERV-VAL
               ,:PMO-ETA-AA-SOGL-BNFICR
                :IND-PMO-ETA-AA-SOGL-BNFICR

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B290-EX.
           EXIT.
      *----
      *----  gestione WC Senza Storicit{
      *----
       C205-DECLARE-CURSOR-WC-NST.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       C205-EX.
           EXIT.

       C210-SELECT-WC-NST.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       C210-EX.
           EXIT.

       C260-OPEN-CURSOR-WC-NST.
           PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       C260-EX.
           EXIT.

       C270-CLOSE-CURSOR-WC-NST.
           SET IDSV0003-INVALID-OPER TO TRUE.
       C270-EX.
           EXIT.

       C280-FETCH-FIRST-WC-NST.
           PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
           END-IF.
       C280-EX.
           EXIT.

       C290-FETCH-NEXT-WC-NST.
           SET IDSV0003-INVALID-OPER TO TRUE.
       C290-EX.
           EXIT.
      *----
      *----  utilit{ comuni a tutti i livelli operazione
      *----
       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-PMO-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO PMO-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-PMO-TP-MOVI = -1
              MOVE HIGH-VALUES TO PMO-TP-MOVI-NULL
           END-IF
           IF IND-PMO-FRQ-MOVI = -1
              MOVE HIGH-VALUES TO PMO-FRQ-MOVI-NULL
           END-IF
           IF IND-PMO-DUR-AA = -1
              MOVE HIGH-VALUES TO PMO-DUR-AA-NULL
           END-IF
           IF IND-PMO-DUR-MM = -1
              MOVE HIGH-VALUES TO PMO-DUR-MM-NULL
           END-IF
           IF IND-PMO-DUR-GG = -1
              MOVE HIGH-VALUES TO PMO-DUR-GG-NULL
           END-IF
           IF IND-PMO-DT-RICOR-PREC = -1
              MOVE HIGH-VALUES TO PMO-DT-RICOR-PREC-NULL
           END-IF
           IF IND-PMO-DT-RICOR-SUCC = -1
              MOVE HIGH-VALUES TO PMO-DT-RICOR-SUCC-NULL
           END-IF
           IF IND-PMO-PC-INTR-FRAZ = -1
              MOVE HIGH-VALUES TO PMO-PC-INTR-FRAZ-NULL
           END-IF
           IF IND-PMO-IMP-BNS-DA-SCO-TOT = -1
              MOVE HIGH-VALUES TO PMO-IMP-BNS-DA-SCO-TOT-NULL
           END-IF
           IF IND-PMO-IMP-BNS-DA-SCO = -1
              MOVE HIGH-VALUES TO PMO-IMP-BNS-DA-SCO-NULL
           END-IF
           IF IND-PMO-PC-ANTIC-BNS = -1
              MOVE HIGH-VALUES TO PMO-PC-ANTIC-BNS-NULL
           END-IF
           IF IND-PMO-TP-RINN-COLL = -1
              MOVE HIGH-VALUES TO PMO-TP-RINN-COLL-NULL
           END-IF
           IF IND-PMO-TP-RIVAL-PRE = -1
              MOVE HIGH-VALUES TO PMO-TP-RIVAL-PRE-NULL
           END-IF
           IF IND-PMO-TP-RIVAL-PRSTZ = -1
              MOVE HIGH-VALUES TO PMO-TP-RIVAL-PRSTZ-NULL
           END-IF
           IF IND-PMO-FL-EVID-RIVAL = -1
              MOVE HIGH-VALUES TO PMO-FL-EVID-RIVAL-NULL
           END-IF
           IF IND-PMO-ULT-PC-PERD = -1
              MOVE HIGH-VALUES TO PMO-ULT-PC-PERD-NULL
           END-IF
           IF IND-PMO-TOT-AA-GIA-PROR = -1
              MOVE HIGH-VALUES TO PMO-TOT-AA-GIA-PROR-NULL
           END-IF
           IF IND-PMO-TP-OPZ = -1
              MOVE HIGH-VALUES TO PMO-TP-OPZ-NULL
           END-IF
           IF IND-PMO-AA-REN-CER = -1
              MOVE HIGH-VALUES TO PMO-AA-REN-CER-NULL
           END-IF
           IF IND-PMO-PC-REVRSB = -1
              MOVE HIGH-VALUES TO PMO-PC-REVRSB-NULL
           END-IF
           IF IND-PMO-IMP-RISC-PARZ-PRGT = -1
              MOVE HIGH-VALUES TO PMO-IMP-RISC-PARZ-PRGT-NULL
           END-IF
           IF IND-PMO-IMP-LRD-DI-RAT = -1
              MOVE HIGH-VALUES TO PMO-IMP-LRD-DI-RAT-NULL
           END-IF
           IF IND-PMO-IB-OGG = -1
              MOVE HIGH-VALUES TO PMO-IB-OGG-NULL
           END-IF
           IF IND-PMO-COS-ONER = -1
              MOVE HIGH-VALUES TO PMO-COS-ONER-NULL
           END-IF
           IF IND-PMO-SPE-PC = -1
              MOVE HIGH-VALUES TO PMO-SPE-PC-NULL
           END-IF
           IF IND-PMO-FL-ATTIV-GAR = -1
              MOVE HIGH-VALUES TO PMO-FL-ATTIV-GAR-NULL
           END-IF
           IF IND-PMO-CAMBIO-VER-PROD = -1
              MOVE HIGH-VALUES TO PMO-CAMBIO-VER-PROD-NULL
           END-IF
           IF IND-PMO-MM-DIFF = -1
              MOVE HIGH-VALUES TO PMO-MM-DIFF-NULL
           END-IF
           IF IND-PMO-IMP-RAT-MANFEE = -1
              MOVE HIGH-VALUES TO PMO-IMP-RAT-MANFEE-NULL
           END-IF
           IF IND-PMO-DT-ULT-EROG-MANFEE = -1
              MOVE HIGH-VALUES TO PMO-DT-ULT-EROG-MANFEE-NULL
           END-IF
           IF IND-PMO-TP-OGG-RIVAL = -1
              MOVE HIGH-VALUES TO PMO-TP-OGG-RIVAL-NULL
           END-IF
           IF IND-PMO-SOM-ASSTA-GARAC = -1
              MOVE HIGH-VALUES TO PMO-SOM-ASSTA-GARAC-NULL
           END-IF
           IF IND-PMO-PC-APPLZ-OPZ = -1
              MOVE HIGH-VALUES TO PMO-PC-APPLZ-OPZ-NULL
           END-IF
           IF IND-PMO-ID-ADES = -1
              MOVE HIGH-VALUES TO PMO-ID-ADES-NULL
           END-IF
           IF IND-PMO-TP-ESTR-CNT = -1
              MOVE HIGH-VALUES TO PMO-TP-ESTR-CNT-NULL
           END-IF
           IF IND-PMO-COD-RAMO = -1
              MOVE HIGH-VALUES TO PMO-COD-RAMO-NULL
           END-IF
           IF IND-PMO-GEN-DA-SIN = -1
              MOVE HIGH-VALUES TO PMO-GEN-DA-SIN-NULL
           END-IF
           IF IND-PMO-COD-TARI = -1
              MOVE HIGH-VALUES TO PMO-COD-TARI-NULL
           END-IF
           IF IND-PMO-NUM-RAT-PAG-PRE = -1
              MOVE HIGH-VALUES TO PMO-NUM-RAT-PAG-PRE-NULL
           END-IF
           IF IND-PMO-PC-SERV-VAL = -1
              MOVE HIGH-VALUES TO PMO-PC-SERV-VAL-NULL
           END-IF
           IF IND-PMO-ETA-AA-SOGL-BNFICR = -1
              MOVE HIGH-VALUES TO PMO-ETA-AA-SOGL-BNFICR-NULL
           END-IF.

       Z100-EX.
           EXIT.

      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE PMO-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO PMO-DT-INI-EFF-DB
           MOVE PMO-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO PMO-DT-END-EFF-DB
           IF IND-PMO-DT-RICOR-PREC = 0
               MOVE PMO-DT-RICOR-PREC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PMO-DT-RICOR-PREC-DB
           END-IF
           IF IND-PMO-DT-RICOR-SUCC = 0
               MOVE PMO-DT-RICOR-SUCC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PMO-DT-RICOR-SUCC-DB
           END-IF
           IF IND-PMO-DT-ULT-EROG-MANFEE = 0
               MOVE PMO-DT-ULT-EROG-MANFEE TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PMO-DT-ULT-EROG-MANFEE-DB
           END-IF.

       Z900-EX.
           EXIT.

      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE PMO-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO PMO-DT-INI-EFF
           MOVE PMO-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO PMO-DT-END-EFF
           IF IND-PMO-DT-RICOR-PREC = 0
               MOVE PMO-DT-RICOR-PREC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PMO-DT-RICOR-PREC
           END-IF
           IF IND-PMO-DT-RICOR-SUCC = 0
               MOVE PMO-DT-RICOR-SUCC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PMO-DT-RICOR-SUCC
           END-IF
           IF IND-PMO-DT-ULT-EROG-MANFEE = 0
               MOVE PMO-DT-ULT-EROG-MANFEE-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PMO-DT-ULT-EROG-MANFEE
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----
      *----  prevede statements AD HOC PRE Query
      *----
       Z970-CODICE-ADHOC-PRE.

           CONTINUE.
       Z970-EX.
           EXIT.
      *----
      *----  prevede statements AD HOC POST Query
      *----
       Z980-CODICE-ADHOC-POST.

           CONTINUE.
       Z980-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

      *  ============================================================= *
      *                                                                *
      *        PORTAFOGLIO VITA ITALIA  VER 1.0                        *
      *                                                                *
      *  ============================================================= *
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LOAS0660.
       AUTHOR.             AIS&S.
       DATE-WRITTEN.       GENNAIO 2009.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *     PROGRAMMA ..... LOAS0660                                   *
      *     TIPOLOGIA...... OPERAZIONI AUTOMATICHE                     *
      *     PROCESSO....... ADEGUAMENTO PREMIO PRESTAZIONE             *
      *     FUNZIONE....... BATCH                                      *
      *     DESCRIZIONE.... SERVIZIO DI CALCOLO ADEGUAMENTO PREMIO     *
      *                     PRESTAZIONE                                *
      *     PAGINA WEB..... N.A.                                       *
      *----------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
      *
       DATA DIVISION.
      *
       WORKING-STORAGE SECTION.
      *
      *
      *----------------------------------------------------------------*
      *     COPY DISPATCHER
      *----------------------------------------------------------------*
      *
       01  IDSV0012.
           COPY IDSV0012.
      *
       01  DISPATCHER-VARIABLES.
      *  COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      *  COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.
      *
      *----------------------------------------------------------------*
      *    CAMPI X ROUTINES DI GESTIONE DISPLAY IDSP8888
      *----------------------------------------------------------------*
           COPY IDSV8888.

           COPY IDSV0003.
      *
      *
      *----------------------------------------------------------------*
      *     COPY DELLE TABELLE DB2 PER IL DISPATCHER DATI
      *----------------------------------------------------------------*
      *
      *  --> PARAMETRO OGGETTO
           COPY IDBVPOG1.
      *  --> Where condition ad hoc per lettura Parametro Oggetto
           COPY LDBV1131.
      *


      * ---------------------------------------------------------------
      *  INDICI DI SCORRIMENTO
      * ---------------------------------------------------------------
       01  IX-INDICI.
      *
           03 IX-TAB-PMO                   PIC S9(04) COMP.
           03 IX-TAB-GRZ                   PIC S9(04) COMP.
           03 IX-TAB-TGA                   PIC S9(04) COMP.
           03 IX-TAB-POG                   PIC S9(04) COMP.
      *
           03 IX-AREA-ISPC0211             PIC S9(04) COMP.
           03 IX-COMP-ISPC0211             PIC S9(04) COMP.
           03 IX-AREA-SCHEDA               PIC S9(04) COMP.
           03 IX-TAB-VAR                   PIC S9(04) COMP.
           03 IX-TAB-ERR                   PIC S9(04) COMP.
           03 IX-FRAZ                      PIC S9(04) COMP.
           03 IX-TAB-ISPS0211              PIC S9(04) COMP.
           03 IX-TAB-VAR-P                 PIC S9(04) COMP VALUE ZEROES.
           03 IX-TAB-VAR-T                 PIC S9(04) COMP VALUE ZEROES.
           03 IX-AREA-SCHEDA-P             PIC S9(04) COMP VALUE ZEROES.
           03 IX-AREA-SCHEDA-T             PIC S9(04) COMP VALUE ZEROES.


           COPY ISPC000Z.
      *
      *----------------------------------------------------------------*
      *       COPY VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
      *
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IEAV9903.
           COPY IDSV0002.
      *
      * --> Area di appoggio per i messaggi di errore
      *
       01 WK-GESTIONE-MSG-ERR.
          03 WK-COD-ERR                 PIC X(06) VALUE ZEROES.
          03 WK-LABEL-ERR               PIC X(30) VALUE SPACES.
          03 WK-STRING                  PIC X(85) VALUE SPACES.
      *
      *----------------------------------------------------------------*
      *     FLAGS X GESTIONE DEBUG
      *----------------------------------------------------------------*
      *
       01 TEST-I             PIC S9(4) VALUE 0.
       01 TEST-J             PIC S9(4) VALUE 0.
       01 WK-DATA            PIC 9(8)  VALUE 0.
       01 WK-ID              PIC 9(9)  VALUE 0.
      *
      * --> Area di accept della data dalla variabile di sistema DATE
      *
       01 WK-CURRENT-DATE               PIC 9(08) VALUE ZEROES.
      *
      *----------------------------------------------------------------*
      *     VARIABILI PER GESTIONE MANAGEMENT FEE
      *----------------------------------------------------------------*
      *
      * --> Indicatore del calcolo del Management Fee
      *
       01 WK-MANFEE                     PIC X(01).
          88 WK-MANFEE-YES                VALUE 'Y'.
          88 WK-MANFEE-NO                 VALUE 'N'.

      *-- AREA ROUTINE PER IL CALCOLO
       01  FORMATO                 PIC X(01).
       01  DATA-INFERIORE.
           05 AAAA-INF             PIC 9(04).
           05 MM-INF               PIC 9(02).
           05 GG-INF               PIC 9(02).
       01  DATA-SUPERIORE.
           05 AAAA-SUP             PIC 9(04).
           05 MM-SUP               PIC 9(02).
           05 GG-SUP               PIC 9(02).
       01  GG-DIFF                 PIC 9(05).
       01  CODICE-RITORNO          PIC X(01).

      *----------------------------------------------------------------*
      *    VARIABILE INDICATORE DI MODIFICA DATO DI TRANCHE DI GARANZIA
      *----------------------------------------------------------------*
      *
      * --> Indicatore di valorizzazione della Tranche Di Garanzia
      *
       01 WK-WTGA-VAL                   PIC X(01).
          88 WK-WTGA-VAL-Y                VALUE 'Y'.
          88 WK-WTGA-VAL-N                VALUE 'N'.

VSTEF *
  ..  * --> Indicatore di valorizzazione della variabile INTMORA
  ..  *
  ..   01 WK-INTMORA-VAL                PIC X(01).
  ..      88 WK-INTMORA-VAL-Y           VALUE 'Y'.
VSTEF     88 WK-INTMORA-VAL-N           VALUE 'N'.

      *
      * --> Flag per determinare se lavorare il livello di Garanzia
      *
       01 WK-ELAB-LIV-GAR               PIC X(01).
          88 WK-ELAB-LIV-GAR-SI           VALUE 'Y'.
          88 WK-ELAB-LIV-GAR-NO           VALUE 'N'.

      *
       01 WK-PGM                        PIC X(08) VALUE 'LOAS0660'.
      *
CRID2 *----------------------------------------------------------------*
CRID2 *     AREA VARIABILI COSTI IDD FASE 2
CRID2 *----------------------------------------------------------------*
CRID2  01 WK-PERCINDU                  PIC S9(03)V9(3) COMP-3.
CRID2  01 WK-PRESTLORD                 PIC S9(12)V9(3) COMP-3.
CRID2  01 WK-COSTO-ABS                 PIC  9(12)V9(3) COMP-3.
CRID2  01 WK-PPI                       PIC  9(12)V9(3) COMP-3.
CRID2 *
      *----------------------------------------------------------------*
      *     MODULI CHIAMATI
      *----------------------------------------------------------------*
      *
      *  --> SERVIZIO DI PRODOTTO
       01 LOAS0800                         PIC X(8) VALUE 'LOAS0800'.
       01 LCCS0010                         PIC X(8) VALUE 'LCCS0010'.
      *
      *----------------------------------------------------------------*
      *                 AREA PER SERVIZIO DI PRODOTTO
      *    "CALCOLI NOTEVOLI E CONTROLLI E DI LIQUIDAZIONE"
      *----------------------------------------------------------------*
       01  ISPS0211                         PIC X(08) VALUE 'ISPS0211'.
      *----------------------------------------------------------------*
      *    AREA PER SERVIZIO DI PRODOTTO CALCOLI E CONTROLLI ISPS0211
      *----------------------------------------------------------------*
       01 AREA-IO-ISPS0211.
          COPY ISPC0211.
      *
      *----------------------------------------------------------------*
      * AREA GESTIONE MODULO LOAS0800
      *----------------------------------------------------------------*
      *
       01 W800-AREA-PAG.
       COPY LOAC0800                     REPLACING ==(SF)== BY ==W800==.
      *
      *----------------------------------------------------------------*
      *    COPY PER LETTURA MATRICE MOVIMENTO
      *----------------------------------------------------------------*
      *
           COPY LCCV0021.
      *
      *----------------------------------------------------------------*
      *  AREE TIPOLOGICHE DI PORTAFOGLIO
      *----------------------------------------------------------------*
      *
          COPY LCCC0006.
          COPY LCCVXMV0.
          COPY LCCVXDA0.
      *----------------------------------------------------------------*
      * TABELLA DI APPOGGIO PER CARICAMENTO PARAMETRO OGGETTO
      *
      * --  AREA PARAMETRO OGGETTO
       01 WPOG-AREA-PARAM-OGG.
          04 WPOG-ELE-PARAM-OGG-MAX  PIC S9(04) COMP.
             COPY LCCVPOGA REPLACING   ==(SF)==  BY ==WPOG==.
             COPY LCCVPOG1              REPLACING ==(SF)== BY ==WPOG==.

      *----------------------------------------------------------------*
      *
13382 *01  WK-ISPC0211-NUM-COMPON-MAX   PIC 9(003) VALUE 60.
13382  01  WK-ISPC0211-NUM-COMPON-MAX   PIC 9(003) VALUE 86.
      *
      *----------------------------------------------------------------*
      *        L I N K A G E   S E C T I O N
      *----------------------------------------------------------------*
      *
       LINKAGE SECTION.
      *
      *----------------------------------------------------------------*
      *   AREA-IDSV0001
      *----------------------------------------------------------------*
      *
       01  AREA-IDSV0001.
           COPY IDSV0001.

      *  -- AREA INFRASTRUTTURALE
       01 WCOM-IO-STATI.
           03 WCOM-AREA-STATI.
              COPY LCCC0001              REPLACING ==(SF)== BY ==WCOM==.
           03 WCOM-AREA-LCCC0261.
              COPY LCCC0261              REPLACING ==(SF)== BY ==WCOM==.

      *  -- Area parametro movimento
       01 WPMO-AREA-PARAM-MOVI.
          04 WPMO-ELE-PARAM-MOV-MAX      PIC S9(04) COMP.
             COPY LCCVPMOA REPLACING   ==(SF)==  BY ==WPMO==.
             COPY LCCVPMO1               REPLACING ==(SF)== BY ==WPMO==.

      * --> Area Movimento
       01 WMOV-AREA-MOVIMENTO.
          03 WMOV-ELE-MOVI-MAX           PIC S9(04) COMP.
          03 WMOV-TAB-MOV.
             COPY LCCVMOV1               REPLACING ==(SF)== BY ==WMOV==.

      * -- Area polizza
       01 WPOL-AREA-POLIZZA.
          03 WPOL-ELE-POLI-MAX           PIC S9(04) COMP.
          03 WPOL-TAB-POL.
             COPY LCCVPOL1               REPLACING ==(SF)== BY ==WPOL==.

      * -- Area Adesione
       01 WADE-AREA-ADESIONE.
          03 WADE-ELE-ADES-MAX           PIC S9(04) COMP.
          03 WADE-TAB-ADE                OCCURS 1.
             COPY LCCVADE1               REPLACING ==(SF)== BY ==WADE==.

      * -- AREA GARANZIA
       01 WGRZ-AREA-GARANZIA.
          04 WGRZ-ELE-GAR-MAX           PIC S9(04) COMP.
             COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGRZ==.
             COPY LCCVGRZ1              REPLACING ==(SF)== BY ==WGRZ==.

      * --  AREA TRANCHE DI GARANZIA
       01 WTGA-AREA-TRANCHE.
          04 WTGA-ELE-TRAN-MAX       PIC S9(04) COMP.
            COPY LCCVTGAC REPLACING   ==(SF)==  BY ==WTGA==.
            COPY LCCVTGA1              REPLACING ==(SF)== BY ==WTGA==.

      *  -- AREA SCHEDA PER SERVIZI DI PRODOTTO
       01 WSKD-AREA-SCHEDA.
          COPY IVVC0216              REPLACING ==(SF)== BY ==WSKD==.

       01 W660-AREA-PAG.
       COPY LOAC0660                     REPLACING ==(SF)== BY ==W660==.

      *
      *  ====================  M A I N    L I N E  =================== *
      *
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WCOM-IO-STATI
                                WPMO-AREA-PARAM-MOVI
                                WMOV-AREA-MOVIMENTO
                                WPOL-AREA-POLIZZA
                                WADE-AREA-ADESIONE
                                WGRZ-AREA-GARANZIA
                                WTGA-AREA-TRANCHE
                                WSKD-AREA-SCHEDA
                                W660-AREA-PAG.
MIKE  *    DISPLAY '-->LOAS0660'

      *
           PERFORM S00000-OPERAZ-INIZIALI
              THRU S00000-OPERAZ-INIZIALI-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S10000-ELABORAZIONE
                 THRU S10000-ELABORAZIONE-EX
      *
           END-IF.
      *
           PERFORM S90000-OPERAZ-FINALI
              THRU S90000-OPERAZ-FINALI-EX.
      *
           GOBACK.
      *
      * ============================================================== *
      * -->           O P E R Z I O N I   I N I Z I A L I          <-- *
      * ============================================================== *
      *
       S00000-OPERAZ-INIZIALI.
      *
           MOVE 'S00000-OPERAZ-INIZIALI'   TO WK-LABEL-ERR

           PERFORM S00050-INIZIALIZZA-WORK
              THRU S00050-INIZIALIZZA-WORK-EX.

           PERFORM S00100-CTRL-INPUT
              THRU S00100-CTRL-INPUT-EX.
      *

       S00000-OPERAZ-INIZIALI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *                        CONTROLLO DATI DI INPUT                 *
      *----------------------------------------------------------------*
      *
       S00050-INIZIALIZZA-WORK.
           MOVE 'S00050-INIZIALIZZA-WORK'   TO WK-LABEL-ERR.

           INITIALIZE               IX-INDICI.

           SET WK-MANFEE-NO
             TO TRUE

           ACCEPT WK-CURRENT-DATE
             FROM DATE YYYYMMDD.

       S00050-INIZIALIZZA-WORK-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *                        CONTROLLO DATI DI INPUT                 *
      *----------------------------------------------------------------*
      *
       S00100-CTRL-INPUT.
      *
           MOVE 'S00010-CTRL-INPUT'
             TO WK-LABEL-ERR.
      *
           MOVE IDSV0001-TIPO-MOVIMENTO
             TO WS-MOVIMENTO.

           IF NOT ADPRE-PRESTA
      *
              IF IDSV0001-TIPO-MOVIMENTO = 6996
                 CONTINUE
              ELSE
              MOVE 'TIPO MOVIMENTO ERRATO'
                TO WK-STRING
              MOVE '001114'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
              END-IF
      *
           END-IF.
      *
           IF WPMO-ELE-PARAM-MOV-MAX = 0
      *
IDD2          IF IDSV0001-TIPO-MOVIMENTO = 6996
IDD2             CONTINUE
IDD2          ELSE
              MOVE 'NESSUNA OCCORRENZA DI PARAM. MOV. ELABORABILE'
                TO WK-STRING
              MOVE '001114'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
IDD2          END-IF
           END-IF.

           IF WMOV-ELE-MOVI-MAX = 0
      *
              MOVE 'OCCORRENZA DI MOVIMENTO NON VALORIZZATA'
                TO WK-STRING
              MOVE '001114'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.

           IF WPOL-ELE-POLI-MAX = 0
      *
              MOVE 'OCCORRENZA DI POLIZZA NON VALORIZZATA'
                TO WK-STRING
              MOVE '001114'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.

           IF WADE-ELE-ADES-MAX = 0
      *
              MOVE 'OCCORRENZA DI ADESIONE NON VALORIZZATA'
                TO WK-STRING
              MOVE '001114'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.

           IF WGRZ-ELE-GAR-MAX = 0
      *
              MOVE 'NESSUNA OCCORRENZA DI GARANZIA ELABORABILE'
                TO WK-STRING
              MOVE '001114'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.

           IF WTGA-ELE-TRAN-MAX = 0
      *
              MOVE 'NESSUNA OCCORRENZA DI TRANCHE ELABORABILE'
                TO WK-STRING
              MOVE '001114'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.

           IF  WSKD-ELE-LIVELLO-MAX-P = 0
           AND WSKD-ELE-LIVELLO-MAX-T = 0
      *
              MOVE 'AREA VARIABILI NON VALORIZZATA'
                TO WK-STRING
              MOVE '001114'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.
      *
       S00100-CTRL-INPUT-EX.
           EXIT.
      *
      * ============================================================== *
      * -->               E L A B O R A Z I O N E                  <-- *
      * ============================================================== *
      *
       S10000-ELABORAZIONE.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S10600-GESTIONE-ISPS0211
                 THRU S10600-GESTIONE-ISPS0211-EX
      *
           END-IF.
      *
           IF IDSV0001-ESITO-OK
      *
              IF WK-MANFEE-YES
IDD2             IF IDSV0001-TIPO-MOVIMENTO = 6996
IDD2                CONTINUE
IDD2             ELSE
      *
                 SET W660-MF-CALCOLATO-SI
                   TO TRUE
      *
                 PERFORM S10700-CALL-LOAS0800
                    THRU S10700-CALL-LOAS0800-EX
      *
IDD2             END-IF
              END-IF
      *
           END-IF.
      *
       S10000-ELABORAZIONE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  GESTIONE CHIAMATA AL SERVIZIO CALC. NOTEVOLI E CONTR. DI LIQ. *
      *----------------------------------------------------------------*
      *
       S10600-GESTIONE-ISPS0211.
      *
           MOVE 'S10600-GESTIONE-ISPS0211'
             TO WK-LABEL-ERR.

           PERFORM S10610-PREPARA-ISPS0211
              THRU S10610-PREPARA-ISPS0211-EX.
      *
           PERFORM S10615-CALL-ISPS0211
              THRU S10615-CALL-ISPS0211-EX.

      *
           IF IDSV0001-ESITO-OK
      *
      *  -->    Routine valorizzazione dclgen tabelle con
      *  -->    l'output del servizio ISPS0211
      *
              PERFORM S10640-SCORRI-OUT-ISPS0211
                 THRU S10640-SCORRI-OUT-ISPS0211-EX
              VARYING IX-AREA-ISPC0211 FROM 1 BY 1
                UNTIL IX-AREA-ISPC0211 >
                      ISPC0211-ELE-MAX-SCHEDA-T
                   OR IDSV0001-ESITO-KO
      *
           END-IF.
      *
       S10600-GESTIONE-ISPS0211-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *         VALORIZZAZIONE DELL'INPUT DEL SERVIZIO ISPS0211        *
      *----------------------------------------------------------------*
      *
       S10610-PREPARA-ISPS0211.
           MOVE 'S10610-PREPARA-ISPS0211' TO WK-LABEL-ERR
      *
           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LOAS0660'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE 'ISPC0211'                 TO IDSV8888-DESC-PGM
           STRING 'Iniz.tab. work ISPC0211'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
      *
ALEX  *    INITIALIZE AREA-IO-ISPS0211.
           INITIALIZE ISPC0211-DATI-INPUT
                      ISPC0211-ELE-MAX-SCHEDA-P
                      ISPC0211-ELE-MAX-SCHEDA-T
                      ISPC0211-AREA-ERRORI
                      ISPC0211-TIPO-LIVELLO-P      (1)
                      ISPC0211-TIPO-LIVELLO-T      (1)
                      ISPC0211-CODICE-LIVELLO-P    (1)
                      ISPC0211-CODICE-LIVELLO-T    (1)
                      ISPC0211-IDENT-LIVELLO-P     (1)
                      ISPC0211-IDENT-LIVELLO-T     (1)
                      ISPC0211-DT-INIZ-TARI        (1)
                      ISPC0211-DT-INIZ-PROD        (1)
                      ISPC0211-COD-RGM-FISC        (1)
                      ISPC0211-DT-DECOR-TRCH       (1)
                      ISPC0211-FLG-LIQ-P           (1)
                      ISPC0211-FLG-LIQ-T           (1)
                      ISPC0211-CODICE-OPZIONE-P    (1)
                      ISPC0211-CODICE-OPZIONE-T    (1)
                      ISPC0211-NUM-COMPON-MAX-ELE-P(1)
                      ISPC0211-NUM-COMPON-MAX-ELE-T(1)


           PERFORM VARYING IX-TAB-ISPS0211 FROM 1 BY 1
                   UNTIL   IX-TAB-ISPS0211 >
                           WK-ISPC0211-NUM-COMPON-MAX-P

              MOVE SPACES  TO  ISPC0211-CODICE-VARIABILE-P
                               (1,IX-TAB-ISPS0211)
                               ISPC0211-TIPO-DATO-P
                               (1,IX-TAB-ISPS0211)
                               ISPC0211-VAL-GENERICO-P
                               (1,IX-TAB-ISPS0211)

           END-PERFORM

           PERFORM VARYING IX-TAB-ISPS0211 FROM 1 BY 1
                   UNTIL   IX-TAB-ISPS0211 >
                           WK-ISPC0211-NUM-COMPON-MAX-T

              MOVE SPACES  TO  ISPC0211-CODICE-VARIABILE-T
                               (1,IX-TAB-ISPS0211)
                               ISPC0211-TIPO-DATO-T
                               (1,IX-TAB-ISPS0211)
                               ISPC0211-VAL-GENERICO-T
                               (1,IX-TAB-ISPS0211)

           END-PERFORM


           MOVE ISPC0211-TAB-SCHEDA-R-P TO ISPC0211-RESTO-TAB-SCHEDA-P
           MOVE ISPC0211-TAB-SCHEDA-R-T TO ISPC0211-RESTO-TAB-SCHEDA-T

      *
           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LOAS0660'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE 'ISPC0211'                 TO IDSV8888-DESC-PGM
           STRING 'Iniz.tab. work ISPC0211'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           MOVE IDSV0001-COD-COMPAGNIA-ANIA
             TO ISPC0211-COD-COMPAGNIA.
      *
           MOVE WPOL-COD-PROD
             TO ISPC0211-COD-PRODOTTO.
      *
           IF WPOL-COD-CONV-NULL = HIGH-VALUES
      *
              MOVE SPACES
                TO ISPC0211-COD-CONVENZIONE
      *
           ELSE
      *
              MOVE WPOL-COD-CONV
                TO ISPC0211-COD-CONVENZIONE
      *
           END-IF.

           MOVE WSKD-DEE                  TO ISPC0211-DEE

           IF WPOL-IB-OGG-NULL = HIGH-VALUES
              MOVE SPACES
                TO ISPC0211-NUM-POLIZZA
           ELSE
              MOVE WPOL-IB-OGG
                TO ISPC0211-NUM-POLIZZA
           END-IF.
      *
           IF WPOL-DT-INI-VLDT-CONV-NULL = HIGH-VALUE
      *
              MOVE SPACES
                TO ISPC0211-DATA-INIZ-VALID-CONV
      *
           ELSE
      *
              MOVE WPOL-DT-INI-VLDT-CONV
                TO ISPC0211-DATA-INIZ-VALID-CONV
      *
           END-IF.
      *
           MOVE WCOM-DT-ULT-VERS-PROD
             TO ISPC0211-DATA-RIFERIMENTO.
      *
           MOVE WPOL-DT-DECOR
             TO ISPC0211-DATA-DECORR-POLIZZA.
      *
           MOVE WCOM-COD-LIV-AUT-PROFIL
             TO ISPC0211-LIVELLO-UTENTE.
      *
           MOVE IDSV0001-SESSIONE
             TO ISPC0211-SESSION-ID.
      *
           MOVE 'N'
             TO ISPC0211-FLG-REC-PROV.
      *
           MOVE WS-MOVIMENTO
             TO LCCV0021-TP-MOV-PTF.
      *
           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LCCS0020'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lett. matrice movimento'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-MATR-MOVIMENTO
              THRU CALL-MATR-MOVIMENTO-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LCCS0020'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lett. matrice movimento'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

      *
           IF IDSV0001-ESITO-OK
      *
              MOVE LCCV0021-TP-MOV-ACT
                TO ISPC0211-FUNZIONALITA
      *
           END-IF.

           PERFORM VAL-SCHEDE-ISPC0211
              THRU VAL-SCHEDE-ISPC0211-EX.

       S10610-PREPARA-ISPS0211-EX.
           EXIT.
      *
      *
      *----------------------------------------------------------------*
      *                CHIAMATA AL SERVIZIO - ISPS0211 -               *
      *----------------------------------------------------------------*
      *
       S10615-CALL-ISPS0211.
           MOVE 'S10615-CALL-ISPS0211' TO WK-LABEL-ERR
      *
           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'ISPS0211'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Serv. calcoli notevoli e liq'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           CALL ISPS0211          USING AREA-IDSV0001
                                        WCOM-AREA-STATI
                                        AREA-IO-ISPS0211

           ON EXCEPTION
      *
                 MOVE 'ISPS0211'
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'CALCOLI - ISPS0211'
                   TO CALL-DESC
                 MOVE 'CALL-ISPS0211'
                   TO WK-LABEL-ERR
      *
                 PERFORM GESTIONE-ERR-SIST
                    THRU GESTIONE-ERR-SIST-EX
      *
           END-CALL.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'ISPS0211'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Serv. calcoli notevoli e liq'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
       S10615-CALL-ISPS0211-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     GESTIONE DELL'OUTPUT DEL SERVIZIO DI PRODOTTO ISPS0211     *
      *----------------------------------------------------------------*
      *
       S10640-SCORRI-OUT-ISPS0211.
DBG        MOVE 'S10640-SCORRI-OUT-ISPS0211' TO WK-LABEL-ERR
DBG        PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
      *
           IF ISPC0211-TIPO-LIVELLO-T  (IX-AREA-ISPC0211) = 'G' AND
              ISPC0211-IDENT-LIVELLO-T (IX-AREA-ISPC0211) >  0  AND
              ISPC0211-FLG-LIQ-T       (IX-AREA-ISPC0211) = 'NO'
      *
              PERFORM S10650-ALLINEA-AREA-TRANCHE
                 THRU S10650-ALLINEA-AREA-TRANCHE-EX
      *
           END-IF.
      *
       S10640-SCORRI-OUT-ISPS0211-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     GESTIONE DELL'OUTPUT DEL SERVIZIO DI GARANZIA ISPS0211     *
      *    - DETT. TIT. CONT. - TRANCHE DI GAR. - RISERVA DI TR. -     *
      *----------------------------------------------------------------*
      *
       S10650-ALLINEA-AREA-TRANCHE.
           MOVE 'S10650-ALLINEA-AREA-TRANCHE' TO WK-LABEL-ERR
      *
      * -->> Gestione Tranche Di Granzia
      *
           PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
             UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
                OR IDSV0001-ESITO-KO
      *
                IF WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) =
                   ISPC0211-IDENT-LIVELLO-T(IX-AREA-ISPC0211)
      *
                   PERFORM S10660-GESTIONE-LIV-GAR
                      THRU S10660-GESTIONE-LIV-GAR-EX
      *
                END-IF
      *
           END-PERFORM.
      *
       S10650-ALLINEA-AREA-TRANCHE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *         GESTIONE DEL LIVELLO DI GARANZIA                       *
      *----------------------------------------------------------------*
      *
       S10660-GESTIONE-LIV-GAR.
      *
           SET WK-WTGA-VAL-N
             TO TRUE.

VSTEF      SET WK-INTMORA-VAL-N
            TO TRUE.

           PERFORM S10670-INIZIALIZZA-TRCH
             THRU S10670-INIZIALIZZA-TRCH-EX

CRID2      INITIALIZE WK-PERCINDU
CRID2                 WK-PRESTLORD
CRID2                 WK-COSTO-ABS
CRID2                 WK-PPI
      *
      *    DISPLAY 'NUMERO COMPONENTI: '
      *    ISPC0211-NUM-COMPON-MAX-ELE(IX-AREA-ISPC0211)
           PERFORM S10680-CONTR-LIV-GAR
              THRU S10680-CONTR-LIV-GAR-EX
           VARYING IX-COMP-ISPC0211 FROM 1 BY 1
             UNTIL IX-COMP-ISPC0211 >
                   ISPC0211-NUM-COMPON-MAX-ELE-T(IX-AREA-ISPC0211)
                OR IDSV0001-ESITO-KO.


CRID2      IF IDSV0001-ESITO-OK
CRID2         IF WTGA-PRSTZ-ULT-NULL(IX-TAB-TGA)
CRID2            NOT EQUAL HIGH-VALUES
MIKE  *    DISPLAY 'WK-PRESTLORD: ' WK-PRESTLORD
MIKE  *    DISPLAY 'WTGA-PRSTZ-ULT(IX-TAB-TGA):'
MIKE  *             WTGA-PRSTZ-ULT(IX-TAB-TGA)
CRID2            IF WK-PRESTLORD IS NUMERIC
CRID2         COMPUTE WTGA-COS-RUN-ASSVA(IX-TAB-TGA)
CRID2                 = WK-PRESTLORD
CRID2                 - WTGA-PRSTZ-ULT(IX-TAB-TGA)
CRID2            IF WTGA-COS-RUN-ASSVA(IX-TAB-TGA) < 0
CRID2               MOVE 0 TO WTGA-COS-RUN-ASSVA(IX-TAB-TGA)
CRID2            END-IF
CRID2            END-IF
CRID2         ELSE
CRID2            IF WK-PRESTLORD IS NUMERIC
CRID2            COMPUTE WTGA-COS-RUN-ASSVA(IX-TAB-TGA)
CRID2                    = WK-PRESTLORD
CRID2         END-IF
CRID2         END-IF
CRID2 *
CRID2         IF WK-PPI IS NUMERIC
CRID2         IF WK-PPI > 0
CRID2            IF WTGA-COS-RUN-ASSVA-NULL(IX-TAB-TGA)
CRID2            NOT EQUAL HIGH-VALUES
CRID2                IF WTGA-COS-RUN-ASSVA(IX-TAB-TGA) > 0
CRID2                COMPUTE WTGA-COS-RUN-ASSVA(IX-TAB-TGA) =
CRID2                       (WTGA-COS-RUN-ASSVA(IX-TAB-TGA)
CRID2                       * WK-PPI)
CRID2                END-IF
CRID2            END-IF
CRID2         END-IF
CRID2         END-IF
CRID2 *
CRID2         IF WTGA-COS-RUN-ASSVA-NULL(IX-TAB-TGA)
CRID2         NOT EQUAL HIGH-VALUES
CRID2             IF WK-PERCINDU IS NUMERIC
CRID2         COMPUTE WK-COSTO-ABS
CRID2                 = WTGA-COS-RUN-ASSVA(IX-TAB-TGA)
CRID2         COMPUTE WTGA-COS-RUN-ASSVA-IDC(IX-TAB-TGA)
CRID2                 = WK-COSTO-ABS
CRID2                 * (WK-PERCINDU / 100)
CRID2         END-IF
CRID2      END-IF
CRID2      END-IF
      *
      * --> Valorizzo il flag di inser/modif. solo se la tabella h stata
      * --> effettivamente valorizzata con le componenti del servizio
      *
           IF IDSV0001-ESITO-OK
      *
              IF WK-WTGA-VAL-Y

VSTEF *          SIR CQPrd00020310: Se Actuator non fornisce la
  ..  *          componente INTMORA, allora valorizzare a ZERO
  ..  *          l'attributo di tranche
  ..             IF WK-INTMORA-VAL-N
  ..                MOVE ZEROES
  ..                  TO WTGA-INTR-MORA(IX-TAB-TGA)
VSTEF            END-IF

                 SET  WTGA-ST-MOD(IX-TAB-TGA)
                   TO TRUE
      *
IDD2             IF IDSV0001-TIPO-MOVIMENTO = 6996
IDD2                CONTINUE
IDD2             ELSE
                 MOVE WPMO-DT-RICOR-SUCC(1)
                   TO WTGA-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
      *
                 PERFORM CALCOLA-GNT
                    THRU CALCOLA-GNT-EX

IDD2             END-IF
              END-IF
      *
           END-IF.

      *
       S10660-GESTIONE-LIV-GAR-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *         INIZIALIZZAZIONE TRCH DI GAR                           *
      *----------------------------------------------------------------*
      *
       S10670-INIZIALIZZA-TRCH.
      *

            MOVE HIGH-VALUE  TO
                          WTGA-PC-COMMIS-GEST-NULL(IX-TAB-TGA).
      *
       S10670-INIZIALIZZA-TRCH-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *         TEST SUI COMPONENTI DELLA TABELLA TGA
      *----------------------------------------------------------------*
      *
       S10680-CONTR-LIV-GAR.
      *
           MOVE 'S10680-CONTR-LIV-GAR' TO WK-LABEL-ERR.
DBG        PERFORM DISPLAY-LABEL
DBG           THRU DISPLAY-LABEL-EX.
      *

           IF IDSV0001-ESITO-OK
      *
              PERFORM S10690-CONTR-COMP-TRANCHE
                 THRU S10690-CONTR-COMP-TRANCHE-EX
      *
           END-IF.
      *
       S10680-CONTR-LIV-GAR-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *         VALORIZZAZIONE DELL'AREA DI TRANCHE DI GARANZIA        *
      *----------------------------------------------------------------*
      *
       S10690-CONTR-COMP-TRANCHE.
DBG        MOVE 'S10690-CONTR-COMP-TRANCHE' TO WK-LABEL-ERR.
DBG        PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX.

VSTEF *    DISPLAY '********* COMPONENTE OUTPUT ACTUATOR ********'
 ..   *    DISPLAY '* POLIZZA                   = ' WPOL-IB-OGG
 ..   *    DISPLAY '* ISPC0211-CODICE-VARIABILE = '
 ..   *    ISPC0211-CODICE-VARIABILE(IX-AREA-ISPC0211, IX-COMP-ISPC0211)
 ..   *    DISPLAY '* ISPC0211-TIPO-DATO        = '
 ..   *    ISPC0211-TIPO-DATO(IX-AREA-ISPC0211, IX-COMP-ISPC0211)
 ..   *    DISPLAY '* ISPC0211-VALORE-IMP       = '
 ..   *    ISPC0211-VALORE-IMP(IX-AREA-ISPC0211, IX-COMP-ISPC0211)
 ..   *    DISPLAY '* ISPC0211-VALORE-PERC      = '
 ..   *    ISPC0211-VALORE-PERC(IX-AREA-ISPC0211, IX-COMP-ISPC0211)
VSTEF *    DISPLAY '************************************************'

           MOVE ISPC0211-TIPO-DATO-T
                (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
             TO WS-TP-DATO.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PREMORT'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-PRE-CASO-MOR(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP. PREMORT ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'CAPMORTE'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                     TO WTGA-CPT-RSH-MOR(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP CAPMORTE ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PPUROU'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-PRE-PP-ULT(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP PPUROU ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PTARIFU'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-PRE-TARI-ULT(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP PTARIFU ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PINVENU'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-PRE-INVRIO-ULT(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
GD                IF WTGA-PRE-INVRIO-ULT(IX-TAB-TGA) = ZEROES
GD                   MOVE HIGH-VALUE
GD                     TO WTGA-PRE-INVRIO-ULT-NULL(IX-TAB-TGA)
GD                END-IF
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP PINVENU ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'SOPPRO'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-IMP-SOPR-PROF(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP SOPPRO ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'SOPSAN'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-IMP-SOPR-SAN(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP SOPSAN ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'SOPSPO'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-IMP-SOPR-SPO(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP SOPSPO ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'SOPTEC'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-IMP-SOPR-TEC(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP SOPTEC ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'SOPALT'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-IMP-ALT-SOPR(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP SOPALT ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PRESTRIV'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-PRSTZ-ULT(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP PRESTRIV ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'CAPOPZ'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-CPT-IN-OPZ-RIVTO(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP CAPOPZ ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'TRN'
              EVALUATE TRUE
                WHEN TD-PERC-CENTESIMI
                  MOVE ISPC0211-VALORE-PERC-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-RENDTO-LRD(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-RENDTO-LRD(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP TRN ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PERCRETR'
              EVALUATE TRUE
                WHEN TD-PERC-CENTESIMI
                  MOVE ISPC0211-VALORE-PERC-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-PC-RETR(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-PC-RETR(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP PERCRETR ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'RENDRETR'
              EVALUATE TRUE
                WHEN TD-PERC-CENTESIMI
                  MOVE ISPC0211-VALORE-PERC-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-RENDTO-RETR(IX-TAB-TGA)
XTEST *         DISPLAY 'RENDRETR=>'
XTEST *         ISPC0211-VALORE-PERC(IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERRORE COMP RENDRETR ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'MINGAR'
              EVALUATE TRUE
                WHEN TD-PERC-CENTESIMI
                  MOVE ISPC0211-VALORE-PERC-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-MIN-GARTO(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-MIN-GARTO(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERRORE COMP MINGAR ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'MINTRATT'
              EVALUATE TRUE
                WHEN TD-PERC-CENTESIMI
                  MOVE ISPC0211-VALORE-PERC-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-MIN-TRNUT(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-MIN-TRNUT(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP MINTRATT ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'ABBULTIMO'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-ABB-ANNU-ULT(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP ABBULTIMO ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'INTMORA'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-INTR-MORA(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y       TO TRUE
VSTEF             SET WK-INTMORA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP INTMORA ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'MANFEE'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-MANFEE-RICOR(IX-TAB-TGA)
      *
                  IF WTGA-MANFEE-RICOR(IX-TAB-TGA) > 0
      *
                     SET WK-MANFEE-YES TO TRUE
      *
                     MOVE 'RI'
                       TO WTGA-TP-MANFEE-APPL(IX-TAB-TGA)
      *
                  END-IF
      *
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP. MANFEE ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PREURIV'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-PRE-UNI-RIVTO(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
GD                IF WTGA-PRE-UNI-RIVTO(IX-TAB-TGA) = ZEROES
GD                   MOVE HIGH-VALUE
GD                     TO WTGA-PRE-UNI-RIVTO-NULL(IX-TAB-TGA)
GD                END-IF
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP PREURIV ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'INCPRE'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-INCR-PRE(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP INCPRE ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'INCPREST'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-INCR-PRSTZ(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP INCPREST ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'RIVAGG'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-PRSTZ-AGG-ULT(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP RIVAGG ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'TASSONETTO'
              EVALUATE TRUE
                WHEN TD-PERC-CENTESIMI
                  MOVE ISPC0211-VALORE-PERC-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-TS-RIVAL-NET(IX-TAB-TGA)
XTEST *          DISPLAY 'TASSONETTO=>'
XTEST *         ISPC0211-VALORE-PERC(IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP TASSONETTO ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'RISMAT'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-RIS-MAT(IX-TAB-TGA)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP RISMAT ISPS0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
A3365      IF ISPC0211-CODICE-VARIABILE-T
A3365            (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PREULT'

A3365         EVALUATE TRUE
A3365           WHEN TD-IMPORTO
A3365             MOVE ISPC0211-VALORE-IMP-T
A3365                 (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
A3365               TO WTGA-PRE-RIVTO(IX-TAB-TGA)
A3365             SET WK-WTGA-VAL-Y    TO TRUE
A3365           WHEN OTHER
A3365             MOVE 'S10690-CONTR-COMP-TRANCHE'
A3365               TO WK-LABEL-ERR
A3365             MOVE 'ERR COMP PREULT ISPC0211'
A3365               TO WK-STRING
A3365             MOVE '001114'
A3365               TO WK-COD-ERR
A3365             PERFORM GESTIONE-ERR-STD
A3365                THRU GESTIONE-ERR-STD-EX
A3365         END-EVALUATE
A3365      END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
                (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'CAPMINSCA'
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-CPT-MIN-SCAD(IX-TAB-TGA)
                   SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP CAPMINSCA ISPC0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
      *
           IF ISPC0211-CODICE-VARIABILE-T
                (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'GESTIONE'

      *       DISPLAY 'WS-TP-DATO:' WS-TP-DATO
              EVALUATE TRUE
                WHEN TD-IMPORTO
                  MOVE ISPC0211-VALORE-IMP-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    TO WTGA-COMMIS-GEST(IX-TAB-TGA)
XTEST *          DISPLAY 'GESTIONE X IMPORTO=>'
XTEST *         ISPC0211-VALORE-IMP(IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                   SET WK-WTGA-VAL-Y    TO TRUE
                WHEN TD-PERC-CENTESIMI
                  MOVE ISPC0211-VALORE-PERC-T
                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
      *             TO WTGA-COMMIS-GEST(IX-TAB-TGA)
23209               TO WTGA-PC-COMMIS-GEST(IX-TAB-TGA)
XTEST *          DISPLAY 'GESTIONE X PERCENT=>'
XTEST *         ISPC0211-VALORE-PERC(IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                  SET WK-WTGA-VAL-Y    TO TRUE
                WHEN OTHER
      *           DISPLAY '* COMPONENTE GESTIONE TIPO DATO -> '
      *                                          WS-TP-DATO
                  MOVE 'S10690-CONTR-COMP-TRANCHE'
                    TO WK-LABEL-ERR
                  MOVE 'ERR COMP GESTIONE ISPC0211'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
              END-EVALUATE
           END-IF.
CRID2 *--> COSTI IDD FASE 2
CRID2      IF ISPC0211-CODICE-VARIABILE-T
CRID2        (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PRESTLORD'
MIKE  *       DISPLAY 'TROVATA PRESTLORD'
CRID2         EVALUATE TRUE
CRID2           WHEN TD-IMPORTO
CRID2             MOVE ISPC0211-VALORE-IMP-T
CRID2                 (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
CRID2 *             TO WTGA-PRSTZ-ULT(IX-TAB-TGA)
CRID2               TO WK-PRESTLORD
CRID2             SET WK-WTGA-VAL-Y    TO TRUE
CRID2           WHEN OTHER
CRID2             MOVE 'S10690-CONTR-COMP-TRANCHE'
CRID2               TO WK-LABEL-ERR
CRID2             MOVE 'ERR COMP PRESTLORD ISPS0211'
CRID2               TO WK-STRING
CRID2             MOVE '001114'
CRID2               TO WK-COD-ERR
CRID2             PERFORM GESTIONE-ERR-STD
CRID2                THRU GESTIONE-ERR-STD-EX
CRID2         END-EVALUATE
CRID2      END-IF.
      *
CRID2 *--> COSTI IDD FASE 2
CRID2      IF ISPC0211-CODICE-VARIABILE-T
CRID2        (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PERCINDU'
CRID2         EVALUATE TRUE
CRID2           WHEN TD-PERC-CENTESIMI
CRID2             MOVE ISPC0211-VALORE-PERC-T
CRID2                 (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
CRID2               TO WK-PERCINDU
CRID2             SET WK-WTGA-VAL-Y    TO TRUE
CRID2           WHEN OTHER
CRID2             MOVE 'S10690-CONTR-COMP-TRANCHE'
CRID2               TO WK-LABEL-ERR
CRID2             MOVE 'ERR COMP PRESTLORD ISPS0211'
CRID2               TO WK-STRING
CRID2             MOVE '001114'
CRID2               TO WK-COD-ERR
CRID2             PERFORM GESTIONE-ERR-STD
CRID2                THRU GESTIONE-ERR-STD-EX
CRID2         END-EVALUATE
CRID2      END-IF.
      *
CRID2 *--> COSTI IDD FASE 2 - PPI
CRID2      IF ISPC0211-CODICE-VARIABILE-T
CRID2        (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PPI'
CRID2         EVALUATE TRUE
CRID2           WHEN TD-IMPORTO
CRID2             MOVE ISPC0211-VALORE-IMP-T
CRID2                 (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
CRID2 *             TO WTGA-PRSTZ-ULT(IX-TAB-TGA)
CRID2               TO WK-PPI
CRID2             SET WK-WTGA-VAL-Y    TO TRUE
CRID2           WHEN OTHER
CRID2             MOVE 'S10690-CONTR-COMP-TRANCHE'
CRID2               TO WK-LABEL-ERR
CRID2             MOVE 'ERR COMP PRESTLORD ISPS0211'
CRID2               TO WK-STRING
CRID2             MOVE '001114'
CRID2               TO WK-COD-ERR
CRID2             PERFORM GESTIONE-ERR-STD
CRID2                THRU GESTIONE-ERR-STD-EX
CRID2         END-EVALUATE
CRID2      END-IF.
       S10690-CONTR-COMP-TRANCHE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * CHIAMATA AL MODULO DI GESTIONE DEL MANAGEMENT FEE
      *----------------------------------------------------------------*
      *
       S10700-CALL-LOAS0800.
      *
           MOVE 'S10700-CALL-LOAS0800'  TO WK-LABEL-ERR.
DBG        PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX.
      *
           SET W800-SENZA-CALCOLO
             TO TRUE.


           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LOAS0800'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'GESTIONE MANAGEMENT FEE'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

      *
           CALL LOAS0800  USING AREA-IDSV0001
                                WCOM-IO-STATI
                                WPMO-AREA-PARAM-MOVI
                                WMOV-AREA-MOVIMENTO
                                WPOL-AREA-POLIZZA
                                WADE-AREA-ADESIONE
                                WGRZ-AREA-GARANZIA
                                WTGA-AREA-TRANCHE
                                WSKD-AREA-SCHEDA
                                W660-AREA-PAG
                                W800-AREA-PAG
      *
            ON EXCEPTION
      *
               MOVE 'LOAS0800'
                 TO IEAI9901-COD-SERVIZIO-BE
               MOVE 'ERRORE CALL MODULO LOAS0800'
                 TO CALL-DESC
               MOVE WK-LABEL-ERR
                 TO IEAI9901-LABEL-ERR
               PERFORM S0290-ERRORE-DI-SISTEMA
                  THRU EX-S0290
      *
            END-CALL.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LOAS0800'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'GESTIONE MANAGEMENT FEE'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
       S10700-CALL-LOAS0800-EX.
           EXIT.

      *----------------------------------------------------------------*
      *  CALCOLA-GNT
      *----------------------------------------------------------------*
       CALCOLA-GNT.
      *

           MOVE 'A'                            TO FORMATO
           MOVE IDSV0001-DATA-EFFETTO          TO DATA-SUPERIORE

           IF WPMO-DT-RICOR-PREC-NULL(1) = HIGH-VALUE
              MOVE WTGA-DT-DECOR(IX-TAB-TGA)   TO WK-DATA
              MOVE WK-DATA                     TO DATA-INFERIORE
           ELSE
      *SIR BNL 12887
12887        IF WPMO-DT-RICOR-PREC(1) < WTGA-DT-DECOR(IX-TAB-TGA)
12887           MOVE WTGA-DT-DECOR(IX-TAB-TGA) TO WK-DATA
12887           MOVE WK-DATA                   TO DATA-INFERIORE
             ELSE
                MOVE WPMO-DT-RICOR-PREC(1)     TO WK-DATA
                MOVE WK-DATA                   TO DATA-INFERIORE
             END-IF
           END-IF.
      *
           CALL LCCS0010     USING FORMATO,
                                   DATA-INFERIORE,
                                   DATA-SUPERIORE,
                                   GG-DIFF,
                                   CODICE-RITORNO
           ON EXCEPTION
                 MOVE 'LCCS0010'
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'DIFFERENZA TRA DATE - LCCS0010'
                   TO CALL-DESC
                 MOVE 'CALCOLA-GNT'
                   TO WK-LABEL-ERR
      *
                 PERFORM GESTIONE-ERR-SIST
                    THRU GESTIONE-ERR-SIST-EX
           END-CALL.
      *
           IF CODICE-RITORNO = ZERO
              MOVE GG-DIFF              TO WTGA-NUM-GG-RIVAL(IX-TAB-TGA)
           ELSE
              MOVE 'LCCS0010'
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'DIFFERENZA TRA DATE - LCCS0010'
                TO CALL-DESC
              MOVE 'CALCOLA-GNT'
                TO WK-LABEL-ERR
      *
              PERFORM GESTIONE-ERR-SIST
                 THRU GESTIONE-ERR-SIST-EX
           END-IF.
      *
       CALCOLA-GNT-EX.
           EXIT.
      * ============================================================== *
      * -->           O P E R Z I O N I   F I N A L I              <-- *
      * ============================================================== *
      *
       S90000-OPERAZ-FINALI.
           MOVE 'S90000-OPERAZ-FINALI'  TO WK-LABEL-ERR.
DBG        PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX.

       S90000-OPERAZ-FINALI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *                GESTIONE STANDARD DELL'ERRORE                   *
      *----------------------------------------------------------------*
      *
       GESTIONE-ERR-STD.
      *
           MOVE WK-PGM
             TO IEAI9901-COD-SERVIZIO-BE.
           MOVE WK-LABEL-ERR
             TO IEAI9901-LABEL-ERR.
           MOVE WK-COD-ERR
             TO IEAI9901-COD-ERRORE.
           STRING WK-STRING ';'
                  IDSO0011-RETURN-CODE ';'
                  IDSO0011-SQLCODE
                  DELIMITED BY SIZE
                  INTO IEAI9901-PARAMETRI-ERR
           END-STRING.
      *
           PERFORM S0300-RICERCA-GRAVITA-ERRORE
              THRU EX-S0300.
      *
       GESTIONE-ERR-STD-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *           GESTIONE STANDARD DELL'ERRORE DI SISTEMA             *
      *----------------------------------------------------------------*
      *
       GESTIONE-ERR-SIST.
      *
           MOVE WK-LABEL-ERR
             TO IEAI9901-LABEL-ERR
      *
           PERFORM S0290-ERRORE-DI-SISTEMA
              THRU EX-S0290.
      *
       GESTIONE-ERR-SIST-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     ROUTINES CALL DISPATCHER                                   *
      *----------------------------------------------------------------*
      *
           COPY IDSP0012.
      *
      *----------------------------------------------------------------*
      *    COPY PER LETTURA MATRICE MOVIMENTO
      *----------------------------------------------------------------*
      *
           COPY LCCP0021.
      *
      *----------------------------------------------------------------*
      *  ROUTINES GESTIONE ERRORI                                      *
      *----------------------------------------------------------------*
      *
           COPY IERP9901.
           COPY IERP9902.
           COPY IERP9903.
      *
      *----------------------------------------------------------------*
      *  COPY DI VALORIZZAZIONE OUTPUT LETTURA DB
      *----------------------------------------------------------------*
      *  --> PARAMETRO OGGETTO
           COPY LCCVPOG3                REPLACING ==(SF)== BY ==WPOG==.
      *
      *-----------------------------------------------------------------
      *  ROUTINE DEBUG
      *-----------------------------------------------------------------
      *
       DISPLAY-LABEL.
      *
           IF (IDSV0001-DEBUG-BASSO OR IDSV0001-DEBUG-ESASPERATO) AND
           NOT IDSV0001-ON-LINE
      *
      *        DISPLAY WK-LABEL-ERR
               CONTINUE
      *
           END-IF.
      *
       DISPLAY-LABEL-EX.
           EXIT.
      *
      *DISPLAY-ESITO-PROD.
      *
      *    IF IDSV0001-DEBUG-ESASPERATO AND
      *    NOT IDSV0001-ON-LINE
      *      DISPLAY 'AREA-ERRORI: ' AREA-PRODUCT-SERVICES
      *      DISPLAY 'ESITO PRODOTTO: ' IJCCMQ03-ESITO
      *      DISPLAY 'JAVA SERVICE NAME: ' IJCCMQ03-JAVA-SERVICE-NAME
      *      DISPLAY 'NUMERO ELEMENTI: ' IJCCMQ03-MAX-ELE-ERRORI
      *      PERFORM VARYING IX-TAB-ERR FROM 1 BY 1
      *         UNTIL IX-TAB-ERR > IJCCMQ03-MAX-ELE-ERRORI
      *
      *          DISPLAY 'COD-ERRORE: '
      *              IJCCMQ03-COD-ERRORE(IX-TAB-ERR)
      *          DISPLAY 'DESC-ERRORE: '
      *              IJCCMQ03-DESC-ERRORE(IX-TAB-ERR)
      *          DISPLAY 'LIV-ERRORE: '
      *              IJCCMQ03-LIV-GRAVITA-BE(IX-TAB-ERR)
      *
      *      END-PERFORM.
      *    END-IF.
      *
      *DISPLAY-ESITO-PROD-EX.
      *    EXIT.
      *
      *DISPLAY-COMPONENTE.
      *
      *    IF (IDSV0001-DEBUG-MEDIO OR IDSV0001-DEBUG-ESASPERATO) AND
      *    NOT IDSV0001-ON-LINE
      *
      *       DISPLAY ' TIPO-LIVELLO : '
      *           ISPC0211-TIPO-LIVELLO(IX-AREA-ISPC0211)
      *       DISPLAY ' CODICE-LIVELLO: '
      *         ISPC0211-CODICE-LIVELLO(IX-AREA-ISPC0211)
      *       MOVE ISPC0211-IDENT-LIVELLO(IX-AREA-ISPC0211)
      *         TO WK-ID
      *       DISPLAY ' ID-LIVELLO :'   WK-ID
      *       MOVE ISPC0211-TIPO-DATO
      *        (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
      *         TO WS-TP-DATO
      *       DISPLAY 'TIPO-DATO: ' WS-TP-DATO
      *
      *       DISPLAY 'COMPONENTE: '
      *         ISPC0211-CODICE-VARIABILE
      *           (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
      *       EVALUATE TRUE
      *          WHEN TD-IMPORTO
      *             DISPLAY ' IMPORTO : '
      *             ISPC0211-VALORE-IMP
      *                (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
      *          WHEN TD-PERC-CENTESIMI
      *             DISPLAY ' PERCENTUALE : '
      *             ISPC0211-VALORE-PERC
      *                (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
      *
      *          WHEN OTHER
      *             DISPLAY '? ' WS-TP-DATO
      *       END-EVALUATE
      *    END-IF.
      *
      *DISPLAY-COMPONENTE-EX.
      *    EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI GESTIONE DISPLAY
      *----------------------------------------------------------------*
           COPY IDSP8888.
      * COPY SWAP ISPS0211
           COPY ISPP0211.

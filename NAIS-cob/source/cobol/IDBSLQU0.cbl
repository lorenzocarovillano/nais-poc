       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSLQU0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  03 GIU 2019.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDLQU0 END-EXEC.
           EXEC SQL INCLUDE IDBVLQU2 END-EXEC.
           EXEC SQL INCLUDE IDBVLQU3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVLQU1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 LIQ.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSLQU0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'LIQ' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_LIQ
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,TP_LIQ
                ,DESC_CAU_EVE_SIN
                ,COD_CAU_SIN
                ,COD_SIN_CATSTRF
                ,DT_MOR
                ,DT_DEN
                ,DT_PERV_DEN
                ,DT_RICH
                ,TP_SIN
                ,TP_RISC
                ,TP_MET_RISC
                ,DT_LIQ
                ,COD_DVS
                ,TOT_IMP_LRD_LIQTO
                ,TOT_IMP_PREST
                ,TOT_IMP_INTR_PREST
                ,TOT_IMP_UTI
                ,TOT_IMP_RIT_TFR
                ,TOT_IMP_RIT_ACC
                ,TOT_IMP_RIT_VIS
                ,TOT_IMPB_TFR
                ,TOT_IMPB_ACC
                ,TOT_IMPB_VIS
                ,TOT_IMP_RIMB
                ,IMPB_IMPST_PRVR
                ,IMPST_PRVR
                ,IMPB_IMPST_252
                ,IMPST_252
                ,TOT_IMP_IS
                ,IMP_DIR_LIQ
                ,TOT_IMP_NET_LIQTO
                ,MONT_END2000
                ,MONT_END2006
                ,PC_REN
                ,IMP_PNL
                ,IMPB_IRPEF
                ,IMPST_IRPEF
                ,DT_VLT
                ,DT_END_ISTR
                ,TP_RIMB
                ,SPE_RCS
                ,IB_LIQ
                ,TOT_IAS_ONER_PRVNT
                ,TOT_IAS_MGG_SIN
                ,TOT_IAS_RST_DPST
                ,IMP_ONER_LIQ
                ,COMPON_TAX_RIMB
                ,TP_MEZ_PAG
                ,IMP_EXCONTR
                ,IMP_INTR_RIT_PAG
                ,BNS_NON_GODUTO
                ,CNBT_INPSTFM
                ,IMPST_DA_RIMB
                ,IMPB_IS
                ,TAX_SEP
                ,IMPB_TAX_SEP
                ,IMPB_INTR_SU_PREST
                ,ADDIZ_COMUN
                ,IMPB_ADDIZ_COMUN
                ,ADDIZ_REGION
                ,IMPB_ADDIZ_REGION
                ,MONT_DAL2007
                ,IMPB_CNBT_INPSTFM
                ,IMP_LRD_DA_RIMB
                ,IMP_DIR_DA_RIMB
                ,RIS_MAT
                ,RIS_SPE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TOT_IAS_PNL
                ,FL_EVE_GARTO
                ,IMP_REN_K1
                ,IMP_REN_K2
                ,IMP_REN_K3
                ,PC_REN_K1
                ,PC_REN_K2
                ,PC_REN_K3
                ,TP_CAUS_ANTIC
                ,IMP_LRD_LIQTO_RILT
                ,IMPST_APPL_RILT
                ,PC_RISC_PARZ
                ,IMPST_BOLLO_TOT_V
                ,IMPST_BOLLO_DETT_C
                ,IMPST_BOLLO_TOT_SW
                ,IMPST_BOLLO_TOT_AA
                ,IMPB_VIS_1382011
                ,IMPST_VIS_1382011
                ,IMPB_IS_1382011
                ,IMPST_SOST_1382011
                ,PC_ABB_TIT_STAT
                ,IMPB_BOLLO_DETT_C
                ,FL_PRE_COMP
                ,IMPB_VIS_662014
                ,IMPST_VIS_662014
                ,IMPB_IS_662014
                ,IMPST_SOST_662014
                ,PC_ABB_TS_662014
                ,IMP_LRD_CALC_CP
                ,COS_TUNNEL_USCITA
             INTO
                :LQU-ID-LIQ
               ,:LQU-ID-OGG
               ,:LQU-TP-OGG
               ,:LQU-ID-MOVI-CRZ
               ,:LQU-ID-MOVI-CHIU
                :IND-LQU-ID-MOVI-CHIU
               ,:LQU-DT-INI-EFF-DB
               ,:LQU-DT-END-EFF-DB
               ,:LQU-COD-COMP-ANIA
               ,:LQU-IB-OGG
                :IND-LQU-IB-OGG
               ,:LQU-TP-LIQ
               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
                :IND-LQU-DESC-CAU-EVE-SIN
               ,:LQU-COD-CAU-SIN
                :IND-LQU-COD-CAU-SIN
               ,:LQU-COD-SIN-CATSTRF
                :IND-LQU-COD-SIN-CATSTRF
               ,:LQU-DT-MOR-DB
                :IND-LQU-DT-MOR
               ,:LQU-DT-DEN-DB
                :IND-LQU-DT-DEN
               ,:LQU-DT-PERV-DEN-DB
                :IND-LQU-DT-PERV-DEN
               ,:LQU-DT-RICH-DB
                :IND-LQU-DT-RICH
               ,:LQU-TP-SIN
                :IND-LQU-TP-SIN
               ,:LQU-TP-RISC
                :IND-LQU-TP-RISC
               ,:LQU-TP-MET-RISC
                :IND-LQU-TP-MET-RISC
               ,:LQU-DT-LIQ-DB
                :IND-LQU-DT-LIQ
               ,:LQU-COD-DVS
                :IND-LQU-COD-DVS
               ,:LQU-TOT-IMP-LRD-LIQTO
                :IND-LQU-TOT-IMP-LRD-LIQTO
               ,:LQU-TOT-IMP-PREST
                :IND-LQU-TOT-IMP-PREST
               ,:LQU-TOT-IMP-INTR-PREST
                :IND-LQU-TOT-IMP-INTR-PREST
               ,:LQU-TOT-IMP-UTI
                :IND-LQU-TOT-IMP-UTI
               ,:LQU-TOT-IMP-RIT-TFR
                :IND-LQU-TOT-IMP-RIT-TFR
               ,:LQU-TOT-IMP-RIT-ACC
                :IND-LQU-TOT-IMP-RIT-ACC
               ,:LQU-TOT-IMP-RIT-VIS
                :IND-LQU-TOT-IMP-RIT-VIS
               ,:LQU-TOT-IMPB-TFR
                :IND-LQU-TOT-IMPB-TFR
               ,:LQU-TOT-IMPB-ACC
                :IND-LQU-TOT-IMPB-ACC
               ,:LQU-TOT-IMPB-VIS
                :IND-LQU-TOT-IMPB-VIS
               ,:LQU-TOT-IMP-RIMB
                :IND-LQU-TOT-IMP-RIMB
               ,:LQU-IMPB-IMPST-PRVR
                :IND-LQU-IMPB-IMPST-PRVR
               ,:LQU-IMPST-PRVR
                :IND-LQU-IMPST-PRVR
               ,:LQU-IMPB-IMPST-252
                :IND-LQU-IMPB-IMPST-252
               ,:LQU-IMPST-252
                :IND-LQU-IMPST-252
               ,:LQU-TOT-IMP-IS
                :IND-LQU-TOT-IMP-IS
               ,:LQU-IMP-DIR-LIQ
                :IND-LQU-IMP-DIR-LIQ
               ,:LQU-TOT-IMP-NET-LIQTO
                :IND-LQU-TOT-IMP-NET-LIQTO
               ,:LQU-MONT-END2000
                :IND-LQU-MONT-END2000
               ,:LQU-MONT-END2006
                :IND-LQU-MONT-END2006
               ,:LQU-PC-REN
                :IND-LQU-PC-REN
               ,:LQU-IMP-PNL
                :IND-LQU-IMP-PNL
               ,:LQU-IMPB-IRPEF
                :IND-LQU-IMPB-IRPEF
               ,:LQU-IMPST-IRPEF
                :IND-LQU-IMPST-IRPEF
               ,:LQU-DT-VLT-DB
                :IND-LQU-DT-VLT
               ,:LQU-DT-END-ISTR-DB
                :IND-LQU-DT-END-ISTR
               ,:LQU-TP-RIMB
               ,:LQU-SPE-RCS
                :IND-LQU-SPE-RCS
               ,:LQU-IB-LIQ
                :IND-LQU-IB-LIQ
               ,:LQU-TOT-IAS-ONER-PRVNT
                :IND-LQU-TOT-IAS-ONER-PRVNT
               ,:LQU-TOT-IAS-MGG-SIN
                :IND-LQU-TOT-IAS-MGG-SIN
               ,:LQU-TOT-IAS-RST-DPST
                :IND-LQU-TOT-IAS-RST-DPST
               ,:LQU-IMP-ONER-LIQ
                :IND-LQU-IMP-ONER-LIQ
               ,:LQU-COMPON-TAX-RIMB
                :IND-LQU-COMPON-TAX-RIMB
               ,:LQU-TP-MEZ-PAG
                :IND-LQU-TP-MEZ-PAG
               ,:LQU-IMP-EXCONTR
                :IND-LQU-IMP-EXCONTR
               ,:LQU-IMP-INTR-RIT-PAG
                :IND-LQU-IMP-INTR-RIT-PAG
               ,:LQU-BNS-NON-GODUTO
                :IND-LQU-BNS-NON-GODUTO
               ,:LQU-CNBT-INPSTFM
                :IND-LQU-CNBT-INPSTFM
               ,:LQU-IMPST-DA-RIMB
                :IND-LQU-IMPST-DA-RIMB
               ,:LQU-IMPB-IS
                :IND-LQU-IMPB-IS
               ,:LQU-TAX-SEP
                :IND-LQU-TAX-SEP
               ,:LQU-IMPB-TAX-SEP
                :IND-LQU-IMPB-TAX-SEP
               ,:LQU-IMPB-INTR-SU-PREST
                :IND-LQU-IMPB-INTR-SU-PREST
               ,:LQU-ADDIZ-COMUN
                :IND-LQU-ADDIZ-COMUN
               ,:LQU-IMPB-ADDIZ-COMUN
                :IND-LQU-IMPB-ADDIZ-COMUN
               ,:LQU-ADDIZ-REGION
                :IND-LQU-ADDIZ-REGION
               ,:LQU-IMPB-ADDIZ-REGION
                :IND-LQU-IMPB-ADDIZ-REGION
               ,:LQU-MONT-DAL2007
                :IND-LQU-MONT-DAL2007
               ,:LQU-IMPB-CNBT-INPSTFM
                :IND-LQU-IMPB-CNBT-INPSTFM
               ,:LQU-IMP-LRD-DA-RIMB
                :IND-LQU-IMP-LRD-DA-RIMB
               ,:LQU-IMP-DIR-DA-RIMB
                :IND-LQU-IMP-DIR-DA-RIMB
               ,:LQU-RIS-MAT
                :IND-LQU-RIS-MAT
               ,:LQU-RIS-SPE
                :IND-LQU-RIS-SPE
               ,:LQU-DS-RIGA
               ,:LQU-DS-OPER-SQL
               ,:LQU-DS-VER
               ,:LQU-DS-TS-INI-CPTZ
               ,:LQU-DS-TS-END-CPTZ
               ,:LQU-DS-UTENTE
               ,:LQU-DS-STATO-ELAB
               ,:LQU-TOT-IAS-PNL
                :IND-LQU-TOT-IAS-PNL
               ,:LQU-FL-EVE-GARTO
                :IND-LQU-FL-EVE-GARTO
               ,:LQU-IMP-REN-K1
                :IND-LQU-IMP-REN-K1
               ,:LQU-IMP-REN-K2
                :IND-LQU-IMP-REN-K2
               ,:LQU-IMP-REN-K3
                :IND-LQU-IMP-REN-K3
               ,:LQU-PC-REN-K1
                :IND-LQU-PC-REN-K1
               ,:LQU-PC-REN-K2
                :IND-LQU-PC-REN-K2
               ,:LQU-PC-REN-K3
                :IND-LQU-PC-REN-K3
               ,:LQU-TP-CAUS-ANTIC
                :IND-LQU-TP-CAUS-ANTIC
               ,:LQU-IMP-LRD-LIQTO-RILT
                :IND-LQU-IMP-LRD-LIQTO-RILT
               ,:LQU-IMPST-APPL-RILT
                :IND-LQU-IMPST-APPL-RILT
               ,:LQU-PC-RISC-PARZ
                :IND-LQU-PC-RISC-PARZ
               ,:LQU-IMPST-BOLLO-TOT-V
                :IND-LQU-IMPST-BOLLO-TOT-V
               ,:LQU-IMPST-BOLLO-DETT-C
                :IND-LQU-IMPST-BOLLO-DETT-C
               ,:LQU-IMPST-BOLLO-TOT-SW
                :IND-LQU-IMPST-BOLLO-TOT-SW
               ,:LQU-IMPST-BOLLO-TOT-AA
                :IND-LQU-IMPST-BOLLO-TOT-AA
               ,:LQU-IMPB-VIS-1382011
                :IND-LQU-IMPB-VIS-1382011
               ,:LQU-IMPST-VIS-1382011
                :IND-LQU-IMPST-VIS-1382011
               ,:LQU-IMPB-IS-1382011
                :IND-LQU-IMPB-IS-1382011
               ,:LQU-IMPST-SOST-1382011
                :IND-LQU-IMPST-SOST-1382011
               ,:LQU-PC-ABB-TIT-STAT
                :IND-LQU-PC-ABB-TIT-STAT
               ,:LQU-IMPB-BOLLO-DETT-C
                :IND-LQU-IMPB-BOLLO-DETT-C
               ,:LQU-FL-PRE-COMP
                :IND-LQU-FL-PRE-COMP
               ,:LQU-IMPB-VIS-662014
                :IND-LQU-IMPB-VIS-662014
               ,:LQU-IMPST-VIS-662014
                :IND-LQU-IMPST-VIS-662014
               ,:LQU-IMPB-IS-662014
                :IND-LQU-IMPB-IS-662014
               ,:LQU-IMPST-SOST-662014
                :IND-LQU-IMPST-SOST-662014
               ,:LQU-PC-ABB-TS-662014
                :IND-LQU-PC-ABB-TS-662014
               ,:LQU-IMP-LRD-CALC-CP
                :IND-LQU-IMP-LRD-CALC-CP
               ,:LQU-COS-TUNNEL-USCITA
                :IND-LQU-COS-TUNNEL-USCITA
             FROM LIQ
             WHERE     DS_RIGA = :LQU-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO LIQ
                     (
                        ID_LIQ
                       ,ID_OGG
                       ,TP_OGG
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,IB_OGG
                       ,TP_LIQ
                       ,DESC_CAU_EVE_SIN
                       ,COD_CAU_SIN
                       ,COD_SIN_CATSTRF
                       ,DT_MOR
                       ,DT_DEN
                       ,DT_PERV_DEN
                       ,DT_RICH
                       ,TP_SIN
                       ,TP_RISC
                       ,TP_MET_RISC
                       ,DT_LIQ
                       ,COD_DVS
                       ,TOT_IMP_LRD_LIQTO
                       ,TOT_IMP_PREST
                       ,TOT_IMP_INTR_PREST
                       ,TOT_IMP_UTI
                       ,TOT_IMP_RIT_TFR
                       ,TOT_IMP_RIT_ACC
                       ,TOT_IMP_RIT_VIS
                       ,TOT_IMPB_TFR
                       ,TOT_IMPB_ACC
                       ,TOT_IMPB_VIS
                       ,TOT_IMP_RIMB
                       ,IMPB_IMPST_PRVR
                       ,IMPST_PRVR
                       ,IMPB_IMPST_252
                       ,IMPST_252
                       ,TOT_IMP_IS
                       ,IMP_DIR_LIQ
                       ,TOT_IMP_NET_LIQTO
                       ,MONT_END2000
                       ,MONT_END2006
                       ,PC_REN
                       ,IMP_PNL
                       ,IMPB_IRPEF
                       ,IMPST_IRPEF
                       ,DT_VLT
                       ,DT_END_ISTR
                       ,TP_RIMB
                       ,SPE_RCS
                       ,IB_LIQ
                       ,TOT_IAS_ONER_PRVNT
                       ,TOT_IAS_MGG_SIN
                       ,TOT_IAS_RST_DPST
                       ,IMP_ONER_LIQ
                       ,COMPON_TAX_RIMB
                       ,TP_MEZ_PAG
                       ,IMP_EXCONTR
                       ,IMP_INTR_RIT_PAG
                       ,BNS_NON_GODUTO
                       ,CNBT_INPSTFM
                       ,IMPST_DA_RIMB
                       ,IMPB_IS
                       ,TAX_SEP
                       ,IMPB_TAX_SEP
                       ,IMPB_INTR_SU_PREST
                       ,ADDIZ_COMUN
                       ,IMPB_ADDIZ_COMUN
                       ,ADDIZ_REGION
                       ,IMPB_ADDIZ_REGION
                       ,MONT_DAL2007
                       ,IMPB_CNBT_INPSTFM
                       ,IMP_LRD_DA_RIMB
                       ,IMP_DIR_DA_RIMB
                       ,RIS_MAT
                       ,RIS_SPE
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,TOT_IAS_PNL
                       ,FL_EVE_GARTO
                       ,IMP_REN_K1
                       ,IMP_REN_K2
                       ,IMP_REN_K3
                       ,PC_REN_K1
                       ,PC_REN_K2
                       ,PC_REN_K3
                       ,TP_CAUS_ANTIC
                       ,IMP_LRD_LIQTO_RILT
                       ,IMPST_APPL_RILT
                       ,PC_RISC_PARZ
                       ,IMPST_BOLLO_TOT_V
                       ,IMPST_BOLLO_DETT_C
                       ,IMPST_BOLLO_TOT_SW
                       ,IMPST_BOLLO_TOT_AA
                       ,IMPB_VIS_1382011
                       ,IMPST_VIS_1382011
                       ,IMPB_IS_1382011
                       ,IMPST_SOST_1382011
                       ,PC_ABB_TIT_STAT
                       ,IMPB_BOLLO_DETT_C
                       ,FL_PRE_COMP
                       ,IMPB_VIS_662014
                       ,IMPST_VIS_662014
                       ,IMPB_IS_662014
                       ,IMPST_SOST_662014
                       ,PC_ABB_TS_662014
                       ,IMP_LRD_CALC_CP
                       ,COS_TUNNEL_USCITA
                     )
                 VALUES
                     (
                       :LQU-ID-LIQ
                       ,:LQU-ID-OGG
                       ,:LQU-TP-OGG
                       ,:LQU-ID-MOVI-CRZ
                       ,:LQU-ID-MOVI-CHIU
                        :IND-LQU-ID-MOVI-CHIU
                       ,:LQU-DT-INI-EFF-DB
                       ,:LQU-DT-END-EFF-DB
                       ,:LQU-COD-COMP-ANIA
                       ,:LQU-IB-OGG
                        :IND-LQU-IB-OGG
                       ,:LQU-TP-LIQ
                       ,:LQU-DESC-CAU-EVE-SIN-VCHAR
                        :IND-LQU-DESC-CAU-EVE-SIN
                       ,:LQU-COD-CAU-SIN
                        :IND-LQU-COD-CAU-SIN
                       ,:LQU-COD-SIN-CATSTRF
                        :IND-LQU-COD-SIN-CATSTRF
                       ,:LQU-DT-MOR-DB
                        :IND-LQU-DT-MOR
                       ,:LQU-DT-DEN-DB
                        :IND-LQU-DT-DEN
                       ,:LQU-DT-PERV-DEN-DB
                        :IND-LQU-DT-PERV-DEN
                       ,:LQU-DT-RICH-DB
                        :IND-LQU-DT-RICH
                       ,:LQU-TP-SIN
                        :IND-LQU-TP-SIN
                       ,:LQU-TP-RISC
                        :IND-LQU-TP-RISC
                       ,:LQU-TP-MET-RISC
                        :IND-LQU-TP-MET-RISC
                       ,:LQU-DT-LIQ-DB
                        :IND-LQU-DT-LIQ
                       ,:LQU-COD-DVS
                        :IND-LQU-COD-DVS
                       ,:LQU-TOT-IMP-LRD-LIQTO
                        :IND-LQU-TOT-IMP-LRD-LIQTO
                       ,:LQU-TOT-IMP-PREST
                        :IND-LQU-TOT-IMP-PREST
                       ,:LQU-TOT-IMP-INTR-PREST
                        :IND-LQU-TOT-IMP-INTR-PREST
                       ,:LQU-TOT-IMP-UTI
                        :IND-LQU-TOT-IMP-UTI
                       ,:LQU-TOT-IMP-RIT-TFR
                        :IND-LQU-TOT-IMP-RIT-TFR
                       ,:LQU-TOT-IMP-RIT-ACC
                        :IND-LQU-TOT-IMP-RIT-ACC
                       ,:LQU-TOT-IMP-RIT-VIS
                        :IND-LQU-TOT-IMP-RIT-VIS
                       ,:LQU-TOT-IMPB-TFR
                        :IND-LQU-TOT-IMPB-TFR
                       ,:LQU-TOT-IMPB-ACC
                        :IND-LQU-TOT-IMPB-ACC
                       ,:LQU-TOT-IMPB-VIS
                        :IND-LQU-TOT-IMPB-VIS
                       ,:LQU-TOT-IMP-RIMB
                        :IND-LQU-TOT-IMP-RIMB
                       ,:LQU-IMPB-IMPST-PRVR
                        :IND-LQU-IMPB-IMPST-PRVR
                       ,:LQU-IMPST-PRVR
                        :IND-LQU-IMPST-PRVR
                       ,:LQU-IMPB-IMPST-252
                        :IND-LQU-IMPB-IMPST-252
                       ,:LQU-IMPST-252
                        :IND-LQU-IMPST-252
                       ,:LQU-TOT-IMP-IS
                        :IND-LQU-TOT-IMP-IS
                       ,:LQU-IMP-DIR-LIQ
                        :IND-LQU-IMP-DIR-LIQ
                       ,:LQU-TOT-IMP-NET-LIQTO
                        :IND-LQU-TOT-IMP-NET-LIQTO
                       ,:LQU-MONT-END2000
                        :IND-LQU-MONT-END2000
                       ,:LQU-MONT-END2006
                        :IND-LQU-MONT-END2006
                       ,:LQU-PC-REN
                        :IND-LQU-PC-REN
                       ,:LQU-IMP-PNL
                        :IND-LQU-IMP-PNL
                       ,:LQU-IMPB-IRPEF
                        :IND-LQU-IMPB-IRPEF
                       ,:LQU-IMPST-IRPEF
                        :IND-LQU-IMPST-IRPEF
                       ,:LQU-DT-VLT-DB
                        :IND-LQU-DT-VLT
                       ,:LQU-DT-END-ISTR-DB
                        :IND-LQU-DT-END-ISTR
                       ,:LQU-TP-RIMB
                       ,:LQU-SPE-RCS
                        :IND-LQU-SPE-RCS
                       ,:LQU-IB-LIQ
                        :IND-LQU-IB-LIQ
                       ,:LQU-TOT-IAS-ONER-PRVNT
                        :IND-LQU-TOT-IAS-ONER-PRVNT
                       ,:LQU-TOT-IAS-MGG-SIN
                        :IND-LQU-TOT-IAS-MGG-SIN
                       ,:LQU-TOT-IAS-RST-DPST
                        :IND-LQU-TOT-IAS-RST-DPST
                       ,:LQU-IMP-ONER-LIQ
                        :IND-LQU-IMP-ONER-LIQ
                       ,:LQU-COMPON-TAX-RIMB
                        :IND-LQU-COMPON-TAX-RIMB
                       ,:LQU-TP-MEZ-PAG
                        :IND-LQU-TP-MEZ-PAG
                       ,:LQU-IMP-EXCONTR
                        :IND-LQU-IMP-EXCONTR
                       ,:LQU-IMP-INTR-RIT-PAG
                        :IND-LQU-IMP-INTR-RIT-PAG
                       ,:LQU-BNS-NON-GODUTO
                        :IND-LQU-BNS-NON-GODUTO
                       ,:LQU-CNBT-INPSTFM
                        :IND-LQU-CNBT-INPSTFM
                       ,:LQU-IMPST-DA-RIMB
                        :IND-LQU-IMPST-DA-RIMB
                       ,:LQU-IMPB-IS
                        :IND-LQU-IMPB-IS
                       ,:LQU-TAX-SEP
                        :IND-LQU-TAX-SEP
                       ,:LQU-IMPB-TAX-SEP
                        :IND-LQU-IMPB-TAX-SEP
                       ,:LQU-IMPB-INTR-SU-PREST
                        :IND-LQU-IMPB-INTR-SU-PREST
                       ,:LQU-ADDIZ-COMUN
                        :IND-LQU-ADDIZ-COMUN
                       ,:LQU-IMPB-ADDIZ-COMUN
                        :IND-LQU-IMPB-ADDIZ-COMUN
                       ,:LQU-ADDIZ-REGION
                        :IND-LQU-ADDIZ-REGION
                       ,:LQU-IMPB-ADDIZ-REGION
                        :IND-LQU-IMPB-ADDIZ-REGION
                       ,:LQU-MONT-DAL2007
                        :IND-LQU-MONT-DAL2007
                       ,:LQU-IMPB-CNBT-INPSTFM
                        :IND-LQU-IMPB-CNBT-INPSTFM
                       ,:LQU-IMP-LRD-DA-RIMB
                        :IND-LQU-IMP-LRD-DA-RIMB
                       ,:LQU-IMP-DIR-DA-RIMB
                        :IND-LQU-IMP-DIR-DA-RIMB
                       ,:LQU-RIS-MAT
                        :IND-LQU-RIS-MAT
                       ,:LQU-RIS-SPE
                        :IND-LQU-RIS-SPE
                       ,:LQU-DS-RIGA
                       ,:LQU-DS-OPER-SQL
                       ,:LQU-DS-VER
                       ,:LQU-DS-TS-INI-CPTZ
                       ,:LQU-DS-TS-END-CPTZ
                       ,:LQU-DS-UTENTE
                       ,:LQU-DS-STATO-ELAB
                       ,:LQU-TOT-IAS-PNL
                        :IND-LQU-TOT-IAS-PNL
                       ,:LQU-FL-EVE-GARTO
                        :IND-LQU-FL-EVE-GARTO
                       ,:LQU-IMP-REN-K1
                        :IND-LQU-IMP-REN-K1
                       ,:LQU-IMP-REN-K2
                        :IND-LQU-IMP-REN-K2
                       ,:LQU-IMP-REN-K3
                        :IND-LQU-IMP-REN-K3
                       ,:LQU-PC-REN-K1
                        :IND-LQU-PC-REN-K1
                       ,:LQU-PC-REN-K2
                        :IND-LQU-PC-REN-K2
                       ,:LQU-PC-REN-K3
                        :IND-LQU-PC-REN-K3
                       ,:LQU-TP-CAUS-ANTIC
                        :IND-LQU-TP-CAUS-ANTIC
                       ,:LQU-IMP-LRD-LIQTO-RILT
                        :IND-LQU-IMP-LRD-LIQTO-RILT
                       ,:LQU-IMPST-APPL-RILT
                        :IND-LQU-IMPST-APPL-RILT
                       ,:LQU-PC-RISC-PARZ
                        :IND-LQU-PC-RISC-PARZ
                       ,:LQU-IMPST-BOLLO-TOT-V
                        :IND-LQU-IMPST-BOLLO-TOT-V
                       ,:LQU-IMPST-BOLLO-DETT-C
                        :IND-LQU-IMPST-BOLLO-DETT-C
                       ,:LQU-IMPST-BOLLO-TOT-SW
                        :IND-LQU-IMPST-BOLLO-TOT-SW
                       ,:LQU-IMPST-BOLLO-TOT-AA
                        :IND-LQU-IMPST-BOLLO-TOT-AA
                       ,:LQU-IMPB-VIS-1382011
                        :IND-LQU-IMPB-VIS-1382011
                       ,:LQU-IMPST-VIS-1382011
                        :IND-LQU-IMPST-VIS-1382011
                       ,:LQU-IMPB-IS-1382011
                        :IND-LQU-IMPB-IS-1382011
                       ,:LQU-IMPST-SOST-1382011
                        :IND-LQU-IMPST-SOST-1382011
                       ,:LQU-PC-ABB-TIT-STAT
                        :IND-LQU-PC-ABB-TIT-STAT
                       ,:LQU-IMPB-BOLLO-DETT-C
                        :IND-LQU-IMPB-BOLLO-DETT-C
                       ,:LQU-FL-PRE-COMP
                        :IND-LQU-FL-PRE-COMP
                       ,:LQU-IMPB-VIS-662014
                        :IND-LQU-IMPB-VIS-662014
                       ,:LQU-IMPST-VIS-662014
                        :IND-LQU-IMPST-VIS-662014
                       ,:LQU-IMPB-IS-662014
                        :IND-LQU-IMPB-IS-662014
                       ,:LQU-IMPST-SOST-662014
                        :IND-LQU-IMPST-SOST-662014
                       ,:LQU-PC-ABB-TS-662014
                        :IND-LQU-PC-ABB-TS-662014
                       ,:LQU-IMP-LRD-CALC-CP
                        :IND-LQU-IMP-LRD-CALC-CP
                       ,:LQU-COS-TUNNEL-USCITA
                        :IND-LQU-COS-TUNNEL-USCITA
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE LIQ SET

                   ID_LIQ                 =
                :LQU-ID-LIQ
                  ,ID_OGG                 =
                :LQU-ID-OGG
                  ,TP_OGG                 =
                :LQU-TP-OGG
                  ,ID_MOVI_CRZ            =
                :LQU-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :LQU-ID-MOVI-CHIU
                                       :IND-LQU-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :LQU-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :LQU-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :LQU-COD-COMP-ANIA
                  ,IB_OGG                 =
                :LQU-IB-OGG
                                       :IND-LQU-IB-OGG
                  ,TP_LIQ                 =
                :LQU-TP-LIQ
                  ,DESC_CAU_EVE_SIN       =
                :LQU-DESC-CAU-EVE-SIN-VCHAR
                                       :IND-LQU-DESC-CAU-EVE-SIN
                  ,COD_CAU_SIN            =
                :LQU-COD-CAU-SIN
                                       :IND-LQU-COD-CAU-SIN
                  ,COD_SIN_CATSTRF        =
                :LQU-COD-SIN-CATSTRF
                                       :IND-LQU-COD-SIN-CATSTRF
                  ,DT_MOR                 =
           :LQU-DT-MOR-DB
                                       :IND-LQU-DT-MOR
                  ,DT_DEN                 =
           :LQU-DT-DEN-DB
                                       :IND-LQU-DT-DEN
                  ,DT_PERV_DEN            =
           :LQU-DT-PERV-DEN-DB
                                       :IND-LQU-DT-PERV-DEN
                  ,DT_RICH                =
           :LQU-DT-RICH-DB
                                       :IND-LQU-DT-RICH
                  ,TP_SIN                 =
                :LQU-TP-SIN
                                       :IND-LQU-TP-SIN
                  ,TP_RISC                =
                :LQU-TP-RISC
                                       :IND-LQU-TP-RISC
                  ,TP_MET_RISC            =
                :LQU-TP-MET-RISC
                                       :IND-LQU-TP-MET-RISC
                  ,DT_LIQ                 =
           :LQU-DT-LIQ-DB
                                       :IND-LQU-DT-LIQ
                  ,COD_DVS                =
                :LQU-COD-DVS
                                       :IND-LQU-COD-DVS
                  ,TOT_IMP_LRD_LIQTO      =
                :LQU-TOT-IMP-LRD-LIQTO
                                       :IND-LQU-TOT-IMP-LRD-LIQTO
                  ,TOT_IMP_PREST          =
                :LQU-TOT-IMP-PREST
                                       :IND-LQU-TOT-IMP-PREST
                  ,TOT_IMP_INTR_PREST     =
                :LQU-TOT-IMP-INTR-PREST
                                       :IND-LQU-TOT-IMP-INTR-PREST
                  ,TOT_IMP_UTI            =
                :LQU-TOT-IMP-UTI
                                       :IND-LQU-TOT-IMP-UTI
                  ,TOT_IMP_RIT_TFR        =
                :LQU-TOT-IMP-RIT-TFR
                                       :IND-LQU-TOT-IMP-RIT-TFR
                  ,TOT_IMP_RIT_ACC        =
                :LQU-TOT-IMP-RIT-ACC
                                       :IND-LQU-TOT-IMP-RIT-ACC
                  ,TOT_IMP_RIT_VIS        =
                :LQU-TOT-IMP-RIT-VIS
                                       :IND-LQU-TOT-IMP-RIT-VIS
                  ,TOT_IMPB_TFR           =
                :LQU-TOT-IMPB-TFR
                                       :IND-LQU-TOT-IMPB-TFR
                  ,TOT_IMPB_ACC           =
                :LQU-TOT-IMPB-ACC
                                       :IND-LQU-TOT-IMPB-ACC
                  ,TOT_IMPB_VIS           =
                :LQU-TOT-IMPB-VIS
                                       :IND-LQU-TOT-IMPB-VIS
                  ,TOT_IMP_RIMB           =
                :LQU-TOT-IMP-RIMB
                                       :IND-LQU-TOT-IMP-RIMB
                  ,IMPB_IMPST_PRVR        =
                :LQU-IMPB-IMPST-PRVR
                                       :IND-LQU-IMPB-IMPST-PRVR
                  ,IMPST_PRVR             =
                :LQU-IMPST-PRVR
                                       :IND-LQU-IMPST-PRVR
                  ,IMPB_IMPST_252         =
                :LQU-IMPB-IMPST-252
                                       :IND-LQU-IMPB-IMPST-252
                  ,IMPST_252              =
                :LQU-IMPST-252
                                       :IND-LQU-IMPST-252
                  ,TOT_IMP_IS             =
                :LQU-TOT-IMP-IS
                                       :IND-LQU-TOT-IMP-IS
                  ,IMP_DIR_LIQ            =
                :LQU-IMP-DIR-LIQ
                                       :IND-LQU-IMP-DIR-LIQ
                  ,TOT_IMP_NET_LIQTO      =
                :LQU-TOT-IMP-NET-LIQTO
                                       :IND-LQU-TOT-IMP-NET-LIQTO
                  ,MONT_END2000           =
                :LQU-MONT-END2000
                                       :IND-LQU-MONT-END2000
                  ,MONT_END2006           =
                :LQU-MONT-END2006
                                       :IND-LQU-MONT-END2006
                  ,PC_REN                 =
                :LQU-PC-REN
                                       :IND-LQU-PC-REN
                  ,IMP_PNL                =
                :LQU-IMP-PNL
                                       :IND-LQU-IMP-PNL
                  ,IMPB_IRPEF             =
                :LQU-IMPB-IRPEF
                                       :IND-LQU-IMPB-IRPEF
                  ,IMPST_IRPEF            =
                :LQU-IMPST-IRPEF
                                       :IND-LQU-IMPST-IRPEF
                  ,DT_VLT                 =
           :LQU-DT-VLT-DB
                                       :IND-LQU-DT-VLT
                  ,DT_END_ISTR            =
           :LQU-DT-END-ISTR-DB
                                       :IND-LQU-DT-END-ISTR
                  ,TP_RIMB                =
                :LQU-TP-RIMB
                  ,SPE_RCS                =
                :LQU-SPE-RCS
                                       :IND-LQU-SPE-RCS
                  ,IB_LIQ                 =
                :LQU-IB-LIQ
                                       :IND-LQU-IB-LIQ
                  ,TOT_IAS_ONER_PRVNT     =
                :LQU-TOT-IAS-ONER-PRVNT
                                       :IND-LQU-TOT-IAS-ONER-PRVNT
                  ,TOT_IAS_MGG_SIN        =
                :LQU-TOT-IAS-MGG-SIN
                                       :IND-LQU-TOT-IAS-MGG-SIN
                  ,TOT_IAS_RST_DPST       =
                :LQU-TOT-IAS-RST-DPST
                                       :IND-LQU-TOT-IAS-RST-DPST
                  ,IMP_ONER_LIQ           =
                :LQU-IMP-ONER-LIQ
                                       :IND-LQU-IMP-ONER-LIQ
                  ,COMPON_TAX_RIMB        =
                :LQU-COMPON-TAX-RIMB
                                       :IND-LQU-COMPON-TAX-RIMB
                  ,TP_MEZ_PAG             =
                :LQU-TP-MEZ-PAG
                                       :IND-LQU-TP-MEZ-PAG
                  ,IMP_EXCONTR            =
                :LQU-IMP-EXCONTR
                                       :IND-LQU-IMP-EXCONTR
                  ,IMP_INTR_RIT_PAG       =
                :LQU-IMP-INTR-RIT-PAG
                                       :IND-LQU-IMP-INTR-RIT-PAG
                  ,BNS_NON_GODUTO         =
                :LQU-BNS-NON-GODUTO
                                       :IND-LQU-BNS-NON-GODUTO
                  ,CNBT_INPSTFM           =
                :LQU-CNBT-INPSTFM
                                       :IND-LQU-CNBT-INPSTFM
                  ,IMPST_DA_RIMB          =
                :LQU-IMPST-DA-RIMB
                                       :IND-LQU-IMPST-DA-RIMB
                  ,IMPB_IS                =
                :LQU-IMPB-IS
                                       :IND-LQU-IMPB-IS
                  ,TAX_SEP                =
                :LQU-TAX-SEP
                                       :IND-LQU-TAX-SEP
                  ,IMPB_TAX_SEP           =
                :LQU-IMPB-TAX-SEP
                                       :IND-LQU-IMPB-TAX-SEP
                  ,IMPB_INTR_SU_PREST     =
                :LQU-IMPB-INTR-SU-PREST
                                       :IND-LQU-IMPB-INTR-SU-PREST
                  ,ADDIZ_COMUN            =
                :LQU-ADDIZ-COMUN
                                       :IND-LQU-ADDIZ-COMUN
                  ,IMPB_ADDIZ_COMUN       =
                :LQU-IMPB-ADDIZ-COMUN
                                       :IND-LQU-IMPB-ADDIZ-COMUN
                  ,ADDIZ_REGION           =
                :LQU-ADDIZ-REGION
                                       :IND-LQU-ADDIZ-REGION
                  ,IMPB_ADDIZ_REGION      =
                :LQU-IMPB-ADDIZ-REGION
                                       :IND-LQU-IMPB-ADDIZ-REGION
                  ,MONT_DAL2007           =
                :LQU-MONT-DAL2007
                                       :IND-LQU-MONT-DAL2007
                  ,IMPB_CNBT_INPSTFM      =
                :LQU-IMPB-CNBT-INPSTFM
                                       :IND-LQU-IMPB-CNBT-INPSTFM
                  ,IMP_LRD_DA_RIMB        =
                :LQU-IMP-LRD-DA-RIMB
                                       :IND-LQU-IMP-LRD-DA-RIMB
                  ,IMP_DIR_DA_RIMB        =
                :LQU-IMP-DIR-DA-RIMB
                                       :IND-LQU-IMP-DIR-DA-RIMB
                  ,RIS_MAT                =
                :LQU-RIS-MAT
                                       :IND-LQU-RIS-MAT
                  ,RIS_SPE                =
                :LQU-RIS-SPE
                                       :IND-LQU-RIS-SPE
                  ,DS_RIGA                =
                :LQU-DS-RIGA
                  ,DS_OPER_SQL            =
                :LQU-DS-OPER-SQL
                  ,DS_VER                 =
                :LQU-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :LQU-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :LQU-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :LQU-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :LQU-DS-STATO-ELAB
                  ,TOT_IAS_PNL            =
                :LQU-TOT-IAS-PNL
                                       :IND-LQU-TOT-IAS-PNL
                  ,FL_EVE_GARTO           =
                :LQU-FL-EVE-GARTO
                                       :IND-LQU-FL-EVE-GARTO
                  ,IMP_REN_K1             =
                :LQU-IMP-REN-K1
                                       :IND-LQU-IMP-REN-K1
                  ,IMP_REN_K2             =
                :LQU-IMP-REN-K2
                                       :IND-LQU-IMP-REN-K2
                  ,IMP_REN_K3             =
                :LQU-IMP-REN-K3
                                       :IND-LQU-IMP-REN-K3
                  ,PC_REN_K1              =
                :LQU-PC-REN-K1
                                       :IND-LQU-PC-REN-K1
                  ,PC_REN_K2              =
                :LQU-PC-REN-K2
                                       :IND-LQU-PC-REN-K2
                  ,PC_REN_K3              =
                :LQU-PC-REN-K3
                                       :IND-LQU-PC-REN-K3
                  ,TP_CAUS_ANTIC          =
                :LQU-TP-CAUS-ANTIC
                                       :IND-LQU-TP-CAUS-ANTIC
                  ,IMP_LRD_LIQTO_RILT     =
                :LQU-IMP-LRD-LIQTO-RILT
                                       :IND-LQU-IMP-LRD-LIQTO-RILT
                  ,IMPST_APPL_RILT        =
                :LQU-IMPST-APPL-RILT
                                       :IND-LQU-IMPST-APPL-RILT
                  ,PC_RISC_PARZ           =
                :LQU-PC-RISC-PARZ
                                       :IND-LQU-PC-RISC-PARZ
                  ,IMPST_BOLLO_TOT_V      =
                :LQU-IMPST-BOLLO-TOT-V
                                       :IND-LQU-IMPST-BOLLO-TOT-V
                  ,IMPST_BOLLO_DETT_C     =
                :LQU-IMPST-BOLLO-DETT-C
                                       :IND-LQU-IMPST-BOLLO-DETT-C
                  ,IMPST_BOLLO_TOT_SW     =
                :LQU-IMPST-BOLLO-TOT-SW
                                       :IND-LQU-IMPST-BOLLO-TOT-SW
                  ,IMPST_BOLLO_TOT_AA     =
                :LQU-IMPST-BOLLO-TOT-AA
                                       :IND-LQU-IMPST-BOLLO-TOT-AA
                  ,IMPB_VIS_1382011       =
                :LQU-IMPB-VIS-1382011
                                       :IND-LQU-IMPB-VIS-1382011
                  ,IMPST_VIS_1382011      =
                :LQU-IMPST-VIS-1382011
                                       :IND-LQU-IMPST-VIS-1382011
                  ,IMPB_IS_1382011        =
                :LQU-IMPB-IS-1382011
                                       :IND-LQU-IMPB-IS-1382011
                  ,IMPST_SOST_1382011     =
                :LQU-IMPST-SOST-1382011
                                       :IND-LQU-IMPST-SOST-1382011
                  ,PC_ABB_TIT_STAT        =
                :LQU-PC-ABB-TIT-STAT
                                       :IND-LQU-PC-ABB-TIT-STAT
                  ,IMPB_BOLLO_DETT_C      =
                :LQU-IMPB-BOLLO-DETT-C
                                       :IND-LQU-IMPB-BOLLO-DETT-C
                  ,FL_PRE_COMP            =
                :LQU-FL-PRE-COMP
                                       :IND-LQU-FL-PRE-COMP
                  ,IMPB_VIS_662014        =
                :LQU-IMPB-VIS-662014
                                       :IND-LQU-IMPB-VIS-662014
                  ,IMPST_VIS_662014       =
                :LQU-IMPST-VIS-662014
                                       :IND-LQU-IMPST-VIS-662014
                  ,IMPB_IS_662014         =
                :LQU-IMPB-IS-662014
                                       :IND-LQU-IMPB-IS-662014
                  ,IMPST_SOST_662014      =
                :LQU-IMPST-SOST-662014
                                       :IND-LQU-IMPST-SOST-662014
                  ,PC_ABB_TS_662014       =
                :LQU-PC-ABB-TS-662014
                                       :IND-LQU-PC-ABB-TS-662014
                  ,IMP_LRD_CALC_CP        =
                :LQU-IMP-LRD-CALC-CP
                                       :IND-LQU-IMP-LRD-CALC-CP
                  ,COS_TUNNEL_USCITA      =
                :LQU-COS-TUNNEL-USCITA
                                       :IND-LQU-COS-TUNNEL-USCITA
                WHERE     DS_RIGA = :LQU-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM LIQ
                WHERE     DS_RIGA = :LQU-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-LQU CURSOR FOR
              SELECT
                     ID_LIQ
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,IB_OGG
                    ,TP_LIQ
                    ,DESC_CAU_EVE_SIN
                    ,COD_CAU_SIN
                    ,COD_SIN_CATSTRF
                    ,DT_MOR
                    ,DT_DEN
                    ,DT_PERV_DEN
                    ,DT_RICH
                    ,TP_SIN
                    ,TP_RISC
                    ,TP_MET_RISC
                    ,DT_LIQ
                    ,COD_DVS
                    ,TOT_IMP_LRD_LIQTO
                    ,TOT_IMP_PREST
                    ,TOT_IMP_INTR_PREST
                    ,TOT_IMP_UTI
                    ,TOT_IMP_RIT_TFR
                    ,TOT_IMP_RIT_ACC
                    ,TOT_IMP_RIT_VIS
                    ,TOT_IMPB_TFR
                    ,TOT_IMPB_ACC
                    ,TOT_IMPB_VIS
                    ,TOT_IMP_RIMB
                    ,IMPB_IMPST_PRVR
                    ,IMPST_PRVR
                    ,IMPB_IMPST_252
                    ,IMPST_252
                    ,TOT_IMP_IS
                    ,IMP_DIR_LIQ
                    ,TOT_IMP_NET_LIQTO
                    ,MONT_END2000
                    ,MONT_END2006
                    ,PC_REN
                    ,IMP_PNL
                    ,IMPB_IRPEF
                    ,IMPST_IRPEF
                    ,DT_VLT
                    ,DT_END_ISTR
                    ,TP_RIMB
                    ,SPE_RCS
                    ,IB_LIQ
                    ,TOT_IAS_ONER_PRVNT
                    ,TOT_IAS_MGG_SIN
                    ,TOT_IAS_RST_DPST
                    ,IMP_ONER_LIQ
                    ,COMPON_TAX_RIMB
                    ,TP_MEZ_PAG
                    ,IMP_EXCONTR
                    ,IMP_INTR_RIT_PAG
                    ,BNS_NON_GODUTO
                    ,CNBT_INPSTFM
                    ,IMPST_DA_RIMB
                    ,IMPB_IS
                    ,TAX_SEP
                    ,IMPB_TAX_SEP
                    ,IMPB_INTR_SU_PREST
                    ,ADDIZ_COMUN
                    ,IMPB_ADDIZ_COMUN
                    ,ADDIZ_REGION
                    ,IMPB_ADDIZ_REGION
                    ,MONT_DAL2007
                    ,IMPB_CNBT_INPSTFM
                    ,IMP_LRD_DA_RIMB
                    ,IMP_DIR_DA_RIMB
                    ,RIS_MAT
                    ,RIS_SPE
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,TOT_IAS_PNL
                    ,FL_EVE_GARTO
                    ,IMP_REN_K1
                    ,IMP_REN_K2
                    ,IMP_REN_K3
                    ,PC_REN_K1
                    ,PC_REN_K2
                    ,PC_REN_K3
                    ,TP_CAUS_ANTIC
                    ,IMP_LRD_LIQTO_RILT
                    ,IMPST_APPL_RILT
                    ,PC_RISC_PARZ
                    ,IMPST_BOLLO_TOT_V
                    ,IMPST_BOLLO_DETT_C
                    ,IMPST_BOLLO_TOT_SW
                    ,IMPST_BOLLO_TOT_AA
                    ,IMPB_VIS_1382011
                    ,IMPST_VIS_1382011
                    ,IMPB_IS_1382011
                    ,IMPST_SOST_1382011
                    ,PC_ABB_TIT_STAT
                    ,IMPB_BOLLO_DETT_C
                    ,FL_PRE_COMP
                    ,IMPB_VIS_662014
                    ,IMPST_VIS_662014
                    ,IMPB_IS_662014
                    ,IMPST_SOST_662014
                    ,PC_ABB_TS_662014
                    ,IMP_LRD_CALC_CP
                    ,COS_TUNNEL_USCITA
              FROM LIQ
              WHERE     ID_LIQ = :LQU-ID-LIQ
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY DT_INI_EFF ASC

           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_LIQ
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,TP_LIQ
                ,DESC_CAU_EVE_SIN
                ,COD_CAU_SIN
                ,COD_SIN_CATSTRF
                ,DT_MOR
                ,DT_DEN
                ,DT_PERV_DEN
                ,DT_RICH
                ,TP_SIN
                ,TP_RISC
                ,TP_MET_RISC
                ,DT_LIQ
                ,COD_DVS
                ,TOT_IMP_LRD_LIQTO
                ,TOT_IMP_PREST
                ,TOT_IMP_INTR_PREST
                ,TOT_IMP_UTI
                ,TOT_IMP_RIT_TFR
                ,TOT_IMP_RIT_ACC
                ,TOT_IMP_RIT_VIS
                ,TOT_IMPB_TFR
                ,TOT_IMPB_ACC
                ,TOT_IMPB_VIS
                ,TOT_IMP_RIMB
                ,IMPB_IMPST_PRVR
                ,IMPST_PRVR
                ,IMPB_IMPST_252
                ,IMPST_252
                ,TOT_IMP_IS
                ,IMP_DIR_LIQ
                ,TOT_IMP_NET_LIQTO
                ,MONT_END2000
                ,MONT_END2006
                ,PC_REN
                ,IMP_PNL
                ,IMPB_IRPEF
                ,IMPST_IRPEF
                ,DT_VLT
                ,DT_END_ISTR
                ,TP_RIMB
                ,SPE_RCS
                ,IB_LIQ
                ,TOT_IAS_ONER_PRVNT
                ,TOT_IAS_MGG_SIN
                ,TOT_IAS_RST_DPST
                ,IMP_ONER_LIQ
                ,COMPON_TAX_RIMB
                ,TP_MEZ_PAG
                ,IMP_EXCONTR
                ,IMP_INTR_RIT_PAG
                ,BNS_NON_GODUTO
                ,CNBT_INPSTFM
                ,IMPST_DA_RIMB
                ,IMPB_IS
                ,TAX_SEP
                ,IMPB_TAX_SEP
                ,IMPB_INTR_SU_PREST
                ,ADDIZ_COMUN
                ,IMPB_ADDIZ_COMUN
                ,ADDIZ_REGION
                ,IMPB_ADDIZ_REGION
                ,MONT_DAL2007
                ,IMPB_CNBT_INPSTFM
                ,IMP_LRD_DA_RIMB
                ,IMP_DIR_DA_RIMB
                ,RIS_MAT
                ,RIS_SPE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TOT_IAS_PNL
                ,FL_EVE_GARTO
                ,IMP_REN_K1
                ,IMP_REN_K2
                ,IMP_REN_K3
                ,PC_REN_K1
                ,PC_REN_K2
                ,PC_REN_K3
                ,TP_CAUS_ANTIC
                ,IMP_LRD_LIQTO_RILT
                ,IMPST_APPL_RILT
                ,PC_RISC_PARZ
                ,IMPST_BOLLO_TOT_V
                ,IMPST_BOLLO_DETT_C
                ,IMPST_BOLLO_TOT_SW
                ,IMPST_BOLLO_TOT_AA
                ,IMPB_VIS_1382011
                ,IMPST_VIS_1382011
                ,IMPB_IS_1382011
                ,IMPST_SOST_1382011
                ,PC_ABB_TIT_STAT
                ,IMPB_BOLLO_DETT_C
                ,FL_PRE_COMP
                ,IMPB_VIS_662014
                ,IMPST_VIS_662014
                ,IMPB_IS_662014
                ,IMPST_SOST_662014
                ,PC_ABB_TS_662014
                ,IMP_LRD_CALC_CP
                ,COS_TUNNEL_USCITA
             INTO
                :LQU-ID-LIQ
               ,:LQU-ID-OGG
               ,:LQU-TP-OGG
               ,:LQU-ID-MOVI-CRZ
               ,:LQU-ID-MOVI-CHIU
                :IND-LQU-ID-MOVI-CHIU
               ,:LQU-DT-INI-EFF-DB
               ,:LQU-DT-END-EFF-DB
               ,:LQU-COD-COMP-ANIA
               ,:LQU-IB-OGG
                :IND-LQU-IB-OGG
               ,:LQU-TP-LIQ
               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
                :IND-LQU-DESC-CAU-EVE-SIN
               ,:LQU-COD-CAU-SIN
                :IND-LQU-COD-CAU-SIN
               ,:LQU-COD-SIN-CATSTRF
                :IND-LQU-COD-SIN-CATSTRF
               ,:LQU-DT-MOR-DB
                :IND-LQU-DT-MOR
               ,:LQU-DT-DEN-DB
                :IND-LQU-DT-DEN
               ,:LQU-DT-PERV-DEN-DB
                :IND-LQU-DT-PERV-DEN
               ,:LQU-DT-RICH-DB
                :IND-LQU-DT-RICH
               ,:LQU-TP-SIN
                :IND-LQU-TP-SIN
               ,:LQU-TP-RISC
                :IND-LQU-TP-RISC
               ,:LQU-TP-MET-RISC
                :IND-LQU-TP-MET-RISC
               ,:LQU-DT-LIQ-DB
                :IND-LQU-DT-LIQ
               ,:LQU-COD-DVS
                :IND-LQU-COD-DVS
               ,:LQU-TOT-IMP-LRD-LIQTO
                :IND-LQU-TOT-IMP-LRD-LIQTO
               ,:LQU-TOT-IMP-PREST
                :IND-LQU-TOT-IMP-PREST
               ,:LQU-TOT-IMP-INTR-PREST
                :IND-LQU-TOT-IMP-INTR-PREST
               ,:LQU-TOT-IMP-UTI
                :IND-LQU-TOT-IMP-UTI
               ,:LQU-TOT-IMP-RIT-TFR
                :IND-LQU-TOT-IMP-RIT-TFR
               ,:LQU-TOT-IMP-RIT-ACC
                :IND-LQU-TOT-IMP-RIT-ACC
               ,:LQU-TOT-IMP-RIT-VIS
                :IND-LQU-TOT-IMP-RIT-VIS
               ,:LQU-TOT-IMPB-TFR
                :IND-LQU-TOT-IMPB-TFR
               ,:LQU-TOT-IMPB-ACC
                :IND-LQU-TOT-IMPB-ACC
               ,:LQU-TOT-IMPB-VIS
                :IND-LQU-TOT-IMPB-VIS
               ,:LQU-TOT-IMP-RIMB
                :IND-LQU-TOT-IMP-RIMB
               ,:LQU-IMPB-IMPST-PRVR
                :IND-LQU-IMPB-IMPST-PRVR
               ,:LQU-IMPST-PRVR
                :IND-LQU-IMPST-PRVR
               ,:LQU-IMPB-IMPST-252
                :IND-LQU-IMPB-IMPST-252
               ,:LQU-IMPST-252
                :IND-LQU-IMPST-252
               ,:LQU-TOT-IMP-IS
                :IND-LQU-TOT-IMP-IS
               ,:LQU-IMP-DIR-LIQ
                :IND-LQU-IMP-DIR-LIQ
               ,:LQU-TOT-IMP-NET-LIQTO
                :IND-LQU-TOT-IMP-NET-LIQTO
               ,:LQU-MONT-END2000
                :IND-LQU-MONT-END2000
               ,:LQU-MONT-END2006
                :IND-LQU-MONT-END2006
               ,:LQU-PC-REN
                :IND-LQU-PC-REN
               ,:LQU-IMP-PNL
                :IND-LQU-IMP-PNL
               ,:LQU-IMPB-IRPEF
                :IND-LQU-IMPB-IRPEF
               ,:LQU-IMPST-IRPEF
                :IND-LQU-IMPST-IRPEF
               ,:LQU-DT-VLT-DB
                :IND-LQU-DT-VLT
               ,:LQU-DT-END-ISTR-DB
                :IND-LQU-DT-END-ISTR
               ,:LQU-TP-RIMB
               ,:LQU-SPE-RCS
                :IND-LQU-SPE-RCS
               ,:LQU-IB-LIQ
                :IND-LQU-IB-LIQ
               ,:LQU-TOT-IAS-ONER-PRVNT
                :IND-LQU-TOT-IAS-ONER-PRVNT
               ,:LQU-TOT-IAS-MGG-SIN
                :IND-LQU-TOT-IAS-MGG-SIN
               ,:LQU-TOT-IAS-RST-DPST
                :IND-LQU-TOT-IAS-RST-DPST
               ,:LQU-IMP-ONER-LIQ
                :IND-LQU-IMP-ONER-LIQ
               ,:LQU-COMPON-TAX-RIMB
                :IND-LQU-COMPON-TAX-RIMB
               ,:LQU-TP-MEZ-PAG
                :IND-LQU-TP-MEZ-PAG
               ,:LQU-IMP-EXCONTR
                :IND-LQU-IMP-EXCONTR
               ,:LQU-IMP-INTR-RIT-PAG
                :IND-LQU-IMP-INTR-RIT-PAG
               ,:LQU-BNS-NON-GODUTO
                :IND-LQU-BNS-NON-GODUTO
               ,:LQU-CNBT-INPSTFM
                :IND-LQU-CNBT-INPSTFM
               ,:LQU-IMPST-DA-RIMB
                :IND-LQU-IMPST-DA-RIMB
               ,:LQU-IMPB-IS
                :IND-LQU-IMPB-IS
               ,:LQU-TAX-SEP
                :IND-LQU-TAX-SEP
               ,:LQU-IMPB-TAX-SEP
                :IND-LQU-IMPB-TAX-SEP
               ,:LQU-IMPB-INTR-SU-PREST
                :IND-LQU-IMPB-INTR-SU-PREST
               ,:LQU-ADDIZ-COMUN
                :IND-LQU-ADDIZ-COMUN
               ,:LQU-IMPB-ADDIZ-COMUN
                :IND-LQU-IMPB-ADDIZ-COMUN
               ,:LQU-ADDIZ-REGION
                :IND-LQU-ADDIZ-REGION
               ,:LQU-IMPB-ADDIZ-REGION
                :IND-LQU-IMPB-ADDIZ-REGION
               ,:LQU-MONT-DAL2007
                :IND-LQU-MONT-DAL2007
               ,:LQU-IMPB-CNBT-INPSTFM
                :IND-LQU-IMPB-CNBT-INPSTFM
               ,:LQU-IMP-LRD-DA-RIMB
                :IND-LQU-IMP-LRD-DA-RIMB
               ,:LQU-IMP-DIR-DA-RIMB
                :IND-LQU-IMP-DIR-DA-RIMB
               ,:LQU-RIS-MAT
                :IND-LQU-RIS-MAT
               ,:LQU-RIS-SPE
                :IND-LQU-RIS-SPE
               ,:LQU-DS-RIGA
               ,:LQU-DS-OPER-SQL
               ,:LQU-DS-VER
               ,:LQU-DS-TS-INI-CPTZ
               ,:LQU-DS-TS-END-CPTZ
               ,:LQU-DS-UTENTE
               ,:LQU-DS-STATO-ELAB
               ,:LQU-TOT-IAS-PNL
                :IND-LQU-TOT-IAS-PNL
               ,:LQU-FL-EVE-GARTO
                :IND-LQU-FL-EVE-GARTO
               ,:LQU-IMP-REN-K1
                :IND-LQU-IMP-REN-K1
               ,:LQU-IMP-REN-K2
                :IND-LQU-IMP-REN-K2
               ,:LQU-IMP-REN-K3
                :IND-LQU-IMP-REN-K3
               ,:LQU-PC-REN-K1
                :IND-LQU-PC-REN-K1
               ,:LQU-PC-REN-K2
                :IND-LQU-PC-REN-K2
               ,:LQU-PC-REN-K3
                :IND-LQU-PC-REN-K3
               ,:LQU-TP-CAUS-ANTIC
                :IND-LQU-TP-CAUS-ANTIC
               ,:LQU-IMP-LRD-LIQTO-RILT
                :IND-LQU-IMP-LRD-LIQTO-RILT
               ,:LQU-IMPST-APPL-RILT
                :IND-LQU-IMPST-APPL-RILT
               ,:LQU-PC-RISC-PARZ
                :IND-LQU-PC-RISC-PARZ
               ,:LQU-IMPST-BOLLO-TOT-V
                :IND-LQU-IMPST-BOLLO-TOT-V
               ,:LQU-IMPST-BOLLO-DETT-C
                :IND-LQU-IMPST-BOLLO-DETT-C
               ,:LQU-IMPST-BOLLO-TOT-SW
                :IND-LQU-IMPST-BOLLO-TOT-SW
               ,:LQU-IMPST-BOLLO-TOT-AA
                :IND-LQU-IMPST-BOLLO-TOT-AA
               ,:LQU-IMPB-VIS-1382011
                :IND-LQU-IMPB-VIS-1382011
               ,:LQU-IMPST-VIS-1382011
                :IND-LQU-IMPST-VIS-1382011
               ,:LQU-IMPB-IS-1382011
                :IND-LQU-IMPB-IS-1382011
               ,:LQU-IMPST-SOST-1382011
                :IND-LQU-IMPST-SOST-1382011
               ,:LQU-PC-ABB-TIT-STAT
                :IND-LQU-PC-ABB-TIT-STAT
               ,:LQU-IMPB-BOLLO-DETT-C
                :IND-LQU-IMPB-BOLLO-DETT-C
               ,:LQU-FL-PRE-COMP
                :IND-LQU-FL-PRE-COMP
               ,:LQU-IMPB-VIS-662014
                :IND-LQU-IMPB-VIS-662014
               ,:LQU-IMPST-VIS-662014
                :IND-LQU-IMPST-VIS-662014
               ,:LQU-IMPB-IS-662014
                :IND-LQU-IMPB-IS-662014
               ,:LQU-IMPST-SOST-662014
                :IND-LQU-IMPST-SOST-662014
               ,:LQU-PC-ABB-TS-662014
                :IND-LQU-PC-ABB-TS-662014
               ,:LQU-IMP-LRD-CALC-CP
                :IND-LQU-IMP-LRD-CALC-CP
               ,:LQU-COS-TUNNEL-USCITA
                :IND-LQU-COS-TUNNEL-USCITA
             FROM LIQ
             WHERE     ID_LIQ = :LQU-ID-LIQ
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE LIQ SET

                   ID_LIQ                 =
                :LQU-ID-LIQ
                  ,ID_OGG                 =
                :LQU-ID-OGG
                  ,TP_OGG                 =
                :LQU-TP-OGG
                  ,ID_MOVI_CRZ            =
                :LQU-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :LQU-ID-MOVI-CHIU
                                       :IND-LQU-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :LQU-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :LQU-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :LQU-COD-COMP-ANIA
                  ,IB_OGG                 =
                :LQU-IB-OGG
                                       :IND-LQU-IB-OGG
                  ,TP_LIQ                 =
                :LQU-TP-LIQ
                  ,DESC_CAU_EVE_SIN       =
                :LQU-DESC-CAU-EVE-SIN-VCHAR
                                       :IND-LQU-DESC-CAU-EVE-SIN
                  ,COD_CAU_SIN            =
                :LQU-COD-CAU-SIN
                                       :IND-LQU-COD-CAU-SIN
                  ,COD_SIN_CATSTRF        =
                :LQU-COD-SIN-CATSTRF
                                       :IND-LQU-COD-SIN-CATSTRF
                  ,DT_MOR                 =
           :LQU-DT-MOR-DB
                                       :IND-LQU-DT-MOR
                  ,DT_DEN                 =
           :LQU-DT-DEN-DB
                                       :IND-LQU-DT-DEN
                  ,DT_PERV_DEN            =
           :LQU-DT-PERV-DEN-DB
                                       :IND-LQU-DT-PERV-DEN
                  ,DT_RICH                =
           :LQU-DT-RICH-DB
                                       :IND-LQU-DT-RICH
                  ,TP_SIN                 =
                :LQU-TP-SIN
                                       :IND-LQU-TP-SIN
                  ,TP_RISC                =
                :LQU-TP-RISC
                                       :IND-LQU-TP-RISC
                  ,TP_MET_RISC            =
                :LQU-TP-MET-RISC
                                       :IND-LQU-TP-MET-RISC
                  ,DT_LIQ                 =
           :LQU-DT-LIQ-DB
                                       :IND-LQU-DT-LIQ
                  ,COD_DVS                =
                :LQU-COD-DVS
                                       :IND-LQU-COD-DVS
                  ,TOT_IMP_LRD_LIQTO      =
                :LQU-TOT-IMP-LRD-LIQTO
                                       :IND-LQU-TOT-IMP-LRD-LIQTO
                  ,TOT_IMP_PREST          =
                :LQU-TOT-IMP-PREST
                                       :IND-LQU-TOT-IMP-PREST
                  ,TOT_IMP_INTR_PREST     =
                :LQU-TOT-IMP-INTR-PREST
                                       :IND-LQU-TOT-IMP-INTR-PREST
                  ,TOT_IMP_UTI            =
                :LQU-TOT-IMP-UTI
                                       :IND-LQU-TOT-IMP-UTI
                  ,TOT_IMP_RIT_TFR        =
                :LQU-TOT-IMP-RIT-TFR
                                       :IND-LQU-TOT-IMP-RIT-TFR
                  ,TOT_IMP_RIT_ACC        =
                :LQU-TOT-IMP-RIT-ACC
                                       :IND-LQU-TOT-IMP-RIT-ACC
                  ,TOT_IMP_RIT_VIS        =
                :LQU-TOT-IMP-RIT-VIS
                                       :IND-LQU-TOT-IMP-RIT-VIS
                  ,TOT_IMPB_TFR           =
                :LQU-TOT-IMPB-TFR
                                       :IND-LQU-TOT-IMPB-TFR
                  ,TOT_IMPB_ACC           =
                :LQU-TOT-IMPB-ACC
                                       :IND-LQU-TOT-IMPB-ACC
                  ,TOT_IMPB_VIS           =
                :LQU-TOT-IMPB-VIS
                                       :IND-LQU-TOT-IMPB-VIS
                  ,TOT_IMP_RIMB           =
                :LQU-TOT-IMP-RIMB
                                       :IND-LQU-TOT-IMP-RIMB
                  ,IMPB_IMPST_PRVR        =
                :LQU-IMPB-IMPST-PRVR
                                       :IND-LQU-IMPB-IMPST-PRVR
                  ,IMPST_PRVR             =
                :LQU-IMPST-PRVR
                                       :IND-LQU-IMPST-PRVR
                  ,IMPB_IMPST_252         =
                :LQU-IMPB-IMPST-252
                                       :IND-LQU-IMPB-IMPST-252
                  ,IMPST_252              =
                :LQU-IMPST-252
                                       :IND-LQU-IMPST-252
                  ,TOT_IMP_IS             =
                :LQU-TOT-IMP-IS
                                       :IND-LQU-TOT-IMP-IS
                  ,IMP_DIR_LIQ            =
                :LQU-IMP-DIR-LIQ
                                       :IND-LQU-IMP-DIR-LIQ
                  ,TOT_IMP_NET_LIQTO      =
                :LQU-TOT-IMP-NET-LIQTO
                                       :IND-LQU-TOT-IMP-NET-LIQTO
                  ,MONT_END2000           =
                :LQU-MONT-END2000
                                       :IND-LQU-MONT-END2000
                  ,MONT_END2006           =
                :LQU-MONT-END2006
                                       :IND-LQU-MONT-END2006
                  ,PC_REN                 =
                :LQU-PC-REN
                                       :IND-LQU-PC-REN
                  ,IMP_PNL                =
                :LQU-IMP-PNL
                                       :IND-LQU-IMP-PNL
                  ,IMPB_IRPEF             =
                :LQU-IMPB-IRPEF
                                       :IND-LQU-IMPB-IRPEF
                  ,IMPST_IRPEF            =
                :LQU-IMPST-IRPEF
                                       :IND-LQU-IMPST-IRPEF
                  ,DT_VLT                 =
           :LQU-DT-VLT-DB
                                       :IND-LQU-DT-VLT
                  ,DT_END_ISTR            =
           :LQU-DT-END-ISTR-DB
                                       :IND-LQU-DT-END-ISTR
                  ,TP_RIMB                =
                :LQU-TP-RIMB
                  ,SPE_RCS                =
                :LQU-SPE-RCS
                                       :IND-LQU-SPE-RCS
                  ,IB_LIQ                 =
                :LQU-IB-LIQ
                                       :IND-LQU-IB-LIQ
                  ,TOT_IAS_ONER_PRVNT     =
                :LQU-TOT-IAS-ONER-PRVNT
                                       :IND-LQU-TOT-IAS-ONER-PRVNT
                  ,TOT_IAS_MGG_SIN        =
                :LQU-TOT-IAS-MGG-SIN
                                       :IND-LQU-TOT-IAS-MGG-SIN
                  ,TOT_IAS_RST_DPST       =
                :LQU-TOT-IAS-RST-DPST
                                       :IND-LQU-TOT-IAS-RST-DPST
                  ,IMP_ONER_LIQ           =
                :LQU-IMP-ONER-LIQ
                                       :IND-LQU-IMP-ONER-LIQ
                  ,COMPON_TAX_RIMB        =
                :LQU-COMPON-TAX-RIMB
                                       :IND-LQU-COMPON-TAX-RIMB
                  ,TP_MEZ_PAG             =
                :LQU-TP-MEZ-PAG
                                       :IND-LQU-TP-MEZ-PAG
                  ,IMP_EXCONTR            =
                :LQU-IMP-EXCONTR
                                       :IND-LQU-IMP-EXCONTR
                  ,IMP_INTR_RIT_PAG       =
                :LQU-IMP-INTR-RIT-PAG
                                       :IND-LQU-IMP-INTR-RIT-PAG
                  ,BNS_NON_GODUTO         =
                :LQU-BNS-NON-GODUTO
                                       :IND-LQU-BNS-NON-GODUTO
                  ,CNBT_INPSTFM           =
                :LQU-CNBT-INPSTFM
                                       :IND-LQU-CNBT-INPSTFM
                  ,IMPST_DA_RIMB          =
                :LQU-IMPST-DA-RIMB
                                       :IND-LQU-IMPST-DA-RIMB
                  ,IMPB_IS                =
                :LQU-IMPB-IS
                                       :IND-LQU-IMPB-IS
                  ,TAX_SEP                =
                :LQU-TAX-SEP
                                       :IND-LQU-TAX-SEP
                  ,IMPB_TAX_SEP           =
                :LQU-IMPB-TAX-SEP
                                       :IND-LQU-IMPB-TAX-SEP
                  ,IMPB_INTR_SU_PREST     =
                :LQU-IMPB-INTR-SU-PREST
                                       :IND-LQU-IMPB-INTR-SU-PREST
                  ,ADDIZ_COMUN            =
                :LQU-ADDIZ-COMUN
                                       :IND-LQU-ADDIZ-COMUN
                  ,IMPB_ADDIZ_COMUN       =
                :LQU-IMPB-ADDIZ-COMUN
                                       :IND-LQU-IMPB-ADDIZ-COMUN
                  ,ADDIZ_REGION           =
                :LQU-ADDIZ-REGION
                                       :IND-LQU-ADDIZ-REGION
                  ,IMPB_ADDIZ_REGION      =
                :LQU-IMPB-ADDIZ-REGION
                                       :IND-LQU-IMPB-ADDIZ-REGION
                  ,MONT_DAL2007           =
                :LQU-MONT-DAL2007
                                       :IND-LQU-MONT-DAL2007
                  ,IMPB_CNBT_INPSTFM      =
                :LQU-IMPB-CNBT-INPSTFM
                                       :IND-LQU-IMPB-CNBT-INPSTFM
                  ,IMP_LRD_DA_RIMB        =
                :LQU-IMP-LRD-DA-RIMB
                                       :IND-LQU-IMP-LRD-DA-RIMB
                  ,IMP_DIR_DA_RIMB        =
                :LQU-IMP-DIR-DA-RIMB
                                       :IND-LQU-IMP-DIR-DA-RIMB
                  ,RIS_MAT                =
                :LQU-RIS-MAT
                                       :IND-LQU-RIS-MAT
                  ,RIS_SPE                =
                :LQU-RIS-SPE
                                       :IND-LQU-RIS-SPE
                  ,DS_RIGA                =
                :LQU-DS-RIGA
                  ,DS_OPER_SQL            =
                :LQU-DS-OPER-SQL
                  ,DS_VER                 =
                :LQU-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :LQU-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :LQU-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :LQU-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :LQU-DS-STATO-ELAB
                  ,TOT_IAS_PNL            =
                :LQU-TOT-IAS-PNL
                                       :IND-LQU-TOT-IAS-PNL
                  ,FL_EVE_GARTO           =
                :LQU-FL-EVE-GARTO
                                       :IND-LQU-FL-EVE-GARTO
                  ,IMP_REN_K1             =
                :LQU-IMP-REN-K1
                                       :IND-LQU-IMP-REN-K1
                  ,IMP_REN_K2             =
                :LQU-IMP-REN-K2
                                       :IND-LQU-IMP-REN-K2
                  ,IMP_REN_K3             =
                :LQU-IMP-REN-K3
                                       :IND-LQU-IMP-REN-K3
                  ,PC_REN_K1              =
                :LQU-PC-REN-K1
                                       :IND-LQU-PC-REN-K1
                  ,PC_REN_K2              =
                :LQU-PC-REN-K2
                                       :IND-LQU-PC-REN-K2
                  ,PC_REN_K3              =
                :LQU-PC-REN-K3
                                       :IND-LQU-PC-REN-K3
                  ,TP_CAUS_ANTIC          =
                :LQU-TP-CAUS-ANTIC
                                       :IND-LQU-TP-CAUS-ANTIC
                  ,IMP_LRD_LIQTO_RILT     =
                :LQU-IMP-LRD-LIQTO-RILT
                                       :IND-LQU-IMP-LRD-LIQTO-RILT
                  ,IMPST_APPL_RILT        =
                :LQU-IMPST-APPL-RILT
                                       :IND-LQU-IMPST-APPL-RILT
                  ,PC_RISC_PARZ           =
                :LQU-PC-RISC-PARZ
                                       :IND-LQU-PC-RISC-PARZ
                  ,IMPST_BOLLO_TOT_V      =
                :LQU-IMPST-BOLLO-TOT-V
                                       :IND-LQU-IMPST-BOLLO-TOT-V
                  ,IMPST_BOLLO_DETT_C     =
                :LQU-IMPST-BOLLO-DETT-C
                                       :IND-LQU-IMPST-BOLLO-DETT-C
                  ,IMPST_BOLLO_TOT_SW     =
                :LQU-IMPST-BOLLO-TOT-SW
                                       :IND-LQU-IMPST-BOLLO-TOT-SW
                  ,IMPST_BOLLO_TOT_AA     =
                :LQU-IMPST-BOLLO-TOT-AA
                                       :IND-LQU-IMPST-BOLLO-TOT-AA
                  ,IMPB_VIS_1382011       =
                :LQU-IMPB-VIS-1382011
                                       :IND-LQU-IMPB-VIS-1382011
                  ,IMPST_VIS_1382011      =
                :LQU-IMPST-VIS-1382011
                                       :IND-LQU-IMPST-VIS-1382011
                  ,IMPB_IS_1382011        =
                :LQU-IMPB-IS-1382011
                                       :IND-LQU-IMPB-IS-1382011
                  ,IMPST_SOST_1382011     =
                :LQU-IMPST-SOST-1382011
                                       :IND-LQU-IMPST-SOST-1382011
                  ,PC_ABB_TIT_STAT        =
                :LQU-PC-ABB-TIT-STAT
                                       :IND-LQU-PC-ABB-TIT-STAT
                  ,IMPB_BOLLO_DETT_C      =
                :LQU-IMPB-BOLLO-DETT-C
                                       :IND-LQU-IMPB-BOLLO-DETT-C
                  ,FL_PRE_COMP            =
                :LQU-FL-PRE-COMP
                                       :IND-LQU-FL-PRE-COMP
                  ,IMPB_VIS_662014        =
                :LQU-IMPB-VIS-662014
                                       :IND-LQU-IMPB-VIS-662014
                  ,IMPST_VIS_662014       =
                :LQU-IMPST-VIS-662014
                                       :IND-LQU-IMPST-VIS-662014
                  ,IMPB_IS_662014         =
                :LQU-IMPB-IS-662014
                                       :IND-LQU-IMPB-IS-662014
                  ,IMPST_SOST_662014      =
                :LQU-IMPST-SOST-662014
                                       :IND-LQU-IMPST-SOST-662014
                  ,PC_ABB_TS_662014       =
                :LQU-PC-ABB-TS-662014
                                       :IND-LQU-PC-ABB-TS-662014
                  ,IMP_LRD_CALC_CP        =
                :LQU-IMP-LRD-CALC-CP
                                       :IND-LQU-IMP-LRD-CALC-CP
                  ,COS_TUNNEL_USCITA      =
                :LQU-COS-TUNNEL-USCITA
                                       :IND-LQU-COS-TUNNEL-USCITA
                WHERE     DS_RIGA = :LQU-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-LQU
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-LQU
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-LQU
           INTO
                :LQU-ID-LIQ
               ,:LQU-ID-OGG
               ,:LQU-TP-OGG
               ,:LQU-ID-MOVI-CRZ
               ,:LQU-ID-MOVI-CHIU
                :IND-LQU-ID-MOVI-CHIU
               ,:LQU-DT-INI-EFF-DB
               ,:LQU-DT-END-EFF-DB
               ,:LQU-COD-COMP-ANIA
               ,:LQU-IB-OGG
                :IND-LQU-IB-OGG
               ,:LQU-TP-LIQ
               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
                :IND-LQU-DESC-CAU-EVE-SIN
               ,:LQU-COD-CAU-SIN
                :IND-LQU-COD-CAU-SIN
               ,:LQU-COD-SIN-CATSTRF
                :IND-LQU-COD-SIN-CATSTRF
               ,:LQU-DT-MOR-DB
                :IND-LQU-DT-MOR
               ,:LQU-DT-DEN-DB
                :IND-LQU-DT-DEN
               ,:LQU-DT-PERV-DEN-DB
                :IND-LQU-DT-PERV-DEN
               ,:LQU-DT-RICH-DB
                :IND-LQU-DT-RICH
               ,:LQU-TP-SIN
                :IND-LQU-TP-SIN
               ,:LQU-TP-RISC
                :IND-LQU-TP-RISC
               ,:LQU-TP-MET-RISC
                :IND-LQU-TP-MET-RISC
               ,:LQU-DT-LIQ-DB
                :IND-LQU-DT-LIQ
               ,:LQU-COD-DVS
                :IND-LQU-COD-DVS
               ,:LQU-TOT-IMP-LRD-LIQTO
                :IND-LQU-TOT-IMP-LRD-LIQTO
               ,:LQU-TOT-IMP-PREST
                :IND-LQU-TOT-IMP-PREST
               ,:LQU-TOT-IMP-INTR-PREST
                :IND-LQU-TOT-IMP-INTR-PREST
               ,:LQU-TOT-IMP-UTI
                :IND-LQU-TOT-IMP-UTI
               ,:LQU-TOT-IMP-RIT-TFR
                :IND-LQU-TOT-IMP-RIT-TFR
               ,:LQU-TOT-IMP-RIT-ACC
                :IND-LQU-TOT-IMP-RIT-ACC
               ,:LQU-TOT-IMP-RIT-VIS
                :IND-LQU-TOT-IMP-RIT-VIS
               ,:LQU-TOT-IMPB-TFR
                :IND-LQU-TOT-IMPB-TFR
               ,:LQU-TOT-IMPB-ACC
                :IND-LQU-TOT-IMPB-ACC
               ,:LQU-TOT-IMPB-VIS
                :IND-LQU-TOT-IMPB-VIS
               ,:LQU-TOT-IMP-RIMB
                :IND-LQU-TOT-IMP-RIMB
               ,:LQU-IMPB-IMPST-PRVR
                :IND-LQU-IMPB-IMPST-PRVR
               ,:LQU-IMPST-PRVR
                :IND-LQU-IMPST-PRVR
               ,:LQU-IMPB-IMPST-252
                :IND-LQU-IMPB-IMPST-252
               ,:LQU-IMPST-252
                :IND-LQU-IMPST-252
               ,:LQU-TOT-IMP-IS
                :IND-LQU-TOT-IMP-IS
               ,:LQU-IMP-DIR-LIQ
                :IND-LQU-IMP-DIR-LIQ
               ,:LQU-TOT-IMP-NET-LIQTO
                :IND-LQU-TOT-IMP-NET-LIQTO
               ,:LQU-MONT-END2000
                :IND-LQU-MONT-END2000
               ,:LQU-MONT-END2006
                :IND-LQU-MONT-END2006
               ,:LQU-PC-REN
                :IND-LQU-PC-REN
               ,:LQU-IMP-PNL
                :IND-LQU-IMP-PNL
               ,:LQU-IMPB-IRPEF
                :IND-LQU-IMPB-IRPEF
               ,:LQU-IMPST-IRPEF
                :IND-LQU-IMPST-IRPEF
               ,:LQU-DT-VLT-DB
                :IND-LQU-DT-VLT
               ,:LQU-DT-END-ISTR-DB
                :IND-LQU-DT-END-ISTR
               ,:LQU-TP-RIMB
               ,:LQU-SPE-RCS
                :IND-LQU-SPE-RCS
               ,:LQU-IB-LIQ
                :IND-LQU-IB-LIQ
               ,:LQU-TOT-IAS-ONER-PRVNT
                :IND-LQU-TOT-IAS-ONER-PRVNT
               ,:LQU-TOT-IAS-MGG-SIN
                :IND-LQU-TOT-IAS-MGG-SIN
               ,:LQU-TOT-IAS-RST-DPST
                :IND-LQU-TOT-IAS-RST-DPST
               ,:LQU-IMP-ONER-LIQ
                :IND-LQU-IMP-ONER-LIQ
               ,:LQU-COMPON-TAX-RIMB
                :IND-LQU-COMPON-TAX-RIMB
               ,:LQU-TP-MEZ-PAG
                :IND-LQU-TP-MEZ-PAG
               ,:LQU-IMP-EXCONTR
                :IND-LQU-IMP-EXCONTR
               ,:LQU-IMP-INTR-RIT-PAG
                :IND-LQU-IMP-INTR-RIT-PAG
               ,:LQU-BNS-NON-GODUTO
                :IND-LQU-BNS-NON-GODUTO
               ,:LQU-CNBT-INPSTFM
                :IND-LQU-CNBT-INPSTFM
               ,:LQU-IMPST-DA-RIMB
                :IND-LQU-IMPST-DA-RIMB
               ,:LQU-IMPB-IS
                :IND-LQU-IMPB-IS
               ,:LQU-TAX-SEP
                :IND-LQU-TAX-SEP
               ,:LQU-IMPB-TAX-SEP
                :IND-LQU-IMPB-TAX-SEP
               ,:LQU-IMPB-INTR-SU-PREST
                :IND-LQU-IMPB-INTR-SU-PREST
               ,:LQU-ADDIZ-COMUN
                :IND-LQU-ADDIZ-COMUN
               ,:LQU-IMPB-ADDIZ-COMUN
                :IND-LQU-IMPB-ADDIZ-COMUN
               ,:LQU-ADDIZ-REGION
                :IND-LQU-ADDIZ-REGION
               ,:LQU-IMPB-ADDIZ-REGION
                :IND-LQU-IMPB-ADDIZ-REGION
               ,:LQU-MONT-DAL2007
                :IND-LQU-MONT-DAL2007
               ,:LQU-IMPB-CNBT-INPSTFM
                :IND-LQU-IMPB-CNBT-INPSTFM
               ,:LQU-IMP-LRD-DA-RIMB
                :IND-LQU-IMP-LRD-DA-RIMB
               ,:LQU-IMP-DIR-DA-RIMB
                :IND-LQU-IMP-DIR-DA-RIMB
               ,:LQU-RIS-MAT
                :IND-LQU-RIS-MAT
               ,:LQU-RIS-SPE
                :IND-LQU-RIS-SPE
               ,:LQU-DS-RIGA
               ,:LQU-DS-OPER-SQL
               ,:LQU-DS-VER
               ,:LQU-DS-TS-INI-CPTZ
               ,:LQU-DS-TS-END-CPTZ
               ,:LQU-DS-UTENTE
               ,:LQU-DS-STATO-ELAB
               ,:LQU-TOT-IAS-PNL
                :IND-LQU-TOT-IAS-PNL
               ,:LQU-FL-EVE-GARTO
                :IND-LQU-FL-EVE-GARTO
               ,:LQU-IMP-REN-K1
                :IND-LQU-IMP-REN-K1
               ,:LQU-IMP-REN-K2
                :IND-LQU-IMP-REN-K2
               ,:LQU-IMP-REN-K3
                :IND-LQU-IMP-REN-K3
               ,:LQU-PC-REN-K1
                :IND-LQU-PC-REN-K1
               ,:LQU-PC-REN-K2
                :IND-LQU-PC-REN-K2
               ,:LQU-PC-REN-K3
                :IND-LQU-PC-REN-K3
               ,:LQU-TP-CAUS-ANTIC
                :IND-LQU-TP-CAUS-ANTIC
               ,:LQU-IMP-LRD-LIQTO-RILT
                :IND-LQU-IMP-LRD-LIQTO-RILT
               ,:LQU-IMPST-APPL-RILT
                :IND-LQU-IMPST-APPL-RILT
               ,:LQU-PC-RISC-PARZ
                :IND-LQU-PC-RISC-PARZ
               ,:LQU-IMPST-BOLLO-TOT-V
                :IND-LQU-IMPST-BOLLO-TOT-V
               ,:LQU-IMPST-BOLLO-DETT-C
                :IND-LQU-IMPST-BOLLO-DETT-C
               ,:LQU-IMPST-BOLLO-TOT-SW
                :IND-LQU-IMPST-BOLLO-TOT-SW
               ,:LQU-IMPST-BOLLO-TOT-AA
                :IND-LQU-IMPST-BOLLO-TOT-AA
               ,:LQU-IMPB-VIS-1382011
                :IND-LQU-IMPB-VIS-1382011
               ,:LQU-IMPST-VIS-1382011
                :IND-LQU-IMPST-VIS-1382011
               ,:LQU-IMPB-IS-1382011
                :IND-LQU-IMPB-IS-1382011
               ,:LQU-IMPST-SOST-1382011
                :IND-LQU-IMPST-SOST-1382011
               ,:LQU-PC-ABB-TIT-STAT
                :IND-LQU-PC-ABB-TIT-STAT
               ,:LQU-IMPB-BOLLO-DETT-C
                :IND-LQU-IMPB-BOLLO-DETT-C
               ,:LQU-FL-PRE-COMP
                :IND-LQU-FL-PRE-COMP
               ,:LQU-IMPB-VIS-662014
                :IND-LQU-IMPB-VIS-662014
               ,:LQU-IMPST-VIS-662014
                :IND-LQU-IMPST-VIS-662014
               ,:LQU-IMPB-IS-662014
                :IND-LQU-IMPB-IS-662014
               ,:LQU-IMPST-SOST-662014
                :IND-LQU-IMPST-SOST-662014
               ,:LQU-PC-ABB-TS-662014
                :IND-LQU-PC-ABB-TS-662014
               ,:LQU-IMP-LRD-CALC-CP
                :IND-LQU-IMP-LRD-CALC-CP
               ,:LQU-COS-TUNNEL-USCITA
                :IND-LQU-COS-TUNNEL-USCITA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IBO-EFF-LQU CURSOR FOR
              SELECT
                     ID_LIQ
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,IB_OGG
                    ,TP_LIQ
                    ,DESC_CAU_EVE_SIN
                    ,COD_CAU_SIN
                    ,COD_SIN_CATSTRF
                    ,DT_MOR
                    ,DT_DEN
                    ,DT_PERV_DEN
                    ,DT_RICH
                    ,TP_SIN
                    ,TP_RISC
                    ,TP_MET_RISC
                    ,DT_LIQ
                    ,COD_DVS
                    ,TOT_IMP_LRD_LIQTO
                    ,TOT_IMP_PREST
                    ,TOT_IMP_INTR_PREST
                    ,TOT_IMP_UTI
                    ,TOT_IMP_RIT_TFR
                    ,TOT_IMP_RIT_ACC
                    ,TOT_IMP_RIT_VIS
                    ,TOT_IMPB_TFR
                    ,TOT_IMPB_ACC
                    ,TOT_IMPB_VIS
                    ,TOT_IMP_RIMB
                    ,IMPB_IMPST_PRVR
                    ,IMPST_PRVR
                    ,IMPB_IMPST_252
                    ,IMPST_252
                    ,TOT_IMP_IS
                    ,IMP_DIR_LIQ
                    ,TOT_IMP_NET_LIQTO
                    ,MONT_END2000
                    ,MONT_END2006
                    ,PC_REN
                    ,IMP_PNL
                    ,IMPB_IRPEF
                    ,IMPST_IRPEF
                    ,DT_VLT
                    ,DT_END_ISTR
                    ,TP_RIMB
                    ,SPE_RCS
                    ,IB_LIQ
                    ,TOT_IAS_ONER_PRVNT
                    ,TOT_IAS_MGG_SIN
                    ,TOT_IAS_RST_DPST
                    ,IMP_ONER_LIQ
                    ,COMPON_TAX_RIMB
                    ,TP_MEZ_PAG
                    ,IMP_EXCONTR
                    ,IMP_INTR_RIT_PAG
                    ,BNS_NON_GODUTO
                    ,CNBT_INPSTFM
                    ,IMPST_DA_RIMB
                    ,IMPB_IS
                    ,TAX_SEP
                    ,IMPB_TAX_SEP
                    ,IMPB_INTR_SU_PREST
                    ,ADDIZ_COMUN
                    ,IMPB_ADDIZ_COMUN
                    ,ADDIZ_REGION
                    ,IMPB_ADDIZ_REGION
                    ,MONT_DAL2007
                    ,IMPB_CNBT_INPSTFM
                    ,IMP_LRD_DA_RIMB
                    ,IMP_DIR_DA_RIMB
                    ,RIS_MAT
                    ,RIS_SPE
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,TOT_IAS_PNL
                    ,FL_EVE_GARTO
                    ,IMP_REN_K1
                    ,IMP_REN_K2
                    ,IMP_REN_K3
                    ,PC_REN_K1
                    ,PC_REN_K2
                    ,PC_REN_K3
                    ,TP_CAUS_ANTIC
                    ,IMP_LRD_LIQTO_RILT
                    ,IMPST_APPL_RILT
                    ,PC_RISC_PARZ
                    ,IMPST_BOLLO_TOT_V
                    ,IMPST_BOLLO_DETT_C
                    ,IMPST_BOLLO_TOT_SW
                    ,IMPST_BOLLO_TOT_AA
                    ,IMPB_VIS_1382011
                    ,IMPST_VIS_1382011
                    ,IMPB_IS_1382011
                    ,IMPST_SOST_1382011
                    ,PC_ABB_TIT_STAT
                    ,IMPB_BOLLO_DETT_C
                    ,FL_PRE_COMP
                    ,IMPB_VIS_662014
                    ,IMPST_VIS_662014
                    ,IMPB_IS_662014
                    ,IMPST_SOST_662014
                    ,PC_ABB_TS_662014
                    ,IMP_LRD_CALC_CP
                    ,COS_TUNNEL_USCITA
              FROM LIQ
              WHERE     IB_OGG = :LQU-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_LIQ ASC

           END-EXEC.

       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_LIQ
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,TP_LIQ
                ,DESC_CAU_EVE_SIN
                ,COD_CAU_SIN
                ,COD_SIN_CATSTRF
                ,DT_MOR
                ,DT_DEN
                ,DT_PERV_DEN
                ,DT_RICH
                ,TP_SIN
                ,TP_RISC
                ,TP_MET_RISC
                ,DT_LIQ
                ,COD_DVS
                ,TOT_IMP_LRD_LIQTO
                ,TOT_IMP_PREST
                ,TOT_IMP_INTR_PREST
                ,TOT_IMP_UTI
                ,TOT_IMP_RIT_TFR
                ,TOT_IMP_RIT_ACC
                ,TOT_IMP_RIT_VIS
                ,TOT_IMPB_TFR
                ,TOT_IMPB_ACC
                ,TOT_IMPB_VIS
                ,TOT_IMP_RIMB
                ,IMPB_IMPST_PRVR
                ,IMPST_PRVR
                ,IMPB_IMPST_252
                ,IMPST_252
                ,TOT_IMP_IS
                ,IMP_DIR_LIQ
                ,TOT_IMP_NET_LIQTO
                ,MONT_END2000
                ,MONT_END2006
                ,PC_REN
                ,IMP_PNL
                ,IMPB_IRPEF
                ,IMPST_IRPEF
                ,DT_VLT
                ,DT_END_ISTR
                ,TP_RIMB
                ,SPE_RCS
                ,IB_LIQ
                ,TOT_IAS_ONER_PRVNT
                ,TOT_IAS_MGG_SIN
                ,TOT_IAS_RST_DPST
                ,IMP_ONER_LIQ
                ,COMPON_TAX_RIMB
                ,TP_MEZ_PAG
                ,IMP_EXCONTR
                ,IMP_INTR_RIT_PAG
                ,BNS_NON_GODUTO
                ,CNBT_INPSTFM
                ,IMPST_DA_RIMB
                ,IMPB_IS
                ,TAX_SEP
                ,IMPB_TAX_SEP
                ,IMPB_INTR_SU_PREST
                ,ADDIZ_COMUN
                ,IMPB_ADDIZ_COMUN
                ,ADDIZ_REGION
                ,IMPB_ADDIZ_REGION
                ,MONT_DAL2007
                ,IMPB_CNBT_INPSTFM
                ,IMP_LRD_DA_RIMB
                ,IMP_DIR_DA_RIMB
                ,RIS_MAT
                ,RIS_SPE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TOT_IAS_PNL
                ,FL_EVE_GARTO
                ,IMP_REN_K1
                ,IMP_REN_K2
                ,IMP_REN_K3
                ,PC_REN_K1
                ,PC_REN_K2
                ,PC_REN_K3
                ,TP_CAUS_ANTIC
                ,IMP_LRD_LIQTO_RILT
                ,IMPST_APPL_RILT
                ,PC_RISC_PARZ
                ,IMPST_BOLLO_TOT_V
                ,IMPST_BOLLO_DETT_C
                ,IMPST_BOLLO_TOT_SW
                ,IMPST_BOLLO_TOT_AA
                ,IMPB_VIS_1382011
                ,IMPST_VIS_1382011
                ,IMPB_IS_1382011
                ,IMPST_SOST_1382011
                ,PC_ABB_TIT_STAT
                ,IMPB_BOLLO_DETT_C
                ,FL_PRE_COMP
                ,IMPB_VIS_662014
                ,IMPST_VIS_662014
                ,IMPB_IS_662014
                ,IMPST_SOST_662014
                ,PC_ABB_TS_662014
                ,IMP_LRD_CALC_CP
                ,COS_TUNNEL_USCITA
             INTO
                :LQU-ID-LIQ
               ,:LQU-ID-OGG
               ,:LQU-TP-OGG
               ,:LQU-ID-MOVI-CRZ
               ,:LQU-ID-MOVI-CHIU
                :IND-LQU-ID-MOVI-CHIU
               ,:LQU-DT-INI-EFF-DB
               ,:LQU-DT-END-EFF-DB
               ,:LQU-COD-COMP-ANIA
               ,:LQU-IB-OGG
                :IND-LQU-IB-OGG
               ,:LQU-TP-LIQ
               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
                :IND-LQU-DESC-CAU-EVE-SIN
               ,:LQU-COD-CAU-SIN
                :IND-LQU-COD-CAU-SIN
               ,:LQU-COD-SIN-CATSTRF
                :IND-LQU-COD-SIN-CATSTRF
               ,:LQU-DT-MOR-DB
                :IND-LQU-DT-MOR
               ,:LQU-DT-DEN-DB
                :IND-LQU-DT-DEN
               ,:LQU-DT-PERV-DEN-DB
                :IND-LQU-DT-PERV-DEN
               ,:LQU-DT-RICH-DB
                :IND-LQU-DT-RICH
               ,:LQU-TP-SIN
                :IND-LQU-TP-SIN
               ,:LQU-TP-RISC
                :IND-LQU-TP-RISC
               ,:LQU-TP-MET-RISC
                :IND-LQU-TP-MET-RISC
               ,:LQU-DT-LIQ-DB
                :IND-LQU-DT-LIQ
               ,:LQU-COD-DVS
                :IND-LQU-COD-DVS
               ,:LQU-TOT-IMP-LRD-LIQTO
                :IND-LQU-TOT-IMP-LRD-LIQTO
               ,:LQU-TOT-IMP-PREST
                :IND-LQU-TOT-IMP-PREST
               ,:LQU-TOT-IMP-INTR-PREST
                :IND-LQU-TOT-IMP-INTR-PREST
               ,:LQU-TOT-IMP-UTI
                :IND-LQU-TOT-IMP-UTI
               ,:LQU-TOT-IMP-RIT-TFR
                :IND-LQU-TOT-IMP-RIT-TFR
               ,:LQU-TOT-IMP-RIT-ACC
                :IND-LQU-TOT-IMP-RIT-ACC
               ,:LQU-TOT-IMP-RIT-VIS
                :IND-LQU-TOT-IMP-RIT-VIS
               ,:LQU-TOT-IMPB-TFR
                :IND-LQU-TOT-IMPB-TFR
               ,:LQU-TOT-IMPB-ACC
                :IND-LQU-TOT-IMPB-ACC
               ,:LQU-TOT-IMPB-VIS
                :IND-LQU-TOT-IMPB-VIS
               ,:LQU-TOT-IMP-RIMB
                :IND-LQU-TOT-IMP-RIMB
               ,:LQU-IMPB-IMPST-PRVR
                :IND-LQU-IMPB-IMPST-PRVR
               ,:LQU-IMPST-PRVR
                :IND-LQU-IMPST-PRVR
               ,:LQU-IMPB-IMPST-252
                :IND-LQU-IMPB-IMPST-252
               ,:LQU-IMPST-252
                :IND-LQU-IMPST-252
               ,:LQU-TOT-IMP-IS
                :IND-LQU-TOT-IMP-IS
               ,:LQU-IMP-DIR-LIQ
                :IND-LQU-IMP-DIR-LIQ
               ,:LQU-TOT-IMP-NET-LIQTO
                :IND-LQU-TOT-IMP-NET-LIQTO
               ,:LQU-MONT-END2000
                :IND-LQU-MONT-END2000
               ,:LQU-MONT-END2006
                :IND-LQU-MONT-END2006
               ,:LQU-PC-REN
                :IND-LQU-PC-REN
               ,:LQU-IMP-PNL
                :IND-LQU-IMP-PNL
               ,:LQU-IMPB-IRPEF
                :IND-LQU-IMPB-IRPEF
               ,:LQU-IMPST-IRPEF
                :IND-LQU-IMPST-IRPEF
               ,:LQU-DT-VLT-DB
                :IND-LQU-DT-VLT
               ,:LQU-DT-END-ISTR-DB
                :IND-LQU-DT-END-ISTR
               ,:LQU-TP-RIMB
               ,:LQU-SPE-RCS
                :IND-LQU-SPE-RCS
               ,:LQU-IB-LIQ
                :IND-LQU-IB-LIQ
               ,:LQU-TOT-IAS-ONER-PRVNT
                :IND-LQU-TOT-IAS-ONER-PRVNT
               ,:LQU-TOT-IAS-MGG-SIN
                :IND-LQU-TOT-IAS-MGG-SIN
               ,:LQU-TOT-IAS-RST-DPST
                :IND-LQU-TOT-IAS-RST-DPST
               ,:LQU-IMP-ONER-LIQ
                :IND-LQU-IMP-ONER-LIQ
               ,:LQU-COMPON-TAX-RIMB
                :IND-LQU-COMPON-TAX-RIMB
               ,:LQU-TP-MEZ-PAG
                :IND-LQU-TP-MEZ-PAG
               ,:LQU-IMP-EXCONTR
                :IND-LQU-IMP-EXCONTR
               ,:LQU-IMP-INTR-RIT-PAG
                :IND-LQU-IMP-INTR-RIT-PAG
               ,:LQU-BNS-NON-GODUTO
                :IND-LQU-BNS-NON-GODUTO
               ,:LQU-CNBT-INPSTFM
                :IND-LQU-CNBT-INPSTFM
               ,:LQU-IMPST-DA-RIMB
                :IND-LQU-IMPST-DA-RIMB
               ,:LQU-IMPB-IS
                :IND-LQU-IMPB-IS
               ,:LQU-TAX-SEP
                :IND-LQU-TAX-SEP
               ,:LQU-IMPB-TAX-SEP
                :IND-LQU-IMPB-TAX-SEP
               ,:LQU-IMPB-INTR-SU-PREST
                :IND-LQU-IMPB-INTR-SU-PREST
               ,:LQU-ADDIZ-COMUN
                :IND-LQU-ADDIZ-COMUN
               ,:LQU-IMPB-ADDIZ-COMUN
                :IND-LQU-IMPB-ADDIZ-COMUN
               ,:LQU-ADDIZ-REGION
                :IND-LQU-ADDIZ-REGION
               ,:LQU-IMPB-ADDIZ-REGION
                :IND-LQU-IMPB-ADDIZ-REGION
               ,:LQU-MONT-DAL2007
                :IND-LQU-MONT-DAL2007
               ,:LQU-IMPB-CNBT-INPSTFM
                :IND-LQU-IMPB-CNBT-INPSTFM
               ,:LQU-IMP-LRD-DA-RIMB
                :IND-LQU-IMP-LRD-DA-RIMB
               ,:LQU-IMP-DIR-DA-RIMB
                :IND-LQU-IMP-DIR-DA-RIMB
               ,:LQU-RIS-MAT
                :IND-LQU-RIS-MAT
               ,:LQU-RIS-SPE
                :IND-LQU-RIS-SPE
               ,:LQU-DS-RIGA
               ,:LQU-DS-OPER-SQL
               ,:LQU-DS-VER
               ,:LQU-DS-TS-INI-CPTZ
               ,:LQU-DS-TS-END-CPTZ
               ,:LQU-DS-UTENTE
               ,:LQU-DS-STATO-ELAB
               ,:LQU-TOT-IAS-PNL
                :IND-LQU-TOT-IAS-PNL
               ,:LQU-FL-EVE-GARTO
                :IND-LQU-FL-EVE-GARTO
               ,:LQU-IMP-REN-K1
                :IND-LQU-IMP-REN-K1
               ,:LQU-IMP-REN-K2
                :IND-LQU-IMP-REN-K2
               ,:LQU-IMP-REN-K3
                :IND-LQU-IMP-REN-K3
               ,:LQU-PC-REN-K1
                :IND-LQU-PC-REN-K1
               ,:LQU-PC-REN-K2
                :IND-LQU-PC-REN-K2
               ,:LQU-PC-REN-K3
                :IND-LQU-PC-REN-K3
               ,:LQU-TP-CAUS-ANTIC
                :IND-LQU-TP-CAUS-ANTIC
               ,:LQU-IMP-LRD-LIQTO-RILT
                :IND-LQU-IMP-LRD-LIQTO-RILT
               ,:LQU-IMPST-APPL-RILT
                :IND-LQU-IMPST-APPL-RILT
               ,:LQU-PC-RISC-PARZ
                :IND-LQU-PC-RISC-PARZ
               ,:LQU-IMPST-BOLLO-TOT-V
                :IND-LQU-IMPST-BOLLO-TOT-V
               ,:LQU-IMPST-BOLLO-DETT-C
                :IND-LQU-IMPST-BOLLO-DETT-C
               ,:LQU-IMPST-BOLLO-TOT-SW
                :IND-LQU-IMPST-BOLLO-TOT-SW
               ,:LQU-IMPST-BOLLO-TOT-AA
                :IND-LQU-IMPST-BOLLO-TOT-AA
               ,:LQU-IMPB-VIS-1382011
                :IND-LQU-IMPB-VIS-1382011
               ,:LQU-IMPST-VIS-1382011
                :IND-LQU-IMPST-VIS-1382011
               ,:LQU-IMPB-IS-1382011
                :IND-LQU-IMPB-IS-1382011
               ,:LQU-IMPST-SOST-1382011
                :IND-LQU-IMPST-SOST-1382011
               ,:LQU-PC-ABB-TIT-STAT
                :IND-LQU-PC-ABB-TIT-STAT
               ,:LQU-IMPB-BOLLO-DETT-C
                :IND-LQU-IMPB-BOLLO-DETT-C
               ,:LQU-FL-PRE-COMP
                :IND-LQU-FL-PRE-COMP
               ,:LQU-IMPB-VIS-662014
                :IND-LQU-IMPB-VIS-662014
               ,:LQU-IMPST-VIS-662014
                :IND-LQU-IMPST-VIS-662014
               ,:LQU-IMPB-IS-662014
                :IND-LQU-IMPB-IS-662014
               ,:LQU-IMPST-SOST-662014
                :IND-LQU-IMPST-SOST-662014
               ,:LQU-PC-ABB-TS-662014
                :IND-LQU-PC-ABB-TS-662014
               ,:LQU-IMP-LRD-CALC-CP
                :IND-LQU-IMP-LRD-CALC-CP
               ,:LQU-COS-TUNNEL-USCITA
                :IND-LQU-COS-TUNNEL-USCITA
             FROM LIQ
             WHERE     IB_OGG = :LQU-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           EXEC SQL
                OPEN C-IBO-EFF-LQU
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           EXEC SQL
                CLOSE C-IBO-EFF-LQU
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           EXEC SQL
                FETCH C-IBO-EFF-LQU
           INTO
                :LQU-ID-LIQ
               ,:LQU-ID-OGG
               ,:LQU-TP-OGG
               ,:LQU-ID-MOVI-CRZ
               ,:LQU-ID-MOVI-CHIU
                :IND-LQU-ID-MOVI-CHIU
               ,:LQU-DT-INI-EFF-DB
               ,:LQU-DT-END-EFF-DB
               ,:LQU-COD-COMP-ANIA
               ,:LQU-IB-OGG
                :IND-LQU-IB-OGG
               ,:LQU-TP-LIQ
               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
                :IND-LQU-DESC-CAU-EVE-SIN
               ,:LQU-COD-CAU-SIN
                :IND-LQU-COD-CAU-SIN
               ,:LQU-COD-SIN-CATSTRF
                :IND-LQU-COD-SIN-CATSTRF
               ,:LQU-DT-MOR-DB
                :IND-LQU-DT-MOR
               ,:LQU-DT-DEN-DB
                :IND-LQU-DT-DEN
               ,:LQU-DT-PERV-DEN-DB
                :IND-LQU-DT-PERV-DEN
               ,:LQU-DT-RICH-DB
                :IND-LQU-DT-RICH
               ,:LQU-TP-SIN
                :IND-LQU-TP-SIN
               ,:LQU-TP-RISC
                :IND-LQU-TP-RISC
               ,:LQU-TP-MET-RISC
                :IND-LQU-TP-MET-RISC
               ,:LQU-DT-LIQ-DB
                :IND-LQU-DT-LIQ
               ,:LQU-COD-DVS
                :IND-LQU-COD-DVS
               ,:LQU-TOT-IMP-LRD-LIQTO
                :IND-LQU-TOT-IMP-LRD-LIQTO
               ,:LQU-TOT-IMP-PREST
                :IND-LQU-TOT-IMP-PREST
               ,:LQU-TOT-IMP-INTR-PREST
                :IND-LQU-TOT-IMP-INTR-PREST
               ,:LQU-TOT-IMP-UTI
                :IND-LQU-TOT-IMP-UTI
               ,:LQU-TOT-IMP-RIT-TFR
                :IND-LQU-TOT-IMP-RIT-TFR
               ,:LQU-TOT-IMP-RIT-ACC
                :IND-LQU-TOT-IMP-RIT-ACC
               ,:LQU-TOT-IMP-RIT-VIS
                :IND-LQU-TOT-IMP-RIT-VIS
               ,:LQU-TOT-IMPB-TFR
                :IND-LQU-TOT-IMPB-TFR
               ,:LQU-TOT-IMPB-ACC
                :IND-LQU-TOT-IMPB-ACC
               ,:LQU-TOT-IMPB-VIS
                :IND-LQU-TOT-IMPB-VIS
               ,:LQU-TOT-IMP-RIMB
                :IND-LQU-TOT-IMP-RIMB
               ,:LQU-IMPB-IMPST-PRVR
                :IND-LQU-IMPB-IMPST-PRVR
               ,:LQU-IMPST-PRVR
                :IND-LQU-IMPST-PRVR
               ,:LQU-IMPB-IMPST-252
                :IND-LQU-IMPB-IMPST-252
               ,:LQU-IMPST-252
                :IND-LQU-IMPST-252
               ,:LQU-TOT-IMP-IS
                :IND-LQU-TOT-IMP-IS
               ,:LQU-IMP-DIR-LIQ
                :IND-LQU-IMP-DIR-LIQ
               ,:LQU-TOT-IMP-NET-LIQTO
                :IND-LQU-TOT-IMP-NET-LIQTO
               ,:LQU-MONT-END2000
                :IND-LQU-MONT-END2000
               ,:LQU-MONT-END2006
                :IND-LQU-MONT-END2006
               ,:LQU-PC-REN
                :IND-LQU-PC-REN
               ,:LQU-IMP-PNL
                :IND-LQU-IMP-PNL
               ,:LQU-IMPB-IRPEF
                :IND-LQU-IMPB-IRPEF
               ,:LQU-IMPST-IRPEF
                :IND-LQU-IMPST-IRPEF
               ,:LQU-DT-VLT-DB
                :IND-LQU-DT-VLT
               ,:LQU-DT-END-ISTR-DB
                :IND-LQU-DT-END-ISTR
               ,:LQU-TP-RIMB
               ,:LQU-SPE-RCS
                :IND-LQU-SPE-RCS
               ,:LQU-IB-LIQ
                :IND-LQU-IB-LIQ
               ,:LQU-TOT-IAS-ONER-PRVNT
                :IND-LQU-TOT-IAS-ONER-PRVNT
               ,:LQU-TOT-IAS-MGG-SIN
                :IND-LQU-TOT-IAS-MGG-SIN
               ,:LQU-TOT-IAS-RST-DPST
                :IND-LQU-TOT-IAS-RST-DPST
               ,:LQU-IMP-ONER-LIQ
                :IND-LQU-IMP-ONER-LIQ
               ,:LQU-COMPON-TAX-RIMB
                :IND-LQU-COMPON-TAX-RIMB
               ,:LQU-TP-MEZ-PAG
                :IND-LQU-TP-MEZ-PAG
               ,:LQU-IMP-EXCONTR
                :IND-LQU-IMP-EXCONTR
               ,:LQU-IMP-INTR-RIT-PAG
                :IND-LQU-IMP-INTR-RIT-PAG
               ,:LQU-BNS-NON-GODUTO
                :IND-LQU-BNS-NON-GODUTO
               ,:LQU-CNBT-INPSTFM
                :IND-LQU-CNBT-INPSTFM
               ,:LQU-IMPST-DA-RIMB
                :IND-LQU-IMPST-DA-RIMB
               ,:LQU-IMPB-IS
                :IND-LQU-IMPB-IS
               ,:LQU-TAX-SEP
                :IND-LQU-TAX-SEP
               ,:LQU-IMPB-TAX-SEP
                :IND-LQU-IMPB-TAX-SEP
               ,:LQU-IMPB-INTR-SU-PREST
                :IND-LQU-IMPB-INTR-SU-PREST
               ,:LQU-ADDIZ-COMUN
                :IND-LQU-ADDIZ-COMUN
               ,:LQU-IMPB-ADDIZ-COMUN
                :IND-LQU-IMPB-ADDIZ-COMUN
               ,:LQU-ADDIZ-REGION
                :IND-LQU-ADDIZ-REGION
               ,:LQU-IMPB-ADDIZ-REGION
                :IND-LQU-IMPB-ADDIZ-REGION
               ,:LQU-MONT-DAL2007
                :IND-LQU-MONT-DAL2007
               ,:LQU-IMPB-CNBT-INPSTFM
                :IND-LQU-IMPB-CNBT-INPSTFM
               ,:LQU-IMP-LRD-DA-RIMB
                :IND-LQU-IMP-LRD-DA-RIMB
               ,:LQU-IMP-DIR-DA-RIMB
                :IND-LQU-IMP-DIR-DA-RIMB
               ,:LQU-RIS-MAT
                :IND-LQU-RIS-MAT
               ,:LQU-RIS-SPE
                :IND-LQU-RIS-SPE
               ,:LQU-DS-RIGA
               ,:LQU-DS-OPER-SQL
               ,:LQU-DS-VER
               ,:LQU-DS-TS-INI-CPTZ
               ,:LQU-DS-TS-END-CPTZ
               ,:LQU-DS-UTENTE
               ,:LQU-DS-STATO-ELAB
               ,:LQU-TOT-IAS-PNL
                :IND-LQU-TOT-IAS-PNL
               ,:LQU-FL-EVE-GARTO
                :IND-LQU-FL-EVE-GARTO
               ,:LQU-IMP-REN-K1
                :IND-LQU-IMP-REN-K1
               ,:LQU-IMP-REN-K2
                :IND-LQU-IMP-REN-K2
               ,:LQU-IMP-REN-K3
                :IND-LQU-IMP-REN-K3
               ,:LQU-PC-REN-K1
                :IND-LQU-PC-REN-K1
               ,:LQU-PC-REN-K2
                :IND-LQU-PC-REN-K2
               ,:LQU-PC-REN-K3
                :IND-LQU-PC-REN-K3
               ,:LQU-TP-CAUS-ANTIC
                :IND-LQU-TP-CAUS-ANTIC
               ,:LQU-IMP-LRD-LIQTO-RILT
                :IND-LQU-IMP-LRD-LIQTO-RILT
               ,:LQU-IMPST-APPL-RILT
                :IND-LQU-IMPST-APPL-RILT
               ,:LQU-PC-RISC-PARZ
                :IND-LQU-PC-RISC-PARZ
               ,:LQU-IMPST-BOLLO-TOT-V
                :IND-LQU-IMPST-BOLLO-TOT-V
               ,:LQU-IMPST-BOLLO-DETT-C
                :IND-LQU-IMPST-BOLLO-DETT-C
               ,:LQU-IMPST-BOLLO-TOT-SW
                :IND-LQU-IMPST-BOLLO-TOT-SW
               ,:LQU-IMPST-BOLLO-TOT-AA
                :IND-LQU-IMPST-BOLLO-TOT-AA
               ,:LQU-IMPB-VIS-1382011
                :IND-LQU-IMPB-VIS-1382011
               ,:LQU-IMPST-VIS-1382011
                :IND-LQU-IMPST-VIS-1382011
               ,:LQU-IMPB-IS-1382011
                :IND-LQU-IMPB-IS-1382011
               ,:LQU-IMPST-SOST-1382011
                :IND-LQU-IMPST-SOST-1382011
               ,:LQU-PC-ABB-TIT-STAT
                :IND-LQU-PC-ABB-TIT-STAT
               ,:LQU-IMPB-BOLLO-DETT-C
                :IND-LQU-IMPB-BOLLO-DETT-C
               ,:LQU-FL-PRE-COMP
                :IND-LQU-FL-PRE-COMP
               ,:LQU-IMPB-VIS-662014
                :IND-LQU-IMPB-VIS-662014
               ,:LQU-IMPST-VIS-662014
                :IND-LQU-IMPST-VIS-662014
               ,:LQU-IMPB-IS-662014
                :IND-LQU-IMPB-IS-662014
               ,:LQU-IMPST-SOST-662014
                :IND-LQU-IMPST-SOST-662014
               ,:LQU-PC-ABB-TS-662014
                :IND-LQU-PC-ABB-TS-662014
               ,:LQU-IMP-LRD-CALC-CP
                :IND-LQU-IMP-LRD-CALC-CP
               ,:LQU-COS-TUNNEL-USCITA
                :IND-LQU-COS-TUNNEL-USCITA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A570-CLOSE-CURSOR-IBO     THRU A570-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DCL-CUR-IBS-LIQ.
           EXEC SQL
                DECLARE C-IBS-EFF-LQU-0 CURSOR FOR
              SELECT
                     ID_LIQ
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,IB_OGG
                    ,TP_LIQ
                    ,DESC_CAU_EVE_SIN
                    ,COD_CAU_SIN
                    ,COD_SIN_CATSTRF
                    ,DT_MOR
                    ,DT_DEN
                    ,DT_PERV_DEN
                    ,DT_RICH
                    ,TP_SIN
                    ,TP_RISC
                    ,TP_MET_RISC
                    ,DT_LIQ
                    ,COD_DVS
                    ,TOT_IMP_LRD_LIQTO
                    ,TOT_IMP_PREST
                    ,TOT_IMP_INTR_PREST
                    ,TOT_IMP_UTI
                    ,TOT_IMP_RIT_TFR
                    ,TOT_IMP_RIT_ACC
                    ,TOT_IMP_RIT_VIS
                    ,TOT_IMPB_TFR
                    ,TOT_IMPB_ACC
                    ,TOT_IMPB_VIS
                    ,TOT_IMP_RIMB
                    ,IMPB_IMPST_PRVR
                    ,IMPST_PRVR
                    ,IMPB_IMPST_252
                    ,IMPST_252
                    ,TOT_IMP_IS
                    ,IMP_DIR_LIQ
                    ,TOT_IMP_NET_LIQTO
                    ,MONT_END2000
                    ,MONT_END2006
                    ,PC_REN
                    ,IMP_PNL
                    ,IMPB_IRPEF
                    ,IMPST_IRPEF
                    ,DT_VLT
                    ,DT_END_ISTR
                    ,TP_RIMB
                    ,SPE_RCS
                    ,IB_LIQ
                    ,TOT_IAS_ONER_PRVNT
                    ,TOT_IAS_MGG_SIN
                    ,TOT_IAS_RST_DPST
                    ,IMP_ONER_LIQ
                    ,COMPON_TAX_RIMB
                    ,TP_MEZ_PAG
                    ,IMP_EXCONTR
                    ,IMP_INTR_RIT_PAG
                    ,BNS_NON_GODUTO
                    ,CNBT_INPSTFM
                    ,IMPST_DA_RIMB
                    ,IMPB_IS
                    ,TAX_SEP
                    ,IMPB_TAX_SEP
                    ,IMPB_INTR_SU_PREST
                    ,ADDIZ_COMUN
                    ,IMPB_ADDIZ_COMUN
                    ,ADDIZ_REGION
                    ,IMPB_ADDIZ_REGION
                    ,MONT_DAL2007
                    ,IMPB_CNBT_INPSTFM
                    ,IMP_LRD_DA_RIMB
                    ,IMP_DIR_DA_RIMB
                    ,RIS_MAT
                    ,RIS_SPE
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,TOT_IAS_PNL
                    ,FL_EVE_GARTO
                    ,IMP_REN_K1
                    ,IMP_REN_K2
                    ,IMP_REN_K3
                    ,PC_REN_K1
                    ,PC_REN_K2
                    ,PC_REN_K3
                    ,TP_CAUS_ANTIC
                    ,IMP_LRD_LIQTO_RILT
                    ,IMPST_APPL_RILT
                    ,PC_RISC_PARZ
                    ,IMPST_BOLLO_TOT_V
                    ,IMPST_BOLLO_DETT_C
                    ,IMPST_BOLLO_TOT_SW
                    ,IMPST_BOLLO_TOT_AA
                    ,IMPB_VIS_1382011
                    ,IMPST_VIS_1382011
                    ,IMPB_IS_1382011
                    ,IMPST_SOST_1382011
                    ,PC_ABB_TIT_STAT
                    ,IMPB_BOLLO_DETT_C
                    ,FL_PRE_COMP
                    ,IMPB_VIS_662014
                    ,IMPST_VIS_662014
                    ,IMPB_IS_662014
                    ,IMPST_SOST_662014
                    ,PC_ABB_TS_662014
                    ,IMP_LRD_CALC_CP
                    ,COS_TUNNEL_USCITA
              FROM LIQ
              WHERE     IB_LIQ = :LQU-IB-LIQ
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_LIQ ASC

           END-EXEC.
       A605-LIQ-EX.
           EXIT.


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF LQU-IB-LIQ NOT = HIGH-VALUES
               PERFORM A605-DCL-CUR-IBS-LIQ
                  THRU A605-LIQ-EX
           END-IF.

       A605-EX.
           EXIT.


       A610-SELECT-IBS-LIQ.
           EXEC SQL
             SELECT
                ID_LIQ
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,TP_LIQ
                ,DESC_CAU_EVE_SIN
                ,COD_CAU_SIN
                ,COD_SIN_CATSTRF
                ,DT_MOR
                ,DT_DEN
                ,DT_PERV_DEN
                ,DT_RICH
                ,TP_SIN
                ,TP_RISC
                ,TP_MET_RISC
                ,DT_LIQ
                ,COD_DVS
                ,TOT_IMP_LRD_LIQTO
                ,TOT_IMP_PREST
                ,TOT_IMP_INTR_PREST
                ,TOT_IMP_UTI
                ,TOT_IMP_RIT_TFR
                ,TOT_IMP_RIT_ACC
                ,TOT_IMP_RIT_VIS
                ,TOT_IMPB_TFR
                ,TOT_IMPB_ACC
                ,TOT_IMPB_VIS
                ,TOT_IMP_RIMB
                ,IMPB_IMPST_PRVR
                ,IMPST_PRVR
                ,IMPB_IMPST_252
                ,IMPST_252
                ,TOT_IMP_IS
                ,IMP_DIR_LIQ
                ,TOT_IMP_NET_LIQTO
                ,MONT_END2000
                ,MONT_END2006
                ,PC_REN
                ,IMP_PNL
                ,IMPB_IRPEF
                ,IMPST_IRPEF
                ,DT_VLT
                ,DT_END_ISTR
                ,TP_RIMB
                ,SPE_RCS
                ,IB_LIQ
                ,TOT_IAS_ONER_PRVNT
                ,TOT_IAS_MGG_SIN
                ,TOT_IAS_RST_DPST
                ,IMP_ONER_LIQ
                ,COMPON_TAX_RIMB
                ,TP_MEZ_PAG
                ,IMP_EXCONTR
                ,IMP_INTR_RIT_PAG
                ,BNS_NON_GODUTO
                ,CNBT_INPSTFM
                ,IMPST_DA_RIMB
                ,IMPB_IS
                ,TAX_SEP
                ,IMPB_TAX_SEP
                ,IMPB_INTR_SU_PREST
                ,ADDIZ_COMUN
                ,IMPB_ADDIZ_COMUN
                ,ADDIZ_REGION
                ,IMPB_ADDIZ_REGION
                ,MONT_DAL2007
                ,IMPB_CNBT_INPSTFM
                ,IMP_LRD_DA_RIMB
                ,IMP_DIR_DA_RIMB
                ,RIS_MAT
                ,RIS_SPE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TOT_IAS_PNL
                ,FL_EVE_GARTO
                ,IMP_REN_K1
                ,IMP_REN_K2
                ,IMP_REN_K3
                ,PC_REN_K1
                ,PC_REN_K2
                ,PC_REN_K3
                ,TP_CAUS_ANTIC
                ,IMP_LRD_LIQTO_RILT
                ,IMPST_APPL_RILT
                ,PC_RISC_PARZ
                ,IMPST_BOLLO_TOT_V
                ,IMPST_BOLLO_DETT_C
                ,IMPST_BOLLO_TOT_SW
                ,IMPST_BOLLO_TOT_AA
                ,IMPB_VIS_1382011
                ,IMPST_VIS_1382011
                ,IMPB_IS_1382011
                ,IMPST_SOST_1382011
                ,PC_ABB_TIT_STAT
                ,IMPB_BOLLO_DETT_C
                ,FL_PRE_COMP
                ,IMPB_VIS_662014
                ,IMPST_VIS_662014
                ,IMPB_IS_662014
                ,IMPST_SOST_662014
                ,PC_ABB_TS_662014
                ,IMP_LRD_CALC_CP
                ,COS_TUNNEL_USCITA
             INTO
                :LQU-ID-LIQ
               ,:LQU-ID-OGG
               ,:LQU-TP-OGG
               ,:LQU-ID-MOVI-CRZ
               ,:LQU-ID-MOVI-CHIU
                :IND-LQU-ID-MOVI-CHIU
               ,:LQU-DT-INI-EFF-DB
               ,:LQU-DT-END-EFF-DB
               ,:LQU-COD-COMP-ANIA
               ,:LQU-IB-OGG
                :IND-LQU-IB-OGG
               ,:LQU-TP-LIQ
               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
                :IND-LQU-DESC-CAU-EVE-SIN
               ,:LQU-COD-CAU-SIN
                :IND-LQU-COD-CAU-SIN
               ,:LQU-COD-SIN-CATSTRF
                :IND-LQU-COD-SIN-CATSTRF
               ,:LQU-DT-MOR-DB
                :IND-LQU-DT-MOR
               ,:LQU-DT-DEN-DB
                :IND-LQU-DT-DEN
               ,:LQU-DT-PERV-DEN-DB
                :IND-LQU-DT-PERV-DEN
               ,:LQU-DT-RICH-DB
                :IND-LQU-DT-RICH
               ,:LQU-TP-SIN
                :IND-LQU-TP-SIN
               ,:LQU-TP-RISC
                :IND-LQU-TP-RISC
               ,:LQU-TP-MET-RISC
                :IND-LQU-TP-MET-RISC
               ,:LQU-DT-LIQ-DB
                :IND-LQU-DT-LIQ
               ,:LQU-COD-DVS
                :IND-LQU-COD-DVS
               ,:LQU-TOT-IMP-LRD-LIQTO
                :IND-LQU-TOT-IMP-LRD-LIQTO
               ,:LQU-TOT-IMP-PREST
                :IND-LQU-TOT-IMP-PREST
               ,:LQU-TOT-IMP-INTR-PREST
                :IND-LQU-TOT-IMP-INTR-PREST
               ,:LQU-TOT-IMP-UTI
                :IND-LQU-TOT-IMP-UTI
               ,:LQU-TOT-IMP-RIT-TFR
                :IND-LQU-TOT-IMP-RIT-TFR
               ,:LQU-TOT-IMP-RIT-ACC
                :IND-LQU-TOT-IMP-RIT-ACC
               ,:LQU-TOT-IMP-RIT-VIS
                :IND-LQU-TOT-IMP-RIT-VIS
               ,:LQU-TOT-IMPB-TFR
                :IND-LQU-TOT-IMPB-TFR
               ,:LQU-TOT-IMPB-ACC
                :IND-LQU-TOT-IMPB-ACC
               ,:LQU-TOT-IMPB-VIS
                :IND-LQU-TOT-IMPB-VIS
               ,:LQU-TOT-IMP-RIMB
                :IND-LQU-TOT-IMP-RIMB
               ,:LQU-IMPB-IMPST-PRVR
                :IND-LQU-IMPB-IMPST-PRVR
               ,:LQU-IMPST-PRVR
                :IND-LQU-IMPST-PRVR
               ,:LQU-IMPB-IMPST-252
                :IND-LQU-IMPB-IMPST-252
               ,:LQU-IMPST-252
                :IND-LQU-IMPST-252
               ,:LQU-TOT-IMP-IS
                :IND-LQU-TOT-IMP-IS
               ,:LQU-IMP-DIR-LIQ
                :IND-LQU-IMP-DIR-LIQ
               ,:LQU-TOT-IMP-NET-LIQTO
                :IND-LQU-TOT-IMP-NET-LIQTO
               ,:LQU-MONT-END2000
                :IND-LQU-MONT-END2000
               ,:LQU-MONT-END2006
                :IND-LQU-MONT-END2006
               ,:LQU-PC-REN
                :IND-LQU-PC-REN
               ,:LQU-IMP-PNL
                :IND-LQU-IMP-PNL
               ,:LQU-IMPB-IRPEF
                :IND-LQU-IMPB-IRPEF
               ,:LQU-IMPST-IRPEF
                :IND-LQU-IMPST-IRPEF
               ,:LQU-DT-VLT-DB
                :IND-LQU-DT-VLT
               ,:LQU-DT-END-ISTR-DB
                :IND-LQU-DT-END-ISTR
               ,:LQU-TP-RIMB
               ,:LQU-SPE-RCS
                :IND-LQU-SPE-RCS
               ,:LQU-IB-LIQ
                :IND-LQU-IB-LIQ
               ,:LQU-TOT-IAS-ONER-PRVNT
                :IND-LQU-TOT-IAS-ONER-PRVNT
               ,:LQU-TOT-IAS-MGG-SIN
                :IND-LQU-TOT-IAS-MGG-SIN
               ,:LQU-TOT-IAS-RST-DPST
                :IND-LQU-TOT-IAS-RST-DPST
               ,:LQU-IMP-ONER-LIQ
                :IND-LQU-IMP-ONER-LIQ
               ,:LQU-COMPON-TAX-RIMB
                :IND-LQU-COMPON-TAX-RIMB
               ,:LQU-TP-MEZ-PAG
                :IND-LQU-TP-MEZ-PAG
               ,:LQU-IMP-EXCONTR
                :IND-LQU-IMP-EXCONTR
               ,:LQU-IMP-INTR-RIT-PAG
                :IND-LQU-IMP-INTR-RIT-PAG
               ,:LQU-BNS-NON-GODUTO
                :IND-LQU-BNS-NON-GODUTO
               ,:LQU-CNBT-INPSTFM
                :IND-LQU-CNBT-INPSTFM
               ,:LQU-IMPST-DA-RIMB
                :IND-LQU-IMPST-DA-RIMB
               ,:LQU-IMPB-IS
                :IND-LQU-IMPB-IS
               ,:LQU-TAX-SEP
                :IND-LQU-TAX-SEP
               ,:LQU-IMPB-TAX-SEP
                :IND-LQU-IMPB-TAX-SEP
               ,:LQU-IMPB-INTR-SU-PREST
                :IND-LQU-IMPB-INTR-SU-PREST
               ,:LQU-ADDIZ-COMUN
                :IND-LQU-ADDIZ-COMUN
               ,:LQU-IMPB-ADDIZ-COMUN
                :IND-LQU-IMPB-ADDIZ-COMUN
               ,:LQU-ADDIZ-REGION
                :IND-LQU-ADDIZ-REGION
               ,:LQU-IMPB-ADDIZ-REGION
                :IND-LQU-IMPB-ADDIZ-REGION
               ,:LQU-MONT-DAL2007
                :IND-LQU-MONT-DAL2007
               ,:LQU-IMPB-CNBT-INPSTFM
                :IND-LQU-IMPB-CNBT-INPSTFM
               ,:LQU-IMP-LRD-DA-RIMB
                :IND-LQU-IMP-LRD-DA-RIMB
               ,:LQU-IMP-DIR-DA-RIMB
                :IND-LQU-IMP-DIR-DA-RIMB
               ,:LQU-RIS-MAT
                :IND-LQU-RIS-MAT
               ,:LQU-RIS-SPE
                :IND-LQU-RIS-SPE
               ,:LQU-DS-RIGA
               ,:LQU-DS-OPER-SQL
               ,:LQU-DS-VER
               ,:LQU-DS-TS-INI-CPTZ
               ,:LQU-DS-TS-END-CPTZ
               ,:LQU-DS-UTENTE
               ,:LQU-DS-STATO-ELAB
               ,:LQU-TOT-IAS-PNL
                :IND-LQU-TOT-IAS-PNL
               ,:LQU-FL-EVE-GARTO
                :IND-LQU-FL-EVE-GARTO
               ,:LQU-IMP-REN-K1
                :IND-LQU-IMP-REN-K1
               ,:LQU-IMP-REN-K2
                :IND-LQU-IMP-REN-K2
               ,:LQU-IMP-REN-K3
                :IND-LQU-IMP-REN-K3
               ,:LQU-PC-REN-K1
                :IND-LQU-PC-REN-K1
               ,:LQU-PC-REN-K2
                :IND-LQU-PC-REN-K2
               ,:LQU-PC-REN-K3
                :IND-LQU-PC-REN-K3
               ,:LQU-TP-CAUS-ANTIC
                :IND-LQU-TP-CAUS-ANTIC
               ,:LQU-IMP-LRD-LIQTO-RILT
                :IND-LQU-IMP-LRD-LIQTO-RILT
               ,:LQU-IMPST-APPL-RILT
                :IND-LQU-IMPST-APPL-RILT
               ,:LQU-PC-RISC-PARZ
                :IND-LQU-PC-RISC-PARZ
               ,:LQU-IMPST-BOLLO-TOT-V
                :IND-LQU-IMPST-BOLLO-TOT-V
               ,:LQU-IMPST-BOLLO-DETT-C
                :IND-LQU-IMPST-BOLLO-DETT-C
               ,:LQU-IMPST-BOLLO-TOT-SW
                :IND-LQU-IMPST-BOLLO-TOT-SW
               ,:LQU-IMPST-BOLLO-TOT-AA
                :IND-LQU-IMPST-BOLLO-TOT-AA
               ,:LQU-IMPB-VIS-1382011
                :IND-LQU-IMPB-VIS-1382011
               ,:LQU-IMPST-VIS-1382011
                :IND-LQU-IMPST-VIS-1382011
               ,:LQU-IMPB-IS-1382011
                :IND-LQU-IMPB-IS-1382011
               ,:LQU-IMPST-SOST-1382011
                :IND-LQU-IMPST-SOST-1382011
               ,:LQU-PC-ABB-TIT-STAT
                :IND-LQU-PC-ABB-TIT-STAT
               ,:LQU-IMPB-BOLLO-DETT-C
                :IND-LQU-IMPB-BOLLO-DETT-C
               ,:LQU-FL-PRE-COMP
                :IND-LQU-FL-PRE-COMP
               ,:LQU-IMPB-VIS-662014
                :IND-LQU-IMPB-VIS-662014
               ,:LQU-IMPST-VIS-662014
                :IND-LQU-IMPST-VIS-662014
               ,:LQU-IMPB-IS-662014
                :IND-LQU-IMPB-IS-662014
               ,:LQU-IMPST-SOST-662014
                :IND-LQU-IMPST-SOST-662014
               ,:LQU-PC-ABB-TS-662014
                :IND-LQU-PC-ABB-TS-662014
               ,:LQU-IMP-LRD-CALC-CP
                :IND-LQU-IMP-LRD-CALC-CP
               ,:LQU-COS-TUNNEL-USCITA
                :IND-LQU-COS-TUNNEL-USCITA
             FROM LIQ
             WHERE     IB_LIQ = :LQU-IB-LIQ
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.
       A610-LIQ-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF LQU-IB-LIQ NOT = HIGH-VALUES
               PERFORM A610-SELECT-IBS-LIQ
                  THRU A610-LIQ-EX
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           IF LQU-IB-LIQ NOT = HIGH-VALUES
              EXEC SQL
                   OPEN C-IBS-EFF-LQU-0
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           IF LQU-IB-LIQ NOT = HIGH-VALUES
              EXEC SQL
                   CLOSE C-IBS-EFF-LQU-0
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FN-IBS-LIQ.
           EXEC SQL
                FETCH C-IBS-EFF-LQU-0
           INTO
                :LQU-ID-LIQ
               ,:LQU-ID-OGG
               ,:LQU-TP-OGG
               ,:LQU-ID-MOVI-CRZ
               ,:LQU-ID-MOVI-CHIU
                :IND-LQU-ID-MOVI-CHIU
               ,:LQU-DT-INI-EFF-DB
               ,:LQU-DT-END-EFF-DB
               ,:LQU-COD-COMP-ANIA
               ,:LQU-IB-OGG
                :IND-LQU-IB-OGG
               ,:LQU-TP-LIQ
               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
                :IND-LQU-DESC-CAU-EVE-SIN
               ,:LQU-COD-CAU-SIN
                :IND-LQU-COD-CAU-SIN
               ,:LQU-COD-SIN-CATSTRF
                :IND-LQU-COD-SIN-CATSTRF
               ,:LQU-DT-MOR-DB
                :IND-LQU-DT-MOR
               ,:LQU-DT-DEN-DB
                :IND-LQU-DT-DEN
               ,:LQU-DT-PERV-DEN-DB
                :IND-LQU-DT-PERV-DEN
               ,:LQU-DT-RICH-DB
                :IND-LQU-DT-RICH
               ,:LQU-TP-SIN
                :IND-LQU-TP-SIN
               ,:LQU-TP-RISC
                :IND-LQU-TP-RISC
               ,:LQU-TP-MET-RISC
                :IND-LQU-TP-MET-RISC
               ,:LQU-DT-LIQ-DB
                :IND-LQU-DT-LIQ
               ,:LQU-COD-DVS
                :IND-LQU-COD-DVS
               ,:LQU-TOT-IMP-LRD-LIQTO
                :IND-LQU-TOT-IMP-LRD-LIQTO
               ,:LQU-TOT-IMP-PREST
                :IND-LQU-TOT-IMP-PREST
               ,:LQU-TOT-IMP-INTR-PREST
                :IND-LQU-TOT-IMP-INTR-PREST
               ,:LQU-TOT-IMP-UTI
                :IND-LQU-TOT-IMP-UTI
               ,:LQU-TOT-IMP-RIT-TFR
                :IND-LQU-TOT-IMP-RIT-TFR
               ,:LQU-TOT-IMP-RIT-ACC
                :IND-LQU-TOT-IMP-RIT-ACC
               ,:LQU-TOT-IMP-RIT-VIS
                :IND-LQU-TOT-IMP-RIT-VIS
               ,:LQU-TOT-IMPB-TFR
                :IND-LQU-TOT-IMPB-TFR
               ,:LQU-TOT-IMPB-ACC
                :IND-LQU-TOT-IMPB-ACC
               ,:LQU-TOT-IMPB-VIS
                :IND-LQU-TOT-IMPB-VIS
               ,:LQU-TOT-IMP-RIMB
                :IND-LQU-TOT-IMP-RIMB
               ,:LQU-IMPB-IMPST-PRVR
                :IND-LQU-IMPB-IMPST-PRVR
               ,:LQU-IMPST-PRVR
                :IND-LQU-IMPST-PRVR
               ,:LQU-IMPB-IMPST-252
                :IND-LQU-IMPB-IMPST-252
               ,:LQU-IMPST-252
                :IND-LQU-IMPST-252
               ,:LQU-TOT-IMP-IS
                :IND-LQU-TOT-IMP-IS
               ,:LQU-IMP-DIR-LIQ
                :IND-LQU-IMP-DIR-LIQ
               ,:LQU-TOT-IMP-NET-LIQTO
                :IND-LQU-TOT-IMP-NET-LIQTO
               ,:LQU-MONT-END2000
                :IND-LQU-MONT-END2000
               ,:LQU-MONT-END2006
                :IND-LQU-MONT-END2006
               ,:LQU-PC-REN
                :IND-LQU-PC-REN
               ,:LQU-IMP-PNL
                :IND-LQU-IMP-PNL
               ,:LQU-IMPB-IRPEF
                :IND-LQU-IMPB-IRPEF
               ,:LQU-IMPST-IRPEF
                :IND-LQU-IMPST-IRPEF
               ,:LQU-DT-VLT-DB
                :IND-LQU-DT-VLT
               ,:LQU-DT-END-ISTR-DB
                :IND-LQU-DT-END-ISTR
               ,:LQU-TP-RIMB
               ,:LQU-SPE-RCS
                :IND-LQU-SPE-RCS
               ,:LQU-IB-LIQ
                :IND-LQU-IB-LIQ
               ,:LQU-TOT-IAS-ONER-PRVNT
                :IND-LQU-TOT-IAS-ONER-PRVNT
               ,:LQU-TOT-IAS-MGG-SIN
                :IND-LQU-TOT-IAS-MGG-SIN
               ,:LQU-TOT-IAS-RST-DPST
                :IND-LQU-TOT-IAS-RST-DPST
               ,:LQU-IMP-ONER-LIQ
                :IND-LQU-IMP-ONER-LIQ
               ,:LQU-COMPON-TAX-RIMB
                :IND-LQU-COMPON-TAX-RIMB
               ,:LQU-TP-MEZ-PAG
                :IND-LQU-TP-MEZ-PAG
               ,:LQU-IMP-EXCONTR
                :IND-LQU-IMP-EXCONTR
               ,:LQU-IMP-INTR-RIT-PAG
                :IND-LQU-IMP-INTR-RIT-PAG
               ,:LQU-BNS-NON-GODUTO
                :IND-LQU-BNS-NON-GODUTO
               ,:LQU-CNBT-INPSTFM
                :IND-LQU-CNBT-INPSTFM
               ,:LQU-IMPST-DA-RIMB
                :IND-LQU-IMPST-DA-RIMB
               ,:LQU-IMPB-IS
                :IND-LQU-IMPB-IS
               ,:LQU-TAX-SEP
                :IND-LQU-TAX-SEP
               ,:LQU-IMPB-TAX-SEP
                :IND-LQU-IMPB-TAX-SEP
               ,:LQU-IMPB-INTR-SU-PREST
                :IND-LQU-IMPB-INTR-SU-PREST
               ,:LQU-ADDIZ-COMUN
                :IND-LQU-ADDIZ-COMUN
               ,:LQU-IMPB-ADDIZ-COMUN
                :IND-LQU-IMPB-ADDIZ-COMUN
               ,:LQU-ADDIZ-REGION
                :IND-LQU-ADDIZ-REGION
               ,:LQU-IMPB-ADDIZ-REGION
                :IND-LQU-IMPB-ADDIZ-REGION
               ,:LQU-MONT-DAL2007
                :IND-LQU-MONT-DAL2007
               ,:LQU-IMPB-CNBT-INPSTFM
                :IND-LQU-IMPB-CNBT-INPSTFM
               ,:LQU-IMP-LRD-DA-RIMB
                :IND-LQU-IMP-LRD-DA-RIMB
               ,:LQU-IMP-DIR-DA-RIMB
                :IND-LQU-IMP-DIR-DA-RIMB
               ,:LQU-RIS-MAT
                :IND-LQU-RIS-MAT
               ,:LQU-RIS-SPE
                :IND-LQU-RIS-SPE
               ,:LQU-DS-RIGA
               ,:LQU-DS-OPER-SQL
               ,:LQU-DS-VER
               ,:LQU-DS-TS-INI-CPTZ
               ,:LQU-DS-TS-END-CPTZ
               ,:LQU-DS-UTENTE
               ,:LQU-DS-STATO-ELAB
               ,:LQU-TOT-IAS-PNL
                :IND-LQU-TOT-IAS-PNL
               ,:LQU-FL-EVE-GARTO
                :IND-LQU-FL-EVE-GARTO
               ,:LQU-IMP-REN-K1
                :IND-LQU-IMP-REN-K1
               ,:LQU-IMP-REN-K2
                :IND-LQU-IMP-REN-K2
               ,:LQU-IMP-REN-K3
                :IND-LQU-IMP-REN-K3
               ,:LQU-PC-REN-K1
                :IND-LQU-PC-REN-K1
               ,:LQU-PC-REN-K2
                :IND-LQU-PC-REN-K2
               ,:LQU-PC-REN-K3
                :IND-LQU-PC-REN-K3
               ,:LQU-TP-CAUS-ANTIC
                :IND-LQU-TP-CAUS-ANTIC
               ,:LQU-IMP-LRD-LIQTO-RILT
                :IND-LQU-IMP-LRD-LIQTO-RILT
               ,:LQU-IMPST-APPL-RILT
                :IND-LQU-IMPST-APPL-RILT
               ,:LQU-PC-RISC-PARZ
                :IND-LQU-PC-RISC-PARZ
               ,:LQU-IMPST-BOLLO-TOT-V
                :IND-LQU-IMPST-BOLLO-TOT-V
               ,:LQU-IMPST-BOLLO-DETT-C
                :IND-LQU-IMPST-BOLLO-DETT-C
               ,:LQU-IMPST-BOLLO-TOT-SW
                :IND-LQU-IMPST-BOLLO-TOT-SW
               ,:LQU-IMPST-BOLLO-TOT-AA
                :IND-LQU-IMPST-BOLLO-TOT-AA
               ,:LQU-IMPB-VIS-1382011
                :IND-LQU-IMPB-VIS-1382011
               ,:LQU-IMPST-VIS-1382011
                :IND-LQU-IMPST-VIS-1382011
               ,:LQU-IMPB-IS-1382011
                :IND-LQU-IMPB-IS-1382011
               ,:LQU-IMPST-SOST-1382011
                :IND-LQU-IMPST-SOST-1382011
               ,:LQU-PC-ABB-TIT-STAT
                :IND-LQU-PC-ABB-TIT-STAT
               ,:LQU-IMPB-BOLLO-DETT-C
                :IND-LQU-IMPB-BOLLO-DETT-C
               ,:LQU-FL-PRE-COMP
                :IND-LQU-FL-PRE-COMP
               ,:LQU-IMPB-VIS-662014
                :IND-LQU-IMPB-VIS-662014
               ,:LQU-IMPST-VIS-662014
                :IND-LQU-IMPST-VIS-662014
               ,:LQU-IMPB-IS-662014
                :IND-LQU-IMPB-IS-662014
               ,:LQU-IMPST-SOST-662014
                :IND-LQU-IMPST-SOST-662014
               ,:LQU-PC-ABB-TS-662014
                :IND-LQU-PC-ABB-TS-662014
               ,:LQU-IMP-LRD-CALC-CP
                :IND-LQU-IMP-LRD-CALC-CP
               ,:LQU-COS-TUNNEL-USCITA
                :IND-LQU-COS-TUNNEL-USCITA
           END-EXEC.
       A690-LIQ-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           IF LQU-IB-LIQ NOT = HIGH-VALUES
               PERFORM A690-FN-IBS-LIQ
                  THRU A690-LIQ-EX
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A670-CLOSE-CURSOR-IBS     THRU A670-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDO-EFF-LQU CURSOR FOR
              SELECT
                     ID_LIQ
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,IB_OGG
                    ,TP_LIQ
                    ,DESC_CAU_EVE_SIN
                    ,COD_CAU_SIN
                    ,COD_SIN_CATSTRF
                    ,DT_MOR
                    ,DT_DEN
                    ,DT_PERV_DEN
                    ,DT_RICH
                    ,TP_SIN
                    ,TP_RISC
                    ,TP_MET_RISC
                    ,DT_LIQ
                    ,COD_DVS
                    ,TOT_IMP_LRD_LIQTO
                    ,TOT_IMP_PREST
                    ,TOT_IMP_INTR_PREST
                    ,TOT_IMP_UTI
                    ,TOT_IMP_RIT_TFR
                    ,TOT_IMP_RIT_ACC
                    ,TOT_IMP_RIT_VIS
                    ,TOT_IMPB_TFR
                    ,TOT_IMPB_ACC
                    ,TOT_IMPB_VIS
                    ,TOT_IMP_RIMB
                    ,IMPB_IMPST_PRVR
                    ,IMPST_PRVR
                    ,IMPB_IMPST_252
                    ,IMPST_252
                    ,TOT_IMP_IS
                    ,IMP_DIR_LIQ
                    ,TOT_IMP_NET_LIQTO
                    ,MONT_END2000
                    ,MONT_END2006
                    ,PC_REN
                    ,IMP_PNL
                    ,IMPB_IRPEF
                    ,IMPST_IRPEF
                    ,DT_VLT
                    ,DT_END_ISTR
                    ,TP_RIMB
                    ,SPE_RCS
                    ,IB_LIQ
                    ,TOT_IAS_ONER_PRVNT
                    ,TOT_IAS_MGG_SIN
                    ,TOT_IAS_RST_DPST
                    ,IMP_ONER_LIQ
                    ,COMPON_TAX_RIMB
                    ,TP_MEZ_PAG
                    ,IMP_EXCONTR
                    ,IMP_INTR_RIT_PAG
                    ,BNS_NON_GODUTO
                    ,CNBT_INPSTFM
                    ,IMPST_DA_RIMB
                    ,IMPB_IS
                    ,TAX_SEP
                    ,IMPB_TAX_SEP
                    ,IMPB_INTR_SU_PREST
                    ,ADDIZ_COMUN
                    ,IMPB_ADDIZ_COMUN
                    ,ADDIZ_REGION
                    ,IMPB_ADDIZ_REGION
                    ,MONT_DAL2007
                    ,IMPB_CNBT_INPSTFM
                    ,IMP_LRD_DA_RIMB
                    ,IMP_DIR_DA_RIMB
                    ,RIS_MAT
                    ,RIS_SPE
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,TOT_IAS_PNL
                    ,FL_EVE_GARTO
                    ,IMP_REN_K1
                    ,IMP_REN_K2
                    ,IMP_REN_K3
                    ,PC_REN_K1
                    ,PC_REN_K2
                    ,PC_REN_K3
                    ,TP_CAUS_ANTIC
                    ,IMP_LRD_LIQTO_RILT
                    ,IMPST_APPL_RILT
                    ,PC_RISC_PARZ
                    ,IMPST_BOLLO_TOT_V
                    ,IMPST_BOLLO_DETT_C
                    ,IMPST_BOLLO_TOT_SW
                    ,IMPST_BOLLO_TOT_AA
                    ,IMPB_VIS_1382011
                    ,IMPST_VIS_1382011
                    ,IMPB_IS_1382011
                    ,IMPST_SOST_1382011
                    ,PC_ABB_TIT_STAT
                    ,IMPB_BOLLO_DETT_C
                    ,FL_PRE_COMP
                    ,IMPB_VIS_662014
                    ,IMPST_VIS_662014
                    ,IMPB_IS_662014
                    ,IMPST_SOST_662014
                    ,PC_ABB_TS_662014
                    ,IMP_LRD_CALC_CP
                    ,COS_TUNNEL_USCITA
              FROM LIQ
              WHERE     ID_OGG = :LQU-ID-OGG
                    AND TP_OGG = :LQU-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_LIQ ASC

           END-EXEC.

       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_LIQ
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,TP_LIQ
                ,DESC_CAU_EVE_SIN
                ,COD_CAU_SIN
                ,COD_SIN_CATSTRF
                ,DT_MOR
                ,DT_DEN
                ,DT_PERV_DEN
                ,DT_RICH
                ,TP_SIN
                ,TP_RISC
                ,TP_MET_RISC
                ,DT_LIQ
                ,COD_DVS
                ,TOT_IMP_LRD_LIQTO
                ,TOT_IMP_PREST
                ,TOT_IMP_INTR_PREST
                ,TOT_IMP_UTI
                ,TOT_IMP_RIT_TFR
                ,TOT_IMP_RIT_ACC
                ,TOT_IMP_RIT_VIS
                ,TOT_IMPB_TFR
                ,TOT_IMPB_ACC
                ,TOT_IMPB_VIS
                ,TOT_IMP_RIMB
                ,IMPB_IMPST_PRVR
                ,IMPST_PRVR
                ,IMPB_IMPST_252
                ,IMPST_252
                ,TOT_IMP_IS
                ,IMP_DIR_LIQ
                ,TOT_IMP_NET_LIQTO
                ,MONT_END2000
                ,MONT_END2006
                ,PC_REN
                ,IMP_PNL
                ,IMPB_IRPEF
                ,IMPST_IRPEF
                ,DT_VLT
                ,DT_END_ISTR
                ,TP_RIMB
                ,SPE_RCS
                ,IB_LIQ
                ,TOT_IAS_ONER_PRVNT
                ,TOT_IAS_MGG_SIN
                ,TOT_IAS_RST_DPST
                ,IMP_ONER_LIQ
                ,COMPON_TAX_RIMB
                ,TP_MEZ_PAG
                ,IMP_EXCONTR
                ,IMP_INTR_RIT_PAG
                ,BNS_NON_GODUTO
                ,CNBT_INPSTFM
                ,IMPST_DA_RIMB
                ,IMPB_IS
                ,TAX_SEP
                ,IMPB_TAX_SEP
                ,IMPB_INTR_SU_PREST
                ,ADDIZ_COMUN
                ,IMPB_ADDIZ_COMUN
                ,ADDIZ_REGION
                ,IMPB_ADDIZ_REGION
                ,MONT_DAL2007
                ,IMPB_CNBT_INPSTFM
                ,IMP_LRD_DA_RIMB
                ,IMP_DIR_DA_RIMB
                ,RIS_MAT
                ,RIS_SPE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TOT_IAS_PNL
                ,FL_EVE_GARTO
                ,IMP_REN_K1
                ,IMP_REN_K2
                ,IMP_REN_K3
                ,PC_REN_K1
                ,PC_REN_K2
                ,PC_REN_K3
                ,TP_CAUS_ANTIC
                ,IMP_LRD_LIQTO_RILT
                ,IMPST_APPL_RILT
                ,PC_RISC_PARZ
                ,IMPST_BOLLO_TOT_V
                ,IMPST_BOLLO_DETT_C
                ,IMPST_BOLLO_TOT_SW
                ,IMPST_BOLLO_TOT_AA
                ,IMPB_VIS_1382011
                ,IMPST_VIS_1382011
                ,IMPB_IS_1382011
                ,IMPST_SOST_1382011
                ,PC_ABB_TIT_STAT
                ,IMPB_BOLLO_DETT_C
                ,FL_PRE_COMP
                ,IMPB_VIS_662014
                ,IMPST_VIS_662014
                ,IMPB_IS_662014
                ,IMPST_SOST_662014
                ,PC_ABB_TS_662014
                ,IMP_LRD_CALC_CP
                ,COS_TUNNEL_USCITA
             INTO
                :LQU-ID-LIQ
               ,:LQU-ID-OGG
               ,:LQU-TP-OGG
               ,:LQU-ID-MOVI-CRZ
               ,:LQU-ID-MOVI-CHIU
                :IND-LQU-ID-MOVI-CHIU
               ,:LQU-DT-INI-EFF-DB
               ,:LQU-DT-END-EFF-DB
               ,:LQU-COD-COMP-ANIA
               ,:LQU-IB-OGG
                :IND-LQU-IB-OGG
               ,:LQU-TP-LIQ
               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
                :IND-LQU-DESC-CAU-EVE-SIN
               ,:LQU-COD-CAU-SIN
                :IND-LQU-COD-CAU-SIN
               ,:LQU-COD-SIN-CATSTRF
                :IND-LQU-COD-SIN-CATSTRF
               ,:LQU-DT-MOR-DB
                :IND-LQU-DT-MOR
               ,:LQU-DT-DEN-DB
                :IND-LQU-DT-DEN
               ,:LQU-DT-PERV-DEN-DB
                :IND-LQU-DT-PERV-DEN
               ,:LQU-DT-RICH-DB
                :IND-LQU-DT-RICH
               ,:LQU-TP-SIN
                :IND-LQU-TP-SIN
               ,:LQU-TP-RISC
                :IND-LQU-TP-RISC
               ,:LQU-TP-MET-RISC
                :IND-LQU-TP-MET-RISC
               ,:LQU-DT-LIQ-DB
                :IND-LQU-DT-LIQ
               ,:LQU-COD-DVS
                :IND-LQU-COD-DVS
               ,:LQU-TOT-IMP-LRD-LIQTO
                :IND-LQU-TOT-IMP-LRD-LIQTO
               ,:LQU-TOT-IMP-PREST
                :IND-LQU-TOT-IMP-PREST
               ,:LQU-TOT-IMP-INTR-PREST
                :IND-LQU-TOT-IMP-INTR-PREST
               ,:LQU-TOT-IMP-UTI
                :IND-LQU-TOT-IMP-UTI
               ,:LQU-TOT-IMP-RIT-TFR
                :IND-LQU-TOT-IMP-RIT-TFR
               ,:LQU-TOT-IMP-RIT-ACC
                :IND-LQU-TOT-IMP-RIT-ACC
               ,:LQU-TOT-IMP-RIT-VIS
                :IND-LQU-TOT-IMP-RIT-VIS
               ,:LQU-TOT-IMPB-TFR
                :IND-LQU-TOT-IMPB-TFR
               ,:LQU-TOT-IMPB-ACC
                :IND-LQU-TOT-IMPB-ACC
               ,:LQU-TOT-IMPB-VIS
                :IND-LQU-TOT-IMPB-VIS
               ,:LQU-TOT-IMP-RIMB
                :IND-LQU-TOT-IMP-RIMB
               ,:LQU-IMPB-IMPST-PRVR
                :IND-LQU-IMPB-IMPST-PRVR
               ,:LQU-IMPST-PRVR
                :IND-LQU-IMPST-PRVR
               ,:LQU-IMPB-IMPST-252
                :IND-LQU-IMPB-IMPST-252
               ,:LQU-IMPST-252
                :IND-LQU-IMPST-252
               ,:LQU-TOT-IMP-IS
                :IND-LQU-TOT-IMP-IS
               ,:LQU-IMP-DIR-LIQ
                :IND-LQU-IMP-DIR-LIQ
               ,:LQU-TOT-IMP-NET-LIQTO
                :IND-LQU-TOT-IMP-NET-LIQTO
               ,:LQU-MONT-END2000
                :IND-LQU-MONT-END2000
               ,:LQU-MONT-END2006
                :IND-LQU-MONT-END2006
               ,:LQU-PC-REN
                :IND-LQU-PC-REN
               ,:LQU-IMP-PNL
                :IND-LQU-IMP-PNL
               ,:LQU-IMPB-IRPEF
                :IND-LQU-IMPB-IRPEF
               ,:LQU-IMPST-IRPEF
                :IND-LQU-IMPST-IRPEF
               ,:LQU-DT-VLT-DB
                :IND-LQU-DT-VLT
               ,:LQU-DT-END-ISTR-DB
                :IND-LQU-DT-END-ISTR
               ,:LQU-TP-RIMB
               ,:LQU-SPE-RCS
                :IND-LQU-SPE-RCS
               ,:LQU-IB-LIQ
                :IND-LQU-IB-LIQ
               ,:LQU-TOT-IAS-ONER-PRVNT
                :IND-LQU-TOT-IAS-ONER-PRVNT
               ,:LQU-TOT-IAS-MGG-SIN
                :IND-LQU-TOT-IAS-MGG-SIN
               ,:LQU-TOT-IAS-RST-DPST
                :IND-LQU-TOT-IAS-RST-DPST
               ,:LQU-IMP-ONER-LIQ
                :IND-LQU-IMP-ONER-LIQ
               ,:LQU-COMPON-TAX-RIMB
                :IND-LQU-COMPON-TAX-RIMB
               ,:LQU-TP-MEZ-PAG
                :IND-LQU-TP-MEZ-PAG
               ,:LQU-IMP-EXCONTR
                :IND-LQU-IMP-EXCONTR
               ,:LQU-IMP-INTR-RIT-PAG
                :IND-LQU-IMP-INTR-RIT-PAG
               ,:LQU-BNS-NON-GODUTO
                :IND-LQU-BNS-NON-GODUTO
               ,:LQU-CNBT-INPSTFM
                :IND-LQU-CNBT-INPSTFM
               ,:LQU-IMPST-DA-RIMB
                :IND-LQU-IMPST-DA-RIMB
               ,:LQU-IMPB-IS
                :IND-LQU-IMPB-IS
               ,:LQU-TAX-SEP
                :IND-LQU-TAX-SEP
               ,:LQU-IMPB-TAX-SEP
                :IND-LQU-IMPB-TAX-SEP
               ,:LQU-IMPB-INTR-SU-PREST
                :IND-LQU-IMPB-INTR-SU-PREST
               ,:LQU-ADDIZ-COMUN
                :IND-LQU-ADDIZ-COMUN
               ,:LQU-IMPB-ADDIZ-COMUN
                :IND-LQU-IMPB-ADDIZ-COMUN
               ,:LQU-ADDIZ-REGION
                :IND-LQU-ADDIZ-REGION
               ,:LQU-IMPB-ADDIZ-REGION
                :IND-LQU-IMPB-ADDIZ-REGION
               ,:LQU-MONT-DAL2007
                :IND-LQU-MONT-DAL2007
               ,:LQU-IMPB-CNBT-INPSTFM
                :IND-LQU-IMPB-CNBT-INPSTFM
               ,:LQU-IMP-LRD-DA-RIMB
                :IND-LQU-IMP-LRD-DA-RIMB
               ,:LQU-IMP-DIR-DA-RIMB
                :IND-LQU-IMP-DIR-DA-RIMB
               ,:LQU-RIS-MAT
                :IND-LQU-RIS-MAT
               ,:LQU-RIS-SPE
                :IND-LQU-RIS-SPE
               ,:LQU-DS-RIGA
               ,:LQU-DS-OPER-SQL
               ,:LQU-DS-VER
               ,:LQU-DS-TS-INI-CPTZ
               ,:LQU-DS-TS-END-CPTZ
               ,:LQU-DS-UTENTE
               ,:LQU-DS-STATO-ELAB
               ,:LQU-TOT-IAS-PNL
                :IND-LQU-TOT-IAS-PNL
               ,:LQU-FL-EVE-GARTO
                :IND-LQU-FL-EVE-GARTO
               ,:LQU-IMP-REN-K1
                :IND-LQU-IMP-REN-K1
               ,:LQU-IMP-REN-K2
                :IND-LQU-IMP-REN-K2
               ,:LQU-IMP-REN-K3
                :IND-LQU-IMP-REN-K3
               ,:LQU-PC-REN-K1
                :IND-LQU-PC-REN-K1
               ,:LQU-PC-REN-K2
                :IND-LQU-PC-REN-K2
               ,:LQU-PC-REN-K3
                :IND-LQU-PC-REN-K3
               ,:LQU-TP-CAUS-ANTIC
                :IND-LQU-TP-CAUS-ANTIC
               ,:LQU-IMP-LRD-LIQTO-RILT
                :IND-LQU-IMP-LRD-LIQTO-RILT
               ,:LQU-IMPST-APPL-RILT
                :IND-LQU-IMPST-APPL-RILT
               ,:LQU-PC-RISC-PARZ
                :IND-LQU-PC-RISC-PARZ
               ,:LQU-IMPST-BOLLO-TOT-V
                :IND-LQU-IMPST-BOLLO-TOT-V
               ,:LQU-IMPST-BOLLO-DETT-C
                :IND-LQU-IMPST-BOLLO-DETT-C
               ,:LQU-IMPST-BOLLO-TOT-SW
                :IND-LQU-IMPST-BOLLO-TOT-SW
               ,:LQU-IMPST-BOLLO-TOT-AA
                :IND-LQU-IMPST-BOLLO-TOT-AA
               ,:LQU-IMPB-VIS-1382011
                :IND-LQU-IMPB-VIS-1382011
               ,:LQU-IMPST-VIS-1382011
                :IND-LQU-IMPST-VIS-1382011
               ,:LQU-IMPB-IS-1382011
                :IND-LQU-IMPB-IS-1382011
               ,:LQU-IMPST-SOST-1382011
                :IND-LQU-IMPST-SOST-1382011
               ,:LQU-PC-ABB-TIT-STAT
                :IND-LQU-PC-ABB-TIT-STAT
               ,:LQU-IMPB-BOLLO-DETT-C
                :IND-LQU-IMPB-BOLLO-DETT-C
               ,:LQU-FL-PRE-COMP
                :IND-LQU-FL-PRE-COMP
               ,:LQU-IMPB-VIS-662014
                :IND-LQU-IMPB-VIS-662014
               ,:LQU-IMPST-VIS-662014
                :IND-LQU-IMPST-VIS-662014
               ,:LQU-IMPB-IS-662014
                :IND-LQU-IMPB-IS-662014
               ,:LQU-IMPST-SOST-662014
                :IND-LQU-IMPST-SOST-662014
               ,:LQU-PC-ABB-TS-662014
                :IND-LQU-PC-ABB-TS-662014
               ,:LQU-IMP-LRD-CALC-CP
                :IND-LQU-IMP-LRD-CALC-CP
               ,:LQU-COS-TUNNEL-USCITA
                :IND-LQU-COS-TUNNEL-USCITA
             FROM LIQ
             WHERE     ID_OGG = :LQU-ID-OGG
                    AND TP_OGG = :LQU-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           EXEC SQL
                OPEN C-IDO-EFF-LQU
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           EXEC SQL
                CLOSE C-IDO-EFF-LQU
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           EXEC SQL
                FETCH C-IDO-EFF-LQU
           INTO
                :LQU-ID-LIQ
               ,:LQU-ID-OGG
               ,:LQU-TP-OGG
               ,:LQU-ID-MOVI-CRZ
               ,:LQU-ID-MOVI-CHIU
                :IND-LQU-ID-MOVI-CHIU
               ,:LQU-DT-INI-EFF-DB
               ,:LQU-DT-END-EFF-DB
               ,:LQU-COD-COMP-ANIA
               ,:LQU-IB-OGG
                :IND-LQU-IB-OGG
               ,:LQU-TP-LIQ
               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
                :IND-LQU-DESC-CAU-EVE-SIN
               ,:LQU-COD-CAU-SIN
                :IND-LQU-COD-CAU-SIN
               ,:LQU-COD-SIN-CATSTRF
                :IND-LQU-COD-SIN-CATSTRF
               ,:LQU-DT-MOR-DB
                :IND-LQU-DT-MOR
               ,:LQU-DT-DEN-DB
                :IND-LQU-DT-DEN
               ,:LQU-DT-PERV-DEN-DB
                :IND-LQU-DT-PERV-DEN
               ,:LQU-DT-RICH-DB
                :IND-LQU-DT-RICH
               ,:LQU-TP-SIN
                :IND-LQU-TP-SIN
               ,:LQU-TP-RISC
                :IND-LQU-TP-RISC
               ,:LQU-TP-MET-RISC
                :IND-LQU-TP-MET-RISC
               ,:LQU-DT-LIQ-DB
                :IND-LQU-DT-LIQ
               ,:LQU-COD-DVS
                :IND-LQU-COD-DVS
               ,:LQU-TOT-IMP-LRD-LIQTO
                :IND-LQU-TOT-IMP-LRD-LIQTO
               ,:LQU-TOT-IMP-PREST
                :IND-LQU-TOT-IMP-PREST
               ,:LQU-TOT-IMP-INTR-PREST
                :IND-LQU-TOT-IMP-INTR-PREST
               ,:LQU-TOT-IMP-UTI
                :IND-LQU-TOT-IMP-UTI
               ,:LQU-TOT-IMP-RIT-TFR
                :IND-LQU-TOT-IMP-RIT-TFR
               ,:LQU-TOT-IMP-RIT-ACC
                :IND-LQU-TOT-IMP-RIT-ACC
               ,:LQU-TOT-IMP-RIT-VIS
                :IND-LQU-TOT-IMP-RIT-VIS
               ,:LQU-TOT-IMPB-TFR
                :IND-LQU-TOT-IMPB-TFR
               ,:LQU-TOT-IMPB-ACC
                :IND-LQU-TOT-IMPB-ACC
               ,:LQU-TOT-IMPB-VIS
                :IND-LQU-TOT-IMPB-VIS
               ,:LQU-TOT-IMP-RIMB
                :IND-LQU-TOT-IMP-RIMB
               ,:LQU-IMPB-IMPST-PRVR
                :IND-LQU-IMPB-IMPST-PRVR
               ,:LQU-IMPST-PRVR
                :IND-LQU-IMPST-PRVR
               ,:LQU-IMPB-IMPST-252
                :IND-LQU-IMPB-IMPST-252
               ,:LQU-IMPST-252
                :IND-LQU-IMPST-252
               ,:LQU-TOT-IMP-IS
                :IND-LQU-TOT-IMP-IS
               ,:LQU-IMP-DIR-LIQ
                :IND-LQU-IMP-DIR-LIQ
               ,:LQU-TOT-IMP-NET-LIQTO
                :IND-LQU-TOT-IMP-NET-LIQTO
               ,:LQU-MONT-END2000
                :IND-LQU-MONT-END2000
               ,:LQU-MONT-END2006
                :IND-LQU-MONT-END2006
               ,:LQU-PC-REN
                :IND-LQU-PC-REN
               ,:LQU-IMP-PNL
                :IND-LQU-IMP-PNL
               ,:LQU-IMPB-IRPEF
                :IND-LQU-IMPB-IRPEF
               ,:LQU-IMPST-IRPEF
                :IND-LQU-IMPST-IRPEF
               ,:LQU-DT-VLT-DB
                :IND-LQU-DT-VLT
               ,:LQU-DT-END-ISTR-DB
                :IND-LQU-DT-END-ISTR
               ,:LQU-TP-RIMB
               ,:LQU-SPE-RCS
                :IND-LQU-SPE-RCS
               ,:LQU-IB-LIQ
                :IND-LQU-IB-LIQ
               ,:LQU-TOT-IAS-ONER-PRVNT
                :IND-LQU-TOT-IAS-ONER-PRVNT
               ,:LQU-TOT-IAS-MGG-SIN
                :IND-LQU-TOT-IAS-MGG-SIN
               ,:LQU-TOT-IAS-RST-DPST
                :IND-LQU-TOT-IAS-RST-DPST
               ,:LQU-IMP-ONER-LIQ
                :IND-LQU-IMP-ONER-LIQ
               ,:LQU-COMPON-TAX-RIMB
                :IND-LQU-COMPON-TAX-RIMB
               ,:LQU-TP-MEZ-PAG
                :IND-LQU-TP-MEZ-PAG
               ,:LQU-IMP-EXCONTR
                :IND-LQU-IMP-EXCONTR
               ,:LQU-IMP-INTR-RIT-PAG
                :IND-LQU-IMP-INTR-RIT-PAG
               ,:LQU-BNS-NON-GODUTO
                :IND-LQU-BNS-NON-GODUTO
               ,:LQU-CNBT-INPSTFM
                :IND-LQU-CNBT-INPSTFM
               ,:LQU-IMPST-DA-RIMB
                :IND-LQU-IMPST-DA-RIMB
               ,:LQU-IMPB-IS
                :IND-LQU-IMPB-IS
               ,:LQU-TAX-SEP
                :IND-LQU-TAX-SEP
               ,:LQU-IMPB-TAX-SEP
                :IND-LQU-IMPB-TAX-SEP
               ,:LQU-IMPB-INTR-SU-PREST
                :IND-LQU-IMPB-INTR-SU-PREST
               ,:LQU-ADDIZ-COMUN
                :IND-LQU-ADDIZ-COMUN
               ,:LQU-IMPB-ADDIZ-COMUN
                :IND-LQU-IMPB-ADDIZ-COMUN
               ,:LQU-ADDIZ-REGION
                :IND-LQU-ADDIZ-REGION
               ,:LQU-IMPB-ADDIZ-REGION
                :IND-LQU-IMPB-ADDIZ-REGION
               ,:LQU-MONT-DAL2007
                :IND-LQU-MONT-DAL2007
               ,:LQU-IMPB-CNBT-INPSTFM
                :IND-LQU-IMPB-CNBT-INPSTFM
               ,:LQU-IMP-LRD-DA-RIMB
                :IND-LQU-IMP-LRD-DA-RIMB
               ,:LQU-IMP-DIR-DA-RIMB
                :IND-LQU-IMP-DIR-DA-RIMB
               ,:LQU-RIS-MAT
                :IND-LQU-RIS-MAT
               ,:LQU-RIS-SPE
                :IND-LQU-RIS-SPE
               ,:LQU-DS-RIGA
               ,:LQU-DS-OPER-SQL
               ,:LQU-DS-VER
               ,:LQU-DS-TS-INI-CPTZ
               ,:LQU-DS-TS-END-CPTZ
               ,:LQU-DS-UTENTE
               ,:LQU-DS-STATO-ELAB
               ,:LQU-TOT-IAS-PNL
                :IND-LQU-TOT-IAS-PNL
               ,:LQU-FL-EVE-GARTO
                :IND-LQU-FL-EVE-GARTO
               ,:LQU-IMP-REN-K1
                :IND-LQU-IMP-REN-K1
               ,:LQU-IMP-REN-K2
                :IND-LQU-IMP-REN-K2
               ,:LQU-IMP-REN-K3
                :IND-LQU-IMP-REN-K3
               ,:LQU-PC-REN-K1
                :IND-LQU-PC-REN-K1
               ,:LQU-PC-REN-K2
                :IND-LQU-PC-REN-K2
               ,:LQU-PC-REN-K3
                :IND-LQU-PC-REN-K3
               ,:LQU-TP-CAUS-ANTIC
                :IND-LQU-TP-CAUS-ANTIC
               ,:LQU-IMP-LRD-LIQTO-RILT
                :IND-LQU-IMP-LRD-LIQTO-RILT
               ,:LQU-IMPST-APPL-RILT
                :IND-LQU-IMPST-APPL-RILT
               ,:LQU-PC-RISC-PARZ
                :IND-LQU-PC-RISC-PARZ
               ,:LQU-IMPST-BOLLO-TOT-V
                :IND-LQU-IMPST-BOLLO-TOT-V
               ,:LQU-IMPST-BOLLO-DETT-C
                :IND-LQU-IMPST-BOLLO-DETT-C
               ,:LQU-IMPST-BOLLO-TOT-SW
                :IND-LQU-IMPST-BOLLO-TOT-SW
               ,:LQU-IMPST-BOLLO-TOT-AA
                :IND-LQU-IMPST-BOLLO-TOT-AA
               ,:LQU-IMPB-VIS-1382011
                :IND-LQU-IMPB-VIS-1382011
               ,:LQU-IMPST-VIS-1382011
                :IND-LQU-IMPST-VIS-1382011
               ,:LQU-IMPB-IS-1382011
                :IND-LQU-IMPB-IS-1382011
               ,:LQU-IMPST-SOST-1382011
                :IND-LQU-IMPST-SOST-1382011
               ,:LQU-PC-ABB-TIT-STAT
                :IND-LQU-PC-ABB-TIT-STAT
               ,:LQU-IMPB-BOLLO-DETT-C
                :IND-LQU-IMPB-BOLLO-DETT-C
               ,:LQU-FL-PRE-COMP
                :IND-LQU-FL-PRE-COMP
               ,:LQU-IMPB-VIS-662014
                :IND-LQU-IMPB-VIS-662014
               ,:LQU-IMPST-VIS-662014
                :IND-LQU-IMPST-VIS-662014
               ,:LQU-IMPB-IS-662014
                :IND-LQU-IMPB-IS-662014
               ,:LQU-IMPST-SOST-662014
                :IND-LQU-IMPST-SOST-662014
               ,:LQU-PC-ABB-TS-662014
                :IND-LQU-PC-ABB-TS-662014
               ,:LQU-IMP-LRD-CALC-CP
                :IND-LQU-IMP-LRD-CALC-CP
               ,:LQU-COS-TUNNEL-USCITA
                :IND-LQU-COS-TUNNEL-USCITA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A770-CLOSE-CURSOR-IDO     THRU A770-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_LIQ
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,TP_LIQ
                ,DESC_CAU_EVE_SIN
                ,COD_CAU_SIN
                ,COD_SIN_CATSTRF
                ,DT_MOR
                ,DT_DEN
                ,DT_PERV_DEN
                ,DT_RICH
                ,TP_SIN
                ,TP_RISC
                ,TP_MET_RISC
                ,DT_LIQ
                ,COD_DVS
                ,TOT_IMP_LRD_LIQTO
                ,TOT_IMP_PREST
                ,TOT_IMP_INTR_PREST
                ,TOT_IMP_UTI
                ,TOT_IMP_RIT_TFR
                ,TOT_IMP_RIT_ACC
                ,TOT_IMP_RIT_VIS
                ,TOT_IMPB_TFR
                ,TOT_IMPB_ACC
                ,TOT_IMPB_VIS
                ,TOT_IMP_RIMB
                ,IMPB_IMPST_PRVR
                ,IMPST_PRVR
                ,IMPB_IMPST_252
                ,IMPST_252
                ,TOT_IMP_IS
                ,IMP_DIR_LIQ
                ,TOT_IMP_NET_LIQTO
                ,MONT_END2000
                ,MONT_END2006
                ,PC_REN
                ,IMP_PNL
                ,IMPB_IRPEF
                ,IMPST_IRPEF
                ,DT_VLT
                ,DT_END_ISTR
                ,TP_RIMB
                ,SPE_RCS
                ,IB_LIQ
                ,TOT_IAS_ONER_PRVNT
                ,TOT_IAS_MGG_SIN
                ,TOT_IAS_RST_DPST
                ,IMP_ONER_LIQ
                ,COMPON_TAX_RIMB
                ,TP_MEZ_PAG
                ,IMP_EXCONTR
                ,IMP_INTR_RIT_PAG
                ,BNS_NON_GODUTO
                ,CNBT_INPSTFM
                ,IMPST_DA_RIMB
                ,IMPB_IS
                ,TAX_SEP
                ,IMPB_TAX_SEP
                ,IMPB_INTR_SU_PREST
                ,ADDIZ_COMUN
                ,IMPB_ADDIZ_COMUN
                ,ADDIZ_REGION
                ,IMPB_ADDIZ_REGION
                ,MONT_DAL2007
                ,IMPB_CNBT_INPSTFM
                ,IMP_LRD_DA_RIMB
                ,IMP_DIR_DA_RIMB
                ,RIS_MAT
                ,RIS_SPE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TOT_IAS_PNL
                ,FL_EVE_GARTO
                ,IMP_REN_K1
                ,IMP_REN_K2
                ,IMP_REN_K3
                ,PC_REN_K1
                ,PC_REN_K2
                ,PC_REN_K3
                ,TP_CAUS_ANTIC
                ,IMP_LRD_LIQTO_RILT
                ,IMPST_APPL_RILT
                ,PC_RISC_PARZ
                ,IMPST_BOLLO_TOT_V
                ,IMPST_BOLLO_DETT_C
                ,IMPST_BOLLO_TOT_SW
                ,IMPST_BOLLO_TOT_AA
                ,IMPB_VIS_1382011
                ,IMPST_VIS_1382011
                ,IMPB_IS_1382011
                ,IMPST_SOST_1382011
                ,PC_ABB_TIT_STAT
                ,IMPB_BOLLO_DETT_C
                ,FL_PRE_COMP
                ,IMPB_VIS_662014
                ,IMPST_VIS_662014
                ,IMPB_IS_662014
                ,IMPST_SOST_662014
                ,PC_ABB_TS_662014
                ,IMP_LRD_CALC_CP
                ,COS_TUNNEL_USCITA
             INTO
                :LQU-ID-LIQ
               ,:LQU-ID-OGG
               ,:LQU-TP-OGG
               ,:LQU-ID-MOVI-CRZ
               ,:LQU-ID-MOVI-CHIU
                :IND-LQU-ID-MOVI-CHIU
               ,:LQU-DT-INI-EFF-DB
               ,:LQU-DT-END-EFF-DB
               ,:LQU-COD-COMP-ANIA
               ,:LQU-IB-OGG
                :IND-LQU-IB-OGG
               ,:LQU-TP-LIQ
               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
                :IND-LQU-DESC-CAU-EVE-SIN
               ,:LQU-COD-CAU-SIN
                :IND-LQU-COD-CAU-SIN
               ,:LQU-COD-SIN-CATSTRF
                :IND-LQU-COD-SIN-CATSTRF
               ,:LQU-DT-MOR-DB
                :IND-LQU-DT-MOR
               ,:LQU-DT-DEN-DB
                :IND-LQU-DT-DEN
               ,:LQU-DT-PERV-DEN-DB
                :IND-LQU-DT-PERV-DEN
               ,:LQU-DT-RICH-DB
                :IND-LQU-DT-RICH
               ,:LQU-TP-SIN
                :IND-LQU-TP-SIN
               ,:LQU-TP-RISC
                :IND-LQU-TP-RISC
               ,:LQU-TP-MET-RISC
                :IND-LQU-TP-MET-RISC
               ,:LQU-DT-LIQ-DB
                :IND-LQU-DT-LIQ
               ,:LQU-COD-DVS
                :IND-LQU-COD-DVS
               ,:LQU-TOT-IMP-LRD-LIQTO
                :IND-LQU-TOT-IMP-LRD-LIQTO
               ,:LQU-TOT-IMP-PREST
                :IND-LQU-TOT-IMP-PREST
               ,:LQU-TOT-IMP-INTR-PREST
                :IND-LQU-TOT-IMP-INTR-PREST
               ,:LQU-TOT-IMP-UTI
                :IND-LQU-TOT-IMP-UTI
               ,:LQU-TOT-IMP-RIT-TFR
                :IND-LQU-TOT-IMP-RIT-TFR
               ,:LQU-TOT-IMP-RIT-ACC
                :IND-LQU-TOT-IMP-RIT-ACC
               ,:LQU-TOT-IMP-RIT-VIS
                :IND-LQU-TOT-IMP-RIT-VIS
               ,:LQU-TOT-IMPB-TFR
                :IND-LQU-TOT-IMPB-TFR
               ,:LQU-TOT-IMPB-ACC
                :IND-LQU-TOT-IMPB-ACC
               ,:LQU-TOT-IMPB-VIS
                :IND-LQU-TOT-IMPB-VIS
               ,:LQU-TOT-IMP-RIMB
                :IND-LQU-TOT-IMP-RIMB
               ,:LQU-IMPB-IMPST-PRVR
                :IND-LQU-IMPB-IMPST-PRVR
               ,:LQU-IMPST-PRVR
                :IND-LQU-IMPST-PRVR
               ,:LQU-IMPB-IMPST-252
                :IND-LQU-IMPB-IMPST-252
               ,:LQU-IMPST-252
                :IND-LQU-IMPST-252
               ,:LQU-TOT-IMP-IS
                :IND-LQU-TOT-IMP-IS
               ,:LQU-IMP-DIR-LIQ
                :IND-LQU-IMP-DIR-LIQ
               ,:LQU-TOT-IMP-NET-LIQTO
                :IND-LQU-TOT-IMP-NET-LIQTO
               ,:LQU-MONT-END2000
                :IND-LQU-MONT-END2000
               ,:LQU-MONT-END2006
                :IND-LQU-MONT-END2006
               ,:LQU-PC-REN
                :IND-LQU-PC-REN
               ,:LQU-IMP-PNL
                :IND-LQU-IMP-PNL
               ,:LQU-IMPB-IRPEF
                :IND-LQU-IMPB-IRPEF
               ,:LQU-IMPST-IRPEF
                :IND-LQU-IMPST-IRPEF
               ,:LQU-DT-VLT-DB
                :IND-LQU-DT-VLT
               ,:LQU-DT-END-ISTR-DB
                :IND-LQU-DT-END-ISTR
               ,:LQU-TP-RIMB
               ,:LQU-SPE-RCS
                :IND-LQU-SPE-RCS
               ,:LQU-IB-LIQ
                :IND-LQU-IB-LIQ
               ,:LQU-TOT-IAS-ONER-PRVNT
                :IND-LQU-TOT-IAS-ONER-PRVNT
               ,:LQU-TOT-IAS-MGG-SIN
                :IND-LQU-TOT-IAS-MGG-SIN
               ,:LQU-TOT-IAS-RST-DPST
                :IND-LQU-TOT-IAS-RST-DPST
               ,:LQU-IMP-ONER-LIQ
                :IND-LQU-IMP-ONER-LIQ
               ,:LQU-COMPON-TAX-RIMB
                :IND-LQU-COMPON-TAX-RIMB
               ,:LQU-TP-MEZ-PAG
                :IND-LQU-TP-MEZ-PAG
               ,:LQU-IMP-EXCONTR
                :IND-LQU-IMP-EXCONTR
               ,:LQU-IMP-INTR-RIT-PAG
                :IND-LQU-IMP-INTR-RIT-PAG
               ,:LQU-BNS-NON-GODUTO
                :IND-LQU-BNS-NON-GODUTO
               ,:LQU-CNBT-INPSTFM
                :IND-LQU-CNBT-INPSTFM
               ,:LQU-IMPST-DA-RIMB
                :IND-LQU-IMPST-DA-RIMB
               ,:LQU-IMPB-IS
                :IND-LQU-IMPB-IS
               ,:LQU-TAX-SEP
                :IND-LQU-TAX-SEP
               ,:LQU-IMPB-TAX-SEP
                :IND-LQU-IMPB-TAX-SEP
               ,:LQU-IMPB-INTR-SU-PREST
                :IND-LQU-IMPB-INTR-SU-PREST
               ,:LQU-ADDIZ-COMUN
                :IND-LQU-ADDIZ-COMUN
               ,:LQU-IMPB-ADDIZ-COMUN
                :IND-LQU-IMPB-ADDIZ-COMUN
               ,:LQU-ADDIZ-REGION
                :IND-LQU-ADDIZ-REGION
               ,:LQU-IMPB-ADDIZ-REGION
                :IND-LQU-IMPB-ADDIZ-REGION
               ,:LQU-MONT-DAL2007
                :IND-LQU-MONT-DAL2007
               ,:LQU-IMPB-CNBT-INPSTFM
                :IND-LQU-IMPB-CNBT-INPSTFM
               ,:LQU-IMP-LRD-DA-RIMB
                :IND-LQU-IMP-LRD-DA-RIMB
               ,:LQU-IMP-DIR-DA-RIMB
                :IND-LQU-IMP-DIR-DA-RIMB
               ,:LQU-RIS-MAT
                :IND-LQU-RIS-MAT
               ,:LQU-RIS-SPE
                :IND-LQU-RIS-SPE
               ,:LQU-DS-RIGA
               ,:LQU-DS-OPER-SQL
               ,:LQU-DS-VER
               ,:LQU-DS-TS-INI-CPTZ
               ,:LQU-DS-TS-END-CPTZ
               ,:LQU-DS-UTENTE
               ,:LQU-DS-STATO-ELAB
               ,:LQU-TOT-IAS-PNL
                :IND-LQU-TOT-IAS-PNL
               ,:LQU-FL-EVE-GARTO
                :IND-LQU-FL-EVE-GARTO
               ,:LQU-IMP-REN-K1
                :IND-LQU-IMP-REN-K1
               ,:LQU-IMP-REN-K2
                :IND-LQU-IMP-REN-K2
               ,:LQU-IMP-REN-K3
                :IND-LQU-IMP-REN-K3
               ,:LQU-PC-REN-K1
                :IND-LQU-PC-REN-K1
               ,:LQU-PC-REN-K2
                :IND-LQU-PC-REN-K2
               ,:LQU-PC-REN-K3
                :IND-LQU-PC-REN-K3
               ,:LQU-TP-CAUS-ANTIC
                :IND-LQU-TP-CAUS-ANTIC
               ,:LQU-IMP-LRD-LIQTO-RILT
                :IND-LQU-IMP-LRD-LIQTO-RILT
               ,:LQU-IMPST-APPL-RILT
                :IND-LQU-IMPST-APPL-RILT
               ,:LQU-PC-RISC-PARZ
                :IND-LQU-PC-RISC-PARZ
               ,:LQU-IMPST-BOLLO-TOT-V
                :IND-LQU-IMPST-BOLLO-TOT-V
               ,:LQU-IMPST-BOLLO-DETT-C
                :IND-LQU-IMPST-BOLLO-DETT-C
               ,:LQU-IMPST-BOLLO-TOT-SW
                :IND-LQU-IMPST-BOLLO-TOT-SW
               ,:LQU-IMPST-BOLLO-TOT-AA
                :IND-LQU-IMPST-BOLLO-TOT-AA
               ,:LQU-IMPB-VIS-1382011
                :IND-LQU-IMPB-VIS-1382011
               ,:LQU-IMPST-VIS-1382011
                :IND-LQU-IMPST-VIS-1382011
               ,:LQU-IMPB-IS-1382011
                :IND-LQU-IMPB-IS-1382011
               ,:LQU-IMPST-SOST-1382011
                :IND-LQU-IMPST-SOST-1382011
               ,:LQU-PC-ABB-TIT-STAT
                :IND-LQU-PC-ABB-TIT-STAT
               ,:LQU-IMPB-BOLLO-DETT-C
                :IND-LQU-IMPB-BOLLO-DETT-C
               ,:LQU-FL-PRE-COMP
                :IND-LQU-FL-PRE-COMP
               ,:LQU-IMPB-VIS-662014
                :IND-LQU-IMPB-VIS-662014
               ,:LQU-IMPST-VIS-662014
                :IND-LQU-IMPST-VIS-662014
               ,:LQU-IMPB-IS-662014
                :IND-LQU-IMPB-IS-662014
               ,:LQU-IMPST-SOST-662014
                :IND-LQU-IMPST-SOST-662014
               ,:LQU-PC-ABB-TS-662014
                :IND-LQU-PC-ABB-TS-662014
               ,:LQU-IMP-LRD-CALC-CP
                :IND-LQU-IMP-LRD-CALC-CP
               ,:LQU-COS-TUNNEL-USCITA
                :IND-LQU-COS-TUNNEL-USCITA
             FROM LIQ
             WHERE     ID_LIQ = :LQU-ID-LIQ
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IBO-CPZ-LQU CURSOR FOR
              SELECT
                     ID_LIQ
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,IB_OGG
                    ,TP_LIQ
                    ,DESC_CAU_EVE_SIN
                    ,COD_CAU_SIN
                    ,COD_SIN_CATSTRF
                    ,DT_MOR
                    ,DT_DEN
                    ,DT_PERV_DEN
                    ,DT_RICH
                    ,TP_SIN
                    ,TP_RISC
                    ,TP_MET_RISC
                    ,DT_LIQ
                    ,COD_DVS
                    ,TOT_IMP_LRD_LIQTO
                    ,TOT_IMP_PREST
                    ,TOT_IMP_INTR_PREST
                    ,TOT_IMP_UTI
                    ,TOT_IMP_RIT_TFR
                    ,TOT_IMP_RIT_ACC
                    ,TOT_IMP_RIT_VIS
                    ,TOT_IMPB_TFR
                    ,TOT_IMPB_ACC
                    ,TOT_IMPB_VIS
                    ,TOT_IMP_RIMB
                    ,IMPB_IMPST_PRVR
                    ,IMPST_PRVR
                    ,IMPB_IMPST_252
                    ,IMPST_252
                    ,TOT_IMP_IS
                    ,IMP_DIR_LIQ
                    ,TOT_IMP_NET_LIQTO
                    ,MONT_END2000
                    ,MONT_END2006
                    ,PC_REN
                    ,IMP_PNL
                    ,IMPB_IRPEF
                    ,IMPST_IRPEF
                    ,DT_VLT
                    ,DT_END_ISTR
                    ,TP_RIMB
                    ,SPE_RCS
                    ,IB_LIQ
                    ,TOT_IAS_ONER_PRVNT
                    ,TOT_IAS_MGG_SIN
                    ,TOT_IAS_RST_DPST
                    ,IMP_ONER_LIQ
                    ,COMPON_TAX_RIMB
                    ,TP_MEZ_PAG
                    ,IMP_EXCONTR
                    ,IMP_INTR_RIT_PAG
                    ,BNS_NON_GODUTO
                    ,CNBT_INPSTFM
                    ,IMPST_DA_RIMB
                    ,IMPB_IS
                    ,TAX_SEP
                    ,IMPB_TAX_SEP
                    ,IMPB_INTR_SU_PREST
                    ,ADDIZ_COMUN
                    ,IMPB_ADDIZ_COMUN
                    ,ADDIZ_REGION
                    ,IMPB_ADDIZ_REGION
                    ,MONT_DAL2007
                    ,IMPB_CNBT_INPSTFM
                    ,IMP_LRD_DA_RIMB
                    ,IMP_DIR_DA_RIMB
                    ,RIS_MAT
                    ,RIS_SPE
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,TOT_IAS_PNL
                    ,FL_EVE_GARTO
                    ,IMP_REN_K1
                    ,IMP_REN_K2
                    ,IMP_REN_K3
                    ,PC_REN_K1
                    ,PC_REN_K2
                    ,PC_REN_K3
                    ,TP_CAUS_ANTIC
                    ,IMP_LRD_LIQTO_RILT
                    ,IMPST_APPL_RILT
                    ,PC_RISC_PARZ
                    ,IMPST_BOLLO_TOT_V
                    ,IMPST_BOLLO_DETT_C
                    ,IMPST_BOLLO_TOT_SW
                    ,IMPST_BOLLO_TOT_AA
                    ,IMPB_VIS_1382011
                    ,IMPST_VIS_1382011
                    ,IMPB_IS_1382011
                    ,IMPST_SOST_1382011
                    ,PC_ABB_TIT_STAT
                    ,IMPB_BOLLO_DETT_C
                    ,FL_PRE_COMP
                    ,IMPB_VIS_662014
                    ,IMPST_VIS_662014
                    ,IMPB_IS_662014
                    ,IMPST_SOST_662014
                    ,PC_ABB_TS_662014
                    ,IMP_LRD_CALC_CP
                    ,COS_TUNNEL_USCITA
              FROM LIQ
              WHERE     IB_OGG = :LQU-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_LIQ ASC

           END-EXEC.

       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_LIQ
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,TP_LIQ
                ,DESC_CAU_EVE_SIN
                ,COD_CAU_SIN
                ,COD_SIN_CATSTRF
                ,DT_MOR
                ,DT_DEN
                ,DT_PERV_DEN
                ,DT_RICH
                ,TP_SIN
                ,TP_RISC
                ,TP_MET_RISC
                ,DT_LIQ
                ,COD_DVS
                ,TOT_IMP_LRD_LIQTO
                ,TOT_IMP_PREST
                ,TOT_IMP_INTR_PREST
                ,TOT_IMP_UTI
                ,TOT_IMP_RIT_TFR
                ,TOT_IMP_RIT_ACC
                ,TOT_IMP_RIT_VIS
                ,TOT_IMPB_TFR
                ,TOT_IMPB_ACC
                ,TOT_IMPB_VIS
                ,TOT_IMP_RIMB
                ,IMPB_IMPST_PRVR
                ,IMPST_PRVR
                ,IMPB_IMPST_252
                ,IMPST_252
                ,TOT_IMP_IS
                ,IMP_DIR_LIQ
                ,TOT_IMP_NET_LIQTO
                ,MONT_END2000
                ,MONT_END2006
                ,PC_REN
                ,IMP_PNL
                ,IMPB_IRPEF
                ,IMPST_IRPEF
                ,DT_VLT
                ,DT_END_ISTR
                ,TP_RIMB
                ,SPE_RCS
                ,IB_LIQ
                ,TOT_IAS_ONER_PRVNT
                ,TOT_IAS_MGG_SIN
                ,TOT_IAS_RST_DPST
                ,IMP_ONER_LIQ
                ,COMPON_TAX_RIMB
                ,TP_MEZ_PAG
                ,IMP_EXCONTR
                ,IMP_INTR_RIT_PAG
                ,BNS_NON_GODUTO
                ,CNBT_INPSTFM
                ,IMPST_DA_RIMB
                ,IMPB_IS
                ,TAX_SEP
                ,IMPB_TAX_SEP
                ,IMPB_INTR_SU_PREST
                ,ADDIZ_COMUN
                ,IMPB_ADDIZ_COMUN
                ,ADDIZ_REGION
                ,IMPB_ADDIZ_REGION
                ,MONT_DAL2007
                ,IMPB_CNBT_INPSTFM
                ,IMP_LRD_DA_RIMB
                ,IMP_DIR_DA_RIMB
                ,RIS_MAT
                ,RIS_SPE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TOT_IAS_PNL
                ,FL_EVE_GARTO
                ,IMP_REN_K1
                ,IMP_REN_K2
                ,IMP_REN_K3
                ,PC_REN_K1
                ,PC_REN_K2
                ,PC_REN_K3
                ,TP_CAUS_ANTIC
                ,IMP_LRD_LIQTO_RILT
                ,IMPST_APPL_RILT
                ,PC_RISC_PARZ
                ,IMPST_BOLLO_TOT_V
                ,IMPST_BOLLO_DETT_C
                ,IMPST_BOLLO_TOT_SW
                ,IMPST_BOLLO_TOT_AA
                ,IMPB_VIS_1382011
                ,IMPST_VIS_1382011
                ,IMPB_IS_1382011
                ,IMPST_SOST_1382011
                ,PC_ABB_TIT_STAT
                ,IMPB_BOLLO_DETT_C
                ,FL_PRE_COMP
                ,IMPB_VIS_662014
                ,IMPST_VIS_662014
                ,IMPB_IS_662014
                ,IMPST_SOST_662014
                ,PC_ABB_TS_662014
                ,IMP_LRD_CALC_CP
                ,COS_TUNNEL_USCITA
             INTO
                :LQU-ID-LIQ
               ,:LQU-ID-OGG
               ,:LQU-TP-OGG
               ,:LQU-ID-MOVI-CRZ
               ,:LQU-ID-MOVI-CHIU
                :IND-LQU-ID-MOVI-CHIU
               ,:LQU-DT-INI-EFF-DB
               ,:LQU-DT-END-EFF-DB
               ,:LQU-COD-COMP-ANIA
               ,:LQU-IB-OGG
                :IND-LQU-IB-OGG
               ,:LQU-TP-LIQ
               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
                :IND-LQU-DESC-CAU-EVE-SIN
               ,:LQU-COD-CAU-SIN
                :IND-LQU-COD-CAU-SIN
               ,:LQU-COD-SIN-CATSTRF
                :IND-LQU-COD-SIN-CATSTRF
               ,:LQU-DT-MOR-DB
                :IND-LQU-DT-MOR
               ,:LQU-DT-DEN-DB
                :IND-LQU-DT-DEN
               ,:LQU-DT-PERV-DEN-DB
                :IND-LQU-DT-PERV-DEN
               ,:LQU-DT-RICH-DB
                :IND-LQU-DT-RICH
               ,:LQU-TP-SIN
                :IND-LQU-TP-SIN
               ,:LQU-TP-RISC
                :IND-LQU-TP-RISC
               ,:LQU-TP-MET-RISC
                :IND-LQU-TP-MET-RISC
               ,:LQU-DT-LIQ-DB
                :IND-LQU-DT-LIQ
               ,:LQU-COD-DVS
                :IND-LQU-COD-DVS
               ,:LQU-TOT-IMP-LRD-LIQTO
                :IND-LQU-TOT-IMP-LRD-LIQTO
               ,:LQU-TOT-IMP-PREST
                :IND-LQU-TOT-IMP-PREST
               ,:LQU-TOT-IMP-INTR-PREST
                :IND-LQU-TOT-IMP-INTR-PREST
               ,:LQU-TOT-IMP-UTI
                :IND-LQU-TOT-IMP-UTI
               ,:LQU-TOT-IMP-RIT-TFR
                :IND-LQU-TOT-IMP-RIT-TFR
               ,:LQU-TOT-IMP-RIT-ACC
                :IND-LQU-TOT-IMP-RIT-ACC
               ,:LQU-TOT-IMP-RIT-VIS
                :IND-LQU-TOT-IMP-RIT-VIS
               ,:LQU-TOT-IMPB-TFR
                :IND-LQU-TOT-IMPB-TFR
               ,:LQU-TOT-IMPB-ACC
                :IND-LQU-TOT-IMPB-ACC
               ,:LQU-TOT-IMPB-VIS
                :IND-LQU-TOT-IMPB-VIS
               ,:LQU-TOT-IMP-RIMB
                :IND-LQU-TOT-IMP-RIMB
               ,:LQU-IMPB-IMPST-PRVR
                :IND-LQU-IMPB-IMPST-PRVR
               ,:LQU-IMPST-PRVR
                :IND-LQU-IMPST-PRVR
               ,:LQU-IMPB-IMPST-252
                :IND-LQU-IMPB-IMPST-252
               ,:LQU-IMPST-252
                :IND-LQU-IMPST-252
               ,:LQU-TOT-IMP-IS
                :IND-LQU-TOT-IMP-IS
               ,:LQU-IMP-DIR-LIQ
                :IND-LQU-IMP-DIR-LIQ
               ,:LQU-TOT-IMP-NET-LIQTO
                :IND-LQU-TOT-IMP-NET-LIQTO
               ,:LQU-MONT-END2000
                :IND-LQU-MONT-END2000
               ,:LQU-MONT-END2006
                :IND-LQU-MONT-END2006
               ,:LQU-PC-REN
                :IND-LQU-PC-REN
               ,:LQU-IMP-PNL
                :IND-LQU-IMP-PNL
               ,:LQU-IMPB-IRPEF
                :IND-LQU-IMPB-IRPEF
               ,:LQU-IMPST-IRPEF
                :IND-LQU-IMPST-IRPEF
               ,:LQU-DT-VLT-DB
                :IND-LQU-DT-VLT
               ,:LQU-DT-END-ISTR-DB
                :IND-LQU-DT-END-ISTR
               ,:LQU-TP-RIMB
               ,:LQU-SPE-RCS
                :IND-LQU-SPE-RCS
               ,:LQU-IB-LIQ
                :IND-LQU-IB-LIQ
               ,:LQU-TOT-IAS-ONER-PRVNT
                :IND-LQU-TOT-IAS-ONER-PRVNT
               ,:LQU-TOT-IAS-MGG-SIN
                :IND-LQU-TOT-IAS-MGG-SIN
               ,:LQU-TOT-IAS-RST-DPST
                :IND-LQU-TOT-IAS-RST-DPST
               ,:LQU-IMP-ONER-LIQ
                :IND-LQU-IMP-ONER-LIQ
               ,:LQU-COMPON-TAX-RIMB
                :IND-LQU-COMPON-TAX-RIMB
               ,:LQU-TP-MEZ-PAG
                :IND-LQU-TP-MEZ-PAG
               ,:LQU-IMP-EXCONTR
                :IND-LQU-IMP-EXCONTR
               ,:LQU-IMP-INTR-RIT-PAG
                :IND-LQU-IMP-INTR-RIT-PAG
               ,:LQU-BNS-NON-GODUTO
                :IND-LQU-BNS-NON-GODUTO
               ,:LQU-CNBT-INPSTFM
                :IND-LQU-CNBT-INPSTFM
               ,:LQU-IMPST-DA-RIMB
                :IND-LQU-IMPST-DA-RIMB
               ,:LQU-IMPB-IS
                :IND-LQU-IMPB-IS
               ,:LQU-TAX-SEP
                :IND-LQU-TAX-SEP
               ,:LQU-IMPB-TAX-SEP
                :IND-LQU-IMPB-TAX-SEP
               ,:LQU-IMPB-INTR-SU-PREST
                :IND-LQU-IMPB-INTR-SU-PREST
               ,:LQU-ADDIZ-COMUN
                :IND-LQU-ADDIZ-COMUN
               ,:LQU-IMPB-ADDIZ-COMUN
                :IND-LQU-IMPB-ADDIZ-COMUN
               ,:LQU-ADDIZ-REGION
                :IND-LQU-ADDIZ-REGION
               ,:LQU-IMPB-ADDIZ-REGION
                :IND-LQU-IMPB-ADDIZ-REGION
               ,:LQU-MONT-DAL2007
                :IND-LQU-MONT-DAL2007
               ,:LQU-IMPB-CNBT-INPSTFM
                :IND-LQU-IMPB-CNBT-INPSTFM
               ,:LQU-IMP-LRD-DA-RIMB
                :IND-LQU-IMP-LRD-DA-RIMB
               ,:LQU-IMP-DIR-DA-RIMB
                :IND-LQU-IMP-DIR-DA-RIMB
               ,:LQU-RIS-MAT
                :IND-LQU-RIS-MAT
               ,:LQU-RIS-SPE
                :IND-LQU-RIS-SPE
               ,:LQU-DS-RIGA
               ,:LQU-DS-OPER-SQL
               ,:LQU-DS-VER
               ,:LQU-DS-TS-INI-CPTZ
               ,:LQU-DS-TS-END-CPTZ
               ,:LQU-DS-UTENTE
               ,:LQU-DS-STATO-ELAB
               ,:LQU-TOT-IAS-PNL
                :IND-LQU-TOT-IAS-PNL
               ,:LQU-FL-EVE-GARTO
                :IND-LQU-FL-EVE-GARTO
               ,:LQU-IMP-REN-K1
                :IND-LQU-IMP-REN-K1
               ,:LQU-IMP-REN-K2
                :IND-LQU-IMP-REN-K2
               ,:LQU-IMP-REN-K3
                :IND-LQU-IMP-REN-K3
               ,:LQU-PC-REN-K1
                :IND-LQU-PC-REN-K1
               ,:LQU-PC-REN-K2
                :IND-LQU-PC-REN-K2
               ,:LQU-PC-REN-K3
                :IND-LQU-PC-REN-K3
               ,:LQU-TP-CAUS-ANTIC
                :IND-LQU-TP-CAUS-ANTIC
               ,:LQU-IMP-LRD-LIQTO-RILT
                :IND-LQU-IMP-LRD-LIQTO-RILT
               ,:LQU-IMPST-APPL-RILT
                :IND-LQU-IMPST-APPL-RILT
               ,:LQU-PC-RISC-PARZ
                :IND-LQU-PC-RISC-PARZ
               ,:LQU-IMPST-BOLLO-TOT-V
                :IND-LQU-IMPST-BOLLO-TOT-V
               ,:LQU-IMPST-BOLLO-DETT-C
                :IND-LQU-IMPST-BOLLO-DETT-C
               ,:LQU-IMPST-BOLLO-TOT-SW
                :IND-LQU-IMPST-BOLLO-TOT-SW
               ,:LQU-IMPST-BOLLO-TOT-AA
                :IND-LQU-IMPST-BOLLO-TOT-AA
               ,:LQU-IMPB-VIS-1382011
                :IND-LQU-IMPB-VIS-1382011
               ,:LQU-IMPST-VIS-1382011
                :IND-LQU-IMPST-VIS-1382011
               ,:LQU-IMPB-IS-1382011
                :IND-LQU-IMPB-IS-1382011
               ,:LQU-IMPST-SOST-1382011
                :IND-LQU-IMPST-SOST-1382011
               ,:LQU-PC-ABB-TIT-STAT
                :IND-LQU-PC-ABB-TIT-STAT
               ,:LQU-IMPB-BOLLO-DETT-C
                :IND-LQU-IMPB-BOLLO-DETT-C
               ,:LQU-FL-PRE-COMP
                :IND-LQU-FL-PRE-COMP
               ,:LQU-IMPB-VIS-662014
                :IND-LQU-IMPB-VIS-662014
               ,:LQU-IMPST-VIS-662014
                :IND-LQU-IMPST-VIS-662014
               ,:LQU-IMPB-IS-662014
                :IND-LQU-IMPB-IS-662014
               ,:LQU-IMPST-SOST-662014
                :IND-LQU-IMPST-SOST-662014
               ,:LQU-PC-ABB-TS-662014
                :IND-LQU-PC-ABB-TS-662014
               ,:LQU-IMP-LRD-CALC-CP
                :IND-LQU-IMP-LRD-CALC-CP
               ,:LQU-COS-TUNNEL-USCITA
                :IND-LQU-COS-TUNNEL-USCITA
             FROM LIQ
             WHERE     IB_OGG = :LQU-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           EXEC SQL
                OPEN C-IBO-CPZ-LQU
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           EXEC SQL
                CLOSE C-IBO-CPZ-LQU
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           EXEC SQL
                FETCH C-IBO-CPZ-LQU
           INTO
                :LQU-ID-LIQ
               ,:LQU-ID-OGG
               ,:LQU-TP-OGG
               ,:LQU-ID-MOVI-CRZ
               ,:LQU-ID-MOVI-CHIU
                :IND-LQU-ID-MOVI-CHIU
               ,:LQU-DT-INI-EFF-DB
               ,:LQU-DT-END-EFF-DB
               ,:LQU-COD-COMP-ANIA
               ,:LQU-IB-OGG
                :IND-LQU-IB-OGG
               ,:LQU-TP-LIQ
               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
                :IND-LQU-DESC-CAU-EVE-SIN
               ,:LQU-COD-CAU-SIN
                :IND-LQU-COD-CAU-SIN
               ,:LQU-COD-SIN-CATSTRF
                :IND-LQU-COD-SIN-CATSTRF
               ,:LQU-DT-MOR-DB
                :IND-LQU-DT-MOR
               ,:LQU-DT-DEN-DB
                :IND-LQU-DT-DEN
               ,:LQU-DT-PERV-DEN-DB
                :IND-LQU-DT-PERV-DEN
               ,:LQU-DT-RICH-DB
                :IND-LQU-DT-RICH
               ,:LQU-TP-SIN
                :IND-LQU-TP-SIN
               ,:LQU-TP-RISC
                :IND-LQU-TP-RISC
               ,:LQU-TP-MET-RISC
                :IND-LQU-TP-MET-RISC
               ,:LQU-DT-LIQ-DB
                :IND-LQU-DT-LIQ
               ,:LQU-COD-DVS
                :IND-LQU-COD-DVS
               ,:LQU-TOT-IMP-LRD-LIQTO
                :IND-LQU-TOT-IMP-LRD-LIQTO
               ,:LQU-TOT-IMP-PREST
                :IND-LQU-TOT-IMP-PREST
               ,:LQU-TOT-IMP-INTR-PREST
                :IND-LQU-TOT-IMP-INTR-PREST
               ,:LQU-TOT-IMP-UTI
                :IND-LQU-TOT-IMP-UTI
               ,:LQU-TOT-IMP-RIT-TFR
                :IND-LQU-TOT-IMP-RIT-TFR
               ,:LQU-TOT-IMP-RIT-ACC
                :IND-LQU-TOT-IMP-RIT-ACC
               ,:LQU-TOT-IMP-RIT-VIS
                :IND-LQU-TOT-IMP-RIT-VIS
               ,:LQU-TOT-IMPB-TFR
                :IND-LQU-TOT-IMPB-TFR
               ,:LQU-TOT-IMPB-ACC
                :IND-LQU-TOT-IMPB-ACC
               ,:LQU-TOT-IMPB-VIS
                :IND-LQU-TOT-IMPB-VIS
               ,:LQU-TOT-IMP-RIMB
                :IND-LQU-TOT-IMP-RIMB
               ,:LQU-IMPB-IMPST-PRVR
                :IND-LQU-IMPB-IMPST-PRVR
               ,:LQU-IMPST-PRVR
                :IND-LQU-IMPST-PRVR
               ,:LQU-IMPB-IMPST-252
                :IND-LQU-IMPB-IMPST-252
               ,:LQU-IMPST-252
                :IND-LQU-IMPST-252
               ,:LQU-TOT-IMP-IS
                :IND-LQU-TOT-IMP-IS
               ,:LQU-IMP-DIR-LIQ
                :IND-LQU-IMP-DIR-LIQ
               ,:LQU-TOT-IMP-NET-LIQTO
                :IND-LQU-TOT-IMP-NET-LIQTO
               ,:LQU-MONT-END2000
                :IND-LQU-MONT-END2000
               ,:LQU-MONT-END2006
                :IND-LQU-MONT-END2006
               ,:LQU-PC-REN
                :IND-LQU-PC-REN
               ,:LQU-IMP-PNL
                :IND-LQU-IMP-PNL
               ,:LQU-IMPB-IRPEF
                :IND-LQU-IMPB-IRPEF
               ,:LQU-IMPST-IRPEF
                :IND-LQU-IMPST-IRPEF
               ,:LQU-DT-VLT-DB
                :IND-LQU-DT-VLT
               ,:LQU-DT-END-ISTR-DB
                :IND-LQU-DT-END-ISTR
               ,:LQU-TP-RIMB
               ,:LQU-SPE-RCS
                :IND-LQU-SPE-RCS
               ,:LQU-IB-LIQ
                :IND-LQU-IB-LIQ
               ,:LQU-TOT-IAS-ONER-PRVNT
                :IND-LQU-TOT-IAS-ONER-PRVNT
               ,:LQU-TOT-IAS-MGG-SIN
                :IND-LQU-TOT-IAS-MGG-SIN
               ,:LQU-TOT-IAS-RST-DPST
                :IND-LQU-TOT-IAS-RST-DPST
               ,:LQU-IMP-ONER-LIQ
                :IND-LQU-IMP-ONER-LIQ
               ,:LQU-COMPON-TAX-RIMB
                :IND-LQU-COMPON-TAX-RIMB
               ,:LQU-TP-MEZ-PAG
                :IND-LQU-TP-MEZ-PAG
               ,:LQU-IMP-EXCONTR
                :IND-LQU-IMP-EXCONTR
               ,:LQU-IMP-INTR-RIT-PAG
                :IND-LQU-IMP-INTR-RIT-PAG
               ,:LQU-BNS-NON-GODUTO
                :IND-LQU-BNS-NON-GODUTO
               ,:LQU-CNBT-INPSTFM
                :IND-LQU-CNBT-INPSTFM
               ,:LQU-IMPST-DA-RIMB
                :IND-LQU-IMPST-DA-RIMB
               ,:LQU-IMPB-IS
                :IND-LQU-IMPB-IS
               ,:LQU-TAX-SEP
                :IND-LQU-TAX-SEP
               ,:LQU-IMPB-TAX-SEP
                :IND-LQU-IMPB-TAX-SEP
               ,:LQU-IMPB-INTR-SU-PREST
                :IND-LQU-IMPB-INTR-SU-PREST
               ,:LQU-ADDIZ-COMUN
                :IND-LQU-ADDIZ-COMUN
               ,:LQU-IMPB-ADDIZ-COMUN
                :IND-LQU-IMPB-ADDIZ-COMUN
               ,:LQU-ADDIZ-REGION
                :IND-LQU-ADDIZ-REGION
               ,:LQU-IMPB-ADDIZ-REGION
                :IND-LQU-IMPB-ADDIZ-REGION
               ,:LQU-MONT-DAL2007
                :IND-LQU-MONT-DAL2007
               ,:LQU-IMPB-CNBT-INPSTFM
                :IND-LQU-IMPB-CNBT-INPSTFM
               ,:LQU-IMP-LRD-DA-RIMB
                :IND-LQU-IMP-LRD-DA-RIMB
               ,:LQU-IMP-DIR-DA-RIMB
                :IND-LQU-IMP-DIR-DA-RIMB
               ,:LQU-RIS-MAT
                :IND-LQU-RIS-MAT
               ,:LQU-RIS-SPE
                :IND-LQU-RIS-SPE
               ,:LQU-DS-RIGA
               ,:LQU-DS-OPER-SQL
               ,:LQU-DS-VER
               ,:LQU-DS-TS-INI-CPTZ
               ,:LQU-DS-TS-END-CPTZ
               ,:LQU-DS-UTENTE
               ,:LQU-DS-STATO-ELAB
               ,:LQU-TOT-IAS-PNL
                :IND-LQU-TOT-IAS-PNL
               ,:LQU-FL-EVE-GARTO
                :IND-LQU-FL-EVE-GARTO
               ,:LQU-IMP-REN-K1
                :IND-LQU-IMP-REN-K1
               ,:LQU-IMP-REN-K2
                :IND-LQU-IMP-REN-K2
               ,:LQU-IMP-REN-K3
                :IND-LQU-IMP-REN-K3
               ,:LQU-PC-REN-K1
                :IND-LQU-PC-REN-K1
               ,:LQU-PC-REN-K2
                :IND-LQU-PC-REN-K2
               ,:LQU-PC-REN-K3
                :IND-LQU-PC-REN-K3
               ,:LQU-TP-CAUS-ANTIC
                :IND-LQU-TP-CAUS-ANTIC
               ,:LQU-IMP-LRD-LIQTO-RILT
                :IND-LQU-IMP-LRD-LIQTO-RILT
               ,:LQU-IMPST-APPL-RILT
                :IND-LQU-IMPST-APPL-RILT
               ,:LQU-PC-RISC-PARZ
                :IND-LQU-PC-RISC-PARZ
               ,:LQU-IMPST-BOLLO-TOT-V
                :IND-LQU-IMPST-BOLLO-TOT-V
               ,:LQU-IMPST-BOLLO-DETT-C
                :IND-LQU-IMPST-BOLLO-DETT-C
               ,:LQU-IMPST-BOLLO-TOT-SW
                :IND-LQU-IMPST-BOLLO-TOT-SW
               ,:LQU-IMPST-BOLLO-TOT-AA
                :IND-LQU-IMPST-BOLLO-TOT-AA
               ,:LQU-IMPB-VIS-1382011
                :IND-LQU-IMPB-VIS-1382011
               ,:LQU-IMPST-VIS-1382011
                :IND-LQU-IMPST-VIS-1382011
               ,:LQU-IMPB-IS-1382011
                :IND-LQU-IMPB-IS-1382011
               ,:LQU-IMPST-SOST-1382011
                :IND-LQU-IMPST-SOST-1382011
               ,:LQU-PC-ABB-TIT-STAT
                :IND-LQU-PC-ABB-TIT-STAT
               ,:LQU-IMPB-BOLLO-DETT-C
                :IND-LQU-IMPB-BOLLO-DETT-C
               ,:LQU-FL-PRE-COMP
                :IND-LQU-FL-PRE-COMP
               ,:LQU-IMPB-VIS-662014
                :IND-LQU-IMPB-VIS-662014
               ,:LQU-IMPST-VIS-662014
                :IND-LQU-IMPST-VIS-662014
               ,:LQU-IMPB-IS-662014
                :IND-LQU-IMPB-IS-662014
               ,:LQU-IMPST-SOST-662014
                :IND-LQU-IMPST-SOST-662014
               ,:LQU-PC-ABB-TS-662014
                :IND-LQU-PC-ABB-TS-662014
               ,:LQU-IMP-LRD-CALC-CP
                :IND-LQU-IMP-LRD-CALC-CP
               ,:LQU-COS-TUNNEL-USCITA
                :IND-LQU-COS-TUNNEL-USCITA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B570-CLOSE-CURSOR-IBO-CPZ     THRU B570-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DCL-CUR-IBS-LIQ.
           EXEC SQL
                DECLARE C-IBS-CPZ-LQU-0 CURSOR FOR
              SELECT
                     ID_LIQ
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,IB_OGG
                    ,TP_LIQ
                    ,DESC_CAU_EVE_SIN
                    ,COD_CAU_SIN
                    ,COD_SIN_CATSTRF
                    ,DT_MOR
                    ,DT_DEN
                    ,DT_PERV_DEN
                    ,DT_RICH
                    ,TP_SIN
                    ,TP_RISC
                    ,TP_MET_RISC
                    ,DT_LIQ
                    ,COD_DVS
                    ,TOT_IMP_LRD_LIQTO
                    ,TOT_IMP_PREST
                    ,TOT_IMP_INTR_PREST
                    ,TOT_IMP_UTI
                    ,TOT_IMP_RIT_TFR
                    ,TOT_IMP_RIT_ACC
                    ,TOT_IMP_RIT_VIS
                    ,TOT_IMPB_TFR
                    ,TOT_IMPB_ACC
                    ,TOT_IMPB_VIS
                    ,TOT_IMP_RIMB
                    ,IMPB_IMPST_PRVR
                    ,IMPST_PRVR
                    ,IMPB_IMPST_252
                    ,IMPST_252
                    ,TOT_IMP_IS
                    ,IMP_DIR_LIQ
                    ,TOT_IMP_NET_LIQTO
                    ,MONT_END2000
                    ,MONT_END2006
                    ,PC_REN
                    ,IMP_PNL
                    ,IMPB_IRPEF
                    ,IMPST_IRPEF
                    ,DT_VLT
                    ,DT_END_ISTR
                    ,TP_RIMB
                    ,SPE_RCS
                    ,IB_LIQ
                    ,TOT_IAS_ONER_PRVNT
                    ,TOT_IAS_MGG_SIN
                    ,TOT_IAS_RST_DPST
                    ,IMP_ONER_LIQ
                    ,COMPON_TAX_RIMB
                    ,TP_MEZ_PAG
                    ,IMP_EXCONTR
                    ,IMP_INTR_RIT_PAG
                    ,BNS_NON_GODUTO
                    ,CNBT_INPSTFM
                    ,IMPST_DA_RIMB
                    ,IMPB_IS
                    ,TAX_SEP
                    ,IMPB_TAX_SEP
                    ,IMPB_INTR_SU_PREST
                    ,ADDIZ_COMUN
                    ,IMPB_ADDIZ_COMUN
                    ,ADDIZ_REGION
                    ,IMPB_ADDIZ_REGION
                    ,MONT_DAL2007
                    ,IMPB_CNBT_INPSTFM
                    ,IMP_LRD_DA_RIMB
                    ,IMP_DIR_DA_RIMB
                    ,RIS_MAT
                    ,RIS_SPE
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,TOT_IAS_PNL
                    ,FL_EVE_GARTO
                    ,IMP_REN_K1
                    ,IMP_REN_K2
                    ,IMP_REN_K3
                    ,PC_REN_K1
                    ,PC_REN_K2
                    ,PC_REN_K3
                    ,TP_CAUS_ANTIC
                    ,IMP_LRD_LIQTO_RILT
                    ,IMPST_APPL_RILT
                    ,PC_RISC_PARZ
                    ,IMPST_BOLLO_TOT_V
                    ,IMPST_BOLLO_DETT_C
                    ,IMPST_BOLLO_TOT_SW
                    ,IMPST_BOLLO_TOT_AA
                    ,IMPB_VIS_1382011
                    ,IMPST_VIS_1382011
                    ,IMPB_IS_1382011
                    ,IMPST_SOST_1382011
                    ,PC_ABB_TIT_STAT
                    ,IMPB_BOLLO_DETT_C
                    ,FL_PRE_COMP
                    ,IMPB_VIS_662014
                    ,IMPST_VIS_662014
                    ,IMPB_IS_662014
                    ,IMPST_SOST_662014
                    ,PC_ABB_TS_662014
                    ,IMP_LRD_CALC_CP
                    ,COS_TUNNEL_USCITA
              FROM LIQ
              WHERE     IB_LIQ = :LQU-IB-LIQ
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_LIQ ASC

           END-EXEC.
       B605-LIQ-EX.
           EXIT.


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF LQU-IB-LIQ NOT = HIGH-VALUES
               PERFORM B605-DCL-CUR-IBS-LIQ
                  THRU B605-LIQ-EX
           END-IF.

       B605-EX.
           EXIT.


       B610-SELECT-IBS-LIQ.
           EXEC SQL
             SELECT
                ID_LIQ
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,TP_LIQ
                ,DESC_CAU_EVE_SIN
                ,COD_CAU_SIN
                ,COD_SIN_CATSTRF
                ,DT_MOR
                ,DT_DEN
                ,DT_PERV_DEN
                ,DT_RICH
                ,TP_SIN
                ,TP_RISC
                ,TP_MET_RISC
                ,DT_LIQ
                ,COD_DVS
                ,TOT_IMP_LRD_LIQTO
                ,TOT_IMP_PREST
                ,TOT_IMP_INTR_PREST
                ,TOT_IMP_UTI
                ,TOT_IMP_RIT_TFR
                ,TOT_IMP_RIT_ACC
                ,TOT_IMP_RIT_VIS
                ,TOT_IMPB_TFR
                ,TOT_IMPB_ACC
                ,TOT_IMPB_VIS
                ,TOT_IMP_RIMB
                ,IMPB_IMPST_PRVR
                ,IMPST_PRVR
                ,IMPB_IMPST_252
                ,IMPST_252
                ,TOT_IMP_IS
                ,IMP_DIR_LIQ
                ,TOT_IMP_NET_LIQTO
                ,MONT_END2000
                ,MONT_END2006
                ,PC_REN
                ,IMP_PNL
                ,IMPB_IRPEF
                ,IMPST_IRPEF
                ,DT_VLT
                ,DT_END_ISTR
                ,TP_RIMB
                ,SPE_RCS
                ,IB_LIQ
                ,TOT_IAS_ONER_PRVNT
                ,TOT_IAS_MGG_SIN
                ,TOT_IAS_RST_DPST
                ,IMP_ONER_LIQ
                ,COMPON_TAX_RIMB
                ,TP_MEZ_PAG
                ,IMP_EXCONTR
                ,IMP_INTR_RIT_PAG
                ,BNS_NON_GODUTO
                ,CNBT_INPSTFM
                ,IMPST_DA_RIMB
                ,IMPB_IS
                ,TAX_SEP
                ,IMPB_TAX_SEP
                ,IMPB_INTR_SU_PREST
                ,ADDIZ_COMUN
                ,IMPB_ADDIZ_COMUN
                ,ADDIZ_REGION
                ,IMPB_ADDIZ_REGION
                ,MONT_DAL2007
                ,IMPB_CNBT_INPSTFM
                ,IMP_LRD_DA_RIMB
                ,IMP_DIR_DA_RIMB
                ,RIS_MAT
                ,RIS_SPE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TOT_IAS_PNL
                ,FL_EVE_GARTO
                ,IMP_REN_K1
                ,IMP_REN_K2
                ,IMP_REN_K3
                ,PC_REN_K1
                ,PC_REN_K2
                ,PC_REN_K3
                ,TP_CAUS_ANTIC
                ,IMP_LRD_LIQTO_RILT
                ,IMPST_APPL_RILT
                ,PC_RISC_PARZ
                ,IMPST_BOLLO_TOT_V
                ,IMPST_BOLLO_DETT_C
                ,IMPST_BOLLO_TOT_SW
                ,IMPST_BOLLO_TOT_AA
                ,IMPB_VIS_1382011
                ,IMPST_VIS_1382011
                ,IMPB_IS_1382011
                ,IMPST_SOST_1382011
                ,PC_ABB_TIT_STAT
                ,IMPB_BOLLO_DETT_C
                ,FL_PRE_COMP
                ,IMPB_VIS_662014
                ,IMPST_VIS_662014
                ,IMPB_IS_662014
                ,IMPST_SOST_662014
                ,PC_ABB_TS_662014
                ,IMP_LRD_CALC_CP
                ,COS_TUNNEL_USCITA
             INTO
                :LQU-ID-LIQ
               ,:LQU-ID-OGG
               ,:LQU-TP-OGG
               ,:LQU-ID-MOVI-CRZ
               ,:LQU-ID-MOVI-CHIU
                :IND-LQU-ID-MOVI-CHIU
               ,:LQU-DT-INI-EFF-DB
               ,:LQU-DT-END-EFF-DB
               ,:LQU-COD-COMP-ANIA
               ,:LQU-IB-OGG
                :IND-LQU-IB-OGG
               ,:LQU-TP-LIQ
               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
                :IND-LQU-DESC-CAU-EVE-SIN
               ,:LQU-COD-CAU-SIN
                :IND-LQU-COD-CAU-SIN
               ,:LQU-COD-SIN-CATSTRF
                :IND-LQU-COD-SIN-CATSTRF
               ,:LQU-DT-MOR-DB
                :IND-LQU-DT-MOR
               ,:LQU-DT-DEN-DB
                :IND-LQU-DT-DEN
               ,:LQU-DT-PERV-DEN-DB
                :IND-LQU-DT-PERV-DEN
               ,:LQU-DT-RICH-DB
                :IND-LQU-DT-RICH
               ,:LQU-TP-SIN
                :IND-LQU-TP-SIN
               ,:LQU-TP-RISC
                :IND-LQU-TP-RISC
               ,:LQU-TP-MET-RISC
                :IND-LQU-TP-MET-RISC
               ,:LQU-DT-LIQ-DB
                :IND-LQU-DT-LIQ
               ,:LQU-COD-DVS
                :IND-LQU-COD-DVS
               ,:LQU-TOT-IMP-LRD-LIQTO
                :IND-LQU-TOT-IMP-LRD-LIQTO
               ,:LQU-TOT-IMP-PREST
                :IND-LQU-TOT-IMP-PREST
               ,:LQU-TOT-IMP-INTR-PREST
                :IND-LQU-TOT-IMP-INTR-PREST
               ,:LQU-TOT-IMP-UTI
                :IND-LQU-TOT-IMP-UTI
               ,:LQU-TOT-IMP-RIT-TFR
                :IND-LQU-TOT-IMP-RIT-TFR
               ,:LQU-TOT-IMP-RIT-ACC
                :IND-LQU-TOT-IMP-RIT-ACC
               ,:LQU-TOT-IMP-RIT-VIS
                :IND-LQU-TOT-IMP-RIT-VIS
               ,:LQU-TOT-IMPB-TFR
                :IND-LQU-TOT-IMPB-TFR
               ,:LQU-TOT-IMPB-ACC
                :IND-LQU-TOT-IMPB-ACC
               ,:LQU-TOT-IMPB-VIS
                :IND-LQU-TOT-IMPB-VIS
               ,:LQU-TOT-IMP-RIMB
                :IND-LQU-TOT-IMP-RIMB
               ,:LQU-IMPB-IMPST-PRVR
                :IND-LQU-IMPB-IMPST-PRVR
               ,:LQU-IMPST-PRVR
                :IND-LQU-IMPST-PRVR
               ,:LQU-IMPB-IMPST-252
                :IND-LQU-IMPB-IMPST-252
               ,:LQU-IMPST-252
                :IND-LQU-IMPST-252
               ,:LQU-TOT-IMP-IS
                :IND-LQU-TOT-IMP-IS
               ,:LQU-IMP-DIR-LIQ
                :IND-LQU-IMP-DIR-LIQ
               ,:LQU-TOT-IMP-NET-LIQTO
                :IND-LQU-TOT-IMP-NET-LIQTO
               ,:LQU-MONT-END2000
                :IND-LQU-MONT-END2000
               ,:LQU-MONT-END2006
                :IND-LQU-MONT-END2006
               ,:LQU-PC-REN
                :IND-LQU-PC-REN
               ,:LQU-IMP-PNL
                :IND-LQU-IMP-PNL
               ,:LQU-IMPB-IRPEF
                :IND-LQU-IMPB-IRPEF
               ,:LQU-IMPST-IRPEF
                :IND-LQU-IMPST-IRPEF
               ,:LQU-DT-VLT-DB
                :IND-LQU-DT-VLT
               ,:LQU-DT-END-ISTR-DB
                :IND-LQU-DT-END-ISTR
               ,:LQU-TP-RIMB
               ,:LQU-SPE-RCS
                :IND-LQU-SPE-RCS
               ,:LQU-IB-LIQ
                :IND-LQU-IB-LIQ
               ,:LQU-TOT-IAS-ONER-PRVNT
                :IND-LQU-TOT-IAS-ONER-PRVNT
               ,:LQU-TOT-IAS-MGG-SIN
                :IND-LQU-TOT-IAS-MGG-SIN
               ,:LQU-TOT-IAS-RST-DPST
                :IND-LQU-TOT-IAS-RST-DPST
               ,:LQU-IMP-ONER-LIQ
                :IND-LQU-IMP-ONER-LIQ
               ,:LQU-COMPON-TAX-RIMB
                :IND-LQU-COMPON-TAX-RIMB
               ,:LQU-TP-MEZ-PAG
                :IND-LQU-TP-MEZ-PAG
               ,:LQU-IMP-EXCONTR
                :IND-LQU-IMP-EXCONTR
               ,:LQU-IMP-INTR-RIT-PAG
                :IND-LQU-IMP-INTR-RIT-PAG
               ,:LQU-BNS-NON-GODUTO
                :IND-LQU-BNS-NON-GODUTO
               ,:LQU-CNBT-INPSTFM
                :IND-LQU-CNBT-INPSTFM
               ,:LQU-IMPST-DA-RIMB
                :IND-LQU-IMPST-DA-RIMB
               ,:LQU-IMPB-IS
                :IND-LQU-IMPB-IS
               ,:LQU-TAX-SEP
                :IND-LQU-TAX-SEP
               ,:LQU-IMPB-TAX-SEP
                :IND-LQU-IMPB-TAX-SEP
               ,:LQU-IMPB-INTR-SU-PREST
                :IND-LQU-IMPB-INTR-SU-PREST
               ,:LQU-ADDIZ-COMUN
                :IND-LQU-ADDIZ-COMUN
               ,:LQU-IMPB-ADDIZ-COMUN
                :IND-LQU-IMPB-ADDIZ-COMUN
               ,:LQU-ADDIZ-REGION
                :IND-LQU-ADDIZ-REGION
               ,:LQU-IMPB-ADDIZ-REGION
                :IND-LQU-IMPB-ADDIZ-REGION
               ,:LQU-MONT-DAL2007
                :IND-LQU-MONT-DAL2007
               ,:LQU-IMPB-CNBT-INPSTFM
                :IND-LQU-IMPB-CNBT-INPSTFM
               ,:LQU-IMP-LRD-DA-RIMB
                :IND-LQU-IMP-LRD-DA-RIMB
               ,:LQU-IMP-DIR-DA-RIMB
                :IND-LQU-IMP-DIR-DA-RIMB
               ,:LQU-RIS-MAT
                :IND-LQU-RIS-MAT
               ,:LQU-RIS-SPE
                :IND-LQU-RIS-SPE
               ,:LQU-DS-RIGA
               ,:LQU-DS-OPER-SQL
               ,:LQU-DS-VER
               ,:LQU-DS-TS-INI-CPTZ
               ,:LQU-DS-TS-END-CPTZ
               ,:LQU-DS-UTENTE
               ,:LQU-DS-STATO-ELAB
               ,:LQU-TOT-IAS-PNL
                :IND-LQU-TOT-IAS-PNL
               ,:LQU-FL-EVE-GARTO
                :IND-LQU-FL-EVE-GARTO
               ,:LQU-IMP-REN-K1
                :IND-LQU-IMP-REN-K1
               ,:LQU-IMP-REN-K2
                :IND-LQU-IMP-REN-K2
               ,:LQU-IMP-REN-K3
                :IND-LQU-IMP-REN-K3
               ,:LQU-PC-REN-K1
                :IND-LQU-PC-REN-K1
               ,:LQU-PC-REN-K2
                :IND-LQU-PC-REN-K2
               ,:LQU-PC-REN-K3
                :IND-LQU-PC-REN-K3
               ,:LQU-TP-CAUS-ANTIC
                :IND-LQU-TP-CAUS-ANTIC
               ,:LQU-IMP-LRD-LIQTO-RILT
                :IND-LQU-IMP-LRD-LIQTO-RILT
               ,:LQU-IMPST-APPL-RILT
                :IND-LQU-IMPST-APPL-RILT
               ,:LQU-PC-RISC-PARZ
                :IND-LQU-PC-RISC-PARZ
               ,:LQU-IMPST-BOLLO-TOT-V
                :IND-LQU-IMPST-BOLLO-TOT-V
               ,:LQU-IMPST-BOLLO-DETT-C
                :IND-LQU-IMPST-BOLLO-DETT-C
               ,:LQU-IMPST-BOLLO-TOT-SW
                :IND-LQU-IMPST-BOLLO-TOT-SW
               ,:LQU-IMPST-BOLLO-TOT-AA
                :IND-LQU-IMPST-BOLLO-TOT-AA
               ,:LQU-IMPB-VIS-1382011
                :IND-LQU-IMPB-VIS-1382011
               ,:LQU-IMPST-VIS-1382011
                :IND-LQU-IMPST-VIS-1382011
               ,:LQU-IMPB-IS-1382011
                :IND-LQU-IMPB-IS-1382011
               ,:LQU-IMPST-SOST-1382011
                :IND-LQU-IMPST-SOST-1382011
               ,:LQU-PC-ABB-TIT-STAT
                :IND-LQU-PC-ABB-TIT-STAT
               ,:LQU-IMPB-BOLLO-DETT-C
                :IND-LQU-IMPB-BOLLO-DETT-C
               ,:LQU-FL-PRE-COMP
                :IND-LQU-FL-PRE-COMP
               ,:LQU-IMPB-VIS-662014
                :IND-LQU-IMPB-VIS-662014
               ,:LQU-IMPST-VIS-662014
                :IND-LQU-IMPST-VIS-662014
               ,:LQU-IMPB-IS-662014
                :IND-LQU-IMPB-IS-662014
               ,:LQU-IMPST-SOST-662014
                :IND-LQU-IMPST-SOST-662014
               ,:LQU-PC-ABB-TS-662014
                :IND-LQU-PC-ABB-TS-662014
               ,:LQU-IMP-LRD-CALC-CP
                :IND-LQU-IMP-LRD-CALC-CP
               ,:LQU-COS-TUNNEL-USCITA
                :IND-LQU-COS-TUNNEL-USCITA
             FROM LIQ
             WHERE     IB_LIQ = :LQU-IB-LIQ
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.
       B610-LIQ-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF LQU-IB-LIQ NOT = HIGH-VALUES
               PERFORM B610-SELECT-IBS-LIQ
                  THRU B610-LIQ-EX
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           IF LQU-IB-LIQ NOT = HIGH-VALUES
              EXEC SQL
                   OPEN C-IBS-CPZ-LQU-0
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           IF LQU-IB-LIQ NOT = HIGH-VALUES
              EXEC SQL
                   CLOSE C-IBS-CPZ-LQU-0
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FN-IBS-LIQ.
           EXEC SQL
                FETCH C-IBS-CPZ-LQU-0
           INTO
                :LQU-ID-LIQ
               ,:LQU-ID-OGG
               ,:LQU-TP-OGG
               ,:LQU-ID-MOVI-CRZ
               ,:LQU-ID-MOVI-CHIU
                :IND-LQU-ID-MOVI-CHIU
               ,:LQU-DT-INI-EFF-DB
               ,:LQU-DT-END-EFF-DB
               ,:LQU-COD-COMP-ANIA
               ,:LQU-IB-OGG
                :IND-LQU-IB-OGG
               ,:LQU-TP-LIQ
               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
                :IND-LQU-DESC-CAU-EVE-SIN
               ,:LQU-COD-CAU-SIN
                :IND-LQU-COD-CAU-SIN
               ,:LQU-COD-SIN-CATSTRF
                :IND-LQU-COD-SIN-CATSTRF
               ,:LQU-DT-MOR-DB
                :IND-LQU-DT-MOR
               ,:LQU-DT-DEN-DB
                :IND-LQU-DT-DEN
               ,:LQU-DT-PERV-DEN-DB
                :IND-LQU-DT-PERV-DEN
               ,:LQU-DT-RICH-DB
                :IND-LQU-DT-RICH
               ,:LQU-TP-SIN
                :IND-LQU-TP-SIN
               ,:LQU-TP-RISC
                :IND-LQU-TP-RISC
               ,:LQU-TP-MET-RISC
                :IND-LQU-TP-MET-RISC
               ,:LQU-DT-LIQ-DB
                :IND-LQU-DT-LIQ
               ,:LQU-COD-DVS
                :IND-LQU-COD-DVS
               ,:LQU-TOT-IMP-LRD-LIQTO
                :IND-LQU-TOT-IMP-LRD-LIQTO
               ,:LQU-TOT-IMP-PREST
                :IND-LQU-TOT-IMP-PREST
               ,:LQU-TOT-IMP-INTR-PREST
                :IND-LQU-TOT-IMP-INTR-PREST
               ,:LQU-TOT-IMP-UTI
                :IND-LQU-TOT-IMP-UTI
               ,:LQU-TOT-IMP-RIT-TFR
                :IND-LQU-TOT-IMP-RIT-TFR
               ,:LQU-TOT-IMP-RIT-ACC
                :IND-LQU-TOT-IMP-RIT-ACC
               ,:LQU-TOT-IMP-RIT-VIS
                :IND-LQU-TOT-IMP-RIT-VIS
               ,:LQU-TOT-IMPB-TFR
                :IND-LQU-TOT-IMPB-TFR
               ,:LQU-TOT-IMPB-ACC
                :IND-LQU-TOT-IMPB-ACC
               ,:LQU-TOT-IMPB-VIS
                :IND-LQU-TOT-IMPB-VIS
               ,:LQU-TOT-IMP-RIMB
                :IND-LQU-TOT-IMP-RIMB
               ,:LQU-IMPB-IMPST-PRVR
                :IND-LQU-IMPB-IMPST-PRVR
               ,:LQU-IMPST-PRVR
                :IND-LQU-IMPST-PRVR
               ,:LQU-IMPB-IMPST-252
                :IND-LQU-IMPB-IMPST-252
               ,:LQU-IMPST-252
                :IND-LQU-IMPST-252
               ,:LQU-TOT-IMP-IS
                :IND-LQU-TOT-IMP-IS
               ,:LQU-IMP-DIR-LIQ
                :IND-LQU-IMP-DIR-LIQ
               ,:LQU-TOT-IMP-NET-LIQTO
                :IND-LQU-TOT-IMP-NET-LIQTO
               ,:LQU-MONT-END2000
                :IND-LQU-MONT-END2000
               ,:LQU-MONT-END2006
                :IND-LQU-MONT-END2006
               ,:LQU-PC-REN
                :IND-LQU-PC-REN
               ,:LQU-IMP-PNL
                :IND-LQU-IMP-PNL
               ,:LQU-IMPB-IRPEF
                :IND-LQU-IMPB-IRPEF
               ,:LQU-IMPST-IRPEF
                :IND-LQU-IMPST-IRPEF
               ,:LQU-DT-VLT-DB
                :IND-LQU-DT-VLT
               ,:LQU-DT-END-ISTR-DB
                :IND-LQU-DT-END-ISTR
               ,:LQU-TP-RIMB
               ,:LQU-SPE-RCS
                :IND-LQU-SPE-RCS
               ,:LQU-IB-LIQ
                :IND-LQU-IB-LIQ
               ,:LQU-TOT-IAS-ONER-PRVNT
                :IND-LQU-TOT-IAS-ONER-PRVNT
               ,:LQU-TOT-IAS-MGG-SIN
                :IND-LQU-TOT-IAS-MGG-SIN
               ,:LQU-TOT-IAS-RST-DPST
                :IND-LQU-TOT-IAS-RST-DPST
               ,:LQU-IMP-ONER-LIQ
                :IND-LQU-IMP-ONER-LIQ
               ,:LQU-COMPON-TAX-RIMB
                :IND-LQU-COMPON-TAX-RIMB
               ,:LQU-TP-MEZ-PAG
                :IND-LQU-TP-MEZ-PAG
               ,:LQU-IMP-EXCONTR
                :IND-LQU-IMP-EXCONTR
               ,:LQU-IMP-INTR-RIT-PAG
                :IND-LQU-IMP-INTR-RIT-PAG
               ,:LQU-BNS-NON-GODUTO
                :IND-LQU-BNS-NON-GODUTO
               ,:LQU-CNBT-INPSTFM
                :IND-LQU-CNBT-INPSTFM
               ,:LQU-IMPST-DA-RIMB
                :IND-LQU-IMPST-DA-RIMB
               ,:LQU-IMPB-IS
                :IND-LQU-IMPB-IS
               ,:LQU-TAX-SEP
                :IND-LQU-TAX-SEP
               ,:LQU-IMPB-TAX-SEP
                :IND-LQU-IMPB-TAX-SEP
               ,:LQU-IMPB-INTR-SU-PREST
                :IND-LQU-IMPB-INTR-SU-PREST
               ,:LQU-ADDIZ-COMUN
                :IND-LQU-ADDIZ-COMUN
               ,:LQU-IMPB-ADDIZ-COMUN
                :IND-LQU-IMPB-ADDIZ-COMUN
               ,:LQU-ADDIZ-REGION
                :IND-LQU-ADDIZ-REGION
               ,:LQU-IMPB-ADDIZ-REGION
                :IND-LQU-IMPB-ADDIZ-REGION
               ,:LQU-MONT-DAL2007
                :IND-LQU-MONT-DAL2007
               ,:LQU-IMPB-CNBT-INPSTFM
                :IND-LQU-IMPB-CNBT-INPSTFM
               ,:LQU-IMP-LRD-DA-RIMB
                :IND-LQU-IMP-LRD-DA-RIMB
               ,:LQU-IMP-DIR-DA-RIMB
                :IND-LQU-IMP-DIR-DA-RIMB
               ,:LQU-RIS-MAT
                :IND-LQU-RIS-MAT
               ,:LQU-RIS-SPE
                :IND-LQU-RIS-SPE
               ,:LQU-DS-RIGA
               ,:LQU-DS-OPER-SQL
               ,:LQU-DS-VER
               ,:LQU-DS-TS-INI-CPTZ
               ,:LQU-DS-TS-END-CPTZ
               ,:LQU-DS-UTENTE
               ,:LQU-DS-STATO-ELAB
               ,:LQU-TOT-IAS-PNL
                :IND-LQU-TOT-IAS-PNL
               ,:LQU-FL-EVE-GARTO
                :IND-LQU-FL-EVE-GARTO
               ,:LQU-IMP-REN-K1
                :IND-LQU-IMP-REN-K1
               ,:LQU-IMP-REN-K2
                :IND-LQU-IMP-REN-K2
               ,:LQU-IMP-REN-K3
                :IND-LQU-IMP-REN-K3
               ,:LQU-PC-REN-K1
                :IND-LQU-PC-REN-K1
               ,:LQU-PC-REN-K2
                :IND-LQU-PC-REN-K2
               ,:LQU-PC-REN-K3
                :IND-LQU-PC-REN-K3
               ,:LQU-TP-CAUS-ANTIC
                :IND-LQU-TP-CAUS-ANTIC
               ,:LQU-IMP-LRD-LIQTO-RILT
                :IND-LQU-IMP-LRD-LIQTO-RILT
               ,:LQU-IMPST-APPL-RILT
                :IND-LQU-IMPST-APPL-RILT
               ,:LQU-PC-RISC-PARZ
                :IND-LQU-PC-RISC-PARZ
               ,:LQU-IMPST-BOLLO-TOT-V
                :IND-LQU-IMPST-BOLLO-TOT-V
               ,:LQU-IMPST-BOLLO-DETT-C
                :IND-LQU-IMPST-BOLLO-DETT-C
               ,:LQU-IMPST-BOLLO-TOT-SW
                :IND-LQU-IMPST-BOLLO-TOT-SW
               ,:LQU-IMPST-BOLLO-TOT-AA
                :IND-LQU-IMPST-BOLLO-TOT-AA
               ,:LQU-IMPB-VIS-1382011
                :IND-LQU-IMPB-VIS-1382011
               ,:LQU-IMPST-VIS-1382011
                :IND-LQU-IMPST-VIS-1382011
               ,:LQU-IMPB-IS-1382011
                :IND-LQU-IMPB-IS-1382011
               ,:LQU-IMPST-SOST-1382011
                :IND-LQU-IMPST-SOST-1382011
               ,:LQU-PC-ABB-TIT-STAT
                :IND-LQU-PC-ABB-TIT-STAT
               ,:LQU-IMPB-BOLLO-DETT-C
                :IND-LQU-IMPB-BOLLO-DETT-C
               ,:LQU-FL-PRE-COMP
                :IND-LQU-FL-PRE-COMP
               ,:LQU-IMPB-VIS-662014
                :IND-LQU-IMPB-VIS-662014
               ,:LQU-IMPST-VIS-662014
                :IND-LQU-IMPST-VIS-662014
               ,:LQU-IMPB-IS-662014
                :IND-LQU-IMPB-IS-662014
               ,:LQU-IMPST-SOST-662014
                :IND-LQU-IMPST-SOST-662014
               ,:LQU-PC-ABB-TS-662014
                :IND-LQU-PC-ABB-TS-662014
               ,:LQU-IMP-LRD-CALC-CP
                :IND-LQU-IMP-LRD-CALC-CP
               ,:LQU-COS-TUNNEL-USCITA
                :IND-LQU-COS-TUNNEL-USCITA
           END-EXEC.
       B690-LIQ-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           IF LQU-IB-LIQ NOT = HIGH-VALUES
               PERFORM B690-FN-IBS-LIQ
                  THRU B690-LIQ-EX
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B670-CLOSE-CURSOR-IBS-CPZ     THRU B670-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDO-CPZ-LQU CURSOR FOR
              SELECT
                     ID_LIQ
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,IB_OGG
                    ,TP_LIQ
                    ,DESC_CAU_EVE_SIN
                    ,COD_CAU_SIN
                    ,COD_SIN_CATSTRF
                    ,DT_MOR
                    ,DT_DEN
                    ,DT_PERV_DEN
                    ,DT_RICH
                    ,TP_SIN
                    ,TP_RISC
                    ,TP_MET_RISC
                    ,DT_LIQ
                    ,COD_DVS
                    ,TOT_IMP_LRD_LIQTO
                    ,TOT_IMP_PREST
                    ,TOT_IMP_INTR_PREST
                    ,TOT_IMP_UTI
                    ,TOT_IMP_RIT_TFR
                    ,TOT_IMP_RIT_ACC
                    ,TOT_IMP_RIT_VIS
                    ,TOT_IMPB_TFR
                    ,TOT_IMPB_ACC
                    ,TOT_IMPB_VIS
                    ,TOT_IMP_RIMB
                    ,IMPB_IMPST_PRVR
                    ,IMPST_PRVR
                    ,IMPB_IMPST_252
                    ,IMPST_252
                    ,TOT_IMP_IS
                    ,IMP_DIR_LIQ
                    ,TOT_IMP_NET_LIQTO
                    ,MONT_END2000
                    ,MONT_END2006
                    ,PC_REN
                    ,IMP_PNL
                    ,IMPB_IRPEF
                    ,IMPST_IRPEF
                    ,DT_VLT
                    ,DT_END_ISTR
                    ,TP_RIMB
                    ,SPE_RCS
                    ,IB_LIQ
                    ,TOT_IAS_ONER_PRVNT
                    ,TOT_IAS_MGG_SIN
                    ,TOT_IAS_RST_DPST
                    ,IMP_ONER_LIQ
                    ,COMPON_TAX_RIMB
                    ,TP_MEZ_PAG
                    ,IMP_EXCONTR
                    ,IMP_INTR_RIT_PAG
                    ,BNS_NON_GODUTO
                    ,CNBT_INPSTFM
                    ,IMPST_DA_RIMB
                    ,IMPB_IS
                    ,TAX_SEP
                    ,IMPB_TAX_SEP
                    ,IMPB_INTR_SU_PREST
                    ,ADDIZ_COMUN
                    ,IMPB_ADDIZ_COMUN
                    ,ADDIZ_REGION
                    ,IMPB_ADDIZ_REGION
                    ,MONT_DAL2007
                    ,IMPB_CNBT_INPSTFM
                    ,IMP_LRD_DA_RIMB
                    ,IMP_DIR_DA_RIMB
                    ,RIS_MAT
                    ,RIS_SPE
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,TOT_IAS_PNL
                    ,FL_EVE_GARTO
                    ,IMP_REN_K1
                    ,IMP_REN_K2
                    ,IMP_REN_K3
                    ,PC_REN_K1
                    ,PC_REN_K2
                    ,PC_REN_K3
                    ,TP_CAUS_ANTIC
                    ,IMP_LRD_LIQTO_RILT
                    ,IMPST_APPL_RILT
                    ,PC_RISC_PARZ
                    ,IMPST_BOLLO_TOT_V
                    ,IMPST_BOLLO_DETT_C
                    ,IMPST_BOLLO_TOT_SW
                    ,IMPST_BOLLO_TOT_AA
                    ,IMPB_VIS_1382011
                    ,IMPST_VIS_1382011
                    ,IMPB_IS_1382011
                    ,IMPST_SOST_1382011
                    ,PC_ABB_TIT_STAT
                    ,IMPB_BOLLO_DETT_C
                    ,FL_PRE_COMP
                    ,IMPB_VIS_662014
                    ,IMPST_VIS_662014
                    ,IMPB_IS_662014
                    ,IMPST_SOST_662014
                    ,PC_ABB_TS_662014
                    ,IMP_LRD_CALC_CP
                    ,COS_TUNNEL_USCITA
              FROM LIQ
              WHERE     ID_OGG = :LQU-ID-OGG
           AND TP_OGG = :LQU-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_LIQ ASC

           END-EXEC.

       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_LIQ
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,TP_LIQ
                ,DESC_CAU_EVE_SIN
                ,COD_CAU_SIN
                ,COD_SIN_CATSTRF
                ,DT_MOR
                ,DT_DEN
                ,DT_PERV_DEN
                ,DT_RICH
                ,TP_SIN
                ,TP_RISC
                ,TP_MET_RISC
                ,DT_LIQ
                ,COD_DVS
                ,TOT_IMP_LRD_LIQTO
                ,TOT_IMP_PREST
                ,TOT_IMP_INTR_PREST
                ,TOT_IMP_UTI
                ,TOT_IMP_RIT_TFR
                ,TOT_IMP_RIT_ACC
                ,TOT_IMP_RIT_VIS
                ,TOT_IMPB_TFR
                ,TOT_IMPB_ACC
                ,TOT_IMPB_VIS
                ,TOT_IMP_RIMB
                ,IMPB_IMPST_PRVR
                ,IMPST_PRVR
                ,IMPB_IMPST_252
                ,IMPST_252
                ,TOT_IMP_IS
                ,IMP_DIR_LIQ
                ,TOT_IMP_NET_LIQTO
                ,MONT_END2000
                ,MONT_END2006
                ,PC_REN
                ,IMP_PNL
                ,IMPB_IRPEF
                ,IMPST_IRPEF
                ,DT_VLT
                ,DT_END_ISTR
                ,TP_RIMB
                ,SPE_RCS
                ,IB_LIQ
                ,TOT_IAS_ONER_PRVNT
                ,TOT_IAS_MGG_SIN
                ,TOT_IAS_RST_DPST
                ,IMP_ONER_LIQ
                ,COMPON_TAX_RIMB
                ,TP_MEZ_PAG
                ,IMP_EXCONTR
                ,IMP_INTR_RIT_PAG
                ,BNS_NON_GODUTO
                ,CNBT_INPSTFM
                ,IMPST_DA_RIMB
                ,IMPB_IS
                ,TAX_SEP
                ,IMPB_TAX_SEP
                ,IMPB_INTR_SU_PREST
                ,ADDIZ_COMUN
                ,IMPB_ADDIZ_COMUN
                ,ADDIZ_REGION
                ,IMPB_ADDIZ_REGION
                ,MONT_DAL2007
                ,IMPB_CNBT_INPSTFM
                ,IMP_LRD_DA_RIMB
                ,IMP_DIR_DA_RIMB
                ,RIS_MAT
                ,RIS_SPE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TOT_IAS_PNL
                ,FL_EVE_GARTO
                ,IMP_REN_K1
                ,IMP_REN_K2
                ,IMP_REN_K3
                ,PC_REN_K1
                ,PC_REN_K2
                ,PC_REN_K3
                ,TP_CAUS_ANTIC
                ,IMP_LRD_LIQTO_RILT
                ,IMPST_APPL_RILT
                ,PC_RISC_PARZ
                ,IMPST_BOLLO_TOT_V
                ,IMPST_BOLLO_DETT_C
                ,IMPST_BOLLO_TOT_SW
                ,IMPST_BOLLO_TOT_AA
                ,IMPB_VIS_1382011
                ,IMPST_VIS_1382011
                ,IMPB_IS_1382011
                ,IMPST_SOST_1382011
                ,PC_ABB_TIT_STAT
                ,IMPB_BOLLO_DETT_C
                ,FL_PRE_COMP
                ,IMPB_VIS_662014
                ,IMPST_VIS_662014
                ,IMPB_IS_662014
                ,IMPST_SOST_662014
                ,PC_ABB_TS_662014
                ,IMP_LRD_CALC_CP
                ,COS_TUNNEL_USCITA
             INTO
                :LQU-ID-LIQ
               ,:LQU-ID-OGG
               ,:LQU-TP-OGG
               ,:LQU-ID-MOVI-CRZ
               ,:LQU-ID-MOVI-CHIU
                :IND-LQU-ID-MOVI-CHIU
               ,:LQU-DT-INI-EFF-DB
               ,:LQU-DT-END-EFF-DB
               ,:LQU-COD-COMP-ANIA
               ,:LQU-IB-OGG
                :IND-LQU-IB-OGG
               ,:LQU-TP-LIQ
               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
                :IND-LQU-DESC-CAU-EVE-SIN
               ,:LQU-COD-CAU-SIN
                :IND-LQU-COD-CAU-SIN
               ,:LQU-COD-SIN-CATSTRF
                :IND-LQU-COD-SIN-CATSTRF
               ,:LQU-DT-MOR-DB
                :IND-LQU-DT-MOR
               ,:LQU-DT-DEN-DB
                :IND-LQU-DT-DEN
               ,:LQU-DT-PERV-DEN-DB
                :IND-LQU-DT-PERV-DEN
               ,:LQU-DT-RICH-DB
                :IND-LQU-DT-RICH
               ,:LQU-TP-SIN
                :IND-LQU-TP-SIN
               ,:LQU-TP-RISC
                :IND-LQU-TP-RISC
               ,:LQU-TP-MET-RISC
                :IND-LQU-TP-MET-RISC
               ,:LQU-DT-LIQ-DB
                :IND-LQU-DT-LIQ
               ,:LQU-COD-DVS
                :IND-LQU-COD-DVS
               ,:LQU-TOT-IMP-LRD-LIQTO
                :IND-LQU-TOT-IMP-LRD-LIQTO
               ,:LQU-TOT-IMP-PREST
                :IND-LQU-TOT-IMP-PREST
               ,:LQU-TOT-IMP-INTR-PREST
                :IND-LQU-TOT-IMP-INTR-PREST
               ,:LQU-TOT-IMP-UTI
                :IND-LQU-TOT-IMP-UTI
               ,:LQU-TOT-IMP-RIT-TFR
                :IND-LQU-TOT-IMP-RIT-TFR
               ,:LQU-TOT-IMP-RIT-ACC
                :IND-LQU-TOT-IMP-RIT-ACC
               ,:LQU-TOT-IMP-RIT-VIS
                :IND-LQU-TOT-IMP-RIT-VIS
               ,:LQU-TOT-IMPB-TFR
                :IND-LQU-TOT-IMPB-TFR
               ,:LQU-TOT-IMPB-ACC
                :IND-LQU-TOT-IMPB-ACC
               ,:LQU-TOT-IMPB-VIS
                :IND-LQU-TOT-IMPB-VIS
               ,:LQU-TOT-IMP-RIMB
                :IND-LQU-TOT-IMP-RIMB
               ,:LQU-IMPB-IMPST-PRVR
                :IND-LQU-IMPB-IMPST-PRVR
               ,:LQU-IMPST-PRVR
                :IND-LQU-IMPST-PRVR
               ,:LQU-IMPB-IMPST-252
                :IND-LQU-IMPB-IMPST-252
               ,:LQU-IMPST-252
                :IND-LQU-IMPST-252
               ,:LQU-TOT-IMP-IS
                :IND-LQU-TOT-IMP-IS
               ,:LQU-IMP-DIR-LIQ
                :IND-LQU-IMP-DIR-LIQ
               ,:LQU-TOT-IMP-NET-LIQTO
                :IND-LQU-TOT-IMP-NET-LIQTO
               ,:LQU-MONT-END2000
                :IND-LQU-MONT-END2000
               ,:LQU-MONT-END2006
                :IND-LQU-MONT-END2006
               ,:LQU-PC-REN
                :IND-LQU-PC-REN
               ,:LQU-IMP-PNL
                :IND-LQU-IMP-PNL
               ,:LQU-IMPB-IRPEF
                :IND-LQU-IMPB-IRPEF
               ,:LQU-IMPST-IRPEF
                :IND-LQU-IMPST-IRPEF
               ,:LQU-DT-VLT-DB
                :IND-LQU-DT-VLT
               ,:LQU-DT-END-ISTR-DB
                :IND-LQU-DT-END-ISTR
               ,:LQU-TP-RIMB
               ,:LQU-SPE-RCS
                :IND-LQU-SPE-RCS
               ,:LQU-IB-LIQ
                :IND-LQU-IB-LIQ
               ,:LQU-TOT-IAS-ONER-PRVNT
                :IND-LQU-TOT-IAS-ONER-PRVNT
               ,:LQU-TOT-IAS-MGG-SIN
                :IND-LQU-TOT-IAS-MGG-SIN
               ,:LQU-TOT-IAS-RST-DPST
                :IND-LQU-TOT-IAS-RST-DPST
               ,:LQU-IMP-ONER-LIQ
                :IND-LQU-IMP-ONER-LIQ
               ,:LQU-COMPON-TAX-RIMB
                :IND-LQU-COMPON-TAX-RIMB
               ,:LQU-TP-MEZ-PAG
                :IND-LQU-TP-MEZ-PAG
               ,:LQU-IMP-EXCONTR
                :IND-LQU-IMP-EXCONTR
               ,:LQU-IMP-INTR-RIT-PAG
                :IND-LQU-IMP-INTR-RIT-PAG
               ,:LQU-BNS-NON-GODUTO
                :IND-LQU-BNS-NON-GODUTO
               ,:LQU-CNBT-INPSTFM
                :IND-LQU-CNBT-INPSTFM
               ,:LQU-IMPST-DA-RIMB
                :IND-LQU-IMPST-DA-RIMB
               ,:LQU-IMPB-IS
                :IND-LQU-IMPB-IS
               ,:LQU-TAX-SEP
                :IND-LQU-TAX-SEP
               ,:LQU-IMPB-TAX-SEP
                :IND-LQU-IMPB-TAX-SEP
               ,:LQU-IMPB-INTR-SU-PREST
                :IND-LQU-IMPB-INTR-SU-PREST
               ,:LQU-ADDIZ-COMUN
                :IND-LQU-ADDIZ-COMUN
               ,:LQU-IMPB-ADDIZ-COMUN
                :IND-LQU-IMPB-ADDIZ-COMUN
               ,:LQU-ADDIZ-REGION
                :IND-LQU-ADDIZ-REGION
               ,:LQU-IMPB-ADDIZ-REGION
                :IND-LQU-IMPB-ADDIZ-REGION
               ,:LQU-MONT-DAL2007
                :IND-LQU-MONT-DAL2007
               ,:LQU-IMPB-CNBT-INPSTFM
                :IND-LQU-IMPB-CNBT-INPSTFM
               ,:LQU-IMP-LRD-DA-RIMB
                :IND-LQU-IMP-LRD-DA-RIMB
               ,:LQU-IMP-DIR-DA-RIMB
                :IND-LQU-IMP-DIR-DA-RIMB
               ,:LQU-RIS-MAT
                :IND-LQU-RIS-MAT
               ,:LQU-RIS-SPE
                :IND-LQU-RIS-SPE
               ,:LQU-DS-RIGA
               ,:LQU-DS-OPER-SQL
               ,:LQU-DS-VER
               ,:LQU-DS-TS-INI-CPTZ
               ,:LQU-DS-TS-END-CPTZ
               ,:LQU-DS-UTENTE
               ,:LQU-DS-STATO-ELAB
               ,:LQU-TOT-IAS-PNL
                :IND-LQU-TOT-IAS-PNL
               ,:LQU-FL-EVE-GARTO
                :IND-LQU-FL-EVE-GARTO
               ,:LQU-IMP-REN-K1
                :IND-LQU-IMP-REN-K1
               ,:LQU-IMP-REN-K2
                :IND-LQU-IMP-REN-K2
               ,:LQU-IMP-REN-K3
                :IND-LQU-IMP-REN-K3
               ,:LQU-PC-REN-K1
                :IND-LQU-PC-REN-K1
               ,:LQU-PC-REN-K2
                :IND-LQU-PC-REN-K2
               ,:LQU-PC-REN-K3
                :IND-LQU-PC-REN-K3
               ,:LQU-TP-CAUS-ANTIC
                :IND-LQU-TP-CAUS-ANTIC
               ,:LQU-IMP-LRD-LIQTO-RILT
                :IND-LQU-IMP-LRD-LIQTO-RILT
               ,:LQU-IMPST-APPL-RILT
                :IND-LQU-IMPST-APPL-RILT
               ,:LQU-PC-RISC-PARZ
                :IND-LQU-PC-RISC-PARZ
               ,:LQU-IMPST-BOLLO-TOT-V
                :IND-LQU-IMPST-BOLLO-TOT-V
               ,:LQU-IMPST-BOLLO-DETT-C
                :IND-LQU-IMPST-BOLLO-DETT-C
               ,:LQU-IMPST-BOLLO-TOT-SW
                :IND-LQU-IMPST-BOLLO-TOT-SW
               ,:LQU-IMPST-BOLLO-TOT-AA
                :IND-LQU-IMPST-BOLLO-TOT-AA
               ,:LQU-IMPB-VIS-1382011
                :IND-LQU-IMPB-VIS-1382011
               ,:LQU-IMPST-VIS-1382011
                :IND-LQU-IMPST-VIS-1382011
               ,:LQU-IMPB-IS-1382011
                :IND-LQU-IMPB-IS-1382011
               ,:LQU-IMPST-SOST-1382011
                :IND-LQU-IMPST-SOST-1382011
               ,:LQU-PC-ABB-TIT-STAT
                :IND-LQU-PC-ABB-TIT-STAT
               ,:LQU-IMPB-BOLLO-DETT-C
                :IND-LQU-IMPB-BOLLO-DETT-C
               ,:LQU-FL-PRE-COMP
                :IND-LQU-FL-PRE-COMP
               ,:LQU-IMPB-VIS-662014
                :IND-LQU-IMPB-VIS-662014
               ,:LQU-IMPST-VIS-662014
                :IND-LQU-IMPST-VIS-662014
               ,:LQU-IMPB-IS-662014
                :IND-LQU-IMPB-IS-662014
               ,:LQU-IMPST-SOST-662014
                :IND-LQU-IMPST-SOST-662014
               ,:LQU-PC-ABB-TS-662014
                :IND-LQU-PC-ABB-TS-662014
               ,:LQU-IMP-LRD-CALC-CP
                :IND-LQU-IMP-LRD-CALC-CP
               ,:LQU-COS-TUNNEL-USCITA
                :IND-LQU-COS-TUNNEL-USCITA
             FROM LIQ
             WHERE     ID_OGG = :LQU-ID-OGG
                    AND TP_OGG = :LQU-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           EXEC SQL
                OPEN C-IDO-CPZ-LQU
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           EXEC SQL
                CLOSE C-IDO-CPZ-LQU
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           EXEC SQL
                FETCH C-IDO-CPZ-LQU
           INTO
                :LQU-ID-LIQ
               ,:LQU-ID-OGG
               ,:LQU-TP-OGG
               ,:LQU-ID-MOVI-CRZ
               ,:LQU-ID-MOVI-CHIU
                :IND-LQU-ID-MOVI-CHIU
               ,:LQU-DT-INI-EFF-DB
               ,:LQU-DT-END-EFF-DB
               ,:LQU-COD-COMP-ANIA
               ,:LQU-IB-OGG
                :IND-LQU-IB-OGG
               ,:LQU-TP-LIQ
               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
                :IND-LQU-DESC-CAU-EVE-SIN
               ,:LQU-COD-CAU-SIN
                :IND-LQU-COD-CAU-SIN
               ,:LQU-COD-SIN-CATSTRF
                :IND-LQU-COD-SIN-CATSTRF
               ,:LQU-DT-MOR-DB
                :IND-LQU-DT-MOR
               ,:LQU-DT-DEN-DB
                :IND-LQU-DT-DEN
               ,:LQU-DT-PERV-DEN-DB
                :IND-LQU-DT-PERV-DEN
               ,:LQU-DT-RICH-DB
                :IND-LQU-DT-RICH
               ,:LQU-TP-SIN
                :IND-LQU-TP-SIN
               ,:LQU-TP-RISC
                :IND-LQU-TP-RISC
               ,:LQU-TP-MET-RISC
                :IND-LQU-TP-MET-RISC
               ,:LQU-DT-LIQ-DB
                :IND-LQU-DT-LIQ
               ,:LQU-COD-DVS
                :IND-LQU-COD-DVS
               ,:LQU-TOT-IMP-LRD-LIQTO
                :IND-LQU-TOT-IMP-LRD-LIQTO
               ,:LQU-TOT-IMP-PREST
                :IND-LQU-TOT-IMP-PREST
               ,:LQU-TOT-IMP-INTR-PREST
                :IND-LQU-TOT-IMP-INTR-PREST
               ,:LQU-TOT-IMP-UTI
                :IND-LQU-TOT-IMP-UTI
               ,:LQU-TOT-IMP-RIT-TFR
                :IND-LQU-TOT-IMP-RIT-TFR
               ,:LQU-TOT-IMP-RIT-ACC
                :IND-LQU-TOT-IMP-RIT-ACC
               ,:LQU-TOT-IMP-RIT-VIS
                :IND-LQU-TOT-IMP-RIT-VIS
               ,:LQU-TOT-IMPB-TFR
                :IND-LQU-TOT-IMPB-TFR
               ,:LQU-TOT-IMPB-ACC
                :IND-LQU-TOT-IMPB-ACC
               ,:LQU-TOT-IMPB-VIS
                :IND-LQU-TOT-IMPB-VIS
               ,:LQU-TOT-IMP-RIMB
                :IND-LQU-TOT-IMP-RIMB
               ,:LQU-IMPB-IMPST-PRVR
                :IND-LQU-IMPB-IMPST-PRVR
               ,:LQU-IMPST-PRVR
                :IND-LQU-IMPST-PRVR
               ,:LQU-IMPB-IMPST-252
                :IND-LQU-IMPB-IMPST-252
               ,:LQU-IMPST-252
                :IND-LQU-IMPST-252
               ,:LQU-TOT-IMP-IS
                :IND-LQU-TOT-IMP-IS
               ,:LQU-IMP-DIR-LIQ
                :IND-LQU-IMP-DIR-LIQ
               ,:LQU-TOT-IMP-NET-LIQTO
                :IND-LQU-TOT-IMP-NET-LIQTO
               ,:LQU-MONT-END2000
                :IND-LQU-MONT-END2000
               ,:LQU-MONT-END2006
                :IND-LQU-MONT-END2006
               ,:LQU-PC-REN
                :IND-LQU-PC-REN
               ,:LQU-IMP-PNL
                :IND-LQU-IMP-PNL
               ,:LQU-IMPB-IRPEF
                :IND-LQU-IMPB-IRPEF
               ,:LQU-IMPST-IRPEF
                :IND-LQU-IMPST-IRPEF
               ,:LQU-DT-VLT-DB
                :IND-LQU-DT-VLT
               ,:LQU-DT-END-ISTR-DB
                :IND-LQU-DT-END-ISTR
               ,:LQU-TP-RIMB
               ,:LQU-SPE-RCS
                :IND-LQU-SPE-RCS
               ,:LQU-IB-LIQ
                :IND-LQU-IB-LIQ
               ,:LQU-TOT-IAS-ONER-PRVNT
                :IND-LQU-TOT-IAS-ONER-PRVNT
               ,:LQU-TOT-IAS-MGG-SIN
                :IND-LQU-TOT-IAS-MGG-SIN
               ,:LQU-TOT-IAS-RST-DPST
                :IND-LQU-TOT-IAS-RST-DPST
               ,:LQU-IMP-ONER-LIQ
                :IND-LQU-IMP-ONER-LIQ
               ,:LQU-COMPON-TAX-RIMB
                :IND-LQU-COMPON-TAX-RIMB
               ,:LQU-TP-MEZ-PAG
                :IND-LQU-TP-MEZ-PAG
               ,:LQU-IMP-EXCONTR
                :IND-LQU-IMP-EXCONTR
               ,:LQU-IMP-INTR-RIT-PAG
                :IND-LQU-IMP-INTR-RIT-PAG
               ,:LQU-BNS-NON-GODUTO
                :IND-LQU-BNS-NON-GODUTO
               ,:LQU-CNBT-INPSTFM
                :IND-LQU-CNBT-INPSTFM
               ,:LQU-IMPST-DA-RIMB
                :IND-LQU-IMPST-DA-RIMB
               ,:LQU-IMPB-IS
                :IND-LQU-IMPB-IS
               ,:LQU-TAX-SEP
                :IND-LQU-TAX-SEP
               ,:LQU-IMPB-TAX-SEP
                :IND-LQU-IMPB-TAX-SEP
               ,:LQU-IMPB-INTR-SU-PREST
                :IND-LQU-IMPB-INTR-SU-PREST
               ,:LQU-ADDIZ-COMUN
                :IND-LQU-ADDIZ-COMUN
               ,:LQU-IMPB-ADDIZ-COMUN
                :IND-LQU-IMPB-ADDIZ-COMUN
               ,:LQU-ADDIZ-REGION
                :IND-LQU-ADDIZ-REGION
               ,:LQU-IMPB-ADDIZ-REGION
                :IND-LQU-IMPB-ADDIZ-REGION
               ,:LQU-MONT-DAL2007
                :IND-LQU-MONT-DAL2007
               ,:LQU-IMPB-CNBT-INPSTFM
                :IND-LQU-IMPB-CNBT-INPSTFM
               ,:LQU-IMP-LRD-DA-RIMB
                :IND-LQU-IMP-LRD-DA-RIMB
               ,:LQU-IMP-DIR-DA-RIMB
                :IND-LQU-IMP-DIR-DA-RIMB
               ,:LQU-RIS-MAT
                :IND-LQU-RIS-MAT
               ,:LQU-RIS-SPE
                :IND-LQU-RIS-SPE
               ,:LQU-DS-RIGA
               ,:LQU-DS-OPER-SQL
               ,:LQU-DS-VER
               ,:LQU-DS-TS-INI-CPTZ
               ,:LQU-DS-TS-END-CPTZ
               ,:LQU-DS-UTENTE
               ,:LQU-DS-STATO-ELAB
               ,:LQU-TOT-IAS-PNL
                :IND-LQU-TOT-IAS-PNL
               ,:LQU-FL-EVE-GARTO
                :IND-LQU-FL-EVE-GARTO
               ,:LQU-IMP-REN-K1
                :IND-LQU-IMP-REN-K1
               ,:LQU-IMP-REN-K2
                :IND-LQU-IMP-REN-K2
               ,:LQU-IMP-REN-K3
                :IND-LQU-IMP-REN-K3
               ,:LQU-PC-REN-K1
                :IND-LQU-PC-REN-K1
               ,:LQU-PC-REN-K2
                :IND-LQU-PC-REN-K2
               ,:LQU-PC-REN-K3
                :IND-LQU-PC-REN-K3
               ,:LQU-TP-CAUS-ANTIC
                :IND-LQU-TP-CAUS-ANTIC
               ,:LQU-IMP-LRD-LIQTO-RILT
                :IND-LQU-IMP-LRD-LIQTO-RILT
               ,:LQU-IMPST-APPL-RILT
                :IND-LQU-IMPST-APPL-RILT
               ,:LQU-PC-RISC-PARZ
                :IND-LQU-PC-RISC-PARZ
               ,:LQU-IMPST-BOLLO-TOT-V
                :IND-LQU-IMPST-BOLLO-TOT-V
               ,:LQU-IMPST-BOLLO-DETT-C
                :IND-LQU-IMPST-BOLLO-DETT-C
               ,:LQU-IMPST-BOLLO-TOT-SW
                :IND-LQU-IMPST-BOLLO-TOT-SW
               ,:LQU-IMPST-BOLLO-TOT-AA
                :IND-LQU-IMPST-BOLLO-TOT-AA
               ,:LQU-IMPB-VIS-1382011
                :IND-LQU-IMPB-VIS-1382011
               ,:LQU-IMPST-VIS-1382011
                :IND-LQU-IMPST-VIS-1382011
               ,:LQU-IMPB-IS-1382011
                :IND-LQU-IMPB-IS-1382011
               ,:LQU-IMPST-SOST-1382011
                :IND-LQU-IMPST-SOST-1382011
               ,:LQU-PC-ABB-TIT-STAT
                :IND-LQU-PC-ABB-TIT-STAT
               ,:LQU-IMPB-BOLLO-DETT-C
                :IND-LQU-IMPB-BOLLO-DETT-C
               ,:LQU-FL-PRE-COMP
                :IND-LQU-FL-PRE-COMP
               ,:LQU-IMPB-VIS-662014
                :IND-LQU-IMPB-VIS-662014
               ,:LQU-IMPST-VIS-662014
                :IND-LQU-IMPST-VIS-662014
               ,:LQU-IMPB-IS-662014
                :IND-LQU-IMPB-IS-662014
               ,:LQU-IMPST-SOST-662014
                :IND-LQU-IMPST-SOST-662014
               ,:LQU-PC-ABB-TS-662014
                :IND-LQU-PC-ABB-TS-662014
               ,:LQU-IMP-LRD-CALC-CP
                :IND-LQU-IMP-LRD-CALC-CP
               ,:LQU-COS-TUNNEL-USCITA
                :IND-LQU-COS-TUNNEL-USCITA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B770-CLOSE-CURSOR-IDO-CPZ     THRU B770-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-LQU-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO LQU-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-LQU-IB-OGG = -1
              MOVE HIGH-VALUES TO LQU-IB-OGG-NULL
           END-IF
           IF IND-LQU-DESC-CAU-EVE-SIN = -1
              MOVE HIGH-VALUES TO LQU-DESC-CAU-EVE-SIN
           END-IF
           IF IND-LQU-COD-CAU-SIN = -1
              MOVE HIGH-VALUES TO LQU-COD-CAU-SIN-NULL
           END-IF
           IF IND-LQU-COD-SIN-CATSTRF = -1
              MOVE HIGH-VALUES TO LQU-COD-SIN-CATSTRF-NULL
           END-IF
           IF IND-LQU-DT-MOR = -1
              MOVE HIGH-VALUES TO LQU-DT-MOR-NULL
           END-IF
           IF IND-LQU-DT-DEN = -1
              MOVE HIGH-VALUES TO LQU-DT-DEN-NULL
           END-IF
           IF IND-LQU-DT-PERV-DEN = -1
              MOVE HIGH-VALUES TO LQU-DT-PERV-DEN-NULL
           END-IF
           IF IND-LQU-DT-RICH = -1
              MOVE HIGH-VALUES TO LQU-DT-RICH-NULL
           END-IF
           IF IND-LQU-TP-SIN = -1
              MOVE HIGH-VALUES TO LQU-TP-SIN-NULL
           END-IF
           IF IND-LQU-TP-RISC = -1
              MOVE HIGH-VALUES TO LQU-TP-RISC-NULL
           END-IF
           IF IND-LQU-TP-MET-RISC = -1
              MOVE HIGH-VALUES TO LQU-TP-MET-RISC-NULL
           END-IF
           IF IND-LQU-DT-LIQ = -1
              MOVE HIGH-VALUES TO LQU-DT-LIQ-NULL
           END-IF
           IF IND-LQU-COD-DVS = -1
              MOVE HIGH-VALUES TO LQU-COD-DVS-NULL
           END-IF
           IF IND-LQU-TOT-IMP-LRD-LIQTO = -1
              MOVE HIGH-VALUES TO LQU-TOT-IMP-LRD-LIQTO-NULL
           END-IF
           IF IND-LQU-TOT-IMP-PREST = -1
              MOVE HIGH-VALUES TO LQU-TOT-IMP-PREST-NULL
           END-IF
           IF IND-LQU-TOT-IMP-INTR-PREST = -1
              MOVE HIGH-VALUES TO LQU-TOT-IMP-INTR-PREST-NULL
           END-IF
           IF IND-LQU-TOT-IMP-UTI = -1
              MOVE HIGH-VALUES TO LQU-TOT-IMP-UTI-NULL
           END-IF
           IF IND-LQU-TOT-IMP-RIT-TFR = -1
              MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-TFR-NULL
           END-IF
           IF IND-LQU-TOT-IMP-RIT-ACC = -1
              MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-ACC-NULL
           END-IF
           IF IND-LQU-TOT-IMP-RIT-VIS = -1
              MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-VIS-NULL
           END-IF
           IF IND-LQU-TOT-IMPB-TFR = -1
              MOVE HIGH-VALUES TO LQU-TOT-IMPB-TFR-NULL
           END-IF
           IF IND-LQU-TOT-IMPB-ACC = -1
              MOVE HIGH-VALUES TO LQU-TOT-IMPB-ACC-NULL
           END-IF
           IF IND-LQU-TOT-IMPB-VIS = -1
              MOVE HIGH-VALUES TO LQU-TOT-IMPB-VIS-NULL
           END-IF
           IF IND-LQU-TOT-IMP-RIMB = -1
              MOVE HIGH-VALUES TO LQU-TOT-IMP-RIMB-NULL
           END-IF
           IF IND-LQU-IMPB-IMPST-PRVR = -1
              MOVE HIGH-VALUES TO LQU-IMPB-IMPST-PRVR-NULL
           END-IF
           IF IND-LQU-IMPST-PRVR = -1
              MOVE HIGH-VALUES TO LQU-IMPST-PRVR-NULL
           END-IF
           IF IND-LQU-IMPB-IMPST-252 = -1
              MOVE HIGH-VALUES TO LQU-IMPB-IMPST-252-NULL
           END-IF
           IF IND-LQU-IMPST-252 = -1
              MOVE HIGH-VALUES TO LQU-IMPST-252-NULL
           END-IF
           IF IND-LQU-TOT-IMP-IS = -1
              MOVE HIGH-VALUES TO LQU-TOT-IMP-IS-NULL
           END-IF
           IF IND-LQU-IMP-DIR-LIQ = -1
              MOVE HIGH-VALUES TO LQU-IMP-DIR-LIQ-NULL
           END-IF
           IF IND-LQU-TOT-IMP-NET-LIQTO = -1
              MOVE HIGH-VALUES TO LQU-TOT-IMP-NET-LIQTO-NULL
           END-IF
           IF IND-LQU-MONT-END2000 = -1
              MOVE HIGH-VALUES TO LQU-MONT-END2000-NULL
           END-IF
           IF IND-LQU-MONT-END2006 = -1
              MOVE HIGH-VALUES TO LQU-MONT-END2006-NULL
           END-IF
           IF IND-LQU-PC-REN = -1
              MOVE HIGH-VALUES TO LQU-PC-REN-NULL
           END-IF
           IF IND-LQU-IMP-PNL = -1
              MOVE HIGH-VALUES TO LQU-IMP-PNL-NULL
           END-IF
           IF IND-LQU-IMPB-IRPEF = -1
              MOVE HIGH-VALUES TO LQU-IMPB-IRPEF-NULL
           END-IF
           IF IND-LQU-IMPST-IRPEF = -1
              MOVE HIGH-VALUES TO LQU-IMPST-IRPEF-NULL
           END-IF
           IF IND-LQU-DT-VLT = -1
              MOVE HIGH-VALUES TO LQU-DT-VLT-NULL
           END-IF
           IF IND-LQU-DT-END-ISTR = -1
              MOVE HIGH-VALUES TO LQU-DT-END-ISTR-NULL
           END-IF
           IF IND-LQU-SPE-RCS = -1
              MOVE HIGH-VALUES TO LQU-SPE-RCS-NULL
           END-IF
           IF IND-LQU-IB-LIQ = -1
              MOVE HIGH-VALUES TO LQU-IB-LIQ-NULL
           END-IF
           IF IND-LQU-TOT-IAS-ONER-PRVNT = -1
              MOVE HIGH-VALUES TO LQU-TOT-IAS-ONER-PRVNT-NULL
           END-IF
           IF IND-LQU-TOT-IAS-MGG-SIN = -1
              MOVE HIGH-VALUES TO LQU-TOT-IAS-MGG-SIN-NULL
           END-IF
           IF IND-LQU-TOT-IAS-RST-DPST = -1
              MOVE HIGH-VALUES TO LQU-TOT-IAS-RST-DPST-NULL
           END-IF
           IF IND-LQU-IMP-ONER-LIQ = -1
              MOVE HIGH-VALUES TO LQU-IMP-ONER-LIQ-NULL
           END-IF
           IF IND-LQU-COMPON-TAX-RIMB = -1
              MOVE HIGH-VALUES TO LQU-COMPON-TAX-RIMB-NULL
           END-IF
           IF IND-LQU-TP-MEZ-PAG = -1
              MOVE HIGH-VALUES TO LQU-TP-MEZ-PAG-NULL
           END-IF
           IF IND-LQU-IMP-EXCONTR = -1
              MOVE HIGH-VALUES TO LQU-IMP-EXCONTR-NULL
           END-IF
           IF IND-LQU-IMP-INTR-RIT-PAG = -1
              MOVE HIGH-VALUES TO LQU-IMP-INTR-RIT-PAG-NULL
           END-IF
           IF IND-LQU-BNS-NON-GODUTO = -1
              MOVE HIGH-VALUES TO LQU-BNS-NON-GODUTO-NULL
           END-IF
           IF IND-LQU-CNBT-INPSTFM = -1
              MOVE HIGH-VALUES TO LQU-CNBT-INPSTFM-NULL
           END-IF
           IF IND-LQU-IMPST-DA-RIMB = -1
              MOVE HIGH-VALUES TO LQU-IMPST-DA-RIMB-NULL
           END-IF
           IF IND-LQU-IMPB-IS = -1
              MOVE HIGH-VALUES TO LQU-IMPB-IS-NULL
           END-IF
           IF IND-LQU-TAX-SEP = -1
              MOVE HIGH-VALUES TO LQU-TAX-SEP-NULL
           END-IF
           IF IND-LQU-IMPB-TAX-SEP = -1
              MOVE HIGH-VALUES TO LQU-IMPB-TAX-SEP-NULL
           END-IF
           IF IND-LQU-IMPB-INTR-SU-PREST = -1
              MOVE HIGH-VALUES TO LQU-IMPB-INTR-SU-PREST-NULL
           END-IF
           IF IND-LQU-ADDIZ-COMUN = -1
              MOVE HIGH-VALUES TO LQU-ADDIZ-COMUN-NULL
           END-IF
           IF IND-LQU-IMPB-ADDIZ-COMUN = -1
              MOVE HIGH-VALUES TO LQU-IMPB-ADDIZ-COMUN-NULL
           END-IF
           IF IND-LQU-ADDIZ-REGION = -1
              MOVE HIGH-VALUES TO LQU-ADDIZ-REGION-NULL
           END-IF
           IF IND-LQU-IMPB-ADDIZ-REGION = -1
              MOVE HIGH-VALUES TO LQU-IMPB-ADDIZ-REGION-NULL
           END-IF
           IF IND-LQU-MONT-DAL2007 = -1
              MOVE HIGH-VALUES TO LQU-MONT-DAL2007-NULL
           END-IF
           IF IND-LQU-IMPB-CNBT-INPSTFM = -1
              MOVE HIGH-VALUES TO LQU-IMPB-CNBT-INPSTFM-NULL
           END-IF
           IF IND-LQU-IMP-LRD-DA-RIMB = -1
              MOVE HIGH-VALUES TO LQU-IMP-LRD-DA-RIMB-NULL
           END-IF
           IF IND-LQU-IMP-DIR-DA-RIMB = -1
              MOVE HIGH-VALUES TO LQU-IMP-DIR-DA-RIMB-NULL
           END-IF
           IF IND-LQU-RIS-MAT = -1
              MOVE HIGH-VALUES TO LQU-RIS-MAT-NULL
           END-IF
           IF IND-LQU-RIS-SPE = -1
              MOVE HIGH-VALUES TO LQU-RIS-SPE-NULL
           END-IF
           IF IND-LQU-TOT-IAS-PNL = -1
              MOVE HIGH-VALUES TO LQU-TOT-IAS-PNL-NULL
           END-IF
           IF IND-LQU-FL-EVE-GARTO = -1
              MOVE HIGH-VALUES TO LQU-FL-EVE-GARTO-NULL
           END-IF
           IF IND-LQU-IMP-REN-K1 = -1
              MOVE HIGH-VALUES TO LQU-IMP-REN-K1-NULL
           END-IF
           IF IND-LQU-IMP-REN-K2 = -1
              MOVE HIGH-VALUES TO LQU-IMP-REN-K2-NULL
           END-IF
           IF IND-LQU-IMP-REN-K3 = -1
              MOVE HIGH-VALUES TO LQU-IMP-REN-K3-NULL
           END-IF
           IF IND-LQU-PC-REN-K1 = -1
              MOVE HIGH-VALUES TO LQU-PC-REN-K1-NULL
           END-IF
           IF IND-LQU-PC-REN-K2 = -1
              MOVE HIGH-VALUES TO LQU-PC-REN-K2-NULL
           END-IF
           IF IND-LQU-PC-REN-K3 = -1
              MOVE HIGH-VALUES TO LQU-PC-REN-K3-NULL
           END-IF
           IF IND-LQU-TP-CAUS-ANTIC = -1
              MOVE HIGH-VALUES TO LQU-TP-CAUS-ANTIC-NULL
           END-IF
           IF IND-LQU-IMP-LRD-LIQTO-RILT = -1
              MOVE HIGH-VALUES TO LQU-IMP-LRD-LIQTO-RILT-NULL
           END-IF
           IF IND-LQU-IMPST-APPL-RILT = -1
              MOVE HIGH-VALUES TO LQU-IMPST-APPL-RILT-NULL
           END-IF
           IF IND-LQU-PC-RISC-PARZ = -1
              MOVE HIGH-VALUES TO LQU-PC-RISC-PARZ-NULL
           END-IF
           IF IND-LQU-IMPST-BOLLO-TOT-V = -1
              MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-V-NULL
           END-IF
           IF IND-LQU-IMPST-BOLLO-DETT-C = -1
              MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-DETT-C-NULL
           END-IF
           IF IND-LQU-IMPST-BOLLO-TOT-SW = -1
              MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-SW-NULL
           END-IF
           IF IND-LQU-IMPST-BOLLO-TOT-AA = -1
              MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-AA-NULL
           END-IF
           IF IND-LQU-IMPB-VIS-1382011 = -1
              MOVE HIGH-VALUES TO LQU-IMPB-VIS-1382011-NULL
           END-IF
           IF IND-LQU-IMPST-VIS-1382011 = -1
              MOVE HIGH-VALUES TO LQU-IMPST-VIS-1382011-NULL
           END-IF
           IF IND-LQU-IMPB-IS-1382011 = -1
              MOVE HIGH-VALUES TO LQU-IMPB-IS-1382011-NULL
           END-IF
           IF IND-LQU-IMPST-SOST-1382011 = -1
              MOVE HIGH-VALUES TO LQU-IMPST-SOST-1382011-NULL
           END-IF
           IF IND-LQU-PC-ABB-TIT-STAT = -1
              MOVE HIGH-VALUES TO LQU-PC-ABB-TIT-STAT-NULL
           END-IF
           IF IND-LQU-IMPB-BOLLO-DETT-C = -1
              MOVE HIGH-VALUES TO LQU-IMPB-BOLLO-DETT-C-NULL
           END-IF
           IF IND-LQU-FL-PRE-COMP = -1
              MOVE HIGH-VALUES TO LQU-FL-PRE-COMP-NULL
           END-IF
           IF IND-LQU-IMPB-VIS-662014 = -1
              MOVE HIGH-VALUES TO LQU-IMPB-VIS-662014-NULL
           END-IF
           IF IND-LQU-IMPST-VIS-662014 = -1
              MOVE HIGH-VALUES TO LQU-IMPST-VIS-662014-NULL
           END-IF
           IF IND-LQU-IMPB-IS-662014 = -1
              MOVE HIGH-VALUES TO LQU-IMPB-IS-662014-NULL
           END-IF
           IF IND-LQU-IMPST-SOST-662014 = -1
              MOVE HIGH-VALUES TO LQU-IMPST-SOST-662014-NULL
           END-IF
           IF IND-LQU-PC-ABB-TS-662014 = -1
              MOVE HIGH-VALUES TO LQU-PC-ABB-TS-662014-NULL
           END-IF
           IF IND-LQU-IMP-LRD-CALC-CP = -1
              MOVE HIGH-VALUES TO LQU-IMP-LRD-CALC-CP-NULL
           END-IF
           IF IND-LQU-COS-TUNNEL-USCITA = -1
              MOVE HIGH-VALUES TO LQU-COS-TUNNEL-USCITA-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO LQU-DS-OPER-SQL
           MOVE 0                   TO LQU-DS-VER
           MOVE IDSV0003-USER-NAME TO LQU-DS-UTENTE
           MOVE '1'                   TO LQU-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO LQU-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO LQU-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF LQU-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-LQU-ID-MOVI-CHIU
           END-IF
           IF LQU-IB-OGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IB-OGG
           ELSE
              MOVE 0 TO IND-LQU-IB-OGG
           END-IF
           IF LQU-DESC-CAU-EVE-SIN = HIGH-VALUES
              MOVE -1 TO IND-LQU-DESC-CAU-EVE-SIN
           ELSE
              MOVE 0 TO IND-LQU-DESC-CAU-EVE-SIN
           END-IF
           IF LQU-COD-CAU-SIN-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-COD-CAU-SIN
           ELSE
              MOVE 0 TO IND-LQU-COD-CAU-SIN
           END-IF
           IF LQU-COD-SIN-CATSTRF-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-COD-SIN-CATSTRF
           ELSE
              MOVE 0 TO IND-LQU-COD-SIN-CATSTRF
           END-IF
           IF LQU-DT-MOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-DT-MOR
           ELSE
              MOVE 0 TO IND-LQU-DT-MOR
           END-IF
           IF LQU-DT-DEN-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-DT-DEN
           ELSE
              MOVE 0 TO IND-LQU-DT-DEN
           END-IF
           IF LQU-DT-PERV-DEN-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-DT-PERV-DEN
           ELSE
              MOVE 0 TO IND-LQU-DT-PERV-DEN
           END-IF
           IF LQU-DT-RICH-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-DT-RICH
           ELSE
              MOVE 0 TO IND-LQU-DT-RICH
           END-IF
           IF LQU-TP-SIN-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TP-SIN
           ELSE
              MOVE 0 TO IND-LQU-TP-SIN
           END-IF
           IF LQU-TP-RISC-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TP-RISC
           ELSE
              MOVE 0 TO IND-LQU-TP-RISC
           END-IF
           IF LQU-TP-MET-RISC-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TP-MET-RISC
           ELSE
              MOVE 0 TO IND-LQU-TP-MET-RISC
           END-IF
           IF LQU-DT-LIQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-DT-LIQ
           ELSE
              MOVE 0 TO IND-LQU-DT-LIQ
           END-IF
           IF LQU-COD-DVS-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-COD-DVS
           ELSE
              MOVE 0 TO IND-LQU-COD-DVS
           END-IF
           IF LQU-TOT-IMP-LRD-LIQTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TOT-IMP-LRD-LIQTO
           ELSE
              MOVE 0 TO IND-LQU-TOT-IMP-LRD-LIQTO
           END-IF
           IF LQU-TOT-IMP-PREST-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TOT-IMP-PREST
           ELSE
              MOVE 0 TO IND-LQU-TOT-IMP-PREST
           END-IF
           IF LQU-TOT-IMP-INTR-PREST-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TOT-IMP-INTR-PREST
           ELSE
              MOVE 0 TO IND-LQU-TOT-IMP-INTR-PREST
           END-IF
           IF LQU-TOT-IMP-UTI-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TOT-IMP-UTI
           ELSE
              MOVE 0 TO IND-LQU-TOT-IMP-UTI
           END-IF
           IF LQU-TOT-IMP-RIT-TFR-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TOT-IMP-RIT-TFR
           ELSE
              MOVE 0 TO IND-LQU-TOT-IMP-RIT-TFR
           END-IF
           IF LQU-TOT-IMP-RIT-ACC-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TOT-IMP-RIT-ACC
           ELSE
              MOVE 0 TO IND-LQU-TOT-IMP-RIT-ACC
           END-IF
           IF LQU-TOT-IMP-RIT-VIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TOT-IMP-RIT-VIS
           ELSE
              MOVE 0 TO IND-LQU-TOT-IMP-RIT-VIS
           END-IF
           IF LQU-TOT-IMPB-TFR-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TOT-IMPB-TFR
           ELSE
              MOVE 0 TO IND-LQU-TOT-IMPB-TFR
           END-IF
           IF LQU-TOT-IMPB-ACC-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TOT-IMPB-ACC
           ELSE
              MOVE 0 TO IND-LQU-TOT-IMPB-ACC
           END-IF
           IF LQU-TOT-IMPB-VIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TOT-IMPB-VIS
           ELSE
              MOVE 0 TO IND-LQU-TOT-IMPB-VIS
           END-IF
           IF LQU-TOT-IMP-RIMB-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TOT-IMP-RIMB
           ELSE
              MOVE 0 TO IND-LQU-TOT-IMP-RIMB
           END-IF
           IF LQU-IMPB-IMPST-PRVR-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPB-IMPST-PRVR
           ELSE
              MOVE 0 TO IND-LQU-IMPB-IMPST-PRVR
           END-IF
           IF LQU-IMPST-PRVR-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPST-PRVR
           ELSE
              MOVE 0 TO IND-LQU-IMPST-PRVR
           END-IF
           IF LQU-IMPB-IMPST-252-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPB-IMPST-252
           ELSE
              MOVE 0 TO IND-LQU-IMPB-IMPST-252
           END-IF
           IF LQU-IMPST-252-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPST-252
           ELSE
              MOVE 0 TO IND-LQU-IMPST-252
           END-IF
           IF LQU-TOT-IMP-IS-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TOT-IMP-IS
           ELSE
              MOVE 0 TO IND-LQU-TOT-IMP-IS
           END-IF
           IF LQU-IMP-DIR-LIQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMP-DIR-LIQ
           ELSE
              MOVE 0 TO IND-LQU-IMP-DIR-LIQ
           END-IF
           IF LQU-TOT-IMP-NET-LIQTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TOT-IMP-NET-LIQTO
           ELSE
              MOVE 0 TO IND-LQU-TOT-IMP-NET-LIQTO
           END-IF
           IF LQU-MONT-END2000-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-MONT-END2000
           ELSE
              MOVE 0 TO IND-LQU-MONT-END2000
           END-IF
           IF LQU-MONT-END2006-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-MONT-END2006
           ELSE
              MOVE 0 TO IND-LQU-MONT-END2006
           END-IF
           IF LQU-PC-REN-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-PC-REN
           ELSE
              MOVE 0 TO IND-LQU-PC-REN
           END-IF
           IF LQU-IMP-PNL-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMP-PNL
           ELSE
              MOVE 0 TO IND-LQU-IMP-PNL
           END-IF
           IF LQU-IMPB-IRPEF-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPB-IRPEF
           ELSE
              MOVE 0 TO IND-LQU-IMPB-IRPEF
           END-IF
           IF LQU-IMPST-IRPEF-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPST-IRPEF
           ELSE
              MOVE 0 TO IND-LQU-IMPST-IRPEF
           END-IF
           IF LQU-DT-VLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-DT-VLT
           ELSE
              MOVE 0 TO IND-LQU-DT-VLT
           END-IF
           IF LQU-DT-END-ISTR-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-DT-END-ISTR
           ELSE
              MOVE 0 TO IND-LQU-DT-END-ISTR
           END-IF
           IF LQU-SPE-RCS-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-SPE-RCS
           ELSE
              MOVE 0 TO IND-LQU-SPE-RCS
           END-IF
           IF LQU-IB-LIQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IB-LIQ
           ELSE
              MOVE 0 TO IND-LQU-IB-LIQ
           END-IF
           IF LQU-TOT-IAS-ONER-PRVNT-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TOT-IAS-ONER-PRVNT
           ELSE
              MOVE 0 TO IND-LQU-TOT-IAS-ONER-PRVNT
           END-IF
           IF LQU-TOT-IAS-MGG-SIN-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TOT-IAS-MGG-SIN
           ELSE
              MOVE 0 TO IND-LQU-TOT-IAS-MGG-SIN
           END-IF
           IF LQU-TOT-IAS-RST-DPST-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TOT-IAS-RST-DPST
           ELSE
              MOVE 0 TO IND-LQU-TOT-IAS-RST-DPST
           END-IF
           IF LQU-IMP-ONER-LIQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMP-ONER-LIQ
           ELSE
              MOVE 0 TO IND-LQU-IMP-ONER-LIQ
           END-IF
           IF LQU-COMPON-TAX-RIMB-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-COMPON-TAX-RIMB
           ELSE
              MOVE 0 TO IND-LQU-COMPON-TAX-RIMB
           END-IF
           IF LQU-TP-MEZ-PAG-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TP-MEZ-PAG
           ELSE
              MOVE 0 TO IND-LQU-TP-MEZ-PAG
           END-IF
           IF LQU-IMP-EXCONTR-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMP-EXCONTR
           ELSE
              MOVE 0 TO IND-LQU-IMP-EXCONTR
           END-IF
           IF LQU-IMP-INTR-RIT-PAG-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMP-INTR-RIT-PAG
           ELSE
              MOVE 0 TO IND-LQU-IMP-INTR-RIT-PAG
           END-IF
           IF LQU-BNS-NON-GODUTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-BNS-NON-GODUTO
           ELSE
              MOVE 0 TO IND-LQU-BNS-NON-GODUTO
           END-IF
           IF LQU-CNBT-INPSTFM-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-CNBT-INPSTFM
           ELSE
              MOVE 0 TO IND-LQU-CNBT-INPSTFM
           END-IF
           IF LQU-IMPST-DA-RIMB-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPST-DA-RIMB
           ELSE
              MOVE 0 TO IND-LQU-IMPST-DA-RIMB
           END-IF
           IF LQU-IMPB-IS-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPB-IS
           ELSE
              MOVE 0 TO IND-LQU-IMPB-IS
           END-IF
           IF LQU-TAX-SEP-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TAX-SEP
           ELSE
              MOVE 0 TO IND-LQU-TAX-SEP
           END-IF
           IF LQU-IMPB-TAX-SEP-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPB-TAX-SEP
           ELSE
              MOVE 0 TO IND-LQU-IMPB-TAX-SEP
           END-IF
           IF LQU-IMPB-INTR-SU-PREST-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPB-INTR-SU-PREST
           ELSE
              MOVE 0 TO IND-LQU-IMPB-INTR-SU-PREST
           END-IF
           IF LQU-ADDIZ-COMUN-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-ADDIZ-COMUN
           ELSE
              MOVE 0 TO IND-LQU-ADDIZ-COMUN
           END-IF
           IF LQU-IMPB-ADDIZ-COMUN-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPB-ADDIZ-COMUN
           ELSE
              MOVE 0 TO IND-LQU-IMPB-ADDIZ-COMUN
           END-IF
           IF LQU-ADDIZ-REGION-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-ADDIZ-REGION
           ELSE
              MOVE 0 TO IND-LQU-ADDIZ-REGION
           END-IF
           IF LQU-IMPB-ADDIZ-REGION-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPB-ADDIZ-REGION
           ELSE
              MOVE 0 TO IND-LQU-IMPB-ADDIZ-REGION
           END-IF
           IF LQU-MONT-DAL2007-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-MONT-DAL2007
           ELSE
              MOVE 0 TO IND-LQU-MONT-DAL2007
           END-IF
           IF LQU-IMPB-CNBT-INPSTFM-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPB-CNBT-INPSTFM
           ELSE
              MOVE 0 TO IND-LQU-IMPB-CNBT-INPSTFM
           END-IF
           IF LQU-IMP-LRD-DA-RIMB-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMP-LRD-DA-RIMB
           ELSE
              MOVE 0 TO IND-LQU-IMP-LRD-DA-RIMB
           END-IF
           IF LQU-IMP-DIR-DA-RIMB-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMP-DIR-DA-RIMB
           ELSE
              MOVE 0 TO IND-LQU-IMP-DIR-DA-RIMB
           END-IF
           IF LQU-RIS-MAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-RIS-MAT
           ELSE
              MOVE 0 TO IND-LQU-RIS-MAT
           END-IF
           IF LQU-RIS-SPE-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-RIS-SPE
           ELSE
              MOVE 0 TO IND-LQU-RIS-SPE
           END-IF
           IF LQU-TOT-IAS-PNL-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TOT-IAS-PNL
           ELSE
              MOVE 0 TO IND-LQU-TOT-IAS-PNL
           END-IF
           IF LQU-FL-EVE-GARTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-FL-EVE-GARTO
           ELSE
              MOVE 0 TO IND-LQU-FL-EVE-GARTO
           END-IF
           IF LQU-IMP-REN-K1-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMP-REN-K1
           ELSE
              MOVE 0 TO IND-LQU-IMP-REN-K1
           END-IF
           IF LQU-IMP-REN-K2-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMP-REN-K2
           ELSE
              MOVE 0 TO IND-LQU-IMP-REN-K2
           END-IF
           IF LQU-IMP-REN-K3-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMP-REN-K3
           ELSE
              MOVE 0 TO IND-LQU-IMP-REN-K3
           END-IF
           IF LQU-PC-REN-K1-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-PC-REN-K1
           ELSE
              MOVE 0 TO IND-LQU-PC-REN-K1
           END-IF
           IF LQU-PC-REN-K2-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-PC-REN-K2
           ELSE
              MOVE 0 TO IND-LQU-PC-REN-K2
           END-IF
           IF LQU-PC-REN-K3-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-PC-REN-K3
           ELSE
              MOVE 0 TO IND-LQU-PC-REN-K3
           END-IF
           IF LQU-TP-CAUS-ANTIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-TP-CAUS-ANTIC
           ELSE
              MOVE 0 TO IND-LQU-TP-CAUS-ANTIC
           END-IF
           IF LQU-IMP-LRD-LIQTO-RILT-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMP-LRD-LIQTO-RILT
           ELSE
              MOVE 0 TO IND-LQU-IMP-LRD-LIQTO-RILT
           END-IF
           IF LQU-IMPST-APPL-RILT-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPST-APPL-RILT
           ELSE
              MOVE 0 TO IND-LQU-IMPST-APPL-RILT
           END-IF
           IF LQU-PC-RISC-PARZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-PC-RISC-PARZ
           ELSE
              MOVE 0 TO IND-LQU-PC-RISC-PARZ
           END-IF
           IF LQU-IMPST-BOLLO-TOT-V-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPST-BOLLO-TOT-V
           ELSE
              MOVE 0 TO IND-LQU-IMPST-BOLLO-TOT-V
           END-IF
           IF LQU-IMPST-BOLLO-DETT-C-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPST-BOLLO-DETT-C
           ELSE
              MOVE 0 TO IND-LQU-IMPST-BOLLO-DETT-C
           END-IF
           IF LQU-IMPST-BOLLO-TOT-SW-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPST-BOLLO-TOT-SW
           ELSE
              MOVE 0 TO IND-LQU-IMPST-BOLLO-TOT-SW
           END-IF
           IF LQU-IMPST-BOLLO-TOT-AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPST-BOLLO-TOT-AA
           ELSE
              MOVE 0 TO IND-LQU-IMPST-BOLLO-TOT-AA
           END-IF
           IF LQU-IMPB-VIS-1382011-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPB-VIS-1382011
           ELSE
              MOVE 0 TO IND-LQU-IMPB-VIS-1382011
           END-IF
           IF LQU-IMPST-VIS-1382011-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPST-VIS-1382011
           ELSE
              MOVE 0 TO IND-LQU-IMPST-VIS-1382011
           END-IF
           IF LQU-IMPB-IS-1382011-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPB-IS-1382011
           ELSE
              MOVE 0 TO IND-LQU-IMPB-IS-1382011
           END-IF
           IF LQU-IMPST-SOST-1382011-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPST-SOST-1382011
           ELSE
              MOVE 0 TO IND-LQU-IMPST-SOST-1382011
           END-IF
           IF LQU-PC-ABB-TIT-STAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-PC-ABB-TIT-STAT
           ELSE
              MOVE 0 TO IND-LQU-PC-ABB-TIT-STAT
           END-IF
           IF LQU-IMPB-BOLLO-DETT-C-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPB-BOLLO-DETT-C
           ELSE
              MOVE 0 TO IND-LQU-IMPB-BOLLO-DETT-C
           END-IF
           IF LQU-FL-PRE-COMP-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-FL-PRE-COMP
           ELSE
              MOVE 0 TO IND-LQU-FL-PRE-COMP
           END-IF
           IF LQU-IMPB-VIS-662014-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPB-VIS-662014
           ELSE
              MOVE 0 TO IND-LQU-IMPB-VIS-662014
           END-IF
           IF LQU-IMPST-VIS-662014-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPST-VIS-662014
           ELSE
              MOVE 0 TO IND-LQU-IMPST-VIS-662014
           END-IF
           IF LQU-IMPB-IS-662014-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPB-IS-662014
           ELSE
              MOVE 0 TO IND-LQU-IMPB-IS-662014
           END-IF
           IF LQU-IMPST-SOST-662014-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMPST-SOST-662014
           ELSE
              MOVE 0 TO IND-LQU-IMPST-SOST-662014
           END-IF
           IF LQU-PC-ABB-TS-662014-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-PC-ABB-TS-662014
           ELSE
              MOVE 0 TO IND-LQU-PC-ABB-TS-662014
           END-IF
           IF LQU-IMP-LRD-CALC-CP-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-IMP-LRD-CALC-CP
           ELSE
              MOVE 0 TO IND-LQU-IMP-LRD-CALC-CP
           END-IF
           IF LQU-COS-TUNNEL-USCITA-NULL = HIGH-VALUES
              MOVE -1 TO IND-LQU-COS-TUNNEL-USCITA
           ELSE
              MOVE 0 TO IND-LQU-COS-TUNNEL-USCITA
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : LQU-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE LIQ TO WS-BUFFER-TABLE.

           MOVE LQU-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO LQU-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO LQU-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO LQU-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO LQU-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO LQU-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO LQU-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO LQU-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.

           MOVE LIQ TO WS-BUFFER-TABLE.

           MOVE LQU-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.

       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO LIQ.

           MOVE WS-ID-MOVI-CRZ  TO LQU-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO LQU-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO LQU-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO LQU-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO LQU-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO LQU-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO LQU-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE LQU-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO LQU-DT-INI-EFF-DB
           MOVE LQU-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO LQU-DT-END-EFF-DB
           IF IND-LQU-DT-MOR = 0
               MOVE LQU-DT-MOR TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO LQU-DT-MOR-DB
           END-IF
           IF IND-LQU-DT-DEN = 0
               MOVE LQU-DT-DEN TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO LQU-DT-DEN-DB
           END-IF
           IF IND-LQU-DT-PERV-DEN = 0
               MOVE LQU-DT-PERV-DEN TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO LQU-DT-PERV-DEN-DB
           END-IF
           IF IND-LQU-DT-RICH = 0
               MOVE LQU-DT-RICH TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO LQU-DT-RICH-DB
           END-IF
           IF IND-LQU-DT-LIQ = 0
               MOVE LQU-DT-LIQ TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO LQU-DT-LIQ-DB
           END-IF
           IF IND-LQU-DT-VLT = 0
               MOVE LQU-DT-VLT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO LQU-DT-VLT-DB
           END-IF
           IF IND-LQU-DT-END-ISTR = 0
               MOVE LQU-DT-END-ISTR TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO LQU-DT-END-ISTR-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE LQU-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO LQU-DT-INI-EFF
           MOVE LQU-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO LQU-DT-END-EFF
           IF IND-LQU-DT-MOR = 0
               MOVE LQU-DT-MOR-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO LQU-DT-MOR
           END-IF
           IF IND-LQU-DT-DEN = 0
               MOVE LQU-DT-DEN-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO LQU-DT-DEN
           END-IF
           IF IND-LQU-DT-PERV-DEN = 0
               MOVE LQU-DT-PERV-DEN-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO LQU-DT-PERV-DEN
           END-IF
           IF IND-LQU-DT-RICH = 0
               MOVE LQU-DT-RICH-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO LQU-DT-RICH
           END-IF
           IF IND-LQU-DT-LIQ = 0
               MOVE LQU-DT-LIQ-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO LQU-DT-LIQ
           END-IF
           IF IND-LQU-DT-VLT = 0
               MOVE LQU-DT-VLT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO LQU-DT-VLT
           END-IF
           IF IND-LQU-DT-END-ISTR = 0
               MOVE LQU-DT-END-ISTR-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO LQU-DT-END-ISTR
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.


           MOVE LENGTH OF LQU-DESC-CAU-EVE-SIN
                       TO LQU-DESC-CAU-EVE-SIN-LEN.

       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

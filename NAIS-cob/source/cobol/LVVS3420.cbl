      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS3420.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS3420'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01 FL-LIQUI                          PIC 9(001).
          88 FL-LIQUI-SI                    VALUE 1.
          88 FL-LIQUI-NO                    VALUE 0.
      *----------------------------------------------------------------*
      *--> AREA MOVIMENTO
      *----------------------------------------------------------------*
       01 DMOV-IO-MOV.
          03 DMOV-AREA-MOV.
             04 DMOV-ELE-MOV-MAX       PIC S9(04) COMP.
             04 DMOV-TAB-MOV.
             COPY LCCVMOV1             REPLACING ==(SF)== BY ==DMOV==.
      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY LCCC0006.

           COPY LCCVXMVZ.
           COPY LCCVXMV1.
           COPY LCCVXMV2.
           COPY LCCVXMV3.
           COPY LCCVXMV4.
           COPY LCCVXMV5.
           COPY LCCVXMV6.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0007.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0007.
      *----------------------------------------------------------------*
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.
      *
           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000
      *
           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.
      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
      *
           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.
           SET FL-LIQUI-NO                   TO TRUE.
       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.
      *
      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

           PERFORM DETERMINA-MOVI
              THRU DETERMINA-MOVI-EX.
      *
       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-MOVIMENTO
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DMOV-IO-MOV
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *--> SETTO LA VARIABILE PER VEDERE SE SONO IN FASE DI LIQUI OPPURE
      *--> IN FASE DI COMUNICAZIONE
      *----------------------------------------------------------------*
       DETERMINA-MOVI.

           MOVE DMOV-TP-MOVI
             TO WS-MOVIMENTO

           IF RISTO-INDIVI
           OR COMUN-RISPAR-IND
           OR SINIS-INDIVI
           OR VARIA-OPZION
           OR RECES-INDIVI
              SET FL-LIQUI-NO     TO TRUE
           END-IF

           IF LIQUI-RISPAR-POLIND
           OR LIQUI-RISTOT-IND
           OR LIQUI-SCAPOL
           OR LIQUI-SININD
              SET FL-LIQUI-SI     TO TRUE
           END-IF

           MOVE FL-LIQUI
             TO IVVC0213-VAL-IMP-O.

       DETERMINA-MOVI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.

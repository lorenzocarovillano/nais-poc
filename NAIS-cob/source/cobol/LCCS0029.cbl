      *  ============================================================= *
      *                                                                *
      *        PORTAFOGLIO VITA ITALIA  VER 1.0                        *
      *                                                                *
      *  ============================================================= *
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LCCS0029.
       AUTHOR.             ATS.
       DATE-WRITTEN.       MARZO 2009.
       DATE-COMPILED.
      *  ------------------------------------------------------------- *
      *     PROGRAMMA ..... LCCS0029
      *     TIPOLOGIA...... MODULO COMUNE
      *     FUNZIONE....... DOPPIO LOAD
      *     DESCRIZIONE.... CALCOLO DATA FINE MESE
      *     PAGINA WEB..... N.A.
      *  ------------------------------------------------------------- *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
      *
       WORKING-STORAGE SECTION.
      *
      *----------------------------------------------------------------*
      * COPY VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
      *
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IEAV9903.
           COPY IDSV0002.
      *
      *----------------------------------------------------------------*
      * COSTANTI E MODULI CHIAMATI
      *----------------------------------------------------------------*
      *
       77 WK-PGM                           PIC X(08) VALUE 'LOAS0800'.
      *  --> CALCOLI SU DATE
       01 LCCS0004                         PIC X(08) VALUE 'LCCS0004'.
      *
      *----------------------------------------------------------------*
      * VARIABILI E CAMPI DI COMODO
      *----------------------------------------------------------------*
      *
       01 WK-GEST-ERR.
          03 WK-LABEL-ERR                PIC X(30) VALUE SPACES.
      *
       01 WK-APPO-DT.
          03 WK-APPO-AAAA               PIC 9(04).
          03 WK-APPO-MM                 PIC 9(02).
          03 WK-APPO-GG                 PIC 9(02).
      *
       01 IX-CICLO                      PIC 9(01) VALUE ZEROES.
      *
      *----------------------------------------------------------------*
      * AREA LCCS0004
      *----------------------------------------------------------------*
      *
       01  PARAM                          PIC 9.
       01  X-DATA.
           05 X-GG                        PIC 9(2).
           05 X-MM                        PIC 9(2).
           05 X-AAAA                      PIC 9(4).
      *
      *----------------------------------------------------------------*
      * L I N K A G E   S E C T I O N
      *----------------------------------------------------------------*
      *
       LINKAGE SECTION.
      *
      *--> Area Contesto
      *
       01 AREA-IDSV0001.
          COPY IDSV0001.
      *
      *--> Area Interfaccia
      *
       01 AREA-IO-LCCS0029.
          COPY LCCC0029                  REPLACING ==(SF)== BY ==S029==.
      *
      *----------------------------------------------------------------*
      * P R O C E D U R E   D I V I S I O N
      *----------------------------------------------------------------*
      *
       PROCEDURE DIVISION USING AREA-IDSV0001
                                AREA-IO-LCCS0029.
      *
           PERFORM S00000-OPERAZ-INIZIALI
              THRU S00000-OPERAZ-INIZIALI-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S10000-ELABORAZIONE
                 THRU S10000-ELABORAZIONE-EX
      *
           END-IF.
      *
           PERFORM S90000-OPERAZ-FINALI
              THRU S90000-OPERAZ-FINALI-EX.
      *
      *----------------------------------------------------------------*
      * OPERAZIONI INIZIALI
      *----------------------------------------------------------------*
      *
       S00000-OPERAZ-INIZIALI.
      *
           MOVE 'S00000-OPERAZ-INIZIALI'
             TO WK-LABEL-ERR.
      *
           MOVE ZEROES
             TO IX-CICLO.
      *
           IF S029-DT-CALC IS NOT NUMERIC
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'DATA NON NUMERICA'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
       S00000-OPERAZ-INIZIALI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * E L A B O R A Z I O N E
      *----------------------------------------------------------------*
      *
       S10000-ELABORAZIONE.
      *
           MOVE 'S10000-ELABORAZIONE'
             TO WK-LABEL-ERR.
      *
           MOVE 1
             TO PARAM.
      *
           MOVE S029-DT-CALC
             TO WK-APPO-DT.
      *
           MOVE 31
             TO WK-APPO-GG.
      *
           PERFORM S10100-VALIDA-FINE-MESE
              THRU S10100-VALIDA-FINE-MESE-EX
             UNTIL PARAM = 0
                OR IDSV0001-ESITO-KO
                OR IX-CICLO > 4.
      *
      * --> Se Entro La Quarta Chiamata La Data Non T Stata Calcolata
      * --> Viene Imposta L'uscita Attrraverso Un Indice Di Chiamate
      *
           IF IDSV0001-ESITO-OK
      *
              IF IX-CICLO > 4
      *
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL-ERR
                   TO IEAI9901-LABEL-ERR
                 MOVE '001114'
                   TO IEAI9901-COD-ERRORE
                 MOVE 'IMPOSSIBILE CALCOLARE DATA FINE MESE'
                   TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
      *
              ELSE
      *
                 MOVE WK-APPO-DT
                   TO S029-DT-CALC
      *
              END-IF
      *
           END-IF.
      *
       S10000-ELABORAZIONE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VALIDAZIONE DATA FINE MESE
      *----------------------------------------------------------------*
      *
       S10100-VALIDA-FINE-MESE.
      *
           MOVE 'S10100-VALIDA-FINE-MESE'
             TO WK-LABEL-ERR.
      *
           ADD 1
             TO IX-CICLO.
      *
           MOVE WK-APPO-AAAA
             TO X-AAAA.
           MOVE WK-APPO-MM
             TO X-MM.
           MOVE WK-APPO-GG
             TO X-GG.
      *
           CALL LCCS0004         USING PARAM
                                       X-DATA
           ON EXCEPTION
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'ERRORE CHIAMATA LCCS0004'
                TO CALL-DESC
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
      *
           END-CALL.
      *
           IF PARAM NOT = ZEROES
      *
              SUBTRACT 1
                  FROM WK-APPO-GG
      *
           END-IF.
      *
       S10100-VALIDA-FINE-MESE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     OPERAZIONI FINALI
      *----------------------------------------------------------------*
      *
       S90000-OPERAZ-FINALI.
      *
           MOVE 'S90000-OPERAZ-FINALI'
             TO WK-LABEL-ERR.
      *
           GOBACK.
      *
       S90000-OPERAZ-FINALI-EX.
           EXIT.
      *
      *
      *----------------------------------------------------------------*
      * ROUTINES GESTIONE ERRORI
      *----------------------------------------------------------------*
      *
           COPY IERP9901.
           COPY IERP9902.
      *


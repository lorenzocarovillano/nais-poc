       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LDBS2780.
       AUTHOR.        AISS.
       DATE-WRITTEN.  29 LUG 2008.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDB030 END-EXEC.
           EXEC SQL INCLUDE IDBVB032 END-EXEC.
           EXEC SQL INCLUDE IDBVB033 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVB031 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 BILA-TRCH-ESTR.

           PERFORM A000-INIZIO              THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-WHERE-CONDITION
                           PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                      SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSB030'       TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'BILA_TRCH_ESTR' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE   OR
                        IDSV0003-UPDATE

                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.

       C200-ELABORA-WC-NST.
           EVALUATE TRUE
              WHEN IDSV0003-UPDATE
                 PERFORM C210-UPDATE-WC-NST          THRU C210-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       C200-EX.
           EXIT.

       C210-UPDATE-WC-NST.

           PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE BILA_TRCH_ESTR
                SET  TP_STAT_BUS_TRCH   = :B03-TP-STAT-BUS-TRCH

                    ,TP_CAUS_TRCH       = :B03-TP-CAUS-TRCH

                    ,TP_STAT_BUS_POLI   = :B03-TP-STAT-BUS-POLI

                    ,TP_CAUS_POLI       = :B03-TP-CAUS-POLI

                    ,TP_STAT_BUS_ADES   = :B03-TP-STAT-BUS-ADES

                    ,TP_CAUS_ADES       = :B03-TP-CAUS-ADES

                    ,ID_RICH_ESTRAZ_AGG = :B03-ID-RICH-ESTRAZ-AGG
                                          :IND-B03-ID-RICH-ESTRAZ-AGG

                WHERE  ID_RICH_ESTRAZ_MAS = :B03-ID-RICH-ESTRAZ-MAS
                AND    ID_ADES            = :B03-ID-ADES
                AND    ID_POLI            = :B03-ID-POLI
                AND    ID_TRCH_DI_GAR     = :B03-ID-TRCH-DI-GAR

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       C210-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           CONTINUE.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES.

           MOVE IDSV0003-OPERAZIONE TO B03-DS-OPER-SQL
           MOVE 1                   TO B03-DS-VER
           MOVE IDSV0003-USER-NAME TO B03-DS-UTENTE
           MOVE '1'                   TO B03-DS-STATO-ELAB
           MOVE WS-TS-COMPETENZA-AGG-STOR TO B03-DS-TS-CPTZ.

       Z150-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.

           IF B03-ID-RICH-ESTRAZ-AGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ID-RICH-ESTRAZ-AGG
           ELSE
              MOVE 0 TO IND-B03-ID-RICH-ESTRAZ-AGG
           END-IF
           IF B03-FL-SIMULAZIONE-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FL-SIMULAZIONE
           ELSE
              MOVE 0 TO IND-B03-FL-SIMULAZIONE
           END-IF
           IF B03-DT-INI-VAL-TAR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-INI-VAL-TAR
           ELSE
              MOVE 0 TO IND-B03-DT-INI-VAL-TAR
           END-IF
           IF B03-COD-PROD-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-PROD
           ELSE
              MOVE 0 TO IND-B03-COD-PROD
           END-IF
           IF B03-COD-TARI-ORGN-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-TARI-ORGN
           ELSE
              MOVE 0 TO IND-B03-COD-TARI-ORGN
           END-IF
           IF B03-MIN-GARTO-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-MIN-GARTO-T
           ELSE
              MOVE 0 TO IND-B03-MIN-GARTO-T
           END-IF
           IF B03-TP-TARI-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-TARI
           ELSE
              MOVE 0 TO IND-B03-TP-TARI
           END-IF
           IF B03-TP-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-PRE
           ELSE
              MOVE 0 TO IND-B03-TP-PRE
           END-IF
           IF B03-TP-ADEG-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-ADEG-PRE
           ELSE
              MOVE 0 TO IND-B03-TP-ADEG-PRE
           END-IF
           IF B03-TP-RIVAL-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-RIVAL
           ELSE
              MOVE 0 TO IND-B03-TP-RIVAL
           END-IF
           IF B03-FL-DA-TRASF-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FL-DA-TRASF
           ELSE
              MOVE 0 TO IND-B03-FL-DA-TRASF
           END-IF
           IF B03-FL-CAR-CONT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FL-CAR-CONT
           ELSE
              MOVE 0 TO IND-B03-FL-CAR-CONT
           END-IF
           IF B03-FL-PRE-DA-RIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FL-PRE-DA-RIS
           ELSE
              MOVE 0 TO IND-B03-FL-PRE-DA-RIS
           END-IF
           IF B03-FL-PRE-AGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FL-PRE-AGG
           ELSE
              MOVE 0 TO IND-B03-FL-PRE-AGG
           END-IF
           IF B03-TP-TRCH-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-TRCH
           ELSE
              MOVE 0 TO IND-B03-TP-TRCH
           END-IF
           IF B03-TP-TST-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-TST
           ELSE
              MOVE 0 TO IND-B03-TP-TST
           END-IF
           IF B03-COD-CONV-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-CONV
           ELSE
              MOVE 0 TO IND-B03-COD-CONV
           END-IF
           IF B03-DT-DECOR-ADES-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-DECOR-ADES
           ELSE
              MOVE 0 TO IND-B03-DT-DECOR-ADES
           END-IF
           IF B03-DT-EMIS-TRCH-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-EMIS-TRCH
           ELSE
              MOVE 0 TO IND-B03-DT-EMIS-TRCH
           END-IF
           IF B03-DT-SCAD-TRCH-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-SCAD-TRCH
           ELSE
              MOVE 0 TO IND-B03-DT-SCAD-TRCH
           END-IF
           IF B03-DT-SCAD-INTMD-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-SCAD-INTMD
           ELSE
              MOVE 0 TO IND-B03-DT-SCAD-INTMD
           END-IF
           IF B03-DT-SCAD-PAG-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-SCAD-PAG-PRE
           ELSE
              MOVE 0 TO IND-B03-DT-SCAD-PAG-PRE
           END-IF
           IF B03-DT-ULT-PRE-PAG-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-ULT-PRE-PAG
           ELSE
              MOVE 0 TO IND-B03-DT-ULT-PRE-PAG
           END-IF
           IF B03-DT-NASC-1O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-NASC-1O-ASSTO
           ELSE
              MOVE 0 TO IND-B03-DT-NASC-1O-ASSTO
           END-IF
           IF B03-SEX-1O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-SEX-1O-ASSTO
           ELSE
              MOVE 0 TO IND-B03-SEX-1O-ASSTO
           END-IF
           IF B03-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ETA-AA-1O-ASSTO
           ELSE
              MOVE 0 TO IND-B03-ETA-AA-1O-ASSTO
           END-IF
           IF B03-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ETA-MM-1O-ASSTO
           ELSE
              MOVE 0 TO IND-B03-ETA-MM-1O-ASSTO
           END-IF
           IF B03-ETA-RAGGN-DT-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ETA-RAGGN-DT-CALC
           ELSE
              MOVE 0 TO IND-B03-ETA-RAGGN-DT-CALC
           END-IF
           IF B03-DUR-AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-AA
           ELSE
              MOVE 0 TO IND-B03-DUR-AA
           END-IF
           IF B03-DUR-MM-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-MM
           ELSE
              MOVE 0 TO IND-B03-DUR-MM
           END-IF
           IF B03-DUR-GG-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-GG
           ELSE
              MOVE 0 TO IND-B03-DUR-GG
           END-IF
           IF B03-DUR-1O-PER-AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-1O-PER-AA
           ELSE
              MOVE 0 TO IND-B03-DUR-1O-PER-AA
           END-IF
           IF B03-DUR-1O-PER-MM-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-1O-PER-MM
           ELSE
              MOVE 0 TO IND-B03-DUR-1O-PER-MM
           END-IF
           IF B03-DUR-1O-PER-GG-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-1O-PER-GG
           ELSE
              MOVE 0 TO IND-B03-DUR-1O-PER-GG
           END-IF
           IF B03-ANTIDUR-RICOR-PREC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ANTIDUR-RICOR-PREC
           ELSE
              MOVE 0 TO IND-B03-ANTIDUR-RICOR-PREC
           END-IF
           IF B03-ANTIDUR-DT-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ANTIDUR-DT-CALC
           ELSE
              MOVE 0 TO IND-B03-ANTIDUR-DT-CALC
           END-IF
           IF B03-DUR-RES-DT-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-RES-DT-CALC
           ELSE
              MOVE 0 TO IND-B03-DUR-RES-DT-CALC
           END-IF
           IF B03-DT-EFF-CAMB-STAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-EFF-CAMB-STAT
           ELSE
              MOVE 0 TO IND-B03-DT-EFF-CAMB-STAT
           END-IF
           IF B03-DT-EMIS-CAMB-STAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-EMIS-CAMB-STAT
           ELSE
              MOVE 0 TO IND-B03-DT-EMIS-CAMB-STAT
           END-IF
           IF B03-DT-EFF-STAB-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-EFF-STAB
           ELSE
              MOVE 0 TO IND-B03-DT-EFF-STAB
           END-IF
           IF B03-CPT-DT-STAB-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CPT-DT-STAB
           ELSE
              MOVE 0 TO IND-B03-CPT-DT-STAB
           END-IF
           IF B03-DT-EFF-RIDZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-EFF-RIDZ
           ELSE
              MOVE 0 TO IND-B03-DT-EFF-RIDZ
           END-IF
           IF B03-DT-EMIS-RIDZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-EMIS-RIDZ
           ELSE
              MOVE 0 TO IND-B03-DT-EMIS-RIDZ
           END-IF
           IF B03-CPT-DT-RIDZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CPT-DT-RIDZ
           ELSE
              MOVE 0 TO IND-B03-CPT-DT-RIDZ
           END-IF
           IF B03-FRAZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FRAZ
           ELSE
              MOVE 0 TO IND-B03-FRAZ
           END-IF
           IF B03-DUR-PAG-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-PAG-PRE
           ELSE
              MOVE 0 TO IND-B03-DUR-PAG-PRE
           END-IF
           IF B03-NUM-PRE-PATT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-NUM-PRE-PATT
           ELSE
              MOVE 0 TO IND-B03-NUM-PRE-PATT
           END-IF
           IF B03-FRAZ-INI-EROG-REN-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FRAZ-INI-EROG-REN
           ELSE
              MOVE 0 TO IND-B03-FRAZ-INI-EROG-REN
           END-IF
           IF B03-AA-REN-CER-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-AA-REN-CER
           ELSE
              MOVE 0 TO IND-B03-AA-REN-CER
           END-IF
           IF B03-RAT-REN-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RAT-REN
           ELSE
              MOVE 0 TO IND-B03-RAT-REN
           END-IF
           IF B03-COD-DIV-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-DIV
           ELSE
              MOVE 0 TO IND-B03-COD-DIV
           END-IF
           IF B03-RISCPAR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RISCPAR
           ELSE
              MOVE 0 TO IND-B03-RISCPAR
           END-IF
           IF B03-CUM-RISCPAR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CUM-RISCPAR
           ELSE
              MOVE 0 TO IND-B03-CUM-RISCPAR
           END-IF
           IF B03-ULT-RM-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ULT-RM
           ELSE
              MOVE 0 TO IND-B03-ULT-RM
           END-IF
           IF B03-TS-RENDTO-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TS-RENDTO-T
           ELSE
              MOVE 0 TO IND-B03-TS-RENDTO-T
           END-IF
           IF B03-ALQ-RETR-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ALQ-RETR-T
           ELSE
              MOVE 0 TO IND-B03-ALQ-RETR-T
           END-IF
           IF B03-MIN-TRNUT-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-MIN-TRNUT-T
           ELSE
              MOVE 0 TO IND-B03-MIN-TRNUT-T
           END-IF
           IF B03-TS-NET-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TS-NET-T
           ELSE
              MOVE 0 TO IND-B03-TS-NET-T
           END-IF
           IF B03-DT-ULT-RIVAL-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-ULT-RIVAL
           ELSE
              MOVE 0 TO IND-B03-DT-ULT-RIVAL
           END-IF
           IF B03-PRSTZ-INI-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRSTZ-INI
           ELSE
              MOVE 0 TO IND-B03-PRSTZ-INI
           END-IF
           IF B03-PRSTZ-AGG-INI-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRSTZ-AGG-INI
           ELSE
              MOVE 0 TO IND-B03-PRSTZ-AGG-INI
           END-IF
           IF B03-PRSTZ-AGG-ULT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRSTZ-AGG-ULT
           ELSE
              MOVE 0 TO IND-B03-PRSTZ-AGG-ULT
           END-IF
           IF B03-RAPPEL-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RAPPEL
           ELSE
              MOVE 0 TO IND-B03-RAPPEL
           END-IF
           IF B03-PRE-PATTUITO-INI-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-PATTUITO-INI
           ELSE
              MOVE 0 TO IND-B03-PRE-PATTUITO-INI
           END-IF
           IF B03-PRE-DOV-INI-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-DOV-INI
           ELSE
              MOVE 0 TO IND-B03-PRE-DOV-INI
           END-IF
           IF B03-PRE-DOV-RIVTO-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-DOV-RIVTO-T
           ELSE
              MOVE 0 TO IND-B03-PRE-DOV-RIVTO-T
           END-IF
           IF B03-PRE-ANNUALIZ-RICOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-ANNUALIZ-RICOR
           ELSE
              MOVE 0 TO IND-B03-PRE-ANNUALIZ-RICOR
           END-IF
           IF B03-PRE-CONT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-CONT
           ELSE
              MOVE 0 TO IND-B03-PRE-CONT
           END-IF
           IF B03-PRE-PP-INI-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-PP-INI
           ELSE
              MOVE 0 TO IND-B03-PRE-PP-INI
           END-IF
           IF B03-RIS-PURA-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RIS-PURA-T
           ELSE
              MOVE 0 TO IND-B03-RIS-PURA-T
           END-IF
           IF B03-PROV-ACQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PROV-ACQ
           ELSE
              MOVE 0 TO IND-B03-PROV-ACQ
           END-IF
           IF B03-PROV-ACQ-RICOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PROV-ACQ-RICOR
           ELSE
              MOVE 0 TO IND-B03-PROV-ACQ-RICOR
           END-IF
           IF B03-PROV-INC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PROV-INC
           ELSE
              MOVE 0 TO IND-B03-PROV-INC
           END-IF
           IF B03-CAR-ACQ-NON-SCON-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CAR-ACQ-NON-SCON
           ELSE
              MOVE 0 TO IND-B03-CAR-ACQ-NON-SCON
           END-IF
           IF B03-OVER-COMM-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-OVER-COMM
           ELSE
              MOVE 0 TO IND-B03-OVER-COMM
           END-IF
           IF B03-CAR-ACQ-PRECONTATO-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CAR-ACQ-PRECONTATO
           ELSE
              MOVE 0 TO IND-B03-CAR-ACQ-PRECONTATO
           END-IF
           IF B03-RIS-ACQ-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RIS-ACQ-T
           ELSE
              MOVE 0 TO IND-B03-RIS-ACQ-T
           END-IF
           IF B03-RIS-ZIL-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RIS-ZIL-T
           ELSE
              MOVE 0 TO IND-B03-RIS-ZIL-T
           END-IF
           IF B03-CAR-GEST-NON-SCON-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CAR-GEST-NON-SCON
           ELSE
              MOVE 0 TO IND-B03-CAR-GEST-NON-SCON
           END-IF
           IF B03-CAR-GEST-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CAR-GEST
           ELSE
              MOVE 0 TO IND-B03-CAR-GEST
           END-IF
           IF B03-RIS-SPE-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RIS-SPE-T
           ELSE
              MOVE 0 TO IND-B03-RIS-SPE-T
           END-IF
           IF B03-CAR-INC-NON-SCON-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CAR-INC-NON-SCON
           ELSE
              MOVE 0 TO IND-B03-CAR-INC-NON-SCON
           END-IF
           IF B03-CAR-INC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CAR-INC
           ELSE
              MOVE 0 TO IND-B03-CAR-INC
           END-IF
           IF B03-RIS-RISTORNI-CAP-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RIS-RISTORNI-CAP
           ELSE
              MOVE 0 TO IND-B03-RIS-RISTORNI-CAP
           END-IF
           IF B03-INTR-TECN-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-INTR-TECN
           ELSE
              MOVE 0 TO IND-B03-INTR-TECN
           END-IF
           IF B03-CPT-RSH-MOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CPT-RSH-MOR
           ELSE
              MOVE 0 TO IND-B03-CPT-RSH-MOR
           END-IF
           IF B03-C-SUBRSH-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-C-SUBRSH-T
           ELSE
              MOVE 0 TO IND-B03-C-SUBRSH-T
           END-IF
           IF B03-PRE-RSH-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-RSH-T
           ELSE
              MOVE 0 TO IND-B03-PRE-RSH-T
           END-IF
           IF B03-ALQ-MARG-RIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ALQ-MARG-RIS
           ELSE
              MOVE 0 TO IND-B03-ALQ-MARG-RIS
           END-IF
           IF B03-ALQ-MARG-C-SUBRSH-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ALQ-MARG-C-SUBRSH
           ELSE
              MOVE 0 TO IND-B03-ALQ-MARG-C-SUBRSH
           END-IF
           IF B03-TS-RENDTO-SPPR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TS-RENDTO-SPPR
           ELSE
              MOVE 0 TO IND-B03-TS-RENDTO-SPPR
           END-IF
           IF B03-TP-IAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-IAS
           ELSE
              MOVE 0 TO IND-B03-TP-IAS
           END-IF
           IF B03-NS-QUO-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-NS-QUO
           ELSE
              MOVE 0 TO IND-B03-NS-QUO
           END-IF
           IF B03-TS-MEDIO-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TS-MEDIO
           ELSE
              MOVE 0 TO IND-B03-TS-MEDIO
           END-IF
           IF B03-CPT-RIASTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CPT-RIASTO
           ELSE
              MOVE 0 TO IND-B03-CPT-RIASTO
           END-IF
           IF B03-PRE-RIASTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-RIASTO
           ELSE
              MOVE 0 TO IND-B03-PRE-RIASTO
           END-IF
           IF B03-RIS-RIASTA-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RIS-RIASTA
           ELSE
              MOVE 0 TO IND-B03-RIS-RIASTA
           END-IF
           IF B03-CPT-RIASTO-ECC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CPT-RIASTO-ECC
           ELSE
              MOVE 0 TO IND-B03-CPT-RIASTO-ECC
           END-IF
           IF B03-PRE-RIASTO-ECC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-RIASTO-ECC
           ELSE
              MOVE 0 TO IND-B03-PRE-RIASTO-ECC
           END-IF
           IF B03-RIS-RIASTA-ECC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RIS-RIASTA-ECC
           ELSE
              MOVE 0 TO IND-B03-RIS-RIASTA-ECC
           END-IF
           IF B03-COD-AGE-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-AGE
           ELSE
              MOVE 0 TO IND-B03-COD-AGE
           END-IF
           IF B03-COD-SUBAGE-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-SUBAGE
           ELSE
              MOVE 0 TO IND-B03-COD-SUBAGE
           END-IF
           IF B03-COD-CAN-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-CAN
           ELSE
              MOVE 0 TO IND-B03-COD-CAN
           END-IF
           IF B03-IB-POLI-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-IB-POLI
           ELSE
              MOVE 0 TO IND-B03-IB-POLI
           END-IF
           IF B03-IB-ADES-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-IB-ADES
           ELSE
              MOVE 0 TO IND-B03-IB-ADES
           END-IF
           IF B03-IB-TRCH-DI-GAR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-IB-TRCH-DI-GAR
           ELSE
              MOVE 0 TO IND-B03-IB-TRCH-DI-GAR
           END-IF
           IF B03-TP-PRSTZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-PRSTZ
           ELSE
              MOVE 0 TO IND-B03-TP-PRSTZ
           END-IF
           IF B03-TP-TRASF-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-TRASF
           ELSE
              MOVE 0 TO IND-B03-TP-TRASF
           END-IF
           IF B03-PP-INVRIO-TARI-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PP-INVRIO-TARI
           ELSE
              MOVE 0 TO IND-B03-PP-INVRIO-TARI
           END-IF
           IF B03-COEFF-OPZ-REN-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COEFF-OPZ-REN
           ELSE
              MOVE 0 TO IND-B03-COEFF-OPZ-REN
           END-IF
           IF B03-COEFF-OPZ-CPT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COEFF-OPZ-CPT
           ELSE
              MOVE 0 TO IND-B03-COEFF-OPZ-CPT
           END-IF
           IF B03-DUR-PAG-REN-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-PAG-REN
           ELSE
              MOVE 0 TO IND-B03-DUR-PAG-REN
           END-IF
           IF B03-VLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-VLT
           ELSE
              MOVE 0 TO IND-B03-VLT
           END-IF
           IF B03-RIS-MAT-CHIU-PREC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RIS-MAT-CHIU-PREC
           ELSE
              MOVE 0 TO IND-B03-RIS-MAT-CHIU-PREC
           END-IF
           IF B03-COD-FND-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-FND
           ELSE
              MOVE 0 TO IND-B03-COD-FND
           END-IF
           IF B03-PRSTZ-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRSTZ-T
           ELSE
              MOVE 0 TO IND-B03-PRSTZ-T
           END-IF
           IF B03-TS-TARI-DOV-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TS-TARI-DOV
           ELSE
              MOVE 0 TO IND-B03-TS-TARI-DOV
           END-IF
           IF B03-TS-TARI-SCON-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TS-TARI-SCON
           ELSE
              MOVE 0 TO IND-B03-TS-TARI-SCON
           END-IF
           IF B03-TS-PP-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TS-PP
           ELSE
              MOVE 0 TO IND-B03-TS-PP
           END-IF
           IF B03-COEFF-RIS-1-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COEFF-RIS-1-T
           ELSE
              MOVE 0 TO IND-B03-COEFF-RIS-1-T
           END-IF
           IF B03-COEFF-RIS-2-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COEFF-RIS-2-T
           ELSE
              MOVE 0 TO IND-B03-COEFF-RIS-2-T
           END-IF
           IF B03-ABB-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ABB
           ELSE
              MOVE 0 TO IND-B03-ABB
           END-IF
           IF B03-TP-COASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-COASS
           ELSE
              MOVE 0 TO IND-B03-TP-COASS
           END-IF
           IF B03-TRAT-RIASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TRAT-RIASS
           ELSE
              MOVE 0 TO IND-B03-TRAT-RIASS
           END-IF
           IF B03-TRAT-RIASS-ECC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TRAT-RIASS-ECC
           ELSE
              MOVE 0 TO IND-B03-TRAT-RIASS-ECC
           END-IF
           IF B03-TP-RGM-FISC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-RGM-FISC
           ELSE
              MOVE 0 TO IND-B03-TP-RGM-FISC
           END-IF
           IF B03-DUR-GAR-AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-GAR-AA
           ELSE
              MOVE 0 TO IND-B03-DUR-GAR-AA
           END-IF
           IF B03-DUR-GAR-MM-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-GAR-MM
           ELSE
              MOVE 0 TO IND-B03-DUR-GAR-MM
           END-IF
           IF B03-DUR-GAR-GG-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-GAR-GG
           ELSE
              MOVE 0 TO IND-B03-DUR-GAR-GG
           END-IF
           IF B03-ANTIDUR-CALC-365-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ANTIDUR-CALC-365
           ELSE
              MOVE 0 TO IND-B03-ANTIDUR-CALC-365
           END-IF
           IF B03-COD-FISC-CNTR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-FISC-CNTR
           ELSE
              MOVE 0 TO IND-B03-COD-FISC-CNTR
           END-IF
           IF B03-COD-FISC-ASSTO1-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-FISC-ASSTO1
           ELSE
              MOVE 0 TO IND-B03-COD-FISC-ASSTO1
           END-IF
           IF B03-COD-FISC-ASSTO2-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-FISC-ASSTO2
           ELSE
              MOVE 0 TO IND-B03-COD-FISC-ASSTO2
           END-IF
           IF B03-COD-FISC-ASSTO3-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-FISC-ASSTO3
           ELSE
              MOVE 0 TO IND-B03-COD-FISC-ASSTO3
           END-IF
           IF B03-CAUS-SCON = HIGH-VALUES
              MOVE -1 TO IND-B03-CAUS-SCON
           ELSE
              MOVE 0 TO IND-B03-CAUS-SCON
           END-IF
           IF B03-EMIT-TIT-OPZ = HIGH-VALUES
              MOVE -1 TO IND-B03-EMIT-TIT-OPZ
           ELSE
              MOVE 0 TO IND-B03-EMIT-TIT-OPZ
           END-IF
           IF B03-QTZ-SP-Z-COUP-EMIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-QTZ-SP-Z-COUP-EMIS
           ELSE
              MOVE 0 TO IND-B03-QTZ-SP-Z-COUP-EMIS
           END-IF
           IF B03-QTZ-SP-Z-OPZ-EMIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-QTZ-SP-Z-OPZ-EMIS
           ELSE
              MOVE 0 TO IND-B03-QTZ-SP-Z-OPZ-EMIS
           END-IF
           IF B03-QTZ-SP-Z-COUP-DT-C-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-QTZ-SP-Z-COUP-DT-C
           ELSE
              MOVE 0 TO IND-B03-QTZ-SP-Z-COUP-DT-C
           END-IF
           IF B03-QTZ-SP-Z-OPZ-DT-CA-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-QTZ-SP-Z-OPZ-DT-CA
           ELSE
              MOVE 0 TO IND-B03-QTZ-SP-Z-OPZ-DT-CA
           END-IF
           IF B03-QTZ-TOT-EMIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-QTZ-TOT-EMIS
           ELSE
              MOVE 0 TO IND-B03-QTZ-TOT-EMIS
           END-IF
           IF B03-QTZ-TOT-DT-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-QTZ-TOT-DT-CALC
           ELSE
              MOVE 0 TO IND-B03-QTZ-TOT-DT-CALC
           END-IF
           IF B03-QTZ-TOT-DT-ULT-BIL-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-QTZ-TOT-DT-ULT-BIL
           ELSE
              MOVE 0 TO IND-B03-QTZ-TOT-DT-ULT-BIL
           END-IF
           IF B03-DT-QTZ-EMIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-QTZ-EMIS
           ELSE
              MOVE 0 TO IND-B03-DT-QTZ-EMIS
           END-IF
           IF B03-PC-CAR-GEST-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PC-CAR-GEST
           ELSE
              MOVE 0 TO IND-B03-PC-CAR-GEST
           END-IF
           IF B03-PC-CAR-ACQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PC-CAR-ACQ
           ELSE
              MOVE 0 TO IND-B03-PC-CAR-ACQ
           END-IF
           IF B03-IMP-CAR-CASO-MOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-IMP-CAR-CASO-MOR
           ELSE
              MOVE 0 TO IND-B03-IMP-CAR-CASO-MOR
           END-IF
           IF B03-PC-CAR-MOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PC-CAR-MOR
           ELSE
              MOVE 0 TO IND-B03-PC-CAR-MOR
           END-IF
           IF B03-TP-VERS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-VERS
           ELSE
              MOVE 0 TO IND-B03-TP-VERS
           END-IF
           IF B03-FL-SWITCH-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FL-SWITCH
           ELSE
              MOVE 0 TO IND-B03-FL-SWITCH
           END-IF
           IF B03-FL-IAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FL-IAS
           ELSE
              MOVE 0 TO IND-B03-FL-IAS
           END-IF
           IF B03-DIR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DIR
           ELSE
              MOVE 0 TO IND-B03-DIR
           END-IF
           IF B03-TP-COP-CASO-MOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-COP-CASO-MOR
           ELSE
              MOVE 0 TO IND-B03-TP-COP-CASO-MOR
           END-IF
           IF B03-MET-RISC-SPCL-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-MET-RISC-SPCL
           ELSE
              MOVE 0 TO IND-B03-MET-RISC-SPCL
           END-IF
           IF B03-TP-STAT-INVST-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-STAT-INVST
           ELSE
              MOVE 0 TO IND-B03-TP-STAT-INVST
           END-IF
           IF B03-COD-PRDT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-PRDT
           ELSE
              MOVE 0 TO IND-B03-COD-PRDT
           END-IF
           IF B03-STAT-ASSTO-1-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-STAT-ASSTO-1
           ELSE
              MOVE 0 TO IND-B03-STAT-ASSTO-1
           END-IF
           IF B03-STAT-ASSTO-2-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-STAT-ASSTO-2
           ELSE
              MOVE 0 TO IND-B03-STAT-ASSTO-2
           END-IF
           IF B03-STAT-ASSTO-3-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-STAT-ASSTO-3
           ELSE
              MOVE 0 TO IND-B03-STAT-ASSTO-3
           END-IF
           IF B03-CPT-ASSTO-INI-MOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CPT-ASSTO-INI-MOR
           ELSE
              MOVE 0 TO IND-B03-CPT-ASSTO-INI-MOR
           END-IF
           IF B03-TS-STAB-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TS-STAB-PRE
           ELSE
              MOVE 0 TO IND-B03-TS-STAB-PRE
           END-IF
           IF B03-DIR-EMIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DIR-EMIS
           ELSE
              MOVE 0 TO IND-B03-DIR-EMIS
           END-IF
           IF B03-DT-INC-ULT-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-INC-ULT-PRE
           ELSE
              MOVE 0 TO IND-B03-DT-INC-ULT-PRE
           END-IF
           IF B03-STAT-TBGC-ASSTO-1-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-STAT-TBGC-ASSTO-1
           ELSE
              MOVE 0 TO IND-B03-STAT-TBGC-ASSTO-1
           END-IF
           IF B03-STAT-TBGC-ASSTO-2-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-STAT-TBGC-ASSTO-2
           ELSE
              MOVE 0 TO IND-B03-STAT-TBGC-ASSTO-2
           END-IF
           IF B03-STAT-TBGC-ASSTO-3-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-STAT-TBGC-ASSTO-3
           ELSE
              MOVE 0 TO IND-B03-STAT-TBGC-ASSTO-3
           END-IF
           IF B03-FRAZ-DECR-CPT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FRAZ-DECR-CPT
           ELSE
              MOVE 0 TO IND-B03-FRAZ-DECR-CPT
           END-IF
           IF B03-PRE-PP-ULT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-PP-ULT
           ELSE
              MOVE 0 TO IND-B03-PRE-PP-ULT
           END-IF
           IF B03-ACQ-EXP-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ACQ-EXP
           ELSE
              MOVE 0 TO IND-B03-ACQ-EXP
           END-IF
           IF B03-REMUN-ASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-REMUN-ASS
           ELSE
              MOVE 0 TO IND-B03-REMUN-ASS
           END-IF
           IF B03-COMMIS-INTER-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COMMIS-INTER
           ELSE
              MOVE 0 TO IND-B03-COMMIS-INTER
           END-IF
           IF B03-NUM-FINANZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-NUM-FINANZ
           ELSE
              MOVE 0 TO IND-B03-NUM-FINANZ
           END-IF
           IF B03-TP-ACC-COMM-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-ACC-COMM
           ELSE
              MOVE 0 TO IND-B03-TP-ACC-COMM
           END-IF
           IF B03-IB-ACC-COMM-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-IB-ACC-COMM
           ELSE
              MOVE 0 TO IND-B03-IB-ACC-COMM
           END-IF
           IF B03-CARZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CARZ
           ELSE
              MOVE 0 TO IND-B03-CARZ
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.

           CONTINUE.
       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.
           CONTINUE.
       Z500-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.
           CONTINUE.
       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.

           MOVE B03-DT-RIS TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO B03-DT-RIS-DB
           MOVE B03-DT-PRODUZIONE TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO B03-DT-PRODUZIONE-DB
           IF IND-B03-DT-INI-VAL-TAR = 0
               MOVE B03-DT-INI-VAL-TAR TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-INI-VAL-TAR-DB
           END-IF
           MOVE B03-DT-INI-VLDT-PROD TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO B03-DT-INI-VLDT-PROD-DB
           MOVE B03-DT-DECOR-POLI TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO B03-DT-DECOR-POLI-DB
           IF IND-B03-DT-DECOR-ADES = 0
               MOVE B03-DT-DECOR-ADES TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-DECOR-ADES-DB
           END-IF
           MOVE B03-DT-DECOR-TRCH TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO B03-DT-DECOR-TRCH-DB
           MOVE B03-DT-EMIS-POLI TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO B03-DT-EMIS-POLI-DB
           IF IND-B03-DT-EMIS-TRCH = 0
               MOVE B03-DT-EMIS-TRCH TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-EMIS-TRCH-DB
           END-IF
           IF IND-B03-DT-SCAD-TRCH = 0
               MOVE B03-DT-SCAD-TRCH TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-SCAD-TRCH-DB
           END-IF
           IF IND-B03-DT-SCAD-INTMD = 0
               MOVE B03-DT-SCAD-INTMD TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-SCAD-INTMD-DB
           END-IF
           IF IND-B03-DT-SCAD-PAG-PRE = 0
               MOVE B03-DT-SCAD-PAG-PRE TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-SCAD-PAG-PRE-DB
           END-IF
           IF IND-B03-DT-ULT-PRE-PAG = 0
               MOVE B03-DT-ULT-PRE-PAG TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-ULT-PRE-PAG-DB
           END-IF
           IF IND-B03-DT-NASC-1O-ASSTO = 0
               MOVE B03-DT-NASC-1O-ASSTO TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-NASC-1O-ASSTO-DB
           END-IF
           IF IND-B03-DT-EFF-CAMB-STAT = 0
               MOVE B03-DT-EFF-CAMB-STAT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-EFF-CAMB-STAT-DB
           END-IF
           IF IND-B03-DT-EMIS-CAMB-STAT = 0
               MOVE B03-DT-EMIS-CAMB-STAT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-EMIS-CAMB-STAT-DB
           END-IF
           IF IND-B03-DT-EFF-STAB = 0
               MOVE B03-DT-EFF-STAB TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-EFF-STAB-DB
           END-IF
           IF IND-B03-DT-EFF-RIDZ = 0
               MOVE B03-DT-EFF-RIDZ TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-EFF-RIDZ-DB
           END-IF
           IF IND-B03-DT-EMIS-RIDZ = 0
               MOVE B03-DT-EMIS-RIDZ TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-EMIS-RIDZ-DB
           END-IF
           IF IND-B03-DT-ULT-RIVAL = 0
               MOVE B03-DT-ULT-RIVAL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-ULT-RIVAL-DB
           END-IF
           IF IND-B03-DT-QTZ-EMIS = 0
               MOVE B03-DT-QTZ-EMIS TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-QTZ-EMIS-DB
           END-IF
           IF IND-B03-DT-INC-ULT-PRE = 0
               MOVE B03-DT-INC-ULT-PRE TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-INC-ULT-PRE-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.
           CONTINUE.


       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

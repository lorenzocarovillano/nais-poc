      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0145.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0145
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0145'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01 IX-TAB-TGA                       PIC 9(4) COMP VALUE ZEROES.
      *----------------------------------------------------------------*
      *    COPY DI INPUT PER LA CHIAMATA AL LVVS0000
      *----------------------------------------------------------------*
       01  INPUT-LVVS0000.
           COPY LVVC0000        REPLACING ==(SF)== BY ==LVVC0000==.
      *---------------------------------------------------------------*
      *  COPY PER CHIAMATA LDBS5480
      *---------------------------------------------------------------*
           COPY LDBV2271.
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-LIQUID.
          03 DLQU-AREA-LIQ.
             04 DLQU-ELE-LIQ-MAX        PIC S9(04) COMP.
                COPY LCCVLQUB REPLACING   ==(SF)==  BY ==DLQU==.
             COPY LCCVLQU1              REPLACING ==(SF)== BY ==DLQU==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0145.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0145.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE AREA-IO-LIQUID.
      *
      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.
      *
           IF IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
              PERFORM S1251-CALCOLA-TOT            THRU S1251-EX
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-LIQUIDAZ
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DLQU-AREA-LIQ
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA DATA 2
      *----------------------------------------------------------------*
       S1251-CALCOLA-TOT.
      *
           INITIALIZE AREA-LDBV2271.
           MOVE IVVC0213-ID-ADESIONE           TO LDBV2271-ID-OGG.
           MOVE 'AD'                           TO LDBV2271-TP-OGG.
           MOVE 'CE'                           TO LDBV2271-TP-LIQ.
      *
           MOVE 'LDBS2270'                     TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 AREA-LDBV2271
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS5480 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE LDBV2271-IMP-LRD-LIQTO
                TO IVVC0213-VAL-IMP-O
           ELSE
              IF IDSV0003-SQLCODE = +100
              OR IDSV0003-SQLCODE = -305
                 SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                 MOVE WK-PGM
                  TO IDSV0003-COD-SERVIZIO-BE
                MOVE 'LVVS0145 - TOTALE NON VALORIZZATO'
                  TO IDSV0003-DESCRIZ-ERR-DB2
              ELSE
                 SET IDSV0003-INVALID-OPER        TO TRUE
                 MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
                 STRING 'CHIAMATA LDBS5480 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                     DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                 END-STRING
              END-IF
           END-IF.
      *
       S1251-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.

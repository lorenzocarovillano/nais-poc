       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LDBS2770.
       AUTHOR.        AISS.
       DATE-WRITTEN.  29 LUG 2008.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDB030 END-EXEC.
           EXEC SQL INCLUDE IDBVB032 END-EXEC.
           EXEC SQL INCLUDE IDBVB033 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVB031 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 BILA-TRCH-ESTR.

           PERFORM A000-INIZIO              THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-WHERE-CONDITION
                           PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                      SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSB030'       TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'BILA_TRCH_ESTR' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE   OR
                        IDSV0003-UPDATE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.

       C200-ELABORA-WC-NST.
           EVALUATE TRUE
              WHEN IDSV0003-UPDATE
                 PERFORM C210-UPDATE-WC-NST          THRU C210-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       C200-EX.
           EXIT.

       C210-UPDATE-WC-NST.

           PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE BILA_TRCH_ESTR
                SET
                     TP_STAT_BUS_TRCH   = :B03-TP-STAT-BUS-TRCH

                    ,TP_CAUS_TRCH       = :B03-TP-CAUS-TRCH

                    ,TP_STAT_BUS_POLI   = :B03-TP-STAT-BUS-POLI

                    ,TP_CAUS_POLI       = :B03-TP-CAUS-POLI

                    ,TP_STAT_BUS_ADES   = :B03-TP-STAT-BUS-ADES

                    ,TP_CAUS_ADES       = :B03-TP-CAUS-ADES

                    ,ID_RICH_ESTRAZ_AGG = :B03-ID-RICH-ESTRAZ-AGG
                                          :IND-B03-ID-RICH-ESTRAZ-AGG
                WHERE  ID_RICH_ESTRAZ_MAS = :B03-ID-RICH-ESTRAZ-MAS
                AND    ID_ADES            = :B03-ID-ADES
                AND    ID_POLI            = :B03-ID-POLI

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       C210-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           CONTINUE.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES.

           MOVE IDSV0003-OPERAZIONE TO B03-DS-OPER-SQL
           MOVE 1                   TO B03-DS-VER
           MOVE IDSV0003-USER-NAME TO B03-DS-UTENTE
           MOVE '1'                   TO B03-DS-STATO-ELAB
           MOVE WS-TS-COMPETENZA-AGG-STOR TO B03-DS-TS-CPTZ.

       Z150-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.

           IF IND-B03-ID-RICH-ESTRAZ-AGG = -1
              MOVE HIGH-VALUES TO B03-ID-RICH-ESTRAZ-AGG-NULL
           END-IF
           IF IND-B03-FL-SIMULAZIONE = -1
              MOVE HIGH-VALUES TO B03-FL-SIMULAZIONE-NULL
           END-IF
           IF IND-B03-DT-INI-VAL-TAR = -1
              MOVE HIGH-VALUES TO B03-DT-INI-VAL-TAR-NULL
           END-IF
           IF IND-B03-COD-PROD = -1
              MOVE HIGH-VALUES TO B03-COD-PROD-NULL
           END-IF
           IF IND-B03-COD-TARI-ORGN = -1
              MOVE HIGH-VALUES TO B03-COD-TARI-ORGN-NULL
           END-IF
           IF IND-B03-MIN-GARTO-T = -1
              MOVE HIGH-VALUES TO B03-MIN-GARTO-T-NULL
           END-IF
           IF IND-B03-TP-TARI = -1
              MOVE HIGH-VALUES TO B03-TP-TARI-NULL
           END-IF
           IF IND-B03-TP-PRE = -1
              MOVE HIGH-VALUES TO B03-TP-PRE-NULL
           END-IF
           IF IND-B03-TP-ADEG-PRE = -1
              MOVE HIGH-VALUES TO B03-TP-ADEG-PRE-NULL
           END-IF
           IF IND-B03-TP-RIVAL = -1
              MOVE HIGH-VALUES TO B03-TP-RIVAL-NULL
           END-IF
           IF IND-B03-FL-DA-TRASF = -1
              MOVE HIGH-VALUES TO B03-FL-DA-TRASF-NULL
           END-IF
           IF IND-B03-FL-CAR-CONT = -1
              MOVE HIGH-VALUES TO B03-FL-CAR-CONT-NULL
           END-IF
           IF IND-B03-FL-PRE-DA-RIS = -1
              MOVE HIGH-VALUES TO B03-FL-PRE-DA-RIS-NULL
           END-IF
           IF IND-B03-FL-PRE-AGG = -1
              MOVE HIGH-VALUES TO B03-FL-PRE-AGG-NULL
           END-IF
           IF IND-B03-TP-TRCH = -1
              MOVE HIGH-VALUES TO B03-TP-TRCH-NULL
           END-IF
           IF IND-B03-TP-TST = -1
              MOVE HIGH-VALUES TO B03-TP-TST-NULL
           END-IF
           IF IND-B03-COD-CONV = -1
              MOVE HIGH-VALUES TO B03-COD-CONV-NULL
           END-IF
           IF IND-B03-DT-DECOR-ADES = -1
              MOVE HIGH-VALUES TO B03-DT-DECOR-ADES-NULL
           END-IF
           IF IND-B03-DT-EMIS-TRCH = -1
              MOVE HIGH-VALUES TO B03-DT-EMIS-TRCH-NULL
           END-IF
           IF IND-B03-DT-SCAD-TRCH = -1
              MOVE HIGH-VALUES TO B03-DT-SCAD-TRCH-NULL
           END-IF
           IF IND-B03-DT-SCAD-INTMD = -1
              MOVE HIGH-VALUES TO B03-DT-SCAD-INTMD-NULL
           END-IF
           IF IND-B03-DT-SCAD-PAG-PRE = -1
              MOVE HIGH-VALUES TO B03-DT-SCAD-PAG-PRE-NULL
           END-IF
           IF IND-B03-DT-ULT-PRE-PAG = -1
              MOVE HIGH-VALUES TO B03-DT-ULT-PRE-PAG-NULL
           END-IF
           IF IND-B03-DT-NASC-1O-ASSTO = -1
              MOVE HIGH-VALUES TO B03-DT-NASC-1O-ASSTO-NULL
           END-IF
           IF IND-B03-SEX-1O-ASSTO = -1
              MOVE HIGH-VALUES TO B03-SEX-1O-ASSTO-NULL
           END-IF
           IF IND-B03-ETA-AA-1O-ASSTO = -1
              MOVE HIGH-VALUES TO B03-ETA-AA-1O-ASSTO-NULL
           END-IF
           IF IND-B03-ETA-MM-1O-ASSTO = -1
              MOVE HIGH-VALUES TO B03-ETA-MM-1O-ASSTO-NULL
           END-IF
           IF IND-B03-ETA-RAGGN-DT-CALC = -1
              MOVE HIGH-VALUES TO B03-ETA-RAGGN-DT-CALC-NULL
           END-IF
           IF IND-B03-DUR-AA = -1
              MOVE HIGH-VALUES TO B03-DUR-AA-NULL
           END-IF
           IF IND-B03-DUR-MM = -1
              MOVE HIGH-VALUES TO B03-DUR-MM-NULL
           END-IF
           IF IND-B03-DUR-GG = -1
              MOVE HIGH-VALUES TO B03-DUR-GG-NULL
           END-IF
           IF IND-B03-DUR-1O-PER-AA = -1
              MOVE HIGH-VALUES TO B03-DUR-1O-PER-AA-NULL
           END-IF
           IF IND-B03-DUR-1O-PER-MM = -1
              MOVE HIGH-VALUES TO B03-DUR-1O-PER-MM-NULL
           END-IF
           IF IND-B03-DUR-1O-PER-GG = -1
              MOVE HIGH-VALUES TO B03-DUR-1O-PER-GG-NULL
           END-IF
           IF IND-B03-ANTIDUR-RICOR-PREC = -1
              MOVE HIGH-VALUES TO B03-ANTIDUR-RICOR-PREC-NULL
           END-IF
           IF IND-B03-ANTIDUR-DT-CALC = -1
              MOVE HIGH-VALUES TO B03-ANTIDUR-DT-CALC-NULL
           END-IF
           IF IND-B03-DUR-RES-DT-CALC = -1
              MOVE HIGH-VALUES TO B03-DUR-RES-DT-CALC-NULL
           END-IF
           IF IND-B03-DT-EFF-CAMB-STAT = -1
              MOVE HIGH-VALUES TO B03-DT-EFF-CAMB-STAT-NULL
           END-IF
           IF IND-B03-DT-EMIS-CAMB-STAT = -1
              MOVE HIGH-VALUES TO B03-DT-EMIS-CAMB-STAT-NULL
           END-IF
           IF IND-B03-DT-EFF-STAB = -1
              MOVE HIGH-VALUES TO B03-DT-EFF-STAB-NULL
           END-IF
           IF IND-B03-CPT-DT-STAB = -1
              MOVE HIGH-VALUES TO B03-CPT-DT-STAB-NULL
           END-IF
           IF IND-B03-DT-EFF-RIDZ = -1
              MOVE HIGH-VALUES TO B03-DT-EFF-RIDZ-NULL
           END-IF
           IF IND-B03-DT-EMIS-RIDZ = -1
              MOVE HIGH-VALUES TO B03-DT-EMIS-RIDZ-NULL
           END-IF
           IF IND-B03-CPT-DT-RIDZ = -1
              MOVE HIGH-VALUES TO B03-CPT-DT-RIDZ-NULL
           END-IF
           IF IND-B03-FRAZ = -1
              MOVE HIGH-VALUES TO B03-FRAZ-NULL
           END-IF
           IF IND-B03-DUR-PAG-PRE = -1
              MOVE HIGH-VALUES TO B03-DUR-PAG-PRE-NULL
           END-IF
           IF IND-B03-NUM-PRE-PATT = -1
              MOVE HIGH-VALUES TO B03-NUM-PRE-PATT-NULL
           END-IF
           IF IND-B03-FRAZ-INI-EROG-REN = -1
              MOVE HIGH-VALUES TO B03-FRAZ-INI-EROG-REN-NULL
           END-IF
           IF IND-B03-AA-REN-CER = -1
              MOVE HIGH-VALUES TO B03-AA-REN-CER-NULL
           END-IF
           IF IND-B03-RAT-REN = -1
              MOVE HIGH-VALUES TO B03-RAT-REN-NULL
           END-IF
           IF IND-B03-COD-DIV = -1
              MOVE HIGH-VALUES TO B03-COD-DIV-NULL
           END-IF
           IF IND-B03-RISCPAR = -1
              MOVE HIGH-VALUES TO B03-RISCPAR-NULL
           END-IF
           IF IND-B03-CUM-RISCPAR = -1
              MOVE HIGH-VALUES TO B03-CUM-RISCPAR-NULL
           END-IF
           IF IND-B03-ULT-RM = -1
              MOVE HIGH-VALUES TO B03-ULT-RM-NULL
           END-IF
           IF IND-B03-TS-RENDTO-T = -1
              MOVE HIGH-VALUES TO B03-TS-RENDTO-T-NULL
           END-IF
           IF IND-B03-ALQ-RETR-T = -1
              MOVE HIGH-VALUES TO B03-ALQ-RETR-T-NULL
           END-IF
           IF IND-B03-MIN-TRNUT-T = -1
              MOVE HIGH-VALUES TO B03-MIN-TRNUT-T-NULL
           END-IF
           IF IND-B03-TS-NET-T = -1
              MOVE HIGH-VALUES TO B03-TS-NET-T-NULL
           END-IF
           IF IND-B03-DT-ULT-RIVAL = -1
              MOVE HIGH-VALUES TO B03-DT-ULT-RIVAL-NULL
           END-IF
           IF IND-B03-PRSTZ-INI = -1
              MOVE HIGH-VALUES TO B03-PRSTZ-INI-NULL
           END-IF
           IF IND-B03-PRSTZ-AGG-INI = -1
              MOVE HIGH-VALUES TO B03-PRSTZ-AGG-INI-NULL
           END-IF
           IF IND-B03-PRSTZ-AGG-ULT = -1
              MOVE HIGH-VALUES TO B03-PRSTZ-AGG-ULT-NULL
           END-IF
           IF IND-B03-RAPPEL = -1
              MOVE HIGH-VALUES TO B03-RAPPEL-NULL
           END-IF
           IF IND-B03-PRE-PATTUITO-INI = -1
              MOVE HIGH-VALUES TO B03-PRE-PATTUITO-INI-NULL
           END-IF
           IF IND-B03-PRE-DOV-INI = -1
              MOVE HIGH-VALUES TO B03-PRE-DOV-INI-NULL
           END-IF
           IF IND-B03-PRE-DOV-RIVTO-T = -1
              MOVE HIGH-VALUES TO B03-PRE-DOV-RIVTO-T-NULL
           END-IF
           IF IND-B03-PRE-ANNUALIZ-RICOR = -1
              MOVE HIGH-VALUES TO B03-PRE-ANNUALIZ-RICOR-NULL
           END-IF
           IF IND-B03-PRE-CONT = -1
              MOVE HIGH-VALUES TO B03-PRE-CONT-NULL
           END-IF
           IF IND-B03-PRE-PP-INI = -1
              MOVE HIGH-VALUES TO B03-PRE-PP-INI-NULL
           END-IF
           IF IND-B03-RIS-PURA-T = -1
              MOVE HIGH-VALUES TO B03-RIS-PURA-T-NULL
           END-IF
           IF IND-B03-PROV-ACQ = -1
              MOVE HIGH-VALUES TO B03-PROV-ACQ-NULL
           END-IF
           IF IND-B03-PROV-ACQ-RICOR = -1
              MOVE HIGH-VALUES TO B03-PROV-ACQ-RICOR-NULL
           END-IF
           IF IND-B03-PROV-INC = -1
              MOVE HIGH-VALUES TO B03-PROV-INC-NULL
           END-IF
           IF IND-B03-CAR-ACQ-NON-SCON = -1
              MOVE HIGH-VALUES TO B03-CAR-ACQ-NON-SCON-NULL
           END-IF
           IF IND-B03-OVER-COMM = -1
              MOVE HIGH-VALUES TO B03-OVER-COMM-NULL
           END-IF
           IF IND-B03-CAR-ACQ-PRECONTATO = -1
              MOVE HIGH-VALUES TO B03-CAR-ACQ-PRECONTATO-NULL
           END-IF
           IF IND-B03-RIS-ACQ-T = -1
              MOVE HIGH-VALUES TO B03-RIS-ACQ-T-NULL
           END-IF
           IF IND-B03-RIS-ZIL-T = -1
              MOVE HIGH-VALUES TO B03-RIS-ZIL-T-NULL
           END-IF
           IF IND-B03-CAR-GEST-NON-SCON = -1
              MOVE HIGH-VALUES TO B03-CAR-GEST-NON-SCON-NULL
           END-IF
           IF IND-B03-CAR-GEST = -1
              MOVE HIGH-VALUES TO B03-CAR-GEST-NULL
           END-IF
           IF IND-B03-RIS-SPE-T = -1
              MOVE HIGH-VALUES TO B03-RIS-SPE-T-NULL
           END-IF
           IF IND-B03-CAR-INC-NON-SCON = -1
              MOVE HIGH-VALUES TO B03-CAR-INC-NON-SCON-NULL
           END-IF
           IF IND-B03-CAR-INC = -1
              MOVE HIGH-VALUES TO B03-CAR-INC-NULL
           END-IF
           IF IND-B03-RIS-RISTORNI-CAP = -1
              MOVE HIGH-VALUES TO B03-RIS-RISTORNI-CAP-NULL
           END-IF
           IF IND-B03-INTR-TECN = -1
              MOVE HIGH-VALUES TO B03-INTR-TECN-NULL
           END-IF
           IF IND-B03-CPT-RSH-MOR = -1
              MOVE HIGH-VALUES TO B03-CPT-RSH-MOR-NULL
           END-IF
           IF IND-B03-C-SUBRSH-T = -1
              MOVE HIGH-VALUES TO B03-C-SUBRSH-T-NULL
           END-IF
           IF IND-B03-PRE-RSH-T = -1
              MOVE HIGH-VALUES TO B03-PRE-RSH-T-NULL
           END-IF
           IF IND-B03-ALQ-MARG-RIS = -1
              MOVE HIGH-VALUES TO B03-ALQ-MARG-RIS-NULL
           END-IF
           IF IND-B03-ALQ-MARG-C-SUBRSH = -1
              MOVE HIGH-VALUES TO B03-ALQ-MARG-C-SUBRSH-NULL
           END-IF
           IF IND-B03-TS-RENDTO-SPPR = -1
              MOVE HIGH-VALUES TO B03-TS-RENDTO-SPPR-NULL
           END-IF
           IF IND-B03-TP-IAS = -1
              MOVE HIGH-VALUES TO B03-TP-IAS-NULL
           END-IF
           IF IND-B03-NS-QUO = -1
              MOVE HIGH-VALUES TO B03-NS-QUO-NULL
           END-IF
           IF IND-B03-TS-MEDIO = -1
              MOVE HIGH-VALUES TO B03-TS-MEDIO-NULL
           END-IF
           IF IND-B03-CPT-RIASTO = -1
              MOVE HIGH-VALUES TO B03-CPT-RIASTO-NULL
           END-IF
           IF IND-B03-PRE-RIASTO = -1
              MOVE HIGH-VALUES TO B03-PRE-RIASTO-NULL
           END-IF
           IF IND-B03-RIS-RIASTA = -1
              MOVE HIGH-VALUES TO B03-RIS-RIASTA-NULL
           END-IF
           IF IND-B03-CPT-RIASTO-ECC = -1
              MOVE HIGH-VALUES TO B03-CPT-RIASTO-ECC-NULL
           END-IF
           IF IND-B03-PRE-RIASTO-ECC = -1
              MOVE HIGH-VALUES TO B03-PRE-RIASTO-ECC-NULL
           END-IF
           IF IND-B03-RIS-RIASTA-ECC = -1
              MOVE HIGH-VALUES TO B03-RIS-RIASTA-ECC-NULL
           END-IF
           IF IND-B03-COD-AGE = -1
              MOVE HIGH-VALUES TO B03-COD-AGE-NULL
           END-IF
           IF IND-B03-COD-SUBAGE = -1
              MOVE HIGH-VALUES TO B03-COD-SUBAGE-NULL
           END-IF
           IF IND-B03-COD-CAN = -1
              MOVE HIGH-VALUES TO B03-COD-CAN-NULL
           END-IF
           IF IND-B03-IB-POLI = -1
              MOVE HIGH-VALUES TO B03-IB-POLI-NULL
           END-IF
           IF IND-B03-IB-ADES = -1
              MOVE HIGH-VALUES TO B03-IB-ADES-NULL
           END-IF
           IF IND-B03-IB-TRCH-DI-GAR = -1
              MOVE HIGH-VALUES TO B03-IB-TRCH-DI-GAR-NULL
           END-IF
           IF IND-B03-TP-PRSTZ = -1
              MOVE HIGH-VALUES TO B03-TP-PRSTZ-NULL
           END-IF
           IF IND-B03-TP-TRASF = -1
              MOVE HIGH-VALUES TO B03-TP-TRASF-NULL
           END-IF
           IF IND-B03-PP-INVRIO-TARI = -1
              MOVE HIGH-VALUES TO B03-PP-INVRIO-TARI-NULL
           END-IF
           IF IND-B03-COEFF-OPZ-REN = -1
              MOVE HIGH-VALUES TO B03-COEFF-OPZ-REN-NULL
           END-IF
           IF IND-B03-COEFF-OPZ-CPT = -1
              MOVE HIGH-VALUES TO B03-COEFF-OPZ-CPT-NULL
           END-IF
           IF IND-B03-DUR-PAG-REN = -1
              MOVE HIGH-VALUES TO B03-DUR-PAG-REN-NULL
           END-IF
           IF IND-B03-VLT = -1
              MOVE HIGH-VALUES TO B03-VLT-NULL
           END-IF
           IF IND-B03-RIS-MAT-CHIU-PREC = -1
              MOVE HIGH-VALUES TO B03-RIS-MAT-CHIU-PREC-NULL
           END-IF
           IF IND-B03-COD-FND = -1
              MOVE HIGH-VALUES TO B03-COD-FND-NULL
           END-IF
           IF IND-B03-PRSTZ-T = -1
              MOVE HIGH-VALUES TO B03-PRSTZ-T-NULL
           END-IF
           IF IND-B03-TS-TARI-DOV = -1
              MOVE HIGH-VALUES TO B03-TS-TARI-DOV-NULL
           END-IF
           IF IND-B03-TS-TARI-SCON = -1
              MOVE HIGH-VALUES TO B03-TS-TARI-SCON-NULL
           END-IF
           IF IND-B03-TS-PP = -1
              MOVE HIGH-VALUES TO B03-TS-PP-NULL
           END-IF
           IF IND-B03-COEFF-RIS-1-T = -1
              MOVE HIGH-VALUES TO B03-COEFF-RIS-1-T-NULL
           END-IF
           IF IND-B03-COEFF-RIS-2-T = -1
              MOVE HIGH-VALUES TO B03-COEFF-RIS-2-T-NULL
           END-IF
           IF IND-B03-ABB = -1
              MOVE HIGH-VALUES TO B03-ABB-NULL
           END-IF
           IF IND-B03-TP-COASS = -1
              MOVE HIGH-VALUES TO B03-TP-COASS-NULL
           END-IF
           IF IND-B03-TRAT-RIASS = -1
              MOVE HIGH-VALUES TO B03-TRAT-RIASS-NULL
           END-IF
           IF IND-B03-TRAT-RIASS-ECC = -1
              MOVE HIGH-VALUES TO B03-TRAT-RIASS-ECC-NULL
           END-IF
           IF IND-B03-TP-RGM-FISC = -1
              MOVE HIGH-VALUES TO B03-TP-RGM-FISC-NULL
           END-IF
           IF IND-B03-DUR-GAR-AA = -1
              MOVE HIGH-VALUES TO B03-DUR-GAR-AA-NULL
           END-IF
           IF IND-B03-DUR-GAR-MM = -1
              MOVE HIGH-VALUES TO B03-DUR-GAR-MM-NULL
           END-IF
           IF IND-B03-DUR-GAR-GG = -1
              MOVE HIGH-VALUES TO B03-DUR-GAR-GG-NULL
           END-IF
           IF IND-B03-ANTIDUR-CALC-365 = -1
              MOVE HIGH-VALUES TO B03-ANTIDUR-CALC-365-NULL
           END-IF
           IF IND-B03-COD-FISC-CNTR = -1
              MOVE HIGH-VALUES TO B03-COD-FISC-CNTR-NULL
           END-IF
           IF IND-B03-COD-FISC-ASSTO1 = -1
              MOVE HIGH-VALUES TO B03-COD-FISC-ASSTO1-NULL
           END-IF
           IF IND-B03-COD-FISC-ASSTO2 = -1
              MOVE HIGH-VALUES TO B03-COD-FISC-ASSTO2-NULL
           END-IF
           IF IND-B03-COD-FISC-ASSTO3 = -1
              MOVE HIGH-VALUES TO B03-COD-FISC-ASSTO3-NULL
           END-IF
           IF IND-B03-CAUS-SCON = -1
              MOVE HIGH-VALUES TO B03-CAUS-SCON
           END-IF
           IF IND-B03-EMIT-TIT-OPZ = -1
              MOVE HIGH-VALUES TO B03-EMIT-TIT-OPZ
           END-IF
           IF IND-B03-QTZ-SP-Z-COUP-EMIS = -1
              MOVE HIGH-VALUES TO B03-QTZ-SP-Z-COUP-EMIS-NULL
           END-IF
           IF IND-B03-QTZ-SP-Z-OPZ-EMIS = -1
              MOVE HIGH-VALUES TO B03-QTZ-SP-Z-OPZ-EMIS-NULL
           END-IF
           IF IND-B03-QTZ-SP-Z-COUP-DT-C = -1
              MOVE HIGH-VALUES TO B03-QTZ-SP-Z-COUP-DT-C-NULL
           END-IF
           IF IND-B03-QTZ-SP-Z-OPZ-DT-CA = -1
              MOVE HIGH-VALUES TO B03-QTZ-SP-Z-OPZ-DT-CA-NULL
           END-IF
           IF IND-B03-QTZ-TOT-EMIS = -1
              MOVE HIGH-VALUES TO B03-QTZ-TOT-EMIS-NULL
           END-IF
           IF IND-B03-QTZ-TOT-DT-CALC = -1
              MOVE HIGH-VALUES TO B03-QTZ-TOT-DT-CALC-NULL
           END-IF
           IF IND-B03-QTZ-TOT-DT-ULT-BIL = -1
              MOVE HIGH-VALUES TO B03-QTZ-TOT-DT-ULT-BIL-NULL
           END-IF
           IF IND-B03-DT-QTZ-EMIS = -1
              MOVE HIGH-VALUES TO B03-DT-QTZ-EMIS-NULL
           END-IF
           IF IND-B03-PC-CAR-GEST = -1
              MOVE HIGH-VALUES TO B03-PC-CAR-GEST-NULL
           END-IF
           IF IND-B03-PC-CAR-ACQ = -1
              MOVE HIGH-VALUES TO B03-PC-CAR-ACQ-NULL
           END-IF
           IF IND-B03-IMP-CAR-CASO-MOR = -1
              MOVE HIGH-VALUES TO B03-IMP-CAR-CASO-MOR-NULL
           END-IF
           IF IND-B03-PC-CAR-MOR = -1
              MOVE HIGH-VALUES TO B03-PC-CAR-MOR-NULL
           END-IF
           IF IND-B03-TP-VERS = -1
              MOVE HIGH-VALUES TO B03-TP-VERS-NULL
           END-IF
           IF IND-B03-FL-SWITCH = -1
              MOVE HIGH-VALUES TO B03-FL-SWITCH-NULL
           END-IF
           IF IND-B03-FL-IAS = -1
              MOVE HIGH-VALUES TO B03-FL-IAS-NULL
           END-IF
           IF IND-B03-DIR = -1
              MOVE HIGH-VALUES TO B03-DIR-NULL
           END-IF
           IF IND-B03-TP-COP-CASO-MOR = -1
              MOVE HIGH-VALUES TO B03-TP-COP-CASO-MOR-NULL
           END-IF
           IF IND-B03-MET-RISC-SPCL = -1
              MOVE HIGH-VALUES TO B03-MET-RISC-SPCL-NULL
           END-IF
           IF IND-B03-TP-STAT-INVST = -1
              MOVE HIGH-VALUES TO B03-TP-STAT-INVST-NULL
           END-IF
           IF IND-B03-COD-PRDT = -1
              MOVE HIGH-VALUES TO B03-COD-PRDT-NULL
           END-IF
           IF IND-B03-STAT-ASSTO-1 = -1
              MOVE HIGH-VALUES TO B03-STAT-ASSTO-1-NULL
           END-IF
           IF IND-B03-STAT-ASSTO-2 = -1
              MOVE HIGH-VALUES TO B03-STAT-ASSTO-2-NULL
           END-IF
           IF IND-B03-STAT-ASSTO-3 = -1
              MOVE HIGH-VALUES TO B03-STAT-ASSTO-3-NULL
           END-IF
           IF IND-B03-CPT-ASSTO-INI-MOR = -1
              MOVE HIGH-VALUES TO B03-CPT-ASSTO-INI-MOR-NULL
           END-IF
           IF IND-B03-TS-STAB-PRE = -1
              MOVE HIGH-VALUES TO B03-TS-STAB-PRE-NULL
           END-IF
           IF IND-B03-DIR-EMIS = -1
              MOVE HIGH-VALUES TO B03-DIR-EMIS-NULL
           END-IF
           IF IND-B03-DT-INC-ULT-PRE = -1
              MOVE HIGH-VALUES TO B03-DT-INC-ULT-PRE-NULL
           END-IF
           IF IND-B03-STAT-TBGC-ASSTO-1 = -1
              MOVE HIGH-VALUES TO B03-STAT-TBGC-ASSTO-1-NULL
           END-IF
           IF IND-B03-STAT-TBGC-ASSTO-2 = -1
              MOVE HIGH-VALUES TO B03-STAT-TBGC-ASSTO-2-NULL
           END-IF
           IF IND-B03-STAT-TBGC-ASSTO-3 = -1
              MOVE HIGH-VALUES TO B03-STAT-TBGC-ASSTO-3-NULL
           END-IF
           IF IND-B03-FRAZ-DECR-CPT = -1
              MOVE HIGH-VALUES TO B03-FRAZ-DECR-CPT-NULL
           END-IF
           IF IND-B03-PRE-PP-ULT = -1
              MOVE HIGH-VALUES TO B03-PRE-PP-ULT-NULL
           END-IF
           IF IND-B03-ACQ-EXP = -1
              MOVE HIGH-VALUES TO B03-ACQ-EXP-NULL
           END-IF
           IF IND-B03-REMUN-ASS = -1
              MOVE HIGH-VALUES TO B03-REMUN-ASS-NULL
           END-IF
           IF IND-B03-COMMIS-INTER = -1
              MOVE HIGH-VALUES TO B03-COMMIS-INTER-NULL
           END-IF
           IF IND-B03-NUM-FINANZ = -1
              MOVE HIGH-VALUES TO B03-NUM-FINANZ-NULL
           END-IF
           IF IND-B03-TP-ACC-COMM = -1
              MOVE HIGH-VALUES TO B03-TP-ACC-COMM-NULL
           END-IF
           IF IND-B03-IB-ACC-COMM = -1
              MOVE HIGH-VALUES TO B03-IB-ACC-COMM-NULL
           END-IF
           IF IND-B03-CARZ = -1
              MOVE HIGH-VALUES TO B03-CARZ-NULL
           END-IF.


       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.

           CONTINUE.
       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.
           CONTINUE.
       Z500-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.
           CONTINUE.
       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.

           MOVE B03-DT-RIS TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO B03-DT-RIS-DB
           MOVE B03-DT-PRODUZIONE TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO B03-DT-PRODUZIONE-DB
           IF IND-B03-DT-INI-VAL-TAR = 0
               MOVE B03-DT-INI-VAL-TAR TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-INI-VAL-TAR-DB
           END-IF
           MOVE B03-DT-INI-VLDT-PROD TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO B03-DT-INI-VLDT-PROD-DB
           MOVE B03-DT-DECOR-POLI TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO B03-DT-DECOR-POLI-DB
           IF IND-B03-DT-DECOR-ADES = 0
               MOVE B03-DT-DECOR-ADES TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-DECOR-ADES-DB
           END-IF
           MOVE B03-DT-DECOR-TRCH TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO B03-DT-DECOR-TRCH-DB
           MOVE B03-DT-EMIS-POLI TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO B03-DT-EMIS-POLI-DB
           IF IND-B03-DT-EMIS-TRCH = 0
               MOVE B03-DT-EMIS-TRCH TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-EMIS-TRCH-DB
           END-IF
           IF IND-B03-DT-SCAD-TRCH = 0
               MOVE B03-DT-SCAD-TRCH TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-SCAD-TRCH-DB
           END-IF
           IF IND-B03-DT-SCAD-INTMD = 0
               MOVE B03-DT-SCAD-INTMD TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-SCAD-INTMD-DB
           END-IF
           IF IND-B03-DT-SCAD-PAG-PRE = 0
               MOVE B03-DT-SCAD-PAG-PRE TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-SCAD-PAG-PRE-DB
           END-IF
           IF IND-B03-DT-ULT-PRE-PAG = 0
               MOVE B03-DT-ULT-PRE-PAG TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-ULT-PRE-PAG-DB
           END-IF
           IF IND-B03-DT-NASC-1O-ASSTO = 0
               MOVE B03-DT-NASC-1O-ASSTO TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-NASC-1O-ASSTO-DB
           END-IF
           IF IND-B03-DT-EFF-CAMB-STAT = 0
               MOVE B03-DT-EFF-CAMB-STAT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-EFF-CAMB-STAT-DB
           END-IF
           IF IND-B03-DT-EMIS-CAMB-STAT = 0
               MOVE B03-DT-EMIS-CAMB-STAT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-EMIS-CAMB-STAT-DB
           END-IF
           IF IND-B03-DT-EFF-STAB = 0
               MOVE B03-DT-EFF-STAB TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-EFF-STAB-DB
           END-IF
           IF IND-B03-DT-EFF-RIDZ = 0
               MOVE B03-DT-EFF-RIDZ TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-EFF-RIDZ-DB
           END-IF
           IF IND-B03-DT-EMIS-RIDZ = 0
               MOVE B03-DT-EMIS-RIDZ TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-EMIS-RIDZ-DB
           END-IF
           IF IND-B03-DT-ULT-RIVAL = 0
               MOVE B03-DT-ULT-RIVAL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-ULT-RIVAL-DB
           END-IF
           IF IND-B03-DT-QTZ-EMIS = 0
               MOVE B03-DT-QTZ-EMIS TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-QTZ-EMIS-DB
           END-IF
           IF IND-B03-DT-INC-ULT-PRE = 0
               MOVE B03-DT-INC-ULT-PRE TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-INC-ULT-PRE-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.
           CONTINUE.


       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

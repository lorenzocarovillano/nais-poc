       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDSS0300 IS INITIAL.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN.  10 NOV 2009.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

           EXEC SQL INCLUDE IDSV0010 END-EXEC.

       01 WK-DATA-PACKAGE-MAX            PIC 9(09) COMP.
       01 WK-LEN-PACKAGES-NO-STD         PIC 9(09) COMP.
       01 WK-LEN-TRANS-DATA              PIC S9(9) COMP.
       01 WK-TOT-NUM-FRAMES              PIC S9(4) COMP.
       01 WK-REMAINDER-PACKAGE           PIC S9(9) COMP.
       01 WK-START-POSITION              PIC 9(09).
       01 WK-IND-FRAME                   PIC S9(04) COMP.

       01 NUM-FRAME-APPEND               PIC S9(04) COMP.

       01 TEMPORARY-DATA-TROVATI         PIC X(01).
          88 TEMPORARY-DATA-TROVATI-SI   VALUE 'S'.
          88 TEMPORARY-DATA-TROVATI-NO   VALUE 'N'.

      ***************************************************************
      * NOMI PGM
      ***************************************************************
       01  WK-PGM                        PIC  X(08) VALUE 'IDSS0300'.

      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDD080 END-EXEC.
           EXEC SQL INCLUDE IDBVD081 END-EXEC.
           EXEC SQL INCLUDE IDBVD082 END-EXEC.

      *++ ATTENZIONE - tale statement DEVE essere disasteriscato
      *   su ambiente MainFrame !!!!
      *     EXEC SQL
      *          DECLARE :D08-BUFFER-DATA-VCHAR
      *          VARIABLE FOR BIT DATA
      *     END-EXEC.
      *-- ATTENZIONE - tale statement DEVE essere disasteriscato
      *   su ambiente MainFrame !!!!

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0301 END-EXEC.

           EXEC SQL INCLUDE IDSV0302 END-EXEC.

       PROCEDURE DIVISION USING IDSV0301 IDSV0302.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0301-ESITO-OK
              IF IDSV0301-DECLARE
                 PERFORM D000-CREA-SESSION-TABLE  THRU D000-EX
              ELSE
                 PERFORM B000-ELABORA             THRU B000-EX
              END-IF

           END-IF.

           GOBACK.

       A000-INIZIO.

           MOVE 'IDSS0300'             TO IDSV0301-COD-SERVIZIO-BE.

           SET IDSV0301-ESITO-OK       TO TRUE.
           SET IDSV0301-SUCCESSFUL-SQL TO TRUE.

           SET TEMPORARY-DATA-TROVATI-NO TO TRUE.

           MOVE SPACES                 TO IDSV0301-DESC-ERRORE-ESTESA.

           PERFORM A010-CNTL-INPUT     THRU A010-EX.

       A000-EX.
           EXIT.

      *****************************************************************
       A010-CNTL-INPUT.

           IF IDSV0301-DECLARE
              IF NOT IDSV0301-SESSION-TEMP-TABLE
                 SET IDSV0301-ESITO-KO   TO TRUE
                 MOVE 'DECLARE NON ESEGUIBILE'
                                      TO IDSV0301-DESC-ERRORE-ESTESA
              END-IF
           ELSE
              SET ADDRESS OF IDSV0302 TO IDSV0301-ADDRESS

              IF IDSV0301-WRITE
                 IF IDSV0301-BUFFER-DATA-LEN NOT NUMERIC OR
                    IDSV0301-BUFFER-DATA-LEN   = ZEROES

                    SET IDSV0301-ESITO-KO TO TRUE
                    MOVE 'BUFFER-DATA-LEN NON VALIDA'
                                       TO IDSV0301-DESC-ERRORE-ESTESA
                 ELSE
                    IF IDSV0301-CONTIGUOUS-NO   AND
                       IDSV0301-BUFFER-DATA-LEN >
                       LENGTH OF D08-BUFFER-DATA

                          SET IDSV0301-ESITO-KO TO TRUE
                          MOVE 'BUFFER-DATA-LEN NON VALIDA'
                                       TO IDSV0301-DESC-ERRORE-ESTESA
                       END-IF
                 END-IF
              END-IF

              IF IDSV0301-COD-COMP-ANIA NOT NUMERIC OR
                 IDSV0301-COD-COMP-ANIA   = ZEROES

                 SET IDSV0301-ESITO-KO   TO TRUE
                 MOVE 'COD. COMPAGNIA NON VALIDO'
                                      TO IDSV0301-DESC-ERRORE-ESTESA
              END-IF

              IF IDSV0301-ESITO-OK
                 IF IDSV0301-ID-TEMPORARY-DATA NOT NUMERIC

                    SET IDSV0301-ESITO-KO   TO TRUE
                    MOVE 'ID TEMPORARY DATA NON VALIDO'
                                         TO IDSV0301-DESC-ERRORE-ESTESA
                 END-IF
              END-IF

              IF IDSV0301-ESITO-OK
                 IF IDSV0301-ALIAS-STR-DATO = HIGH-VALUE OR
                                              LOW-VALUE  OR
                                              SPACES

                    SET IDSV0301-ESITO-KO   TO TRUE
                    MOVE 'ALIAS-STR-DATO NON VALIDO'
                                         TO IDSV0301-DESC-ERRORE-ESTESA
                 END-IF
              END-IF

              IF IDSV0301-ESITO-OK
                 IF IDSV0301-ID-SESSION = HIGH-VALUE OR
                                          LOW-VALUE  OR
                                          SPACES

                    SET IDSV0301-ESITO-KO   TO TRUE
                    MOVE 'SESSIONE NON VALIDA'
                                         TO IDSV0301-DESC-ERRORE-ESTESA
                 END-IF
              END-IF
           END-IF.

       A010-EX.
           EXIT.

      *****************************************************************
       B000-ELABORA.

           EVALUATE TRUE

              WHEN IDSV0301-WRITE
                   PERFORM B200-TRATTA-WRITE     THRU B200-EX

              WHEN IDSV0301-READ
                   PERFORM B300-TRATTA-READ      THRU B300-EX

              WHEN IDSV0301-READ-NEXT
                   PERFORM B400-TRATTA-READ-NEXT THRU B400-EX

              WHEN OTHER
                SET IDSV0301-ESITO-KO   TO TRUE
                MOVE 'OPERAZIONE NON VALIDA'
                                        TO IDSV0301-DESC-ERRORE-ESTESA
           END-EVALUATE.

       B000-EX.
           EXIT.

      *****************************************************************
       B200-TRATTA-WRITE.

           EVALUATE TRUE
              WHEN IDSV0301-CONTIGUOUS-SI
              WHEN IDSV0301-CONTIGUOUS-NO

                MOVE IDSV0301-BUFFER-DATA-LEN
                                         TO WK-LEN-TRANS-DATA
                MOVE LENGTH OF D08-BUFFER-DATA
                                         TO WK-DATA-PACKAGE-MAX
                PERFORM CALCOLA-PACKAGES THRU CALCOLA-PACKAGES-EX
                PERFORM B250-SCRIVI-TEMPORARY-TABLE
                                         THRU B250-EX

              WHEN OTHER
                SET IDSV0301-ESITO-KO   TO TRUE
                MOVE 'FLAG CONTIGUOUS NON VALIDO'
                                        TO IDSV0301-DESC-ERRORE-ESTESA
           END-EVALUATE.

       B200-EX.
           EXIT.

      *****************************************************************
       B300-TRATTA-READ.

           PERFORM E100-DECLARE-CURSOR-PACK       THRU E100-EX

           IF IDSV0301-ESITO-OK
              PERFORM E200-OPEN-CURSOR-PACK       THRU E200-EX

              IF IDSV0301-ESITO-OK
                 PERFORM E300-FETCH-CURSOR-PACK   THRU E300-EX

                 IF IDSV0301-ESITO-OK
                    IF IDSV0301-SUCCESSFUL-SQL
                       MOVE 1                     TO WK-START-POSITION
                       PERFORM E600-VALORIZZA-OUT THRU E600-EX

                       IF IDSV0301-CONTIGUOUS-SI
                          PERFORM B400-TRATTA-READ-NEXT
                                                  THRU B400-EX
                                  UNTIL NOT IDSV0301-SUCCESSFUL-SQL
                       END-IF
                    END-IF
                 END-IF
              END-IF
           END-IF.

       B300-EX.
           EXIT.

      *****************************************************************
       B250-SCRIVI-TEMPORARY-TABLE.

           PERFORM V100-VERIFICA-TEMP-TABLE   THRU V100-EX

           MOVE 1               TO WK-START-POSITION
           PERFORM VARYING WK-IND-FRAME FROM 1 BY 1
                   UNTIL WK-IND-FRAME > WK-TOT-NUM-FRAMES
                   OR    IDSV0301-ESITO-KO

                   IF WK-IND-FRAME = WK-TOT-NUM-FRAMES AND
                      WK-LEN-PACKAGES-NO-STD NOT =  ZEROES
                      MOVE WK-LEN-PACKAGES-NO-STD
                                      TO WK-DATA-PACKAGE-MAX
                   END-IF
                   PERFORM B255-ELAB-CONT    THRU B255-EX
           END-PERFORM.

       B250-EX.
           EXIT.

      *****************************************************************
       B255-ELAB-CONT.

           PERFORM B260-VAL-INSERT-CONT      THRU B260-EX

           PERFORM A220-INSERT               THRU A220-EX.

       B255-EX.
           EXIT.

      *****************************************************************
       B260-VAL-INSERT-CONT.

           INITIALIZE TEMPORARY-DATA.

           MOVE IDSV0301-COD-COMP-ANIA      TO D08-COD-COMP-ANIA
           MOVE IDSV0301-ID-TEMPORARY-DATA  TO D08-ID-TEMPORARY-DATA
           MOVE IDSV0301-ALIAS-STR-DATO     TO D08-ALIAS-STR-DATO

           IF TEMPORARY-DATA-TROVATI-NO
              MOVE WK-IND-FRAME             TO D08-NUM-FRAME
           ELSE
              MOVE NUM-FRAME-APPEND         TO D08-NUM-FRAME
           END-IF

           MOVE IDSV0301-ID-SESSION         TO D08-ID-SESSION
           MOVE IDSV0301-FL-CONTIGUOUS      TO D08-FL-CONTIGUOUS-DATA
           MOVE IDSV0301-TOTAL-RECURRENCE   TO D08-TOTAL-RECURRENCE
           MOVE IDSV0301-PARTIAL-RECURRENCE TO D08-PARTIAL-RECURRENCE
           MOVE IDSV0301-ACTUAL-RECURRENCE  TO D08-ACTUAL-RECURRENCE

           MOVE 'I'                         TO D08-DS-OPER-SQL
           MOVE 0                           TO D08-DS-VER
           MOVE 'USER'                      TO D08-DS-UTENTE
           MOVE '1'                         TO D08-DS-STATO-ELAB
           MOVE 0                           TO D08-DS-TS-CPTZ

           MOVE WK-DATA-PACKAGE-MAX         TO D08-BUFFER-DATA-LEN

           MOVE SPACES                      TO D08-BUFFER-DATA.
           MOVE IDSV0302-BUFFER-DATA
                (WK-START-POSITION : WK-DATA-PACKAGE-MAX )
                                            TO D08-BUFFER-DATA.

           ADD  WK-DATA-PACKAGE-MAX         TO WK-START-POSITION.

       B260-EX.
           EXIT.

      *****************************************************************
       B400-TRATTA-READ-NEXT.

           PERFORM E300-FETCH-CURSOR-PACK      THRU E300-EX.

           IF IDSV0301-ESITO-OK
              IF IDSV0301-SUCCESSFUL-SQL
                 PERFORM E600-VALORIZZA-OUT    THRU E600-EX
              END-IF
           END-IF.

       B400-EX.
           EXIT.

      *****************************************************************
       E100-DECLARE-CURSOR-PACK.

           IF IDSV0301-SESSION-TEMP-TABLE
              PERFORM E101-DECL-CUR-PACK-SESSION-TAB THRU E101-EX
           ELSE
              PERFORM E103-DECL-CUR-PACK-STATIC-TAB  THRU E103-EX
           END-IF.

       E100-EX.
           EXIT.

      *****************************************************************
       E101-DECL-CUR-PACK-SESSION-TAB.

           EXEC SQL
             DECLARE CUR-PACKAGE-SESSIO CURSOR FOR
             SELECT
                ID_TEMPORARY_DATA
                ,ALIAS_STR_DATO
                ,NUM_FRAME
                ,COD_COMP_ANIA
                ,ID_SESSION
                ,FL_CONTIGUOUS_DATA
                ,TOTAL_RECURRENCE
                ,PARTIAL_RECURRENCE
                ,ACTUAL_RECURRENCE
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TYPE_RECORD
                ,BUFFER_DATA

             FROM SESSION.TEMPORARY_DATA
             WHERE     COD_COMP_ANIA     = :IDSV0301-COD-COMP-ANIA
                   AND ID_TEMPORARY_DATA = :IDSV0301-ID-TEMPORARY-DATA
                   AND ALIAS_STR_DATO    = :IDSV0301-ALIAS-STR-DATO
                   ORDER BY COD_COMP_ANIA
                           ,ID_TEMPORARY_DATA
                           ,ALIAS_STR_DATO
                           ,NUM_FRAME
           END-EXEC.

       E101-EX.
           EXIT.

      *****************************************************************
       E103-DECL-CUR-PACK-STATIC-TAB.

           EXEC SQL
             DECLARE CUR-PACKAGE-STATIC CURSOR FOR
             SELECT
                ID_TEMPORARY_DATA
                ,ALIAS_STR_DATO
                ,NUM_FRAME
                ,COD_COMP_ANIA
                ,ID_SESSION
                ,FL_CONTIGUOUS_DATA
                ,TOTAL_RECURRENCE
                ,PARTIAL_RECURRENCE
                ,ACTUAL_RECURRENCE
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TYPE_RECORD
                ,BUFFER_DATA

             FROM TEMPORARY_DATA
             WHERE     COD_COMP_ANIA     = :IDSV0301-COD-COMP-ANIA
                   AND ID_TEMPORARY_DATA = :IDSV0301-ID-TEMPORARY-DATA
                   AND ALIAS_STR_DATO    = :IDSV0301-ALIAS-STR-DATO
                   ORDER BY COD_COMP_ANIA
                           ,ID_TEMPORARY_DATA
                           ,ALIAS_STR_DATO
                           ,NUM_FRAME
           END-EXEC.

       E103-EX.
           EXIT.

      *****************************************************************
       E200-OPEN-CURSOR-PACK.

           IF IDSV0301-SESSION-TEMP-TABLE
              PERFORM E201-DECL-CUR-PACK-SESSION-TAB THRU E201-EX
           ELSE
              PERFORM E203-DECL-CUR-PACK-STATIC-TAB  THRU E203-EX
           END-IF.

       E200-EX.
           EXIT.

      *****************************************************************
       E201-DECL-CUR-PACK-SESSION-TAB.

           EXEC SQL
                OPEN CUR-PACKAGE-SESSIO
           END-EXEC.

           MOVE SQLCODE          TO IDSV0301-SQLCODE-SIGNED

           IF NOT IDSV0301-SUCCESSFUL-SQL

              SET IDSV0301-ESITO-KO TO TRUE

              MOVE 'ERRORE OPEN CURSOR STATIC TEMPORARY TABLE'
                                    TO IDSV0301-DESC-ERRORE-ESTESA
           END-IF.

       E201-EX.
           EXIT.

      *****************************************************************
       E203-DECL-CUR-PACK-STATIC-TAB.

           EXEC SQL
                OPEN CUR-PACKAGE-STATIC
           END-EXEC.

           MOVE SQLCODE          TO IDSV0301-SQLCODE-SIGNED

           IF NOT IDSV0301-SUCCESSFUL-SQL

              SET IDSV0301-ESITO-KO TO TRUE

              MOVE 'ERRORE OPEN CURSOR SESSION TEMPORARY TABLE'
                                    TO IDSV0301-DESC-ERRORE-ESTESA
           END-IF.

       E203-EX.
           EXIT.

      *****************************************************************
       E300-FETCH-CURSOR-PACK.

           IF IDSV0301-SESSION-TEMP-TABLE
              PERFORM E301-FTCH-CUR-PACK-SESSION-TAB THRU E301-EX
           ELSE
              PERFORM E303-FTCH-CUR-PACK-STATIC-TAB  THRU E303-EX
           END-IF.

       E300-EX.
           EXIT.

      *****************************************************************
       E301-FTCH-CUR-PACK-SESSION-TAB.

           EXEC SQL
                FETCH CUR-PACKAGE-SESSIO
             INTO
                :D08-ID-TEMPORARY-DATA
               ,:D08-ALIAS-STR-DATO
               ,:D08-NUM-FRAME
               ,:D08-COD-COMP-ANIA
               ,:D08-ID-SESSION
               ,:D08-FL-CONTIGUOUS-DATA
                :IND-D08-FL-CONTIGUOUS-DATA
               ,:D08-TOTAL-RECURRENCE
                :IND-D08-TOTAL-RECURRENCE
               ,:D08-PARTIAL-RECURRENCE
                :IND-D08-PARTIAL-RECURRENCE
               ,:D08-ACTUAL-RECURRENCE
                :IND-D08-ACTUAL-RECURRENCE
               ,:D08-DS-OPER-SQL
               ,:D08-DS-VER
               ,:D08-DS-TS-CPTZ
               ,:D08-DS-UTENTE
               ,:D08-DS-STATO-ELAB
               ,:D08-TYPE-RECORD
                :IND-D08-TYPE-RECORD
               ,:D08-BUFFER-DATA-VCHAR
                :IND-D08-BUFFER-DATA

           END-EXEC.

           MOVE SQLCODE            TO IDSV0301-SQLCODE-SIGNED

           IF NOT IDSV0301-SUCCESSFUL-SQL

              IF IDSV0301-NOT-FOUND

                 PERFORM E400-CLOSE-CURSOR-PACK THRU E400-EX

                 IF IDSV0301-ESITO-OK
                    IF IDSV0301-READ
                       SET IDSV0301-ESITO-KO TO TRUE
                       MOVE 'DATI NON TROVATI SU TAB. TEMPORANEA'
                                   TO IDSV0301-DESC-ERRORE-ESTESA
                    END-IF

                    SET IDSV0301-NOT-FOUND      TO TRUE
                 END-IF

              ELSE

                 SET IDSV0301-ESITO-KO TO TRUE
                 MOVE 'ERRORE FETCH CURSOR'
                                   TO IDSV0301-DESC-ERRORE-ESTESA
              END-IF

           END-IF.

       E301-EX.
           EXIT.

      *****************************************************************
       E303-FTCH-CUR-PACK-STATIC-TAB.

           EXEC SQL
                FETCH CUR-PACKAGE-STATIC
             INTO
                :D08-ID-TEMPORARY-DATA
               ,:D08-ALIAS-STR-DATO
               ,:D08-NUM-FRAME
               ,:D08-COD-COMP-ANIA
               ,:D08-ID-SESSION
               ,:D08-FL-CONTIGUOUS-DATA
                :IND-D08-FL-CONTIGUOUS-DATA
               ,:D08-TOTAL-RECURRENCE
                :IND-D08-TOTAL-RECURRENCE
               ,:D08-PARTIAL-RECURRENCE
                :IND-D08-PARTIAL-RECURRENCE
               ,:D08-ACTUAL-RECURRENCE
                :IND-D08-ACTUAL-RECURRENCE
               ,:D08-DS-OPER-SQL
               ,:D08-DS-VER
               ,:D08-DS-TS-CPTZ
               ,:D08-DS-UTENTE
               ,:D08-DS-STATO-ELAB
               ,:D08-TYPE-RECORD
                :IND-D08-TYPE-RECORD
               ,:D08-BUFFER-DATA-VCHAR
                :IND-D08-BUFFER-DATA
           END-EXEC.

           MOVE SQLCODE            TO IDSV0301-SQLCODE-SIGNED

           IF NOT IDSV0301-SUCCESSFUL-SQL

              IF IDSV0301-NOT-FOUND

                 PERFORM E400-CLOSE-CURSOR-PACK THRU E400-EX

                 IF IDSV0301-ESITO-OK
                    IF IDSV0301-READ
                       SET IDSV0301-ESITO-KO TO TRUE
                       MOVE 'DATI NON TROVATI SU TAB. TEMPORANEA'
                                   TO IDSV0301-DESC-ERRORE-ESTESA
                    END-IF

                    SET IDSV0301-NOT-FOUND      TO TRUE
                 END-IF

              ELSE

                 SET IDSV0301-ESITO-KO TO TRUE
                 MOVE 'ERRORE FETCH CURSOR'
                                   TO IDSV0301-DESC-ERRORE-ESTESA
              END-IF

           END-IF.

       E303-EX.
           EXIT.

      *****************************************************************
       E400-CLOSE-CURSOR-PACK.

           IF IDSV0301-SESSION-TEMP-TABLE
              PERFORM E401-CLS-CUR-PACK-SESSION-TAB THRU E401-EX
           ELSE
              PERFORM E403-CLS-CUR-PACK-STATIC-TAB  THRU E403-EX
           END-IF.

       E400-EX.
           EXIT.

      *****************************************************************
       E401-CLS-CUR-PACK-SESSION-TAB.

              EXEC SQL
                   CLOSE CUR-PACKAGE-SESSIO
              END-EXEC.

              MOVE SQLCODE            TO IDSV0301-SQLCODE-SIGNED

              IF NOT IDSV0301-SUCCESSFUL-SQL
                 SET IDSV0301-ESITO-KO TO TRUE
                 MOVE 'ERRORE CLOSE CURSOR SESSION TEMPORARY TABLE'
                                    TO IDSV0301-DESC-ERRORE-ESTESA
              END-IF.

       E401-EX.
           EXIT.

      *****************************************************************
       E403-CLS-CUR-PACK-STATIC-TAB.

              EXEC SQL
                   CLOSE CUR-PACKAGE-STATIC
              END-EXEC.

              MOVE SQLCODE            TO IDSV0301-SQLCODE-SIGNED

              IF NOT IDSV0301-SUCCESSFUL-SQL
                 SET IDSV0301-ESITO-KO TO TRUE
                 MOVE 'ERRORE CLOSE CURSOR STATIC TEMPORARY TABLE'
                                    TO IDSV0301-DESC-ERRORE-ESTESA
              END-IF.

       E403-EX.
           EXIT.

      *****************************************************************
       E600-VALORIZZA-OUT.

           SET IDSV0301-READ-NEXT      TO TRUE.

           MOVE D08-ID-SESSION         TO IDSV0301-ID-SESSION
           MOVE D08-FL-CONTIGUOUS-DATA TO IDSV0301-FL-CONTIGUOUS
           MOVE D08-TOTAL-RECURRENCE   TO IDSV0301-TOTAL-RECURRENCE
           MOVE D08-PARTIAL-RECURRENCE TO IDSV0301-PARTIAL-RECURRENCE
           MOVE D08-ACTUAL-RECURRENCE  TO IDSV0301-ACTUAL-RECURRENCE

           IF IDSV0301-CONTIGUOUS-NO
              MOVE D08-BUFFER-DATA     TO IDSV0302-BUFFER-DATA
           ELSE
              MOVE D08-BUFFER-DATA (1 : D08-BUFFER-DATA-LEN)
                                       TO IDSV0302-BUFFER-DATA
                   (WK-START-POSITION : D08-BUFFER-DATA-LEN)

              ADD  D08-BUFFER-DATA-LEN TO WK-START-POSITION
           END-IF.

       E600-EX.
           EXIT.

      *****************************************************************
       A210-SELECT.

           IF IDSV0301-SESSION-TEMP-TABLE
              PERFORM A211-SELECT-SESSION-TAB THRU A211-EX
           ELSE
              PERFORM A213-SELECT-STATIC-TAB  THRU A213-EX
           END-IF.

       A210-EX.
           EXIT.

      *****************************************************************
       A211-SELECT-SESSION-TAB.

           EXEC SQL
             SELECT
                 VALUE (MAX(NUM_FRAME), 0)
             INTO
                :D08-NUM-FRAME
             FROM SESSION.TEMPORARY_DATA
             WHERE     COD_COMP_ANIA     = :IDSV0301-COD-COMP-ANIA
                   AND ID_TEMPORARY_DATA = :IDSV0301-ID-TEMPORARY-DATA
                   AND ALIAS_STR_DATO    = :IDSV0301-ALIAS-STR-DATO
           END-EXEC.

           MOVE SQLCODE             TO IDSV0301-SQLCODE-SIGNED

           IF IDSV0301-SUCCESSFUL-SQL
              IF D08-NUM-FRAME > ZEROES
                 SET TEMPORARY-DATA-TROVATI-SI TO TRUE
              END-IF
           ELSE
              SET IDSV0301-ESITO-KO            TO TRUE
              MOVE 'ERRORE SELECT SESSION TEMPORARY TABLE'
                                    TO IDSV0301-DESC-ERRORE-ESTESA
           END-IF.

       A211-EX.
           EXIT.

      *****************************************************************
       A213-SELECT-STATIC-TAB.

           EXEC SQL
             SELECT
                 VALUE (MAX(NUM_FRAME), 0)
             INTO
                :D08-NUM-FRAME
             FROM TEMPORARY_DATA
             WHERE     COD_COMP_ANIA     = :IDSV0301-COD-COMP-ANIA
                   AND ID_TEMPORARY_DATA = :IDSV0301-ID-TEMPORARY-DATA
                   AND ALIAS_STR_DATO    = :IDSV0301-ALIAS-STR-DATO
           END-EXEC.

           MOVE SQLCODE       TO IDSV0301-SQLCODE-SIGNED

           IF IDSV0301-SUCCESSFUL-SQL
              IF D08-NUM-FRAME > ZEROES
                 SET TEMPORARY-DATA-TROVATI-SI TO TRUE
              END-IF
           ELSE
              SET IDSV0301-ESITO-KO            TO TRUE
              MOVE 'ERRORE SELECT STATIC TEMPORARY TABLE'
                                    TO IDSV0301-DESC-ERRORE-ESTESA
           END-IF.

       A213-EX.
           EXIT.

      *****************************************************************
       A220-INSERT.

           IF IDSV0301-SESSION-TEMP-TABLE
              PERFORM A221-INSERT-SESSION-TABLE      THRU A221-EX
           ELSE
              PERFORM A223-INSERT-STATIC-TABLE       THRU A223-EX
           END-IF.

       A220-EX.
           EXIT.

      *****************************************************************
       A221-INSERT-SESSION-TABLE.

           PERFORM Z200-SET-INDICATORI-NULL THRU Z200-EX

           EXEC SQL
              INSERT
              INTO SESSION.TEMPORARY_DATA
                  (
                ID_TEMPORARY_DATA
                ,ALIAS_STR_DATO
                ,NUM_FRAME
                ,COD_COMP_ANIA
                ,ID_SESSION
                ,FL_CONTIGUOUS_DATA
                ,TOTAL_RECURRENCE
                ,PARTIAL_RECURRENCE
                ,ACTUAL_RECURRENCE
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TYPE_RECORD
                ,BUFFER_DATA
                  )
              VALUES
                  (
                :D08-ID-TEMPORARY-DATA
               ,:D08-ALIAS-STR-DATO
               ,:D08-NUM-FRAME
               ,:D08-COD-COMP-ANIA
               ,:D08-ID-SESSION
               ,:D08-FL-CONTIGUOUS-DATA
                :IND-D08-FL-CONTIGUOUS-DATA
               ,:D08-TOTAL-RECURRENCE
                :IND-D08-TOTAL-RECURRENCE
               ,:D08-PARTIAL-RECURRENCE
                :IND-D08-PARTIAL-RECURRENCE
               ,:D08-ACTUAL-RECURRENCE
                :IND-D08-ACTUAL-RECURRENCE
               ,:D08-DS-OPER-SQL
               ,:D08-DS-VER
               ,:D08-DS-TS-CPTZ
               ,:D08-DS-UTENTE
               ,:D08-DS-STATO-ELAB
               ,:D08-TYPE-RECORD
                :IND-D08-TYPE-RECORD
               ,:D08-BUFFER-DATA-VCHAR
                :IND-D08-BUFFER-DATA
                  )

           END-EXEC

           MOVE SQLCODE  TO IDSV0301-SQLCODE-SIGNED

           IF NOT IDSV0301-SUCCESSFUL-SQL
              SET IDSV0301-ESITO-KO TO TRUE
              MOVE SQLCODE          TO IDSV0301-SQLCODE
              MOVE 'ERRORE INSERT TEMPORARY_DATA'
                                      TO IDSV0301-DESC-ERRORE-ESTESA
           END-IF.

       A221-EX.
           EXIT.

      *****************************************************************
       A223-INSERT-STATIC-TABLE.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

           EXEC SQL
              INSERT
              INTO TEMPORARY_DATA
                  (
                ID_TEMPORARY_DATA
                ,ALIAS_STR_DATO
                ,NUM_FRAME
                ,COD_COMP_ANIA
                ,ID_SESSION
                ,FL_CONTIGUOUS_DATA
                ,TOTAL_RECURRENCE
                ,PARTIAL_RECURRENCE
                ,ACTUAL_RECURRENCE
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TYPE_RECORD
                ,BUFFER_DATA
                  )
              VALUES
                  (
                :D08-ID-TEMPORARY-DATA
               ,:D08-ALIAS-STR-DATO
               ,:D08-NUM-FRAME
               ,:D08-COD-COMP-ANIA
               ,:D08-ID-SESSION
               ,:D08-FL-CONTIGUOUS-DATA
                :IND-D08-FL-CONTIGUOUS-DATA
               ,:D08-TOTAL-RECURRENCE
                :IND-D08-TOTAL-RECURRENCE
               ,:D08-PARTIAL-RECURRENCE
                :IND-D08-PARTIAL-RECURRENCE
               ,:D08-ACTUAL-RECURRENCE
                :IND-D08-ACTUAL-RECURRENCE
               ,:D08-DS-OPER-SQL
               ,:D08-DS-VER
               ,:D08-DS-TS-CPTZ
               ,:D08-DS-UTENTE
               ,:D08-DS-STATO-ELAB
               ,:D08-TYPE-RECORD
                :IND-D08-TYPE-RECORD
               ,:D08-BUFFER-DATA-VCHAR
                :IND-D08-BUFFER-DATA
                  )

           END-EXEC

           MOVE SQLCODE  TO IDSV0301-SQLCODE-SIGNED

           IF NOT IDSV0301-SUCCESSFUL-SQL
              SET IDSV0301-ESITO-KO TO TRUE
              MOVE SQLCODE          TO IDSV0301-SQLCODE
              MOVE 'ERRORE INSERT STATIC TEMPORARY_DATA'
                                      TO IDSV0301-DESC-ERRORE-ESTESA
           END-IF.

       A223-EX.
           EXIT.

      *****************************************************************
       A240-DELETE.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           EXEC SQL
                DELETE
                FROM TEMPORARY_DATA
                WHERE COD_COMP_ANIA      = :IDSV0301-COD-COMP-ANIA
                   AND ID_TEMPORARY_DATA = :IDSV0301-ID-TEMPORARY-DATA
                   AND ALIAS_STR_DATO    = :IDSV0301-ALIAS-STR-DATO
           END-EXEC.

           MOVE SQLCODE               TO IDSV0301-SQLCODE-SIGNED

           IF NOT IDSV0301-SUCCESSFUL-SQL
              SET IDSV0301-ESITO-KO   TO TRUE
              MOVE SQLCODE            TO IDSV0301-SQLCODE
              MOVE 'ERRORE DELETE TEMPORARY_DATA'
                                      TO IDSV0301-DESC-ERRORE-ESTESA
           END-IF.
       A240-EX.
           EXIT.

      *****************************************************************
       D000-CREA-SESSION-TABLE.

           EXEC SQL DECLARE GLOBAL TEMPORARY
                TABLE SESSION.TEMPORARY_DATA
                  (
                   ID_TEMPORARY_DATA   DECIMAL(10, 0) NOT NULL,
                   ALIAS_STR_DATO      CHAR(30) NOT NULL,
                   NUM_FRAME           DECIMAL(5, 0) NOT NULL,
                   COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
                   ID_SESSION          CHAR(20) NOT NULL,
                   FL_CONTIGUOUS_DATA  CHAR(1),
                   TOTAL_RECURRENCE    DECIMAL(5, 0),
                   PARTIAL_RECURRENCE  DECIMAL(5, 0),
                   ACTUAL_RECURRENCE   DECIMAL(5, 0),
                   DS_OPER_SQL         CHAR(1) NOT NULL,
                   DS_VER              DECIMAL(9, 0) NOT NULL,
                   DS_TS_CPTZ          DECIMAL(18, 0) NOT NULL,
                   DS_UTENTE           CHAR(20) NOT NULL,
                   DS_STATO_ELAB       CHAR(1) NOT NULL,
                   TYPE_RECORD         CHAR(3),
                   BUFFER_DATA         VARCHAR(32000)
                  )
           END-EXEC.

           MOVE SQLCODE       TO IDSV0301-SQLCODE-SIGNED

           IF NOT IDSV0301-SUCCESSFUL-SQL
              SET IDSV0301-ESITO-KO TO TRUE
              MOVE SQLCODE          TO IDSV0301-SQLCODE
              MOVE 'ERRORE CREAZIONE SESSION TABLE'
                                      TO IDSV0301-DESC-ERRORE-ESTESA
           END-IF.

       D000-EX.
           EXIT.

      *****************************************************************
       V100-VERIFICA-TEMP-TABLE.

           PERFORM A210-SELECT                    THRU A210-EX

           IF IDSV0301-ESITO-OK
              IF TEMPORARY-DATA-TROVATI-SI
                 IF IDSV0301-DELETE-ACTION
                    PERFORM A240-DELETE           THRU A240-EX
                    SET IDSV0301-APPEND-ACTION    TO TRUE
                    SET TEMPORARY-DATA-TROVATI-NO TO TRUE
                 ELSE
                    IF IDSV0301-CONTIGUOUS-SI
                       SET IDSV0301-ESITO-KO TO TRUE
                       MOVE SQLCODE       TO IDSV0301-SQLCODE
                       MOVE 'DATI TEMPORANEI GIA'' PRESENTI X ALIAS'
                                          TO IDSV0301-DESC-ERRORE-ESTESA
                    ELSE
                       MOVE D08-NUM-FRAME TO NUM-FRAME-APPEND
                       ADD 1              TO NUM-FRAME-APPEND
                    END-IF
                 END-IF
              END-IF
           END-IF.

       V100-EX.
           EXIT.

      *****************************************************************
       Z100-SET-COLONNE-NULL.

           IF IND-D08-FL-CONTIGUOUS-DATA = -1
              MOVE HIGH-VALUES TO D08-FL-CONTIGUOUS-DATA-NULL
           END-IF
           IF IND-D08-TOTAL-RECURRENCE = -1
              MOVE HIGH-VALUES TO D08-TOTAL-RECURRENCE-NULL
           END-IF
           IF IND-D08-PARTIAL-RECURRENCE = -1
              MOVE HIGH-VALUES TO D08-PARTIAL-RECURRENCE-NULL
           END-IF
           IF IND-D08-ACTUAL-RECURRENCE = -1
              MOVE HIGH-VALUES TO D08-ACTUAL-RECURRENCE-NULL
           END-IF
           IF IND-D08-TYPE-RECORD = -1
              MOVE HIGH-VALUES TO D08-TYPE-RECORD-NULL
           END-IF
           IF IND-D08-BUFFER-DATA = -1
              MOVE HIGH-VALUES TO D08-BUFFER-DATA
           END-IF.

       Z100-EX.
           EXIT.

      **************************************************************
       Z200-SET-INDICATORI-NULL.

           IF D08-FL-CONTIGUOUS-DATA-NULL = HIGH-VALUES
              MOVE -1 TO IND-D08-FL-CONTIGUOUS-DATA
           ELSE
              MOVE 0 TO IND-D08-FL-CONTIGUOUS-DATA
           END-IF
           IF D08-TOTAL-RECURRENCE-NULL = HIGH-VALUES
              MOVE -1 TO IND-D08-TOTAL-RECURRENCE
           ELSE
              MOVE 0 TO IND-D08-TOTAL-RECURRENCE
           END-IF
           IF D08-PARTIAL-RECURRENCE-NULL = HIGH-VALUES
              MOVE -1 TO IND-D08-PARTIAL-RECURRENCE
           ELSE
              MOVE 0 TO IND-D08-PARTIAL-RECURRENCE
           END-IF
           IF D08-ACTUAL-RECURRENCE-NULL = HIGH-VALUES
              MOVE -1 TO IND-D08-ACTUAL-RECURRENCE
           ELSE
              MOVE 0 TO IND-D08-ACTUAL-RECURRENCE
           END-IF
           IF D08-TYPE-RECORD-NULL = HIGH-VALUES
              MOVE -1 TO IND-D08-TYPE-RECORD
           ELSE
              MOVE 0 TO IND-D08-TYPE-RECORD
           END-IF
           IF D08-BUFFER-DATA = HIGH-VALUES
              MOVE -1 TO IND-D08-BUFFER-DATA
           ELSE
              MOVE 0 TO IND-D08-BUFFER-DATA
           END-IF.

       Z200-EX.
           EXIT.

      **************************************************************
       CALCOLA-PACKAGES.

            PERFORM INIT-CAMPI-PACKAGE THRU INIT-CAMPI-PACKAGE-EX

            DIVIDE    WK-DATA-PACKAGE-MAX
            INTO      WK-LEN-TRANS-DATA
            GIVING    WK-TOT-NUM-FRAMES
            REMAINDER WK-REMAINDER-PACKAGE

            IF WK-REMAINDER-PACKAGE > ZEROES
               MOVE WK-REMAINDER-PACKAGE
                                   TO WK-LEN-PACKAGES-NO-STD
               ADD 1               TO WK-TOT-NUM-FRAMES
            END-IF.

       CALCOLA-PACKAGES-EX.
           EXIT.

      **************************************************************
       INIT-CAMPI-PACKAGE.

           MOVE ZEROES TO  WK-LEN-PACKAGES-NO-STD
                           WK-TOT-NUM-FRAMES
                           WK-REMAINDER-PACKAGE.

       INIT-CAMPI-PACKAGE-EX.
           EXIT.

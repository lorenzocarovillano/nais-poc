
      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
      * >>2107 CORRETTA MAPPATURA DELL'OUTPUT ALLA VARIABILE
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0026.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0026
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... MODULO DI CALCOLO
      *  DESCRIZIONE.... Accesso su Param_Di_Calc (LDBS1650) e
      *                  ritorno in Output del campo Val_Pc
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

      *----------------------------------------------------------------*
      *    PROGRAMMI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0026'.
       01  WK-PGM-CALLED                    PIC X(008) VALUE 'LDBS1650'.

      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-TIPO-TRANCHE                  PIC X(02) VALUE  'TG'.


      *----------------------------------------------------------------*
      *    COPY
      *----------------------------------------------------------------*
           COPY IDBVPCA1.


      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0026.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0026.
      *----------------------------------------------------------------*

           PERFORM L000-OPERAZIONI-INIZIALI
              THRU L000-EX.

           PERFORM L100-ELABORAZIONE
              THRU L100-EX.

           PERFORM L900-OPERAZIONI-FINALI
              THRU L900-EX.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       L000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE      TO IVVC0213-TAB-OUTPUT.

       L000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       L100-ELABORAZIONE.
      *
           PERFORM L500-PREPARA-CALL-LDBS1650 THRU L500-EX.

           PERFORM L700-CALL-LDBS1650         THRU L700-EX.
      *
       L100-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  PRAPARA CALL LDBS1650
      *----------------------------------------------------------------*
       L500-PREPARA-CALL-LDBS1650.
      *
           SET IDSV0003-SELECT           TO TRUE
           SET IDSV0003-WHERE-CONDITION
                                         TO TRUE
           SET IDSV0003-TRATT-X-COMPETENZA
                                         TO TRUE


           MOVE IVVC0213-ID-LIVELLO      TO PCA-ID-OGG
           MOVE WK-TIPO-TRANCHE          TO PCA-TP-OGG
           MOVE IVVC0213-COD-VARIABILE   TO PCA-COD-PARAM.
      *
       L500-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALL LDBS1650
      *----------------------------------------------------------------*
       L700-CALL-LDBS1650.
      *
           CALL WK-PGM-CALLED  USING  IDSV0003 PARAM-DI-CALC
      *
           ON EXCEPTION
              MOVE WK-PGM-CALLED
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL LDBS1650 ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              IF PCA-VAL-PC IS NUMERIC
                 IF PCA-VAL-PC GREATER ZERO
                    MOVE PCA-VAL-PC        TO IVVC0213-VAL-PERC-O
                 END-IF
              END-IF

              IF PCA-VAL-IMP IS NUMERIC
                 IF PCA-VAL-IMP GREATER ZERO
                    MOVE PCA-VAL-IMP       TO IVVC0213-VAL-PERC-O
                 END-IF
              END-IF

              IF PCA-VAL-TS IS NUMERIC
                 IF PCA-VAL-TS GREATER ZERO
>>2107              MOVE PCA-VAL-TS        TO IVVC0213-VAL-PERC-O
>>2107              MOVE PCA-VAL-TS        TO IVVC0213-VAL-IMP-O
                 END-IF
              END-IF

              IF PCA-VAL-NUM IS NUMERIC
                 IF PCA-VAL-NUM GREATER ZERO
                    MOVE PCA-VAL-NUM       TO IVVC0213-VAL-PERC-O
                 END-IF
              END-IF
           ELSE
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
                 MOVE WK-PGM-CALLED          TO IDSV0003-COD-SERVIZIO-BE

                 STRING 'CHIAMATA LDBS1650 ;'
                        IDSV0003-RETURN-CODE ';'
                        IDSV0003-SQLCODE
                        DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                 END-STRING
              END-IF
           END-IF.
      *
       L700-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       L900-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       L900-EX.
           EXIT.

************************************************************************
********                                                        ********
********               ROUTINE DI CALCOLO COMPETENZE            ********
********                                                        ********
************************************************************************

       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDSS0150 IS INITIAL.
       AUTHOR.
       DATE-WRITTEN.  MARZO 2008.
       DATE-COMPILED.
      *REMARKS.
      *00000000000000000000000000000000000000000000000000000000000000000
      *    PROGRAAMMA .... IDSS0150
      *    FUNZIONE ...... ROUTINE DI CALCOLO COMPETENZE
      *00000000000000000000000000000000000000000000000000000000000000000
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.

      *************************************************
      * W O R K I N G   S T O R A G E   S E C T I O N *
      *************************************************
       WORKING-STORAGE SECTION.
       77 WK-PGM                         PIC X(008)  VALUE 'IDSS0150'.
       77 PGM-IDBSPCO0                   PIC X(008)  VALUE 'IDBSPCO0'.
       77 LCCS0004                       PIC X(008)  VALUE 'LCCS0004'.

       77 DESCRIZ-ERR-DB2                PIC X(300)  VALUE SPACES.
       77 PGM-ERRORE                     PIC X(008)  VALUE SPACES.
       77 WK-SQLCODE                     PIC S9(009).
       77 WK-SQLCODE-ED                  PIC  Z(008)9-.
       77 WK-LABEL                       PIC X(030).
       77 DIFFERENZA-DISPLAY             PIC Z9(09).

       77 LIMITE-POSITIVO                PIC S9(02) VALUE +59.
       77 LIMITE-NEGATIVO                PIC S9(02) VALUE -40.

       01 WK-DATA-AMG                    PIC X(008) VALUE SPACES.
       01 WK-PARAM                       PIC 9(001) VALUE ZEROES.

       01 WK-LIVELLO-GRAVITA.
          05 MESSAGGIO-INFORMATIVO        PIC 9(01) VALUE 1.
          05 WARNING                      PIC 9(01) VALUE 2.
          05 ERRORE-BLOCCANTE             PIC 9(01) VALUE 3.
          05 ERRORE-FATALE                PIC 9(01) VALUE 4.

           EXEC SQL INCLUDE SQLCA    END-EXEC.

           EXEC SQL INCLUDE IDBVPCO1 END-EXEC.
           EXEC SQL INCLUDE IDBVPCO2 END-EXEC.
           EXEC SQL INCLUDE IDBVPCO3 END-EXEC.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDSV0010 END-EXEC.

      *----------------------------------------------------------------*
       LINKAGE SECTION.

       01 AREA-IDSV0001.
           EXEC SQL INCLUDE IDSV0001 END-EXEC.

      ******************************************************************
      * P R O C E D U R E   D I V I S I O N                            *
      ******************************************************************
       PROCEDURE DIVISION USING AREA-IDSV0001.

           PERFORM A000-FASE-INIZIALE             THRU A000-EX

           IF IDSV0003-SUCCESSFUL-RC
      *--- ESTRAE IL TIMESTAMP CORRENTE IN FORMATO DB --> X(26)

              PERFORM A010-ESTRAI-TIMESTAMP-DB    THRU A010-EX

              IF IDSV0003-SUCCESSFUL-RC

      *--- ESTRAE LA DATA CORRENTE IN FORMATO DB --> X(10)

                 PERFORM A011-ESTRAI-DATE-DB      THRU A011-EX

                 IF IDSV0003-SUCCESSFUL-RC

                    PERFORM A050-CALCOLA-CPTZ     THRU A050-EX

                    IF IDSV0003-SUCCESSFUL-RC

                       IF IDSV0001-ID-TMPRY-DATA-SI
                          PERFORM E000-ESTRAI-ID-TMPRY-DATA
                             THRU E000-ESTRAI-ID-TMPRY-DATA-EX
                       ELSE
                          IF IDSV0001-ID-TEMPORARY-DATA NOT NUMERIC
                             MOVE ZEROES TO IDSV0001-ID-TEMPORARY-DATA
                          END-IF
                       END-IF
                       SET IDSV0001-NO-TEMP-TABLE TO TRUE
                    END-IF

                 END-IF

              END-IF
           END-IF

           PERFORM Z000-FASE-FINALE               THRU Z000-EX

           GOBACK.

      *******************************************************
       A000-FASE-INIZIALE.

           SET IDSV0001-ESITO-OK       TO TRUE
           SET IDSV0003-SUCCESSFUL-RC  TO TRUE
           SET IDSV0003-SUCCESSFUL-SQL TO TRUE
           INITIALIZE WS-TS-SYSTEM-DB
                      WS-TS-SYSTEM
                      WS-DATE-SYSTEM-DB
                      WS-DIFFERENZA
                      WS-RISULTATO.

           PERFORM A001-CONTROLLA-INPUT THRU A001-EX.

       A000-EX.
           EXIT.

      *******************************************************
       A001-CONTROLLA-INPUT.

           MOVE 'A001-CONTROLLA-INPUT' TO WK-LABEL.

           IF IDSV0001-COD-COMPAGNIA-ANIA NOT NUMERIC OR
              IDSV0001-COD-COMPAGNIA-ANIA = ZEROES

              SET IDSV0003-COD-COMP-NOT-VALID TO TRUE
              MOVE WK-PGM       TO IDSV0001-COD-SERVIZIO-BE
              MOVE WK-LABEL     TO IDSV0001-LABEL-ERR

             STRING 'CODICE COMPAGNIA NON VALIDO'
                    ' : '
                    IDSV0001-COD-COMPAGNIA-ANIA
                    DELIMITED BY SIZE INTO
                    IDSV0001-DESC-ERRORE-ESTESA
             END-STRING

           END-IF.

           IF IDSV0003-SUCCESSFUL-RC
              IF NOT IDSV0001-DB-ISO AND
                 NOT IDSV0001-DB-EUR

                 SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                 MOVE WK-PGM       TO IDSV0001-COD-SERVIZIO-BE
                 MOVE WK-LABEL     TO IDSV0001-LABEL-ERR

                 STRING 'FORMATO DB NON VALIDO'
                       DELIMITED BY SIZE INTO
                       IDSV0001-DESC-ERRORE-ESTESA
                 END-STRING
              END-IF
           END-IF.

       A001-EX.
           EXIT.

      *******************************************************
       A010-ESTRAI-TIMESTAMP-DB.

           EXEC SQL
                 VALUES CURRENT TIMESTAMP
                   INTO :WS-TS-SYSTEM-DB
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-RC

              MOVE WS-TS-SYSTEM-DB TO WS-TIMESTAMP-X
              PERFORM Z801-TS-X-TO-N   THRU Z801-EX
              MOVE WS-TIMESTAMP-N   TO WS-TS-SYSTEM

           ELSE

              MOVE IDSV0003-SQLCODE TO WK-SQLCODE-ED
              STRING WK-PGM ' - ESTRAZIONE TIMESTAMP'
                           ' - '
                           'SQLCODE : '
                           WK-SQLCODE-ED
                           DELIMITED BY SIZE INTO
                           IDSV0001-DESC-ERRORE-ESTESA
              END-STRING

           END-IF.

       A010-EX.
           EXIT.

      *******************************************************
       A011-ESTRAI-DATE-DB.

           EXEC SQL
                 VALUES CURRENT DATE
                   INTO :WS-DATE-SYSTEM-DB
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF NOT IDSV0003-SUCCESSFUL-RC

              MOVE IDSV0003-SQLCODE TO WK-SQLCODE-ED
              STRING WK-PGM ' - ESTRAZIONE DATA DI SISTEMA'
                           ' - '
                           'SQLCODE : '
                           WK-SQLCODE-ED
                           DELIMITED BY SIZE INTO
                           IDSV0001-DESC-ERRORE-ESTESA
              END-STRING

           END-IF.

       A011-EX.
           EXIT.

      ***************************************************************
       A050-CALCOLA-CPTZ.

           PERFORM A053-ESTRAI-DT-COMPETENZA THRU A053-EX

           IF IDSV0003-SUCCESSFUL-RC

              IF PCO-DT-CONT-NULL = HIGH-VALUE

                 IF  IDSV0001-BATCH-INFR
                 AND IDSV0001-KEY-BUSINESS3 NOT = SPACES

                     IF IDSV0001-KEY-BUSINESS3(1:8) IS NUMERIC
                        MOVE IDSV0001-KEY-BUSINESS3(1:8)
                          TO WK-DATA-AMG
                        MOVE 4          TO WK-PARAM

                        PERFORM A060-CTRL-DATA
                           THRU A060-EX

                        IF WK-PARAM = 0  AND
                           IDSV0003-SUCCESSFUL-RC
      *
                           MOVE WK-DATA-AMG           TO WS-DATE-N
                           PERFORM A070-GESTISCI-DIFF THRU A070-EX

                        ELSE

                           IF  WK-PARAM  NOT = 0

                               SET IDSV0003-FIELD-NOT-VALUED TO TRUE

                               MOVE WK-PGM      TO
                                                IDSV0001-COD-SERVIZIO-BE
                               MOVE WK-LABEL    TO IDSV0001-LABEL-ERR
                               MOVE 'LCCS0004'  TO IDSV0001-NOME-TABELLA

                               STRING 'DATA CONTABILE NON VALIDA IN '
                                      ' SK-PARAM: '
                                      WK-DATA-AMG
                                      DELIMITED BY SIZE INTO
                                      IDSV0001-DESC-ERRORE-ESTESA
                               END-STRING

                           END-IF
      *
                        END-IF
                     ELSE

                        MOVE IDSV0001-KEY-BUSINESS3(1:8)
                          TO WK-DATA-AMG
                        SET IDSV0003-FIELD-NOT-VALUED TO TRUE

                        MOVE WK-PGM      TO IDSV0001-COD-SERVIZIO-BE
                        MOVE WK-LABEL    TO IDSV0001-LABEL-ERR
                        MOVE 'CTRL_FORMALE'  TO IDSV0001-NOME-TABELLA

                        STRING 'DATA CONTABILE NON VALIDA IN SK-PARAM'
                               ' : '
                               WK-DATA-AMG
                               DELIMITED BY SIZE INTO
                               IDSV0001-DESC-ERRORE-ESTESA
                        END-STRING

                     END-IF
                 ELSE

                     MOVE ZEROES      TO WS-DIFFERENZA

                     MOVE WS-DATE-SYSTEM-DB  TO WS-DATE-X
                     PERFORM Z800-DT-X-TO-N  THRU Z800-EX

                 END-IF

              ELSE

                 MOVE PCO-DT-CONT           TO WS-DATE-N
                 PERFORM A070-GESTISCI-DIFF THRU A070-EX

              END-IF

              IF IDSV0003-SUCCESSFUL-RC

                 MOVE WS-TS-SYSTEM(1:8) TO WS-TS-DISPLAY(1:8)

                 MOVE QUARANTA          TO WS-TS-DISPLAY(9:2)

                 MOVE WS-TS-SYSTEM(9:8) TO WS-TS-DISPLAY(11:8)

                 MOVE WS-TS-DISPLAY     TO IDSV0001-DATA-COMPETENZA
      *--
                 MOVE WS-DATE-N         TO WS-TS-DISPLAY(1:8)
      *---
      *--- COMPONE IL TIMESTAMP PER L'AGGIORNAMENTO STORICO
      *---

                 IF WS-DIFFERENZA NOT = 0

                    COMPUTE WS-RISULTATO = QUARANTA + WS-DIFFERENZA

                    MOVE WS-STR-RISULTATO TO WS-TS-DISPLAY(9:2)

                 END-IF

                 MOVE WS-TS-DISPLAY       TO IDSV0001-DATA-COMP-AGG-STOR
      *---------------------------------------------------------------
      *-- Gestione Temporanea Inizio
      *-- da ELIMINARE in caso di Gestione delle Competenze Distinte
      *--
                                             IDSV0001-DATA-COMPETENZA
      *--
      *-- Gestione Temporanea Fine
      *---------------------------------------------------------------


      *---
      *--- COMPONE IL TIMESTAMP PER LE SELEZIONI STORICHE



              END-IF
           END-IF.

       A050-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ESTRAZIONE DATA COMPETENZA
      *----------------------------------------------------------------*
       A053-ESTRAI-DT-COMPETENZA.

           MOVE 'A053-ESTRAI-DT-COMPETENZA'    TO WK-LABEL.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA TO PCO-COD-COMP-ANIA
           MOVE IDSV0001-FORMATO-DATA-DB    TO IDSV0003-FORMATO-DATA-DB

           EXEC SQL
             SELECT
                  DT_CONT
                 ,FL_LIV_DEBUG
               INTO
                :PCO-DT-CONT-DB
                :IND-PCO-DT-CONT
               ,:PCO-FL-LIV-DEBUG

             FROM PARAM_COMP
             WHERE     COD_COMP_ANIA = :PCO-COD-COMP-ANIA

           END-EXEC.

           MOVE SQLCODE       TO IDSV0003-SQLCODE

           EVALUATE TRUE
               WHEN IDSV0003-SUCCESSFUL-SQL
      *-->     OPERAZIONE ESEGUITA CORRETTAMENTE
                    PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

                    PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

                    PERFORM D000-TRATTA-DEBUG     THRU D000-EX

               WHEN IDSV0003-NOT-FOUND
      *--->    CAMPO $ NON TROVATO
                    SET IDSV0003-SQL-ERROR TO TRUE
                    MOVE WK-PGM       TO IDSV0001-COD-SERVIZIO-BE
                    MOVE WK-LABEL     TO IDSV0001-LABEL-ERR
                    MOVE 'PARAM_COMP - OCCORRENZA NON TROVATA'
                           TO IDSV0001-DESC-ERRORE-ESTESA
                    MOVE 'PARAM_COMP' TO IDSV0001-NOME-TABELLA

               WHEN OTHER
      *--->    ERRORE DI ACCESSO AL DB
                    SET IDSV0003-SQL-ERROR TO TRUE
                    MOVE WK-PGM       TO IDSV0001-COD-SERVIZIO-BE
                    MOVE WK-LABEL     TO IDSV0001-LABEL-ERR

                    MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    STRING 'PARAM_COMP'
                           ' - '
                           'SQLCODE : '
                           WK-SQLCODE
                           DELIMITED BY SIZE INTO
                           IDSV0001-DESC-ERRORE-ESTESA
                    END-STRING

                    MOVE 'PARAM_COMP' TO IDSV0001-NOME-TABELLA

           END-EVALUATE.


      *
       A053-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CONTROLLO FORMALE DELLA DATA
      *----------------------------------------------------------------*
      *
       A060-CTRL-DATA.
      *
           MOVE 'A060-CTRL-DATA'
             TO WK-LABEL.
      *
           CALL LCCS0004           USING WK-PARAM
                                         WK-DATA-AMG
      *
           ON EXCEPTION
      *
              SET IDSV0003-GENERIC-ERROR TO TRUE

              MOVE WK-PGM        TO IDSV0001-COD-SERVIZIO-BE
              MOVE WK-LABEL      TO IDSV0001-LABEL-ERR
              MOVE 'LCCS0004'    TO IDSV0001-NOME-TABELLA

              MOVE 'ERRORE CHIAMATA MODULO LCCS0004'
                TO  IDSV0001-DESC-ERRORE-ESTESA
      *
           END-CALL.
      *
       A060-EX.
           EXIT.
      *----------------------------------------------------------------*
      * GSTIONE DIFFERENZA TRA DATE
      *----------------------------------------------------------------*
      *
       A070-GESTISCI-DIFF.
      *
           PERFORM Z700-DT-N-TO-X THRU Z700-EX.

           PERFORM Y000-CALCOLA-DIFFERENZA THRU Y000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF WS-DIFFERENZA > LIMITE-POSITIVO OR
                 WS-DIFFERENZA < LIMITE-NEGATIVO

                 SET IDSV0003-FIELD-NOT-VALUED TO TRUE

                 MOVE WK-PGM        TO IDSV0001-COD-SERVIZIO-BE
                 MOVE WK-LABEL      TO IDSV0001-LABEL-ERR
                 MOVE 'PARAM_COMP'  TO IDSV0001-NOME-TABELLA
                 MOVE WS-DIFFERENZA TO DIFFERENZA-DISPLAY

                 STRING 'DIFFERENZA CONTABILE NON VALIDA'
                        ' : '
                        DIFFERENZA-DISPLAY
                        DELIMITED BY SIZE INTO
                        IDSV0001-DESC-ERRORE-ESTESA
                 END-STRING

              END-IF
           END-IF.
      *
       A070-EX.
           EXIT.
      **********************************************************
       A100-CHECK-RETURN-CODE.

           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.

       A100-EX.
           EXIT.

      **********************************************************
       D000-TRATTA-DEBUG.

              MOVE PCO-FL-LIV-DEBUG   TO IDSV0001-LIVELLO-DEBUG.

       D000-EX.
           EXIT.
      **********************************************************
       E000-ESTRAI-ID-TMPRY-DATA.

           EXEC SQL
              VALUES NEXTVAL FOR SEQ_TEMPORARY_DATA
                INTO :IDSV0001-ID-TEMPORARY-DATA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF NOT IDSV0003-SUCCESSFUL-RC

              MOVE IDSV0003-SQLCODE TO WK-SQLCODE-ED
              STRING WK-PGM ' - CALCOLA ID TEMPORARY DATA'
                           ' - '
                           'SQLCODE : '
                           WK-SQLCODE-ED
                           DELIMITED BY SIZE INTO
                           IDSV0001-DESC-ERRORE-ESTESA
              END-STRING

           END-IF.

       E000-ESTRAI-ID-TMPRY-DATA-EX.
           EXIT.

      **********************************************************
       Y000-CALCOLA-DIFFERENZA.

      *---
      *--- CALCOLA DIFFERENZA TRA DATA DI SISTEMA E
      *--- DATA COMPETENZA DI CONTESTO
      *---
           EXEC SQL
                VALUES
                       DAYS(:WS-DATE-SYSTEM-DB) -
                       DAYS(:WS-DATE-X)
                INTO   :WS-DIFFERENZA
           END-EXEC

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF NOT IDSV0003-SUCCESSFUL-RC

              MOVE IDSV0003-SQLCODE TO WK-SQLCODE-ED
              STRING WK-PGM ' - CALCOLA DIFFERENZA TRA DATA DI SISTEMA'
                           ' E DATA COMPETENZA DI CONTESTO - '
                           'SQLCODE : '
                           WK-SQLCODE-ED
                           DELIMITED BY SIZE INTO
                           IDSV0001-DESC-ERRORE-ESTESA
              END-STRING

           END-IF.

       Y000-EX.
           EXIT.

      **********************************************************
       Z000-FASE-FINALE.

           IF NOT IDSV0003-SUCCESSFUL-RC
              SET IDSV0001-ESITO-KO       TO TRUE

              ADD 1 TO IDSV0001-MAX-ELE-ERRORI

              MOVE ERRORE-FATALE          TO IDSV0001-LIV-GRAVITA-BE
                                            (IDSV0001-MAX-ELE-ERRORI)

              MOVE IDSV0001-DESC-ERRORE-ESTESA
                                          TO IDSV0001-DESC-ERRORE
                                            (IDSV0001-MAX-ELE-ERRORI)

           END-IF.

       Z000-EX.
           EXIT.

      **********************************************************
       Z100-SET-COLONNE-NULL.

           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.

           IF IND-PCO-DT-CONT = -1
              MOVE HIGH-VALUES TO PCO-DT-CONT-NULL
           END-IF.

       Z100-EX.
           EXIT.

      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.

           IF IND-PCO-DT-CONT = 0
               MOVE PCO-DT-CONT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-CONT
           END-IF.

       Z950-EX.
           EXIT.
      *---
      *--- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
      *---
           COPY IDSP0014.


       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSA250 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  13 NOV 2018.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDA250 END-EXEC.
           EXEC SQL INCLUDE IDBVA252 END-EXEC.
           EXEC SQL INCLUDE IDBVA253 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVA251 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 PERS.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSA250'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'PERS' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_PERS
                ,TSTAM_INI_VLDT
                ,TSTAM_END_VLDT
                ,COD_PERS
                ,RIFTO_RETE
                ,COD_PRT_IVA
                ,IND_PVCY_PRSNL
                ,IND_PVCY_CMMRC
                ,IND_PVCY_INDST
                ,DT_NASC_CLI
                ,DT_ACQS_PERS
                ,IND_CLI
                ,COD_CMN
                ,COD_FRM_GIURD
                ,COD_ENTE_PUBB
                ,DEN_RGN_SOC
                ,DEN_SIG_RGN_SOC
                ,IND_ESE_FISC
                ,COD_STAT_CVL
                ,DEN_NOME
                ,DEN_COGN
                ,COD_FISC
                ,IND_SEX
                ,IND_CPCT_GIURD
                ,IND_PORT_HDCP
                ,COD_USER_INS
                ,TSTAM_INS_RIGA
                ,COD_USER_AGGM
                ,TSTAM_AGGM_RIGA
                ,DEN_CMN_NASC_STRN
                ,COD_RAMO_STGR
                ,COD_STGR_ATVT_UIC
                ,COD_RAMO_ATVT_UIC
                ,DT_END_VLDT_PERS
                ,DT_DEAD_PERS
                ,TP_STAT_CLI
                ,DT_BLOC_CLI
                ,COD_PERS_SECOND
                ,ID_SEGMENTAZ_CLI
                ,DT_1A_ATVT
                ,DT_SEGNAL_PARTNER
             INTO
                :A25-ID-PERS
               ,:A25-TSTAM-INI-VLDT
               ,:A25-TSTAM-END-VLDT
                :IND-A25-TSTAM-END-VLDT
               ,:A25-COD-PERS
               ,:A25-RIFTO-RETE
                :IND-A25-RIFTO-RETE
               ,:A25-COD-PRT-IVA
                :IND-A25-COD-PRT-IVA
               ,:A25-IND-PVCY-PRSNL
                :IND-A25-IND-PVCY-PRSNL
               ,:A25-IND-PVCY-CMMRC
                :IND-A25-IND-PVCY-CMMRC
               ,:A25-IND-PVCY-INDST
                :IND-A25-IND-PVCY-INDST
               ,:A25-DT-NASC-CLI-DB
                :IND-A25-DT-NASC-CLI
               ,:A25-DT-ACQS-PERS-DB
                :IND-A25-DT-ACQS-PERS
               ,:A25-IND-CLI
                :IND-A25-IND-CLI
               ,:A25-COD-CMN
                :IND-A25-COD-CMN
               ,:A25-COD-FRM-GIURD
                :IND-A25-COD-FRM-GIURD
               ,:A25-COD-ENTE-PUBB
                :IND-A25-COD-ENTE-PUBB
               ,:A25-DEN-RGN-SOC-VCHAR
                :IND-A25-DEN-RGN-SOC
               ,:A25-DEN-SIG-RGN-SOC-VCHAR
                :IND-A25-DEN-SIG-RGN-SOC
               ,:A25-IND-ESE-FISC
                :IND-A25-IND-ESE-FISC
               ,:A25-COD-STAT-CVL
                :IND-A25-COD-STAT-CVL
               ,:A25-DEN-NOME-VCHAR
                :IND-A25-DEN-NOME
               ,:A25-DEN-COGN-VCHAR
                :IND-A25-DEN-COGN
               ,:A25-COD-FISC
                :IND-A25-COD-FISC
               ,:A25-IND-SEX
                :IND-A25-IND-SEX
               ,:A25-IND-CPCT-GIURD
                :IND-A25-IND-CPCT-GIURD
               ,:A25-IND-PORT-HDCP
                :IND-A25-IND-PORT-HDCP
               ,:A25-COD-USER-INS
               ,:A25-TSTAM-INS-RIGA
               ,:A25-COD-USER-AGGM
                :IND-A25-COD-USER-AGGM
               ,:A25-TSTAM-AGGM-RIGA
                :IND-A25-TSTAM-AGGM-RIGA
               ,:A25-DEN-CMN-NASC-STRN-VCHAR
                :IND-A25-DEN-CMN-NASC-STRN
               ,:A25-COD-RAMO-STGR
                :IND-A25-COD-RAMO-STGR
               ,:A25-COD-STGR-ATVT-UIC
                :IND-A25-COD-STGR-ATVT-UIC
               ,:A25-COD-RAMO-ATVT-UIC
                :IND-A25-COD-RAMO-ATVT-UIC
               ,:A25-DT-END-VLDT-PERS-DB
                :IND-A25-DT-END-VLDT-PERS
               ,:A25-DT-DEAD-PERS-DB
                :IND-A25-DT-DEAD-PERS
               ,:A25-TP-STAT-CLI
                :IND-A25-TP-STAT-CLI
               ,:A25-DT-BLOC-CLI-DB
                :IND-A25-DT-BLOC-CLI
               ,:A25-COD-PERS-SECOND
                :IND-A25-COD-PERS-SECOND
               ,:A25-ID-SEGMENTAZ-CLI
                :IND-A25-ID-SEGMENTAZ-CLI
               ,:A25-DT-1A-ATVT-DB
                :IND-A25-DT-1A-ATVT
               ,:A25-DT-SEGNAL-PARTNER-DB
                :IND-A25-DT-SEGNAL-PARTNER
             FROM PERS
             WHERE     ID_PERS = :A25-ID-PERS
                   AND TSTAM_INI_VLDT = :A25-TSTAM-INI-VLDT

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO PERS
                     (
                        ID_PERS
                       ,TSTAM_INI_VLDT
                       ,TSTAM_END_VLDT
                       ,COD_PERS
                       ,RIFTO_RETE
                       ,COD_PRT_IVA
                       ,IND_PVCY_PRSNL
                       ,IND_PVCY_CMMRC
                       ,IND_PVCY_INDST
                       ,DT_NASC_CLI
                       ,DT_ACQS_PERS
                       ,IND_CLI
                       ,COD_CMN
                       ,COD_FRM_GIURD
                       ,COD_ENTE_PUBB
                       ,DEN_RGN_SOC
                       ,DEN_SIG_RGN_SOC
                       ,IND_ESE_FISC
                       ,COD_STAT_CVL
                       ,DEN_NOME
                       ,DEN_COGN
                       ,COD_FISC
                       ,IND_SEX
                       ,IND_CPCT_GIURD
                       ,IND_PORT_HDCP
                       ,COD_USER_INS
                       ,TSTAM_INS_RIGA
                       ,COD_USER_AGGM
                       ,TSTAM_AGGM_RIGA
                       ,DEN_CMN_NASC_STRN
                       ,COD_RAMO_STGR
                       ,COD_STGR_ATVT_UIC
                       ,COD_RAMO_ATVT_UIC
                       ,DT_END_VLDT_PERS
                       ,DT_DEAD_PERS
                       ,TP_STAT_CLI
                       ,DT_BLOC_CLI
                       ,COD_PERS_SECOND
                       ,ID_SEGMENTAZ_CLI
                       ,DT_1A_ATVT
                       ,DT_SEGNAL_PARTNER
                     )
                 VALUES
                     (
                       :A25-ID-PERS
                       ,:A25-TSTAM-INI-VLDT
                       ,:A25-TSTAM-END-VLDT
                        :IND-A25-TSTAM-END-VLDT
                       ,:A25-COD-PERS
                       ,:A25-RIFTO-RETE
                        :IND-A25-RIFTO-RETE
                       ,:A25-COD-PRT-IVA
                        :IND-A25-COD-PRT-IVA
                       ,:A25-IND-PVCY-PRSNL
                        :IND-A25-IND-PVCY-PRSNL
                       ,:A25-IND-PVCY-CMMRC
                        :IND-A25-IND-PVCY-CMMRC
                       ,:A25-IND-PVCY-INDST
                        :IND-A25-IND-PVCY-INDST
                       ,:A25-DT-NASC-CLI-DB
                        :IND-A25-DT-NASC-CLI
                       ,:A25-DT-ACQS-PERS-DB
                        :IND-A25-DT-ACQS-PERS
                       ,:A25-IND-CLI
                        :IND-A25-IND-CLI
                       ,:A25-COD-CMN
                        :IND-A25-COD-CMN
                       ,:A25-COD-FRM-GIURD
                        :IND-A25-COD-FRM-GIURD
                       ,:A25-COD-ENTE-PUBB
                        :IND-A25-COD-ENTE-PUBB
                       ,:A25-DEN-RGN-SOC-VCHAR
                        :IND-A25-DEN-RGN-SOC
                       ,:A25-DEN-SIG-RGN-SOC-VCHAR
                        :IND-A25-DEN-SIG-RGN-SOC
                       ,:A25-IND-ESE-FISC
                        :IND-A25-IND-ESE-FISC
                       ,:A25-COD-STAT-CVL
                        :IND-A25-COD-STAT-CVL
                       ,:A25-DEN-NOME-VCHAR
                        :IND-A25-DEN-NOME
                       ,:A25-DEN-COGN-VCHAR
                        :IND-A25-DEN-COGN
                       ,:A25-COD-FISC
                        :IND-A25-COD-FISC
                       ,:A25-IND-SEX
                        :IND-A25-IND-SEX
                       ,:A25-IND-CPCT-GIURD
                        :IND-A25-IND-CPCT-GIURD
                       ,:A25-IND-PORT-HDCP
                        :IND-A25-IND-PORT-HDCP
                       ,:A25-COD-USER-INS
                       ,:A25-TSTAM-INS-RIGA
                       ,:A25-COD-USER-AGGM
                        :IND-A25-COD-USER-AGGM
                       ,:A25-TSTAM-AGGM-RIGA
                        :IND-A25-TSTAM-AGGM-RIGA
                       ,:A25-DEN-CMN-NASC-STRN-VCHAR
                        :IND-A25-DEN-CMN-NASC-STRN
                       ,:A25-COD-RAMO-STGR
                        :IND-A25-COD-RAMO-STGR
                       ,:A25-COD-STGR-ATVT-UIC
                        :IND-A25-COD-STGR-ATVT-UIC
                       ,:A25-COD-RAMO-ATVT-UIC
                        :IND-A25-COD-RAMO-ATVT-UIC
                       ,:A25-DT-END-VLDT-PERS-DB
                        :IND-A25-DT-END-VLDT-PERS
                       ,:A25-DT-DEAD-PERS-DB
                        :IND-A25-DT-DEAD-PERS
                       ,:A25-TP-STAT-CLI
                        :IND-A25-TP-STAT-CLI
                       ,:A25-DT-BLOC-CLI-DB
                        :IND-A25-DT-BLOC-CLI
                       ,:A25-COD-PERS-SECOND
                        :IND-A25-COD-PERS-SECOND
                       ,:A25-ID-SEGMENTAZ-CLI
                        :IND-A25-ID-SEGMENTAZ-CLI
                       ,:A25-DT-1A-ATVT-DB
                        :IND-A25-DT-1A-ATVT
                       ,:A25-DT-SEGNAL-PARTNER-DB
                        :IND-A25-DT-SEGNAL-PARTNER
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE PERS SET

                   ID_PERS                =
                :A25-ID-PERS
                  ,TSTAM_INI_VLDT         =
                :A25-TSTAM-INI-VLDT
                  ,TSTAM_END_VLDT         =
                :A25-TSTAM-END-VLDT
                                       :IND-A25-TSTAM-END-VLDT
                  ,COD_PERS               =
                :A25-COD-PERS
                  ,RIFTO_RETE             =
                :A25-RIFTO-RETE
                                       :IND-A25-RIFTO-RETE
                  ,COD_PRT_IVA            =
                :A25-COD-PRT-IVA
                                       :IND-A25-COD-PRT-IVA
                  ,IND_PVCY_PRSNL         =
                :A25-IND-PVCY-PRSNL
                                       :IND-A25-IND-PVCY-PRSNL
                  ,IND_PVCY_CMMRC         =
                :A25-IND-PVCY-CMMRC
                                       :IND-A25-IND-PVCY-CMMRC
                  ,IND_PVCY_INDST         =
                :A25-IND-PVCY-INDST
                                       :IND-A25-IND-PVCY-INDST
                  ,DT_NASC_CLI            =
           :A25-DT-NASC-CLI-DB
                                       :IND-A25-DT-NASC-CLI
                  ,DT_ACQS_PERS           =
           :A25-DT-ACQS-PERS-DB
                                       :IND-A25-DT-ACQS-PERS
                  ,IND_CLI                =
                :A25-IND-CLI
                                       :IND-A25-IND-CLI
                  ,COD_CMN                =
                :A25-COD-CMN
                                       :IND-A25-COD-CMN
                  ,COD_FRM_GIURD          =
                :A25-COD-FRM-GIURD
                                       :IND-A25-COD-FRM-GIURD
                  ,COD_ENTE_PUBB          =
                :A25-COD-ENTE-PUBB
                                       :IND-A25-COD-ENTE-PUBB
                  ,DEN_RGN_SOC            =
                :A25-DEN-RGN-SOC-VCHAR
                                       :IND-A25-DEN-RGN-SOC
                  ,DEN_SIG_RGN_SOC        =
                :A25-DEN-SIG-RGN-SOC-VCHAR
                                       :IND-A25-DEN-SIG-RGN-SOC
                  ,IND_ESE_FISC           =
                :A25-IND-ESE-FISC
                                       :IND-A25-IND-ESE-FISC
                  ,COD_STAT_CVL           =
                :A25-COD-STAT-CVL
                                       :IND-A25-COD-STAT-CVL
                  ,DEN_NOME               =
                :A25-DEN-NOME-VCHAR
                                       :IND-A25-DEN-NOME
                  ,DEN_COGN               =
                :A25-DEN-COGN-VCHAR
                                       :IND-A25-DEN-COGN
                  ,COD_FISC               =
                :A25-COD-FISC
                                       :IND-A25-COD-FISC
                  ,IND_SEX                =
                :A25-IND-SEX
                                       :IND-A25-IND-SEX
                  ,IND_CPCT_GIURD         =
                :A25-IND-CPCT-GIURD
                                       :IND-A25-IND-CPCT-GIURD
                  ,IND_PORT_HDCP          =
                :A25-IND-PORT-HDCP
                                       :IND-A25-IND-PORT-HDCP
                  ,COD_USER_INS           =
                :A25-COD-USER-INS
                  ,TSTAM_INS_RIGA         =
                :A25-TSTAM-INS-RIGA
                  ,COD_USER_AGGM          =
                :A25-COD-USER-AGGM
                                       :IND-A25-COD-USER-AGGM
                  ,TSTAM_AGGM_RIGA        =
                :A25-TSTAM-AGGM-RIGA
                                       :IND-A25-TSTAM-AGGM-RIGA
                  ,DEN_CMN_NASC_STRN      =
                :A25-DEN-CMN-NASC-STRN-VCHAR
                                       :IND-A25-DEN-CMN-NASC-STRN
                  ,COD_RAMO_STGR          =
                :A25-COD-RAMO-STGR
                                       :IND-A25-COD-RAMO-STGR
                  ,COD_STGR_ATVT_UIC      =
                :A25-COD-STGR-ATVT-UIC
                                       :IND-A25-COD-STGR-ATVT-UIC
                  ,COD_RAMO_ATVT_UIC      =
                :A25-COD-RAMO-ATVT-UIC
                                       :IND-A25-COD-RAMO-ATVT-UIC
                  ,DT_END_VLDT_PERS       =
           :A25-DT-END-VLDT-PERS-DB
                                       :IND-A25-DT-END-VLDT-PERS
                  ,DT_DEAD_PERS           =
           :A25-DT-DEAD-PERS-DB
                                       :IND-A25-DT-DEAD-PERS
                  ,TP_STAT_CLI            =
                :A25-TP-STAT-CLI
                                       :IND-A25-TP-STAT-CLI
                  ,DT_BLOC_CLI            =
           :A25-DT-BLOC-CLI-DB
                                       :IND-A25-DT-BLOC-CLI
                  ,COD_PERS_SECOND        =
                :A25-COD-PERS-SECOND
                                       :IND-A25-COD-PERS-SECOND
                  ,ID_SEGMENTAZ_CLI       =
                :A25-ID-SEGMENTAZ-CLI
                                       :IND-A25-ID-SEGMENTAZ-CLI
                  ,DT_1A_ATVT             =
           :A25-DT-1A-ATVT-DB
                                       :IND-A25-DT-1A-ATVT
                  ,DT_SEGNAL_PARTNER      =
           :A25-DT-SEGNAL-PARTNER-DB
                                       :IND-A25-DT-SEGNAL-PARTNER
                WHERE     ID_PERS = :A25-ID-PERS
                   AND TSTAM_INI_VLDT = :A25-TSTAM-INI-VLDT

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM PERS
                WHERE     ID_PERS = :A25-ID-PERS
                   AND TSTAM_INI_VLDT = :A25-TSTAM-INI-VLDT

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-A25-TSTAM-END-VLDT = -1
              MOVE HIGH-VALUES TO A25-TSTAM-END-VLDT-NULL
           END-IF
           IF IND-A25-RIFTO-RETE = -1
              MOVE HIGH-VALUES TO A25-RIFTO-RETE-NULL
           END-IF
           IF IND-A25-COD-PRT-IVA = -1
              MOVE HIGH-VALUES TO A25-COD-PRT-IVA-NULL
           END-IF
           IF IND-A25-IND-PVCY-PRSNL = -1
              MOVE HIGH-VALUES TO A25-IND-PVCY-PRSNL-NULL
           END-IF
           IF IND-A25-IND-PVCY-CMMRC = -1
              MOVE HIGH-VALUES TO A25-IND-PVCY-CMMRC-NULL
           END-IF
           IF IND-A25-IND-PVCY-INDST = -1
              MOVE HIGH-VALUES TO A25-IND-PVCY-INDST-NULL
           END-IF
           IF IND-A25-DT-NASC-CLI = -1
              MOVE HIGH-VALUES TO A25-DT-NASC-CLI-NULL
           END-IF
           IF IND-A25-DT-ACQS-PERS = -1
              MOVE HIGH-VALUES TO A25-DT-ACQS-PERS-NULL
           END-IF
           IF IND-A25-IND-CLI = -1
              MOVE HIGH-VALUES TO A25-IND-CLI-NULL
           END-IF
           IF IND-A25-COD-CMN = -1
              MOVE HIGH-VALUES TO A25-COD-CMN-NULL
           END-IF
           IF IND-A25-COD-FRM-GIURD = -1
              MOVE HIGH-VALUES TO A25-COD-FRM-GIURD-NULL
           END-IF
           IF IND-A25-COD-ENTE-PUBB = -1
              MOVE HIGH-VALUES TO A25-COD-ENTE-PUBB-NULL
           END-IF
           IF IND-A25-DEN-RGN-SOC = -1
              MOVE HIGH-VALUES TO A25-DEN-RGN-SOC
           END-IF
           IF IND-A25-DEN-SIG-RGN-SOC = -1
              MOVE HIGH-VALUES TO A25-DEN-SIG-RGN-SOC
           END-IF
           IF IND-A25-IND-ESE-FISC = -1
              MOVE HIGH-VALUES TO A25-IND-ESE-FISC-NULL
           END-IF
           IF IND-A25-COD-STAT-CVL = -1
              MOVE HIGH-VALUES TO A25-COD-STAT-CVL-NULL
           END-IF
           IF IND-A25-DEN-NOME = -1
              MOVE HIGH-VALUES TO A25-DEN-NOME
           END-IF
           IF IND-A25-DEN-COGN = -1
              MOVE HIGH-VALUES TO A25-DEN-COGN
           END-IF
           IF IND-A25-COD-FISC = -1
              MOVE HIGH-VALUES TO A25-COD-FISC-NULL
           END-IF
           IF IND-A25-IND-SEX = -1
              MOVE HIGH-VALUES TO A25-IND-SEX-NULL
           END-IF
           IF IND-A25-IND-CPCT-GIURD = -1
              MOVE HIGH-VALUES TO A25-IND-CPCT-GIURD-NULL
           END-IF
           IF IND-A25-IND-PORT-HDCP = -1
              MOVE HIGH-VALUES TO A25-IND-PORT-HDCP-NULL
           END-IF
           IF IND-A25-COD-USER-AGGM = -1
              MOVE HIGH-VALUES TO A25-COD-USER-AGGM-NULL
           END-IF
           IF IND-A25-TSTAM-AGGM-RIGA = -1
              MOVE HIGH-VALUES TO A25-TSTAM-AGGM-RIGA-NULL
           END-IF
           IF IND-A25-DEN-CMN-NASC-STRN = -1
              MOVE HIGH-VALUES TO A25-DEN-CMN-NASC-STRN
           END-IF
           IF IND-A25-COD-RAMO-STGR = -1
              MOVE HIGH-VALUES TO A25-COD-RAMO-STGR-NULL
           END-IF
           IF IND-A25-COD-STGR-ATVT-UIC = -1
              MOVE HIGH-VALUES TO A25-COD-STGR-ATVT-UIC-NULL
           END-IF
           IF IND-A25-COD-RAMO-ATVT-UIC = -1
              MOVE HIGH-VALUES TO A25-COD-RAMO-ATVT-UIC-NULL
           END-IF
           IF IND-A25-DT-END-VLDT-PERS = -1
              MOVE HIGH-VALUES TO A25-DT-END-VLDT-PERS-NULL
           END-IF
           IF IND-A25-DT-DEAD-PERS = -1
              MOVE HIGH-VALUES TO A25-DT-DEAD-PERS-NULL
           END-IF
           IF IND-A25-TP-STAT-CLI = -1
              MOVE HIGH-VALUES TO A25-TP-STAT-CLI-NULL
           END-IF
           IF IND-A25-DT-BLOC-CLI = -1
              MOVE HIGH-VALUES TO A25-DT-BLOC-CLI-NULL
           END-IF
           IF IND-A25-COD-PERS-SECOND = -1
              MOVE HIGH-VALUES TO A25-COD-PERS-SECOND-NULL
           END-IF
           IF IND-A25-ID-SEGMENTAZ-CLI = -1
              MOVE HIGH-VALUES TO A25-ID-SEGMENTAZ-CLI-NULL
           END-IF
           IF IND-A25-DT-1A-ATVT = -1
              MOVE HIGH-VALUES TO A25-DT-1A-ATVT-NULL
           END-IF
           IF IND-A25-DT-SEGNAL-PARTNER = -1
              MOVE HIGH-VALUES TO A25-DT-SEGNAL-PARTNER-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.

           CONTINUE.
       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.

           CONTINUE.
       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF A25-TSTAM-END-VLDT-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-TSTAM-END-VLDT
           ELSE
              MOVE 0 TO IND-A25-TSTAM-END-VLDT
           END-IF
           IF A25-RIFTO-RETE-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-RIFTO-RETE
           ELSE
              MOVE 0 TO IND-A25-RIFTO-RETE
           END-IF
           IF A25-COD-PRT-IVA-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-PRT-IVA
           ELSE
              MOVE 0 TO IND-A25-COD-PRT-IVA
           END-IF
           IF A25-IND-PVCY-PRSNL-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-IND-PVCY-PRSNL
           ELSE
              MOVE 0 TO IND-A25-IND-PVCY-PRSNL
           END-IF
           IF A25-IND-PVCY-CMMRC-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-IND-PVCY-CMMRC
           ELSE
              MOVE 0 TO IND-A25-IND-PVCY-CMMRC
           END-IF
           IF A25-IND-PVCY-INDST-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-IND-PVCY-INDST
           ELSE
              MOVE 0 TO IND-A25-IND-PVCY-INDST
           END-IF
           IF A25-DT-NASC-CLI-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-DT-NASC-CLI
           ELSE
              MOVE 0 TO IND-A25-DT-NASC-CLI
           END-IF
           IF A25-DT-ACQS-PERS-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-DT-ACQS-PERS
           ELSE
              MOVE 0 TO IND-A25-DT-ACQS-PERS
           END-IF
           IF A25-IND-CLI-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-IND-CLI
           ELSE
              MOVE 0 TO IND-A25-IND-CLI
           END-IF
           IF A25-COD-CMN-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-CMN
           ELSE
              MOVE 0 TO IND-A25-COD-CMN
           END-IF
           IF A25-COD-FRM-GIURD-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-FRM-GIURD
           ELSE
              MOVE 0 TO IND-A25-COD-FRM-GIURD
           END-IF
           IF A25-COD-ENTE-PUBB-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-ENTE-PUBB
           ELSE
              MOVE 0 TO IND-A25-COD-ENTE-PUBB
           END-IF
           IF A25-DEN-RGN-SOC = HIGH-VALUES
              MOVE -1 TO IND-A25-DEN-RGN-SOC
           ELSE
              MOVE 0 TO IND-A25-DEN-RGN-SOC
           END-IF
           IF A25-DEN-SIG-RGN-SOC = HIGH-VALUES
              MOVE -1 TO IND-A25-DEN-SIG-RGN-SOC
           ELSE
              MOVE 0 TO IND-A25-DEN-SIG-RGN-SOC
           END-IF
           IF A25-IND-ESE-FISC-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-IND-ESE-FISC
           ELSE
              MOVE 0 TO IND-A25-IND-ESE-FISC
           END-IF
           IF A25-COD-STAT-CVL-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-STAT-CVL
           ELSE
              MOVE 0 TO IND-A25-COD-STAT-CVL
           END-IF
           IF A25-DEN-NOME = HIGH-VALUES
              MOVE -1 TO IND-A25-DEN-NOME
           ELSE
              MOVE 0 TO IND-A25-DEN-NOME
           END-IF
           IF A25-DEN-COGN = HIGH-VALUES
              MOVE -1 TO IND-A25-DEN-COGN
           ELSE
              MOVE 0 TO IND-A25-DEN-COGN
           END-IF
           IF A25-COD-FISC-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-FISC
           ELSE
              MOVE 0 TO IND-A25-COD-FISC
           END-IF
           IF A25-IND-SEX-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-IND-SEX
           ELSE
              MOVE 0 TO IND-A25-IND-SEX
           END-IF
           IF A25-IND-CPCT-GIURD-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-IND-CPCT-GIURD
           ELSE
              MOVE 0 TO IND-A25-IND-CPCT-GIURD
           END-IF
           IF A25-IND-PORT-HDCP-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-IND-PORT-HDCP
           ELSE
              MOVE 0 TO IND-A25-IND-PORT-HDCP
           END-IF
           IF A25-COD-USER-AGGM-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-USER-AGGM
           ELSE
              MOVE 0 TO IND-A25-COD-USER-AGGM
           END-IF
           IF A25-TSTAM-AGGM-RIGA-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-TSTAM-AGGM-RIGA
           ELSE
              MOVE 0 TO IND-A25-TSTAM-AGGM-RIGA
           END-IF
           IF A25-DEN-CMN-NASC-STRN = HIGH-VALUES
              MOVE -1 TO IND-A25-DEN-CMN-NASC-STRN
           ELSE
              MOVE 0 TO IND-A25-DEN-CMN-NASC-STRN
           END-IF
           IF A25-COD-RAMO-STGR-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-RAMO-STGR
           ELSE
              MOVE 0 TO IND-A25-COD-RAMO-STGR
           END-IF
           IF A25-COD-STGR-ATVT-UIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-STGR-ATVT-UIC
           ELSE
              MOVE 0 TO IND-A25-COD-STGR-ATVT-UIC
           END-IF
           IF A25-COD-RAMO-ATVT-UIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-RAMO-ATVT-UIC
           ELSE
              MOVE 0 TO IND-A25-COD-RAMO-ATVT-UIC
           END-IF
           IF A25-DT-END-VLDT-PERS-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-DT-END-VLDT-PERS
           ELSE
              MOVE 0 TO IND-A25-DT-END-VLDT-PERS
           END-IF
           IF A25-DT-DEAD-PERS-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-DT-DEAD-PERS
           ELSE
              MOVE 0 TO IND-A25-DT-DEAD-PERS
           END-IF
           IF A25-TP-STAT-CLI-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-TP-STAT-CLI
           ELSE
              MOVE 0 TO IND-A25-TP-STAT-CLI
           END-IF
           IF A25-DT-BLOC-CLI-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-DT-BLOC-CLI
           ELSE
              MOVE 0 TO IND-A25-DT-BLOC-CLI
           END-IF
           IF A25-COD-PERS-SECOND-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-PERS-SECOND
           ELSE
              MOVE 0 TO IND-A25-COD-PERS-SECOND
           END-IF
           IF A25-ID-SEGMENTAZ-CLI-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-ID-SEGMENTAZ-CLI
           ELSE
              MOVE 0 TO IND-A25-ID-SEGMENTAZ-CLI
           END-IF
           IF A25-DT-1A-ATVT-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-DT-1A-ATVT
           ELSE
              MOVE 0 TO IND-A25-DT-1A-ATVT
           END-IF
           IF A25-DT-SEGNAL-PARTNER-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-DT-SEGNAL-PARTNER
           ELSE
              MOVE 0 TO IND-A25-DT-SEGNAL-PARTNER
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.

           CONTINUE.
       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.
           CONTINUE.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.
           CONTINUE.
       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.
           CONTINUE.
       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           IF IND-A25-DT-NASC-CLI = 0
               MOVE A25-DT-NASC-CLI TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO A25-DT-NASC-CLI-DB
           END-IF
           IF IND-A25-DT-ACQS-PERS = 0
               MOVE A25-DT-ACQS-PERS TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO A25-DT-ACQS-PERS-DB
           END-IF
           IF IND-A25-DT-END-VLDT-PERS = 0
               MOVE A25-DT-END-VLDT-PERS TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO A25-DT-END-VLDT-PERS-DB
           END-IF
           IF IND-A25-DT-DEAD-PERS = 0
               MOVE A25-DT-DEAD-PERS TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO A25-DT-DEAD-PERS-DB
           END-IF
           IF IND-A25-DT-BLOC-CLI = 0
               MOVE A25-DT-BLOC-CLI TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO A25-DT-BLOC-CLI-DB
           END-IF
           IF IND-A25-DT-1A-ATVT = 0
               MOVE A25-DT-1A-ATVT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO A25-DT-1A-ATVT-DB
           END-IF
           IF IND-A25-DT-SEGNAL-PARTNER = 0
               MOVE A25-DT-SEGNAL-PARTNER TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO A25-DT-SEGNAL-PARTNER-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           IF IND-A25-DT-NASC-CLI = 0
               MOVE A25-DT-NASC-CLI-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO A25-DT-NASC-CLI
           END-IF
           IF IND-A25-DT-ACQS-PERS = 0
               MOVE A25-DT-ACQS-PERS-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO A25-DT-ACQS-PERS
           END-IF
           IF IND-A25-DT-END-VLDT-PERS = 0
               MOVE A25-DT-END-VLDT-PERS-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO A25-DT-END-VLDT-PERS
           END-IF
           IF IND-A25-DT-DEAD-PERS = 0
               MOVE A25-DT-DEAD-PERS-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO A25-DT-DEAD-PERS
           END-IF
           IF IND-A25-DT-BLOC-CLI = 0
               MOVE A25-DT-BLOC-CLI-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO A25-DT-BLOC-CLI
           END-IF
           IF IND-A25-DT-1A-ATVT = 0
               MOVE A25-DT-1A-ATVT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO A25-DT-1A-ATVT
           END-IF
           IF IND-A25-DT-SEGNAL-PARTNER = 0
               MOVE A25-DT-SEGNAL-PARTNER-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO A25-DT-SEGNAL-PARTNER
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.


           MOVE LENGTH OF A25-DEN-RGN-SOC
                       TO A25-DEN-RGN-SOC-LEN
           MOVE LENGTH OF A25-DEN-SIG-RGN-SOC
                       TO A25-DEN-SIG-RGN-SOC-LEN
           MOVE LENGTH OF A25-DEN-NOME
                       TO A25-DEN-NOME-LEN
           MOVE LENGTH OF A25-DEN-COGN
                       TO A25-DEN-COGN-LEN
           MOVE LENGTH OF A25-DEN-CMN-NASC-STRN
                       TO A25-DEN-CMN-NASC-STRN-LEN.

       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

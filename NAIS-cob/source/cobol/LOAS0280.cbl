      *****************************************************************
      **                                                              *
      **    PORTAFOGLIO VITA ITALIA VER. 1.0                          *
      **                                                              *
      *****************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LOAS0280.
       AUTHOR.             A.I.& S.S.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      **--------------------------------------------------------------**
      *    PROGRAMMA ..... LOAS0280
      *    TIPOLOGIA...... CONTROLLO STATO DEROGA
      *    PROCESSO.......
      *    FUNZIONE.......
      *    DESCRIZIONE....
      *
      **--------------------------------------------------------------**
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------
      *    COSTANTI
      *----------------------------------------------------------------
       01  WK-PGM                       PIC X(08) VALUE 'LOAS0280'.

      *
      *----------------------------------------------------------------*
      *    COPY DELLE TABELLE DB2
      *----------------------------------------------------------------*
      *--> STATO OGGETTO WORKFLOW
           COPY IDBVSTW1.
      *--> OGGETTO DEROGA
           COPY IDBVODE1.

      *
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01 WK-MESS-VAR.
          05 WK-LABEL-ERR         PIC X(30).
          05 WK-MESS              PIC X(15).

       01 WK-CTRL-TP-OGG                        PIC X(2).
          88 WK-TP-OGG-VALID                    VALUE
                                                  'PO',
                                                  'AD'.
      *
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           05 IX-ERR                    PIC S9(04) COMP.
           05 IX-TAB-ERR                PIC S9(04) COMP.
      *
      *
      ******************************************************************
      *  TP_MOVI (Tipi Movimenti)
      ******************************************************************
           COPY LCCC0006.
           COPY LCCVXOG0.
      *
      ******************************************************************
      *    COPY DISPATCHER
      ******************************************************************
      *
       01  IDSV0012.
           COPY IDSV0012.
      *
       01  DISPATCHER-VARIABLES.
      ** COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      ** COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.
      *
      ** COPY WHERE CONDITION JOIN STW-ODE
           COPY LDBV3361.
      *
      *----------------------------------------------------------------
      *      COPY VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
           COPY IEAV9903.
      *
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.

       01  WORKAREA.
           COPY LOAC0280.
      *
      *----------------------------------------------------------------*
      *    COMMAREA SERVIZIO
      *----------------------------------------------------------------*
       01 AREA-IO-STATI.
           03 WCOM-AREA-STATI.
              COPY LCCC0001              REPLACING ==(SF)== BY ==WCOM==.
      *--> AREA IO CONTROLLI STATI E CAUSALI
           03 S280-AREA-LCCC0261.
              COPY LCCC0261              REPLACING ==(SF)== BY ==WCOM==.
      *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                AREA-IO-STATI
                                WORKAREA.
      *----------------------------------------------------------------*
      *
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.
      *
           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.
      *
           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.
      *
      *----------------------------------------------------------------
      *    OPERAZIONI INIZIALI
      *----------------------------------------------------------------
       S0000-OPERAZIONI-INIZIALI.
      *
           INITIALIZE
               IX-INDICI.
           SET LOAC0280-DEROGA-BLOCCANTE-NO TO TRUE
           SET LOAC0280-NO-DEROGA           TO TRUE
           SET LOAC0280-NULL                TO TRUE
      *
      *--> CONTROLLI FORMALI
           PERFORM S0050-CONTROLLI            THRU EX-S0050.

      *
       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTROLLI FORMALLI                                          *
      *----------------------------------------------------------------*
       S0050-CONTROLLI.

           MOVE LOAC0280-TP-OGG              TO WK-CTRL-TP-OGG
                                                WS-TP-OGG.

      *--> TIPO OGGETTO (VALIDO)
           IF IDSV0001-ESITO-OK
              IF NOT WK-TP-OGG-VALID
      *          IL CAMPO $ DEVE ESSERE VALORIZZATO
                 MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S0050-CONTROLLI'      TO IEAI9901-LABEL-ERR
                 MOVE '005018'               TO IEAI9901-COD-ERRORE
                 MOVE 'TIPO OGGETTO'         TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

      *--> TIPO OGGETTO (VALORIZZATO)
           IF IDSV0001-ESITO-OK
              IF LOAC0280-TP-OGG  = SPACES OR HIGH-VALUE
      *          IL CAMPO $ DEVE ESSERE VALORIZZATO
                 MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S0050-CONTROLLI'      TO IEAI9901-LABEL-ERR
                 MOVE '005007'               TO IEAI9901-COD-ERRORE
                 MOVE 'TIPO OGGETTO'         TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

      *--> ID POLIZZA
           IF LOAC0280-ID-POLI = ZEROES
      *         IL CAMPO $ DEVE ESSERE VALORIZZATO
                MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                MOVE 'S0050-CONTROLLI'     TO IEAI9901-LABEL-ERR
                MOVE '005007'              TO IEAI9901-COD-ERRORE
                MOVE 'ID POLIZZA'          TO IEAI9901-PARAMETRI-ERR
                PERFORM S0300-RICERCA-GRAVITA-ERRORE
                   THRU EX-S0300
           END-IF.

      *--> ID ADESIONE
           IF IDSV0001-ESITO-OK
              IF ADESIONE
                 IF LOAC0280-ID-ADES = ZEROES
      *             IL CAMPO $ DEVE ESSERE VALORIZZATO
                    MOVE WK-PGM              TO IEAI9901-COD-SERVIZIO-BE
                    MOVE 'S0050-CONTROLLI'   TO IEAI9901-LABEL-ERR
                    MOVE '005007'            TO IEAI9901-COD-ERRORE
                    MOVE 'ID ADESIONE'       TO IEAI9901-PARAMETRI-ERR
                    PERFORM S0300-RICERCA-GRAVITA-ERRORE
                       THRU EX-S0300
                 END-IF
              END-IF
           END-IF.

       EX-S0050.
           EXIT.
      *
      *----------------------------------------------------------------
      *    ELABORAZIONE
      *----------------------------------------------------------------
       S1000-ELABORAZIONE.

           IF  IDSV0001-ESITO-OK AND
               ADESIONE
               PERFORM S5000-JOIN-STW-ODE-ADE
                  THRU EX-S5000

           END-IF

           IF  IDSV0001-ESITO-OK AND
               LOAC0280-DEROGA-BLOCCANTE-NO
               PERFORM S4000-JOIN-STW-ODE-POL
                  THRU EX-S4000
           END-IF.
      *
       EX-S1000.
           EXIT.
      *
      *----------------------------------------------------------------*
      * JOIN TABELLE STATO OGGETTO WORKFLOW E OGGETTO DEROGA PER
      * POLIZZA.
      *----------------------------------------------------------------*
       S4000-JOIN-STW-ODE-POL.
      *
           IF IDSV0001-ESITO-OK
             MOVE LOAC0280-ID-POLI
               TO LDBV3361-ID-OGG

             SET  POLIZZA
              TO TRUE

             MOVE WS-TP-OGG
               TO LDBV3361-TP-OGG

             PERFORM S4110-PREPARA-AREA-LDBS3360
                THRU EX-S4110
           END-IF.

           IF IDSV0001-ESITO-OK
              PERFORM S4120-CALL-LDBS3360
                 THRU EX-S4120
           END-IF.
      *
       EX-S4000.
           EXIT.

      *----------------------------------------------------------------*
      * Preapara dati x call modulo LDBS3360
      *----------------------------------------------------------------*
       S4110-PREPARA-AREA-LDBS3360.
           MOVE ZEROES
             TO IDSI0011-DATA-INIZIO-EFFETTO
                IDSI0011-DATA-FINE-EFFETTO
                IDSI0011-DATA-COMPETENZA.

            SET IDSI0011-TRATT-DEFAULT
             TO TRUE.

           MOVE 'LDBS3360'
             TO IDSI0011-CODICE-STR-DATO.

           MOVE LDBV3361
             TO IDSI0011-BUFFER-WHERE-COND

            SET IDSI0011-WHERE-CONDITION
             TO TRUE

            SET IDSI0011-SELECT
             TO TRUE.

            SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.

            SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.

       EX-S4110.
           EXIT.

      *----------------------------------------------------------------*
      * CALL MODULO LDBS3360
      *----------------------------------------------------------------*
       S4120-CALL-LDBS3360.
           MOVE 'S4120-CALL-LDBS3360'
             TO  WK-LABEL-ERR

           PERFORM S6000-SET-MESSAGGIO
              THRU EX-S6000.

      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
      *--> NESSUNA DEROGA ATTIVA TROVATA
                       CONTINUE

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE

                       SET LOAC0280-SI-DEROGA
                        TO TRUE

                       SET LOAC0280-DEROGA-BLOCCANTE-SI
                        TO TRUE

                       IF POLIZZA
                          SET LOAC0280-LIV-POLI
                           TO TRUE
                       ELSE
                          SET LOAC0280-LIV-ADES
                           TO TRUE
                       END-IF

                  WHEN OTHER
      *--> ERRORE DI ACCESSO AL DB
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL-ERR
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING WK-MESS   ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                         DELIMITED BY SIZE
                         INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *--> ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '005056'
                TO IEAI9901-COD-ERRORE
              STRING WK-MESS   ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                DELIMITED BY SIZE
                INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.


       EX-S4120.
           EXIT.

      *----------------------------------------------------------------*
      * JOIN TABELLE STATO OGGETTO WORKFLOW E OGGETTO DEROGA PER
      * ADESIONE.
      *----------------------------------------------------------------*
       S5000-JOIN-STW-ODE-ADE.
      *

           IF IDSV0001-ESITO-OK
             MOVE LOAC0280-ID-ADES
               TO LDBV3361-ID-OGG

             MOVE WS-TP-OGG
               TO LDBV3361-TP-OGG

             PERFORM S4110-PREPARA-AREA-LDBS3360
                THRU EX-S4110
           END-IF.

           IF IDSV0001-ESITO-OK
              PERFORM S4120-CALL-LDBS3360
                 THRU EX-S4120
           END-IF.

      *
       EX-S5000.
           EXIT.
      *---------------------------------------------------------------
      *
      *---------------------------------------------------------------
       S6000-SET-MESSAGGIO.
           IF POLIZZA
             MOVE 'JOIN STW-POL' TO WK-MESS
           ELSE
             MOVE 'JOIN STW-ODE' TO WK-MESS
           END-IF.
       EX-S6000.
           EXIT.
      *
      *---------------------------------------------------------------
      *    OPERAZIONI FINALI
      *---------------------------------------------------------------
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.
      *---------------------------------------------------------------
      *    ROUTINES GESTIONE ERRORI
      *---------------------------------------------------------------
           COPY IERP9901.
           COPY IERP9902.
           COPY IERP9903.
      *
      *----------------------------------------------------------------*
      *    ROUTINES DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.
      *

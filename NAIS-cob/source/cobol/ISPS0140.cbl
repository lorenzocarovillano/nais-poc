      ******************************************************************
      **                                                        ********
      **    PORTAFOGLIO VITA ITALIA  VER 1.0                    ********
      **                                                        ********
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         ISPS0140.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2009.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *    PROGRAMMA ..... ISPS0140
      *    TIPOLOGIA...... ACTUATOR
      *    PROCESSO....... XXX
      *    FUNZIONE....... XXX
      *    DESCRIZIONE.... SERVIZIO CALCOLI NOTEVOLI E LIQUIDAZIONE
      *----------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    VARIABILI DI WORKING
      *----------------------------------------------------------------*
       01  WK-PGM                         PIC X(008) VALUE 'ISPS0140'.

      *----------------------------------------------------------------*
      *    PUNTATORE AREA PRODOTTO
      *----------------------------------------------------------------*
       01  MQ02-ADDRESS.
           05 WS-ADDRESS USAGE POINTER.

      *----------------------------------------------------------------*
      *    DEFINIZIONE DI INDICI                                       *
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-TAB-ERR                  PIC S9(04) COMP VALUE 0.

      *----------------------------------------------------------------*
      * CHIAMATA AL SERVIZIO INFRASTRUTTURALE PER COLLEGAMENTO AL
      * SERVIZI DI PRODOTTO
      *----------------------------------------------------------------*
           COPY IJCCMQ02.

      *----------------------------------------------------------------*
      *    COPY VARIBILI PER LA GESTIONE ERRORI                        *
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
           COPY IEAV9903.

      *----------------------------------------------------------------*
      * MODULI CHIAMATI                                                *
      *----------------------------------------------------------------*
       01  INTERF-MQSERIES                    PIC X(8) VALUE 'IJCSMQ02'.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.

       01  WCOM-AREA-STATI.
           COPY LCCC0001             REPLACING ==(SF)== BY ==WCOM==.

       01  AREA-IO-ISPS0140.
           COPY ISPC0140.

      *---------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WCOM-AREA-STATI
                                AREA-IO-ISPS0140.
      *---------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      * OPERAZIONI INIZIALI                                            *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           CONTINUE.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *  ELABORAZIONE                                                  *
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> VALORIZZA AREA SERVIZIO ISPS0140
           PERFORM S1050-PREP-AREA-ISPS0140
              THRU EX-S1050.

      *--> CALL SERVIZIO ISPS0140
           PERFORM S1100-CALL-ISPS0140
              THRU EX-S1100.

       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA SERVIZIO DATI ISPS0140
      *----------------------------------------------------------------*
       S1050-PREP-AREA-ISPS0140.

           MOVE LOW-VALUES
             TO AREA-PRODUCT-SERVICES.

           MOVE IDSV0001-MODALITA-ESECUTIVA
             TO IJCCMQ02-MODALITA-ESECUTIVA.

           SET IJCCMQ02-ESITO-OK TO TRUE.

           MOVE 'ISPS0140'       TO IJCCMQ02-JAVA-SERVICE-NAME.

       EX-S1050.
           EXIT.

      *----------------------------------------------------------------*
      *  CHIAMATA AL SERVIZIO ISPS0140
      *----------------------------------------------------------------*
       S1100-CALL-ISPS0140.

           MOVE LENGTH OF AREA-IO-ISPS0140
             TO IJCCMQ02-LENGTH-DATI-SERVIZIO.

           MOVE AREA-IO-ISPS0140
             TO IJCCMQ02-AREA-DATI-SERVIZIO.

           SET WS-ADDRESS TO ADDRESS OF AREA-PRODUCT-SERVICES.

           MOVE IDSV0001-LIVELLO-DEBUG   TO IJCCMQ00-LIVELLO-DEBUG
           MOVE IDSV0001-AREA-ADDRESSES  TO IJCCMQ02-AREA-ADDRESSES
           MOVE IDSV0001-USER-NAME       TO IJCCMQ02-USER-NAME

           CALL INTERF-MQSERIES USING MQ02-ADDRESS
                                      AREA-PRODUCT-SERVICES
           ON EXCEPTION
              MOVE WK-PGM                   TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SERVIZIO DATI PRODOTTO' TO CALL-DESC
              MOVE 'S1100-CALL-ISPS0140'    TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

      *--> RETURNCODE ACTUATOR
           IF IJCCMQ02-ESITO-OK
              MOVE IJCCMQ02-AREA-DATI-SERVIZIO
                TO AREA-IO-ISPS0140

              MOVE ISPC0140-AREA-ERRORI TO ISPC0001-AREA-ERRORI

              PERFORM S0320-OUTPUT-PRODOTTO
                 THRU EX-S0320-OUTPUT-PRODOTTO
           ELSE
      *--> GESTIONE ERRORI AREA IJCSMQ02
              SET IDSV0001-ESITO-KO TO TRUE
              MOVE IJCCMQ02-MAX-ELE-ERRORI
                                TO IDSV0001-MAX-ELE-ERRORI
              PERFORM VARYING IX-TAB-ERR FROM 1 BY 1
                        UNTIL IX-TAB-ERR > IJCCMQ02-MAX-ELE-ERRORI
                  MOVE IJCCMQ02-COD-ERRORE(IX-TAB-ERR)
                    TO IDSV0001-COD-ERRORE(IX-TAB-ERR)
                  MOVE IJCCMQ02-DESC-ERRORE(IX-TAB-ERR)
                    TO IDSV0001-DESC-ERRORE(IX-TAB-ERR)
                  MOVE IJCCMQ02-LIV-GRAVITA-BE(IX-TAB-ERR)
                    TO IDSV0001-LIV-GRAVITA-BE(IX-TAB-ERR)
              END-PERFORM
           END-IF.

       EX-S1100.
           EXIT.

      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORE
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.
           COPY IERP9903.

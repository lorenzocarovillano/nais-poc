       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSB030 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  12 SET 2014.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDB030 END-EXEC.
           EXEC SQL INCLUDE IDBVB032 END-EXEC.
           EXEC SQL INCLUDE IDBVB033 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVB031 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 BILA-TRCH-ESTR.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSB030'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'BILA_TRCH_ESTR' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_BILA_TRCH_ESTR
                ,COD_COMP_ANIA
                ,ID_RICH_ESTRAZ_MAS
                ,ID_RICH_ESTRAZ_AGG
                ,FL_SIMULAZIONE
                ,DT_RIS
                ,DT_PRODUZIONE
                ,ID_POLI
                ,ID_ADES
                ,ID_GAR
                ,ID_TRCH_DI_GAR
                ,TP_FRM_ASSVA
                ,TP_RAMO_BILA
                ,TP_CALC_RIS
                ,COD_RAMO
                ,COD_TARI
                ,DT_INI_VAL_TAR
                ,COD_PROD
                ,DT_INI_VLDT_PROD
                ,COD_TARI_ORGN
                ,MIN_GARTO_T
                ,TP_TARI
                ,TP_PRE
                ,TP_ADEG_PRE
                ,TP_RIVAL
                ,FL_DA_TRASF
                ,FL_CAR_CONT
                ,FL_PRE_DA_RIS
                ,FL_PRE_AGG
                ,TP_TRCH
                ,TP_TST
                ,COD_CONV
                ,DT_DECOR_POLI
                ,DT_DECOR_ADES
                ,DT_DECOR_TRCH
                ,DT_EMIS_POLI
                ,DT_EMIS_TRCH
                ,DT_SCAD_TRCH
                ,DT_SCAD_INTMD
                ,DT_SCAD_PAG_PRE
                ,DT_ULT_PRE_PAG
                ,DT_NASC_1O_ASSTO
                ,SEX_1O_ASSTO
                ,ETA_AA_1O_ASSTO
                ,ETA_MM_1O_ASSTO
                ,ETA_RAGGN_DT_CALC
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,DUR_1O_PER_AA
                ,DUR_1O_PER_MM
                ,DUR_1O_PER_GG
                ,ANTIDUR_RICOR_PREC
                ,ANTIDUR_DT_CALC
                ,DUR_RES_DT_CALC
                ,TP_STAT_BUS_POLI
                ,TP_CAUS_POLI
                ,TP_STAT_BUS_ADES
                ,TP_CAUS_ADES
                ,TP_STAT_BUS_TRCH
                ,TP_CAUS_TRCH
                ,DT_EFF_CAMB_STAT
                ,DT_EMIS_CAMB_STAT
                ,DT_EFF_STAB
                ,CPT_DT_STAB
                ,DT_EFF_RIDZ
                ,DT_EMIS_RIDZ
                ,CPT_DT_RIDZ
                ,FRAZ
                ,DUR_PAG_PRE
                ,NUM_PRE_PATT
                ,FRAZ_INI_EROG_REN
                ,AA_REN_CER
                ,RAT_REN
                ,COD_DIV
                ,RISCPAR
                ,CUM_RISCPAR
                ,ULT_RM
                ,TS_RENDTO_T
                ,ALQ_RETR_T
                ,MIN_TRNUT_T
                ,TS_NET_T
                ,DT_ULT_RIVAL
                ,PRSTZ_INI
                ,PRSTZ_AGG_INI
                ,PRSTZ_AGG_ULT
                ,RAPPEL
                ,PRE_PATTUITO_INI
                ,PRE_DOV_INI
                ,PRE_DOV_RIVTO_T
                ,PRE_ANNUALIZ_RICOR
                ,PRE_CONT
                ,PRE_PP_INI
                ,RIS_PURA_T
                ,PROV_ACQ
                ,PROV_ACQ_RICOR
                ,PROV_INC
                ,CAR_ACQ_NON_SCON
                ,OVER_COMM
                ,CAR_ACQ_PRECONTATO
                ,RIS_ACQ_T
                ,RIS_ZIL_T
                ,CAR_GEST_NON_SCON
                ,CAR_GEST
                ,RIS_SPE_T
                ,CAR_INC_NON_SCON
                ,CAR_INC
                ,RIS_RISTORNI_CAP
                ,INTR_TECN
                ,CPT_RSH_MOR
                ,C_SUBRSH_T
                ,PRE_RSH_T
                ,ALQ_MARG_RIS
                ,ALQ_MARG_C_SUBRSH
                ,TS_RENDTO_SPPR
                ,TP_IAS
                ,NS_QUO
                ,TS_MEDIO
                ,CPT_RIASTO
                ,PRE_RIASTO
                ,RIS_RIASTA
                ,CPT_RIASTO_ECC
                ,PRE_RIASTO_ECC
                ,RIS_RIASTA_ECC
                ,COD_AGE
                ,COD_SUBAGE
                ,COD_CAN
                ,IB_POLI
                ,IB_ADES
                ,IB_TRCH_DI_GAR
                ,TP_PRSTZ
                ,TP_TRASF
                ,PP_INVRIO_TARI
                ,COEFF_OPZ_REN
                ,COEFF_OPZ_CPT
                ,DUR_PAG_REN
                ,VLT
                ,RIS_MAT_CHIU_PREC
                ,COD_FND
                ,PRSTZ_T
                ,TS_TARI_DOV
                ,TS_TARI_SCON
                ,TS_PP
                ,COEFF_RIS_1_T
                ,COEFF_RIS_2_T
                ,ABB
                ,TP_COASS
                ,TRAT_RIASS
                ,TRAT_RIASS_ECC
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TP_RGM_FISC
                ,DUR_GAR_AA
                ,DUR_GAR_MM
                ,DUR_GAR_GG
                ,ANTIDUR_CALC_365
                ,COD_FISC_CNTR
                ,COD_FISC_ASSTO1
                ,COD_FISC_ASSTO2
                ,COD_FISC_ASSTO3
                ,CAUS_SCON
                ,EMIT_TIT_OPZ
                ,QTZ_SP_Z_COUP_EMIS
                ,QTZ_SP_Z_OPZ_EMIS
                ,QTZ_SP_Z_COUP_DT_C
                ,QTZ_SP_Z_OPZ_DT_CA
                ,QTZ_TOT_EMIS
                ,QTZ_TOT_DT_CALC
                ,QTZ_TOT_DT_ULT_BIL
                ,DT_QTZ_EMIS
                ,PC_CAR_GEST
                ,PC_CAR_ACQ
                ,IMP_CAR_CASO_MOR
                ,PC_CAR_MOR
                ,TP_VERS
                ,FL_SWITCH
                ,FL_IAS
                ,DIR
                ,TP_COP_CASO_MOR
                ,MET_RISC_SPCL
                ,TP_STAT_INVST
                ,COD_PRDT
                ,STAT_ASSTO_1
                ,STAT_ASSTO_2
                ,STAT_ASSTO_3
                ,CPT_ASSTO_INI_MOR
                ,TS_STAB_PRE
                ,DIR_EMIS
                ,DT_INC_ULT_PRE
                ,STAT_TBGC_ASSTO_1
                ,STAT_TBGC_ASSTO_2
                ,STAT_TBGC_ASSTO_3
                ,FRAZ_DECR_CPT
                ,PRE_PP_ULT
                ,ACQ_EXP
                ,REMUN_ASS
                ,COMMIS_INTER
                ,NUM_FINANZ
                ,TP_ACC_COMM
                ,IB_ACC_COMM
                ,RAMO_BILA
                ,CARZ
             INTO
                :B03-ID-BILA-TRCH-ESTR
               ,:B03-COD-COMP-ANIA
               ,:B03-ID-RICH-ESTRAZ-MAS
               ,:B03-ID-RICH-ESTRAZ-AGG
                :IND-B03-ID-RICH-ESTRAZ-AGG
               ,:B03-FL-SIMULAZIONE
                :IND-B03-FL-SIMULAZIONE
               ,:B03-DT-RIS-DB
               ,:B03-DT-PRODUZIONE-DB
               ,:B03-ID-POLI
               ,:B03-ID-ADES
               ,:B03-ID-GAR
               ,:B03-ID-TRCH-DI-GAR
               ,:B03-TP-FRM-ASSVA
               ,:B03-TP-RAMO-BILA
               ,:B03-TP-CALC-RIS
               ,:B03-COD-RAMO
               ,:B03-COD-TARI
               ,:B03-DT-INI-VAL-TAR-DB
                :IND-B03-DT-INI-VAL-TAR
               ,:B03-COD-PROD
                :IND-B03-COD-PROD
               ,:B03-DT-INI-VLDT-PROD-DB
               ,:B03-COD-TARI-ORGN
                :IND-B03-COD-TARI-ORGN
               ,:B03-MIN-GARTO-T
                :IND-B03-MIN-GARTO-T
               ,:B03-TP-TARI
                :IND-B03-TP-TARI
               ,:B03-TP-PRE
                :IND-B03-TP-PRE
               ,:B03-TP-ADEG-PRE
                :IND-B03-TP-ADEG-PRE
               ,:B03-TP-RIVAL
                :IND-B03-TP-RIVAL
               ,:B03-FL-DA-TRASF
                :IND-B03-FL-DA-TRASF
               ,:B03-FL-CAR-CONT
                :IND-B03-FL-CAR-CONT
               ,:B03-FL-PRE-DA-RIS
                :IND-B03-FL-PRE-DA-RIS
               ,:B03-FL-PRE-AGG
                :IND-B03-FL-PRE-AGG
               ,:B03-TP-TRCH
                :IND-B03-TP-TRCH
               ,:B03-TP-TST
                :IND-B03-TP-TST
               ,:B03-COD-CONV
                :IND-B03-COD-CONV
               ,:B03-DT-DECOR-POLI-DB
               ,:B03-DT-DECOR-ADES-DB
                :IND-B03-DT-DECOR-ADES
               ,:B03-DT-DECOR-TRCH-DB
               ,:B03-DT-EMIS-POLI-DB
               ,:B03-DT-EMIS-TRCH-DB
                :IND-B03-DT-EMIS-TRCH
               ,:B03-DT-SCAD-TRCH-DB
                :IND-B03-DT-SCAD-TRCH
               ,:B03-DT-SCAD-INTMD-DB
                :IND-B03-DT-SCAD-INTMD
               ,:B03-DT-SCAD-PAG-PRE-DB
                :IND-B03-DT-SCAD-PAG-PRE
               ,:B03-DT-ULT-PRE-PAG-DB
                :IND-B03-DT-ULT-PRE-PAG
               ,:B03-DT-NASC-1O-ASSTO-DB
                :IND-B03-DT-NASC-1O-ASSTO
               ,:B03-SEX-1O-ASSTO
                :IND-B03-SEX-1O-ASSTO
               ,:B03-ETA-AA-1O-ASSTO
                :IND-B03-ETA-AA-1O-ASSTO
               ,:B03-ETA-MM-1O-ASSTO
                :IND-B03-ETA-MM-1O-ASSTO
               ,:B03-ETA-RAGGN-DT-CALC
                :IND-B03-ETA-RAGGN-DT-CALC
               ,:B03-DUR-AA
                :IND-B03-DUR-AA
               ,:B03-DUR-MM
                :IND-B03-DUR-MM
               ,:B03-DUR-GG
                :IND-B03-DUR-GG
               ,:B03-DUR-1O-PER-AA
                :IND-B03-DUR-1O-PER-AA
               ,:B03-DUR-1O-PER-MM
                :IND-B03-DUR-1O-PER-MM
               ,:B03-DUR-1O-PER-GG
                :IND-B03-DUR-1O-PER-GG
               ,:B03-ANTIDUR-RICOR-PREC
                :IND-B03-ANTIDUR-RICOR-PREC
               ,:B03-ANTIDUR-DT-CALC
                :IND-B03-ANTIDUR-DT-CALC
               ,:B03-DUR-RES-DT-CALC
                :IND-B03-DUR-RES-DT-CALC
               ,:B03-TP-STAT-BUS-POLI
               ,:B03-TP-CAUS-POLI
               ,:B03-TP-STAT-BUS-ADES
               ,:B03-TP-CAUS-ADES
               ,:B03-TP-STAT-BUS-TRCH
               ,:B03-TP-CAUS-TRCH
               ,:B03-DT-EFF-CAMB-STAT-DB
                :IND-B03-DT-EFF-CAMB-STAT
               ,:B03-DT-EMIS-CAMB-STAT-DB
                :IND-B03-DT-EMIS-CAMB-STAT
               ,:B03-DT-EFF-STAB-DB
                :IND-B03-DT-EFF-STAB
               ,:B03-CPT-DT-STAB
                :IND-B03-CPT-DT-STAB
               ,:B03-DT-EFF-RIDZ-DB
                :IND-B03-DT-EFF-RIDZ
               ,:B03-DT-EMIS-RIDZ-DB
                :IND-B03-DT-EMIS-RIDZ
               ,:B03-CPT-DT-RIDZ
                :IND-B03-CPT-DT-RIDZ
               ,:B03-FRAZ
                :IND-B03-FRAZ
               ,:B03-DUR-PAG-PRE
                :IND-B03-DUR-PAG-PRE
               ,:B03-NUM-PRE-PATT
                :IND-B03-NUM-PRE-PATT
               ,:B03-FRAZ-INI-EROG-REN
                :IND-B03-FRAZ-INI-EROG-REN
               ,:B03-AA-REN-CER
                :IND-B03-AA-REN-CER
               ,:B03-RAT-REN
                :IND-B03-RAT-REN
               ,:B03-COD-DIV
                :IND-B03-COD-DIV
               ,:B03-RISCPAR
                :IND-B03-RISCPAR
               ,:B03-CUM-RISCPAR
                :IND-B03-CUM-RISCPAR
               ,:B03-ULT-RM
                :IND-B03-ULT-RM
               ,:B03-TS-RENDTO-T
                :IND-B03-TS-RENDTO-T
               ,:B03-ALQ-RETR-T
                :IND-B03-ALQ-RETR-T
               ,:B03-MIN-TRNUT-T
                :IND-B03-MIN-TRNUT-T
               ,:B03-TS-NET-T
                :IND-B03-TS-NET-T
               ,:B03-DT-ULT-RIVAL-DB
                :IND-B03-DT-ULT-RIVAL
               ,:B03-PRSTZ-INI
                :IND-B03-PRSTZ-INI
               ,:B03-PRSTZ-AGG-INI
                :IND-B03-PRSTZ-AGG-INI
               ,:B03-PRSTZ-AGG-ULT
                :IND-B03-PRSTZ-AGG-ULT
               ,:B03-RAPPEL
                :IND-B03-RAPPEL
               ,:B03-PRE-PATTUITO-INI
                :IND-B03-PRE-PATTUITO-INI
               ,:B03-PRE-DOV-INI
                :IND-B03-PRE-DOV-INI
               ,:B03-PRE-DOV-RIVTO-T
                :IND-B03-PRE-DOV-RIVTO-T
               ,:B03-PRE-ANNUALIZ-RICOR
                :IND-B03-PRE-ANNUALIZ-RICOR
               ,:B03-PRE-CONT
                :IND-B03-PRE-CONT
               ,:B03-PRE-PP-INI
                :IND-B03-PRE-PP-INI
               ,:B03-RIS-PURA-T
                :IND-B03-RIS-PURA-T
               ,:B03-PROV-ACQ
                :IND-B03-PROV-ACQ
               ,:B03-PROV-ACQ-RICOR
                :IND-B03-PROV-ACQ-RICOR
               ,:B03-PROV-INC
                :IND-B03-PROV-INC
               ,:B03-CAR-ACQ-NON-SCON
                :IND-B03-CAR-ACQ-NON-SCON
               ,:B03-OVER-COMM
                :IND-B03-OVER-COMM
               ,:B03-CAR-ACQ-PRECONTATO
                :IND-B03-CAR-ACQ-PRECONTATO
               ,:B03-RIS-ACQ-T
                :IND-B03-RIS-ACQ-T
               ,:B03-RIS-ZIL-T
                :IND-B03-RIS-ZIL-T
               ,:B03-CAR-GEST-NON-SCON
                :IND-B03-CAR-GEST-NON-SCON
               ,:B03-CAR-GEST
                :IND-B03-CAR-GEST
               ,:B03-RIS-SPE-T
                :IND-B03-RIS-SPE-T
               ,:B03-CAR-INC-NON-SCON
                :IND-B03-CAR-INC-NON-SCON
               ,:B03-CAR-INC
                :IND-B03-CAR-INC
               ,:B03-RIS-RISTORNI-CAP
                :IND-B03-RIS-RISTORNI-CAP
               ,:B03-INTR-TECN
                :IND-B03-INTR-TECN
               ,:B03-CPT-RSH-MOR
                :IND-B03-CPT-RSH-MOR
               ,:B03-C-SUBRSH-T
                :IND-B03-C-SUBRSH-T
               ,:B03-PRE-RSH-T
                :IND-B03-PRE-RSH-T
               ,:B03-ALQ-MARG-RIS
                :IND-B03-ALQ-MARG-RIS
               ,:B03-ALQ-MARG-C-SUBRSH
                :IND-B03-ALQ-MARG-C-SUBRSH
               ,:B03-TS-RENDTO-SPPR
                :IND-B03-TS-RENDTO-SPPR
               ,:B03-TP-IAS
                :IND-B03-TP-IAS
               ,:B03-NS-QUO
                :IND-B03-NS-QUO
               ,:B03-TS-MEDIO
                :IND-B03-TS-MEDIO
               ,:B03-CPT-RIASTO
                :IND-B03-CPT-RIASTO
               ,:B03-PRE-RIASTO
                :IND-B03-PRE-RIASTO
               ,:B03-RIS-RIASTA
                :IND-B03-RIS-RIASTA
               ,:B03-CPT-RIASTO-ECC
                :IND-B03-CPT-RIASTO-ECC
               ,:B03-PRE-RIASTO-ECC
                :IND-B03-PRE-RIASTO-ECC
               ,:B03-RIS-RIASTA-ECC
                :IND-B03-RIS-RIASTA-ECC
               ,:B03-COD-AGE
                :IND-B03-COD-AGE
               ,:B03-COD-SUBAGE
                :IND-B03-COD-SUBAGE
               ,:B03-COD-CAN
                :IND-B03-COD-CAN
               ,:B03-IB-POLI
                :IND-B03-IB-POLI
               ,:B03-IB-ADES
                :IND-B03-IB-ADES
               ,:B03-IB-TRCH-DI-GAR
                :IND-B03-IB-TRCH-DI-GAR
               ,:B03-TP-PRSTZ
                :IND-B03-TP-PRSTZ
               ,:B03-TP-TRASF
                :IND-B03-TP-TRASF
               ,:B03-PP-INVRIO-TARI
                :IND-B03-PP-INVRIO-TARI
               ,:B03-COEFF-OPZ-REN
                :IND-B03-COEFF-OPZ-REN
               ,:B03-COEFF-OPZ-CPT
                :IND-B03-COEFF-OPZ-CPT
               ,:B03-DUR-PAG-REN
                :IND-B03-DUR-PAG-REN
               ,:B03-VLT
                :IND-B03-VLT
               ,:B03-RIS-MAT-CHIU-PREC
                :IND-B03-RIS-MAT-CHIU-PREC
               ,:B03-COD-FND
                :IND-B03-COD-FND
               ,:B03-PRSTZ-T
                :IND-B03-PRSTZ-T
               ,:B03-TS-TARI-DOV
                :IND-B03-TS-TARI-DOV
               ,:B03-TS-TARI-SCON
                :IND-B03-TS-TARI-SCON
               ,:B03-TS-PP
                :IND-B03-TS-PP
               ,:B03-COEFF-RIS-1-T
                :IND-B03-COEFF-RIS-1-T
               ,:B03-COEFF-RIS-2-T
                :IND-B03-COEFF-RIS-2-T
               ,:B03-ABB
                :IND-B03-ABB
               ,:B03-TP-COASS
                :IND-B03-TP-COASS
               ,:B03-TRAT-RIASS
                :IND-B03-TRAT-RIASS
               ,:B03-TRAT-RIASS-ECC
                :IND-B03-TRAT-RIASS-ECC
               ,:B03-DS-OPER-SQL
               ,:B03-DS-VER
               ,:B03-DS-TS-CPTZ
               ,:B03-DS-UTENTE
               ,:B03-DS-STATO-ELAB
               ,:B03-TP-RGM-FISC
                :IND-B03-TP-RGM-FISC
               ,:B03-DUR-GAR-AA
                :IND-B03-DUR-GAR-AA
               ,:B03-DUR-GAR-MM
                :IND-B03-DUR-GAR-MM
               ,:B03-DUR-GAR-GG
                :IND-B03-DUR-GAR-GG
               ,:B03-ANTIDUR-CALC-365
                :IND-B03-ANTIDUR-CALC-365
               ,:B03-COD-FISC-CNTR
                :IND-B03-COD-FISC-CNTR
               ,:B03-COD-FISC-ASSTO1
                :IND-B03-COD-FISC-ASSTO1
               ,:B03-COD-FISC-ASSTO2
                :IND-B03-COD-FISC-ASSTO2
               ,:B03-COD-FISC-ASSTO3
                :IND-B03-COD-FISC-ASSTO3
               ,:B03-CAUS-SCON-VCHAR
                :IND-B03-CAUS-SCON
               ,:B03-EMIT-TIT-OPZ-VCHAR
                :IND-B03-EMIT-TIT-OPZ
               ,:B03-QTZ-SP-Z-COUP-EMIS
                :IND-B03-QTZ-SP-Z-COUP-EMIS
               ,:B03-QTZ-SP-Z-OPZ-EMIS
                :IND-B03-QTZ-SP-Z-OPZ-EMIS
               ,:B03-QTZ-SP-Z-COUP-DT-C
                :IND-B03-QTZ-SP-Z-COUP-DT-C
               ,:B03-QTZ-SP-Z-OPZ-DT-CA
                :IND-B03-QTZ-SP-Z-OPZ-DT-CA
               ,:B03-QTZ-TOT-EMIS
                :IND-B03-QTZ-TOT-EMIS
               ,:B03-QTZ-TOT-DT-CALC
                :IND-B03-QTZ-TOT-DT-CALC
               ,:B03-QTZ-TOT-DT-ULT-BIL
                :IND-B03-QTZ-TOT-DT-ULT-BIL
               ,:B03-DT-QTZ-EMIS-DB
                :IND-B03-DT-QTZ-EMIS
               ,:B03-PC-CAR-GEST
                :IND-B03-PC-CAR-GEST
               ,:B03-PC-CAR-ACQ
                :IND-B03-PC-CAR-ACQ
               ,:B03-IMP-CAR-CASO-MOR
                :IND-B03-IMP-CAR-CASO-MOR
               ,:B03-PC-CAR-MOR
                :IND-B03-PC-CAR-MOR
               ,:B03-TP-VERS
                :IND-B03-TP-VERS
               ,:B03-FL-SWITCH
                :IND-B03-FL-SWITCH
               ,:B03-FL-IAS
                :IND-B03-FL-IAS
               ,:B03-DIR
                :IND-B03-DIR
               ,:B03-TP-COP-CASO-MOR
                :IND-B03-TP-COP-CASO-MOR
               ,:B03-MET-RISC-SPCL
                :IND-B03-MET-RISC-SPCL
               ,:B03-TP-STAT-INVST
                :IND-B03-TP-STAT-INVST
               ,:B03-COD-PRDT
                :IND-B03-COD-PRDT
               ,:B03-STAT-ASSTO-1
                :IND-B03-STAT-ASSTO-1
               ,:B03-STAT-ASSTO-2
                :IND-B03-STAT-ASSTO-2
               ,:B03-STAT-ASSTO-3
                :IND-B03-STAT-ASSTO-3
               ,:B03-CPT-ASSTO-INI-MOR
                :IND-B03-CPT-ASSTO-INI-MOR
               ,:B03-TS-STAB-PRE
                :IND-B03-TS-STAB-PRE
               ,:B03-DIR-EMIS
                :IND-B03-DIR-EMIS
               ,:B03-DT-INC-ULT-PRE-DB
                :IND-B03-DT-INC-ULT-PRE
               ,:B03-STAT-TBGC-ASSTO-1
                :IND-B03-STAT-TBGC-ASSTO-1
               ,:B03-STAT-TBGC-ASSTO-2
                :IND-B03-STAT-TBGC-ASSTO-2
               ,:B03-STAT-TBGC-ASSTO-3
                :IND-B03-STAT-TBGC-ASSTO-3
               ,:B03-FRAZ-DECR-CPT
                :IND-B03-FRAZ-DECR-CPT
               ,:B03-PRE-PP-ULT
                :IND-B03-PRE-PP-ULT
               ,:B03-ACQ-EXP
                :IND-B03-ACQ-EXP
               ,:B03-REMUN-ASS
                :IND-B03-REMUN-ASS
               ,:B03-COMMIS-INTER
                :IND-B03-COMMIS-INTER
               ,:B03-NUM-FINANZ
                :IND-B03-NUM-FINANZ
               ,:B03-TP-ACC-COMM
                :IND-B03-TP-ACC-COMM
               ,:B03-IB-ACC-COMM
                :IND-B03-IB-ACC-COMM
               ,:B03-RAMO-BILA
               ,:B03-CARZ
                :IND-B03-CARZ
             FROM BILA_TRCH_ESTR
             WHERE     ID_BILA_TRCH_ESTR = :B03-ID-BILA-TRCH-ESTR

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO BILA_TRCH_ESTR
                     (
                        ID_BILA_TRCH_ESTR
                       ,COD_COMP_ANIA
                       ,ID_RICH_ESTRAZ_MAS
                       ,ID_RICH_ESTRAZ_AGG
                       ,FL_SIMULAZIONE
                       ,DT_RIS
                       ,DT_PRODUZIONE
                       ,ID_POLI
                       ,ID_ADES
                       ,ID_GAR
                       ,ID_TRCH_DI_GAR
                       ,TP_FRM_ASSVA
                       ,TP_RAMO_BILA
                       ,TP_CALC_RIS
                       ,COD_RAMO
                       ,COD_TARI
                       ,DT_INI_VAL_TAR
                       ,COD_PROD
                       ,DT_INI_VLDT_PROD
                       ,COD_TARI_ORGN
                       ,MIN_GARTO_T
                       ,TP_TARI
                       ,TP_PRE
                       ,TP_ADEG_PRE
                       ,TP_RIVAL
                       ,FL_DA_TRASF
                       ,FL_CAR_CONT
                       ,FL_PRE_DA_RIS
                       ,FL_PRE_AGG
                       ,TP_TRCH
                       ,TP_TST
                       ,COD_CONV
                       ,DT_DECOR_POLI
                       ,DT_DECOR_ADES
                       ,DT_DECOR_TRCH
                       ,DT_EMIS_POLI
                       ,DT_EMIS_TRCH
                       ,DT_SCAD_TRCH
                       ,DT_SCAD_INTMD
                       ,DT_SCAD_PAG_PRE
                       ,DT_ULT_PRE_PAG
                       ,DT_NASC_1O_ASSTO
                       ,SEX_1O_ASSTO
                       ,ETA_AA_1O_ASSTO
                       ,ETA_MM_1O_ASSTO
                       ,ETA_RAGGN_DT_CALC
                       ,DUR_AA
                       ,DUR_MM
                       ,DUR_GG
                       ,DUR_1O_PER_AA
                       ,DUR_1O_PER_MM
                       ,DUR_1O_PER_GG
                       ,ANTIDUR_RICOR_PREC
                       ,ANTIDUR_DT_CALC
                       ,DUR_RES_DT_CALC
                       ,TP_STAT_BUS_POLI
                       ,TP_CAUS_POLI
                       ,TP_STAT_BUS_ADES
                       ,TP_CAUS_ADES
                       ,TP_STAT_BUS_TRCH
                       ,TP_CAUS_TRCH
                       ,DT_EFF_CAMB_STAT
                       ,DT_EMIS_CAMB_STAT
                       ,DT_EFF_STAB
                       ,CPT_DT_STAB
                       ,DT_EFF_RIDZ
                       ,DT_EMIS_RIDZ
                       ,CPT_DT_RIDZ
                       ,FRAZ
                       ,DUR_PAG_PRE
                       ,NUM_PRE_PATT
                       ,FRAZ_INI_EROG_REN
                       ,AA_REN_CER
                       ,RAT_REN
                       ,COD_DIV
                       ,RISCPAR
                       ,CUM_RISCPAR
                       ,ULT_RM
                       ,TS_RENDTO_T
                       ,ALQ_RETR_T
                       ,MIN_TRNUT_T
                       ,TS_NET_T
                       ,DT_ULT_RIVAL
                       ,PRSTZ_INI
                       ,PRSTZ_AGG_INI
                       ,PRSTZ_AGG_ULT
                       ,RAPPEL
                       ,PRE_PATTUITO_INI
                       ,PRE_DOV_INI
                       ,PRE_DOV_RIVTO_T
                       ,PRE_ANNUALIZ_RICOR
                       ,PRE_CONT
                       ,PRE_PP_INI
                       ,RIS_PURA_T
                       ,PROV_ACQ
                       ,PROV_ACQ_RICOR
                       ,PROV_INC
                       ,CAR_ACQ_NON_SCON
                       ,OVER_COMM
                       ,CAR_ACQ_PRECONTATO
                       ,RIS_ACQ_T
                       ,RIS_ZIL_T
                       ,CAR_GEST_NON_SCON
                       ,CAR_GEST
                       ,RIS_SPE_T
                       ,CAR_INC_NON_SCON
                       ,CAR_INC
                       ,RIS_RISTORNI_CAP
                       ,INTR_TECN
                       ,CPT_RSH_MOR
                       ,C_SUBRSH_T
                       ,PRE_RSH_T
                       ,ALQ_MARG_RIS
                       ,ALQ_MARG_C_SUBRSH
                       ,TS_RENDTO_SPPR
                       ,TP_IAS
                       ,NS_QUO
                       ,TS_MEDIO
                       ,CPT_RIASTO
                       ,PRE_RIASTO
                       ,RIS_RIASTA
                       ,CPT_RIASTO_ECC
                       ,PRE_RIASTO_ECC
                       ,RIS_RIASTA_ECC
                       ,COD_AGE
                       ,COD_SUBAGE
                       ,COD_CAN
                       ,IB_POLI
                       ,IB_ADES
                       ,IB_TRCH_DI_GAR
                       ,TP_PRSTZ
                       ,TP_TRASF
                       ,PP_INVRIO_TARI
                       ,COEFF_OPZ_REN
                       ,COEFF_OPZ_CPT
                       ,DUR_PAG_REN
                       ,VLT
                       ,RIS_MAT_CHIU_PREC
                       ,COD_FND
                       ,PRSTZ_T
                       ,TS_TARI_DOV
                       ,TS_TARI_SCON
                       ,TS_PP
                       ,COEFF_RIS_1_T
                       ,COEFF_RIS_2_T
                       ,ABB
                       ,TP_COASS
                       ,TRAT_RIASS
                       ,TRAT_RIASS_ECC
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,TP_RGM_FISC
                       ,DUR_GAR_AA
                       ,DUR_GAR_MM
                       ,DUR_GAR_GG
                       ,ANTIDUR_CALC_365
                       ,COD_FISC_CNTR
                       ,COD_FISC_ASSTO1
                       ,COD_FISC_ASSTO2
                       ,COD_FISC_ASSTO3
                       ,CAUS_SCON
                       ,EMIT_TIT_OPZ
                       ,QTZ_SP_Z_COUP_EMIS
                       ,QTZ_SP_Z_OPZ_EMIS
                       ,QTZ_SP_Z_COUP_DT_C
                       ,QTZ_SP_Z_OPZ_DT_CA
                       ,QTZ_TOT_EMIS
                       ,QTZ_TOT_DT_CALC
                       ,QTZ_TOT_DT_ULT_BIL
                       ,DT_QTZ_EMIS
                       ,PC_CAR_GEST
                       ,PC_CAR_ACQ
                       ,IMP_CAR_CASO_MOR
                       ,PC_CAR_MOR
                       ,TP_VERS
                       ,FL_SWITCH
                       ,FL_IAS
                       ,DIR
                       ,TP_COP_CASO_MOR
                       ,MET_RISC_SPCL
                       ,TP_STAT_INVST
                       ,COD_PRDT
                       ,STAT_ASSTO_1
                       ,STAT_ASSTO_2
                       ,STAT_ASSTO_3
                       ,CPT_ASSTO_INI_MOR
                       ,TS_STAB_PRE
                       ,DIR_EMIS
                       ,DT_INC_ULT_PRE
                       ,STAT_TBGC_ASSTO_1
                       ,STAT_TBGC_ASSTO_2
                       ,STAT_TBGC_ASSTO_3
                       ,FRAZ_DECR_CPT
                       ,PRE_PP_ULT
                       ,ACQ_EXP
                       ,REMUN_ASS
                       ,COMMIS_INTER
                       ,NUM_FINANZ
                       ,TP_ACC_COMM
                       ,IB_ACC_COMM
                       ,RAMO_BILA
                       ,CARZ
                     )
                 VALUES
                     (
                       :B03-ID-BILA-TRCH-ESTR
                       ,:B03-COD-COMP-ANIA
                       ,:B03-ID-RICH-ESTRAZ-MAS
                       ,:B03-ID-RICH-ESTRAZ-AGG
                        :IND-B03-ID-RICH-ESTRAZ-AGG
                       ,:B03-FL-SIMULAZIONE
                        :IND-B03-FL-SIMULAZIONE
                       ,:B03-DT-RIS-DB
                       ,:B03-DT-PRODUZIONE-DB
                       ,:B03-ID-POLI
                       ,:B03-ID-ADES
                       ,:B03-ID-GAR
                       ,:B03-ID-TRCH-DI-GAR
                       ,:B03-TP-FRM-ASSVA
                       ,:B03-TP-RAMO-BILA
                       ,:B03-TP-CALC-RIS
                       ,:B03-COD-RAMO
                       ,:B03-COD-TARI
                       ,:B03-DT-INI-VAL-TAR-DB
                        :IND-B03-DT-INI-VAL-TAR
                       ,:B03-COD-PROD
                        :IND-B03-COD-PROD
                       ,:B03-DT-INI-VLDT-PROD-DB
                       ,:B03-COD-TARI-ORGN
                        :IND-B03-COD-TARI-ORGN
                       ,:B03-MIN-GARTO-T
                        :IND-B03-MIN-GARTO-T
                       ,:B03-TP-TARI
                        :IND-B03-TP-TARI
                       ,:B03-TP-PRE
                        :IND-B03-TP-PRE
                       ,:B03-TP-ADEG-PRE
                        :IND-B03-TP-ADEG-PRE
                       ,:B03-TP-RIVAL
                        :IND-B03-TP-RIVAL
                       ,:B03-FL-DA-TRASF
                        :IND-B03-FL-DA-TRASF
                       ,:B03-FL-CAR-CONT
                        :IND-B03-FL-CAR-CONT
                       ,:B03-FL-PRE-DA-RIS
                        :IND-B03-FL-PRE-DA-RIS
                       ,:B03-FL-PRE-AGG
                        :IND-B03-FL-PRE-AGG
                       ,:B03-TP-TRCH
                        :IND-B03-TP-TRCH
                       ,:B03-TP-TST
                        :IND-B03-TP-TST
                       ,:B03-COD-CONV
                        :IND-B03-COD-CONV
                       ,:B03-DT-DECOR-POLI-DB
                       ,:B03-DT-DECOR-ADES-DB
                        :IND-B03-DT-DECOR-ADES
                       ,:B03-DT-DECOR-TRCH-DB
                       ,:B03-DT-EMIS-POLI-DB
                       ,:B03-DT-EMIS-TRCH-DB
                        :IND-B03-DT-EMIS-TRCH
                       ,:B03-DT-SCAD-TRCH-DB
                        :IND-B03-DT-SCAD-TRCH
                       ,:B03-DT-SCAD-INTMD-DB
                        :IND-B03-DT-SCAD-INTMD
                       ,:B03-DT-SCAD-PAG-PRE-DB
                        :IND-B03-DT-SCAD-PAG-PRE
                       ,:B03-DT-ULT-PRE-PAG-DB
                        :IND-B03-DT-ULT-PRE-PAG
                       ,:B03-DT-NASC-1O-ASSTO-DB
                        :IND-B03-DT-NASC-1O-ASSTO
                       ,:B03-SEX-1O-ASSTO
                        :IND-B03-SEX-1O-ASSTO
                       ,:B03-ETA-AA-1O-ASSTO
                        :IND-B03-ETA-AA-1O-ASSTO
                       ,:B03-ETA-MM-1O-ASSTO
                        :IND-B03-ETA-MM-1O-ASSTO
                       ,:B03-ETA-RAGGN-DT-CALC
                        :IND-B03-ETA-RAGGN-DT-CALC
                       ,:B03-DUR-AA
                        :IND-B03-DUR-AA
                       ,:B03-DUR-MM
                        :IND-B03-DUR-MM
                       ,:B03-DUR-GG
                        :IND-B03-DUR-GG
                       ,:B03-DUR-1O-PER-AA
                        :IND-B03-DUR-1O-PER-AA
                       ,:B03-DUR-1O-PER-MM
                        :IND-B03-DUR-1O-PER-MM
                       ,:B03-DUR-1O-PER-GG
                        :IND-B03-DUR-1O-PER-GG
                       ,:B03-ANTIDUR-RICOR-PREC
                        :IND-B03-ANTIDUR-RICOR-PREC
                       ,:B03-ANTIDUR-DT-CALC
                        :IND-B03-ANTIDUR-DT-CALC
                       ,:B03-DUR-RES-DT-CALC
                        :IND-B03-DUR-RES-DT-CALC
                       ,:B03-TP-STAT-BUS-POLI
                       ,:B03-TP-CAUS-POLI
                       ,:B03-TP-STAT-BUS-ADES
                       ,:B03-TP-CAUS-ADES
                       ,:B03-TP-STAT-BUS-TRCH
                       ,:B03-TP-CAUS-TRCH
                       ,:B03-DT-EFF-CAMB-STAT-DB
                        :IND-B03-DT-EFF-CAMB-STAT
                       ,:B03-DT-EMIS-CAMB-STAT-DB
                        :IND-B03-DT-EMIS-CAMB-STAT
                       ,:B03-DT-EFF-STAB-DB
                        :IND-B03-DT-EFF-STAB
                       ,:B03-CPT-DT-STAB
                        :IND-B03-CPT-DT-STAB
                       ,:B03-DT-EFF-RIDZ-DB
                        :IND-B03-DT-EFF-RIDZ
                       ,:B03-DT-EMIS-RIDZ-DB
                        :IND-B03-DT-EMIS-RIDZ
                       ,:B03-CPT-DT-RIDZ
                        :IND-B03-CPT-DT-RIDZ
                       ,:B03-FRAZ
                        :IND-B03-FRAZ
                       ,:B03-DUR-PAG-PRE
                        :IND-B03-DUR-PAG-PRE
                       ,:B03-NUM-PRE-PATT
                        :IND-B03-NUM-PRE-PATT
                       ,:B03-FRAZ-INI-EROG-REN
                        :IND-B03-FRAZ-INI-EROG-REN
                       ,:B03-AA-REN-CER
                        :IND-B03-AA-REN-CER
                       ,:B03-RAT-REN
                        :IND-B03-RAT-REN
                       ,:B03-COD-DIV
                        :IND-B03-COD-DIV
                       ,:B03-RISCPAR
                        :IND-B03-RISCPAR
                       ,:B03-CUM-RISCPAR
                        :IND-B03-CUM-RISCPAR
                       ,:B03-ULT-RM
                        :IND-B03-ULT-RM
                       ,:B03-TS-RENDTO-T
                        :IND-B03-TS-RENDTO-T
                       ,:B03-ALQ-RETR-T
                        :IND-B03-ALQ-RETR-T
                       ,:B03-MIN-TRNUT-T
                        :IND-B03-MIN-TRNUT-T
                       ,:B03-TS-NET-T
                        :IND-B03-TS-NET-T
                       ,:B03-DT-ULT-RIVAL-DB
                        :IND-B03-DT-ULT-RIVAL
                       ,:B03-PRSTZ-INI
                        :IND-B03-PRSTZ-INI
                       ,:B03-PRSTZ-AGG-INI
                        :IND-B03-PRSTZ-AGG-INI
                       ,:B03-PRSTZ-AGG-ULT
                        :IND-B03-PRSTZ-AGG-ULT
                       ,:B03-RAPPEL
                        :IND-B03-RAPPEL
                       ,:B03-PRE-PATTUITO-INI
                        :IND-B03-PRE-PATTUITO-INI
                       ,:B03-PRE-DOV-INI
                        :IND-B03-PRE-DOV-INI
                       ,:B03-PRE-DOV-RIVTO-T
                        :IND-B03-PRE-DOV-RIVTO-T
                       ,:B03-PRE-ANNUALIZ-RICOR
                        :IND-B03-PRE-ANNUALIZ-RICOR
                       ,:B03-PRE-CONT
                        :IND-B03-PRE-CONT
                       ,:B03-PRE-PP-INI
                        :IND-B03-PRE-PP-INI
                       ,:B03-RIS-PURA-T
                        :IND-B03-RIS-PURA-T
                       ,:B03-PROV-ACQ
                        :IND-B03-PROV-ACQ
                       ,:B03-PROV-ACQ-RICOR
                        :IND-B03-PROV-ACQ-RICOR
                       ,:B03-PROV-INC
                        :IND-B03-PROV-INC
                       ,:B03-CAR-ACQ-NON-SCON
                        :IND-B03-CAR-ACQ-NON-SCON
                       ,:B03-OVER-COMM
                        :IND-B03-OVER-COMM
                       ,:B03-CAR-ACQ-PRECONTATO
                        :IND-B03-CAR-ACQ-PRECONTATO
                       ,:B03-RIS-ACQ-T
                        :IND-B03-RIS-ACQ-T
                       ,:B03-RIS-ZIL-T
                        :IND-B03-RIS-ZIL-T
                       ,:B03-CAR-GEST-NON-SCON
                        :IND-B03-CAR-GEST-NON-SCON
                       ,:B03-CAR-GEST
                        :IND-B03-CAR-GEST
                       ,:B03-RIS-SPE-T
                        :IND-B03-RIS-SPE-T
                       ,:B03-CAR-INC-NON-SCON
                        :IND-B03-CAR-INC-NON-SCON
                       ,:B03-CAR-INC
                        :IND-B03-CAR-INC
                       ,:B03-RIS-RISTORNI-CAP
                        :IND-B03-RIS-RISTORNI-CAP
                       ,:B03-INTR-TECN
                        :IND-B03-INTR-TECN
                       ,:B03-CPT-RSH-MOR
                        :IND-B03-CPT-RSH-MOR
                       ,:B03-C-SUBRSH-T
                        :IND-B03-C-SUBRSH-T
                       ,:B03-PRE-RSH-T
                        :IND-B03-PRE-RSH-T
                       ,:B03-ALQ-MARG-RIS
                        :IND-B03-ALQ-MARG-RIS
                       ,:B03-ALQ-MARG-C-SUBRSH
                        :IND-B03-ALQ-MARG-C-SUBRSH
                       ,:B03-TS-RENDTO-SPPR
                        :IND-B03-TS-RENDTO-SPPR
                       ,:B03-TP-IAS
                        :IND-B03-TP-IAS
                       ,:B03-NS-QUO
                        :IND-B03-NS-QUO
                       ,:B03-TS-MEDIO
                        :IND-B03-TS-MEDIO
                       ,:B03-CPT-RIASTO
                        :IND-B03-CPT-RIASTO
                       ,:B03-PRE-RIASTO
                        :IND-B03-PRE-RIASTO
                       ,:B03-RIS-RIASTA
                        :IND-B03-RIS-RIASTA
                       ,:B03-CPT-RIASTO-ECC
                        :IND-B03-CPT-RIASTO-ECC
                       ,:B03-PRE-RIASTO-ECC
                        :IND-B03-PRE-RIASTO-ECC
                       ,:B03-RIS-RIASTA-ECC
                        :IND-B03-RIS-RIASTA-ECC
                       ,:B03-COD-AGE
                        :IND-B03-COD-AGE
                       ,:B03-COD-SUBAGE
                        :IND-B03-COD-SUBAGE
                       ,:B03-COD-CAN
                        :IND-B03-COD-CAN
                       ,:B03-IB-POLI
                        :IND-B03-IB-POLI
                       ,:B03-IB-ADES
                        :IND-B03-IB-ADES
                       ,:B03-IB-TRCH-DI-GAR
                        :IND-B03-IB-TRCH-DI-GAR
                       ,:B03-TP-PRSTZ
                        :IND-B03-TP-PRSTZ
                       ,:B03-TP-TRASF
                        :IND-B03-TP-TRASF
                       ,:B03-PP-INVRIO-TARI
                        :IND-B03-PP-INVRIO-TARI
                       ,:B03-COEFF-OPZ-REN
                        :IND-B03-COEFF-OPZ-REN
                       ,:B03-COEFF-OPZ-CPT
                        :IND-B03-COEFF-OPZ-CPT
                       ,:B03-DUR-PAG-REN
                        :IND-B03-DUR-PAG-REN
                       ,:B03-VLT
                        :IND-B03-VLT
                       ,:B03-RIS-MAT-CHIU-PREC
                        :IND-B03-RIS-MAT-CHIU-PREC
                       ,:B03-COD-FND
                        :IND-B03-COD-FND
                       ,:B03-PRSTZ-T
                        :IND-B03-PRSTZ-T
                       ,:B03-TS-TARI-DOV
                        :IND-B03-TS-TARI-DOV
                       ,:B03-TS-TARI-SCON
                        :IND-B03-TS-TARI-SCON
                       ,:B03-TS-PP
                        :IND-B03-TS-PP
                       ,:B03-COEFF-RIS-1-T
                        :IND-B03-COEFF-RIS-1-T
                       ,:B03-COEFF-RIS-2-T
                        :IND-B03-COEFF-RIS-2-T
                       ,:B03-ABB
                        :IND-B03-ABB
                       ,:B03-TP-COASS
                        :IND-B03-TP-COASS
                       ,:B03-TRAT-RIASS
                        :IND-B03-TRAT-RIASS
                       ,:B03-TRAT-RIASS-ECC
                        :IND-B03-TRAT-RIASS-ECC
                       ,:B03-DS-OPER-SQL
                       ,:B03-DS-VER
                       ,:B03-DS-TS-CPTZ
                       ,:B03-DS-UTENTE
                       ,:B03-DS-STATO-ELAB
                       ,:B03-TP-RGM-FISC
                        :IND-B03-TP-RGM-FISC
                       ,:B03-DUR-GAR-AA
                        :IND-B03-DUR-GAR-AA
                       ,:B03-DUR-GAR-MM
                        :IND-B03-DUR-GAR-MM
                       ,:B03-DUR-GAR-GG
                        :IND-B03-DUR-GAR-GG
                       ,:B03-ANTIDUR-CALC-365
                        :IND-B03-ANTIDUR-CALC-365
                       ,:B03-COD-FISC-CNTR
                        :IND-B03-COD-FISC-CNTR
                       ,:B03-COD-FISC-ASSTO1
                        :IND-B03-COD-FISC-ASSTO1
                       ,:B03-COD-FISC-ASSTO2
                        :IND-B03-COD-FISC-ASSTO2
                       ,:B03-COD-FISC-ASSTO3
                        :IND-B03-COD-FISC-ASSTO3
                       ,:B03-CAUS-SCON-VCHAR
                        :IND-B03-CAUS-SCON
                       ,:B03-EMIT-TIT-OPZ-VCHAR
                        :IND-B03-EMIT-TIT-OPZ
                       ,:B03-QTZ-SP-Z-COUP-EMIS
                        :IND-B03-QTZ-SP-Z-COUP-EMIS
                       ,:B03-QTZ-SP-Z-OPZ-EMIS
                        :IND-B03-QTZ-SP-Z-OPZ-EMIS
                       ,:B03-QTZ-SP-Z-COUP-DT-C
                        :IND-B03-QTZ-SP-Z-COUP-DT-C
                       ,:B03-QTZ-SP-Z-OPZ-DT-CA
                        :IND-B03-QTZ-SP-Z-OPZ-DT-CA
                       ,:B03-QTZ-TOT-EMIS
                        :IND-B03-QTZ-TOT-EMIS
                       ,:B03-QTZ-TOT-DT-CALC
                        :IND-B03-QTZ-TOT-DT-CALC
                       ,:B03-QTZ-TOT-DT-ULT-BIL
                        :IND-B03-QTZ-TOT-DT-ULT-BIL
                       ,:B03-DT-QTZ-EMIS-DB
                        :IND-B03-DT-QTZ-EMIS
                       ,:B03-PC-CAR-GEST
                        :IND-B03-PC-CAR-GEST
                       ,:B03-PC-CAR-ACQ
                        :IND-B03-PC-CAR-ACQ
                       ,:B03-IMP-CAR-CASO-MOR
                        :IND-B03-IMP-CAR-CASO-MOR
                       ,:B03-PC-CAR-MOR
                        :IND-B03-PC-CAR-MOR
                       ,:B03-TP-VERS
                        :IND-B03-TP-VERS
                       ,:B03-FL-SWITCH
                        :IND-B03-FL-SWITCH
                       ,:B03-FL-IAS
                        :IND-B03-FL-IAS
                       ,:B03-DIR
                        :IND-B03-DIR
                       ,:B03-TP-COP-CASO-MOR
                        :IND-B03-TP-COP-CASO-MOR
                       ,:B03-MET-RISC-SPCL
                        :IND-B03-MET-RISC-SPCL
                       ,:B03-TP-STAT-INVST
                        :IND-B03-TP-STAT-INVST
                       ,:B03-COD-PRDT
                        :IND-B03-COD-PRDT
                       ,:B03-STAT-ASSTO-1
                        :IND-B03-STAT-ASSTO-1
                       ,:B03-STAT-ASSTO-2
                        :IND-B03-STAT-ASSTO-2
                       ,:B03-STAT-ASSTO-3
                        :IND-B03-STAT-ASSTO-3
                       ,:B03-CPT-ASSTO-INI-MOR
                        :IND-B03-CPT-ASSTO-INI-MOR
                       ,:B03-TS-STAB-PRE
                        :IND-B03-TS-STAB-PRE
                       ,:B03-DIR-EMIS
                        :IND-B03-DIR-EMIS
                       ,:B03-DT-INC-ULT-PRE-DB
                        :IND-B03-DT-INC-ULT-PRE
                       ,:B03-STAT-TBGC-ASSTO-1
                        :IND-B03-STAT-TBGC-ASSTO-1
                       ,:B03-STAT-TBGC-ASSTO-2
                        :IND-B03-STAT-TBGC-ASSTO-2
                       ,:B03-STAT-TBGC-ASSTO-3
                        :IND-B03-STAT-TBGC-ASSTO-3
                       ,:B03-FRAZ-DECR-CPT
                        :IND-B03-FRAZ-DECR-CPT
                       ,:B03-PRE-PP-ULT
                        :IND-B03-PRE-PP-ULT
                       ,:B03-ACQ-EXP
                        :IND-B03-ACQ-EXP
                       ,:B03-REMUN-ASS
                        :IND-B03-REMUN-ASS
                       ,:B03-COMMIS-INTER
                        :IND-B03-COMMIS-INTER
                       ,:B03-NUM-FINANZ
                        :IND-B03-NUM-FINANZ
                       ,:B03-TP-ACC-COMM
                        :IND-B03-TP-ACC-COMM
                       ,:B03-IB-ACC-COMM
                        :IND-B03-IB-ACC-COMM
                       ,:B03-RAMO-BILA
                       ,:B03-CARZ
                        :IND-B03-CARZ
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE BILA_TRCH_ESTR SET

                   ID_BILA_TRCH_ESTR      =
                :B03-ID-BILA-TRCH-ESTR
                  ,COD_COMP_ANIA          =
                :B03-COD-COMP-ANIA
                  ,ID_RICH_ESTRAZ_MAS     =
                :B03-ID-RICH-ESTRAZ-MAS
                  ,ID_RICH_ESTRAZ_AGG     =
                :B03-ID-RICH-ESTRAZ-AGG
                                       :IND-B03-ID-RICH-ESTRAZ-AGG
                  ,FL_SIMULAZIONE         =
                :B03-FL-SIMULAZIONE
                                       :IND-B03-FL-SIMULAZIONE
                  ,DT_RIS                 =
           :B03-DT-RIS-DB
                  ,DT_PRODUZIONE          =
           :B03-DT-PRODUZIONE-DB
                  ,ID_POLI                =
                :B03-ID-POLI
                  ,ID_ADES                =
                :B03-ID-ADES
                  ,ID_GAR                 =
                :B03-ID-GAR
                  ,ID_TRCH_DI_GAR         =
                :B03-ID-TRCH-DI-GAR
                  ,TP_FRM_ASSVA           =
                :B03-TP-FRM-ASSVA
                  ,TP_RAMO_BILA           =
                :B03-TP-RAMO-BILA
                  ,TP_CALC_RIS            =
                :B03-TP-CALC-RIS
                  ,COD_RAMO               =
                :B03-COD-RAMO
                  ,COD_TARI               =
                :B03-COD-TARI
                  ,DT_INI_VAL_TAR         =
           :B03-DT-INI-VAL-TAR-DB
                                       :IND-B03-DT-INI-VAL-TAR
                  ,COD_PROD               =
                :B03-COD-PROD
                                       :IND-B03-COD-PROD
                  ,DT_INI_VLDT_PROD       =
           :B03-DT-INI-VLDT-PROD-DB
                  ,COD_TARI_ORGN          =
                :B03-COD-TARI-ORGN
                                       :IND-B03-COD-TARI-ORGN
                  ,MIN_GARTO_T            =
                :B03-MIN-GARTO-T
                                       :IND-B03-MIN-GARTO-T
                  ,TP_TARI                =
                :B03-TP-TARI
                                       :IND-B03-TP-TARI
                  ,TP_PRE                 =
                :B03-TP-PRE
                                       :IND-B03-TP-PRE
                  ,TP_ADEG_PRE            =
                :B03-TP-ADEG-PRE
                                       :IND-B03-TP-ADEG-PRE
                  ,TP_RIVAL               =
                :B03-TP-RIVAL
                                       :IND-B03-TP-RIVAL
                  ,FL_DA_TRASF            =
                :B03-FL-DA-TRASF
                                       :IND-B03-FL-DA-TRASF
                  ,FL_CAR_CONT            =
                :B03-FL-CAR-CONT
                                       :IND-B03-FL-CAR-CONT
                  ,FL_PRE_DA_RIS          =
                :B03-FL-PRE-DA-RIS
                                       :IND-B03-FL-PRE-DA-RIS
                  ,FL_PRE_AGG             =
                :B03-FL-PRE-AGG
                                       :IND-B03-FL-PRE-AGG
                  ,TP_TRCH                =
                :B03-TP-TRCH
                                       :IND-B03-TP-TRCH
                  ,TP_TST                 =
                :B03-TP-TST
                                       :IND-B03-TP-TST
                  ,COD_CONV               =
                :B03-COD-CONV
                                       :IND-B03-COD-CONV
                  ,DT_DECOR_POLI          =
           :B03-DT-DECOR-POLI-DB
                  ,DT_DECOR_ADES          =
           :B03-DT-DECOR-ADES-DB
                                       :IND-B03-DT-DECOR-ADES
                  ,DT_DECOR_TRCH          =
           :B03-DT-DECOR-TRCH-DB
                  ,DT_EMIS_POLI           =
           :B03-DT-EMIS-POLI-DB
                  ,DT_EMIS_TRCH           =
           :B03-DT-EMIS-TRCH-DB
                                       :IND-B03-DT-EMIS-TRCH
                  ,DT_SCAD_TRCH           =
           :B03-DT-SCAD-TRCH-DB
                                       :IND-B03-DT-SCAD-TRCH
                  ,DT_SCAD_INTMD          =
           :B03-DT-SCAD-INTMD-DB
                                       :IND-B03-DT-SCAD-INTMD
                  ,DT_SCAD_PAG_PRE        =
           :B03-DT-SCAD-PAG-PRE-DB
                                       :IND-B03-DT-SCAD-PAG-PRE
                  ,DT_ULT_PRE_PAG         =
           :B03-DT-ULT-PRE-PAG-DB
                                       :IND-B03-DT-ULT-PRE-PAG
                  ,DT_NASC_1O_ASSTO       =
           :B03-DT-NASC-1O-ASSTO-DB
                                       :IND-B03-DT-NASC-1O-ASSTO
                  ,SEX_1O_ASSTO           =
                :B03-SEX-1O-ASSTO
                                       :IND-B03-SEX-1O-ASSTO
                  ,ETA_AA_1O_ASSTO        =
                :B03-ETA-AA-1O-ASSTO
                                       :IND-B03-ETA-AA-1O-ASSTO
                  ,ETA_MM_1O_ASSTO        =
                :B03-ETA-MM-1O-ASSTO
                                       :IND-B03-ETA-MM-1O-ASSTO
                  ,ETA_RAGGN_DT_CALC      =
                :B03-ETA-RAGGN-DT-CALC
                                       :IND-B03-ETA-RAGGN-DT-CALC
                  ,DUR_AA                 =
                :B03-DUR-AA
                                       :IND-B03-DUR-AA
                  ,DUR_MM                 =
                :B03-DUR-MM
                                       :IND-B03-DUR-MM
                  ,DUR_GG                 =
                :B03-DUR-GG
                                       :IND-B03-DUR-GG
                  ,DUR_1O_PER_AA          =
                :B03-DUR-1O-PER-AA
                                       :IND-B03-DUR-1O-PER-AA
                  ,DUR_1O_PER_MM          =
                :B03-DUR-1O-PER-MM
                                       :IND-B03-DUR-1O-PER-MM
                  ,DUR_1O_PER_GG          =
                :B03-DUR-1O-PER-GG
                                       :IND-B03-DUR-1O-PER-GG
                  ,ANTIDUR_RICOR_PREC     =
                :B03-ANTIDUR-RICOR-PREC
                                       :IND-B03-ANTIDUR-RICOR-PREC
                  ,ANTIDUR_DT_CALC        =
                :B03-ANTIDUR-DT-CALC
                                       :IND-B03-ANTIDUR-DT-CALC
                  ,DUR_RES_DT_CALC        =
                :B03-DUR-RES-DT-CALC
                                       :IND-B03-DUR-RES-DT-CALC
                  ,TP_STAT_BUS_POLI       =
                :B03-TP-STAT-BUS-POLI
                  ,TP_CAUS_POLI           =
                :B03-TP-CAUS-POLI
                  ,TP_STAT_BUS_ADES       =
                :B03-TP-STAT-BUS-ADES
                  ,TP_CAUS_ADES           =
                :B03-TP-CAUS-ADES
                  ,TP_STAT_BUS_TRCH       =
                :B03-TP-STAT-BUS-TRCH
                  ,TP_CAUS_TRCH           =
                :B03-TP-CAUS-TRCH
                  ,DT_EFF_CAMB_STAT       =
           :B03-DT-EFF-CAMB-STAT-DB
                                       :IND-B03-DT-EFF-CAMB-STAT
                  ,DT_EMIS_CAMB_STAT      =
           :B03-DT-EMIS-CAMB-STAT-DB
                                       :IND-B03-DT-EMIS-CAMB-STAT
                  ,DT_EFF_STAB            =
           :B03-DT-EFF-STAB-DB
                                       :IND-B03-DT-EFF-STAB
                  ,CPT_DT_STAB            =
                :B03-CPT-DT-STAB
                                       :IND-B03-CPT-DT-STAB
                  ,DT_EFF_RIDZ            =
           :B03-DT-EFF-RIDZ-DB
                                       :IND-B03-DT-EFF-RIDZ
                  ,DT_EMIS_RIDZ           =
           :B03-DT-EMIS-RIDZ-DB
                                       :IND-B03-DT-EMIS-RIDZ
                  ,CPT_DT_RIDZ            =
                :B03-CPT-DT-RIDZ
                                       :IND-B03-CPT-DT-RIDZ
                  ,FRAZ                   =
                :B03-FRAZ
                                       :IND-B03-FRAZ
                  ,DUR_PAG_PRE            =
                :B03-DUR-PAG-PRE
                                       :IND-B03-DUR-PAG-PRE
                  ,NUM_PRE_PATT           =
                :B03-NUM-PRE-PATT
                                       :IND-B03-NUM-PRE-PATT
                  ,FRAZ_INI_EROG_REN      =
                :B03-FRAZ-INI-EROG-REN
                                       :IND-B03-FRAZ-INI-EROG-REN
                  ,AA_REN_CER             =
                :B03-AA-REN-CER
                                       :IND-B03-AA-REN-CER
                  ,RAT_REN                =
                :B03-RAT-REN
                                       :IND-B03-RAT-REN
                  ,COD_DIV                =
                :B03-COD-DIV
                                       :IND-B03-COD-DIV
                  ,RISCPAR                =
                :B03-RISCPAR
                                       :IND-B03-RISCPAR
                  ,CUM_RISCPAR            =
                :B03-CUM-RISCPAR
                                       :IND-B03-CUM-RISCPAR
                  ,ULT_RM                 =
                :B03-ULT-RM
                                       :IND-B03-ULT-RM
                  ,TS_RENDTO_T            =
                :B03-TS-RENDTO-T
                                       :IND-B03-TS-RENDTO-T
                  ,ALQ_RETR_T             =
                :B03-ALQ-RETR-T
                                       :IND-B03-ALQ-RETR-T
                  ,MIN_TRNUT_T            =
                :B03-MIN-TRNUT-T
                                       :IND-B03-MIN-TRNUT-T
                  ,TS_NET_T               =
                :B03-TS-NET-T
                                       :IND-B03-TS-NET-T
                  ,DT_ULT_RIVAL           =
           :B03-DT-ULT-RIVAL-DB
                                       :IND-B03-DT-ULT-RIVAL
                  ,PRSTZ_INI              =
                :B03-PRSTZ-INI
                                       :IND-B03-PRSTZ-INI
                  ,PRSTZ_AGG_INI          =
                :B03-PRSTZ-AGG-INI
                                       :IND-B03-PRSTZ-AGG-INI
                  ,PRSTZ_AGG_ULT          =
                :B03-PRSTZ-AGG-ULT
                                       :IND-B03-PRSTZ-AGG-ULT
                  ,RAPPEL                 =
                :B03-RAPPEL
                                       :IND-B03-RAPPEL
                  ,PRE_PATTUITO_INI       =
                :B03-PRE-PATTUITO-INI
                                       :IND-B03-PRE-PATTUITO-INI
                  ,PRE_DOV_INI            =
                :B03-PRE-DOV-INI
                                       :IND-B03-PRE-DOV-INI
                  ,PRE_DOV_RIVTO_T        =
                :B03-PRE-DOV-RIVTO-T
                                       :IND-B03-PRE-DOV-RIVTO-T
                  ,PRE_ANNUALIZ_RICOR     =
                :B03-PRE-ANNUALIZ-RICOR
                                       :IND-B03-PRE-ANNUALIZ-RICOR
                  ,PRE_CONT               =
                :B03-PRE-CONT
                                       :IND-B03-PRE-CONT
                  ,PRE_PP_INI             =
                :B03-PRE-PP-INI
                                       :IND-B03-PRE-PP-INI
                  ,RIS_PURA_T             =
                :B03-RIS-PURA-T
                                       :IND-B03-RIS-PURA-T
                  ,PROV_ACQ               =
                :B03-PROV-ACQ
                                       :IND-B03-PROV-ACQ
                  ,PROV_ACQ_RICOR         =
                :B03-PROV-ACQ-RICOR
                                       :IND-B03-PROV-ACQ-RICOR
                  ,PROV_INC               =
                :B03-PROV-INC
                                       :IND-B03-PROV-INC
                  ,CAR_ACQ_NON_SCON       =
                :B03-CAR-ACQ-NON-SCON
                                       :IND-B03-CAR-ACQ-NON-SCON
                  ,OVER_COMM              =
                :B03-OVER-COMM
                                       :IND-B03-OVER-COMM
                  ,CAR_ACQ_PRECONTATO     =
                :B03-CAR-ACQ-PRECONTATO
                                       :IND-B03-CAR-ACQ-PRECONTATO
                  ,RIS_ACQ_T              =
                :B03-RIS-ACQ-T
                                       :IND-B03-RIS-ACQ-T
                  ,RIS_ZIL_T              =
                :B03-RIS-ZIL-T
                                       :IND-B03-RIS-ZIL-T
                  ,CAR_GEST_NON_SCON      =
                :B03-CAR-GEST-NON-SCON
                                       :IND-B03-CAR-GEST-NON-SCON
                  ,CAR_GEST               =
                :B03-CAR-GEST
                                       :IND-B03-CAR-GEST
                  ,RIS_SPE_T              =
                :B03-RIS-SPE-T
                                       :IND-B03-RIS-SPE-T
                  ,CAR_INC_NON_SCON       =
                :B03-CAR-INC-NON-SCON
                                       :IND-B03-CAR-INC-NON-SCON
                  ,CAR_INC                =
                :B03-CAR-INC
                                       :IND-B03-CAR-INC
                  ,RIS_RISTORNI_CAP       =
                :B03-RIS-RISTORNI-CAP
                                       :IND-B03-RIS-RISTORNI-CAP
                  ,INTR_TECN              =
                :B03-INTR-TECN
                                       :IND-B03-INTR-TECN
                  ,CPT_RSH_MOR            =
                :B03-CPT-RSH-MOR
                                       :IND-B03-CPT-RSH-MOR
                  ,C_SUBRSH_T             =
                :B03-C-SUBRSH-T
                                       :IND-B03-C-SUBRSH-T
                  ,PRE_RSH_T              =
                :B03-PRE-RSH-T
                                       :IND-B03-PRE-RSH-T
                  ,ALQ_MARG_RIS           =
                :B03-ALQ-MARG-RIS
                                       :IND-B03-ALQ-MARG-RIS
                  ,ALQ_MARG_C_SUBRSH      =
                :B03-ALQ-MARG-C-SUBRSH
                                       :IND-B03-ALQ-MARG-C-SUBRSH
                  ,TS_RENDTO_SPPR         =
                :B03-TS-RENDTO-SPPR
                                       :IND-B03-TS-RENDTO-SPPR
                  ,TP_IAS                 =
                :B03-TP-IAS
                                       :IND-B03-TP-IAS
                  ,NS_QUO                 =
                :B03-NS-QUO
                                       :IND-B03-NS-QUO
                  ,TS_MEDIO               =
                :B03-TS-MEDIO
                                       :IND-B03-TS-MEDIO
                  ,CPT_RIASTO             =
                :B03-CPT-RIASTO
                                       :IND-B03-CPT-RIASTO
                  ,PRE_RIASTO             =
                :B03-PRE-RIASTO
                                       :IND-B03-PRE-RIASTO
                  ,RIS_RIASTA             =
                :B03-RIS-RIASTA
                                       :IND-B03-RIS-RIASTA
                  ,CPT_RIASTO_ECC         =
                :B03-CPT-RIASTO-ECC
                                       :IND-B03-CPT-RIASTO-ECC
                  ,PRE_RIASTO_ECC         =
                :B03-PRE-RIASTO-ECC
                                       :IND-B03-PRE-RIASTO-ECC
                  ,RIS_RIASTA_ECC         =
                :B03-RIS-RIASTA-ECC
                                       :IND-B03-RIS-RIASTA-ECC
                  ,COD_AGE                =
                :B03-COD-AGE
                                       :IND-B03-COD-AGE
                  ,COD_SUBAGE             =
                :B03-COD-SUBAGE
                                       :IND-B03-COD-SUBAGE
                  ,COD_CAN                =
                :B03-COD-CAN
                                       :IND-B03-COD-CAN
                  ,IB_POLI                =
                :B03-IB-POLI
                                       :IND-B03-IB-POLI
                  ,IB_ADES                =
                :B03-IB-ADES
                                       :IND-B03-IB-ADES
                  ,IB_TRCH_DI_GAR         =
                :B03-IB-TRCH-DI-GAR
                                       :IND-B03-IB-TRCH-DI-GAR
                  ,TP_PRSTZ               =
                :B03-TP-PRSTZ
                                       :IND-B03-TP-PRSTZ
                  ,TP_TRASF               =
                :B03-TP-TRASF
                                       :IND-B03-TP-TRASF
                  ,PP_INVRIO_TARI         =
                :B03-PP-INVRIO-TARI
                                       :IND-B03-PP-INVRIO-TARI
                  ,COEFF_OPZ_REN          =
                :B03-COEFF-OPZ-REN
                                       :IND-B03-COEFF-OPZ-REN
                  ,COEFF_OPZ_CPT          =
                :B03-COEFF-OPZ-CPT
                                       :IND-B03-COEFF-OPZ-CPT
                  ,DUR_PAG_REN            =
                :B03-DUR-PAG-REN
                                       :IND-B03-DUR-PAG-REN
                  ,VLT                    =
                :B03-VLT
                                       :IND-B03-VLT
                  ,RIS_MAT_CHIU_PREC      =
                :B03-RIS-MAT-CHIU-PREC
                                       :IND-B03-RIS-MAT-CHIU-PREC
                  ,COD_FND                =
                :B03-COD-FND
                                       :IND-B03-COD-FND
                  ,PRSTZ_T                =
                :B03-PRSTZ-T
                                       :IND-B03-PRSTZ-T
                  ,TS_TARI_DOV            =
                :B03-TS-TARI-DOV
                                       :IND-B03-TS-TARI-DOV
                  ,TS_TARI_SCON           =
                :B03-TS-TARI-SCON
                                       :IND-B03-TS-TARI-SCON
                  ,TS_PP                  =
                :B03-TS-PP
                                       :IND-B03-TS-PP
                  ,COEFF_RIS_1_T          =
                :B03-COEFF-RIS-1-T
                                       :IND-B03-COEFF-RIS-1-T
                  ,COEFF_RIS_2_T          =
                :B03-COEFF-RIS-2-T
                                       :IND-B03-COEFF-RIS-2-T
                  ,ABB                    =
                :B03-ABB
                                       :IND-B03-ABB
                  ,TP_COASS               =
                :B03-TP-COASS
                                       :IND-B03-TP-COASS
                  ,TRAT_RIASS             =
                :B03-TRAT-RIASS
                                       :IND-B03-TRAT-RIASS
                  ,TRAT_RIASS_ECC         =
                :B03-TRAT-RIASS-ECC
                                       :IND-B03-TRAT-RIASS-ECC
                  ,DS_OPER_SQL            =
                :B03-DS-OPER-SQL
                  ,DS_VER                 =
                :B03-DS-VER
                  ,DS_TS_CPTZ             =
                :B03-DS-TS-CPTZ
                  ,DS_UTENTE              =
                :B03-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :B03-DS-STATO-ELAB
                  ,TP_RGM_FISC            =
                :B03-TP-RGM-FISC
                                       :IND-B03-TP-RGM-FISC
                  ,DUR_GAR_AA             =
                :B03-DUR-GAR-AA
                                       :IND-B03-DUR-GAR-AA
                  ,DUR_GAR_MM             =
                :B03-DUR-GAR-MM
                                       :IND-B03-DUR-GAR-MM
                  ,DUR_GAR_GG             =
                :B03-DUR-GAR-GG
                                       :IND-B03-DUR-GAR-GG
                  ,ANTIDUR_CALC_365       =
                :B03-ANTIDUR-CALC-365
                                       :IND-B03-ANTIDUR-CALC-365
                  ,COD_FISC_CNTR          =
                :B03-COD-FISC-CNTR
                                       :IND-B03-COD-FISC-CNTR
                  ,COD_FISC_ASSTO1        =
                :B03-COD-FISC-ASSTO1
                                       :IND-B03-COD-FISC-ASSTO1
                  ,COD_FISC_ASSTO2        =
                :B03-COD-FISC-ASSTO2
                                       :IND-B03-COD-FISC-ASSTO2
                  ,COD_FISC_ASSTO3        =
                :B03-COD-FISC-ASSTO3
                                       :IND-B03-COD-FISC-ASSTO3
                  ,CAUS_SCON              =
                :B03-CAUS-SCON-VCHAR
                                       :IND-B03-CAUS-SCON
                  ,EMIT_TIT_OPZ           =
                :B03-EMIT-TIT-OPZ-VCHAR
                                       :IND-B03-EMIT-TIT-OPZ
                  ,QTZ_SP_Z_COUP_EMIS     =
                :B03-QTZ-SP-Z-COUP-EMIS
                                       :IND-B03-QTZ-SP-Z-COUP-EMIS
                  ,QTZ_SP_Z_OPZ_EMIS      =
                :B03-QTZ-SP-Z-OPZ-EMIS
                                       :IND-B03-QTZ-SP-Z-OPZ-EMIS
                  ,QTZ_SP_Z_COUP_DT_C     =
                :B03-QTZ-SP-Z-COUP-DT-C
                                       :IND-B03-QTZ-SP-Z-COUP-DT-C
                  ,QTZ_SP_Z_OPZ_DT_CA     =
                :B03-QTZ-SP-Z-OPZ-DT-CA
                                       :IND-B03-QTZ-SP-Z-OPZ-DT-CA
                  ,QTZ_TOT_EMIS           =
                :B03-QTZ-TOT-EMIS
                                       :IND-B03-QTZ-TOT-EMIS
                  ,QTZ_TOT_DT_CALC        =
                :B03-QTZ-TOT-DT-CALC
                                       :IND-B03-QTZ-TOT-DT-CALC
                  ,QTZ_TOT_DT_ULT_BIL     =
                :B03-QTZ-TOT-DT-ULT-BIL
                                       :IND-B03-QTZ-TOT-DT-ULT-BIL
                  ,DT_QTZ_EMIS            =
           :B03-DT-QTZ-EMIS-DB
                                       :IND-B03-DT-QTZ-EMIS
                  ,PC_CAR_GEST            =
                :B03-PC-CAR-GEST
                                       :IND-B03-PC-CAR-GEST
                  ,PC_CAR_ACQ             =
                :B03-PC-CAR-ACQ
                                       :IND-B03-PC-CAR-ACQ
                  ,IMP_CAR_CASO_MOR       =
                :B03-IMP-CAR-CASO-MOR
                                       :IND-B03-IMP-CAR-CASO-MOR
                  ,PC_CAR_MOR             =
                :B03-PC-CAR-MOR
                                       :IND-B03-PC-CAR-MOR
                  ,TP_VERS                =
                :B03-TP-VERS
                                       :IND-B03-TP-VERS
                  ,FL_SWITCH              =
                :B03-FL-SWITCH
                                       :IND-B03-FL-SWITCH
                  ,FL_IAS                 =
                :B03-FL-IAS
                                       :IND-B03-FL-IAS
                  ,DIR                    =
                :B03-DIR
                                       :IND-B03-DIR
                  ,TP_COP_CASO_MOR        =
                :B03-TP-COP-CASO-MOR
                                       :IND-B03-TP-COP-CASO-MOR
                  ,MET_RISC_SPCL          =
                :B03-MET-RISC-SPCL
                                       :IND-B03-MET-RISC-SPCL
                  ,TP_STAT_INVST          =
                :B03-TP-STAT-INVST
                                       :IND-B03-TP-STAT-INVST
                  ,COD_PRDT               =
                :B03-COD-PRDT
                                       :IND-B03-COD-PRDT
                  ,STAT_ASSTO_1           =
                :B03-STAT-ASSTO-1
                                       :IND-B03-STAT-ASSTO-1
                  ,STAT_ASSTO_2           =
                :B03-STAT-ASSTO-2
                                       :IND-B03-STAT-ASSTO-2
                  ,STAT_ASSTO_3           =
                :B03-STAT-ASSTO-3
                                       :IND-B03-STAT-ASSTO-3
                  ,CPT_ASSTO_INI_MOR      =
                :B03-CPT-ASSTO-INI-MOR
                                       :IND-B03-CPT-ASSTO-INI-MOR
                  ,TS_STAB_PRE            =
                :B03-TS-STAB-PRE
                                       :IND-B03-TS-STAB-PRE
                  ,DIR_EMIS               =
                :B03-DIR-EMIS
                                       :IND-B03-DIR-EMIS
                  ,DT_INC_ULT_PRE         =
           :B03-DT-INC-ULT-PRE-DB
                                       :IND-B03-DT-INC-ULT-PRE
                  ,STAT_TBGC_ASSTO_1      =
                :B03-STAT-TBGC-ASSTO-1
                                       :IND-B03-STAT-TBGC-ASSTO-1
                  ,STAT_TBGC_ASSTO_2      =
                :B03-STAT-TBGC-ASSTO-2
                                       :IND-B03-STAT-TBGC-ASSTO-2
                  ,STAT_TBGC_ASSTO_3      =
                :B03-STAT-TBGC-ASSTO-3
                                       :IND-B03-STAT-TBGC-ASSTO-3
                  ,FRAZ_DECR_CPT          =
                :B03-FRAZ-DECR-CPT
                                       :IND-B03-FRAZ-DECR-CPT
                  ,PRE_PP_ULT             =
                :B03-PRE-PP-ULT
                                       :IND-B03-PRE-PP-ULT
                  ,ACQ_EXP                =
                :B03-ACQ-EXP
                                       :IND-B03-ACQ-EXP
                  ,REMUN_ASS              =
                :B03-REMUN-ASS
                                       :IND-B03-REMUN-ASS
                  ,COMMIS_INTER           =
                :B03-COMMIS-INTER
                                       :IND-B03-COMMIS-INTER
                  ,NUM_FINANZ             =
                :B03-NUM-FINANZ
                                       :IND-B03-NUM-FINANZ
                  ,TP_ACC_COMM            =
                :B03-TP-ACC-COMM
                                       :IND-B03-TP-ACC-COMM
                  ,IB_ACC_COMM            =
                :B03-IB-ACC-COMM
                                       :IND-B03-IB-ACC-COMM
                  ,RAMO_BILA              =
                :B03-RAMO-BILA
                  ,CARZ                   =
                :B03-CARZ
                                       :IND-B03-CARZ
                WHERE     ID_BILA_TRCH_ESTR = :B03-ID-BILA-TRCH-ESTR

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM BILA_TRCH_ESTR
                WHERE     ID_BILA_TRCH_ESTR = :B03-ID-BILA-TRCH-ESTR

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-B03-ID-RICH-ESTRAZ-AGG = -1
              MOVE HIGH-VALUES TO B03-ID-RICH-ESTRAZ-AGG-NULL
           END-IF
           IF IND-B03-FL-SIMULAZIONE = -1
              MOVE HIGH-VALUES TO B03-FL-SIMULAZIONE-NULL
           END-IF
           IF IND-B03-DT-INI-VAL-TAR = -1
              MOVE HIGH-VALUES TO B03-DT-INI-VAL-TAR-NULL
           END-IF
           IF IND-B03-COD-PROD = -1
              MOVE HIGH-VALUES TO B03-COD-PROD-NULL
           END-IF
           IF IND-B03-COD-TARI-ORGN = -1
              MOVE HIGH-VALUES TO B03-COD-TARI-ORGN-NULL
           END-IF
           IF IND-B03-MIN-GARTO-T = -1
              MOVE HIGH-VALUES TO B03-MIN-GARTO-T-NULL
           END-IF
           IF IND-B03-TP-TARI = -1
              MOVE HIGH-VALUES TO B03-TP-TARI-NULL
           END-IF
           IF IND-B03-TP-PRE = -1
              MOVE HIGH-VALUES TO B03-TP-PRE-NULL
           END-IF
           IF IND-B03-TP-ADEG-PRE = -1
              MOVE HIGH-VALUES TO B03-TP-ADEG-PRE-NULL
           END-IF
           IF IND-B03-TP-RIVAL = -1
              MOVE HIGH-VALUES TO B03-TP-RIVAL-NULL
           END-IF
           IF IND-B03-FL-DA-TRASF = -1
              MOVE HIGH-VALUES TO B03-FL-DA-TRASF-NULL
           END-IF
           IF IND-B03-FL-CAR-CONT = -1
              MOVE HIGH-VALUES TO B03-FL-CAR-CONT-NULL
           END-IF
           IF IND-B03-FL-PRE-DA-RIS = -1
              MOVE HIGH-VALUES TO B03-FL-PRE-DA-RIS-NULL
           END-IF
           IF IND-B03-FL-PRE-AGG = -1
              MOVE HIGH-VALUES TO B03-FL-PRE-AGG-NULL
           END-IF
           IF IND-B03-TP-TRCH = -1
              MOVE HIGH-VALUES TO B03-TP-TRCH-NULL
           END-IF
           IF IND-B03-TP-TST = -1
              MOVE HIGH-VALUES TO B03-TP-TST-NULL
           END-IF
           IF IND-B03-COD-CONV = -1
              MOVE HIGH-VALUES TO B03-COD-CONV-NULL
           END-IF
           IF IND-B03-DT-DECOR-ADES = -1
              MOVE HIGH-VALUES TO B03-DT-DECOR-ADES-NULL
           END-IF
           IF IND-B03-DT-EMIS-TRCH = -1
              MOVE HIGH-VALUES TO B03-DT-EMIS-TRCH-NULL
           END-IF
           IF IND-B03-DT-SCAD-TRCH = -1
              MOVE HIGH-VALUES TO B03-DT-SCAD-TRCH-NULL
           END-IF
           IF IND-B03-DT-SCAD-INTMD = -1
              MOVE HIGH-VALUES TO B03-DT-SCAD-INTMD-NULL
           END-IF
           IF IND-B03-DT-SCAD-PAG-PRE = -1
              MOVE HIGH-VALUES TO B03-DT-SCAD-PAG-PRE-NULL
           END-IF
           IF IND-B03-DT-ULT-PRE-PAG = -1
              MOVE HIGH-VALUES TO B03-DT-ULT-PRE-PAG-NULL
           END-IF
           IF IND-B03-DT-NASC-1O-ASSTO = -1
              MOVE HIGH-VALUES TO B03-DT-NASC-1O-ASSTO-NULL
           END-IF
           IF IND-B03-SEX-1O-ASSTO = -1
              MOVE HIGH-VALUES TO B03-SEX-1O-ASSTO-NULL
           END-IF
           IF IND-B03-ETA-AA-1O-ASSTO = -1
              MOVE HIGH-VALUES TO B03-ETA-AA-1O-ASSTO-NULL
           END-IF
           IF IND-B03-ETA-MM-1O-ASSTO = -1
              MOVE HIGH-VALUES TO B03-ETA-MM-1O-ASSTO-NULL
           END-IF
           IF IND-B03-ETA-RAGGN-DT-CALC = -1
              MOVE HIGH-VALUES TO B03-ETA-RAGGN-DT-CALC-NULL
           END-IF
           IF IND-B03-DUR-AA = -1
              MOVE HIGH-VALUES TO B03-DUR-AA-NULL
           END-IF
           IF IND-B03-DUR-MM = -1
              MOVE HIGH-VALUES TO B03-DUR-MM-NULL
           END-IF
           IF IND-B03-DUR-GG = -1
              MOVE HIGH-VALUES TO B03-DUR-GG-NULL
           END-IF
           IF IND-B03-DUR-1O-PER-AA = -1
              MOVE HIGH-VALUES TO B03-DUR-1O-PER-AA-NULL
           END-IF
           IF IND-B03-DUR-1O-PER-MM = -1
              MOVE HIGH-VALUES TO B03-DUR-1O-PER-MM-NULL
           END-IF
           IF IND-B03-DUR-1O-PER-GG = -1
              MOVE HIGH-VALUES TO B03-DUR-1O-PER-GG-NULL
           END-IF
           IF IND-B03-ANTIDUR-RICOR-PREC = -1
              MOVE HIGH-VALUES TO B03-ANTIDUR-RICOR-PREC-NULL
           END-IF
           IF IND-B03-ANTIDUR-DT-CALC = -1
              MOVE HIGH-VALUES TO B03-ANTIDUR-DT-CALC-NULL
           END-IF
           IF IND-B03-DUR-RES-DT-CALC = -1
              MOVE HIGH-VALUES TO B03-DUR-RES-DT-CALC-NULL
           END-IF
           IF IND-B03-DT-EFF-CAMB-STAT = -1
              MOVE HIGH-VALUES TO B03-DT-EFF-CAMB-STAT-NULL
           END-IF
           IF IND-B03-DT-EMIS-CAMB-STAT = -1
              MOVE HIGH-VALUES TO B03-DT-EMIS-CAMB-STAT-NULL
           END-IF
           IF IND-B03-DT-EFF-STAB = -1
              MOVE HIGH-VALUES TO B03-DT-EFF-STAB-NULL
           END-IF
           IF IND-B03-CPT-DT-STAB = -1
              MOVE HIGH-VALUES TO B03-CPT-DT-STAB-NULL
           END-IF
           IF IND-B03-DT-EFF-RIDZ = -1
              MOVE HIGH-VALUES TO B03-DT-EFF-RIDZ-NULL
           END-IF
           IF IND-B03-DT-EMIS-RIDZ = -1
              MOVE HIGH-VALUES TO B03-DT-EMIS-RIDZ-NULL
           END-IF
           IF IND-B03-CPT-DT-RIDZ = -1
              MOVE HIGH-VALUES TO B03-CPT-DT-RIDZ-NULL
           END-IF
           IF IND-B03-FRAZ = -1
              MOVE HIGH-VALUES TO B03-FRAZ-NULL
           END-IF
           IF IND-B03-DUR-PAG-PRE = -1
              MOVE HIGH-VALUES TO B03-DUR-PAG-PRE-NULL
           END-IF
           IF IND-B03-NUM-PRE-PATT = -1
              MOVE HIGH-VALUES TO B03-NUM-PRE-PATT-NULL
           END-IF
           IF IND-B03-FRAZ-INI-EROG-REN = -1
              MOVE HIGH-VALUES TO B03-FRAZ-INI-EROG-REN-NULL
           END-IF
           IF IND-B03-AA-REN-CER = -1
              MOVE HIGH-VALUES TO B03-AA-REN-CER-NULL
           END-IF
           IF IND-B03-RAT-REN = -1
              MOVE HIGH-VALUES TO B03-RAT-REN-NULL
           END-IF
           IF IND-B03-COD-DIV = -1
              MOVE HIGH-VALUES TO B03-COD-DIV-NULL
           END-IF
           IF IND-B03-RISCPAR = -1
              MOVE HIGH-VALUES TO B03-RISCPAR-NULL
           END-IF
           IF IND-B03-CUM-RISCPAR = -1
              MOVE HIGH-VALUES TO B03-CUM-RISCPAR-NULL
           END-IF
           IF IND-B03-ULT-RM = -1
              MOVE HIGH-VALUES TO B03-ULT-RM-NULL
           END-IF
           IF IND-B03-TS-RENDTO-T = -1
              MOVE HIGH-VALUES TO B03-TS-RENDTO-T-NULL
           END-IF
           IF IND-B03-ALQ-RETR-T = -1
              MOVE HIGH-VALUES TO B03-ALQ-RETR-T-NULL
           END-IF
           IF IND-B03-MIN-TRNUT-T = -1
              MOVE HIGH-VALUES TO B03-MIN-TRNUT-T-NULL
           END-IF
           IF IND-B03-TS-NET-T = -1
              MOVE HIGH-VALUES TO B03-TS-NET-T-NULL
           END-IF
           IF IND-B03-DT-ULT-RIVAL = -1
              MOVE HIGH-VALUES TO B03-DT-ULT-RIVAL-NULL
           END-IF
           IF IND-B03-PRSTZ-INI = -1
              MOVE HIGH-VALUES TO B03-PRSTZ-INI-NULL
           END-IF
           IF IND-B03-PRSTZ-AGG-INI = -1
              MOVE HIGH-VALUES TO B03-PRSTZ-AGG-INI-NULL
           END-IF
           IF IND-B03-PRSTZ-AGG-ULT = -1
              MOVE HIGH-VALUES TO B03-PRSTZ-AGG-ULT-NULL
           END-IF
           IF IND-B03-RAPPEL = -1
              MOVE HIGH-VALUES TO B03-RAPPEL-NULL
           END-IF
           IF IND-B03-PRE-PATTUITO-INI = -1
              MOVE HIGH-VALUES TO B03-PRE-PATTUITO-INI-NULL
           END-IF
           IF IND-B03-PRE-DOV-INI = -1
              MOVE HIGH-VALUES TO B03-PRE-DOV-INI-NULL
           END-IF
           IF IND-B03-PRE-DOV-RIVTO-T = -1
              MOVE HIGH-VALUES TO B03-PRE-DOV-RIVTO-T-NULL
           END-IF
           IF IND-B03-PRE-ANNUALIZ-RICOR = -1
              MOVE HIGH-VALUES TO B03-PRE-ANNUALIZ-RICOR-NULL
           END-IF
           IF IND-B03-PRE-CONT = -1
              MOVE HIGH-VALUES TO B03-PRE-CONT-NULL
           END-IF
           IF IND-B03-PRE-PP-INI = -1
              MOVE HIGH-VALUES TO B03-PRE-PP-INI-NULL
           END-IF
           IF IND-B03-RIS-PURA-T = -1
              MOVE HIGH-VALUES TO B03-RIS-PURA-T-NULL
           END-IF
           IF IND-B03-PROV-ACQ = -1
              MOVE HIGH-VALUES TO B03-PROV-ACQ-NULL
           END-IF
           IF IND-B03-PROV-ACQ-RICOR = -1
              MOVE HIGH-VALUES TO B03-PROV-ACQ-RICOR-NULL
           END-IF
           IF IND-B03-PROV-INC = -1
              MOVE HIGH-VALUES TO B03-PROV-INC-NULL
           END-IF
           IF IND-B03-CAR-ACQ-NON-SCON = -1
              MOVE HIGH-VALUES TO B03-CAR-ACQ-NON-SCON-NULL
           END-IF
           IF IND-B03-OVER-COMM = -1
              MOVE HIGH-VALUES TO B03-OVER-COMM-NULL
           END-IF
           IF IND-B03-CAR-ACQ-PRECONTATO = -1
              MOVE HIGH-VALUES TO B03-CAR-ACQ-PRECONTATO-NULL
           END-IF
           IF IND-B03-RIS-ACQ-T = -1
              MOVE HIGH-VALUES TO B03-RIS-ACQ-T-NULL
           END-IF
           IF IND-B03-RIS-ZIL-T = -1
              MOVE HIGH-VALUES TO B03-RIS-ZIL-T-NULL
           END-IF
           IF IND-B03-CAR-GEST-NON-SCON = -1
              MOVE HIGH-VALUES TO B03-CAR-GEST-NON-SCON-NULL
           END-IF
           IF IND-B03-CAR-GEST = -1
              MOVE HIGH-VALUES TO B03-CAR-GEST-NULL
           END-IF
           IF IND-B03-RIS-SPE-T = -1
              MOVE HIGH-VALUES TO B03-RIS-SPE-T-NULL
           END-IF
           IF IND-B03-CAR-INC-NON-SCON = -1
              MOVE HIGH-VALUES TO B03-CAR-INC-NON-SCON-NULL
           END-IF
           IF IND-B03-CAR-INC = -1
              MOVE HIGH-VALUES TO B03-CAR-INC-NULL
           END-IF
           IF IND-B03-RIS-RISTORNI-CAP = -1
              MOVE HIGH-VALUES TO B03-RIS-RISTORNI-CAP-NULL
           END-IF
           IF IND-B03-INTR-TECN = -1
              MOVE HIGH-VALUES TO B03-INTR-TECN-NULL
           END-IF
           IF IND-B03-CPT-RSH-MOR = -1
              MOVE HIGH-VALUES TO B03-CPT-RSH-MOR-NULL
           END-IF
           IF IND-B03-C-SUBRSH-T = -1
              MOVE HIGH-VALUES TO B03-C-SUBRSH-T-NULL
           END-IF
           IF IND-B03-PRE-RSH-T = -1
              MOVE HIGH-VALUES TO B03-PRE-RSH-T-NULL
           END-IF
           IF IND-B03-ALQ-MARG-RIS = -1
              MOVE HIGH-VALUES TO B03-ALQ-MARG-RIS-NULL
           END-IF
           IF IND-B03-ALQ-MARG-C-SUBRSH = -1
              MOVE HIGH-VALUES TO B03-ALQ-MARG-C-SUBRSH-NULL
           END-IF
           IF IND-B03-TS-RENDTO-SPPR = -1
              MOVE HIGH-VALUES TO B03-TS-RENDTO-SPPR-NULL
           END-IF
           IF IND-B03-TP-IAS = -1
              MOVE HIGH-VALUES TO B03-TP-IAS-NULL
           END-IF
           IF IND-B03-NS-QUO = -1
              MOVE HIGH-VALUES TO B03-NS-QUO-NULL
           END-IF
           IF IND-B03-TS-MEDIO = -1
              MOVE HIGH-VALUES TO B03-TS-MEDIO-NULL
           END-IF
           IF IND-B03-CPT-RIASTO = -1
              MOVE HIGH-VALUES TO B03-CPT-RIASTO-NULL
           END-IF
           IF IND-B03-PRE-RIASTO = -1
              MOVE HIGH-VALUES TO B03-PRE-RIASTO-NULL
           END-IF
           IF IND-B03-RIS-RIASTA = -1
              MOVE HIGH-VALUES TO B03-RIS-RIASTA-NULL
           END-IF
           IF IND-B03-CPT-RIASTO-ECC = -1
              MOVE HIGH-VALUES TO B03-CPT-RIASTO-ECC-NULL
           END-IF
           IF IND-B03-PRE-RIASTO-ECC = -1
              MOVE HIGH-VALUES TO B03-PRE-RIASTO-ECC-NULL
           END-IF
           IF IND-B03-RIS-RIASTA-ECC = -1
              MOVE HIGH-VALUES TO B03-RIS-RIASTA-ECC-NULL
           END-IF
           IF IND-B03-COD-AGE = -1
              MOVE HIGH-VALUES TO B03-COD-AGE-NULL
           END-IF
           IF IND-B03-COD-SUBAGE = -1
              MOVE HIGH-VALUES TO B03-COD-SUBAGE-NULL
           END-IF
           IF IND-B03-COD-CAN = -1
              MOVE HIGH-VALUES TO B03-COD-CAN-NULL
           END-IF
           IF IND-B03-IB-POLI = -1
              MOVE HIGH-VALUES TO B03-IB-POLI-NULL
           END-IF
           IF IND-B03-IB-ADES = -1
              MOVE HIGH-VALUES TO B03-IB-ADES-NULL
           END-IF
           IF IND-B03-IB-TRCH-DI-GAR = -1
              MOVE HIGH-VALUES TO B03-IB-TRCH-DI-GAR-NULL
           END-IF
           IF IND-B03-TP-PRSTZ = -1
              MOVE HIGH-VALUES TO B03-TP-PRSTZ-NULL
           END-IF
           IF IND-B03-TP-TRASF = -1
              MOVE HIGH-VALUES TO B03-TP-TRASF-NULL
           END-IF
           IF IND-B03-PP-INVRIO-TARI = -1
              MOVE HIGH-VALUES TO B03-PP-INVRIO-TARI-NULL
           END-IF
           IF IND-B03-COEFF-OPZ-REN = -1
              MOVE HIGH-VALUES TO B03-COEFF-OPZ-REN-NULL
           END-IF
           IF IND-B03-COEFF-OPZ-CPT = -1
              MOVE HIGH-VALUES TO B03-COEFF-OPZ-CPT-NULL
           END-IF
           IF IND-B03-DUR-PAG-REN = -1
              MOVE HIGH-VALUES TO B03-DUR-PAG-REN-NULL
           END-IF
           IF IND-B03-VLT = -1
              MOVE HIGH-VALUES TO B03-VLT-NULL
           END-IF
           IF IND-B03-RIS-MAT-CHIU-PREC = -1
              MOVE HIGH-VALUES TO B03-RIS-MAT-CHIU-PREC-NULL
           END-IF
           IF IND-B03-COD-FND = -1
              MOVE HIGH-VALUES TO B03-COD-FND-NULL
           END-IF
           IF IND-B03-PRSTZ-T = -1
              MOVE HIGH-VALUES TO B03-PRSTZ-T-NULL
           END-IF
           IF IND-B03-TS-TARI-DOV = -1
              MOVE HIGH-VALUES TO B03-TS-TARI-DOV-NULL
           END-IF
           IF IND-B03-TS-TARI-SCON = -1
              MOVE HIGH-VALUES TO B03-TS-TARI-SCON-NULL
           END-IF
           IF IND-B03-TS-PP = -1
              MOVE HIGH-VALUES TO B03-TS-PP-NULL
           END-IF
           IF IND-B03-COEFF-RIS-1-T = -1
              MOVE HIGH-VALUES TO B03-COEFF-RIS-1-T-NULL
           END-IF
           IF IND-B03-COEFF-RIS-2-T = -1
              MOVE HIGH-VALUES TO B03-COEFF-RIS-2-T-NULL
           END-IF
           IF IND-B03-ABB = -1
              MOVE HIGH-VALUES TO B03-ABB-NULL
           END-IF
           IF IND-B03-TP-COASS = -1
              MOVE HIGH-VALUES TO B03-TP-COASS-NULL
           END-IF
           IF IND-B03-TRAT-RIASS = -1
              MOVE HIGH-VALUES TO B03-TRAT-RIASS-NULL
           END-IF
           IF IND-B03-TRAT-RIASS-ECC = -1
              MOVE HIGH-VALUES TO B03-TRAT-RIASS-ECC-NULL
           END-IF
           IF IND-B03-TP-RGM-FISC = -1
              MOVE HIGH-VALUES TO B03-TP-RGM-FISC-NULL
           END-IF
           IF IND-B03-DUR-GAR-AA = -1
              MOVE HIGH-VALUES TO B03-DUR-GAR-AA-NULL
           END-IF
           IF IND-B03-DUR-GAR-MM = -1
              MOVE HIGH-VALUES TO B03-DUR-GAR-MM-NULL
           END-IF
           IF IND-B03-DUR-GAR-GG = -1
              MOVE HIGH-VALUES TO B03-DUR-GAR-GG-NULL
           END-IF
           IF IND-B03-ANTIDUR-CALC-365 = -1
              MOVE HIGH-VALUES TO B03-ANTIDUR-CALC-365-NULL
           END-IF
           IF IND-B03-COD-FISC-CNTR = -1
              MOVE HIGH-VALUES TO B03-COD-FISC-CNTR-NULL
           END-IF
           IF IND-B03-COD-FISC-ASSTO1 = -1
              MOVE HIGH-VALUES TO B03-COD-FISC-ASSTO1-NULL
           END-IF
           IF IND-B03-COD-FISC-ASSTO2 = -1
              MOVE HIGH-VALUES TO B03-COD-FISC-ASSTO2-NULL
           END-IF
           IF IND-B03-COD-FISC-ASSTO3 = -1
              MOVE HIGH-VALUES TO B03-COD-FISC-ASSTO3-NULL
           END-IF
           IF IND-B03-CAUS-SCON = -1
              MOVE HIGH-VALUES TO B03-CAUS-SCON
           END-IF
           IF IND-B03-EMIT-TIT-OPZ = -1
              MOVE HIGH-VALUES TO B03-EMIT-TIT-OPZ
           END-IF
           IF IND-B03-QTZ-SP-Z-COUP-EMIS = -1
              MOVE HIGH-VALUES TO B03-QTZ-SP-Z-COUP-EMIS-NULL
           END-IF
           IF IND-B03-QTZ-SP-Z-OPZ-EMIS = -1
              MOVE HIGH-VALUES TO B03-QTZ-SP-Z-OPZ-EMIS-NULL
           END-IF
           IF IND-B03-QTZ-SP-Z-COUP-DT-C = -1
              MOVE HIGH-VALUES TO B03-QTZ-SP-Z-COUP-DT-C-NULL
           END-IF
           IF IND-B03-QTZ-SP-Z-OPZ-DT-CA = -1
              MOVE HIGH-VALUES TO B03-QTZ-SP-Z-OPZ-DT-CA-NULL
           END-IF
           IF IND-B03-QTZ-TOT-EMIS = -1
              MOVE HIGH-VALUES TO B03-QTZ-TOT-EMIS-NULL
           END-IF
           IF IND-B03-QTZ-TOT-DT-CALC = -1
              MOVE HIGH-VALUES TO B03-QTZ-TOT-DT-CALC-NULL
           END-IF
           IF IND-B03-QTZ-TOT-DT-ULT-BIL = -1
              MOVE HIGH-VALUES TO B03-QTZ-TOT-DT-ULT-BIL-NULL
           END-IF
           IF IND-B03-DT-QTZ-EMIS = -1
              MOVE HIGH-VALUES TO B03-DT-QTZ-EMIS-NULL
           END-IF
           IF IND-B03-PC-CAR-GEST = -1
              MOVE HIGH-VALUES TO B03-PC-CAR-GEST-NULL
           END-IF
           IF IND-B03-PC-CAR-ACQ = -1
              MOVE HIGH-VALUES TO B03-PC-CAR-ACQ-NULL
           END-IF
           IF IND-B03-IMP-CAR-CASO-MOR = -1
              MOVE HIGH-VALUES TO B03-IMP-CAR-CASO-MOR-NULL
           END-IF
           IF IND-B03-PC-CAR-MOR = -1
              MOVE HIGH-VALUES TO B03-PC-CAR-MOR-NULL
           END-IF
           IF IND-B03-TP-VERS = -1
              MOVE HIGH-VALUES TO B03-TP-VERS-NULL
           END-IF
           IF IND-B03-FL-SWITCH = -1
              MOVE HIGH-VALUES TO B03-FL-SWITCH-NULL
           END-IF
           IF IND-B03-FL-IAS = -1
              MOVE HIGH-VALUES TO B03-FL-IAS-NULL
           END-IF
           IF IND-B03-DIR = -1
              MOVE HIGH-VALUES TO B03-DIR-NULL
           END-IF
           IF IND-B03-TP-COP-CASO-MOR = -1
              MOVE HIGH-VALUES TO B03-TP-COP-CASO-MOR-NULL
           END-IF
           IF IND-B03-MET-RISC-SPCL = -1
              MOVE HIGH-VALUES TO B03-MET-RISC-SPCL-NULL
           END-IF
           IF IND-B03-TP-STAT-INVST = -1
              MOVE HIGH-VALUES TO B03-TP-STAT-INVST-NULL
           END-IF
           IF IND-B03-COD-PRDT = -1
              MOVE HIGH-VALUES TO B03-COD-PRDT-NULL
           END-IF
           IF IND-B03-STAT-ASSTO-1 = -1
              MOVE HIGH-VALUES TO B03-STAT-ASSTO-1-NULL
           END-IF
           IF IND-B03-STAT-ASSTO-2 = -1
              MOVE HIGH-VALUES TO B03-STAT-ASSTO-2-NULL
           END-IF
           IF IND-B03-STAT-ASSTO-3 = -1
              MOVE HIGH-VALUES TO B03-STAT-ASSTO-3-NULL
           END-IF
           IF IND-B03-CPT-ASSTO-INI-MOR = -1
              MOVE HIGH-VALUES TO B03-CPT-ASSTO-INI-MOR-NULL
           END-IF
           IF IND-B03-TS-STAB-PRE = -1
              MOVE HIGH-VALUES TO B03-TS-STAB-PRE-NULL
           END-IF
           IF IND-B03-DIR-EMIS = -1
              MOVE HIGH-VALUES TO B03-DIR-EMIS-NULL
           END-IF
           IF IND-B03-DT-INC-ULT-PRE = -1
              MOVE HIGH-VALUES TO B03-DT-INC-ULT-PRE-NULL
           END-IF
           IF IND-B03-STAT-TBGC-ASSTO-1 = -1
              MOVE HIGH-VALUES TO B03-STAT-TBGC-ASSTO-1-NULL
           END-IF
           IF IND-B03-STAT-TBGC-ASSTO-2 = -1
              MOVE HIGH-VALUES TO B03-STAT-TBGC-ASSTO-2-NULL
           END-IF
           IF IND-B03-STAT-TBGC-ASSTO-3 = -1
              MOVE HIGH-VALUES TO B03-STAT-TBGC-ASSTO-3-NULL
           END-IF
           IF IND-B03-FRAZ-DECR-CPT = -1
              MOVE HIGH-VALUES TO B03-FRAZ-DECR-CPT-NULL
           END-IF
           IF IND-B03-PRE-PP-ULT = -1
              MOVE HIGH-VALUES TO B03-PRE-PP-ULT-NULL
           END-IF
           IF IND-B03-ACQ-EXP = -1
              MOVE HIGH-VALUES TO B03-ACQ-EXP-NULL
           END-IF
           IF IND-B03-REMUN-ASS = -1
              MOVE HIGH-VALUES TO B03-REMUN-ASS-NULL
           END-IF
           IF IND-B03-COMMIS-INTER = -1
              MOVE HIGH-VALUES TO B03-COMMIS-INTER-NULL
           END-IF
           IF IND-B03-NUM-FINANZ = -1
              MOVE HIGH-VALUES TO B03-NUM-FINANZ-NULL
           END-IF
           IF IND-B03-TP-ACC-COMM = -1
              MOVE HIGH-VALUES TO B03-TP-ACC-COMM-NULL
           END-IF
           IF IND-B03-IB-ACC-COMM = -1
              MOVE HIGH-VALUES TO B03-IB-ACC-COMM-NULL
           END-IF
           IF IND-B03-CARZ = -1
              MOVE HIGH-VALUES TO B03-CARZ-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO B03-DS-OPER-SQL
           MOVE 0                   TO B03-DS-VER
           MOVE IDSV0003-USER-NAME TO B03-DS-UTENTE
           MOVE '1'                   TO B03-DS-STATO-ELAB
           MOVE WS-TS-COMPETENZA-AGG-STOR TO B03-DS-TS-CPTZ.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO B03-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO B03-DS-UTENTE
           MOVE WS-TS-COMPETENZA-AGG-STOR TO B03-DS-TS-CPTZ.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF B03-ID-RICH-ESTRAZ-AGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ID-RICH-ESTRAZ-AGG
           ELSE
              MOVE 0 TO IND-B03-ID-RICH-ESTRAZ-AGG
           END-IF
           IF B03-FL-SIMULAZIONE-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FL-SIMULAZIONE
           ELSE
              MOVE 0 TO IND-B03-FL-SIMULAZIONE
           END-IF
           IF B03-DT-INI-VAL-TAR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-INI-VAL-TAR
           ELSE
              MOVE 0 TO IND-B03-DT-INI-VAL-TAR
           END-IF
           IF B03-COD-PROD-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-PROD
           ELSE
              MOVE 0 TO IND-B03-COD-PROD
           END-IF
           IF B03-COD-TARI-ORGN-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-TARI-ORGN
           ELSE
              MOVE 0 TO IND-B03-COD-TARI-ORGN
           END-IF
           IF B03-MIN-GARTO-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-MIN-GARTO-T
           ELSE
              MOVE 0 TO IND-B03-MIN-GARTO-T
           END-IF
           IF B03-TP-TARI-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-TARI
           ELSE
              MOVE 0 TO IND-B03-TP-TARI
           END-IF
           IF B03-TP-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-PRE
           ELSE
              MOVE 0 TO IND-B03-TP-PRE
           END-IF
           IF B03-TP-ADEG-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-ADEG-PRE
           ELSE
              MOVE 0 TO IND-B03-TP-ADEG-PRE
           END-IF
           IF B03-TP-RIVAL-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-RIVAL
           ELSE
              MOVE 0 TO IND-B03-TP-RIVAL
           END-IF
           IF B03-FL-DA-TRASF-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FL-DA-TRASF
           ELSE
              MOVE 0 TO IND-B03-FL-DA-TRASF
           END-IF
           IF B03-FL-CAR-CONT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FL-CAR-CONT
           ELSE
              MOVE 0 TO IND-B03-FL-CAR-CONT
           END-IF
           IF B03-FL-PRE-DA-RIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FL-PRE-DA-RIS
           ELSE
              MOVE 0 TO IND-B03-FL-PRE-DA-RIS
           END-IF
           IF B03-FL-PRE-AGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FL-PRE-AGG
           ELSE
              MOVE 0 TO IND-B03-FL-PRE-AGG
           END-IF
           IF B03-TP-TRCH-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-TRCH
           ELSE
              MOVE 0 TO IND-B03-TP-TRCH
           END-IF
           IF B03-TP-TST-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-TST
           ELSE
              MOVE 0 TO IND-B03-TP-TST
           END-IF
           IF B03-COD-CONV-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-CONV
           ELSE
              MOVE 0 TO IND-B03-COD-CONV
           END-IF
           IF B03-DT-DECOR-ADES-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-DECOR-ADES
           ELSE
              MOVE 0 TO IND-B03-DT-DECOR-ADES
           END-IF
           IF B03-DT-EMIS-TRCH-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-EMIS-TRCH
           ELSE
              MOVE 0 TO IND-B03-DT-EMIS-TRCH
           END-IF
           IF B03-DT-SCAD-TRCH-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-SCAD-TRCH
           ELSE
              MOVE 0 TO IND-B03-DT-SCAD-TRCH
           END-IF
           IF B03-DT-SCAD-INTMD-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-SCAD-INTMD
           ELSE
              MOVE 0 TO IND-B03-DT-SCAD-INTMD
           END-IF
           IF B03-DT-SCAD-PAG-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-SCAD-PAG-PRE
           ELSE
              MOVE 0 TO IND-B03-DT-SCAD-PAG-PRE
           END-IF
           IF B03-DT-ULT-PRE-PAG-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-ULT-PRE-PAG
           ELSE
              MOVE 0 TO IND-B03-DT-ULT-PRE-PAG
           END-IF
           IF B03-DT-NASC-1O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-NASC-1O-ASSTO
           ELSE
              MOVE 0 TO IND-B03-DT-NASC-1O-ASSTO
           END-IF
           IF B03-SEX-1O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-SEX-1O-ASSTO
           ELSE
              MOVE 0 TO IND-B03-SEX-1O-ASSTO
           END-IF
           IF B03-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ETA-AA-1O-ASSTO
           ELSE
              MOVE 0 TO IND-B03-ETA-AA-1O-ASSTO
           END-IF
           IF B03-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ETA-MM-1O-ASSTO
           ELSE
              MOVE 0 TO IND-B03-ETA-MM-1O-ASSTO
           END-IF
           IF B03-ETA-RAGGN-DT-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ETA-RAGGN-DT-CALC
           ELSE
              MOVE 0 TO IND-B03-ETA-RAGGN-DT-CALC
           END-IF
           IF B03-DUR-AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-AA
           ELSE
              MOVE 0 TO IND-B03-DUR-AA
           END-IF
           IF B03-DUR-MM-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-MM
           ELSE
              MOVE 0 TO IND-B03-DUR-MM
           END-IF
           IF B03-DUR-GG-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-GG
           ELSE
              MOVE 0 TO IND-B03-DUR-GG
           END-IF
           IF B03-DUR-1O-PER-AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-1O-PER-AA
           ELSE
              MOVE 0 TO IND-B03-DUR-1O-PER-AA
           END-IF
           IF B03-DUR-1O-PER-MM-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-1O-PER-MM
           ELSE
              MOVE 0 TO IND-B03-DUR-1O-PER-MM
           END-IF
           IF B03-DUR-1O-PER-GG-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-1O-PER-GG
           ELSE
              MOVE 0 TO IND-B03-DUR-1O-PER-GG
           END-IF
           IF B03-ANTIDUR-RICOR-PREC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ANTIDUR-RICOR-PREC
           ELSE
              MOVE 0 TO IND-B03-ANTIDUR-RICOR-PREC
           END-IF
           IF B03-ANTIDUR-DT-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ANTIDUR-DT-CALC
           ELSE
              MOVE 0 TO IND-B03-ANTIDUR-DT-CALC
           END-IF
           IF B03-DUR-RES-DT-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-RES-DT-CALC
           ELSE
              MOVE 0 TO IND-B03-DUR-RES-DT-CALC
           END-IF
           IF B03-DT-EFF-CAMB-STAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-EFF-CAMB-STAT
           ELSE
              MOVE 0 TO IND-B03-DT-EFF-CAMB-STAT
           END-IF
           IF B03-DT-EMIS-CAMB-STAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-EMIS-CAMB-STAT
           ELSE
              MOVE 0 TO IND-B03-DT-EMIS-CAMB-STAT
           END-IF
           IF B03-DT-EFF-STAB-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-EFF-STAB
           ELSE
              MOVE 0 TO IND-B03-DT-EFF-STAB
           END-IF
           IF B03-CPT-DT-STAB-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CPT-DT-STAB
           ELSE
              MOVE 0 TO IND-B03-CPT-DT-STAB
           END-IF
           IF B03-DT-EFF-RIDZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-EFF-RIDZ
           ELSE
              MOVE 0 TO IND-B03-DT-EFF-RIDZ
           END-IF
           IF B03-DT-EMIS-RIDZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-EMIS-RIDZ
           ELSE
              MOVE 0 TO IND-B03-DT-EMIS-RIDZ
           END-IF
           IF B03-CPT-DT-RIDZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CPT-DT-RIDZ
           ELSE
              MOVE 0 TO IND-B03-CPT-DT-RIDZ
           END-IF
           IF B03-FRAZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FRAZ
           ELSE
              MOVE 0 TO IND-B03-FRAZ
           END-IF
           IF B03-DUR-PAG-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-PAG-PRE
           ELSE
              MOVE 0 TO IND-B03-DUR-PAG-PRE
           END-IF
           IF B03-NUM-PRE-PATT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-NUM-PRE-PATT
           ELSE
              MOVE 0 TO IND-B03-NUM-PRE-PATT
           END-IF
           IF B03-FRAZ-INI-EROG-REN-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FRAZ-INI-EROG-REN
           ELSE
              MOVE 0 TO IND-B03-FRAZ-INI-EROG-REN
           END-IF
           IF B03-AA-REN-CER-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-AA-REN-CER
           ELSE
              MOVE 0 TO IND-B03-AA-REN-CER
           END-IF
           IF B03-RAT-REN-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RAT-REN
           ELSE
              MOVE 0 TO IND-B03-RAT-REN
           END-IF
           IF B03-COD-DIV-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-DIV
           ELSE
              MOVE 0 TO IND-B03-COD-DIV
           END-IF
           IF B03-RISCPAR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RISCPAR
           ELSE
              MOVE 0 TO IND-B03-RISCPAR
           END-IF
           IF B03-CUM-RISCPAR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CUM-RISCPAR
           ELSE
              MOVE 0 TO IND-B03-CUM-RISCPAR
           END-IF
           IF B03-ULT-RM-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ULT-RM
           ELSE
              MOVE 0 TO IND-B03-ULT-RM
           END-IF
           IF B03-TS-RENDTO-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TS-RENDTO-T
           ELSE
              MOVE 0 TO IND-B03-TS-RENDTO-T
           END-IF
           IF B03-ALQ-RETR-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ALQ-RETR-T
           ELSE
              MOVE 0 TO IND-B03-ALQ-RETR-T
           END-IF
           IF B03-MIN-TRNUT-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-MIN-TRNUT-T
           ELSE
              MOVE 0 TO IND-B03-MIN-TRNUT-T
           END-IF
           IF B03-TS-NET-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TS-NET-T
           ELSE
              MOVE 0 TO IND-B03-TS-NET-T
           END-IF
           IF B03-DT-ULT-RIVAL-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-ULT-RIVAL
           ELSE
              MOVE 0 TO IND-B03-DT-ULT-RIVAL
           END-IF
           IF B03-PRSTZ-INI-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRSTZ-INI
           ELSE
              MOVE 0 TO IND-B03-PRSTZ-INI
           END-IF
           IF B03-PRSTZ-AGG-INI-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRSTZ-AGG-INI
           ELSE
              MOVE 0 TO IND-B03-PRSTZ-AGG-INI
           END-IF
           IF B03-PRSTZ-AGG-ULT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRSTZ-AGG-ULT
           ELSE
              MOVE 0 TO IND-B03-PRSTZ-AGG-ULT
           END-IF
           IF B03-RAPPEL-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RAPPEL
           ELSE
              MOVE 0 TO IND-B03-RAPPEL
           END-IF
           IF B03-PRE-PATTUITO-INI-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-PATTUITO-INI
           ELSE
              MOVE 0 TO IND-B03-PRE-PATTUITO-INI
           END-IF
           IF B03-PRE-DOV-INI-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-DOV-INI
           ELSE
              MOVE 0 TO IND-B03-PRE-DOV-INI
           END-IF
           IF B03-PRE-DOV-RIVTO-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-DOV-RIVTO-T
           ELSE
              MOVE 0 TO IND-B03-PRE-DOV-RIVTO-T
           END-IF
           IF B03-PRE-ANNUALIZ-RICOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-ANNUALIZ-RICOR
           ELSE
              MOVE 0 TO IND-B03-PRE-ANNUALIZ-RICOR
           END-IF
           IF B03-PRE-CONT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-CONT
           ELSE
              MOVE 0 TO IND-B03-PRE-CONT
           END-IF
           IF B03-PRE-PP-INI-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-PP-INI
           ELSE
              MOVE 0 TO IND-B03-PRE-PP-INI
           END-IF
           IF B03-RIS-PURA-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RIS-PURA-T
           ELSE
              MOVE 0 TO IND-B03-RIS-PURA-T
           END-IF
           IF B03-PROV-ACQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PROV-ACQ
           ELSE
              MOVE 0 TO IND-B03-PROV-ACQ
           END-IF
           IF B03-PROV-ACQ-RICOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PROV-ACQ-RICOR
           ELSE
              MOVE 0 TO IND-B03-PROV-ACQ-RICOR
           END-IF
           IF B03-PROV-INC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PROV-INC
           ELSE
              MOVE 0 TO IND-B03-PROV-INC
           END-IF
           IF B03-CAR-ACQ-NON-SCON-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CAR-ACQ-NON-SCON
           ELSE
              MOVE 0 TO IND-B03-CAR-ACQ-NON-SCON
           END-IF
           IF B03-OVER-COMM-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-OVER-COMM
           ELSE
              MOVE 0 TO IND-B03-OVER-COMM
           END-IF
           IF B03-CAR-ACQ-PRECONTATO-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CAR-ACQ-PRECONTATO
           ELSE
              MOVE 0 TO IND-B03-CAR-ACQ-PRECONTATO
           END-IF
           IF B03-RIS-ACQ-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RIS-ACQ-T
           ELSE
              MOVE 0 TO IND-B03-RIS-ACQ-T
           END-IF
           IF B03-RIS-ZIL-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RIS-ZIL-T
           ELSE
              MOVE 0 TO IND-B03-RIS-ZIL-T
           END-IF
           IF B03-CAR-GEST-NON-SCON-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CAR-GEST-NON-SCON
           ELSE
              MOVE 0 TO IND-B03-CAR-GEST-NON-SCON
           END-IF
           IF B03-CAR-GEST-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CAR-GEST
           ELSE
              MOVE 0 TO IND-B03-CAR-GEST
           END-IF
           IF B03-RIS-SPE-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RIS-SPE-T
           ELSE
              MOVE 0 TO IND-B03-RIS-SPE-T
           END-IF
           IF B03-CAR-INC-NON-SCON-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CAR-INC-NON-SCON
           ELSE
              MOVE 0 TO IND-B03-CAR-INC-NON-SCON
           END-IF
           IF B03-CAR-INC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CAR-INC
           ELSE
              MOVE 0 TO IND-B03-CAR-INC
           END-IF
           IF B03-RIS-RISTORNI-CAP-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RIS-RISTORNI-CAP
           ELSE
              MOVE 0 TO IND-B03-RIS-RISTORNI-CAP
           END-IF
           IF B03-INTR-TECN-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-INTR-TECN
           ELSE
              MOVE 0 TO IND-B03-INTR-TECN
           END-IF
           IF B03-CPT-RSH-MOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CPT-RSH-MOR
           ELSE
              MOVE 0 TO IND-B03-CPT-RSH-MOR
           END-IF
           IF B03-C-SUBRSH-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-C-SUBRSH-T
           ELSE
              MOVE 0 TO IND-B03-C-SUBRSH-T
           END-IF
           IF B03-PRE-RSH-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-RSH-T
           ELSE
              MOVE 0 TO IND-B03-PRE-RSH-T
           END-IF
           IF B03-ALQ-MARG-RIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ALQ-MARG-RIS
           ELSE
              MOVE 0 TO IND-B03-ALQ-MARG-RIS
           END-IF
           IF B03-ALQ-MARG-C-SUBRSH-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ALQ-MARG-C-SUBRSH
           ELSE
              MOVE 0 TO IND-B03-ALQ-MARG-C-SUBRSH
           END-IF
           IF B03-TS-RENDTO-SPPR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TS-RENDTO-SPPR
           ELSE
              MOVE 0 TO IND-B03-TS-RENDTO-SPPR
           END-IF
           IF B03-TP-IAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-IAS
           ELSE
              MOVE 0 TO IND-B03-TP-IAS
           END-IF
           IF B03-NS-QUO-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-NS-QUO
           ELSE
              MOVE 0 TO IND-B03-NS-QUO
           END-IF
           IF B03-TS-MEDIO-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TS-MEDIO
           ELSE
              MOVE 0 TO IND-B03-TS-MEDIO
           END-IF
           IF B03-CPT-RIASTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CPT-RIASTO
           ELSE
              MOVE 0 TO IND-B03-CPT-RIASTO
           END-IF
           IF B03-PRE-RIASTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-RIASTO
           ELSE
              MOVE 0 TO IND-B03-PRE-RIASTO
           END-IF
           IF B03-RIS-RIASTA-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RIS-RIASTA
           ELSE
              MOVE 0 TO IND-B03-RIS-RIASTA
           END-IF
           IF B03-CPT-RIASTO-ECC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CPT-RIASTO-ECC
           ELSE
              MOVE 0 TO IND-B03-CPT-RIASTO-ECC
           END-IF
           IF B03-PRE-RIASTO-ECC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-RIASTO-ECC
           ELSE
              MOVE 0 TO IND-B03-PRE-RIASTO-ECC
           END-IF
           IF B03-RIS-RIASTA-ECC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RIS-RIASTA-ECC
           ELSE
              MOVE 0 TO IND-B03-RIS-RIASTA-ECC
           END-IF
           IF B03-COD-AGE-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-AGE
           ELSE
              MOVE 0 TO IND-B03-COD-AGE
           END-IF
           IF B03-COD-SUBAGE-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-SUBAGE
           ELSE
              MOVE 0 TO IND-B03-COD-SUBAGE
           END-IF
           IF B03-COD-CAN-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-CAN
           ELSE
              MOVE 0 TO IND-B03-COD-CAN
           END-IF
           IF B03-IB-POLI-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-IB-POLI
           ELSE
              MOVE 0 TO IND-B03-IB-POLI
           END-IF
           IF B03-IB-ADES-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-IB-ADES
           ELSE
              MOVE 0 TO IND-B03-IB-ADES
           END-IF
           IF B03-IB-TRCH-DI-GAR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-IB-TRCH-DI-GAR
           ELSE
              MOVE 0 TO IND-B03-IB-TRCH-DI-GAR
           END-IF
           IF B03-TP-PRSTZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-PRSTZ
           ELSE
              MOVE 0 TO IND-B03-TP-PRSTZ
           END-IF
           IF B03-TP-TRASF-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-TRASF
           ELSE
              MOVE 0 TO IND-B03-TP-TRASF
           END-IF
           IF B03-PP-INVRIO-TARI-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PP-INVRIO-TARI
           ELSE
              MOVE 0 TO IND-B03-PP-INVRIO-TARI
           END-IF
           IF B03-COEFF-OPZ-REN-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COEFF-OPZ-REN
           ELSE
              MOVE 0 TO IND-B03-COEFF-OPZ-REN
           END-IF
           IF B03-COEFF-OPZ-CPT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COEFF-OPZ-CPT
           ELSE
              MOVE 0 TO IND-B03-COEFF-OPZ-CPT
           END-IF
           IF B03-DUR-PAG-REN-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-PAG-REN
           ELSE
              MOVE 0 TO IND-B03-DUR-PAG-REN
           END-IF
           IF B03-VLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-VLT
           ELSE
              MOVE 0 TO IND-B03-VLT
           END-IF
           IF B03-RIS-MAT-CHIU-PREC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-RIS-MAT-CHIU-PREC
           ELSE
              MOVE 0 TO IND-B03-RIS-MAT-CHIU-PREC
           END-IF
           IF B03-COD-FND-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-FND
           ELSE
              MOVE 0 TO IND-B03-COD-FND
           END-IF
           IF B03-PRSTZ-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRSTZ-T
           ELSE
              MOVE 0 TO IND-B03-PRSTZ-T
           END-IF
           IF B03-TS-TARI-DOV-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TS-TARI-DOV
           ELSE
              MOVE 0 TO IND-B03-TS-TARI-DOV
           END-IF
           IF B03-TS-TARI-SCON-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TS-TARI-SCON
           ELSE
              MOVE 0 TO IND-B03-TS-TARI-SCON
           END-IF
           IF B03-TS-PP-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TS-PP
           ELSE
              MOVE 0 TO IND-B03-TS-PP
           END-IF
           IF B03-COEFF-RIS-1-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COEFF-RIS-1-T
           ELSE
              MOVE 0 TO IND-B03-COEFF-RIS-1-T
           END-IF
           IF B03-COEFF-RIS-2-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COEFF-RIS-2-T
           ELSE
              MOVE 0 TO IND-B03-COEFF-RIS-2-T
           END-IF
           IF B03-ABB-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ABB
           ELSE
              MOVE 0 TO IND-B03-ABB
           END-IF
           IF B03-TP-COASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-COASS
           ELSE
              MOVE 0 TO IND-B03-TP-COASS
           END-IF
           IF B03-TRAT-RIASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TRAT-RIASS
           ELSE
              MOVE 0 TO IND-B03-TRAT-RIASS
           END-IF
           IF B03-TRAT-RIASS-ECC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TRAT-RIASS-ECC
           ELSE
              MOVE 0 TO IND-B03-TRAT-RIASS-ECC
           END-IF
           IF B03-TP-RGM-FISC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-RGM-FISC
           ELSE
              MOVE 0 TO IND-B03-TP-RGM-FISC
           END-IF
           IF B03-DUR-GAR-AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-GAR-AA
           ELSE
              MOVE 0 TO IND-B03-DUR-GAR-AA
           END-IF
           IF B03-DUR-GAR-MM-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-GAR-MM
           ELSE
              MOVE 0 TO IND-B03-DUR-GAR-MM
           END-IF
           IF B03-DUR-GAR-GG-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DUR-GAR-GG
           ELSE
              MOVE 0 TO IND-B03-DUR-GAR-GG
           END-IF
           IF B03-ANTIDUR-CALC-365-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ANTIDUR-CALC-365
           ELSE
              MOVE 0 TO IND-B03-ANTIDUR-CALC-365
           END-IF
           IF B03-COD-FISC-CNTR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-FISC-CNTR
           ELSE
              MOVE 0 TO IND-B03-COD-FISC-CNTR
           END-IF
           IF B03-COD-FISC-ASSTO1-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-FISC-ASSTO1
           ELSE
              MOVE 0 TO IND-B03-COD-FISC-ASSTO1
           END-IF
           IF B03-COD-FISC-ASSTO2-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-FISC-ASSTO2
           ELSE
              MOVE 0 TO IND-B03-COD-FISC-ASSTO2
           END-IF
           IF B03-COD-FISC-ASSTO3-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-FISC-ASSTO3
           ELSE
              MOVE 0 TO IND-B03-COD-FISC-ASSTO3
           END-IF
           IF B03-CAUS-SCON = HIGH-VALUES
              MOVE -1 TO IND-B03-CAUS-SCON
           ELSE
              MOVE 0 TO IND-B03-CAUS-SCON
           END-IF
           IF B03-EMIT-TIT-OPZ = HIGH-VALUES
              MOVE -1 TO IND-B03-EMIT-TIT-OPZ
           ELSE
              MOVE 0 TO IND-B03-EMIT-TIT-OPZ
           END-IF
           IF B03-QTZ-SP-Z-COUP-EMIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-QTZ-SP-Z-COUP-EMIS
           ELSE
              MOVE 0 TO IND-B03-QTZ-SP-Z-COUP-EMIS
           END-IF
           IF B03-QTZ-SP-Z-OPZ-EMIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-QTZ-SP-Z-OPZ-EMIS
           ELSE
              MOVE 0 TO IND-B03-QTZ-SP-Z-OPZ-EMIS
           END-IF
           IF B03-QTZ-SP-Z-COUP-DT-C-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-QTZ-SP-Z-COUP-DT-C
           ELSE
              MOVE 0 TO IND-B03-QTZ-SP-Z-COUP-DT-C
           END-IF
           IF B03-QTZ-SP-Z-OPZ-DT-CA-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-QTZ-SP-Z-OPZ-DT-CA
           ELSE
              MOVE 0 TO IND-B03-QTZ-SP-Z-OPZ-DT-CA
           END-IF
           IF B03-QTZ-TOT-EMIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-QTZ-TOT-EMIS
           ELSE
              MOVE 0 TO IND-B03-QTZ-TOT-EMIS
           END-IF
           IF B03-QTZ-TOT-DT-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-QTZ-TOT-DT-CALC
           ELSE
              MOVE 0 TO IND-B03-QTZ-TOT-DT-CALC
           END-IF
           IF B03-QTZ-TOT-DT-ULT-BIL-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-QTZ-TOT-DT-ULT-BIL
           ELSE
              MOVE 0 TO IND-B03-QTZ-TOT-DT-ULT-BIL
           END-IF
           IF B03-DT-QTZ-EMIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-QTZ-EMIS
           ELSE
              MOVE 0 TO IND-B03-DT-QTZ-EMIS
           END-IF
           IF B03-PC-CAR-GEST-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PC-CAR-GEST
           ELSE
              MOVE 0 TO IND-B03-PC-CAR-GEST
           END-IF
           IF B03-PC-CAR-ACQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PC-CAR-ACQ
           ELSE
              MOVE 0 TO IND-B03-PC-CAR-ACQ
           END-IF
           IF B03-IMP-CAR-CASO-MOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-IMP-CAR-CASO-MOR
           ELSE
              MOVE 0 TO IND-B03-IMP-CAR-CASO-MOR
           END-IF
           IF B03-PC-CAR-MOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PC-CAR-MOR
           ELSE
              MOVE 0 TO IND-B03-PC-CAR-MOR
           END-IF
           IF B03-TP-VERS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-VERS
           ELSE
              MOVE 0 TO IND-B03-TP-VERS
           END-IF
           IF B03-FL-SWITCH-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FL-SWITCH
           ELSE
              MOVE 0 TO IND-B03-FL-SWITCH
           END-IF
           IF B03-FL-IAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FL-IAS
           ELSE
              MOVE 0 TO IND-B03-FL-IAS
           END-IF
           IF B03-DIR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DIR
           ELSE
              MOVE 0 TO IND-B03-DIR
           END-IF
           IF B03-TP-COP-CASO-MOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-COP-CASO-MOR
           ELSE
              MOVE 0 TO IND-B03-TP-COP-CASO-MOR
           END-IF
           IF B03-MET-RISC-SPCL-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-MET-RISC-SPCL
           ELSE
              MOVE 0 TO IND-B03-MET-RISC-SPCL
           END-IF
           IF B03-TP-STAT-INVST-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-STAT-INVST
           ELSE
              MOVE 0 TO IND-B03-TP-STAT-INVST
           END-IF
           IF B03-COD-PRDT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COD-PRDT
           ELSE
              MOVE 0 TO IND-B03-COD-PRDT
           END-IF
           IF B03-STAT-ASSTO-1-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-STAT-ASSTO-1
           ELSE
              MOVE 0 TO IND-B03-STAT-ASSTO-1
           END-IF
           IF B03-STAT-ASSTO-2-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-STAT-ASSTO-2
           ELSE
              MOVE 0 TO IND-B03-STAT-ASSTO-2
           END-IF
           IF B03-STAT-ASSTO-3-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-STAT-ASSTO-3
           ELSE
              MOVE 0 TO IND-B03-STAT-ASSTO-3
           END-IF
           IF B03-CPT-ASSTO-INI-MOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CPT-ASSTO-INI-MOR
           ELSE
              MOVE 0 TO IND-B03-CPT-ASSTO-INI-MOR
           END-IF
           IF B03-TS-STAB-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TS-STAB-PRE
           ELSE
              MOVE 0 TO IND-B03-TS-STAB-PRE
           END-IF
           IF B03-DIR-EMIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DIR-EMIS
           ELSE
              MOVE 0 TO IND-B03-DIR-EMIS
           END-IF
           IF B03-DT-INC-ULT-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-DT-INC-ULT-PRE
           ELSE
              MOVE 0 TO IND-B03-DT-INC-ULT-PRE
           END-IF
           IF B03-STAT-TBGC-ASSTO-1-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-STAT-TBGC-ASSTO-1
           ELSE
              MOVE 0 TO IND-B03-STAT-TBGC-ASSTO-1
           END-IF
           IF B03-STAT-TBGC-ASSTO-2-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-STAT-TBGC-ASSTO-2
           ELSE
              MOVE 0 TO IND-B03-STAT-TBGC-ASSTO-2
           END-IF
           IF B03-STAT-TBGC-ASSTO-3-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-STAT-TBGC-ASSTO-3
           ELSE
              MOVE 0 TO IND-B03-STAT-TBGC-ASSTO-3
           END-IF
           IF B03-FRAZ-DECR-CPT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-FRAZ-DECR-CPT
           ELSE
              MOVE 0 TO IND-B03-FRAZ-DECR-CPT
           END-IF
           IF B03-PRE-PP-ULT-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-PRE-PP-ULT
           ELSE
              MOVE 0 TO IND-B03-PRE-PP-ULT
           END-IF
           IF B03-ACQ-EXP-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-ACQ-EXP
           ELSE
              MOVE 0 TO IND-B03-ACQ-EXP
           END-IF
           IF B03-REMUN-ASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-REMUN-ASS
           ELSE
              MOVE 0 TO IND-B03-REMUN-ASS
           END-IF
           IF B03-COMMIS-INTER-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-COMMIS-INTER
           ELSE
              MOVE 0 TO IND-B03-COMMIS-INTER
           END-IF
           IF B03-NUM-FINANZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-NUM-FINANZ
           ELSE
              MOVE 0 TO IND-B03-NUM-FINANZ
           END-IF
           IF B03-TP-ACC-COMM-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-TP-ACC-COMM
           ELSE
              MOVE 0 TO IND-B03-TP-ACC-COMM
           END-IF
           IF B03-IB-ACC-COMM-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-IB-ACC-COMM
           ELSE
              MOVE 0 TO IND-B03-IB-ACC-COMM
           END-IF
           IF B03-CARZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-B03-CARZ
           ELSE
              MOVE 0 TO IND-B03-CARZ
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.

           CONTINUE.
       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.
           CONTINUE.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.
           CONTINUE.
       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.
           CONTINUE.
       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE B03-DT-RIS TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO B03-DT-RIS-DB
           MOVE B03-DT-PRODUZIONE TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO B03-DT-PRODUZIONE-DB
           IF IND-B03-DT-INI-VAL-TAR = 0
               MOVE B03-DT-INI-VAL-TAR TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-INI-VAL-TAR-DB
           END-IF
           MOVE B03-DT-INI-VLDT-PROD TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO B03-DT-INI-VLDT-PROD-DB
           MOVE B03-DT-DECOR-POLI TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO B03-DT-DECOR-POLI-DB
           IF IND-B03-DT-DECOR-ADES = 0
               MOVE B03-DT-DECOR-ADES TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-DECOR-ADES-DB
           END-IF
           MOVE B03-DT-DECOR-TRCH TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO B03-DT-DECOR-TRCH-DB
           MOVE B03-DT-EMIS-POLI TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO B03-DT-EMIS-POLI-DB
           IF IND-B03-DT-EMIS-TRCH = 0
               MOVE B03-DT-EMIS-TRCH TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-EMIS-TRCH-DB
           END-IF
           IF IND-B03-DT-SCAD-TRCH = 0
               MOVE B03-DT-SCAD-TRCH TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-SCAD-TRCH-DB
           END-IF
           IF IND-B03-DT-SCAD-INTMD = 0
               MOVE B03-DT-SCAD-INTMD TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-SCAD-INTMD-DB
           END-IF
           IF IND-B03-DT-SCAD-PAG-PRE = 0
               MOVE B03-DT-SCAD-PAG-PRE TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-SCAD-PAG-PRE-DB
           END-IF
           IF IND-B03-DT-ULT-PRE-PAG = 0
               MOVE B03-DT-ULT-PRE-PAG TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-ULT-PRE-PAG-DB
           END-IF
           IF IND-B03-DT-NASC-1O-ASSTO = 0
               MOVE B03-DT-NASC-1O-ASSTO TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-NASC-1O-ASSTO-DB
           END-IF
           IF IND-B03-DT-EFF-CAMB-STAT = 0
               MOVE B03-DT-EFF-CAMB-STAT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-EFF-CAMB-STAT-DB
           END-IF
           IF IND-B03-DT-EMIS-CAMB-STAT = 0
               MOVE B03-DT-EMIS-CAMB-STAT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-EMIS-CAMB-STAT-DB
           END-IF
           IF IND-B03-DT-EFF-STAB = 0
               MOVE B03-DT-EFF-STAB TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-EFF-STAB-DB
           END-IF
           IF IND-B03-DT-EFF-RIDZ = 0
               MOVE B03-DT-EFF-RIDZ TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-EFF-RIDZ-DB
           END-IF
           IF IND-B03-DT-EMIS-RIDZ = 0
               MOVE B03-DT-EMIS-RIDZ TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-EMIS-RIDZ-DB
           END-IF
           IF IND-B03-DT-ULT-RIVAL = 0
               MOVE B03-DT-ULT-RIVAL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-ULT-RIVAL-DB
           END-IF
           IF IND-B03-DT-QTZ-EMIS = 0
               MOVE B03-DT-QTZ-EMIS TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-QTZ-EMIS-DB
           END-IF
           IF IND-B03-DT-INC-ULT-PRE = 0
               MOVE B03-DT-INC-ULT-PRE TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO B03-DT-INC-ULT-PRE-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE B03-DT-RIS-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO B03-DT-RIS
           MOVE B03-DT-PRODUZIONE-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO B03-DT-PRODUZIONE
           IF IND-B03-DT-INI-VAL-TAR = 0
               MOVE B03-DT-INI-VAL-TAR-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO B03-DT-INI-VAL-TAR
           END-IF
           MOVE B03-DT-INI-VLDT-PROD-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO B03-DT-INI-VLDT-PROD
           MOVE B03-DT-DECOR-POLI-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO B03-DT-DECOR-POLI
           IF IND-B03-DT-DECOR-ADES = 0
               MOVE B03-DT-DECOR-ADES-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO B03-DT-DECOR-ADES
           END-IF
           MOVE B03-DT-DECOR-TRCH-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO B03-DT-DECOR-TRCH
           MOVE B03-DT-EMIS-POLI-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO B03-DT-EMIS-POLI
           IF IND-B03-DT-EMIS-TRCH = 0
               MOVE B03-DT-EMIS-TRCH-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO B03-DT-EMIS-TRCH
           END-IF
           IF IND-B03-DT-SCAD-TRCH = 0
               MOVE B03-DT-SCAD-TRCH-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO B03-DT-SCAD-TRCH
           END-IF
           IF IND-B03-DT-SCAD-INTMD = 0
               MOVE B03-DT-SCAD-INTMD-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO B03-DT-SCAD-INTMD
           END-IF
           IF IND-B03-DT-SCAD-PAG-PRE = 0
               MOVE B03-DT-SCAD-PAG-PRE-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO B03-DT-SCAD-PAG-PRE
           END-IF
           IF IND-B03-DT-ULT-PRE-PAG = 0
               MOVE B03-DT-ULT-PRE-PAG-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO B03-DT-ULT-PRE-PAG
           END-IF
           IF IND-B03-DT-NASC-1O-ASSTO = 0
               MOVE B03-DT-NASC-1O-ASSTO-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO B03-DT-NASC-1O-ASSTO
           END-IF
           IF IND-B03-DT-EFF-CAMB-STAT = 0
               MOVE B03-DT-EFF-CAMB-STAT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO B03-DT-EFF-CAMB-STAT
           END-IF
           IF IND-B03-DT-EMIS-CAMB-STAT = 0
               MOVE B03-DT-EMIS-CAMB-STAT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO B03-DT-EMIS-CAMB-STAT
           END-IF
           IF IND-B03-DT-EFF-STAB = 0
               MOVE B03-DT-EFF-STAB-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO B03-DT-EFF-STAB
           END-IF
           IF IND-B03-DT-EFF-RIDZ = 0
               MOVE B03-DT-EFF-RIDZ-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO B03-DT-EFF-RIDZ
           END-IF
           IF IND-B03-DT-EMIS-RIDZ = 0
               MOVE B03-DT-EMIS-RIDZ-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO B03-DT-EMIS-RIDZ
           END-IF
           IF IND-B03-DT-ULT-RIVAL = 0
               MOVE B03-DT-ULT-RIVAL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO B03-DT-ULT-RIVAL
           END-IF
           IF IND-B03-DT-QTZ-EMIS = 0
               MOVE B03-DT-QTZ-EMIS-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO B03-DT-QTZ-EMIS
           END-IF
           IF IND-B03-DT-INC-ULT-PRE = 0
               MOVE B03-DT-INC-ULT-PRE-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO B03-DT-INC-ULT-PRE
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.


           MOVE LENGTH OF B03-CAUS-SCON
                       TO B03-CAUS-SCON-LEN
           MOVE LENGTH OF B03-EMIT-TIT-OPZ
                       TO B03-EMIT-TIT-OPZ-LEN.

       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

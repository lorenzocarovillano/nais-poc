      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS3250.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2011.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      ***         CALCOLO DELLA VARIABILE FP - Frequenza movimento   ***
      ***------------------------------------------------------------***

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS3250'.
       01  WK-CALL-PGM                      PIC X(008).
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*

      *---------------------------------------------------------------*
      *  COPY TIPOLOGICHE
      *---------------------------------------------------------------*
           COPY LCCVXMV0.

      *---------------------------------------------------------------*
      *  COPY LDBSF970
      *---------------------------------------------------------------*
           COPY LDBVF971.

      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*

       01 AREA-IO-PMO.
          03 DPMO-AREA-PMO.
             04 DPMO-ELE-PMO-MAX        PIC S9(04) COMP.
                COPY LCCVPMOB REPLACING   ==(SF)==  BY ==DPMO==.
             COPY LCCVPMO1              REPLACING ==(SF)== BY ==DPMO==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

           COPY IDBVPMO1.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP VALUE 0.
           03 IX-TAB-PMO                     PIC S9(04) COMP VALUE 0.

      *
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS3250.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003
                                INPUT-LVVS3250.
      *
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.
      *
           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000
      *

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.

           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           INITIALIZE AREA-IO-PMO.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

           MOVE IDSV0003-TIPO-MOVIMENTO      TO WS-MOVIMENTO.

       EX-S0000.
           EXIT.
      *
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           MOVE ZEROES TO IVVC0213-VAL-IMP-O.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL

               PERFORM S1300-CERCA-PARAM-MOVI
                  THRU S1300-EX

              IF  IDSV0003-SUCCESSFUL-RC
              AND IDSV0003-SUCCESSFUL-SQL
                  IF  CALCOLO-RISERVE
                  AND IVVC0213-VAL-IMP-O = ZEROES
                      PERFORM S1310-LEGGI-PMO-CHIUSA
                         THRU S1310-EX
                  END-IF
              END-IF

           END-IF.

       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-PARAM-MOV
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DPMO-AREA-PMO
           END-IF.
      *
       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   CONTROLLO DATI DCLGEN
      *----------------------------------------------------------------*
       S1300-CERCA-PARAM-MOVI.
      *
           IF DPMO-ELE-PMO-MAX > 0
              PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
                UNTIL IX-TAB-PMO > DPMO-ELE-PMO-MAX
                IF DPMO-TP-MOVI(IX-TAB-PMO) = 6101
                OR DPMO-TP-MOVI(IX-TAB-PMO) = 6002
                OR DPMO-TP-MOVI(IX-TAB-PMO) = 6003
                   IF DPMO-FRQ-MOVI-NULL(IX-TAB-PMO)= HIGH-VALUES
                      SET IDSV0003-FIELD-NOT-VALUED   TO TRUE
                   ELSE
                      MOVE DPMO-FRQ-MOVI(IX-TAB-PMO)
                        TO IVVC0213-VAL-IMP-O
                   END-IF
                END-IF
              END-PERFORM
           END-IF.
      *
       S1300-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   LEGGI L'IMMAGINE DELLA PMO CHIUSA DALLA RIDUZIONE O
      *   DALLA SOSPENSIONE PIANO VERSAMENTO
      *----------------------------------------------------------------*
       S1310-LEGGI-PMO-CHIUSA.
      *
           INITIALIZE PARAM-MOVI.

           SET  IDSV0003-TRATT-X-COMPETENZA TO TRUE

      *-->  LIVELLO OPERAZIONE
           SET IDSV0003-WHERE-CONDITION  TO TRUE

      *-->  TIPO OPERAZIONE
           SET IDSV0003-SELECT           TO TRUE

           MOVE IVVC0213-ID-POLIZZA       TO PMO-ID-POLI
           MOVE 6101                      TO LDBVF971-TP-MOVI-1
           MOVE 6002                      TO LDBVF971-TP-MOVI-2
           MOVE 6003                      TO LDBVF971-TP-MOVI-3

           MOVE LDBVF971                 TO IDSV0003-BUFFER-WHERE-COND

           MOVE 'LDBSF970'               TO WK-CALL-PGM

           CALL WK-CALL-PGM  USING  IDSV0003 PARAM-MOVI
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBSF970 ERRORE CHIAMATA '
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
      *
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               IF PMO-FRQ-MOVI-NULL = HIGH-VALUES
                  SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
               ELSE
                  MOVE PMO-FRQ-MOVI        TO  IVVC0213-VAL-IMP-O
               END-IF
           ELSE
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA LDBSF970 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF
           END-IF.
      *
       S1310-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.


      *****************************************************************
      *                                                               *
      *                   IDENTIFICATION DIVISION                     *
      *                                                               *
      *****************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IEAS9700.
       AUTHOR.        ACCENTURE.
       DATE-WRITTEN.  18/07/06.

      *****************************************************************
      *                                                               *
      *    NOME :           IEAS9700                                  *
      *    TIPO :           ROUTINE COBOL-DB2                         *
      *    DESCRIZIONE :    ROUTINE RICERCA GRAVITA ERRORE            *
      *                                                               *
      *    AREE DI PASSAGGIO DATI                                     *
      *                                                               *
      *    DATI DI INPUT : IDSV0001, IEAI9701                         *
      *    DATI DI OUTPUT: IEAO9701                                   *
      *                                                               *
      *****************************************************************
      * LOG MODIFICHE :                                               *
      *                                                               *
      *                                                               *
      *****************************************************************
      *****************************************************************
      *                                                               *
      *                   ENVIRONMENT  DIVISION                       *
      *                                                               *
      *****************************************************************
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
      *SOURCE-COMPUTER. IBM-370 WITH DEBUGGING MODE.
      *OBJECT-COMPUTER. IBM-370.
       SPECIAL-NAMES.
      *    NULL-IND     Carattere usato per indicare campi NULL;
           SYMBOLIC CHARACTERS
              NULL-IND         IS 256
           DECIMAL-POINT IS COMMA.


      *****************************************************************
      *                                                                *
      *                   DATA  DIVISION                              *
      *                                                               *
      *****************************************************************
       DATA DIVISION.

      *****************************************************************
      *                                                               *
      *                   WORKING STORAGE SECTION                     *
      *                                                               *
      *****************************************************************
       WORKING-STORAGE SECTION.

      *****************************************************************
      *                                                               *
      *                   VARIABILI DI COMODO                         *
      *                                                               *
      *****************************************************************
       01 WS-VARIABILI.
          05 WS-PRO-LOG-ERRORE                     PIC S9(5)V  COMP-3.
          05 WS-DESCR-ERRORE                       PIC X(200).
          05 WS-SESSIONE-X.
             10 WS-SESSIONE                        PIC 9(009).
             10 WS-SESSIONE-FILLER                 PIC X(011).
      *****************************************************************
      *                                                               *
      *                   FLAG PER ESITO ELABORAZIONE                 *
      *                                                               *
      *****************************************************************

       01 WS-ESITO                                 PIC X(02).
          88 SI-CORRETTO                           VALUE 'OK'.
          88 NO-CORRETTO                           VALUE 'KO'.

      *---> dichiaro variabile di appoggio per set timestamp
       01 WS-TIMESTAMP                             PIC X(26).
      *---> dichiaro variabile di appoggio per valorizzazione campo di
      *---> tabella log_errore
       01 WN-TIMESTAMP-DCL                         PIC S9(18)V COMP-3.
      *---appoggio per formattazione timestamp------------------------*
       01 WS-TIMESTAMP-DCL-FRM                     PIC X(18).
       01 WN-TIMESTAMP-DCL-FRM REDEFINES WS-TIMESTAMP-DCL-FRM PIC 9(18).
      *---------------------------------------------------------------*
       01 CONTA-TXT                              PIC X(200) VALUE SPACE.
       01 CONTA-VALORE                           PIC 9(3) VALUE ZERO.

      *****************************************************************
      *                                                               *
      *                   INCLUDE PER TEBELLE DB2                     *
      *                                                               *
      *****************************************************************

      *-- DCLGEN PER VARIABILI DI INTERFACCIA DB2
           EXEC SQL
              INCLUDE SQLCA
           END-EXEC.

      *-- DCLGEN TABELLA PER DI LOG PER ERRORI
           EXEC SQL INCLUDE IDBDLOR0 END-EXEC.
           EXEC SQL INCLUDE IDBVLOR1 END-EXEC.
           EXEC SQL INCLUDE IDBVLOR2 END-EXEC.


      *-- COPY DI INPUT DELLA ROUTINE
      *    COPY IEAI9701.
      *
      *-- COPY DI OUTPUT DELLA ROUTINE
      *    COPY IEAO9701.
      *
      *****************************************************************
      *                                                               *
      *                   LINKAGE SECTION                             *
      *                                                               *
      *****************************************************************
       LINKAGE SECTION.

       01 AREA-IDSV0001.
          COPY IDSV0001.

      *-- COPY DI INPUT DELLA ROUTINE
           COPY IEAI9701.

      *-- COPY DI OUTPUT DELLA ROUTINE
           COPY IEAO9701.

      *****************************************************************
      *                                                               *
      *                   PROCEDURE DIVISION                          *
      *                                                               *
      *****************************************************************
      *
       PROCEDURE DIVISION USING   AREA-IDSV0001
                                  IEAI9701-AREA
                                  IEAO9701-AREA.

      *****************************************************************
      *                                                               *
      *                   1000-PRINCIPALE                             *
      *                                                               *
      *****************************************************************
       1000-PRINCIPALE.

      *-- INIZIALIZZO IL FLAG PER L'ESITO DELL'ELABORAZIONE
           SET SI-CORRETTO                         TO TRUE.

      *-- INIZIALIZZO I CAMPI DI WORKING
           MOVE SPACES                             TO WS-VARIABILI.


      *-- CONTROLLO COERENZA DEI DATI IN INPUT
           PERFORM 1100-CHK-FORMALI-INPUT
              THRU 1100-CHK-FORMALI-INPUT-EXIT.

      *-- ESTRAZIONE PROGRESSIVO PER TABELLA LOG_ERRORE
           IF SI-CORRETTO
              PERFORM 1200-ESTRAI-PROGRESSIVO
                 THRU 1200-ESTRAI-PROGRESSIVO-EXIT
           END-IF.

      *-- VALORIZZAZIONE CAMPI DCLGEN
           IF SI-CORRETTO
              PERFORM 1300-VALORIZZAZIONE-CAMPI
                 THRU 1300-VALORIZZAZIONE-CAMPI-EXIT
           END-IF.

      *-- INSERIMENTO DEI DATI IN TABELLA
           IF SI-CORRETTO
              PERFORM 1400-INSERIMENTO-LOG
                 THRU 1400-INSERIMENTO-LOG-EXIT


      *-- VALORIZZAZIONE CAMPI DI OUTPUT
           IF SI-CORRETTO
              PERFORM 1500-VALORIZZA-OUTPUT
                 THRU 1500-VALORIZZA-OUTPUT-EXIT
           END-IF.

       1000-PRINCIPALE-FINE.
           EXIT.
           GOBACK.


       1100-CHK-FORMALI-INPUT.

      *-- CONTROLLO DI NUMERICITA SUL CAMPO DI GRAVITA ERRORE

           IF IEAI9701-ID-GRAVITA-ERRORE IS NOT NUMERIC
              SET NO-CORRETTO                TO TRUE
              MOVE 04                        TO  IEAO9701-COD-ERRORE-990
           END-IF.

      *-- CONTROLLO DI NUMERICITA SUL CAMPO SESSIONE
           IF SI-CORRETTO
              MOVE IDSV0001-SESSIONE         TO WS-SESSIONE-X
              IF WS-SESSIONE IS NOT NUMERIC
                 SET NO-CORRETTO             TO TRUE
                 MOVE 05                     TO IEAO9701-COD-ERRORE-990
              END-IF
           END-IF.

      *-- CONTROLLO CAMPI ALFANUMERICI

           IF SI-CORRETTO
              MOVE IDSV0001-DESC-ERRORE-ESTESA  OF IDSV0001-LOG-ERRORE
                                             TO WS-DESCR-ERRORE
              IF WS-DESCR-ERRORE = SPACES
              OR WS-DESCR-ERRORE = HIGH-VALUE
              OR WS-DESCR-ERRORE = LOW-VALUE
                 SET NO-CORRETTO             TO TRUE
                 MOVE 06                     TO IEAO9701-COD-ERRORE-990
              END-IF
           END-IF.

           IF SI-CORRETTO
              IF IDSV0001-COD-MAIN-BATCH = SPACES
              OR IDSV0001-COD-MAIN-BATCH = HIGH-VALUE
              OR IDSV0001-COD-MAIN-BATCH = LOW-VALUE
                 SET NO-CORRETTO             TO TRUE
                 MOVE 07                     TO IEAO9701-COD-ERRORE-990
              END-IF
           END-IF.

           IF SI-CORRETTO
              IF IDSV0001-COD-SERVIZIO-BE = SPACES
              OR IDSV0001-COD-SERVIZIO-BE = HIGH-VALUE
              OR IDSV0001-COD-SERVIZIO-BE = LOW-VALUE
                 SET NO-CORRETTO             TO TRUE
                 MOVE 08                     TO IEAO9701-COD-ERRORE-990
              END-IF
           END-IF.

           IF SI-CORRETTO
              IF IDSV0001-LABEL-ERR = SPACES
              OR IDSV0001-LABEL-ERR = HIGH-VALUE
              OR IDSV0001-LABEL-ERR = LOW-VALUE
                 SET NO-CORRETTO             TO TRUE
                 MOVE 09                     TO IEAO9701-COD-ERRORE-990
              END-IF
           END-IF.

OTT06      IF    IDSV0001-OPER-TABELLA = HIGH-VALUE
OTT06         OR IDSV0001-OPER-TABELLA = LOW-VALUE
OTT06         MOVE SPACE  TO IDSV0001-OPER-TABELLA.

OTT06      IF    IDSV0001-NOME-TABELLA = HIGH-VALUE
OTT06         OR IDSV0001-NOME-TABELLA = LOW-VALUE
OTT06         MOVE SPACE  TO IDSV0001-NOME-TABELLA.

OTT06      IF    IDSV0001-STATUS-TABELLA = HIGH-VALUE
OTT06         OR IDSV0001-STATUS-TABELLA = LOW-VALUE
OTT06         MOVE SPACE  TO IDSV0001-STATUS-TABELLA.

OTT06      IF    IDSV0001-KEY-TABELLA = HIGH-VALUE
OTT06         OR IDSV0001-KEY-TABELLA = LOW-VALUE
OTT06         MOVE SPACE  TO IDSV0001-KEY-TABELLA.

       1100-CHK-FORMALI-INPUT-EXIT.
           EXIT.



      *-- ESTRAZIONE DELL'ULTIMO PROGRESSIVO DALLA TABELLA DI LOG
       1200-ESTRAI-PROGRESSIVO.

           MOVE WS-SESSIONE                  TO LOR-ID-LOG-ERRORE.
           MOVE ZEROES                       TO WS-PRO-LOG-ERRORE.

           EXEC SQL
                SELECT MAX(PROG_LOG_ERRORE)
                 INTO :WS-PRO-LOG-ERRORE
                 FROM LOG_ERRORE
                 WHERE ID_LOG_ERRORE = :LOR-ID-LOG-ERRORE
           END-EXEC.


      *-- CONTROLLO L'ESITO DELLA SELECT ANALIZZANDO L'SQLCODE
           EVALUATE SQLCODE
              WHEN 0
                 ADD  1                      TO WS-PRO-LOG-ERRORE
                 MOVE ZEROES                 TO IEAO9701-COD-ERRORE-990
              WHEN 100
                 MOVE 1                      TO WS-PRO-LOG-ERRORE
                 MOVE ZEROES                 TO IEAO9701-COD-ERRORE-990
              WHEN -305
                 MOVE 1                      TO WS-PRO-LOG-ERRORE
                 MOVE ZEROES                 TO IEAO9701-COD-ERRORE-990
              WHEN OTHER
                 SET NO-CORRETTO             TO TRUE
                 MOVE 99                     TO IEAO9701-COD-ERRORE-990
           END-EVALUATE.

       1200-ESTRAI-PROGRESSIVO-EXIT.
           EXIT.


      *-- VALORIZZAZIONE CAMPI DELLA DCLGEN PER TABELLA DI LOG
       1300-VALORIZZAZIONE-CAMPI.
           INITIALIZE LOG-ERRORE.

OTT06      MOVE WS-SESSIONE             TO LOR-ID-LOG-ERRORE.
           MOVE WS-PRO-LOG-ERRORE       TO LOR-PROG-LOG-ERRORE.
           MOVE IEAI9701-ID-GRAVITA-ERRORE
                                        TO LOR-ID-GRAVITA-ERRORE.
           MOVE IDSV0001-DESC-ERRORE-ESTESA
                                        TO LOR-DESC-ERRORE-ESTESA

           MOVE IDSV0001-COD-MAIN-BATCH
                                        TO LOR-COD-MAIN-BATCH.
           MOVE IDSV0001-COD-SERVIZIO-BE
                                        TO LOR-COD-SERVIZIO-BE.
           MOVE IDSV0001-LABEL-ERR      TO LOR-LABEL-ERR

OTT06      MOVE 0                       TO IND-LOR-LABEL-ERR

           MOVE IDSV0001-OPER-TABELLA   TO LOR-OPER-TABELLA.
OTT06      MOVE 0                       TO IND-LOR-OPER-TABELLA

           MOVE IDSV0001-NOME-TABELLA   TO LOR-NOME-TABELLA

OTT06      MOVE 0                       TO IND-LOR-NOME-TABELLA

           MOVE IDSV0001-STATUS-TABELLA TO LOR-STATUS-TABELLA

OTT06      MOVE 0                       TO IND-LOR-STATUS-TABELLA

           MOVE IDSV0001-KEY-TABELLA    TO LOR-KEY-TABELLA

OTT06      MOVE 0                       TO IND-LOR-KEY-TABELLA

      *---> INTERCETTO IL TIMESTAMP DI SISTEMA
            PERFORM 1305-CURRENT-TIMESTAMP
               THRU 1305-CURRENT-TIMESTAMP-EXIT.

            MOVE ZEROES                  TO LOR-TIPO-OGGETTO.
OTT06       MOVE 0                       TO IND-LOR-TIPO-OGGETTO.

            MOVE SPACES                  TO LOR-IB-OGGETTO.
OTT06       MOVE 0                       TO IND-LOR-IB-OGGETTO.

       1300-VALORIZZAZIONE-CAMPI-EXIT.
           EXIT.

       1305-CURRENT-TIMESTAMP.

           MOVE ZEROES                   TO WS-TIMESTAMP.

OTT06      MOVE '20'     TO WS-TIMESTAMP(1:2)
OTT06      ACCEPT WS-TIMESTAMP(3:6) FROM DATE.
OTT06      ACCEPT WS-TIMESTAMP(9:6) FROM TIME.

      *---> INIZIALIZZO I CAMPI DI APPOGGIO
           MOVE ZEROES                  TO WN-TIMESTAMP-DCL
                                           WN-TIMESTAMP-DCL-FRM.
      *----> FORMATTO IL TIMESTAM PER IL FORMATO LOG ERRORE CIOE' SENZA
      *----> CARATTERI ALFANUMERICI

OTT06      MOVE  WS-TIMESTAMP(1:14)     TO WS-TIMESTAMP-DCL-FRM(1:14).
OTT06      MOVE  '0000'                 TO WS-TIMESTAMP-DCL-FRM(15:4).

           MOVE  WN-TIMESTAMP-DCL-FRM   TO WN-TIMESTAMP-DCL
           MOVE  WN-TIMESTAMP-DCL       TO LOR-TIMESTAMP-REG.

       1305-CURRENT-TIMESTAMP-EXIT.
           EXIT.

       1400-INSERIMENTO-LOG.

           PERFORM 1401-SET-LENGTH-VCHAR   THRU 1401-EX.


      *-- INSERIMENTO DATI IN TABELLA DI LOG ERRORE

           EXEC SQL
                INSERT
                  INTO LOG_ERRORE
                      ( ID_LOG_ERRORE
                      , PROG_LOG_ERRORE
                      , ID_GRAVITA_ERRORE
                      , DESC_ERRORE_ESTESA
                      , COD_MAIN_BATCH
                      , COD_SERVIZIO_BE
                      , LABEL_ERR
                      , OPER_TABELLA
                      , NOME_TABELLA
                      , STATUS_TABELLA
                      , KEY_TABELLA
                      , TIMESTAMP_REG
                      , TIPO_OGGETTO
                      , IB_OGGETTO)
                VALUES
                     ( :LOR-ID-LOG-ERRORE
                      ,:LOR-PROG-LOG-ERRORE
                      ,:LOR-ID-GRAVITA-ERRORE
                      ,:LOR-DESC-ERRORE-ESTESA-VCHAR
                      ,:LOR-COD-MAIN-BATCH
                      ,:LOR-COD-SERVIZIO-BE
                      ,:LOR-LABEL-ERR
                       :IND-LOR-LABEL-ERR
                      ,:LOR-OPER-TABELLA
                       :IND-LOR-OPER-TABELLA
                      ,:LOR-NOME-TABELLA
                       :IND-LOR-NOME-TABELLA
                      ,:LOR-STATUS-TABELLA
                       :IND-LOR-STATUS-TABELLA
                      ,:LOR-KEY-TABELLA-VCHAR
                       :IND-LOR-KEY-TABELLA
                      ,:LOR-TIMESTAMP-REG
                      ,:LOR-TIPO-OGGETTO
                       :IND-LOR-TIPO-OGGETTO
                      ,:LOR-IB-OGGETTO
                       :IND-LOR-IB-OGGETTO)

           END-EXEC.

      *-- CONTROLLO L'ESITO DELLA INSERT ANALIZZANDO L'SQLCODE

           EVALUATE SQLCODE
              WHEN 0
                  MOVE ZEROES                TO IEAO9701-COD-ERRORE-990
              WHEN OTHER
                 SET NO-CORRETTO             TO TRUE
                 MOVE 99                     TO IEAO9701-COD-ERRORE-990
           END-EVALUATE.
       1400-INSERIMENTO-LOG-EXIT.
           EXIT.

       1401-SET-LENGTH-VCHAR.

           MOVE LENGTH OF LOR-DESC-ERRORE-ESTESA
                          TO LOR-DESC-ERRORE-ESTESA-LEN
           MOVE LENGTH OF LOR-KEY-TABELLA
                          TO LOR-KEY-TABELLA-LEN.

       1401-EX.
           EXIT.

OTT06  1410-COMMIT-LOG.
           EXEC SQL
                COMMIT
           END-EXEC.
       1410-COMMIT-LOG-EXIT.
OTT06      EXIT.


      *-- VALORIZZAZIONE CAMPI COPY DI OUTPUT
       1500-VALORIZZA-OUTPUT.
           MOVE WS-PRO-LOG-ERRORE           TO IEAO9701-PROG-LOG-ERR.
           MOVE WS-SESSIONE                 TO IEAO9701-ID-LOG-ERR.
       1500-VALORIZZA-OUTPUT-EXIT.
           EXIT.


       1600-CONTA-VALORE.
           MOVE 1   TO CONTA-VALORE.
           PERFORM UNTIL CONTA-TXT(CONTA-VALORE:) = SPACES
                     OR CONTA-VALORE > 200
              ADD 1 TO CONTA-VALORE
           END-PERFORM.
       1600-CONTA-VALORE-EXIT.
           EXIT.

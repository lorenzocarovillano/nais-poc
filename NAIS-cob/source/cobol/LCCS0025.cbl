      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA  VER 1.0                          **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LCCS0025.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2008.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *    PROGRAMMA ..... LCCS0025
      *    TIPOLOGIA...... CONTROLLI
      *    PROCESSO....... CROSS
      *    FUNZIONE....... CROSS
      *    DESCRIZIONE.... VERIFICA ESISTENZA POLIZZA
      *    PAGINA WEB..... N/A
      ***------------------------------------------------------------***
      *    DESCRIZIONE SERVIZIO:
      *    IL SERVIZIO RICEVE IN INPUT UNA STRINGA CHE CONTIENE
      *    L'IB POLIZZA.
      *    IN OUTPUT RESTITUISCE UN FLAG CHE INDICA SE L'IB POLIZZA E'
      *    GIA' PRESENTE (O MENO) IN PORTAFOGLIO.
      *
      *    2010 AGGIUNTO IN OUTUPT IN CASO DI POLIZZA PRESENTE
      *    L'ID DELLA POLIZZA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    VARIABILI DI WORKING
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LCCS0025'.
      *----------------------------------------------------------------*
      *    COPY DELLE TABELLE DB2
      *----------------------------------------------------------------*
           COPY IDBVPOL1.
      *----------------------------------------------------------------*
      *    COPY DISPATCHER
      *----------------------------------------------------------------*
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
           COPY IDSI0011.
           COPY IDSO0011.
      *----------------------------------------------------------------*
      *    DEFINIZIONE DI INDICI                                       *
      *----------------------------------------------------------------*
      *01 IX-INDICI.
      *----------------------------------------------------------------*
      *    VARIABILI                                                   *
      *----------------------------------------------------------------*
      *01 WK-VARIABILI.
      *----------------------------------------------------------------*
      *    COPY VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
           COPY IEAV9903.
      *----------------------------------------------------------------*
      *    COPY IDSV0015
      *----------------------------------------------------------------*
           COPY IDSV0015.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
       01   AREA-IDSV0001.
            COPY IDSV0001.
      *----------------------------------------------------------------*
      *     AREE COMUNICAZIONE
      *----------------------------------------------------------------*
      *   AREA COMUNE
       01 WCOM-AREA-STATI.
          COPY LCCC0001                  REPLACING ==(SF)== BY ==WCOM==.
      *   INTERFACCIA DI COMUNICAZIONE (INPUT/OUTPUT)
       01 LCCC0025-AREA-COMUNICAZ.
          COPY LCCC0025.
      *---------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WCOM-AREA-STATI
                                LCCC0025-AREA-COMUNICAZ.
      *---------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *    OPERAZIONI INIZIALI                                         *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

      *--> CONTROLLI FORMALI
           PERFORM S0050-CONTROLLI            THRU EX-S0050.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    CONTROLLI FORMALI                                          *
      *----------------------------------------------------------------*
       S0050-CONTROLLI.

      *--> TIPO IB
           IF LCCS0025-FL-TIPO-IB = HIGH-VALUE OR SPACES
      *       IL CAMPO $ DEVE ESSERE VALORIZZATO
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0050-CONTROLLI'         TO IEAI9901-LABEL-ERR
              MOVE '005007'                  TO IEAI9901-COD-ERRORE
              MOVE 'TIPO IB'                 TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

      *--> IB
           IF IDSV0001-ESITO-OK
              IF LCCS0025-IB = HIGH-VALUE OR SPACES
      *          IL CAMPO $ DEVE ESSERE VALORIZZATO
                 MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S0050-CONTROLLI'      TO IEAI9901-LABEL-ERR
                 MOVE '005007'               TO IEAI9901-COD-ERRORE
                 MOVE 'IB'                   TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

       EX-S0050.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> LETTURA SULLA TABELLA POLI
           PERFORM S1100-LETTURA-POLI
              THRU EX-S1100.

       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    LETTURA TABELLA POLIZZA
      *----------------------------------------------------------------*
       S1100-LETTURA-POLI.

      *--> PREPARA DATI PER CALL DISPATCHER
           PERFORM S1110-PREPARA-AREA-POLI
              THRU EX-S1110.

           PERFORM S1120-CALL-POLI
              THRU EX-S1120.

       EX-S1100.
           EXIT.

      *----------------------------------------------------------------*
      *    PREPARA AREA PER LETTURA POLIZZA
      *----------------------------------------------------------------*
       S1110-PREPARA-AREA-POLI.

           MOVE WS-DT-INFINITO-1-N       TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE WS-TS-INFINITO-1-N       TO IDSI0011-DATA-COMPETENZA
           MOVE ZEROES                   TO IDSI0011-DATA-FINE-EFFETTO

           IF LCCS0025-FL-IB-PROP
              MOVE LCCS0025-IB              TO POL-IB-PROP
              SET IDSI0011-IB-SECONDARIO    TO TRUE
           ELSE
              MOVE LCCS0025-IB              TO POL-IB-OGG
              SET IDSI0011-IB-OGGETTO       TO TRUE
           END-IF

           MOVE 'POLI'                   TO IDSI0011-CODICE-STR-DATO.
           MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE POLI                     TO IDSI0011-BUFFER-DATI.

           SET IDSI0011-TRATT-X-COMPETENZA
            TO TRUE.
           SET IDSI0011-SELECT           TO TRUE.
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

       EX-S1110.
           EXIT.

      *----------------------------------------------------------------*
      *   LETTURA OGGETTO BLOCCO (LDBS2410)
      *----------------------------------------------------------------*
       S1120-CALL-POLI.

      *-->  EFFETTUO LA LETTURA SUL DB
            PERFORM CALL-DISPATCHER
               THRU CALL-DISPATCHER-EX

      *-->  SE IL RETURN CODE E' TRUE
            IF IDSO0011-SUCCESSFUL-RC
               EVALUATE TRUE

                   WHEN IDSO0011-NOT-FOUND
                        SET LCCS0025-FL-PRES-PTF-NO TO TRUE

                   WHEN IDSO0011-SUCCESSFUL-SQL
                        SET LCCS0025-FL-PRES-PTF-SI TO TRUE
                        MOVE IDSO0011-BUFFER-DATI   TO POLI
                        MOVE POL-ID-POLI            TO LCCS0025-ID-POLI

               END-EVALUATE
            ELSE
      *-->     ERRORE DISPATCHER
               MOVE WK-PGM
                 TO IEAI9901-COD-SERVIZIO-BE
               MOVE 'S1120-CALL-POLI'
                 TO IEAI9901-LABEL-ERR
               MOVE '005016'
                 TO IEAI9901-COD-ERRORE
               STRING 'POLI'               ';'
                      IDSO0011-RETURN-CODE ';'
                      IDSO0011-SQLCODE
               DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
               END-STRING
               PERFORM S0300-RICERCA-GRAVITA-ERRORE
                  THRU EX-S0300
            END-IF.

       EX-S1120.
           EXIT.

      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.
      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORE
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.
           COPY IERP9903.

************************************************************************
*********                  BATCH EXECUTOR                              *
*********                  MODALITA' : INTERNA                         *
************************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LOAM0170.
       AUTHOR.        ATS.
       DATE-WRITTEN.  2007.
       DATE-COMPILED.
      *REMARKS.
      *
      * xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      *    PROGRAAMMA .... LOAM0170
      *    FUNZIONE ...... BATCH EXECUTOR
      *                    ADEGUAMENTO PREMIO/PRESTAZIONE
      * xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
      *
           SELECT PBTCEXEC ASSIGN    TO PBTCEXEC
           ACCESS IS SEQUENTIAL
           FILE STATUS  IS FS-PARA.
      *
           SELECT REPORT01 ASSIGN    TO REPORT01
           ACCESS IS SEQUENTIAL
           FILE STATUS  IS FS-REPORT01.
      *
       DATA DIVISION.
       FILE SECTION.
       FD  PBTCEXEC.
       01  PARAM-REC              PIC X(80).
      *
       FD  REPORT01.
       01  REPORT01-REC           PIC X(500).
      *
      *----------------------------------------------------------------*
      * W O R K I N G   S T O R A G E   S E C T I O N *
      *----------------------------------------------------------------*
      *
       WORKING-STORAGE SECTION.
       77  WK-PGM-ELAB                    PIC X(008)  VALUE 'LOAS0110'.
       77  LOAS0350                       PIC X(008)  VALUE 'LOAS0350'.
ID67   77  LOAS1050                       PIC X(008)  VALUE 'LOAS1050'.
       77  WK-PGM                         PIC X(008)  VALUE 'LOAM0170'.
ID67   77  IDBSPOL0                       PIC X(008)  VALUE 'IDBSPOL0'.
       77  WK-FILE-INP                    PIC X(008)  VALUE 'PBTCEXEC'.
       77  WK-PGM-SERV-ROTTURA            PIC X(008)  VALUE '        '.
       77  WK-PGM-SERV-SECON-BUS          PIC X(008)  VALUE 'LOAS0110'.
       77  WK-PGM-SERV-SECON-ROTT         PIC X(008)  VALUE '        '.
      *
PERF   01  WK-GESTIONE-SAVEPOINT         PIC X(001) VALUE 'N'.
PERF       88 WK-GESTIONE-SAVEPOINT-SI   VALUE 'S'.
PERF       88 WK-GESTIONE-SAVEPOINT-NO   VALUE 'N'.
      *----------------------------------------------------------------*
      *   CAMPI COMODO PER GESTIONE ROTTURA CHIAVE
      *----------------------------------------------------------------*
      *
       01 WK-CHIAVE-ROTTURA.
          03 WK-CHIAVE-OLD.
             05 WK-OLD-ID-POL             PIC S9(09) COMP-3.
             05 WK-OLD-ID-ADE             PIC S9(09) COMP-3.
             05 WK-OLD-DT-RICOR           PIC  9(08).
          03 WK-CHIAVE-NEW.
             05 WK-NEW-ID-POL             PIC S9(09) COMP-3.
             05 WK-NEW-ID-ADE             PIC S9(09) COMP-3.
             05 WK-NEW-DT-RICOR           PIC  9(08).
      *
      *----------------------------------------------------------------*
      *   VARIABILI
      *----------------------------------------------------------------*
      *
       01 WK-VARIABILI.
          03 IX-TAB-PMO                   PIC S9(04) COMP VALUE 0.
ID67      03 WK-ID-POLI                   PIC S9(09) COMP-3 VALUE 0.
MIGR      03 WK-ID-ADES                   PIC S9(09) COMP-3 VALUE 0.
          03 WK-DATA-APPO.
             05 WK-DATA-APPO-AA           PIC 9(04).
             05 WK-DATA-APPO-MM           PIC 9(02).
             05 WK-DATA-APPO-GG           PIC 9(02).

      *
      *---------------------------------------------------------------*
      * COPY ELE-MAX
      *---------------------------------------------------------------*
           COPY LCCVPMOZ.
      *
      *----------------------------------------------------------------*
      * -- INTERVALLO DI ELABORAZIONE
      *----------------------------------------------------------------*
      *
       01  WCOM-INTERVALLO-ELAB.
           03 WCOM-PERIODO-ELAB-DA        PIC 9(08).
           03 WCOM-PERIODO-ELAB-A         PIC 9(08).
      *
      ******************************************************************
      * STRUTTURA PER GESTIONE CACHE COMP_STR_DATO / ANAG_DATO
      ******************************************************************

       01 AREA-IDSV0102.
          COPY IDSV0102.

ID67  *----------------------------------------------------------------*
      *    COPY DISPATHCER
      *----------------------------------------------------------------*
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
      *--> COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      *--> COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.
ID67  *

      *----------------------------------------------------------------*
      *  COPY INFRASTRUTTURALE PER BATCH EXECUTOR
      *----------------------------------------------------------------*
      *
           EXEC SQL INCLUDE IABV0007 END-EXEC.
           EXEC SQL INCLUDE IABV0008 END-EXEC.
      *
      *----------------------------------------------------------------*
      * AREA TABELLE DB2
      *----------------------------------------------------------------*
      *
           EXEC SQL INCLUDE SQLCA    END-EXEC.
      *
           EXEC SQL INCLUDE IDBVBBS1 END-EXEC.
           EXEC SQL INCLUDE IDBVBBT1 END-EXEC.
           EXEC SQL INCLUDE IDBVBEM1 END-EXEC.
           EXEC SQL INCLUDE IDBVBES1 END-EXEC.
           EXEC SQL INCLUDE IDBVBJE1 END-EXEC.
           EXEC SQL INCLUDE IDBVBJS1 END-EXEC.
           EXEC SQL INCLUDE IDBVBRS1 END-EXEC.
           EXEC SQL INCLUDE IDBVBTC1 END-EXEC.
           EXEC SQL INCLUDE IDBVPCO1 END-EXEC.
           EXEC SQL INCLUDE IDBVLOR1 END-EXEC.
           EXEC SQL INCLUDE IDBVBPA1 END-EXEC.
           EXEC SQL INCLUDE IDBVGRU1 END-EXEC.
           EXEC SQL INCLUDE IDBVRIC1 END-EXEC.
ID67       EXEC SQL INCLUDE IDBVPOL1 END-EXEC.
MIGR       EXEC SQL INCLUDE IDBVADE1 END-EXEC.
      *
       01  IABV0006.
           COPY IABV0006.
      *
      *----------------------------------------------------------------*
      *    INCLUDE DB2 PORTAFOGLIO
      *----------------------------------------------------------------*
      *
           EXEC SQL INCLUDE IDBVPMO1 END-EXEC.
      *
      *----------------------------------------------------------------*
      * AREA PROFILAZIONE
      *----------------------------------------------------------------*
      *
       01  WCOM-IO-STATI.
           03 WCOM-AREA-STATI.
              COPY LCCC0001              REPLACING ==(SF)== BY ==WCOM==.
           03 WCOM-AREA-LCCC0261.
              COPY LCCC0261              REPLACING ==(SF)== BY ==WCOM==.
      *
      * ---------------------------------------------------------------*
      * STRUTTURA WHERE CONDITION PROVENIENTE DA SCHEDA PARAMETRO      *
      * ---------------------------------------------------------------*
      *
       01  BUFFER-WHERE-CONDITION.
----->     05 BUFFER-WH-COD-RAMO       PIC  X(12).
23072      05 BUFFER-WH-ANNO-ELAB      PIC  X(04).
      *


      * ---------------------------------------------------------------*
      * STRUTTURA WHERE CONDITION PROVENIENTE DA PRENOTAZIONE SU       *
      * DETTAGLIO RICHIESTA                                            *
      * ---------------------------------------------------------------*
      *
       01 WK-AREA-PRENOTAZIONE.
          COPY LOAC0560 REPLACING ==(SF)== BY ==WPRE==.
      *
      *----------------------------------------------------------------*
      *    TIPOLOGICHE DI PRODOTTO                                     *
      *----------------------------------------------------------------*
      *
           COPY ISPV0000.
      *
      *----------------------------------------------------------------*
      * AREE PER AGGIORNAMENTO PARAMETRO COMPAGNIA
      *----------------------------------------------------------------*
      *
       01 SWITCHES.
          05 SW-ESITO-BUSINESS            PIC X(002) VALUE 'SI'.
             88 ESITO-BUSINESS-OK         VALUE 'SI'.
             88 ESITO-BUSINESS-KO         VALUE 'NO'.
          05 WK-AREA-INIZIALIZE           PIC X(02).
             88 WK-AREA-INIZIALIZE-SI     VALUE 'SI'.
             88 WK-AREA-INIZIALIZE-NO     VALUE 'NO'.
      *
      *----------------------------------------------------------------*
      * AREA PER LOAS0350
      *----------------------------------------------------------------*
      *
       01 AREA-LOAS350.
          COPY LOAV0351 REPLACING ==(SF)== BY ==S350==.
      *
ID67  *----------------------------------------------------------------*
      * AREA PER LOAS1050
      *----------------------------------------------------------------*
      *
       01 AREA-LOAS1050.
          COPY LOAV1051 REPLACING ==(SF)== BY ==S1050==.
ID67  *
      *----------------------------------------------------------------*
      * AREA OUTPUT DA DARE IN INPUT AL SERVIZIO SECONDARIO
      *----------------------------------------------------------------*
      *
       01 AREA-MAIN.
      * -- Area parametro movimento
          03 WPMO-AREA-PARAM-MOVI.
             04 WPMO-ELE-PMO-MAX         PIC S9(04) COMP.
                COPY LCCVPMOA REPLACING   ==(SF)==  BY ==WPMO==.
             COPY LCCVPMO1               REPLACING ==(SF)== BY ==WPMO==.
      * -- Area polizza
          03 WPOL-AREA-POLIZZA.
             04 WPOL-ELE-POL-MAX         PIC S9(04) COMP.
             04 WPOL-TAB-POL.
             COPY LCCVPOL1               REPLACING ==(SF)== BY ==WPOL==.
      * -- Area Adesione
          03 WADE-AREA-ADESIONE.
             04 WADE-ELE-ADE-MAX         PIC S9(04) COMP.
             04 WADE-TAB-ADE             OCCURS 1.
             COPY LCCVADE1               REPLACING ==(SF)== BY ==WADE==.
      * --> Area Movimento
          03 WMOV-AREA-MOVIMENTO.
              04 WMOV-ELE-MOVI-MAX        PIC S9(04) COMP.
              04 WMOV-TAB-MOV.
              COPY LCCVMOV1              REPLACING ==(SF)== BY ==WMOV==.
      *
      *----------------------------------------------------------------*
      *    AREA PER MODULO LOAS0110 - EOC COMUNE
      *----------------------------------------------------------------*
      *
       01 WRIC-AREA-RICHIESTA.
          03 WRIC-ELE-RICH-MAX       PIC S9(04) COMP.
          03 WRIC-TAB-RICH.
          COPY LCCVRIC1              REPLACING ==(SF)== BY ==WRIC==.
      *
       01 WGRZ-AREA-GARANZIA.
          04 WGRZ-ELE-GAR-MAX        PIC S9(04) COMP.
                COPY LCCVGRZA REPLACING   ==(SF)==  BY ==WGRZ==.
          COPY LCCVGRZ1              REPLACING ==(SF)== BY ==WGRZ==.
      *
       01 WTGA-AREA-TRANCHE.
          04 WTGA-ELE-TRAN-MAX       PIC S9(04) COMP.
                COPY LCCVTGAA REPLACING   ==(SF)==  BY ==WTGA==.
          COPY LCCVTGA1              REPLACING ==(SF)== BY ==WTGA==.

      *----------------------------------------------------------------*
      *    COPY GESTIONE ERRORI EXTRA
      *----------------------------------------------------------------*
           COPY IEAV9904.
      ******************************************************************
      * STRUTTURA PER GESTIONE CACHE X ACTUATOR
      ******************************************************************

          COPY IJCC0050.

      *
      *----------------------------------------------------------------*
      *                P R O C E D U R E   D I V I S I O N             *
      *----------------------------------------------------------------*
      *
       PROCEDURE DIVISION.
      *
           MOVE 'PROCEDURE DIVISION'
             TO WK-LABEL.
      *
      *
           PERFORM A000-OPERAZ-INIZ
              THRU A000-EX.
      *
           IF WK-ERRORE-NO
      *
              PERFORM B000-ELABORA-MACRO
                 THRU B000-EX
      *
           END-IF.
      *
           PERFORM Z000-OPERAZ-FINALI
              THRU Z000-OPERAZ-FINALI-FINE.
      *
           STOP RUN.
      *
      *----------------------------------------------------------------*
      * OPERAZIONI INIZIALI
      *----------------------------------------------------------------*
      *
       A001-OPERAZIONI-INIZIALI.
      *
           MOVE 'A001-OPERAZ-INIZIALI'
             TO WK-LABEL.

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'LOAM0170'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Iniz. area di cache'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

15473      INITIALIZE ACTCACHE-AREA-ACTUATOR.
           INITIALIZE             ACTCACHE-CONTATORI-CACHE
                                  ACTCACHE-TOT-ELE-CARICATI
                                  ACTCACHE-CONTA-CACHE
                                  IDSV0102-ELE-MAX-COD-STR-DATO
                                  IDSV0102A-ELE-MAX-ACTU
                                  IDSV0102-CONTA-CACHE-CSV
                                  IDSV0102A-CONTA-CACHE-ADA-MVV
                                  IDSV0102-AREA-COD-STR-DATO(1)
                                  IDSV0102A-TAB-PARAM(1).

           MOVE IDSV0102-STR-COD-STR-DATO  TO  IDSV0102-RESTO-TABELLA.
           MOVE IDSV0102A-ADA-MVV          TO  IDSV0102A-RESTO-TABELLA.

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'LOAM0170'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Iniz. area di cache'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
       A001-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  DISPLAY INIZIO ELABORAZIONE
      *----------------------------------------------------------------*
      *
       A101-DISPLAY-INIZIO-ELABORA.
      *
           DISPLAY '***********************************************'.
           DISPLAY '* NOME PROGRAMMA    = ' WK-PGM.
           DISPLAY '* DESCRIZIONE       = Inizio elaborazione di '
           DISPLAY '* BATCH EXECUTOR '.
           DISPLAY '* DATA ELABORAZIONE = ' WK-DATA-SISTEMA.
           DISPLAY '* ORA  ELABORAZIONE = ' WK-ORA-SISTEMA.
           DISPLAY '************************************************'.
           DISPLAY ALL SPACES.
      *
       A101-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * DISPLAY FINE ELABORAZIONE
      *----------------------------------------------------------------*
      *
       A102-DISPLAY-FINE-ELABORA.
      *
           DISPLAY '***********************************************'.
           DISPLAY '* NOME PROGRAMMA    = ' WK-PGM.
           DISPLAY '* DESCRIZIONE       = Fine elaborazione di '
           DISPLAY '* BATCH EXECUTOR '.
           DISPLAY '* DATA ELABORAZIONE = ' WK-DATA-SISTEMA.
           DISPLAY '* ORA  ELABORAZIONE = ' WK-ORA-SISTEMA.
           DISPLAY '************************************************'.
           DISPLAY ALL SPACES.
      *
       A102-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  PRE BUSINESS
      *----------------------------------------------------------------*
      *
       L500-PRE-BUSINESS.
      *
           MOVE 'L500-PRE-BUSINESS'
             TO WK-LABEL.
      *
           IF PRIMA-VOLTA-SUSP-YES
      *
              SET PRIMA-VOLTA-SUSP-NO
                TO TRUE
      *
           END-IF.
      *
           SET GESTIONE-SUSPEND
             TO TRUE.
      *
           IF WK-NEW-ID-ADE NOT = WK-OLD-ID-ADE
      *
      * Rottura Di Contratto
      *
              MOVE WK-NEW-ID-ADE
                TO WK-OLD-ID-ADE
              MOVE WK-NEW-DT-RICOR
                TO WK-OLD-DT-RICOR
              SET WK-AREA-INIZIALIZE-SI
                TO TRUE
      *
      * Valorizzazione Data Effetto Con La Data Ricorrenza Successiva
      *
              MOVE WPMO-DT-RICOR-SUCC(1)
                TO IDSV0001-DATA-EFFETTO
      *
           ELSE
      *
      * Rottura Di Ricorrenza All'interno Dello Stesso Contratto
      *
              IF WK-NEW-DT-RICOR NOT = WK-OLD-DT-RICOR
      *
                 MOVE WK-NEW-DT-RICOR
                   TO WK-OLD-DT-RICOR
                 SET  WK-AREA-INIZIALIZE-SI
                   TO TRUE
                 MOVE WPMO-DT-RICOR-SUCC(1)
                   TO IDSV0001-DATA-EFFETTO
      *
              ELSE
      *
                 IF WK-FINE-ALIMENTAZIONE-YES
      *
                    SET WK-AREA-INIZIALIZE-SI   TO TRUE
                    MOVE WPMO-DT-RICOR-SUCC(1)
                      TO IDSV0001-DATA-EFFETTO
      *
                 ELSE
      *
                    SET WK-LANCIA-BUSINESS-NO
                      TO TRUE
                    SET WK-AREA-INIZIALIZE-NO
                      TO TRUE
      *
                 END-IF
      *
              END-IF
      *
           END-IF.
      *
      *    RICERCA DELL' EVENTUALE AREA CACHE DA UTILIZZARE
      *
           PERFORM VARYING IX-ADDRESS FROM 1 BY 1
                   UNTIL   IX-ADDRESS > 5
                   MOVE IDSV0001-ADDRESS-TYPE(IX-ADDRESS) TO
                        TIPO-ADDRESS
                   EVALUATE TRUE
                   WHEN ADDRESS-CACHE-VAL-VAR
                        SET IDSV0001-ADDRESS        (IX-ADDRESS)
                                             TO ADDRESS OF AREA-IDSV0102
                   WHEN ADDRESS-CACHE-ACTUATOR
                        SET IDSV0001-ADDRESS        (IX-ADDRESS)
                                    TO ADDRESS OF ACTCACHE-AREA-ACTUATOR
                   END-EVALUATE
           END-PERFORM.
      *
       L500-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *   POST BUSINESS
      *----------------------------------------------------------------*
      *
       L700-POST-BUSINESS.
      *
           MOVE 'L700-POST-BUSINESS'    TO WK-LABEL.
      *
           IF IDSV0001-ESITO-OK
      *
              IF WK-AREA-INIZIALIZE-SI
      *
                  PERFORM INIZ-TABELLE-BUS
                     THRU INIZ-TABELLE-BUS-EX
      *
              END-IF
      *
              ADD 1                            TO IX-TAB-PMO
              MOVE IX-TAB-PMO                  TO WPMO-ELE-PMO-MAX
              SET WPMO-ST-INV(IX-TAB-PMO)      TO TRUE
              PERFORM VALORIZZA-OUTPUT-PMO
                 THRU VALORIZZA-OUTPUT-PMO-EX
      *
           END-IF.
      *
       L700-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * INIZIALIZZAZIONE TABELLA PARAM MOVI
      *----------------------------------------------------------------*
      *
       INIZ-TABELLE-BUS.
      *
           PERFORM INIZIA-TOT-PMO
              THRU INIZIA-TOT-PMO-EX
           VARYING IX-TAB-PMO FROM 1 BY 1
             UNTIL IX-TAB-PMO > WK-PMO-MAX-A.
      *
           MOVE ZEROES
             TO IX-TAB-PMO.
           MOVE ZEROES
             TO WPMO-ELE-PMO-MAX.
      *
       INIZ-TABELLE-BUS-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  POST BUSINESS
      *----------------------------------------------------------------*
      *
       L800-ESEGUI-CALL-BUS.
      *
           MOVE 'L800-ESEGUI-CALL-BUS'    TO WK-LABEL.
      *
           IF IABV0006-GUIDE-AD-HOC
              CALL WK-PGM-BUSINESS      USING AREA-IDSV0001
                                              IABV0006
                                              AREA-MAIN
                                              WCOM-INTERVALLO-ELAB
                                              WCOM-IO-STATI
           END-IF.
      *
       L800-EX.
           EXIT.
      *----------------------------------------------------------------*
      * SOSPENDI STATI
      *----------------------------------------------------------------*
       L801-SOSPENDI-STATI.

      *--    adeguare la valorizzazione
      *--    del campo WK-STATI-SOSP-PK(IND-STATI-SOSP) con il campo PK
      *--    della dclgen necessaria al Guide Service

           MOVE PMO-DS-RIGA        TO WK-STATI-SOSP-PK(IND-STATI-SOSP).

       L801-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CARICA STATI SOSPESI
      *----------------------------------------------------------------*
       L802-CARICA-STATI-SOSPESI.

      *--    adeguare la valorizzazione con il campo PK
      *--    della dclgen necessaria al Guide Service
      *--    conil campo WK-STATI-SOSP-PK(IND-STATI-SOSP)

           MOVE WK-STATI-SOSP-PK(IND-STATI-SOSP) TO PMO-DS-RIGA.

       L802-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *    PRE SERVIZIO SECONDARIO DI BUSINESS
      *----------------------------------------------------------------*
      *
       L901-PRE-SERV-SECOND-BUS.
      *
           MOVE 'L901-PRE-SERV-SECOND-BUS'
             TO WK-LABEL.
      *
       L901-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  POST SERVIZIO SECONDARIO DI BUSINESS
      *----------------------------------------------------------------*
      *
       L902-POST-SERV-SECOND-BUS.
      *
           MOVE 'L902-POST-SERV-SECOND-BUS'
             TO WK-LABEL.
      *
       L902-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *      CALL EVENTUALE SERVIZIO SECONDARIO IN FASE DI ESITO KO    *
      *                       DEL SERVIZIO BUSINESS                    *
      *----------------------------------------------------------------*
      *
       L900-CALL-SERV-SECOND-BUS.
      *
           MOVE 'L900-CALL-SERV-SECONDARIO'    TO WK-LABEL.
      *
       L900-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  PRE SERVIZIO SECONDARIO DI ROTTURA
      *----------------------------------------------------------------*
      *
       L951-PRE-SERV-SECOND-ROTT.
      *
           MOVE 'L951-PRE-SERV-SECOND-ROTT'
             TO WK-LABEL.
      *
       L951-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *   POST SERVIZIO SECONDARIO DI ROTTURA
      *----------------------------------------------------------------*
      *
       L952-POST-SERV-SECOND-ROTT.
      *
           MOVE 'L952-POST-SERV-SECOND-ROTT'
             TO WK-LABEL.
      *
       L952-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *       CALL EVENTUALE SERVIZIO SECONDARIO IN FASE DI ESITO KO
      *                      DEL SERVIZIO DI ROTTURA
      *----------------------------------------------------------------*
      *
       L950-CALL-SERV-SECOND-ROTT.
      *
           MOVE 'L950-CALL-SERV-SECOND-ROTT'
             TO WK-LABEL.
      *
       L950-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * PRE GUIDE AD HOC
      *----------------------------------------------------------------*
      *
       N501-PRE-GUIDE-AD-HOC.
      *
           MOVE 'N501-PRE-GUIDE'
             TO WK-LABEL.
      *
      *--> PER CONTO CORRENTE ASSICURATIVO NON ABBIAMO PRENOTAZIONE
      *--> LE INFORMAZIONI CI ARRIVANO DALLA SCHEDA PARAMETRO
      *
           MOVE IDSV0003-BUFFER-WHERE-COND
             TO BUFFER-WHERE-CONDITION.
      *
           MOVE BUFFER-WH-COD-RAMO
             TO ISPV0000-COD-RAMO.
      *
           IF ISPV0000-IN-CC-ASSICURATIVO
      *
      *-->    LA DATA DI FINE PERIODO DI ELABORAZIONE DEVE ESSERE IL
      *-->    31/12 DELL'ANNO CHE CORRISPONDE ALLA DATA DI SCHEDULAZIONE
23072 *       MOVE WK-TIMESTAMP(1:8)            TO WK-DATA-APPO
23072         IF BUFFER-WH-ANNO-ELAB IS NUMERIC
23072            MOVE BUFFER-WH-ANNO-ELAB       TO WK-DATA-APPO-AA
23072         ELSE
23072            MOVE 9999                      TO WK-DATA-APPO-AA
23072         END-IF
      *
      *-->    LA DATA DI FINE PERIODO DI ELABORAZIONE DEVE ESSERE IL
      *-->    31/12 DELL'ANNO
              MOVE 12                           TO WK-DATA-APPO-MM
              MOVE 31                           TO WK-DATA-APPO-GG
              MOVE WK-DATA-APPO                 TO WCOM-PERIODO-ELAB-A
      *
      *-->    LA DATA DI INIZIO PERIODO DI ELABORAZIONE DEVE ESSERE IL
      *-->    01/01 DELL'ANNO
              MOVE 01                           TO WK-DATA-APPO-MM
              MOVE 01                           TO WK-DATA-APPO-GG
              MOVE WK-DATA-APPO                 TO WCOM-PERIODO-ELAB-DA
      *
              SET IDSV0003-WHERE-CONDITION-01   TO TRUE
      *
           ELSE
      *
      *-->    PER L'ELABORAZIONE MASSIVA LE INFORMAZIONI CI ARRIVANO
      *-->    DALLA PRENOTAZIONE
      *
              MOVE IABV0009-BLOB-DATA-REC
                TO WPRE-AREA-TAB-BATCH-PRES(1)
      *
ID67          MOVE ZEROES                     TO WK-ID-POLI
MIGR                                             WK-ID-ADES
      *
ID67          IF WPRE-IB-OGGETTO(1) = SPACES OR HIGH-VALUE OR LOW-VALUE
                 CONTINUE
              ELSE
MIGR           IF WPRE-TP-FRM-ASSVA(1)='CO'
MIGR             MOVE WPRE-IB-OGGETTO(1)      TO ADE-IB-OGG
MIGR             PERFORM A1044-ACCEDI-ADESIONE THRU A1044-EX
MIGR           ELSE
                 MOVE WPRE-IB-OGGETTO(1)      TO POL-IB-OGG
                 PERFORM A1043-ACCEDI-POLIZZA THRU A1043-EX
MIGR           END-IF
              END-IF

              IF IDSV0001-ESITO-OK
MIGR           IF WPRE-TP-FRM-ASSVA(1)='CO'
MIGR             IF WPRE-RAMO(1) = SPACES AND WK-ID-ADES = ZEROES
MIGR                SET IDSV0003-WHERE-CONDITION-05     TO TRUE
MIGR             ELSE
MIGR               IF WPRE-RAMO(1) NOT = SPACES
MIGR                  SET IDSV0003-WHERE-CONDITION-06   TO TRUE
MIGR               ELSE
MIGR                 IF WK-ID-ADES NOT = ZEROES
MIGR                    SET IDSV0003-WHERE-CONDITION-07 TO TRUE
MIGR                 END-IF
MIGR               END-IF
MIGR             END-IF
MIGR           ELSE
                 IF WPRE-RAMO(1) = SPACES AND WK-ID-POLI = ZEROES
                    SET IDSV0003-WHERE-CONDITION-02     TO TRUE
                 ELSE
                   IF WPRE-RAMO(1) NOT = SPACES
                      SET IDSV0003-WHERE-CONDITION-03   TO TRUE
                   ELSE
                     IF WK-ID-POLI NOT = ZEROES
                        SET IDSV0003-WHERE-CONDITION-04 TO TRUE
                     END-IF
                   END-IF
                 END-IF
MIGR           END-IF
              END-IF
ID67       END-IF.
      *
MIGR       IF IDSV0001-ESITO-OK
MIGR        IF IDSV0003-WHERE-CONDITION-04 OR
MIGR           IDSV0003-WHERE-CONDITION-07
MIGR           IF IABV0009-ID-OGG-DA IS NUMERIC AND
MIGR             IABV0009-ID-OGG-DA NOT = ZEROES
MIGR             IF IABV0009-ID-OGG-A  IS NUMERIC AND
MIGR                IABV0009-ID-OGG-A  NOT = ZEROES
MIGR                  INITIALIZE               WK-LOG-ERRORE
MIGR                  MOVE IDSV0003-SQLCODE TO WK-SQLCODE
MIGR                  STRING 'TIPO ELABORAZIONE NON'
MIGR                         ' PARALLELIZZABILE - DISABILITARE I '
MIGR                         ' PARALLELI SULLA BTC_DEFAULT_PARALL'
MIGR                         DELIMITED BY SIZE
MIGR                         INTO
MIGR                         WK-LOR-DESC-ERRORE-ESTESA
MIGR                  END-STRING
MIGR                  SET WK-ERRORE-BLOCCANTE          TO TRUE
MIGR                  MOVE 4           TO WK-LOR-ID-GRAVITA-ERRORE
MIGR                  PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
MIGR             END-IF
MIGR           END-IF
MIGR        END-IF
MIGR       END-IF.

       N501-EX.
           EXIT.
      *
ID67  *----------------------------------------------------------------*
      * LEGGIAMO DALL'ENTITA' POLI L'ID-POLI, DATO L'IB-OGGETTO
      *----------------------------------------------------------------*
       A1043-ACCEDI-POLIZZA.

           MOVE 'A1043-ACCEDI-POLIZZA'    TO WK-LABEL.
           MOVE WK-LABEL                  TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY         THRU ESEGUI-DISPLAY-EX

           INITIALIZE IDSI0011-AREA

      *--> ACCESSO DEFAULT PER EFFETTO E COMPETENZA
           SET IDSI0011-TRATT-DEFAULT     TO TRUE

      *--> LIVELLO OPERAZIONE
           SET IDSI0011-IB-OGGETTO        TO TRUE

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT            TO TRUE

           MOVE 'POLI'                    TO IDSI0011-CODICE-STR-DATO

           MOVE POLI                      TO IDSI0011-BUFFER-DATI.

           SET  IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL   TO TRUE.

           PERFORM CALL-DISPATCHER        THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                       INITIALIZE POLI
                       MOVE IDSO0011-BUFFER-DATI TO POLI
                       MOVE POL-ID-POLI          TO WK-ID-POLI

                  WHEN OTHER
      *--->       ERRORE DI ACCESSO AL DB
                       MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL      TO IEAI9901-LABEL-ERR
                       MOVE '005016'      TO IEAI9901-COD-ERRORE
                       MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                       STRING IDBSPOL0             ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE
                              INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                               THRU EX-S0300
              END-EVALUATE
           ELSE
      *--> GESTIRE ERRORE
              MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL           TO IEAI9901-LABEL-ERR
              MOVE '005016'           TO IEAI9901-COD-ERRORE
              MOVE SPACES TO IEAI9901-PARAMETRI-ERR
              STRING IDBSPOL0             ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                      THRU EX-S0300
           END-IF.

       A1043-EX.
           EXIT.
ID67  *
MIGR  *----------------------------------------------------------------*
      * LEGGIAMO DALL'ENTITA' ADES L'ID-ADES, DATO L'IB-OGGETTO
      *----------------------------------------------------------------*
       A1044-ACCEDI-ADESIONE.

           MOVE 'A1044-ACCEDI-ADESIONE'   TO WK-LABEL.
           MOVE WK-LABEL                  TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY         THRU ESEGUI-DISPLAY-EX

           INITIALIZE IDSI0011-AREA

      *--> ACCESSO DEFAULT PER EFFETTO E COMPETENZA
           SET IDSI0011-TRATT-DEFAULT     TO TRUE

      *--> LIVELLO OPERAZIONE
           SET IDSI0011-IB-OGGETTO        TO TRUE

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT            TO TRUE

           MOVE 'ADES'                    TO IDSI0011-CODICE-STR-DATO

           MOVE ADES                      TO IDSI0011-BUFFER-DATI.

           SET  IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL   TO TRUE.

           PERFORM CALL-DISPATCHER        THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                       INITIALIZE ADES
                       MOVE IDSO0011-BUFFER-DATI TO ADES
                       MOVE ADE-ID-ADES          TO WK-ID-ADES

                  WHEN OTHER
      *--->       ERRORE DI ACCESSO AL DB
                       MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL      TO IEAI9901-LABEL-ERR
                       MOVE '005016'      TO IEAI9901-COD-ERRORE
                       MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                       STRING IDBSPOL0             ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE
                              INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                               THRU EX-S0300
              END-EVALUATE
           ELSE
      *--> GESTIRE ERRORE
              MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL           TO IEAI9901-LABEL-ERR
              MOVE '005016'           TO IEAI9901-COD-ERRORE
              MOVE SPACES TO IEAI9901-PARAMETRI-ERR
              STRING IDBSPOL0             ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                      THRU EX-S0300
           END-IF.

       A1044-EX.
           EXIT.
ID67  *
      *----------------------------------------------------------------*
      *                       CALL GUIDE                               *
      *----------------------------------------------------------------*
      *
       N502-CALL-GUIDE-AD-HOC.
      *
           MOVE 'N502-CALL-GUIDE-AD-HOC' TO WK-LABEL.
      *
           CALL WK-PGM-GUIDA      USING IDSV0003
                                        IABV0002
ID67                                    WK-ID-POLI
MIGR                                    WK-ID-ADES
                                        PARAM-MOVI.
      *
       N502-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *                          POST GUIDE                            *
      *----------------------------------------------------------------*
      *
       N504-POST-GUIDE-AD-HOC.
      *
           MOVE 'N504-POST-GUIDE-AD-HOC'    TO WK-LABEL.
      *
           MOVE PMO-ID-POLI                 TO WK-NEW-ID-POL.
           MOVE PMO-ID-ADES                 TO WK-NEW-ID-ADE.
           MOVE PMO-DT-RICOR-SUCC           TO WK-NEW-DT-RICOR.
           MOVE PMO-DS-RIGA                 TO WK-CURRENT-PK.
      *
           IF PRIMA-VOLTA-YES
      *
              SET PRIMA-VOLTA-NO            TO TRUE
      *
              PERFORM INIZ-FLAG-ROTTURE
                 THRU INIZ-FLAG-ROTTURE-EX
      *
              PERFORM INIZ-TABELLE
                 THRU INIZ-TABELLE-EX
      *
              PERFORM INIZ-AREA-COMUNE
                 THRU INIZ-AREA-COMUNE-EX
      *
           END-IF.
      *
           PERFORM N506-CNTL-ROTTURA-AD-HOC
              THRU N506-EX.
      *
       N504-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * INIZIALIZZAZIONE FLAG ROTTURA
      *----------------------------------------------------------------*
      *
       INIZ-FLAG-ROTTURE.
      *
           MOVE WK-NEW-ID-POL
             TO WK-OLD-ID-POL.
           MOVE WK-NEW-ID-ADE
             TO WK-OLD-ID-ADE.
           MOVE WK-NEW-DT-RICOR
             TO WK-OLD-DT-RICOR.
      *
       INIZ-FLAG-ROTTURE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * INIZIALIZZAZIONE TABELLE
      *----------------------------------------------------------------*
      *
       INIZ-TABELLE.
      *
           PERFORM INIZIA-TOT-MOV
              THRU INIZIA-TOT-MOV-EX
      *
           PERFORM INIZIA-TOT-PMO
              THRU INIZIA-TOT-PMO-EX
           VARYING IX-TAB-PMO FROM 1 BY 1
             UNTIL IX-TAB-PMO > WK-PMO-MAX-A
      *
           MOVE ZEROES
             TO WPMO-ELE-PMO-MAX
                WMOV-ELE-MOVI-MAX.
      *
           MOVE ZEROES
             TO IX-TAB-PMO.
      *
       INIZ-TABELLE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * INIZIALIZZAZIONE AREA COMUNE
      *----------------------------------------------------------------*
      *
       INIZ-AREA-COMUNE.
      *
           INITIALIZE WCOM-IO-STATI.
      *
           MOVE IABV0009-ID-BATCH
             TO WCOM-ID-BATCH.
      *
           MOVE HIGH-VALUES
             TO WCOM-ID-JOB-NULL.
      *
           MOVE IABV0006-COD-LIV-AUT-PROFIL
             TO WCOM-COD-LIV-AUT-PROFIL.
      *
           MOVE IABV0006-COD-GRU-AUT-PROFIL
             TO WCOM-COD-GRU-AUT-PROFIL.
      *
           MOVE IABV0006-CANALE-VENDITA
             TO WCOM-CANALE-VENDITA.
      *
           MOVE IABV0006-PUNTO-VENDITA
             TO WCOM-PUNTO-VENDITA.
      *
           MOVE ZEROES
             TO WCOM-DT-ULT-VERS-PROD
                WCOM-ID-BLOCCO-CRZ
                WCOM-ID-MOVI-CREAZ.
      *
           IF ISPV0000-IN-CC-ASSICURATIVO
      *
              CONTINUE
      *
           ELSE
      *
              MOVE IABV0009-BLOB-DATA-REC
                TO WPRE-AREA-TAB-BATCH-PRES(1)
      *
              MOVE WPRE-DT-ELAB-DA(1)
                TO WCOM-PERIODO-ELAB-DA
              MOVE WPRE-DT-ELAB-A(1)
                TO WCOM-PERIODO-ELAB-A
      *
           END-IF.
      *
           SET WCOM-STEP-SUCC-SI
             TO TRUE.
      *
           SET WCOM-STEP-SUCC-SI
             TO TRUE.
      *
           SET STANDARD-CALL
             TO TRUE.
      *
       INIZ-AREA-COMUNE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *   POST GUIDE
      *----------------------------------------------------------------*
      *
       N506-CNTL-ROTTURA-AD-HOC.
      *
           MOVE 'N506-CNTL-ROTTURA-AD-HOC'    TO WK-LABEL.
      *
           IF WK-NEW-ID-POL NOT = WK-OLD-ID-POL
      *
              MOVE WK-NEW-ID-POL
                TO WK-OLD-ID-POL
              MOVE ZEROES
                TO WCOM-DT-ULT-VERS-PROD
      *
           END-IF.
      *
       N506-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * CONTROLLA ROTTURA DA ALIMENTAZIONE FLUSSO ESTERNO
      *----------------------------------------------------------------*
      *
       E602-CNTL-ROTTURA-EST.
      *
           MOVE 'E602-CNTL-ROTTURA-EST'    TO WK-LABEL.
      *
       E602-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * CALL SERVIZIO ROTTURA
      *----------------------------------------------------------------*
      *
       Q500-CALL-SERV-ROTTURA.
      *
           MOVE 'Q500-CALL-SERV-ROTTURA'    TO WK-LABEL.
      *
       Q500-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  PRE SERVIZIO ROTTURA
      *----------------------------------------------------------------*
      *
       Q501-PRE-SERV-ROTTURA.
      *
           MOVE 'Q501-PRE-SERV-ROTTURA'    TO WK-LABEL.
      *
       Q501-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  POST SERVIZIO ROTTURA
      *----------------------------------------------------------------*
      *
       Q502-POST-SERV-ROTTURA.
      *
           MOVE 'Q502-POST-SERV-ROTTURA'    TO WK-LABEL.
      *
       Q502-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * PREPARA CALL AGGIORNAMENTO MATR_ELAB_BATCH PER CONTO CORRENTE
      * ASSICURATIVO (DA SCHEDA PARAMETRO)
      *----------------------------------------------------------------*
      *
       Z060-PRE-GEST-L71-CCAS.
      *
           MOVE 6006
             TO S1050-TP-MOVI.
           MOVE 'IN'
             TO S1050-FORMA-ASS.
           MOVE BUFFER-WH-COD-RAMO
             TO S1050-COD-RAMO.
           MOVE WCOM-PERIODO-ELAB-A
             TO S1050-DT-ULT-ELABORAZIONE.
      *
       EX-Z060.
           EXIT.
      *
ID67  *----------------------------------------------------------------*
      * PREPARA CALL AGGIORNAMENTO MATR_ELAB_BATCH PER ELABORAZIONE
      * PER RAMO (DA PRENOTAZIONE)
      *----------------------------------------------------------------*
      *
       Z070-PRE-GEST-L71-PRE.
      *
           MOVE WPRE-MOVI-BATCH(1)
             TO S1050-TP-MOVI.
           MOVE WPRE-TP-FRM-ASSVA(1)
             TO S1050-FORMA-ASS.
           MOVE WPRE-RAMO(1)
             TO S1050-COD-RAMO.
           MOVE WPRE-DT-ELAB-A(1)
             TO S1050-DT-ULT-ELABORAZIONE.
      *
       EX-Z070.
           EXIT.
ID67  *
ID67  *----------------------------------------------------------------*
      * AGGIORNAMENTO TABELLA MATR_ELAB_BATCH
      *----------------------------------------------------------------*
      *
       Z080-GEST-L71.
      *
           IF IABV0006-SIMULAZIONE-NO
              CALL LOAS1050          USING AREA-IDSV0001
                                           AREA-LOAS1050
      *
                ON EXCEPTION
      *
                   MOVE WK-PGM
                     TO IEAI9901-COD-SERVIZIO-BE
                   MOVE 'GESTIONE TABELLA MATR_ELAB_BATCH'
                     TO CALL-DESC
                   MOVE 'Z080-GEST-L71'
                     TO IEAI9901-LABEL-ERR
                   PERFORM S0290-ERRORE-DI-SISTEMA
                     THRU EX-S0290
      *
               END-CALL
           END-IF.
      *
       EX-Z080.
           EXIT.
ID67  *
      *----------------------------------------------------------------*
      * PREPARA CALL AGGIORNAMENTO PARAMETRO COMPAGNIA
      *----------------------------------------------------------------*
      *
       Z090-PRE-GEST-PCO.
      *
           MOVE WPRE-DT-ELAB-A(1)
             TO S350-DT-ULT-ELABORAZIONE.
           MOVE WPRE-TP-FRM-ASSVA(1)
             TO S350-FORMA-ASS.
      *
       EX-Z090.
           EXIT.
      *
      *----------------------------------------------------------------*
      * AGGIORNAMENTO TABELLA PARAMETRO COMPAGNIA
      *----------------------------------------------------------------*
      *
       Z100-GEST-PCO.
      *
A3227      IF IABV0006-SIMULAZIONE-NO
              CALL LOAS0350          USING AREA-IDSV0001
                                           AREA-LOAS350
      *
                ON EXCEPTION
      *
                   MOVE WK-PGM
                     TO IEAI9901-COD-SERVIZIO-BE
                   MOVE 'GESTIONE TABELLA PARAMETRO COMPAGNIA'
                     TO CALL-DESC
                   MOVE 'Z100-GEST-PCO'
                     TO IEAI9901-LABEL-ERR
                   PERFORM S0290-ERRORE-DI-SISTEMA
                     THRU EX-S0290
      *
               END-CALL
A3227      END-IF.
      *
       EX-Z100.
           EXIT.
      *
      *----------------------------------------------------------------*
      * OPERAZIONI FINALI
      *----------------------------------------------------------------*
       Z001-OPERAZIONI-FINALI.
      *
           MOVE 'Z001-OPERAZIONI-FINALI'
             TO WK-LABEL.

CACHE *--  DISPLAY PER STATISTICHE SULLA CACHE
CACHE *--
CACHE      DISPLAY '*------------------------------------------------*'
CACHE      DISPLAY '*     Cache Valorizzatore (Tipo Address "A")     *'
CACHE      DISPLAY '*------------------------------------------------*'
CACHE      DISPLAY '* Num. elem. Struttura Dato caricati ..:     '
CACHE               IDSV0102-ELE-MAX-COD-STR-DATO
CACHE      DISPLAY '* Num. elem. Struttura Dato trovati ...:     '
CACHE               IDSV0102-CONTA-CACHE-CSV
CACHE      DISPLAY '* Num. elem. Matr. Val. Var. caricati .:     '
CACHE               IDSV0102A-ELE-MAX-ACTU
CACHE      DISPLAY '* Num. elem. Matr. Val. Var. trovati ..:     '
CACHE               IDSV0102A-CONTA-CACHE-ADA-MVV
CACHE      DISPLAY '*------------------------------------------------*'
CACHE      DISPLAY '*     Cache Actuator      (Tipo Address "B")     *'
CACHE      DISPLAY '*------------------------------------------------*'
CACHE      DISPLAY '* Num. elem. caricati .................: '
CACHE               ACTCACHE-TOT-ELE-CARICATI
CACHE      DISPLAY '* Num. elem. trovati ..................: '
CACHE               ACTCACHE-CONTA-CACHE
CACHE      DISPLAY '*------------------------------------------------*'.
      *
       Z001-EX.
           EXIT.
      *----------------------------------------------------------------*
      * OPERAZIONI FINALI X SINGOLA RICHIESTA DI ELABORAZIONE (ID-BATCH)
      *----------------------------------------------------------------*
       Z002-OPERAZ-FINALI-X-BATCH.
      *
           MOVE 'Z002-OPERAZ-FINALI-X-BATCH'
             TO WK-LABEL.
      *
           IF ISPV0000-IN-CC-ASSICURATIVO
      *
              PERFORM Z060-PRE-GEST-L71-CCAS
                 THRU EX-Z060
              PERFORM Z080-GEST-L71
                 THRU EX-Z080
      *
           ELSE
      *
ID67          IF WPRE-RAMO(1) NOT = SPACES
      *
                 PERFORM Z070-PRE-GEST-L71-PRE
                    THRU EX-Z070
                 PERFORM Z080-GEST-L71
                    THRU EX-Z080
      *
ID67          ELSE
      *
      *-->    SIR 9914 - INIZIO
                  IF WPRE-IB-OGGETTO(1) = SPACES OR HIGH-VALUES OR
                                          LOW-VALUES
      *
                     PERFORM Z090-PRE-GEST-PCO
                        THRU EX-Z090
                     PERFORM Z100-GEST-PCO
                        THRU EX-Z100
      *
                  END-IF
      *-->    SIR 9914 - FINE
      *
ID67          END-IF
      *
           END-IF.
      *
       Z002-EX.
           EXIT.
      *----------------------------------------------------------------*
      * COPY INIZIALIZZAZIONE/VALORIZZAZIONE TABELLE
      *----------------------------------------------------------------*
      *
      * --> PARAMETRO MOVIMENTO
           COPY LCCVPMO3               REPLACING ==(SF)== BY ==WPMO==.
           COPY LCCVPMO4               REPLACING ==(SF)== BY ==WPMO==.
      * --> MOVIMENTO
           COPY LCCVMOV4               REPLACING ==(SF)== BY ==WMOV==.
      *
      *----------------------------------------------------------------*
      *    CONTIENE STATEMENTS PER LA FASE MACRO DEL BATCH EXECUTOR
      *----------------------------------------------------------------*
      *
PERF  *    EXEC SQL INCLUDE IABP0011 END-EXEC.
PERF       EXEC SQL INCLUDE IABPA011 END-EXEC.
      *
      *----------------------------------------------------------------*
      *    CONTIENE STATEMENTS PER LA FASE MICRO DEL BATCH EXECUTOR
      *----------------------------------------------------------------*
      *
           EXEC SQL INCLUDE IABP0012 END-EXEC.
      *
      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORE
      *----------------------------------------------------------------*
      *
           COPY IERP9902.
           COPY IERP9901.
      *
ID67  *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.
ID67  *
      *----------------------------------------------------------------*
      *    CONTIENE STATEMENTS PER LA GESTIONE DEI FILES SEQUENZIALI
      *----------------------------------------------------------------*
      *
           COPY IABPSQG0.
      *

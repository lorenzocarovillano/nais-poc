      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS3190.
       AUTHOR.             ATS.
       DATE-WRITTEN.       DICEMBRE 2014.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS3190
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE.......
      *  DESCRIZIONE.... VALORIZZAZIONE VARIABILE DATAPREAMM
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS3190'.
       01  WK-CALL-PGM                      PIC X(008) VALUE 'LDBSF510'.

      *---------------------------------------------------------------*
      *  COPY DELLE TABELLE DB2
      *---------------------------------------------------------------*
      * --> PARAM-MOVI
           COPY IDBVPOL1.

      *----------------------------------------------------------------*
      * --> AREA POLIZZA INPUT
      *----------------------------------------------------------------*
       01 DPOL-AREA-POLIZZA.
          04 DPOL-ELE-POLI-MAX          PIC S9(004) COMP.
          04 DPOL-TAB-POLI.
          COPY LCCVPOL1                 REPLACING ==(SF)== BY ==DPOL==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS3190.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS3190.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI

           MOVE SPACES                       TO IVVC0213-VAL-STR-O
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE

           MOVE IVVC0213-AREA-VARIABILE      TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE

      *--> RECUPERA PRIMA IMMAGINE DI POLIZZA
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
              PERFORM S1200-LEGGI-POLIZZA
                 THRU S1200-LEGGI-POLIZZA-EX
           END-IF.

       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

            IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
               IVVC0218-ALIAS-POLI
               MOVE IVVC0213-BUFFER-DATI
                   (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                    IVVC0213-LUNGHEZZA(IX-DCLGEN))
                 TO DPOL-AREA-POLIZZA
            END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      * RICERCA PARAM MOVI
      *----------------------------------------------------------------*
       S1200-LEGGI-POLIZZA.

           INITIALIZE POLI.
      *
           MOVE DPOL-ID-POLI              TO POL-ID-POLI

           MOVE POLI                      TO IDSV0003-BUFFER-WHERE-COND

           SET IDSV0003-SELECT            TO TRUE
           SET IDSV0003-WHERE-CONDITION   TO TRUE
           SET IDSV0003-TRATT-X-COMPETENZA TO TRUE

           MOVE 'LDBSF510'                TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 POLI
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBSF510 ERRORE CHIAMATA - LDBSF510'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-RC

              EVALUATE TRUE
                  WHEN IDSV0003-SUCCESSFUL-SQL
                       IF POL-DT-PRESC-NULL NOT = HIGH-VALUE
                                            AND LOW-VALUE AND SPACES
                          MOVE POL-DT-PRESC
                            TO IVVC0213-VAL-STR-O
                       END-IF

                  WHEN IDSV0003-NOT-FOUND
                       SET IDSV0003-FIELD-NOT-VALUED        TO TRUE

                  WHEN OTHER
                       SET IDSV0003-INVALID-OPER  TO TRUE
                       MOVE WK-PGM          TO IDSV0003-COD-SERVIZIO-BE
                       STRING 'ERRORE LETTURA TABELLA LDBSF510 ;'
                              IDSV0003-RETURN-CODE ';'
                              IDSV0003-SQLCODE
                              DELIMITED BY SIZE INTO
                              IDSV0003-DESCRIZ-ERR-DB2
                       END-STRING
              END-EVALUATE
           ELSE
              SET IDSV0003-INVALID-OPER  TO TRUE
              MOVE WK-PGM          TO IDSV0003-COD-SERVIZIO-BE
              STRING 'ERRORE LETTURA TABELLA LDBSF510 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                     DELIMITED BY SIZE INTO
                     IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           END-IF.

       S1200-LEGGI-POLIZZA-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.

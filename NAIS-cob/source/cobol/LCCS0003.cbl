      ******************************************************************
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LCCS0003.
       AUTHOR.             NOMOS SISTEMA.
       DATE-WRITTEN.       2006.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *    PROGRAMMA...... LCCS0003
      *    TIPOLOGIA...... CONTROLLI SULLE DATE
      *    PROCESSO....... XXX
      *    FUNZIONE....... XXX
      *    DESCRIZIONE....
      *     LA  ROUTINE CONVERTE INIZIALMENTE IL FORMATO DATA
      *     FORNITO IN INPUT AL FORMATO GGGGGGG  PROGRESSIVO
      *     SOMMA/SOTTRAE I GIORNI/MESI/ANNI
      *     RICONVERTE IL PROGRESSIVO NEI FORMATI DI OUTPUT
      *
      *
      *     AREADAT E' DESCRITTA NELLA COPY LCCC0003
      *     IN ESSA DEVE ESSERE DEFINITO :
      *
      *    -FUNZ = FUNZIONE DA EFFETTUARE SULLA DATA PASSATA
      *
      *          01  CONVERSIONE TRA IL FORMATO DI INPUT E TUTTI
      *              QUELLI DI OUTPUT
      *          02  SOMMARE IL CAMPO DELTA ALLA DATA DI INPUT
      *          03  SOTTRARRE  DELTA  ALLA DATA DI INPUT
      *          04  CONTROLLO SE LA DATA DI INPUT E' FESTA
      *          05  CONTROLLO SE FINE MESE, DECADALE, QUINDICINA
      *              LE, NR. TRIMESTRE
      *          06  CALCOLO PASQUA PER LA DATA DI INPUT
      *          07  COME FUNZ 01 TRANNE CHE PER OUGG07 E OUFA07
      *              NUMERO GG LAV DEL MESE E ULTIMO GG LAV DEL MESE
      *
      *    -INFO = INDICARE  IL FORMATO DELLA DATA DI INPUT
      *          01 LA DATA E' IN INGMA   FORMATO GGMMAAAA
      *          02 LA DATA E' IN INGBMBA FORMATO GG/MM/AAAA
      *          03 LA DATA E' IN INAMG   FORMATO AAAAMMGG
      *          04 LA DATA E' IN INAMGP  PACKED  AAAAMMGGS
      *          05 LA DATA E' IN INAMG0P PACKED  AAAAMMGG0S
      *          06 LA DATA E' IN INPROG9 PACKED  GGGGGS PROGRESS.
      *                                                 DAL 1900
      *          07 LA DATA E' IN INPROG PACKED GGGGGGGS PROGRESSIVO
      *          08 LA DATA E' IN INJUL  FORMATO AAAAGGG GIULIANO
      *          09 LA DATA E' IN INPROGC PACKED  GGGGGS PROGRESS
      *                          DAL 1900 SECONDO ANNO COMMERCIALE
      *          10 LA DATA E' IN INPROCO PACKED  GGGGGS PROGRESS
      *                                    ANNO COMMERCIALE
      *          11 LA DATA E' IN INGBMBA FORMATO GG.MM.AAAA
      *
      *    -DELTA= INDICARE IL NUMERO DA SOMMARE/SOTTRARRE
      *
      *    -TDELTA = SPACE DELTA CONTIENE GIORNI
      *          M       DELTA CONTIENE MESI
      *          A       DELTA CONTIENE ANNI
      *
      *    -FISLAV= 0 DELTA CONTIENE GIORNI FISSI
      *             1 DELTA CONTIENE GIORNI LAVORATIVI
      *
      *    -INICON= INIZIO CONTEGGIO PER SOMMA E SOTTRAZIONE
      *             0 INIZIO DALLO STESSO GIORNO
      *             1 INIZIO DAL PRIMO GIORNO LAVORATIVO
      *             2 INIZIO DAL GIORNO SOLO SE FESTIVO
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      ***------------------------------------------------------------***


       01  WA-INIPROG                  PIC S9(7)  COMP-3 VALUE +693595.
      *--------------

       01  WA-GG-IN-400-ANNI           PIC S9(7)  COMP-3 VALUE +146097.
      *---------------------

       01  WA-GG-IN-100-ANNI           PIC S9(5)  COMP-3 VALUE +36524.
      *---------------------

       01  WA-GG-IN-4-ANNI             PIC S9(5)  COMP-3 VALUE +1461.
      *-------------------

       01  WA-TABMESI                  PIC X(24)
      *--------------
           VALUE  '312831303130313130313031'.

       01  WA-TABDMESI.
      *----------------
           05  FILLER                  PIC X(9)   VALUE 'GENNAIO  '.
           05  FILLER                  PIC X(9)   VALUE 'FEBBRAIO '.
           05  FILLER                  PIC X(9)   VALUE 'MARZO    '.
           05  FILLER                  PIC X(9)   VALUE 'APRILE   '.
           05  FILLER                  PIC X(9)   VALUE 'MAGGIO   '.
           05  FILLER                  PIC X(9)   VALUE 'GIUGNO   '.
           05  FILLER                  PIC X(9)   VALUE 'LUGLIO   '.
           05  FILLER                  PIC X(9)   VALUE 'AGOSTO   '.
           05  FILLER                  PIC X(9)   VALUE 'SETTEMBRE'.
           05  FILLER                  PIC X(9)   VALUE 'OTTOBRE  '.
           05  FILLER                  PIC X(9)   VALUE 'NOVEMBRE '.
           05  FILLER                  PIC X(9)   VALUE 'DICEMBRE '.

       01  FILLER  REDEFINES  WA-TABDMESI.
           05  WA-DMESE                PIC X(9)   OCCURS 12.

       01  WA-TABDGIORNI.
      *------------------
           05  FILLER                  PIC X(9)   VALUE 'DOMENICA '.
           05  FILLER                  PIC X(9)   VALUE 'LUNEDI   '.
           05  FILLER                  PIC X(9)   VALUE 'MARTEDI  '.
           05  FILLER                  PIC X(9)   VALUE 'MERCOLEDI'.
           05  FILLER                  PIC X(9)   VALUE 'GIOVEDI  '.
           05  FILLER                  PIC X(9)   VALUE 'VENERDI  '.
           05  FILLER                  PIC X(9)   VALUE 'SABATO   '.

       01  FILLER  REDEFINES  WA-TABDGIORNI.
           05  WA-DGIORNO              PIC X(9)   OCCURS 7.

       01  WS-DATA-X.
      *--------------
           05  WS-DATA                 PIC 9(8).
           05  FILLER  REDEFINES  WS-DATA.
             10  WS-DATA-GG            PIC 9(2).
             10  WS-DATA-MM            PIC 9(2).
             10  WS-DATA-SS            PIC 9(2).
             10  WS-DATA-AA            PIC 9(2).
           05  FILLER  REDEFINES  WS-DATA.
             10  FILLER                PIC 9(4).
             10  WS-DATA-SSAA          PIC 9(4).

       01  WS-DATA-NUM                 PIC 9(8).
      *---------------
       01  FILLER  REDEFINES  WS-DATA-NUM.
           05  WS-DATA-NUM-AA          PIC 9(4).
           05  WS-DATA-NUM-MMGG.
             10  WS-DATA-NUM-MM        PIC 9(2).
             10  WS-DATA-NUM-GG        PIC 9(2).
       01  FILLER  REDEFINES  WS-DATA-NUM.
           05  WS-DATA-NUM-AAMM        PIC 9(6).
           05  FILLER                  PIC 9(2).

       01  WS-DATAP                    PIC S9(7)  COMP-3.
      *------------

       01  WS-ANY-NUM                  PIC S9(7)  COMP-3.
      *--------------

       01  WS-DATA-PASQUETTA           PIC S9(7)  COMP-3.
      *---------------------

       01  WS-DATA-WK                  PIC S9(7)  COMP-3.
      *--------------

       01  WS-DELTA                    PIC 9(3).
      *------------
       01  WS-DATA-MM3                 PIC 9(3) VALUE 0.
      *------------

       01  WS-GMLAV                    PIC 9(2).
      *------------

       01  WS-FMLAV                    PIC 9(2).
      *------------

       01  FILLER.
      *-----------
           05  WS-QUOTIENT             PIC S9(7)  COMP-3.
           05  WS-REMAINDER            PIC S9(7)  COMP-3.

       01  FILLER.
      *-----------
           05  WS-PROD                 PIC S9(5)  COMP-3.
           05  WS-COMO                 PIC S9(5)  COMP-3.
           05  WS-RESTOA               PIC 9(2).
           05  WS-DELTB                PIC 9(2).
           05  WS-DELTAA               PIC 9(2).

       01  WS-MESI.
      *------------
           05  WS-MESE                 PIC 9(2)   OCCURS 12.

       01  WS-MESI-WK                  PIC 9(2).
      *--------------

       01  I2                          PIC 9(2).
      *------

       01  WS-RCODE                    PIC 9(2).
      *------------

       01  WA-TAFESTEX.
      *----------------
           05  FILLER                  PIC X(4)   VALUE '0101'.
           05  FILLER                  PIC X(4)   VALUE '0106'.
           05  FILLER                  PIC X(4)   VALUE '0425'.
           05  FILLER                  PIC X(4)   VALUE '0501'.
           05  FILLER                  PIC X(4)   VALUE '0602'.
           05  FILLER                  PIC X(4)   VALUE '0815'.
           05  FILLER                  PIC X(4)   VALUE '1101'.
           05  FILLER                  PIC X(4)   VALUE '1208'.
           05  FILLER                  PIC X(4)   VALUE '1225'.
           05  FILLER                  PIC X(4)   VALUE '1226'.
           05  FILLER                  PIC X(4)   VALUE HIGH-VALUE.
           05  FILLER                  PIC X(4)   VALUE HIGH-VALUE.
           05  FILLER                  PIC X(4)   VALUE HIGH-VALUE.
           05  FILLER                  PIC X(4)   VALUE HIGH-VALUE.
           05  FILLER                  PIC X(4)   VALUE HIGH-VALUE.
           05  FILLER                  PIC X(4)   VALUE HIGH-VALUE.
           05  FILLER                  PIC X(4)   VALUE HIGH-VALUE.
           05  FILLER                  PIC X(4)   VALUE HIGH-VALUE.
           05  FILLER                  PIC X(4)   VALUE HIGH-VALUE.
           05  FILLER                  PIC X(4)   VALUE HIGH-VALUE.

       01  FILLER  REDEFINES  WA-TAFESTEX.
           05  WA-FESTEX               PIC X(4)   OCCURS 20.

       01  WA-MAX-FESTEX               PIC 9(2)   VALUE 20.
      *-----------------

      *----------------------------------------------------------------

       LINKAGE SECTION.
      *----------------

           COPY LCCC0003.

       01  IN-RCODE                    PIC 9(2).
      *------------

      *----------------------------------------------------------------

       PROCEDURE DIVISION
      *------------------
                                 USING IO-A2K-LCCC0003
                                       IN-RCODE.

       A-MAIN SECTION.
      *---------------

      *>>  OTTIENI IL NUMERO DI PARAMETRI DI CHIAMATA - IN RETURN-CODE
      *>>  POSSIBILE 1 O 2 PARAMETRI
      *>>  SE NESSUN PARAMETRO, RETURN-CODE = 99

           MOVE SPACES TO A2K-OUTPUT
           MOVE ZERO   TO A2K-OUAMGP
                          A2K-OUAMG0P
                          A2K-OUPROG9
                          A2K-OUPROGC
                          A2K-OUPROG
                          A2K-OUPROCO
           MOVE ZERO   TO WS-RCODE

           PERFORM B-VALIDATE-INPUT

           IF  WS-RCODE NOT = ZERO
               GO TO A-999-EXIT
           END-IF

           IF  A2K-FUNZ = '02'
               PERFORM C-ADD-DELTA
           END-IF

           IF  A2K-FUNZ = '03'
               PERFORM D-SUBTRACT-DELTA
           END-IF

           IF  WS-RCODE NOT = ZERO
               GO TO A-999-EXIT
           END-IF

           PERFORM E-PROCESS-CONVERSIONI.

       A-999-EXIT.
      *-----------


           MOVE WS-RCODE TO IN-RCODE

           MOVE ZERO TO RETURN-CODE
           GOBACK.

      *----------------------------------------------------------------

       B-VALIDATE-INPUT SECTION.
      *-------------------------

           IF  A2K-FUNZ LESS    '01'
           OR  A2K-FUNZ GREATER '07'
               MOVE '84' TO WS-RCODE
           END-IF

           IF  A2K-INFO LESS    '01'
           OR  A2K-INFO GREATER '11'
               MOVE '85' TO WS-RCODE
           END-IF

           EVALUATE A2K-INFO
               WHEN '01'
                    PERFORM  B01-VALIDATE-INFO-01
               WHEN '02'
                    PERFORM  B02-VALIDATE-INFO-02
               WHEN '03'
                    PERFORM  B03-VALIDATE-INFO-03
               WHEN '04'
                    PERFORM  B04-VALIDATE-INFO-04
               WHEN '05'
                    PERFORM  B05-VALIDATE-INFO-05
               WHEN '06'
                    PERFORM  B06-VALIDATE-INFO-06
               WHEN '07'
                    PERFORM  B07-VALIDATE-INFO-07
               WHEN '08'
                    PERFORM  B08-VALIDATE-INFO-08
               WHEN '09'
                    PERFORM  B09-VALIDATE-INFO-09
               WHEN '10'
                    PERFORM  B10-VALIDATE-INFO-10
               WHEN '11'
                    PERFORM  B02-VALIDATE-INFO-02
           END-EVALUATE.

       B-999-EXIT.
      *-----------
           EXIT.

      *----------------------------------------------------------------

       B01-VALIDATE-INFO-01 SECTION.
      *-----------------------------

      *    FORMATO GGMMAAAA IN A2K-INGMA

           IF  A2K-INGMA-X NOT NUMERIC
               MOVE '93' TO WS-RCODE
               GO TO B01-999-EXIT
           END-IF

           IF  A2K-INSS = ZERO
           AND A2K-INAA = ZERO
               MOVE '93' TO WS-RCODE
               GO TO B01-999-EXIT
           END-IF

           MOVE A2K-INGMA TO WS-DATA
           PERFORM BA-VALIDATE-MMGG.

       B01-999-EXIT.
      *-------------
           EXIT.

      *----------------------------------------------------------------

       B02-VALIDATE-INFO-02 SECTION.
      *-----------------------------

      *    FORMATO GG/MM/AAAA IN A2K-INGBMBA
      *            GG.MM.AAAA

           IF  A2K-INSS02 = ZERO
           AND A2K-INAA02 = ZERO
               MOVE '95' TO WS-RCODE
               GO TO B02-999-EXIT
           END-IF

           MOVE A2K-INGG02 TO WS-DATA-GG
           MOVE A2K-INMM02 TO WS-DATA-MM
           MOVE A2K-INSS02 TO WS-DATA-SS
           MOVE A2K-INAA02 TO WS-DATA-AA
           PERFORM BA-VALIDATE-MMGG.

       B02-999-EXIT.
      *-------------
           EXIT.

      *----------------------------------------------------------------

       B03-VALIDATE-INFO-03 SECTION.
      *-----------------------------

      *    FORMATO AAAAMMGG IN A2K-INAMG

           IF  A2K-INSS03 = ZERO
           AND A2K-INAA03 = ZERO
               MOVE '96' TO WS-RCODE
               GO TO B03-999-EXIT
           END-IF

           MOVE A2K-INGG03 TO WS-DATA-GG
           MOVE A2K-INMM03 TO WS-DATA-MM
           MOVE A2K-INSS03 TO WS-DATA-SS
           MOVE A2K-INAA03 TO WS-DATA-AA
           PERFORM BA-VALIDATE-MMGG.

       B03-999-EXIT.
      *-------------
           EXIT.

      *----------------------------------------------------------------

       B04-VALIDATE-INFO-04 SECTION.
      *-----------------------------

      *    FORMATO AAAAMMGGS IN A2K-INAMGP

           IF  A2K-INDATA (1:5) = SPACES
           OR  A2K-INAMGP NOT NUMERIC
           OR  A2K-INAMGP = ZERO
               MOVE '97' TO WS-RCODE
               GO TO B04-999-EXIT
           END-IF

           MOVE A2K-INAMGP TO WS-DATA-NUM
           MOVE WS-DATA-NUM (1:2) TO WS-DATA-SS
           MOVE WS-DATA-NUM (3:2) TO WS-DATA-AA
           MOVE WS-DATA-NUM (5:2) TO WS-DATA-MM
           MOVE WS-DATA-NUM (7:2) TO WS-DATA-GG
           PERFORM BA-VALIDATE-MMGG
           IF  WS-RCODE = ZERO
               MOVE A2K-INAMGP TO A2K-OUAMG
           END-IF.

       B04-999-EXIT.
      *-------------
           EXIT.

      *----------------------------------------------------------------

       B05-VALIDATE-INFO-05 SECTION.
      *-----------------------------

      *    FORMATO AAAAMMGG0S IN A2K-INAMG0P

           IF  A2K-INDATA (1:5) = SPACES
           OR  A2K-INAMG0P NOT NUMERIC
           OR  A2K-INAMG0P = ZERO
               MOVE '98' TO WS-RCODE
               GO TO B05-999-EXIT
           END-IF

           DIVIDE 10 INTO A2K-INAMG0P GIVING WS-DATA-NUM
           MOVE WS-DATA-NUM (1:2) TO WS-DATA-SS
           MOVE WS-DATA-NUM (3:2) TO WS-DATA-AA
           MOVE WS-DATA-NUM (5:2) TO WS-DATA-MM
           MOVE WS-DATA-NUM (7:2) TO WS-DATA-GG
           PERFORM BA-VALIDATE-MMGG.

       B05-999-EXIT.
      *-------------
           EXIT.

      *----------------------------------------------------------------

       B06-VALIDATE-INFO-06 SECTION.
      *-----------------------------

      *    FORMATO GGGGGS IN A2K-INPROG9
      *             PROGRESSIVO DAL 1900

           IF  A2K-INDATA (1:3) = SPACES
           OR  A2K-INPROG9 NOT NUMERIC
           OR  A2K-INPROG9 = ZERO
               MOVE '92' TO WS-RCODE
               GO TO B06-999-EXIT
           END-IF

           MOVE A2K-INPROG9 TO A2K-OUPROG
           ADD  WA-INIPROG  TO A2K-OUPROG.

       B06-999-EXIT.
      *-------------
           EXIT.

      *----------------------------------------------------------------

       B07-VALIDATE-INFO-07 SECTION.
      *-----------------------------

      *    FORMATO GGGGGGGS IN A2K-INPROG
      *                       PROGRESSIVO

           IF  A2K-INDATA (1:4) = SPACES
           OR  A2K-INPROG NOT NUMERIC
           OR  A2K-INPROG = ZERO
               MOVE '91' TO WS-RCODE
               GO TO B07-999-EXIT
           END-IF

           MOVE A2K-INPROG TO A2K-OUPROG.

       B07-999-EXIT.
      *-------------
           EXIT.

      *----------------------------------------------------------------

       B08-VALIDATE-INFO-08 SECTION.
      *-----------------------------

      *    FORMATO AAAAGGG IN A2K-INJUL
      *                   DATA GIULIANA

           IF  A2K-INJUL-X = SPACES
           OR  A2K-INJUL NOT NUMERIC
           OR  A2K-INJUL = ZERO
               MOVE '99' TO WS-RCODE
               GO TO B08-999-EXIT
           END-IF

           IF  A2K-INJGGG GREATER 366
               MOVE '99' TO WS-RCODE
               GO TO B08-999-EXIT
           END-IF

           MOVE ZERO TO WS-DATA
           MOVE A2K-INJSS TO WS-DATA-SS
           MOVE A2K-INJAA TO WS-DATA-AA
           MOVE WS-DATA   TO WS-DATAP
           PERFORM N-CHECK-BISESTIL

           IF  A2K-INJGGG = 366
           AND WS-MESE (2) NOT = 29
               MOVE '99' TO WS-RCODE
               GO TO B08-999-EXIT
           END-IF

           SUBTRACT 1 FROM WS-DATAP
           PERFORM O-COMPUTE-CAL3112

           ADD A2K-INJGGG TO WS-DATAP
           MOVE WS-DATAP TO A2K-OUPROG.

       B08-999-EXIT.
      *-------------
           EXIT.

      *----------------------------------------------------------------

       B09-VALIDATE-INFO-09 SECTION.
      *-----------------------------

      *    FORMATO GGGGGS IN A2K-INPROGC
      *            PROGR. COMM. DAL 1900

           IF  A2K-INDATA (1:3) = SPACES
           OR  A2K-INPROGC NOT NUMERIC
           OR  A2K-INPROGC = ZERO
               MOVE '90' TO WS-RCODE
               GO TO B09-999-EXIT
           END-IF

           MOVE A2K-INPROGC TO WS-DATAP
           DIVIDE 360 INTO WS-DATAP
                    GIVING WS-QUOTIENT
                 REMAINDER WS-REMAINDER
           END-DIVIDE

           IF  WS-REMAINDER = ZERO
               ADD 1900 WS-QUOTIENT GIVING WS-DATA-SSAA
               MOVE 31 TO WS-DATA-GG
               MOVE 12 TO WS-DATA-MM
               PERFORM BA-VALIDATE-MMGG
               GO TO B09-999-EXIT
           END-IF

           ADD 1901 WS-QUOTIENT GIVING WS-DATA-SSAA

           MOVE WS-REMAINDER TO WS-DATA-WK
           DIVIDE 30  INTO WS-DATA-WK
                    GIVING WS-QUOTIENT
                 REMAINDER WS-REMAINDER
           END-DIVIDE

           IF  WS-REMAINDER = ZERO
               MOVE WS-QUOTIENT TO WS-DATA-MM
               MOVE 99 TO WS-DATA-GG
           ELSE
               ADD 1 WS-QUOTIENT GIVING WS-DATA-MM
               MOVE WS-REMAINDER TO WS-DATA-GG
           END-IF

           MOVE WS-DATA-SSAA TO WS-DATAP
           PERFORM N-CHECK-BISESTIL

           MOVE WS-DATA-MM TO I2
           IF  WS-DATA-GG GREATER WS-MESE (I2)
               MOVE WS-MESE (I2) TO WS-DATA-GG
           END-IF.

           PERFORM BA-VALIDATE-MMGG.

       B09-999-EXIT.
      *-------------
           EXIT.

      *----------------------------------------------------------------

       B10-VALIDATE-INFO-10 SECTION.
      *-----------------------------

      *    FORMATO GGGGGGGS IN A2K-INPROGO
      *            PROGRESSIVO COMMERCIALE

           IF  A2K-INDATA (1:4) = SPACES
           OR  A2K-INPROCO NOT NUMERIC
           OR  A2K-INPROCO = ZERO
               MOVE '86' TO WS-RCODE
               GO TO B09-999-EXIT
           END-IF

           MOVE A2K-INPROCO TO A2K-OUPROCO

           MOVE A2K-INPROCO TO WS-DATAP
           DIVIDE 360 INTO WS-DATAP
                    GIVING WS-QUOTIENT
                 REMAINDER WS-REMAINDER
           END-DIVIDE

           IF  WS-REMAINDER = ZERO
               MOVE WS-QUOTIENT TO WS-DATA-SSAA
               MOVE 31 TO WS-DATA-GG
               MOVE 12 TO WS-DATA-MM
               PERFORM BA-VALIDATE-MMGG
               GO TO B10-999-EXIT
           END-IF

           ADD 1 WS-QUOTIENT GIVING WS-DATA-SSAA

           MOVE WS-REMAINDER TO WS-DATA-WK
           DIVIDE 30  INTO WS-DATA-WK
                    GIVING WS-QUOTIENT
                 REMAINDER WS-REMAINDER
           END-DIVIDE

           IF  WS-REMAINDER = ZERO
               MOVE WS-QUOTIENT TO WS-DATA-MM
               MOVE 99 TO WS-DATA-GG
           ELSE
               ADD 1 WS-QUOTIENT GIVING WS-DATA-MM
               MOVE WS-REMAINDER TO WS-DATA-GG
           END-IF

           MOVE WS-DATA-SSAA TO WS-DATAP
           PERFORM N-CHECK-BISESTIL

           MOVE WS-DATA-MM TO I2
           IF  WS-DATA-GG GREATER WS-MESE (I2)
               MOVE WS-MESE (I2) TO WS-DATA-GG
           END-IF.

           PERFORM BA-VALIDATE-MMGG.

       B10-999-EXIT.
      *-------------
           EXIT.

      *----------------------------------------------------------------

       BA-VALIDATE-MMGG SECTION.
      *-------------------------

           IF  WS-DATA-MM LESS    '01'
           OR  WS-DATA-MM GREATER '12'
               MOVE '94' TO WS-RCODE
               GO TO BA-999-EXIT
           END-IF

           IF  WS-DATA-GG LESS    '01'
           OR  WS-DATA-GG GREATER '31'
               MOVE '80' TO WS-RCODE
               GO TO BA-999-EXIT
           END-IF

           PERFORM L-CONVERT-CONVDG
           IF  WS-RCODE = ZERO
               MOVE WS-DATAP TO A2K-OUPROG
           END-IF.

       BA-999-EXIT.
      *------------
           EXIT.

      *----------------------------------------------------------------

       C-ADD-DELTA SECTION.
      *--------------------

      *    SOMMA DELTA

           IF  A2K-DELTA-X = SPACES
               GO TO C-999-EXIT
           END-IF

           IF  A2K-TDELTA = ' ' OR 'G'
               PERFORM CA-ADD-GIORNI
               GO TO C-999-EXIT
           END-IF

           IF  A2K-TDELTA NOT = 'M'
           AND A2K-TDELTA NOT = 'A'
               MOVE '81' TO WS-RCODE
               GO TO C-999-EXIT
           END-IF

           PERFORM M-CONVERT-CONVGE
           IF  WS-RCODE NOT = ZERO
               GO TO C-999-EXIT
           END-IF

           IF  A2K-TDELTA = 'M'
               ADD A2K-DELTA WS-DATA-MM  GIVING WS-DATA-MM3
               PERFORM UNTIL WS-DATA-MM3 NOT GREATER 12
                   SUBTRACT 12 FROM WS-DATA-MM3
                   ADD 1 TO WS-DATA-SSAA
               END-PERFORM
               MOVE WS-DATA-MM3          TO WS-DATA-MM
           ELSE
               ADD A2K-DELTA TO WS-DATA-SSAA
           END-IF

           MOVE WS-DATA-SSAA TO WS-DATAP
           PERFORM N-CHECK-BISESTIL

           MOVE WS-DATA-MM TO I2
           IF  WS-DATA-GG GREATER WS-MESE (I2)
               MOVE WS-MESE (I2) TO WS-DATA-GG
           END-IF.

           MOVE WS-DATA TO A2K-OUGMA

           PERFORM L-CONVERT-CONVDG
           IF  WS-RCODE = ZERO
               MOVE WS-DATAP TO A2K-OUPROG
           END-IF.

       C-999-EXIT.
      *-----------
           EXIT.

      *----------------------------------------------------------------

       CA-ADD-GIORNI SECTION.
      *----------------------

      *>>  INIZIA DAL PRIMO GIORNO LAVORATIVO
           IF  A2K-INICON = '1'
               ADD 1 TO A2K-OUPROG
               PERFORM P-CHECK-VEDIFEST
               PERFORM UNTIL A2K-OUTG06 NOT = 'F'
                   ADD 1 TO A2K-OUPROG
                   PERFORM P-CHECK-VEDIFEST
               END-PERFORM
           END-IF

      *>>  SOMMA GIORNI FISSI
           IF  A2K-FISLAV = ' ' OR '0'
               ADD A2K-DELTA TO A2K-OUPROG
               GO TO CA-999-EXIT
           END-IF

           IF  A2K-FISLAV NOT = '1'
               MOVE '83' TO WS-RCODE
               GO TO CA-999-EXIT
           END-IF

      *>>  GIORNI LAVORATIVI
           MOVE A2K-DELTA TO WS-DELTA
           PERFORM UNTIL WS-DELTA = ZERO
               ADD 1 TO A2K-OUPROG
               PERFORM P-CHECK-VEDIFEST
               PERFORM UNTIL A2K-OUTG06 NOT = 'F'
                   ADD 1 TO A2K-OUPROG
                   PERFORM P-CHECK-VEDIFEST
               END-PERFORM
               SUBTRACT 1 FROM WS-DELTA
           END-PERFORM.

       CA-999-EXIT.
      *------------
           EXIT.

      *----------------------------------------------------------------

       D-SUBTRACT-DELTA SECTION.
      *-------------------------

      *    SOTTRAE DELTA

           IF  A2K-DELTA-X = SPACES
               GO TO D-999-EXIT
           END-IF

           IF  A2K-TDELTA = ' ' OR 'G'
               PERFORM DA-SUBTRACT-GIORNI
               GO TO D-999-EXIT
           END-IF

           IF  A2K-TDELTA NOT = 'M'
           AND A2K-TDELTA NOT = 'A'
               MOVE '81' TO WS-RCODE
               GO TO D-999-EXIT
           END-IF

           PERFORM M-CONVERT-CONVGE
           IF  WS-RCODE NOT = ZERO
               GO TO D-999-EXIT
           END-IF

           MOVE A2K-DELTA TO WS-DATA-WK
           IF  A2K-TDELTA = 'M'
               PERFORM UNTIL WS-DATA-MM GREATER WS-DATA-WK
                   SUBTRACT WS-DATA-MM FROM WS-DATA-WK
                   MOVE 12 TO WS-DATA-MM
                   SUBTRACT 1 FROM WS-DATA-SSAA
               END-PERFORM
               SUBTRACT WS-DATA-WK FROM WS-DATA-MM
           ELSE
               SUBTRACT A2K-DELTA FROM WS-DATA-SSAA
           END-IF

           MOVE WS-DATA-SSAA TO WS-DATAP
           PERFORM N-CHECK-BISESTIL

           MOVE WS-DATA-MM TO I2
           IF  WS-DATA-GG GREATER WS-MESE (I2)
               MOVE WS-MESE (I2) TO WS-DATA-GG
           END-IF.

           MOVE WS-DATA TO A2K-OUGMA

           PERFORM L-CONVERT-CONVDG
           IF  WS-RCODE = ZERO
               MOVE WS-DATAP TO A2K-OUPROG
           END-IF.

       D-999-EXIT.
      *-----------
           EXIT.

      *----------------------------------------------------------------

       DA-SUBTRACT-GIORNI SECTION.
      *---------------------------

      *>>  SE FESTIVO LAVORATIVO PRECEDENTE
           IF  A2K-INICON = '2'
               PERFORM P-CHECK-VEDIFEST
               PERFORM UNTIL A2K-OUTG06 NOT = 'F'
                   SUBTRACT 1 FROM A2K-OUPROG
                   PERFORM P-CHECK-VEDIFEST
               END-PERFORM
               GO TO DA-999-EXIT
           END-IF

      *>>  INIZIA DAL PRIMO GIORNO LAVORATIVO
           IF  A2K-INICON = '1'
               SUBTRACT 1 FROM A2K-OUPROG
               PERFORM P-CHECK-VEDIFEST
               PERFORM UNTIL A2K-OUTG06 NOT = 'F'
                   SUBTRACT 1 FROM A2K-OUPROG
                   PERFORM P-CHECK-VEDIFEST
               END-PERFORM
           END-IF

      *>>  SOTTRAE GIORNI FISSI
           IF  A2K-FISLAV = ' ' OR '0'
               SUBTRACT A2K-DELTA FROM A2K-OUPROG
               GO TO DA-999-EXIT
           END-IF

           IF  A2K-FISLAV NOT = '1'
               MOVE '83' TO WS-RCODE
               GO TO DA-999-EXIT
           END-IF

      *>>  GIORNI LABORATIVI
           MOVE A2K-DELTA TO WS-DELTA
           PERFORM UNTIL WS-DELTA = ZERO
               SUBTRACT 1 FROM A2K-OUPROG
               PERFORM P-CHECK-VEDIFEST
               PERFORM UNTIL A2K-OUTG06 NOT = 'F'
                   SUBTRACT 1 FROM A2K-OUPROG
                   PERFORM P-CHECK-VEDIFEST
               END-PERFORM
               SUBTRACT 1 FROM WS-DELTA
           END-PERFORM.

       DA-999-EXIT.
      *------------
           EXIT.

      *----------------------------------------------------------------

       E-PROCESS-CONVERSIONI SECTION.
      *------------------------------

      *>>  CONTROLLO SE FESTA
           IF  A2K-FUNZ = '06'
               PERFORM EA-PROCESS-FUNPASQ
           ELSE
               PERFORM P-CHECK-VEDIFEST
               IF  A2K-FUNZ = '04'
                   GO TO E-999-EXIT
               END-IF
           END-IF

      *>>  PROGRESSIVO DA 1/1/1900 =
      *>>  PROGRESSIVO CORRENTE - PROGRESSIVO AL 31/12/1899
           MOVE A2K-OUPROG TO WS-DATAP
           SUBTRACT WA-INIPROG FROM WS-DATAP
           MOVE WS-DATAP TO A2K-OUPROG9

      *>>  IL GIORNO DELLA SETTIMANA
           PERFORM Q-CHECK-SABDOM
           ADD 1 A2K-OUNG06 GIVING I2
           MOVE WA-DGIORNO (I2) TO A2K-OUGG06

      *>>  CONVERTE DATA DA PROGRESSIVO
           PERFORM M-CONVERT-CONVGE
           IF  WS-RCODE NOT = ZERO
               GO TO E-999-EXIT
           END-IF

           MOVE WS-DATA TO A2K-OUGMA

           MOVE WS-DATA-GG TO A2K-OUGG02
           MOVE WS-DATA-MM TO A2K-OUMM02
           MOVE WS-DATA-SS TO A2K-OUSS02
           MOVE WS-DATA-AA TO A2K-OUAA02
           MOVE '/'        TO A2K-OUS102
                              A2K-OUS202

           MOVE WS-DATA-GG TO A2K-OUGG03
           MOVE WS-DATA-MM TO A2K-OUMM03
           MOVE WS-DATA-SS TO A2K-OUSS03
           MOVE WS-DATA-AA TO A2K-OUAA03

           MOVE A2K-OUAMG TO A2K-OUAMGP

           MULTIPLY A2K-OUAMG BY 10 GIVING A2K-OUAMG0P

           MOVE WS-DATA-SS TO A2K-OUJSS
           MOVE WS-DATA-AA TO A2K-OUJAA

      *>>  CALCOLO PROGRESSIVO AL 31/12/DATAP
           MOVE WS-DATA-SSAA TO WS-DATAP
           PERFORM N-CHECK-BISESTIL
           PERFORM O-COMPUTE-CAL3112

           MOVE A2K-OUPROG TO WS-DATA-WK
           SUBTRACT WS-DATAP FROM WS-DATA-WK
           MOVE WS-DATA-WK TO A2K-OUFA07

      *>>  PROGRESSIVO GIULIANO NELL'ANNO
           IF  WS-MESE (2) = 28
               ADD 365 TO WS-DATA-WK
           ELSE
               ADD 366 TO WS-DATA-WK
           END-IF

           MOVE WS-DATA-WK TO A2K-OUJGGG

      *>>  NR. GIORNI DEL MESE
           MOVE A2K-OUMM TO I2
           MOVE WS-MESE (I2) TO A2K-OUGG07

      *>>  DESCRIZIONE MESE
           MOVE WS-DATA-GG    TO A2K-OUGG04
           MOVE WA-DMESE (I2) TO A2K-OUMM04
           MOVE WS-DATA-SS    TO A2K-OUSS04
           MOVE WS-DATA-AA    TO A2K-OUAA04

           MOVE WS-DATA-GG    TO A2K-OUGG05
           MOVE WA-DMESE (I2) TO A2K-OUMM05
           MOVE WS-DATA-AA    TO A2K-OUAA05

      *>>  TIPO GIORNO
           IF  WS-DATA-GG = A2K-OUGG07
               MOVE 'M' TO A2K-OUGG09
           END-IF

           IF  WS-DATA-GG = 15
               MOVE 'Q' TO A2K-OUGG09
           END-IF

           IF  WS-DATA-GG = 10 OR 20
               MOVE 'D' TO A2K-OUGG09
           END-IF

      *>>  NR. TRIMESTRE
           COMPUTE WS-DATA-WK = (WS-DATA-MM - 1) / 3 + 1
           MOVE WS-DATA-WK TO A2K-OUGG08

           IF  A2K-FUNZ = '07'
               PERFORM R-COMPUTE-RLAVOM
           END-IF

      *>>  CONV. PROGRESSIVO COMMERCIALE
           COMPUTE A2K-OUPROGC = (WS-DATA-MM - 1) * 30
           COMPUTE WS-DATA-WK  = (WS-DATA-SSAA - 1901) * 360
           COMPUTE A2K-OUPROGC = A2K-OUPROGC + WS-DATA-WK + WS-DATA-GG

           IF  WS-DATA-GG NOT LESS 28
               IF  WS-DATA-MM = 2
               OR  WS-DATA-GG NOT LESS 30
                   COMPUTE A2K-OUPROGC = A2K-OUPROGC - WS-DATA-GG + 30
               END-IF
           END-IF

           COMPUTE A2K-OUPROCO = (WS-DATA-MM - 1) * 30
           COMPUTE WS-DATA-WK  = (WS-DATA-SSAA - 1) * 360
           COMPUTE A2K-OUPROCO = A2K-OUPROCO + WS-DATA-WK + WS-DATA-GG

           IF  WS-DATA-GG NOT LESS 28
               IF  WS-DATA-MM = 2
               OR  WS-DATA-GG NOT LESS 30
                   COMPUTE A2K-OUPROCO = A2K-OUPROCO - WS-DATA-GG + 30
               END-IF
           END-IF.

       E-999-EXIT.
      *-----------
           EXIT.

      *----------------------------------------------------------------

       EA-PROCESS-FUNPASQ SECTION.
      *---------------------------

      *----------------------------------------------------------------
      *  RICHIESTA CALCOLO PASQUA
      *----------------------------------------------------------------

           MOVE 'N' TO A2K-OUTG06
           MOVE WS-DATA TO A2K-OUGMA

      *>>  CONV. PROGRESSIVO A DATA
           PERFORM M-CONVERT-CONVGE
           IF  WS-RCODE NOT = ZERO
               GO TO EA-999-EXIT
           END-IF

      *>>  CALCOLO DI PASQUA
           PERFORM S-CHECK-TESTPASQ
           MOVE A2K-OUGMA TO WS-DATA
           COMPUTE A2K-OUPROG = WS-DATAP - 1

           MOVE 'F' TO A2K-OUTG06
           MOVE 'P' TO A2K-OUGG10.

       EA-999-EXIT.
      *------------
           EXIT.

      *----------------------------------------------------------------

       L-CONVERT-CONVDG SECTION.
      *-------------------------

      *----------------------------------------------------------------
      *  CONVERSIONE DA GGMMAAAA A PROGRESSIVO
      *----------------------------------------------------------------

           MOVE WS-DATA-SSAA TO WS-DATAP
           PERFORM N-CHECK-BISESTIL

           SUBTRACT 1 FROM WS-DATAP
           PERFORM O-COMPUTE-CAL3112

           SUBTRACT 1 FROM WS-DATA-MM GIVING WS-MESI-WK

           MOVE 1 TO I2
           IF  WS-MESI-WK NOT = ZERO
               PERFORM UNTIL I2 GREATER WS-MESI-WK
                   ADD WS-MESE (I2) TO WS-DATAP
                   ADD 1 TO I2
               END-PERFORM
           END-IF

           IF  WS-DATA-GG GREATER WS-MESE (I2)
               MOVE '80' TO WS-RCODE
           ELSE
               ADD WS-DATA-GG TO WS-DATAP
           END-IF.

       L-999-EXIT.
      *-----------
           EXIT.

      *----------------------------------------------------------------

       M-CONVERT-CONVGE SECTION.
      *-------------------------

      *----------------------------------------------------------------
      *  CONVERSIONE DA PROGRESSIVO A GGMMAAAA
      *----------------------------------------------------------------

           MOVE WA-TABMESI TO WS-MESI

           DIVIDE WA-GG-IN-400-ANNI INTO A2K-OUPROG
                    GIVING WS-QUOTIENT
                 REMAINDER WS-REMAINDER
           END-DIVIDE

           COMPUTE WS-DATAP = WS-QUOTIENT * 400

           IF  WS-REMAINDER = ZERO
               MOVE WS-DATAP TO WS-DATA
               MOVE 31 TO WS-DATA-GG
               MOVE 12 TO WS-DATA-MM
               GO TO M-999-EXIT
           END-IF

           DIVIDE WA-GG-IN-100-ANNI INTO WS-REMAINDER
                    GIVING WS-QUOTIENT
                 REMAINDER WS-REMAINDER
           END-DIVIDE

           COMPUTE WS-DATAP = WS-DATAP + WS-QUOTIENT * 100

           IF  WS-REMAINDER = ZERO
               MOVE WS-DATAP TO WS-DATA
               PERFORM N-CHECK-BISESTIL
               IF  WS-MESE (2) = 28
                   MOVE 31 TO WS-DATA-GG
                   MOVE 12 TO WS-DATA-MM
               ELSE
                   MOVE 30 TO WS-DATA-GG
                   MOVE 12 TO WS-DATA-MM
               END-IF
               GO TO M-999-EXIT
           END-IF

           DIVIDE WA-GG-IN-4-ANNI INTO WS-REMAINDER
                    GIVING WS-QUOTIENT
                 REMAINDER WS-REMAINDER
           END-DIVIDE

           COMPUTE WS-DATAP = WS-DATAP + WS-QUOTIENT * 4

           IF  WS-REMAINDER = ZERO
               MOVE WS-DATAP TO WS-DATA
               MOVE 31 TO WS-DATA-GG
               MOVE 12 TO WS-DATA-MM
               GO TO M-999-EXIT
           END-IF

           DIVIDE 365 INTO WS-REMAINDER
                    GIVING WS-QUOTIENT
                 REMAINDER WS-REMAINDER
           END-DIVIDE

           COMPUTE WS-DATAP = WS-DATAP + WS-QUOTIENT

           IF  WS-REMAINDER = ZERO
               MOVE WS-DATAP TO WS-DATA
               PERFORM N-CHECK-BISESTIL
               IF  WS-MESE (2) = 28
                   MOVE 31 TO WS-DATA-GG
                   MOVE 12 TO WS-DATA-MM
               ELSE
                   MOVE 30 TO WS-DATA-GG
                   MOVE 12 TO WS-DATA-MM
               END-IF
               GO TO M-999-EXIT
           END-IF

           ADD 1 TO WS-DATAP

           MOVE WS-DATAP TO WS-DATA
           MOVE WS-REMAINDER TO WS-ANY-NUM
           PERFORM N-CHECK-BISESTIL

           MOVE 1 TO WS-DATA-MM
           MOVE 1 TO I2
           PERFORM UNTIL WS-ANY-NUM NOT GREATER WS-MESE (I2)
               SUBTRACT WS-MESE (I2) FROM WS-ANY-NUM
               ADD 1 TO WS-DATA-MM
               ADD 1 TO I2
           END-PERFORM.

           MOVE WS-ANY-NUM TO WS-DATA-GG.

       M-999-EXIT.
      *-----------
           EXIT.

      *----------------------------------------------------------------

       N-CHECK-BISESTIL SECTION.
      *-------------------------

      *----------------------------------------------------------------
      *  CONTROLLO ANNO BISESTILE
      *----------------------------------------------------------------

           MOVE WA-TABMESI TO WS-MESI

           DIVIDE 400 INTO WS-DATAP
                    GIVING WS-QUOTIENT
                 REMAINDER WS-REMAINDER
           END-DIVIDE

           IF  WS-REMAINDER = ZERO
               MOVE 29 TO WS-MESE (2)
               GO TO N-999-EXIT
           END-IF

           DIVIDE 100 INTO WS-DATAP
                    GIVING WS-QUOTIENT
                 REMAINDER WS-REMAINDER
           END-DIVIDE

           IF  WS-REMAINDER = ZERO
               GO TO N-999-EXIT
           END-IF

           DIVIDE 4 INTO WS-DATAP
                    GIVING WS-QUOTIENT
                 REMAINDER WS-REMAINDER
           END-DIVIDE

           IF  WS-REMAINDER = ZERO
               MOVE 29 TO WS-MESE (2)
           END-IF.

       N-999-EXIT.
      *-----------
           EXIT.

      *----------------------------------------------------------------

       O-COMPUTE-CAL3112 SECTION.
      *--------------------------

      *----------------------------------------------------------------
      *  CALCOLA PROGRESSIVO 31/12/ANNO DATAP
      *----------------------------------------------------------------

           DIVIDE 400 INTO WS-DATAP
                    GIVING WS-QUOTIENT
                 REMAINDER WS-REMAINDER
           END-DIVIDE

           COMPUTE WS-DATAP = WS-QUOTIENT * WA-GG-IN-400-ANNI

           MOVE WS-REMAINDER TO WS-DATA-WK
           DIVIDE 100 INTO WS-DATA-WK
                    GIVING WS-QUOTIENT
                 REMAINDER WS-REMAINDER
           END-DIVIDE

           COMPUTE WS-DATAP = WS-DATAP + WS-QUOTIENT * WA-GG-IN-100-ANNI

           MOVE WS-REMAINDER TO WS-DATA-WK
           DIVIDE 4   INTO WS-DATA-WK
                    GIVING WS-QUOTIENT
                 REMAINDER WS-REMAINDER
           END-DIVIDE

           COMPUTE WS-DATAP = WS-DATAP + WS-QUOTIENT * WA-GG-IN-4-ANNI
                            + WS-REMAINDER * 365.

       O-999-EXIT.
      *-----------
           EXIT.

      *----------------------------------------------------------------

       P-CHECK-VEDIFEST SECTION.
      *-------------------------

      *----------------------------------------------------------------
      *  CONTROLLO FESTIVITA'   SABATO DOMENICA
      *----------------------------------------------------------------

           MOVE 'N' TO A2K-OUTG06

           PERFORM M-CONVERT-CONVGE
           IF  WS-RCODE NOT = ZERO
               GO TO P-999-EXIT
           END-IF

           MOVE WS-DATA TO A2K-OUGMA

           PERFORM Q-CHECK-SABDOM

      *>>  SE FESTA FISSA
           PERFORM T-CHECK-TESTFFIS

      *>>  SE DOMENICA DI PASQUA
           IF  A2K-OUTG06 = 'F'
           AND A2K-OUNG06 = 0
               ADD 1 TO A2K-OUPROG
               PERFORM S-CHECK-TESTPASQ
               MOVE A2K-OUGMA TO WS-DATA
               SUBTRACT 1 FROM A2K-OUPROG
           END-IF

      *>>  SE FESTA FISSA

      *>>  SE LUNEDI DI PASQUA
           IF  A2K-OUTG06 NOT = 'F'
           AND A2K-OUNG06 = 1
               PERFORM S-CHECK-TESTPASQ
               MOVE A2K-OUGMA TO WS-DATA
           END-IF.

       P-999-EXIT.
      *-----------
           EXIT.

      *----------------------------------------------------------------

       Q-CHECK-SABDOM SECTION.
      *-----------------------

      *----------------------------------------------------------------
      *  CONTROLLO SABATO O DOMENICA
      *----------------------------------------------------------------

      *>>  GIORNO DELLA SETTIMANA
           DIVIDE 7   INTO A2K-OUPROG
                    GIVING WS-QUOTIENT
                 REMAINDER WS-REMAINDER
           END-DIVIDE

           MOVE WS-REMAINDER TO A2K-OUNG06

      *>>  SE SABATO O DOMENICA
           IF  A2K-OUNG06 = 0 OR 6
               MOVE 'F' TO A2K-OUTG06
           END-IF.

       Q-999-EXIT.
      *-----------
           EXIT.

      *----------------------------------------------------------------

       R-COMPUTE-RLAVOM SECTION.
      *-----------------------

      *----------------------------------------------------------------
      *  CALCOLO GIORNI LAVORATIVI E
      *   L'ULTIMO GIORNO LAVORATIVO DEL MESE
      *----------------------------------------------------------------

           MOVE 1 TO WS-DATA-NUM
           MOVE A2K-OUAMG-X (1:6) TO WS-DATA-NUM-AAMM
           MOVE ZERO TO WS-GMLAV

      *>>  SE MARZO O APRILE, CALCOLA DATA PASQUETTA
           IF  WS-DATA-NUM-MM = 03 OR 04
               PERFORM S-CHECK-TESTPASQ
               MOVE A2K-OUGMA TO WS-DATA
               MOVE WS-DATAP TO WS-DATA-PASQUETTA
           ELSE
               MOVE ZERO TO WS-DATA-PASQUETTA
           END-IF

      *>>  IL CALCOLO PER IL BISESTILE E' GIA' FATTO
           SUBTRACT 1 FROM WS-DATA-NUM-AA GIVING WS-DATAP
           PERFORM O-COMPUTE-CAL3112

      *>>  NR. GIORNI AL FINE DEL MESE PRECEDENTE
           MOVE WS-DATA-NUM-MM TO I2
           SUBTRACT 1 FROM I2
           PERFORM UNTIL I2 EQUAL ZERO
               ADD WS-MESE (I2) TO WS-DATAP
               SUBTRACT 1 FROM I2
           END-PERFORM

      *>>  NR. GIORNI AD OGGI
           ADD WS-DATA-NUM-GG TO WS-DATAP

      *>>  PROCESS FINO ALL'ULTIMO DEL MESE
           PERFORM RA-VEDI-SE-LAVORATIVO
               UNTIL WS-DATA-NUM-GG GREATER A2K-OUGG07

           MOVE WS-GMLAV TO A2K-OUGG07
           MOVE SPACES TO A2K-OUGL07-X
           MOVE WS-FMLAV TO A2K-OUGL07.

       R-999-EXIT.
      *-----------
           EXIT.

      *----------------------------------------------------------------

       RA-VEDI-SE-LAVORATIVO SECTION.
      *------------------------------

      *----------------------------------------------------------------
      *  VEDI SE UN GIORNO E' LAVORATIVO
      *----------------------------------------------------------------

      *>>  VEDI SE FESTA FISSA
           MOVE 1 TO I2
           PERFORM UNTIL WS-DATA-NUM-MMGG EQUAL WA-FESTEX (I2)
                      OR WA-FESTEX (I2) EQUAL HIGH-VALUE
                      OR I2 GREATER WA-MAX-FESTEX
               ADD 1 TO I2
           END-PERFORM

      *>>  RITORNA SE FESTA FISSA O PASQUETTA
           IF  WS-DATA-NUM-MMGG EQUAL WA-FESTEX (I2)
           OR  WS-DATAP EQUAL WS-DATA-PASQUETTA
               ADD 1 TO WS-DATA-NUM-GG
               ADD 1 TO WS-DATAP
               GO TO RA-999-EXIT
           END-IF

      *>>  GIORNO DELLA SETTIMANA
           DIVIDE 7   INTO WS-DATAP
                    GIVING WS-QUOTIENT
                 REMAINDER WS-REMAINDER
           END-DIVIDE

      *>>  RITORNA SE SABATO O DOMENICA
           IF  WS-REMAINDER = 0 OR 6
               ADD 1 TO WS-DATA-NUM-GG
               ADD 1 TO WS-DATAP
               GO TO RA-999-EXIT
           END-IF

           ADD 1 TO WS-GMLAV
           MOVE WS-DATA-NUM-GG TO WS-FMLAV

           ADD 1 TO WS-DATA-NUM-GG
           ADD 1 TO WS-DATAP.

       RA-999-EXIT.
      *------------
           EXIT.

      *----------------------------------------------------------------

       S-CHECK-TESTPASQ SECTION.
      *-------------------------

      *----------------------------------------------------------------
      *  CALCOLO LA PASQUA CON LA FORMULA DI GAUSS
      *----------------------------------------------------------------

           DIVIDE 19  INTO WS-DATA-SSAA
                    GIVING WS-QUOTIENT
                 REMAINDER WS-RESTOA
           END-DIVIDE

           COMPUTE WS-PROD = WS-RESTOA * 19 + 24

           DIVIDE 30  INTO WS-PROD
                    GIVING WS-QUOTIENT
                 REMAINDER WS-DELTB
           END-DIVIDE

           MOVE WS-DELTB TO WS-DELTAA

           COMPUTE WS-COMO = WS-DELTB * 6 + 5

           DIVIDE 4   INTO WS-DATA-SSAA
                    GIVING WS-QUOTIENT
                 REMAINDER WS-REMAINDER
           END-DIVIDE

           COMPUTE WS-COMO = WS-COMO + WS-REMAINDER * 2

           DIVIDE 7   INTO WS-DATA-SSAA
                    GIVING WS-QUOTIENT
                 REMAINDER WS-REMAINDER
           END-DIVIDE

           COMPUTE WS-COMO = WS-COMO + WS-REMAINDER * 4

           DIVIDE 7   INTO WS-COMO
                    GIVING WS-QUOTIENT
                 REMAINDER WS-REMAINDER
           END-DIVIDE

           ADD WS-REMAINDER TO WS-DELTB

      *>>  CONTROLLO LE 2 ECCEZIONI DEL 25 E 26 APRILE

      *>>  SE CADE IL 25 APRILE E
      *>>      IL PRIMO RESTO DIVERSO DA ZERO E
      *>>      IL DELTAA UGUALE A 28
      *>>  ... ALORA PASQUE E' IL 18 APRILE
           IF  WS-DELTB = 34
           AND WS-RESTOA NOT = ZERO
           AND WS-DELTAA = 28
               MOVE 27 TO WS-DELTB
           END-IF

      *>>  SE CADE IL 26 APRILE E
      *>>  ... ALORA PASQUE E' IL 19 APRILE
           IF  WS-DELTB = 35
               MOVE 28 TO WS-DELTB
           END-IF

      *>>  CALCOLO PROGRESSIVO PER 22/03/ANNO DATA
           MOVE 22 TO WS-DATA-GG
           MOVE 03 TO WS-DATA-MM
           PERFORM L-CONVERT-CONVDG

      *>>  CALCOLO PROGRESSIVO PASQUETTA - LUNEDI
           COMPUTE WS-DATAP = WS-DATAP + WS-DELTB + 1

           IF  WS-DATAP = A2K-OUPROG
               MOVE 'F' TO A2K-OUTG06
               MOVE 'P' TO A2K-OUGG10
           END-IF.

       S-999-EXIT.
      *-----------
           EXIT.

      *----------------------------------------------------------------

       T-CHECK-TESTFFIS SECTION.
      *-------------------------

      *----------------------------------------------------------------
      *  CONTROLLO FESTE FISSE
      *----------------------------------------------------------------

           MOVE SPACES     TO A2K-OUGG10
           MOVE WS-DATA-MM TO WS-DATA-NUM-MM
           MOVE WS-DATA-GG TO WS-DATA-NUM-GG

           MOVE 1 TO I2
           PERFORM UNTIL WS-DATA-NUM-MMGG EQUAL WA-FESTEX (I2)
                      OR WA-FESTEX (I2) EQUAL HIGH-VALUE
                      OR I2 GREATER WA-MAX-FESTEX
               ADD 1 TO I2
           END-PERFORM

           IF  WS-DATA-NUM-MMGG EQUAL WA-FESTEX (I2)
               MOVE 'F' TO A2K-OUTG06
               MOVE 'F' TO A2K-OUGG10
           END-IF.

       T-999-EXIT.
      *-----------
           EXIT.

      *----------------------------------------------------------------*

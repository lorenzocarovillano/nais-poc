       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSL300 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  09 LUG 2009.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDL300 END-EXEC.
           EXEC SQL INCLUDE IDBVL302 END-EXEC.
           EXEC SQL INCLUDE IDBVL303 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVL301 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 REINVST-POLI-LQ.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSL300'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'REINVST_POLI_LQ' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_REINVST_POLI_LQ
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,COD_RAMO
                ,IB_POLI
                ,PR_KEY_SIST_ESTNO
                ,SEC_KEY_SIST_ESTNO
                ,COD_CAN
                ,COD_AGE
                ,COD_SUB_AGE
                ,IMP_RES
                ,TP_LIQ
                ,TP_SIST_ESTNO
                ,COD_FISC_PART_IVA
                ,COD_MOVI_LIQ
                ,TP_INVST_LIQ
                ,IMP_TOT_LIQ
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,DT_DECOR_POLI_LQ
             INTO
                :L30-ID-REINVST-POLI-LQ
               ,:L30-ID-MOVI-CRZ
               ,:L30-ID-MOVI-CHIU
                :IND-L30-ID-MOVI-CHIU
               ,:L30-DT-INI-EFF-DB
               ,:L30-DT-END-EFF-DB
               ,:L30-COD-COMP-ANIA
               ,:L30-COD-RAMO
               ,:L30-IB-POLI
               ,:L30-PR-KEY-SIST-ESTNO
                :IND-L30-PR-KEY-SIST-ESTNO
               ,:L30-SEC-KEY-SIST-ESTNO
                :IND-L30-SEC-KEY-SIST-ESTNO
               ,:L30-COD-CAN
               ,:L30-COD-AGE
               ,:L30-COD-SUB-AGE
                :IND-L30-COD-SUB-AGE
               ,:L30-IMP-RES
               ,:L30-TP-LIQ
               ,:L30-TP-SIST-ESTNO
               ,:L30-COD-FISC-PART-IVA
               ,:L30-COD-MOVI-LIQ
               ,:L30-TP-INVST-LIQ
                :IND-L30-TP-INVST-LIQ
               ,:L30-IMP-TOT-LIQ
               ,:L30-DS-RIGA
               ,:L30-DS-OPER-SQL
               ,:L30-DS-VER
               ,:L30-DS-TS-INI-CPTZ
               ,:L30-DS-TS-END-CPTZ
               ,:L30-DS-UTENTE
               ,:L30-DS-STATO-ELAB
               ,:L30-DT-DECOR-POLI-LQ-DB
                :IND-L30-DT-DECOR-POLI-LQ
             FROM REINVST_POLI_LQ
             WHERE     DS_RIGA = :L30-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO REINVST_POLI_LQ
                     (
                        ID_REINVST_POLI_LQ
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,COD_RAMO
                       ,IB_POLI
                       ,PR_KEY_SIST_ESTNO
                       ,SEC_KEY_SIST_ESTNO
                       ,COD_CAN
                       ,COD_AGE
                       ,COD_SUB_AGE
                       ,IMP_RES
                       ,TP_LIQ
                       ,TP_SIST_ESTNO
                       ,COD_FISC_PART_IVA
                       ,COD_MOVI_LIQ
                       ,TP_INVST_LIQ
                       ,IMP_TOT_LIQ
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,DT_DECOR_POLI_LQ
                     )
                 VALUES
                     (
                       :L30-ID-REINVST-POLI-LQ
                       ,:L30-ID-MOVI-CRZ
                       ,:L30-ID-MOVI-CHIU
                        :IND-L30-ID-MOVI-CHIU
                       ,:L30-DT-INI-EFF-DB
                       ,:L30-DT-END-EFF-DB
                       ,:L30-COD-COMP-ANIA
                       ,:L30-COD-RAMO
                       ,:L30-IB-POLI
                       ,:L30-PR-KEY-SIST-ESTNO
                        :IND-L30-PR-KEY-SIST-ESTNO
                       ,:L30-SEC-KEY-SIST-ESTNO
                        :IND-L30-SEC-KEY-SIST-ESTNO
                       ,:L30-COD-CAN
                       ,:L30-COD-AGE
                       ,:L30-COD-SUB-AGE
                        :IND-L30-COD-SUB-AGE
                       ,:L30-IMP-RES
                       ,:L30-TP-LIQ
                       ,:L30-TP-SIST-ESTNO
                       ,:L30-COD-FISC-PART-IVA
                       ,:L30-COD-MOVI-LIQ
                       ,:L30-TP-INVST-LIQ
                        :IND-L30-TP-INVST-LIQ
                       ,:L30-IMP-TOT-LIQ
                       ,:L30-DS-RIGA
                       ,:L30-DS-OPER-SQL
                       ,:L30-DS-VER
                       ,:L30-DS-TS-INI-CPTZ
                       ,:L30-DS-TS-END-CPTZ
                       ,:L30-DS-UTENTE
                       ,:L30-DS-STATO-ELAB
                       ,:L30-DT-DECOR-POLI-LQ-DB
                        :IND-L30-DT-DECOR-POLI-LQ
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE REINVST_POLI_LQ SET

                   ID_REINVST_POLI_LQ     =
                :L30-ID-REINVST-POLI-LQ
                  ,ID_MOVI_CRZ            =
                :L30-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :L30-ID-MOVI-CHIU
                                       :IND-L30-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :L30-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :L30-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :L30-COD-COMP-ANIA
                  ,COD_RAMO               =
                :L30-COD-RAMO
                  ,IB_POLI                =
                :L30-IB-POLI
                  ,PR_KEY_SIST_ESTNO      =
                :L30-PR-KEY-SIST-ESTNO
                                       :IND-L30-PR-KEY-SIST-ESTNO
                  ,SEC_KEY_SIST_ESTNO     =
                :L30-SEC-KEY-SIST-ESTNO
                                       :IND-L30-SEC-KEY-SIST-ESTNO
                  ,COD_CAN                =
                :L30-COD-CAN
                  ,COD_AGE                =
                :L30-COD-AGE
                  ,COD_SUB_AGE            =
                :L30-COD-SUB-AGE
                                       :IND-L30-COD-SUB-AGE
                  ,IMP_RES                =
                :L30-IMP-RES
                  ,TP_LIQ                 =
                :L30-TP-LIQ
                  ,TP_SIST_ESTNO          =
                :L30-TP-SIST-ESTNO
                  ,COD_FISC_PART_IVA      =
                :L30-COD-FISC-PART-IVA
                  ,COD_MOVI_LIQ           =
                :L30-COD-MOVI-LIQ
                  ,TP_INVST_LIQ           =
                :L30-TP-INVST-LIQ
                                       :IND-L30-TP-INVST-LIQ
                  ,IMP_TOT_LIQ            =
                :L30-IMP-TOT-LIQ
                  ,DS_RIGA                =
                :L30-DS-RIGA
                  ,DS_OPER_SQL            =
                :L30-DS-OPER-SQL
                  ,DS_VER                 =
                :L30-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :L30-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :L30-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :L30-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :L30-DS-STATO-ELAB
                  ,DT_DECOR_POLI_LQ       =
           :L30-DT-DECOR-POLI-LQ-DB
                                       :IND-L30-DT-DECOR-POLI-LQ
                WHERE     DS_RIGA = :L30-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM REINVST_POLI_LQ
                WHERE     DS_RIGA = :L30-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-L30 CURSOR FOR
              SELECT
                     ID_REINVST_POLI_LQ
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,COD_RAMO
                    ,IB_POLI
                    ,PR_KEY_SIST_ESTNO
                    ,SEC_KEY_SIST_ESTNO
                    ,COD_CAN
                    ,COD_AGE
                    ,COD_SUB_AGE
                    ,IMP_RES
                    ,TP_LIQ
                    ,TP_SIST_ESTNO
                    ,COD_FISC_PART_IVA
                    ,COD_MOVI_LIQ
                    ,TP_INVST_LIQ
                    ,IMP_TOT_LIQ
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,DT_DECOR_POLI_LQ
              FROM REINVST_POLI_LQ
              WHERE     ID_REINVST_POLI_LQ = :L30-ID-REINVST-POLI-LQ
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY DT_INI_EFF ASC

           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_REINVST_POLI_LQ
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,COD_RAMO
                ,IB_POLI
                ,PR_KEY_SIST_ESTNO
                ,SEC_KEY_SIST_ESTNO
                ,COD_CAN
                ,COD_AGE
                ,COD_SUB_AGE
                ,IMP_RES
                ,TP_LIQ
                ,TP_SIST_ESTNO
                ,COD_FISC_PART_IVA
                ,COD_MOVI_LIQ
                ,TP_INVST_LIQ
                ,IMP_TOT_LIQ
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,DT_DECOR_POLI_LQ
             INTO
                :L30-ID-REINVST-POLI-LQ
               ,:L30-ID-MOVI-CRZ
               ,:L30-ID-MOVI-CHIU
                :IND-L30-ID-MOVI-CHIU
               ,:L30-DT-INI-EFF-DB
               ,:L30-DT-END-EFF-DB
               ,:L30-COD-COMP-ANIA
               ,:L30-COD-RAMO
               ,:L30-IB-POLI
               ,:L30-PR-KEY-SIST-ESTNO
                :IND-L30-PR-KEY-SIST-ESTNO
               ,:L30-SEC-KEY-SIST-ESTNO
                :IND-L30-SEC-KEY-SIST-ESTNO
               ,:L30-COD-CAN
               ,:L30-COD-AGE
               ,:L30-COD-SUB-AGE
                :IND-L30-COD-SUB-AGE
               ,:L30-IMP-RES
               ,:L30-TP-LIQ
               ,:L30-TP-SIST-ESTNO
               ,:L30-COD-FISC-PART-IVA
               ,:L30-COD-MOVI-LIQ
               ,:L30-TP-INVST-LIQ
                :IND-L30-TP-INVST-LIQ
               ,:L30-IMP-TOT-LIQ
               ,:L30-DS-RIGA
               ,:L30-DS-OPER-SQL
               ,:L30-DS-VER
               ,:L30-DS-TS-INI-CPTZ
               ,:L30-DS-TS-END-CPTZ
               ,:L30-DS-UTENTE
               ,:L30-DS-STATO-ELAB
               ,:L30-DT-DECOR-POLI-LQ-DB
                :IND-L30-DT-DECOR-POLI-LQ
             FROM REINVST_POLI_LQ
             WHERE     ID_REINVST_POLI_LQ = :L30-ID-REINVST-POLI-LQ
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE REINVST_POLI_LQ SET

                   ID_REINVST_POLI_LQ     =
                :L30-ID-REINVST-POLI-LQ
                  ,ID_MOVI_CRZ            =
                :L30-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :L30-ID-MOVI-CHIU
                                       :IND-L30-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :L30-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :L30-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :L30-COD-COMP-ANIA
                  ,COD_RAMO               =
                :L30-COD-RAMO
                  ,IB_POLI                =
                :L30-IB-POLI
                  ,PR_KEY_SIST_ESTNO      =
                :L30-PR-KEY-SIST-ESTNO
                                       :IND-L30-PR-KEY-SIST-ESTNO
                  ,SEC_KEY_SIST_ESTNO     =
                :L30-SEC-KEY-SIST-ESTNO
                                       :IND-L30-SEC-KEY-SIST-ESTNO
                  ,COD_CAN                =
                :L30-COD-CAN
                  ,COD_AGE                =
                :L30-COD-AGE
                  ,COD_SUB_AGE            =
                :L30-COD-SUB-AGE
                                       :IND-L30-COD-SUB-AGE
                  ,IMP_RES                =
                :L30-IMP-RES
                  ,TP_LIQ                 =
                :L30-TP-LIQ
                  ,TP_SIST_ESTNO          =
                :L30-TP-SIST-ESTNO
                  ,COD_FISC_PART_IVA      =
                :L30-COD-FISC-PART-IVA
                  ,COD_MOVI_LIQ           =
                :L30-COD-MOVI-LIQ
                  ,TP_INVST_LIQ           =
                :L30-TP-INVST-LIQ
                                       :IND-L30-TP-INVST-LIQ
                  ,IMP_TOT_LIQ            =
                :L30-IMP-TOT-LIQ
                  ,DS_RIGA                =
                :L30-DS-RIGA
                  ,DS_OPER_SQL            =
                :L30-DS-OPER-SQL
                  ,DS_VER                 =
                :L30-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :L30-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :L30-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :L30-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :L30-DS-STATO-ELAB
                  ,DT_DECOR_POLI_LQ       =
           :L30-DT-DECOR-POLI-LQ-DB
                                       :IND-L30-DT-DECOR-POLI-LQ
                WHERE     DS_RIGA = :L30-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-L30
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-L30
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-L30
           INTO
                :L30-ID-REINVST-POLI-LQ
               ,:L30-ID-MOVI-CRZ
               ,:L30-ID-MOVI-CHIU
                :IND-L30-ID-MOVI-CHIU
               ,:L30-DT-INI-EFF-DB
               ,:L30-DT-END-EFF-DB
               ,:L30-COD-COMP-ANIA
               ,:L30-COD-RAMO
               ,:L30-IB-POLI
               ,:L30-PR-KEY-SIST-ESTNO
                :IND-L30-PR-KEY-SIST-ESTNO
               ,:L30-SEC-KEY-SIST-ESTNO
                :IND-L30-SEC-KEY-SIST-ESTNO
               ,:L30-COD-CAN
               ,:L30-COD-AGE
               ,:L30-COD-SUB-AGE
                :IND-L30-COD-SUB-AGE
               ,:L30-IMP-RES
               ,:L30-TP-LIQ
               ,:L30-TP-SIST-ESTNO
               ,:L30-COD-FISC-PART-IVA
               ,:L30-COD-MOVI-LIQ
               ,:L30-TP-INVST-LIQ
                :IND-L30-TP-INVST-LIQ
               ,:L30-IMP-TOT-LIQ
               ,:L30-DS-RIGA
               ,:L30-DS-OPER-SQL
               ,:L30-DS-VER
               ,:L30-DS-TS-INI-CPTZ
               ,:L30-DS-TS-END-CPTZ
               ,:L30-DS-UTENTE
               ,:L30-DS-STATO-ELAB
               ,:L30-DT-DECOR-POLI-LQ-DB
                :IND-L30-DT-DECOR-POLI-LQ
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DCL-CUR-IBS-POLI.
           EXEC SQL
                DECLARE C-IBS-EFF-L30-0 CURSOR FOR
              SELECT
                     ID_REINVST_POLI_LQ
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,COD_RAMO
                    ,IB_POLI
                    ,PR_KEY_SIST_ESTNO
                    ,SEC_KEY_SIST_ESTNO
                    ,COD_CAN
                    ,COD_AGE
                    ,COD_SUB_AGE
                    ,IMP_RES
                    ,TP_LIQ
                    ,TP_SIST_ESTNO
                    ,COD_FISC_PART_IVA
                    ,COD_MOVI_LIQ
                    ,TP_INVST_LIQ
                    ,IMP_TOT_LIQ
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,DT_DECOR_POLI_LQ
              FROM REINVST_POLI_LQ
              WHERE     IB_POLI = :L30-IB-POLI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_REINVST_POLI_LQ ASC

           END-EXEC.
       A605-POLI-EX.
           EXIT.


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF L30-IB-POLI NOT = HIGH-VALUES
               PERFORM A605-DCL-CUR-IBS-POLI
                  THRU A605-POLI-EX
           END-IF.

       A605-EX.
           EXIT.


       A610-SELECT-IBS-POLI.
           EXEC SQL
             SELECT
                ID_REINVST_POLI_LQ
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,COD_RAMO
                ,IB_POLI
                ,PR_KEY_SIST_ESTNO
                ,SEC_KEY_SIST_ESTNO
                ,COD_CAN
                ,COD_AGE
                ,COD_SUB_AGE
                ,IMP_RES
                ,TP_LIQ
                ,TP_SIST_ESTNO
                ,COD_FISC_PART_IVA
                ,COD_MOVI_LIQ
                ,TP_INVST_LIQ
                ,IMP_TOT_LIQ
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,DT_DECOR_POLI_LQ
             INTO
                :L30-ID-REINVST-POLI-LQ
               ,:L30-ID-MOVI-CRZ
               ,:L30-ID-MOVI-CHIU
                :IND-L30-ID-MOVI-CHIU
               ,:L30-DT-INI-EFF-DB
               ,:L30-DT-END-EFF-DB
               ,:L30-COD-COMP-ANIA
               ,:L30-COD-RAMO
               ,:L30-IB-POLI
               ,:L30-PR-KEY-SIST-ESTNO
                :IND-L30-PR-KEY-SIST-ESTNO
               ,:L30-SEC-KEY-SIST-ESTNO
                :IND-L30-SEC-KEY-SIST-ESTNO
               ,:L30-COD-CAN
               ,:L30-COD-AGE
               ,:L30-COD-SUB-AGE
                :IND-L30-COD-SUB-AGE
               ,:L30-IMP-RES
               ,:L30-TP-LIQ
               ,:L30-TP-SIST-ESTNO
               ,:L30-COD-FISC-PART-IVA
               ,:L30-COD-MOVI-LIQ
               ,:L30-TP-INVST-LIQ
                :IND-L30-TP-INVST-LIQ
               ,:L30-IMP-TOT-LIQ
               ,:L30-DS-RIGA
               ,:L30-DS-OPER-SQL
               ,:L30-DS-VER
               ,:L30-DS-TS-INI-CPTZ
               ,:L30-DS-TS-END-CPTZ
               ,:L30-DS-UTENTE
               ,:L30-DS-STATO-ELAB
               ,:L30-DT-DECOR-POLI-LQ-DB
                :IND-L30-DT-DECOR-POLI-LQ
             FROM REINVST_POLI_LQ
             WHERE     IB_POLI = :L30-IB-POLI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.
       A610-POLI-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF L30-IB-POLI NOT = HIGH-VALUES
               PERFORM A610-SELECT-IBS-POLI
                  THRU A610-POLI-EX
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           IF L30-IB-POLI NOT = HIGH-VALUES
              EXEC SQL
                   OPEN C-IBS-EFF-L30-0
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           IF L30-IB-POLI NOT = HIGH-VALUES
              EXEC SQL
                   CLOSE C-IBS-EFF-L30-0
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FN-IBS-POLI.
           EXEC SQL
                FETCH C-IBS-EFF-L30-0
           INTO
                :L30-ID-REINVST-POLI-LQ
               ,:L30-ID-MOVI-CRZ
               ,:L30-ID-MOVI-CHIU
                :IND-L30-ID-MOVI-CHIU
               ,:L30-DT-INI-EFF-DB
               ,:L30-DT-END-EFF-DB
               ,:L30-COD-COMP-ANIA
               ,:L30-COD-RAMO
               ,:L30-IB-POLI
               ,:L30-PR-KEY-SIST-ESTNO
                :IND-L30-PR-KEY-SIST-ESTNO
               ,:L30-SEC-KEY-SIST-ESTNO
                :IND-L30-SEC-KEY-SIST-ESTNO
               ,:L30-COD-CAN
               ,:L30-COD-AGE
               ,:L30-COD-SUB-AGE
                :IND-L30-COD-SUB-AGE
               ,:L30-IMP-RES
               ,:L30-TP-LIQ
               ,:L30-TP-SIST-ESTNO
               ,:L30-COD-FISC-PART-IVA
               ,:L30-COD-MOVI-LIQ
               ,:L30-TP-INVST-LIQ
                :IND-L30-TP-INVST-LIQ
               ,:L30-IMP-TOT-LIQ
               ,:L30-DS-RIGA
               ,:L30-DS-OPER-SQL
               ,:L30-DS-VER
               ,:L30-DS-TS-INI-CPTZ
               ,:L30-DS-TS-END-CPTZ
               ,:L30-DS-UTENTE
               ,:L30-DS-STATO-ELAB
               ,:L30-DT-DECOR-POLI-LQ-DB
                :IND-L30-DT-DECOR-POLI-LQ
           END-EXEC.
       A690-POLI-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           IF L30-IB-POLI NOT = HIGH-VALUES
               PERFORM A690-FN-IBS-POLI
                  THRU A690-POLI-EX
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A670-CLOSE-CURSOR-IBS     THRU A670-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_REINVST_POLI_LQ
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,COD_RAMO
                ,IB_POLI
                ,PR_KEY_SIST_ESTNO
                ,SEC_KEY_SIST_ESTNO
                ,COD_CAN
                ,COD_AGE
                ,COD_SUB_AGE
                ,IMP_RES
                ,TP_LIQ
                ,TP_SIST_ESTNO
                ,COD_FISC_PART_IVA
                ,COD_MOVI_LIQ
                ,TP_INVST_LIQ
                ,IMP_TOT_LIQ
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,DT_DECOR_POLI_LQ
             INTO
                :L30-ID-REINVST-POLI-LQ
               ,:L30-ID-MOVI-CRZ
               ,:L30-ID-MOVI-CHIU
                :IND-L30-ID-MOVI-CHIU
               ,:L30-DT-INI-EFF-DB
               ,:L30-DT-END-EFF-DB
               ,:L30-COD-COMP-ANIA
               ,:L30-COD-RAMO
               ,:L30-IB-POLI
               ,:L30-PR-KEY-SIST-ESTNO
                :IND-L30-PR-KEY-SIST-ESTNO
               ,:L30-SEC-KEY-SIST-ESTNO
                :IND-L30-SEC-KEY-SIST-ESTNO
               ,:L30-COD-CAN
               ,:L30-COD-AGE
               ,:L30-COD-SUB-AGE
                :IND-L30-COD-SUB-AGE
               ,:L30-IMP-RES
               ,:L30-TP-LIQ
               ,:L30-TP-SIST-ESTNO
               ,:L30-COD-FISC-PART-IVA
               ,:L30-COD-MOVI-LIQ
               ,:L30-TP-INVST-LIQ
                :IND-L30-TP-INVST-LIQ
               ,:L30-IMP-TOT-LIQ
               ,:L30-DS-RIGA
               ,:L30-DS-OPER-SQL
               ,:L30-DS-VER
               ,:L30-DS-TS-INI-CPTZ
               ,:L30-DS-TS-END-CPTZ
               ,:L30-DS-UTENTE
               ,:L30-DS-STATO-ELAB
               ,:L30-DT-DECOR-POLI-LQ-DB
                :IND-L30-DT-DECOR-POLI-LQ
             FROM REINVST_POLI_LQ
             WHERE     ID_REINVST_POLI_LQ = :L30-ID-REINVST-POLI-LQ
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DCL-CUR-IBS-POLI.
           EXEC SQL
                DECLARE C-IBS-CPZ-L30-0 CURSOR FOR
              SELECT
                     ID_REINVST_POLI_LQ
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,COD_RAMO
                    ,IB_POLI
                    ,PR_KEY_SIST_ESTNO
                    ,SEC_KEY_SIST_ESTNO
                    ,COD_CAN
                    ,COD_AGE
                    ,COD_SUB_AGE
                    ,IMP_RES
                    ,TP_LIQ
                    ,TP_SIST_ESTNO
                    ,COD_FISC_PART_IVA
                    ,COD_MOVI_LIQ
                    ,TP_INVST_LIQ
                    ,IMP_TOT_LIQ
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,DT_DECOR_POLI_LQ
              FROM REINVST_POLI_LQ
              WHERE     IB_POLI = :L30-IB-POLI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_REINVST_POLI_LQ ASC

           END-EXEC.
       B605-POLI-EX.
           EXIT.


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF L30-IB-POLI NOT = HIGH-VALUES
               PERFORM B605-DCL-CUR-IBS-POLI
                  THRU B605-POLI-EX
           END-IF.

       B605-EX.
           EXIT.


       B610-SELECT-IBS-POLI.
           EXEC SQL
             SELECT
                ID_REINVST_POLI_LQ
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,COD_RAMO
                ,IB_POLI
                ,PR_KEY_SIST_ESTNO
                ,SEC_KEY_SIST_ESTNO
                ,COD_CAN
                ,COD_AGE
                ,COD_SUB_AGE
                ,IMP_RES
                ,TP_LIQ
                ,TP_SIST_ESTNO
                ,COD_FISC_PART_IVA
                ,COD_MOVI_LIQ
                ,TP_INVST_LIQ
                ,IMP_TOT_LIQ
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,DT_DECOR_POLI_LQ
             INTO
                :L30-ID-REINVST-POLI-LQ
               ,:L30-ID-MOVI-CRZ
               ,:L30-ID-MOVI-CHIU
                :IND-L30-ID-MOVI-CHIU
               ,:L30-DT-INI-EFF-DB
               ,:L30-DT-END-EFF-DB
               ,:L30-COD-COMP-ANIA
               ,:L30-COD-RAMO
               ,:L30-IB-POLI
               ,:L30-PR-KEY-SIST-ESTNO
                :IND-L30-PR-KEY-SIST-ESTNO
               ,:L30-SEC-KEY-SIST-ESTNO
                :IND-L30-SEC-KEY-SIST-ESTNO
               ,:L30-COD-CAN
               ,:L30-COD-AGE
               ,:L30-COD-SUB-AGE
                :IND-L30-COD-SUB-AGE
               ,:L30-IMP-RES
               ,:L30-TP-LIQ
               ,:L30-TP-SIST-ESTNO
               ,:L30-COD-FISC-PART-IVA
               ,:L30-COD-MOVI-LIQ
               ,:L30-TP-INVST-LIQ
                :IND-L30-TP-INVST-LIQ
               ,:L30-IMP-TOT-LIQ
               ,:L30-DS-RIGA
               ,:L30-DS-OPER-SQL
               ,:L30-DS-VER
               ,:L30-DS-TS-INI-CPTZ
               ,:L30-DS-TS-END-CPTZ
               ,:L30-DS-UTENTE
               ,:L30-DS-STATO-ELAB
               ,:L30-DT-DECOR-POLI-LQ-DB
                :IND-L30-DT-DECOR-POLI-LQ
             FROM REINVST_POLI_LQ
             WHERE     IB_POLI = :L30-IB-POLI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.
       B610-POLI-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF L30-IB-POLI NOT = HIGH-VALUES
               PERFORM B610-SELECT-IBS-POLI
                  THRU B610-POLI-EX
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           IF L30-IB-POLI NOT = HIGH-VALUES
              EXEC SQL
                   OPEN C-IBS-CPZ-L30-0
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           IF L30-IB-POLI NOT = HIGH-VALUES
              EXEC SQL
                   CLOSE C-IBS-CPZ-L30-0
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FN-IBS-POLI.
           EXEC SQL
                FETCH C-IBS-CPZ-L30-0
           INTO
                :L30-ID-REINVST-POLI-LQ
               ,:L30-ID-MOVI-CRZ
               ,:L30-ID-MOVI-CHIU
                :IND-L30-ID-MOVI-CHIU
               ,:L30-DT-INI-EFF-DB
               ,:L30-DT-END-EFF-DB
               ,:L30-COD-COMP-ANIA
               ,:L30-COD-RAMO
               ,:L30-IB-POLI
               ,:L30-PR-KEY-SIST-ESTNO
                :IND-L30-PR-KEY-SIST-ESTNO
               ,:L30-SEC-KEY-SIST-ESTNO
                :IND-L30-SEC-KEY-SIST-ESTNO
               ,:L30-COD-CAN
               ,:L30-COD-AGE
               ,:L30-COD-SUB-AGE
                :IND-L30-COD-SUB-AGE
               ,:L30-IMP-RES
               ,:L30-TP-LIQ
               ,:L30-TP-SIST-ESTNO
               ,:L30-COD-FISC-PART-IVA
               ,:L30-COD-MOVI-LIQ
               ,:L30-TP-INVST-LIQ
                :IND-L30-TP-INVST-LIQ
               ,:L30-IMP-TOT-LIQ
               ,:L30-DS-RIGA
               ,:L30-DS-OPER-SQL
               ,:L30-DS-VER
               ,:L30-DS-TS-INI-CPTZ
               ,:L30-DS-TS-END-CPTZ
               ,:L30-DS-UTENTE
               ,:L30-DS-STATO-ELAB
               ,:L30-DT-DECOR-POLI-LQ-DB
                :IND-L30-DT-DECOR-POLI-LQ
           END-EXEC.
       B690-POLI-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           IF L30-IB-POLI NOT = HIGH-VALUES
               PERFORM B690-FN-IBS-POLI
                  THRU B690-POLI-EX
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B670-CLOSE-CURSOR-IBS-CPZ     THRU B670-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-L30-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO L30-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-L30-PR-KEY-SIST-ESTNO = -1
              MOVE HIGH-VALUES TO L30-PR-KEY-SIST-ESTNO-NULL
           END-IF
           IF IND-L30-SEC-KEY-SIST-ESTNO = -1
              MOVE HIGH-VALUES TO L30-SEC-KEY-SIST-ESTNO-NULL
           END-IF
           IF IND-L30-COD-SUB-AGE = -1
              MOVE HIGH-VALUES TO L30-COD-SUB-AGE-NULL
           END-IF
           IF IND-L30-TP-INVST-LIQ = -1
              MOVE HIGH-VALUES TO L30-TP-INVST-LIQ-NULL
           END-IF
           IF IND-L30-DT-DECOR-POLI-LQ = -1
              MOVE HIGH-VALUES TO L30-DT-DECOR-POLI-LQ-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO L30-DS-OPER-SQL
           MOVE 0                   TO L30-DS-VER
           MOVE IDSV0003-USER-NAME TO L30-DS-UTENTE
           MOVE '1'                   TO L30-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO L30-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO L30-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF L30-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-L30-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-L30-ID-MOVI-CHIU
           END-IF
           IF L30-PR-KEY-SIST-ESTNO-NULL = HIGH-VALUES
              MOVE -1 TO IND-L30-PR-KEY-SIST-ESTNO
           ELSE
              MOVE 0 TO IND-L30-PR-KEY-SIST-ESTNO
           END-IF
           IF L30-SEC-KEY-SIST-ESTNO-NULL = HIGH-VALUES
              MOVE -1 TO IND-L30-SEC-KEY-SIST-ESTNO
           ELSE
              MOVE 0 TO IND-L30-SEC-KEY-SIST-ESTNO
           END-IF
           IF L30-COD-SUB-AGE-NULL = HIGH-VALUES
              MOVE -1 TO IND-L30-COD-SUB-AGE
           ELSE
              MOVE 0 TO IND-L30-COD-SUB-AGE
           END-IF
           IF L30-TP-INVST-LIQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-L30-TP-INVST-LIQ
           ELSE
              MOVE 0 TO IND-L30-TP-INVST-LIQ
           END-IF
           IF L30-DT-DECOR-POLI-LQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-L30-DT-DECOR-POLI-LQ
           ELSE
              MOVE 0 TO IND-L30-DT-DECOR-POLI-LQ
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : L30-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE REINVST-POLI-LQ TO WS-BUFFER-TABLE.

           MOVE L30-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO L30-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO L30-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO L30-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO L30-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO L30-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO L30-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO L30-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO REINVST-POLI-LQ.

           MOVE WS-ID-MOVI-CRZ  TO L30-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO L30-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO L30-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO L30-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO L30-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO L30-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO L30-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE L30-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO L30-DT-INI-EFF-DB
           MOVE L30-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO L30-DT-END-EFF-DB
           IF IND-L30-DT-DECOR-POLI-LQ = 0
               MOVE L30-DT-DECOR-POLI-LQ TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO L30-DT-DECOR-POLI-LQ-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE L30-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO L30-DT-INI-EFF
           MOVE L30-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO L30-DT-END-EFF
           IF IND-L30-DT-DECOR-POLI-LQ = 0
               MOVE L30-DT-DECOR-POLI-LQ-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO L30-DT-DECOR-POLI-LQ
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

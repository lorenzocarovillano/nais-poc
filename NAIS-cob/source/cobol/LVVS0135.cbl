      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0135.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2009.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *    MODULO DI CALCOLO PER VARIABILE CTT
      ***------------------------------------------------------------***
      *    MODALITA' VALORIZZAZIONE:
      *    Reperire il valore del fondo alla data di ricorrenza
      *    anniversaria per ogni fondo della tranche.
      *    Se il valore non h presente per la data ricorrenza
      *    anniversaria, reperire valore presente alla data piy vicina e
      *    precedente la ricorrenza anniversaria.
      *    Reperire il numero quote di ogni fondo sulla Valore Asset.
      *    Determinare il controvalore per ogni fondo:
      *    Valore quote (L19) * Numero Quote (VAS).
      *    Sommare il controvalore dei fondi della tranche.
      *    Se il valore della variabile h zero, oppure non h possibile
      *    valorizzarla (perchi ad es. non h Ramo III),
      *    non fornire la variabile ad Actuator.
      ***------------------------------------------------------------***
      *    TABELLE:   VALORE_ASSET, QUOTAZIONE_FONDI_UNIT
      *    ATTRIBUTO: NUMERO_QUOTE
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0135'.
       01  LDBS4910                         PIC X(008) VALUE 'LDBS4910'.
       01  LDBS2080                         PIC X(008) VALUE 'LDBS2080'.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-VAL-FONDO                  PIC S9(12)V9(3) COMP-3 VALUE 0.
      *
       01  WK-ID-COD-LIV-FLAG               PIC X(001).
           88 WK-ID-COD-TROVATO             VALUE 'S'.
           88 WK-ID-COD-NON-TROVATO         VALUE 'N'.
      *
       01  WK-NUM-QUO                      PIC X(002).
           88 WK-NUM-QUO-OK                VALUE 'OK'.
           88 WK-NUM-QUO-KO                VALUE 'KO'.

       77 WK-APP-ARRO                      PIC 9(001) VALUE 0.
       77 WK-APP-ARRO-1                    PIC 9(001) VALUE 0.
       01 WK-AREA-ARRO.
          05 WK-TAB-ARRO                   OCCURS 20.
             10 WK-IMP-ARRO                PIC X(1).
       01 WK-VARIABILI.
          03  WK-DT-RICOR-TRANCHE.
              04 WK-DT-RICOR-TRANCHE-AA  PIC 9(4).
              04 WK-DT-RICOR-TRANCHE-MM  PIC 9(2).
              04 WK-DT-RICOR-TRANCHE-GG  PIC 9(2).
          03  WK-DT-DECOR-TRCH-RED
              REDEFINES WK-DT-RICOR-TRANCHE PIC S9(8).
          03  WK-ANNO                    PIC 9(4).
          03  WK-DATA-APPO.
              04 WK-DATA-APPO-AA  PIC 9(4).
              04 WK-DATA-APPO-MM  PIC 9(2).
              04 WK-DATA-APPO-GG  PIC 9(2).
      *----------------------------------------------------------------*
      *  COPY PER CHIAMATA VALORE ASSET
      *----------------------------------------------------------------*
           COPY IDBVVAS1.
           COPY IDBVL191.
      *----------------------------------------------------------------*
      *  COPY PER CHIAMATE LDBS
      *----------------------------------------------------------------*
           COPY LDBV4911.
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-TGA.
          03 DTGA-AREA-TGA.
             04 DTGA-ELE-TGA-MAX        PIC S9(04) COMP.
                COPY LCCVTGAB REPLACING   ==(SF)==  BY ==DTGA==.
             COPY LCCVTGA1              REPLACING ==(SF)== BY ==DTGA==.

       01 AREA-IO-GAR.
          03 DGRZ-AREA-GRA.
             04 DGRZ-ELE-GAR-MAX        PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==DGRZ==.
             COPY LCCVGRZ1              REPLACING ==(SF)== BY ==DGRZ==.

       01 AREA-IO-L19.
          02 DL19-AREA-QUOTA.
             COPY LCCVL197              REPLACING ==(SF)== BY ==DL19==.
             COPY LCCVL191              REPLACING ==(SF)== BY ==DL19==.

       01 WVAS-AREA-VALORE-ASSET.
          04 WVAS-ELE-VAL-AST-MAX       PIC S9(004) COMP.
                COPY LCCVVASA REPLACING   ==(SF)==  BY ==WVAS==.
          COPY LCCVVAS1                 REPLACING ==(SF)== BY ==WVAS==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218                REPLACING ==(SF)==
                                               BY ==IVVC0218==.
      *----------------------------------------------------------------*
      *--  Routine date
       01  LCCS0003                         PIC X(008) VALUE 'LCCS0003'.
      *--  COPY PER SERVIZIO VERIFICA CALCOLI SU DATE
           COPY LCCC0003.
       01  IN-RCODE                       PIC 9(2) VALUE ZEROES.
      *----------------------------------------------------------------*

      *----------------------------------------------------------------*
      *    COPY VARIBILI PER LA GESTIONE ERRORI                        *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP VALUE 0.
           03 IX-TAB-GRZ                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-L19                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-TGA                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-VAS                     PIC S9(04) COMP VALUE 0.
           03 IX-GUIDA-GRZ                   PIC S9(04) COMP VALUE 0.
      *----------------------------------------------------------------*
       77  WK-LABEL                    PIC X(030).
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0007.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0007.
      *----------------------------------------------------------------*
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.
      *
           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000
      *
           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.
      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT
                                             WK-VARIABILI.

           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
           SET WK-NUM-QUO-OK                 TO TRUE.

           INITIALIZE VAL-AST
                      AREA-IO-TGA
                      AREA-IO-GAR
                      AREA-IO-L19.

           MOVE ZEROES
             TO DL19-ELE-FND-MAX
                WVAS-ELE-VAL-AST-MAX

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.
      *
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1200-CONTROLLO-DATI
                  THRU S1200-CONTROLLO-DATI-EX
      *
               IF  IDSV0003-SUCCESSFUL-RC
               AND IDSV0003-SUCCESSFUL-SQL

      *            La variabile deve essere valorizzata
      *            solo per prodotti Ramo III
                   IF DGRZ-TP-INVST(IX-GUIDA-GRZ) = 7 OR 8

      *               Calcolo prima la Data Ricorrenza
      *               Anniversaria di Tranche
                      PERFORM A191-CALCOLA-DT-RICOR-TRANCHE
                         THRU A191-EX

      *               Lettura Fondi
                      IF IDSV0003-SUCCESSFUL-RC
                         PERFORM A140-RECUP-VALORE-ASSET
                            THRU A140-EX
                      END-IF

      *               Lettura Quotazioni
                      IF IDSV0003-SUCCESSFUL-RC
                         PERFORM A160-RECUP-QUOTAZ-FONDO
                            THRU A160-EX
                         VARYING IX-TAB-VAS FROM 1 BY 1
                           UNTIL IX-TAB-VAS > WVAS-ELE-VAL-AST-MAX
                              OR NOT IDSV0003-SUCCESSFUL-RC
                      END-IF

      *               Calcolo della variabile
                      IF IDSV0003-SUCCESSFUL-RC
                         PERFORM S1250-CALCOLA-VARIABILE
                            THRU S1250-EX
                         VARYING IX-TAB-VAS FROM 1 BY 1
                           UNTIL IX-TAB-VAS > WVAS-ELE-VAL-AST-MAX
                              OR NOT IDSV0003-SUCCESSFUL-SQL
                      END-IF

                   ELSE
      *            Scarta la variabile
                   SET  IDSV0003-FIELD-NOT-VALUED      TO TRUE
                   MOVE WK-PGM              TO IDSV0003-COD-SERVIZIO-BE
                   STRING 'VARIABILE NON VALORIZZATA PERCHE'' IL '
                          'PRODOTTO NON E'' DI RAMO III'
                   DELIMITED BY SIZE   INTO IDSV0003-DESCRIZ-ERR-DB2
                   END-STRING

                   END-IF
               END-IF
           END-IF.

           IF IVVC0213-VAL-IMP-O EQUAL ZERO
              SET  IDSV0003-FIELD-NOT-VALUED      TO TRUE
              MOVE WK-PGM              TO IDSV0003-COD-SERVIZIO-BE
           END-IF.
      *
       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

      *    AREA TRANCHE
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-TRCH-GAR
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DTGA-AREA-TGA
           END-IF.

      *    AREA GARANZIA
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-GARANZIA
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DGRZ-AREA-GRA
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   CONTROLLO DATI DCLGEN
      *----------------------------------------------------------------*
       S1200-CONTROLLO-DATI.
      *
           SET WK-ID-COD-NON-TROVATO              TO TRUE.
      *
           IF DGRZ-ELE-GAR-MAX GREATER ZERO
      *
              PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
                UNTIL IX-TAB-GRZ > DGRZ-ELE-GAR-MAX
                   OR WK-ID-COD-TROVATO
                   IF DTGA-ID-GAR(IVVC0213-IX-TABB) =
                      DGRZ-ID-GAR(IX-TAB-GRZ)
                      SET WK-ID-COD-TROVATO     TO TRUE
                      MOVE IX-TAB-GRZ           TO IX-GUIDA-GRZ
                   END-IF
              END-PERFORM
      *
              IF WK-ID-COD-NON-TROVATO
                 SET  IDSV0003-INVALID-OPER               TO TRUE
                 MOVE WK-PGM
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'GARANZIA NON VALORIZZATA PER CALCOLO QUOTE'
                   TO IDSV0003-DESCRIZ-ERR-DB2
              END-IF
      *
           ELSE
              SET  IDSV0003-INVALID-OPER                  TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'GARANZIA NON VALORIZZATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
           END-IF.
      *
       S1200-CONTROLLO-DATI-EX.
           EXIT.

      *----------------------------------------------------------------*
      *  CALCOLA LA VARIABILE
      *----------------------------------------------------------------*
       S1250-CALCOLA-VARIABILE.

           SET WK-ID-COD-NON-TROVATO         TO TRUE.
      *
           PERFORM VARYING IX-TAB-L19 FROM 1 BY 1
             UNTIL IX-TAB-L19 > DL19-ELE-FND-MAX
                OR WK-ID-COD-TROVATO
                OR WK-NUM-QUO-KO

                IF WVAS-COD-FND(IX-TAB-VAS) = DL19-COD-FND(IX-TAB-L19)
                   SET WK-ID-COD-TROVATO        TO TRUE
      *
                   IF DL19-VAL-QUO(IX-TAB-L19) IS NUMERIC
      *--> PER IL PRIMO FONDO CHE HA NUM QUOTE A NULL ESCE DAL CICLO
                      IF WVAS-NUM-QUO(IX-TAB-VAS) IS NUMERIC
                         MOVE ZERO              TO WK-VAL-FONDO
                         COMPUTE WK-VAL-FONDO =
                         (WVAS-NUM-QUO(IX-TAB-VAS) *
                          DL19-VAL-QUO(IX-TAB-L19))
                         COMPUTE IVVC0213-VAL-IMP-O
                                  = (IVVC0213-VAL-IMP-O + WK-VAL-FONDO)
                       ELSE
                         SET WK-NUM-QUO-KO      TO TRUE
                       END-IF
                   ELSE
                      MOVE LDBS4910
                        TO IDSV0003-COD-SERVIZIO-BE
                      MOVE 'NUM-QUO / VAL-QUO NON NUMERICI'
                        TO IDSV0003-DESCRIZ-ERR-DB2
                       SET IDSV0003-INVALID-OPER  TO TRUE
                   END-IF
                END-IF
           END-PERFORM.

           PERFORM S1253-ARROTONDA-IMP
              THRU S1253-EX.

       S1250-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   ARROTONDAMENTO IMPORTO
      *----------------------------------------------------------------*
       S1253-ARROTONDA-IMP.
      *
           IF IVVC0213-VAL-IMP-O GREATER ZERO
              MOVE IVVC0213-VAL-IMP-O           TO WK-AREA-ARRO

              MOVE WK-IMP-ARRO(13)              TO WK-APP-ARRO
              MOVE WK-IMP-ARRO(14)              TO WK-APP-ARRO-1

              IF WK-APP-ARRO-1 NOT LESS 5
                 ADD 1                          TO WK-APP-ARRO
                 MOVE WK-APP-ARRO               TO WK-IMP-ARRO(13)
                 MOVE ZEROES                    TO WK-IMP-ARRO(14)
              ELSE
                 MOVE ZEROES                    TO WK-IMP-ARRO(14)
              END-IF
      *
              MOVE WK-AREA-ARRO                 TO IVVC0213-VAL-IMP-O
           ELSE
              SET  IDSV0003-FIELD-NOT-VALUED    TO TRUE
           END-IF.
      *
       S1253-EX.
           EXIT.

      *----------------------------------------------------------------*
      *  CONVERTI VALORE DA STRINGA IN NUMERICO
      *----------------------------------------------------------------*
       A191-CALCOLA-DT-RICOR-TRANCHE.

           INITIALIZE           IO-A2K-LCCC0003
                                IN-RCODE

      *    somma DELTA e conversione data
           MOVE '02'                        TO A2K-FUNZ

      *    FORMATO AAAAMMGG
           MOVE '03'                        TO A2K-INFO

      *    1 ANNO
           MOVE 1                           TO A2K-DELTA

      *    ANNI
           MOVE 'A'                         TO A2K-TDELTA

      *    GIORNI FISSI
           MOVE '0'                         TO A2K-FISLAV

      *    INIZIO CONTEGGIO DA STESSO GIORNO
           MOVE 0                           TO A2K-INICON

      *    DATA INPUT
      *    Data decorrenza di tranche
           MOVE IVVC0213-DT-DECOR
             TO A2K-INDATA

           PERFORM CALL-ROUTINE-DATE
              THRU CALL-ROUTINE-DATE-EX

      *    Se Data decorrenza di tranche + 1 anno h > della Data
      *    calcolo Riserva allora la data di ricorrenza
      *    h la Data decorrenza di tranche
           IF IDSV0003-SUCCESSFUL-RC
      *       DATA OUTPUT (CALCOLATA)
              IF A2K-OUAMG > IVVC0213-DATA-EFFETTO
                 MOVE IVVC0213-DT-DECOR
                   TO WK-DT-RICOR-TRANCHE
              ELSE
      *          Altrimenti calcolare data ricorrenza con gg e mm
      *          di decorrenza tranche e anno = anno di calcolo
      *          riserva
                 MOVE ZEROES
                   TO WK-DT-RICOR-TRANCHE
                 MOVE IVVC0213-DT-DECOR
                   TO WK-DATA-APPO
                 MOVE WK-DATA-APPO-GG
                   TO WK-DT-RICOR-TRANCHE-GG
                 MOVE WK-DATA-APPO-MM
                   TO WK-DT-RICOR-TRANCHE-MM
                 MOVE WK-DATA-APPO-AA
                   TO WK-DT-RICOR-TRANCHE-AA

      *          Se la data cosl calcolata h < = alla data di calcolo
      *          riserva allora impostare la data ricorrenza
      *          con tale data
                 IF WK-DT-RICOR-TRANCHE <= IVVC0213-DATA-EFFETTO
                    CONTINUE
                 ELSE
      *             Altrimenti (se > di data calcolo riserva)
      *             sottrarre 1 anno e valorizzare la data
      *             ricorrenza con tale data
                    MOVE WK-DT-RICOR-TRANCHE-AA
                      TO WK-ANNO
                    SUBTRACT 1
                        FROM WK-ANNO
                    MOVE WK-ANNO
                      TO WK-DT-RICOR-TRANCHE-AA
                 END-IF

              END-IF

           END-IF.

       A191-EX.
           EXIT.

      *----------------------------------------------------------------*
      *  CHIAMATA AL SERVIZIO CALCOLA DATA
      *----------------------------------------------------------------*
       CALL-ROUTINE-DATE.

           CALL LCCS0003      USING IO-A2K-LCCC0003
                                    IN-RCODE

           ON EXCEPTION
              SET  IDSV0003-INVALID-OPER                  TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ROUTINE CALCOLO DATA (LCCS0003)'
                TO IDSV0003-DESCRIZ-ERR-DB2
           END-CALL.

           IF IN-RCODE  = '00'
              CONTINUE
           ELSE
              SET  IDSV0003-INVALID-OPER                  TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ROUTINE CALCOLO DATA (LCCS0003)'
                TO IDSV0003-DESCRIZ-ERR-DB2
           END-IF.

       CALL-ROUTINE-DATE-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    LETTURA DATI TABELLA VALORE ASSET
      *----------------------------------------------------------------*
       A140-RECUP-VALORE-ASSET.

           MOVE 'A140-RECUP-VALORE-ASSET'
             TO WK-LABEL

           MOVE ZEROES
             TO IX-TAB-VAS

      *--> TIPO OPERAZIONE
           SET IDSV0003-FETCH-FIRST       TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
      *
           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR NOT IDSV0003-SUCCESSFUL-SQL
                      OR WK-NUM-QUO-KO

              INITIALIZE LDBV4911
              MOVE SPACES                TO LDBV4911-TP-VAL-AST-1
              MOVE SPACES                TO LDBV4911-TP-VAL-AST-2
              MOVE IVVC0213-ID-TRANCHE   TO LDBV4911-ID-TRCH-DI-GAR
              MOVE LDBV4911              TO IDSV0003-BUFFER-WHERE-COND


      *--> LIVELLO OPERAZIONE
              SET IDSV0003-WHERE-CONDITION TO TRUE

              CALL LDBS4910  USING  IDSV0003 VAL-AST
              ON EXCEPTION
                 MOVE LDBS4910
                   TO IDSV0003-COD-SERVIZIO-BE
                MOVE 'ERRORE CALL LDBS4910'
                   TO IDSV0003-DESCRIZ-ERR-DB2
                   SET IDSV0003-INVALID-OPER  TO TRUE
              END-CALL

                    EVALUATE TRUE
                        WHEN IDSV0003-NOT-FOUND
      *-->    NESSUN DATO IN TABELLA
                             CONTINUE

                        WHEN IDSV0003-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE

                             ADD 1
                              TO IX-TAB-VAS

                             PERFORM INIZIA-TOT-VAS
                             PERFORM INIZIA-TOT-VAS-EX

                             PERFORM VALORIZZA-OUTPUT-VAS
                                THRU VALORIZZA-OUTPUT-VAS-EX

                              SET WVAS-ST-INV(IX-TAB-VAS)
                               TO TRUE

                             MOVE IX-TAB-VAS
                               TO WVAS-ELE-VAL-AST-MAX

                             SET IDSV0003-FETCH-NEXT   TO TRUE

                        WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB

                    SET IDSV0003-INVALID-OPER  TO TRUE
                    MOVE WK-PGM
                      TO IDSV0003-COD-SERVIZIO-BE
                    STRING 'ERRORE LETTURA TABELLA VALORE ASSET ;'
                          IDSV0003-RETURN-CODE ';'
                          IDSV0003-SQLCODE
                      DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    END-STRING

                    END-EVALUATE

           END-PERFORM

           IF WK-NUM-QUO-KO
              PERFORM S1252-CHIUDE-CURVAS         THRU S1252-EX
           END-IF.

       A140-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   CHIUSURA CURSORE VALORE ASSET VALORE ASSETT
      *----------------------------------------------------------------*
       S1252-CHIUDE-CURVAS.
      *
           SET IDSV0003-CLOSE-CURSOR            TO TRUE.
      *
           CALL LDBS4910 USING  IDSV0003 VAL-AST
             ON EXCEPTION
                MOVE LDBS4910
                  TO IDSV0003-COD-SERVIZIO-BE
                MOVE 'ERRORE CALL LDBS4910'
                  TO IDSV0003-DESCRIZ-ERR-DB2
                 SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
      *
           IF NOT IDSV0003-SUCCESSFUL-RC
           OR NOT IDSV0003-SUCCESSFUL-SQL
              MOVE LDBS4910
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CALL LDBS4910'
                TO IDSV0003-DESCRIZ-ERR-DB2
               SET IDSV0003-INVALID-OPER
                TO TRUE
           END-IF.
      *
       S1252-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    LETTURA QUOTAZIONI FONDO
      *----------------------------------------------------------------*
       A160-RECUP-QUOTAZ-FONDO.

           MOVE 'A160-RECUP-QUOTAZ-FONDO'
             TO WK-LABEL

           INITIALIZE QUOTZ-FND-UNIT

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.

           MOVE ZEROES
             TO IX-TAB-L19

           SET IDSV0003-TRATT-SENZA-STOR  TO TRUE.
      *--> LIVELLO OPERAZIONE
           SET IDSV0003-WHERE-CONDITION   TO TRUE.
      *--> TIPO OPERAZIONE
           SET IDSV0003-SELECT            TO TRUE.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                          TO L19-COD-COMP-ANIA.
           MOVE WVAS-COD-FND(IX-TAB-VAS)  TO L19-COD-FND.
           MOVE WK-DT-DECOR-TRCH-RED      TO L19-DT-QTZ.

           CALL LDBS2080  USING  IDSV0003 QUOTZ-FND-UNIT
           ON EXCEPTION
              MOVE LDBS4910
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'ERRORE CALL LDBS4910'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL

                 EVALUATE TRUE
                     WHEN IDSV0003-SUCCESSFUL-SQL
      *-->    OPERAZIONE ESEGUITA CORRETTAMENTE

                            ADD 1
                             TO IX-TAB-L19

                            PERFORM INIZIA-TOT-L19
                            PERFORM INIZIA-TOT-L19-EX

                            PERFORM VALORIZZA-OUTPUT-L19
                               THRU VALORIZZA-OUTPUT-L19-EX

                             SET DL19-ST-INV(IX-TAB-L19)
                              TO TRUE

                            MOVE IX-TAB-L19
                              TO DL19-ELE-FND-MAX

                     WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB

                    SET IDSV0003-INVALID-OPER  TO TRUE
                    MOVE WK-PGM
                      TO IDSV0003-COD-SERVIZIO-BE
                    STRING 'ERRORE LETTURA TABELLA QUOTZ-FND-UNIT ;'
                          IDSV0003-RETURN-CODE ';'
                          IDSV0003-SQLCODE
                      DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    END-STRING

                 END-EVALUATE.

       A160-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   VALORIZZA OUTPUT
      *----------------------------------------------------------------*
       VALORIZZA-OUTPUT-L19.

           MOVE L19-COD-COMP-ANIA
             TO DL19-COD-COMP-ANIA(IX-TAB-L19)
           MOVE L19-COD-FND
             TO DL19-COD-FND(IX-TAB-L19)
           MOVE L19-DT-QTZ
             TO DL19-DT-QTZ(IX-TAB-L19)
           IF L19-VAL-QUO-NULL = HIGH-VALUES
              MOVE L19-VAL-QUO-NULL
                TO DL19-VAL-QUO-NULL(IX-TAB-L19)
           ELSE
              MOVE L19-VAL-QUO
                TO DL19-VAL-QUO(IX-TAB-L19)
           END-IF
           IF L19-VAL-QUO-MANFEE-NULL = HIGH-VALUES
              MOVE L19-VAL-QUO-MANFEE-NULL
                TO DL19-VAL-QUO-MANFEE-NULL(IX-TAB-L19)
           ELSE
              MOVE L19-VAL-QUO-MANFEE
                TO DL19-VAL-QUO-MANFEE(IX-TAB-L19)
           END-IF
           MOVE L19-DS-OPER-SQL
             TO DL19-DS-OPER-SQL(IX-TAB-L19)
           MOVE L19-DS-VER
             TO DL19-DS-VER(IX-TAB-L19)
           MOVE L19-DS-TS-CPTZ
             TO DL19-DS-TS-CPTZ(IX-TAB-L19)
           MOVE L19-DS-UTENTE
             TO DL19-DS-UTENTE(IX-TAB-L19)
           MOVE L19-DS-STATO-ELAB
             TO DL19-DS-STATO-ELAB(IX-TAB-L19)
           MOVE L19-TP-FND
             TO DL19-TP-FND(IX-TAB-L19)
           IF L19-DT-RILEVAZIONE-NAV-NULL = HIGH-VALUES
              MOVE L19-DT-RILEVAZIONE-NAV-NULL
                TO DL19-DT-RILEVAZIONE-NAV-NULL(IX-TAB-L19)
           ELSE
              MOVE L19-DT-RILEVAZIONE-NAV
                TO DL19-DT-RILEVAZIONE-NAV(IX-TAB-L19)
           END-IF.

       VALORIZZA-OUTPUT-L19-EX.
           EXIT.

       INIZIA-TOT-L19.

           PERFORM INIZIA-ZEROES-L19 THRU INIZIA-ZEROES-L19-EX

           PERFORM INIZIA-SPACES-L19 THRU INIZIA-SPACES-L19-EX

           PERFORM INIZIA-NULL-L19 THRU INIZIA-NULL-L19-EX.

       INIZIA-TOT-L19-EX.
           EXIT.

       INIZIA-NULL-L19.

           MOVE HIGH-VALUES TO DL19-VAL-QUO-NULL(IX-TAB-L19)
           MOVE HIGH-VALUES TO DL19-VAL-QUO-MANFEE-NULL(IX-TAB-L19)
           MOVE HIGH-VALUES TO DL19-DT-RILEVAZIONE-NAV-NULL(IX-TAB-L19).

       INIZIA-NULL-L19-EX.
           EXIT.

       INIZIA-ZEROES-L19.

           MOVE 0 TO DL19-COD-COMP-ANIA(IX-TAB-L19)
           MOVE 0 TO DL19-DT-QTZ(IX-TAB-L19)
           MOVE 0 TO DL19-DS-VER(IX-TAB-L19)
           MOVE 0 TO DL19-DS-TS-CPTZ(IX-TAB-L19).

       INIZIA-ZEROES-L19-EX.
           EXIT.

       INIZIA-SPACES-L19.

           MOVE SPACES TO DL19-COD-FND(IX-TAB-L19)
           MOVE SPACES TO DL19-DS-OPER-SQL(IX-TAB-L19)
           MOVE SPACES TO DL19-DS-UTENTE(IX-TAB-L19)
           MOVE SPACES TO DL19-DS-STATO-ELAB(IX-TAB-L19)
           MOVE SPACES TO DL19-TP-FND(IX-TAB-L19).

       INIZIA-SPACES-L19-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZA OUTPUT TABELLE
      *----------------------------------------------------------------*
      *--> VALORE ASSET
           COPY LCCVVAS3               REPLACING ==(SF)== BY ==WVAS==.
      *----------------------------------------------------------------*
      *    COPY INIZIALIZZAZIONE AREE OUTPUT TABELLE
      *----------------------------------------------------------------*
      *--> VALORE ASSET
           COPY LCCVVAS4               REPLACING ==(SF)== BY ==WVAS==.

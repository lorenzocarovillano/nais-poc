      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0093.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0093'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
      * --> MODULO PER CALCOLO DELLA DATA
11258  01  LCCS0003                       PIC X(8) VALUE 'LCCS0003'.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-DATA-APP               PIC 9(08).
       01  WK-DATA-APP-RED           REDEFINES
           WK-DATA-APP.
           03 WK-AA                  PIC 9(04).
           03 WK-MM                  PIC 9(02).
           03 WK-GG                  PIC 9(02).
       01 IN-RCODE                   PIC 9(02).
      *---------------------------------------------------------------
      *  COPY PER CHIAMATA LDBS2920
      *---------------------------------------------------------------
           COPY LDBV2921.
      *---------------------------------------------------------------
      *  AREA DI COMUNICAZIONE CON LA ROUTINE CALCOLO DATA
      *---------------------------------------------------------------
11258      COPY LCCC0003.
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-POLI.
          03 DPOL-AREA-POLI.
             04 DPOL-ELE-POLI-MAX       PIC S9(04) COMP.
             04 DPOL-TAB-POLI.
             COPY LCCVPOL1              REPLACING ==(SF)== BY ==DPOL==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
           03 IX-TAB-TGA                     PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0007.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0007.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           INITIALIZE LDBV2921.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.
      *
      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL

               MOVE IVVC0213-ID-POLIZZA        TO LDBV2921-ID-POL
               MOVE DPOL-DT-EMIS               TO LDBV2921-DT-EMIS

11258 *        MOVE DPOL-DT-DECOR              TO WK-DATA-APP
 "    *        ADD 5                           TO WK-AA
 "    *        MOVE WK-DATA-APP                TO LDBV2921-DT-DECOR-MAX

      *  --> AGGIUNGI 5 ANNI ALLA DATA DECORRENZA DELLA POLIZZA
11258          PERFORM S60120-PREPARA-LCCS0003
 "                THRU S60120-PREPARA-LCCS0003-EX
 "             PERFORM S60130-CALL-LCCS0003
 "                THRU S60130-CALL-LCCS0003-EX

      *  --> TIPO OPERAZIONE
               SET IDSV0003-SELECT             TO TRUE
      *  --> TIPO STORICIT`
               SET IDSV0003-TRATT-X-COMPETENZA TO TRUE
      *  --> TIPO LIVELLO
               SET IDSV0003-WHERE-CONDITION    TO TRUE

               PERFORM S1250-CALCOLA-DATA1     THRU S1250-EX
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *   VALORIZZAZIONE INPUT MODULO LCCS0003
      *----------------------------------------------------------------*
       S60120-PREPARA-LCCS0003.
      *
      * --> A2K-INAMG -> Data Di Partenza Alla Quale Sommare Il Delta
      *
           MOVE DPOL-DT-DECOR
             TO A2K-INAMG.
      *
      * --> A2K-TDELTA -> Tipo Delta: Mesi(M) Anni(A)
      *
           MOVE 'A'
             TO A2K-TDELTA
      *
      * --> A2K-DELTA  -> Delta Da Sommare
      *
           MOVE 5
             TO A2K-DELTA
      *
      * --> A2K-FUNZ -> Tipo Funzione: Somma Delta
      *
           MOVE '02'
             TO A2K-FUNZ.
      *
      * --> A2K-INFO -> Tipo Formato Data: AAAAMMGG
      *
           MOVE '03'
             TO A2K-INFO.
      *
      * --> A2K-INICON -> Giorno Inizio Conteggio (Stesso Giorno)
      *
           MOVE '0'
             TO A2K-INICON.
      *
      * --> A2K-FISLAV -> Tipo Giorni Da Sommare (Fissi)
      *
           MOVE '0'
             TO A2K-FISLAV.
      *
       S60120-PREPARA-LCCS0003-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *   CHIAMATA AL MODULO LCCS0003
      *----------------------------------------------------------------*
       S60130-CALL-LCCS0003.
      *
           CALL LCCS0003         USING IO-A2K-LCCC0003
                                       IN-RCODE
           ON EXCEPTION
      *
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL-LCCS0003 ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER    TO TRUE
      *
           END-CALL.
      *
           IF IN-RCODE NOT = '00'
      *
              MOVE WK-CALL-PGM               TO IDSV0003-COD-SERVIZIO-BE
              STRING 'ERRORE CHIAMATA LCCS0003 ;'
                     IN-RCODE
                     DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
           ELSE
              MOVE A2K-OUAMG         TO LDBV2921-DT-DECOR-MAX
           END-IF.
      *
       S60130-CALL-LCCS0003-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-POLI
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DPOL-AREA-POLI
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA DATA 1
      *----------------------------------------------------------------*
       S1250-CALCOLA-DATA1.
      *
           MOVE 'LDBS2920'                     TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 LDBV2921
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS2920 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.


           IF IDSV0003-SUCCESSFUL-SQL
              MOVE LDBV2921-DT-EFF         TO IVVC0213-VAL-IMP-O
           ELSE
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA LDBS2920 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
              OR IDSV0003-SQLCODE = -305
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                 MOVE ZERO  TO IDSV0003-SQLCODE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF
           END-IF.
      *
       S1250-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.


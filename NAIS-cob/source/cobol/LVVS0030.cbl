      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0030.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0030
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... MODULO DI CALCOLO
      *  DESCRIZIONE.... Et` SECONDO Assicurato alla decorrenza
      *                  Tranche (AA,MM)
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

      *----------------------------------------------------------------*
      *    PROGRAMMI
      *----------------------------------------------------------------*
       01  WK-PGM                        PIC X(008) VALUE 'LVVS0030'.
       01  PGM-LVVS0000                  PIC X(008) VALUE 'LVVS0000'.

       01  WK-DATA-INPUT.
           03 WK-AAAA-INPUT              PIC 9(04).
           03 WK-MM-INPUT                PIC 9(02).
           03 WK-GG-INPUT                PIC 9(02).

       01  IX-INDICI.
           03 IND-STR                    PIC S9(04) COMP.
           03 IND-TGA                    PIC S9(04) COMP.

       01 ALIAS-TRANCHE                  PIC X(3) VALUE 'TGA'.

       01 FLAG-AREA-TROVATA              PIC X(01).
          88 AREA-TROVATA-SI             VALUE 'S'.
          88 AREA-TROVATA-NO             VALUE 'N'.

       01 FLAG-STRUTTURA-TROVATA         PIC X(01).
          88 STRUTTURA-TROVATA-SI        VALUE 'S'.
          88 STRUTTURA-TROVATA-NO        VALUE 'N'.

       01 AREA-BUSINESS.
      *--  AREA TRANCHE DI GARANZIA
           03 WTGA-AREA-TRANCHE.
              04 WTGA-ELE-TRAN-MAX       PIC S9(04) COMP.
                COPY LCCVTGAC REPLACING   ==(SF)==  BY ==WTGA==.
              COPY LCCVTGA1              REPLACING ==(SF)== BY ==WTGA==.

       01 LIMITI.
          05 LIMITE-TGA                  PIC 9(03) VALUE 300.

       01  INPUT-LVVS0000.
           COPY LVVC0000        REPLACING ==(SF)== BY ==LVVC0000==.

           COPY LCCVTGAZ.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0030.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0030.
      *----------------------------------------------------------------*

           PERFORM L000-OPERAZIONI-INIZIALI
              THRU L000-EX.

           PERFORM L100-ELABORAZIONE
              THRU L100-EX.

           PERFORM L900-OPERAZIONI-FINALI
              THRU L900-EX.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       L000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           SET AREA-TROVATA-NO               TO TRUE.
           SET STRUTTURA-TROVATA-NO          TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE      TO IVVC0213-TAB-OUTPUT.

       L000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       L100-ELABORAZIONE.
      *

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE

           PERFORM L500-VALORIZZA-DCLGEN THRU L500-EX
                   VARYING IND-STR FROM 1 BY 1
                     UNTIL IND-STR > IVVC0213-ELE-INFO-MAX OR
                           IVVC0213-TAB-ALIAS(IND-STR) =
                      SPACES OR LOW-VALUE OR HIGH-VALUE

           IF STRUTTURA-TROVATA-NO
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING 'STRUTTURA NON TROVATA'
                     ' : '
                     ALIAS-TRANCHE
                     DELIMITED BY SIZE INTO
                     IDSV0003-DESCRIZ-ERR-DB2
               END-STRING

                SET IDSV0003-FIELD-NOT-VALUED    TO TRUE
           END-IF.

           IF AREA-TROVATA-NO
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING 'AREA NON TROVATA'
                     ' : '
                     ALIAS-TRANCHE
                     DELIMITED BY SIZE INTO
                     IDSV0003-DESCRIZ-ERR-DB2
               END-STRING

                SET IDSV0003-FIELD-NOT-VALUED    TO TRUE
           END-IF.
      *
       L100-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE DI DISTRIBUZIONE
      *----------------------------------------------------------------*
       L500-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IND-STR) = ALIAS-TRANCHE
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IND-STR) :
                   IVVC0213-LUNGHEZZA(IND-STR))
                TO WTGA-AREA-TRANCHE

                SET AREA-TROVATA-SI          TO TRUE
                PERFORM L550-CERCA-TRANCHE   THRU L550-EX

           END-IF.

       L500-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CERCA LA TRANCHE DA ELABORARE
      *----------------------------------------------------------------*
       L550-CERCA-TRANCHE.

           PERFORM  VARYING IND-TGA FROM 1 BY 1
                     UNTIL  IND-TGA > WTGA-ELE-TRAN-MAX OR
                            IND-TGA > WK-TGA-MAX-C      OR
                     WTGA-ID-TRCH-DI-GAR(IND-TGA)
                     NOT NUMERIC                        OR
                     WTGA-ID-TRCH-DI-GAR(IND-TGA) = 0

                     IF IVVC0213-ID-LIVELLO =
                        WTGA-ID-TRCH-DI-GAR(IND-TGA)

                        SET STRUTTURA-TROVATA-SI   TO TRUE
                        PERFORM L700-CALL-LVVS0000 THRU L700-EX

                     END-IF

           END-PERFORM.

       L550-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPARA CALL LVVS0000
      *----------------------------------------------------------------*
       L600-PREPARA-CALL.

      *--> COPY LVVC0000
           INITIALIZE INPUT-LVVS0000
                      WK-DATA-INPUT.

           SET LVVC0000-AAA-V-MM          TO TRUE.

           MOVE WTGA-ETA-AA-2O-ASSTO(IND-TGA) TO WK-AAAA-INPUT.
           MOVE WTGA-ETA-MM-2O-ASSTO(IND-TGA) TO WK-MM-INPUT.

           MOVE WK-DATA-INPUT             TO LVVC0000-DATA-INPUT-1.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET IDSV0003-SUCCESSFUL-RC     TO TRUE
           SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.

       L600-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALL LDBS1650
      *----------------------------------------------------------------*
       L700-CALL-LVVS0000.
      *
           PERFORM L600-PREPARA-CALL  THRU L600-EX

           CALL PGM-LVVS0000  USING  IDSV0003 INPUT-LVVS0000
      *
           ON EXCEPTION
              MOVE PGM-LVVS0000
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL LVVS0000 ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE LVVC0000-DATA-OUTPUT    TO IVVC0213-VAL-IMP-O
           ELSE
              MOVE PGM-LVVS0000            TO IDSV0003-COD-SERVIZIO-BE

              STRING 'ERRORE ELABORAZIONE LVVS0000'
                     IDSV0003-RETURN-CODE
                     DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING

           END-IF.
      *
       L700-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       L900-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       L900-EX.
           EXIT.

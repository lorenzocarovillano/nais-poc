      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0095.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0007
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0095'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01 WK-GG-APP                        PIC S9(05) VALUE 0.

       01 WK-DATA-OUT                      PIC 9(08) VALUE ZEROES.

       01 WK-DATA-X-12.
          03 WK-X-AA                          PIC X(04).
          03 WK-VIROGLA                       PIC X(01) VALUE ','.
          03 WK-X-GG                          PIC X(07).

      *-- AREA ROUTINE PER IL CALCOLO
       01  FORMATO                 PIC X(01).
       01  DATA-INFERIORE.
           05 AAAA-INF             PIC 9(04).
           05 MM-INF               PIC 9(02).
           05 GG-INF               PIC 9(02).
       01  DATA-SUPERIORE.
           05 AAAA-SUP             PIC 9(04).
           05 MM-SUP               PIC 9(02).
           05 GG-SUP               PIC 9(02).
       01  GG-DIFF                 PIC 9(05).
       01  CODICE-RITORNO          PIC X(01).

       01  WK-POLIZZA                     PIC X(001).
           88 WK-POLI-OK                    VALUE 'S'.
           88 WK-POLI-KO                    VALUE 'N'.
      *---------------------------------------------------------------*
      *  COPY PER CHIAMATA LDBS
      *---------------------------------------------------------------*
           COPY LDBV2971.
           COPY LDBV2961.
10819X*    COPY LCCVXMV0.
10819X     COPY LCCVXMVZ.
10819X     COPY LCCVXMV2.
10819X     COPY LCCVXMV3.
10819X     COPY LCCVXMV4.
10819X     COPY LCCVXMV5.
      *---------------------------------------------------------------*
      *
           COPY IDBVSTB1.
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-TGA.
          03 DTGA-AREA-TGA.
             04 DTGA-ELE-TGA-MAX        PIC S9(04) COMP.
                COPY LCCVTGAC REPLACING   ==(SF)==  BY ==DTGA==.
             COPY LCCVTGA1              REPLACING ==(SF)== BY ==DTGA==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
           03 IX-TAB-TGA                     PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0007.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0007.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT
                                             WK-DATA-OUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
           SET WK-POLI-OK                    TO TRUE.

           INITIALIZE LDBV2971.

           MOVE IDSV0003-TIPO-MOVIMENTO
             TO WS-MOVIMENTO.
           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE AREA-IO-TGA
                      WK-DATA-X-12.
      *
      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
           AND ( VARIA-RIDUZI
40976      OR  RISTO-INDIVI
10819X     OR COMUN-RISTOT-INCAPIENZA
FNZS2      OR COMUN-RISTOT-INCAP)
               CONTINUE
           ELSE
               PERFORM S1250-VERIFICA-POLI         THRU S1250-EX
           END-IF.
      *
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
             IF WK-POLI-OK
               PERFORM S1255-CALCOLA-DATA2         THRU S1255-EX
             END-IF
           END-IF.
      *
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1260-CALCOLA-DIFF          THRU S1260-EX
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-TRCH-GAR
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DTGA-AREA-TGA
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA DATAULTADEGP
      *----------------------------------------------------------------*
       S1250-VERIFICA-POLI.
      *
           INITIALIZE STAT-OGG-BUS
                      LDBV2961.
           MOVE IVVC0213-ID-POLIZZA            TO STB-ID-OGG.
           MOVE 'PO'                           TO STB-TP-OGG.
           MOVE 'ST'                           TO STB-TP-STAT-BUS.
           MOVE 'AP'                           TO LDBV2961-TP-CAUS-01.
           MOVE 'MP'                           TO LDBV2961-TP-CAUS-02.
      *
           MOVE SPACES
             TO IDSV0003-BUFFER-WHERE-COND.

           MOVE LDBV2961
             TO IDSV0003-BUFFER-WHERE-COND.
      *
           MOVE 'LDBS2960'                     TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 STAT-OGG-BUS
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS2960 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              CONTINUE
           ELSE
              IF IDSV0003-SQLCODE = +100
                 SET IDSV0003-SUCCESSFUL-SQL      TO TRUE
                 SET IDSV0003-SUCCESSFUL-RC       TO TRUE
                 SET WK-POLI-KO                   TO TRUE
                 MOVE ZERO                        TO IDSV0003-SQLCODE
                 MOVE 0                           TO IVVC0213-VAL-IMP-O
              ELSE
                 SET IDSV0003-INVALID-OPER        TO TRUE
                 MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
                 STRING 'CHIAMATA LDBS2960 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                     DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                 END-STRING
              END-IF
           END-IF.
      *
       S1250-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA
      *----------------------------------------------------------------*
       S1255-CALCOLA-DATA2.
      *
           INITIALIZE LDBV2971.
           MOVE DTGA-ID-TRCH-DI-GAR(IVVC0213-IX-TABB)
                                               TO LDBV2971-ID-OGG.
           MOVE 'TG'                           TO LDBV2971-TP-OGG.
      *
           MOVE 'LDBS2970'                     TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 LDBV2971
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS2970 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              CONTINUE
           ELSE
              IF IDSV0003-NOT-FOUND
              OR IDSV0003-SQLCODE = -305
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF
      *
              MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA LDBS2970 ;'
                  IDSV0003-RETURN-CODE ';'
                  IDSV0003-SQLCODE
                  DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
      *
           END-IF.
      *
       S1255-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA DATA 1
      *----------------------------------------------------------------*
       S1260-CALCOLA-DIFF.
      *
           MOVE 'A'                               TO FORMATO.
           MOVE DTGA-DT-DECOR(IVVC0213-IX-TABB)   TO WK-DATA-OUT.
           MOVE WK-DATA-OUT                       TO DATA-INFERIORE.
           MOVE LDBV2971-DT-INI-COP               TO DATA-SUPERIORE.
           MOVE 'LCCS0010'                        TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING FORMATO,
                                   DATA-INFERIORE,
                                   DATA-SUPERIORE,
                                   GG-DIFF,
                                   CODICE-RITORNO
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS1660 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
      *
           IF CODICE-RITORNO = ZERO
              IF LDBV2971-FRAZ GREATER ZEROES
*******SIR 19310 ******
                 IF LDBV2971-NUM-RAT-ACCORPATE > 0
                   COMPUTE WK-GG-APP = ((360 / LDBV2971-FRAZ) *
                                            LDBV2971-NUM-RAT-ACCORPATE)
                 ELSE
                   COMPUTE WK-GG-APP = (360 / LDBV2971-FRAZ)
                 END-IF
*******SIR 19310 ******
              ELSE
                 MOVE ZEROES               TO WK-GG-APP
              END-IF
              COMPUTE IVVC0213-VAL-IMP-O = ((GG-DIFF + WK-GG-APP) / 360)
           ELSE
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA LCCS0010 COD-RIT:'
                      CODICE-RITORNO ';'
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
           END-IF.
      *
       S1260-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.

       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSRIC0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  21 NOV 2013.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDRIC0 END-EXEC.
           EXEC SQL INCLUDE IDBVRIC2 END-EXEC.
           EXEC SQL INCLUDE IDBVRIC3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVRIC1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 RICH.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSRIC0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'RICH' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_RICH
                ,COD_COMP_ANIA
                ,TP_RICH
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_RICH
                ,DT_EFF
                ,DT_RGSTRZ_RICH
                ,DT_PERV_RICH
                ,DT_ESEC_RICH
                ,TS_EFF_ESEC_RICH
                ,ID_OGG
                ,TP_OGG
                ,IB_POLI
                ,IB_ADES
                ,IB_GAR
                ,IB_TRCH_DI_GAR
                ,ID_BATCH
                ,ID_JOB
                ,FL_SIMULAZIONE
                ,KEY_ORDINAMENTO
                ,ID_RICH_COLLG
                ,TP_RAMO_BILA
                ,TP_FRM_ASSVA
                ,TP_CALC_RIS
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,RAMO_BILA
             INTO
                :RIC-ID-RICH
               ,:RIC-COD-COMP-ANIA
               ,:RIC-TP-RICH
               ,:RIC-COD-MACROFUNCT
               ,:RIC-TP-MOVI
               ,:RIC-IB-RICH
                :IND-RIC-IB-RICH
               ,:RIC-DT-EFF-DB
               ,:RIC-DT-RGSTRZ-RICH-DB
               ,:RIC-DT-PERV-RICH-DB
               ,:RIC-DT-ESEC-RICH-DB
               ,:RIC-TS-EFF-ESEC-RICH
                :IND-RIC-TS-EFF-ESEC-RICH
               ,:RIC-ID-OGG
                :IND-RIC-ID-OGG
               ,:RIC-TP-OGG
                :IND-RIC-TP-OGG
               ,:RIC-IB-POLI
                :IND-RIC-IB-POLI
               ,:RIC-IB-ADES
                :IND-RIC-IB-ADES
               ,:RIC-IB-GAR
                :IND-RIC-IB-GAR
               ,:RIC-IB-TRCH-DI-GAR
                :IND-RIC-IB-TRCH-DI-GAR
               ,:RIC-ID-BATCH
                :IND-RIC-ID-BATCH
               ,:RIC-ID-JOB
                :IND-RIC-ID-JOB
               ,:RIC-FL-SIMULAZIONE
                :IND-RIC-FL-SIMULAZIONE
               ,:RIC-KEY-ORDINAMENTO-VCHAR
                :IND-RIC-KEY-ORDINAMENTO
               ,:RIC-ID-RICH-COLLG
                :IND-RIC-ID-RICH-COLLG
               ,:RIC-TP-RAMO-BILA
                :IND-RIC-TP-RAMO-BILA
               ,:RIC-TP-FRM-ASSVA
                :IND-RIC-TP-FRM-ASSVA
               ,:RIC-TP-CALC-RIS
                :IND-RIC-TP-CALC-RIS
               ,:RIC-DS-OPER-SQL
               ,:RIC-DS-VER
               ,:RIC-DS-TS-CPTZ
               ,:RIC-DS-UTENTE
               ,:RIC-DS-STATO-ELAB
               ,:RIC-RAMO-BILA
                :IND-RIC-RAMO-BILA
             FROM RICH
             WHERE     ID_RICH = :RIC-ID-RICH

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO RICH
                     (
                        ID_RICH
                       ,COD_COMP_ANIA
                       ,TP_RICH
                       ,COD_MACROFUNCT
                       ,TP_MOVI
                       ,IB_RICH
                       ,DT_EFF
                       ,DT_RGSTRZ_RICH
                       ,DT_PERV_RICH
                       ,DT_ESEC_RICH
                       ,TS_EFF_ESEC_RICH
                       ,ID_OGG
                       ,TP_OGG
                       ,IB_POLI
                       ,IB_ADES
                       ,IB_GAR
                       ,IB_TRCH_DI_GAR
                       ,ID_BATCH
                       ,ID_JOB
                       ,FL_SIMULAZIONE
                       ,KEY_ORDINAMENTO
                       ,ID_RICH_COLLG
                       ,TP_RAMO_BILA
                       ,TP_FRM_ASSVA
                       ,TP_CALC_RIS
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,RAMO_BILA
                     )
                 VALUES
                     (
                       :RIC-ID-RICH
                       ,:RIC-COD-COMP-ANIA
                       ,:RIC-TP-RICH
                       ,:RIC-COD-MACROFUNCT
                       ,:RIC-TP-MOVI
                       ,:RIC-IB-RICH
                        :IND-RIC-IB-RICH
                       ,:RIC-DT-EFF-DB
                       ,:RIC-DT-RGSTRZ-RICH-DB
                       ,:RIC-DT-PERV-RICH-DB
                       ,:RIC-DT-ESEC-RICH-DB
                       ,:RIC-TS-EFF-ESEC-RICH
                        :IND-RIC-TS-EFF-ESEC-RICH
                       ,:RIC-ID-OGG
                        :IND-RIC-ID-OGG
                       ,:RIC-TP-OGG
                        :IND-RIC-TP-OGG
                       ,:RIC-IB-POLI
                        :IND-RIC-IB-POLI
                       ,:RIC-IB-ADES
                        :IND-RIC-IB-ADES
                       ,:RIC-IB-GAR
                        :IND-RIC-IB-GAR
                       ,:RIC-IB-TRCH-DI-GAR
                        :IND-RIC-IB-TRCH-DI-GAR
                       ,:RIC-ID-BATCH
                        :IND-RIC-ID-BATCH
                       ,:RIC-ID-JOB
                        :IND-RIC-ID-JOB
                       ,:RIC-FL-SIMULAZIONE
                        :IND-RIC-FL-SIMULAZIONE
                       ,:RIC-KEY-ORDINAMENTO-VCHAR
                        :IND-RIC-KEY-ORDINAMENTO
                       ,:RIC-ID-RICH-COLLG
                        :IND-RIC-ID-RICH-COLLG
                       ,:RIC-TP-RAMO-BILA
                        :IND-RIC-TP-RAMO-BILA
                       ,:RIC-TP-FRM-ASSVA
                        :IND-RIC-TP-FRM-ASSVA
                       ,:RIC-TP-CALC-RIS
                        :IND-RIC-TP-CALC-RIS
                       ,:RIC-DS-OPER-SQL
                       ,:RIC-DS-VER
                       ,:RIC-DS-TS-CPTZ
                       ,:RIC-DS-UTENTE
                       ,:RIC-DS-STATO-ELAB
                       ,:RIC-RAMO-BILA
                        :IND-RIC-RAMO-BILA
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE RICH SET

                   ID_RICH                =
                :RIC-ID-RICH
                  ,COD_COMP_ANIA          =
                :RIC-COD-COMP-ANIA
                  ,TP_RICH                =
                :RIC-TP-RICH
                  ,COD_MACROFUNCT         =
                :RIC-COD-MACROFUNCT
                  ,TP_MOVI                =
                :RIC-TP-MOVI
                  ,IB_RICH                =
                :RIC-IB-RICH
                                       :IND-RIC-IB-RICH
                  ,DT_EFF                 =
           :RIC-DT-EFF-DB
                  ,DT_RGSTRZ_RICH         =
           :RIC-DT-RGSTRZ-RICH-DB
                  ,DT_PERV_RICH           =
           :RIC-DT-PERV-RICH-DB
                  ,DT_ESEC_RICH           =
           :RIC-DT-ESEC-RICH-DB
                  ,TS_EFF_ESEC_RICH       =
                :RIC-TS-EFF-ESEC-RICH
                                       :IND-RIC-TS-EFF-ESEC-RICH
                  ,ID_OGG                 =
                :RIC-ID-OGG
                                       :IND-RIC-ID-OGG
                  ,TP_OGG                 =
                :RIC-TP-OGG
                                       :IND-RIC-TP-OGG
                  ,IB_POLI                =
                :RIC-IB-POLI
                                       :IND-RIC-IB-POLI
                  ,IB_ADES                =
                :RIC-IB-ADES
                                       :IND-RIC-IB-ADES
                  ,IB_GAR                 =
                :RIC-IB-GAR
                                       :IND-RIC-IB-GAR
                  ,IB_TRCH_DI_GAR         =
                :RIC-IB-TRCH-DI-GAR
                                       :IND-RIC-IB-TRCH-DI-GAR
                  ,ID_BATCH               =
                :RIC-ID-BATCH
                                       :IND-RIC-ID-BATCH
                  ,ID_JOB                 =
                :RIC-ID-JOB
                                       :IND-RIC-ID-JOB
                  ,FL_SIMULAZIONE         =
                :RIC-FL-SIMULAZIONE
                                       :IND-RIC-FL-SIMULAZIONE
                  ,KEY_ORDINAMENTO        =
                :RIC-KEY-ORDINAMENTO-VCHAR
                                       :IND-RIC-KEY-ORDINAMENTO
                  ,ID_RICH_COLLG          =
                :RIC-ID-RICH-COLLG
                                       :IND-RIC-ID-RICH-COLLG
                  ,TP_RAMO_BILA           =
                :RIC-TP-RAMO-BILA
                                       :IND-RIC-TP-RAMO-BILA
                  ,TP_FRM_ASSVA           =
                :RIC-TP-FRM-ASSVA
                                       :IND-RIC-TP-FRM-ASSVA
                  ,TP_CALC_RIS            =
                :RIC-TP-CALC-RIS
                                       :IND-RIC-TP-CALC-RIS
                  ,DS_OPER_SQL            =
                :RIC-DS-OPER-SQL
                  ,DS_VER                 =
                :RIC-DS-VER
                  ,DS_TS_CPTZ             =
                :RIC-DS-TS-CPTZ
                  ,DS_UTENTE              =
                :RIC-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :RIC-DS-STATO-ELAB
                  ,RAMO_BILA              =
                :RIC-RAMO-BILA
                                       :IND-RIC-RAMO-BILA
                WHERE     ID_RICH = :RIC-ID-RICH

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM RICH
                WHERE     ID_RICH = :RIC-ID-RICH

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDO-NST-RIC CURSOR FOR
              SELECT
                     ID_RICH
                    ,COD_COMP_ANIA
                    ,TP_RICH
                    ,COD_MACROFUNCT
                    ,TP_MOVI
                    ,IB_RICH
                    ,DT_EFF
                    ,DT_RGSTRZ_RICH
                    ,DT_PERV_RICH
                    ,DT_ESEC_RICH
                    ,TS_EFF_ESEC_RICH
                    ,ID_OGG
                    ,TP_OGG
                    ,IB_POLI
                    ,IB_ADES
                    ,IB_GAR
                    ,IB_TRCH_DI_GAR
                    ,ID_BATCH
                    ,ID_JOB
                    ,FL_SIMULAZIONE
                    ,KEY_ORDINAMENTO
                    ,ID_RICH_COLLG
                    ,TP_RAMO_BILA
                    ,TP_FRM_ASSVA
                    ,TP_CALC_RIS
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,RAMO_BILA
              FROM RICH
              WHERE     ID_OGG = :RIC-ID-OGG
                    AND TP_OGG = :RIC-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY ID_RICH ASC

           END-EXEC.

       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_RICH
                ,COD_COMP_ANIA
                ,TP_RICH
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_RICH
                ,DT_EFF
                ,DT_RGSTRZ_RICH
                ,DT_PERV_RICH
                ,DT_ESEC_RICH
                ,TS_EFF_ESEC_RICH
                ,ID_OGG
                ,TP_OGG
                ,IB_POLI
                ,IB_ADES
                ,IB_GAR
                ,IB_TRCH_DI_GAR
                ,ID_BATCH
                ,ID_JOB
                ,FL_SIMULAZIONE
                ,KEY_ORDINAMENTO
                ,ID_RICH_COLLG
                ,TP_RAMO_BILA
                ,TP_FRM_ASSVA
                ,TP_CALC_RIS
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,RAMO_BILA
             INTO
                :RIC-ID-RICH
               ,:RIC-COD-COMP-ANIA
               ,:RIC-TP-RICH
               ,:RIC-COD-MACROFUNCT
               ,:RIC-TP-MOVI
               ,:RIC-IB-RICH
                :IND-RIC-IB-RICH
               ,:RIC-DT-EFF-DB
               ,:RIC-DT-RGSTRZ-RICH-DB
               ,:RIC-DT-PERV-RICH-DB
               ,:RIC-DT-ESEC-RICH-DB
               ,:RIC-TS-EFF-ESEC-RICH
                :IND-RIC-TS-EFF-ESEC-RICH
               ,:RIC-ID-OGG
                :IND-RIC-ID-OGG
               ,:RIC-TP-OGG
                :IND-RIC-TP-OGG
               ,:RIC-IB-POLI
                :IND-RIC-IB-POLI
               ,:RIC-IB-ADES
                :IND-RIC-IB-ADES
               ,:RIC-IB-GAR
                :IND-RIC-IB-GAR
               ,:RIC-IB-TRCH-DI-GAR
                :IND-RIC-IB-TRCH-DI-GAR
               ,:RIC-ID-BATCH
                :IND-RIC-ID-BATCH
               ,:RIC-ID-JOB
                :IND-RIC-ID-JOB
               ,:RIC-FL-SIMULAZIONE
                :IND-RIC-FL-SIMULAZIONE
               ,:RIC-KEY-ORDINAMENTO-VCHAR
                :IND-RIC-KEY-ORDINAMENTO
               ,:RIC-ID-RICH-COLLG
                :IND-RIC-ID-RICH-COLLG
               ,:RIC-TP-RAMO-BILA
                :IND-RIC-TP-RAMO-BILA
               ,:RIC-TP-FRM-ASSVA
                :IND-RIC-TP-FRM-ASSVA
               ,:RIC-TP-CALC-RIS
                :IND-RIC-TP-CALC-RIS
               ,:RIC-DS-OPER-SQL
               ,:RIC-DS-VER
               ,:RIC-DS-TS-CPTZ
               ,:RIC-DS-UTENTE
               ,:RIC-DS-STATO-ELAB
               ,:RIC-RAMO-BILA
                :IND-RIC-RAMO-BILA
             FROM RICH
             WHERE     ID_OGG = :RIC-ID-OGG
                    AND TP_OGG = :RIC-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           EXEC SQL
                OPEN C-IDO-NST-RIC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           EXEC SQL
                CLOSE C-IDO-NST-RIC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           EXEC SQL
                FETCH C-IDO-NST-RIC
           INTO
                :RIC-ID-RICH
               ,:RIC-COD-COMP-ANIA
               ,:RIC-TP-RICH
               ,:RIC-COD-MACROFUNCT
               ,:RIC-TP-MOVI
               ,:RIC-IB-RICH
                :IND-RIC-IB-RICH
               ,:RIC-DT-EFF-DB
               ,:RIC-DT-RGSTRZ-RICH-DB
               ,:RIC-DT-PERV-RICH-DB
               ,:RIC-DT-ESEC-RICH-DB
               ,:RIC-TS-EFF-ESEC-RICH
                :IND-RIC-TS-EFF-ESEC-RICH
               ,:RIC-ID-OGG
                :IND-RIC-ID-OGG
               ,:RIC-TP-OGG
                :IND-RIC-TP-OGG
               ,:RIC-IB-POLI
                :IND-RIC-IB-POLI
               ,:RIC-IB-ADES
                :IND-RIC-IB-ADES
               ,:RIC-IB-GAR
                :IND-RIC-IB-GAR
               ,:RIC-IB-TRCH-DI-GAR
                :IND-RIC-IB-TRCH-DI-GAR
               ,:RIC-ID-BATCH
                :IND-RIC-ID-BATCH
               ,:RIC-ID-JOB
                :IND-RIC-ID-JOB
               ,:RIC-FL-SIMULAZIONE
                :IND-RIC-FL-SIMULAZIONE
               ,:RIC-KEY-ORDINAMENTO-VCHAR
                :IND-RIC-KEY-ORDINAMENTO
               ,:RIC-ID-RICH-COLLG
                :IND-RIC-ID-RICH-COLLG
               ,:RIC-TP-RAMO-BILA
                :IND-RIC-TP-RAMO-BILA
               ,:RIC-TP-FRM-ASSVA
                :IND-RIC-TP-FRM-ASSVA
               ,:RIC-TP-CALC-RIS
                :IND-RIC-TP-CALC-RIS
               ,:RIC-DS-OPER-SQL
               ,:RIC-DS-VER
               ,:RIC-DS-TS-CPTZ
               ,:RIC-DS-UTENTE
               ,:RIC-DS-STATO-ELAB
               ,:RIC-RAMO-BILA
                :IND-RIC-RAMO-BILA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A770-CLOSE-CURSOR-IDO     THRU A770-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-RIC-IB-RICH = -1
              MOVE HIGH-VALUES TO RIC-IB-RICH-NULL
           END-IF
           IF IND-RIC-TS-EFF-ESEC-RICH = -1
              MOVE HIGH-VALUES TO RIC-TS-EFF-ESEC-RICH-NULL
           END-IF
           IF IND-RIC-ID-OGG = -1
              MOVE HIGH-VALUES TO RIC-ID-OGG-NULL
           END-IF
           IF IND-RIC-TP-OGG = -1
              MOVE HIGH-VALUES TO RIC-TP-OGG-NULL
           END-IF
           IF IND-RIC-IB-POLI = -1
              MOVE HIGH-VALUES TO RIC-IB-POLI-NULL
           END-IF
           IF IND-RIC-IB-ADES = -1
              MOVE HIGH-VALUES TO RIC-IB-ADES-NULL
           END-IF
           IF IND-RIC-IB-GAR = -1
              MOVE HIGH-VALUES TO RIC-IB-GAR-NULL
           END-IF
           IF IND-RIC-IB-TRCH-DI-GAR = -1
              MOVE HIGH-VALUES TO RIC-IB-TRCH-DI-GAR-NULL
           END-IF
           IF IND-RIC-ID-BATCH = -1
              MOVE HIGH-VALUES TO RIC-ID-BATCH-NULL
           END-IF
           IF IND-RIC-ID-JOB = -1
              MOVE HIGH-VALUES TO RIC-ID-JOB-NULL
           END-IF
           IF IND-RIC-FL-SIMULAZIONE = -1
              MOVE HIGH-VALUES TO RIC-FL-SIMULAZIONE-NULL
           END-IF
           IF IND-RIC-KEY-ORDINAMENTO = -1
              MOVE HIGH-VALUES TO RIC-KEY-ORDINAMENTO
           END-IF
           IF IND-RIC-ID-RICH-COLLG = -1
              MOVE HIGH-VALUES TO RIC-ID-RICH-COLLG-NULL
           END-IF
           IF IND-RIC-TP-RAMO-BILA = -1
              MOVE HIGH-VALUES TO RIC-TP-RAMO-BILA-NULL
           END-IF
           IF IND-RIC-TP-FRM-ASSVA = -1
              MOVE HIGH-VALUES TO RIC-TP-FRM-ASSVA-NULL
           END-IF
           IF IND-RIC-TP-CALC-RIS = -1
              MOVE HIGH-VALUES TO RIC-TP-CALC-RIS-NULL
           END-IF
           IF IND-RIC-RAMO-BILA = -1
              MOVE HIGH-VALUES TO RIC-RAMO-BILA-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO RIC-DS-OPER-SQL
           MOVE 0                   TO RIC-DS-VER
           MOVE IDSV0003-USER-NAME TO RIC-DS-UTENTE
           MOVE '1'                   TO RIC-DS-STATO-ELAB
           MOVE WS-TS-COMPETENZA-AGG-STOR TO RIC-DS-TS-CPTZ.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO RIC-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO RIC-DS-UTENTE
           MOVE WS-TS-COMPETENZA-AGG-STOR TO RIC-DS-TS-CPTZ.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF RIC-IB-RICH-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-IB-RICH
           ELSE
              MOVE 0 TO IND-RIC-IB-RICH
           END-IF
           IF RIC-TS-EFF-ESEC-RICH-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-TS-EFF-ESEC-RICH
           ELSE
              MOVE 0 TO IND-RIC-TS-EFF-ESEC-RICH
           END-IF
           IF RIC-ID-OGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-ID-OGG
           ELSE
              MOVE 0 TO IND-RIC-ID-OGG
           END-IF
           IF RIC-TP-OGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-TP-OGG
           ELSE
              MOVE 0 TO IND-RIC-TP-OGG
           END-IF
           IF RIC-IB-POLI-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-IB-POLI
           ELSE
              MOVE 0 TO IND-RIC-IB-POLI
           END-IF
           IF RIC-IB-ADES-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-IB-ADES
           ELSE
              MOVE 0 TO IND-RIC-IB-ADES
           END-IF
           IF RIC-IB-GAR-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-IB-GAR
           ELSE
              MOVE 0 TO IND-RIC-IB-GAR
           END-IF
           IF RIC-IB-TRCH-DI-GAR-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-IB-TRCH-DI-GAR
           ELSE
              MOVE 0 TO IND-RIC-IB-TRCH-DI-GAR
           END-IF
           IF RIC-ID-BATCH-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-ID-BATCH
           ELSE
              MOVE 0 TO IND-RIC-ID-BATCH
           END-IF
           IF RIC-ID-JOB-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-ID-JOB
           ELSE
              MOVE 0 TO IND-RIC-ID-JOB
           END-IF
           IF RIC-FL-SIMULAZIONE-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-FL-SIMULAZIONE
           ELSE
              MOVE 0 TO IND-RIC-FL-SIMULAZIONE
           END-IF
           IF RIC-KEY-ORDINAMENTO = HIGH-VALUES
              MOVE -1 TO IND-RIC-KEY-ORDINAMENTO
           ELSE
              MOVE 0 TO IND-RIC-KEY-ORDINAMENTO
           END-IF
           IF RIC-ID-RICH-COLLG-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-ID-RICH-COLLG
           ELSE
              MOVE 0 TO IND-RIC-ID-RICH-COLLG
           END-IF
           IF RIC-TP-RAMO-BILA-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-TP-RAMO-BILA
           ELSE
              MOVE 0 TO IND-RIC-TP-RAMO-BILA
           END-IF
           IF RIC-TP-FRM-ASSVA-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-TP-FRM-ASSVA
           ELSE
              MOVE 0 TO IND-RIC-TP-FRM-ASSVA
           END-IF
           IF RIC-TP-CALC-RIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-TP-CALC-RIS
           ELSE
              MOVE 0 TO IND-RIC-TP-CALC-RIS
           END-IF
           IF RIC-RAMO-BILA-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-RAMO-BILA
           ELSE
              MOVE 0 TO IND-RIC-RAMO-BILA
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.

           CONTINUE.
       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.
           CONTINUE.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.
           CONTINUE.
       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.
           CONTINUE.
       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE RIC-DT-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO RIC-DT-EFF-DB
           MOVE RIC-DT-RGSTRZ-RICH TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO RIC-DT-RGSTRZ-RICH-DB
           MOVE RIC-DT-PERV-RICH TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO RIC-DT-PERV-RICH-DB
           MOVE RIC-DT-ESEC-RICH TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO RIC-DT-ESEC-RICH-DB.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE RIC-DT-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO RIC-DT-EFF
           MOVE RIC-DT-RGSTRZ-RICH-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO RIC-DT-RGSTRZ-RICH
           MOVE RIC-DT-PERV-RICH-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO RIC-DT-PERV-RICH
           MOVE RIC-DT-ESEC-RICH-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO RIC-DT-ESEC-RICH.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.


           MOVE LENGTH OF RIC-KEY-ORDINAMENTO
                       TO RIC-KEY-ORDINAMENTO-LEN.

       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS2720.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2013.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS2720
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... IMPOSTA DI BOLLO
      *                  CALCOLO DELLA VARIABILE IMPPAGANNLIQ
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS2720'.
       01  WK-CALL-PGM                      PIC X(008).
       01  LDBS6040                         PIC X(008) VALUE 'LDBS6040'.
       01  LDBSE590                         PIC X(008) VALUE 'LDBSE590'.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-DATA                         PIC 9(08).
       01  WK-DATA-SEPARATA REDEFINES WK-DATA.
           03 WK-ANNO                  PIC 9(04).
           03 WK-MESE                  PIC 9(02).
           03 WK-GG                    PIC 9(02).

       01 WK-DATA-INIZIO.
           03 WK-ANNO-INI              PIC 9(04).
           03 WK-MESE-INI              PIC 9(02).
           03 WK-GG-INI                PIC 9(02).

       01 WK-DATA-INIZIO-D             PIC S9(8)V COMP-3.

       01 WK-DATA-FINE.
           03 WK-ANNO-FINE              PIC 9(04).
           03 WK-MESE-FINE              PIC 9(02).
           03 WK-GG-FINE                PIC 9(02).

       01 WK-DATA-FINE-D             PIC S9(8)V COMP-3.

       01  WK-VAR-MOVI-COMUN.
           05 WK-ID-MOVI-COMUN             PIC S9(09) COMP-3.
           05 WK-DATA-EFF-PREC             PIC S9(08) COMP-3.
           05 WK-DATA-CPTZ-PREC            PIC S9(18) COMP-3.
           05 WK-DATA-EFF-RIP              PIC S9(08) COMP-3.
           05 WK-DATA-CPTZ-RIP             PIC S9(18) COMP-3.
      *---------------------------------------------------------------*
      *  WORKIN ANAGRAFICA
      *---------------------------------------------------------------*
       01 AREA-RECUPERO-ANAGRAFICA.
         03 WK-TIMESTAMP.
            05 WK-DATA-EFFETTO            PIC  X(008)     VALUE SPACES.
            05 WK-ORA                     PIC  X(010)
                                          VALUE '2359999999'.
         03 WK-TIMESTAMP-N                REDEFINES
            WK-TIMESTAMP                  PIC 9(18).

         03 WK-ID-STRINGA                 PIC  X(020).
         03 WK-ID-NUM                     PIC  9(011).
         03 WK-ID-PERS                    PIC S9(009) COMP-3.

         03 WK-COD-PRT-IVA                PIC X(11).
         03 WK-COD-FISC                   PIC X(16).
      *---------------------------------------------------------------*
      *    Area per modulo conversione stringa
      *---------------------------------------------------------------*
      *--> Conversione Stringa a Numerico
       01  IWFS0050                      PIC X(008) VALUE 'IWFS0050'.

       01  AREA-CALL-IWFS0050.
           COPY IWFI0051.
           COPY IWFO0051.
      *
           COPY IABC0010.
      *---------------------------------------------------------------*
      *  COPY TABELLE
      *---------------------------------------------------------------*
           COPY LDBVE391.
           COPY LDBVE421.
           COPY LDBV1241.
           COPY LDBV1301.
      *--> TABELLA RAPPORTO ANAGRAFICO
           COPY IDBVRAN1.
      *--> TABELLA PERSONA
           COPY IDBVA251.
      *--> TABELLA MOVIMENTO
           COPY IDBVMOV1.
      *--> TABELLA IMPOSTA DI BOLLO
           COPY IDBVP581.
      *---------------------------------------------------------------*
      *  LISTA TABELLE E TIPOLOGICHE
      *---------------------------------------------------------------*
10819      COPY LCCVXMVZ.
10819      COPY LCCVXMV2.
10819      COPY LCCVXMV3.
10819      COPY LCCVXMV5.
10819      COPY LCCVXMV6.
      *----------------------------------------------------------------*
      *    COPY DI INPUT PER LA CHIAMATA AL LVVS0000
      *----------------------------------------------------------------*
       01  INPUT-LVVS0000.
           COPY LVVC0000        REPLACING ==(SF)== BY ==LVVC0000==.
      *---------------------------------------------------------------*
      *  LISTA MOVIMENTI IMPLEMENTATI LCCC0006
      *---------------------------------------------------------------*
           COPY LCCC0006.
      *----------------------------------------------------------------*
      *--> AREA IMPOSTA DI BOLLO
      *----------------------------------------------------------------*
       01 AREA-IO-P58.
          03 DP58-AREA-P58.
             04 DP58-ELE-P58-MAX         PIC S9(04) COMP.
52921 *      04 DP58-TAB-P58             OCCURS 50.
52921        04 DP58-TAB-P58             OCCURS 75.
             COPY LCCVP581               REPLACING ==(SF)== BY ==DP58==.
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-POLI.
          03 DPOL-AREA-POL.
             04 DPOL-ELE-POL-MAX         PIC S9(04) COMP.
             04 DPOL-TAB-POLI.
             COPY LCCVPOL1               REPLACING ==(SF)== BY ==DPOL==.
      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
           03 IX-TAB-RAN                     PIC S9(04) COMP.
           03 IX-STRINGA                     PIC S9(04) COMP.
           03 IX-CONVERS                     PIC S9(04) COMP.
           03 IX-TAB-P58                     PIC 9(09) VALUE 0.
      *----------------------------------------------------------------*
      *    FLAG
      *----------------------------------------------------------------*
       01 FLAG-CUR-MOV                       PIC X(01).
           88 INIT-CUR-MOV                   VALUE 'S'.
           88 FINE-CUR-MOV                   VALUE 'N'.

       01 FLAG-ULTIMA-LETTURA               PIC X(01).
          88 SI-ULTIMA-LETTURA              VALUE 'S'.
          88 NO-ULTIMA-LETTURA              VALUE 'N'.

       01 FLAG-FINE-LETTURA-P58               PIC X(01).
          88 FINE-LETT-P58-SI               VALUE 'S'.
          88 FINE-LETT-P58-NO               VALUE 'N'.

       01 FLAG-COMUN-TROV                    PIC X(01).
           88 COMUN-TROV-SI                  VALUE 'S'.
           88 COMUN-TROV-NO                  VALUE 'N'.

       01 FLAG-PERSONA-FISICA                PIC X(01).
           88 PERSONA-FISICA-SI              VALUE 'S'.
           88 PERSONA-FISICA-NO              VALUE 'N'.

       01 FLAG-PERSONA-GIURIDICA             PIC X(01).
           88 PERSONA-GIURIDICA-SI           VALUE 'S'.
           88 PERSONA-GIURIDICA-NO           VALUE 'N'.

       01 FLAG-CODICE-FISCALE                PIC X(01).
           88 CODICE-FISCALE-SI              VALUE 'S'.
           88 CODICE-FISCALE-NO              VALUE 'N'.

       01 FLAG-PARTITA-IVA                   PIC X(01).
           88 PARTITA-IVA-SI                 VALUE 'S'.
           88 PARTITA-IVA-NO                 VALUE 'N'.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS2720.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS2720.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           SET COMUN-TROV-NO                 TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE AREA-IO-P58
                      AREA-IO-POLI.

           PERFORM VERIFICA-MOVIMENTO
              THRU VERIFICA-MOVIMENTO-EX

      *--> PERFORM DI CALCOLO ANNO CORRENTE
           IF  IDSV0003-SUCCESSFUL-RC
      *    AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1150-CALCOLO-ANNO
                  THRU S1150-CALCOLO-ANNO-EX
           END-IF.

      *--> PERFORM DI SOMMATORIA
           IF  IDSV0003-SUCCESSFUL-RC
      *    AND IDSV0003-SUCCESSFUL-SQL
               IF DP58-ELE-P58-MAX > 0
                  PERFORM S1300-CALCOLO-SOMMA
                     THRU S1300-CALCOLO-SOMMA-EX
               ELSE
                  PERFORM S1350-RECUPERO-INFO
                     THRU S1350-RECUPERO-INFO-EX

                  IF  IDSV0003-SUCCESSFUL-RC
                  AND IDSV0003-SUCCESSFUL-SQL
                      PERFORM S1390-CALCOLO-SOMMA-2
                         THRU S1390-CALCOLO-SOMMA-2-EX
                  END-IF
               END-IF
           END-IF.

       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    VERIFICA MOVIMENTO
      *----------------------------------------------------------------*
       VERIFICA-MOVIMENTO.

      *--> SE CI TROVIAMO IN FASE DI LIQUIDAZIONE DEVO RECUPERARE LE
      *--> IMMAGINI IN FASE DI COMUNICAZIONE
NEW   *    MOVE IVVC0213-TIPO-MOVIMENTO
NEW   *      TO WS-MOVIMENTO
NEW        MOVE IVVC0213-TIPO-MOVI-ORIG
NEW          TO WS-MOVIMENTO

NEW        IF LIQUI-RISTOT-IND OR LIQUI-RISPAR-POLIND OR
NEW           LIQUI-SCAPOL OR LIQUI-SININD OR LIQUI-RECIND OR
10819         LIQUI-RPP-TAKE-PROFIT
10819X     OR LIQUI-RISTOT-INCAPIENZA
10819X     OR LIQUI-RPP-REDDITO-PROGR
10819      OR LIQUI-RPP-BENEFICIO-CONTR
FNZS2      OR LIQUI-RISTOT-INCAP
      *--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
NEW           PERFORM RECUP-MOVI-COMUN
NEW              THRU RECUP-MOVI-COMUN-EX

              IF COMUN-TROV-SI
                 MOVE IDSV0003-DATA-COMPETENZA
                   TO WK-DATA-CPTZ-RIP

                 MOVE IDSV0003-DATA-INIZIO-EFFETTO
                   TO WK-DATA-EFF-RIP

                 MOVE WK-DATA-CPTZ-PREC
                   TO IDSV0003-DATA-COMPETENZA

                 MOVE WK-DATA-EFF-PREC
                   TO IDSV0003-DATA-INIZIO-EFFETTO
                      IDSV0003-DATA-FINE-EFFETTO

                 PERFORM VARYING IX-TAB-P58 FROM 1 BY 1
52921 *             UNTIL IX-TAB-P58 > 10
52921               UNTIL IX-TAB-P58 > 75
                    PERFORM INIZIA-TOT-P58
                       THRU INIZIA-TOT-P58-EX
                 END-PERFORM

                 PERFORM LETTURA-IMPST-BOLLO
                    THRU LETTURA-IMPST-BOLLO-EX
NEW           ELSE
      *-->       ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *-->       RISPETTIVE AREE DCLGEN IN WORKING
                 PERFORM S1100-VALORIZZA-DCLGEN
                    THRU S1100-VALORIZZA-DCLGEN-EX
                 VARYING IX-DCLGEN FROM 1 BY 1
                   UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                      OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                         SPACES OR LOW-VALUE OR HIGH-VALUE
NEW           END-IF
NEW        ELSE

      *-->    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *-->    RISPETTIVE AREE DCLGEN IN WORKING
              PERFORM S1100-VALORIZZA-DCLGEN
                 THRU S1100-VALORIZZA-DCLGEN-EX
              VARYING IX-DCLGEN FROM 1 BY 1
                UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                   OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                      SPACES OR LOW-VALUE OR HIGH-VALUE
           END-IF.

       VERIFICA-MOVIMENTO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    Recupero il movimento di comunicazione
      *----------------------------------------------------------------*
       RECUP-MOVI-COMUN.

           SET INIT-CUR-MOV               TO TRUE
           SET COMUN-TROV-NO              TO TRUE

           INITIALIZE MOVI

      *--> TIPO OPERAZIONE
           SET IDSV0003-FETCH-FIRST       TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
           SET  NO-ULTIMA-LETTURA         TO TRUE

      *
           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR FINE-CUR-MOV
                      OR COMUN-TROV-SI

              INITIALIZE MOVI
      *
              MOVE IVVC0213-ID-POLIZZA    TO MOV-ID-OGG
              MOVE 'PO'                   TO MOV-TP-OGG

              SET IDSV0003-TRATT-SENZA-STOR   TO TRUE

      *--> LIVELLO OPERAZIONE
              SET IDSV0003-WHERE-CONDITION TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
              SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
              SET  IDSV0003-SUCCESSFUL-SQL TO TRUE

              CALL  LDBS6040   USING IDSV0003 MOVI
                ON EXCEPTION
                   MOVE LDBS6040          TO IDSV0003-COD-SERVIZIO-BE
                   MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                     TO IDSV0003-DESCRIZ-ERR-DB2
                   SET IDSV0003-INVALID-OPER   TO TRUE
              END-CALL


                IF IDSV0003-SUCCESSFUL-RC

                    EVALUATE TRUE
                        WHEN IDSV0003-NOT-FOUND
      *-->    NESSUN DATO IN TABELLA
      *-->     LIQUIDAZIONE CONTESTUALE - UTILIZZO L'AREA DEL
      *-->     BOLLO IN INPUT
                          SET FINE-CUR-MOV TO TRUE

                        WHEN IDSV0003-SUCCESSFUL-SQL
      *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
      *                   MOVE IDSV0003-BUFFER-DATI TO MOVI
      *      TROVO IL MOVIMENTO DI COMUNICAZIONE RISCATTO PARZIALE
                          MOVE MOV-TP-MOVI      TO WS-MOVIMENTO
                          IF COMUN-RISPAR-IND
                          OR RISTO-INDIVI
                          OR VARIA-OPZION
                          OR SINIS-INDIVI
                          OR RECES-INDIVI
10819                     OR RPP-TAKE-PROFIT
10819X                    OR COMUN-RISTOT-INCAPIENZA
10819X                    OR RPP-REDDITO-PROGRAMMATO
10819                     OR RPP-BENEFICIO-CONTR
FNZS2                     OR COMUN-RISTOT-INCAP
                             MOVE MOV-ID-MOVI    TO WK-ID-MOVI-COMUN
                             SET SI-ULTIMA-LETTURA  TO TRUE
                             MOVE MOV-DT-EFF     TO WK-DATA-EFF-PREC
                             COMPUTE WK-DATA-CPTZ-PREC =  MOV-DS-TS-CPTZ
                                                       - 1
                             SET COMUN-TROV-SI   TO TRUE
                             PERFORM CLOSE-MOVI
                                THRU CLOSE-MOVI-EX
                             SET FINE-CUR-MOV   TO TRUE
                          ELSE
                             SET IDSV0003-FETCH-NEXT   TO TRUE
                          END-IF
                        WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                             MOVE LDBS6040
                               TO IDSV0003-COD-SERVIZIO-BE
                             MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                               TO IDSV0003-DESCRIZ-ERR-DB2
                             SET IDSV0003-INVALID-OPER   TO TRUE
                    END-EVALUATE
                 ELSE
      *--> GESTIRE ERRORE
                    MOVE LDBS6040
                      TO IDSV0003-COD-SERVIZIO-BE
                    MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                      TO IDSV0003-DESCRIZ-ERR-DB2
                    SET IDSV0003-INVALID-OPER   TO TRUE
                 END-IF
           END-PERFORM.
           MOVE IDSV0003-TIPO-MOVIMENTO TO WS-MOVIMENTO.

       RECUP-MOVI-COMUN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CHIUDO IL CURSORE SULLA TABELLA MOVIMENTO LDBS6040
      *----------------------------------------------------------------*
       CLOSE-MOVI.

           SET IDSV0003-CLOSE-CURSOR            TO TRUE.

           CALL LDBS6040 USING IDSV0003 MOVI
           ON EXCEPTION
              MOVE LDBS6040
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CALL LDBS6040 CLOSE'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER TO TRUE
           END-CALL.

           IF NOT IDSV0003-SUCCESSFUL-RC
           OR NOT IDSV0003-SUCCESSFUL-SQL
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CLOSE CURSORE LDBS6040'
                TO IDSV0003-DESCRIZ-ERR-DB2
               SET IDSV0003-INVALID-OPER
                TO TRUE
           END-IF.
       CLOSE-MOVI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    RECUPERO IMPOSTA DI BOLLO DA MOVIMENTO DI COMUNICAZIONE
      *----------------------------------------------------------------*
       LETTURA-IMPST-BOLLO.

           SET FINE-LETT-P58-NO           TO TRUE

           INITIALIZE IMPST-BOLLO

           MOVE 0 TO DP58-ELE-P58-MAX
           MOVE 0 TO IX-TAB-P58

      *--> TIPO OPERAZIONE
           SET IDSV0003-FETCH-FIRST       TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.

      *
           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR FINE-LETT-P58-SI

              INITIALIZE IMPST-BOLLO
      *
              MOVE IVVC0213-ID-POLIZZA    TO P58-ID-POLI

              SET IDSV0003-TRATT-X-COMPETENZA TO TRUE

      *--> LIVELLO OPERAZIONE
              SET IDSV0003-WHERE-CONDITION TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
              SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
              SET  IDSV0003-SUCCESSFUL-SQL TO TRUE

              CALL  LDBSE590   USING IDSV0003 IMPST-BOLLO
                ON EXCEPTION
                   MOVE LDBSE590          TO IDSV0003-COD-SERVIZIO-BE
                   MOVE 'CALL-LDBSE590 ERRORE CHIAMATA'
                     TO IDSV0003-DESCRIZ-ERR-DB2
                   SET IDSV0003-INVALID-OPER   TO TRUE
              END-CALL


                IF IDSV0003-SUCCESSFUL-RC

                    EVALUATE TRUE
                        WHEN IDSV0003-NOT-FOUND
      *-->    NESSUN DATO IN TABELLA

                          SET FINE-LETT-P58-SI           TO TRUE
37049                     IF NOT IDSV0003-FETCH-FIRST
37049                        SET IDSV0003-SUCCESSFUL-SQL TO TRUE
37049                        SET IDSV0003-SUCCESSFUL-RC  TO TRUE
37049                     END-IF

                        WHEN IDSV0003-SUCCESSFUL-SQL
      *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                             ADD 1 TO DP58-ELE-P58-MAX
                                      IX-TAB-P58

                             PERFORM VALORIZZA-OUTPUT-P58
                                THRU VALORIZZA-OUTPUT-P58-EX

                             SET IDSV0003-FETCH-NEXT   TO TRUE

                        WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                             MOVE LDBSE590
                               TO IDSV0003-COD-SERVIZIO-BE
                             MOVE 'CALL-LDBSE590 ERRORE CHIAMATA'
                               TO IDSV0003-DESCRIZ-ERR-DB2
                             SET IDSV0003-INVALID-OPER   TO TRUE
                             SET FINE-LETT-P58-SI TO TRUE
                    END-EVALUATE
                 ELSE
      *--> GESTIRE ERRORE
                    MOVE LDBSE590
                      TO IDSV0003-COD-SERVIZIO-BE
                    MOVE 'CALL-LDBSE590 ERRORE CHIAMATA'
                      TO IDSV0003-DESCRIZ-ERR-DB2
                    SET IDSV0003-INVALID-OPER   TO TRUE
                    SET FINE-LETT-P58-SI TO TRUE
                 END-IF
           END-PERFORM.

       LETTURA-IMPST-BOLLO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.


           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-IMPOSTA-BOLLO
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DP58-AREA-P58
           END-IF.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-POLI
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DPOL-AREA-POL
           END-IF.


       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CALCOLO ANNO
      *----------------------------------------------------------------*
       S1150-CALCOLO-ANNO.


           MOVE IDSV0003-DATA-INIZIO-EFFETTO
             TO WK-DATA

           MOVE WK-ANNO
             TO WK-ANNO-INI
                WK-ANNO-FINE

           MOVE 01
             TO WK-MESE-INI
                WK-GG-INI

           MOVE 12
             TO WK-MESE-FINE

           MOVE 31
             TO WK-GG-FINE.

           MOVE WK-DATA-INIZIO
             TO WK-DATA
           MOVE WK-DATA
             TO WK-DATA-INIZIO-D.

           MOVE WK-DATA-FINE
             TO WK-DATA
           MOVE WK-DATA
             TO WK-DATA-FINE-D.
       S1150-CALCOLO-ANNO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CALCOLO DELLA SOMMAPAG
      *----------------------------------------------------------------*
       S1300-CALCOLO-SOMMA.

           IF DP58-COD-FISC-NULL(1) EQUAL HIGH-VALUES
                                       OR LOW-VALUES
      *
              MOVE 'LDBSE420'                     TO WK-CALL-PGM

              INITIALIZE LDBVE421
      *
              MOVE DP58-COD-PART-IVA(1)
                TO LDBVE421-COD-PART-IVA
      *
              MOVE 'SW' TO   LDBVE421-TP-CAUS-BOLLO-1
              MOVE 'AN' TO   LDBVE421-TP-CAUS-BOLLO-2
              MOVE SPACES TO LDBVE421-TP-CAUS-BOLLO-3
              MOVE SPACES TO LDBVE421-TP-CAUS-BOLLO-4
              MOVE SPACES TO LDBVE421-TP-CAUS-BOLLO-5
              MOVE SPACES TO LDBVE421-TP-CAUS-BOLLO-6
              MOVE SPACES TO LDBVE421-TP-CAUS-BOLLO-7
      *
              MOVE WK-DATA-INIZIO-D
                TO LDBVE421-DT-INI-CALC
      *
              MOVE WK-DATA-FINE-D
                TO LDBVE421-DT-END-CALC
      *
              SET IDSV0003-TRATT-X-COMPETENZA TO TRUE
      *
              SET IDSV0003-WHERE-CONDITION    TO TRUE
      *
              SET IDSV0003-SELECT             TO TRUE
      *
              CALL WK-CALL-PGM  USING  IDSV0003 LDBVE421
              ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING WK-PGM ' ERRORE CALL - MODULO LVVS2720'
              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
               END-STRING
              SET IDSV0003-INVALID-OPER  TO TRUE
              END-CALL
              IF IDSV0003-SUCCESSFUL-SQL
                 MOVE LDBVE421-IMP-BOLLO-DETT-C
                   TO IVVC0213-VAL-IMP-O
              END-IF
      *
           ELSE
      *
              MOVE 'LDBSE390'                     TO WK-CALL-PGM

              INITIALIZE LDBVE391
      *
              MOVE DP58-COD-FISC(1)
                TO LDBVE391-COD-FISC
      *
              MOVE 'SW' TO   LDBVE391-TP-CAUS-BOLLO-1
              MOVE 'AN' TO   LDBVE391-TP-CAUS-BOLLO-2
              MOVE SPACES TO LDBVE391-TP-CAUS-BOLLO-3
              MOVE SPACES TO LDBVE391-TP-CAUS-BOLLO-4
              MOVE SPACES TO LDBVE391-TP-CAUS-BOLLO-5
              MOVE SPACES TO LDBVE391-TP-CAUS-BOLLO-6
              MOVE SPACES TO LDBVE391-TP-CAUS-BOLLO-7

      *
              MOVE WK-DATA-INIZIO-D
                TO LDBVE391-DT-INI-CALC
      *
              MOVE WK-DATA-FINE-D
                TO LDBVE391-DT-END-CALC
      *
              SET IDSV0003-TRATT-X-COMPETENZA TO TRUE
      *
              SET IDSV0003-WHERE-CONDITION    TO TRUE
      *
              SET IDSV0003-SELECT             TO TRUE
      *
              CALL WK-CALL-PGM  USING  IDSV0003 LDBVE391
              ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CALL ' WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2720'
              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
               END-STRING
              SET IDSV0003-INVALID-OPER  TO TRUE
              END-CALL
              IF IDSV0003-SUCCESSFUL-SQL
                 MOVE LDBVE391-IMP-BOLLO-DETT-C
                   TO IVVC0213-VAL-IMP-O
              END-IF


           END-IF

           IF NOT IDSV0003-SUCCESSFUL-SQL
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
              STRING  'ERRORE CHIAMATA - MODULO LVVS2720'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF
           END-IF.
      *
       S1300-CALCOLO-SOMMA-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CALCOLO DELLA SOMMAPAG
      *----------------------------------------------------------------*
       S1390-CALCOLO-SOMMA-2.

      * IL RECUPERO DEL BOLLO TOTALE VARIA IN BASE AL TIPO DI PERSONA
      * CHE HO RECUPERATO SULLA TABELLA PERS
      * LA GESTIONE PARTICOLARE E' STATA NECESSARIA PER IL CASO
      * DITTA INDIVIDUALE.
           IF PERSONA-GIURIDICA-SI AND
              PARTITA-IVA-SI

              MOVE 'LDBSE420'                     TO WK-CALL-PGM

              SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE
              SET IDSV0003-WHERE-CONDITION        TO TRUE
              SET IDSV0003-SELECT                 TO TRUE

              INITIALIZE LDBVE421
      *
              MOVE WK-COD-PRT-IVA
                TO LDBVE421-COD-PART-IVA
      *
              MOVE 'SW' TO   LDBVE421-TP-CAUS-BOLLO-1
              MOVE 'AN' TO   LDBVE421-TP-CAUS-BOLLO-2

      *
              MOVE WK-DATA-INIZIO-D
                TO LDBVE421-DT-INI-CALC
      *
              MOVE WK-DATA-FINE-D
                TO LDBVE421-DT-END-CALC

              CALL WK-CALL-PGM  USING  IDSV0003 LDBVE421
              ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2720'
              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              SET IDSV0003-INVALID-OPER  TO TRUE
              END-CALL
              IF IDSV0003-SUCCESSFUL-SQL
                 MOVE LDBVE421-IMP-BOLLO-DETT-C
                   TO IVVC0213-VAL-IMP-O
              END-IF
      *
           END-IF
      *
           IF ((PERSONA-GIURIDICA-SI AND
                CODICE-FISCALE-SI)   OR
               (PERSONA-FISICA-SI AND
                CODICE-FISCALE-SI))

              MOVE 'LDBSE390'                     TO WK-CALL-PGM

              SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE
              SET IDSV0003-WHERE-CONDITION        TO TRUE
              SET IDSV0003-SELECT                 TO TRUE

              INITIALIZE LDBVE391
      *
              MOVE WK-COD-FISC
                TO LDBVE391-COD-FISC
      *
              MOVE 'SW' TO LDBVE391-TP-CAUS-BOLLO-1
              MOVE 'AN' TO LDBVE391-TP-CAUS-BOLLO-2
      *
              MOVE WK-DATA-INIZIO-D
                TO LDBVE391-DT-INI-CALC
      *
              MOVE WK-DATA-FINE-D
                TO LDBVE391-DT-END-CALC

              CALL WK-CALL-PGM  USING  IDSV0003 LDBVE391
              ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CALL ' WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2720'
              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
               END-STRING
              SET IDSV0003-INVALID-OPER  TO TRUE
              END-CALL
              IF IDSV0003-SUCCESSFUL-SQL
                 MOVE LDBVE391-IMP-BOLLO-DETT-C
                   TO IVVC0213-VAL-IMP-O
              END-IF


           END-IF

           IF NOT IDSV0003-SUCCESSFUL-SQL
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
              STRING  WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2720'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF
           END-IF.
      *
       S1390-CALCOLO-SOMMA-2-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   RECUPERO INFORMAZIONI
      *----------------------------------------------------------------*
       S1350-RECUPERO-INFO.

              MOVE 'LDBS1240'                     TO WK-CALL-PGM

              INITIALIZE LDBV1241
                         RAPP-ANA.
      *
      * INIZIALIZZA SETTAGGI
              SET PERSONA-FISICA-NO               TO TRUE
              SET PERSONA-GIURIDICA-NO            TO TRUE
              SET CODICE-FISCALE-NO               TO TRUE
              SET PARTITA-IVA-NO                  TO TRUE

              SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE
              SET IDSV0003-WHERE-CONDITION        TO TRUE
              SET IDSV0003-SELECT                 TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
              SET  IDSV0003-SUCCESSFUL-RC         TO TRUE
              SET  IDSV0003-SUCCESSFUL-SQL        TO TRUE


              MOVE IVVC0213-ID-POLIZZA
                TO LDBV1241-ID-OGG
      *
              MOVE 'PO'
                TO LDBV1241-TP-OGG
      *
              MOVE 'CO'
                TO LDBV1241-TP-RAPP-ANA
      *
              MOVE LDBV1241 TO IDSV0003-BUFFER-WHERE-COND

              CALL WK-CALL-PGM  USING  IDSV0003 LDBV1241
              ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2720'
              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
               END-STRING
              SET IDSV0003-INVALID-OPER  TO TRUE
              END-CALL
              IF IDSV0003-SUCCESSFUL-SQL
                 PERFORM ACCESSO-PERS
                    THRU ACCESSO-PERS-EX
              ELSE
                 MOVE WK-CALL-PGM       TO IDSV0003-COD-SERVIZIO-BE
                 STRING  WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2720'
                        IDSV0003-RETURN-CODE ';'
                        IDSV0003-SQLCODE
                    DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                 END-STRING
                 IF IDSV0003-NOT-FOUND
                    SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                 ELSE
                    SET IDSV0003-INVALID-OPER            TO TRUE
                 END-IF
              END-IF.

       S1350-RECUPERO-INFO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   RECUPERO DELLA PERS
      *----------------------------------------------------------------*
       ACCESSO-PERS.

           MOVE RAN-COD-SOGG       TO WK-ID-STRINGA
           PERFORM E000-CONVERTI-CHAR
              THRU EX-E000
           MOVE WK-ID-NUM              TO WK-ID-PERS

           MOVE 'LDBS1300'                TO WK-CALL-PGM
           SET IDSV0003-PRIMARY-KEY            TO TRUE
           SET IDSV0003-SELECT                 TO TRUE


           MOVE WK-ID-PERS                TO LDBV1301-ID-TAB
           MOVE WK-TIMESTAMP-N            TO LDBV1301-TIMESTAMP
           MOVE LDBV1301                  TO IDSV0003-BUFFER-WHERE-COND
           CALL WK-CALL-PGM  USING  IDSV0003 PERS
           ON EXCEPTION

              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2720'
              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
               END-STRING
              SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL

           IF IDSV0003-SUCCESSFUL-SQL
              IF A25-IND-CLI = 'F'
                 SET PERSONA-FISICA-SI     TO TRUE
                 IF A25-COD-FISC-NULL NOT = HIGH-VALUES OR
                                            LOW-VALUES  OR
                                            SPACES
                 SET CODICE-FISCALE-SI     TO TRUE
                 MOVE A25-COD-FISC         TO WK-COD-FISC
              ELSE
                 SET PERSONA-GIURIDICA-SI  TO TRUE
                 IF A25-COD-PRT-IVA-NULL = HIGH-VALUES OR
                                           LOW-VALUES  OR
                                           SPACES
                    SET CODICE-FISCALE-SI  TO TRUE
                    MOVE A25-COD-FISC      TO WK-COD-FISC
                 ELSE
                    SET PARTITA-IVA-SI     TO TRUE
                    MOVE A25-COD-PRT-IVA   TO WK-COD-PRT-IVA
                 END-IF
              END-IF
           ELSE
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
              STRING  WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2720'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF
           END-IF.

       ACCESSO-PERS-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CONVERTI VALORE DA STRINGA IN NUMERICO
      *----------------------------------------------------------------*
       E000-CONVERTI-CHAR.

           MOVE ZERO                  TO WK-ID-NUM.
           MOVE HIGH-VALUE            TO AREA-CALL-IWFS0050.

           MOVE WK-ID-STRINGA         TO IWFI0051-ARRAY-STRINGA-INPUT.

           CALL IWFS0050              USING AREA-CALL-IWFS0050

      *    MOVE IWFO0051-ESITO        TO IDSV0003-ESITO.
      *    MOVE IWFO0051-LOG-ERRORE   TO IDSV0003-LOG-ERRORE.
           MOVE IWFO0051-CAMPO-OUTPUT-DEFI
                                      TO WK-ID-NUM.

       EX-E000.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           MOVE SPACES                     TO IVVC0213-VAL-STR-O.
           MOVE 0                          TO IVVC0213-VAL-PERC-O.

           IF COMUN-TROV-SI
              MOVE WK-DATA-CPTZ-RIP
                TO IDSV0003-DATA-COMPETENZA

              MOVE WK-DATA-EFF-RIP
                TO IDSV0003-DATA-INIZIO-EFFETTO
                   IDSV0003-DATA-FINE-EFFETTO
           END-IF
      *
           GOBACK.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      *   COPY IMPOSTA DI BOLLO
      *----------------------------------------------------------------*

            COPY LCCVP583               REPLACING ==(SF)== BY ==DP58==.
            COPY LCCVP584               REPLACING ==(SF)== BY ==DP58==.



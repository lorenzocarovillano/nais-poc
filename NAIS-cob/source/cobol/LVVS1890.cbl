      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS1890.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2011.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      ***         CALCOLO DELLA VARIABILE FR - Frazion. Rendita      ***
      ***------------------------------------------------------------***

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS1890'.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*

      *---------------------------------------------------------------*
      *  COPY TIPOLOGICHE
      *---------------------------------------------------------------*
           COPY LCCVXMV0.

      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*

       01 AREA-IO-GAR.
          03 DGRZ-AREA-GRA.
             04 DGRZ-ELE-GAR-MAX        PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==DGRZ==.
             COPY LCCVGRZ1              REPLACING ==(SF)== BY ==DGRZ==.

       01 AREA-IO-PMO.
          03 DPMO-AREA-PMO.
             04 DPMO-ELE-PMO-MAX        PIC S9(04) COMP.
                COPY LCCVPMOB REPLACING   ==(SF)==  BY ==DPMO==.
             COPY LCCVPMO1              REPLACING ==(SF)== BY ==DPMO==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP VALUE 0.
           03 IX-TAB-GRZ                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-TGA                     PIC S9(04) COMP VALUE 0.
           03 IX-GUIDA-GRZ                   PIC S9(04) COMP VALUE 0.
           03 IX-TGA-APPO                    PIC S9(04) COMP VALUE 0.
           03 IX-TAB-PMO                     PIC S9(04) COMP VALUE 0.

       01  WK-ID-COD-LIV-FLAG-GRZ            PIC X(001).
           88 WK-FRZ-GAR-TROVATO          VALUE 'S'.
           88 WK-FRZ-GAR-NON-TROVATO      VALUE 'N'.

       01 WK-PMO-SCADENZA                    PIC 9(4) VALUE 6009.

      *
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS1450.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003
                                INPUT-LVVS1450.
      *
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.
      *
           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000
      *

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.

           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           INITIALIZE AREA-IO-GAR
                      AREA-IO-PMO.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

           MOVE IVVC0213-TIPO-MOVI-ORIG
             TO WS-MOVIMENTO.

       EX-S0000.
           EXIT.
      *
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE

           MOVE ZEROES TO IVVC0213-VAL-IMP-O.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL

             PERFORM S1200-CERCA-FRAZ-GAR
                THRU S1200-EX

             IF  IDSV0003-SUCCESSFUL-RC
             AND IDSV0003-SUCCESSFUL-SQL
             AND WK-FRZ-GAR-TROVATO
                PERFORM S1300-CERCA-PARAM-MOVI
                   THRU S1300-EX
             END-IF

           END-IF.

       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-GARANZIA
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DGRZ-AREA-GRA
           END-IF.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-PARAM-MOV
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DPMO-AREA-PMO
           END-IF.
      *
       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   CONTROLLO DATI DCLGEN
      *----------------------------------------------------------------*
       S1200-CERCA-FRAZ-GAR.
      *
           SET WK-FRZ-GAR-NON-TROVATO              TO TRUE.
      *
           IF DGRZ-ELE-GAR-MAX > 0
              IF DGRZ-FRAZ-INI-EROG-REN(IVVC0213-IX-TABB) IS NUMERIC
                 MOVE DGRZ-FRAZ-INI-EROG-REN(IVVC0213-IX-TABB)
                   TO IVVC0213-VAL-IMP-O
                 SET WK-FRZ-GAR-TROVATO    TO TRUE
              END-IF
           END-IF.
      *
      *
       S1200-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   CONTROLLO DATI DCLGEN
      *----------------------------------------------------------------*
       S1300-CERCA-PARAM-MOVI.
      *
           IF DPMO-ELE-PMO-MAX > 0
              PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
                UNTIL IX-TAB-PMO > DPMO-ELE-PMO-MAX
                 IF   DPMO-TP-MOVI(IX-TAB-PMO) IS NUMERIC
                  AND DPMO-TP-MOVI(IX-TAB-PMO) = WK-PMO-SCADENZA
                  AND DPMO-FRQ-MOVI(IX-TAB-PMO) IS NUMERIC
                       MOVE DPMO-FRQ-MOVI(IX-TAB-PMO)
                         TO IVVC0213-VAL-IMP-O
                 END-IF
              END-PERFORM
           END-IF.
      *
       S1300-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.


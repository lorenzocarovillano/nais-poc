      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA  VER 1.0                          **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LCCS0022.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2008.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *    PROGRAMMA ..... LCCS0022
      *    TIPOLOGIA...... CONTROLLI
      *    PROCESSO....... BLOCCHI
      *    FUNZIONE....... BLOCCHI
      *    DESCRIZIONE.... VERIFICA PRESENZA BLOCCHI
      *    PAGINA WEB..... N/A
      ***------------------------------------------------------------***
      *    DESCRIZIONE SERVIZIO: IL SERVIZIO RICEVE IN INPUT
      *    IL TIPO OGGETTO (POLIZZA O ADESIONE), L'ID POLIZZA (SEMPRE)
      *    ED EVENTUALMENTE L'ID ADESIONE (SE IL TP-OGGETTO E' ADESIONE)
      *    ED IL TIPO STATO BLOCCO (ATTIVO/NON ATTIVO).
      *
      *    IN OUTPUT RESTITUISCE I DATI DELLA TABELLA OGGETTO BLOCCO
      *    (SOLO QUELLI CHE HANNO LA GRAVITA' PRESENTE E CHE HANNO
      *    IL TIPO STATO BLOCCO UGUALE A QUELLO DI INPUT)
      *    E IL FLAG (FL-AMMIS-BLOC) SARA' VALORIZZATO CON 'S' SE
      *    ESISTE ALMENO 1 BLOCCO CON GRAVITA' BLOCCANTE, ALTRIMENTI 'N'
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    VARIABILI DI WORKING
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LCCS0022'.
      *----------------------------------------------------------------*
      *  LISTA MOVIMENTI IMPLEMENTATI LCCC0006
      *----------------------------------------------------------------*
           COPY LCCC0006.
           COPY LCCVXOG0.
      *----------------------------------------------------------------*
      *    COPY DELLE TABELLE DB2
      *----------------------------------------------------------------*
           COPY IDBVL111.
           COPY IDBVL161.
           COPY IDBVXAB1.
      *----------------------------------------------------------------*
      *    COPY DISPATCHER
      *----------------------------------------------------------------*
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
           COPY IDSI0011.
           COPY IDSO0011.
      *----------------------------------------------------------------*
      *    DEFINIZIONE DI INDICI                                       *
      *----------------------------------------------------------------*
       01 IX-INDICI.
          03 IX-TAB-L11                         PIC S9(04) COMP VALUE 0.
          03 IX-TAB-L16                         PIC S9(04) COMP VALUE 0.
      *----------------------------------------------------------------*
      *    VARIABILI                                                   *
      *----------------------------------------------------------------*
       01 WK-VARIABILI.
      *   CONTATORE DEGLI ERRORI BLOCCANTI
          03 WK-CONT-ERR-BLOCC                  PIC S9(04) COMP VALUE 0.

      *   CONTROLLO SU TIPO STATO BLOCCO
       01 WK-CTRL-TP-STAT-BLOC                  PIC X(2).
          88 WK-TP-STAT-BLOC-VALID              VALUE
                                                  'AT',
                                                  'NA'.
      *   CONTROLLO SU TIPO OGGETTO
       01 WK-CTRL-TP-OGG                        PIC X(2).
          88 WK-TP-OGG-VALID                    VALUE
                                                  'PO',
                                                  'AD',
                                                  'PV'.

       01 WK-GRAV-FUNZ-BLOCCO                   PIC X(1).
          88 WK-GRAVITA-WARNING                 VALUE 'W'.
          88 WK-GRAVITA-BLOCCANTE               VALUE 'B'.
          88 WK-GRAVITA-AMMESSO                 VALUE 'A'.

       01 WK-FL-GRAV-PRESENTE                   PIC X(1).
          88 WK-FL-GRAV-PRESENTE-SI             VALUE 'S'.
          88 WK-FL-GRAV-PRESENTE-NO             VALUE 'N'.

      *----------------------------------------------------------------*
      *    COPY VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
           COPY IEAV9903.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
       01   AREA-IDSV0001.
            COPY IDSV0001.
      *----------------------------------------------------------------*
      *     AREE COMUNICAZIONE
      *----------------------------------------------------------------*
      *   AREA COMUNE.
       01 WCOM-AREA-STATI.
          COPY LCCC0001                  REPLACING ==(SF)== BY ==WCOM==.
      *   INTERFACCIA DI COMUNICAZIONE (INPUT/OUTPUT)
       01 LCCC0022-AREA-COMUNICAZ.
          COPY LCCC0022.
      *---------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WCOM-AREA-STATI
                                LCCC0022-AREA-COMUNICAZ.
      *---------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *    OPERAZIONI INIZIALI                                         *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.
VSTEF      MOVE ZEROES TO IX-TAB-L11.
ALCO       MOVE ZEROES TO WK-CONT-ERR-BLOCC.

      *--> CONTROLLI FORMALI
           PERFORM S0050-CONTROLLI            THRU EX-S0050.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    CONTROLLI FORMALLI                                          *
      *----------------------------------------------------------------*
       S0050-CONTROLLI.

           MOVE LCCC0022-TP-STATO-BLOCCO     TO WK-CTRL-TP-STAT-BLOC.
           MOVE LCCC0022-TP-OGG              TO WK-CTRL-TP-OGG
                                                WS-TP-OGG.
      *--> ID POLIZZA
           IF LCCC0022-ID-POLI = ZEROES
      *       IL CAMPO $ DEVE ESSERE VALORIZZATO
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0050-CONTROLLI'         TO IEAI9901-LABEL-ERR
              MOVE '005007'                  TO IEAI9901-COD-ERRORE
              MOVE 'ID POLIZZA'              TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

      *--> ID ADESIONE
           IF IDSV0001-ESITO-OK
              IF ADESIONE
                 IF LCCC0022-ID-ADES = ZEROES
      *             IL CAMPO $ DEVE ESSERE VALORIZZATO
                    MOVE WK-PGM              TO IEAI9901-COD-SERVIZIO-BE
                    MOVE 'S0050-CONTROLLI'   TO IEAI9901-LABEL-ERR
                    MOVE '005007'            TO IEAI9901-COD-ERRORE
                    MOVE 'ID ADESIONE'       TO IEAI9901-PARAMETRI-ERR
                    PERFORM S0300-RICERCA-GRAVITA-ERRORE
                       THRU EX-S0300
                 END-IF
              END-IF
           END-IF.

      *--> TIPO OGGETTO (VALORIZZATO)
           IF IDSV0001-ESITO-OK
              IF LCCC0022-TP-OGG  = SPACES OR HIGH-VALUE
      *          IL CAMPO $ DEVE ESSERE VALORIZZATO
                 MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S0050-CONTROLLI'      TO IEAI9901-LABEL-ERR
                 MOVE '005007'               TO IEAI9901-COD-ERRORE
                 MOVE 'TIPO OGGETTO'         TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

      *--> TIPO OGGETTO (VALIDO)
           IF IDSV0001-ESITO-OK
              IF NOT WK-TP-OGG-VALID
      *          IL CAMPO $ DEVE ESSERE VALORIZZATO
                 MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S0050-CONTROLLI'      TO IEAI9901-LABEL-ERR
                 MOVE '005018'               TO IEAI9901-COD-ERRORE
                 MOVE 'TIPO OGGETTO'         TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

      *--> TIPO STATO BLOCCO (VALORIZZATO)
           IF IDSV0001-ESITO-OK
              IF LCCC0022-TP-STATO-BLOCCO = SPACES OR HIGH-VALUE
      *          IL CAMPO $ DEVE ESSERE VALORIZZATO
                 MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S0050-CONTROLLI'      TO IEAI9901-LABEL-ERR
                 MOVE '005007'               TO IEAI9901-COD-ERRORE
                 MOVE 'TIPO STATO BLOCCO'    TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

      *--> TIPO STATO BLOCCO (VALIDO)
           IF IDSV0001-ESITO-OK
              IF NOT WK-TP-STAT-BLOC-VALID
      *          IL CAMPO $ NON E' VALIDO
                 MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S0050-CONTROLLI'      TO IEAI9901-LABEL-ERR
                 MOVE '005018'               TO IEAI9901-COD-ERRORE
                 MOVE 'TIPO STATO BLOCCO'    TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

       EX-S0050.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> SE IL TP-OGG E' ADESIONE
           IF IDSV0001-ESITO-OK
              IF ADESIONE
      *-->       LETTURA OGGETTO BLOCCO PER ADESIONE
                 PERFORM S1100-LETTURA-OGG-BLOCCO-ADE
                    THRU EX-S1100
              END-IF
           END-IF.

      *--> LETTURA OGGETTO BLOCCO PER POLIZZA
           IF IDSV0001-ESITO-OK
              PERFORM S1105-LETTURA-OGG-BLOCCO-POL
                 THRU EX-S1105
           END-IF.

           IF IDSV0001-ESITO-OK
      *-->    SE CI SONO ERRORI BLOCCANTI
              IF WK-CONT-ERR-BLOCC > 0
                 MOVE 'S' TO LCCC0022-FL-PRES-GRAV-BLOC
              ELSE
                 MOVE 'N' TO LCCC0022-FL-PRES-GRAV-BLOC
              END-IF
           END-IF.

       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    LETTURA TABELLA OGGETTO BLOCCO (ADESIONE)
      *----------------------------------------------------------------*
       S1100-LETTURA-OGG-BLOCCO-ADE.

      *--> VALORIZZAZIONE ID-OGG CON ID ADESIONE
           MOVE LCCC0022-ID-ADES      TO L11-ID-OGG.
           MOVE LCCC0022-TP-OGG       TO L11-TP-OGG.

      *--> PREPARA DATI PER CALL LDBS2410
           PERFORM S1110-PREPARA-AREA-LDBS2410
              THRU EX-S1110.
      *--> CALL LDBS2410
           PERFORM S1120-CALL-LDBS2410
              THRU EX-S1120.

           MOVE IX-TAB-L11    TO LCCC0022-ELE-MAX-OGG-BLOC.

       EX-S1100.
           EXIT.

      *----------------------------------------------------------------*
      *    LETTURA TABELLA OGGETTO BLOCCO (POLIZZA)
      *----------------------------------------------------------------*
       S1105-LETTURA-OGG-BLOCCO-POL.

      *--> VALORIZZAZIONE ID-OGG CON ID POLIZZA
           MOVE LCCC0022-ID-POLI      TO L11-ID-OGG.
           SET  POLIZZA               TO TRUE.
           MOVE WS-TP-OGG             TO L11-TP-OGG.

      *--> PREPARA DATI PER CALL LDBS2410
           PERFORM S1110-PREPARA-AREA-LDBS2410
              THRU EX-S1110.
      *--> CALL LDBS2410
           PERFORM S1120-CALL-LDBS2410
              THRU EX-S1120.

           MOVE IX-TAB-L11    TO LCCC0022-ELE-MAX-OGG-BLOC.

       EX-S1105.
           EXIT.

      *----------------------------------------------------------------*
      *    PREPARA AREA PER LETTURA OGGETTO BLOCCO (LDBS2410)
      *----------------------------------------------------------------*
       S1110-PREPARA-AREA-LDBS2410.

           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA     TO L11-COD-COMP-ANIA.
           MOVE LCCC0022-TP-STATO-BLOCCO        TO L11-TP-STAT-BLOCCO.

           MOVE 'LDBS2410'               TO IDSI0011-CODICE-STR-DATO.
           MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE OGG-BLOCCO               TO IDSI0011-BUFFER-DATI.
           MOVE OGG-BLOCCO               TO IDSI0011-BUFFER-WHERE-COND.

           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.
           SET IDSI0011-FETCH-FIRST      TO TRUE.
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

       EX-S1110.
           EXIT.

      *----------------------------------------------------------------*
      *   LETTURA OGGETTO BLOCCO (LDBS2410)
      *----------------------------------------------------------------*
       S1120-CALL-LDBS2410.

      *--> CICLO PER LA LETTURA SUL DB
           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSO0011-SUCCESSFUL-SQL
                      OR IDSV0001-ESITO-KO

      *-->     EFFETTUO LA LETTURA SUL DB
               PERFORM CALL-DISPATCHER
                  THRU CALL-DISPATCHER-EX

      *-->     SE IL RETURN CODE E' TRUE
               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE

      *-->            SE NON HA TROVATO NESSUNA OCCORRENZA
                      WHEN IDSO0011-NOT-FOUND
                           CONTINUE

      *-->            SE CI SONO DELLE OCCORRENZE
                      WHEN IDSO0011-SUCCESSFUL-SQL
                           MOVE IDSO0011-BUFFER-DATI
                             TO OGG-BLOCCO

      *-->                 LETTURA SU AMMISSIBILITA' FUNZIONE BLOCCO
      *                    PER RICERCARE LA GRAVITA'
                           PERFORM S1300-LETTURA-AMM-BLOCCO
                              THRU EX-S1300

      *-->                 SE E' PRESENTE LA GRAVITA' PER IL BLOCCO
                           IF WK-FL-GRAV-PRESENTE-SI
                              ADD 1 TO IX-TAB-L11

      *-->                    VALORIZZA OUTPUT AREA COMUNICAZIONE
                              PERFORM VALORIZZA-OUTPUT-L11
                                 THRU VALORIZZA-OUTPUT-L11-EX

                              MOVE L16-GRAV-FUNZ-BLOCCO
                                TO LCCC0022-OGB-GRAVITA(IX-TAB-L11)

      *-->                    LETTURA SU ANA-BLOCCO PER RECUPERARE LA
      *                       DESCRIZIONE
                              PERFORM S1400-LETTURA-ANA-BLOCCO
                                 THRU EX-S1400

                           END-IF

                           PERFORM S1110-PREPARA-AREA-LDBS2410
                              THRU EX-S1110
                           SET  IDSI0011-FETCH-NEXT  TO TRUE

                  END-EVALUATE
               ELSE
      *-->        ERRORE DISPATCHER
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S1120-CALL-LDBS2410'
                    TO IEAI9901-LABEL-ERR
                  MOVE '005016'
                    TO IEAI9901-COD-ERRORE
                  STRING 'LDBS2410'           ';'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                  DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF
           END-PERFORM.

       EX-S1120.
           EXIT.

      *----------------------------------------------------------------*
      *    LETTURA TABELLA AMMISSIBILITA' FUNZIONE BLOCCO
      *----------------------------------------------------------------*
       S1300-LETTURA-AMM-BLOCCO.

      *--> LA LETTURA DELLA TABELLA AMMB_FUNZ_BLOCCO VIENE EFFETTUATA
      *--> CON 5 MODALITA' DI ACCESSO DIFFERENTI, PARTENDO DA UN
      *--> ACCESSO CON DETTAGLIO MAGGIORE A QUELLO CON DETTAGLIO MINORE
      *--> 1) ACCESSO CON COD_BLOCCO LETTO DALLA TABELLA OGG_BLOCCO
      *-->    TP_MOVI DI CONTESTO CODICE-CANALE LEGATO ALL'UTENZA
      *-->    D'ACCESSO E TIPO MOVIMENTO RIFERIMENTO UGUALE A TIPO
      *-->    MOVIMENTO PRESENTE SULLA TABELLA OGG_BLOCCO
      *--> 2) ACCESSO CON COD_BLOCCO LETTO DALLA TABELLA OGG_BLOCCO
      *-->    TP_MOVI DI CONTESTO E CODICE-CANALE LEGATO ALL'UTENZA
      *-->    D'ACCESSO
      *--> 3) ACCESSO CON COD_BLOCCO LETTO DALLA TABELLA OGG_BLOCCO
      *-->    TP_MOVI DI CONTESTO E CODICE-CANALE ZERO, OVVERO PER
      *-->    UTENZA DI DEFAULT
      *--> 4) ACCESSO CON COD_BLOCCO LETTO DALLA TABELLA OGG_BLOCCO
      *-->    TP_MOVI ZERO OVVERO TUTTI GENERALIZZATO PER TUTTI I
      *-->    TIPO MOVIMENTO E CODICE-CANALE ZERO, LEGATO ALL'UTENZA
      *-->    D'ACCESSO
      *--> 5) ACCESSO CON COD_BLOCCO LETTO DALLA TABELLA OGG_BLOCCO
      *-->    TP_MOVI ZERO OVVERO TUTTI GENERALIZZATO PER TUTTI I
      *-->    TIPO MOVIMENTO E CODICE-CANALE ZERO, OVVERO PER
      *-->    UTENZA DI DEFAULT


           SET  WK-FL-GRAV-PRESENTE-NO    TO TRUE.

      *--> PREPARA DATI PER CALL LDBS2420
           PERFORM S1310-PREPARA-AREA-LDBSF920
              THRU EX-S1310.
      *--> CALL LDBS2420
           PERFORM S1320-CALL-LDBSF920
              THRU EX-S1320.

       EX-S1300.
           EXIT.

      *----------------------------------------------------------------*
      *    LETTURA TABELLA ANAGRAFICA BLOCCO
      *----------------------------------------------------------------*
       S1400-LETTURA-ANA-BLOCCO.

      *--> PREPARA DATI PER CALL ANA-BLOCCO
           PERFORM S1410-PREPARA-AREA-ANA-BLOCCO
              THRU EX-S1410.
      *--> CALL ANA-BLOCCO
           PERFORM S1420-CALL-ANA-BLOCCO
              THRU EX-S1420.

       EX-S1400.
           EXIT.

      *----------------------------------------------------------------*
      *    PREPARA AREA PER LETTURA TABELLA ANAGRAFICA BLOCCO
      *----------------------------------------------------------------*
       S1410-PREPARA-AREA-ANA-BLOCCO.

           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.


           MOVE IDSV0001-COD-COMPAGNIA-ANIA     TO XAB-COD-COMP-ANIA.
           MOVE L11-COD-BLOCCO                  TO XAB-COD-BLOCCO.

           MOVE 'ANA-BLOCCO'             TO IDSI0011-CODICE-STR-DATO.
           MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE ANA-BLOCCO               TO IDSI0011-BUFFER-DATI.

           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-SELECT           TO TRUE.
           SET IDSI0011-PRIMARY-KEY      TO TRUE.
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

       EX-S1410.
           EXIT.

      *----------------------------------------------------------------*
      *   LETTURA ANAGRAFICA BLOCCO
      *----------------------------------------------------------------*
       S1420-CALL-ANA-BLOCCO.

      *--> EFFETTUO LA LETTURA SUL DB
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

      *--> SE IL RETURN CODE E' TRUE
           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE

      *-->        SE NON HA TROVATO NESSUNA OCCORRENZA
                  WHEN IDSO0011-NOT-FOUND
                       MOVE SPACES
                         TO LCCC0022-OGB-DESCRIZIONE(IX-TAB-L11)

      *-->        SE CI SONO DELLE OCCORRENZE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI
                         TO ANA-BLOCCO

                       MOVE XAB-DESC
                         TO LCCC0022-OGB-DESCRIZIONE(IX-TAB-L11)

              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1420-CALL-ANA-BLOCCO'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'ANA-BLOCCO'         ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1420.
           EXIT.

      *----------------------------------------------------------------*
      *   VALORIZZA OUTPUT OGGETTO BLOCCO
      *----------------------------------------------------------------*
       VALORIZZA-OUTPUT-L11.

           MOVE L11-ID-OGG-BLOCCO
             TO LCCC0022-OGB-ID-OGG-BLOCCO(IX-TAB-L11).
           MOVE L11-ID-OGG-1RIO
             TO LCCC0022-OGB-ID-OGG-1RIO(IX-TAB-L11).
           MOVE L11-TP-OGG-1RIO
             TO LCCC0022-OGB-TP-OGG-1RIO(IX-TAB-L11).
           MOVE L11-COD-COMP-ANIA
             TO LCCC0022-OGB-COD-COMP-ANIA(IX-TAB-L11).
           MOVE L11-TP-MOVI
             TO LCCC0022-OGB-TP-MOVI(IX-TAB-L11).
           MOVE L11-TP-OGG
             TO LCCC0022-OGB-TP-OGG(IX-TAB-L11).
           MOVE L11-ID-OGG
             TO LCCC0022-OGB-ID-OGG(IX-TAB-L11).
           MOVE L11-DT-EFF
             TO LCCC0022-OGB-DT-EFFETTO(IX-TAB-L11).
           MOVE L11-TP-STAT-BLOCCO
             TO LCCC0022-OGB-TP-STAT-BLOCCO(IX-TAB-L11).
           MOVE L11-COD-BLOCCO
             TO LCCC0022-OGB-COD-BLOCCO(IX-TAB-L11).

       VALORIZZA-OUTPUT-L11-EX.
           EXIT.
12431 *----------------------------------------------------------------*
      *    PREPARA AREA PER LETTURA AMMISSIB. FUNZIONE BLOCCO (LDBSF920)
      *----------------------------------------------------------------*
       S1310-PREPARA-AREA-LDBSF920.

           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA  TO L16-COD-COMP-ANIA.
           MOVE L11-COD-BLOCCO               TO L16-COD-BLOCCO.
           MOVE IDSV0001-TIPO-MOVIMENTO      TO L16-TP-MOVI.
           MOVE WCOM-CANALE-VENDITA          TO L16-COD-CAN.
           MOVE L11-TP-MOVI                  TO L16-TP-MOVI-RIFTO.

           MOVE 'LDBSF920'               TO IDSI0011-CODICE-STR-DATO.
           MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-DATI.
           MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-WHERE-COND.

           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.
           SET IDSI0011-SELECT           TO TRUE.
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

       EX-S1310.
           EXIT.
      *----------------------------------------------------------------*
      *   LETTURA AMMISSIBILITA' FUNZIONE BLOCCO (LDBSF920)
      *----------------------------------------------------------------*
       S1320-CALL-LDBSF920.

      *--> EFFETTUO LA LETTURA SUL DB
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

      *--> SE IL RETURN CODE E' TRUE
           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE

      *-->        SE NON HA TROVATO NESSUNA OCCORRENZA
                  WHEN IDSO0011-NOT-FOUND
      *-->            PREPARA DATI PER CALL LDBS2420
                      PERFORM S1150-PREPARA-AREA-LDBSE200-1
                         THRU EX-S1150
      *-->            CALL LDBS2420
                      PERFORM S1160-CALL-LDBSE200-1
                         THRU EX-S1160

      *-->        SE CI SONO DELLE OCCORRENZE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI
                         TO AMMB-FUNZ-BLOCCO

                       MOVE L16-GRAV-FUNZ-BLOCCO
                         TO WK-GRAV-FUNZ-BLOCCO

                       IF WK-GRAVITA-AMMESSO
                          CONTINUE
                       ELSE
                          SET  WK-FL-GRAV-PRESENTE-SI  TO TRUE
                       END-IF

                       IF WK-GRAVITA-BLOCCANTE
                          ADD 1 TO WK-CONT-ERR-BLOCC
                       END-IF

              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1320-CALL-LDBSF920'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'LDBSF920'           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1320.
12431      EXIT.

      *----------------------------------------------------------------*
      *    PREPARA AREA PER LETTURA AMMISSIB. FUNZIONE BLOCCO (LDBSE200)
      *----------------------------------------------------------------*
       S1150-PREPARA-AREA-LDBSE200-1.

           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA  TO L16-COD-COMP-ANIA.
           MOVE L11-COD-BLOCCO               TO L16-COD-BLOCCO.
           MOVE IDSV0001-TIPO-MOVIMENTO      TO L16-TP-MOVI.
           MOVE WCOM-CANALE-VENDITA          TO L16-COD-CAN.

           MOVE 'LDBSE200'               TO IDSI0011-CODICE-STR-DATO.
           MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-DATI.
           MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-WHERE-COND.

           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.
           SET IDSI0011-SELECT           TO TRUE.
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

       EX-S1150.
           EXIT.

      *----------------------------------------------------------------*
      *   LETTURA AMMISSIBILITA' FUNZIONE BLOCCO (LDBSE200) PER CANALE
      *----------------------------------------------------------------*
       S1160-CALL-LDBSE200-1.

      *--> EFFETTUO LA LETTURA SUL DB
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

      *--> SE IL RETURN CODE E' TRUE
           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE

      *-->        SE NON HA TROVATO NESSUNA OCCORRENZA
                  WHEN IDSO0011-NOT-FOUND
      *-->             PREPARA DATI PER CALL LDBSE200
                       PERFORM S1130-PREPARA-AREA-LDBSE200-2
                          THRU EX-S1130
      *-->             CALL LDBS2420
                       PERFORM S1140-CALL-LDBSE200-2
                          THRU EX-S1140

      *-->        SE CI SONO DELLE OCCORRENZE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI
                         TO AMMB-FUNZ-BLOCCO

                       MOVE L16-GRAV-FUNZ-BLOCCO
                         TO WK-GRAV-FUNZ-BLOCCO

                       IF WK-GRAVITA-AMMESSO
                          CONTINUE
                       ELSE
                          SET  WK-FL-GRAV-PRESENTE-SI  TO TRUE
                       END-IF

                       IF WK-GRAVITA-BLOCCANTE
                          ADD 1 TO WK-CONT-ERR-BLOCC
                       END-IF

              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1160-CALL-LDBSE200-1'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'LDBSE200'           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1160.
           EXIT.

      *----------------------------------------------------------------*
      * PREPARA LETTURA AMMISSIB. FUNZIONE BLOCCO (LDBSE200) PER CANALE
      *----------------------------------------------------------------*
       S1170-PREPARA-AREA-LDBSE200-3.

           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA  TO L16-COD-COMP-ANIA.
           MOVE L11-COD-BLOCCO               TO L16-COD-BLOCCO.
           MOVE ZEROES                       TO L16-TP-MOVI.
           MOVE WCOM-CANALE-VENDITA          TO L16-COD-CAN.

           MOVE 'LDBSE200'               TO IDSI0011-CODICE-STR-DATO.
           MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-DATI.
           MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-WHERE-COND.

           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.
           SET IDSI0011-SELECT           TO TRUE.
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

       EX-S1170.
           EXIT.

      *----------------------------------------------------------------*
      *   LETTURA AMMISSIBILITA' FUNZIONE BLOCCO (LDBSE200) PER CANALE
      *----------------------------------------------------------------*
       S1180-CALL-LDBSE200-3.

      *--> EFFETTUO LA LETTURA SUL DB
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

      *--> SE IL RETURN CODE E' TRUE
           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE

      *-->        SE NON HA TROVATO NESSUNA OCCORRENZA
                  WHEN IDSO0011-NOT-FOUND
                       PERFORM S1131-PREPARA-AREA-LDBSE200-4
                          THRU EX-S1131
      *-->             CALL LDBS2420
                       PERFORM S1141-CALL-LDBSE200-4
                          THRU EX-S1141

      *-->        SE CI SONO DELLE OCCORRENZE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI
                         TO AMMB-FUNZ-BLOCCO

                       MOVE L16-GRAV-FUNZ-BLOCCO
                         TO WK-GRAV-FUNZ-BLOCCO

                       IF WK-GRAVITA-AMMESSO
                          CONTINUE
                       ELSE
                          SET  WK-FL-GRAV-PRESENTE-SI  TO TRUE
                       END-IF

                       IF WK-GRAVITA-BLOCCANTE
                          ADD 1 TO WK-CONT-ERR-BLOCC
                       END-IF

              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1180-CALL-LDBSE200-2'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'LDBSE200'           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1180.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPARA AREA PER LETTURA AMMISSIB. FUNZIONE BLOCCO (LDBSE200)
      *----------------------------------------------------------------*
       S1130-PREPARA-AREA-LDBSE200-2.

           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA  TO L16-COD-COMP-ANIA.
           MOVE L11-COD-BLOCCO               TO L16-COD-BLOCCO.
           MOVE IDSV0001-TIPO-MOVIMENTO      TO L16-TP-MOVI.
           MOVE ZEROES                       TO L16-COD-CAN.

           MOVE 'LDBSE200'               TO IDSI0011-CODICE-STR-DATO.
           MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-DATI.
           MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-WHERE-COND.

           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.
           SET IDSI0011-SELECT           TO TRUE.
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

       EX-S1130.
           EXIT.

      *----------------------------------------------------------------*
      *   LETTURA AMMISSIBILITA' FUNZIONE BLOCCO (LDBSE200)
      *----------------------------------------------------------------*
       S1140-CALL-LDBSE200-2.

      *--> EFFETTUO LA LETTURA SUL DB
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

      *--> SE IL RETURN CODE E' TRUE
           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE

      *-->        SE NON HA TROVATO NESSUNA OCCORRENZA
                  WHEN IDSO0011-NOT-FOUND
      *-->             PREPARA DATI PER CALL LDBSE200
                       PERFORM S1170-PREPARA-AREA-LDBSE200-3
                          THRU EX-S1170
      *-->             CALL LDBSE200
                       PERFORM S1180-CALL-LDBSE200-3
                          THRU EX-S1180

      *-->        SE CI SONO DELLE OCCORRENZE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI
                         TO AMMB-FUNZ-BLOCCO

                       MOVE L16-GRAV-FUNZ-BLOCCO
                         TO WK-GRAV-FUNZ-BLOCCO

                       IF WK-GRAVITA-AMMESSO
                          CONTINUE
                       ELSE
                          SET  WK-FL-GRAV-PRESENTE-SI  TO TRUE
                       END-IF

                       IF WK-GRAVITA-BLOCCANTE
                          ADD 1 TO WK-CONT-ERR-BLOCC
                       END-IF

              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1140-CALL-LDBSE200-2'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'LDBS2410'           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1140.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPARA AREA PER LETTURA AMMISSIB. FUNZIONE BLOCCO (LDBSE200)
      *----------------------------------------------------------------*
       S1131-PREPARA-AREA-LDBSE200-4.

           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA  TO L16-COD-COMP-ANIA.
           MOVE L11-COD-BLOCCO               TO L16-COD-BLOCCO.
           MOVE ZEROES                       TO L16-TP-MOVI.
           MOVE ZEROES                       TO L16-COD-CAN.

           MOVE 'LDBSE200'               TO IDSI0011-CODICE-STR-DATO.
           MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-DATI.
           MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-WHERE-COND.

           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.
           SET IDSI0011-SELECT           TO TRUE.
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

       EX-S1131.
           EXIT.

      *----------------------------------------------------------------*
      *   LETTURA AMMISSIBILITA' FUNZIONE BLOCCO (LDBSE200)
      *----------------------------------------------------------------*
       S1141-CALL-LDBSE200-4.

      *--> EFFETTUO LA LETTURA SUL DB
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

      *--> SE IL RETURN CODE E' TRUE
           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE

      *-->        SE NON HA TROVATO NESSUNA OCCORRENZA
                  WHEN IDSO0011-NOT-FOUND
                       CONTINUE

      *-->        SE CI SONO DELLE OCCORRENZE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI
                         TO AMMB-FUNZ-BLOCCO

                       MOVE L16-GRAV-FUNZ-BLOCCO
                         TO WK-GRAV-FUNZ-BLOCCO

                       IF WK-GRAVITA-AMMESSO
                          CONTINUE
                       ELSE
                          SET  WK-FL-GRAV-PRESENTE-SI  TO TRUE
                       END-IF

                       IF WK-GRAVITA-BLOCCANTE
                          ADD 1 TO WK-CONT-ERR-BLOCC
                       END-IF

              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1141-CALL-LDBSE200-4'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'LDBS2410'           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1141.
           EXIT.
      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.
      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORE
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.
           COPY IERP9903.

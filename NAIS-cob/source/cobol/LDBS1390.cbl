       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LDBS1390 IS INITIAL.
      *****************************************************************
      *                                                               *
      *                    PGM :  LDBS1390                            *
      *                                                               *
      *****************************************************************
       AUTHOR.  ATS NAPOLI.
      *****************************************************************
       ENVIRONMENT DIVISION.
      *****************************************************************
       CONFIGURATION SECTION.
      *****************************************************************
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
      *****************************************************************
       DATA DIVISION.
      *****************************************************************

      *****************************************************************
       WORKING-STORAGE SECTION.
      *
       77  WK-IND-SERV                      PIC S9(4) COMP VALUE ZEROES.
       77  WS-SQLCODE                       PIC S9(009) VALUE ZEROES.
      *****************************************************************
      *               DEFINIZIONE SQLCA E TABELLE                     *
      *****************************************************************
           EXEC SQL INCLUDE SQLCA    END-EXEC.

           EXEC SQL INCLUDE IDBDMVV0 END-EXEC.
           EXEC SQL INCLUDE IDBVMVV1 END-EXEC.
           EXEC SQL INCLUDE IDBVMVV2 END-EXEC.

           EXEC SQL INCLUDE IDBDADA0 END-EXEC.
           EXEC SQL INCLUDE IDBVADA1 END-EXEC.
           EXEC SQL INCLUDE IDBVADA2 END-EXEC.

      *****************************************************************
       LINKAGE SECTION.
      *
       01  DISPATCHER-VARIABLES.
      ** COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      ** COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.
      *
           EXEC SQL INCLUDE LDBV1391 END-EXEC.

      ***************************************************************
       PROCEDURE DIVISION USING DISPATCHER-VARIABLES
                                V1391-AREA-ACTU.
      *
           MOVE ZEROES                       TO WK-IND-SERV.

           INITIALIZE ANAG-DATO
                      MATR-VAL-VAR.
      *               IDSO0011-AREA
      *
           SET IDSO0011-SUCCESSFUL-RC        TO TRUE.
      *

           MOVE V1391-COD-COMPAGNIA-ANIA     TO ADA-COD-COMPAGNIA-ANIA
                                                MVV-COD-COMPAGNIA-ANIA.
           MOVE V1391-COD-ACTU               TO ADA-COD-DATO
                                                MVV-COD-DATO-EXT.

           PERFORM A100-SELECT-MVV           THRU A100-EX.
      *
           GOBACK.
      *

      *****************************************************************
      * SELECT MVV
      *****************************************************************
       A100-SELECT-MVV.
      *
           PERFORM Z960-LENGTH-VCHAR             THRU Z960-EX.

           EXEC SQL
              SELECT A.TIPO_DATO
                    ,A.LUNGHEZZA_DATO
                    ,A.PRECISIONE_DATO
                    ,A.FORMATTAZIONE_DATO
                    ,B.ID_MATR_VAL_VAR
                    ,B.IDP_MATR_VAL_VAR
                    ,B.TIPO_MOVIMENTO
                    ,B.COD_DATO_EXT
                    ,B.OBBLIGATORIETA
                    ,B.VALORE_DEFAULT
                    ,B.COD_STR_DATO_PTF
                    ,B.COD_DATO_PTF
                    ,B.COD_PARAMETRO
                    ,B.OPERAZIONE
                    ,B.LIVELLO_OPERAZIONE
                    ,B.TIPO_OGGETTO
                    ,B.WHERE_CONDITION
                    ,B.SERVIZIO_LETTURA
                    ,B.MODULO_CALCOLO
                    ,B.COD_DATO_INTERNO
                INTO  :ADA-TIPO-DATO
                      :IND-ADA-TIPO-DATO
                     ,:ADA-LUNGHEZZA-DATO
                      :IND-ADA-LUNGHEZZA-DATO
                     ,:ADA-PRECISIONE-DATO
                      :IND-ADA-PRECISIONE-DATO
                     ,:ADA-FORMATTAZIONE-DATO
                      :IND-ADA-FORMATTAZIONE-DATO
                     ,:MVV-ID-MATR-VAL-VAR
                     ,:MVV-IDP-MATR-VAL-VAR
                      :IND-MVV-IDP-MATR-VAL-VAR
                     ,:MVV-TIPO-MOVIMENTO
                      :IND-MVV-TIPO-MOVIMENTO
                     ,:MVV-COD-DATO-EXT
                      :IND-MVV-COD-DATO-EXT
                     ,:MVV-OBBLIGATORIETA
                      :IND-MVV-OBBLIGATORIETA
                     ,:MVV-VALORE-DEFAULT
                      :IND-MVV-VALORE-DEFAULT
                     ,:MVV-COD-STR-DATO-PTF
                      :IND-MVV-COD-STR-DATO-PTF
                     ,:MVV-COD-DATO-PTF
                      :IND-MVV-COD-DATO-PTF
                     ,:MVV-COD-PARAMETRO
                      :IND-MVV-COD-PARAMETRO
                     ,:MVV-OPERAZIONE
                      :IND-MVV-OPERAZIONE
                     ,:MVV-LIVELLO-OPERAZIONE
                      :IND-MVV-LIVELLO-OPERAZIONE
                     ,:MVV-TIPO-OGGETTO
                      :IND-MVV-TIPO-OGGETTO
                     ,:MVV-WHERE-CONDITION-VCHAR
                      :IND-MVV-WHERE-CONDITION
                     ,:MVV-SERVIZIO-LETTURA
                      :IND-MVV-SERVIZIO-LETTURA
                     ,:MVV-MODULO-CALCOLO
                      :IND-MVV-MODULO-CALCOLO
                     ,:MVV-COD-DATO-INTERNO
                      :IND-MVV-COD-DATO-INTERNO
              FROM ANAG_DATO A, MATR_VAL_VAR B
               WHERE A.COD_COMPAGNIA_ANIA  =
                    :ADA-COD-COMPAGNIA-ANIA
                AND B.COD_COMPAGNIA_ANIA =
                    :MVV-COD-COMPAGNIA-ANIA
                AND A.COD_DATO =
                    :V1391-COD-ACTU
                AND B.COD_DATO_EXT =
                    :V1391-COD-ACTU
                AND B.STEP_VALORIZZATORE = 'B'
               END-EXEC.

           MOVE SQLCODE           TO IDSO0011-SQLCODE-SIGNED.

           IF NOT IDSO0011-SUCCESSFUL-SQL

              IF IDSO0011-MORE-THAN-ONE-ROW
                 PERFORM B000-ACCEDI-X-CURSORE THRU B000-EX
              ELSE
                 IF IDSO0011-NOT-FOUND
                    STRING 'VARIABILE '
                      V1391-COD-ACTU ' '
                      'NON ANCORA CENSITA SULLA TABELLA '
                      'DEL VALORIZZATORE : '
                      IDSO0011-NOME-TABELLA
                    DELIMITED BY SIZE INTO IDSO0011-DESCRIZ-ERR-DB2
                 ELSE
                    MOVE 'MATR_VAL_VAR'  TO IDSO0011-NOME-TABELLA
                    STRING 'ERRORE CHIAMATA LDBS1390 ;'
                       IDSO0011-SQLCODE-SIGNED ';'
                       V1391-COD-ACTU
                    DELIMITED BY SIZE INTO IDSO0011-DESCRIZ-ERR-DB2
                 END-IF

                 SET IDSO0011-SQL-ERROR             TO TRUE
              END-IF

           ELSE
              PERFORM N000-CNTL-CAMPI-NULL        THRU N000-EX
              MOVE 1                              TO WK-IND-SERV
              PERFORM N001-IMPOSTA-OUTPUT         THRU N001-EX
              MOVE WK-IND-SERV                    TO V1391-ELE-MAX-ACTU
           END-IF.
      *
       A100-EX.
           EXIT.

      *****************************************************************
      * ACCEDI X CURSORE
      *****************************************************************
       B000-ACCEDI-X-CURSORE.
      *
           PERFORM B100-DECLARE-CUR-SERVIZI      THRU B100-EX.
           PERFORM B200-OPEN-CUR-SERVIZI         THRU B200-EX.
           PERFORM B250-FETCH-FIRST              THRU B250-EX.
      *
           IF IDSO0011-SUCCESSFUL-RC
              PERFORM B300-FETCH-CUR-SERVIZI     THRU B300-EX
                      UNTIL NOT IDSO0011-SUCCESSFUL-SQL
           END-IF.
      *
           IF IDSO0011-SUCCESSFUL-RC
              PERFORM B400-CLOSE-CUR-SERVIZI     THRU B400-EX
           END-IF.
      *
       B000-EX.
           EXIT.
      *****************************************************************
      * DECLARE CURSORE SERVIZI
      *****************************************************************
       B100-DECLARE-CUR-SERVIZI.
      *
           PERFORM Z960-LENGTH-VCHAR             THRU Z960-EX.

           EXEC SQL DECLARE CUR_SERVIZI CURSOR FOR
              SELECT A.TIPO_DATO
                    ,A.LUNGHEZZA_DATO
                    ,A.PRECISIONE_DATO
                    ,A.FORMATTAZIONE_DATO
                    ,B.ID_MATR_VAL_VAR
                    ,B.IDP_MATR_VAL_VAR
                    ,B.TIPO_MOVIMENTO
                    ,B.COD_DATO_EXT
                    ,B.OBBLIGATORIETA
                    ,B.VALORE_DEFAULT
                    ,B.COD_STR_DATO_PTF
                    ,B.COD_DATO_PTF
                    ,B.COD_PARAMETRO
                    ,B.OPERAZIONE
                    ,B.LIVELLO_OPERAZIONE
                    ,B.TIPO_OGGETTO
                    ,B.WHERE_CONDITION
                    ,B.SERVIZIO_LETTURA
                    ,B.MODULO_CALCOLO
                    ,B.COD_DATO_INTERNO
              FROM ANAG_DATO A, MATR_VAL_VAR B
               WHERE A.COD_COMPAGNIA_ANIA  =
                    :ADA-COD-COMPAGNIA-ANIA
                AND B.COD_COMPAGNIA_ANIA =
                    :MVV-COD-COMPAGNIA-ANIA
                AND A.COD_DATO =
                    :V1391-COD-ACTU
                AND B.COD_DATO_EXT =
                    :V1391-COD-ACTU
                AND B.STEP_VALORIZZATORE = 'B'
                ORDER BY B.ID_MATR_VAL_VAR
               END-EXEC.
      *
       B100-EX.
           EXIT.
      *****************************************************************
      * OPEN CUR-SERVIZI
      ****************************************************************
       B200-OPEN-CUR-SERVIZI.
      *
           EXEC SQL OPEN CUR_SERVIZI END-EXEC.

           MOVE SQLCODE           TO IDSO0011-SQLCODE-SIGNED.

           IF NOT IDSO0011-SUCCESSFUL-SQL
              SET IDSO0011-SQL-ERROR    TO TRUE
              STRING 'ERRORE CHIAMATA LDBS1390 ;'
                     IDSO0011-SQLCODE-SIGNED ';'
                     V1391-COD-ACTU
              DELIMITED BY SIZE INTO IDSO0011-DESCRIZ-ERR-DB2
              MOVE 'MATR_VAL_VAR'    TO IDSO0011-NOME-TABELLA
              SET IDSO0011-SQL-ERROR             TO TRUE
           END-IF.
      *
       B200-EX.
           EXIT.
      *****************************************************************
      * FETCH CUR-SERVIZI
      *****************************************************************
       B250-FETCH-FIRST.
      *
            EXEC SQL FETCH CUR_SERVIZI
                INTO  :ADA-TIPO-DATO
                      :IND-ADA-TIPO-DATO
                     ,:ADA-LUNGHEZZA-DATO
                      :IND-ADA-LUNGHEZZA-DATO
                     ,:ADA-PRECISIONE-DATO
                      :IND-ADA-PRECISIONE-DATO
                     ,:ADA-FORMATTAZIONE-DATO
                      :IND-ADA-FORMATTAZIONE-DATO
                     ,:MVV-ID-MATR-VAL-VAR
                     ,:MVV-IDP-MATR-VAL-VAR
                      :IND-MVV-IDP-MATR-VAL-VAR
                     ,:MVV-TIPO-MOVIMENTO
                      :IND-MVV-TIPO-MOVIMENTO
                     ,:MVV-COD-DATO-EXT
                      :IND-MVV-COD-DATO-EXT
                     ,:MVV-OBBLIGATORIETA
                      :IND-MVV-OBBLIGATORIETA
                     ,:MVV-VALORE-DEFAULT
                      :IND-MVV-VALORE-DEFAULT
                     ,:MVV-COD-STR-DATO-PTF
                      :IND-MVV-COD-STR-DATO-PTF
                     ,:MVV-COD-DATO-PTF
                      :IND-MVV-COD-DATO-PTF
                     ,:MVV-COD-PARAMETRO
                      :IND-MVV-COD-PARAMETRO
                     ,:MVV-OPERAZIONE
                      :IND-MVV-OPERAZIONE
                     ,:MVV-LIVELLO-OPERAZIONE
                      :IND-MVV-LIVELLO-OPERAZIONE
                     ,:MVV-TIPO-OGGETTO
                      :IND-MVV-TIPO-OGGETTO
                     ,:MVV-WHERE-CONDITION-VCHAR
                      :IND-MVV-WHERE-CONDITION
                     ,:MVV-SERVIZIO-LETTURA
                      :IND-MVV-SERVIZIO-LETTURA
                     ,:MVV-MODULO-CALCOLO
                      :IND-MVV-MODULO-CALCOLO
                     ,:MVV-COD-DATO-INTERNO
                      :IND-MVV-COD-DATO-INTERNO
           END-EXEC.

           MOVE SQLCODE           TO IDSO0011-SQLCODE-SIGNED.
      *
           IF  NOT IDSO0011-SUCCESSFUL-SQL
      *         NOT IDSO0011-NOT-FOUND
               MOVE 'MATR_VAL_VAR'  TO IDSO0011-NOME-TABELLA
      *
               IF IDSO0011-NOT-FOUND
                  STRING 'VARIABILE '
                    V1391-COD-ACTU ' '
                    'NON ANCORA CENSITA SULLA TABELLA '
                    'DEL VALORIZZATORE : '
                    IDSO0011-NOME-TABELLA
                  DELIMITED BY SIZE INTO IDSO0011-DESCRIZ-ERR-DB2
               ELSE
                  STRING 'ERRORE CHIAMATA LDBS1390 ;'
                     IDSO0011-SQLCODE-SIGNED ';'
                     V1391-COD-ACTU
                  DELIMITED BY SIZE INTO IDSO0011-DESCRIZ-ERR-DB2
               END-IF
      *
               SET IDSO0011-SQL-ERROR             TO TRUE
           END-IF.

           IF IDSO0011-SUCCESSFUL-SQL
              PERFORM N000-CNTL-CAMPI-NULL        THRU N000-EX
              MOVE 1                              TO WK-IND-SERV
              PERFORM N001-IMPOSTA-OUTPUT         THRU N001-EX
              MOVE WK-IND-SERV                    TO V1391-ELE-MAX-ACTU
           ELSE
              PERFORM B350-CLOSE-CUR-SERVIZI      THRU B350-EX
           END-IF.
      *
       B250-EX.
           EXIT.
      *****************************************************************
      * FETCH CUR-SERVIZI
      *****************************************************************
       B300-FETCH-CUR-SERVIZI.
      *
            EXEC SQL FETCH CUR_SERVIZI
                INTO  :ADA-TIPO-DATO
                      :IND-ADA-TIPO-DATO
                     ,:ADA-LUNGHEZZA-DATO
                      :IND-ADA-LUNGHEZZA-DATO
                     ,:ADA-PRECISIONE-DATO
                      :IND-ADA-PRECISIONE-DATO
                     ,:ADA-FORMATTAZIONE-DATO
                      :IND-ADA-FORMATTAZIONE-DATO
                     ,:MVV-ID-MATR-VAL-VAR
                     ,:MVV-IDP-MATR-VAL-VAR
                      :IND-MVV-IDP-MATR-VAL-VAR
                     ,:MVV-TIPO-MOVIMENTO
                      :IND-MVV-TIPO-MOVIMENTO
                     ,:MVV-COD-DATO-EXT
                      :IND-MVV-COD-DATO-EXT
                     ,:MVV-OBBLIGATORIETA
                      :IND-MVV-OBBLIGATORIETA
                     ,:MVV-VALORE-DEFAULT
                      :IND-MVV-VALORE-DEFAULT
                     ,:MVV-COD-STR-DATO-PTF
                      :IND-MVV-COD-STR-DATO-PTF
                     ,:MVV-COD-DATO-PTF
                      :IND-MVV-COD-DATO-PTF
                     ,:MVV-COD-PARAMETRO
                      :IND-MVV-COD-PARAMETRO
                     ,:MVV-OPERAZIONE
                      :IND-MVV-OPERAZIONE
                     ,:MVV-LIVELLO-OPERAZIONE
                      :IND-MVV-LIVELLO-OPERAZIONE
                     ,:MVV-TIPO-OGGETTO
                      :IND-MVV-TIPO-OGGETTO
                     ,:MVV-WHERE-CONDITION-VCHAR
                      :IND-MVV-WHERE-CONDITION
                     ,:MVV-SERVIZIO-LETTURA
                      :IND-MVV-SERVIZIO-LETTURA
                     ,:MVV-MODULO-CALCOLO
                      :IND-MVV-MODULO-CALCOLO
                     ,:MVV-COD-DATO-INTERNO
                      :IND-MVV-COD-DATO-INTERNO
           END-EXEC.

           MOVE SQLCODE           TO IDSO0011-SQLCODE-SIGNED.
      *
           IF  NOT IDSO0011-SUCCESSFUL-SQL
           AND NOT IDSO0011-NOT-FOUND
               MOVE 'MATR_VAL_VAR'     TO IDSO0011-NOME-TABELLA
      *
               STRING 'ERRORE CHIAMATA LDBS1390 ;'
                     IDSO0011-SQLCODE-SIGNED ';'
                     V1391-COD-ACTU
               DELIMITED BY SIZE INTO IDSO0011-DESCRIZ-ERR-DB2
      *
               SET IDSO0011-SQL-ERROR             TO TRUE
           END-IF.
      *
           IF IDSO0011-SUCCESSFUL-SQL
              PERFORM N000-CNTL-CAMPI-NULL        THRU N000-EX
              ADD 1                               TO WK-IND-SERV
              PERFORM N001-IMPOSTA-OUTPUT         THRU N001-EX
              MOVE WK-IND-SERV                    TO V1391-ELE-MAX-ACTU
           END-IF.
      *
       B300-EX.
           EXIT.
      *****************************************************************
      * CLOSE CUR-SERVIZIO
      *****************************************************************
       B350-CLOSE-CUR-SERVIZI.
      *
           MOVE IDSO0011-SQLCODE-SIGNED           TO WS-SQLCODE.
      *
           EXEC SQL CLOSE CUR_SERVIZI END-EXEC.

           MOVE SQLCODE           TO IDSO0011-SQLCODE-SIGNED.
      *
           IF NOT IDSO0011-SUCCESSFUL-SQL
              MOVE 'MOV_STR_SERVIZI'
                                  TO IDSO0011-NOME-TABELLA
              SET IDSO0011-SQL-ERROR             TO TRUE
           ELSE
              MOVE WS-SQLCODE                TO IDSO0011-SQLCODE-SIGNED
           END-IF.
      *
       B350-EX.
           EXIT.
      *****************************************************************
      * CLOSE CUR-SERVIZIO
      *****************************************************************
       B400-CLOSE-CUR-SERVIZI.
      *
           EXEC SQL CLOSE CUR_SERVIZI END-EXEC.

           MOVE SQLCODE           TO IDSO0011-SQLCODE-SIGNED.
      *
           IF NOT IDSO0011-SUCCESSFUL-SQL
              MOVE 'MOV_STR_SERVIZI'
                                  TO IDSO0011-NOME-TABELLA
              SET IDSO0011-SQL-ERROR             TO TRUE
           END-IF.
      *
       B400-EX.
           EXIT.
      ******************************************************************
       N000-CNTL-CAMPI-NULL.
      *
           IF IND-ADA-TIPO-DATO = -1
              MOVE HIGH-VALUES TO ADA-TIPO-DATO-NULL
           END-IF.
           IF IND-ADA-LUNGHEZZA-DATO = -1
              MOVE HIGH-VALUES TO ADA-LUNGHEZZA-DATO-NULL
           END-IF.
           IF IND-ADA-PRECISIONE-DATO = -1
              MOVE HIGH-VALUES TO ADA-PRECISIONE-DATO-NULL
           END-IF.
           IF IND-ADA-FORMATTAZIONE-DATO = -1
              MOVE HIGH-VALUES TO ADA-FORMATTAZIONE-DATO
           END-IF.
           IF IND-MVV-IDP-MATR-VAL-VAR = -1
              MOVE HIGH-VALUES TO MVV-IDP-MATR-VAL-VAR-NULL
           END-IF.
           IF IND-MVV-TIPO-MOVIMENTO = -1
              MOVE HIGH-VALUES TO MVV-TIPO-MOVIMENTO-NULL
           END-IF.
           IF IND-MVV-COD-DATO-EXT = -1
              MOVE HIGH-VALUES TO MVV-COD-DATO-EXT
           END-IF.
           IF IND-MVV-OBBLIGATORIETA = -1
              MOVE HIGH-VALUES TO MVV-OBBLIGATORIETA-NULL
           END-IF.
           IF IND-MVV-VALORE-DEFAULT = -1
              MOVE HIGH-VALUES TO MVV-VALORE-DEFAULT
           END-IF.
           IF IND-MVV-COD-STR-DATO-PTF = -1
              MOVE HIGH-VALUES TO MVV-COD-STR-DATO-PTF
           END-IF.
           IF IND-MVV-COD-DATO-PTF = -1
              MOVE HIGH-VALUES TO MVV-COD-DATO-PTF
           END-IF.
           IF IND-MVV-COD-PARAMETRO = -1
              MOVE HIGH-VALUES TO MVV-COD-PARAMETRO
           END-IF.
           IF IND-MVV-OPERAZIONE = -1
              MOVE HIGH-VALUES TO MVV-OPERAZIONE
           END-IF.
           IF IND-MVV-LIVELLO-OPERAZIONE = -1
              MOVE HIGH-VALUES TO MVV-LIVELLO-OPERAZIONE-NULL
           END-IF.
           IF IND-MVV-TIPO-OGGETTO = -1
              MOVE HIGH-VALUES TO MVV-TIPO-OGGETTO-NULL
           END-IF.
           IF IND-MVV-WHERE-CONDITION = -1
              MOVE HIGH-VALUES TO MVV-WHERE-CONDITION
           END-IF.
           IF IND-MVV-SERVIZIO-LETTURA = -1
              MOVE HIGH-VALUES TO MVV-SERVIZIO-LETTURA-NULL
           END-IF.
           IF IND-MVV-MODULO-CALCOLO = -1
              MOVE HIGH-VALUES TO MVV-MODULO-CALCOLO-NULL
           END-IF.
           IF IND-MVV-COD-DATO-INTERNO = -1
              MOVE HIGH-VALUES TO MVV-COD-DATO-INTERNO
           END-IF.
      *
       N000-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VALORIZZA OUTPUT
      *----------------------------------------------------------------*
       N001-IMPOSTA-OUTPUT.
      *
           IF ADA-TIPO-DATO-NULL = HIGH-VALUES
              MOVE ADA-TIPO-DATO-NULL
                TO V1391-TIPO-DATO-NULL(WK-IND-SERV)
           ELSE
              MOVE ADA-TIPO-DATO
                TO V1391-TIPO-DATO(WK-IND-SERV)
           END-IF.
           IF ADA-LUNGHEZZA-DATO-NULL = HIGH-VALUES
              MOVE ADA-LUNGHEZZA-DATO-NULL
                TO V1391-LUNGHEZZA-DATO-NULL(WK-IND-SERV)
           ELSE
              MOVE ADA-LUNGHEZZA-DATO
                TO V1391-LUNGHEZZA-DATO(WK-IND-SERV)
           END-IF.
           IF ADA-PRECISIONE-DATO-NULL = HIGH-VALUES
              MOVE ADA-PRECISIONE-DATO-NULL
                TO V1391-PRECISIONE-DATO-NULL(WK-IND-SERV)
           ELSE
              MOVE ADA-PRECISIONE-DATO
                TO V1391-PRECISIONE-DATO(WK-IND-SERV)
           END-IF.

           MOVE ADA-FORMATTAZIONE-DATO
             TO V1391-FORMATTAZIONE-DATO(WK-IND-SERV).
      *
            MOVE MVV-ID-MATR-VAL-VAR
             TO V1391-ID-MATR-VAL-VAR(WK-IND-SERV).
           IF MVV-IDP-MATR-VAL-VAR-NULL = HIGH-VALUES
              MOVE MVV-IDP-MATR-VAL-VAR-NULL
                TO V1391-IDP-MATR-VAL-VAR-NULL(WK-IND-SERV)
           ELSE
              MOVE MVV-IDP-MATR-VAL-VAR
                TO V1391-IDP-MATR-VAL-VAR(WK-IND-SERV)
           END-IF.

           IF MVV-TIPO-MOVIMENTO-NULL = HIGH-VALUES
              MOVE MVV-TIPO-MOVIMENTO-NULL
                TO V1391-TIPO-MOVIMENTO-NULL(WK-IND-SERV)
           ELSE
              MOVE MVV-TIPO-MOVIMENTO
                TO V1391-TIPO-MOVIMENTO(WK-IND-SERV)
           END-IF.

           MOVE MVV-COD-DATO-EXT
             TO V1391-COD-DATO-EXT(WK-IND-SERV).

           IF MVV-OBBLIGATORIETA-NULL = HIGH-VALUES
              MOVE MVV-OBBLIGATORIETA-NULL
                TO V1391-OBBLIGATORIETA-NULL(WK-IND-SERV)
           ELSE
              MOVE MVV-OBBLIGATORIETA
                TO V1391-OBBLIGATORIETA(WK-IND-SERV)
           END-IF.

           MOVE MVV-VALORE-DEFAULT
             TO V1391-VALORE-DEFAULT(WK-IND-SERV).
           MOVE MVV-COD-STR-DATO-PTF
             TO V1391-COD-STR-DATO-PTF(WK-IND-SERV).
           MOVE MVV-COD-DATO-PTF
             TO V1391-COD-DATO-PTF(WK-IND-SERV).
           MOVE MVV-COD-PARAMETRO
             TO V1391-COD-PARAMETRO(WK-IND-SERV).
           MOVE MVV-OPERAZIONE
             TO V1391-OPERAZIONE(WK-IND-SERV).

           IF MVV-LIVELLO-OPERAZIONE-NULL = HIGH-VALUES
              MOVE MVV-LIVELLO-OPERAZIONE-NULL
                TO V1391-LIVELLO-OPERAZIONE-NULL(WK-IND-SERV)
           ELSE
              MOVE MVV-LIVELLO-OPERAZIONE
                TO V1391-LIVELLO-OPERAZIONE(WK-IND-SERV)
           END-IF.
           IF MVV-TIPO-OGGETTO-NULL = HIGH-VALUES
              MOVE MVV-TIPO-OGGETTO-NULL
                TO V1391-TIPO-OGGETTO-NULL(WK-IND-SERV)
           ELSE
              MOVE MVV-TIPO-OGGETTO
                TO V1391-TIPO-OGGETTO(WK-IND-SERV)
           END-IF.

           MOVE MVV-WHERE-CONDITION
             TO V1391-WHERE-CONDITION(WK-IND-SERV).

           IF MVV-SERVIZIO-LETTURA-NULL = HIGH-VALUES
              MOVE MVV-SERVIZIO-LETTURA-NULL
                TO V1391-SERVIZIO-LETTURA-NULL(WK-IND-SERV)
           ELSE
              MOVE MVV-SERVIZIO-LETTURA
                TO V1391-SERVIZIO-LETTURA(WK-IND-SERV)
           END-IF.

           IF MVV-MODULO-CALCOLO-NULL = HIGH-VALUES
              MOVE MVV-MODULO-CALCOLO-NULL
                TO V1391-MODULO-CALCOLO-NULL(WK-IND-SERV)
           ELSE
              MOVE MVV-MODULO-CALCOLO
                TO V1391-MODULO-CALCOLO(WK-IND-SERV)
           END-IF.

           MOVE MVV-COD-DATO-INTERNO
             TO V1391-COD-DATO-INTERNO(WK-IND-SERV).
      *
       N001-EX.
           EXIT.
      ******************************************************************
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.
      *
           MOVE LENGTH OF ADA-DESC-DATO
                       TO ADA-DESC-DATO-LEN.
           MOVE LENGTH OF MVV-WHERE-CONDITION
                       TO MVV-WHERE-CONDITION-LEN.
      *
       Z960-EX.
           EXIT.

      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0024.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0024
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0024'.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-DATA-OUTPUT                     PIC 9(04)V9(07).
       01  WK-DATA-OUTPUT-V REDEFINES WK-DATA-OUTPUT.
           03 WK-NUMERICI                     PIC 9(04).
           03 WK-DECIMALI                     PIC 9(07).
      *
       01  WK-VAR-APPO.
           02 WK-DURA-AA                      PIC S9(5) COMP-3.
           02 WK-DURA-MM                      PIC S9(5) COMP-3.
      *
       77 WK-APPO-MESI                        PIC 9(9).
       77 WK-APPO-ANNI                        PIC 9(9).
      *
       01 WK-FIND-LETTO                       PIC X(001).
          88 WK-LETTO-SI                      VALUE 'S'.
          88 WK-LETTO-NO                      VALUE 'N'.

      *---------------------------------------------------------------*
      *  LISTA MOVIMENTI IMPLEMENTATI LCCC0006
      *---------------------------------------------------------------*
           COPY LCCC0006.

      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-TRANCHE.
          03 DTGA-AREA-TRANCHE.
             04 DTGA-ELE-TGA-MAX        PIC S9(04) COMP.
                COPY LCCVTGAB REPLACING   ==(SF)==  BY ==DTGA==.
             COPY LCCVTGA1              REPLACING ==(SF)== BY ==DTGA==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
           03 IX-TAB-TGA                     PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0024.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0024.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT
                                             WK-VAR-APPO
                                             AREA-IO-TRANCHE.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.
      *
      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE

      *--> PERFORM DI CONTROLLI SUI I CAMPI CARICATI NELLE
      *--> DCLGEN DI WORKING
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1200-CONTROLLO-DATI
                  THRU S1200-CONTROLLO-DATI-EX
           END-IF

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
      *--> CALL MODULO PER RECUPERO DATA
               PERFORM S1300-TIPO-TRANCHE
                  THRU S1300-TIPO-TRANCHE-EX

           END-IF.

       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-TRCH-GAR
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DTGA-AREA-TRANCHE
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO DATI DCLGEN
      *----------------------------------------------------------------*
       S1200-CONTROLLO-DATI.

           IF DTGA-DUR-AA(IVVC0213-IX-TABB)  NOT NUMERIC
           AND DTGA-DUR-MM(IVVC0213-IX-TABB) NOT NUMERIC
              SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'DATO-NON NUMERICO'
                TO IDSV0003-DESCRIZ-ERR-DB2
           END-IF.

           IF DTGA-DUR-AA(IVVC0213-IX-TABB)  NUMERIC
           AND DTGA-DUR-MM(IVVC0213-IX-TABB) NUMERIC
             IF DTGA-DUR-AA(IVVC0213-IX-TABB) EQUAL ZEROES
             AND DTGA-DUR-MM(IVVC0213-IX-TABB) EQUAL ZEROES
                SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                MOVE WK-PGM
                  TO IDSV0003-COD-SERVIZIO-BE
                MOVE 'DATO-NON NUMERICO'
                  TO IDSV0003-DESCRIZ-ERR-DB2
             END-IF
           END-IF.

       S1200-CONTROLLO-DATI-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   GESTIONE TIPO TRANCHE DI GARANZIA
      *----------------------------------------------------------------*
       S1300-TIPO-TRANCHE.

      *--> TEST TIPO LIVELLO
           IF IVVC0213-TP-LIVELLO = 'G'
              PERFORM S2027-TRANCHE
                 THRU S2027-EX
           END-IF.

       S1300-TIPO-TRANCHE-EX.
           EXIT.

      *----------------------------------------------------------------*
      *  ELABORA  TRANCHE DI GAR
      *----------------------------------------------------------------*
       S2027-TRANCHE.
      *
           IF DTGA-DUR-AA(IVVC0213-IX-TABB) IS NUMERIC
              MOVE DTGA-DUR-AA(IVVC0213-IX-TABB)  TO WK-DURA-AA
           ELSE
              MOVE ZEROES                         TO WK-DURA-AA
           END-IF.
           IF DTGA-DUR-MM(IVVC0213-IX-TABB) IS NUMERIC
              MOVE DTGA-DUR-MM(IVVC0213-IX-TABB)  TO WK-DURA-MM
           ELSE
              MOVE ZEROES                         TO WK-DURA-MM
           END-IF.
      *
           COMPUTE WK-APPO-MESI = (WK-DURA-MM * 30).
           COMPUTE WK-APPO-ANNI = (WK-DURA-AA * 360).
      *
           COMPUTE IVVC0213-VAL-IMP-O =
                   ((WK-APPO-ANNI + WK-APPO-MESI) / 360).
      *
       S2027-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           MOVE SPACES                     TO IVVC0213-VAL-STR-O.
           MOVE 0                          TO IVVC0213-VAL-PERC-O.
      *
           GOBACK.

       EX-S9000.
           EXIT.

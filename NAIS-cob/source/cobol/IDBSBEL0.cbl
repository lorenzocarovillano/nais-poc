       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSBEL0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  02 LUG 2014.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDBEL0 END-EXEC.
           EXEC SQL INCLUDE IDBVBEL2 END-EXEC.
           EXEC SQL INCLUDE IDBVBEL3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVBEL1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 BNFICR-LIQ.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSBEL0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'BNFICR_LIQ' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_BNFICR_LIQ
                ,ID_LIQ
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,COD_COMP_ANIA
                ,DT_INI_EFF
                ,DT_END_EFF
                ,ID_RAPP_ANA
                ,COD_BNFICR
                ,DESC_BNFICR
                ,PC_LIQ
                ,ESRCN_ATTVT_IMPRS
                ,TP_IND_BNFICR
                ,FL_ESE
                ,FL_IRREV
                ,IMP_LRD_LIQTO
                ,IMPST_IPT
                ,IMP_NET_LIQTO
                ,RIT_ACC_IPT
                ,RIT_VIS_IPT
                ,RIT_TFR_IPT
                ,IMPST_IRPEF_IPT
                ,IMPST_SOST_IPT
                ,IMPST_PRVR_IPT
                ,IMPST_252_IPT
                ,ID_ASSTO
                ,TAX_SEP
                ,DT_RISERVE_SOM_P
                ,DT_VLT
                ,TP_STAT_LIQ_BNFICR
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,RICH_CALC_CNBT_INP
                ,IMP_INTR_RIT_PAG
                ,DT_ULT_DOCTO
                ,DT_DORMIENZA
                ,IMPST_BOLLO_TOT_V
                ,IMPST_VIS_1382011
                ,IMPST_SOST_1382011
                ,IMPST_VIS_662014
                ,IMPST_SOST_662014
             INTO
                :BEL-ID-BNFICR-LIQ
               ,:BEL-ID-LIQ
               ,:BEL-ID-MOVI-CRZ
               ,:BEL-ID-MOVI-CHIU
                :IND-BEL-ID-MOVI-CHIU
               ,:BEL-COD-COMP-ANIA
               ,:BEL-DT-INI-EFF-DB
               ,:BEL-DT-END-EFF-DB
               ,:BEL-ID-RAPP-ANA
                :IND-BEL-ID-RAPP-ANA
               ,:BEL-COD-BNFICR
                :IND-BEL-COD-BNFICR
               ,:BEL-DESC-BNFICR-VCHAR
                :IND-BEL-DESC-BNFICR
               ,:BEL-PC-LIQ
                :IND-BEL-PC-LIQ
               ,:BEL-ESRCN-ATTVT-IMPRS
                :IND-BEL-ESRCN-ATTVT-IMPRS
               ,:BEL-TP-IND-BNFICR
                :IND-BEL-TP-IND-BNFICR
               ,:BEL-FL-ESE
                :IND-BEL-FL-ESE
               ,:BEL-FL-IRREV
                :IND-BEL-FL-IRREV
               ,:BEL-IMP-LRD-LIQTO
                :IND-BEL-IMP-LRD-LIQTO
               ,:BEL-IMPST-IPT
                :IND-BEL-IMPST-IPT
               ,:BEL-IMP-NET-LIQTO
                :IND-BEL-IMP-NET-LIQTO
               ,:BEL-RIT-ACC-IPT
                :IND-BEL-RIT-ACC-IPT
               ,:BEL-RIT-VIS-IPT
                :IND-BEL-RIT-VIS-IPT
               ,:BEL-RIT-TFR-IPT
                :IND-BEL-RIT-TFR-IPT
               ,:BEL-IMPST-IRPEF-IPT
                :IND-BEL-IMPST-IRPEF-IPT
               ,:BEL-IMPST-SOST-IPT
                :IND-BEL-IMPST-SOST-IPT
               ,:BEL-IMPST-PRVR-IPT
                :IND-BEL-IMPST-PRVR-IPT
               ,:BEL-IMPST-252-IPT
                :IND-BEL-IMPST-252-IPT
               ,:BEL-ID-ASSTO
                :IND-BEL-ID-ASSTO
               ,:BEL-TAX-SEP
                :IND-BEL-TAX-SEP
               ,:BEL-DT-RISERVE-SOM-P-DB
                :IND-BEL-DT-RISERVE-SOM-P
               ,:BEL-DT-VLT-DB
                :IND-BEL-DT-VLT
               ,:BEL-TP-STAT-LIQ-BNFICR
                :IND-BEL-TP-STAT-LIQ-BNFICR
               ,:BEL-DS-RIGA
               ,:BEL-DS-OPER-SQL
               ,:BEL-DS-VER
               ,:BEL-DS-TS-INI-CPTZ
               ,:BEL-DS-TS-END-CPTZ
               ,:BEL-DS-UTENTE
               ,:BEL-DS-STATO-ELAB
               ,:BEL-RICH-CALC-CNBT-INP
                :IND-BEL-RICH-CALC-CNBT-INP
               ,:BEL-IMP-INTR-RIT-PAG
                :IND-BEL-IMP-INTR-RIT-PAG
               ,:BEL-DT-ULT-DOCTO-DB
                :IND-BEL-DT-ULT-DOCTO
               ,:BEL-DT-DORMIENZA-DB
                :IND-BEL-DT-DORMIENZA
               ,:BEL-IMPST-BOLLO-TOT-V
                :IND-BEL-IMPST-BOLLO-TOT-V
               ,:BEL-IMPST-VIS-1382011
                :IND-BEL-IMPST-VIS-1382011
               ,:BEL-IMPST-SOST-1382011
                :IND-BEL-IMPST-SOST-1382011
               ,:BEL-IMPST-VIS-662014
                :IND-BEL-IMPST-VIS-662014
               ,:BEL-IMPST-SOST-662014
                :IND-BEL-IMPST-SOST-662014
             FROM BNFICR_LIQ
             WHERE     DS_RIGA = :BEL-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO BNFICR_LIQ
                     (
                        ID_BNFICR_LIQ
                       ,ID_LIQ
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,COD_COMP_ANIA
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,ID_RAPP_ANA
                       ,COD_BNFICR
                       ,DESC_BNFICR
                       ,PC_LIQ
                       ,ESRCN_ATTVT_IMPRS
                       ,TP_IND_BNFICR
                       ,FL_ESE
                       ,FL_IRREV
                       ,IMP_LRD_LIQTO
                       ,IMPST_IPT
                       ,IMP_NET_LIQTO
                       ,RIT_ACC_IPT
                       ,RIT_VIS_IPT
                       ,RIT_TFR_IPT
                       ,IMPST_IRPEF_IPT
                       ,IMPST_SOST_IPT
                       ,IMPST_PRVR_IPT
                       ,IMPST_252_IPT
                       ,ID_ASSTO
                       ,TAX_SEP
                       ,DT_RISERVE_SOM_P
                       ,DT_VLT
                       ,TP_STAT_LIQ_BNFICR
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,RICH_CALC_CNBT_INP
                       ,IMP_INTR_RIT_PAG
                       ,DT_ULT_DOCTO
                       ,DT_DORMIENZA
                       ,IMPST_BOLLO_TOT_V
                       ,IMPST_VIS_1382011
                       ,IMPST_SOST_1382011
                       ,IMPST_VIS_662014
                       ,IMPST_SOST_662014
                     )
                 VALUES
                     (
                       :BEL-ID-BNFICR-LIQ
                       ,:BEL-ID-LIQ
                       ,:BEL-ID-MOVI-CRZ
                       ,:BEL-ID-MOVI-CHIU
                        :IND-BEL-ID-MOVI-CHIU
                       ,:BEL-COD-COMP-ANIA
                       ,:BEL-DT-INI-EFF-DB
                       ,:BEL-DT-END-EFF-DB
                       ,:BEL-ID-RAPP-ANA
                        :IND-BEL-ID-RAPP-ANA
                       ,:BEL-COD-BNFICR
                        :IND-BEL-COD-BNFICR
                       ,:BEL-DESC-BNFICR-VCHAR
                        :IND-BEL-DESC-BNFICR
                       ,:BEL-PC-LIQ
                        :IND-BEL-PC-LIQ
                       ,:BEL-ESRCN-ATTVT-IMPRS
                        :IND-BEL-ESRCN-ATTVT-IMPRS
                       ,:BEL-TP-IND-BNFICR
                        :IND-BEL-TP-IND-BNFICR
                       ,:BEL-FL-ESE
                        :IND-BEL-FL-ESE
                       ,:BEL-FL-IRREV
                        :IND-BEL-FL-IRREV
                       ,:BEL-IMP-LRD-LIQTO
                        :IND-BEL-IMP-LRD-LIQTO
                       ,:BEL-IMPST-IPT
                        :IND-BEL-IMPST-IPT
                       ,:BEL-IMP-NET-LIQTO
                        :IND-BEL-IMP-NET-LIQTO
                       ,:BEL-RIT-ACC-IPT
                        :IND-BEL-RIT-ACC-IPT
                       ,:BEL-RIT-VIS-IPT
                        :IND-BEL-RIT-VIS-IPT
                       ,:BEL-RIT-TFR-IPT
                        :IND-BEL-RIT-TFR-IPT
                       ,:BEL-IMPST-IRPEF-IPT
                        :IND-BEL-IMPST-IRPEF-IPT
                       ,:BEL-IMPST-SOST-IPT
                        :IND-BEL-IMPST-SOST-IPT
                       ,:BEL-IMPST-PRVR-IPT
                        :IND-BEL-IMPST-PRVR-IPT
                       ,:BEL-IMPST-252-IPT
                        :IND-BEL-IMPST-252-IPT
                       ,:BEL-ID-ASSTO
                        :IND-BEL-ID-ASSTO
                       ,:BEL-TAX-SEP
                        :IND-BEL-TAX-SEP
                       ,:BEL-DT-RISERVE-SOM-P-DB
                        :IND-BEL-DT-RISERVE-SOM-P
                       ,:BEL-DT-VLT-DB
                        :IND-BEL-DT-VLT
                       ,:BEL-TP-STAT-LIQ-BNFICR
                        :IND-BEL-TP-STAT-LIQ-BNFICR
                       ,:BEL-DS-RIGA
                       ,:BEL-DS-OPER-SQL
                       ,:BEL-DS-VER
                       ,:BEL-DS-TS-INI-CPTZ
                       ,:BEL-DS-TS-END-CPTZ
                       ,:BEL-DS-UTENTE
                       ,:BEL-DS-STATO-ELAB
                       ,:BEL-RICH-CALC-CNBT-INP
                        :IND-BEL-RICH-CALC-CNBT-INP
                       ,:BEL-IMP-INTR-RIT-PAG
                        :IND-BEL-IMP-INTR-RIT-PAG
                       ,:BEL-DT-ULT-DOCTO-DB
                        :IND-BEL-DT-ULT-DOCTO
                       ,:BEL-DT-DORMIENZA-DB
                        :IND-BEL-DT-DORMIENZA
                       ,:BEL-IMPST-BOLLO-TOT-V
                        :IND-BEL-IMPST-BOLLO-TOT-V
                       ,:BEL-IMPST-VIS-1382011
                        :IND-BEL-IMPST-VIS-1382011
                       ,:BEL-IMPST-SOST-1382011
                        :IND-BEL-IMPST-SOST-1382011
                       ,:BEL-IMPST-VIS-662014
                        :IND-BEL-IMPST-VIS-662014
                       ,:BEL-IMPST-SOST-662014
                        :IND-BEL-IMPST-SOST-662014
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE BNFICR_LIQ SET

                   ID_BNFICR_LIQ          =
                :BEL-ID-BNFICR-LIQ
                  ,ID_LIQ                 =
                :BEL-ID-LIQ
                  ,ID_MOVI_CRZ            =
                :BEL-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :BEL-ID-MOVI-CHIU
                                       :IND-BEL-ID-MOVI-CHIU
                  ,COD_COMP_ANIA          =
                :BEL-COD-COMP-ANIA
                  ,DT_INI_EFF             =
           :BEL-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :BEL-DT-END-EFF-DB
                  ,ID_RAPP_ANA            =
                :BEL-ID-RAPP-ANA
                                       :IND-BEL-ID-RAPP-ANA
                  ,COD_BNFICR             =
                :BEL-COD-BNFICR
                                       :IND-BEL-COD-BNFICR
                  ,DESC_BNFICR            =
                :BEL-DESC-BNFICR-VCHAR
                                       :IND-BEL-DESC-BNFICR
                  ,PC_LIQ                 =
                :BEL-PC-LIQ
                                       :IND-BEL-PC-LIQ
                  ,ESRCN_ATTVT_IMPRS      =
                :BEL-ESRCN-ATTVT-IMPRS
                                       :IND-BEL-ESRCN-ATTVT-IMPRS
                  ,TP_IND_BNFICR          =
                :BEL-TP-IND-BNFICR
                                       :IND-BEL-TP-IND-BNFICR
                  ,FL_ESE                 =
                :BEL-FL-ESE
                                       :IND-BEL-FL-ESE
                  ,FL_IRREV               =
                :BEL-FL-IRREV
                                       :IND-BEL-FL-IRREV
                  ,IMP_LRD_LIQTO          =
                :BEL-IMP-LRD-LIQTO
                                       :IND-BEL-IMP-LRD-LIQTO
                  ,IMPST_IPT              =
                :BEL-IMPST-IPT
                                       :IND-BEL-IMPST-IPT
                  ,IMP_NET_LIQTO          =
                :BEL-IMP-NET-LIQTO
                                       :IND-BEL-IMP-NET-LIQTO
                  ,RIT_ACC_IPT            =
                :BEL-RIT-ACC-IPT
                                       :IND-BEL-RIT-ACC-IPT
                  ,RIT_VIS_IPT            =
                :BEL-RIT-VIS-IPT
                                       :IND-BEL-RIT-VIS-IPT
                  ,RIT_TFR_IPT            =
                :BEL-RIT-TFR-IPT
                                       :IND-BEL-RIT-TFR-IPT
                  ,IMPST_IRPEF_IPT        =
                :BEL-IMPST-IRPEF-IPT
                                       :IND-BEL-IMPST-IRPEF-IPT
                  ,IMPST_SOST_IPT         =
                :BEL-IMPST-SOST-IPT
                                       :IND-BEL-IMPST-SOST-IPT
                  ,IMPST_PRVR_IPT         =
                :BEL-IMPST-PRVR-IPT
                                       :IND-BEL-IMPST-PRVR-IPT
                  ,IMPST_252_IPT          =
                :BEL-IMPST-252-IPT
                                       :IND-BEL-IMPST-252-IPT
                  ,ID_ASSTO               =
                :BEL-ID-ASSTO
                                       :IND-BEL-ID-ASSTO
                  ,TAX_SEP                =
                :BEL-TAX-SEP
                                       :IND-BEL-TAX-SEP
                  ,DT_RISERVE_SOM_P       =
           :BEL-DT-RISERVE-SOM-P-DB
                                       :IND-BEL-DT-RISERVE-SOM-P
                  ,DT_VLT                 =
           :BEL-DT-VLT-DB
                                       :IND-BEL-DT-VLT
                  ,TP_STAT_LIQ_BNFICR     =
                :BEL-TP-STAT-LIQ-BNFICR
                                       :IND-BEL-TP-STAT-LIQ-BNFICR
                  ,DS_RIGA                =
                :BEL-DS-RIGA
                  ,DS_OPER_SQL            =
                :BEL-DS-OPER-SQL
                  ,DS_VER                 =
                :BEL-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :BEL-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :BEL-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :BEL-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :BEL-DS-STATO-ELAB
                  ,RICH_CALC_CNBT_INP     =
                :BEL-RICH-CALC-CNBT-INP
                                       :IND-BEL-RICH-CALC-CNBT-INP
                  ,IMP_INTR_RIT_PAG       =
                :BEL-IMP-INTR-RIT-PAG
                                       :IND-BEL-IMP-INTR-RIT-PAG
                  ,DT_ULT_DOCTO           =
           :BEL-DT-ULT-DOCTO-DB
                                       :IND-BEL-DT-ULT-DOCTO
                  ,DT_DORMIENZA           =
           :BEL-DT-DORMIENZA-DB
                                       :IND-BEL-DT-DORMIENZA
                  ,IMPST_BOLLO_TOT_V      =
                :BEL-IMPST-BOLLO-TOT-V
                                       :IND-BEL-IMPST-BOLLO-TOT-V
                  ,IMPST_VIS_1382011      =
                :BEL-IMPST-VIS-1382011
                                       :IND-BEL-IMPST-VIS-1382011
                  ,IMPST_SOST_1382011     =
                :BEL-IMPST-SOST-1382011
                                       :IND-BEL-IMPST-SOST-1382011
                  ,IMPST_VIS_662014       =
                :BEL-IMPST-VIS-662014
                                       :IND-BEL-IMPST-VIS-662014
                  ,IMPST_SOST_662014      =
                :BEL-IMPST-SOST-662014
                                       :IND-BEL-IMPST-SOST-662014
                WHERE     DS_RIGA = :BEL-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM BNFICR_LIQ
                WHERE     DS_RIGA = :BEL-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-BEL CURSOR FOR
              SELECT
                     ID_BNFICR_LIQ
                    ,ID_LIQ
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,COD_COMP_ANIA
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,ID_RAPP_ANA
                    ,COD_BNFICR
                    ,DESC_BNFICR
                    ,PC_LIQ
                    ,ESRCN_ATTVT_IMPRS
                    ,TP_IND_BNFICR
                    ,FL_ESE
                    ,FL_IRREV
                    ,IMP_LRD_LIQTO
                    ,IMPST_IPT
                    ,IMP_NET_LIQTO
                    ,RIT_ACC_IPT
                    ,RIT_VIS_IPT
                    ,RIT_TFR_IPT
                    ,IMPST_IRPEF_IPT
                    ,IMPST_SOST_IPT
                    ,IMPST_PRVR_IPT
                    ,IMPST_252_IPT
                    ,ID_ASSTO
                    ,TAX_SEP
                    ,DT_RISERVE_SOM_P
                    ,DT_VLT
                    ,TP_STAT_LIQ_BNFICR
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,RICH_CALC_CNBT_INP
                    ,IMP_INTR_RIT_PAG
                    ,DT_ULT_DOCTO
                    ,DT_DORMIENZA
                    ,IMPST_BOLLO_TOT_V
                    ,IMPST_VIS_1382011
                    ,IMPST_SOST_1382011
                    ,IMPST_VIS_662014
                    ,IMPST_SOST_662014
              FROM BNFICR_LIQ
              WHERE     ID_BNFICR_LIQ = :BEL-ID-BNFICR-LIQ
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY DT_INI_EFF ASC

           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_BNFICR_LIQ
                ,ID_LIQ
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,COD_COMP_ANIA
                ,DT_INI_EFF
                ,DT_END_EFF
                ,ID_RAPP_ANA
                ,COD_BNFICR
                ,DESC_BNFICR
                ,PC_LIQ
                ,ESRCN_ATTVT_IMPRS
                ,TP_IND_BNFICR
                ,FL_ESE
                ,FL_IRREV
                ,IMP_LRD_LIQTO
                ,IMPST_IPT
                ,IMP_NET_LIQTO
                ,RIT_ACC_IPT
                ,RIT_VIS_IPT
                ,RIT_TFR_IPT
                ,IMPST_IRPEF_IPT
                ,IMPST_SOST_IPT
                ,IMPST_PRVR_IPT
                ,IMPST_252_IPT
                ,ID_ASSTO
                ,TAX_SEP
                ,DT_RISERVE_SOM_P
                ,DT_VLT
                ,TP_STAT_LIQ_BNFICR
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,RICH_CALC_CNBT_INP
                ,IMP_INTR_RIT_PAG
                ,DT_ULT_DOCTO
                ,DT_DORMIENZA
                ,IMPST_BOLLO_TOT_V
                ,IMPST_VIS_1382011
                ,IMPST_SOST_1382011
                ,IMPST_VIS_662014
                ,IMPST_SOST_662014
             INTO
                :BEL-ID-BNFICR-LIQ
               ,:BEL-ID-LIQ
               ,:BEL-ID-MOVI-CRZ
               ,:BEL-ID-MOVI-CHIU
                :IND-BEL-ID-MOVI-CHIU
               ,:BEL-COD-COMP-ANIA
               ,:BEL-DT-INI-EFF-DB
               ,:BEL-DT-END-EFF-DB
               ,:BEL-ID-RAPP-ANA
                :IND-BEL-ID-RAPP-ANA
               ,:BEL-COD-BNFICR
                :IND-BEL-COD-BNFICR
               ,:BEL-DESC-BNFICR-VCHAR
                :IND-BEL-DESC-BNFICR
               ,:BEL-PC-LIQ
                :IND-BEL-PC-LIQ
               ,:BEL-ESRCN-ATTVT-IMPRS
                :IND-BEL-ESRCN-ATTVT-IMPRS
               ,:BEL-TP-IND-BNFICR
                :IND-BEL-TP-IND-BNFICR
               ,:BEL-FL-ESE
                :IND-BEL-FL-ESE
               ,:BEL-FL-IRREV
                :IND-BEL-FL-IRREV
               ,:BEL-IMP-LRD-LIQTO
                :IND-BEL-IMP-LRD-LIQTO
               ,:BEL-IMPST-IPT
                :IND-BEL-IMPST-IPT
               ,:BEL-IMP-NET-LIQTO
                :IND-BEL-IMP-NET-LIQTO
               ,:BEL-RIT-ACC-IPT
                :IND-BEL-RIT-ACC-IPT
               ,:BEL-RIT-VIS-IPT
                :IND-BEL-RIT-VIS-IPT
               ,:BEL-RIT-TFR-IPT
                :IND-BEL-RIT-TFR-IPT
               ,:BEL-IMPST-IRPEF-IPT
                :IND-BEL-IMPST-IRPEF-IPT
               ,:BEL-IMPST-SOST-IPT
                :IND-BEL-IMPST-SOST-IPT
               ,:BEL-IMPST-PRVR-IPT
                :IND-BEL-IMPST-PRVR-IPT
               ,:BEL-IMPST-252-IPT
                :IND-BEL-IMPST-252-IPT
               ,:BEL-ID-ASSTO
                :IND-BEL-ID-ASSTO
               ,:BEL-TAX-SEP
                :IND-BEL-TAX-SEP
               ,:BEL-DT-RISERVE-SOM-P-DB
                :IND-BEL-DT-RISERVE-SOM-P
               ,:BEL-DT-VLT-DB
                :IND-BEL-DT-VLT
               ,:BEL-TP-STAT-LIQ-BNFICR
                :IND-BEL-TP-STAT-LIQ-BNFICR
               ,:BEL-DS-RIGA
               ,:BEL-DS-OPER-SQL
               ,:BEL-DS-VER
               ,:BEL-DS-TS-INI-CPTZ
               ,:BEL-DS-TS-END-CPTZ
               ,:BEL-DS-UTENTE
               ,:BEL-DS-STATO-ELAB
               ,:BEL-RICH-CALC-CNBT-INP
                :IND-BEL-RICH-CALC-CNBT-INP
               ,:BEL-IMP-INTR-RIT-PAG
                :IND-BEL-IMP-INTR-RIT-PAG
               ,:BEL-DT-ULT-DOCTO-DB
                :IND-BEL-DT-ULT-DOCTO
               ,:BEL-DT-DORMIENZA-DB
                :IND-BEL-DT-DORMIENZA
               ,:BEL-IMPST-BOLLO-TOT-V
                :IND-BEL-IMPST-BOLLO-TOT-V
               ,:BEL-IMPST-VIS-1382011
                :IND-BEL-IMPST-VIS-1382011
               ,:BEL-IMPST-SOST-1382011
                :IND-BEL-IMPST-SOST-1382011
               ,:BEL-IMPST-VIS-662014
                :IND-BEL-IMPST-VIS-662014
               ,:BEL-IMPST-SOST-662014
                :IND-BEL-IMPST-SOST-662014
             FROM BNFICR_LIQ
             WHERE     ID_BNFICR_LIQ = :BEL-ID-BNFICR-LIQ
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE BNFICR_LIQ SET

                   ID_BNFICR_LIQ          =
                :BEL-ID-BNFICR-LIQ
                  ,ID_LIQ                 =
                :BEL-ID-LIQ
                  ,ID_MOVI_CRZ            =
                :BEL-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :BEL-ID-MOVI-CHIU
                                       :IND-BEL-ID-MOVI-CHIU
                  ,COD_COMP_ANIA          =
                :BEL-COD-COMP-ANIA
                  ,DT_INI_EFF             =
           :BEL-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :BEL-DT-END-EFF-DB
                  ,ID_RAPP_ANA            =
                :BEL-ID-RAPP-ANA
                                       :IND-BEL-ID-RAPP-ANA
                  ,COD_BNFICR             =
                :BEL-COD-BNFICR
                                       :IND-BEL-COD-BNFICR
                  ,DESC_BNFICR            =
                :BEL-DESC-BNFICR-VCHAR
                                       :IND-BEL-DESC-BNFICR
                  ,PC_LIQ                 =
                :BEL-PC-LIQ
                                       :IND-BEL-PC-LIQ
                  ,ESRCN_ATTVT_IMPRS      =
                :BEL-ESRCN-ATTVT-IMPRS
                                       :IND-BEL-ESRCN-ATTVT-IMPRS
                  ,TP_IND_BNFICR          =
                :BEL-TP-IND-BNFICR
                                       :IND-BEL-TP-IND-BNFICR
                  ,FL_ESE                 =
                :BEL-FL-ESE
                                       :IND-BEL-FL-ESE
                  ,FL_IRREV               =
                :BEL-FL-IRREV
                                       :IND-BEL-FL-IRREV
                  ,IMP_LRD_LIQTO          =
                :BEL-IMP-LRD-LIQTO
                                       :IND-BEL-IMP-LRD-LIQTO
                  ,IMPST_IPT              =
                :BEL-IMPST-IPT
                                       :IND-BEL-IMPST-IPT
                  ,IMP_NET_LIQTO          =
                :BEL-IMP-NET-LIQTO
                                       :IND-BEL-IMP-NET-LIQTO
                  ,RIT_ACC_IPT            =
                :BEL-RIT-ACC-IPT
                                       :IND-BEL-RIT-ACC-IPT
                  ,RIT_VIS_IPT            =
                :BEL-RIT-VIS-IPT
                                       :IND-BEL-RIT-VIS-IPT
                  ,RIT_TFR_IPT            =
                :BEL-RIT-TFR-IPT
                                       :IND-BEL-RIT-TFR-IPT
                  ,IMPST_IRPEF_IPT        =
                :BEL-IMPST-IRPEF-IPT
                                       :IND-BEL-IMPST-IRPEF-IPT
                  ,IMPST_SOST_IPT         =
                :BEL-IMPST-SOST-IPT
                                       :IND-BEL-IMPST-SOST-IPT
                  ,IMPST_PRVR_IPT         =
                :BEL-IMPST-PRVR-IPT
                                       :IND-BEL-IMPST-PRVR-IPT
                  ,IMPST_252_IPT          =
                :BEL-IMPST-252-IPT
                                       :IND-BEL-IMPST-252-IPT
                  ,ID_ASSTO               =
                :BEL-ID-ASSTO
                                       :IND-BEL-ID-ASSTO
                  ,TAX_SEP                =
                :BEL-TAX-SEP
                                       :IND-BEL-TAX-SEP
                  ,DT_RISERVE_SOM_P       =
           :BEL-DT-RISERVE-SOM-P-DB
                                       :IND-BEL-DT-RISERVE-SOM-P
                  ,DT_VLT                 =
           :BEL-DT-VLT-DB
                                       :IND-BEL-DT-VLT
                  ,TP_STAT_LIQ_BNFICR     =
                :BEL-TP-STAT-LIQ-BNFICR
                                       :IND-BEL-TP-STAT-LIQ-BNFICR
                  ,DS_RIGA                =
                :BEL-DS-RIGA
                  ,DS_OPER_SQL            =
                :BEL-DS-OPER-SQL
                  ,DS_VER                 =
                :BEL-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :BEL-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :BEL-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :BEL-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :BEL-DS-STATO-ELAB
                  ,RICH_CALC_CNBT_INP     =
                :BEL-RICH-CALC-CNBT-INP
                                       :IND-BEL-RICH-CALC-CNBT-INP
                  ,IMP_INTR_RIT_PAG       =
                :BEL-IMP-INTR-RIT-PAG
                                       :IND-BEL-IMP-INTR-RIT-PAG
                  ,DT_ULT_DOCTO           =
           :BEL-DT-ULT-DOCTO-DB
                                       :IND-BEL-DT-ULT-DOCTO
                  ,DT_DORMIENZA           =
           :BEL-DT-DORMIENZA-DB
                                       :IND-BEL-DT-DORMIENZA
                  ,IMPST_BOLLO_TOT_V      =
                :BEL-IMPST-BOLLO-TOT-V
                                       :IND-BEL-IMPST-BOLLO-TOT-V
                  ,IMPST_VIS_1382011      =
                :BEL-IMPST-VIS-1382011
                                       :IND-BEL-IMPST-VIS-1382011
                  ,IMPST_SOST_1382011     =
                :BEL-IMPST-SOST-1382011
                                       :IND-BEL-IMPST-SOST-1382011
                  ,IMPST_VIS_662014       =
                :BEL-IMPST-VIS-662014
                                       :IND-BEL-IMPST-VIS-662014
                  ,IMPST_SOST_662014      =
                :BEL-IMPST-SOST-662014
                                       :IND-BEL-IMPST-SOST-662014
                WHERE     DS_RIGA = :BEL-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-BEL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-BEL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-BEL
           INTO
                :BEL-ID-BNFICR-LIQ
               ,:BEL-ID-LIQ
               ,:BEL-ID-MOVI-CRZ
               ,:BEL-ID-MOVI-CHIU
                :IND-BEL-ID-MOVI-CHIU
               ,:BEL-COD-COMP-ANIA
               ,:BEL-DT-INI-EFF-DB
               ,:BEL-DT-END-EFF-DB
               ,:BEL-ID-RAPP-ANA
                :IND-BEL-ID-RAPP-ANA
               ,:BEL-COD-BNFICR
                :IND-BEL-COD-BNFICR
               ,:BEL-DESC-BNFICR-VCHAR
                :IND-BEL-DESC-BNFICR
               ,:BEL-PC-LIQ
                :IND-BEL-PC-LIQ
               ,:BEL-ESRCN-ATTVT-IMPRS
                :IND-BEL-ESRCN-ATTVT-IMPRS
               ,:BEL-TP-IND-BNFICR
                :IND-BEL-TP-IND-BNFICR
               ,:BEL-FL-ESE
                :IND-BEL-FL-ESE
               ,:BEL-FL-IRREV
                :IND-BEL-FL-IRREV
               ,:BEL-IMP-LRD-LIQTO
                :IND-BEL-IMP-LRD-LIQTO
               ,:BEL-IMPST-IPT
                :IND-BEL-IMPST-IPT
               ,:BEL-IMP-NET-LIQTO
                :IND-BEL-IMP-NET-LIQTO
               ,:BEL-RIT-ACC-IPT
                :IND-BEL-RIT-ACC-IPT
               ,:BEL-RIT-VIS-IPT
                :IND-BEL-RIT-VIS-IPT
               ,:BEL-RIT-TFR-IPT
                :IND-BEL-RIT-TFR-IPT
               ,:BEL-IMPST-IRPEF-IPT
                :IND-BEL-IMPST-IRPEF-IPT
               ,:BEL-IMPST-SOST-IPT
                :IND-BEL-IMPST-SOST-IPT
               ,:BEL-IMPST-PRVR-IPT
                :IND-BEL-IMPST-PRVR-IPT
               ,:BEL-IMPST-252-IPT
                :IND-BEL-IMPST-252-IPT
               ,:BEL-ID-ASSTO
                :IND-BEL-ID-ASSTO
               ,:BEL-TAX-SEP
                :IND-BEL-TAX-SEP
               ,:BEL-DT-RISERVE-SOM-P-DB
                :IND-BEL-DT-RISERVE-SOM-P
               ,:BEL-DT-VLT-DB
                :IND-BEL-DT-VLT
               ,:BEL-TP-STAT-LIQ-BNFICR
                :IND-BEL-TP-STAT-LIQ-BNFICR
               ,:BEL-DS-RIGA
               ,:BEL-DS-OPER-SQL
               ,:BEL-DS-VER
               ,:BEL-DS-TS-INI-CPTZ
               ,:BEL-DS-TS-END-CPTZ
               ,:BEL-DS-UTENTE
               ,:BEL-DS-STATO-ELAB
               ,:BEL-RICH-CALC-CNBT-INP
                :IND-BEL-RICH-CALC-CNBT-INP
               ,:BEL-IMP-INTR-RIT-PAG
                :IND-BEL-IMP-INTR-RIT-PAG
               ,:BEL-DT-ULT-DOCTO-DB
                :IND-BEL-DT-ULT-DOCTO
               ,:BEL-DT-DORMIENZA-DB
                :IND-BEL-DT-DORMIENZA
               ,:BEL-IMPST-BOLLO-TOT-V
                :IND-BEL-IMPST-BOLLO-TOT-V
               ,:BEL-IMPST-VIS-1382011
                :IND-BEL-IMPST-VIS-1382011
               ,:BEL-IMPST-SOST-1382011
                :IND-BEL-IMPST-SOST-1382011
               ,:BEL-IMPST-VIS-662014
                :IND-BEL-IMPST-VIS-662014
               ,:BEL-IMPST-SOST-662014
                :IND-BEL-IMPST-SOST-662014
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-EFF-BEL CURSOR FOR
              SELECT
                     ID_BNFICR_LIQ
                    ,ID_LIQ
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,COD_COMP_ANIA
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,ID_RAPP_ANA
                    ,COD_BNFICR
                    ,DESC_BNFICR
                    ,PC_LIQ
                    ,ESRCN_ATTVT_IMPRS
                    ,TP_IND_BNFICR
                    ,FL_ESE
                    ,FL_IRREV
                    ,IMP_LRD_LIQTO
                    ,IMPST_IPT
                    ,IMP_NET_LIQTO
                    ,RIT_ACC_IPT
                    ,RIT_VIS_IPT
                    ,RIT_TFR_IPT
                    ,IMPST_IRPEF_IPT
                    ,IMPST_SOST_IPT
                    ,IMPST_PRVR_IPT
                    ,IMPST_252_IPT
                    ,ID_ASSTO
                    ,TAX_SEP
                    ,DT_RISERVE_SOM_P
                    ,DT_VLT
                    ,TP_STAT_LIQ_BNFICR
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,RICH_CALC_CNBT_INP
                    ,IMP_INTR_RIT_PAG
                    ,DT_ULT_DOCTO
                    ,DT_DORMIENZA
                    ,IMPST_BOLLO_TOT_V
                    ,IMPST_VIS_1382011
                    ,IMPST_SOST_1382011
                    ,IMPST_VIS_662014
                    ,IMPST_SOST_662014
              FROM BNFICR_LIQ
              WHERE     ID_LIQ = :BEL-ID-LIQ
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_BNFICR_LIQ ASC

           END-EXEC.

       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_BNFICR_LIQ
                ,ID_LIQ
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,COD_COMP_ANIA
                ,DT_INI_EFF
                ,DT_END_EFF
                ,ID_RAPP_ANA
                ,COD_BNFICR
                ,DESC_BNFICR
                ,PC_LIQ
                ,ESRCN_ATTVT_IMPRS
                ,TP_IND_BNFICR
                ,FL_ESE
                ,FL_IRREV
                ,IMP_LRD_LIQTO
                ,IMPST_IPT
                ,IMP_NET_LIQTO
                ,RIT_ACC_IPT
                ,RIT_VIS_IPT
                ,RIT_TFR_IPT
                ,IMPST_IRPEF_IPT
                ,IMPST_SOST_IPT
                ,IMPST_PRVR_IPT
                ,IMPST_252_IPT
                ,ID_ASSTO
                ,TAX_SEP
                ,DT_RISERVE_SOM_P
                ,DT_VLT
                ,TP_STAT_LIQ_BNFICR
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,RICH_CALC_CNBT_INP
                ,IMP_INTR_RIT_PAG
                ,DT_ULT_DOCTO
                ,DT_DORMIENZA
                ,IMPST_BOLLO_TOT_V
                ,IMPST_VIS_1382011
                ,IMPST_SOST_1382011
                ,IMPST_VIS_662014
                ,IMPST_SOST_662014
             INTO
                :BEL-ID-BNFICR-LIQ
               ,:BEL-ID-LIQ
               ,:BEL-ID-MOVI-CRZ
               ,:BEL-ID-MOVI-CHIU
                :IND-BEL-ID-MOVI-CHIU
               ,:BEL-COD-COMP-ANIA
               ,:BEL-DT-INI-EFF-DB
               ,:BEL-DT-END-EFF-DB
               ,:BEL-ID-RAPP-ANA
                :IND-BEL-ID-RAPP-ANA
               ,:BEL-COD-BNFICR
                :IND-BEL-COD-BNFICR
               ,:BEL-DESC-BNFICR-VCHAR
                :IND-BEL-DESC-BNFICR
               ,:BEL-PC-LIQ
                :IND-BEL-PC-LIQ
               ,:BEL-ESRCN-ATTVT-IMPRS
                :IND-BEL-ESRCN-ATTVT-IMPRS
               ,:BEL-TP-IND-BNFICR
                :IND-BEL-TP-IND-BNFICR
               ,:BEL-FL-ESE
                :IND-BEL-FL-ESE
               ,:BEL-FL-IRREV
                :IND-BEL-FL-IRREV
               ,:BEL-IMP-LRD-LIQTO
                :IND-BEL-IMP-LRD-LIQTO
               ,:BEL-IMPST-IPT
                :IND-BEL-IMPST-IPT
               ,:BEL-IMP-NET-LIQTO
                :IND-BEL-IMP-NET-LIQTO
               ,:BEL-RIT-ACC-IPT
                :IND-BEL-RIT-ACC-IPT
               ,:BEL-RIT-VIS-IPT
                :IND-BEL-RIT-VIS-IPT
               ,:BEL-RIT-TFR-IPT
                :IND-BEL-RIT-TFR-IPT
               ,:BEL-IMPST-IRPEF-IPT
                :IND-BEL-IMPST-IRPEF-IPT
               ,:BEL-IMPST-SOST-IPT
                :IND-BEL-IMPST-SOST-IPT
               ,:BEL-IMPST-PRVR-IPT
                :IND-BEL-IMPST-PRVR-IPT
               ,:BEL-IMPST-252-IPT
                :IND-BEL-IMPST-252-IPT
               ,:BEL-ID-ASSTO
                :IND-BEL-ID-ASSTO
               ,:BEL-TAX-SEP
                :IND-BEL-TAX-SEP
               ,:BEL-DT-RISERVE-SOM-P-DB
                :IND-BEL-DT-RISERVE-SOM-P
               ,:BEL-DT-VLT-DB
                :IND-BEL-DT-VLT
               ,:BEL-TP-STAT-LIQ-BNFICR
                :IND-BEL-TP-STAT-LIQ-BNFICR
               ,:BEL-DS-RIGA
               ,:BEL-DS-OPER-SQL
               ,:BEL-DS-VER
               ,:BEL-DS-TS-INI-CPTZ
               ,:BEL-DS-TS-END-CPTZ
               ,:BEL-DS-UTENTE
               ,:BEL-DS-STATO-ELAB
               ,:BEL-RICH-CALC-CNBT-INP
                :IND-BEL-RICH-CALC-CNBT-INP
               ,:BEL-IMP-INTR-RIT-PAG
                :IND-BEL-IMP-INTR-RIT-PAG
               ,:BEL-DT-ULT-DOCTO-DB
                :IND-BEL-DT-ULT-DOCTO
               ,:BEL-DT-DORMIENZA-DB
                :IND-BEL-DT-DORMIENZA
               ,:BEL-IMPST-BOLLO-TOT-V
                :IND-BEL-IMPST-BOLLO-TOT-V
               ,:BEL-IMPST-VIS-1382011
                :IND-BEL-IMPST-VIS-1382011
               ,:BEL-IMPST-SOST-1382011
                :IND-BEL-IMPST-SOST-1382011
               ,:BEL-IMPST-VIS-662014
                :IND-BEL-IMPST-VIS-662014
               ,:BEL-IMPST-SOST-662014
                :IND-BEL-IMPST-SOST-662014
             FROM BNFICR_LIQ
             WHERE     ID_LIQ = :BEL-ID-LIQ
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           EXEC SQL
                OPEN C-IDP-EFF-BEL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           EXEC SQL
                CLOSE C-IDP-EFF-BEL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           EXEC SQL
                FETCH C-IDP-EFF-BEL
           INTO
                :BEL-ID-BNFICR-LIQ
               ,:BEL-ID-LIQ
               ,:BEL-ID-MOVI-CRZ
               ,:BEL-ID-MOVI-CHIU
                :IND-BEL-ID-MOVI-CHIU
               ,:BEL-COD-COMP-ANIA
               ,:BEL-DT-INI-EFF-DB
               ,:BEL-DT-END-EFF-DB
               ,:BEL-ID-RAPP-ANA
                :IND-BEL-ID-RAPP-ANA
               ,:BEL-COD-BNFICR
                :IND-BEL-COD-BNFICR
               ,:BEL-DESC-BNFICR-VCHAR
                :IND-BEL-DESC-BNFICR
               ,:BEL-PC-LIQ
                :IND-BEL-PC-LIQ
               ,:BEL-ESRCN-ATTVT-IMPRS
                :IND-BEL-ESRCN-ATTVT-IMPRS
               ,:BEL-TP-IND-BNFICR
                :IND-BEL-TP-IND-BNFICR
               ,:BEL-FL-ESE
                :IND-BEL-FL-ESE
               ,:BEL-FL-IRREV
                :IND-BEL-FL-IRREV
               ,:BEL-IMP-LRD-LIQTO
                :IND-BEL-IMP-LRD-LIQTO
               ,:BEL-IMPST-IPT
                :IND-BEL-IMPST-IPT
               ,:BEL-IMP-NET-LIQTO
                :IND-BEL-IMP-NET-LIQTO
               ,:BEL-RIT-ACC-IPT
                :IND-BEL-RIT-ACC-IPT
               ,:BEL-RIT-VIS-IPT
                :IND-BEL-RIT-VIS-IPT
               ,:BEL-RIT-TFR-IPT
                :IND-BEL-RIT-TFR-IPT
               ,:BEL-IMPST-IRPEF-IPT
                :IND-BEL-IMPST-IRPEF-IPT
               ,:BEL-IMPST-SOST-IPT
                :IND-BEL-IMPST-SOST-IPT
               ,:BEL-IMPST-PRVR-IPT
                :IND-BEL-IMPST-PRVR-IPT
               ,:BEL-IMPST-252-IPT
                :IND-BEL-IMPST-252-IPT
               ,:BEL-ID-ASSTO
                :IND-BEL-ID-ASSTO
               ,:BEL-TAX-SEP
                :IND-BEL-TAX-SEP
               ,:BEL-DT-RISERVE-SOM-P-DB
                :IND-BEL-DT-RISERVE-SOM-P
               ,:BEL-DT-VLT-DB
                :IND-BEL-DT-VLT
               ,:BEL-TP-STAT-LIQ-BNFICR
                :IND-BEL-TP-STAT-LIQ-BNFICR
               ,:BEL-DS-RIGA
               ,:BEL-DS-OPER-SQL
               ,:BEL-DS-VER
               ,:BEL-DS-TS-INI-CPTZ
               ,:BEL-DS-TS-END-CPTZ
               ,:BEL-DS-UTENTE
               ,:BEL-DS-STATO-ELAB
               ,:BEL-RICH-CALC-CNBT-INP
                :IND-BEL-RICH-CALC-CNBT-INP
               ,:BEL-IMP-INTR-RIT-PAG
                :IND-BEL-IMP-INTR-RIT-PAG
               ,:BEL-DT-ULT-DOCTO-DB
                :IND-BEL-DT-ULT-DOCTO
               ,:BEL-DT-DORMIENZA-DB
                :IND-BEL-DT-DORMIENZA
               ,:BEL-IMPST-BOLLO-TOT-V
                :IND-BEL-IMPST-BOLLO-TOT-V
               ,:BEL-IMPST-VIS-1382011
                :IND-BEL-IMPST-VIS-1382011
               ,:BEL-IMPST-SOST-1382011
                :IND-BEL-IMPST-SOST-1382011
               ,:BEL-IMPST-VIS-662014
                :IND-BEL-IMPST-VIS-662014
               ,:BEL-IMPST-SOST-662014
                :IND-BEL-IMPST-SOST-662014
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_BNFICR_LIQ
                ,ID_LIQ
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,COD_COMP_ANIA
                ,DT_INI_EFF
                ,DT_END_EFF
                ,ID_RAPP_ANA
                ,COD_BNFICR
                ,DESC_BNFICR
                ,PC_LIQ
                ,ESRCN_ATTVT_IMPRS
                ,TP_IND_BNFICR
                ,FL_ESE
                ,FL_IRREV
                ,IMP_LRD_LIQTO
                ,IMPST_IPT
                ,IMP_NET_LIQTO
                ,RIT_ACC_IPT
                ,RIT_VIS_IPT
                ,RIT_TFR_IPT
                ,IMPST_IRPEF_IPT
                ,IMPST_SOST_IPT
                ,IMPST_PRVR_IPT
                ,IMPST_252_IPT
                ,ID_ASSTO
                ,TAX_SEP
                ,DT_RISERVE_SOM_P
                ,DT_VLT
                ,TP_STAT_LIQ_BNFICR
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,RICH_CALC_CNBT_INP
                ,IMP_INTR_RIT_PAG
                ,DT_ULT_DOCTO
                ,DT_DORMIENZA
                ,IMPST_BOLLO_TOT_V
                ,IMPST_VIS_1382011
                ,IMPST_SOST_1382011
                ,IMPST_VIS_662014
                ,IMPST_SOST_662014
             INTO
                :BEL-ID-BNFICR-LIQ
               ,:BEL-ID-LIQ
               ,:BEL-ID-MOVI-CRZ
               ,:BEL-ID-MOVI-CHIU
                :IND-BEL-ID-MOVI-CHIU
               ,:BEL-COD-COMP-ANIA
               ,:BEL-DT-INI-EFF-DB
               ,:BEL-DT-END-EFF-DB
               ,:BEL-ID-RAPP-ANA
                :IND-BEL-ID-RAPP-ANA
               ,:BEL-COD-BNFICR
                :IND-BEL-COD-BNFICR
               ,:BEL-DESC-BNFICR-VCHAR
                :IND-BEL-DESC-BNFICR
               ,:BEL-PC-LIQ
                :IND-BEL-PC-LIQ
               ,:BEL-ESRCN-ATTVT-IMPRS
                :IND-BEL-ESRCN-ATTVT-IMPRS
               ,:BEL-TP-IND-BNFICR
                :IND-BEL-TP-IND-BNFICR
               ,:BEL-FL-ESE
                :IND-BEL-FL-ESE
               ,:BEL-FL-IRREV
                :IND-BEL-FL-IRREV
               ,:BEL-IMP-LRD-LIQTO
                :IND-BEL-IMP-LRD-LIQTO
               ,:BEL-IMPST-IPT
                :IND-BEL-IMPST-IPT
               ,:BEL-IMP-NET-LIQTO
                :IND-BEL-IMP-NET-LIQTO
               ,:BEL-RIT-ACC-IPT
                :IND-BEL-RIT-ACC-IPT
               ,:BEL-RIT-VIS-IPT
                :IND-BEL-RIT-VIS-IPT
               ,:BEL-RIT-TFR-IPT
                :IND-BEL-RIT-TFR-IPT
               ,:BEL-IMPST-IRPEF-IPT
                :IND-BEL-IMPST-IRPEF-IPT
               ,:BEL-IMPST-SOST-IPT
                :IND-BEL-IMPST-SOST-IPT
               ,:BEL-IMPST-PRVR-IPT
                :IND-BEL-IMPST-PRVR-IPT
               ,:BEL-IMPST-252-IPT
                :IND-BEL-IMPST-252-IPT
               ,:BEL-ID-ASSTO
                :IND-BEL-ID-ASSTO
               ,:BEL-TAX-SEP
                :IND-BEL-TAX-SEP
               ,:BEL-DT-RISERVE-SOM-P-DB
                :IND-BEL-DT-RISERVE-SOM-P
               ,:BEL-DT-VLT-DB
                :IND-BEL-DT-VLT
               ,:BEL-TP-STAT-LIQ-BNFICR
                :IND-BEL-TP-STAT-LIQ-BNFICR
               ,:BEL-DS-RIGA
               ,:BEL-DS-OPER-SQL
               ,:BEL-DS-VER
               ,:BEL-DS-TS-INI-CPTZ
               ,:BEL-DS-TS-END-CPTZ
               ,:BEL-DS-UTENTE
               ,:BEL-DS-STATO-ELAB
               ,:BEL-RICH-CALC-CNBT-INP
                :IND-BEL-RICH-CALC-CNBT-INP
               ,:BEL-IMP-INTR-RIT-PAG
                :IND-BEL-IMP-INTR-RIT-PAG
               ,:BEL-DT-ULT-DOCTO-DB
                :IND-BEL-DT-ULT-DOCTO
               ,:BEL-DT-DORMIENZA-DB
                :IND-BEL-DT-DORMIENZA
               ,:BEL-IMPST-BOLLO-TOT-V
                :IND-BEL-IMPST-BOLLO-TOT-V
               ,:BEL-IMPST-VIS-1382011
                :IND-BEL-IMPST-VIS-1382011
               ,:BEL-IMPST-SOST-1382011
                :IND-BEL-IMPST-SOST-1382011
               ,:BEL-IMPST-VIS-662014
                :IND-BEL-IMPST-VIS-662014
               ,:BEL-IMPST-SOST-662014
                :IND-BEL-IMPST-SOST-662014
             FROM BNFICR_LIQ
             WHERE     ID_BNFICR_LIQ = :BEL-ID-BNFICR-LIQ
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-CPZ-BEL CURSOR FOR
              SELECT
                     ID_BNFICR_LIQ
                    ,ID_LIQ
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,COD_COMP_ANIA
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,ID_RAPP_ANA
                    ,COD_BNFICR
                    ,DESC_BNFICR
                    ,PC_LIQ
                    ,ESRCN_ATTVT_IMPRS
                    ,TP_IND_BNFICR
                    ,FL_ESE
                    ,FL_IRREV
                    ,IMP_LRD_LIQTO
                    ,IMPST_IPT
                    ,IMP_NET_LIQTO
                    ,RIT_ACC_IPT
                    ,RIT_VIS_IPT
                    ,RIT_TFR_IPT
                    ,IMPST_IRPEF_IPT
                    ,IMPST_SOST_IPT
                    ,IMPST_PRVR_IPT
                    ,IMPST_252_IPT
                    ,ID_ASSTO
                    ,TAX_SEP
                    ,DT_RISERVE_SOM_P
                    ,DT_VLT
                    ,TP_STAT_LIQ_BNFICR
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,RICH_CALC_CNBT_INP
                    ,IMP_INTR_RIT_PAG
                    ,DT_ULT_DOCTO
                    ,DT_DORMIENZA
                    ,IMPST_BOLLO_TOT_V
                    ,IMPST_VIS_1382011
                    ,IMPST_SOST_1382011
                    ,IMPST_VIS_662014
                    ,IMPST_SOST_662014
              FROM BNFICR_LIQ
              WHERE     ID_LIQ = :BEL-ID-LIQ
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_BNFICR_LIQ ASC

           END-EXEC.

       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_BNFICR_LIQ
                ,ID_LIQ
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,COD_COMP_ANIA
                ,DT_INI_EFF
                ,DT_END_EFF
                ,ID_RAPP_ANA
                ,COD_BNFICR
                ,DESC_BNFICR
                ,PC_LIQ
                ,ESRCN_ATTVT_IMPRS
                ,TP_IND_BNFICR
                ,FL_ESE
                ,FL_IRREV
                ,IMP_LRD_LIQTO
                ,IMPST_IPT
                ,IMP_NET_LIQTO
                ,RIT_ACC_IPT
                ,RIT_VIS_IPT
                ,RIT_TFR_IPT
                ,IMPST_IRPEF_IPT
                ,IMPST_SOST_IPT
                ,IMPST_PRVR_IPT
                ,IMPST_252_IPT
                ,ID_ASSTO
                ,TAX_SEP
                ,DT_RISERVE_SOM_P
                ,DT_VLT
                ,TP_STAT_LIQ_BNFICR
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,RICH_CALC_CNBT_INP
                ,IMP_INTR_RIT_PAG
                ,DT_ULT_DOCTO
                ,DT_DORMIENZA
                ,IMPST_BOLLO_TOT_V
                ,IMPST_VIS_1382011
                ,IMPST_SOST_1382011
                ,IMPST_VIS_662014
                ,IMPST_SOST_662014
             INTO
                :BEL-ID-BNFICR-LIQ
               ,:BEL-ID-LIQ
               ,:BEL-ID-MOVI-CRZ
               ,:BEL-ID-MOVI-CHIU
                :IND-BEL-ID-MOVI-CHIU
               ,:BEL-COD-COMP-ANIA
               ,:BEL-DT-INI-EFF-DB
               ,:BEL-DT-END-EFF-DB
               ,:BEL-ID-RAPP-ANA
                :IND-BEL-ID-RAPP-ANA
               ,:BEL-COD-BNFICR
                :IND-BEL-COD-BNFICR
               ,:BEL-DESC-BNFICR-VCHAR
                :IND-BEL-DESC-BNFICR
               ,:BEL-PC-LIQ
                :IND-BEL-PC-LIQ
               ,:BEL-ESRCN-ATTVT-IMPRS
                :IND-BEL-ESRCN-ATTVT-IMPRS
               ,:BEL-TP-IND-BNFICR
                :IND-BEL-TP-IND-BNFICR
               ,:BEL-FL-ESE
                :IND-BEL-FL-ESE
               ,:BEL-FL-IRREV
                :IND-BEL-FL-IRREV
               ,:BEL-IMP-LRD-LIQTO
                :IND-BEL-IMP-LRD-LIQTO
               ,:BEL-IMPST-IPT
                :IND-BEL-IMPST-IPT
               ,:BEL-IMP-NET-LIQTO
                :IND-BEL-IMP-NET-LIQTO
               ,:BEL-RIT-ACC-IPT
                :IND-BEL-RIT-ACC-IPT
               ,:BEL-RIT-VIS-IPT
                :IND-BEL-RIT-VIS-IPT
               ,:BEL-RIT-TFR-IPT
                :IND-BEL-RIT-TFR-IPT
               ,:BEL-IMPST-IRPEF-IPT
                :IND-BEL-IMPST-IRPEF-IPT
               ,:BEL-IMPST-SOST-IPT
                :IND-BEL-IMPST-SOST-IPT
               ,:BEL-IMPST-PRVR-IPT
                :IND-BEL-IMPST-PRVR-IPT
               ,:BEL-IMPST-252-IPT
                :IND-BEL-IMPST-252-IPT
               ,:BEL-ID-ASSTO
                :IND-BEL-ID-ASSTO
               ,:BEL-TAX-SEP
                :IND-BEL-TAX-SEP
               ,:BEL-DT-RISERVE-SOM-P-DB
                :IND-BEL-DT-RISERVE-SOM-P
               ,:BEL-DT-VLT-DB
                :IND-BEL-DT-VLT
               ,:BEL-TP-STAT-LIQ-BNFICR
                :IND-BEL-TP-STAT-LIQ-BNFICR
               ,:BEL-DS-RIGA
               ,:BEL-DS-OPER-SQL
               ,:BEL-DS-VER
               ,:BEL-DS-TS-INI-CPTZ
               ,:BEL-DS-TS-END-CPTZ
               ,:BEL-DS-UTENTE
               ,:BEL-DS-STATO-ELAB
               ,:BEL-RICH-CALC-CNBT-INP
                :IND-BEL-RICH-CALC-CNBT-INP
               ,:BEL-IMP-INTR-RIT-PAG
                :IND-BEL-IMP-INTR-RIT-PAG
               ,:BEL-DT-ULT-DOCTO-DB
                :IND-BEL-DT-ULT-DOCTO
               ,:BEL-DT-DORMIENZA-DB
                :IND-BEL-DT-DORMIENZA
               ,:BEL-IMPST-BOLLO-TOT-V
                :IND-BEL-IMPST-BOLLO-TOT-V
               ,:BEL-IMPST-VIS-1382011
                :IND-BEL-IMPST-VIS-1382011
               ,:BEL-IMPST-SOST-1382011
                :IND-BEL-IMPST-SOST-1382011
               ,:BEL-IMPST-VIS-662014
                :IND-BEL-IMPST-VIS-662014
               ,:BEL-IMPST-SOST-662014
                :IND-BEL-IMPST-SOST-662014
             FROM BNFICR_LIQ
             WHERE     ID_LIQ = :BEL-ID-LIQ
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           EXEC SQL
                OPEN C-IDP-CPZ-BEL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           EXEC SQL
                CLOSE C-IDP-CPZ-BEL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           EXEC SQL
                FETCH C-IDP-CPZ-BEL
           INTO
                :BEL-ID-BNFICR-LIQ
               ,:BEL-ID-LIQ
               ,:BEL-ID-MOVI-CRZ
               ,:BEL-ID-MOVI-CHIU
                :IND-BEL-ID-MOVI-CHIU
               ,:BEL-COD-COMP-ANIA
               ,:BEL-DT-INI-EFF-DB
               ,:BEL-DT-END-EFF-DB
               ,:BEL-ID-RAPP-ANA
                :IND-BEL-ID-RAPP-ANA
               ,:BEL-COD-BNFICR
                :IND-BEL-COD-BNFICR
               ,:BEL-DESC-BNFICR-VCHAR
                :IND-BEL-DESC-BNFICR
               ,:BEL-PC-LIQ
                :IND-BEL-PC-LIQ
               ,:BEL-ESRCN-ATTVT-IMPRS
                :IND-BEL-ESRCN-ATTVT-IMPRS
               ,:BEL-TP-IND-BNFICR
                :IND-BEL-TP-IND-BNFICR
               ,:BEL-FL-ESE
                :IND-BEL-FL-ESE
               ,:BEL-FL-IRREV
                :IND-BEL-FL-IRREV
               ,:BEL-IMP-LRD-LIQTO
                :IND-BEL-IMP-LRD-LIQTO
               ,:BEL-IMPST-IPT
                :IND-BEL-IMPST-IPT
               ,:BEL-IMP-NET-LIQTO
                :IND-BEL-IMP-NET-LIQTO
               ,:BEL-RIT-ACC-IPT
                :IND-BEL-RIT-ACC-IPT
               ,:BEL-RIT-VIS-IPT
                :IND-BEL-RIT-VIS-IPT
               ,:BEL-RIT-TFR-IPT
                :IND-BEL-RIT-TFR-IPT
               ,:BEL-IMPST-IRPEF-IPT
                :IND-BEL-IMPST-IRPEF-IPT
               ,:BEL-IMPST-SOST-IPT
                :IND-BEL-IMPST-SOST-IPT
               ,:BEL-IMPST-PRVR-IPT
                :IND-BEL-IMPST-PRVR-IPT
               ,:BEL-IMPST-252-IPT
                :IND-BEL-IMPST-252-IPT
               ,:BEL-ID-ASSTO
                :IND-BEL-ID-ASSTO
               ,:BEL-TAX-SEP
                :IND-BEL-TAX-SEP
               ,:BEL-DT-RISERVE-SOM-P-DB
                :IND-BEL-DT-RISERVE-SOM-P
               ,:BEL-DT-VLT-DB
                :IND-BEL-DT-VLT
               ,:BEL-TP-STAT-LIQ-BNFICR
                :IND-BEL-TP-STAT-LIQ-BNFICR
               ,:BEL-DS-RIGA
               ,:BEL-DS-OPER-SQL
               ,:BEL-DS-VER
               ,:BEL-DS-TS-INI-CPTZ
               ,:BEL-DS-TS-END-CPTZ
               ,:BEL-DS-UTENTE
               ,:BEL-DS-STATO-ELAB
               ,:BEL-RICH-CALC-CNBT-INP
                :IND-BEL-RICH-CALC-CNBT-INP
               ,:BEL-IMP-INTR-RIT-PAG
                :IND-BEL-IMP-INTR-RIT-PAG
               ,:BEL-DT-ULT-DOCTO-DB
                :IND-BEL-DT-ULT-DOCTO
               ,:BEL-DT-DORMIENZA-DB
                :IND-BEL-DT-DORMIENZA
               ,:BEL-IMPST-BOLLO-TOT-V
                :IND-BEL-IMPST-BOLLO-TOT-V
               ,:BEL-IMPST-VIS-1382011
                :IND-BEL-IMPST-VIS-1382011
               ,:BEL-IMPST-SOST-1382011
                :IND-BEL-IMPST-SOST-1382011
               ,:BEL-IMPST-VIS-662014
                :IND-BEL-IMPST-VIS-662014
               ,:BEL-IMPST-SOST-662014
                :IND-BEL-IMPST-SOST-662014
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-BEL-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO BEL-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-BEL-ID-RAPP-ANA = -1
              MOVE HIGH-VALUES TO BEL-ID-RAPP-ANA-NULL
           END-IF
           IF IND-BEL-COD-BNFICR = -1
              MOVE HIGH-VALUES TO BEL-COD-BNFICR-NULL
           END-IF
           IF IND-BEL-DESC-BNFICR = -1
              MOVE HIGH-VALUES TO BEL-DESC-BNFICR
           END-IF
           IF IND-BEL-PC-LIQ = -1
              MOVE HIGH-VALUES TO BEL-PC-LIQ-NULL
           END-IF
           IF IND-BEL-ESRCN-ATTVT-IMPRS = -1
              MOVE HIGH-VALUES TO BEL-ESRCN-ATTVT-IMPRS-NULL
           END-IF
           IF IND-BEL-TP-IND-BNFICR = -1
              MOVE HIGH-VALUES TO BEL-TP-IND-BNFICR-NULL
           END-IF
           IF IND-BEL-FL-ESE = -1
              MOVE HIGH-VALUES TO BEL-FL-ESE-NULL
           END-IF
           IF IND-BEL-FL-IRREV = -1
              MOVE HIGH-VALUES TO BEL-FL-IRREV-NULL
           END-IF
           IF IND-BEL-IMP-LRD-LIQTO = -1
              MOVE HIGH-VALUES TO BEL-IMP-LRD-LIQTO-NULL
           END-IF
           IF IND-BEL-IMPST-IPT = -1
              MOVE HIGH-VALUES TO BEL-IMPST-IPT-NULL
           END-IF
           IF IND-BEL-IMP-NET-LIQTO = -1
              MOVE HIGH-VALUES TO BEL-IMP-NET-LIQTO-NULL
           END-IF
           IF IND-BEL-RIT-ACC-IPT = -1
              MOVE HIGH-VALUES TO BEL-RIT-ACC-IPT-NULL
           END-IF
           IF IND-BEL-RIT-VIS-IPT = -1
              MOVE HIGH-VALUES TO BEL-RIT-VIS-IPT-NULL
           END-IF
           IF IND-BEL-RIT-TFR-IPT = -1
              MOVE HIGH-VALUES TO BEL-RIT-TFR-IPT-NULL
           END-IF
           IF IND-BEL-IMPST-IRPEF-IPT = -1
              MOVE HIGH-VALUES TO BEL-IMPST-IRPEF-IPT-NULL
           END-IF
           IF IND-BEL-IMPST-SOST-IPT = -1
              MOVE HIGH-VALUES TO BEL-IMPST-SOST-IPT-NULL
           END-IF
           IF IND-BEL-IMPST-PRVR-IPT = -1
              MOVE HIGH-VALUES TO BEL-IMPST-PRVR-IPT-NULL
           END-IF
           IF IND-BEL-IMPST-252-IPT = -1
              MOVE HIGH-VALUES TO BEL-IMPST-252-IPT-NULL
           END-IF
           IF IND-BEL-ID-ASSTO = -1
              MOVE HIGH-VALUES TO BEL-ID-ASSTO-NULL
           END-IF
           IF IND-BEL-TAX-SEP = -1
              MOVE HIGH-VALUES TO BEL-TAX-SEP-NULL
           END-IF
           IF IND-BEL-DT-RISERVE-SOM-P = -1
              MOVE HIGH-VALUES TO BEL-DT-RISERVE-SOM-P-NULL
           END-IF
           IF IND-BEL-DT-VLT = -1
              MOVE HIGH-VALUES TO BEL-DT-VLT-NULL
           END-IF
           IF IND-BEL-TP-STAT-LIQ-BNFICR = -1
              MOVE HIGH-VALUES TO BEL-TP-STAT-LIQ-BNFICR-NULL
           END-IF
           IF IND-BEL-RICH-CALC-CNBT-INP = -1
              MOVE HIGH-VALUES TO BEL-RICH-CALC-CNBT-INP-NULL
           END-IF
           IF IND-BEL-IMP-INTR-RIT-PAG = -1
              MOVE HIGH-VALUES TO BEL-IMP-INTR-RIT-PAG-NULL
           END-IF
           IF IND-BEL-DT-ULT-DOCTO = -1
              MOVE HIGH-VALUES TO BEL-DT-ULT-DOCTO-NULL
           END-IF
           IF IND-BEL-DT-DORMIENZA = -1
              MOVE HIGH-VALUES TO BEL-DT-DORMIENZA-NULL
           END-IF
           IF IND-BEL-IMPST-BOLLO-TOT-V = -1
              MOVE HIGH-VALUES TO BEL-IMPST-BOLLO-TOT-V-NULL
           END-IF
           IF IND-BEL-IMPST-VIS-1382011 = -1
              MOVE HIGH-VALUES TO BEL-IMPST-VIS-1382011-NULL
           END-IF
           IF IND-BEL-IMPST-SOST-1382011 = -1
              MOVE HIGH-VALUES TO BEL-IMPST-SOST-1382011-NULL
           END-IF
           IF IND-BEL-IMPST-VIS-662014 = -1
              MOVE HIGH-VALUES TO BEL-IMPST-VIS-662014-NULL
           END-IF
           IF IND-BEL-IMPST-SOST-662014 = -1
              MOVE HIGH-VALUES TO BEL-IMPST-SOST-662014-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO BEL-DS-OPER-SQL
           MOVE 0                   TO BEL-DS-VER
           MOVE IDSV0003-USER-NAME TO BEL-DS-UTENTE
           MOVE '1'                   TO BEL-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO BEL-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO BEL-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF BEL-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-BEL-ID-MOVI-CHIU
           END-IF
           IF BEL-ID-RAPP-ANA-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-ID-RAPP-ANA
           ELSE
              MOVE 0 TO IND-BEL-ID-RAPP-ANA
           END-IF
           IF BEL-COD-BNFICR-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-COD-BNFICR
           ELSE
              MOVE 0 TO IND-BEL-COD-BNFICR
           END-IF
           IF BEL-DESC-BNFICR = HIGH-VALUES
              MOVE -1 TO IND-BEL-DESC-BNFICR
           ELSE
              MOVE 0 TO IND-BEL-DESC-BNFICR
           END-IF
           IF BEL-PC-LIQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-PC-LIQ
           ELSE
              MOVE 0 TO IND-BEL-PC-LIQ
           END-IF
           IF BEL-ESRCN-ATTVT-IMPRS-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-ESRCN-ATTVT-IMPRS
           ELSE
              MOVE 0 TO IND-BEL-ESRCN-ATTVT-IMPRS
           END-IF
           IF BEL-TP-IND-BNFICR-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-TP-IND-BNFICR
           ELSE
              MOVE 0 TO IND-BEL-TP-IND-BNFICR
           END-IF
           IF BEL-FL-ESE-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-FL-ESE
           ELSE
              MOVE 0 TO IND-BEL-FL-ESE
           END-IF
           IF BEL-FL-IRREV-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-FL-IRREV
           ELSE
              MOVE 0 TO IND-BEL-FL-IRREV
           END-IF
           IF BEL-IMP-LRD-LIQTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-IMP-LRD-LIQTO
           ELSE
              MOVE 0 TO IND-BEL-IMP-LRD-LIQTO
           END-IF
           IF BEL-IMPST-IPT-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-IMPST-IPT
           ELSE
              MOVE 0 TO IND-BEL-IMPST-IPT
           END-IF
           IF BEL-IMP-NET-LIQTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-IMP-NET-LIQTO
           ELSE
              MOVE 0 TO IND-BEL-IMP-NET-LIQTO
           END-IF
           IF BEL-RIT-ACC-IPT-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-RIT-ACC-IPT
           ELSE
              MOVE 0 TO IND-BEL-RIT-ACC-IPT
           END-IF
           IF BEL-RIT-VIS-IPT-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-RIT-VIS-IPT
           ELSE
              MOVE 0 TO IND-BEL-RIT-VIS-IPT
           END-IF
           IF BEL-RIT-TFR-IPT-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-RIT-TFR-IPT
           ELSE
              MOVE 0 TO IND-BEL-RIT-TFR-IPT
           END-IF
           IF BEL-IMPST-IRPEF-IPT-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-IMPST-IRPEF-IPT
           ELSE
              MOVE 0 TO IND-BEL-IMPST-IRPEF-IPT
           END-IF
           IF BEL-IMPST-SOST-IPT-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-IMPST-SOST-IPT
           ELSE
              MOVE 0 TO IND-BEL-IMPST-SOST-IPT
           END-IF
           IF BEL-IMPST-PRVR-IPT-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-IMPST-PRVR-IPT
           ELSE
              MOVE 0 TO IND-BEL-IMPST-PRVR-IPT
           END-IF
           IF BEL-IMPST-252-IPT-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-IMPST-252-IPT
           ELSE
              MOVE 0 TO IND-BEL-IMPST-252-IPT
           END-IF
           IF BEL-ID-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-ID-ASSTO
           ELSE
              MOVE 0 TO IND-BEL-ID-ASSTO
           END-IF
           IF BEL-TAX-SEP-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-TAX-SEP
           ELSE
              MOVE 0 TO IND-BEL-TAX-SEP
           END-IF
           IF BEL-DT-RISERVE-SOM-P-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-DT-RISERVE-SOM-P
           ELSE
              MOVE 0 TO IND-BEL-DT-RISERVE-SOM-P
           END-IF
           IF BEL-DT-VLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-DT-VLT
           ELSE
              MOVE 0 TO IND-BEL-DT-VLT
           END-IF
           IF BEL-TP-STAT-LIQ-BNFICR-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-TP-STAT-LIQ-BNFICR
           ELSE
              MOVE 0 TO IND-BEL-TP-STAT-LIQ-BNFICR
           END-IF
           IF BEL-RICH-CALC-CNBT-INP-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-RICH-CALC-CNBT-INP
           ELSE
              MOVE 0 TO IND-BEL-RICH-CALC-CNBT-INP
           END-IF
           IF BEL-IMP-INTR-RIT-PAG-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-IMP-INTR-RIT-PAG
           ELSE
              MOVE 0 TO IND-BEL-IMP-INTR-RIT-PAG
           END-IF
           IF BEL-DT-ULT-DOCTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-DT-ULT-DOCTO
           ELSE
              MOVE 0 TO IND-BEL-DT-ULT-DOCTO
           END-IF
           IF BEL-DT-DORMIENZA-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-DT-DORMIENZA
           ELSE
              MOVE 0 TO IND-BEL-DT-DORMIENZA
           END-IF
           IF BEL-IMPST-BOLLO-TOT-V-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-IMPST-BOLLO-TOT-V
           ELSE
              MOVE 0 TO IND-BEL-IMPST-BOLLO-TOT-V
           END-IF
           IF BEL-IMPST-VIS-1382011-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-IMPST-VIS-1382011
           ELSE
              MOVE 0 TO IND-BEL-IMPST-VIS-1382011
           END-IF
           IF BEL-IMPST-SOST-1382011-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-IMPST-SOST-1382011
           ELSE
              MOVE 0 TO IND-BEL-IMPST-SOST-1382011
           END-IF
           IF BEL-IMPST-VIS-662014-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-IMPST-VIS-662014
           ELSE
              MOVE 0 TO IND-BEL-IMPST-VIS-662014
           END-IF
           IF BEL-IMPST-SOST-662014-NULL = HIGH-VALUES
              MOVE -1 TO IND-BEL-IMPST-SOST-662014
           ELSE
              MOVE 0 TO IND-BEL-IMPST-SOST-662014
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : BEL-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE BNFICR-LIQ TO WS-BUFFER-TABLE.

           MOVE BEL-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO BEL-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO BEL-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO BEL-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO BEL-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO BEL-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO BEL-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO BEL-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.

           MOVE BNFICR-LIQ TO WS-BUFFER-TABLE.

           MOVE BEL-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.

       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO BNFICR-LIQ.

           MOVE WS-ID-MOVI-CRZ  TO BEL-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO BEL-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO BEL-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO BEL-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO BEL-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO BEL-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO BEL-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE BEL-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO BEL-DT-INI-EFF-DB
           MOVE BEL-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO BEL-DT-END-EFF-DB
           IF IND-BEL-DT-RISERVE-SOM-P = 0
               MOVE BEL-DT-RISERVE-SOM-P TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO BEL-DT-RISERVE-SOM-P-DB
           END-IF
           IF IND-BEL-DT-VLT = 0
               MOVE BEL-DT-VLT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO BEL-DT-VLT-DB
           END-IF
           IF IND-BEL-DT-ULT-DOCTO = 0
               MOVE BEL-DT-ULT-DOCTO TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO BEL-DT-ULT-DOCTO-DB
           END-IF
           IF IND-BEL-DT-DORMIENZA = 0
               MOVE BEL-DT-DORMIENZA TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO BEL-DT-DORMIENZA-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE BEL-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO BEL-DT-INI-EFF
           MOVE BEL-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO BEL-DT-END-EFF
           IF IND-BEL-DT-RISERVE-SOM-P = 0
               MOVE BEL-DT-RISERVE-SOM-P-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO BEL-DT-RISERVE-SOM-P
           END-IF
           IF IND-BEL-DT-VLT = 0
               MOVE BEL-DT-VLT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO BEL-DT-VLT
           END-IF
           IF IND-BEL-DT-ULT-DOCTO = 0
               MOVE BEL-DT-ULT-DOCTO-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO BEL-DT-ULT-DOCTO
           END-IF
           IF IND-BEL-DT-DORMIENZA = 0
               MOVE BEL-DT-DORMIENZA-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO BEL-DT-DORMIENZA
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.


           MOVE LENGTH OF BEL-DESC-BNFICR
                       TO BEL-DESC-BNFICR-LEN.

       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

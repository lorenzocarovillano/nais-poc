       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSRST0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  09 LUG 2010.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDRST0 END-EXEC.
           EXEC SQL INCLUDE IDBVRST2 END-EXEC.
           EXEC SQL INCLUDE IDBVRST3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVRST1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 RIS-DI-TRCH.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSRST0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'RIS_DI_TRCH' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_RIS_DI_TRCH
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_CALC_RIS
                ,ULT_RM
                ,DT_CALC
                ,DT_ELAB
                ,RIS_BILA
                ,RIS_MAT
                ,INCR_X_RIVAL
                ,RPTO_PRE
                ,FRAZ_PRE_PP
                ,RIS_TOT
                ,RIS_SPE
                ,RIS_ABB
                ,RIS_BNSFDT
                ,RIS_SOPR
                ,RIS_INTEG_BAS_TEC
                ,RIS_INTEG_DECR_TS
                ,RIS_GAR_CASO_MOR
                ,RIS_ZIL
                ,RIS_FAIVL
                ,RIS_COS_AMMTZ
                ,RIS_SPE_FAIVL
                ,RIS_PREST_FAIVL
                ,RIS_COMPON_ASSVA
                ,ULT_COEFF_RIS
                ,ULT_COEFF_AGG_RIS
                ,RIS_ACQ
                ,RIS_UTI
                ,RIS_MAT_EFF
                ,RIS_RISTORNI_CAP
                ,RIS_TRM_BNS
                ,RIS_BNSRIC
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,ID_TRCH_DI_GAR
                ,COD_FND
                ,ID_POLI
                ,ID_ADES
                ,ID_GAR
                ,RIS_MIN_GARTO
                ,RIS_RSH_DFLT
                ,RIS_MOVI_NON_INVES
             INTO
                :RST-ID-RIS-DI-TRCH
               ,:RST-ID-MOVI-CRZ
               ,:RST-ID-MOVI-CHIU
                :IND-RST-ID-MOVI-CHIU
               ,:RST-DT-INI-EFF-DB
               ,:RST-DT-END-EFF-DB
               ,:RST-COD-COMP-ANIA
               ,:RST-TP-CALC-RIS
                :IND-RST-TP-CALC-RIS
               ,:RST-ULT-RM
                :IND-RST-ULT-RM
               ,:RST-DT-CALC-DB
                :IND-RST-DT-CALC
               ,:RST-DT-ELAB-DB
                :IND-RST-DT-ELAB
               ,:RST-RIS-BILA
                :IND-RST-RIS-BILA
               ,:RST-RIS-MAT
                :IND-RST-RIS-MAT
               ,:RST-INCR-X-RIVAL
                :IND-RST-INCR-X-RIVAL
               ,:RST-RPTO-PRE
                :IND-RST-RPTO-PRE
               ,:RST-FRAZ-PRE-PP
                :IND-RST-FRAZ-PRE-PP
               ,:RST-RIS-TOT
                :IND-RST-RIS-TOT
               ,:RST-RIS-SPE
                :IND-RST-RIS-SPE
               ,:RST-RIS-ABB
                :IND-RST-RIS-ABB
               ,:RST-RIS-BNSFDT
                :IND-RST-RIS-BNSFDT
               ,:RST-RIS-SOPR
                :IND-RST-RIS-SOPR
               ,:RST-RIS-INTEG-BAS-TEC
                :IND-RST-RIS-INTEG-BAS-TEC
               ,:RST-RIS-INTEG-DECR-TS
                :IND-RST-RIS-INTEG-DECR-TS
               ,:RST-RIS-GAR-CASO-MOR
                :IND-RST-RIS-GAR-CASO-MOR
               ,:RST-RIS-ZIL
                :IND-RST-RIS-ZIL
               ,:RST-RIS-FAIVL
                :IND-RST-RIS-FAIVL
               ,:RST-RIS-COS-AMMTZ
                :IND-RST-RIS-COS-AMMTZ
               ,:RST-RIS-SPE-FAIVL
                :IND-RST-RIS-SPE-FAIVL
               ,:RST-RIS-PREST-FAIVL
                :IND-RST-RIS-PREST-FAIVL
               ,:RST-RIS-COMPON-ASSVA
                :IND-RST-RIS-COMPON-ASSVA
               ,:RST-ULT-COEFF-RIS
                :IND-RST-ULT-COEFF-RIS
               ,:RST-ULT-COEFF-AGG-RIS
                :IND-RST-ULT-COEFF-AGG-RIS
               ,:RST-RIS-ACQ
                :IND-RST-RIS-ACQ
               ,:RST-RIS-UTI
                :IND-RST-RIS-UTI
               ,:RST-RIS-MAT-EFF
                :IND-RST-RIS-MAT-EFF
               ,:RST-RIS-RISTORNI-CAP
                :IND-RST-RIS-RISTORNI-CAP
               ,:RST-RIS-TRM-BNS
                :IND-RST-RIS-TRM-BNS
               ,:RST-RIS-BNSRIC
                :IND-RST-RIS-BNSRIC
               ,:RST-DS-RIGA
               ,:RST-DS-OPER-SQL
               ,:RST-DS-VER
               ,:RST-DS-TS-INI-CPTZ
               ,:RST-DS-TS-END-CPTZ
               ,:RST-DS-UTENTE
               ,:RST-DS-STATO-ELAB
               ,:RST-ID-TRCH-DI-GAR
               ,:RST-COD-FND
                :IND-RST-COD-FND
               ,:RST-ID-POLI
               ,:RST-ID-ADES
               ,:RST-ID-GAR
               ,:RST-RIS-MIN-GARTO
                :IND-RST-RIS-MIN-GARTO
               ,:RST-RIS-RSH-DFLT
                :IND-RST-RIS-RSH-DFLT
               ,:RST-RIS-MOVI-NON-INVES
                :IND-RST-RIS-MOVI-NON-INVES
             FROM RIS_DI_TRCH
             WHERE     DS_RIGA = :RST-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO RIS_DI_TRCH
                     (
                        ID_RIS_DI_TRCH
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,TP_CALC_RIS
                       ,ULT_RM
                       ,DT_CALC
                       ,DT_ELAB
                       ,RIS_BILA
                       ,RIS_MAT
                       ,INCR_X_RIVAL
                       ,RPTO_PRE
                       ,FRAZ_PRE_PP
                       ,RIS_TOT
                       ,RIS_SPE
                       ,RIS_ABB
                       ,RIS_BNSFDT
                       ,RIS_SOPR
                       ,RIS_INTEG_BAS_TEC
                       ,RIS_INTEG_DECR_TS
                       ,RIS_GAR_CASO_MOR
                       ,RIS_ZIL
                       ,RIS_FAIVL
                       ,RIS_COS_AMMTZ
                       ,RIS_SPE_FAIVL
                       ,RIS_PREST_FAIVL
                       ,RIS_COMPON_ASSVA
                       ,ULT_COEFF_RIS
                       ,ULT_COEFF_AGG_RIS
                       ,RIS_ACQ
                       ,RIS_UTI
                       ,RIS_MAT_EFF
                       ,RIS_RISTORNI_CAP
                       ,RIS_TRM_BNS
                       ,RIS_BNSRIC
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,ID_TRCH_DI_GAR
                       ,COD_FND
                       ,ID_POLI
                       ,ID_ADES
                       ,ID_GAR
                       ,RIS_MIN_GARTO
                       ,RIS_RSH_DFLT
                       ,RIS_MOVI_NON_INVES
                     )
                 VALUES
                     (
                       :RST-ID-RIS-DI-TRCH
                       ,:RST-ID-MOVI-CRZ
                       ,:RST-ID-MOVI-CHIU
                        :IND-RST-ID-MOVI-CHIU
                       ,:RST-DT-INI-EFF-DB
                       ,:RST-DT-END-EFF-DB
                       ,:RST-COD-COMP-ANIA
                       ,:RST-TP-CALC-RIS
                        :IND-RST-TP-CALC-RIS
                       ,:RST-ULT-RM
                        :IND-RST-ULT-RM
                       ,:RST-DT-CALC-DB
                        :IND-RST-DT-CALC
                       ,:RST-DT-ELAB-DB
                        :IND-RST-DT-ELAB
                       ,:RST-RIS-BILA
                        :IND-RST-RIS-BILA
                       ,:RST-RIS-MAT
                        :IND-RST-RIS-MAT
                       ,:RST-INCR-X-RIVAL
                        :IND-RST-INCR-X-RIVAL
                       ,:RST-RPTO-PRE
                        :IND-RST-RPTO-PRE
                       ,:RST-FRAZ-PRE-PP
                        :IND-RST-FRAZ-PRE-PP
                       ,:RST-RIS-TOT
                        :IND-RST-RIS-TOT
                       ,:RST-RIS-SPE
                        :IND-RST-RIS-SPE
                       ,:RST-RIS-ABB
                        :IND-RST-RIS-ABB
                       ,:RST-RIS-BNSFDT
                        :IND-RST-RIS-BNSFDT
                       ,:RST-RIS-SOPR
                        :IND-RST-RIS-SOPR
                       ,:RST-RIS-INTEG-BAS-TEC
                        :IND-RST-RIS-INTEG-BAS-TEC
                       ,:RST-RIS-INTEG-DECR-TS
                        :IND-RST-RIS-INTEG-DECR-TS
                       ,:RST-RIS-GAR-CASO-MOR
                        :IND-RST-RIS-GAR-CASO-MOR
                       ,:RST-RIS-ZIL
                        :IND-RST-RIS-ZIL
                       ,:RST-RIS-FAIVL
                        :IND-RST-RIS-FAIVL
                       ,:RST-RIS-COS-AMMTZ
                        :IND-RST-RIS-COS-AMMTZ
                       ,:RST-RIS-SPE-FAIVL
                        :IND-RST-RIS-SPE-FAIVL
                       ,:RST-RIS-PREST-FAIVL
                        :IND-RST-RIS-PREST-FAIVL
                       ,:RST-RIS-COMPON-ASSVA
                        :IND-RST-RIS-COMPON-ASSVA
                       ,:RST-ULT-COEFF-RIS
                        :IND-RST-ULT-COEFF-RIS
                       ,:RST-ULT-COEFF-AGG-RIS
                        :IND-RST-ULT-COEFF-AGG-RIS
                       ,:RST-RIS-ACQ
                        :IND-RST-RIS-ACQ
                       ,:RST-RIS-UTI
                        :IND-RST-RIS-UTI
                       ,:RST-RIS-MAT-EFF
                        :IND-RST-RIS-MAT-EFF
                       ,:RST-RIS-RISTORNI-CAP
                        :IND-RST-RIS-RISTORNI-CAP
                       ,:RST-RIS-TRM-BNS
                        :IND-RST-RIS-TRM-BNS
                       ,:RST-RIS-BNSRIC
                        :IND-RST-RIS-BNSRIC
                       ,:RST-DS-RIGA
                       ,:RST-DS-OPER-SQL
                       ,:RST-DS-VER
                       ,:RST-DS-TS-INI-CPTZ
                       ,:RST-DS-TS-END-CPTZ
                       ,:RST-DS-UTENTE
                       ,:RST-DS-STATO-ELAB
                       ,:RST-ID-TRCH-DI-GAR
                       ,:RST-COD-FND
                        :IND-RST-COD-FND
                       ,:RST-ID-POLI
                       ,:RST-ID-ADES
                       ,:RST-ID-GAR
                       ,:RST-RIS-MIN-GARTO
                        :IND-RST-RIS-MIN-GARTO
                       ,:RST-RIS-RSH-DFLT
                        :IND-RST-RIS-RSH-DFLT
                       ,:RST-RIS-MOVI-NON-INVES
                        :IND-RST-RIS-MOVI-NON-INVES
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE RIS_DI_TRCH SET

                   ID_RIS_DI_TRCH         =
                :RST-ID-RIS-DI-TRCH
                  ,ID_MOVI_CRZ            =
                :RST-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :RST-ID-MOVI-CHIU
                                       :IND-RST-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :RST-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :RST-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :RST-COD-COMP-ANIA
                  ,TP_CALC_RIS            =
                :RST-TP-CALC-RIS
                                       :IND-RST-TP-CALC-RIS
                  ,ULT_RM                 =
                :RST-ULT-RM
                                       :IND-RST-ULT-RM
                  ,DT_CALC                =
           :RST-DT-CALC-DB
                                       :IND-RST-DT-CALC
                  ,DT_ELAB                =
           :RST-DT-ELAB-DB
                                       :IND-RST-DT-ELAB
                  ,RIS_BILA               =
                :RST-RIS-BILA
                                       :IND-RST-RIS-BILA
                  ,RIS_MAT                =
                :RST-RIS-MAT
                                       :IND-RST-RIS-MAT
                  ,INCR_X_RIVAL           =
                :RST-INCR-X-RIVAL
                                       :IND-RST-INCR-X-RIVAL
                  ,RPTO_PRE               =
                :RST-RPTO-PRE
                                       :IND-RST-RPTO-PRE
                  ,FRAZ_PRE_PP            =
                :RST-FRAZ-PRE-PP
                                       :IND-RST-FRAZ-PRE-PP
                  ,RIS_TOT                =
                :RST-RIS-TOT
                                       :IND-RST-RIS-TOT
                  ,RIS_SPE                =
                :RST-RIS-SPE
                                       :IND-RST-RIS-SPE
                  ,RIS_ABB                =
                :RST-RIS-ABB
                                       :IND-RST-RIS-ABB
                  ,RIS_BNSFDT             =
                :RST-RIS-BNSFDT
                                       :IND-RST-RIS-BNSFDT
                  ,RIS_SOPR               =
                :RST-RIS-SOPR
                                       :IND-RST-RIS-SOPR
                  ,RIS_INTEG_BAS_TEC      =
                :RST-RIS-INTEG-BAS-TEC
                                       :IND-RST-RIS-INTEG-BAS-TEC
                  ,RIS_INTEG_DECR_TS      =
                :RST-RIS-INTEG-DECR-TS
                                       :IND-RST-RIS-INTEG-DECR-TS
                  ,RIS_GAR_CASO_MOR       =
                :RST-RIS-GAR-CASO-MOR
                                       :IND-RST-RIS-GAR-CASO-MOR
                  ,RIS_ZIL                =
                :RST-RIS-ZIL
                                       :IND-RST-RIS-ZIL
                  ,RIS_FAIVL              =
                :RST-RIS-FAIVL
                                       :IND-RST-RIS-FAIVL
                  ,RIS_COS_AMMTZ          =
                :RST-RIS-COS-AMMTZ
                                       :IND-RST-RIS-COS-AMMTZ
                  ,RIS_SPE_FAIVL          =
                :RST-RIS-SPE-FAIVL
                                       :IND-RST-RIS-SPE-FAIVL
                  ,RIS_PREST_FAIVL        =
                :RST-RIS-PREST-FAIVL
                                       :IND-RST-RIS-PREST-FAIVL
                  ,RIS_COMPON_ASSVA       =
                :RST-RIS-COMPON-ASSVA
                                       :IND-RST-RIS-COMPON-ASSVA
                  ,ULT_COEFF_RIS          =
                :RST-ULT-COEFF-RIS
                                       :IND-RST-ULT-COEFF-RIS
                  ,ULT_COEFF_AGG_RIS      =
                :RST-ULT-COEFF-AGG-RIS
                                       :IND-RST-ULT-COEFF-AGG-RIS
                  ,RIS_ACQ                =
                :RST-RIS-ACQ
                                       :IND-RST-RIS-ACQ
                  ,RIS_UTI                =
                :RST-RIS-UTI
                                       :IND-RST-RIS-UTI
                  ,RIS_MAT_EFF            =
                :RST-RIS-MAT-EFF
                                       :IND-RST-RIS-MAT-EFF
                  ,RIS_RISTORNI_CAP       =
                :RST-RIS-RISTORNI-CAP
                                       :IND-RST-RIS-RISTORNI-CAP
                  ,RIS_TRM_BNS            =
                :RST-RIS-TRM-BNS
                                       :IND-RST-RIS-TRM-BNS
                  ,RIS_BNSRIC             =
                :RST-RIS-BNSRIC
                                       :IND-RST-RIS-BNSRIC
                  ,DS_RIGA                =
                :RST-DS-RIGA
                  ,DS_OPER_SQL            =
                :RST-DS-OPER-SQL
                  ,DS_VER                 =
                :RST-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :RST-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :RST-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :RST-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :RST-DS-STATO-ELAB
                  ,ID_TRCH_DI_GAR         =
                :RST-ID-TRCH-DI-GAR
                  ,COD_FND                =
                :RST-COD-FND
                                       :IND-RST-COD-FND
                  ,ID_POLI                =
                :RST-ID-POLI
                  ,ID_ADES                =
                :RST-ID-ADES
                  ,ID_GAR                 =
                :RST-ID-GAR
                  ,RIS_MIN_GARTO          =
                :RST-RIS-MIN-GARTO
                                       :IND-RST-RIS-MIN-GARTO
                  ,RIS_RSH_DFLT           =
                :RST-RIS-RSH-DFLT
                                       :IND-RST-RIS-RSH-DFLT
                  ,RIS_MOVI_NON_INVES     =
                :RST-RIS-MOVI-NON-INVES
                                       :IND-RST-RIS-MOVI-NON-INVES
                WHERE     DS_RIGA = :RST-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM RIS_DI_TRCH
                WHERE     DS_RIGA = :RST-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-RST CURSOR FOR
              SELECT
                     ID_RIS_DI_TRCH
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_CALC_RIS
                    ,ULT_RM
                    ,DT_CALC
                    ,DT_ELAB
                    ,RIS_BILA
                    ,RIS_MAT
                    ,INCR_X_RIVAL
                    ,RPTO_PRE
                    ,FRAZ_PRE_PP
                    ,RIS_TOT
                    ,RIS_SPE
                    ,RIS_ABB
                    ,RIS_BNSFDT
                    ,RIS_SOPR
                    ,RIS_INTEG_BAS_TEC
                    ,RIS_INTEG_DECR_TS
                    ,RIS_GAR_CASO_MOR
                    ,RIS_ZIL
                    ,RIS_FAIVL
                    ,RIS_COS_AMMTZ
                    ,RIS_SPE_FAIVL
                    ,RIS_PREST_FAIVL
                    ,RIS_COMPON_ASSVA
                    ,ULT_COEFF_RIS
                    ,ULT_COEFF_AGG_RIS
                    ,RIS_ACQ
                    ,RIS_UTI
                    ,RIS_MAT_EFF
                    ,RIS_RISTORNI_CAP
                    ,RIS_TRM_BNS
                    ,RIS_BNSRIC
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,ID_TRCH_DI_GAR
                    ,COD_FND
                    ,ID_POLI
                    ,ID_ADES
                    ,ID_GAR
                    ,RIS_MIN_GARTO
                    ,RIS_RSH_DFLT
                    ,RIS_MOVI_NON_INVES
              FROM RIS_DI_TRCH
              WHERE     ID_RIS_DI_TRCH = :RST-ID-RIS-DI-TRCH
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY DT_INI_EFF ASC

           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_RIS_DI_TRCH
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_CALC_RIS
                ,ULT_RM
                ,DT_CALC
                ,DT_ELAB
                ,RIS_BILA
                ,RIS_MAT
                ,INCR_X_RIVAL
                ,RPTO_PRE
                ,FRAZ_PRE_PP
                ,RIS_TOT
                ,RIS_SPE
                ,RIS_ABB
                ,RIS_BNSFDT
                ,RIS_SOPR
                ,RIS_INTEG_BAS_TEC
                ,RIS_INTEG_DECR_TS
                ,RIS_GAR_CASO_MOR
                ,RIS_ZIL
                ,RIS_FAIVL
                ,RIS_COS_AMMTZ
                ,RIS_SPE_FAIVL
                ,RIS_PREST_FAIVL
                ,RIS_COMPON_ASSVA
                ,ULT_COEFF_RIS
                ,ULT_COEFF_AGG_RIS
                ,RIS_ACQ
                ,RIS_UTI
                ,RIS_MAT_EFF
                ,RIS_RISTORNI_CAP
                ,RIS_TRM_BNS
                ,RIS_BNSRIC
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,ID_TRCH_DI_GAR
                ,COD_FND
                ,ID_POLI
                ,ID_ADES
                ,ID_GAR
                ,RIS_MIN_GARTO
                ,RIS_RSH_DFLT
                ,RIS_MOVI_NON_INVES
             INTO
                :RST-ID-RIS-DI-TRCH
               ,:RST-ID-MOVI-CRZ
               ,:RST-ID-MOVI-CHIU
                :IND-RST-ID-MOVI-CHIU
               ,:RST-DT-INI-EFF-DB
               ,:RST-DT-END-EFF-DB
               ,:RST-COD-COMP-ANIA
               ,:RST-TP-CALC-RIS
                :IND-RST-TP-CALC-RIS
               ,:RST-ULT-RM
                :IND-RST-ULT-RM
               ,:RST-DT-CALC-DB
                :IND-RST-DT-CALC
               ,:RST-DT-ELAB-DB
                :IND-RST-DT-ELAB
               ,:RST-RIS-BILA
                :IND-RST-RIS-BILA
               ,:RST-RIS-MAT
                :IND-RST-RIS-MAT
               ,:RST-INCR-X-RIVAL
                :IND-RST-INCR-X-RIVAL
               ,:RST-RPTO-PRE
                :IND-RST-RPTO-PRE
               ,:RST-FRAZ-PRE-PP
                :IND-RST-FRAZ-PRE-PP
               ,:RST-RIS-TOT
                :IND-RST-RIS-TOT
               ,:RST-RIS-SPE
                :IND-RST-RIS-SPE
               ,:RST-RIS-ABB
                :IND-RST-RIS-ABB
               ,:RST-RIS-BNSFDT
                :IND-RST-RIS-BNSFDT
               ,:RST-RIS-SOPR
                :IND-RST-RIS-SOPR
               ,:RST-RIS-INTEG-BAS-TEC
                :IND-RST-RIS-INTEG-BAS-TEC
               ,:RST-RIS-INTEG-DECR-TS
                :IND-RST-RIS-INTEG-DECR-TS
               ,:RST-RIS-GAR-CASO-MOR
                :IND-RST-RIS-GAR-CASO-MOR
               ,:RST-RIS-ZIL
                :IND-RST-RIS-ZIL
               ,:RST-RIS-FAIVL
                :IND-RST-RIS-FAIVL
               ,:RST-RIS-COS-AMMTZ
                :IND-RST-RIS-COS-AMMTZ
               ,:RST-RIS-SPE-FAIVL
                :IND-RST-RIS-SPE-FAIVL
               ,:RST-RIS-PREST-FAIVL
                :IND-RST-RIS-PREST-FAIVL
               ,:RST-RIS-COMPON-ASSVA
                :IND-RST-RIS-COMPON-ASSVA
               ,:RST-ULT-COEFF-RIS
                :IND-RST-ULT-COEFF-RIS
               ,:RST-ULT-COEFF-AGG-RIS
                :IND-RST-ULT-COEFF-AGG-RIS
               ,:RST-RIS-ACQ
                :IND-RST-RIS-ACQ
               ,:RST-RIS-UTI
                :IND-RST-RIS-UTI
               ,:RST-RIS-MAT-EFF
                :IND-RST-RIS-MAT-EFF
               ,:RST-RIS-RISTORNI-CAP
                :IND-RST-RIS-RISTORNI-CAP
               ,:RST-RIS-TRM-BNS
                :IND-RST-RIS-TRM-BNS
               ,:RST-RIS-BNSRIC
                :IND-RST-RIS-BNSRIC
               ,:RST-DS-RIGA
               ,:RST-DS-OPER-SQL
               ,:RST-DS-VER
               ,:RST-DS-TS-INI-CPTZ
               ,:RST-DS-TS-END-CPTZ
               ,:RST-DS-UTENTE
               ,:RST-DS-STATO-ELAB
               ,:RST-ID-TRCH-DI-GAR
               ,:RST-COD-FND
                :IND-RST-COD-FND
               ,:RST-ID-POLI
               ,:RST-ID-ADES
               ,:RST-ID-GAR
               ,:RST-RIS-MIN-GARTO
                :IND-RST-RIS-MIN-GARTO
               ,:RST-RIS-RSH-DFLT
                :IND-RST-RIS-RSH-DFLT
               ,:RST-RIS-MOVI-NON-INVES
                :IND-RST-RIS-MOVI-NON-INVES
             FROM RIS_DI_TRCH
             WHERE     ID_RIS_DI_TRCH = :RST-ID-RIS-DI-TRCH
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE RIS_DI_TRCH SET

                   ID_RIS_DI_TRCH         =
                :RST-ID-RIS-DI-TRCH
                  ,ID_MOVI_CRZ            =
                :RST-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :RST-ID-MOVI-CHIU
                                       :IND-RST-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :RST-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :RST-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :RST-COD-COMP-ANIA
                  ,TP_CALC_RIS            =
                :RST-TP-CALC-RIS
                                       :IND-RST-TP-CALC-RIS
                  ,ULT_RM                 =
                :RST-ULT-RM
                                       :IND-RST-ULT-RM
                  ,DT_CALC                =
           :RST-DT-CALC-DB
                                       :IND-RST-DT-CALC
                  ,DT_ELAB                =
           :RST-DT-ELAB-DB
                                       :IND-RST-DT-ELAB
                  ,RIS_BILA               =
                :RST-RIS-BILA
                                       :IND-RST-RIS-BILA
                  ,RIS_MAT                =
                :RST-RIS-MAT
                                       :IND-RST-RIS-MAT
                  ,INCR_X_RIVAL           =
                :RST-INCR-X-RIVAL
                                       :IND-RST-INCR-X-RIVAL
                  ,RPTO_PRE               =
                :RST-RPTO-PRE
                                       :IND-RST-RPTO-PRE
                  ,FRAZ_PRE_PP            =
                :RST-FRAZ-PRE-PP
                                       :IND-RST-FRAZ-PRE-PP
                  ,RIS_TOT                =
                :RST-RIS-TOT
                                       :IND-RST-RIS-TOT
                  ,RIS_SPE                =
                :RST-RIS-SPE
                                       :IND-RST-RIS-SPE
                  ,RIS_ABB                =
                :RST-RIS-ABB
                                       :IND-RST-RIS-ABB
                  ,RIS_BNSFDT             =
                :RST-RIS-BNSFDT
                                       :IND-RST-RIS-BNSFDT
                  ,RIS_SOPR               =
                :RST-RIS-SOPR
                                       :IND-RST-RIS-SOPR
                  ,RIS_INTEG_BAS_TEC      =
                :RST-RIS-INTEG-BAS-TEC
                                       :IND-RST-RIS-INTEG-BAS-TEC
                  ,RIS_INTEG_DECR_TS      =
                :RST-RIS-INTEG-DECR-TS
                                       :IND-RST-RIS-INTEG-DECR-TS
                  ,RIS_GAR_CASO_MOR       =
                :RST-RIS-GAR-CASO-MOR
                                       :IND-RST-RIS-GAR-CASO-MOR
                  ,RIS_ZIL                =
                :RST-RIS-ZIL
                                       :IND-RST-RIS-ZIL
                  ,RIS_FAIVL              =
                :RST-RIS-FAIVL
                                       :IND-RST-RIS-FAIVL
                  ,RIS_COS_AMMTZ          =
                :RST-RIS-COS-AMMTZ
                                       :IND-RST-RIS-COS-AMMTZ
                  ,RIS_SPE_FAIVL          =
                :RST-RIS-SPE-FAIVL
                                       :IND-RST-RIS-SPE-FAIVL
                  ,RIS_PREST_FAIVL        =
                :RST-RIS-PREST-FAIVL
                                       :IND-RST-RIS-PREST-FAIVL
                  ,RIS_COMPON_ASSVA       =
                :RST-RIS-COMPON-ASSVA
                                       :IND-RST-RIS-COMPON-ASSVA
                  ,ULT_COEFF_RIS          =
                :RST-ULT-COEFF-RIS
                                       :IND-RST-ULT-COEFF-RIS
                  ,ULT_COEFF_AGG_RIS      =
                :RST-ULT-COEFF-AGG-RIS
                                       :IND-RST-ULT-COEFF-AGG-RIS
                  ,RIS_ACQ                =
                :RST-RIS-ACQ
                                       :IND-RST-RIS-ACQ
                  ,RIS_UTI                =
                :RST-RIS-UTI
                                       :IND-RST-RIS-UTI
                  ,RIS_MAT_EFF            =
                :RST-RIS-MAT-EFF
                                       :IND-RST-RIS-MAT-EFF
                  ,RIS_RISTORNI_CAP       =
                :RST-RIS-RISTORNI-CAP
                                       :IND-RST-RIS-RISTORNI-CAP
                  ,RIS_TRM_BNS            =
                :RST-RIS-TRM-BNS
                                       :IND-RST-RIS-TRM-BNS
                  ,RIS_BNSRIC             =
                :RST-RIS-BNSRIC
                                       :IND-RST-RIS-BNSRIC
                  ,DS_RIGA                =
                :RST-DS-RIGA
                  ,DS_OPER_SQL            =
                :RST-DS-OPER-SQL
                  ,DS_VER                 =
                :RST-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :RST-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :RST-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :RST-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :RST-DS-STATO-ELAB
                  ,ID_TRCH_DI_GAR         =
                :RST-ID-TRCH-DI-GAR
                  ,COD_FND                =
                :RST-COD-FND
                                       :IND-RST-COD-FND
                  ,ID_POLI                =
                :RST-ID-POLI
                  ,ID_ADES                =
                :RST-ID-ADES
                  ,ID_GAR                 =
                :RST-ID-GAR
                  ,RIS_MIN_GARTO          =
                :RST-RIS-MIN-GARTO
                                       :IND-RST-RIS-MIN-GARTO
                  ,RIS_RSH_DFLT           =
                :RST-RIS-RSH-DFLT
                                       :IND-RST-RIS-RSH-DFLT
                  ,RIS_MOVI_NON_INVES     =
                :RST-RIS-MOVI-NON-INVES
                                       :IND-RST-RIS-MOVI-NON-INVES
                WHERE     DS_RIGA = :RST-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-RST
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-RST
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-RST
           INTO
                :RST-ID-RIS-DI-TRCH
               ,:RST-ID-MOVI-CRZ
               ,:RST-ID-MOVI-CHIU
                :IND-RST-ID-MOVI-CHIU
               ,:RST-DT-INI-EFF-DB
               ,:RST-DT-END-EFF-DB
               ,:RST-COD-COMP-ANIA
               ,:RST-TP-CALC-RIS
                :IND-RST-TP-CALC-RIS
               ,:RST-ULT-RM
                :IND-RST-ULT-RM
               ,:RST-DT-CALC-DB
                :IND-RST-DT-CALC
               ,:RST-DT-ELAB-DB
                :IND-RST-DT-ELAB
               ,:RST-RIS-BILA
                :IND-RST-RIS-BILA
               ,:RST-RIS-MAT
                :IND-RST-RIS-MAT
               ,:RST-INCR-X-RIVAL
                :IND-RST-INCR-X-RIVAL
               ,:RST-RPTO-PRE
                :IND-RST-RPTO-PRE
               ,:RST-FRAZ-PRE-PP
                :IND-RST-FRAZ-PRE-PP
               ,:RST-RIS-TOT
                :IND-RST-RIS-TOT
               ,:RST-RIS-SPE
                :IND-RST-RIS-SPE
               ,:RST-RIS-ABB
                :IND-RST-RIS-ABB
               ,:RST-RIS-BNSFDT
                :IND-RST-RIS-BNSFDT
               ,:RST-RIS-SOPR
                :IND-RST-RIS-SOPR
               ,:RST-RIS-INTEG-BAS-TEC
                :IND-RST-RIS-INTEG-BAS-TEC
               ,:RST-RIS-INTEG-DECR-TS
                :IND-RST-RIS-INTEG-DECR-TS
               ,:RST-RIS-GAR-CASO-MOR
                :IND-RST-RIS-GAR-CASO-MOR
               ,:RST-RIS-ZIL
                :IND-RST-RIS-ZIL
               ,:RST-RIS-FAIVL
                :IND-RST-RIS-FAIVL
               ,:RST-RIS-COS-AMMTZ
                :IND-RST-RIS-COS-AMMTZ
               ,:RST-RIS-SPE-FAIVL
                :IND-RST-RIS-SPE-FAIVL
               ,:RST-RIS-PREST-FAIVL
                :IND-RST-RIS-PREST-FAIVL
               ,:RST-RIS-COMPON-ASSVA
                :IND-RST-RIS-COMPON-ASSVA
               ,:RST-ULT-COEFF-RIS
                :IND-RST-ULT-COEFF-RIS
               ,:RST-ULT-COEFF-AGG-RIS
                :IND-RST-ULT-COEFF-AGG-RIS
               ,:RST-RIS-ACQ
                :IND-RST-RIS-ACQ
               ,:RST-RIS-UTI
                :IND-RST-RIS-UTI
               ,:RST-RIS-MAT-EFF
                :IND-RST-RIS-MAT-EFF
               ,:RST-RIS-RISTORNI-CAP
                :IND-RST-RIS-RISTORNI-CAP
               ,:RST-RIS-TRM-BNS
                :IND-RST-RIS-TRM-BNS
               ,:RST-RIS-BNSRIC
                :IND-RST-RIS-BNSRIC
               ,:RST-DS-RIGA
               ,:RST-DS-OPER-SQL
               ,:RST-DS-VER
               ,:RST-DS-TS-INI-CPTZ
               ,:RST-DS-TS-END-CPTZ
               ,:RST-DS-UTENTE
               ,:RST-DS-STATO-ELAB
               ,:RST-ID-TRCH-DI-GAR
               ,:RST-COD-FND
                :IND-RST-COD-FND
               ,:RST-ID-POLI
               ,:RST-ID-ADES
               ,:RST-ID-GAR
               ,:RST-RIS-MIN-GARTO
                :IND-RST-RIS-MIN-GARTO
               ,:RST-RIS-RSH-DFLT
                :IND-RST-RIS-RSH-DFLT
               ,:RST-RIS-MOVI-NON-INVES
                :IND-RST-RIS-MOVI-NON-INVES
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-EFF-RST CURSOR FOR
              SELECT
                     ID_RIS_DI_TRCH
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_CALC_RIS
                    ,ULT_RM
                    ,DT_CALC
                    ,DT_ELAB
                    ,RIS_BILA
                    ,RIS_MAT
                    ,INCR_X_RIVAL
                    ,RPTO_PRE
                    ,FRAZ_PRE_PP
                    ,RIS_TOT
                    ,RIS_SPE
                    ,RIS_ABB
                    ,RIS_BNSFDT
                    ,RIS_SOPR
                    ,RIS_INTEG_BAS_TEC
                    ,RIS_INTEG_DECR_TS
                    ,RIS_GAR_CASO_MOR
                    ,RIS_ZIL
                    ,RIS_FAIVL
                    ,RIS_COS_AMMTZ
                    ,RIS_SPE_FAIVL
                    ,RIS_PREST_FAIVL
                    ,RIS_COMPON_ASSVA
                    ,ULT_COEFF_RIS
                    ,ULT_COEFF_AGG_RIS
                    ,RIS_ACQ
                    ,RIS_UTI
                    ,RIS_MAT_EFF
                    ,RIS_RISTORNI_CAP
                    ,RIS_TRM_BNS
                    ,RIS_BNSRIC
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,ID_TRCH_DI_GAR
                    ,COD_FND
                    ,ID_POLI
                    ,ID_ADES
                    ,ID_GAR
                    ,RIS_MIN_GARTO
                    ,RIS_RSH_DFLT
                    ,RIS_MOVI_NON_INVES
              FROM RIS_DI_TRCH
              WHERE     ID_TRCH_DI_GAR = :RST-ID-TRCH-DI-GAR
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_RIS_DI_TRCH ASC

           END-EXEC.

       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_RIS_DI_TRCH
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_CALC_RIS
                ,ULT_RM
                ,DT_CALC
                ,DT_ELAB
                ,RIS_BILA
                ,RIS_MAT
                ,INCR_X_RIVAL
                ,RPTO_PRE
                ,FRAZ_PRE_PP
                ,RIS_TOT
                ,RIS_SPE
                ,RIS_ABB
                ,RIS_BNSFDT
                ,RIS_SOPR
                ,RIS_INTEG_BAS_TEC
                ,RIS_INTEG_DECR_TS
                ,RIS_GAR_CASO_MOR
                ,RIS_ZIL
                ,RIS_FAIVL
                ,RIS_COS_AMMTZ
                ,RIS_SPE_FAIVL
                ,RIS_PREST_FAIVL
                ,RIS_COMPON_ASSVA
                ,ULT_COEFF_RIS
                ,ULT_COEFF_AGG_RIS
                ,RIS_ACQ
                ,RIS_UTI
                ,RIS_MAT_EFF
                ,RIS_RISTORNI_CAP
                ,RIS_TRM_BNS
                ,RIS_BNSRIC
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,ID_TRCH_DI_GAR
                ,COD_FND
                ,ID_POLI
                ,ID_ADES
                ,ID_GAR
                ,RIS_MIN_GARTO
                ,RIS_RSH_DFLT
                ,RIS_MOVI_NON_INVES
             INTO
                :RST-ID-RIS-DI-TRCH
               ,:RST-ID-MOVI-CRZ
               ,:RST-ID-MOVI-CHIU
                :IND-RST-ID-MOVI-CHIU
               ,:RST-DT-INI-EFF-DB
               ,:RST-DT-END-EFF-DB
               ,:RST-COD-COMP-ANIA
               ,:RST-TP-CALC-RIS
                :IND-RST-TP-CALC-RIS
               ,:RST-ULT-RM
                :IND-RST-ULT-RM
               ,:RST-DT-CALC-DB
                :IND-RST-DT-CALC
               ,:RST-DT-ELAB-DB
                :IND-RST-DT-ELAB
               ,:RST-RIS-BILA
                :IND-RST-RIS-BILA
               ,:RST-RIS-MAT
                :IND-RST-RIS-MAT
               ,:RST-INCR-X-RIVAL
                :IND-RST-INCR-X-RIVAL
               ,:RST-RPTO-PRE
                :IND-RST-RPTO-PRE
               ,:RST-FRAZ-PRE-PP
                :IND-RST-FRAZ-PRE-PP
               ,:RST-RIS-TOT
                :IND-RST-RIS-TOT
               ,:RST-RIS-SPE
                :IND-RST-RIS-SPE
               ,:RST-RIS-ABB
                :IND-RST-RIS-ABB
               ,:RST-RIS-BNSFDT
                :IND-RST-RIS-BNSFDT
               ,:RST-RIS-SOPR
                :IND-RST-RIS-SOPR
               ,:RST-RIS-INTEG-BAS-TEC
                :IND-RST-RIS-INTEG-BAS-TEC
               ,:RST-RIS-INTEG-DECR-TS
                :IND-RST-RIS-INTEG-DECR-TS
               ,:RST-RIS-GAR-CASO-MOR
                :IND-RST-RIS-GAR-CASO-MOR
               ,:RST-RIS-ZIL
                :IND-RST-RIS-ZIL
               ,:RST-RIS-FAIVL
                :IND-RST-RIS-FAIVL
               ,:RST-RIS-COS-AMMTZ
                :IND-RST-RIS-COS-AMMTZ
               ,:RST-RIS-SPE-FAIVL
                :IND-RST-RIS-SPE-FAIVL
               ,:RST-RIS-PREST-FAIVL
                :IND-RST-RIS-PREST-FAIVL
               ,:RST-RIS-COMPON-ASSVA
                :IND-RST-RIS-COMPON-ASSVA
               ,:RST-ULT-COEFF-RIS
                :IND-RST-ULT-COEFF-RIS
               ,:RST-ULT-COEFF-AGG-RIS
                :IND-RST-ULT-COEFF-AGG-RIS
               ,:RST-RIS-ACQ
                :IND-RST-RIS-ACQ
               ,:RST-RIS-UTI
                :IND-RST-RIS-UTI
               ,:RST-RIS-MAT-EFF
                :IND-RST-RIS-MAT-EFF
               ,:RST-RIS-RISTORNI-CAP
                :IND-RST-RIS-RISTORNI-CAP
               ,:RST-RIS-TRM-BNS
                :IND-RST-RIS-TRM-BNS
               ,:RST-RIS-BNSRIC
                :IND-RST-RIS-BNSRIC
               ,:RST-DS-RIGA
               ,:RST-DS-OPER-SQL
               ,:RST-DS-VER
               ,:RST-DS-TS-INI-CPTZ
               ,:RST-DS-TS-END-CPTZ
               ,:RST-DS-UTENTE
               ,:RST-DS-STATO-ELAB
               ,:RST-ID-TRCH-DI-GAR
               ,:RST-COD-FND
                :IND-RST-COD-FND
               ,:RST-ID-POLI
               ,:RST-ID-ADES
               ,:RST-ID-GAR
               ,:RST-RIS-MIN-GARTO
                :IND-RST-RIS-MIN-GARTO
               ,:RST-RIS-RSH-DFLT
                :IND-RST-RIS-RSH-DFLT
               ,:RST-RIS-MOVI-NON-INVES
                :IND-RST-RIS-MOVI-NON-INVES
             FROM RIS_DI_TRCH
             WHERE     ID_TRCH_DI_GAR = :RST-ID-TRCH-DI-GAR
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           EXEC SQL
                OPEN C-IDP-EFF-RST
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           EXEC SQL
                CLOSE C-IDP-EFF-RST
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           EXEC SQL
                FETCH C-IDP-EFF-RST
           INTO
                :RST-ID-RIS-DI-TRCH
               ,:RST-ID-MOVI-CRZ
               ,:RST-ID-MOVI-CHIU
                :IND-RST-ID-MOVI-CHIU
               ,:RST-DT-INI-EFF-DB
               ,:RST-DT-END-EFF-DB
               ,:RST-COD-COMP-ANIA
               ,:RST-TP-CALC-RIS
                :IND-RST-TP-CALC-RIS
               ,:RST-ULT-RM
                :IND-RST-ULT-RM
               ,:RST-DT-CALC-DB
                :IND-RST-DT-CALC
               ,:RST-DT-ELAB-DB
                :IND-RST-DT-ELAB
               ,:RST-RIS-BILA
                :IND-RST-RIS-BILA
               ,:RST-RIS-MAT
                :IND-RST-RIS-MAT
               ,:RST-INCR-X-RIVAL
                :IND-RST-INCR-X-RIVAL
               ,:RST-RPTO-PRE
                :IND-RST-RPTO-PRE
               ,:RST-FRAZ-PRE-PP
                :IND-RST-FRAZ-PRE-PP
               ,:RST-RIS-TOT
                :IND-RST-RIS-TOT
               ,:RST-RIS-SPE
                :IND-RST-RIS-SPE
               ,:RST-RIS-ABB
                :IND-RST-RIS-ABB
               ,:RST-RIS-BNSFDT
                :IND-RST-RIS-BNSFDT
               ,:RST-RIS-SOPR
                :IND-RST-RIS-SOPR
               ,:RST-RIS-INTEG-BAS-TEC
                :IND-RST-RIS-INTEG-BAS-TEC
               ,:RST-RIS-INTEG-DECR-TS
                :IND-RST-RIS-INTEG-DECR-TS
               ,:RST-RIS-GAR-CASO-MOR
                :IND-RST-RIS-GAR-CASO-MOR
               ,:RST-RIS-ZIL
                :IND-RST-RIS-ZIL
               ,:RST-RIS-FAIVL
                :IND-RST-RIS-FAIVL
               ,:RST-RIS-COS-AMMTZ
                :IND-RST-RIS-COS-AMMTZ
               ,:RST-RIS-SPE-FAIVL
                :IND-RST-RIS-SPE-FAIVL
               ,:RST-RIS-PREST-FAIVL
                :IND-RST-RIS-PREST-FAIVL
               ,:RST-RIS-COMPON-ASSVA
                :IND-RST-RIS-COMPON-ASSVA
               ,:RST-ULT-COEFF-RIS
                :IND-RST-ULT-COEFF-RIS
               ,:RST-ULT-COEFF-AGG-RIS
                :IND-RST-ULT-COEFF-AGG-RIS
               ,:RST-RIS-ACQ
                :IND-RST-RIS-ACQ
               ,:RST-RIS-UTI
                :IND-RST-RIS-UTI
               ,:RST-RIS-MAT-EFF
                :IND-RST-RIS-MAT-EFF
               ,:RST-RIS-RISTORNI-CAP
                :IND-RST-RIS-RISTORNI-CAP
               ,:RST-RIS-TRM-BNS
                :IND-RST-RIS-TRM-BNS
               ,:RST-RIS-BNSRIC
                :IND-RST-RIS-BNSRIC
               ,:RST-DS-RIGA
               ,:RST-DS-OPER-SQL
               ,:RST-DS-VER
               ,:RST-DS-TS-INI-CPTZ
               ,:RST-DS-TS-END-CPTZ
               ,:RST-DS-UTENTE
               ,:RST-DS-STATO-ELAB
               ,:RST-ID-TRCH-DI-GAR
               ,:RST-COD-FND
                :IND-RST-COD-FND
               ,:RST-ID-POLI
               ,:RST-ID-ADES
               ,:RST-ID-GAR
               ,:RST-RIS-MIN-GARTO
                :IND-RST-RIS-MIN-GARTO
               ,:RST-RIS-RSH-DFLT
                :IND-RST-RIS-RSH-DFLT
               ,:RST-RIS-MOVI-NON-INVES
                :IND-RST-RIS-MOVI-NON-INVES
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_RIS_DI_TRCH
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_CALC_RIS
                ,ULT_RM
                ,DT_CALC
                ,DT_ELAB
                ,RIS_BILA
                ,RIS_MAT
                ,INCR_X_RIVAL
                ,RPTO_PRE
                ,FRAZ_PRE_PP
                ,RIS_TOT
                ,RIS_SPE
                ,RIS_ABB
                ,RIS_BNSFDT
                ,RIS_SOPR
                ,RIS_INTEG_BAS_TEC
                ,RIS_INTEG_DECR_TS
                ,RIS_GAR_CASO_MOR
                ,RIS_ZIL
                ,RIS_FAIVL
                ,RIS_COS_AMMTZ
                ,RIS_SPE_FAIVL
                ,RIS_PREST_FAIVL
                ,RIS_COMPON_ASSVA
                ,ULT_COEFF_RIS
                ,ULT_COEFF_AGG_RIS
                ,RIS_ACQ
                ,RIS_UTI
                ,RIS_MAT_EFF
                ,RIS_RISTORNI_CAP
                ,RIS_TRM_BNS
                ,RIS_BNSRIC
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,ID_TRCH_DI_GAR
                ,COD_FND
                ,ID_POLI
                ,ID_ADES
                ,ID_GAR
                ,RIS_MIN_GARTO
                ,RIS_RSH_DFLT
                ,RIS_MOVI_NON_INVES
             INTO
                :RST-ID-RIS-DI-TRCH
               ,:RST-ID-MOVI-CRZ
               ,:RST-ID-MOVI-CHIU
                :IND-RST-ID-MOVI-CHIU
               ,:RST-DT-INI-EFF-DB
               ,:RST-DT-END-EFF-DB
               ,:RST-COD-COMP-ANIA
               ,:RST-TP-CALC-RIS
                :IND-RST-TP-CALC-RIS
               ,:RST-ULT-RM
                :IND-RST-ULT-RM
               ,:RST-DT-CALC-DB
                :IND-RST-DT-CALC
               ,:RST-DT-ELAB-DB
                :IND-RST-DT-ELAB
               ,:RST-RIS-BILA
                :IND-RST-RIS-BILA
               ,:RST-RIS-MAT
                :IND-RST-RIS-MAT
               ,:RST-INCR-X-RIVAL
                :IND-RST-INCR-X-RIVAL
               ,:RST-RPTO-PRE
                :IND-RST-RPTO-PRE
               ,:RST-FRAZ-PRE-PP
                :IND-RST-FRAZ-PRE-PP
               ,:RST-RIS-TOT
                :IND-RST-RIS-TOT
               ,:RST-RIS-SPE
                :IND-RST-RIS-SPE
               ,:RST-RIS-ABB
                :IND-RST-RIS-ABB
               ,:RST-RIS-BNSFDT
                :IND-RST-RIS-BNSFDT
               ,:RST-RIS-SOPR
                :IND-RST-RIS-SOPR
               ,:RST-RIS-INTEG-BAS-TEC
                :IND-RST-RIS-INTEG-BAS-TEC
               ,:RST-RIS-INTEG-DECR-TS
                :IND-RST-RIS-INTEG-DECR-TS
               ,:RST-RIS-GAR-CASO-MOR
                :IND-RST-RIS-GAR-CASO-MOR
               ,:RST-RIS-ZIL
                :IND-RST-RIS-ZIL
               ,:RST-RIS-FAIVL
                :IND-RST-RIS-FAIVL
               ,:RST-RIS-COS-AMMTZ
                :IND-RST-RIS-COS-AMMTZ
               ,:RST-RIS-SPE-FAIVL
                :IND-RST-RIS-SPE-FAIVL
               ,:RST-RIS-PREST-FAIVL
                :IND-RST-RIS-PREST-FAIVL
               ,:RST-RIS-COMPON-ASSVA
                :IND-RST-RIS-COMPON-ASSVA
               ,:RST-ULT-COEFF-RIS
                :IND-RST-ULT-COEFF-RIS
               ,:RST-ULT-COEFF-AGG-RIS
                :IND-RST-ULT-COEFF-AGG-RIS
               ,:RST-RIS-ACQ
                :IND-RST-RIS-ACQ
               ,:RST-RIS-UTI
                :IND-RST-RIS-UTI
               ,:RST-RIS-MAT-EFF
                :IND-RST-RIS-MAT-EFF
               ,:RST-RIS-RISTORNI-CAP
                :IND-RST-RIS-RISTORNI-CAP
               ,:RST-RIS-TRM-BNS
                :IND-RST-RIS-TRM-BNS
               ,:RST-RIS-BNSRIC
                :IND-RST-RIS-BNSRIC
               ,:RST-DS-RIGA
               ,:RST-DS-OPER-SQL
               ,:RST-DS-VER
               ,:RST-DS-TS-INI-CPTZ
               ,:RST-DS-TS-END-CPTZ
               ,:RST-DS-UTENTE
               ,:RST-DS-STATO-ELAB
               ,:RST-ID-TRCH-DI-GAR
               ,:RST-COD-FND
                :IND-RST-COD-FND
               ,:RST-ID-POLI
               ,:RST-ID-ADES
               ,:RST-ID-GAR
               ,:RST-RIS-MIN-GARTO
                :IND-RST-RIS-MIN-GARTO
               ,:RST-RIS-RSH-DFLT
                :IND-RST-RIS-RSH-DFLT
               ,:RST-RIS-MOVI-NON-INVES
                :IND-RST-RIS-MOVI-NON-INVES
             FROM RIS_DI_TRCH
             WHERE     ID_RIS_DI_TRCH = :RST-ID-RIS-DI-TRCH
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-CPZ-RST CURSOR FOR
              SELECT
                     ID_RIS_DI_TRCH
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_CALC_RIS
                    ,ULT_RM
                    ,DT_CALC
                    ,DT_ELAB
                    ,RIS_BILA
                    ,RIS_MAT
                    ,INCR_X_RIVAL
                    ,RPTO_PRE
                    ,FRAZ_PRE_PP
                    ,RIS_TOT
                    ,RIS_SPE
                    ,RIS_ABB
                    ,RIS_BNSFDT
                    ,RIS_SOPR
                    ,RIS_INTEG_BAS_TEC
                    ,RIS_INTEG_DECR_TS
                    ,RIS_GAR_CASO_MOR
                    ,RIS_ZIL
                    ,RIS_FAIVL
                    ,RIS_COS_AMMTZ
                    ,RIS_SPE_FAIVL
                    ,RIS_PREST_FAIVL
                    ,RIS_COMPON_ASSVA
                    ,ULT_COEFF_RIS
                    ,ULT_COEFF_AGG_RIS
                    ,RIS_ACQ
                    ,RIS_UTI
                    ,RIS_MAT_EFF
                    ,RIS_RISTORNI_CAP
                    ,RIS_TRM_BNS
                    ,RIS_BNSRIC
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,ID_TRCH_DI_GAR
                    ,COD_FND
                    ,ID_POLI
                    ,ID_ADES
                    ,ID_GAR
                    ,RIS_MIN_GARTO
                    ,RIS_RSH_DFLT
                    ,RIS_MOVI_NON_INVES
              FROM RIS_DI_TRCH
              WHERE     ID_TRCH_DI_GAR = :RST-ID-TRCH-DI-GAR
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_RIS_DI_TRCH ASC

           END-EXEC.

       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_RIS_DI_TRCH
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_CALC_RIS
                ,ULT_RM
                ,DT_CALC
                ,DT_ELAB
                ,RIS_BILA
                ,RIS_MAT
                ,INCR_X_RIVAL
                ,RPTO_PRE
                ,FRAZ_PRE_PP
                ,RIS_TOT
                ,RIS_SPE
                ,RIS_ABB
                ,RIS_BNSFDT
                ,RIS_SOPR
                ,RIS_INTEG_BAS_TEC
                ,RIS_INTEG_DECR_TS
                ,RIS_GAR_CASO_MOR
                ,RIS_ZIL
                ,RIS_FAIVL
                ,RIS_COS_AMMTZ
                ,RIS_SPE_FAIVL
                ,RIS_PREST_FAIVL
                ,RIS_COMPON_ASSVA
                ,ULT_COEFF_RIS
                ,ULT_COEFF_AGG_RIS
                ,RIS_ACQ
                ,RIS_UTI
                ,RIS_MAT_EFF
                ,RIS_RISTORNI_CAP
                ,RIS_TRM_BNS
                ,RIS_BNSRIC
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,ID_TRCH_DI_GAR
                ,COD_FND
                ,ID_POLI
                ,ID_ADES
                ,ID_GAR
                ,RIS_MIN_GARTO
                ,RIS_RSH_DFLT
                ,RIS_MOVI_NON_INVES
             INTO
                :RST-ID-RIS-DI-TRCH
               ,:RST-ID-MOVI-CRZ
               ,:RST-ID-MOVI-CHIU
                :IND-RST-ID-MOVI-CHIU
               ,:RST-DT-INI-EFF-DB
               ,:RST-DT-END-EFF-DB
               ,:RST-COD-COMP-ANIA
               ,:RST-TP-CALC-RIS
                :IND-RST-TP-CALC-RIS
               ,:RST-ULT-RM
                :IND-RST-ULT-RM
               ,:RST-DT-CALC-DB
                :IND-RST-DT-CALC
               ,:RST-DT-ELAB-DB
                :IND-RST-DT-ELAB
               ,:RST-RIS-BILA
                :IND-RST-RIS-BILA
               ,:RST-RIS-MAT
                :IND-RST-RIS-MAT
               ,:RST-INCR-X-RIVAL
                :IND-RST-INCR-X-RIVAL
               ,:RST-RPTO-PRE
                :IND-RST-RPTO-PRE
               ,:RST-FRAZ-PRE-PP
                :IND-RST-FRAZ-PRE-PP
               ,:RST-RIS-TOT
                :IND-RST-RIS-TOT
               ,:RST-RIS-SPE
                :IND-RST-RIS-SPE
               ,:RST-RIS-ABB
                :IND-RST-RIS-ABB
               ,:RST-RIS-BNSFDT
                :IND-RST-RIS-BNSFDT
               ,:RST-RIS-SOPR
                :IND-RST-RIS-SOPR
               ,:RST-RIS-INTEG-BAS-TEC
                :IND-RST-RIS-INTEG-BAS-TEC
               ,:RST-RIS-INTEG-DECR-TS
                :IND-RST-RIS-INTEG-DECR-TS
               ,:RST-RIS-GAR-CASO-MOR
                :IND-RST-RIS-GAR-CASO-MOR
               ,:RST-RIS-ZIL
                :IND-RST-RIS-ZIL
               ,:RST-RIS-FAIVL
                :IND-RST-RIS-FAIVL
               ,:RST-RIS-COS-AMMTZ
                :IND-RST-RIS-COS-AMMTZ
               ,:RST-RIS-SPE-FAIVL
                :IND-RST-RIS-SPE-FAIVL
               ,:RST-RIS-PREST-FAIVL
                :IND-RST-RIS-PREST-FAIVL
               ,:RST-RIS-COMPON-ASSVA
                :IND-RST-RIS-COMPON-ASSVA
               ,:RST-ULT-COEFF-RIS
                :IND-RST-ULT-COEFF-RIS
               ,:RST-ULT-COEFF-AGG-RIS
                :IND-RST-ULT-COEFF-AGG-RIS
               ,:RST-RIS-ACQ
                :IND-RST-RIS-ACQ
               ,:RST-RIS-UTI
                :IND-RST-RIS-UTI
               ,:RST-RIS-MAT-EFF
                :IND-RST-RIS-MAT-EFF
               ,:RST-RIS-RISTORNI-CAP
                :IND-RST-RIS-RISTORNI-CAP
               ,:RST-RIS-TRM-BNS
                :IND-RST-RIS-TRM-BNS
               ,:RST-RIS-BNSRIC
                :IND-RST-RIS-BNSRIC
               ,:RST-DS-RIGA
               ,:RST-DS-OPER-SQL
               ,:RST-DS-VER
               ,:RST-DS-TS-INI-CPTZ
               ,:RST-DS-TS-END-CPTZ
               ,:RST-DS-UTENTE
               ,:RST-DS-STATO-ELAB
               ,:RST-ID-TRCH-DI-GAR
               ,:RST-COD-FND
                :IND-RST-COD-FND
               ,:RST-ID-POLI
               ,:RST-ID-ADES
               ,:RST-ID-GAR
               ,:RST-RIS-MIN-GARTO
                :IND-RST-RIS-MIN-GARTO
               ,:RST-RIS-RSH-DFLT
                :IND-RST-RIS-RSH-DFLT
               ,:RST-RIS-MOVI-NON-INVES
                :IND-RST-RIS-MOVI-NON-INVES
             FROM RIS_DI_TRCH
             WHERE     ID_TRCH_DI_GAR = :RST-ID-TRCH-DI-GAR
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           EXEC SQL
                OPEN C-IDP-CPZ-RST
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           EXEC SQL
                CLOSE C-IDP-CPZ-RST
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           EXEC SQL
                FETCH C-IDP-CPZ-RST
           INTO
                :RST-ID-RIS-DI-TRCH
               ,:RST-ID-MOVI-CRZ
               ,:RST-ID-MOVI-CHIU
                :IND-RST-ID-MOVI-CHIU
               ,:RST-DT-INI-EFF-DB
               ,:RST-DT-END-EFF-DB
               ,:RST-COD-COMP-ANIA
               ,:RST-TP-CALC-RIS
                :IND-RST-TP-CALC-RIS
               ,:RST-ULT-RM
                :IND-RST-ULT-RM
               ,:RST-DT-CALC-DB
                :IND-RST-DT-CALC
               ,:RST-DT-ELAB-DB
                :IND-RST-DT-ELAB
               ,:RST-RIS-BILA
                :IND-RST-RIS-BILA
               ,:RST-RIS-MAT
                :IND-RST-RIS-MAT
               ,:RST-INCR-X-RIVAL
                :IND-RST-INCR-X-RIVAL
               ,:RST-RPTO-PRE
                :IND-RST-RPTO-PRE
               ,:RST-FRAZ-PRE-PP
                :IND-RST-FRAZ-PRE-PP
               ,:RST-RIS-TOT
                :IND-RST-RIS-TOT
               ,:RST-RIS-SPE
                :IND-RST-RIS-SPE
               ,:RST-RIS-ABB
                :IND-RST-RIS-ABB
               ,:RST-RIS-BNSFDT
                :IND-RST-RIS-BNSFDT
               ,:RST-RIS-SOPR
                :IND-RST-RIS-SOPR
               ,:RST-RIS-INTEG-BAS-TEC
                :IND-RST-RIS-INTEG-BAS-TEC
               ,:RST-RIS-INTEG-DECR-TS
                :IND-RST-RIS-INTEG-DECR-TS
               ,:RST-RIS-GAR-CASO-MOR
                :IND-RST-RIS-GAR-CASO-MOR
               ,:RST-RIS-ZIL
                :IND-RST-RIS-ZIL
               ,:RST-RIS-FAIVL
                :IND-RST-RIS-FAIVL
               ,:RST-RIS-COS-AMMTZ
                :IND-RST-RIS-COS-AMMTZ
               ,:RST-RIS-SPE-FAIVL
                :IND-RST-RIS-SPE-FAIVL
               ,:RST-RIS-PREST-FAIVL
                :IND-RST-RIS-PREST-FAIVL
               ,:RST-RIS-COMPON-ASSVA
                :IND-RST-RIS-COMPON-ASSVA
               ,:RST-ULT-COEFF-RIS
                :IND-RST-ULT-COEFF-RIS
               ,:RST-ULT-COEFF-AGG-RIS
                :IND-RST-ULT-COEFF-AGG-RIS
               ,:RST-RIS-ACQ
                :IND-RST-RIS-ACQ
               ,:RST-RIS-UTI
                :IND-RST-RIS-UTI
               ,:RST-RIS-MAT-EFF
                :IND-RST-RIS-MAT-EFF
               ,:RST-RIS-RISTORNI-CAP
                :IND-RST-RIS-RISTORNI-CAP
               ,:RST-RIS-TRM-BNS
                :IND-RST-RIS-TRM-BNS
               ,:RST-RIS-BNSRIC
                :IND-RST-RIS-BNSRIC
               ,:RST-DS-RIGA
               ,:RST-DS-OPER-SQL
               ,:RST-DS-VER
               ,:RST-DS-TS-INI-CPTZ
               ,:RST-DS-TS-END-CPTZ
               ,:RST-DS-UTENTE
               ,:RST-DS-STATO-ELAB
               ,:RST-ID-TRCH-DI-GAR
               ,:RST-COD-FND
                :IND-RST-COD-FND
               ,:RST-ID-POLI
               ,:RST-ID-ADES
               ,:RST-ID-GAR
               ,:RST-RIS-MIN-GARTO
                :IND-RST-RIS-MIN-GARTO
               ,:RST-RIS-RSH-DFLT
                :IND-RST-RIS-RSH-DFLT
               ,:RST-RIS-MOVI-NON-INVES
                :IND-RST-RIS-MOVI-NON-INVES
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-RST-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO RST-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-RST-TP-CALC-RIS = -1
              MOVE HIGH-VALUES TO RST-TP-CALC-RIS-NULL
           END-IF
           IF IND-RST-ULT-RM = -1
              MOVE HIGH-VALUES TO RST-ULT-RM-NULL
           END-IF
           IF IND-RST-DT-CALC = -1
              MOVE HIGH-VALUES TO RST-DT-CALC-NULL
           END-IF
           IF IND-RST-DT-ELAB = -1
              MOVE HIGH-VALUES TO RST-DT-ELAB-NULL
           END-IF
           IF IND-RST-RIS-BILA = -1
              MOVE HIGH-VALUES TO RST-RIS-BILA-NULL
           END-IF
           IF IND-RST-RIS-MAT = -1
              MOVE HIGH-VALUES TO RST-RIS-MAT-NULL
           END-IF
           IF IND-RST-INCR-X-RIVAL = -1
              MOVE HIGH-VALUES TO RST-INCR-X-RIVAL-NULL
           END-IF
           IF IND-RST-RPTO-PRE = -1
              MOVE HIGH-VALUES TO RST-RPTO-PRE-NULL
           END-IF
           IF IND-RST-FRAZ-PRE-PP = -1
              MOVE HIGH-VALUES TO RST-FRAZ-PRE-PP-NULL
           END-IF
           IF IND-RST-RIS-TOT = -1
              MOVE HIGH-VALUES TO RST-RIS-TOT-NULL
           END-IF
           IF IND-RST-RIS-SPE = -1
              MOVE HIGH-VALUES TO RST-RIS-SPE-NULL
           END-IF
           IF IND-RST-RIS-ABB = -1
              MOVE HIGH-VALUES TO RST-RIS-ABB-NULL
           END-IF
           IF IND-RST-RIS-BNSFDT = -1
              MOVE HIGH-VALUES TO RST-RIS-BNSFDT-NULL
           END-IF
           IF IND-RST-RIS-SOPR = -1
              MOVE HIGH-VALUES TO RST-RIS-SOPR-NULL
           END-IF
           IF IND-RST-RIS-INTEG-BAS-TEC = -1
              MOVE HIGH-VALUES TO RST-RIS-INTEG-BAS-TEC-NULL
           END-IF
           IF IND-RST-RIS-INTEG-DECR-TS = -1
              MOVE HIGH-VALUES TO RST-RIS-INTEG-DECR-TS-NULL
           END-IF
           IF IND-RST-RIS-GAR-CASO-MOR = -1
              MOVE HIGH-VALUES TO RST-RIS-GAR-CASO-MOR-NULL
           END-IF
           IF IND-RST-RIS-ZIL = -1
              MOVE HIGH-VALUES TO RST-RIS-ZIL-NULL
           END-IF
           IF IND-RST-RIS-FAIVL = -1
              MOVE HIGH-VALUES TO RST-RIS-FAIVL-NULL
           END-IF
           IF IND-RST-RIS-COS-AMMTZ = -1
              MOVE HIGH-VALUES TO RST-RIS-COS-AMMTZ-NULL
           END-IF
           IF IND-RST-RIS-SPE-FAIVL = -1
              MOVE HIGH-VALUES TO RST-RIS-SPE-FAIVL-NULL
           END-IF
           IF IND-RST-RIS-PREST-FAIVL = -1
              MOVE HIGH-VALUES TO RST-RIS-PREST-FAIVL-NULL
           END-IF
           IF IND-RST-RIS-COMPON-ASSVA = -1
              MOVE HIGH-VALUES TO RST-RIS-COMPON-ASSVA-NULL
           END-IF
           IF IND-RST-ULT-COEFF-RIS = -1
              MOVE HIGH-VALUES TO RST-ULT-COEFF-RIS-NULL
           END-IF
           IF IND-RST-ULT-COEFF-AGG-RIS = -1
              MOVE HIGH-VALUES TO RST-ULT-COEFF-AGG-RIS-NULL
           END-IF
           IF IND-RST-RIS-ACQ = -1
              MOVE HIGH-VALUES TO RST-RIS-ACQ-NULL
           END-IF
           IF IND-RST-RIS-UTI = -1
              MOVE HIGH-VALUES TO RST-RIS-UTI-NULL
           END-IF
           IF IND-RST-RIS-MAT-EFF = -1
              MOVE HIGH-VALUES TO RST-RIS-MAT-EFF-NULL
           END-IF
           IF IND-RST-RIS-RISTORNI-CAP = -1
              MOVE HIGH-VALUES TO RST-RIS-RISTORNI-CAP-NULL
           END-IF
           IF IND-RST-RIS-TRM-BNS = -1
              MOVE HIGH-VALUES TO RST-RIS-TRM-BNS-NULL
           END-IF
           IF IND-RST-RIS-BNSRIC = -1
              MOVE HIGH-VALUES TO RST-RIS-BNSRIC-NULL
           END-IF
           IF IND-RST-COD-FND = -1
              MOVE HIGH-VALUES TO RST-COD-FND-NULL
           END-IF
           IF IND-RST-RIS-MIN-GARTO = -1
              MOVE HIGH-VALUES TO RST-RIS-MIN-GARTO-NULL
           END-IF
           IF IND-RST-RIS-RSH-DFLT = -1
              MOVE HIGH-VALUES TO RST-RIS-RSH-DFLT-NULL
           END-IF
           IF IND-RST-RIS-MOVI-NON-INVES = -1
              MOVE HIGH-VALUES TO RST-RIS-MOVI-NON-INVES-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO RST-DS-OPER-SQL
           MOVE 0                   TO RST-DS-VER
           MOVE IDSV0003-USER-NAME TO RST-DS-UTENTE
           MOVE '1'                   TO RST-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO RST-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO RST-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF RST-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-RST-ID-MOVI-CHIU
           END-IF
           IF RST-TP-CALC-RIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-TP-CALC-RIS
           ELSE
              MOVE 0 TO IND-RST-TP-CALC-RIS
           END-IF
           IF RST-ULT-RM-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-ULT-RM
           ELSE
              MOVE 0 TO IND-RST-ULT-RM
           END-IF
           IF RST-DT-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-DT-CALC
           ELSE
              MOVE 0 TO IND-RST-DT-CALC
           END-IF
           IF RST-DT-ELAB-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-DT-ELAB
           ELSE
              MOVE 0 TO IND-RST-DT-ELAB
           END-IF
           IF RST-RIS-BILA-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-BILA
           ELSE
              MOVE 0 TO IND-RST-RIS-BILA
           END-IF
           IF RST-RIS-MAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-MAT
           ELSE
              MOVE 0 TO IND-RST-RIS-MAT
           END-IF
           IF RST-INCR-X-RIVAL-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-INCR-X-RIVAL
           ELSE
              MOVE 0 TO IND-RST-INCR-X-RIVAL
           END-IF
           IF RST-RPTO-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RPTO-PRE
           ELSE
              MOVE 0 TO IND-RST-RPTO-PRE
           END-IF
           IF RST-FRAZ-PRE-PP-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-FRAZ-PRE-PP
           ELSE
              MOVE 0 TO IND-RST-FRAZ-PRE-PP
           END-IF
           IF RST-RIS-TOT-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-TOT
           ELSE
              MOVE 0 TO IND-RST-RIS-TOT
           END-IF
           IF RST-RIS-SPE-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-SPE
           ELSE
              MOVE 0 TO IND-RST-RIS-SPE
           END-IF
           IF RST-RIS-ABB-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-ABB
           ELSE
              MOVE 0 TO IND-RST-RIS-ABB
           END-IF
           IF RST-RIS-BNSFDT-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-BNSFDT
           ELSE
              MOVE 0 TO IND-RST-RIS-BNSFDT
           END-IF
           IF RST-RIS-SOPR-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-SOPR
           ELSE
              MOVE 0 TO IND-RST-RIS-SOPR
           END-IF
           IF RST-RIS-INTEG-BAS-TEC-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-INTEG-BAS-TEC
           ELSE
              MOVE 0 TO IND-RST-RIS-INTEG-BAS-TEC
           END-IF
           IF RST-RIS-INTEG-DECR-TS-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-INTEG-DECR-TS
           ELSE
              MOVE 0 TO IND-RST-RIS-INTEG-DECR-TS
           END-IF
           IF RST-RIS-GAR-CASO-MOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-GAR-CASO-MOR
           ELSE
              MOVE 0 TO IND-RST-RIS-GAR-CASO-MOR
           END-IF
           IF RST-RIS-ZIL-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-ZIL
           ELSE
              MOVE 0 TO IND-RST-RIS-ZIL
           END-IF
           IF RST-RIS-FAIVL-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-FAIVL
           ELSE
              MOVE 0 TO IND-RST-RIS-FAIVL
           END-IF
           IF RST-RIS-COS-AMMTZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-COS-AMMTZ
           ELSE
              MOVE 0 TO IND-RST-RIS-COS-AMMTZ
           END-IF
           IF RST-RIS-SPE-FAIVL-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-SPE-FAIVL
           ELSE
              MOVE 0 TO IND-RST-RIS-SPE-FAIVL
           END-IF
           IF RST-RIS-PREST-FAIVL-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-PREST-FAIVL
           ELSE
              MOVE 0 TO IND-RST-RIS-PREST-FAIVL
           END-IF
           IF RST-RIS-COMPON-ASSVA-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-COMPON-ASSVA
           ELSE
              MOVE 0 TO IND-RST-RIS-COMPON-ASSVA
           END-IF
           IF RST-ULT-COEFF-RIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-ULT-COEFF-RIS
           ELSE
              MOVE 0 TO IND-RST-ULT-COEFF-RIS
           END-IF
           IF RST-ULT-COEFF-AGG-RIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-ULT-COEFF-AGG-RIS
           ELSE
              MOVE 0 TO IND-RST-ULT-COEFF-AGG-RIS
           END-IF
           IF RST-RIS-ACQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-ACQ
           ELSE
              MOVE 0 TO IND-RST-RIS-ACQ
           END-IF
           IF RST-RIS-UTI-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-UTI
           ELSE
              MOVE 0 TO IND-RST-RIS-UTI
           END-IF
           IF RST-RIS-MAT-EFF-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-MAT-EFF
           ELSE
              MOVE 0 TO IND-RST-RIS-MAT-EFF
           END-IF
           IF RST-RIS-RISTORNI-CAP-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-RISTORNI-CAP
           ELSE
              MOVE 0 TO IND-RST-RIS-RISTORNI-CAP
           END-IF
           IF RST-RIS-TRM-BNS-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-TRM-BNS
           ELSE
              MOVE 0 TO IND-RST-RIS-TRM-BNS
           END-IF
           IF RST-RIS-BNSRIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-BNSRIC
           ELSE
              MOVE 0 TO IND-RST-RIS-BNSRIC
           END-IF
           IF RST-COD-FND-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-COD-FND
           ELSE
              MOVE 0 TO IND-RST-COD-FND
           END-IF
           IF RST-RIS-MIN-GARTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-MIN-GARTO
           ELSE
              MOVE 0 TO IND-RST-RIS-MIN-GARTO
           END-IF
           IF RST-RIS-RSH-DFLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-RSH-DFLT
           ELSE
              MOVE 0 TO IND-RST-RIS-RSH-DFLT
           END-IF
           IF RST-RIS-MOVI-NON-INVES-NULL = HIGH-VALUES
              MOVE -1 TO IND-RST-RIS-MOVI-NON-INVES
           ELSE
              MOVE 0 TO IND-RST-RIS-MOVI-NON-INVES
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : RST-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE RIS-DI-TRCH TO WS-BUFFER-TABLE.

           MOVE RST-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO RST-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO RST-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO RST-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO RST-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO RST-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO RST-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO RST-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.

           MOVE RIS-DI-TRCH TO WS-BUFFER-TABLE.

           MOVE RST-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.

       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO RIS-DI-TRCH.

           MOVE WS-ID-MOVI-CRZ  TO RST-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO RST-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO RST-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO RST-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO RST-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO RST-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO RST-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE RST-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO RST-DT-INI-EFF-DB
           MOVE RST-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO RST-DT-END-EFF-DB
           IF IND-RST-DT-CALC = 0
               MOVE RST-DT-CALC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO RST-DT-CALC-DB
           END-IF
           IF IND-RST-DT-ELAB = 0
               MOVE RST-DT-ELAB TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO RST-DT-ELAB-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE RST-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO RST-DT-INI-EFF
           MOVE RST-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO RST-DT-END-EFF
           IF IND-RST-DT-CALC = 0
               MOVE RST-DT-CALC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO RST-DT-CALC
           END-IF
           IF IND-RST-DT-ELAB = 0
               MOVE RST-DT-ELAB-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO RST-DT-ELAB
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

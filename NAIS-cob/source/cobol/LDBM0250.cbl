       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LDBM0250.
       AUTHOR.        A.I.I.S
       DATE-WRITTEN.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : SERVIZIO ESTRAZIONE OCCORRENZE              *
      *                   DA TABELLE GUIDA RICHIAMATO DAL BATCH EXEC. *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2         PIC X(300)  VALUE SPACES.
       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.

           EXEC SQL INCLUDE IDSV0010 END-EXEC.


      *---------------------------------------------------------------*
      * STRUTTURA WHERE CONDITION
      *---------------------------------------------------------------*

      * 01 BUFFER-PRENOTAZIONE.
      *    05 WS-ID-RICH          PIC 9(09).
      *    05 WS-STAT-RIS         PIC X(02).
      *    05 WS-IB-RICH          PIC X(40).
      *    05 WS-ID-ESTRAZ        PIC 9(09).
      *    05 WS-DT-RISERVA       PIC 9(08).
      *    05 WS-DT-PTF           PIC 9(08).
      *    05 WS-TP-FRM-ASSVA     PIC X(02).
      *    05 WS-RAMO-BILA        PIC X(12).
      *    05 WS-COD-PROD         PIC X(12).
      *    05 WS-IB-POLI-FIRST    PIC X(40).
      *    05 WS-IB-POLI-LAST     PIC X(40).
      *    05 WS-IB-ADE-FIRST     PIC X(40).
      *    05 WS-IB-ADE-LAST      PIC X(40).
      *    05 FILLER              PIC X(39).

           EXEC SQL INCLUDE LLBO0261 END-EXEC.


      *
       01 WS-TP-FRM-ASSVA1              PIC X(02).
       01 WS-TP-FRM-ASSVA2              PIC X(02).
       01 WS-TIMESTAMP                  PIC X(18).
       01 WS-TIMESTAMP-NUM REDEFINES WS-TIMESTAMP PIC 9(18).
      *
       01 WCOM-DT-SYS                     PIC 9(08).
       01 WCOM-TIMESTAMP-SYS              PIC 9(18).

       01 WS-ORA-SISTEMA-9.
          05 HH-SYS-9                     PIC 9(002) VALUE ZEROES.
          05 MI-SYS-9                     PIC 9(002) VALUE ZEROES.
          05 SS-SYS-9                     PIC 9(002) VALUE ZEROES.

       01 WS-DT-N.
          05 WS-DT-N-AA                   PIC 9(04).
          05 WS-DT-N-MM                   PIC 9(02).
          05 WS-DT-N-GG                   PIC 9(02).
       01 WS-DT-COMP                      REDEFINES
             WS-DT-N                      PIC S9(08) COMP-3.

       01 WS-DT-X.
          05 WS-DT-X-AA                   PIC 9(04).
          05 FILLER                       PIC X(01)  VALUE '-'.
          05 WS-DT-X-MM                   PIC 9(02).
          05 FILLER                       PIC X(01)  VALUE '-'.
          05 WS-DT-X-GG                   PIC 9(02).

       01 WS-DT-PTF-X                     PIC X(10).
       01 WS-DT-TS-PTF                    PIC S9(18) COMP-3.
       01 WS-DT-TS-PTF-PRE                PIC S9(18) COMP-3.

       01 WS-DATA-COMPETENZA.
          03 WS-DATA-COMPETENZA-X.
             05 WS-DT-COMPETENZA-8           PIC 9(08).
             05 WS-LT-40                     PIC X(02).
             05 WS-LT-ORA-COMPETENZA-8       PIC X(08).
          03 WS-DATA-COMPETENZA-9            REDEFINES
             WS-DATA-COMPETENZA-X            PIC 9(18).

       01 WK-LABEL-ERR                       PIC X(50) VALUE SPACES.
      *----------------------------------------------------------------*
      *   FLAG e SWITCH
      *----------------------------------------------------------------*
VSTEF  01  WK-TRACE-ON                     PIC X(001) VALUE 'Y'.
           88 SI-TRACE                                VALUE 'S'.
           88 NO-TRACE                                VALUE 'N'.

       01  FLAG-ACCESSO-X-RANGE            PIC X(001) VALUE SPACES.
           88 ACCESSO-X-RANGE-SI                      VALUE 'S'.
           88 ACCESSO-X-RANGE-NO                      VALUE 'N'.

      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBVADE2 END-EXEC.
           EXEC SQL INCLUDE IDBVADE3 END-EXEC.
           EXEC SQL INCLUDE IDBVPOL2 END-EXEC.
           EXEC SQL INCLUDE IDBVPOL3 END-EXEC.
           EXEC SQL INCLUDE IDBVSTB2 END-EXEC.
           EXEC SQL INCLUDE IDBVSTB3 END-EXEC.
           EXEC SQL INCLUDE IDBVMFZ1 END-EXEC.
           EXEC SQL INCLUDE IDBVMFZ2 END-EXEC.
           EXEC SQL INCLUDE IDBVMFZ3 END-EXEC.
      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.
      *-----------------------------------------------------------------
      *  CAMPI DI ESITO, AREE DI SERVIZIO
      *-----------------------------------------------------------------

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

      *-----------------------------------------------------------------
      * AREA STATI
      *-----------------------------------------------------------------

           EXEC SQL INCLUDE IABV0002 END-EXEC.

      *-----------------------------------------------------------------
      * DCLGEN PORTAFOGLIO UTILIZZATE DAL GUIDE SERVICE
      *-----------------------------------------------------------------

           EXEC SQL INCLUDE IDBVADE1 END-EXEC.
           EXEC SQL INCLUDE IDBVPOL1 END-EXEC.
           EXEC SQL INCLUDE IDBVSTB1 END-EXEC.

      *-----------------------------------------------------------------
      * DCLGEN PORTAFOGLIO UTILIZZATE DAL GUIDE SERVICE
      *-----------------------------------------------------------------

           EXEC SQL INCLUDE LLBV0269 END-EXEC.

      ******************************************************************

       PROCEDURE DIVISION         USING IDSV0003
                                        IABV0002
                                        ADES
                                        POLI
                                        STAT-OGG-BUS
                                        LLBV0269.

           PERFORM A000-INIZIO                  THRU A000-EX.

           PERFORM A025-CNTL-INPUT              THRU A025-EX.

           PERFORM A040-CARICA-WHERE-CONDITION  THRU A040-EX.

           PERFORM A300-ELABORA                 THRU A300-EX

           PERFORM A350-CTRL-COMMIT             THRU A350-EX.

           PERFORM A400-FINE                    THRU A400-EX.

           GOBACK.

      ******************************************************************
       A000-INIZIO.

           MOVE 'LDBM0250'              TO   IDSV0003-COD-SERVIZIO-BE.
           MOVE 'ADES'                  TO   IDSV0003-NOME-TABELLA.

           MOVE '00'                    TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                  TO   IDSV0003-SQLCODE
                                             IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                  TO   IDSV0003-DESCRIZ-ERR-DB2
                                             IDSV0003-KEY-TABELLA.

           SET ACCESSO-X-RANGE-NO       TO   TRUE.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

      ******************************************************************
       A025-CNTL-INPUT.

           IF IABV0009-ID-OGG-DA IS NUMERIC AND
              IABV0009-ID-OGG-DA NOT = ZEROES

              IF IABV0009-ID-OGG-A  IS NUMERIC AND
                 IABV0009-ID-OGG-A  NOT = ZEROES

                 SET ACCESSO-X-RANGE-SI    TO TRUE

              END-IF

           END-IF.

       A025-EX.
           EXIT.

      ******************************************************************
       A040-CARICA-WHERE-CONDITION.


           MOVE IABV0009-BLOB-DATA-REC        TO WLB-REC-PREN

           INITIALIZE WS-DT-PTF-X
                      WS-DT-TS-PTF
                      WS-DT-TS-PTF-PRE.

      *    LA DATA EFFETTO VIENE VALORIZZATA CON LA DATA PRODUZIONE
           MOVE WLB-DT-PRODUZIONE
             TO WS-DT-N.
           MOVE WS-DT-N-AA
             TO WS-DT-X-AA.
           MOVE WS-DT-N-MM
             TO WS-DT-X-MM.
           MOVE WS-DT-N-GG
             TO WS-DT-X-GG.
           MOVE WS-DT-X
             TO WS-DT-PTF-X.

           MOVE WLB-TS-COMPETENZA
             TO WS-DT-TS-PTF.

           MOVE LLBV0269-TS-PRECED
             TO WS-DT-TS-PTF-PRE.

      *    DISPLAY PER VERIFICA CORRETTEZZA DATE
      *    DISPLAY 'DATA RISERVA =                   ' WLB-DT-RISERVA
      *    DISPLAY 'DATA PRODUZIONE =                ' WLB-DT-PRODUZIONE
      *    DISPLAY 'DATA EFFETTO (DATA PRODUZIONE)=  ' WS-DT-PTF-X
      *    DISPLAY 'TIMESTAMP COMPETENZA =           ' WS-DT-TS-PTF
      *    DISPLAY 'TIMESTAMP COMPETENZA PRECEDENTE = '
      *                                                WS-DT-TS-PTF-PRE

           IF  WLB-TP-FRM-ASSVA EQUAL 'EN'
               MOVE 'IN'                      TO WS-TP-FRM-ASSVA1
               MOVE 'CO'                      TO WS-TP-FRM-ASSVA2
           ELSE
               MOVE WLB-TP-FRM-ASSVA          TO WS-TP-FRM-ASSVA1
               MOVE SPACES                    TO WS-TP-FRM-ASSVA2
           END-IF.

       A040-EX.
           EXIT.
      ******************************************************************
       A100-CHECK-RETURN-CODE.

           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.

       A100-EX.
           EXIT.
      ******************************************************************
       A300-ELABORA.

           EVALUATE TRUE

              WHEN IDSV0003-WHERE-CONDITION-01
      * Estrazione massiva (parametrizzata per tp_forma_ass)
                   PERFORM SC01-SELECTION-CURSOR-01 THRU SC01-EX
      * Estrazione per Prodotto
              WHEN IDSV0003-WHERE-CONDITION-02
                   PERFORM SC02-SELECTION-CURSOR-02 THRU SC02-EX
      * Estrazione per Range di polizze
              WHEN IDSV0003-WHERE-CONDITION-03
                   PERFORM SC03-SELECTION-CURSOR-03 THRU SC03-EX
      * Estrazione per collettiva\range di adesioni
              WHEN IDSV0003-WHERE-CONDITION-04
                   PERFORM SC04-SELECTION-CURSOR-04 THRU SC04-EX
      * Aggiornamento estrazione massiva
              WHEN IDSV0003-WHERE-CONDITION-05
                   PERFORM SC05-SELECTION-CURSOR-05 THRU SC05-EX
      * Aggiornamento estrazione massiva per Prodotto
              WHEN IDSV0003-WHERE-CONDITION-06
                   PERFORM SC06-SELECTION-CURSOR-06 THRU SC06-EX
      * Aggiornamento estrazione massiva per Range di polizze
              WHEN IDSV0003-WHERE-CONDITION-07
                   PERFORM SC07-SELECTION-CURSOR-07 THRU SC07-EX
      * Aggiornamento estrazione massiva per Range di polizze\adesioni
              WHEN IDSV0003-WHERE-CONDITION-08
                   PERFORM SC08-SELECTION-CURSOR-08 THRU SC08-EX
      *
              WHEN IDSV0003-WHERE-CONDITION-09
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
      *
              WHEN IDSV0003-WHERE-CONDITION-10
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE

           END-EVALUATE.

       A300-EX.
           EXIT.
      ******************************************************************
       SC01-SELECTION-CURSOR-01.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-SC01           THRU A310-SC01-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A360-OPEN-CURSOR-SC01      THRU A360-SC01-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR-SC01     THRU A370-SC01-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST-SC01      THRU A380-SC01-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT-SC01       THRU A390-SC01-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A320-UPDATE-SC01           THRU A320-SC01-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER          TO TRUE
           END-EVALUATE.

       SC01-EX.
           EXIT.
      ******************************************************************
       A305-DECLARE-CURSOR-SC01.

           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   DECLARE CUR-SC01-RANGE CURSOR WITH HOLD FOR
                SELECT
                    A.ID_ADES
                   ,A.ID_POLI
                   ,A.ID_MOVI_CRZ
                   ,A.ID_MOVI_CHIU
                   ,A.DT_INI_EFF
                   ,A.DT_END_EFF
                   ,A.IB_PREV
                   ,A.IB_OGG
                   ,A.COD_COMP_ANIA
                   ,A.DT_DECOR
                   ,A.DT_SCAD
                   ,A.ETA_A_SCAD
                   ,A.DUR_AA
                   ,A.DUR_MM
                   ,A.DUR_GG
                   ,A.TP_RGM_FISC
                   ,A.TP_RIAT
                   ,A.TP_MOD_PAG_TIT
                   ,A.TP_IAS
                   ,A.DT_VARZ_TP_IAS
                   ,A.PRE_NET_IND
                   ,A.PRE_LRD_IND
                   ,A.RAT_LRD_IND
                   ,A.PRSTZ_INI_IND
                   ,A.FL_COINC_ASSTO
                   ,A.IB_DFLT
                   ,A.MOD_CALC
                   ,A.TP_FNT_CNBTVA
                   ,A.IMP_AZ
                   ,A.IMP_ADER
                   ,A.IMP_TFR
                   ,A.IMP_VOLO
                   ,A.PC_AZ
                   ,A.PC_ADER
                   ,A.PC_TFR
                   ,A.PC_VOLO
                   ,A.DT_NOVA_RGM_FISC
                   ,A.FL_ATTIV
                   ,A.IMP_REC_RIT_VIS
                   ,A.IMP_REC_RIT_ACC
                   ,A.FL_VARZ_STAT_TBGC
                   ,A.FL_PROVZA_MIGRAZ
                   ,A.IMPB_VIS_DA_REC
                   ,A.DT_DECOR_PREST_BAN
                   ,A.DT_EFF_VARZ_STAT_T
                   ,A.DS_RIGA
                   ,A.DS_OPER_SQL
                   ,A.DS_VER
                   ,A.DS_TS_INI_CPTZ
                   ,A.DS_TS_END_CPTZ
                   ,A.DS_UTENTE
                   ,A.DS_STATO_ELAB
                   ,A.CUM_CNBT_CAP
                   ,A.IMP_GAR_CNBT
                   ,A.DT_ULT_CONS_CNBT
                   ,A.IDEN_ISC_FND
                   ,A.NUM_RAT_PIAN
                   ,A.DT_PRESC
                   ,A.CONCS_PREST
                   ,B.ID_POLI
                   ,B.ID_MOVI_CRZ
                   ,B.ID_MOVI_CHIU
                   ,B.IB_OGG
                   ,B.IB_PROP
                   ,B.DT_PROP
                   ,B.DT_INI_EFF
                   ,B.DT_END_EFF
                   ,B.COD_COMP_ANIA
                   ,B.DT_DECOR
                   ,B.DT_EMIS
                   ,B.TP_POLI
                   ,B.DUR_AA
                   ,B.DUR_MM
                   ,B.DT_SCAD
                   ,B.COD_PROD
                   ,B.DT_INI_VLDT_PROD
                   ,B.COD_CONV
                   ,B.COD_RAMO
                   ,B.DT_INI_VLDT_CONV
                   ,B.DT_APPLZ_CONV
                   ,B.TP_FRM_ASSVA
                   ,B.TP_RGM_FISC
                   ,B.FL_ESTAS
                   ,B.FL_RSH_COMUN
                   ,B.FL_RSH_COMUN_COND
                   ,B.TP_LIV_GENZ_TIT
                   ,B.FL_COP_FINANZ
                   ,B.TP_APPLZ_DIR
                   ,B.SPE_MED
                   ,B.DIR_EMIS
                   ,B.DIR_1O_VERS
                   ,B.DIR_VERS_AGG
                   ,B.COD_DVS
                   ,B.FL_FNT_AZ
                   ,B.FL_FNT_ADER
                   ,B.FL_FNT_TFR
                   ,B.FL_FNT_VOLO
                   ,B.TP_OPZ_A_SCAD
                   ,B.AA_DIFF_PROR_DFLT
                   ,B.FL_VER_PROD
                   ,B.DUR_GG
                   ,B.DIR_QUIET
                   ,B.TP_PTF_ESTNO
                   ,B.FL_CUM_PRE_CNTR
                   ,B.FL_AMMB_MOVI
                   ,B.CONV_GECO
                   ,B.DS_RIGA
                   ,B.DS_OPER_SQL
                   ,B.DS_VER
                   ,B.DS_TS_INI_CPTZ
                   ,B.DS_TS_END_CPTZ
                   ,B.DS_UTENTE
                   ,B.DS_STATO_ELAB
                   ,B.FL_SCUDO_FISC
                   ,B.FL_TRASFE
                   ,B.FL_TFR_STRC
                   ,B.DT_PRESC
                   ,B.COD_CONV_AGG
                   ,B.SUBCAT_PROD
                   ,B.FL_QUEST_ADEGZ_ASS
                   ,B.COD_TPA
                   ,B.ID_ACC_COMM
                   ,B.FL_POLI_CPI_PR
                   ,B.FL_POLI_BUNDLING
                   ,B.IND_POLI_PRIN_COLL
                   ,B.FL_VND_BUNDLE
                   ,B.IB_BS
                   ,B.FL_POLI_IFP
                   ,C.ID_STAT_OGG_BUS
                   ,C.ID_OGG
                   ,C.TP_OGG
                   ,C.ID_MOVI_CRZ
                   ,C.ID_MOVI_CHIU
                   ,C.DT_INI_EFF
                   ,C.DT_END_EFF
                   ,C.COD_COMP_ANIA
                   ,C.TP_STAT_BUS
                   ,C.TP_CAUS
                   ,C.DS_RIGA
                   ,C.DS_OPER_SQL
                   ,C.DS_VER
                   ,C.DS_TS_INI_CPTZ
                   ,C.DS_TS_END_CPTZ
                   ,C.DS_UTENTE
                   ,C.DS_STATO_ELAB
                FROM ADES         A,
                     POLI         B,
                     STAT_OGG_BUS C
                WHERE B.TP_FRM_ASSVA   IN (:WS-TP-FRM-ASSVA1,
                                           :WS-TP-FRM-ASSVA2)
                  AND A.ID_POLI         =   B.ID_POLI
                  AND A.ID_ADES         =   C.ID_OGG
                  AND C.TP_OGG          =  'AD'
                  AND C.TP_STAT_BUS    =  'VI'
      *           OR
      *               (C.TP_STAT_BUS    =  'ST'  AND
      *               EXISTS (SELECT
      *                     D.ID_MOVI_FINRIO
      *
      *                  FROM  MOVI_FINRIO D
      *                  WHERE A.ID_ADES = D.ID_ADES
      *                    AND D.STAT_MOVI IN ('AL','IE')
      *                    AND D.TP_MOVI_FINRIO = 'DI'
      *                    AND D.COD_COMP_ANIA   =
      *                        :IDSV0003-CODICE-COMPAGNIA-ANIA
      *                    AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
      *                    AND D.DT_END_EFF     >   :WS-DT-PTF-X
      *                    AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
      *                    AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
                AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
PROVA           AND A.ID_POLI        BETWEEN   :IABV0009-ID-OGG-DA AND
                                               :IABV0009-ID-OGG-A
                AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
                AND A.DT_END_EFF     >   :WS-DT-PTF-X
                AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
                AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
                AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
                AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
                AND B.DT_END_EFF     >   :WS-DT-PTF-X
                AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
                AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
                AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
                AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
                AND C.DT_END_EFF     >   :WS-DT-PTF-X
                AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
                AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
      *         AND A.DS_STATO_ELAB IN (
      *                                 :IABV0002-STATE-01,
      *                                 :IABV0002-STATE-02,
      *                                 :IABV0002-STATE-03,
      *                                 :IABV0002-STATE-04,
      *                                 :IABV0002-STATE-05,
      *                                 :IABV0002-STATE-06,
      *                                 :IABV0002-STATE-07,
      *                                 :IABV0002-STATE-08,
      *                                 :IABV0002-STATE-09,
      *                                 :IABV0002-STATE-10
      *                                     )
      *           AND NOT A.DS_VER     = :IABV0009-VERSIONING

                  ORDER BY A.ID_POLI, A.ID_ADES
              END-EXEC
              CONTINUE
           ELSE
              EXEC SQL
                   DECLARE CUR-SC01 CURSOR WITH HOLD FOR
                SELECT
                    A.ID_ADES
                   ,A.ID_POLI
                   ,A.ID_MOVI_CRZ
                   ,A.ID_MOVI_CHIU
                   ,A.DT_INI_EFF
                   ,A.DT_END_EFF
                   ,A.IB_PREV
                   ,A.IB_OGG
                   ,A.COD_COMP_ANIA
                   ,A.DT_DECOR
                   ,A.DT_SCAD
                   ,A.ETA_A_SCAD
                   ,A.DUR_AA
                   ,A.DUR_MM
                   ,A.DUR_GG
                   ,A.TP_RGM_FISC
                   ,A.TP_RIAT
                   ,A.TP_MOD_PAG_TIT
                   ,A.TP_IAS
                   ,A.DT_VARZ_TP_IAS
                   ,A.PRE_NET_IND
                   ,A.PRE_LRD_IND
                   ,A.RAT_LRD_IND
                   ,A.PRSTZ_INI_IND
                   ,A.FL_COINC_ASSTO
                   ,A.IB_DFLT
                   ,A.MOD_CALC
                   ,A.TP_FNT_CNBTVA
                   ,A.IMP_AZ
                   ,A.IMP_ADER
                   ,A.IMP_TFR
                   ,A.IMP_VOLO
                   ,A.PC_AZ
                   ,A.PC_ADER
                   ,A.PC_TFR
                   ,A.PC_VOLO
                   ,A.DT_NOVA_RGM_FISC
                   ,A.FL_ATTIV
                   ,A.IMP_REC_RIT_VIS
                   ,A.IMP_REC_RIT_ACC
                   ,A.FL_VARZ_STAT_TBGC
                   ,A.FL_PROVZA_MIGRAZ
                   ,A.IMPB_VIS_DA_REC
                   ,A.DT_DECOR_PREST_BAN
                   ,A.DT_EFF_VARZ_STAT_T
                   ,A.DS_RIGA
                   ,A.DS_OPER_SQL
                   ,A.DS_VER
                   ,A.DS_TS_INI_CPTZ
                   ,A.DS_TS_END_CPTZ
                   ,A.DS_UTENTE
                   ,A.DS_STATO_ELAB
                   ,A.CUM_CNBT_CAP
                   ,A.IMP_GAR_CNBT
                   ,A.DT_ULT_CONS_CNBT
                   ,A.IDEN_ISC_FND
                   ,A.NUM_RAT_PIAN
                   ,A.DT_PRESC
                   ,A.CONCS_PREST
                   ,B.ID_POLI
                   ,B.ID_MOVI_CRZ
                   ,B.ID_MOVI_CHIU
                   ,B.IB_OGG
                   ,B.IB_PROP
                   ,B.DT_PROP
                   ,B.DT_INI_EFF
                   ,B.DT_END_EFF
                   ,B.COD_COMP_ANIA
                   ,B.DT_DECOR
                   ,B.DT_EMIS
                   ,B.TP_POLI
                   ,B.DUR_AA
                   ,B.DUR_MM
                   ,B.DT_SCAD
                   ,B.COD_PROD
                   ,B.DT_INI_VLDT_PROD
                   ,B.COD_CONV
                   ,B.COD_RAMO
                   ,B.DT_INI_VLDT_CONV
                   ,B.DT_APPLZ_CONV
                   ,B.TP_FRM_ASSVA
                   ,B.TP_RGM_FISC
                   ,B.FL_ESTAS
                   ,B.FL_RSH_COMUN
                   ,B.FL_RSH_COMUN_COND
                   ,B.TP_LIV_GENZ_TIT
                   ,B.FL_COP_FINANZ
                   ,B.TP_APPLZ_DIR
                   ,B.SPE_MED
                   ,B.DIR_EMIS
                   ,B.DIR_1O_VERS
                   ,B.DIR_VERS_AGG
                   ,B.COD_DVS
                   ,B.FL_FNT_AZ
                   ,B.FL_FNT_ADER
                   ,B.FL_FNT_TFR
                   ,B.FL_FNT_VOLO
                   ,B.TP_OPZ_A_SCAD
                   ,B.AA_DIFF_PROR_DFLT
                   ,B.FL_VER_PROD
                   ,B.DUR_GG
                   ,B.DIR_QUIET
                   ,B.TP_PTF_ESTNO
                   ,B.FL_CUM_PRE_CNTR
                   ,B.FL_AMMB_MOVI
                   ,B.CONV_GECO
                   ,B.DS_RIGA
                   ,B.DS_OPER_SQL
                   ,B.DS_VER
                   ,B.DS_TS_INI_CPTZ
                   ,B.DS_TS_END_CPTZ
                   ,B.DS_UTENTE
                   ,B.DS_STATO_ELAB
                   ,B.FL_SCUDO_FISC
                   ,B.FL_TRASFE
                   ,B.FL_TFR_STRC
                   ,B.DT_PRESC
                   ,B.COD_CONV_AGG
                   ,B.SUBCAT_PROD
                   ,B.FL_QUEST_ADEGZ_ASS
                   ,B.COD_TPA
                   ,B.ID_ACC_COMM
                   ,B.FL_POLI_CPI_PR
                   ,B.FL_POLI_BUNDLING
                   ,B.IND_POLI_PRIN_COLL
                   ,B.FL_VND_BUNDLE
                   ,B.IB_BS
                   ,B.FL_POLI_IFP
                   ,C.ID_STAT_OGG_BUS
                   ,C.ID_OGG
                   ,C.TP_OGG
                   ,C.ID_MOVI_CRZ
                   ,C.ID_MOVI_CHIU
                   ,C.DT_INI_EFF
                   ,C.DT_END_EFF
                   ,C.COD_COMP_ANIA
                   ,C.TP_STAT_BUS
                   ,C.TP_CAUS
                   ,C.DS_RIGA
                   ,C.DS_OPER_SQL
                   ,C.DS_VER
                   ,C.DS_TS_INI_CPTZ
                   ,C.DS_TS_END_CPTZ
                   ,C.DS_UTENTE
                   ,C.DS_STATO_ELAB
                FROM ADES         A,
                     POLI         B,
                     STAT_OGG_BUS C
                WHERE B.TP_FRM_ASSVA   IN (:WS-TP-FRM-ASSVA1,
                                           :WS-TP-FRM-ASSVA2)
                  AND A.ID_POLI         =   B.ID_POLI
                  AND A.ID_ADES         =   C.ID_OGG
                  AND C.TP_OGG          =  'AD'
                  AND C.TP_STAT_BUS    =  'VI'
      *           OR
      *               (C.TP_STAT_BUS    =  'ST'  AND
      *               EXISTS (SELECT
      *                     D.ID_MOVI_FINRIO
      *
      *                  FROM  MOVI_FINRIO D
      *                  WHERE A.ID_ADES = D.ID_ADES
      *                    AND D.STAT_MOVI IN ('AL','IE')
      *                    AND D.TP_MOVI_FINRIO = 'DI'
      *                    AND D.COD_COMP_ANIA   =
      *                        :IDSV0003-CODICE-COMPAGNIA-ANIA
      *                    AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
      *                    AND D.DT_END_EFF     >   :WS-DT-PTF-X
      *                    AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
      *                    AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
                AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
                AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
                AND A.DT_END_EFF     >   :WS-DT-PTF-X
                AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
                AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
                AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
                AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
                AND B.DT_END_EFF     >   :WS-DT-PTF-X
                AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
                AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
                AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
                AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
                AND C.DT_END_EFF     >   :WS-DT-PTF-X
                AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
                AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
      *         AND A.DS_STATO_ELAB IN (
      *                                 :IABV0002-STATE-01,
      *                                 :IABV0002-STATE-02,
      *                                 :IABV0002-STATE-03,
      *                                 :IABV0002-STATE-04,
      *                                 :IABV0002-STATE-05,
      *                                 :IABV0002-STATE-06,
      *                                 :IABV0002-STATE-07,
      *                                 :IABV0002-STATE-08,
      *                                 :IABV0002-STATE-09,
      *                                 :IABV0002-STATE-10
      *                                     )
      *           AND NOT A.DS_VER     = :IABV0009-VERSIONING

                  ORDER BY A.ID_POLI, A.ID_ADES
              END-EXEC
              CONTINUE
           END-IF.

       A305-SC01-EX.
           EXIT.
      ******************************************************************
       A310-SELECT-SC01.

           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                SELECT
                    A.ID_ADES
                   ,A.ID_POLI
                   ,A.ID_MOVI_CRZ
                   ,A.ID_MOVI_CHIU
                   ,A.DT_INI_EFF
                   ,A.DT_END_EFF
                   ,A.IB_PREV
                   ,A.IB_OGG
                   ,A.COD_COMP_ANIA
                   ,A.DT_DECOR
                   ,A.DT_SCAD
                   ,A.ETA_A_SCAD
                   ,A.DUR_AA
                   ,A.DUR_MM
                   ,A.DUR_GG
                   ,A.TP_RGM_FISC
                   ,A.TP_RIAT
                   ,A.TP_MOD_PAG_TIT
                   ,A.TP_IAS
                   ,A.DT_VARZ_TP_IAS
                   ,A.PRE_NET_IND
                   ,A.PRE_LRD_IND
                   ,A.RAT_LRD_IND
                   ,A.PRSTZ_INI_IND
                   ,A.FL_COINC_ASSTO
                   ,A.IB_DFLT
                   ,A.MOD_CALC
                   ,A.TP_FNT_CNBTVA
                   ,A.IMP_AZ
                   ,A.IMP_ADER
                   ,A.IMP_TFR
                   ,A.IMP_VOLO
                   ,A.PC_AZ
                   ,A.PC_ADER
                   ,A.PC_TFR
                   ,A.PC_VOLO
                   ,A.DT_NOVA_RGM_FISC
                   ,A.FL_ATTIV
                   ,A.IMP_REC_RIT_VIS
                   ,A.IMP_REC_RIT_ACC
                   ,A.FL_VARZ_STAT_TBGC
                   ,A.FL_PROVZA_MIGRAZ
                   ,A.IMPB_VIS_DA_REC
                   ,A.DT_DECOR_PREST_BAN
                   ,A.DT_EFF_VARZ_STAT_T
                   ,A.DS_RIGA
                   ,A.DS_OPER_SQL
                   ,A.DS_VER
                   ,A.DS_TS_INI_CPTZ
                   ,A.DS_TS_END_CPTZ
                   ,A.DS_UTENTE
                   ,A.DS_STATO_ELAB
                   ,A.CUM_CNBT_CAP
                   ,A.IMP_GAR_CNBT
                   ,A.DT_ULT_CONS_CNBT
                   ,A.IDEN_ISC_FND
                   ,A.NUM_RAT_PIAN
                   ,A.DT_PRESC
                   ,A.CONCS_PREST
                   ,B.ID_POLI
                   ,B.ID_MOVI_CRZ
                   ,B.ID_MOVI_CHIU
                   ,B.IB_OGG
                   ,B.IB_PROP
                   ,B.DT_PROP
                   ,B.DT_INI_EFF
                   ,B.DT_END_EFF
                   ,B.COD_COMP_ANIA
                   ,B.DT_DECOR
                   ,B.DT_EMIS
                   ,B.TP_POLI
                   ,B.DUR_AA
                   ,B.DUR_MM
                   ,B.DT_SCAD
                   ,B.COD_PROD
                   ,B.DT_INI_VLDT_PROD
                   ,B.COD_CONV
                   ,B.COD_RAMO
                   ,B.DT_INI_VLDT_CONV
                   ,B.DT_APPLZ_CONV
                   ,B.TP_FRM_ASSVA
                   ,B.TP_RGM_FISC
                   ,B.FL_ESTAS
                   ,B.FL_RSH_COMUN
                   ,B.FL_RSH_COMUN_COND
                   ,B.TP_LIV_GENZ_TIT
                   ,B.FL_COP_FINANZ
                   ,B.TP_APPLZ_DIR
                   ,B.SPE_MED
                   ,B.DIR_EMIS
                   ,B.DIR_1O_VERS
                   ,B.DIR_VERS_AGG
                   ,B.COD_DVS
                   ,B.FL_FNT_AZ
                   ,B.FL_FNT_ADER
                   ,B.FL_FNT_TFR
                   ,B.FL_FNT_VOLO
                   ,B.TP_OPZ_A_SCAD
                   ,B.AA_DIFF_PROR_DFLT
                   ,B.FL_VER_PROD
                   ,B.DUR_GG
                   ,B.DIR_QUIET
                   ,B.TP_PTF_ESTNO
                   ,B.FL_CUM_PRE_CNTR
                   ,B.FL_AMMB_MOVI
                   ,B.CONV_GECO
                   ,B.DS_RIGA
                   ,B.DS_OPER_SQL
                   ,B.DS_VER
                   ,B.DS_TS_INI_CPTZ
                   ,B.DS_TS_END_CPTZ
                   ,B.DS_UTENTE
                   ,B.DS_STATO_ELAB
                   ,B.FL_SCUDO_FISC
                   ,B.FL_TRASFE
                   ,B.FL_TFR_STRC
                   ,B.DT_PRESC
                   ,B.COD_CONV_AGG
                   ,B.SUBCAT_PROD
                   ,B.FL_QUEST_ADEGZ_ASS
                   ,B.COD_TPA
                   ,B.ID_ACC_COMM
                   ,B.FL_POLI_CPI_PR
                   ,B.FL_POLI_BUNDLING
                   ,B.IND_POLI_PRIN_COLL
                   ,B.FL_VND_BUNDLE
                   ,B.IB_BS
                   ,B.FL_POLI_IFP
                   ,C.ID_STAT_OGG_BUS
                   ,C.ID_OGG
                   ,C.TP_OGG
                   ,C.ID_MOVI_CRZ
                   ,C.ID_MOVI_CHIU
                   ,C.DT_INI_EFF
                   ,C.DT_END_EFF
                   ,C.COD_COMP_ANIA
                   ,C.TP_STAT_BUS
                   ,C.TP_CAUS
                   ,C.DS_RIGA
                   ,C.DS_OPER_SQL
                   ,C.DS_VER
                   ,C.DS_TS_INI_CPTZ
                   ,C.DS_TS_END_CPTZ
                   ,C.DS_UTENTE
                   ,C.DS_STATO_ELAB
                INTO
                   :ADE-ID-ADES
                  ,:ADE-ID-POLI
                  ,:ADE-ID-MOVI-CRZ
                  ,:ADE-ID-MOVI-CHIU
                   :IND-ADE-ID-MOVI-CHIU
                  ,:ADE-DT-INI-EFF-DB
                  ,:ADE-DT-END-EFF-DB
                  ,:ADE-IB-PREV
                   :IND-ADE-IB-PREV
                  ,:ADE-IB-OGG
                   :IND-ADE-IB-OGG
                  ,:ADE-COD-COMP-ANIA
                  ,:ADE-DT-DECOR-DB
                   :IND-ADE-DT-DECOR
                  ,:ADE-DT-SCAD-DB
                   :IND-ADE-DT-SCAD
                  ,:ADE-ETA-A-SCAD
                   :IND-ADE-ETA-A-SCAD
                  ,:ADE-DUR-AA
                   :IND-ADE-DUR-AA
                  ,:ADE-DUR-MM
                   :IND-ADE-DUR-MM
                  ,:ADE-DUR-GG
                   :IND-ADE-DUR-GG
                  ,:ADE-TP-RGM-FISC
                  ,:ADE-TP-RIAT
                   :IND-ADE-TP-RIAT
                  ,:ADE-TP-MOD-PAG-TIT
                  ,:ADE-TP-IAS
                   :IND-ADE-TP-IAS
                  ,:ADE-DT-VARZ-TP-IAS-DB
                   :IND-ADE-DT-VARZ-TP-IAS
                  ,:ADE-PRE-NET-IND
                   :IND-ADE-PRE-NET-IND
                  ,:ADE-PRE-LRD-IND
                   :IND-ADE-PRE-LRD-IND
                  ,:ADE-RAT-LRD-IND
                   :IND-ADE-RAT-LRD-IND
                  ,:ADE-PRSTZ-INI-IND
                   :IND-ADE-PRSTZ-INI-IND
                  ,:ADE-FL-COINC-ASSTO
                   :IND-ADE-FL-COINC-ASSTO
                  ,:ADE-IB-DFLT
                   :IND-ADE-IB-DFLT
                  ,:ADE-MOD-CALC
                   :IND-ADE-MOD-CALC
                  ,:ADE-TP-FNT-CNBTVA
                   :IND-ADE-TP-FNT-CNBTVA
                  ,:ADE-IMP-AZ
                   :IND-ADE-IMP-AZ
                  ,:ADE-IMP-ADER
                   :IND-ADE-IMP-ADER
                  ,:ADE-IMP-TFR
                   :IND-ADE-IMP-TFR
                  ,:ADE-IMP-VOLO
                   :IND-ADE-IMP-VOLO
                  ,:ADE-PC-AZ
                   :IND-ADE-PC-AZ
                  ,:ADE-PC-ADER
                   :IND-ADE-PC-ADER
                  ,:ADE-PC-TFR
                   :IND-ADE-PC-TFR
                  ,:ADE-PC-VOLO
                   :IND-ADE-PC-VOLO
                  ,:ADE-DT-NOVA-RGM-FISC-DB
                   :IND-ADE-DT-NOVA-RGM-FISC
                  ,:ADE-FL-ATTIV
                   :IND-ADE-FL-ATTIV
                  ,:ADE-IMP-REC-RIT-VIS
                   :IND-ADE-IMP-REC-RIT-VIS
                  ,:ADE-IMP-REC-RIT-ACC
                   :IND-ADE-IMP-REC-RIT-ACC
                  ,:ADE-FL-VARZ-STAT-TBGC
                   :IND-ADE-FL-VARZ-STAT-TBGC
                  ,:ADE-FL-PROVZA-MIGRAZ
                   :IND-ADE-FL-PROVZA-MIGRAZ
                  ,:ADE-IMPB-VIS-DA-REC
                   :IND-ADE-IMPB-VIS-DA-REC
                  ,:ADE-DT-DECOR-PREST-BAN-DB
                   :IND-ADE-DT-DECOR-PREST-BAN
                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
                   :IND-ADE-DT-EFF-VARZ-STAT-T
                  ,:ADE-DS-RIGA
                  ,:ADE-DS-OPER-SQL
                  ,:ADE-DS-VER
                  ,:ADE-DS-TS-INI-CPTZ
                  ,:ADE-DS-TS-END-CPTZ
                  ,:ADE-DS-UTENTE
                  ,:ADE-DS-STATO-ELAB
                  ,:ADE-CUM-CNBT-CAP
                   :IND-ADE-CUM-CNBT-CAP
                  ,:ADE-IMP-GAR-CNBT
                   :IND-ADE-IMP-GAR-CNBT
                  ,:ADE-DT-ULT-CONS-CNBT-DB
                   :IND-ADE-DT-ULT-CONS-CNBT
                  ,:ADE-IDEN-ISC-FND
                   :IND-ADE-IDEN-ISC-FND
                  ,:ADE-NUM-RAT-PIAN
                   :IND-ADE-NUM-RAT-PIAN
                  ,:ADE-DT-PRESC-DB
                   :IND-ADE-DT-PRESC
                  ,:ADE-CONCS-PREST
                   :IND-ADE-CONCS-PREST
                  ,:POL-ID-POLI
                  ,:POL-ID-MOVI-CRZ
                  ,:POL-ID-MOVI-CHIU
                   :IND-POL-ID-MOVI-CHIU
                  ,:POL-IB-OGG
                   :IND-POL-IB-OGG
                  ,:POL-IB-PROP
                  ,:POL-DT-PROP-DB
                   :IND-POL-DT-PROP
                  ,:POL-DT-INI-EFF-DB
                  ,:POL-DT-END-EFF-DB
                  ,:POL-COD-COMP-ANIA
                  ,:POL-DT-DECOR-DB
                  ,:POL-DT-EMIS-DB
                  ,:POL-TP-POLI
                  ,:POL-DUR-AA
                   :IND-POL-DUR-AA
                  ,:POL-DUR-MM
                   :IND-POL-DUR-MM
                  ,:POL-DT-SCAD-DB
                   :IND-POL-DT-SCAD
                  ,:POL-COD-PROD
                  ,:POL-DT-INI-VLDT-PROD-DB
                  ,:POL-COD-CONV
                   :IND-POL-COD-CONV
                  ,:POL-COD-RAMO
                   :IND-POL-COD-RAMO
                  ,:POL-DT-INI-VLDT-CONV-DB
                   :IND-POL-DT-INI-VLDT-CONV
                  ,:POL-DT-APPLZ-CONV-DB
                   :IND-POL-DT-APPLZ-CONV
                  ,:POL-TP-FRM-ASSVA
                  ,:POL-TP-RGM-FISC
                   :IND-POL-TP-RGM-FISC
                  ,:POL-FL-ESTAS
                   :IND-POL-FL-ESTAS
                  ,:POL-FL-RSH-COMUN
                   :IND-POL-FL-RSH-COMUN
                  ,:POL-FL-RSH-COMUN-COND
                   :IND-POL-FL-RSH-COMUN-COND
                  ,:POL-TP-LIV-GENZ-TIT
                  ,:POL-FL-COP-FINANZ
                   :IND-POL-FL-COP-FINANZ
                  ,:POL-TP-APPLZ-DIR
                   :IND-POL-TP-APPLZ-DIR
                  ,:POL-SPE-MED
                   :IND-POL-SPE-MED
                  ,:POL-DIR-EMIS
                   :IND-POL-DIR-EMIS
                  ,:POL-DIR-1O-VERS
                   :IND-POL-DIR-1O-VERS
                  ,:POL-DIR-VERS-AGG
                   :IND-POL-DIR-VERS-AGG
                  ,:POL-COD-DVS
                   :IND-POL-COD-DVS
                  ,:POL-FL-FNT-AZ
                   :IND-POL-FL-FNT-AZ
                  ,:POL-FL-FNT-ADER
                   :IND-POL-FL-FNT-ADER
                  ,:POL-FL-FNT-TFR
                   :IND-POL-FL-FNT-TFR
                  ,:POL-FL-FNT-VOLO
                   :IND-POL-FL-FNT-VOLO
                  ,:POL-TP-OPZ-A-SCAD
                   :IND-POL-TP-OPZ-A-SCAD
                  ,:POL-AA-DIFF-PROR-DFLT
                   :IND-POL-AA-DIFF-PROR-DFLT
                  ,:POL-FL-VER-PROD
                   :IND-POL-FL-VER-PROD
                  ,:POL-DUR-GG
                   :IND-POL-DUR-GG
                  ,:POL-DIR-QUIET
                   :IND-POL-DIR-QUIET
                  ,:POL-TP-PTF-ESTNO
                   :IND-POL-TP-PTF-ESTNO
                  ,:POL-FL-CUM-PRE-CNTR
                   :IND-POL-FL-CUM-PRE-CNTR
                  ,:POL-FL-AMMB-MOVI
                   :IND-POL-FL-AMMB-MOVI
                  ,:POL-CONV-GECO
                   :IND-POL-CONV-GECO
                  ,:POL-DS-RIGA
                  ,:POL-DS-OPER-SQL
                  ,:POL-DS-VER
                  ,:POL-DS-TS-INI-CPTZ
                  ,:POL-DS-TS-END-CPTZ
                  ,:POL-DS-UTENTE
                  ,:POL-DS-STATO-ELAB
                  ,:POL-FL-SCUDO-FISC
                   :IND-POL-FL-SCUDO-FISC
                  ,:POL-FL-TRASFE
                   :IND-POL-FL-TRASFE
                  ,:POL-FL-TFR-STRC
                   :IND-POL-FL-TFR-STRC
                  ,:POL-DT-PRESC-DB
                   :IND-POL-DT-PRESC
                  ,:POL-COD-CONV-AGG
                   :IND-POL-COD-CONV-AGG
                  ,:POL-SUBCAT-PROD
                   :IND-POL-SUBCAT-PROD
                  ,:POL-FL-QUEST-ADEGZ-ASS
                   :IND-POL-FL-QUEST-ADEGZ-ASS
                  ,:POL-COD-TPA
                   :IND-POL-COD-TPA
                  ,:POL-ID-ACC-COMM
                   :IND-POL-ID-ACC-COMM
                  ,:POL-FL-POLI-CPI-PR
                   :IND-POL-FL-POLI-CPI-PR
                  ,:POL-FL-POLI-BUNDLING
                   :IND-POL-FL-POLI-BUNDLING
                  ,:POL-IND-POLI-PRIN-COLL
                   :IND-POL-IND-POLI-PRIN-COLL
                  ,:POL-FL-VND-BUNDLE
                   :IND-POL-FL-VND-BUNDLE
                  ,:POL-IB-BS
                   :IND-POL-IB-BS
                  ,:POL-FL-POLI-IFP
                   :IND-POL-FL-POLI-IFP
                  ,:STB-ID-STAT-OGG-BUS
                  ,:STB-ID-OGG
                  ,:STB-TP-OGG
                  ,:STB-ID-MOVI-CRZ
                  ,:STB-ID-MOVI-CHIU
                   :IND-STB-ID-MOVI-CHIU
                  ,:STB-DT-INI-EFF-DB
                  ,:STB-DT-END-EFF-DB
                  ,:STB-COD-COMP-ANIA
                  ,:STB-TP-STAT-BUS
                  ,:STB-TP-CAUS
                  ,:STB-DS-RIGA
                  ,:STB-DS-OPER-SQL
                  ,:STB-DS-VER
                  ,:STB-DS-TS-INI-CPTZ
                  ,:STB-DS-TS-END-CPTZ
                  ,:STB-DS-UTENTE
                  ,:STB-DS-STATO-ELAB
                FROM ADES         A,
                     POLI         B,
                     STAT_OGG_BUS C
                WHERE B.TP_FRM_ASSVA   IN (:WS-TP-FRM-ASSVA1,
                                           :WS-TP-FRM-ASSVA2)
                  AND A.ID_POLI         =   B.ID_POLI
                  AND A.ID_ADES         =   C.ID_OGG
                  AND C.TP_OGG          =  'AD'
                  AND C.TP_STAT_BUS     =  'VI'
      *           OR
      *               (C.TP_STAT_BUS    =  'ST'  AND
      *               EXISTS(SELECT
      *                     D.ID_MOVI_FINRIO
      *
      *                  FROM  MOVI_FINRIO D
      *                  WHERE A.ID_ADES = D.ID_ADES
      *                    AND D.STAT_MOVI IN ('AL','IE')
      *                    AND D.TP_MOVI_FINRIO = 'DI'
      *                    AND D.COD_COMP_ANIA   =
      *                        :IDSV0003-CODICE-COMPAGNIA-ANIA
      *                    AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
      *                    AND D.DT_END_EFF     >   :WS-DT-PTF-X
      *                    AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
      *                    AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
                AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
PROVA           AND A.ID_POLI        BETWEEN   :IABV0009-ID-OGG-DA AND
                                               :IABV0009-ID-OGG-A
                AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
                AND A.DT_END_EFF     >   :WS-DT-PTF-X
                AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
                AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
                AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
                AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
                AND B.DT_END_EFF     >   :WS-DT-PTF-X
                AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
                AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
                AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
                AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
                AND C.DT_END_EFF     >   :WS-DT-PTF-X
                AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
                AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
      *         AND A.DS_STATO_ELAB IN   (
      *                                   :IABV0002-STATE-01,
      *                                   :IABV0002-STATE-02,
      *                                   :IABV0002-STATE-03,
      *                                   :IABV0002-STATE-04,
      *                                   :IABV0002-STATE-05,
      *                                   :IABV0002-STATE-06,
      *                                   :IABV0002-STATE-07,
      *                                   :IABV0002-STATE-08,
      *                                   :IABV0002-STATE-09,
      *                                   :IABV0002-STATE-10
      *                                    )
      *           AND NOT A.DS_VER     = :IABV0009-VERSIONING

                 FETCH FIRST ROW ONLY
              END-EXEC
           ELSE
              EXEC SQL
                SELECT
                    A.ID_ADES
                   ,A.ID_POLI
                   ,A.ID_MOVI_CRZ
                   ,A.ID_MOVI_CHIU
                   ,A.DT_INI_EFF
                   ,A.DT_END_EFF
                   ,A.IB_PREV
                   ,A.IB_OGG
                   ,A.COD_COMP_ANIA
                   ,A.DT_DECOR
                   ,A.DT_SCAD
                   ,A.ETA_A_SCAD
                   ,A.DUR_AA
                   ,A.DUR_MM
                   ,A.DUR_GG
                   ,A.TP_RGM_FISC
                   ,A.TP_RIAT
                   ,A.TP_MOD_PAG_TIT
                   ,A.TP_IAS
                   ,A.DT_VARZ_TP_IAS
                   ,A.PRE_NET_IND
                   ,A.PRE_LRD_IND
                   ,A.RAT_LRD_IND
                   ,A.PRSTZ_INI_IND
                   ,A.FL_COINC_ASSTO
                   ,A.IB_DFLT
                   ,A.MOD_CALC
                   ,A.TP_FNT_CNBTVA
                   ,A.IMP_AZ
                   ,A.IMP_ADER
                   ,A.IMP_TFR
                   ,A.IMP_VOLO
                   ,A.PC_AZ
                   ,A.PC_ADER
                   ,A.PC_TFR
                   ,A.PC_VOLO
                   ,A.DT_NOVA_RGM_FISC
                   ,A.FL_ATTIV
                   ,A.IMP_REC_RIT_VIS
                   ,A.IMP_REC_RIT_ACC
                   ,A.FL_VARZ_STAT_TBGC
                   ,A.FL_PROVZA_MIGRAZ
                   ,A.IMPB_VIS_DA_REC
                   ,A.DT_DECOR_PREST_BAN
                   ,A.DT_EFF_VARZ_STAT_T
                   ,A.DS_RIGA
                   ,A.DS_OPER_SQL
                   ,A.DS_VER
                   ,A.DS_TS_INI_CPTZ
                   ,A.DS_TS_END_CPTZ
                   ,A.DS_UTENTE
                   ,A.DS_STATO_ELAB
                   ,A.CUM_CNBT_CAP
                   ,A.IMP_GAR_CNBT
                   ,A.DT_ULT_CONS_CNBT
                   ,A.IDEN_ISC_FND
                   ,A.NUM_RAT_PIAN
                   ,A.DT_PRESC
                   ,A.CONCS_PREST
                   ,B.ID_POLI
                   ,B.ID_MOVI_CRZ
                   ,B.ID_MOVI_CHIU
                   ,B.IB_OGG
                   ,B.IB_PROP
                   ,B.DT_PROP
                   ,B.DT_INI_EFF
                   ,B.DT_END_EFF
                   ,B.COD_COMP_ANIA
                   ,B.DT_DECOR
                   ,B.DT_EMIS
                   ,B.TP_POLI
                   ,B.DUR_AA
                   ,B.DUR_MM
                   ,B.DT_SCAD
                   ,B.COD_PROD
                   ,B.DT_INI_VLDT_PROD
                   ,B.COD_CONV
                   ,B.COD_RAMO
                   ,B.DT_INI_VLDT_CONV
                   ,B.DT_APPLZ_CONV
                   ,B.TP_FRM_ASSVA
                   ,B.TP_RGM_FISC
                   ,B.FL_ESTAS
                   ,B.FL_RSH_COMUN
                   ,B.FL_RSH_COMUN_COND
                   ,B.TP_LIV_GENZ_TIT
                   ,B.FL_COP_FINANZ
                   ,B.TP_APPLZ_DIR
                   ,B.SPE_MED
                   ,B.DIR_EMIS
                   ,B.DIR_1O_VERS
                   ,B.DIR_VERS_AGG
                   ,B.COD_DVS
                   ,B.FL_FNT_AZ
                   ,B.FL_FNT_ADER
                   ,B.FL_FNT_TFR
                   ,B.FL_FNT_VOLO
                   ,B.TP_OPZ_A_SCAD
                   ,B.AA_DIFF_PROR_DFLT
                   ,B.FL_VER_PROD
                   ,B.DUR_GG
                   ,B.DIR_QUIET
                   ,B.TP_PTF_ESTNO
                   ,B.FL_CUM_PRE_CNTR
                   ,B.FL_AMMB_MOVI
                   ,B.CONV_GECO
                   ,B.DS_RIGA
                   ,B.DS_OPER_SQL
                   ,B.DS_VER
                   ,B.DS_TS_INI_CPTZ
                   ,B.DS_TS_END_CPTZ
                   ,B.DS_UTENTE
                   ,B.DS_STATO_ELAB
                   ,B.FL_SCUDO_FISC
                   ,B.FL_TRASFE
                   ,B.FL_TFR_STRC
                   ,B.DT_PRESC
                   ,B.COD_CONV_AGG
                   ,B.SUBCAT_PROD
                   ,B.FL_QUEST_ADEGZ_ASS
                   ,B.COD_TPA
                   ,B.ID_ACC_COMM
                   ,B.FL_POLI_CPI_PR
                   ,B.FL_POLI_BUNDLING
                   ,B.IND_POLI_PRIN_COLL
                   ,B.FL_VND_BUNDLE
                   ,B.IB_BS
                   ,B.FL_POLI_IFP
                   ,C.ID_STAT_OGG_BUS
                   ,C.ID_OGG
                   ,C.TP_OGG
                   ,C.ID_MOVI_CRZ
                   ,C.ID_MOVI_CHIU
                   ,C.DT_INI_EFF
                   ,C.DT_END_EFF
                   ,C.COD_COMP_ANIA
                   ,C.TP_STAT_BUS
                   ,C.TP_CAUS
                   ,C.DS_RIGA
                   ,C.DS_OPER_SQL
                   ,C.DS_VER
                   ,C.DS_TS_INI_CPTZ
                   ,C.DS_TS_END_CPTZ
                   ,C.DS_UTENTE
                   ,C.DS_STATO_ELAB
                INTO
                   :ADE-ID-ADES
                  ,:ADE-ID-POLI
                  ,:ADE-ID-MOVI-CRZ
                  ,:ADE-ID-MOVI-CHIU
                   :IND-ADE-ID-MOVI-CHIU
                  ,:ADE-DT-INI-EFF-DB
                  ,:ADE-DT-END-EFF-DB
                  ,:ADE-IB-PREV
                   :IND-ADE-IB-PREV
                  ,:ADE-IB-OGG
                   :IND-ADE-IB-OGG
                  ,:ADE-COD-COMP-ANIA
                  ,:ADE-DT-DECOR-DB
                   :IND-ADE-DT-DECOR
                  ,:ADE-DT-SCAD-DB
                   :IND-ADE-DT-SCAD
                  ,:ADE-ETA-A-SCAD
                   :IND-ADE-ETA-A-SCAD
                  ,:ADE-DUR-AA
                   :IND-ADE-DUR-AA
                  ,:ADE-DUR-MM
                   :IND-ADE-DUR-MM
                  ,:ADE-DUR-GG
                   :IND-ADE-DUR-GG
                  ,:ADE-TP-RGM-FISC
                  ,:ADE-TP-RIAT
                   :IND-ADE-TP-RIAT
                  ,:ADE-TP-MOD-PAG-TIT
                  ,:ADE-TP-IAS
                   :IND-ADE-TP-IAS
                  ,:ADE-DT-VARZ-TP-IAS-DB
                   :IND-ADE-DT-VARZ-TP-IAS
                  ,:ADE-PRE-NET-IND
                   :IND-ADE-PRE-NET-IND
                  ,:ADE-PRE-LRD-IND
                   :IND-ADE-PRE-LRD-IND
                  ,:ADE-RAT-LRD-IND
                   :IND-ADE-RAT-LRD-IND
                  ,:ADE-PRSTZ-INI-IND
                   :IND-ADE-PRSTZ-INI-IND
                  ,:ADE-FL-COINC-ASSTO
                   :IND-ADE-FL-COINC-ASSTO
                  ,:ADE-IB-DFLT
                   :IND-ADE-IB-DFLT
                  ,:ADE-MOD-CALC
                   :IND-ADE-MOD-CALC
                  ,:ADE-TP-FNT-CNBTVA
                   :IND-ADE-TP-FNT-CNBTVA
                  ,:ADE-IMP-AZ
                   :IND-ADE-IMP-AZ
                  ,:ADE-IMP-ADER
                   :IND-ADE-IMP-ADER
                  ,:ADE-IMP-TFR
                   :IND-ADE-IMP-TFR
                  ,:ADE-IMP-VOLO
                   :IND-ADE-IMP-VOLO
                  ,:ADE-PC-AZ
                   :IND-ADE-PC-AZ
                  ,:ADE-PC-ADER
                   :IND-ADE-PC-ADER
                  ,:ADE-PC-TFR
                   :IND-ADE-PC-TFR
                  ,:ADE-PC-VOLO
                   :IND-ADE-PC-VOLO
                  ,:ADE-DT-NOVA-RGM-FISC-DB
                   :IND-ADE-DT-NOVA-RGM-FISC
                  ,:ADE-FL-ATTIV
                   :IND-ADE-FL-ATTIV
                  ,:ADE-IMP-REC-RIT-VIS
                   :IND-ADE-IMP-REC-RIT-VIS
                  ,:ADE-IMP-REC-RIT-ACC
                   :IND-ADE-IMP-REC-RIT-ACC
                  ,:ADE-FL-VARZ-STAT-TBGC
                   :IND-ADE-FL-VARZ-STAT-TBGC
                  ,:ADE-FL-PROVZA-MIGRAZ
                   :IND-ADE-FL-PROVZA-MIGRAZ
                  ,:ADE-IMPB-VIS-DA-REC
                   :IND-ADE-IMPB-VIS-DA-REC
                  ,:ADE-DT-DECOR-PREST-BAN-DB
                   :IND-ADE-DT-DECOR-PREST-BAN
                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
                   :IND-ADE-DT-EFF-VARZ-STAT-T
                  ,:ADE-DS-RIGA
                  ,:ADE-DS-OPER-SQL
                  ,:ADE-DS-VER
                  ,:ADE-DS-TS-INI-CPTZ
                  ,:ADE-DS-TS-END-CPTZ
                  ,:ADE-DS-UTENTE
                  ,:ADE-DS-STATO-ELAB
                  ,:ADE-CUM-CNBT-CAP
                   :IND-ADE-CUM-CNBT-CAP
                  ,:ADE-IMP-GAR-CNBT
                   :IND-ADE-IMP-GAR-CNBT
                  ,:ADE-DT-ULT-CONS-CNBT-DB
                   :IND-ADE-DT-ULT-CONS-CNBT
                  ,:ADE-IDEN-ISC-FND
                   :IND-ADE-IDEN-ISC-FND
                  ,:ADE-NUM-RAT-PIAN
                   :IND-ADE-NUM-RAT-PIAN
                  ,:ADE-DT-PRESC-DB
                   :IND-ADE-DT-PRESC
                  ,:ADE-CONCS-PREST
                   :IND-ADE-CONCS-PREST
                  ,:POL-ID-POLI
                  ,:POL-ID-MOVI-CRZ
                  ,:POL-ID-MOVI-CHIU
                   :IND-POL-ID-MOVI-CHIU
                  ,:POL-IB-OGG
                   :IND-POL-IB-OGG
                  ,:POL-IB-PROP
                  ,:POL-DT-PROP-DB
                   :IND-POL-DT-PROP
                  ,:POL-DT-INI-EFF-DB
                  ,:POL-DT-END-EFF-DB
                  ,:POL-COD-COMP-ANIA
                  ,:POL-DT-DECOR-DB
                  ,:POL-DT-EMIS-DB
                  ,:POL-TP-POLI
                  ,:POL-DUR-AA
                   :IND-POL-DUR-AA
                  ,:POL-DUR-MM
                   :IND-POL-DUR-MM
                  ,:POL-DT-SCAD-DB
                   :IND-POL-DT-SCAD
                  ,:POL-COD-PROD
                  ,:POL-DT-INI-VLDT-PROD-DB
                  ,:POL-COD-CONV
                   :IND-POL-COD-CONV
                  ,:POL-COD-RAMO
                   :IND-POL-COD-RAMO
                  ,:POL-DT-INI-VLDT-CONV-DB
                   :IND-POL-DT-INI-VLDT-CONV
                  ,:POL-DT-APPLZ-CONV-DB
                   :IND-POL-DT-APPLZ-CONV
                  ,:POL-TP-FRM-ASSVA
                  ,:POL-TP-RGM-FISC
                   :IND-POL-TP-RGM-FISC
                  ,:POL-FL-ESTAS
                   :IND-POL-FL-ESTAS
                  ,:POL-FL-RSH-COMUN
                   :IND-POL-FL-RSH-COMUN
                  ,:POL-FL-RSH-COMUN-COND
                   :IND-POL-FL-RSH-COMUN-COND
                  ,:POL-TP-LIV-GENZ-TIT
                  ,:POL-FL-COP-FINANZ
                   :IND-POL-FL-COP-FINANZ
                  ,:POL-TP-APPLZ-DIR
                   :IND-POL-TP-APPLZ-DIR
                  ,:POL-SPE-MED
                   :IND-POL-SPE-MED
                  ,:POL-DIR-EMIS
                   :IND-POL-DIR-EMIS
                  ,:POL-DIR-1O-VERS
                   :IND-POL-DIR-1O-VERS
                  ,:POL-DIR-VERS-AGG
                   :IND-POL-DIR-VERS-AGG
                  ,:POL-COD-DVS
                   :IND-POL-COD-DVS
                  ,:POL-FL-FNT-AZ
                   :IND-POL-FL-FNT-AZ
                  ,:POL-FL-FNT-ADER
                   :IND-POL-FL-FNT-ADER
                  ,:POL-FL-FNT-TFR
                   :IND-POL-FL-FNT-TFR
                  ,:POL-FL-FNT-VOLO
                   :IND-POL-FL-FNT-VOLO
                  ,:POL-TP-OPZ-A-SCAD
                   :IND-POL-TP-OPZ-A-SCAD
                  ,:POL-AA-DIFF-PROR-DFLT
                   :IND-POL-AA-DIFF-PROR-DFLT
                  ,:POL-FL-VER-PROD
                   :IND-POL-FL-VER-PROD
                  ,:POL-DUR-GG
                   :IND-POL-DUR-GG
                  ,:POL-DIR-QUIET
                   :IND-POL-DIR-QUIET
                  ,:POL-TP-PTF-ESTNO
                   :IND-POL-TP-PTF-ESTNO
                  ,:POL-FL-CUM-PRE-CNTR
                   :IND-POL-FL-CUM-PRE-CNTR
                  ,:POL-FL-AMMB-MOVI
                   :IND-POL-FL-AMMB-MOVI
                  ,:POL-CONV-GECO
                   :IND-POL-CONV-GECO
                  ,:POL-DS-RIGA
                  ,:POL-DS-OPER-SQL
                  ,:POL-DS-VER
                  ,:POL-DS-TS-INI-CPTZ
                  ,:POL-DS-TS-END-CPTZ
                  ,:POL-DS-UTENTE
                  ,:POL-DS-STATO-ELAB
                  ,:POL-FL-SCUDO-FISC
                   :IND-POL-FL-SCUDO-FISC
                  ,:POL-FL-TRASFE
                   :IND-POL-FL-TRASFE
                  ,:POL-FL-TFR-STRC
                   :IND-POL-FL-TFR-STRC
                  ,:POL-DT-PRESC-DB
                   :IND-POL-DT-PRESC
                  ,:POL-COD-CONV-AGG
                   :IND-POL-COD-CONV-AGG
                  ,:POL-SUBCAT-PROD
                   :IND-POL-SUBCAT-PROD
                  ,:POL-FL-QUEST-ADEGZ-ASS
                   :IND-POL-FL-QUEST-ADEGZ-ASS
                  ,:POL-COD-TPA
                   :IND-POL-COD-TPA
                  ,:POL-ID-ACC-COMM
                   :IND-POL-ID-ACC-COMM
                  ,:POL-FL-POLI-CPI-PR
                   :IND-POL-FL-POLI-CPI-PR
                  ,:POL-FL-POLI-BUNDLING
                   :IND-POL-FL-POLI-BUNDLING
                  ,:POL-IND-POLI-PRIN-COLL
                   :IND-POL-IND-POLI-PRIN-COLL
                  ,:POL-FL-VND-BUNDLE
                   :IND-POL-FL-VND-BUNDLE
                  ,:POL-IB-BS
                   :IND-POL-IB-BS
                  ,:POL-FL-POLI-IFP
                   :IND-POL-FL-POLI-IFP
                  ,:STB-ID-STAT-OGG-BUS
                  ,:STB-ID-OGG
                  ,:STB-TP-OGG
                  ,:STB-ID-MOVI-CRZ
                  ,:STB-ID-MOVI-CHIU
                   :IND-STB-ID-MOVI-CHIU
                  ,:STB-DT-INI-EFF-DB
                  ,:STB-DT-END-EFF-DB
                  ,:STB-COD-COMP-ANIA
                  ,:STB-TP-STAT-BUS
                  ,:STB-TP-CAUS
                  ,:STB-DS-RIGA
                  ,:STB-DS-OPER-SQL
                  ,:STB-DS-VER
                  ,:STB-DS-TS-INI-CPTZ
                  ,:STB-DS-TS-END-CPTZ
                  ,:STB-DS-UTENTE
                  ,:STB-DS-STATO-ELAB
                FROM ADES         A,
                     POLI         B,
                     STAT_OGG_BUS C
                WHERE B.TP_FRM_ASSVA   IN (:WS-TP-FRM-ASSVA1,
                                           :WS-TP-FRM-ASSVA2)
                  AND A.ID_POLI         =   B.ID_POLI
                  AND A.ID_ADES         =   C.ID_OGG
                  AND C.TP_OGG          =  'AD'
                  AND C.TP_STAT_BUS     =  'VI'
      *           OR
      *               (C.TP_STAT_BUS    =  'ST'  AND
      *               EXISTS(SELECT
      *                     D.ID_MOVI_FINRIO
      *
      *                  FROM  MOVI_FINRIO D
      *                  WHERE A.ID_ADES = D.ID_ADES
      *                    AND D.STAT_MOVI IN ('AL','IE')
      *                    AND D.TP_MOVI_FINRIO = 'DI'
      *                    AND D.COD_COMP_ANIA   =
      *                        :IDSV0003-CODICE-COMPAGNIA-ANIA
      *                    AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
      *                    AND D.DT_END_EFF     >   :WS-DT-PTF-X
      *                    AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
      *                    AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
                AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
                AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
                AND A.DT_END_EFF     >   :WS-DT-PTF-X
                AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
                AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
                AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
                AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
                AND B.DT_END_EFF     >   :WS-DT-PTF-X
                AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
                AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
                AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
                AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
                AND C.DT_END_EFF     >   :WS-DT-PTF-X
                AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
                AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
      *         AND A.DS_STATO_ELAB IN   (
      *                                   :IABV0002-STATE-01,
      *                                   :IABV0002-STATE-02,
      *                                   :IABV0002-STATE-03,
      *                                   :IABV0002-STATE-04,
      *                                   :IABV0002-STATE-05,
      *                                   :IABV0002-STATE-06,
      *                                   :IABV0002-STATE-07,
      *                                   :IABV0002-STATE-08,
      *                                   :IABV0002-STATE-09,
      *                                   :IABV0002-STATE-10
      *                                    )
      *           AND NOT A.DS_VER     = :IABV0009-VERSIONING

                 FETCH FIRST ROW ONLY
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-SC01-EX.
           EXIT.

      ******************************************************************
       A320-UPDATE-SC01.

           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
                   PERFORM A330-UPDATE-PK-SC01         THRU A330-SC01-EX

              WHEN IDSV0003-WHERE-CONDITION-01
                   PERFORM A340-UPDATE-WHERE-COND-SC01 THRU A340-SC01-EX
              WHEN IDSV0003-FIRST-ACTION
                   PERFORM A345-UPDATE-FIRST-ACTION-SC01
                                                       THRU A345-SC01-EX
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
           END-EVALUATE.

       A320-SC01-EX.
           EXIT.

      ******************************************************************

       A330-UPDATE-PK-SC01.

           PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX

           EXEC SQL
               UPDATE ADES
               SET
                  DS_OPER_SQL            = :ADE-DS-OPER-SQL
      *          ,DS_VER                 = :IABV0009-VERSIONING
                 ,DS_UTENTE              = :ADE-DS-UTENTE
                 ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
               WHERE             DS_RIGA = :ADE-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A330-SC01-EX.
           EXIT.

      ******************************************************************

       A340-UPDATE-WHERE-COND-SC01.

           CONTINUE.

       A340-SC01-EX.
           EXIT.
      ******************************************************************
       A345-UPDATE-FIRST-ACTION-SC01.

           CONTINUE.

       A345-SC01-EX.
           EXIT.
      ******************************************************************

       A350-CTRL-COMMIT.

      *--
      *--    si effettuano i controlli per comandare
      *--    un'eventuale COMMIT al BATCH EXECUTOR
      *--    tramite il settaggio del campo IABV0002-FLAG-COMMIT
      *--

           CONTINUE.

       A350-EX.
           EXIT.

      ******************************************************************

       A360-OPEN-CURSOR-SC01.

           PERFORM A305-DECLARE-CURSOR-SC01 THRU A305-SC01-EX.

           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   OPEN CUR-SC01-RANGE
              END-EXEC
           ELSE
              EXEC SQL
                   OPEN CUR-SC01
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A360-SC01-EX.
           EXIT.

      ******************************************************************

       A370-CLOSE-CURSOR-SC01.

           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   CLOSE CUR-SC01-RANGE
              END-EXEC
           ELSE
              EXEC SQL
                   CLOSE CUR-SC01
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A370-SC01-EX.
           EXIT.

      ******************************************************************

       A380-FETCH-FIRST-SC01.

           PERFORM A360-OPEN-CURSOR-SC01    THRU A360-SC01-EX.


           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-SC01  THRU A390-SC01-EX
           END-IF.

       A380-SC01-EX.
           EXIT.

      ******************************************************************

       A390-FETCH-NEXT-SC01.

           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                FETCH CUR-SC01-RANGE
              INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
               ,:POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
               ,:STB-ID-STAT-OGG-BUS
               ,:STB-ID-OGG
               ,:STB-TP-OGG
               ,:STB-ID-MOVI-CRZ
               ,:STB-ID-MOVI-CHIU
                :IND-STB-ID-MOVI-CHIU
               ,:STB-DT-INI-EFF-DB
               ,:STB-DT-END-EFF-DB
               ,:STB-COD-COMP-ANIA
               ,:STB-TP-STAT-BUS
               ,:STB-TP-CAUS
               ,:STB-DS-RIGA
               ,:STB-DS-OPER-SQL
               ,:STB-DS-VER
               ,:STB-DS-TS-INI-CPTZ
               ,:STB-DS-TS-END-CPTZ
               ,:STB-DS-UTENTE
               ,:STB-DS-STATO-ELAB
              END-EXEC
           ELSE
              EXEC SQL
                FETCH CUR-SC01
              INTO
                   :ADE-ID-ADES
                  ,:ADE-ID-POLI
                  ,:ADE-ID-MOVI-CRZ
                  ,:ADE-ID-MOVI-CHIU
                   :IND-ADE-ID-MOVI-CHIU
                  ,:ADE-DT-INI-EFF-DB
                  ,:ADE-DT-END-EFF-DB
                  ,:ADE-IB-PREV
                   :IND-ADE-IB-PREV
                  ,:ADE-IB-OGG
                   :IND-ADE-IB-OGG
                  ,:ADE-COD-COMP-ANIA
                  ,:ADE-DT-DECOR-DB
                   :IND-ADE-DT-DECOR
                  ,:ADE-DT-SCAD-DB
                   :IND-ADE-DT-SCAD
                  ,:ADE-ETA-A-SCAD
                   :IND-ADE-ETA-A-SCAD
                  ,:ADE-DUR-AA
                   :IND-ADE-DUR-AA
                  ,:ADE-DUR-MM
                   :IND-ADE-DUR-MM
                  ,:ADE-DUR-GG
                   :IND-ADE-DUR-GG
                  ,:ADE-TP-RGM-FISC
                  ,:ADE-TP-RIAT
                   :IND-ADE-TP-RIAT
                  ,:ADE-TP-MOD-PAG-TIT
                  ,:ADE-TP-IAS
                   :IND-ADE-TP-IAS
                  ,:ADE-DT-VARZ-TP-IAS-DB
                   :IND-ADE-DT-VARZ-TP-IAS
                  ,:ADE-PRE-NET-IND
                   :IND-ADE-PRE-NET-IND
                  ,:ADE-PRE-LRD-IND
                   :IND-ADE-PRE-LRD-IND
                  ,:ADE-RAT-LRD-IND
                   :IND-ADE-RAT-LRD-IND
                  ,:ADE-PRSTZ-INI-IND
                   :IND-ADE-PRSTZ-INI-IND
                  ,:ADE-FL-COINC-ASSTO
                   :IND-ADE-FL-COINC-ASSTO
                  ,:ADE-IB-DFLT
                   :IND-ADE-IB-DFLT
                  ,:ADE-MOD-CALC
                   :IND-ADE-MOD-CALC
                  ,:ADE-TP-FNT-CNBTVA
                   :IND-ADE-TP-FNT-CNBTVA
                  ,:ADE-IMP-AZ
                   :IND-ADE-IMP-AZ
                  ,:ADE-IMP-ADER
                   :IND-ADE-IMP-ADER
                  ,:ADE-IMP-TFR
                   :IND-ADE-IMP-TFR
                  ,:ADE-IMP-VOLO
                   :IND-ADE-IMP-VOLO
                  ,:ADE-PC-AZ
                   :IND-ADE-PC-AZ
                  ,:ADE-PC-ADER
                   :IND-ADE-PC-ADER
                  ,:ADE-PC-TFR
                   :IND-ADE-PC-TFR
                  ,:ADE-PC-VOLO
                   :IND-ADE-PC-VOLO
                  ,:ADE-DT-NOVA-RGM-FISC-DB
                   :IND-ADE-DT-NOVA-RGM-FISC
                  ,:ADE-FL-ATTIV
                   :IND-ADE-FL-ATTIV
                  ,:ADE-IMP-REC-RIT-VIS
                   :IND-ADE-IMP-REC-RIT-VIS
                  ,:ADE-IMP-REC-RIT-ACC
                   :IND-ADE-IMP-REC-RIT-ACC
                  ,:ADE-FL-VARZ-STAT-TBGC
                   :IND-ADE-FL-VARZ-STAT-TBGC
                  ,:ADE-FL-PROVZA-MIGRAZ
                   :IND-ADE-FL-PROVZA-MIGRAZ
                  ,:ADE-IMPB-VIS-DA-REC
                   :IND-ADE-IMPB-VIS-DA-REC
                  ,:ADE-DT-DECOR-PREST-BAN-DB
                   :IND-ADE-DT-DECOR-PREST-BAN
                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
                   :IND-ADE-DT-EFF-VARZ-STAT-T
                  ,:ADE-DS-RIGA
                  ,:ADE-DS-OPER-SQL
                  ,:ADE-DS-VER
                  ,:ADE-DS-TS-INI-CPTZ
                  ,:ADE-DS-TS-END-CPTZ
                  ,:ADE-DS-UTENTE
                  ,:ADE-DS-STATO-ELAB
                  ,:ADE-CUM-CNBT-CAP
                   :IND-ADE-CUM-CNBT-CAP
                  ,:ADE-IMP-GAR-CNBT
                   :IND-ADE-IMP-GAR-CNBT
                  ,:ADE-DT-ULT-CONS-CNBT-DB
                   :IND-ADE-DT-ULT-CONS-CNBT
                  ,:ADE-IDEN-ISC-FND
                   :IND-ADE-IDEN-ISC-FND
                  ,:ADE-NUM-RAT-PIAN
                   :IND-ADE-NUM-RAT-PIAN
                  ,:ADE-DT-PRESC-DB
                   :IND-ADE-DT-PRESC
                  ,:ADE-CONCS-PREST
                   :IND-ADE-CONCS-PREST
                  ,:POL-ID-POLI
                  ,:POL-ID-MOVI-CRZ
                  ,:POL-ID-MOVI-CHIU
                   :IND-POL-ID-MOVI-CHIU
                  ,:POL-IB-OGG
                   :IND-POL-IB-OGG
                  ,:POL-IB-PROP
                  ,:POL-DT-PROP-DB
                   :IND-POL-DT-PROP
                  ,:POL-DT-INI-EFF-DB
                  ,:POL-DT-END-EFF-DB
                  ,:POL-COD-COMP-ANIA
                  ,:POL-DT-DECOR-DB
                  ,:POL-DT-EMIS-DB
                  ,:POL-TP-POLI
                  ,:POL-DUR-AA
                   :IND-POL-DUR-AA
                  ,:POL-DUR-MM
                   :IND-POL-DUR-MM
                  ,:POL-DT-SCAD-DB
                   :IND-POL-DT-SCAD
                  ,:POL-COD-PROD
                  ,:POL-DT-INI-VLDT-PROD-DB
                  ,:POL-COD-CONV
                   :IND-POL-COD-CONV
                  ,:POL-COD-RAMO
                   :IND-POL-COD-RAMO
                  ,:POL-DT-INI-VLDT-CONV-DB
                   :IND-POL-DT-INI-VLDT-CONV
                  ,:POL-DT-APPLZ-CONV-DB
                   :IND-POL-DT-APPLZ-CONV
                  ,:POL-TP-FRM-ASSVA
                  ,:POL-TP-RGM-FISC
                   :IND-POL-TP-RGM-FISC
                  ,:POL-FL-ESTAS
                   :IND-POL-FL-ESTAS
                  ,:POL-FL-RSH-COMUN
                   :IND-POL-FL-RSH-COMUN
                  ,:POL-FL-RSH-COMUN-COND
                   :IND-POL-FL-RSH-COMUN-COND
                  ,:POL-TP-LIV-GENZ-TIT
                  ,:POL-FL-COP-FINANZ
                   :IND-POL-FL-COP-FINANZ
                  ,:POL-TP-APPLZ-DIR
                   :IND-POL-TP-APPLZ-DIR
                  ,:POL-SPE-MED
                   :IND-POL-SPE-MED
                  ,:POL-DIR-EMIS
                   :IND-POL-DIR-EMIS
                  ,:POL-DIR-1O-VERS
                   :IND-POL-DIR-1O-VERS
                  ,:POL-DIR-VERS-AGG
                   :IND-POL-DIR-VERS-AGG
                  ,:POL-COD-DVS
                   :IND-POL-COD-DVS
                  ,:POL-FL-FNT-AZ
                   :IND-POL-FL-FNT-AZ
                  ,:POL-FL-FNT-ADER
                   :IND-POL-FL-FNT-ADER
                  ,:POL-FL-FNT-TFR
                   :IND-POL-FL-FNT-TFR
                  ,:POL-FL-FNT-VOLO
                   :IND-POL-FL-FNT-VOLO
                  ,:POL-TP-OPZ-A-SCAD
                   :IND-POL-TP-OPZ-A-SCAD
                  ,:POL-AA-DIFF-PROR-DFLT
                   :IND-POL-AA-DIFF-PROR-DFLT
                  ,:POL-FL-VER-PROD
                   :IND-POL-FL-VER-PROD
                  ,:POL-DUR-GG
                   :IND-POL-DUR-GG
                  ,:POL-DIR-QUIET
                   :IND-POL-DIR-QUIET
                  ,:POL-TP-PTF-ESTNO
                   :IND-POL-TP-PTF-ESTNO
                  ,:POL-FL-CUM-PRE-CNTR
                   :IND-POL-FL-CUM-PRE-CNTR
                  ,:POL-FL-AMMB-MOVI
                   :IND-POL-FL-AMMB-MOVI
                  ,:POL-CONV-GECO
                   :IND-POL-CONV-GECO
                  ,:POL-DS-RIGA
                  ,:POL-DS-OPER-SQL
                  ,:POL-DS-VER
                  ,:POL-DS-TS-INI-CPTZ
                  ,:POL-DS-TS-END-CPTZ
                  ,:POL-DS-UTENTE
                  ,:POL-DS-STATO-ELAB
                  ,:POL-FL-SCUDO-FISC
                   :IND-POL-FL-SCUDO-FISC
                  ,:POL-FL-TRASFE
                   :IND-POL-FL-TRASFE
                  ,:POL-FL-TFR-STRC
                   :IND-POL-FL-TFR-STRC
                  ,:POL-DT-PRESC-DB
                   :IND-POL-DT-PRESC
                  ,:POL-COD-CONV-AGG
                   :IND-POL-COD-CONV-AGG
                  ,:POL-SUBCAT-PROD
                   :IND-POL-SUBCAT-PROD
                  ,:POL-FL-QUEST-ADEGZ-ASS
                   :IND-POL-FL-QUEST-ADEGZ-ASS
                  ,:POL-COD-TPA
                   :IND-POL-COD-TPA
                  ,:POL-ID-ACC-COMM
                   :IND-POL-ID-ACC-COMM
                  ,:POL-FL-POLI-CPI-PR
                   :IND-POL-FL-POLI-CPI-PR
                  ,:POL-FL-POLI-BUNDLING
                   :IND-POL-FL-POLI-BUNDLING
                  ,:POL-IND-POLI-PRIN-COLL
                   :IND-POL-IND-POLI-PRIN-COLL
                  ,:POL-FL-VND-BUNDLE
                   :IND-POL-FL-VND-BUNDLE
                  ,:POL-IB-BS
                   :IND-POL-IB-BS
                  ,:POL-FL-POLI-IFP
                   :IND-POL-FL-POLI-IFP
                  ,:STB-ID-STAT-OGG-BUS
                  ,:STB-ID-OGG
                  ,:STB-TP-OGG
                  ,:STB-ID-MOVI-CRZ
                  ,:STB-ID-MOVI-CHIU
                   :IND-STB-ID-MOVI-CHIU
                  ,:STB-DT-INI-EFF-DB
                  ,:STB-DT-END-EFF-DB
                  ,:STB-COD-COMP-ANIA
                  ,:STB-TP-STAT-BUS
                  ,:STB-TP-CAUS
                  ,:STB-DS-RIGA
                  ,:STB-DS-OPER-SQL
                  ,:STB-DS-VER
                  ,:STB-DS-TS-INI-CPTZ
                  ,:STB-DS-TS-END-CPTZ
                  ,:STB-DS-UTENTE
                  ,:STB-DS-STATO-ELAB
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-SC01 THRU A370-SC01-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       A390-SC01-EX.
           EXIT.
      ******************************************************************
       A400-FINE.

           CONTINUE.

       A400-EX.
           EXIT.
      ******************************************************************
       SC02-SELECTION-CURSOR-02.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-SC02           THRU A310-SC02-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A360-OPEN-CURSOR-SC02      THRU A360-SC02-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR-SC02     THRU A370-SC02-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST-SC02      THRU A380-SC02-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT-SC02       THRU A390-SC02-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A320-UPDATE-SC02           THRU A320-SC02-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER          TO TRUE
           END-EVALUATE.

       SC02-EX.
           EXIT.

      ******************************************************************
       A305-DECLARE-CURSOR-SC02.

           EXEC SQL
                DECLARE CUR-SC02 CURSOR WITH HOLD FOR
             SELECT
                 A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.IB_PREV
                ,A.IB_OGG
                ,A.COD_COMP_ANIA
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.ETA_A_SCAD
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.TP_RGM_FISC
                ,A.TP_RIAT
                ,A.TP_MOD_PAG_TIT
                ,A.TP_IAS
                ,A.DT_VARZ_TP_IAS
                ,A.PRE_NET_IND
                ,A.PRE_LRD_IND
                ,A.RAT_LRD_IND
                ,A.PRSTZ_INI_IND
                ,A.FL_COINC_ASSTO
                ,A.IB_DFLT
                ,A.MOD_CALC
                ,A.TP_FNT_CNBTVA
                ,A.IMP_AZ
                ,A.IMP_ADER
                ,A.IMP_TFR
                ,A.IMP_VOLO
                ,A.PC_AZ
                ,A.PC_ADER
                ,A.PC_TFR
                ,A.PC_VOLO
                ,A.DT_NOVA_RGM_FISC
                ,A.FL_ATTIV
                ,A.IMP_REC_RIT_VIS
                ,A.IMP_REC_RIT_ACC
                ,A.FL_VARZ_STAT_TBGC
                ,A.FL_PROVZA_MIGRAZ
                ,A.IMPB_VIS_DA_REC
                ,A.DT_DECOR_PREST_BAN
                ,A.DT_EFF_VARZ_STAT_T
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.CUM_CNBT_CAP
                ,A.IMP_GAR_CNBT
                ,A.DT_ULT_CONS_CNBT
                ,A.IDEN_ISC_FND
                ,A.NUM_RAT_PIAN
                ,A.DT_PRESC
                ,A.CONCS_PREST
                ,B.ID_POLI
                ,B.ID_MOVI_CRZ
                ,B.ID_MOVI_CHIU
                ,B.IB_OGG
                ,B.IB_PROP
                ,B.DT_PROP
                ,B.DT_INI_EFF
                ,B.DT_END_EFF
                ,B.COD_COMP_ANIA
                ,B.DT_DECOR
                ,B.DT_EMIS
                ,B.TP_POLI
                ,B.DUR_AA
                ,B.DUR_MM
                ,B.DT_SCAD
                ,B.COD_PROD
                ,B.DT_INI_VLDT_PROD
                ,B.COD_CONV
                ,B.COD_RAMO
                ,B.DT_INI_VLDT_CONV
                ,B.DT_APPLZ_CONV
                ,B.TP_FRM_ASSVA
                ,B.TP_RGM_FISC
                ,B.FL_ESTAS
                ,B.FL_RSH_COMUN
                ,B.FL_RSH_COMUN_COND
                ,B.TP_LIV_GENZ_TIT
                ,B.FL_COP_FINANZ
                ,B.TP_APPLZ_DIR
                ,B.SPE_MED
                ,B.DIR_EMIS
                ,B.DIR_1O_VERS
                ,B.DIR_VERS_AGG
                ,B.COD_DVS
                ,B.FL_FNT_AZ
                ,B.FL_FNT_ADER
                ,B.FL_FNT_TFR
                ,B.FL_FNT_VOLO
                ,B.TP_OPZ_A_SCAD
                ,B.AA_DIFF_PROR_DFLT
                ,B.FL_VER_PROD
                ,B.DUR_GG
                ,B.DIR_QUIET
                ,B.TP_PTF_ESTNO
                ,B.FL_CUM_PRE_CNTR
                ,B.FL_AMMB_MOVI
                ,B.CONV_GECO
                ,B.DS_RIGA
                ,B.DS_OPER_SQL
                ,B.DS_VER
                ,B.DS_TS_INI_CPTZ
                ,B.DS_TS_END_CPTZ
                ,B.DS_UTENTE
                ,B.DS_STATO_ELAB
                ,B.FL_SCUDO_FISC
                ,B.FL_TRASFE
                ,B.FL_TFR_STRC
                ,B.DT_PRESC
                ,B.COD_CONV_AGG
                ,B.SUBCAT_PROD
                ,B.FL_QUEST_ADEGZ_ASS
                ,B.COD_TPA
                ,B.ID_ACC_COMM
                ,B.FL_POLI_CPI_PR
                ,B.FL_POLI_BUNDLING
                ,B.IND_POLI_PRIN_COLL
                ,B.FL_VND_BUNDLE
                ,B.IB_BS
                ,B.FL_POLI_IFP
                ,C.ID_STAT_OGG_BUS
                ,C.ID_OGG
                ,C.TP_OGG
                ,C.ID_MOVI_CRZ
                ,C.ID_MOVI_CHIU
                ,C.DT_INI_EFF
                ,C.DT_END_EFF
                ,C.COD_COMP_ANIA
                ,C.TP_STAT_BUS
                ,C.TP_CAUS
                ,C.DS_RIGA
                ,C.DS_OPER_SQL
                ,C.DS_VER
                ,C.DS_TS_INI_CPTZ
                ,C.DS_TS_END_CPTZ
                ,C.DS_UTENTE
                ,C.DS_STATO_ELAB
             FROM ADES         A,
                  POLI         B,
                  STAT_OGG_BUS C
             WHERE B.COD_PROD        =  :WLB-COD-PROD
               AND A.ID_POLI         =   B.ID_POLI
               AND A.ID_ADES         =   C.ID_OGG
               AND C.TP_OGG          =  'AD'
               AND C.TP_STAT_BUS     =  'VI'
      *        OR
      *            (C.TP_STAT_BUS    =  'ST'  AND
      *            EXISTS (SELECT
      *                  D.ID_MOVI_FINRIO
      *
      *               FROM  MOVI_FINRIO D
      *               WHERE A.ID_ADES = D.ID_ADES
      *                 AND D.STAT_MOVI IN ('AL','IE')
      *                 AND D.TP_MOVI_FINRIO = 'DI'
      *                 AND D.COD_COMP_ANIA   =
      *                     :IDSV0003-CODICE-COMPAGNIA-ANIA
      *                 AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
      *                 AND D.DT_END_EFF     >   :WS-DT-PTF-X
      *                 AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
      *                 AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
             AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
             AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
             AND A.DT_END_EFF     >   :WS-DT-PTF-X
             AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
             AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
             AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
             AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
             AND B.DT_END_EFF     >   :WS-DT-PTF-X
             AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
             AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
             AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
             AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
             AND C.DT_END_EFF     >   :WS-DT-PTF-X
             AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
             AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
      *      AND A.DS_STATO_ELAB IN (
      *                              :IABV0002-STATE-01,
      *                              :IABV0002-STATE-02,
      *                              :IABV0002-STATE-03,
      *                              :IABV0002-STATE-04,
      *                              :IABV0002-STATE-05,
      *                              :IABV0002-STATE-06,
      *                              :IABV0002-STATE-07,
      *                              :IABV0002-STATE-08,
      *                              :IABV0002-STATE-09,
      *                              :IABV0002-STATE-10
      *                                  )
      *        AND NOT A.DS_VER     = :IABV0009-VERSIONING
               ORDER BY A.ID_POLI, A.ID_ADES
           END-EXEC.

       A305-SC02-EX.
           EXIT.
      ******************************************************************
       A310-SELECT-SC02.

           EXEC SQL
             SELECT
                 A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.IB_PREV
                ,A.IB_OGG
                ,A.COD_COMP_ANIA
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.ETA_A_SCAD
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.TP_RGM_FISC
                ,A.TP_RIAT
                ,A.TP_MOD_PAG_TIT
                ,A.TP_IAS
                ,A.DT_VARZ_TP_IAS
                ,A.PRE_NET_IND
                ,A.PRE_LRD_IND
                ,A.RAT_LRD_IND
                ,A.PRSTZ_INI_IND
                ,A.FL_COINC_ASSTO
                ,A.IB_DFLT
                ,A.MOD_CALC
                ,A.TP_FNT_CNBTVA
                ,A.IMP_AZ
                ,A.IMP_ADER
                ,A.IMP_TFR
                ,A.IMP_VOLO
                ,A.PC_AZ
                ,A.PC_ADER
                ,A.PC_TFR
                ,A.PC_VOLO
                ,A.DT_NOVA_RGM_FISC
                ,A.FL_ATTIV
                ,A.IMP_REC_RIT_VIS
                ,A.IMP_REC_RIT_ACC
                ,A.FL_VARZ_STAT_TBGC
                ,A.FL_PROVZA_MIGRAZ
                ,A.IMPB_VIS_DA_REC
                ,A.DT_DECOR_PREST_BAN
                ,A.DT_EFF_VARZ_STAT_T
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.CUM_CNBT_CAP
                ,A.IMP_GAR_CNBT
                ,A.DT_ULT_CONS_CNBT
                ,A.IDEN_ISC_FND
                ,A.NUM_RAT_PIAN
                ,A.DT_PRESC
                ,A.CONCS_PREST
                ,B.ID_POLI
                ,B.ID_MOVI_CRZ
                ,B.ID_MOVI_CHIU
                ,B.IB_OGG
                ,B.IB_PROP
                ,B.DT_PROP
                ,B.DT_INI_EFF
                ,B.DT_END_EFF
                ,B.COD_COMP_ANIA
                ,B.DT_DECOR
                ,B.DT_EMIS
                ,B.TP_POLI
                ,B.DUR_AA
                ,B.DUR_MM
                ,B.DT_SCAD
                ,B.COD_PROD
                ,B.DT_INI_VLDT_PROD
                ,B.COD_CONV
                ,B.COD_RAMO
                ,B.DT_INI_VLDT_CONV
                ,B.DT_APPLZ_CONV
                ,B.TP_FRM_ASSVA
                ,B.TP_RGM_FISC
                ,B.FL_ESTAS
                ,B.FL_RSH_COMUN
                ,B.FL_RSH_COMUN_COND
                ,B.TP_LIV_GENZ_TIT
                ,B.FL_COP_FINANZ
                ,B.TP_APPLZ_DIR
                ,B.SPE_MED
                ,B.DIR_EMIS
                ,B.DIR_1O_VERS
                ,B.DIR_VERS_AGG
                ,B.COD_DVS
                ,B.FL_FNT_AZ
                ,B.FL_FNT_ADER
                ,B.FL_FNT_TFR
                ,B.FL_FNT_VOLO
                ,B.TP_OPZ_A_SCAD
                ,B.AA_DIFF_PROR_DFLT
                ,B.FL_VER_PROD
                ,B.DUR_GG
                ,B.DIR_QUIET
                ,B.TP_PTF_ESTNO
                ,B.FL_CUM_PRE_CNTR
                ,B.FL_AMMB_MOVI
                ,B.CONV_GECO
                ,B.DS_RIGA
                ,B.DS_OPER_SQL
                ,B.DS_VER
                ,B.DS_TS_INI_CPTZ
                ,B.DS_TS_END_CPTZ
                ,B.DS_UTENTE
                ,B.DS_STATO_ELAB
                ,B.FL_SCUDO_FISC
                ,B.FL_TRASFE
                ,B.FL_TFR_STRC
                ,B.DT_PRESC
                ,B.COD_CONV_AGG
                ,B.SUBCAT_PROD
                ,B.FL_QUEST_ADEGZ_ASS
                ,B.COD_TPA
                ,B.ID_ACC_COMM
                ,B.FL_POLI_CPI_PR
                ,B.FL_POLI_BUNDLING
                ,B.IND_POLI_PRIN_COLL
                ,B.FL_VND_BUNDLE
                ,B.IB_BS
                ,B.FL_POLI_IFP
                ,C.ID_STAT_OGG_BUS
                ,C.ID_OGG
                ,C.TP_OGG
                ,C.ID_MOVI_CRZ
                ,C.ID_MOVI_CHIU
                ,C.DT_INI_EFF
                ,C.DT_END_EFF
                ,C.COD_COMP_ANIA
                ,C.TP_STAT_BUS
                ,C.TP_CAUS
                ,C.DS_RIGA
                ,C.DS_OPER_SQL
                ,C.DS_VER
                ,C.DS_TS_INI_CPTZ
                ,C.DS_TS_END_CPTZ
                ,C.DS_UTENTE
                ,C.DS_STATO_ELAB
             INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
               ,:POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
               ,:STB-ID-STAT-OGG-BUS
               ,:STB-ID-OGG
               ,:STB-TP-OGG
               ,:STB-ID-MOVI-CRZ
               ,:STB-ID-MOVI-CHIU
                :IND-STB-ID-MOVI-CHIU
               ,:STB-DT-INI-EFF-DB
               ,:STB-DT-END-EFF-DB
               ,:STB-COD-COMP-ANIA
               ,:STB-TP-STAT-BUS
               ,:STB-TP-CAUS
               ,:STB-DS-RIGA
               ,:STB-DS-OPER-SQL
               ,:STB-DS-VER
               ,:STB-DS-TS-INI-CPTZ
               ,:STB-DS-TS-END-CPTZ
               ,:STB-DS-UTENTE
               ,:STB-DS-STATO-ELAB
             FROM ADES         A,
                  POLI         B,
                  STAT_OGG_BUS C
             WHERE B.COD_PROD        =  :WLB-COD-PROD
               AND A.ID_POLI         =   B.ID_POLI
               AND A.ID_ADES         =   C.ID_OGG
               AND C.TP_OGG          =  'AD'
               AND C.TP_STAT_BUS     =  'VI'
      *        OR
      *            (C.TP_STAT_BUS    =  'ST'  AND
      *            EXISTS (SELECT
      *                  D.ID_MOVI_FINRIO
      *
      *               FROM  MOVI_FINRIO D
      *               WHERE A.ID_ADES = D.ID_ADES
      *                 AND D.STAT_MOVI IN ('AL','IE')
      *                 AND D.TP_MOVI_FINRIO = 'DI'
      *                 AND D.COD_COMP_ANIA   =
      *                     :IDSV0003-CODICE-COMPAGNIA-ANIA
      *                 AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
      *                 AND D.DT_END_EFF     >   :WS-DT-PTF-X
      *                 AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
      *                 AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
             AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
             AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
             AND A.DT_END_EFF     >   :WS-DT-PTF-X
             AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
             AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
             AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
             AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
             AND B.DT_END_EFF     >   :WS-DT-PTF-X
             AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
             AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
             AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
             AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
             AND C.DT_END_EFF     >   :WS-DT-PTF-X
             AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
             AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
      *      AND A.DS_STATO_ELAB IN (
      *                              :IABV0002-STATE-01,
      *                              :IABV0002-STATE-02,
      *                              :IABV0002-STATE-03,
      *                              :IABV0002-STATE-04,
      *                              :IABV0002-STATE-05,
      *                              :IABV0002-STATE-06,
      *                              :IABV0002-STATE-07,
      *                              :IABV0002-STATE-08,
      *                              :IABV0002-STATE-09,
      *                              :IABV0002-STATE-10
      *                                  )
      *        AND NOT A.DS_VER     = :IABV0009-VERSIONING
               ORDER BY A.ID_POLI, A.ID_ADES
              FETCH FIRST ROW ONLY
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-SC02-EX.
           EXIT.

      ******************************************************************
       A320-UPDATE-SC02.

           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
                   PERFORM A330-UPDATE-PK-SC02         THRU A330-SC02-EX
              WHEN IDSV0003-WHERE-CONDITION-02
                   PERFORM A340-UPDATE-WHERE-COND-SC02 THRU A340-SC02-EX
              WHEN IDSV0003-FIRST-ACTION
                   PERFORM A345-UPDATE-FIRST-ACTION-SC02
                                                       THRU A345-SC02-EX
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
           END-EVALUATE.

       A320-SC02-EX.
           EXIT.

      ******************************************************************

       A330-UPDATE-PK-SC02.


           PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX

           EXEC SQL
                UPDATE ADES
                SET
                   DS_OPER_SQL            = :ADE-DS-OPER-SQL
      *           ,DS_VER                 = :IABV0009-VERSIONING
                  ,DS_UTENTE              = :ADE-DS-UTENTE
                  ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
                WHERE
                   DS_RIGA                = :ADE-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A330-SC02-EX.
           EXIT.

      ******************************************************************

       A340-UPDATE-WHERE-COND-SC02.

           CONTINUE.

       A340-SC02-EX.
           EXIT.
      ******************************************************************
       A345-UPDATE-FIRST-ACTION-SC02.

           CONTINUE.

       A345-SC02-EX.
           EXIT.

      ******************************************************************

       A360-OPEN-CURSOR-SC02.

           PERFORM A305-DECLARE-CURSOR-SC02 THRU A305-SC02-EX.

           EXEC SQL
                OPEN CUR-SC02
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A360-SC02-EX.
           EXIT.

      ******************************************************************

       A370-CLOSE-CURSOR-SC02.

           EXEC SQL
                CLOSE CUR-SC02
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A370-SC02-EX.
           EXIT.

      ******************************************************************

       A380-FETCH-FIRST-SC02.

           PERFORM A360-OPEN-CURSOR-SC02    THRU A360-SC02-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-SC02  THRU A390-SC02-EX
           END-IF.

       A380-SC02-EX.
           EXIT.

      ******************************************************************

       A390-FETCH-NEXT-SC02.

              EXEC SQL
                   FETCH CUR-SC02
              INTO
                   :ADE-ID-ADES
                  ,:ADE-ID-POLI
                  ,:ADE-ID-MOVI-CRZ
                  ,:ADE-ID-MOVI-CHIU
                   :IND-ADE-ID-MOVI-CHIU
                  ,:ADE-DT-INI-EFF-DB
                  ,:ADE-DT-END-EFF-DB
                  ,:ADE-IB-PREV
                   :IND-ADE-IB-PREV
                  ,:ADE-IB-OGG
                   :IND-ADE-IB-OGG
                  ,:ADE-COD-COMP-ANIA
                  ,:ADE-DT-DECOR-DB
                   :IND-ADE-DT-DECOR
                  ,:ADE-DT-SCAD-DB
                   :IND-ADE-DT-SCAD
                  ,:ADE-ETA-A-SCAD
                   :IND-ADE-ETA-A-SCAD
                  ,:ADE-DUR-AA
                   :IND-ADE-DUR-AA
                  ,:ADE-DUR-MM
                   :IND-ADE-DUR-MM
                  ,:ADE-DUR-GG
                   :IND-ADE-DUR-GG
                  ,:ADE-TP-RGM-FISC
                  ,:ADE-TP-RIAT
                   :IND-ADE-TP-RIAT
                  ,:ADE-TP-MOD-PAG-TIT
                  ,:ADE-TP-IAS
                   :IND-ADE-TP-IAS
                  ,:ADE-DT-VARZ-TP-IAS-DB
                   :IND-ADE-DT-VARZ-TP-IAS
                  ,:ADE-PRE-NET-IND
                   :IND-ADE-PRE-NET-IND
                  ,:ADE-PRE-LRD-IND
                   :IND-ADE-PRE-LRD-IND
                  ,:ADE-RAT-LRD-IND
                   :IND-ADE-RAT-LRD-IND
                  ,:ADE-PRSTZ-INI-IND
                   :IND-ADE-PRSTZ-INI-IND
                  ,:ADE-FL-COINC-ASSTO
                   :IND-ADE-FL-COINC-ASSTO
                  ,:ADE-IB-DFLT
                   :IND-ADE-IB-DFLT
                  ,:ADE-MOD-CALC
                   :IND-ADE-MOD-CALC
                  ,:ADE-TP-FNT-CNBTVA
                   :IND-ADE-TP-FNT-CNBTVA
                  ,:ADE-IMP-AZ
                   :IND-ADE-IMP-AZ
                  ,:ADE-IMP-ADER
                   :IND-ADE-IMP-ADER
                  ,:ADE-IMP-TFR
                   :IND-ADE-IMP-TFR
                  ,:ADE-IMP-VOLO
                   :IND-ADE-IMP-VOLO
                  ,:ADE-PC-AZ
                   :IND-ADE-PC-AZ
                  ,:ADE-PC-ADER
                   :IND-ADE-PC-ADER
                  ,:ADE-PC-TFR
                   :IND-ADE-PC-TFR
                  ,:ADE-PC-VOLO
                   :IND-ADE-PC-VOLO
                  ,:ADE-DT-NOVA-RGM-FISC-DB
                   :IND-ADE-DT-NOVA-RGM-FISC
                  ,:ADE-FL-ATTIV
                   :IND-ADE-FL-ATTIV
                  ,:ADE-IMP-REC-RIT-VIS
                   :IND-ADE-IMP-REC-RIT-VIS
                  ,:ADE-IMP-REC-RIT-ACC
                   :IND-ADE-IMP-REC-RIT-ACC
                  ,:ADE-FL-VARZ-STAT-TBGC
                   :IND-ADE-FL-VARZ-STAT-TBGC
                  ,:ADE-FL-PROVZA-MIGRAZ
                   :IND-ADE-FL-PROVZA-MIGRAZ
                  ,:ADE-IMPB-VIS-DA-REC
                   :IND-ADE-IMPB-VIS-DA-REC
                  ,:ADE-DT-DECOR-PREST-BAN-DB
                   :IND-ADE-DT-DECOR-PREST-BAN
                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
                   :IND-ADE-DT-EFF-VARZ-STAT-T
                  ,:ADE-DS-RIGA
                  ,:ADE-DS-OPER-SQL
                  ,:ADE-DS-VER
                  ,:ADE-DS-TS-INI-CPTZ
                  ,:ADE-DS-TS-END-CPTZ
                  ,:ADE-DS-UTENTE
                  ,:ADE-DS-STATO-ELAB
                  ,:ADE-CUM-CNBT-CAP
                   :IND-ADE-CUM-CNBT-CAP
                  ,:ADE-IMP-GAR-CNBT
                   :IND-ADE-IMP-GAR-CNBT
                  ,:ADE-DT-ULT-CONS-CNBT-DB
                   :IND-ADE-DT-ULT-CONS-CNBT
                  ,:ADE-IDEN-ISC-FND
                   :IND-ADE-IDEN-ISC-FND
                  ,:ADE-NUM-RAT-PIAN
                   :IND-ADE-NUM-RAT-PIAN
                  ,:ADE-DT-PRESC-DB
                   :IND-ADE-DT-PRESC
                  ,:ADE-CONCS-PREST
                   :IND-ADE-CONCS-PREST
                  ,:POL-ID-POLI
                  ,:POL-ID-MOVI-CRZ
                  ,:POL-ID-MOVI-CHIU
                   :IND-POL-ID-MOVI-CHIU
                  ,:POL-IB-OGG
                   :IND-POL-IB-OGG
                  ,:POL-IB-PROP
                  ,:POL-DT-PROP-DB
                   :IND-POL-DT-PROP
                  ,:POL-DT-INI-EFF-DB
                  ,:POL-DT-END-EFF-DB
                  ,:POL-COD-COMP-ANIA
                  ,:POL-DT-DECOR-DB
                  ,:POL-DT-EMIS-DB
                  ,:POL-TP-POLI
                  ,:POL-DUR-AA
                   :IND-POL-DUR-AA
                  ,:POL-DUR-MM
                   :IND-POL-DUR-MM
                  ,:POL-DT-SCAD-DB
                   :IND-POL-DT-SCAD
                  ,:POL-COD-PROD
                  ,:POL-DT-INI-VLDT-PROD-DB
                  ,:POL-COD-CONV
                   :IND-POL-COD-CONV
                  ,:POL-COD-RAMO
                   :IND-POL-COD-RAMO
                  ,:POL-DT-INI-VLDT-CONV-DB
                   :IND-POL-DT-INI-VLDT-CONV
                  ,:POL-DT-APPLZ-CONV-DB
                   :IND-POL-DT-APPLZ-CONV
                  ,:POL-TP-FRM-ASSVA
                  ,:POL-TP-RGM-FISC
                   :IND-POL-TP-RGM-FISC
                  ,:POL-FL-ESTAS
                   :IND-POL-FL-ESTAS
                  ,:POL-FL-RSH-COMUN
                   :IND-POL-FL-RSH-COMUN
                  ,:POL-FL-RSH-COMUN-COND
                   :IND-POL-FL-RSH-COMUN-COND
                  ,:POL-TP-LIV-GENZ-TIT
                  ,:POL-FL-COP-FINANZ
                   :IND-POL-FL-COP-FINANZ
                  ,:POL-TP-APPLZ-DIR
                   :IND-POL-TP-APPLZ-DIR
                  ,:POL-SPE-MED
                   :IND-POL-SPE-MED
                  ,:POL-DIR-EMIS
                   :IND-POL-DIR-EMIS
                  ,:POL-DIR-1O-VERS
                   :IND-POL-DIR-1O-VERS
                  ,:POL-DIR-VERS-AGG
                   :IND-POL-DIR-VERS-AGG
                  ,:POL-COD-DVS
                   :IND-POL-COD-DVS
                  ,:POL-FL-FNT-AZ
                   :IND-POL-FL-FNT-AZ
                  ,:POL-FL-FNT-ADER
                   :IND-POL-FL-FNT-ADER
                  ,:POL-FL-FNT-TFR
                   :IND-POL-FL-FNT-TFR
                  ,:POL-FL-FNT-VOLO
                   :IND-POL-FL-FNT-VOLO
                  ,:POL-TP-OPZ-A-SCAD
                   :IND-POL-TP-OPZ-A-SCAD
                  ,:POL-AA-DIFF-PROR-DFLT
                   :IND-POL-AA-DIFF-PROR-DFLT
                  ,:POL-FL-VER-PROD
                   :IND-POL-FL-VER-PROD
                  ,:POL-DUR-GG
                   :IND-POL-DUR-GG
                  ,:POL-DIR-QUIET
                   :IND-POL-DIR-QUIET
                  ,:POL-TP-PTF-ESTNO
                   :IND-POL-TP-PTF-ESTNO
                  ,:POL-FL-CUM-PRE-CNTR
                   :IND-POL-FL-CUM-PRE-CNTR
                  ,:POL-FL-AMMB-MOVI
                   :IND-POL-FL-AMMB-MOVI
                  ,:POL-CONV-GECO
                   :IND-POL-CONV-GECO
                  ,:POL-DS-RIGA
                  ,:POL-DS-OPER-SQL
                  ,:POL-DS-VER
                  ,:POL-DS-TS-INI-CPTZ
                  ,:POL-DS-TS-END-CPTZ
                  ,:POL-DS-UTENTE
                  ,:POL-DS-STATO-ELAB
                  ,:POL-FL-SCUDO-FISC
                   :IND-POL-FL-SCUDO-FISC
                  ,:POL-FL-TRASFE
                   :IND-POL-FL-TRASFE
                  ,:POL-FL-TFR-STRC
                   :IND-POL-FL-TFR-STRC
                  ,:POL-DT-PRESC-DB
                   :IND-POL-DT-PRESC
                  ,:POL-COD-CONV-AGG
                   :IND-POL-COD-CONV-AGG
                  ,:POL-SUBCAT-PROD
                   :IND-POL-SUBCAT-PROD
                  ,:POL-FL-QUEST-ADEGZ-ASS
                   :IND-POL-FL-QUEST-ADEGZ-ASS
                  ,:POL-COD-TPA
                   :IND-POL-COD-TPA
                  ,:POL-ID-ACC-COMM
                   :IND-POL-ID-ACC-COMM
                  ,:POL-FL-POLI-CPI-PR
                   :IND-POL-FL-POLI-CPI-PR
                  ,:POL-FL-POLI-BUNDLING
                   :IND-POL-FL-POLI-BUNDLING
                  ,:POL-IND-POLI-PRIN-COLL
                   :IND-POL-IND-POLI-PRIN-COLL
                  ,:POL-FL-VND-BUNDLE
                   :IND-POL-FL-VND-BUNDLE
                  ,:POL-IB-BS
                   :IND-POL-IB-BS
                  ,:POL-FL-POLI-IFP
                   :IND-POL-FL-POLI-IFP
                  ,:STB-ID-STAT-OGG-BUS
                  ,:STB-ID-OGG
                  ,:STB-TP-OGG
                  ,:STB-ID-MOVI-CRZ
                  ,:STB-ID-MOVI-CHIU
                   :IND-STB-ID-MOVI-CHIU
                  ,:STB-DT-INI-EFF-DB
                  ,:STB-DT-END-EFF-DB
                  ,:STB-COD-COMP-ANIA
                  ,:STB-TP-STAT-BUS
                  ,:STB-TP-CAUS
                  ,:STB-DS-RIGA
                  ,:STB-DS-OPER-SQL
                  ,:STB-DS-VER
                  ,:STB-DS-TS-INI-CPTZ
                  ,:STB-DS-TS-END-CPTZ
                  ,:STB-DS-UTENTE
                  ,:STB-DS-STATO-ELAB
              END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-SC02 THRU A370-SC02-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       A390-SC02-EX.
           EXIT.
      ******************************************************************
      *
      ******************************************************************
       SC03-SELECTION-CURSOR-03.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-SC03           THRU A310-SC03-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A360-OPEN-CURSOR-SC03      THRU A360-SC03-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR-SC03     THRU A370-SC03-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST-SC03      THRU A380-SC03-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT-SC03       THRU A390-SC03-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A320-UPDATE-SC03           THRU A320-SC03-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER          TO TRUE
           END-EVALUATE.

       SC03-EX.
           EXIT.

      ******************************************************************
       A305-DECLARE-CURSOR-SC03.

           EXEC SQL
                DECLARE CUR-SC03 CURSOR WITH HOLD FOR
                SELECT
                 A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.IB_PREV
                ,A.IB_OGG
                ,A.COD_COMP_ANIA
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.ETA_A_SCAD
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.TP_RGM_FISC
                ,A.TP_RIAT
                ,A.TP_MOD_PAG_TIT
                ,A.TP_IAS
                ,A.DT_VARZ_TP_IAS
                ,A.PRE_NET_IND
                ,A.PRE_LRD_IND
                ,A.RAT_LRD_IND
                ,A.PRSTZ_INI_IND
                ,A.FL_COINC_ASSTO
                ,A.IB_DFLT
                ,A.MOD_CALC
                ,A.TP_FNT_CNBTVA
                ,A.IMP_AZ
                ,A.IMP_ADER
                ,A.IMP_TFR
                ,A.IMP_VOLO
                ,A.PC_AZ
                ,A.PC_ADER
                ,A.PC_TFR
                ,A.PC_VOLO
                ,A.DT_NOVA_RGM_FISC
                ,A.FL_ATTIV
                ,A.IMP_REC_RIT_VIS
                ,A.IMP_REC_RIT_ACC
                ,A.FL_VARZ_STAT_TBGC
                ,A.FL_PROVZA_MIGRAZ
                ,A.IMPB_VIS_DA_REC
                ,A.DT_DECOR_PREST_BAN
                ,A.DT_EFF_VARZ_STAT_T
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.CUM_CNBT_CAP
                ,A.IMP_GAR_CNBT
                ,A.DT_ULT_CONS_CNBT
                ,A.IDEN_ISC_FND
                ,A.NUM_RAT_PIAN
                ,A.DT_PRESC
                ,A.CONCS_PREST
                ,B.ID_POLI
                ,B.ID_MOVI_CRZ
                ,B.ID_MOVI_CHIU
                ,B.IB_OGG
                ,B.IB_PROP
                ,B.DT_PROP
                ,B.DT_INI_EFF
                ,B.DT_END_EFF
                ,B.COD_COMP_ANIA
                ,B.DT_DECOR
                ,B.DT_EMIS
                ,B.TP_POLI
                ,B.DUR_AA
                ,B.DUR_MM
                ,B.DT_SCAD
                ,B.COD_PROD
                ,B.DT_INI_VLDT_PROD
                ,B.COD_CONV
                ,B.COD_RAMO
                ,B.DT_INI_VLDT_CONV
                ,B.DT_APPLZ_CONV
                ,B.TP_FRM_ASSVA
                ,B.TP_RGM_FISC
                ,B.FL_ESTAS
                ,B.FL_RSH_COMUN
                ,B.FL_RSH_COMUN_COND
                ,B.TP_LIV_GENZ_TIT
                ,B.FL_COP_FINANZ
                ,B.TP_APPLZ_DIR
                ,B.SPE_MED
                ,B.DIR_EMIS
                ,B.DIR_1O_VERS
                ,B.DIR_VERS_AGG
                ,B.COD_DVS
                ,B.FL_FNT_AZ
                ,B.FL_FNT_ADER
                ,B.FL_FNT_TFR
                ,B.FL_FNT_VOLO
                ,B.TP_OPZ_A_SCAD
                ,B.AA_DIFF_PROR_DFLT
                ,B.FL_VER_PROD
                ,B.DUR_GG
                ,B.DIR_QUIET
                ,B.TP_PTF_ESTNO
                ,B.FL_CUM_PRE_CNTR
                ,B.FL_AMMB_MOVI
                ,B.CONV_GECO
                ,B.DS_RIGA
                ,B.DS_OPER_SQL
                ,B.DS_VER
                ,B.DS_TS_INI_CPTZ
                ,B.DS_TS_END_CPTZ
                ,B.DS_UTENTE
                ,B.DS_STATO_ELAB
                ,B.FL_SCUDO_FISC
                ,B.FL_TRASFE
                ,B.FL_TFR_STRC
                ,B.DT_PRESC
                ,B.COD_CONV_AGG
                ,B.SUBCAT_PROD
                ,B.FL_QUEST_ADEGZ_ASS
                ,B.COD_TPA
                ,B.ID_ACC_COMM
                ,B.FL_POLI_CPI_PR
                ,B.FL_POLI_BUNDLING
                ,B.IND_POLI_PRIN_COLL
                ,B.FL_VND_BUNDLE
                ,B.IB_BS
                ,B.FL_POLI_IFP
                ,C.ID_STAT_OGG_BUS
                ,C.ID_OGG
                ,C.TP_OGG
                ,C.ID_MOVI_CRZ
                ,C.ID_MOVI_CHIU
                ,C.DT_INI_EFF
                ,C.DT_END_EFF
                ,C.COD_COMP_ANIA
                ,C.TP_STAT_BUS
                ,C.TP_CAUS
                ,C.DS_RIGA
                ,C.DS_OPER_SQL
                ,C.DS_VER
                ,C.DS_TS_INI_CPTZ
                ,C.DS_TS_END_CPTZ
                ,C.DS_UTENTE
                ,C.DS_STATO_ELAB
             FROM ADES         A,
                  POLI         B,
                  STAT_OGG_BUS C
             WHERE B.IB_OGG   BETWEEN   :WLB-IB-POLI-FIRST AND
                                        :WLB-IB-POLI-LAST
               AND A.ID_POLI         =   B.ID_POLI
               AND A.ID_ADES         =   C.ID_OGG
               AND C.TP_OGG          =  'AD'
               AND C.TP_STAT_BUS     =  'VI'
      *        OR
      *            (C.TP_STAT_BUS    =  'ST'  AND
      *            EXISTS (SELECT
      *                  D.ID_MOVI_FINRIO
      *
      *               FROM  MOVI_FINRIO D
      *               WHERE A.ID_ADES = D.ID_ADES
      *                 AND D.STAT_MOVI IN ('AL','IE')
      *                 AND D.TP_MOVI_FINRIO = 'DI'
      *                 AND D.COD_COMP_ANIA   =
      *                     :IDSV0003-CODICE-COMPAGNIA-ANIA
      *                 AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
      *                 AND D.DT_END_EFF     >   :WS-DT-PTF-X
      *                 AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
      *                 AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
             AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
             AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
             AND A.DT_END_EFF     >   :WS-DT-PTF-X
             AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
             AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
             AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
             AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
             AND B.DT_END_EFF     >   :WS-DT-PTF-X
             AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
             AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
             AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
             AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
             AND C.DT_END_EFF     >   :WS-DT-PTF-X
             AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
             AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
      *      AND A.DS_STATO_ELAB IN (
      *                              :IABV0002-STATE-01,
      *                              :IABV0002-STATE-02,
      *                              :IABV0002-STATE-03,
      *                              :IABV0002-STATE-04,
      *                              :IABV0002-STATE-05,
      *                              :IABV0002-STATE-06,
      *                              :IABV0002-STATE-07,
      *                              :IABV0002-STATE-08,
      *                              :IABV0002-STATE-09,
      *                              :IABV0002-STATE-10
      *                                  )
      *        AND NOT A.DS_VER     = :IABV0009-VERSIONING
               ORDER BY A.ID_POLI, A.ID_ADES
           END-EXEC.

       A305-SC03-EX.
           EXIT.
      ******************************************************************
       A310-SELECT-SC03.

           EXEC SQL
             SELECT
                 A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.IB_PREV
                ,A.IB_OGG
                ,A.COD_COMP_ANIA
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.ETA_A_SCAD
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.TP_RGM_FISC
                ,A.TP_RIAT
                ,A.TP_MOD_PAG_TIT
                ,A.TP_IAS
                ,A.DT_VARZ_TP_IAS
                ,A.PRE_NET_IND
                ,A.PRE_LRD_IND
                ,A.RAT_LRD_IND
                ,A.PRSTZ_INI_IND
                ,A.FL_COINC_ASSTO
                ,A.IB_DFLT
                ,A.MOD_CALC
                ,A.TP_FNT_CNBTVA
                ,A.IMP_AZ
                ,A.IMP_ADER
                ,A.IMP_TFR
                ,A.IMP_VOLO
                ,A.PC_AZ
                ,A.PC_ADER
                ,A.PC_TFR
                ,A.PC_VOLO
                ,A.DT_NOVA_RGM_FISC
                ,A.FL_ATTIV
                ,A.IMP_REC_RIT_VIS
                ,A.IMP_REC_RIT_ACC
                ,A.FL_VARZ_STAT_TBGC
                ,A.FL_PROVZA_MIGRAZ
                ,A.IMPB_VIS_DA_REC
                ,A.DT_DECOR_PREST_BAN
                ,A.DT_EFF_VARZ_STAT_T
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.CUM_CNBT_CAP
                ,A.IMP_GAR_CNBT
                ,A.DT_ULT_CONS_CNBT
                ,A.IDEN_ISC_FND
                ,A.NUM_RAT_PIAN
                ,A.DT_PRESC
                ,A.CONCS_PREST
                ,B.ID_POLI
                ,B.ID_MOVI_CRZ
                ,B.ID_MOVI_CHIU
                ,B.IB_OGG
                ,B.IB_PROP
                ,B.DT_PROP
                ,B.DT_INI_EFF
                ,B.DT_END_EFF
                ,B.COD_COMP_ANIA
                ,B.DT_DECOR
                ,B.DT_EMIS
                ,B.TP_POLI
                ,B.DUR_AA
                ,B.DUR_MM
                ,B.DT_SCAD
                ,B.COD_PROD
                ,B.DT_INI_VLDT_PROD
                ,B.COD_CONV
                ,B.COD_RAMO
                ,B.DT_INI_VLDT_CONV
                ,B.DT_APPLZ_CONV
                ,B.TP_FRM_ASSVA
                ,B.TP_RGM_FISC
                ,B.FL_ESTAS
                ,B.FL_RSH_COMUN
                ,B.FL_RSH_COMUN_COND
                ,B.TP_LIV_GENZ_TIT
                ,B.FL_COP_FINANZ
                ,B.TP_APPLZ_DIR
                ,B.SPE_MED
                ,B.DIR_EMIS
                ,B.DIR_1O_VERS
                ,B.DIR_VERS_AGG
                ,B.COD_DVS
                ,B.FL_FNT_AZ
                ,B.FL_FNT_ADER
                ,B.FL_FNT_TFR
                ,B.FL_FNT_VOLO
                ,B.TP_OPZ_A_SCAD
                ,B.AA_DIFF_PROR_DFLT
                ,B.FL_VER_PROD
                ,B.DUR_GG
                ,B.DIR_QUIET
                ,B.TP_PTF_ESTNO
                ,B.FL_CUM_PRE_CNTR
                ,B.FL_AMMB_MOVI
                ,B.CONV_GECO
                ,B.DS_RIGA
                ,B.DS_OPER_SQL
                ,B.DS_VER
                ,B.DS_TS_INI_CPTZ
                ,B.DS_TS_END_CPTZ
                ,B.DS_UTENTE
                ,B.DS_STATO_ELAB
                ,B.FL_SCUDO_FISC
                ,B.FL_TRASFE
                ,B.FL_TFR_STRC
                ,B.DT_PRESC
                ,B.COD_CONV_AGG
                ,B.SUBCAT_PROD
                ,B.FL_QUEST_ADEGZ_ASS
                ,B.COD_TPA
                ,B.ID_ACC_COMM
                ,B.FL_POLI_CPI_PR
                ,B.FL_POLI_BUNDLING
                ,B.IND_POLI_PRIN_COLL
                ,B.FL_VND_BUNDLE
                ,B.IB_BS
                ,B.FL_POLI_IFP
                ,C.ID_STAT_OGG_BUS
                ,C.ID_OGG
                ,C.TP_OGG
                ,C.ID_MOVI_CRZ
                ,C.ID_MOVI_CHIU
                ,C.DT_INI_EFF
                ,C.DT_END_EFF
                ,C.COD_COMP_ANIA
                ,C.TP_STAT_BUS
                ,C.TP_CAUS
                ,C.DS_RIGA
                ,C.DS_OPER_SQL
                ,C.DS_VER
                ,C.DS_TS_INI_CPTZ
                ,C.DS_TS_END_CPTZ
                ,C.DS_UTENTE
                ,C.DS_STATO_ELAB
             INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
               ,:POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
               ,:STB-ID-STAT-OGG-BUS
               ,:STB-ID-OGG
               ,:STB-TP-OGG
               ,:STB-ID-MOVI-CRZ
               ,:STB-ID-MOVI-CHIU
                :IND-STB-ID-MOVI-CHIU
               ,:STB-DT-INI-EFF-DB
               ,:STB-DT-END-EFF-DB
               ,:STB-COD-COMP-ANIA
               ,:STB-TP-STAT-BUS
               ,:STB-TP-CAUS
               ,:STB-DS-RIGA
               ,:STB-DS-OPER-SQL
               ,:STB-DS-VER
               ,:STB-DS-TS-INI-CPTZ
               ,:STB-DS-TS-END-CPTZ
               ,:STB-DS-UTENTE
               ,:STB-DS-STATO-ELAB
             FROM ADES         A,
                  POLI         B,
                  STAT_OGG_BUS C
             WHERE B.IB_OGG   BETWEEN   :WLB-IB-POLI-FIRST AND
                                        :WLB-IB-POLI-LAST
               AND A.ID_POLI         =   B.ID_POLI
               AND A.ID_ADES         =   C.ID_OGG
               AND C.TP_OGG          =  'AD'
               AND C.TP_STAT_BUS     =  'VI'
      *        OR
      *            (C.TP_STAT_BUS    =  'ST'  AND
      *            EXISTS (SELECT
      *                  D.ID_MOVI_FINRIO
      *
      *               FROM  MOVI_FINRIO D
      *               WHERE A.ID_ADES = D.ID_ADES
      *                 AND D.STAT_MOVI IN ('AL','IE')
      *                 AND D.TP_MOVI_FINRIO = 'DI'
      *                 AND D.COD_COMP_ANIA   =
      *                     :IDSV0003-CODICE-COMPAGNIA-ANIA
      *                 AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
      *                 AND D.DT_END_EFF     >   :WS-DT-PTF-X
      *                 AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
      *                 AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
             AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
             AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
             AND A.DT_END_EFF     >   :WS-DT-PTF-X
             AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
             AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
             AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
             AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
             AND B.DT_END_EFF     >   :WS-DT-PTF-X
             AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
             AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
             AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
             AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
             AND C.DT_END_EFF     >   :WS-DT-PTF-X
             AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
             AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
      *      AND A.DS_STATO_ELAB IN (
      *                              :IABV0002-STATE-01,
      *                              :IABV0002-STATE-02,
      *                              :IABV0002-STATE-03,
      *                              :IABV0002-STATE-04,
      *                              :IABV0002-STATE-05,
      *                              :IABV0002-STATE-06,
      *                              :IABV0002-STATE-07,
      *                              :IABV0002-STATE-08,
      *                              :IABV0002-STATE-09,
      *                              :IABV0002-STATE-10
      *                                  )
      *        AND NOT A.DS_VER     = :IABV0009-VERSIONING
               ORDER BY A.ID_POLI, A.ID_ADES
              FETCH FIRST ROW ONLY
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-SC03-EX.
           EXIT.

      ******************************************************************
       A320-UPDATE-SC03.

           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
                   PERFORM A330-UPDATE-PK-SC03         THRU A330-SC03-EX

              WHEN IDSV0003-WHERE-CONDITION-03
                   PERFORM A340-UPDATE-WHERE-COND-SC03 THRU A340-SC03-EX
              WHEN IDSV0003-FIRST-ACTION
                   PERFORM A345-UPDATE-FIRST-ACTION-SC03
                                                       THRU A345-SC03-EX
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
           END-EVALUATE.

       A320-SC03-EX.
           EXIT.

      ******************************************************************

       A330-UPDATE-PK-SC03.

           PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX

           EXEC SQL
                UPDATE ADES
                SET
                   DS_OPER_SQL            = :ADE-DS-OPER-SQL
      *           ,DS_VER                 = :IABV0009-VERSIONING
                  ,DS_UTENTE              = :ADE-DS-UTENTE
                  ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
                WHERE             DS_RIGA = :ADE-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A330-SC03-EX.
           EXIT.

      ******************************************************************

       A340-UPDATE-WHERE-COND-SC03.

           CONTINUE.

       A340-SC03-EX.
           EXIT.
      ******************************************************************
       A345-UPDATE-FIRST-ACTION-SC03.

           CONTINUE.

       A345-SC03-EX.
           EXIT.
      ******************************************************************

       A360-OPEN-CURSOR-SC03.

           PERFORM A305-DECLARE-CURSOR-SC03 THRU A305-SC03-EX.

           EXEC SQL
                OPEN CUR-SC03
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A360-SC03-EX.
           EXIT.

      ******************************************************************

       A370-CLOSE-CURSOR-SC03.

           EXEC SQL
                CLOSE CUR-SC03
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A370-SC03-EX.
           EXIT.

      ******************************************************************

       A380-FETCH-FIRST-SC03.

           PERFORM A360-OPEN-CURSOR-SC03    THRU A360-SC03-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-SC03  THRU A390-SC03-EX
           END-IF.

       A380-SC03-EX.
           EXIT.

      ******************************************************************

       A390-FETCH-NEXT-SC03.

              EXEC SQL
                   FETCH CUR-SC03
              INTO
                   :ADE-ID-ADES
                  ,:ADE-ID-POLI
                  ,:ADE-ID-MOVI-CRZ
                  ,:ADE-ID-MOVI-CHIU
                   :IND-ADE-ID-MOVI-CHIU
                  ,:ADE-DT-INI-EFF-DB
                  ,:ADE-DT-END-EFF-DB
                  ,:ADE-IB-PREV
                   :IND-ADE-IB-PREV
                  ,:ADE-IB-OGG
                   :IND-ADE-IB-OGG
                  ,:ADE-COD-COMP-ANIA
                  ,:ADE-DT-DECOR-DB
                   :IND-ADE-DT-DECOR
                  ,:ADE-DT-SCAD-DB
                   :IND-ADE-DT-SCAD
                  ,:ADE-ETA-A-SCAD
                   :IND-ADE-ETA-A-SCAD
                  ,:ADE-DUR-AA
                   :IND-ADE-DUR-AA
                  ,:ADE-DUR-MM
                   :IND-ADE-DUR-MM
                  ,:ADE-DUR-GG
                   :IND-ADE-DUR-GG
                  ,:ADE-TP-RGM-FISC
                  ,:ADE-TP-RIAT
                   :IND-ADE-TP-RIAT
                  ,:ADE-TP-MOD-PAG-TIT
                  ,:ADE-TP-IAS
                   :IND-ADE-TP-IAS
                  ,:ADE-DT-VARZ-TP-IAS-DB
                   :IND-ADE-DT-VARZ-TP-IAS
                  ,:ADE-PRE-NET-IND
                   :IND-ADE-PRE-NET-IND
                  ,:ADE-PRE-LRD-IND
                   :IND-ADE-PRE-LRD-IND
                  ,:ADE-RAT-LRD-IND
                   :IND-ADE-RAT-LRD-IND
                  ,:ADE-PRSTZ-INI-IND
                   :IND-ADE-PRSTZ-INI-IND
                  ,:ADE-FL-COINC-ASSTO
                   :IND-ADE-FL-COINC-ASSTO
                  ,:ADE-IB-DFLT
                   :IND-ADE-IB-DFLT
                  ,:ADE-MOD-CALC
                   :IND-ADE-MOD-CALC
                  ,:ADE-TP-FNT-CNBTVA
                   :IND-ADE-TP-FNT-CNBTVA
                  ,:ADE-IMP-AZ
                   :IND-ADE-IMP-AZ
                  ,:ADE-IMP-ADER
                   :IND-ADE-IMP-ADER
                  ,:ADE-IMP-TFR
                   :IND-ADE-IMP-TFR
                  ,:ADE-IMP-VOLO
                   :IND-ADE-IMP-VOLO
                  ,:ADE-PC-AZ
                   :IND-ADE-PC-AZ
                  ,:ADE-PC-ADER
                   :IND-ADE-PC-ADER
                  ,:ADE-PC-TFR
                   :IND-ADE-PC-TFR
                  ,:ADE-PC-VOLO
                   :IND-ADE-PC-VOLO
                  ,:ADE-DT-NOVA-RGM-FISC-DB
                   :IND-ADE-DT-NOVA-RGM-FISC
                  ,:ADE-FL-ATTIV
                   :IND-ADE-FL-ATTIV
                  ,:ADE-IMP-REC-RIT-VIS
                   :IND-ADE-IMP-REC-RIT-VIS
                  ,:ADE-IMP-REC-RIT-ACC
                   :IND-ADE-IMP-REC-RIT-ACC
                  ,:ADE-FL-VARZ-STAT-TBGC
                   :IND-ADE-FL-VARZ-STAT-TBGC
                  ,:ADE-FL-PROVZA-MIGRAZ
                   :IND-ADE-FL-PROVZA-MIGRAZ
                  ,:ADE-IMPB-VIS-DA-REC
                   :IND-ADE-IMPB-VIS-DA-REC
                  ,:ADE-DT-DECOR-PREST-BAN-DB
                   :IND-ADE-DT-DECOR-PREST-BAN
                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
                   :IND-ADE-DT-EFF-VARZ-STAT-T
                  ,:ADE-DS-RIGA
                  ,:ADE-DS-OPER-SQL
                  ,:ADE-DS-VER
                  ,:ADE-DS-TS-INI-CPTZ
                  ,:ADE-DS-TS-END-CPTZ
                  ,:ADE-DS-UTENTE
                  ,:ADE-DS-STATO-ELAB
                  ,:ADE-CUM-CNBT-CAP
                   :IND-ADE-CUM-CNBT-CAP
                  ,:ADE-IMP-GAR-CNBT
                   :IND-ADE-IMP-GAR-CNBT
                  ,:ADE-DT-ULT-CONS-CNBT-DB
                   :IND-ADE-DT-ULT-CONS-CNBT
                  ,:ADE-IDEN-ISC-FND
                   :IND-ADE-IDEN-ISC-FND
                  ,:ADE-NUM-RAT-PIAN
                   :IND-ADE-NUM-RAT-PIAN
                  ,:ADE-DT-PRESC-DB
                   :IND-ADE-DT-PRESC
                  ,:ADE-CONCS-PREST
                   :IND-ADE-CONCS-PREST
                  ,:POL-ID-POLI
                  ,:POL-ID-MOVI-CRZ
                  ,:POL-ID-MOVI-CHIU
                   :IND-POL-ID-MOVI-CHIU
                  ,:POL-IB-OGG
                   :IND-POL-IB-OGG
                  ,:POL-IB-PROP
                  ,:POL-DT-PROP-DB
                   :IND-POL-DT-PROP
                  ,:POL-DT-INI-EFF-DB
                  ,:POL-DT-END-EFF-DB
                  ,:POL-COD-COMP-ANIA
                  ,:POL-DT-DECOR-DB
                  ,:POL-DT-EMIS-DB
                  ,:POL-TP-POLI
                  ,:POL-DUR-AA
                   :IND-POL-DUR-AA
                  ,:POL-DUR-MM
                   :IND-POL-DUR-MM
                  ,:POL-DT-SCAD-DB
                   :IND-POL-DT-SCAD
                  ,:POL-COD-PROD
                  ,:POL-DT-INI-VLDT-PROD-DB
                  ,:POL-COD-CONV
                   :IND-POL-COD-CONV
                  ,:POL-COD-RAMO
                   :IND-POL-COD-RAMO
                  ,:POL-DT-INI-VLDT-CONV-DB
                   :IND-POL-DT-INI-VLDT-CONV
                  ,:POL-DT-APPLZ-CONV-DB
                   :IND-POL-DT-APPLZ-CONV
                  ,:POL-TP-FRM-ASSVA
                  ,:POL-TP-RGM-FISC
                   :IND-POL-TP-RGM-FISC
                  ,:POL-FL-ESTAS
                   :IND-POL-FL-ESTAS
                  ,:POL-FL-RSH-COMUN
                   :IND-POL-FL-RSH-COMUN
                  ,:POL-FL-RSH-COMUN-COND
                   :IND-POL-FL-RSH-COMUN-COND
                  ,:POL-TP-LIV-GENZ-TIT
                  ,:POL-FL-COP-FINANZ
                   :IND-POL-FL-COP-FINANZ
                  ,:POL-TP-APPLZ-DIR
                   :IND-POL-TP-APPLZ-DIR
                  ,:POL-SPE-MED
                   :IND-POL-SPE-MED
                  ,:POL-DIR-EMIS
                   :IND-POL-DIR-EMIS
                  ,:POL-DIR-1O-VERS
                   :IND-POL-DIR-1O-VERS
                  ,:POL-DIR-VERS-AGG
                   :IND-POL-DIR-VERS-AGG
                  ,:POL-COD-DVS
                   :IND-POL-COD-DVS
                  ,:POL-FL-FNT-AZ
                   :IND-POL-FL-FNT-AZ
                  ,:POL-FL-FNT-ADER
                   :IND-POL-FL-FNT-ADER
                  ,:POL-FL-FNT-TFR
                   :IND-POL-FL-FNT-TFR
                  ,:POL-FL-FNT-VOLO
                   :IND-POL-FL-FNT-VOLO
                  ,:POL-TP-OPZ-A-SCAD
                   :IND-POL-TP-OPZ-A-SCAD
                  ,:POL-AA-DIFF-PROR-DFLT
                   :IND-POL-AA-DIFF-PROR-DFLT
                  ,:POL-FL-VER-PROD
                   :IND-POL-FL-VER-PROD
                  ,:POL-DUR-GG
                   :IND-POL-DUR-GG
                  ,:POL-DIR-QUIET
                   :IND-POL-DIR-QUIET
                  ,:POL-TP-PTF-ESTNO
                   :IND-POL-TP-PTF-ESTNO
                  ,:POL-FL-CUM-PRE-CNTR
                   :IND-POL-FL-CUM-PRE-CNTR
                  ,:POL-FL-AMMB-MOVI
                   :IND-POL-FL-AMMB-MOVI
                  ,:POL-CONV-GECO
                   :IND-POL-CONV-GECO
                  ,:POL-DS-RIGA
                  ,:POL-DS-OPER-SQL
                  ,:POL-DS-VER
                  ,:POL-DS-TS-INI-CPTZ
                  ,:POL-DS-TS-END-CPTZ
                  ,:POL-DS-UTENTE
                  ,:POL-DS-STATO-ELAB
                  ,:POL-FL-SCUDO-FISC
                   :IND-POL-FL-SCUDO-FISC
                  ,:POL-FL-TRASFE
                   :IND-POL-FL-TRASFE
                  ,:POL-FL-TFR-STRC
                   :IND-POL-FL-TFR-STRC
                  ,:POL-DT-PRESC-DB
                   :IND-POL-DT-PRESC
                  ,:POL-COD-CONV-AGG
                   :IND-POL-COD-CONV-AGG
                  ,:POL-SUBCAT-PROD
                   :IND-POL-SUBCAT-PROD
                  ,:POL-FL-QUEST-ADEGZ-ASS
                   :IND-POL-FL-QUEST-ADEGZ-ASS
                  ,:POL-COD-TPA
                   :IND-POL-COD-TPA
                  ,:POL-ID-ACC-COMM
                   :IND-POL-ID-ACC-COMM
                  ,:POL-FL-POLI-CPI-PR
                   :IND-POL-FL-POLI-CPI-PR
                  ,:POL-FL-POLI-BUNDLING
                   :IND-POL-FL-POLI-BUNDLING
                  ,:POL-IND-POLI-PRIN-COLL
                   :IND-POL-IND-POLI-PRIN-COLL
                  ,:POL-FL-VND-BUNDLE
                   :IND-POL-FL-VND-BUNDLE
                  ,:POL-IB-BS
                   :IND-POL-IB-BS
                  ,:POL-FL-POLI-IFP
                   :IND-POL-FL-POLI-IFP
                  ,:STB-ID-STAT-OGG-BUS
                  ,:STB-ID-OGG
                  ,:STB-TP-OGG
                  ,:STB-ID-MOVI-CRZ
                  ,:STB-ID-MOVI-CHIU
                   :IND-STB-ID-MOVI-CHIU
                  ,:STB-DT-INI-EFF-DB
                  ,:STB-DT-END-EFF-DB
                  ,:STB-COD-COMP-ANIA
                  ,:STB-TP-STAT-BUS
                  ,:STB-TP-CAUS
                  ,:STB-DS-RIGA
                  ,:STB-DS-OPER-SQL
                  ,:STB-DS-VER
                  ,:STB-DS-TS-INI-CPTZ
                  ,:STB-DS-TS-END-CPTZ
                  ,:STB-DS-UTENTE
                  ,:STB-DS-STATO-ELAB
              END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-SC03 THRU A370-SC03-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       A390-SC03-EX.
           EXIT.
      ******************************************************************
      *
      ******************************************************************
       SC04-SELECTION-CURSOR-04.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-SC04           THRU A310-SC04-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A360-OPEN-CURSOR-SC04      THRU A360-SC04-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR-SC04     THRU A370-SC04-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST-SC04      THRU A380-SC04-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT-SC04       THRU A390-SC04-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A320-UPDATE-SC04           THRU A320-SC04-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER          TO TRUE
           END-EVALUATE.

       SC04-EX.
           EXIT.

      ******************************************************************
       A305-DECLARE-CURSOR-SC04.

           EXEC SQL
                DECLARE CUR-SC04 CURSOR WITH HOLD FOR
             SELECT
                 A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.IB_PREV
                ,A.IB_OGG
                ,A.COD_COMP_ANIA
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.ETA_A_SCAD
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.TP_RGM_FISC
                ,A.TP_RIAT
                ,A.TP_MOD_PAG_TIT
                ,A.TP_IAS
                ,A.DT_VARZ_TP_IAS
                ,A.PRE_NET_IND
                ,A.PRE_LRD_IND
                ,A.RAT_LRD_IND
                ,A.PRSTZ_INI_IND
                ,A.FL_COINC_ASSTO
                ,A.IB_DFLT
                ,A.MOD_CALC
                ,A.TP_FNT_CNBTVA
                ,A.IMP_AZ
                ,A.IMP_ADER
                ,A.IMP_TFR
                ,A.IMP_VOLO
                ,A.PC_AZ
                ,A.PC_ADER
                ,A.PC_TFR
                ,A.PC_VOLO
                ,A.DT_NOVA_RGM_FISC
                ,A.FL_ATTIV
                ,A.IMP_REC_RIT_VIS
                ,A.IMP_REC_RIT_ACC
                ,A.FL_VARZ_STAT_TBGC
                ,A.FL_PROVZA_MIGRAZ
                ,A.IMPB_VIS_DA_REC
                ,A.DT_DECOR_PREST_BAN
                ,A.DT_EFF_VARZ_STAT_T
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.CUM_CNBT_CAP
                ,A.IMP_GAR_CNBT
                ,A.DT_ULT_CONS_CNBT
                ,A.IDEN_ISC_FND
                ,A.NUM_RAT_PIAN
                ,A.DT_PRESC
                ,A.CONCS_PREST
                ,B.ID_POLI
                ,B.ID_MOVI_CRZ
                ,B.ID_MOVI_CHIU
                ,B.IB_OGG
                ,B.IB_PROP
                ,B.DT_PROP
                ,B.DT_INI_EFF
                ,B.DT_END_EFF
                ,B.COD_COMP_ANIA
                ,B.DT_DECOR
                ,B.DT_EMIS
                ,B.TP_POLI
                ,B.DUR_AA
                ,B.DUR_MM
                ,B.DT_SCAD
                ,B.COD_PROD
                ,B.DT_INI_VLDT_PROD
                ,B.COD_CONV
                ,B.COD_RAMO
                ,B.DT_INI_VLDT_CONV
                ,B.DT_APPLZ_CONV
                ,B.TP_FRM_ASSVA
                ,B.TP_RGM_FISC
                ,B.FL_ESTAS
                ,B.FL_RSH_COMUN
                ,B.FL_RSH_COMUN_COND
                ,B.TP_LIV_GENZ_TIT
                ,B.FL_COP_FINANZ
                ,B.TP_APPLZ_DIR
                ,B.SPE_MED
                ,B.DIR_EMIS
                ,B.DIR_1O_VERS
                ,B.DIR_VERS_AGG
                ,B.COD_DVS
                ,B.FL_FNT_AZ
                ,B.FL_FNT_ADER
                ,B.FL_FNT_TFR
                ,B.FL_FNT_VOLO
                ,B.TP_OPZ_A_SCAD
                ,B.AA_DIFF_PROR_DFLT
                ,B.FL_VER_PROD
                ,B.DUR_GG
                ,B.DIR_QUIET
                ,B.TP_PTF_ESTNO
                ,B.FL_CUM_PRE_CNTR
                ,B.FL_AMMB_MOVI
                ,B.CONV_GECO
                ,B.DS_RIGA
                ,B.DS_OPER_SQL
                ,B.DS_VER
                ,B.DS_TS_INI_CPTZ
                ,B.DS_TS_END_CPTZ
                ,B.DS_UTENTE
                ,B.DS_STATO_ELAB
                ,B.FL_SCUDO_FISC
                ,B.FL_TRASFE
                ,B.FL_TFR_STRC
                ,B.DT_PRESC
                ,B.COD_CONV_AGG
                ,B.SUBCAT_PROD
                ,B.FL_QUEST_ADEGZ_ASS
                ,B.COD_TPA
                ,B.ID_ACC_COMM
                ,B.FL_POLI_CPI_PR
                ,B.FL_POLI_BUNDLING
                ,B.IND_POLI_PRIN_COLL
                ,B.FL_VND_BUNDLE
                ,B.IB_BS
                ,B.FL_POLI_IFP
                ,C.ID_STAT_OGG_BUS
                ,C.ID_OGG
                ,C.TP_OGG
                ,C.ID_MOVI_CRZ
                ,C.ID_MOVI_CHIU
                ,C.DT_INI_EFF
                ,C.DT_END_EFF
                ,C.COD_COMP_ANIA
                ,C.TP_STAT_BUS
                ,C.TP_CAUS
                ,C.DS_RIGA
                ,C.DS_OPER_SQL
                ,C.DS_VER
                ,C.DS_TS_INI_CPTZ
                ,C.DS_TS_END_CPTZ
                ,C.DS_UTENTE
                ,C.DS_STATO_ELAB
             FROM ADES         A,
                  POLI         B,
                  STAT_OGG_BUS C
             WHERE A.IB_OGG    BETWEEN  :WLB-IB-ADE-FIRST AND
                                        :WLB-IB-ADE-LAST
               AND B.IB_OGG          =  :WLB-IB-POLI-FIRST
               AND A.ID_POLI         =   B.ID_POLI
               AND A.ID_ADES         =   C.ID_OGG
               AND C.TP_OGG          =  'AD'
               AND C.TP_STAT_BUS     =  'VI'
      *        OR
      *            (C.TP_STAT_BUS    =  'ST'  AND
      *            EXISTS (SELECT
      *                  D.ID_MOVI_FINRIO
      *
      *               FROM  MOVI_FINRIO D
      *               WHERE A.ID_ADES = D.ID_ADES
      *                 AND D.STAT_MOVI IN ('AL','IE')
      *                 AND D.TP_MOVI_FINRIO = 'DI'
      *                 AND D.COD_COMP_ANIA   =
      *                     :IDSV0003-CODICE-COMPAGNIA-ANIA
      *                 AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
      *                 AND D.DT_END_EFF     >   :WS-DT-PTF-X
      *                 AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
      *                 AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
             AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
             AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
             AND A.DT_END_EFF     >   :WS-DT-PTF-X
             AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
             AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
             AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
             AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
             AND B.DT_END_EFF     >   :WS-DT-PTF-X
             AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
             AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
             AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
             AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
             AND C.DT_END_EFF     >   :WS-DT-PTF-X
             AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
             AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
      *      AND A.DS_STATO_ELAB IN (
      *                              :IABV0002-STATE-01,
      *                              :IABV0002-STATE-02,
      *                              :IABV0002-STATE-03,
      *                              :IABV0002-STATE-04,
      *                              :IABV0002-STATE-05,
      *                              :IABV0002-STATE-06,
      *                              :IABV0002-STATE-07,
      *                              :IABV0002-STATE-08,
      *                              :IABV0002-STATE-09,
      *                              :IABV0002-STATE-10
      *                                  )
      *        AND NOT A.DS_VER     = :IABV0009-VERSIONING
               ORDER BY A.ID_POLI, A.ID_ADES
           END-EXEC.

       A305-SC04-EX.
           EXIT.
      ******************************************************************
       A310-SELECT-SC04.

           EXEC SQL
             SELECT
                 A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.IB_PREV
                ,A.IB_OGG
                ,A.COD_COMP_ANIA
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.ETA_A_SCAD
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.TP_RGM_FISC
                ,A.TP_RIAT
                ,A.TP_MOD_PAG_TIT
                ,A.TP_IAS
                ,A.DT_VARZ_TP_IAS
                ,A.PRE_NET_IND
                ,A.PRE_LRD_IND
                ,A.RAT_LRD_IND
                ,A.PRSTZ_INI_IND
                ,A.FL_COINC_ASSTO
                ,A.IB_DFLT
                ,A.MOD_CALC
                ,A.TP_FNT_CNBTVA
                ,A.IMP_AZ
                ,A.IMP_ADER
                ,A.IMP_TFR
                ,A.IMP_VOLO
                ,A.PC_AZ
                ,A.PC_ADER
                ,A.PC_TFR
                ,A.PC_VOLO
                ,A.DT_NOVA_RGM_FISC
                ,A.FL_ATTIV
                ,A.IMP_REC_RIT_VIS
                ,A.IMP_REC_RIT_ACC
                ,A.FL_VARZ_STAT_TBGC
                ,A.FL_PROVZA_MIGRAZ
                ,A.IMPB_VIS_DA_REC
                ,A.DT_DECOR_PREST_BAN
                ,A.DT_EFF_VARZ_STAT_T
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.CUM_CNBT_CAP
                ,A.IMP_GAR_CNBT
                ,A.DT_ULT_CONS_CNBT
                ,A.IDEN_ISC_FND
                ,A.NUM_RAT_PIAN
                ,A.DT_PRESC
                ,A.CONCS_PREST
                ,B.ID_POLI
                ,B.ID_MOVI_CRZ
                ,B.ID_MOVI_CHIU
                ,B.IB_OGG
                ,B.IB_PROP
                ,B.DT_PROP
                ,B.DT_INI_EFF
                ,B.DT_END_EFF
                ,B.COD_COMP_ANIA
                ,B.DT_DECOR
                ,B.DT_EMIS
                ,B.TP_POLI
                ,B.DUR_AA
                ,B.DUR_MM
                ,B.DT_SCAD
                ,B.COD_PROD
                ,B.DT_INI_VLDT_PROD
                ,B.COD_CONV
                ,B.COD_RAMO
                ,B.DT_INI_VLDT_CONV
                ,B.DT_APPLZ_CONV
                ,B.TP_FRM_ASSVA
                ,B.TP_RGM_FISC
                ,B.FL_ESTAS
                ,B.FL_RSH_COMUN
                ,B.FL_RSH_COMUN_COND
                ,B.TP_LIV_GENZ_TIT
                ,B.FL_COP_FINANZ
                ,B.TP_APPLZ_DIR
                ,B.SPE_MED
                ,B.DIR_EMIS
                ,B.DIR_1O_VERS
                ,B.DIR_VERS_AGG
                ,B.COD_DVS
                ,B.FL_FNT_AZ
                ,B.FL_FNT_ADER
                ,B.FL_FNT_TFR
                ,B.FL_FNT_VOLO
                ,B.TP_OPZ_A_SCAD
                ,B.AA_DIFF_PROR_DFLT
                ,B.FL_VER_PROD
                ,B.DUR_GG
                ,B.DIR_QUIET
                ,B.TP_PTF_ESTNO
                ,B.FL_CUM_PRE_CNTR
                ,B.FL_AMMB_MOVI
                ,B.CONV_GECO
                ,B.DS_RIGA
                ,B.DS_OPER_SQL
                ,B.DS_VER
                ,B.DS_TS_INI_CPTZ
                ,B.DS_TS_END_CPTZ
                ,B.DS_UTENTE
                ,B.DS_STATO_ELAB
                ,B.FL_SCUDO_FISC
                ,B.FL_TRASFE
                ,B.FL_TFR_STRC
                ,B.DT_PRESC
                ,B.COD_CONV_AGG
                ,B.SUBCAT_PROD
                ,B.FL_QUEST_ADEGZ_ASS
                ,B.COD_TPA
                ,B.ID_ACC_COMM
                ,B.FL_POLI_CPI_PR
                ,B.FL_POLI_BUNDLING
                ,B.IND_POLI_PRIN_COLL
                ,B.FL_VND_BUNDLE
                ,B.IB_BS
                ,B.FL_POLI_IFP
                ,C.ID_STAT_OGG_BUS
                ,C.ID_OGG
                ,C.TP_OGG
                ,C.ID_MOVI_CRZ
                ,C.ID_MOVI_CHIU
                ,C.DT_INI_EFF
                ,C.DT_END_EFF
                ,C.COD_COMP_ANIA
                ,C.TP_STAT_BUS
                ,C.TP_CAUS
                ,C.DS_RIGA
                ,C.DS_OPER_SQL
                ,C.DS_VER
                ,C.DS_TS_INI_CPTZ
                ,C.DS_TS_END_CPTZ
                ,C.DS_UTENTE
                ,C.DS_STATO_ELAB
             INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
               ,:POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
               ,:STB-ID-STAT-OGG-BUS
               ,:STB-ID-OGG
               ,:STB-TP-OGG
               ,:STB-ID-MOVI-CRZ
               ,:STB-ID-MOVI-CHIU
                :IND-STB-ID-MOVI-CHIU
               ,:STB-DT-INI-EFF-DB
               ,:STB-DT-END-EFF-DB
               ,:STB-COD-COMP-ANIA
               ,:STB-TP-STAT-BUS
               ,:STB-TP-CAUS
               ,:STB-DS-RIGA
               ,:STB-DS-OPER-SQL
               ,:STB-DS-VER
               ,:STB-DS-TS-INI-CPTZ
               ,:STB-DS-TS-END-CPTZ
               ,:STB-DS-UTENTE
               ,:STB-DS-STATO-ELAB
             FROM ADES         A,
                  POLI         B,
                  STAT_OGG_BUS C
             WHERE  A.IB_OGG    BETWEEN  :WLB-IB-ADE-FIRST AND
                                        :WLB-IB-ADE-LAST
               AND B.IB_OGG          =  :WLB-IB-POLI-FIRST
               AND A.ID_POLI         =   B.ID_POLI
               AND A.ID_ADES         =   C.ID_OGG
               AND C.TP_OGG          =  'AD'
               AND C.TP_STAT_BUS     =  'VI'
      *        OR
      *            (C.TP_STAT_BUS    =  'ST'  AND
      *            EXISTS (SELECT
      *                  D.ID_MOVI_FINRIO
      *
      *               FROM  MOVI_FINRIO D
      *               WHERE A.ID_ADES = D.ID_ADES
      *                 AND D.STAT_MOVI IN ('AL','IE')
      *                 AND D.TP_MOVI_FINRIO = 'DI'
      *                 AND D.COD_COMP_ANIA   =
      *                     :IDSV0003-CODICE-COMPAGNIA-ANIA
      *                 AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
      *                 AND D.DT_END_EFF     >   :WS-DT-PTF-X
      *                 AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
      *                 AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
             AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
             AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
             AND A.DT_END_EFF     >   :WS-DT-PTF-X
             AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
             AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
             AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
             AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
             AND B.DT_END_EFF     >   :WS-DT-PTF-X
             AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
             AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
             AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
             AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
             AND C.DT_END_EFF     >   :WS-DT-PTF-X
             AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
             AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
      *      AND A.DS_STATO_ELAB IN (
      *                              :IABV0002-STATE-01,
      *                              :IABV0002-STATE-02,
      *                              :IABV0002-STATE-03,
      *                              :IABV0002-STATE-04,
      *                              :IABV0002-STATE-05,
      *                              :IABV0002-STATE-06,
      *                              :IABV0002-STATE-07,
      *                              :IABV0002-STATE-08,
      *                              :IABV0002-STATE-09,
      *                              :IABV0002-STATE-10
      *                                  )
      *        AND NOT A.DS_VER     = :IABV0009-VERSIONING
               ORDER BY A.ID_POLI, A.ID_ADES
              FETCH FIRST ROW ONLY
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-SC04-EX.
           EXIT.

      ******************************************************************
       A320-UPDATE-SC04.

           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
                   PERFORM A330-UPDATE-PK-SC04         THRU A330-SC04-EX

              WHEN IDSV0003-WHERE-CONDITION-04
                   PERFORM A340-UPDATE-WHERE-COND-SC04 THRU A340-SC04-EX
              WHEN IDSV0003-FIRST-ACTION
                   PERFORM A345-UPDATE-FIRST-ACTION-SC04
                                                       THRU A345-SC04-EX
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
           END-EVALUATE.

       A320-SC04-EX.
           EXIT.

      ******************************************************************

       A330-UPDATE-PK-SC04.

           PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX

           EXEC SQL
                UPDATE ADES
                SET
                   DS_OPER_SQL            = :ADE-DS-OPER-SQL
      *           ,DS_VER                 = :IABV0009-VERSIONING
                  ,DS_UTENTE              = :ADE-DS-UTENTE
                  ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
                WHERE             DS_RIGA = :ADE-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A330-SC04-EX.
           EXIT.

      ******************************************************************

       A340-UPDATE-WHERE-COND-SC04.

           CONTINUE.

       A340-SC04-EX.
           EXIT.
      ******************************************************************
       A345-UPDATE-FIRST-ACTION-SC04.

           CONTINUE.

       A345-SC04-EX.
           EXIT.
      ******************************************************************

       A360-OPEN-CURSOR-SC04.

           PERFORM A305-DECLARE-CURSOR-SC04 THRU A305-SC04-EX.

           EXEC SQL
                OPEN CUR-SC04
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A360-SC04-EX.
           EXIT.

      ******************************************************************

       A370-CLOSE-CURSOR-SC04.

           EXEC SQL
                CLOSE CUR-SC04
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A370-SC04-EX.
           EXIT.

      ******************************************************************

       A380-FETCH-FIRST-SC04.


           PERFORM A360-OPEN-CURSOR-SC04    THRU A360-SC04-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-SC04  THRU A390-SC04-EX
           END-IF.

       A380-SC04-EX.
           EXIT.

      ******************************************************************

       A390-FETCH-NEXT-SC04.

              EXEC SQL
                   FETCH CUR-SC04
              INTO
                   :ADE-ID-ADES
                  ,:ADE-ID-POLI
                  ,:ADE-ID-MOVI-CRZ
                  ,:ADE-ID-MOVI-CHIU
                   :IND-ADE-ID-MOVI-CHIU
                  ,:ADE-DT-INI-EFF-DB
                  ,:ADE-DT-END-EFF-DB
                  ,:ADE-IB-PREV
                   :IND-ADE-IB-PREV
                  ,:ADE-IB-OGG
                   :IND-ADE-IB-OGG
                  ,:ADE-COD-COMP-ANIA
                  ,:ADE-DT-DECOR-DB
                   :IND-ADE-DT-DECOR
                  ,:ADE-DT-SCAD-DB
                   :IND-ADE-DT-SCAD
                  ,:ADE-ETA-A-SCAD
                   :IND-ADE-ETA-A-SCAD
                  ,:ADE-DUR-AA
                   :IND-ADE-DUR-AA
                  ,:ADE-DUR-MM
                   :IND-ADE-DUR-MM
                  ,:ADE-DUR-GG
                   :IND-ADE-DUR-GG
                  ,:ADE-TP-RGM-FISC
                  ,:ADE-TP-RIAT
                   :IND-ADE-TP-RIAT
                  ,:ADE-TP-MOD-PAG-TIT
                  ,:ADE-TP-IAS
                   :IND-ADE-TP-IAS
                  ,:ADE-DT-VARZ-TP-IAS-DB
                   :IND-ADE-DT-VARZ-TP-IAS
                  ,:ADE-PRE-NET-IND
                   :IND-ADE-PRE-NET-IND
                  ,:ADE-PRE-LRD-IND
                   :IND-ADE-PRE-LRD-IND
                  ,:ADE-RAT-LRD-IND
                   :IND-ADE-RAT-LRD-IND
                  ,:ADE-PRSTZ-INI-IND
                   :IND-ADE-PRSTZ-INI-IND
                  ,:ADE-FL-COINC-ASSTO
                   :IND-ADE-FL-COINC-ASSTO
                  ,:ADE-IB-DFLT
                   :IND-ADE-IB-DFLT
                  ,:ADE-MOD-CALC
                   :IND-ADE-MOD-CALC
                  ,:ADE-TP-FNT-CNBTVA
                   :IND-ADE-TP-FNT-CNBTVA
                  ,:ADE-IMP-AZ
                   :IND-ADE-IMP-AZ
                  ,:ADE-IMP-ADER
                   :IND-ADE-IMP-ADER
                  ,:ADE-IMP-TFR
                   :IND-ADE-IMP-TFR
                  ,:ADE-IMP-VOLO
                   :IND-ADE-IMP-VOLO
                  ,:ADE-PC-AZ
                   :IND-ADE-PC-AZ
                  ,:ADE-PC-ADER
                   :IND-ADE-PC-ADER
                  ,:ADE-PC-TFR
                   :IND-ADE-PC-TFR
                  ,:ADE-PC-VOLO
                   :IND-ADE-PC-VOLO
                  ,:ADE-DT-NOVA-RGM-FISC-DB
                   :IND-ADE-DT-NOVA-RGM-FISC
                  ,:ADE-FL-ATTIV
                   :IND-ADE-FL-ATTIV
                  ,:ADE-IMP-REC-RIT-VIS
                   :IND-ADE-IMP-REC-RIT-VIS
                  ,:ADE-IMP-REC-RIT-ACC
                   :IND-ADE-IMP-REC-RIT-ACC
                  ,:ADE-FL-VARZ-STAT-TBGC
                   :IND-ADE-FL-VARZ-STAT-TBGC
                  ,:ADE-FL-PROVZA-MIGRAZ
                   :IND-ADE-FL-PROVZA-MIGRAZ
                  ,:ADE-IMPB-VIS-DA-REC
                   :IND-ADE-IMPB-VIS-DA-REC
                  ,:ADE-DT-DECOR-PREST-BAN-DB
                   :IND-ADE-DT-DECOR-PREST-BAN
                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
                   :IND-ADE-DT-EFF-VARZ-STAT-T
                  ,:ADE-DS-RIGA
                  ,:ADE-DS-OPER-SQL
                  ,:ADE-DS-VER
                  ,:ADE-DS-TS-INI-CPTZ
                  ,:ADE-DS-TS-END-CPTZ
                  ,:ADE-DS-UTENTE
                  ,:ADE-DS-STATO-ELAB
                  ,:ADE-CUM-CNBT-CAP
                   :IND-ADE-CUM-CNBT-CAP
                  ,:ADE-IMP-GAR-CNBT
                   :IND-ADE-IMP-GAR-CNBT
                  ,:ADE-DT-ULT-CONS-CNBT-DB
                   :IND-ADE-DT-ULT-CONS-CNBT
                  ,:ADE-IDEN-ISC-FND
                   :IND-ADE-IDEN-ISC-FND
                  ,:ADE-NUM-RAT-PIAN
                   :IND-ADE-NUM-RAT-PIAN
                  ,:ADE-DT-PRESC-DB
                   :IND-ADE-DT-PRESC
                  ,:ADE-CONCS-PREST
                   :IND-ADE-CONCS-PREST
                  ,:POL-ID-POLI
                  ,:POL-ID-MOVI-CRZ
                  ,:POL-ID-MOVI-CHIU
                   :IND-POL-ID-MOVI-CHIU
                  ,:POL-IB-OGG
                   :IND-POL-IB-OGG
                  ,:POL-IB-PROP
                  ,:POL-DT-PROP-DB
                   :IND-POL-DT-PROP
                  ,:POL-DT-INI-EFF-DB
                  ,:POL-DT-END-EFF-DB
                  ,:POL-COD-COMP-ANIA
                  ,:POL-DT-DECOR-DB
                  ,:POL-DT-EMIS-DB
                  ,:POL-TP-POLI
                  ,:POL-DUR-AA
                   :IND-POL-DUR-AA
                  ,:POL-DUR-MM
                   :IND-POL-DUR-MM
                  ,:POL-DT-SCAD-DB
                   :IND-POL-DT-SCAD
                  ,:POL-COD-PROD
                  ,:POL-DT-INI-VLDT-PROD-DB
                  ,:POL-COD-CONV
                   :IND-POL-COD-CONV
                  ,:POL-COD-RAMO
                   :IND-POL-COD-RAMO
                  ,:POL-DT-INI-VLDT-CONV-DB
                   :IND-POL-DT-INI-VLDT-CONV
                  ,:POL-DT-APPLZ-CONV-DB
                   :IND-POL-DT-APPLZ-CONV
                  ,:POL-TP-FRM-ASSVA
                  ,:POL-TP-RGM-FISC
                   :IND-POL-TP-RGM-FISC
                  ,:POL-FL-ESTAS
                   :IND-POL-FL-ESTAS
                  ,:POL-FL-RSH-COMUN
                   :IND-POL-FL-RSH-COMUN
                  ,:POL-FL-RSH-COMUN-COND
                   :IND-POL-FL-RSH-COMUN-COND
                  ,:POL-TP-LIV-GENZ-TIT
                  ,:POL-FL-COP-FINANZ
                   :IND-POL-FL-COP-FINANZ
                  ,:POL-TP-APPLZ-DIR
                   :IND-POL-TP-APPLZ-DIR
                  ,:POL-SPE-MED
                   :IND-POL-SPE-MED
                  ,:POL-DIR-EMIS
                   :IND-POL-DIR-EMIS
                  ,:POL-DIR-1O-VERS
                   :IND-POL-DIR-1O-VERS
                  ,:POL-DIR-VERS-AGG
                   :IND-POL-DIR-VERS-AGG
                  ,:POL-COD-DVS
                   :IND-POL-COD-DVS
                  ,:POL-FL-FNT-AZ
                   :IND-POL-FL-FNT-AZ
                  ,:POL-FL-FNT-ADER
                   :IND-POL-FL-FNT-ADER
                  ,:POL-FL-FNT-TFR
                   :IND-POL-FL-FNT-TFR
                  ,:POL-FL-FNT-VOLO
                   :IND-POL-FL-FNT-VOLO
                  ,:POL-TP-OPZ-A-SCAD
                   :IND-POL-TP-OPZ-A-SCAD
                  ,:POL-AA-DIFF-PROR-DFLT
                   :IND-POL-AA-DIFF-PROR-DFLT
                  ,:POL-FL-VER-PROD
                   :IND-POL-FL-VER-PROD
                  ,:POL-DUR-GG
                   :IND-POL-DUR-GG
                  ,:POL-DIR-QUIET
                   :IND-POL-DIR-QUIET
                  ,:POL-TP-PTF-ESTNO
                   :IND-POL-TP-PTF-ESTNO
                  ,:POL-FL-CUM-PRE-CNTR
                   :IND-POL-FL-CUM-PRE-CNTR
                  ,:POL-FL-AMMB-MOVI
                   :IND-POL-FL-AMMB-MOVI
                  ,:POL-CONV-GECO
                   :IND-POL-CONV-GECO
                  ,:POL-DS-RIGA
                  ,:POL-DS-OPER-SQL
                  ,:POL-DS-VER
                  ,:POL-DS-TS-INI-CPTZ
                  ,:POL-DS-TS-END-CPTZ
                  ,:POL-DS-UTENTE
                  ,:POL-DS-STATO-ELAB
                  ,:POL-FL-SCUDO-FISC
                   :IND-POL-FL-SCUDO-FISC
                  ,:POL-FL-TRASFE
                   :IND-POL-FL-TRASFE
                  ,:POL-FL-TFR-STRC
                   :IND-POL-FL-TFR-STRC
                  ,:POL-DT-PRESC-DB
                   :IND-POL-DT-PRESC
                  ,:POL-COD-CONV-AGG
                   :IND-POL-COD-CONV-AGG
                  ,:POL-SUBCAT-PROD
                   :IND-POL-SUBCAT-PROD
                  ,:POL-FL-QUEST-ADEGZ-ASS
                   :IND-POL-FL-QUEST-ADEGZ-ASS
                  ,:POL-COD-TPA
                   :IND-POL-COD-TPA
                  ,:POL-ID-ACC-COMM
                   :IND-POL-ID-ACC-COMM
                  ,:POL-FL-POLI-CPI-PR
                   :IND-POL-FL-POLI-CPI-PR
                  ,:POL-FL-POLI-BUNDLING
                   :IND-POL-FL-POLI-BUNDLING
                  ,:POL-IND-POLI-PRIN-COLL
                   :IND-POL-IND-POLI-PRIN-COLL
                  ,:POL-FL-VND-BUNDLE
                   :IND-POL-FL-VND-BUNDLE
                  ,:POL-IB-BS
                   :IND-POL-IB-BS
                  ,:POL-FL-POLI-IFP
                   :IND-POL-FL-POLI-IFP
                  ,:STB-ID-STAT-OGG-BUS
                  ,:STB-ID-OGG
                  ,:STB-TP-OGG
                  ,:STB-ID-MOVI-CRZ
                  ,:STB-ID-MOVI-CHIU
                   :IND-STB-ID-MOVI-CHIU
                  ,:STB-DT-INI-EFF-DB
                  ,:STB-DT-END-EFF-DB
                  ,:STB-COD-COMP-ANIA
                  ,:STB-TP-STAT-BUS
                  ,:STB-TP-CAUS
                  ,:STB-DS-RIGA
                  ,:STB-DS-OPER-SQL
                  ,:STB-DS-VER
                  ,:STB-DS-TS-INI-CPTZ
                  ,:STB-DS-TS-END-CPTZ
                  ,:STB-DS-UTENTE
                  ,:STB-DS-STATO-ELAB
              END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-SC04 THRU A370-SC04-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       A390-SC04-EX.
           EXIT.

      ******************************************************************
      *
      ******************************************************************
       SC05-SELECTION-CURSOR-05.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-SC05           THRU A310-SC05-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A360-OPEN-CURSOR-SC05      THRU A360-SC05-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR-SC05     THRU A370-SC05-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST-SC05      THRU A380-SC05-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT-SC05       THRU A390-SC05-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A320-UPDATE-SC05           THRU A320-SC05-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER          TO TRUE
           END-EVALUATE.

       SC05-EX.
           EXIT.

      ******************************************************************
       A305-DECLARE-CURSOR-SC05.

           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   DECLARE CUR-SC05-RANGE CURSOR WITH HOLD FOR
                SELECT
                       A.ID_ADES
                      ,A.ID_POLI
                      ,A.ID_MOVI_CRZ
                      ,A.ID_MOVI_CHIU
                      ,A.DT_INI_EFF
                      ,A.DT_END_EFF
                      ,A.IB_PREV
                      ,A.IB_OGG
                      ,A.COD_COMP_ANIA
                      ,A.DT_DECOR
                      ,A.DT_SCAD
                      ,A.ETA_A_SCAD
                      ,A.DUR_AA
                      ,A.DUR_MM
                      ,A.DUR_GG
                      ,A.TP_RGM_FISC
                      ,A.TP_RIAT
                      ,A.TP_MOD_PAG_TIT
                      ,A.TP_IAS
                      ,A.DT_VARZ_TP_IAS
                      ,A.PRE_NET_IND
                      ,A.PRE_LRD_IND
                      ,A.RAT_LRD_IND
                      ,A.PRSTZ_INI_IND
                      ,A.FL_COINC_ASSTO
                      ,A.IB_DFLT
                      ,A.MOD_CALC
                      ,A.TP_FNT_CNBTVA
                      ,A.IMP_AZ
                      ,A.IMP_ADER
                      ,A.IMP_TFR
                      ,A.IMP_VOLO
                      ,A.PC_AZ
                      ,A.PC_ADER
                      ,A.PC_TFR
                      ,A.PC_VOLO
                      ,A.DT_NOVA_RGM_FISC
                      ,A.FL_ATTIV
                      ,A.IMP_REC_RIT_VIS
                      ,A.IMP_REC_RIT_ACC
                      ,A.FL_VARZ_STAT_TBGC
                      ,A.FL_PROVZA_MIGRAZ
                      ,A.IMPB_VIS_DA_REC
                      ,A.DT_DECOR_PREST_BAN
                      ,A.DT_EFF_VARZ_STAT_T
                      ,A.DS_RIGA
                      ,A.DS_OPER_SQL
                      ,A.DS_VER
                      ,A.DS_TS_INI_CPTZ
                      ,A.DS_TS_END_CPTZ
                      ,A.DS_UTENTE
                      ,A.DS_STATO_ELAB
                      ,A.CUM_CNBT_CAP
                      ,A.IMP_GAR_CNBT
                      ,A.DT_ULT_CONS_CNBT
                      ,A.IDEN_ISC_FND
                      ,A.NUM_RAT_PIAN
                      ,A.DT_PRESC
                      ,A.CONCS_PREST
                      ,B.ID_POLI
                      ,B.ID_MOVI_CRZ
                      ,B.ID_MOVI_CHIU
                      ,B.IB_OGG
                      ,B.IB_PROP
                      ,B.DT_PROP
                      ,B.DT_INI_EFF
                      ,B.DT_END_EFF
                      ,B.COD_COMP_ANIA
                      ,B.DT_DECOR
                      ,B.DT_EMIS
                      ,B.TP_POLI
                      ,B.DUR_AA
                      ,B.DUR_MM
                      ,B.DT_SCAD
                      ,B.COD_PROD
                      ,B.DT_INI_VLDT_PROD
                      ,B.COD_CONV
                      ,B.COD_RAMO
                      ,B.DT_INI_VLDT_CONV
                      ,B.DT_APPLZ_CONV
                      ,B.TP_FRM_ASSVA
                      ,B.TP_RGM_FISC
                      ,B.FL_ESTAS
                      ,B.FL_RSH_COMUN
                      ,B.FL_RSH_COMUN_COND
                      ,B.TP_LIV_GENZ_TIT
                      ,B.FL_COP_FINANZ
                      ,B.TP_APPLZ_DIR
                      ,B.SPE_MED
                      ,B.DIR_EMIS
                      ,B.DIR_1O_VERS
                      ,B.DIR_VERS_AGG
                      ,B.COD_DVS
                      ,B.FL_FNT_AZ
                      ,B.FL_FNT_ADER
                      ,B.FL_FNT_TFR
                      ,B.FL_FNT_VOLO
                      ,B.TP_OPZ_A_SCAD
                      ,B.AA_DIFF_PROR_DFLT
                      ,B.FL_VER_PROD
                      ,B.DUR_GG
                      ,B.DIR_QUIET
                      ,B.TP_PTF_ESTNO
                      ,B.FL_CUM_PRE_CNTR
                      ,B.FL_AMMB_MOVI
                      ,B.CONV_GECO
                      ,B.DS_RIGA
                      ,B.DS_OPER_SQL
                      ,B.DS_VER
                      ,B.DS_TS_INI_CPTZ
                      ,B.DS_TS_END_CPTZ
                      ,B.DS_UTENTE
                      ,B.DS_STATO_ELAB
                      ,B.FL_SCUDO_FISC
                      ,B.FL_TRASFE
                      ,B.FL_TFR_STRC
                      ,B.DT_PRESC
                      ,B.COD_CONV_AGG
                      ,B.SUBCAT_PROD
                      ,B.FL_QUEST_ADEGZ_ASS
                      ,B.COD_TPA
                      ,B.ID_ACC_COMM
                      ,B.FL_POLI_CPI_PR
                      ,B.FL_POLI_BUNDLING
                      ,B.IND_POLI_PRIN_COLL
                      ,B.FL_VND_BUNDLE
                      ,B.IB_BS
                      ,B.FL_POLI_IFP
                      ,C.ID_STAT_OGG_BUS
                      ,C.ID_OGG
                      ,C.TP_OGG
                      ,C.ID_MOVI_CRZ
                      ,C.ID_MOVI_CHIU
                      ,C.DT_INI_EFF
                      ,C.DT_END_EFF
                      ,C.COD_COMP_ANIA
                      ,C.TP_STAT_BUS
                      ,C.TP_CAUS
                      ,C.DS_RIGA
                      ,C.DS_OPER_SQL
                      ,C.DS_VER
                      ,C.DS_TS_INI_CPTZ
                      ,C.DS_TS_END_CPTZ
                      ,C.DS_UTENTE
                      ,C.DS_STATO_ELAB
                FROM ADES      A,
                     POLI      B,
                     STAT_OGG_BUS C
                WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
                                           :WS-TP-FRM-ASSVA2 )
                  AND A.ID_POLI      =   B.ID_POLI
                  AND A.ID_ADES      =   C.ID_OGG
PROVA             AND A.ID_POLI     BETWEEN   :IABV0009-ID-OGG-DA AND
                                              :IABV0009-ID-OGG-A
                  AND C.TP_OGG       =  'AD'
                  AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
                  AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
                  AND A.DT_END_EFF  >   :WS-DT-PTF-X
                  AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                  AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                  AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
                  AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
                  AND B.DT_END_EFF  >   :WS-DT-PTF-X
                  AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                  AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                  AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
                  AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
                  AND C.DT_END_EFF  >   :WS-DT-PTF-X
                  AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                  AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                  AND (A.ID_ADES IN
                     (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D,
                                                      STAT_OGG_BUS E
                      WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
                      AND E.TP_OGG = 'TG'
      *                AND E.TP_STAT_BUS IN ('VI','ST')
                      AND E.TP_STAT_BUS  = 'VI'
                      AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
                      AND D.COD_COMP_ANIA =
                         :IDSV0003-CODICE-COMPAGNIA-ANIA
                      AND D.DT_INI_EFF <=   :WS-DT-PTF-X
                      AND D.DT_END_EFF >    :WS-DT-PTF-X
                      AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                      AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                      AND E.DT_INI_EFF <=   :WS-DT-PTF-X
                      AND E.DT_END_EFF >    :WS-DT-PTF-X
                      AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                      AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                      AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
                           OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
                      ))
      *               OR A.ID_ADES IN (
      *                 SELECT DISTINCT(D.ID_ADES)
      *                 FROM MOVI_FINRIO D
      *                 WHERE A.ID_ADES = D.ID_ADES
      *                 AND D.TP_MOVI_FINRIO = 'DI'
      *                 AND D.COD_COMP_ANIA =
      *                 :IDSV0003-CODICE-COMPAGNIA-ANIA
      *                 AND D.DT_INI_EFF  <=  :WS-DT-PTF-X
      *                 AND D.DT_END_EFF  >   :WS-DT-PTF-X
      *                 AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF-PRE
      *                 AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF-PRE
      *                 AND C.TP_STAT_BUS =  'ST'
      *               ))
      *           AND A.DS_STATO_ELAB IN (
      *                                   :IABV0002-STATE-01,
      *                                   :IABV0002-STATE-02,
      *                                   :IABV0002-STATE-03,
      *                                   :IABV0002-STATE-04,
      *                                   :IABV0002-STATE-05,
      *                                   :IABV0002-STATE-06,
      *                                   :IABV0002-STATE-07,
      *                                   :IABV0002-STATE-08,
      *                                   :IABV0002-STATE-09,
      *                                   :IABV0002-STATE-10
      *                                     )
      *           AND NOT A.DS_VER  = :IABV0009-VERSIONING

                  ORDER BY A.ID_POLI, A.ID_ADES
              END-EXEC
              CONTINUE
           ELSE
              EXEC SQL
                   DECLARE CUR-SC05 CURSOR WITH HOLD FOR
                SELECT
                       A.ID_ADES
                      ,A.ID_POLI
                      ,A.ID_MOVI_CRZ
                      ,A.ID_MOVI_CHIU
                      ,A.DT_INI_EFF
                      ,A.DT_END_EFF
                      ,A.IB_PREV
                      ,A.IB_OGG
                      ,A.COD_COMP_ANIA
                      ,A.DT_DECOR
                      ,A.DT_SCAD
                      ,A.ETA_A_SCAD
                      ,A.DUR_AA
                      ,A.DUR_MM
                      ,A.DUR_GG
                      ,A.TP_RGM_FISC
                      ,A.TP_RIAT
                      ,A.TP_MOD_PAG_TIT
                      ,A.TP_IAS
                      ,A.DT_VARZ_TP_IAS
                      ,A.PRE_NET_IND
                      ,A.PRE_LRD_IND
                      ,A.RAT_LRD_IND
                      ,A.PRSTZ_INI_IND
                      ,A.FL_COINC_ASSTO
                      ,A.IB_DFLT
                      ,A.MOD_CALC
                      ,A.TP_FNT_CNBTVA
                      ,A.IMP_AZ
                      ,A.IMP_ADER
                      ,A.IMP_TFR
                      ,A.IMP_VOLO
                      ,A.PC_AZ
                      ,A.PC_ADER
                      ,A.PC_TFR
                      ,A.PC_VOLO
                      ,A.DT_NOVA_RGM_FISC
                      ,A.FL_ATTIV
                      ,A.IMP_REC_RIT_VIS
                      ,A.IMP_REC_RIT_ACC
                      ,A.FL_VARZ_STAT_TBGC
                      ,A.FL_PROVZA_MIGRAZ
                      ,A.IMPB_VIS_DA_REC
                      ,A.DT_DECOR_PREST_BAN
                      ,A.DT_EFF_VARZ_STAT_T
                      ,A.DS_RIGA
                      ,A.DS_OPER_SQL
                      ,A.DS_VER
                      ,A.DS_TS_INI_CPTZ
                      ,A.DS_TS_END_CPTZ
                      ,A.DS_UTENTE
                      ,A.DS_STATO_ELAB
                      ,A.CUM_CNBT_CAP
                      ,A.IMP_GAR_CNBT
                      ,A.DT_ULT_CONS_CNBT
                      ,A.IDEN_ISC_FND
                      ,A.NUM_RAT_PIAN
                      ,A.DT_PRESC
                      ,A.CONCS_PREST
                      ,B.ID_POLI
                      ,B.ID_MOVI_CRZ
                      ,B.ID_MOVI_CHIU
                      ,B.IB_OGG
                      ,B.IB_PROP
                      ,B.DT_PROP
                      ,B.DT_INI_EFF
                      ,B.DT_END_EFF
                      ,B.COD_COMP_ANIA
                      ,B.DT_DECOR
                      ,B.DT_EMIS
                      ,B.TP_POLI
                      ,B.DUR_AA
                      ,B.DUR_MM
                      ,B.DT_SCAD
                      ,B.COD_PROD
                      ,B.DT_INI_VLDT_PROD
                      ,B.COD_CONV
                      ,B.COD_RAMO
                      ,B.DT_INI_VLDT_CONV
                      ,B.DT_APPLZ_CONV
                      ,B.TP_FRM_ASSVA
                      ,B.TP_RGM_FISC
                      ,B.FL_ESTAS
                      ,B.FL_RSH_COMUN
                      ,B.FL_RSH_COMUN_COND
                      ,B.TP_LIV_GENZ_TIT
                      ,B.FL_COP_FINANZ
                      ,B.TP_APPLZ_DIR
                      ,B.SPE_MED
                      ,B.DIR_EMIS
                      ,B.DIR_1O_VERS
                      ,B.DIR_VERS_AGG
                      ,B.COD_DVS
                      ,B.FL_FNT_AZ
                      ,B.FL_FNT_ADER
                      ,B.FL_FNT_TFR
                      ,B.FL_FNT_VOLO
                      ,B.TP_OPZ_A_SCAD
                      ,B.AA_DIFF_PROR_DFLT
                      ,B.FL_VER_PROD
                      ,B.DUR_GG
                      ,B.DIR_QUIET
                      ,B.TP_PTF_ESTNO
                      ,B.FL_CUM_PRE_CNTR
                      ,B.FL_AMMB_MOVI
                      ,B.CONV_GECO
                      ,B.DS_RIGA
                      ,B.DS_OPER_SQL
                      ,B.DS_VER
                      ,B.DS_TS_INI_CPTZ
                      ,B.DS_TS_END_CPTZ
                      ,B.DS_UTENTE
                      ,B.DS_STATO_ELAB
                      ,B.FL_SCUDO_FISC
                      ,B.FL_TRASFE
                      ,B.FL_TFR_STRC
                      ,B.DT_PRESC
                      ,B.COD_CONV_AGG
                      ,B.SUBCAT_PROD
                      ,B.FL_QUEST_ADEGZ_ASS
                      ,B.COD_TPA
                      ,B.ID_ACC_COMM
                      ,B.FL_POLI_CPI_PR
                      ,B.FL_POLI_BUNDLING
                      ,B.IND_POLI_PRIN_COLL
                      ,B.FL_VND_BUNDLE
                      ,B.IB_BS
                      ,B.FL_POLI_IFP
                      ,C.ID_STAT_OGG_BUS
                      ,C.ID_OGG
                      ,C.TP_OGG
                      ,C.ID_MOVI_CRZ
                      ,C.ID_MOVI_CHIU
                      ,C.DT_INI_EFF
                      ,C.DT_END_EFF
                      ,C.COD_COMP_ANIA
                      ,C.TP_STAT_BUS
                      ,C.TP_CAUS
                      ,C.DS_RIGA
                      ,C.DS_OPER_SQL
                      ,C.DS_VER
                      ,C.DS_TS_INI_CPTZ
                      ,C.DS_TS_END_CPTZ
                      ,C.DS_UTENTE
                      ,C.DS_STATO_ELAB
                FROM ADES      A,
                     POLI      B,
                     STAT_OGG_BUS C
                WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
                                           :WS-TP-FRM-ASSVA2 )
                  AND A.ID_POLI      =   B.ID_POLI
                  AND A.ID_ADES      =   C.ID_OGG
                  AND C.TP_OGG       =  'AD'
                  AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
                  AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
                  AND A.DT_END_EFF  >   :WS-DT-PTF-X
                  AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                  AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                  AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
                  AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
                  AND B.DT_END_EFF  >   :WS-DT-PTF-X
                  AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                  AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                  AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
                  AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
                  AND C.DT_END_EFF  >   :WS-DT-PTF-X
                  AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                  AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                  AND (A.ID_ADES IN
                     (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D,
                                                      STAT_OGG_BUS E
                      WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
                      AND E.TP_OGG = 'TG'
      *                AND E.TP_STAT_BUS IN ('VI','ST')
                      AND E.TP_STAT_BUS = 'VI'
                      AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
                      AND D.COD_COMP_ANIA =
                          :IDSV0003-CODICE-COMPAGNIA-ANIA
                      AND D.DT_INI_EFF <=   :WS-DT-PTF-X
                      AND D.DT_END_EFF >    :WS-DT-PTF-X
                      AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                      AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                      AND E.DT_INI_EFF <=   :WS-DT-PTF-X
                      AND E.DT_END_EFF >    :WS-DT-PTF-X
                      AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                      AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                      AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
                           OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
                      ))
      *               OR A.ID_ADES IN (
      *                 SELECT DISTINCT(D.ID_ADES)
      *                 FROM MOVI_FINRIO D
      *                 WHERE A.ID_ADES = D.ID_ADES
      *                 AND D.TP_MOVI_FINRIO = 'DI'
      *                 AND D.COD_COMP_ANIA =
      *                 :IDSV0003-CODICE-COMPAGNIA-ANIA
      *                 AND D.DT_INI_EFF  <=  :WS-DT-PTF-X
      *                 AND D.DT_END_EFF  >   :WS-DT-PTF-X
      *                 AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF-PRE
      *                 AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF-PRE
      *                 AND C.TP_STAT_BUS =  'ST'
      *               ))
      *           AND A.DS_STATO_ELAB IN (
      *                                   :IABV0002-STATE-01,
      *                                   :IABV0002-STATE-02,
      *                                   :IABV0002-STATE-03,
      *                                   :IABV0002-STATE-04,
      *                                   :IABV0002-STATE-05,
      *                                   :IABV0002-STATE-06,
      *                                   :IABV0002-STATE-07,
      *                                   :IABV0002-STATE-08,
      *                                   :IABV0002-STATE-09,
      *                                   :IABV0002-STATE-10
      *                                     )
      *           AND NOT A.DS_VER  = :IABV0009-VERSIONING

                  ORDER BY A.ID_POLI, A.ID_ADES
              END-EXEC
              CONTINUE
           END-IF.

       A305-SC05-EX.
           EXIT.
      ******************************************************************
       A310-SELECT-SC05.

           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                SELECT
                       A.ID_ADES
                      ,A.ID_POLI
                      ,A.ID_MOVI_CRZ
                      ,A.ID_MOVI_CHIU
                      ,A.DT_INI_EFF
                      ,A.DT_END_EFF
                      ,A.IB_PREV
                      ,A.IB_OGG
                      ,A.COD_COMP_ANIA
                      ,A.DT_DECOR
                      ,A.DT_SCAD
                      ,A.ETA_A_SCAD
                      ,A.DUR_AA
                      ,A.DUR_MM
                      ,A.DUR_GG
                      ,A.TP_RGM_FISC
                      ,A.TP_RIAT
                      ,A.TP_MOD_PAG_TIT
                      ,A.TP_IAS
                      ,A.DT_VARZ_TP_IAS
                      ,A.PRE_NET_IND
                      ,A.PRE_LRD_IND
                      ,A.RAT_LRD_IND
                      ,A.PRSTZ_INI_IND
                      ,A.FL_COINC_ASSTO
                      ,A.IB_DFLT
                      ,A.MOD_CALC
                      ,A.TP_FNT_CNBTVA
                      ,A.IMP_AZ
                      ,A.IMP_ADER
                      ,A.IMP_TFR
                      ,A.IMP_VOLO
                      ,A.PC_AZ
                      ,A.PC_ADER
                      ,A.PC_TFR
                      ,A.PC_VOLO
                      ,A.DT_NOVA_RGM_FISC
                      ,A.FL_ATTIV
                      ,A.IMP_REC_RIT_VIS
                      ,A.IMP_REC_RIT_ACC
                      ,A.FL_VARZ_STAT_TBGC
                      ,A.FL_PROVZA_MIGRAZ
                      ,A.IMPB_VIS_DA_REC
                      ,A.DT_DECOR_PREST_BAN
                      ,A.DT_EFF_VARZ_STAT_T
                      ,A.DS_RIGA
                      ,A.DS_OPER_SQL
                      ,A.DS_VER
                      ,A.DS_TS_INI_CPTZ
                      ,A.DS_TS_END_CPTZ
                      ,A.DS_UTENTE
                      ,A.DS_STATO_ELAB
                      ,A.CUM_CNBT_CAP
                      ,A.IMP_GAR_CNBT
                      ,A.DT_ULT_CONS_CNBT
                      ,A.IDEN_ISC_FND
                      ,A.NUM_RAT_PIAN
                      ,A.DT_PRESC
                      ,A.CONCS_PREST
                      ,B.ID_POLI
                      ,B.ID_MOVI_CRZ
                      ,B.ID_MOVI_CHIU
                      ,B.IB_OGG
                      ,B.IB_PROP
                      ,B.DT_PROP
                      ,B.DT_INI_EFF
                      ,B.DT_END_EFF
                      ,B.COD_COMP_ANIA
                      ,B.DT_DECOR
                      ,B.DT_EMIS
                      ,B.TP_POLI
                      ,B.DUR_AA
                      ,B.DUR_MM
                      ,B.DT_SCAD
                      ,B.COD_PROD
                      ,B.DT_INI_VLDT_PROD
                      ,B.COD_CONV
                      ,B.COD_RAMO
                      ,B.DT_INI_VLDT_CONV
                      ,B.DT_APPLZ_CONV
                      ,B.TP_FRM_ASSVA
                      ,B.TP_RGM_FISC
                      ,B.FL_ESTAS
                      ,B.FL_RSH_COMUN
                      ,B.FL_RSH_COMUN_COND
                      ,B.TP_LIV_GENZ_TIT
                      ,B.FL_COP_FINANZ
                      ,B.TP_APPLZ_DIR
                      ,B.SPE_MED
                      ,B.DIR_EMIS
                      ,B.DIR_1O_VERS
                      ,B.DIR_VERS_AGG
                      ,B.COD_DVS
                      ,B.FL_FNT_AZ
                      ,B.FL_FNT_ADER
                      ,B.FL_FNT_TFR
                      ,B.FL_FNT_VOLO
                      ,B.TP_OPZ_A_SCAD
                      ,B.AA_DIFF_PROR_DFLT
                      ,B.FL_VER_PROD
                      ,B.DUR_GG
                      ,B.DIR_QUIET
                      ,B.TP_PTF_ESTNO
                      ,B.FL_CUM_PRE_CNTR
                      ,B.FL_AMMB_MOVI
                      ,B.CONV_GECO
                      ,B.DS_RIGA
                      ,B.DS_OPER_SQL
                      ,B.DS_VER
                      ,B.DS_TS_INI_CPTZ
                      ,B.DS_TS_END_CPTZ
                      ,B.DS_UTENTE
                      ,B.DS_STATO_ELAB
                      ,B.FL_SCUDO_FISC
                      ,B.FL_TRASFE
                      ,B.FL_TFR_STRC
                      ,B.DT_PRESC
                      ,B.COD_CONV_AGG
                      ,B.SUBCAT_PROD
                      ,B.FL_QUEST_ADEGZ_ASS
                      ,B.COD_TPA
                      ,B.ID_ACC_COMM
                      ,B.FL_POLI_CPI_PR
                      ,B.FL_POLI_BUNDLING
                      ,B.IND_POLI_PRIN_COLL
                      ,B.FL_VND_BUNDLE
                      ,B.IB_BS
                      ,B.FL_POLI_IFP
                      ,C.ID_STAT_OGG_BUS
                      ,C.ID_OGG
                      ,C.TP_OGG
                      ,C.ID_MOVI_CRZ
                      ,C.ID_MOVI_CHIU
                      ,C.DT_INI_EFF
                      ,C.DT_END_EFF
                      ,C.COD_COMP_ANIA
                      ,C.TP_STAT_BUS
                      ,C.TP_CAUS
                      ,C.DS_RIGA
                      ,C.DS_OPER_SQL
                      ,C.DS_VER
                      ,C.DS_TS_INI_CPTZ
                      ,C.DS_TS_END_CPTZ
                      ,C.DS_UTENTE
                      ,C.DS_STATO_ELAB
                INTO
                      :ADE-ID-ADES
                     ,:ADE-ID-POLI
                     ,:ADE-ID-MOVI-CRZ
                     ,:ADE-ID-MOVI-CHIU
                      :IND-ADE-ID-MOVI-CHIU
                     ,:ADE-DT-INI-EFF-DB
                     ,:ADE-DT-END-EFF-DB
                     ,:ADE-IB-PREV
                      :IND-ADE-IB-PREV
                     ,:ADE-IB-OGG
                      :IND-ADE-IB-OGG
                     ,:ADE-COD-COMP-ANIA
                     ,:ADE-DT-DECOR-DB
                      :IND-ADE-DT-DECOR
                     ,:ADE-DT-SCAD-DB
                      :IND-ADE-DT-SCAD
                     ,:ADE-ETA-A-SCAD
                      :IND-ADE-ETA-A-SCAD
                     ,:ADE-DUR-AA
                      :IND-ADE-DUR-AA
                     ,:ADE-DUR-MM
                      :IND-ADE-DUR-MM
                     ,:ADE-DUR-GG
                      :IND-ADE-DUR-GG
                     ,:ADE-TP-RGM-FISC
                     ,:ADE-TP-RIAT
                      :IND-ADE-TP-RIAT
                     ,:ADE-TP-MOD-PAG-TIT
                     ,:ADE-TP-IAS
                      :IND-ADE-TP-IAS
                     ,:ADE-DT-VARZ-TP-IAS-DB
                      :IND-ADE-DT-VARZ-TP-IAS
                     ,:ADE-PRE-NET-IND
                      :IND-ADE-PRE-NET-IND
                     ,:ADE-PRE-LRD-IND
                      :IND-ADE-PRE-LRD-IND
                     ,:ADE-RAT-LRD-IND
                      :IND-ADE-RAT-LRD-IND
                     ,:ADE-PRSTZ-INI-IND
                      :IND-ADE-PRSTZ-INI-IND
                     ,:ADE-FL-COINC-ASSTO
                      :IND-ADE-FL-COINC-ASSTO
                     ,:ADE-IB-DFLT
                      :IND-ADE-IB-DFLT
                     ,:ADE-MOD-CALC
                      :IND-ADE-MOD-CALC
                     ,:ADE-TP-FNT-CNBTVA
                      :IND-ADE-TP-FNT-CNBTVA
                     ,:ADE-IMP-AZ
                      :IND-ADE-IMP-AZ
                     ,:ADE-IMP-ADER
                      :IND-ADE-IMP-ADER
                     ,:ADE-IMP-TFR
                      :IND-ADE-IMP-TFR
                     ,:ADE-IMP-VOLO
                      :IND-ADE-IMP-VOLO
                     ,:ADE-PC-AZ
                      :IND-ADE-PC-AZ
                     ,:ADE-PC-ADER
                      :IND-ADE-PC-ADER
                     ,:ADE-PC-TFR
                      :IND-ADE-PC-TFR
                     ,:ADE-PC-VOLO
                      :IND-ADE-PC-VOLO
                     ,:ADE-DT-NOVA-RGM-FISC-DB
                      :IND-ADE-DT-NOVA-RGM-FISC
                     ,:ADE-FL-ATTIV
                      :IND-ADE-FL-ATTIV
                     ,:ADE-IMP-REC-RIT-VIS
                      :IND-ADE-IMP-REC-RIT-VIS
                     ,:ADE-IMP-REC-RIT-ACC
                      :IND-ADE-IMP-REC-RIT-ACC
                     ,:ADE-FL-VARZ-STAT-TBGC
                      :IND-ADE-FL-VARZ-STAT-TBGC
                     ,:ADE-FL-PROVZA-MIGRAZ
                      :IND-ADE-FL-PROVZA-MIGRAZ
                     ,:ADE-IMPB-VIS-DA-REC
                      :IND-ADE-IMPB-VIS-DA-REC
                     ,:ADE-DT-DECOR-PREST-BAN-DB
                      :IND-ADE-DT-DECOR-PREST-BAN
                     ,:ADE-DT-EFF-VARZ-STAT-T-DB
                      :IND-ADE-DT-EFF-VARZ-STAT-T
                     ,:ADE-DS-RIGA
                     ,:ADE-DS-OPER-SQL
                     ,:ADE-DS-VER
                     ,:ADE-DS-TS-INI-CPTZ
                     ,:ADE-DS-TS-END-CPTZ
                     ,:ADE-DS-UTENTE
                     ,:ADE-DS-STATO-ELAB
                     ,:ADE-CUM-CNBT-CAP
                      :IND-ADE-CUM-CNBT-CAP
                     ,:ADE-IMP-GAR-CNBT
                      :IND-ADE-IMP-GAR-CNBT
                     ,:ADE-DT-ULT-CONS-CNBT-DB
                      :IND-ADE-DT-ULT-CONS-CNBT
                     ,:ADE-IDEN-ISC-FND
                      :IND-ADE-IDEN-ISC-FND
                     ,:ADE-NUM-RAT-PIAN
                      :IND-ADE-NUM-RAT-PIAN
                     ,:ADE-DT-PRESC-DB
                      :IND-ADE-DT-PRESC
                     ,:ADE-CONCS-PREST
                      :IND-ADE-CONCS-PREST
                     ,:POL-ID-POLI
                     ,:POL-ID-MOVI-CRZ
                     ,:POL-ID-MOVI-CHIU
                      :IND-POL-ID-MOVI-CHIU
                     ,:POL-IB-OGG
                      :IND-POL-IB-OGG
                     ,:POL-IB-PROP
                     ,:POL-DT-PROP-DB
                      :IND-POL-DT-PROP
                     ,:POL-DT-INI-EFF-DB
                     ,:POL-DT-END-EFF-DB
                     ,:POL-COD-COMP-ANIA
                     ,:POL-DT-DECOR-DB
                     ,:POL-DT-EMIS-DB
                     ,:POL-TP-POLI
                     ,:POL-DUR-AA
                      :IND-POL-DUR-AA
                     ,:POL-DUR-MM
                      :IND-POL-DUR-MM
                     ,:POL-DT-SCAD-DB
                      :IND-POL-DT-SCAD
                     ,:POL-COD-PROD
                     ,:POL-DT-INI-VLDT-PROD-DB
                     ,:POL-COD-CONV
                      :IND-POL-COD-CONV
                     ,:POL-COD-RAMO
                      :IND-POL-COD-RAMO
                     ,:POL-DT-INI-VLDT-CONV-DB
                      :IND-POL-DT-INI-VLDT-CONV
                     ,:POL-DT-APPLZ-CONV-DB
                      :IND-POL-DT-APPLZ-CONV
                     ,:POL-TP-FRM-ASSVA
                     ,:POL-TP-RGM-FISC
                      :IND-POL-TP-RGM-FISC
                     ,:POL-FL-ESTAS
                      :IND-POL-FL-ESTAS
                     ,:POL-FL-RSH-COMUN
                      :IND-POL-FL-RSH-COMUN
                     ,:POL-FL-RSH-COMUN-COND
                      :IND-POL-FL-RSH-COMUN-COND
                     ,:POL-TP-LIV-GENZ-TIT
                     ,:POL-FL-COP-FINANZ
                      :IND-POL-FL-COP-FINANZ
                     ,:POL-TP-APPLZ-DIR
                      :IND-POL-TP-APPLZ-DIR
                     ,:POL-SPE-MED
                      :IND-POL-SPE-MED
                     ,:POL-DIR-EMIS
                      :IND-POL-DIR-EMIS
                     ,:POL-DIR-1O-VERS
                      :IND-POL-DIR-1O-VERS
                     ,:POL-DIR-VERS-AGG
                      :IND-POL-DIR-VERS-AGG
                     ,:POL-COD-DVS
                      :IND-POL-COD-DVS
                     ,:POL-FL-FNT-AZ
                      :IND-POL-FL-FNT-AZ
                     ,:POL-FL-FNT-ADER
                      :IND-POL-FL-FNT-ADER
                     ,:POL-FL-FNT-TFR
                      :IND-POL-FL-FNT-TFR
                     ,:POL-FL-FNT-VOLO
                      :IND-POL-FL-FNT-VOLO
                     ,:POL-TP-OPZ-A-SCAD
                      :IND-POL-TP-OPZ-A-SCAD
                     ,:POL-AA-DIFF-PROR-DFLT
                      :IND-POL-AA-DIFF-PROR-DFLT
                     ,:POL-FL-VER-PROD
                      :IND-POL-FL-VER-PROD
                     ,:POL-DUR-GG
                      :IND-POL-DUR-GG
                     ,:POL-DIR-QUIET
                      :IND-POL-DIR-QUIET
                     ,:POL-TP-PTF-ESTNO
                      :IND-POL-TP-PTF-ESTNO
                     ,:POL-FL-CUM-PRE-CNTR
                      :IND-POL-FL-CUM-PRE-CNTR
                     ,:POL-FL-AMMB-MOVI
                      :IND-POL-FL-AMMB-MOVI
                     ,:POL-CONV-GECO
                      :IND-POL-CONV-GECO
                     ,:POL-DS-RIGA
                     ,:POL-DS-OPER-SQL
                     ,:POL-DS-VER
                     ,:POL-DS-TS-INI-CPTZ
                     ,:POL-DS-TS-END-CPTZ
                     ,:POL-DS-UTENTE
                     ,:POL-DS-STATO-ELAB
                     ,:POL-FL-SCUDO-FISC
                      :IND-POL-FL-SCUDO-FISC
                     ,:POL-FL-TRASFE
                      :IND-POL-FL-TRASFE
                     ,:POL-FL-TFR-STRC
                      :IND-POL-FL-TFR-STRC
                     ,:POL-DT-PRESC-DB
                      :IND-POL-DT-PRESC
                     ,:POL-COD-CONV-AGG
                      :IND-POL-COD-CONV-AGG
                     ,:POL-SUBCAT-PROD
                      :IND-POL-SUBCAT-PROD
                     ,:POL-FL-QUEST-ADEGZ-ASS
                      :IND-POL-FL-QUEST-ADEGZ-ASS
                     ,:POL-COD-TPA
                      :IND-POL-COD-TPA
                     ,:POL-ID-ACC-COMM
                      :IND-POL-ID-ACC-COMM
                     ,:POL-FL-POLI-CPI-PR
                      :IND-POL-FL-POLI-CPI-PR
                     ,:POL-FL-POLI-BUNDLING
                      :IND-POL-FL-POLI-BUNDLING
                     ,:POL-IND-POLI-PRIN-COLL
                      :IND-POL-IND-POLI-PRIN-COLL
                     ,:POL-FL-VND-BUNDLE
                      :IND-POL-FL-VND-BUNDLE
                     ,:POL-IB-BS
                      :IND-POL-IB-BS
                     ,:POL-FL-POLI-IFP
                      :IND-POL-FL-POLI-IFP
                     ,:STB-ID-STAT-OGG-BUS
                     ,:STB-ID-OGG
                     ,:STB-TP-OGG
                     ,:STB-ID-MOVI-CRZ
                     ,:STB-ID-MOVI-CHIU
                      :IND-STB-ID-MOVI-CHIU
                     ,:STB-DT-INI-EFF-DB
                     ,:STB-DT-END-EFF-DB
                     ,:STB-COD-COMP-ANIA
                     ,:STB-TP-STAT-BUS
                     ,:STB-TP-CAUS
                     ,:STB-DS-RIGA
                     ,:STB-DS-OPER-SQL
                     ,:STB-DS-VER
                     ,:STB-DS-TS-INI-CPTZ
                     ,:STB-DS-TS-END-CPTZ
                     ,:STB-DS-UTENTE
                     ,:STB-DS-STATO-ELAB
                FROM ADES      A,
                     POLI      B,
                     STAT_OGG_BUS C

                WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
                                           :WS-TP-FRM-ASSVA2 )
                  AND A.ID_POLI      =   B.ID_POLI
                  AND A.ID_ADES      =   C.ID_OGG
PROVA             AND A.ID_POLI        BETWEEN   :IABV0009-ID-OGG-DA AND
                                                 :IABV0009-ID-OGG-A
                  AND C.TP_OGG       =  'AD'
      *--      AND C.TP_STAT_BUS     =  'VI'
                  AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
                  AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
                  AND A.DT_END_EFF  >   :WS-DT-PTF-X
                  AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                  AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                  AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
                  AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
                  AND B.DT_END_EFF  >   :WS-DT-PTF-X
                  AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                  AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                  AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
                  AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
                  AND C.DT_END_EFF  >   :WS-DT-PTF-X
                  AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                  AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                  AND A.ID_ADES IN
                     (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
                                                      STAT_OGG_BUS E
                      WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
                      AND E.TP_OGG = 'TG'
                      AND E.TP_STAT_BUS IN ('VI','ST')
                      AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
                      AND D.COD_COMP_ANIA =
                          :IDSV0003-CODICE-COMPAGNIA-ANIA
                      AND D.DT_INI_EFF <=   :WS-DT-PTF-X
                      AND D.DT_END_EFF >    :WS-DT-PTF-X
                      AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                      AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                      AND E.DT_INI_EFF <=   :WS-DT-PTF-X
                      AND E.DT_END_EFF >    :WS-DT-PTF-X
                      AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                      AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                      AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
                           OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
                      )

      *           AND A.DS_STATO_ELAB IN (
      *                                   :IABV0002-STATE-01,
      *                                   :IABV0002-STATE-02,
      *                                   :IABV0002-STATE-03,
      *                                   :IABV0002-STATE-04,
      *                                   :IABV0002-STATE-05,
      *                                   :IABV0002-STATE-06,
      *                                   :IABV0002-STATE-07,
      *                                   :IABV0002-STATE-08,
      *                                   :IABV0002-STATE-09,
      *                                   :IABV0002-STATE-10
      *                                     )
      *           AND NOT A.DS_VER  = :IABV0009-VERSIONING
                  ORDER BY A.ID_POLI, A.ID_ADES
                 FETCH FIRST ROW ONLY
              END-EXEC
           ELSE
              EXEC SQL
                SELECT
                       A.ID_ADES
                      ,A.ID_POLI
                      ,A.ID_MOVI_CRZ
                      ,A.ID_MOVI_CHIU
                      ,A.DT_INI_EFF
                      ,A.DT_END_EFF
                      ,A.IB_PREV
                      ,A.IB_OGG
                      ,A.COD_COMP_ANIA
                      ,A.DT_DECOR
                      ,A.DT_SCAD
                      ,A.ETA_A_SCAD
                      ,A.DUR_AA
                      ,A.DUR_MM
                      ,A.DUR_GG
                      ,A.TP_RGM_FISC
                      ,A.TP_RIAT
                      ,A.TP_MOD_PAG_TIT
                      ,A.TP_IAS
                      ,A.DT_VARZ_TP_IAS
                      ,A.PRE_NET_IND
                      ,A.PRE_LRD_IND
                      ,A.RAT_LRD_IND
                      ,A.PRSTZ_INI_IND
                      ,A.FL_COINC_ASSTO
                      ,A.IB_DFLT
                      ,A.MOD_CALC
                      ,A.TP_FNT_CNBTVA
                      ,A.IMP_AZ
                      ,A.IMP_ADER
                      ,A.IMP_TFR
                      ,A.IMP_VOLO
                      ,A.PC_AZ
                      ,A.PC_ADER
                      ,A.PC_TFR
                      ,A.PC_VOLO
                      ,A.DT_NOVA_RGM_FISC
                      ,A.FL_ATTIV
                      ,A.IMP_REC_RIT_VIS
                      ,A.IMP_REC_RIT_ACC
                      ,A.FL_VARZ_STAT_TBGC
                      ,A.FL_PROVZA_MIGRAZ
                      ,A.IMPB_VIS_DA_REC
                      ,A.DT_DECOR_PREST_BAN
                      ,A.DT_EFF_VARZ_STAT_T
                      ,A.DS_RIGA
                      ,A.DS_OPER_SQL
                      ,A.DS_VER
                      ,A.DS_TS_INI_CPTZ
                      ,A.DS_TS_END_CPTZ
                      ,A.DS_UTENTE
                      ,A.DS_STATO_ELAB
                      ,A.CUM_CNBT_CAP
                      ,A.IMP_GAR_CNBT
                      ,A.DT_ULT_CONS_CNBT
                      ,A.IDEN_ISC_FND
                      ,A.NUM_RAT_PIAN
                      ,A.DT_PRESC
                      ,A.CONCS_PREST
                      ,B.ID_POLI
                      ,B.ID_MOVI_CRZ
                      ,B.ID_MOVI_CHIU
                      ,B.IB_OGG
                      ,B.IB_PROP
                      ,B.DT_PROP
                      ,B.DT_INI_EFF
                      ,B.DT_END_EFF
                      ,B.COD_COMP_ANIA
                      ,B.DT_DECOR
                      ,B.DT_EMIS
                      ,B.TP_POLI
                      ,B.DUR_AA
                      ,B.DUR_MM
                      ,B.DT_SCAD
                      ,B.COD_PROD
                      ,B.DT_INI_VLDT_PROD
                      ,B.COD_CONV
                      ,B.COD_RAMO
                      ,B.DT_INI_VLDT_CONV
                      ,B.DT_APPLZ_CONV
                      ,B.TP_FRM_ASSVA
                      ,B.TP_RGM_FISC
                      ,B.FL_ESTAS
                      ,B.FL_RSH_COMUN
                      ,B.FL_RSH_COMUN_COND
                      ,B.TP_LIV_GENZ_TIT
                      ,B.FL_COP_FINANZ
                      ,B.TP_APPLZ_DIR
                      ,B.SPE_MED
                      ,B.DIR_EMIS
                      ,B.DIR_1O_VERS
                      ,B.DIR_VERS_AGG
                      ,B.COD_DVS
                      ,B.FL_FNT_AZ
                      ,B.FL_FNT_ADER
                      ,B.FL_FNT_TFR
                      ,B.FL_FNT_VOLO
                      ,B.TP_OPZ_A_SCAD
                      ,B.AA_DIFF_PROR_DFLT
                      ,B.FL_VER_PROD
                      ,B.DUR_GG
                      ,B.DIR_QUIET
                      ,B.TP_PTF_ESTNO
                      ,B.FL_CUM_PRE_CNTR
                      ,B.FL_AMMB_MOVI
                      ,B.CONV_GECO
                      ,B.DS_RIGA
                      ,B.DS_OPER_SQL
                      ,B.DS_VER
                      ,B.DS_TS_INI_CPTZ
                      ,B.DS_TS_END_CPTZ
                      ,B.DS_UTENTE
                      ,B.DS_STATO_ELAB
                      ,B.FL_SCUDO_FISC
                      ,B.FL_TRASFE
                      ,B.FL_TFR_STRC
                      ,B.DT_PRESC
                      ,B.COD_CONV_AGG
                      ,B.SUBCAT_PROD
                      ,B.FL_QUEST_ADEGZ_ASS
                      ,B.COD_TPA
                      ,B.ID_ACC_COMM
                      ,B.FL_POLI_CPI_PR
                      ,B.FL_POLI_BUNDLING
                      ,B.IND_POLI_PRIN_COLL
                      ,B.FL_VND_BUNDLE
                      ,B.IB_BS
                      ,B.FL_POLI_IFP
                      ,C.ID_STAT_OGG_BUS
                      ,C.ID_OGG
                      ,C.TP_OGG
                      ,C.ID_MOVI_CRZ
                      ,C.ID_MOVI_CHIU
                      ,C.DT_INI_EFF
                      ,C.DT_END_EFF
                      ,C.COD_COMP_ANIA
                      ,C.TP_STAT_BUS
                      ,C.TP_CAUS
                      ,C.DS_RIGA
                      ,C.DS_OPER_SQL
                      ,C.DS_VER
                      ,C.DS_TS_INI_CPTZ
                      ,C.DS_TS_END_CPTZ
                      ,C.DS_UTENTE
                      ,C.DS_STATO_ELAB
                INTO
                      :ADE-ID-ADES
                     ,:ADE-ID-POLI
                     ,:ADE-ID-MOVI-CRZ
                     ,:ADE-ID-MOVI-CHIU
                      :IND-ADE-ID-MOVI-CHIU
                     ,:ADE-DT-INI-EFF-DB
                     ,:ADE-DT-END-EFF-DB
                     ,:ADE-IB-PREV
                      :IND-ADE-IB-PREV
                     ,:ADE-IB-OGG
                      :IND-ADE-IB-OGG
                     ,:ADE-COD-COMP-ANIA
                     ,:ADE-DT-DECOR-DB
                      :IND-ADE-DT-DECOR
                     ,:ADE-DT-SCAD-DB
                      :IND-ADE-DT-SCAD
                     ,:ADE-ETA-A-SCAD
                      :IND-ADE-ETA-A-SCAD
                     ,:ADE-DUR-AA
                      :IND-ADE-DUR-AA
                     ,:ADE-DUR-MM
                      :IND-ADE-DUR-MM
                     ,:ADE-DUR-GG
                      :IND-ADE-DUR-GG
                     ,:ADE-TP-RGM-FISC
                     ,:ADE-TP-RIAT
                      :IND-ADE-TP-RIAT
                     ,:ADE-TP-MOD-PAG-TIT
                     ,:ADE-TP-IAS
                      :IND-ADE-TP-IAS
                     ,:ADE-DT-VARZ-TP-IAS-DB
                      :IND-ADE-DT-VARZ-TP-IAS
                     ,:ADE-PRE-NET-IND
                      :IND-ADE-PRE-NET-IND
                     ,:ADE-PRE-LRD-IND
                      :IND-ADE-PRE-LRD-IND
                     ,:ADE-RAT-LRD-IND
                      :IND-ADE-RAT-LRD-IND
                     ,:ADE-PRSTZ-INI-IND
                      :IND-ADE-PRSTZ-INI-IND
                     ,:ADE-FL-COINC-ASSTO
                      :IND-ADE-FL-COINC-ASSTO
                     ,:ADE-IB-DFLT
                      :IND-ADE-IB-DFLT
                     ,:ADE-MOD-CALC
                      :IND-ADE-MOD-CALC
                     ,:ADE-TP-FNT-CNBTVA
                      :IND-ADE-TP-FNT-CNBTVA
                     ,:ADE-IMP-AZ
                      :IND-ADE-IMP-AZ
                     ,:ADE-IMP-ADER
                      :IND-ADE-IMP-ADER
                     ,:ADE-IMP-TFR
                      :IND-ADE-IMP-TFR
                     ,:ADE-IMP-VOLO
                      :IND-ADE-IMP-VOLO
                     ,:ADE-PC-AZ
                      :IND-ADE-PC-AZ
                     ,:ADE-PC-ADER
                      :IND-ADE-PC-ADER
                     ,:ADE-PC-TFR
                      :IND-ADE-PC-TFR
                     ,:ADE-PC-VOLO
                      :IND-ADE-PC-VOLO
                     ,:ADE-DT-NOVA-RGM-FISC-DB
                      :IND-ADE-DT-NOVA-RGM-FISC
                     ,:ADE-FL-ATTIV
                      :IND-ADE-FL-ATTIV
                     ,:ADE-IMP-REC-RIT-VIS
                      :IND-ADE-IMP-REC-RIT-VIS
                     ,:ADE-IMP-REC-RIT-ACC
                      :IND-ADE-IMP-REC-RIT-ACC
                     ,:ADE-FL-VARZ-STAT-TBGC
                      :IND-ADE-FL-VARZ-STAT-TBGC
                     ,:ADE-FL-PROVZA-MIGRAZ
                      :IND-ADE-FL-PROVZA-MIGRAZ
                     ,:ADE-IMPB-VIS-DA-REC
                      :IND-ADE-IMPB-VIS-DA-REC
                     ,:ADE-DT-DECOR-PREST-BAN-DB
                      :IND-ADE-DT-DECOR-PREST-BAN
                     ,:ADE-DT-EFF-VARZ-STAT-T-DB
                      :IND-ADE-DT-EFF-VARZ-STAT-T
                     ,:ADE-DS-RIGA
                     ,:ADE-DS-OPER-SQL
                     ,:ADE-DS-VER
                     ,:ADE-DS-TS-INI-CPTZ
                     ,:ADE-DS-TS-END-CPTZ
                     ,:ADE-DS-UTENTE
                     ,:ADE-DS-STATO-ELAB
                     ,:ADE-CUM-CNBT-CAP
                      :IND-ADE-CUM-CNBT-CAP
                     ,:ADE-IMP-GAR-CNBT
                      :IND-ADE-IMP-GAR-CNBT
                     ,:ADE-DT-ULT-CONS-CNBT-DB
                      :IND-ADE-DT-ULT-CONS-CNBT
                     ,:ADE-IDEN-ISC-FND
                      :IND-ADE-IDEN-ISC-FND
                     ,:ADE-NUM-RAT-PIAN
                      :IND-ADE-NUM-RAT-PIAN
                     ,:ADE-DT-PRESC-DB
                      :IND-ADE-DT-PRESC
                     ,:ADE-CONCS-PREST
                      :IND-ADE-CONCS-PREST
                     ,:POL-ID-POLI
                     ,:POL-ID-MOVI-CRZ
                     ,:POL-ID-MOVI-CHIU
                      :IND-POL-ID-MOVI-CHIU
                     ,:POL-IB-OGG
                      :IND-POL-IB-OGG
                     ,:POL-IB-PROP
                     ,:POL-DT-PROP-DB
                      :IND-POL-DT-PROP
                     ,:POL-DT-INI-EFF-DB
                     ,:POL-DT-END-EFF-DB
                     ,:POL-COD-COMP-ANIA
                     ,:POL-DT-DECOR-DB
                     ,:POL-DT-EMIS-DB
                     ,:POL-TP-POLI
                     ,:POL-DUR-AA
                      :IND-POL-DUR-AA
                     ,:POL-DUR-MM
                      :IND-POL-DUR-MM
                     ,:POL-DT-SCAD-DB
                      :IND-POL-DT-SCAD
                     ,:POL-COD-PROD
                     ,:POL-DT-INI-VLDT-PROD-DB
                     ,:POL-COD-CONV
                      :IND-POL-COD-CONV
                     ,:POL-COD-RAMO
                      :IND-POL-COD-RAMO
                     ,:POL-DT-INI-VLDT-CONV-DB
                      :IND-POL-DT-INI-VLDT-CONV
                     ,:POL-DT-APPLZ-CONV-DB
                      :IND-POL-DT-APPLZ-CONV
                     ,:POL-TP-FRM-ASSVA
                     ,:POL-TP-RGM-FISC
                      :IND-POL-TP-RGM-FISC
                     ,:POL-FL-ESTAS
                      :IND-POL-FL-ESTAS
                     ,:POL-FL-RSH-COMUN
                      :IND-POL-FL-RSH-COMUN
                     ,:POL-FL-RSH-COMUN-COND
                      :IND-POL-FL-RSH-COMUN-COND
                     ,:POL-TP-LIV-GENZ-TIT
                     ,:POL-FL-COP-FINANZ
                      :IND-POL-FL-COP-FINANZ
                     ,:POL-TP-APPLZ-DIR
                      :IND-POL-TP-APPLZ-DIR
                     ,:POL-SPE-MED
                      :IND-POL-SPE-MED
                     ,:POL-DIR-EMIS
                      :IND-POL-DIR-EMIS
                     ,:POL-DIR-1O-VERS
                      :IND-POL-DIR-1O-VERS
                     ,:POL-DIR-VERS-AGG
                      :IND-POL-DIR-VERS-AGG
                     ,:POL-COD-DVS
                      :IND-POL-COD-DVS
                     ,:POL-FL-FNT-AZ
                      :IND-POL-FL-FNT-AZ
                     ,:POL-FL-FNT-ADER
                      :IND-POL-FL-FNT-ADER
                     ,:POL-FL-FNT-TFR
                      :IND-POL-FL-FNT-TFR
                     ,:POL-FL-FNT-VOLO
                      :IND-POL-FL-FNT-VOLO
                     ,:POL-TP-OPZ-A-SCAD
                      :IND-POL-TP-OPZ-A-SCAD
                     ,:POL-AA-DIFF-PROR-DFLT
                      :IND-POL-AA-DIFF-PROR-DFLT
                     ,:POL-FL-VER-PROD
                      :IND-POL-FL-VER-PROD
                     ,:POL-DUR-GG
                      :IND-POL-DUR-GG
                     ,:POL-DIR-QUIET
                      :IND-POL-DIR-QUIET
                     ,:POL-TP-PTF-ESTNO
                      :IND-POL-TP-PTF-ESTNO
                     ,:POL-FL-CUM-PRE-CNTR
                      :IND-POL-FL-CUM-PRE-CNTR
                     ,:POL-FL-AMMB-MOVI
                      :IND-POL-FL-AMMB-MOVI
                     ,:POL-CONV-GECO
                      :IND-POL-CONV-GECO
                     ,:POL-DS-RIGA
                     ,:POL-DS-OPER-SQL
                     ,:POL-DS-VER
                     ,:POL-DS-TS-INI-CPTZ
                     ,:POL-DS-TS-END-CPTZ
                     ,:POL-DS-UTENTE
                     ,:POL-DS-STATO-ELAB
                     ,:POL-FL-SCUDO-FISC
                      :IND-POL-FL-SCUDO-FISC
                     ,:POL-FL-TRASFE
                      :IND-POL-FL-TRASFE
                     ,:POL-FL-TFR-STRC
                      :IND-POL-FL-TFR-STRC
                     ,:POL-DT-PRESC-DB
                      :IND-POL-DT-PRESC
                     ,:POL-COD-CONV-AGG
                      :IND-POL-COD-CONV-AGG
                     ,:POL-SUBCAT-PROD
                      :IND-POL-SUBCAT-PROD
                     ,:POL-FL-QUEST-ADEGZ-ASS
                      :IND-POL-FL-QUEST-ADEGZ-ASS
                     ,:POL-COD-TPA
                      :IND-POL-COD-TPA
                     ,:POL-ID-ACC-COMM
                      :IND-POL-ID-ACC-COMM
                     ,:POL-FL-POLI-CPI-PR
                      :IND-POL-FL-POLI-CPI-PR
                     ,:POL-FL-POLI-BUNDLING
                      :IND-POL-FL-POLI-BUNDLING
                     ,:POL-IND-POLI-PRIN-COLL
                      :IND-POL-IND-POLI-PRIN-COLL
                     ,:POL-FL-VND-BUNDLE
                      :IND-POL-FL-VND-BUNDLE
                     ,:POL-IB-BS
                      :IND-POL-IB-BS
                     ,:POL-FL-POLI-IFP
                      :IND-POL-FL-POLI-IFP
                     ,:STB-ID-STAT-OGG-BUS
                     ,:STB-ID-OGG
                     ,:STB-TP-OGG
                     ,:STB-ID-MOVI-CRZ
                     ,:STB-ID-MOVI-CHIU
                      :IND-STB-ID-MOVI-CHIU
                     ,:STB-DT-INI-EFF-DB
                     ,:STB-DT-END-EFF-DB
                     ,:STB-COD-COMP-ANIA
                     ,:STB-TP-STAT-BUS
                     ,:STB-TP-CAUS
                     ,:STB-DS-RIGA
                     ,:STB-DS-OPER-SQL
                     ,:STB-DS-VER
                     ,:STB-DS-TS-INI-CPTZ
                     ,:STB-DS-TS-END-CPTZ
                     ,:STB-DS-UTENTE
                     ,:STB-DS-STATO-ELAB
                FROM ADES      A,
                     POLI      B,
                     STAT_OGG_BUS C

                WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
                                           :WS-TP-FRM-ASSVA2 )
                  AND A.ID_POLI      =   B.ID_POLI
                  AND A.ID_ADES      =   C.ID_OGG
                  AND C.TP_OGG       =  'AD'
      *--      AND C.TP_STAT_BUS     =  'VI'
                  AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
                  AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
                  AND A.DT_END_EFF  >   :WS-DT-PTF-X
                  AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                  AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                  AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
                  AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
                  AND B.DT_END_EFF  >   :WS-DT-PTF-X
                  AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                  AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                  AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
                  AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
                  AND C.DT_END_EFF  >   :WS-DT-PTF-X
                  AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                  AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                  AND A.ID_ADES IN
                     (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
                                                      STAT_OGG_BUS E
                      WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
                      AND E.TP_OGG = 'TG'
                      AND E.TP_STAT_BUS IN ('VI','ST')
                      AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
                      AND D.COD_COMP_ANIA =
                          :IDSV0003-CODICE-COMPAGNIA-ANIA
                      AND D.DT_INI_EFF <=   :WS-DT-PTF-X
                      AND D.DT_END_EFF >    :WS-DT-PTF-X
                      AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                      AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                      AND E.DT_INI_EFF <=   :WS-DT-PTF-X
                      AND E.DT_END_EFF >    :WS-DT-PTF-X
                      AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                      AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                      AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
                           OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
                      )

      *           AND A.DS_STATO_ELAB IN (
      *                                   :IABV0002-STATE-01,
      *                                   :IABV0002-STATE-02,
      *                                   :IABV0002-STATE-03,
      *                                   :IABV0002-STATE-04,
      *                                   :IABV0002-STATE-05,
      *                                   :IABV0002-STATE-06,
      *                                   :IABV0002-STATE-07,
      *                                   :IABV0002-STATE-08,
      *                                   :IABV0002-STATE-09,
      *                                   :IABV0002-STATE-10
      *                                     )
      *           AND NOT A.DS_VER  = :IABV0009-VERSIONING
                  ORDER BY A.ID_POLI, A.ID_ADES
                 FETCH FIRST ROW ONLY
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-SC05-EX.
           EXIT.

      ******************************************************************
       A320-UPDATE-SC05.

           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
                   PERFORM A330-UPDATE-PK-SC05         THRU A330-SC05-EX

              WHEN IDSV0003-WHERE-CONDITION-05
                   PERFORM A340-UPDATE-WHERE-COND-SC05 THRU A340-SC05-EX
              WHEN IDSV0003-FIRST-ACTION
                   PERFORM A345-UPDATE-FIRST-ACTION-SC05
                                                       THRU A345-SC05-EX
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
           END-EVALUATE.

       A320-SC05-EX.
           EXIT.

      ******************************************************************

       A330-UPDATE-PK-SC05.

           PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX

           EXEC SQL
                UPDATE ADES
                SET
                   DS_OPER_SQL            = :ADE-DS-OPER-SQL
      *           ,DS_VER                 = :IABV0009-VERSIONING
                  ,DS_UTENTE              = :ADE-DS-UTENTE
                  ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
                WHERE             DS_RIGA = :ADE-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A330-SC05-EX.
           EXIT.

      ******************************************************************

       A340-UPDATE-WHERE-COND-SC05.

           CONTINUE.

       A340-SC05-EX.
           EXIT.
      ******************************************************************
       A345-UPDATE-FIRST-ACTION-SC05.

           CONTINUE.

       A345-SC05-EX.
           EXIT.
      ******************************************************************

       A360-OPEN-CURSOR-SC05.

           PERFORM A305-DECLARE-CURSOR-SC05 THRU A305-SC05-EX.

           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   OPEN CUR-SC05-RANGE
              END-EXEC
           ELSE
              EXEC SQL
                   OPEN CUR-SC05
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A360-SC05-EX.
           EXIT.

      ******************************************************************

       A370-CLOSE-CURSOR-SC05.

           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   CLOSE CUR-SC05-RANGE
              END-EXEC
           ELSE
              EXEC SQL
                   CLOSE CUR-SC05
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A370-SC05-EX.
           EXIT.

      ******************************************************************

       A380-FETCH-FIRST-SC05.


           PERFORM A360-OPEN-CURSOR-SC05    THRU A360-SC05-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-SC05  THRU A390-SC05-EX
           END-IF.

       A380-SC05-EX.
           EXIT.

      ******************************************************************

       A390-FETCH-NEXT-SC05.


           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   FETCH CUR-SC05-RANGE
              INTO
                      :ADE-ID-ADES
                     ,:ADE-ID-POLI
                     ,:ADE-ID-MOVI-CRZ
                     ,:ADE-ID-MOVI-CHIU
                      :IND-ADE-ID-MOVI-CHIU
                     ,:ADE-DT-INI-EFF-DB
                     ,:ADE-DT-END-EFF-DB
                     ,:ADE-IB-PREV
                      :IND-ADE-IB-PREV
                     ,:ADE-IB-OGG
                      :IND-ADE-IB-OGG
                     ,:ADE-COD-COMP-ANIA
                     ,:ADE-DT-DECOR-DB
                      :IND-ADE-DT-DECOR
                     ,:ADE-DT-SCAD-DB
                      :IND-ADE-DT-SCAD
                     ,:ADE-ETA-A-SCAD
                      :IND-ADE-ETA-A-SCAD
                     ,:ADE-DUR-AA
                      :IND-ADE-DUR-AA
                     ,:ADE-DUR-MM
                      :IND-ADE-DUR-MM
                     ,:ADE-DUR-GG
                      :IND-ADE-DUR-GG
                     ,:ADE-TP-RGM-FISC
                     ,:ADE-TP-RIAT
                      :IND-ADE-TP-RIAT
                     ,:ADE-TP-MOD-PAG-TIT
                     ,:ADE-TP-IAS
                      :IND-ADE-TP-IAS
                     ,:ADE-DT-VARZ-TP-IAS-DB
                      :IND-ADE-DT-VARZ-TP-IAS
                     ,:ADE-PRE-NET-IND
                      :IND-ADE-PRE-NET-IND
                     ,:ADE-PRE-LRD-IND
                      :IND-ADE-PRE-LRD-IND
                     ,:ADE-RAT-LRD-IND
                      :IND-ADE-RAT-LRD-IND
                     ,:ADE-PRSTZ-INI-IND
                      :IND-ADE-PRSTZ-INI-IND
                     ,:ADE-FL-COINC-ASSTO
                      :IND-ADE-FL-COINC-ASSTO
                     ,:ADE-IB-DFLT
                      :IND-ADE-IB-DFLT
                     ,:ADE-MOD-CALC
                      :IND-ADE-MOD-CALC
                     ,:ADE-TP-FNT-CNBTVA
                      :IND-ADE-TP-FNT-CNBTVA
                     ,:ADE-IMP-AZ
                      :IND-ADE-IMP-AZ
                     ,:ADE-IMP-ADER
                      :IND-ADE-IMP-ADER
                     ,:ADE-IMP-TFR
                      :IND-ADE-IMP-TFR
                     ,:ADE-IMP-VOLO
                      :IND-ADE-IMP-VOLO
                     ,:ADE-PC-AZ
                      :IND-ADE-PC-AZ
                     ,:ADE-PC-ADER
                      :IND-ADE-PC-ADER
                     ,:ADE-PC-TFR
                      :IND-ADE-PC-TFR
                     ,:ADE-PC-VOLO
                      :IND-ADE-PC-VOLO
                     ,:ADE-DT-NOVA-RGM-FISC-DB
                      :IND-ADE-DT-NOVA-RGM-FISC
                     ,:ADE-FL-ATTIV
                      :IND-ADE-FL-ATTIV
                     ,:ADE-IMP-REC-RIT-VIS
                      :IND-ADE-IMP-REC-RIT-VIS
                     ,:ADE-IMP-REC-RIT-ACC
                      :IND-ADE-IMP-REC-RIT-ACC
                     ,:ADE-FL-VARZ-STAT-TBGC
                      :IND-ADE-FL-VARZ-STAT-TBGC
                     ,:ADE-FL-PROVZA-MIGRAZ
                      :IND-ADE-FL-PROVZA-MIGRAZ
                     ,:ADE-IMPB-VIS-DA-REC
                      :IND-ADE-IMPB-VIS-DA-REC
                     ,:ADE-DT-DECOR-PREST-BAN-DB
                      :IND-ADE-DT-DECOR-PREST-BAN
                     ,:ADE-DT-EFF-VARZ-STAT-T-DB
                      :IND-ADE-DT-EFF-VARZ-STAT-T
                     ,:ADE-DS-RIGA
                     ,:ADE-DS-OPER-SQL
                     ,:ADE-DS-VER
                     ,:ADE-DS-TS-INI-CPTZ
                     ,:ADE-DS-TS-END-CPTZ
                     ,:ADE-DS-UTENTE
                     ,:ADE-DS-STATO-ELAB
                     ,:ADE-CUM-CNBT-CAP
                      :IND-ADE-CUM-CNBT-CAP
                     ,:ADE-IMP-GAR-CNBT
                      :IND-ADE-IMP-GAR-CNBT
                     ,:ADE-DT-ULT-CONS-CNBT-DB
                      :IND-ADE-DT-ULT-CONS-CNBT
                     ,:ADE-IDEN-ISC-FND
                      :IND-ADE-IDEN-ISC-FND
                     ,:ADE-NUM-RAT-PIAN
                      :IND-ADE-NUM-RAT-PIAN
                     ,:ADE-DT-PRESC-DB
                      :IND-ADE-DT-PRESC
                     ,:ADE-CONCS-PREST
                      :IND-ADE-CONCS-PREST
                     ,:POL-ID-POLI
                     ,:POL-ID-MOVI-CRZ
                     ,:POL-ID-MOVI-CHIU
                      :IND-POL-ID-MOVI-CHIU
                     ,:POL-IB-OGG
                      :IND-POL-IB-OGG
                     ,:POL-IB-PROP
                     ,:POL-DT-PROP-DB
                      :IND-POL-DT-PROP
                     ,:POL-DT-INI-EFF-DB
                     ,:POL-DT-END-EFF-DB
                     ,:POL-COD-COMP-ANIA
                     ,:POL-DT-DECOR-DB
                     ,:POL-DT-EMIS-DB
                     ,:POL-TP-POLI
                     ,:POL-DUR-AA
                      :IND-POL-DUR-AA
                     ,:POL-DUR-MM
                      :IND-POL-DUR-MM
                     ,:POL-DT-SCAD-DB
                      :IND-POL-DT-SCAD
                     ,:POL-COD-PROD
                     ,:POL-DT-INI-VLDT-PROD-DB
                     ,:POL-COD-CONV
                      :IND-POL-COD-CONV
                     ,:POL-COD-RAMO
                      :IND-POL-COD-RAMO
                     ,:POL-DT-INI-VLDT-CONV-DB
                      :IND-POL-DT-INI-VLDT-CONV
                     ,:POL-DT-APPLZ-CONV-DB
                      :IND-POL-DT-APPLZ-CONV
                     ,:POL-TP-FRM-ASSVA
                     ,:POL-TP-RGM-FISC
                      :IND-POL-TP-RGM-FISC
                     ,:POL-FL-ESTAS
                      :IND-POL-FL-ESTAS
                     ,:POL-FL-RSH-COMUN
                      :IND-POL-FL-RSH-COMUN
                     ,:POL-FL-RSH-COMUN-COND
                      :IND-POL-FL-RSH-COMUN-COND
                     ,:POL-TP-LIV-GENZ-TIT
                     ,:POL-FL-COP-FINANZ
                      :IND-POL-FL-COP-FINANZ
                     ,:POL-TP-APPLZ-DIR
                      :IND-POL-TP-APPLZ-DIR
                     ,:POL-SPE-MED
                      :IND-POL-SPE-MED
                     ,:POL-DIR-EMIS
                      :IND-POL-DIR-EMIS
                     ,:POL-DIR-1O-VERS
                      :IND-POL-DIR-1O-VERS
                     ,:POL-DIR-VERS-AGG
                      :IND-POL-DIR-VERS-AGG
                     ,:POL-COD-DVS
                      :IND-POL-COD-DVS
                     ,:POL-FL-FNT-AZ
                      :IND-POL-FL-FNT-AZ
                     ,:POL-FL-FNT-ADER
                      :IND-POL-FL-FNT-ADER
                     ,:POL-FL-FNT-TFR
                      :IND-POL-FL-FNT-TFR
                     ,:POL-FL-FNT-VOLO
                      :IND-POL-FL-FNT-VOLO
                     ,:POL-TP-OPZ-A-SCAD
                      :IND-POL-TP-OPZ-A-SCAD
                     ,:POL-AA-DIFF-PROR-DFLT
                      :IND-POL-AA-DIFF-PROR-DFLT
                     ,:POL-FL-VER-PROD
                      :IND-POL-FL-VER-PROD
                     ,:POL-DUR-GG
                      :IND-POL-DUR-GG
                     ,:POL-DIR-QUIET
                      :IND-POL-DIR-QUIET
                     ,:POL-TP-PTF-ESTNO
                      :IND-POL-TP-PTF-ESTNO
                     ,:POL-FL-CUM-PRE-CNTR
                      :IND-POL-FL-CUM-PRE-CNTR
                     ,:POL-FL-AMMB-MOVI
                      :IND-POL-FL-AMMB-MOVI
                     ,:POL-CONV-GECO
                      :IND-POL-CONV-GECO
                     ,:POL-DS-RIGA
                     ,:POL-DS-OPER-SQL
                     ,:POL-DS-VER
                     ,:POL-DS-TS-INI-CPTZ
                     ,:POL-DS-TS-END-CPTZ
                     ,:POL-DS-UTENTE
                     ,:POL-DS-STATO-ELAB
                     ,:POL-FL-SCUDO-FISC
                      :IND-POL-FL-SCUDO-FISC
                     ,:POL-FL-TRASFE
                      :IND-POL-FL-TRASFE
                     ,:POL-FL-TFR-STRC
                      :IND-POL-FL-TFR-STRC
                     ,:POL-DT-PRESC-DB
                      :IND-POL-DT-PRESC
                     ,:POL-COD-CONV-AGG
                      :IND-POL-COD-CONV-AGG
                     ,:POL-SUBCAT-PROD
                      :IND-POL-SUBCAT-PROD
                     ,:POL-FL-QUEST-ADEGZ-ASS
                      :IND-POL-FL-QUEST-ADEGZ-ASS
                     ,:POL-COD-TPA
                      :IND-POL-COD-TPA
                     ,:POL-ID-ACC-COMM
                      :IND-POL-ID-ACC-COMM
                     ,:POL-FL-POLI-CPI-PR
                      :IND-POL-FL-POLI-CPI-PR
                     ,:POL-FL-POLI-BUNDLING
                      :IND-POL-FL-POLI-BUNDLING
                     ,:POL-IND-POLI-PRIN-COLL
                      :IND-POL-IND-POLI-PRIN-COLL
                     ,:POL-FL-VND-BUNDLE
                      :IND-POL-FL-VND-BUNDLE
                     ,:POL-IB-BS
                      :IND-POL-IB-BS
                     ,:POL-FL-POLI-IFP
                      :IND-POL-FL-POLI-IFP
                     ,:STB-ID-STAT-OGG-BUS
                     ,:STB-ID-OGG
                     ,:STB-TP-OGG
                     ,:STB-ID-MOVI-CRZ
                     ,:STB-ID-MOVI-CHIU
                      :IND-STB-ID-MOVI-CHIU
                     ,:STB-DT-INI-EFF-DB
                     ,:STB-DT-END-EFF-DB
                     ,:STB-COD-COMP-ANIA
                     ,:STB-TP-STAT-BUS
                     ,:STB-TP-CAUS
                     ,:STB-DS-RIGA
                     ,:STB-DS-OPER-SQL
                     ,:STB-DS-VER
                     ,:STB-DS-TS-INI-CPTZ
                     ,:STB-DS-TS-END-CPTZ
                     ,:STB-DS-UTENTE
                     ,:STB-DS-STATO-ELAB
              END-EXEC
           ELSE
              EXEC SQL
                   FETCH CUR-SC05
              INTO
                      :ADE-ID-ADES
                     ,:ADE-ID-POLI
                     ,:ADE-ID-MOVI-CRZ
                     ,:ADE-ID-MOVI-CHIU
                      :IND-ADE-ID-MOVI-CHIU
                     ,:ADE-DT-INI-EFF-DB
                     ,:ADE-DT-END-EFF-DB
                     ,:ADE-IB-PREV
                      :IND-ADE-IB-PREV
                     ,:ADE-IB-OGG
                      :IND-ADE-IB-OGG
                     ,:ADE-COD-COMP-ANIA
                     ,:ADE-DT-DECOR-DB
                      :IND-ADE-DT-DECOR
                     ,:ADE-DT-SCAD-DB
                      :IND-ADE-DT-SCAD
                     ,:ADE-ETA-A-SCAD
                      :IND-ADE-ETA-A-SCAD
                     ,:ADE-DUR-AA
                      :IND-ADE-DUR-AA
                     ,:ADE-DUR-MM
                      :IND-ADE-DUR-MM
                     ,:ADE-DUR-GG
                      :IND-ADE-DUR-GG
                     ,:ADE-TP-RGM-FISC
                     ,:ADE-TP-RIAT
                      :IND-ADE-TP-RIAT
                     ,:ADE-TP-MOD-PAG-TIT
                     ,:ADE-TP-IAS
                      :IND-ADE-TP-IAS
                     ,:ADE-DT-VARZ-TP-IAS-DB
                      :IND-ADE-DT-VARZ-TP-IAS
                     ,:ADE-PRE-NET-IND
                      :IND-ADE-PRE-NET-IND
                     ,:ADE-PRE-LRD-IND
                      :IND-ADE-PRE-LRD-IND
                     ,:ADE-RAT-LRD-IND
                      :IND-ADE-RAT-LRD-IND
                     ,:ADE-PRSTZ-INI-IND
                      :IND-ADE-PRSTZ-INI-IND
                     ,:ADE-FL-COINC-ASSTO
                      :IND-ADE-FL-COINC-ASSTO
                     ,:ADE-IB-DFLT
                      :IND-ADE-IB-DFLT
                     ,:ADE-MOD-CALC
                      :IND-ADE-MOD-CALC
                     ,:ADE-TP-FNT-CNBTVA
                      :IND-ADE-TP-FNT-CNBTVA
                     ,:ADE-IMP-AZ
                      :IND-ADE-IMP-AZ
                     ,:ADE-IMP-ADER
                      :IND-ADE-IMP-ADER
                     ,:ADE-IMP-TFR
                      :IND-ADE-IMP-TFR
                     ,:ADE-IMP-VOLO
                      :IND-ADE-IMP-VOLO
                     ,:ADE-PC-AZ
                      :IND-ADE-PC-AZ
                     ,:ADE-PC-ADER
                      :IND-ADE-PC-ADER
                     ,:ADE-PC-TFR
                      :IND-ADE-PC-TFR
                     ,:ADE-PC-VOLO
                      :IND-ADE-PC-VOLO
                     ,:ADE-DT-NOVA-RGM-FISC-DB
                      :IND-ADE-DT-NOVA-RGM-FISC
                     ,:ADE-FL-ATTIV
                      :IND-ADE-FL-ATTIV
                     ,:ADE-IMP-REC-RIT-VIS
                      :IND-ADE-IMP-REC-RIT-VIS
                     ,:ADE-IMP-REC-RIT-ACC
                      :IND-ADE-IMP-REC-RIT-ACC
                     ,:ADE-FL-VARZ-STAT-TBGC
                      :IND-ADE-FL-VARZ-STAT-TBGC
                     ,:ADE-FL-PROVZA-MIGRAZ
                      :IND-ADE-FL-PROVZA-MIGRAZ
                     ,:ADE-IMPB-VIS-DA-REC
                      :IND-ADE-IMPB-VIS-DA-REC
                     ,:ADE-DT-DECOR-PREST-BAN-DB
                      :IND-ADE-DT-DECOR-PREST-BAN
                     ,:ADE-DT-EFF-VARZ-STAT-T-DB
                      :IND-ADE-DT-EFF-VARZ-STAT-T
                     ,:ADE-DS-RIGA
                     ,:ADE-DS-OPER-SQL
                     ,:ADE-DS-VER
                     ,:ADE-DS-TS-INI-CPTZ
                     ,:ADE-DS-TS-END-CPTZ
                     ,:ADE-DS-UTENTE
                     ,:ADE-DS-STATO-ELAB
                     ,:ADE-CUM-CNBT-CAP
                      :IND-ADE-CUM-CNBT-CAP
                     ,:ADE-IMP-GAR-CNBT
                      :IND-ADE-IMP-GAR-CNBT
                     ,:ADE-DT-ULT-CONS-CNBT-DB
                      :IND-ADE-DT-ULT-CONS-CNBT
                     ,:ADE-IDEN-ISC-FND
                      :IND-ADE-IDEN-ISC-FND
                     ,:ADE-NUM-RAT-PIAN
                      :IND-ADE-NUM-RAT-PIAN
                     ,:ADE-DT-PRESC-DB
                      :IND-ADE-DT-PRESC
                     ,:ADE-CONCS-PREST
                      :IND-ADE-CONCS-PREST
                     ,:POL-ID-POLI
                     ,:POL-ID-MOVI-CRZ
                     ,:POL-ID-MOVI-CHIU
                      :IND-POL-ID-MOVI-CHIU
                     ,:POL-IB-OGG
                      :IND-POL-IB-OGG
                     ,:POL-IB-PROP
                     ,:POL-DT-PROP-DB
                      :IND-POL-DT-PROP
                     ,:POL-DT-INI-EFF-DB
                     ,:POL-DT-END-EFF-DB
                     ,:POL-COD-COMP-ANIA
                     ,:POL-DT-DECOR-DB
                     ,:POL-DT-EMIS-DB
                     ,:POL-TP-POLI
                     ,:POL-DUR-AA
                      :IND-POL-DUR-AA
                     ,:POL-DUR-MM
                      :IND-POL-DUR-MM
                     ,:POL-DT-SCAD-DB
                      :IND-POL-DT-SCAD
                     ,:POL-COD-PROD
                     ,:POL-DT-INI-VLDT-PROD-DB
                     ,:POL-COD-CONV
                      :IND-POL-COD-CONV
                     ,:POL-COD-RAMO
                      :IND-POL-COD-RAMO
                     ,:POL-DT-INI-VLDT-CONV-DB
                      :IND-POL-DT-INI-VLDT-CONV
                     ,:POL-DT-APPLZ-CONV-DB
                      :IND-POL-DT-APPLZ-CONV
                     ,:POL-TP-FRM-ASSVA
                     ,:POL-TP-RGM-FISC
                      :IND-POL-TP-RGM-FISC
                     ,:POL-FL-ESTAS
                      :IND-POL-FL-ESTAS
                     ,:POL-FL-RSH-COMUN
                      :IND-POL-FL-RSH-COMUN
                     ,:POL-FL-RSH-COMUN-COND
                      :IND-POL-FL-RSH-COMUN-COND
                     ,:POL-TP-LIV-GENZ-TIT
                     ,:POL-FL-COP-FINANZ
                      :IND-POL-FL-COP-FINANZ
                     ,:POL-TP-APPLZ-DIR
                      :IND-POL-TP-APPLZ-DIR
                     ,:POL-SPE-MED
                      :IND-POL-SPE-MED
                     ,:POL-DIR-EMIS
                      :IND-POL-DIR-EMIS
                     ,:POL-DIR-1O-VERS
                      :IND-POL-DIR-1O-VERS
                     ,:POL-DIR-VERS-AGG
                      :IND-POL-DIR-VERS-AGG
                     ,:POL-COD-DVS
                      :IND-POL-COD-DVS
                     ,:POL-FL-FNT-AZ
                      :IND-POL-FL-FNT-AZ
                     ,:POL-FL-FNT-ADER
                      :IND-POL-FL-FNT-ADER
                     ,:POL-FL-FNT-TFR
                      :IND-POL-FL-FNT-TFR
                     ,:POL-FL-FNT-VOLO
                      :IND-POL-FL-FNT-VOLO
                     ,:POL-TP-OPZ-A-SCAD
                      :IND-POL-TP-OPZ-A-SCAD
                     ,:POL-AA-DIFF-PROR-DFLT
                      :IND-POL-AA-DIFF-PROR-DFLT
                     ,:POL-FL-VER-PROD
                      :IND-POL-FL-VER-PROD
                     ,:POL-DUR-GG
                      :IND-POL-DUR-GG
                     ,:POL-DIR-QUIET
                      :IND-POL-DIR-QUIET
                     ,:POL-TP-PTF-ESTNO
                      :IND-POL-TP-PTF-ESTNO
                     ,:POL-FL-CUM-PRE-CNTR
                      :IND-POL-FL-CUM-PRE-CNTR
                     ,:POL-FL-AMMB-MOVI
                      :IND-POL-FL-AMMB-MOVI
                     ,:POL-CONV-GECO
                      :IND-POL-CONV-GECO
                     ,:POL-DS-RIGA
                     ,:POL-DS-OPER-SQL
                     ,:POL-DS-VER
                     ,:POL-DS-TS-INI-CPTZ
                     ,:POL-DS-TS-END-CPTZ
                     ,:POL-DS-UTENTE
                     ,:POL-DS-STATO-ELAB
                     ,:POL-FL-SCUDO-FISC
                      :IND-POL-FL-SCUDO-FISC
                     ,:POL-FL-TRASFE
                      :IND-POL-FL-TRASFE
                     ,:POL-FL-TFR-STRC
                      :IND-POL-FL-TFR-STRC
                     ,:POL-DT-PRESC-DB
                      :IND-POL-DT-PRESC
                     ,:POL-COD-CONV-AGG
                      :IND-POL-COD-CONV-AGG
                     ,:POL-SUBCAT-PROD
                      :IND-POL-SUBCAT-PROD
                     ,:POL-FL-QUEST-ADEGZ-ASS
                      :IND-POL-FL-QUEST-ADEGZ-ASS
                     ,:POL-COD-TPA
                      :IND-POL-COD-TPA
                     ,:POL-ID-ACC-COMM
                      :IND-POL-ID-ACC-COMM
                     ,:POL-FL-POLI-CPI-PR
                      :IND-POL-FL-POLI-CPI-PR
                     ,:POL-FL-POLI-BUNDLING
                      :IND-POL-FL-POLI-BUNDLING
                     ,:POL-IND-POLI-PRIN-COLL
                      :IND-POL-IND-POLI-PRIN-COLL
                     ,:POL-FL-VND-BUNDLE
                      :IND-POL-FL-VND-BUNDLE
                     ,:POL-IB-BS
                      :IND-POL-IB-BS
                     ,:POL-FL-POLI-IFP
                      :IND-POL-FL-POLI-IFP
                     ,:STB-ID-STAT-OGG-BUS
                     ,:STB-ID-OGG
                     ,:STB-TP-OGG
                     ,:STB-ID-MOVI-CRZ
                     ,:STB-ID-MOVI-CHIU
                      :IND-STB-ID-MOVI-CHIU
                     ,:STB-DT-INI-EFF-DB
                     ,:STB-DT-END-EFF-DB
                     ,:STB-COD-COMP-ANIA
                     ,:STB-TP-STAT-BUS
                     ,:STB-TP-CAUS
                     ,:STB-DS-RIGA
                     ,:STB-DS-OPER-SQL
                     ,:STB-DS-VER
                     ,:STB-DS-TS-INI-CPTZ
                     ,:STB-DS-TS-END-CPTZ
                     ,:STB-DS-UTENTE
                     ,:STB-DS-STATO-ELAB
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-SC05 THRU A370-SC05-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       A390-SC05-EX.
           EXIT.


      ******************************************************************
      *
      ******************************************************************
       SC06-SELECTION-CURSOR-06.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-SC06           THRU A310-SC06-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A360-OPEN-CURSOR-SC06      THRU A360-SC06-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR-SC06     THRU A370-SC06-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST-SC06      THRU A380-SC06-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT-SC06       THRU A390-SC06-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A320-UPDATE-SC06           THRU A320-SC06-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER          TO TRUE
           END-EVALUATE.

       SC06-EX.
           EXIT.

      ******************************************************************
       A305-DECLARE-CURSOR-SC06.

           EXEC SQL
                DECLARE CUR-SC06 CURSOR WITH HOLD FOR
             SELECT
                    A.ID_ADES
                   ,A.ID_POLI
                   ,A.ID_MOVI_CRZ
                   ,A.ID_MOVI_CHIU
                   ,A.DT_INI_EFF
                   ,A.DT_END_EFF
                   ,A.IB_PREV
                   ,A.IB_OGG
                   ,A.COD_COMP_ANIA
                   ,A.DT_DECOR
                   ,A.DT_SCAD
                   ,A.ETA_A_SCAD
                   ,A.DUR_AA
                   ,A.DUR_MM
                   ,A.DUR_GG
                   ,A.TP_RGM_FISC
                   ,A.TP_RIAT
                   ,A.TP_MOD_PAG_TIT
                   ,A.TP_IAS
                   ,A.DT_VARZ_TP_IAS
                   ,A.PRE_NET_IND
                   ,A.PRE_LRD_IND
                   ,A.RAT_LRD_IND
                   ,A.PRSTZ_INI_IND
                   ,A.FL_COINC_ASSTO
                   ,A.IB_DFLT
                   ,A.MOD_CALC
                   ,A.TP_FNT_CNBTVA
                   ,A.IMP_AZ
                   ,A.IMP_ADER
                   ,A.IMP_TFR
                   ,A.IMP_VOLO
                   ,A.PC_AZ
                   ,A.PC_ADER
                   ,A.PC_TFR
                   ,A.PC_VOLO
                   ,A.DT_NOVA_RGM_FISC
                   ,A.FL_ATTIV
                   ,A.IMP_REC_RIT_VIS
                   ,A.IMP_REC_RIT_ACC
                   ,A.FL_VARZ_STAT_TBGC
                   ,A.FL_PROVZA_MIGRAZ
                   ,A.IMPB_VIS_DA_REC
                   ,A.DT_DECOR_PREST_BAN
                   ,A.DT_EFF_VARZ_STAT_T
                   ,A.DS_RIGA
                   ,A.DS_OPER_SQL
                   ,A.DS_VER
                   ,A.DS_TS_INI_CPTZ
                   ,A.DS_TS_END_CPTZ
                   ,A.DS_UTENTE
                   ,A.DS_STATO_ELAB
                   ,A.CUM_CNBT_CAP
                   ,A.IMP_GAR_CNBT
                   ,A.DT_ULT_CONS_CNBT
                   ,A.IDEN_ISC_FND
                   ,A.NUM_RAT_PIAN
                   ,A.DT_PRESC
                   ,A.CONCS_PREST
                   ,B.ID_POLI
                   ,B.ID_MOVI_CRZ
                   ,B.ID_MOVI_CHIU
                   ,B.IB_OGG
                   ,B.IB_PROP
                   ,B.DT_PROP
                   ,B.DT_INI_EFF
                   ,B.DT_END_EFF
                   ,B.COD_COMP_ANIA
                   ,B.DT_DECOR
                   ,B.DT_EMIS
                   ,B.TP_POLI
                   ,B.DUR_AA
                   ,B.DUR_MM
                   ,B.DT_SCAD
                   ,B.COD_PROD
                   ,B.DT_INI_VLDT_PROD
                   ,B.COD_CONV
                   ,B.COD_RAMO
                   ,B.DT_INI_VLDT_CONV
                   ,B.DT_APPLZ_CONV
                   ,B.TP_FRM_ASSVA
                   ,B.TP_RGM_FISC
                   ,B.FL_ESTAS
                   ,B.FL_RSH_COMUN
                   ,B.FL_RSH_COMUN_COND
                   ,B.TP_LIV_GENZ_TIT
                   ,B.FL_COP_FINANZ
                   ,B.TP_APPLZ_DIR
                   ,B.SPE_MED
                   ,B.DIR_EMIS
                   ,B.DIR_1O_VERS
                   ,B.DIR_VERS_AGG
                   ,B.COD_DVS
                   ,B.FL_FNT_AZ
                   ,B.FL_FNT_ADER
                   ,B.FL_FNT_TFR
                   ,B.FL_FNT_VOLO
                   ,B.TP_OPZ_A_SCAD
                   ,B.AA_DIFF_PROR_DFLT
                   ,B.FL_VER_PROD
                   ,B.DUR_GG
                   ,B.DIR_QUIET
                   ,B.TP_PTF_ESTNO
                   ,B.FL_CUM_PRE_CNTR
                   ,B.FL_AMMB_MOVI
                   ,B.CONV_GECO
                   ,B.DS_RIGA
                   ,B.DS_OPER_SQL
                   ,B.DS_VER
                   ,B.DS_TS_INI_CPTZ
                   ,B.DS_TS_END_CPTZ
                   ,B.DS_UTENTE
                   ,B.DS_STATO_ELAB
                   ,B.FL_SCUDO_FISC
                   ,B.FL_TRASFE
                   ,B.FL_TFR_STRC
                   ,B.DT_PRESC
                   ,B.COD_CONV_AGG
                   ,B.SUBCAT_PROD
                   ,B.FL_QUEST_ADEGZ_ASS
                   ,B.COD_TPA
                   ,B.ID_ACC_COMM
                   ,B.FL_POLI_CPI_PR
                   ,B.FL_POLI_BUNDLING
                   ,B.IND_POLI_PRIN_COLL
                   ,B.FL_VND_BUNDLE
                   ,B.IB_BS
                   ,B.FL_POLI_IFP
                   ,C.ID_STAT_OGG_BUS
                   ,C.ID_OGG
                   ,C.TP_OGG
                   ,C.ID_MOVI_CRZ
                   ,C.ID_MOVI_CHIU
                   ,C.DT_INI_EFF
                   ,C.DT_END_EFF
                   ,C.COD_COMP_ANIA
                   ,C.TP_STAT_BUS
                   ,C.TP_CAUS
                   ,C.DS_RIGA
                   ,C.DS_OPER_SQL
                   ,C.DS_VER
                   ,C.DS_TS_INI_CPTZ
                   ,C.DS_TS_END_CPTZ
                   ,C.DS_UTENTE
                   ,C.DS_STATO_ELAB
             FROM ADES      A,
                  POLI      B,
                  STAT_OGG_BUS C
             WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
                                        :WS-TP-FRM-ASSVA2 )
               AND B.COD_PROD     =  :WLB-COD-PROD
               AND A.ID_POLI      =   B.ID_POLI
               AND A.ID_ADES      =   C.ID_OGG
               AND C.TP_OGG       =  'AD'
      *--  D C.TP_STAT_BUS     =  'VI'
               AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
               AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
               AND A.DT_END_EFF  >   :WS-DT-PTF-X
               AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
               AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
               AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
               AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
               AND B.DT_END_EFF  >   :WS-DT-PTF-X
               AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
               AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
               AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
               AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
               AND C.DT_END_EFF  >   :WS-DT-PTF-X
               AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
               AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
               AND A.ID_ADES IN
                  (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
                                                   STAT_OGG_BUS E
                   WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
                   AND E.TP_OGG = 'TG'
                   AND E.TP_STAT_BUS IN ('VI','ST')
                   AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
                   AND D.COD_COMP_ANIA =
                       :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND D.DT_INI_EFF <=   :WS-DT-PTF-X
                   AND D.DT_END_EFF >    :WS-DT-PTF-X
                   AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                   AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                   AND E.DT_INI_EFF <=   :WS-DT-PTF-X
                   AND E.DT_END_EFF >    :WS-DT-PTF-X
                   AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                   AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                   AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
                        OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
                   )
               AND A.DS_STATO_ELAB IN (
                                       :IABV0002-STATE-01,
                                       :IABV0002-STATE-02,
                                       :IABV0002-STATE-03,
                                       :IABV0002-STATE-04,
                                       :IABV0002-STATE-05,
                                       :IABV0002-STATE-06,
                                       :IABV0002-STATE-07,
                                       :IABV0002-STATE-08,
                                       :IABV0002-STATE-09,
                                       :IABV0002-STATE-10
                                         )
               AND NOT A.DS_VER  = :IABV0009-VERSIONING
               ORDER BY A.ID_POLI, A.ID_ADES
           END-EXEC.

       A305-SC06-EX.
           EXIT.
      ******************************************************************
       A310-SELECT-SC06.

           EXEC SQL
             SELECT
                    A.ID_ADES
                   ,A.ID_POLI
                   ,A.ID_MOVI_CRZ
                   ,A.ID_MOVI_CHIU
                   ,A.DT_INI_EFF
                   ,A.DT_END_EFF
                   ,A.IB_PREV
                   ,A.IB_OGG
                   ,A.COD_COMP_ANIA
                   ,A.DT_DECOR
                   ,A.DT_SCAD
                   ,A.ETA_A_SCAD
                   ,A.DUR_AA
                   ,A.DUR_MM
                   ,A.DUR_GG
                   ,A.TP_RGM_FISC
                   ,A.TP_RIAT
                   ,A.TP_MOD_PAG_TIT
                   ,A.TP_IAS
                   ,A.DT_VARZ_TP_IAS
                   ,A.PRE_NET_IND
                   ,A.PRE_LRD_IND
                   ,A.RAT_LRD_IND
                   ,A.PRSTZ_INI_IND
                   ,A.FL_COINC_ASSTO
                   ,A.IB_DFLT
                   ,A.MOD_CALC
                   ,A.TP_FNT_CNBTVA
                   ,A.IMP_AZ
                   ,A.IMP_ADER
                   ,A.IMP_TFR
                   ,A.IMP_VOLO
                   ,A.PC_AZ
                   ,A.PC_ADER
                   ,A.PC_TFR
                   ,A.PC_VOLO
                   ,A.DT_NOVA_RGM_FISC
                   ,A.FL_ATTIV
                   ,A.IMP_REC_RIT_VIS
                   ,A.IMP_REC_RIT_ACC
                   ,A.FL_VARZ_STAT_TBGC
                   ,A.FL_PROVZA_MIGRAZ
                   ,A.IMPB_VIS_DA_REC
                   ,A.DT_DECOR_PREST_BAN
                   ,A.DT_EFF_VARZ_STAT_T
                   ,A.DS_RIGA
                   ,A.DS_OPER_SQL
                   ,A.DS_VER
                   ,A.DS_TS_INI_CPTZ
                   ,A.DS_TS_END_CPTZ
                   ,A.DS_UTENTE
                   ,A.DS_STATO_ELAB
                   ,A.CUM_CNBT_CAP
                   ,A.IMP_GAR_CNBT
                   ,A.DT_ULT_CONS_CNBT
                   ,A.IDEN_ISC_FND
                   ,A.NUM_RAT_PIAN
                   ,A.DT_PRESC
                   ,A.CONCS_PREST
                   ,B.ID_POLI
                   ,B.ID_MOVI_CRZ
                   ,B.ID_MOVI_CHIU
                   ,B.IB_OGG
                   ,B.IB_PROP
                   ,B.DT_PROP
                   ,B.DT_INI_EFF
                   ,B.DT_END_EFF
                   ,B.COD_COMP_ANIA
                   ,B.DT_DECOR
                   ,B.DT_EMIS
                   ,B.TP_POLI
                   ,B.DUR_AA
                   ,B.DUR_MM
                   ,B.DT_SCAD
                   ,B.COD_PROD
                   ,B.DT_INI_VLDT_PROD
                   ,B.COD_CONV
                   ,B.COD_RAMO
                   ,B.DT_INI_VLDT_CONV
                   ,B.DT_APPLZ_CONV
                   ,B.TP_FRM_ASSVA
                   ,B.TP_RGM_FISC
                   ,B.FL_ESTAS
                   ,B.FL_RSH_COMUN
                   ,B.FL_RSH_COMUN_COND
                   ,B.TP_LIV_GENZ_TIT
                   ,B.FL_COP_FINANZ
                   ,B.TP_APPLZ_DIR
                   ,B.SPE_MED
                   ,B.DIR_EMIS
                   ,B.DIR_1O_VERS
                   ,B.DIR_VERS_AGG
                   ,B.COD_DVS
                   ,B.FL_FNT_AZ
                   ,B.FL_FNT_ADER
                   ,B.FL_FNT_TFR
                   ,B.FL_FNT_VOLO
                   ,B.TP_OPZ_A_SCAD
                   ,B.AA_DIFF_PROR_DFLT
                   ,B.FL_VER_PROD
                   ,B.DUR_GG
                   ,B.DIR_QUIET
                   ,B.TP_PTF_ESTNO
                   ,B.FL_CUM_PRE_CNTR
                   ,B.FL_AMMB_MOVI
                   ,B.CONV_GECO
                   ,B.DS_RIGA
                   ,B.DS_OPER_SQL
                   ,B.DS_VER
                   ,B.DS_TS_INI_CPTZ
                   ,B.DS_TS_END_CPTZ
                   ,B.DS_UTENTE
                   ,B.DS_STATO_ELAB
                   ,B.FL_SCUDO_FISC
                   ,B.FL_TRASFE
                   ,B.FL_TFR_STRC
                   ,B.DT_PRESC
                   ,B.COD_CONV_AGG
                   ,B.SUBCAT_PROD
                   ,B.FL_QUEST_ADEGZ_ASS
                   ,B.COD_TPA
                   ,B.ID_ACC_COMM
                   ,B.FL_POLI_CPI_PR
                   ,B.FL_POLI_BUNDLING
                   ,B.IND_POLI_PRIN_COLL
                   ,B.FL_VND_BUNDLE
                   ,B.IB_BS
                   ,B.FL_POLI_IFP
                   ,C.ID_STAT_OGG_BUS
                   ,C.ID_OGG
                   ,C.TP_OGG
                   ,C.ID_MOVI_CRZ
                   ,C.ID_MOVI_CHIU
                   ,C.DT_INI_EFF
                   ,C.DT_END_EFF
                   ,C.COD_COMP_ANIA
                   ,C.TP_STAT_BUS
                   ,C.TP_CAUS
                   ,C.DS_RIGA
                   ,C.DS_OPER_SQL
                   ,C.DS_VER
                   ,C.DS_TS_INI_CPTZ
                   ,C.DS_TS_END_CPTZ
                   ,C.DS_UTENTE
                   ,C.DS_STATO_ELAB
             INTO
                   :ADE-ID-ADES
                  ,:ADE-ID-POLI
                  ,:ADE-ID-MOVI-CRZ
                  ,:ADE-ID-MOVI-CHIU
                   :IND-ADE-ID-MOVI-CHIU
                  ,:ADE-DT-INI-EFF-DB
                  ,:ADE-DT-END-EFF-DB
                  ,:ADE-IB-PREV
                   :IND-ADE-IB-PREV
                  ,:ADE-IB-OGG
                   :IND-ADE-IB-OGG
                  ,:ADE-COD-COMP-ANIA
                  ,:ADE-DT-DECOR-DB
                   :IND-ADE-DT-DECOR
                  ,:ADE-DT-SCAD-DB
                   :IND-ADE-DT-SCAD
                  ,:ADE-ETA-A-SCAD
                   :IND-ADE-ETA-A-SCAD
                  ,:ADE-DUR-AA
                   :IND-ADE-DUR-AA
                  ,:ADE-DUR-MM
                   :IND-ADE-DUR-MM
                  ,:ADE-DUR-GG
                   :IND-ADE-DUR-GG
                  ,:ADE-TP-RGM-FISC
                  ,:ADE-TP-RIAT
                   :IND-ADE-TP-RIAT
                  ,:ADE-TP-MOD-PAG-TIT
                  ,:ADE-TP-IAS
                   :IND-ADE-TP-IAS
                  ,:ADE-DT-VARZ-TP-IAS-DB
                   :IND-ADE-DT-VARZ-TP-IAS
                  ,:ADE-PRE-NET-IND
                   :IND-ADE-PRE-NET-IND
                  ,:ADE-PRE-LRD-IND
                   :IND-ADE-PRE-LRD-IND
                  ,:ADE-RAT-LRD-IND
                   :IND-ADE-RAT-LRD-IND
                  ,:ADE-PRSTZ-INI-IND
                   :IND-ADE-PRSTZ-INI-IND
                  ,:ADE-FL-COINC-ASSTO
                   :IND-ADE-FL-COINC-ASSTO
                  ,:ADE-IB-DFLT
                   :IND-ADE-IB-DFLT
                  ,:ADE-MOD-CALC
                   :IND-ADE-MOD-CALC
                  ,:ADE-TP-FNT-CNBTVA
                   :IND-ADE-TP-FNT-CNBTVA
                  ,:ADE-IMP-AZ
                   :IND-ADE-IMP-AZ
                  ,:ADE-IMP-ADER
                   :IND-ADE-IMP-ADER
                  ,:ADE-IMP-TFR
                   :IND-ADE-IMP-TFR
                  ,:ADE-IMP-VOLO
                   :IND-ADE-IMP-VOLO
                  ,:ADE-PC-AZ
                   :IND-ADE-PC-AZ
                  ,:ADE-PC-ADER
                   :IND-ADE-PC-ADER
                  ,:ADE-PC-TFR
                   :IND-ADE-PC-TFR
                  ,:ADE-PC-VOLO
                   :IND-ADE-PC-VOLO
                  ,:ADE-DT-NOVA-RGM-FISC-DB
                   :IND-ADE-DT-NOVA-RGM-FISC
                  ,:ADE-FL-ATTIV
                   :IND-ADE-FL-ATTIV
                  ,:ADE-IMP-REC-RIT-VIS
                   :IND-ADE-IMP-REC-RIT-VIS
                  ,:ADE-IMP-REC-RIT-ACC
                   :IND-ADE-IMP-REC-RIT-ACC
                  ,:ADE-FL-VARZ-STAT-TBGC
                   :IND-ADE-FL-VARZ-STAT-TBGC
                  ,:ADE-FL-PROVZA-MIGRAZ
                   :IND-ADE-FL-PROVZA-MIGRAZ
                  ,:ADE-IMPB-VIS-DA-REC
                   :IND-ADE-IMPB-VIS-DA-REC
                  ,:ADE-DT-DECOR-PREST-BAN-DB
                   :IND-ADE-DT-DECOR-PREST-BAN
                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
                   :IND-ADE-DT-EFF-VARZ-STAT-T
                  ,:ADE-DS-RIGA
                  ,:ADE-DS-OPER-SQL
                  ,:ADE-DS-VER
                  ,:ADE-DS-TS-INI-CPTZ
                  ,:ADE-DS-TS-END-CPTZ
                  ,:ADE-DS-UTENTE
                  ,:ADE-DS-STATO-ELAB
                  ,:ADE-CUM-CNBT-CAP
                   :IND-ADE-CUM-CNBT-CAP
                  ,:ADE-IMP-GAR-CNBT
                   :IND-ADE-IMP-GAR-CNBT
                  ,:ADE-DT-ULT-CONS-CNBT-DB
                   :IND-ADE-DT-ULT-CONS-CNBT
                  ,:ADE-IDEN-ISC-FND
                   :IND-ADE-IDEN-ISC-FND
                  ,:ADE-NUM-RAT-PIAN
                   :IND-ADE-NUM-RAT-PIAN
                  ,:ADE-DT-PRESC-DB
                   :IND-ADE-DT-PRESC
                  ,:ADE-CONCS-PREST
                   :IND-ADE-CONCS-PREST
                  ,:POL-ID-POLI
                  ,:POL-ID-MOVI-CRZ
                  ,:POL-ID-MOVI-CHIU
                   :IND-POL-ID-MOVI-CHIU
                  ,:POL-IB-OGG
                   :IND-POL-IB-OGG
                  ,:POL-IB-PROP
                  ,:POL-DT-PROP-DB
                   :IND-POL-DT-PROP
                  ,:POL-DT-INI-EFF-DB
                  ,:POL-DT-END-EFF-DB
                  ,:POL-COD-COMP-ANIA
                  ,:POL-DT-DECOR-DB
                  ,:POL-DT-EMIS-DB
                  ,:POL-TP-POLI
                  ,:POL-DUR-AA
                   :IND-POL-DUR-AA
                  ,:POL-DUR-MM
                   :IND-POL-DUR-MM
                  ,:POL-DT-SCAD-DB
                   :IND-POL-DT-SCAD
                  ,:POL-COD-PROD
                  ,:POL-DT-INI-VLDT-PROD-DB
                  ,:POL-COD-CONV
                   :IND-POL-COD-CONV
                  ,:POL-COD-RAMO
                   :IND-POL-COD-RAMO
                  ,:POL-DT-INI-VLDT-CONV-DB
                   :IND-POL-DT-INI-VLDT-CONV
                  ,:POL-DT-APPLZ-CONV-DB
                   :IND-POL-DT-APPLZ-CONV
                  ,:POL-TP-FRM-ASSVA
                  ,:POL-TP-RGM-FISC
                   :IND-POL-TP-RGM-FISC
                  ,:POL-FL-ESTAS
                   :IND-POL-FL-ESTAS
                  ,:POL-FL-RSH-COMUN
                   :IND-POL-FL-RSH-COMUN
                  ,:POL-FL-RSH-COMUN-COND
                   :IND-POL-FL-RSH-COMUN-COND
                  ,:POL-TP-LIV-GENZ-TIT
                  ,:POL-FL-COP-FINANZ
                   :IND-POL-FL-COP-FINANZ
                  ,:POL-TP-APPLZ-DIR
                   :IND-POL-TP-APPLZ-DIR
                  ,:POL-SPE-MED
                   :IND-POL-SPE-MED
                  ,:POL-DIR-EMIS
                   :IND-POL-DIR-EMIS
                  ,:POL-DIR-1O-VERS
                   :IND-POL-DIR-1O-VERS
                  ,:POL-DIR-VERS-AGG
                   :IND-POL-DIR-VERS-AGG
                  ,:POL-COD-DVS
                   :IND-POL-COD-DVS
                  ,:POL-FL-FNT-AZ
                   :IND-POL-FL-FNT-AZ
                  ,:POL-FL-FNT-ADER
                   :IND-POL-FL-FNT-ADER
                  ,:POL-FL-FNT-TFR
                   :IND-POL-FL-FNT-TFR
                  ,:POL-FL-FNT-VOLO
                   :IND-POL-FL-FNT-VOLO
                  ,:POL-TP-OPZ-A-SCAD
                   :IND-POL-TP-OPZ-A-SCAD
                  ,:POL-AA-DIFF-PROR-DFLT
                   :IND-POL-AA-DIFF-PROR-DFLT
                  ,:POL-FL-VER-PROD
                   :IND-POL-FL-VER-PROD
                  ,:POL-DUR-GG
                   :IND-POL-DUR-GG
                  ,:POL-DIR-QUIET
                   :IND-POL-DIR-QUIET
                  ,:POL-TP-PTF-ESTNO
                   :IND-POL-TP-PTF-ESTNO
                  ,:POL-FL-CUM-PRE-CNTR
                   :IND-POL-FL-CUM-PRE-CNTR
                  ,:POL-FL-AMMB-MOVI
                   :IND-POL-FL-AMMB-MOVI
                  ,:POL-CONV-GECO
                   :IND-POL-CONV-GECO
                  ,:POL-DS-RIGA
                  ,:POL-DS-OPER-SQL
                  ,:POL-DS-VER
                  ,:POL-DS-TS-INI-CPTZ
                  ,:POL-DS-TS-END-CPTZ
                  ,:POL-DS-UTENTE
                  ,:POL-DS-STATO-ELAB
                  ,:POL-FL-SCUDO-FISC
                   :IND-POL-FL-SCUDO-FISC
                  ,:POL-FL-TRASFE
                   :IND-POL-FL-TRASFE
                  ,:POL-FL-TFR-STRC
                   :IND-POL-FL-TFR-STRC
                  ,:POL-DT-PRESC-DB
                   :IND-POL-DT-PRESC
                  ,:POL-COD-CONV-AGG
                   :IND-POL-COD-CONV-AGG
                  ,:POL-SUBCAT-PROD
                   :IND-POL-SUBCAT-PROD
                  ,:POL-FL-QUEST-ADEGZ-ASS
                   :IND-POL-FL-QUEST-ADEGZ-ASS
                  ,:POL-COD-TPA
                   :IND-POL-COD-TPA
                  ,:POL-ID-ACC-COMM
                   :IND-POL-ID-ACC-COMM
                  ,:POL-FL-POLI-CPI-PR
                   :IND-POL-FL-POLI-CPI-PR
                  ,:POL-FL-POLI-BUNDLING
                   :IND-POL-FL-POLI-BUNDLING
                  ,:POL-IND-POLI-PRIN-COLL
                   :IND-POL-IND-POLI-PRIN-COLL
                  ,:POL-FL-VND-BUNDLE
                   :IND-POL-FL-VND-BUNDLE
                  ,:POL-IB-BS
                   :IND-POL-IB-BS
                  ,:POL-FL-POLI-IFP
                   :IND-POL-FL-POLI-IFP
                  ,:STB-ID-STAT-OGG-BUS
                  ,:STB-ID-OGG
                  ,:STB-TP-OGG
                  ,:STB-ID-MOVI-CRZ
                  ,:STB-ID-MOVI-CHIU
                   :IND-STB-ID-MOVI-CHIU
                  ,:STB-DT-INI-EFF-DB
                  ,:STB-DT-END-EFF-DB
                  ,:STB-COD-COMP-ANIA
                  ,:STB-TP-STAT-BUS
                  ,:STB-TP-CAUS
                  ,:STB-DS-RIGA
                  ,:STB-DS-OPER-SQL
                  ,:STB-DS-VER
                  ,:STB-DS-TS-INI-CPTZ
                  ,:STB-DS-TS-END-CPTZ
                  ,:STB-DS-UTENTE
                  ,:STB-DS-STATO-ELAB
             FROM ADES      A,
                  POLI      B,
                  STAT_OGG_BUS C

             WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
                                        :WS-TP-FRM-ASSVA2 )
               AND B.COD_PROD     =  :WLB-COD-PROD
               AND A.ID_POLI      =   B.ID_POLI
               AND A.ID_ADES      =   C.ID_OGG
               AND C.TP_OGG       =  'AD'
      *--  D C.TP_STAT_BUS     =  'VI'
               AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
               AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
               AND A.DT_END_EFF  >   :WS-DT-PTF-X
               AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
               AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
               AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
               AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
               AND B.DT_END_EFF  >   :WS-DT-PTF-X
               AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
               AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
               AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
               AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
               AND C.DT_END_EFF  >   :WS-DT-PTF-X
               AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
               AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
               AND A.ID_ADES IN
                  (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
                                                   STAT_OGG_BUS E
                   WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
                   AND E.TP_OGG = 'TG'
                   AND E.TP_STAT_BUS IN ('VI','ST')
                   AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
                   AND D.COD_COMP_ANIA =
                       :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND D.DT_INI_EFF <=   :WS-DT-PTF-X
                   AND D.DT_END_EFF >    :WS-DT-PTF-X
                   AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                   AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                   AND E.DT_INI_EFF <=   :WS-DT-PTF-X
                   AND E.DT_END_EFF >    :WS-DT-PTF-X
                   AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                   AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                   AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
                        OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
                   )

               AND A.DS_STATO_ELAB IN (
                                       :IABV0002-STATE-01,
                                       :IABV0002-STATE-02,
                                       :IABV0002-STATE-03,
                                       :IABV0002-STATE-04,
                                       :IABV0002-STATE-05,
                                       :IABV0002-STATE-06,
                                       :IABV0002-STATE-07,
                                       :IABV0002-STATE-08,
                                       :IABV0002-STATE-09,
                                       :IABV0002-STATE-10
                                         )
               AND NOT A.DS_VER  = :IABV0009-VERSIONING
               ORDER BY A.ID_POLI, A.ID_ADES
              FETCH FIRST ROW ONLY
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-SC06-EX.
           EXIT.

      ******************************************************************
       A320-UPDATE-SC06.

           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
                   PERFORM A330-UPDATE-PK-SC06         THRU A330-SC06-EX

              WHEN IDSV0003-WHERE-CONDITION-06
                   PERFORM A340-UPDATE-WHERE-COND-SC06 THRU A340-SC06-EX
              WHEN IDSV0003-FIRST-ACTION
                   PERFORM A345-UPDATE-FIRST-ACTION-SC06
                                                       THRU A345-SC06-EX
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
           END-EVALUATE.

       A320-SC06-EX.
           EXIT.

      ******************************************************************

       A330-UPDATE-PK-SC06.

           PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX

           EXEC SQL
                UPDATE ADES
                SET
                   DS_OPER_SQL            = :ADE-DS-OPER-SQL
                  ,DS_VER                 = :IABV0009-VERSIONING
                  ,DS_UTENTE              = :ADE-DS-UTENTE
                  ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
                WHERE             DS_RIGA = :ADE-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A330-SC06-EX.
           EXIT.

      ******************************************************************

       A340-UPDATE-WHERE-COND-SC06.

           CONTINUE.

       A340-SC06-EX.
           EXIT.
      ******************************************************************
       A345-UPDATE-FIRST-ACTION-SC06.

           CONTINUE.

       A345-SC06-EX.
           EXIT.
      ******************************************************************

       A360-OPEN-CURSOR-SC06.

           PERFORM A305-DECLARE-CURSOR-SC06 THRU A305-SC06-EX.

           EXEC SQL
                OPEN CUR-SC06
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A360-SC06-EX.
           EXIT.

      ******************************************************************

       A370-CLOSE-CURSOR-SC06.

           EXEC SQL
                CLOSE CUR-SC06
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A370-SC06-EX.
           EXIT.

      ******************************************************************

       A380-FETCH-FIRST-SC06.


           PERFORM A360-OPEN-CURSOR-SC06    THRU A360-SC06-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-SC06  THRU A390-SC06-EX
           END-IF.

       A380-SC06-EX.
           EXIT.

      ******************************************************************

       A390-FETCH-NEXT-SC06.


           EXEC SQL
                FETCH CUR-SC06
           INTO
                   :ADE-ID-ADES
                  ,:ADE-ID-POLI
                  ,:ADE-ID-MOVI-CRZ
                  ,:ADE-ID-MOVI-CHIU
                   :IND-ADE-ID-MOVI-CHIU
                  ,:ADE-DT-INI-EFF-DB
                  ,:ADE-DT-END-EFF-DB
                  ,:ADE-IB-PREV
                   :IND-ADE-IB-PREV
                  ,:ADE-IB-OGG
                   :IND-ADE-IB-OGG
                  ,:ADE-COD-COMP-ANIA
                  ,:ADE-DT-DECOR-DB
                   :IND-ADE-DT-DECOR
                  ,:ADE-DT-SCAD-DB
                   :IND-ADE-DT-SCAD
                  ,:ADE-ETA-A-SCAD
                   :IND-ADE-ETA-A-SCAD
                  ,:ADE-DUR-AA
                   :IND-ADE-DUR-AA
                  ,:ADE-DUR-MM
                   :IND-ADE-DUR-MM
                  ,:ADE-DUR-GG
                   :IND-ADE-DUR-GG
                  ,:ADE-TP-RGM-FISC
                  ,:ADE-TP-RIAT
                   :IND-ADE-TP-RIAT
                  ,:ADE-TP-MOD-PAG-TIT
                  ,:ADE-TP-IAS
                   :IND-ADE-TP-IAS
                  ,:ADE-DT-VARZ-TP-IAS-DB
                   :IND-ADE-DT-VARZ-TP-IAS
                  ,:ADE-PRE-NET-IND
                   :IND-ADE-PRE-NET-IND
                  ,:ADE-PRE-LRD-IND
                   :IND-ADE-PRE-LRD-IND
                  ,:ADE-RAT-LRD-IND
                   :IND-ADE-RAT-LRD-IND
                  ,:ADE-PRSTZ-INI-IND
                   :IND-ADE-PRSTZ-INI-IND
                  ,:ADE-FL-COINC-ASSTO
                   :IND-ADE-FL-COINC-ASSTO
                  ,:ADE-IB-DFLT
                   :IND-ADE-IB-DFLT
                  ,:ADE-MOD-CALC
                   :IND-ADE-MOD-CALC
                  ,:ADE-TP-FNT-CNBTVA
                   :IND-ADE-TP-FNT-CNBTVA
                  ,:ADE-IMP-AZ
                   :IND-ADE-IMP-AZ
                  ,:ADE-IMP-ADER
                   :IND-ADE-IMP-ADER
                  ,:ADE-IMP-TFR
                   :IND-ADE-IMP-TFR
                  ,:ADE-IMP-VOLO
                   :IND-ADE-IMP-VOLO
                  ,:ADE-PC-AZ
                   :IND-ADE-PC-AZ
                  ,:ADE-PC-ADER
                   :IND-ADE-PC-ADER
                  ,:ADE-PC-TFR
                   :IND-ADE-PC-TFR
                  ,:ADE-PC-VOLO
                   :IND-ADE-PC-VOLO
                  ,:ADE-DT-NOVA-RGM-FISC-DB
                   :IND-ADE-DT-NOVA-RGM-FISC
                  ,:ADE-FL-ATTIV
                   :IND-ADE-FL-ATTIV
                  ,:ADE-IMP-REC-RIT-VIS
                   :IND-ADE-IMP-REC-RIT-VIS
                  ,:ADE-IMP-REC-RIT-ACC
                   :IND-ADE-IMP-REC-RIT-ACC
                  ,:ADE-FL-VARZ-STAT-TBGC
                   :IND-ADE-FL-VARZ-STAT-TBGC
                  ,:ADE-FL-PROVZA-MIGRAZ
                   :IND-ADE-FL-PROVZA-MIGRAZ
                  ,:ADE-IMPB-VIS-DA-REC
                   :IND-ADE-IMPB-VIS-DA-REC
                  ,:ADE-DT-DECOR-PREST-BAN-DB
                   :IND-ADE-DT-DECOR-PREST-BAN
                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
                   :IND-ADE-DT-EFF-VARZ-STAT-T
                  ,:ADE-DS-RIGA
                  ,:ADE-DS-OPER-SQL
                  ,:ADE-DS-VER
                  ,:ADE-DS-TS-INI-CPTZ
                  ,:ADE-DS-TS-END-CPTZ
                  ,:ADE-DS-UTENTE
                  ,:ADE-DS-STATO-ELAB
                  ,:ADE-CUM-CNBT-CAP
                   :IND-ADE-CUM-CNBT-CAP
                  ,:ADE-IMP-GAR-CNBT
                   :IND-ADE-IMP-GAR-CNBT
                  ,:ADE-DT-ULT-CONS-CNBT-DB
                   :IND-ADE-DT-ULT-CONS-CNBT
                  ,:ADE-IDEN-ISC-FND
                   :IND-ADE-IDEN-ISC-FND
                  ,:ADE-NUM-RAT-PIAN
                   :IND-ADE-NUM-RAT-PIAN
                  ,:ADE-DT-PRESC-DB
                   :IND-ADE-DT-PRESC
                  ,:ADE-CONCS-PREST
                   :IND-ADE-CONCS-PREST
                  ,:POL-ID-POLI
                  ,:POL-ID-MOVI-CRZ
                  ,:POL-ID-MOVI-CHIU
                   :IND-POL-ID-MOVI-CHIU
                  ,:POL-IB-OGG
                   :IND-POL-IB-OGG
                  ,:POL-IB-PROP
                  ,:POL-DT-PROP-DB
                   :IND-POL-DT-PROP
                  ,:POL-DT-INI-EFF-DB
                  ,:POL-DT-END-EFF-DB
                  ,:POL-COD-COMP-ANIA
                  ,:POL-DT-DECOR-DB
                  ,:POL-DT-EMIS-DB
                  ,:POL-TP-POLI
                  ,:POL-DUR-AA
                   :IND-POL-DUR-AA
                  ,:POL-DUR-MM
                   :IND-POL-DUR-MM
                  ,:POL-DT-SCAD-DB
                   :IND-POL-DT-SCAD
                  ,:POL-COD-PROD
                  ,:POL-DT-INI-VLDT-PROD-DB
                  ,:POL-COD-CONV
                   :IND-POL-COD-CONV
                  ,:POL-COD-RAMO
                   :IND-POL-COD-RAMO
                  ,:POL-DT-INI-VLDT-CONV-DB
                   :IND-POL-DT-INI-VLDT-CONV
                  ,:POL-DT-APPLZ-CONV-DB
                   :IND-POL-DT-APPLZ-CONV
                  ,:POL-TP-FRM-ASSVA
                  ,:POL-TP-RGM-FISC
                   :IND-POL-TP-RGM-FISC
                  ,:POL-FL-ESTAS
                   :IND-POL-FL-ESTAS
                  ,:POL-FL-RSH-COMUN
                   :IND-POL-FL-RSH-COMUN
                  ,:POL-FL-RSH-COMUN-COND
                   :IND-POL-FL-RSH-COMUN-COND
                  ,:POL-TP-LIV-GENZ-TIT
                  ,:POL-FL-COP-FINANZ
                   :IND-POL-FL-COP-FINANZ
                  ,:POL-TP-APPLZ-DIR
                   :IND-POL-TP-APPLZ-DIR
                  ,:POL-SPE-MED
                   :IND-POL-SPE-MED
                  ,:POL-DIR-EMIS
                   :IND-POL-DIR-EMIS
                  ,:POL-DIR-1O-VERS
                   :IND-POL-DIR-1O-VERS
                  ,:POL-DIR-VERS-AGG
                   :IND-POL-DIR-VERS-AGG
                  ,:POL-COD-DVS
                   :IND-POL-COD-DVS
                  ,:POL-FL-FNT-AZ
                   :IND-POL-FL-FNT-AZ
                  ,:POL-FL-FNT-ADER
                   :IND-POL-FL-FNT-ADER
                  ,:POL-FL-FNT-TFR
                   :IND-POL-FL-FNT-TFR
                  ,:POL-FL-FNT-VOLO
                   :IND-POL-FL-FNT-VOLO
                  ,:POL-TP-OPZ-A-SCAD
                   :IND-POL-TP-OPZ-A-SCAD
                  ,:POL-AA-DIFF-PROR-DFLT
                   :IND-POL-AA-DIFF-PROR-DFLT
                  ,:POL-FL-VER-PROD
                   :IND-POL-FL-VER-PROD
                  ,:POL-DUR-GG
                   :IND-POL-DUR-GG
                  ,:POL-DIR-QUIET
                   :IND-POL-DIR-QUIET
                  ,:POL-TP-PTF-ESTNO
                   :IND-POL-TP-PTF-ESTNO
                  ,:POL-FL-CUM-PRE-CNTR
                   :IND-POL-FL-CUM-PRE-CNTR
                  ,:POL-FL-AMMB-MOVI
                   :IND-POL-FL-AMMB-MOVI
                  ,:POL-CONV-GECO
                   :IND-POL-CONV-GECO
                  ,:POL-DS-RIGA
                  ,:POL-DS-OPER-SQL
                  ,:POL-DS-VER
                  ,:POL-DS-TS-INI-CPTZ
                  ,:POL-DS-TS-END-CPTZ
                  ,:POL-DS-UTENTE
                  ,:POL-DS-STATO-ELAB
                  ,:POL-FL-SCUDO-FISC
                   :IND-POL-FL-SCUDO-FISC
                  ,:POL-FL-TRASFE
                   :IND-POL-FL-TRASFE
                  ,:POL-FL-TFR-STRC
                   :IND-POL-FL-TFR-STRC
                  ,:POL-DT-PRESC-DB
                   :IND-POL-DT-PRESC
                  ,:POL-COD-CONV-AGG
                   :IND-POL-COD-CONV-AGG
                  ,:POL-SUBCAT-PROD
                   :IND-POL-SUBCAT-PROD
                  ,:POL-FL-QUEST-ADEGZ-ASS
                   :IND-POL-FL-QUEST-ADEGZ-ASS
                  ,:POL-COD-TPA
                   :IND-POL-COD-TPA
                  ,:POL-ID-ACC-COMM
                   :IND-POL-ID-ACC-COMM
                  ,:POL-FL-POLI-CPI-PR
                   :IND-POL-FL-POLI-CPI-PR
                  ,:POL-FL-POLI-BUNDLING
                   :IND-POL-FL-POLI-BUNDLING
                  ,:POL-IND-POLI-PRIN-COLL
                   :IND-POL-IND-POLI-PRIN-COLL
                  ,:POL-FL-VND-BUNDLE
                   :IND-POL-FL-VND-BUNDLE
                  ,:POL-IB-BS
                   :IND-POL-IB-BS
                  ,:POL-FL-POLI-IFP
                   :IND-POL-FL-POLI-IFP
                  ,:STB-ID-STAT-OGG-BUS
                  ,:STB-ID-OGG
                  ,:STB-TP-OGG
                  ,:STB-ID-MOVI-CRZ
                  ,:STB-ID-MOVI-CHIU
                   :IND-STB-ID-MOVI-CHIU
                  ,:STB-DT-INI-EFF-DB
                  ,:STB-DT-END-EFF-DB
                  ,:STB-COD-COMP-ANIA
                  ,:STB-TP-STAT-BUS
                  ,:STB-TP-CAUS
                  ,:STB-DS-RIGA
                  ,:STB-DS-OPER-SQL
                  ,:STB-DS-VER
                  ,:STB-DS-TS-INI-CPTZ
                  ,:STB-DS-TS-END-CPTZ
                  ,:STB-DS-UTENTE
                  ,:STB-DS-STATO-ELAB
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-SC06 THRU A370-SC06-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       A390-SC06-EX.
           EXIT.


      ******************************************************************
      *
      ******************************************************************
       SC07-SELECTION-CURSOR-07.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-SC07           THRU A310-SC07-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A360-OPEN-CURSOR-SC07      THRU A360-SC07-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR-SC07     THRU A370-SC07-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST-SC07      THRU A380-SC07-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT-SC07       THRU A390-SC07-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A320-UPDATE-SC07           THRU A320-SC07-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER          TO TRUE
           END-EVALUATE.

       SC07-EX.
           EXIT.

      ******************************************************************
       A305-DECLARE-CURSOR-SC07.

           EXEC SQL
                DECLARE CUR-SC07 CURSOR WITH HOLD FOR
             SELECT
                    A.ID_ADES
                   ,A.ID_POLI
                   ,A.ID_MOVI_CRZ
                   ,A.ID_MOVI_CHIU
                   ,A.DT_INI_EFF
                   ,A.DT_END_EFF
                   ,A.IB_PREV
                   ,A.IB_OGG
                   ,A.COD_COMP_ANIA
                   ,A.DT_DECOR
                   ,A.DT_SCAD
                   ,A.ETA_A_SCAD
                   ,A.DUR_AA
                   ,A.DUR_MM
                   ,A.DUR_GG
                   ,A.TP_RGM_FISC
                   ,A.TP_RIAT
                   ,A.TP_MOD_PAG_TIT
                   ,A.TP_IAS
                   ,A.DT_VARZ_TP_IAS
                   ,A.PRE_NET_IND
                   ,A.PRE_LRD_IND
                   ,A.RAT_LRD_IND
                   ,A.PRSTZ_INI_IND
                   ,A.FL_COINC_ASSTO
                   ,A.IB_DFLT
                   ,A.MOD_CALC
                   ,A.TP_FNT_CNBTVA
                   ,A.IMP_AZ
                   ,A.IMP_ADER
                   ,A.IMP_TFR
                   ,A.IMP_VOLO
                   ,A.PC_AZ
                   ,A.PC_ADER
                   ,A.PC_TFR
                   ,A.PC_VOLO
                   ,A.DT_NOVA_RGM_FISC
                   ,A.FL_ATTIV
                   ,A.IMP_REC_RIT_VIS
                   ,A.IMP_REC_RIT_ACC
                   ,A.FL_VARZ_STAT_TBGC
                   ,A.FL_PROVZA_MIGRAZ
                   ,A.IMPB_VIS_DA_REC
                   ,A.DT_DECOR_PREST_BAN
                   ,A.DT_EFF_VARZ_STAT_T
                   ,A.DS_RIGA
                   ,A.DS_OPER_SQL
                   ,A.DS_VER
                   ,A.DS_TS_INI_CPTZ
                   ,A.DS_TS_END_CPTZ
                   ,A.DS_UTENTE
                   ,A.DS_STATO_ELAB
                   ,A.CUM_CNBT_CAP
                   ,A.IMP_GAR_CNBT
                   ,A.DT_ULT_CONS_CNBT
                   ,A.IDEN_ISC_FND
                   ,A.NUM_RAT_PIAN
                   ,A.DT_PRESC
                   ,A.CONCS_PREST
                   ,B.ID_POLI
                   ,B.ID_MOVI_CRZ
                   ,B.ID_MOVI_CHIU
                   ,B.IB_OGG
                   ,B.IB_PROP
                   ,B.DT_PROP
                   ,B.DT_INI_EFF
                   ,B.DT_END_EFF
                   ,B.COD_COMP_ANIA
                   ,B.DT_DECOR
                   ,B.DT_EMIS
                   ,B.TP_POLI
                   ,B.DUR_AA
                   ,B.DUR_MM
                   ,B.DT_SCAD
                   ,B.COD_PROD
                   ,B.DT_INI_VLDT_PROD
                   ,B.COD_CONV
                   ,B.COD_RAMO
                   ,B.DT_INI_VLDT_CONV
                   ,B.DT_APPLZ_CONV
                   ,B.TP_FRM_ASSVA
                   ,B.TP_RGM_FISC
                   ,B.FL_ESTAS
                   ,B.FL_RSH_COMUN
                   ,B.FL_RSH_COMUN_COND
                   ,B.TP_LIV_GENZ_TIT
                   ,B.FL_COP_FINANZ
                   ,B.TP_APPLZ_DIR
                   ,B.SPE_MED
                   ,B.DIR_EMIS
                   ,B.DIR_1O_VERS
                   ,B.DIR_VERS_AGG
                   ,B.COD_DVS
                   ,B.FL_FNT_AZ
                   ,B.FL_FNT_ADER
                   ,B.FL_FNT_TFR
                   ,B.FL_FNT_VOLO
                   ,B.TP_OPZ_A_SCAD
                   ,B.AA_DIFF_PROR_DFLT
                   ,B.FL_VER_PROD
                   ,B.DUR_GG
                   ,B.DIR_QUIET
                   ,B.TP_PTF_ESTNO
                   ,B.FL_CUM_PRE_CNTR
                   ,B.FL_AMMB_MOVI
                   ,B.CONV_GECO
                   ,B.DS_RIGA
                   ,B.DS_OPER_SQL
                   ,B.DS_VER
                   ,B.DS_TS_INI_CPTZ
                   ,B.DS_TS_END_CPTZ
                   ,B.DS_UTENTE
                   ,B.DS_STATO_ELAB
                   ,B.FL_SCUDO_FISC
                   ,B.FL_TRASFE
                   ,B.FL_TFR_STRC
                   ,B.DT_PRESC
                   ,B.COD_CONV_AGG
                   ,B.SUBCAT_PROD
                   ,B.FL_QUEST_ADEGZ_ASS
                   ,B.COD_TPA
                   ,B.ID_ACC_COMM
                   ,B.FL_POLI_CPI_PR
                   ,B.FL_POLI_BUNDLING
                   ,B.IND_POLI_PRIN_COLL
                   ,B.FL_VND_BUNDLE
                   ,B.IB_BS
                   ,B.FL_POLI_IFP
                   ,C.ID_STAT_OGG_BUS
                   ,C.ID_OGG
                   ,C.TP_OGG
                   ,C.ID_MOVI_CRZ
                   ,C.ID_MOVI_CHIU
                   ,C.DT_INI_EFF
                   ,C.DT_END_EFF
                   ,C.COD_COMP_ANIA
                   ,C.TP_STAT_BUS
                   ,C.TP_CAUS
                   ,C.DS_RIGA
                   ,C.DS_OPER_SQL
                   ,C.DS_VER
                   ,C.DS_TS_INI_CPTZ
                   ,C.DS_TS_END_CPTZ
                   ,C.DS_UTENTE
                   ,C.DS_STATO_ELAB
             FROM ADES      A,
                  POLI      B,
                  STAT_OGG_BUS C
             WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
                                        :WS-TP-FRM-ASSVA2 )
               AND B.IB_OGG BETWEEN  :WLB-IB-POLI-FIRST AND
                                        :WLB-IB-POLI-LAST
               AND A.ID_POLI      =   B.ID_POLI
               AND A.ID_ADES      =   C.ID_OGG
               AND C.TP_OGG       =  'AD'
      *--  D C.TP_STAT_BUS     =  'VI'
               AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
               AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
               AND A.DT_END_EFF  >   :WS-DT-PTF-X
               AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
               AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
               AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
               AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
               AND B.DT_END_EFF  >   :WS-DT-PTF-X
               AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
               AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
               AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
               AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
               AND C.DT_END_EFF  >   :WS-DT-PTF-X
               AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
               AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
               AND A.ID_ADES IN
                  (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
                                                   STAT_OGG_BUS E
                   WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
                   AND E.TP_OGG = 'TG'
                   AND E.TP_STAT_BUS IN ('VI','ST')
                   AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
                   AND D.COD_COMP_ANIA =
                       :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND D.DT_INI_EFF <=   :WS-DT-PTF-X
                   AND D.DT_END_EFF >    :WS-DT-PTF-X
                   AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                   AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                   AND E.DT_INI_EFF <=   :WS-DT-PTF-X
                   AND E.DT_END_EFF >    :WS-DT-PTF-X
                   AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                   AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                   AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
                        OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
                   )
               AND A.DS_STATO_ELAB IN (
                                       :IABV0002-STATE-01,
                                       :IABV0002-STATE-02,
                                       :IABV0002-STATE-03,
                                       :IABV0002-STATE-04,
                                       :IABV0002-STATE-05,
                                       :IABV0002-STATE-06,
                                       :IABV0002-STATE-07,
                                       :IABV0002-STATE-08,
                                       :IABV0002-STATE-09,
                                       :IABV0002-STATE-10
                                         )
               AND NOT A.DS_VER  = :IABV0009-VERSIONING
               ORDER BY A.ID_POLI, A.ID_ADES
           END-EXEC.

       A305-SC07-EX.
           EXIT.
      ******************************************************************
       A310-SELECT-SC07.

           EXEC SQL
             SELECT
                    A.ID_ADES
                   ,A.ID_POLI
                   ,A.ID_MOVI_CRZ
                   ,A.ID_MOVI_CHIU
                   ,A.DT_INI_EFF
                   ,A.DT_END_EFF
                   ,A.IB_PREV
                   ,A.IB_OGG
                   ,A.COD_COMP_ANIA
                   ,A.DT_DECOR
                   ,A.DT_SCAD
                   ,A.ETA_A_SCAD
                   ,A.DUR_AA
                   ,A.DUR_MM
                   ,A.DUR_GG
                   ,A.TP_RGM_FISC
                   ,A.TP_RIAT
                   ,A.TP_MOD_PAG_TIT
                   ,A.TP_IAS
                   ,A.DT_VARZ_TP_IAS
                   ,A.PRE_NET_IND
                   ,A.PRE_LRD_IND
                   ,A.RAT_LRD_IND
                   ,A.PRSTZ_INI_IND
                   ,A.FL_COINC_ASSTO
                   ,A.IB_DFLT
                   ,A.MOD_CALC
                   ,A.TP_FNT_CNBTVA
                   ,A.IMP_AZ
                   ,A.IMP_ADER
                   ,A.IMP_TFR
                   ,A.IMP_VOLO
                   ,A.PC_AZ
                   ,A.PC_ADER
                   ,A.PC_TFR
                   ,A.PC_VOLO
                   ,A.DT_NOVA_RGM_FISC
                   ,A.FL_ATTIV
                   ,A.IMP_REC_RIT_VIS
                   ,A.IMP_REC_RIT_ACC
                   ,A.FL_VARZ_STAT_TBGC
                   ,A.FL_PROVZA_MIGRAZ
                   ,A.IMPB_VIS_DA_REC
                   ,A.DT_DECOR_PREST_BAN
                   ,A.DT_EFF_VARZ_STAT_T
                   ,A.DS_RIGA
                   ,A.DS_OPER_SQL
                   ,A.DS_VER
                   ,A.DS_TS_INI_CPTZ
                   ,A.DS_TS_END_CPTZ
                   ,A.DS_UTENTE
                   ,A.DS_STATO_ELAB
                   ,A.CUM_CNBT_CAP
                   ,A.IMP_GAR_CNBT
                   ,A.DT_ULT_CONS_CNBT
                   ,A.IDEN_ISC_FND
                   ,A.NUM_RAT_PIAN
                   ,A.DT_PRESC
                   ,A.CONCS_PREST
                   ,B.ID_POLI
                   ,B.ID_MOVI_CRZ
                   ,B.ID_MOVI_CHIU
                   ,B.IB_OGG
                   ,B.IB_PROP
                   ,B.DT_PROP
                   ,B.DT_INI_EFF
                   ,B.DT_END_EFF
                   ,B.COD_COMP_ANIA
                   ,B.DT_DECOR
                   ,B.DT_EMIS
                   ,B.TP_POLI
                   ,B.DUR_AA
                   ,B.DUR_MM
                   ,B.DT_SCAD
                   ,B.COD_PROD
                   ,B.DT_INI_VLDT_PROD
                   ,B.COD_CONV
                   ,B.COD_RAMO
                   ,B.DT_INI_VLDT_CONV
                   ,B.DT_APPLZ_CONV
                   ,B.TP_FRM_ASSVA
                   ,B.TP_RGM_FISC
                   ,B.FL_ESTAS
                   ,B.FL_RSH_COMUN
                   ,B.FL_RSH_COMUN_COND
                   ,B.TP_LIV_GENZ_TIT
                   ,B.FL_COP_FINANZ
                   ,B.TP_APPLZ_DIR
                   ,B.SPE_MED
                   ,B.DIR_EMIS
                   ,B.DIR_1O_VERS
                   ,B.DIR_VERS_AGG
                   ,B.COD_DVS
                   ,B.FL_FNT_AZ
                   ,B.FL_FNT_ADER
                   ,B.FL_FNT_TFR
                   ,B.FL_FNT_VOLO
                   ,B.TP_OPZ_A_SCAD
                   ,B.AA_DIFF_PROR_DFLT
                   ,B.FL_VER_PROD
                   ,B.DUR_GG
                   ,B.DIR_QUIET
                   ,B.TP_PTF_ESTNO
                   ,B.FL_CUM_PRE_CNTR
                   ,B.FL_AMMB_MOVI
                   ,B.CONV_GECO
                   ,B.DS_RIGA
                   ,B.DS_OPER_SQL
                   ,B.DS_VER
                   ,B.DS_TS_INI_CPTZ
                   ,B.DS_TS_END_CPTZ
                   ,B.DS_UTENTE
                   ,B.DS_STATO_ELAB
                   ,B.FL_SCUDO_FISC
                   ,B.FL_TRASFE
                   ,B.FL_TFR_STRC
                   ,B.DT_PRESC
                   ,B.COD_CONV_AGG
                   ,B.SUBCAT_PROD
                   ,B.FL_QUEST_ADEGZ_ASS
                   ,B.COD_TPA
                   ,B.ID_ACC_COMM
                   ,B.FL_POLI_CPI_PR
                   ,B.FL_POLI_BUNDLING
                   ,B.IND_POLI_PRIN_COLL
                   ,B.FL_VND_BUNDLE
                   ,B.IB_BS
                   ,B.FL_POLI_IFP
                   ,C.ID_STAT_OGG_BUS
                   ,C.ID_OGG
                   ,C.TP_OGG
                   ,C.ID_MOVI_CRZ
                   ,C.ID_MOVI_CHIU
                   ,C.DT_INI_EFF
                   ,C.DT_END_EFF
                   ,C.COD_COMP_ANIA
                   ,C.TP_STAT_BUS
                   ,C.TP_CAUS
                   ,C.DS_RIGA
                   ,C.DS_OPER_SQL
                   ,C.DS_VER
                   ,C.DS_TS_INI_CPTZ
                   ,C.DS_TS_END_CPTZ
                   ,C.DS_UTENTE
                   ,C.DS_STATO_ELAB
             INTO
                   :ADE-ID-ADES
                  ,:ADE-ID-POLI
                  ,:ADE-ID-MOVI-CRZ
                  ,:ADE-ID-MOVI-CHIU
                   :IND-ADE-ID-MOVI-CHIU
                  ,:ADE-DT-INI-EFF-DB
                  ,:ADE-DT-END-EFF-DB
                  ,:ADE-IB-PREV
                   :IND-ADE-IB-PREV
                  ,:ADE-IB-OGG
                   :IND-ADE-IB-OGG
                  ,:ADE-COD-COMP-ANIA
                  ,:ADE-DT-DECOR-DB
                   :IND-ADE-DT-DECOR
                  ,:ADE-DT-SCAD-DB
                   :IND-ADE-DT-SCAD
                  ,:ADE-ETA-A-SCAD
                   :IND-ADE-ETA-A-SCAD
                  ,:ADE-DUR-AA
                   :IND-ADE-DUR-AA
                  ,:ADE-DUR-MM
                   :IND-ADE-DUR-MM
                  ,:ADE-DUR-GG
                   :IND-ADE-DUR-GG
                  ,:ADE-TP-RGM-FISC
                  ,:ADE-TP-RIAT
                   :IND-ADE-TP-RIAT
                  ,:ADE-TP-MOD-PAG-TIT
                  ,:ADE-TP-IAS
                   :IND-ADE-TP-IAS
                  ,:ADE-DT-VARZ-TP-IAS-DB
                   :IND-ADE-DT-VARZ-TP-IAS
                  ,:ADE-PRE-NET-IND
                   :IND-ADE-PRE-NET-IND
                  ,:ADE-PRE-LRD-IND
                   :IND-ADE-PRE-LRD-IND
                  ,:ADE-RAT-LRD-IND
                   :IND-ADE-RAT-LRD-IND
                  ,:ADE-PRSTZ-INI-IND
                   :IND-ADE-PRSTZ-INI-IND
                  ,:ADE-FL-COINC-ASSTO
                   :IND-ADE-FL-COINC-ASSTO
                  ,:ADE-IB-DFLT
                   :IND-ADE-IB-DFLT
                  ,:ADE-MOD-CALC
                   :IND-ADE-MOD-CALC
                  ,:ADE-TP-FNT-CNBTVA
                   :IND-ADE-TP-FNT-CNBTVA
                  ,:ADE-IMP-AZ
                   :IND-ADE-IMP-AZ
                  ,:ADE-IMP-ADER
                   :IND-ADE-IMP-ADER
                  ,:ADE-IMP-TFR
                   :IND-ADE-IMP-TFR
                  ,:ADE-IMP-VOLO
                   :IND-ADE-IMP-VOLO
                  ,:ADE-PC-AZ
                   :IND-ADE-PC-AZ
                  ,:ADE-PC-ADER
                   :IND-ADE-PC-ADER
                  ,:ADE-PC-TFR
                   :IND-ADE-PC-TFR
                  ,:ADE-PC-VOLO
                   :IND-ADE-PC-VOLO
                  ,:ADE-DT-NOVA-RGM-FISC-DB
                   :IND-ADE-DT-NOVA-RGM-FISC
                  ,:ADE-FL-ATTIV
                   :IND-ADE-FL-ATTIV
                  ,:ADE-IMP-REC-RIT-VIS
                   :IND-ADE-IMP-REC-RIT-VIS
                  ,:ADE-IMP-REC-RIT-ACC
                   :IND-ADE-IMP-REC-RIT-ACC
                  ,:ADE-FL-VARZ-STAT-TBGC
                   :IND-ADE-FL-VARZ-STAT-TBGC
                  ,:ADE-FL-PROVZA-MIGRAZ
                   :IND-ADE-FL-PROVZA-MIGRAZ
                  ,:ADE-IMPB-VIS-DA-REC
                   :IND-ADE-IMPB-VIS-DA-REC
                  ,:ADE-DT-DECOR-PREST-BAN-DB
                   :IND-ADE-DT-DECOR-PREST-BAN
                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
                   :IND-ADE-DT-EFF-VARZ-STAT-T
                  ,:ADE-DS-RIGA
                  ,:ADE-DS-OPER-SQL
                  ,:ADE-DS-VER
                  ,:ADE-DS-TS-INI-CPTZ
                  ,:ADE-DS-TS-END-CPTZ
                  ,:ADE-DS-UTENTE
                  ,:ADE-DS-STATO-ELAB
                  ,:ADE-CUM-CNBT-CAP
                   :IND-ADE-CUM-CNBT-CAP
                  ,:ADE-IMP-GAR-CNBT
                   :IND-ADE-IMP-GAR-CNBT
                  ,:ADE-DT-ULT-CONS-CNBT-DB
                   :IND-ADE-DT-ULT-CONS-CNBT
                  ,:ADE-IDEN-ISC-FND
                   :IND-ADE-IDEN-ISC-FND
                  ,:ADE-NUM-RAT-PIAN
                   :IND-ADE-NUM-RAT-PIAN
                  ,:ADE-DT-PRESC-DB
                   :IND-ADE-DT-PRESC
                  ,:ADE-CONCS-PREST
                   :IND-ADE-CONCS-PREST
                  ,:POL-ID-POLI
                  ,:POL-ID-MOVI-CRZ
                  ,:POL-ID-MOVI-CHIU
                   :IND-POL-ID-MOVI-CHIU
                  ,:POL-IB-OGG
                   :IND-POL-IB-OGG
                  ,:POL-IB-PROP
                  ,:POL-DT-PROP-DB
                   :IND-POL-DT-PROP
                  ,:POL-DT-INI-EFF-DB
                  ,:POL-DT-END-EFF-DB
                  ,:POL-COD-COMP-ANIA
                  ,:POL-DT-DECOR-DB
                  ,:POL-DT-EMIS-DB
                  ,:POL-TP-POLI
                  ,:POL-DUR-AA
                   :IND-POL-DUR-AA
                  ,:POL-DUR-MM
                   :IND-POL-DUR-MM
                  ,:POL-DT-SCAD-DB
                   :IND-POL-DT-SCAD
                  ,:POL-COD-PROD
                  ,:POL-DT-INI-VLDT-PROD-DB
                  ,:POL-COD-CONV
                   :IND-POL-COD-CONV
                  ,:POL-COD-RAMO
                   :IND-POL-COD-RAMO
                  ,:POL-DT-INI-VLDT-CONV-DB
                   :IND-POL-DT-INI-VLDT-CONV
                  ,:POL-DT-APPLZ-CONV-DB
                   :IND-POL-DT-APPLZ-CONV
                  ,:POL-TP-FRM-ASSVA
                  ,:POL-TP-RGM-FISC
                   :IND-POL-TP-RGM-FISC
                  ,:POL-FL-ESTAS
                   :IND-POL-FL-ESTAS
                  ,:POL-FL-RSH-COMUN
                   :IND-POL-FL-RSH-COMUN
                  ,:POL-FL-RSH-COMUN-COND
                   :IND-POL-FL-RSH-COMUN-COND
                  ,:POL-TP-LIV-GENZ-TIT
                  ,:POL-FL-COP-FINANZ
                   :IND-POL-FL-COP-FINANZ
                  ,:POL-TP-APPLZ-DIR
                   :IND-POL-TP-APPLZ-DIR
                  ,:POL-SPE-MED
                   :IND-POL-SPE-MED
                  ,:POL-DIR-EMIS
                   :IND-POL-DIR-EMIS
                  ,:POL-DIR-1O-VERS
                   :IND-POL-DIR-1O-VERS
                  ,:POL-DIR-VERS-AGG
                   :IND-POL-DIR-VERS-AGG
                  ,:POL-COD-DVS
                   :IND-POL-COD-DVS
                  ,:POL-FL-FNT-AZ
                   :IND-POL-FL-FNT-AZ
                  ,:POL-FL-FNT-ADER
                   :IND-POL-FL-FNT-ADER
                  ,:POL-FL-FNT-TFR
                   :IND-POL-FL-FNT-TFR
                  ,:POL-FL-FNT-VOLO
                   :IND-POL-FL-FNT-VOLO
                  ,:POL-TP-OPZ-A-SCAD
                   :IND-POL-TP-OPZ-A-SCAD
                  ,:POL-AA-DIFF-PROR-DFLT
                   :IND-POL-AA-DIFF-PROR-DFLT
                  ,:POL-FL-VER-PROD
                   :IND-POL-FL-VER-PROD
                  ,:POL-DUR-GG
                   :IND-POL-DUR-GG
                  ,:POL-DIR-QUIET
                   :IND-POL-DIR-QUIET
                  ,:POL-TP-PTF-ESTNO
                   :IND-POL-TP-PTF-ESTNO
                  ,:POL-FL-CUM-PRE-CNTR
                   :IND-POL-FL-CUM-PRE-CNTR
                  ,:POL-FL-AMMB-MOVI
                   :IND-POL-FL-AMMB-MOVI
                  ,:POL-CONV-GECO
                   :IND-POL-CONV-GECO
                  ,:POL-DS-RIGA
                  ,:POL-DS-OPER-SQL
                  ,:POL-DS-VER
                  ,:POL-DS-TS-INI-CPTZ
                  ,:POL-DS-TS-END-CPTZ
                  ,:POL-DS-UTENTE
                  ,:POL-DS-STATO-ELAB
                  ,:POL-FL-SCUDO-FISC
                   :IND-POL-FL-SCUDO-FISC
                  ,:POL-FL-TRASFE
                   :IND-POL-FL-TRASFE
                  ,:POL-FL-TFR-STRC
                   :IND-POL-FL-TFR-STRC
                  ,:POL-DT-PRESC-DB
                   :IND-POL-DT-PRESC
                  ,:POL-COD-CONV-AGG
                   :IND-POL-COD-CONV-AGG
                  ,:POL-SUBCAT-PROD
                   :IND-POL-SUBCAT-PROD
                  ,:POL-FL-QUEST-ADEGZ-ASS
                   :IND-POL-FL-QUEST-ADEGZ-ASS
                  ,:POL-COD-TPA
                   :IND-POL-COD-TPA
                  ,:POL-ID-ACC-COMM
                   :IND-POL-ID-ACC-COMM
                  ,:POL-FL-POLI-CPI-PR
                   :IND-POL-FL-POLI-CPI-PR
                  ,:POL-FL-POLI-BUNDLING
                   :IND-POL-FL-POLI-BUNDLING
                  ,:POL-IND-POLI-PRIN-COLL
                   :IND-POL-IND-POLI-PRIN-COLL
                  ,:POL-FL-VND-BUNDLE
                   :IND-POL-FL-VND-BUNDLE
                  ,:POL-IB-BS
                   :IND-POL-IB-BS
                  ,:POL-FL-POLI-IFP
                   :IND-POL-FL-POLI-IFP
                  ,:STB-ID-STAT-OGG-BUS
                  ,:STB-ID-OGG
                  ,:STB-TP-OGG
                  ,:STB-ID-MOVI-CRZ
                  ,:STB-ID-MOVI-CHIU
                   :IND-STB-ID-MOVI-CHIU
                  ,:STB-DT-INI-EFF-DB
                  ,:STB-DT-END-EFF-DB
                  ,:STB-COD-COMP-ANIA
                  ,:STB-TP-STAT-BUS
                  ,:STB-TP-CAUS
                  ,:STB-DS-RIGA
                  ,:STB-DS-OPER-SQL
                  ,:STB-DS-VER
                  ,:STB-DS-TS-INI-CPTZ
                  ,:STB-DS-TS-END-CPTZ
                  ,:STB-DS-UTENTE
                  ,:STB-DS-STATO-ELAB
             FROM ADES      A,
                  POLI      B,
                  STAT_OGG_BUS C

             WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
                                        :WS-TP-FRM-ASSVA2 )
               AND B.IB_OGG BETWEEN  :WLB-IB-POLI-FIRST AND
                                        :WLB-IB-POLI-LAST
               AND A.ID_POLI      =   B.ID_POLI
               AND A.ID_ADES      =   C.ID_OGG
               AND C.TP_OGG       =  'AD'
      *--  D C.TP_STAT_BUS     =  'VI'
               AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
               AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
               AND A.DT_END_EFF  >   :WS-DT-PTF-X
               AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
               AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
               AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
               AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
               AND B.DT_END_EFF  >   :WS-DT-PTF-X
               AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
               AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
               AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
               AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
               AND C.DT_END_EFF  >   :WS-DT-PTF-X
               AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
               AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
               AND A.ID_ADES IN
                  (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
                                                   STAT_OGG_BUS E
                   WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
                   AND E.TP_OGG = 'TG'
                   AND E.TP_STAT_BUS IN ('VI','ST')
                   AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
                   AND D.COD_COMP_ANIA =
                       :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND D.DT_INI_EFF <=   :WS-DT-PTF-X
                   AND D.DT_END_EFF >    :WS-DT-PTF-X
                   AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                   AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                   AND E.DT_INI_EFF <=   :WS-DT-PTF-X
                   AND E.DT_END_EFF >    :WS-DT-PTF-X
                   AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                   AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                   AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
                        OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
                   )

               AND A.DS_STATO_ELAB IN (
                                       :IABV0002-STATE-01,
                                       :IABV0002-STATE-02,
                                       :IABV0002-STATE-03,
                                       :IABV0002-STATE-04,
                                       :IABV0002-STATE-05,
                                       :IABV0002-STATE-06,
                                       :IABV0002-STATE-07,
                                       :IABV0002-STATE-08,
                                       :IABV0002-STATE-09,
                                       :IABV0002-STATE-10
                                         )
               AND NOT A.DS_VER  = :IABV0009-VERSIONING
               ORDER BY A.ID_POLI, A.ID_ADES
              FETCH FIRST ROW ONLY
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-SC07-EX.
           EXIT.

      ******************************************************************
       A320-UPDATE-SC07.

           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
                   PERFORM A330-UPDATE-PK-SC07         THRU A330-SC07-EX

              WHEN IDSV0003-WHERE-CONDITION-07
                   PERFORM A340-UPDATE-WHERE-COND-SC07 THRU A340-SC07-EX
              WHEN IDSV0003-FIRST-ACTION
                   PERFORM A345-UPDATE-FIRST-ACTION-SC07
                                                       THRU A345-SC07-EX
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
           END-EVALUATE.

       A320-SC07-EX.
           EXIT.

      ******************************************************************

       A330-UPDATE-PK-SC07.

           PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX

           EXEC SQL
                UPDATE ADES
                SET
                   DS_OPER_SQL            = :ADE-DS-OPER-SQL
                  ,DS_VER                 = :IABV0009-VERSIONING
                  ,DS_UTENTE              = :ADE-DS-UTENTE
                  ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
                WHERE             DS_RIGA = :ADE-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A330-SC07-EX.
           EXIT.

      ******************************************************************

       A340-UPDATE-WHERE-COND-SC07.

           CONTINUE.

       A340-SC07-EX.
           EXIT.
      ******************************************************************
       A345-UPDATE-FIRST-ACTION-SC07.

           CONTINUE.

       A345-SC07-EX.
           EXIT.
      ******************************************************************

       A360-OPEN-CURSOR-SC07.

           PERFORM A305-DECLARE-CURSOR-SC07 THRU A305-SC07-EX.

           EXEC SQL
               OPEN CUR-SC07
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A360-SC07-EX.
           EXIT.

      ******************************************************************

       A370-CLOSE-CURSOR-SC07.

           EXEC SQL
                CLOSE CUR-SC07
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A370-SC07-EX.
           EXIT.

      ******************************************************************

       A380-FETCH-FIRST-SC07.


           PERFORM A360-OPEN-CURSOR-SC07    THRU A360-SC07-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-SC07  THRU A390-SC07-EX
           END-IF.

       A380-SC07-EX.
           EXIT.

      ******************************************************************

       A390-FETCH-NEXT-SC07.


           EXEC SQL
                FETCH CUR-SC07
           INTO
                   :ADE-ID-ADES
                  ,:ADE-ID-POLI
                  ,:ADE-ID-MOVI-CRZ
                  ,:ADE-ID-MOVI-CHIU
                   :IND-ADE-ID-MOVI-CHIU
                  ,:ADE-DT-INI-EFF-DB
                  ,:ADE-DT-END-EFF-DB
                  ,:ADE-IB-PREV
                   :IND-ADE-IB-PREV
                  ,:ADE-IB-OGG
                   :IND-ADE-IB-OGG
                  ,:ADE-COD-COMP-ANIA
                  ,:ADE-DT-DECOR-DB
                   :IND-ADE-DT-DECOR
                  ,:ADE-DT-SCAD-DB
                   :IND-ADE-DT-SCAD
                  ,:ADE-ETA-A-SCAD
                   :IND-ADE-ETA-A-SCAD
                  ,:ADE-DUR-AA
                   :IND-ADE-DUR-AA
                  ,:ADE-DUR-MM
                   :IND-ADE-DUR-MM
                  ,:ADE-DUR-GG
                   :IND-ADE-DUR-GG
                  ,:ADE-TP-RGM-FISC
                  ,:ADE-TP-RIAT
                   :IND-ADE-TP-RIAT
                  ,:ADE-TP-MOD-PAG-TIT
                  ,:ADE-TP-IAS
                   :IND-ADE-TP-IAS
                  ,:ADE-DT-VARZ-TP-IAS-DB
                   :IND-ADE-DT-VARZ-TP-IAS
                  ,:ADE-PRE-NET-IND
                   :IND-ADE-PRE-NET-IND
                  ,:ADE-PRE-LRD-IND
                   :IND-ADE-PRE-LRD-IND
                  ,:ADE-RAT-LRD-IND
                   :IND-ADE-RAT-LRD-IND
                  ,:ADE-PRSTZ-INI-IND
                   :IND-ADE-PRSTZ-INI-IND
                  ,:ADE-FL-COINC-ASSTO
                   :IND-ADE-FL-COINC-ASSTO
                  ,:ADE-IB-DFLT
                   :IND-ADE-IB-DFLT
                  ,:ADE-MOD-CALC
                   :IND-ADE-MOD-CALC
                  ,:ADE-TP-FNT-CNBTVA
                   :IND-ADE-TP-FNT-CNBTVA
                  ,:ADE-IMP-AZ
                   :IND-ADE-IMP-AZ
                  ,:ADE-IMP-ADER
                   :IND-ADE-IMP-ADER
                  ,:ADE-IMP-TFR
                   :IND-ADE-IMP-TFR
                  ,:ADE-IMP-VOLO
                   :IND-ADE-IMP-VOLO
                  ,:ADE-PC-AZ
                   :IND-ADE-PC-AZ
                  ,:ADE-PC-ADER
                   :IND-ADE-PC-ADER
                  ,:ADE-PC-TFR
                   :IND-ADE-PC-TFR
                  ,:ADE-PC-VOLO
                   :IND-ADE-PC-VOLO
                  ,:ADE-DT-NOVA-RGM-FISC-DB
                   :IND-ADE-DT-NOVA-RGM-FISC
                  ,:ADE-FL-ATTIV
                   :IND-ADE-FL-ATTIV
                  ,:ADE-IMP-REC-RIT-VIS
                   :IND-ADE-IMP-REC-RIT-VIS
                  ,:ADE-IMP-REC-RIT-ACC
                   :IND-ADE-IMP-REC-RIT-ACC
                  ,:ADE-FL-VARZ-STAT-TBGC
                   :IND-ADE-FL-VARZ-STAT-TBGC
                  ,:ADE-FL-PROVZA-MIGRAZ
                   :IND-ADE-FL-PROVZA-MIGRAZ
                  ,:ADE-IMPB-VIS-DA-REC
                   :IND-ADE-IMPB-VIS-DA-REC
                  ,:ADE-DT-DECOR-PREST-BAN-DB
                   :IND-ADE-DT-DECOR-PREST-BAN
                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
                   :IND-ADE-DT-EFF-VARZ-STAT-T
                  ,:ADE-DS-RIGA
                  ,:ADE-DS-OPER-SQL
                  ,:ADE-DS-VER
                  ,:ADE-DS-TS-INI-CPTZ
                  ,:ADE-DS-TS-END-CPTZ
                  ,:ADE-DS-UTENTE
                  ,:ADE-DS-STATO-ELAB
                  ,:ADE-CUM-CNBT-CAP
                   :IND-ADE-CUM-CNBT-CAP
                  ,:ADE-IMP-GAR-CNBT
                   :IND-ADE-IMP-GAR-CNBT
                  ,:ADE-DT-ULT-CONS-CNBT-DB
                   :IND-ADE-DT-ULT-CONS-CNBT
                  ,:ADE-IDEN-ISC-FND
                   :IND-ADE-IDEN-ISC-FND
                  ,:ADE-NUM-RAT-PIAN
                   :IND-ADE-NUM-RAT-PIAN
                  ,:ADE-DT-PRESC-DB
                   :IND-ADE-DT-PRESC
                  ,:ADE-CONCS-PREST
                   :IND-ADE-CONCS-PREST
                  ,:POL-ID-POLI
                  ,:POL-ID-MOVI-CRZ
                  ,:POL-ID-MOVI-CHIU
                   :IND-POL-ID-MOVI-CHIU
                  ,:POL-IB-OGG
                   :IND-POL-IB-OGG
                  ,:POL-IB-PROP
                  ,:POL-DT-PROP-DB
                   :IND-POL-DT-PROP
                  ,:POL-DT-INI-EFF-DB
                  ,:POL-DT-END-EFF-DB
                  ,:POL-COD-COMP-ANIA
                  ,:POL-DT-DECOR-DB
                  ,:POL-DT-EMIS-DB
                  ,:POL-TP-POLI
                  ,:POL-DUR-AA
                   :IND-POL-DUR-AA
                  ,:POL-DUR-MM
                   :IND-POL-DUR-MM
                  ,:POL-DT-SCAD-DB
                   :IND-POL-DT-SCAD
                  ,:POL-COD-PROD
                  ,:POL-DT-INI-VLDT-PROD-DB
                  ,:POL-COD-CONV
                   :IND-POL-COD-CONV
                  ,:POL-COD-RAMO
                   :IND-POL-COD-RAMO
                  ,:POL-DT-INI-VLDT-CONV-DB
                   :IND-POL-DT-INI-VLDT-CONV
                  ,:POL-DT-APPLZ-CONV-DB
                   :IND-POL-DT-APPLZ-CONV
                  ,:POL-TP-FRM-ASSVA
                  ,:POL-TP-RGM-FISC
                   :IND-POL-TP-RGM-FISC
                  ,:POL-FL-ESTAS
                   :IND-POL-FL-ESTAS
                  ,:POL-FL-RSH-COMUN
                   :IND-POL-FL-RSH-COMUN
                  ,:POL-FL-RSH-COMUN-COND
                   :IND-POL-FL-RSH-COMUN-COND
                  ,:POL-TP-LIV-GENZ-TIT
                  ,:POL-FL-COP-FINANZ
                   :IND-POL-FL-COP-FINANZ
                  ,:POL-TP-APPLZ-DIR
                   :IND-POL-TP-APPLZ-DIR
                  ,:POL-SPE-MED
                   :IND-POL-SPE-MED
                  ,:POL-DIR-EMIS
                   :IND-POL-DIR-EMIS
                  ,:POL-DIR-1O-VERS
                   :IND-POL-DIR-1O-VERS
                  ,:POL-DIR-VERS-AGG
                   :IND-POL-DIR-VERS-AGG
                  ,:POL-COD-DVS
                   :IND-POL-COD-DVS
                  ,:POL-FL-FNT-AZ
                   :IND-POL-FL-FNT-AZ
                  ,:POL-FL-FNT-ADER
                   :IND-POL-FL-FNT-ADER
                  ,:POL-FL-FNT-TFR
                   :IND-POL-FL-FNT-TFR
                  ,:POL-FL-FNT-VOLO
                   :IND-POL-FL-FNT-VOLO
                  ,:POL-TP-OPZ-A-SCAD
                   :IND-POL-TP-OPZ-A-SCAD
                  ,:POL-AA-DIFF-PROR-DFLT
                   :IND-POL-AA-DIFF-PROR-DFLT
                  ,:POL-FL-VER-PROD
                   :IND-POL-FL-VER-PROD
                  ,:POL-DUR-GG
                   :IND-POL-DUR-GG
                  ,:POL-DIR-QUIET
                   :IND-POL-DIR-QUIET
                  ,:POL-TP-PTF-ESTNO
                   :IND-POL-TP-PTF-ESTNO
                  ,:POL-FL-CUM-PRE-CNTR
                   :IND-POL-FL-CUM-PRE-CNTR
                  ,:POL-FL-AMMB-MOVI
                   :IND-POL-FL-AMMB-MOVI
                  ,:POL-CONV-GECO
                   :IND-POL-CONV-GECO
                  ,:POL-DS-RIGA
                  ,:POL-DS-OPER-SQL
                  ,:POL-DS-VER
                  ,:POL-DS-TS-INI-CPTZ
                  ,:POL-DS-TS-END-CPTZ
                  ,:POL-DS-UTENTE
                  ,:POL-DS-STATO-ELAB
                  ,:POL-FL-SCUDO-FISC
                   :IND-POL-FL-SCUDO-FISC
                  ,:POL-FL-TRASFE
                   :IND-POL-FL-TRASFE
                  ,:POL-FL-TFR-STRC
                   :IND-POL-FL-TFR-STRC
                  ,:POL-DT-PRESC-DB
                   :IND-POL-DT-PRESC
                  ,:POL-COD-CONV-AGG
                   :IND-POL-COD-CONV-AGG
                  ,:POL-SUBCAT-PROD
                   :IND-POL-SUBCAT-PROD
                  ,:POL-FL-QUEST-ADEGZ-ASS
                   :IND-POL-FL-QUEST-ADEGZ-ASS
                  ,:POL-COD-TPA
                   :IND-POL-COD-TPA
                  ,:POL-ID-ACC-COMM
                   :IND-POL-ID-ACC-COMM
                  ,:POL-FL-POLI-CPI-PR
                   :IND-POL-FL-POLI-CPI-PR
                  ,:POL-FL-POLI-BUNDLING
                   :IND-POL-FL-POLI-BUNDLING
                  ,:POL-IND-POLI-PRIN-COLL
                   :IND-POL-IND-POLI-PRIN-COLL
                  ,:POL-FL-VND-BUNDLE
                   :IND-POL-FL-VND-BUNDLE
                  ,:POL-IB-BS
                   :IND-POL-IB-BS
                  ,:POL-FL-POLI-IFP
                   :IND-POL-FL-POLI-IFP
                  ,:STB-ID-STAT-OGG-BUS
                  ,:STB-ID-OGG
                  ,:STB-TP-OGG
                  ,:STB-ID-MOVI-CRZ
                  ,:STB-ID-MOVI-CHIU
                   :IND-STB-ID-MOVI-CHIU
                  ,:STB-DT-INI-EFF-DB
                  ,:STB-DT-END-EFF-DB
                  ,:STB-COD-COMP-ANIA
                  ,:STB-TP-STAT-BUS
                  ,:STB-TP-CAUS
                  ,:STB-DS-RIGA
                  ,:STB-DS-OPER-SQL
                  ,:STB-DS-VER
                  ,:STB-DS-TS-INI-CPTZ
                  ,:STB-DS-TS-END-CPTZ
                  ,:STB-DS-UTENTE
                  ,:STB-DS-STATO-ELAB
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-SC07 THRU A370-SC07-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       A390-SC07-EX.
           EXIT.


      ******************************************************************
      *
      ******************************************************************
       SC08-SELECTION-CURSOR-08.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-SC08           THRU A310-SC08-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A360-OPEN-CURSOR-SC08      THRU A360-SC08-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR-SC08     THRU A370-SC08-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST-SC08      THRU A380-SC08-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT-SC08       THRU A390-SC08-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A320-UPDATE-SC08           THRU A320-SC08-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER          TO TRUE
           END-EVALUATE.

       SC08-EX.
           EXIT.

      ******************************************************************
       A305-DECLARE-CURSOR-SC08.

           EXEC SQL
                DECLARE CUR-SC08 CURSOR WITH HOLD FOR
             SELECT
                    A.ID_ADES
                   ,A.ID_POLI
                   ,A.ID_MOVI_CRZ
                   ,A.ID_MOVI_CHIU
                   ,A.DT_INI_EFF
                   ,A.DT_END_EFF
                   ,A.IB_PREV
                   ,A.IB_OGG
                   ,A.COD_COMP_ANIA
                   ,A.DT_DECOR
                   ,A.DT_SCAD
                   ,A.ETA_A_SCAD
                   ,A.DUR_AA
                   ,A.DUR_MM
                   ,A.DUR_GG
                   ,A.TP_RGM_FISC
                   ,A.TP_RIAT
                   ,A.TP_MOD_PAG_TIT
                   ,A.TP_IAS
                   ,A.DT_VARZ_TP_IAS
                   ,A.PRE_NET_IND
                   ,A.PRE_LRD_IND
                   ,A.RAT_LRD_IND
                   ,A.PRSTZ_INI_IND
                   ,A.FL_COINC_ASSTO
                   ,A.IB_DFLT
                   ,A.MOD_CALC
                   ,A.TP_FNT_CNBTVA
                   ,A.IMP_AZ
                   ,A.IMP_ADER
                   ,A.IMP_TFR
                   ,A.IMP_VOLO
                   ,A.PC_AZ
                   ,A.PC_ADER
                   ,A.PC_TFR
                   ,A.PC_VOLO
                   ,A.DT_NOVA_RGM_FISC
                   ,A.FL_ATTIV
                   ,A.IMP_REC_RIT_VIS
                   ,A.IMP_REC_RIT_ACC
                   ,A.FL_VARZ_STAT_TBGC
                   ,A.FL_PROVZA_MIGRAZ
                   ,A.IMPB_VIS_DA_REC
                   ,A.DT_DECOR_PREST_BAN
                   ,A.DT_EFF_VARZ_STAT_T
                   ,A.DS_RIGA
                   ,A.DS_OPER_SQL
                   ,A.DS_VER
                   ,A.DS_TS_INI_CPTZ
                   ,A.DS_TS_END_CPTZ
                   ,A.DS_UTENTE
                   ,A.DS_STATO_ELAB
                   ,A.CUM_CNBT_CAP
                   ,A.IMP_GAR_CNBT
                   ,A.DT_ULT_CONS_CNBT
                   ,A.IDEN_ISC_FND
                   ,A.NUM_RAT_PIAN
                   ,A.DT_PRESC
                   ,A.CONCS_PREST
                   ,B.ID_POLI
                   ,B.ID_MOVI_CRZ
                   ,B.ID_MOVI_CHIU
                   ,B.IB_OGG
                   ,B.IB_PROP
                   ,B.DT_PROP
                   ,B.DT_INI_EFF
                   ,B.DT_END_EFF
                   ,B.COD_COMP_ANIA
                   ,B.DT_DECOR
                   ,B.DT_EMIS
                   ,B.TP_POLI
                   ,B.DUR_AA
                   ,B.DUR_MM
                   ,B.DT_SCAD
                   ,B.COD_PROD
                   ,B.DT_INI_VLDT_PROD
                   ,B.COD_CONV
                   ,B.COD_RAMO
                   ,B.DT_INI_VLDT_CONV
                   ,B.DT_APPLZ_CONV
                   ,B.TP_FRM_ASSVA
                   ,B.TP_RGM_FISC
                   ,B.FL_ESTAS
                   ,B.FL_RSH_COMUN
                   ,B.FL_RSH_COMUN_COND
                   ,B.TP_LIV_GENZ_TIT
                   ,B.FL_COP_FINANZ
                   ,B.TP_APPLZ_DIR
                   ,B.SPE_MED
                   ,B.DIR_EMIS
                   ,B.DIR_1O_VERS
                   ,B.DIR_VERS_AGG
                   ,B.COD_DVS
                   ,B.FL_FNT_AZ
                   ,B.FL_FNT_ADER
                   ,B.FL_FNT_TFR
                   ,B.FL_FNT_VOLO
                   ,B.TP_OPZ_A_SCAD
                   ,B.AA_DIFF_PROR_DFLT
                   ,B.FL_VER_PROD
                   ,B.DUR_GG
                   ,B.DIR_QUIET
                   ,B.TP_PTF_ESTNO
                   ,B.FL_CUM_PRE_CNTR
                   ,B.FL_AMMB_MOVI
                   ,B.CONV_GECO
                   ,B.DS_RIGA
                   ,B.DS_OPER_SQL
                   ,B.DS_VER
                   ,B.DS_TS_INI_CPTZ
                   ,B.DS_TS_END_CPTZ
                   ,B.DS_UTENTE
                   ,B.DS_STATO_ELAB
                   ,B.FL_SCUDO_FISC
                   ,B.FL_TRASFE
                   ,B.FL_TFR_STRC
                   ,B.DT_PRESC
                   ,B.COD_CONV_AGG
                   ,B.SUBCAT_PROD
                   ,B.FL_QUEST_ADEGZ_ASS
                   ,B.COD_TPA
                   ,B.ID_ACC_COMM
                   ,B.FL_POLI_CPI_PR
                   ,B.FL_POLI_BUNDLING
                   ,B.IND_POLI_PRIN_COLL
                   ,B.FL_VND_BUNDLE
                   ,B.IB_BS
                   ,B.FL_POLI_IFP
                   ,C.ID_STAT_OGG_BUS
                   ,C.ID_OGG
                   ,C.TP_OGG
                   ,C.ID_MOVI_CRZ
                   ,C.ID_MOVI_CHIU
                   ,C.DT_INI_EFF
                   ,C.DT_END_EFF
                   ,C.COD_COMP_ANIA
                   ,C.TP_STAT_BUS
                   ,C.TP_CAUS
                   ,C.DS_RIGA
                   ,C.DS_OPER_SQL
                   ,C.DS_VER
                   ,C.DS_TS_INI_CPTZ
                   ,C.DS_TS_END_CPTZ
                   ,C.DS_UTENTE
                   ,C.DS_STATO_ELAB
             FROM ADES      A,
                  POLI      B,
                  STAT_OGG_BUS C
             WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
                                        :WS-TP-FRM-ASSVA2 )
               AND B.IB_OGG BETWEEN  :WLB-IB-POLI-FIRST AND
                                        :WLB-IB-POLI-LAST
               AND A.IB_OGG BETWEEN  :WLB-IB-ADE-FIRST AND
                                        :WLB-IB-ADE-LAST
               AND A.ID_POLI      =   B.ID_POLI
               AND A.ID_ADES      =   C.ID_OGG
               AND C.TP_OGG       =  'AD'
      *--  D C.TP_STAT_BUS     =  'VI'
               AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
               AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
               AND A.DT_END_EFF  >   :WS-DT-PTF-X
               AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
               AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
               AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
               AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
               AND B.DT_END_EFF  >   :WS-DT-PTF-X
               AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
               AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
               AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
               AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
               AND C.DT_END_EFF  >   :WS-DT-PTF-X
               AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
               AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
               AND A.ID_ADES IN
                  (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
                                                   STAT_OGG_BUS E
                   WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
                   AND E.TP_OGG = 'TG'
                   AND E.TP_STAT_BUS IN ('VI','ST')
                   AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
                   AND D.COD_COMP_ANIA =
                       :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND D.DT_INI_EFF <=   :WS-DT-PTF-X
                   AND D.DT_END_EFF >    :WS-DT-PTF-X
                   AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                   AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                   AND E.DT_INI_EFF <=   :WS-DT-PTF-X
                   AND E.DT_END_EFF >    :WS-DT-PTF-X
                   AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                   AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                   AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
                        OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
                   )
               AND A.DS_STATO_ELAB IN (
                                       :IABV0002-STATE-01,
                                       :IABV0002-STATE-02,
                                       :IABV0002-STATE-03,
                                       :IABV0002-STATE-04,
                                       :IABV0002-STATE-05,
                                       :IABV0002-STATE-06,
                                       :IABV0002-STATE-07,
                                       :IABV0002-STATE-08,
                                       :IABV0002-STATE-09,
                                       :IABV0002-STATE-10
                                         )
               AND NOT A.DS_VER  = :IABV0009-VERSIONING
               ORDER BY A.ID_POLI, A.ID_ADES
           END-EXEC.

       A305-SC08-EX.
           EXIT.
      ******************************************************************
       A310-SELECT-SC08.

           EXEC SQL
             SELECT
                    A.ID_ADES
                   ,A.ID_POLI
                   ,A.ID_MOVI_CRZ
                   ,A.ID_MOVI_CHIU
                   ,A.DT_INI_EFF
                   ,A.DT_END_EFF
                   ,A.IB_PREV
                   ,A.IB_OGG
                   ,A.COD_COMP_ANIA
                   ,A.DT_DECOR
                   ,A.DT_SCAD
                   ,A.ETA_A_SCAD
                   ,A.DUR_AA
                   ,A.DUR_MM
                   ,A.DUR_GG
                   ,A.TP_RGM_FISC
                   ,A.TP_RIAT
                   ,A.TP_MOD_PAG_TIT
                   ,A.TP_IAS
                   ,A.DT_VARZ_TP_IAS
                   ,A.PRE_NET_IND
                   ,A.PRE_LRD_IND
                   ,A.RAT_LRD_IND
                   ,A.PRSTZ_INI_IND
                   ,A.FL_COINC_ASSTO
                   ,A.IB_DFLT
                   ,A.MOD_CALC
                   ,A.TP_FNT_CNBTVA
                   ,A.IMP_AZ
                   ,A.IMP_ADER
                   ,A.IMP_TFR
                   ,A.IMP_VOLO
                   ,A.PC_AZ
                   ,A.PC_ADER
                   ,A.PC_TFR
                   ,A.PC_VOLO
                   ,A.DT_NOVA_RGM_FISC
                   ,A.FL_ATTIV
                   ,A.IMP_REC_RIT_VIS
                   ,A.IMP_REC_RIT_ACC
                   ,A.FL_VARZ_STAT_TBGC
                   ,A.FL_PROVZA_MIGRAZ
                   ,A.IMPB_VIS_DA_REC
                   ,A.DT_DECOR_PREST_BAN
                   ,A.DT_EFF_VARZ_STAT_T
                   ,A.DS_RIGA
                   ,A.DS_OPER_SQL
                   ,A.DS_VER
                   ,A.DS_TS_INI_CPTZ
                   ,A.DS_TS_END_CPTZ
                   ,A.DS_UTENTE
                   ,A.DS_STATO_ELAB
                   ,A.CUM_CNBT_CAP
                   ,A.IMP_GAR_CNBT
                   ,A.DT_ULT_CONS_CNBT
                   ,A.IDEN_ISC_FND
                   ,A.NUM_RAT_PIAN
                   ,A.DT_PRESC
                   ,A.CONCS_PREST
                   ,B.ID_POLI
                   ,B.ID_MOVI_CRZ
                   ,B.ID_MOVI_CHIU
                   ,B.IB_OGG
                   ,B.IB_PROP
                   ,B.DT_PROP
                   ,B.DT_INI_EFF
                   ,B.DT_END_EFF
                   ,B.COD_COMP_ANIA
                   ,B.DT_DECOR
                   ,B.DT_EMIS
                   ,B.TP_POLI
                   ,B.DUR_AA
                   ,B.DUR_MM
                   ,B.DT_SCAD
                   ,B.COD_PROD
                   ,B.DT_INI_VLDT_PROD
                   ,B.COD_CONV
                   ,B.COD_RAMO
                   ,B.DT_INI_VLDT_CONV
                   ,B.DT_APPLZ_CONV
                   ,B.TP_FRM_ASSVA
                   ,B.TP_RGM_FISC
                   ,B.FL_ESTAS
                   ,B.FL_RSH_COMUN
                   ,B.FL_RSH_COMUN_COND
                   ,B.TP_LIV_GENZ_TIT
                   ,B.FL_COP_FINANZ
                   ,B.TP_APPLZ_DIR
                   ,B.SPE_MED
                   ,B.DIR_EMIS
                   ,B.DIR_1O_VERS
                   ,B.DIR_VERS_AGG
                   ,B.COD_DVS
                   ,B.FL_FNT_AZ
                   ,B.FL_FNT_ADER
                   ,B.FL_FNT_TFR
                   ,B.FL_FNT_VOLO
                   ,B.TP_OPZ_A_SCAD
                   ,B.AA_DIFF_PROR_DFLT
                   ,B.FL_VER_PROD
                   ,B.DUR_GG
                   ,B.DIR_QUIET
                   ,B.TP_PTF_ESTNO
                   ,B.FL_CUM_PRE_CNTR
                   ,B.FL_AMMB_MOVI
                   ,B.CONV_GECO
                   ,B.DS_RIGA
                   ,B.DS_OPER_SQL
                   ,B.DS_VER
                   ,B.DS_TS_INI_CPTZ
                   ,B.DS_TS_END_CPTZ
                   ,B.DS_UTENTE
                   ,B.DS_STATO_ELAB
                   ,B.FL_SCUDO_FISC
                   ,B.FL_TRASFE
                   ,B.FL_TFR_STRC
                   ,B.DT_PRESC
                   ,B.COD_CONV_AGG
                   ,B.SUBCAT_PROD
                   ,B.FL_QUEST_ADEGZ_ASS
                   ,B.COD_TPA
                   ,B.ID_ACC_COMM
                   ,B.FL_POLI_CPI_PR
                   ,B.FL_POLI_BUNDLING
                   ,B.IND_POLI_PRIN_COLL
                   ,B.FL_VND_BUNDLE
                   ,B.IB_BS
                   ,B.FL_POLI_IFP
                   ,C.ID_STAT_OGG_BUS
                   ,C.ID_OGG
                   ,C.TP_OGG
                   ,C.ID_MOVI_CRZ
                   ,C.ID_MOVI_CHIU
                   ,C.DT_INI_EFF
                   ,C.DT_END_EFF
                   ,C.COD_COMP_ANIA
                   ,C.TP_STAT_BUS
                   ,C.TP_CAUS
                   ,C.DS_RIGA
                   ,C.DS_OPER_SQL
                   ,C.DS_VER
                   ,C.DS_TS_INI_CPTZ
                   ,C.DS_TS_END_CPTZ
                   ,C.DS_UTENTE
                   ,C.DS_STATO_ELAB
             INTO
                   :ADE-ID-ADES
                  ,:ADE-ID-POLI
                  ,:ADE-ID-MOVI-CRZ
                  ,:ADE-ID-MOVI-CHIU
                   :IND-ADE-ID-MOVI-CHIU
                  ,:ADE-DT-INI-EFF-DB
                  ,:ADE-DT-END-EFF-DB
                  ,:ADE-IB-PREV
                   :IND-ADE-IB-PREV
                  ,:ADE-IB-OGG
                   :IND-ADE-IB-OGG
                  ,:ADE-COD-COMP-ANIA
                  ,:ADE-DT-DECOR-DB
                   :IND-ADE-DT-DECOR
                  ,:ADE-DT-SCAD-DB
                   :IND-ADE-DT-SCAD
                  ,:ADE-ETA-A-SCAD
                   :IND-ADE-ETA-A-SCAD
                  ,:ADE-DUR-AA
                   :IND-ADE-DUR-AA
                  ,:ADE-DUR-MM
                   :IND-ADE-DUR-MM
                  ,:ADE-DUR-GG
                   :IND-ADE-DUR-GG
                  ,:ADE-TP-RGM-FISC
                  ,:ADE-TP-RIAT
                   :IND-ADE-TP-RIAT
                  ,:ADE-TP-MOD-PAG-TIT
                  ,:ADE-TP-IAS
                   :IND-ADE-TP-IAS
                  ,:ADE-DT-VARZ-TP-IAS-DB
                   :IND-ADE-DT-VARZ-TP-IAS
                  ,:ADE-PRE-NET-IND
                   :IND-ADE-PRE-NET-IND
                  ,:ADE-PRE-LRD-IND
                   :IND-ADE-PRE-LRD-IND
                  ,:ADE-RAT-LRD-IND
                   :IND-ADE-RAT-LRD-IND
                  ,:ADE-PRSTZ-INI-IND
                   :IND-ADE-PRSTZ-INI-IND
                  ,:ADE-FL-COINC-ASSTO
                   :IND-ADE-FL-COINC-ASSTO
                  ,:ADE-IB-DFLT
                   :IND-ADE-IB-DFLT
                  ,:ADE-MOD-CALC
                   :IND-ADE-MOD-CALC
                  ,:ADE-TP-FNT-CNBTVA
                   :IND-ADE-TP-FNT-CNBTVA
                  ,:ADE-IMP-AZ
                   :IND-ADE-IMP-AZ
                  ,:ADE-IMP-ADER
                   :IND-ADE-IMP-ADER
                  ,:ADE-IMP-TFR
                   :IND-ADE-IMP-TFR
                  ,:ADE-IMP-VOLO
                   :IND-ADE-IMP-VOLO
                  ,:ADE-PC-AZ
                   :IND-ADE-PC-AZ
                  ,:ADE-PC-ADER
                   :IND-ADE-PC-ADER
                  ,:ADE-PC-TFR
                   :IND-ADE-PC-TFR
                  ,:ADE-PC-VOLO
                   :IND-ADE-PC-VOLO
                  ,:ADE-DT-NOVA-RGM-FISC-DB
                   :IND-ADE-DT-NOVA-RGM-FISC
                  ,:ADE-FL-ATTIV
                   :IND-ADE-FL-ATTIV
                  ,:ADE-IMP-REC-RIT-VIS
                   :IND-ADE-IMP-REC-RIT-VIS
                  ,:ADE-IMP-REC-RIT-ACC
                   :IND-ADE-IMP-REC-RIT-ACC
                  ,:ADE-FL-VARZ-STAT-TBGC
                   :IND-ADE-FL-VARZ-STAT-TBGC
                  ,:ADE-FL-PROVZA-MIGRAZ
                   :IND-ADE-FL-PROVZA-MIGRAZ
                  ,:ADE-IMPB-VIS-DA-REC
                   :IND-ADE-IMPB-VIS-DA-REC
                  ,:ADE-DT-DECOR-PREST-BAN-DB
                   :IND-ADE-DT-DECOR-PREST-BAN
                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
                   :IND-ADE-DT-EFF-VARZ-STAT-T
                  ,:ADE-DS-RIGA
                  ,:ADE-DS-OPER-SQL
                  ,:ADE-DS-VER
                  ,:ADE-DS-TS-INI-CPTZ
                  ,:ADE-DS-TS-END-CPTZ
                  ,:ADE-DS-UTENTE
                  ,:ADE-DS-STATO-ELAB
                  ,:ADE-CUM-CNBT-CAP
                   :IND-ADE-CUM-CNBT-CAP
                  ,:ADE-IMP-GAR-CNBT
                   :IND-ADE-IMP-GAR-CNBT
                  ,:ADE-DT-ULT-CONS-CNBT-DB
                   :IND-ADE-DT-ULT-CONS-CNBT
                  ,:ADE-IDEN-ISC-FND
                   :IND-ADE-IDEN-ISC-FND
                  ,:ADE-NUM-RAT-PIAN
                   :IND-ADE-NUM-RAT-PIAN
                  ,:ADE-DT-PRESC-DB
                   :IND-ADE-DT-PRESC
                  ,:ADE-CONCS-PREST
                   :IND-ADE-CONCS-PREST
                  ,:POL-ID-POLI
                  ,:POL-ID-MOVI-CRZ
                  ,:POL-ID-MOVI-CHIU
                   :IND-POL-ID-MOVI-CHIU
                  ,:POL-IB-OGG
                   :IND-POL-IB-OGG
                  ,:POL-IB-PROP
                  ,:POL-DT-PROP-DB
                   :IND-POL-DT-PROP
                  ,:POL-DT-INI-EFF-DB
                  ,:POL-DT-END-EFF-DB
                  ,:POL-COD-COMP-ANIA
                  ,:POL-DT-DECOR-DB
                  ,:POL-DT-EMIS-DB
                  ,:POL-TP-POLI
                  ,:POL-DUR-AA
                   :IND-POL-DUR-AA
                  ,:POL-DUR-MM
                   :IND-POL-DUR-MM
                  ,:POL-DT-SCAD-DB
                   :IND-POL-DT-SCAD
                  ,:POL-COD-PROD
                  ,:POL-DT-INI-VLDT-PROD-DB
                  ,:POL-COD-CONV
                   :IND-POL-COD-CONV
                  ,:POL-COD-RAMO
                   :IND-POL-COD-RAMO
                  ,:POL-DT-INI-VLDT-CONV-DB
                   :IND-POL-DT-INI-VLDT-CONV
                  ,:POL-DT-APPLZ-CONV-DB
                   :IND-POL-DT-APPLZ-CONV
                  ,:POL-TP-FRM-ASSVA
                  ,:POL-TP-RGM-FISC
                   :IND-POL-TP-RGM-FISC
                  ,:POL-FL-ESTAS
                   :IND-POL-FL-ESTAS
                  ,:POL-FL-RSH-COMUN
                   :IND-POL-FL-RSH-COMUN
                  ,:POL-FL-RSH-COMUN-COND
                   :IND-POL-FL-RSH-COMUN-COND
                  ,:POL-TP-LIV-GENZ-TIT
                  ,:POL-FL-COP-FINANZ
                   :IND-POL-FL-COP-FINANZ
                  ,:POL-TP-APPLZ-DIR
                   :IND-POL-TP-APPLZ-DIR
                  ,:POL-SPE-MED
                   :IND-POL-SPE-MED
                  ,:POL-DIR-EMIS
                   :IND-POL-DIR-EMIS
                  ,:POL-DIR-1O-VERS
                   :IND-POL-DIR-1O-VERS
                  ,:POL-DIR-VERS-AGG
                   :IND-POL-DIR-VERS-AGG
                  ,:POL-COD-DVS
                   :IND-POL-COD-DVS
                  ,:POL-FL-FNT-AZ
                   :IND-POL-FL-FNT-AZ
                  ,:POL-FL-FNT-ADER
                   :IND-POL-FL-FNT-ADER
                  ,:POL-FL-FNT-TFR
                   :IND-POL-FL-FNT-TFR
                  ,:POL-FL-FNT-VOLO
                   :IND-POL-FL-FNT-VOLO
                  ,:POL-TP-OPZ-A-SCAD
                   :IND-POL-TP-OPZ-A-SCAD
                  ,:POL-AA-DIFF-PROR-DFLT
                   :IND-POL-AA-DIFF-PROR-DFLT
                  ,:POL-FL-VER-PROD
                   :IND-POL-FL-VER-PROD
                  ,:POL-DUR-GG
                   :IND-POL-DUR-GG
                  ,:POL-DIR-QUIET
                   :IND-POL-DIR-QUIET
                  ,:POL-TP-PTF-ESTNO
                   :IND-POL-TP-PTF-ESTNO
                  ,:POL-FL-CUM-PRE-CNTR
                   :IND-POL-FL-CUM-PRE-CNTR
                  ,:POL-FL-AMMB-MOVI
                   :IND-POL-FL-AMMB-MOVI
                  ,:POL-CONV-GECO
                   :IND-POL-CONV-GECO
                  ,:POL-DS-RIGA
                  ,:POL-DS-OPER-SQL
                  ,:POL-DS-VER
                  ,:POL-DS-TS-INI-CPTZ
                  ,:POL-DS-TS-END-CPTZ
                  ,:POL-DS-UTENTE
                  ,:POL-DS-STATO-ELAB
                  ,:POL-FL-SCUDO-FISC
                   :IND-POL-FL-SCUDO-FISC
                  ,:POL-FL-TRASFE
                   :IND-POL-FL-TRASFE
                  ,:POL-FL-TFR-STRC
                   :IND-POL-FL-TFR-STRC
                  ,:POL-DT-PRESC-DB
                   :IND-POL-DT-PRESC
                  ,:POL-COD-CONV-AGG
                   :IND-POL-COD-CONV-AGG
                  ,:POL-SUBCAT-PROD
                   :IND-POL-SUBCAT-PROD
                  ,:POL-FL-QUEST-ADEGZ-ASS
                   :IND-POL-FL-QUEST-ADEGZ-ASS
                  ,:POL-COD-TPA
                   :IND-POL-COD-TPA
                  ,:POL-ID-ACC-COMM
                   :IND-POL-ID-ACC-COMM
                  ,:POL-FL-POLI-CPI-PR
                   :IND-POL-FL-POLI-CPI-PR
                  ,:POL-FL-POLI-BUNDLING
                   :IND-POL-FL-POLI-BUNDLING
                  ,:POL-IND-POLI-PRIN-COLL
                   :IND-POL-IND-POLI-PRIN-COLL
                  ,:POL-FL-VND-BUNDLE
                   :IND-POL-FL-VND-BUNDLE
                  ,:POL-IB-BS
                   :IND-POL-IB-BS
                  ,:POL-FL-POLI-IFP
                   :IND-POL-FL-POLI-IFP
                  ,:STB-ID-STAT-OGG-BUS
                  ,:STB-ID-OGG
                  ,:STB-TP-OGG
                  ,:STB-ID-MOVI-CRZ
                  ,:STB-ID-MOVI-CHIU
                   :IND-STB-ID-MOVI-CHIU
                  ,:STB-DT-INI-EFF-DB
                  ,:STB-DT-END-EFF-DB
                  ,:STB-COD-COMP-ANIA
                  ,:STB-TP-STAT-BUS
                  ,:STB-TP-CAUS
                  ,:STB-DS-RIGA
                  ,:STB-DS-OPER-SQL
                  ,:STB-DS-VER
                  ,:STB-DS-TS-INI-CPTZ
                  ,:STB-DS-TS-END-CPTZ
                  ,:STB-DS-UTENTE
                  ,:STB-DS-STATO-ELAB
             FROM ADES      A,
                  POLI      B,
                  STAT_OGG_BUS C

             WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
                                        :WS-TP-FRM-ASSVA2 )
               AND B.IB_OGG BETWEEN  :WLB-IB-POLI-FIRST AND
                                        :WLB-IB-POLI-LAST
               AND A.IB_OGG BETWEEN  :WLB-IB-ADE-FIRST AND
                                        :WLB-IB-ADE-LAST
               AND A.ID_POLI      =   B.ID_POLI
               AND A.ID_ADES      =   C.ID_OGG
               AND C.TP_OGG       =  'AD'
      *--  D C.TP_STAT_BUS     =  'VI'
               AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
               AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
               AND A.DT_END_EFF  >   :WS-DT-PTF-X
               AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
               AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
               AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
               AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
               AND B.DT_END_EFF  >   :WS-DT-PTF-X
               AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
               AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
               AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
               AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
               AND C.DT_END_EFF  >   :WS-DT-PTF-X
               AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
               AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
               AND A.ID_ADES IN
                  (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
                                                   STAT_OGG_BUS E
                   WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
                   AND E.TP_OGG = 'TG'
                   AND E.TP_STAT_BUS IN ('VI','ST')
                   AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
                   AND D.COD_COMP_ANIA =
                       :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND D.DT_INI_EFF <=   :WS-DT-PTF-X
                   AND D.DT_END_EFF >    :WS-DT-PTF-X
                   AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                   AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                   AND E.DT_INI_EFF <=   :WS-DT-PTF-X
                   AND E.DT_END_EFF >    :WS-DT-PTF-X
                   AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
                   AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
                   AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
                        OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
                   )

               AND A.DS_STATO_ELAB IN (
                                       :IABV0002-STATE-01,
                                       :IABV0002-STATE-02,
                                       :IABV0002-STATE-03,
                                       :IABV0002-STATE-04,
                                       :IABV0002-STATE-05,
                                       :IABV0002-STATE-06,
                                       :IABV0002-STATE-07,
                                       :IABV0002-STATE-08,
                                       :IABV0002-STATE-09,
                                       :IABV0002-STATE-10
                                         )
               AND NOT A.DS_VER  = :IABV0009-VERSIONING
               ORDER BY A.ID_POLI, A.ID_ADES
              FETCH FIRST ROW ONLY
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-SC08-EX.
           EXIT.

      ******************************************************************
       A320-UPDATE-SC08.

           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
                   PERFORM A330-UPDATE-PK-SC08         THRU A330-SC08-EX

              WHEN IDSV0003-WHERE-CONDITION-08
                   PERFORM A340-UPDATE-WHERE-COND-SC08 THRU A340-SC08-EX
              WHEN IDSV0003-FIRST-ACTION
                   PERFORM A345-UPDATE-FIRST-ACTION-SC08
                                                       THRU A345-SC08-EX
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
           END-EVALUATE.

       A320-SC08-EX.
           EXIT.

      ******************************************************************

       A330-UPDATE-PK-SC08.

           PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX

           EXEC SQL
                UPDATE ADES
                SET
                   DS_OPER_SQL            = :ADE-DS-OPER-SQL
                  ,DS_VER                 = :IABV0009-VERSIONING
                  ,DS_UTENTE              = :ADE-DS-UTENTE
                  ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
                WHERE             DS_RIGA = :ADE-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A330-SC08-EX.
           EXIT.

      ******************************************************************

       A340-UPDATE-WHERE-COND-SC08.

           CONTINUE.

       A340-SC08-EX.
           EXIT.
      ******************************************************************
       A345-UPDATE-FIRST-ACTION-SC08.

           CONTINUE.

       A345-SC08-EX.
           EXIT.
      ******************************************************************

       A360-OPEN-CURSOR-SC08.

           PERFORM A305-DECLARE-CURSOR-SC08 THRU A305-SC08-EX.

           EXEC SQL
                OPEN CUR-SC08
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A360-SC08-EX.
           EXIT.

      ******************************************************************

       A370-CLOSE-CURSOR-SC08.

           EXEC SQL
               CLOSE CUR-SC08
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A370-SC08-EX.
           EXIT.

      ******************************************************************

       A380-FETCH-FIRST-SC08.


           PERFORM A360-OPEN-CURSOR-SC08    THRU A360-SC08-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-SC08  THRU A390-SC08-EX
           END-IF.

       A380-SC08-EX.
           EXIT.

      ******************************************************************

       A390-FETCH-NEXT-SC08.


           EXEC SQL
                FETCH CUR-SC08
           INTO
                   :ADE-ID-ADES
                  ,:ADE-ID-POLI
                  ,:ADE-ID-MOVI-CRZ
                  ,:ADE-ID-MOVI-CHIU
                   :IND-ADE-ID-MOVI-CHIU
                  ,:ADE-DT-INI-EFF-DB
                  ,:ADE-DT-END-EFF-DB
                  ,:ADE-IB-PREV
                   :IND-ADE-IB-PREV
                  ,:ADE-IB-OGG
                   :IND-ADE-IB-OGG
                  ,:ADE-COD-COMP-ANIA
                  ,:ADE-DT-DECOR-DB
                   :IND-ADE-DT-DECOR
                  ,:ADE-DT-SCAD-DB
                   :IND-ADE-DT-SCAD
                  ,:ADE-ETA-A-SCAD
                   :IND-ADE-ETA-A-SCAD
                  ,:ADE-DUR-AA
                   :IND-ADE-DUR-AA
                  ,:ADE-DUR-MM
                   :IND-ADE-DUR-MM
                  ,:ADE-DUR-GG
                   :IND-ADE-DUR-GG
                  ,:ADE-TP-RGM-FISC
                  ,:ADE-TP-RIAT
                   :IND-ADE-TP-RIAT
                  ,:ADE-TP-MOD-PAG-TIT
                  ,:ADE-TP-IAS
                   :IND-ADE-TP-IAS
                  ,:ADE-DT-VARZ-TP-IAS-DB
                   :IND-ADE-DT-VARZ-TP-IAS
                  ,:ADE-PRE-NET-IND
                   :IND-ADE-PRE-NET-IND
                  ,:ADE-PRE-LRD-IND
                   :IND-ADE-PRE-LRD-IND
                  ,:ADE-RAT-LRD-IND
                   :IND-ADE-RAT-LRD-IND
                  ,:ADE-PRSTZ-INI-IND
                   :IND-ADE-PRSTZ-INI-IND
                  ,:ADE-FL-COINC-ASSTO
                   :IND-ADE-FL-COINC-ASSTO
                  ,:ADE-IB-DFLT
                   :IND-ADE-IB-DFLT
                  ,:ADE-MOD-CALC
                   :IND-ADE-MOD-CALC
                  ,:ADE-TP-FNT-CNBTVA
                   :IND-ADE-TP-FNT-CNBTVA
                  ,:ADE-IMP-AZ
                   :IND-ADE-IMP-AZ
                  ,:ADE-IMP-ADER
                   :IND-ADE-IMP-ADER
                  ,:ADE-IMP-TFR
                   :IND-ADE-IMP-TFR
                  ,:ADE-IMP-VOLO
                   :IND-ADE-IMP-VOLO
                  ,:ADE-PC-AZ
                   :IND-ADE-PC-AZ
                  ,:ADE-PC-ADER
                   :IND-ADE-PC-ADER
                  ,:ADE-PC-TFR
                   :IND-ADE-PC-TFR
                  ,:ADE-PC-VOLO
                   :IND-ADE-PC-VOLO
                  ,:ADE-DT-NOVA-RGM-FISC-DB
                   :IND-ADE-DT-NOVA-RGM-FISC
                  ,:ADE-FL-ATTIV
                   :IND-ADE-FL-ATTIV
                  ,:ADE-IMP-REC-RIT-VIS
                   :IND-ADE-IMP-REC-RIT-VIS
                  ,:ADE-IMP-REC-RIT-ACC
                   :IND-ADE-IMP-REC-RIT-ACC
                  ,:ADE-FL-VARZ-STAT-TBGC
                   :IND-ADE-FL-VARZ-STAT-TBGC
                  ,:ADE-FL-PROVZA-MIGRAZ
                   :IND-ADE-FL-PROVZA-MIGRAZ
                  ,:ADE-IMPB-VIS-DA-REC
                   :IND-ADE-IMPB-VIS-DA-REC
                  ,:ADE-DT-DECOR-PREST-BAN-DB
                   :IND-ADE-DT-DECOR-PREST-BAN
                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
                   :IND-ADE-DT-EFF-VARZ-STAT-T
                  ,:ADE-DS-RIGA
                  ,:ADE-DS-OPER-SQL
                  ,:ADE-DS-VER
                  ,:ADE-DS-TS-INI-CPTZ
                  ,:ADE-DS-TS-END-CPTZ
                  ,:ADE-DS-UTENTE
                  ,:ADE-DS-STATO-ELAB
                  ,:ADE-CUM-CNBT-CAP
                   :IND-ADE-CUM-CNBT-CAP
                  ,:ADE-IMP-GAR-CNBT
                   :IND-ADE-IMP-GAR-CNBT
                  ,:ADE-DT-ULT-CONS-CNBT-DB
                   :IND-ADE-DT-ULT-CONS-CNBT
                  ,:ADE-IDEN-ISC-FND
                   :IND-ADE-IDEN-ISC-FND
                  ,:ADE-NUM-RAT-PIAN
                   :IND-ADE-NUM-RAT-PIAN
                  ,:ADE-DT-PRESC-DB
                   :IND-ADE-DT-PRESC
                  ,:ADE-CONCS-PREST
                   :IND-ADE-CONCS-PREST
                  ,:POL-ID-POLI
                  ,:POL-ID-MOVI-CRZ
                  ,:POL-ID-MOVI-CHIU
                   :IND-POL-ID-MOVI-CHIU
                  ,:POL-IB-OGG
                   :IND-POL-IB-OGG
                  ,:POL-IB-PROP
                  ,:POL-DT-PROP-DB
                   :IND-POL-DT-PROP
                  ,:POL-DT-INI-EFF-DB
                  ,:POL-DT-END-EFF-DB
                  ,:POL-COD-COMP-ANIA
                  ,:POL-DT-DECOR-DB
                  ,:POL-DT-EMIS-DB
                  ,:POL-TP-POLI
                  ,:POL-DUR-AA
                   :IND-POL-DUR-AA
                  ,:POL-DUR-MM
                   :IND-POL-DUR-MM
                  ,:POL-DT-SCAD-DB
                   :IND-POL-DT-SCAD
                  ,:POL-COD-PROD
                  ,:POL-DT-INI-VLDT-PROD-DB
                  ,:POL-COD-CONV
                   :IND-POL-COD-CONV
                  ,:POL-COD-RAMO
                   :IND-POL-COD-RAMO
                  ,:POL-DT-INI-VLDT-CONV-DB
                   :IND-POL-DT-INI-VLDT-CONV
                  ,:POL-DT-APPLZ-CONV-DB
                   :IND-POL-DT-APPLZ-CONV
                  ,:POL-TP-FRM-ASSVA
                  ,:POL-TP-RGM-FISC
                   :IND-POL-TP-RGM-FISC
                  ,:POL-FL-ESTAS
                   :IND-POL-FL-ESTAS
                  ,:POL-FL-RSH-COMUN
                   :IND-POL-FL-RSH-COMUN
                  ,:POL-FL-RSH-COMUN-COND
                   :IND-POL-FL-RSH-COMUN-COND
                  ,:POL-TP-LIV-GENZ-TIT
                  ,:POL-FL-COP-FINANZ
                   :IND-POL-FL-COP-FINANZ
                  ,:POL-TP-APPLZ-DIR
                   :IND-POL-TP-APPLZ-DIR
                  ,:POL-SPE-MED
                   :IND-POL-SPE-MED
                  ,:POL-DIR-EMIS
                   :IND-POL-DIR-EMIS
                  ,:POL-DIR-1O-VERS
                   :IND-POL-DIR-1O-VERS
                  ,:POL-DIR-VERS-AGG
                   :IND-POL-DIR-VERS-AGG
                  ,:POL-COD-DVS
                   :IND-POL-COD-DVS
                  ,:POL-FL-FNT-AZ
                   :IND-POL-FL-FNT-AZ
                  ,:POL-FL-FNT-ADER
                   :IND-POL-FL-FNT-ADER
                  ,:POL-FL-FNT-TFR
                   :IND-POL-FL-FNT-TFR
                  ,:POL-FL-FNT-VOLO
                   :IND-POL-FL-FNT-VOLO
                  ,:POL-TP-OPZ-A-SCAD
                   :IND-POL-TP-OPZ-A-SCAD
                  ,:POL-AA-DIFF-PROR-DFLT
                   :IND-POL-AA-DIFF-PROR-DFLT
                  ,:POL-FL-VER-PROD
                   :IND-POL-FL-VER-PROD
                  ,:POL-DUR-GG
                   :IND-POL-DUR-GG
                  ,:POL-DIR-QUIET
                   :IND-POL-DIR-QUIET
                  ,:POL-TP-PTF-ESTNO
                   :IND-POL-TP-PTF-ESTNO
                  ,:POL-FL-CUM-PRE-CNTR
                   :IND-POL-FL-CUM-PRE-CNTR
                  ,:POL-FL-AMMB-MOVI
                   :IND-POL-FL-AMMB-MOVI
                  ,:POL-CONV-GECO
                   :IND-POL-CONV-GECO
                  ,:POL-DS-RIGA
                  ,:POL-DS-OPER-SQL
                  ,:POL-DS-VER
                  ,:POL-DS-TS-INI-CPTZ
                  ,:POL-DS-TS-END-CPTZ
                  ,:POL-DS-UTENTE
                  ,:POL-DS-STATO-ELAB
                  ,:POL-FL-SCUDO-FISC
                   :IND-POL-FL-SCUDO-FISC
                  ,:POL-FL-TRASFE
                   :IND-POL-FL-TRASFE
                  ,:POL-FL-TFR-STRC
                   :IND-POL-FL-TFR-STRC
                  ,:POL-DT-PRESC-DB
                   :IND-POL-DT-PRESC
                  ,:POL-COD-CONV-AGG
                   :IND-POL-COD-CONV-AGG
                  ,:POL-SUBCAT-PROD
                   :IND-POL-SUBCAT-PROD
                  ,:POL-FL-QUEST-ADEGZ-ASS
                   :IND-POL-FL-QUEST-ADEGZ-ASS
                  ,:POL-COD-TPA
                   :IND-POL-COD-TPA
                  ,:POL-ID-ACC-COMM
                   :IND-POL-ID-ACC-COMM
                  ,:POL-FL-POLI-CPI-PR
                   :IND-POL-FL-POLI-CPI-PR
                  ,:POL-FL-POLI-BUNDLING
                   :IND-POL-FL-POLI-BUNDLING
                  ,:POL-IND-POLI-PRIN-COLL
                   :IND-POL-IND-POLI-PRIN-COLL
                  ,:POL-FL-VND-BUNDLE
                   :IND-POL-FL-VND-BUNDLE
                  ,:POL-IB-BS
                   :IND-POL-IB-BS
                  ,:POL-FL-POLI-IFP
                   :IND-POL-FL-POLI-IFP
                  ,:STB-ID-STAT-OGG-BUS
                  ,:STB-ID-OGG
                  ,:STB-TP-OGG
                  ,:STB-ID-MOVI-CRZ
                  ,:STB-ID-MOVI-CHIU
                   :IND-STB-ID-MOVI-CHIU
                  ,:STB-DT-INI-EFF-DB
                  ,:STB-DT-END-EFF-DB
                  ,:STB-COD-COMP-ANIA
                  ,:STB-TP-STAT-BUS
                  ,:STB-TP-CAUS
                  ,:STB-DS-RIGA
                  ,:STB-DS-OPER-SQL
                  ,:STB-DS-VER
                  ,:STB-DS-TS-INI-CPTZ
                  ,:STB-DS-TS-END-CPTZ
                  ,:STB-DS-UTENTE
                  ,:STB-DS-STATO-ELAB
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-SC08 THRU A370-SC08-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       A390-SC08-EX.
           EXIT.


      ******************************************************************
       Z100-SET-COLONNE-NULL.

           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-ADE-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO ADE-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-ADE-IB-PREV = -1
              MOVE HIGH-VALUES TO ADE-IB-PREV-NULL
           END-IF
           IF IND-ADE-IB-OGG = -1
              MOVE HIGH-VALUES TO ADE-IB-OGG-NULL
           END-IF
           IF IND-ADE-DT-DECOR = -1
              MOVE HIGH-VALUES TO ADE-DT-DECOR-NULL
           END-IF
           IF IND-ADE-DT-SCAD = -1
              MOVE HIGH-VALUES TO ADE-DT-SCAD-NULL
           END-IF
           IF IND-ADE-ETA-A-SCAD = -1
              MOVE HIGH-VALUES TO ADE-ETA-A-SCAD-NULL
           END-IF
           IF IND-ADE-DUR-AA = -1
              MOVE HIGH-VALUES TO ADE-DUR-AA-NULL
           END-IF
           IF IND-ADE-DUR-MM = -1
              MOVE HIGH-VALUES TO ADE-DUR-MM-NULL
           END-IF
           IF IND-ADE-DUR-GG = -1
              MOVE HIGH-VALUES TO ADE-DUR-GG-NULL
           END-IF
           IF IND-ADE-TP-RIAT = -1
              MOVE HIGH-VALUES TO ADE-TP-RIAT-NULL
           END-IF
           IF IND-ADE-TP-IAS = -1
              MOVE HIGH-VALUES TO ADE-TP-IAS-NULL
           END-IF
           IF IND-ADE-DT-VARZ-TP-IAS = -1
              MOVE HIGH-VALUES TO ADE-DT-VARZ-TP-IAS-NULL
           END-IF
           IF IND-ADE-PRE-NET-IND = -1
              MOVE HIGH-VALUES TO ADE-PRE-NET-IND-NULL
           END-IF
           IF IND-ADE-PRE-LRD-IND = -1
              MOVE HIGH-VALUES TO ADE-PRE-LRD-IND-NULL
           END-IF
           IF IND-ADE-RAT-LRD-IND = -1
              MOVE HIGH-VALUES TO ADE-RAT-LRD-IND-NULL
           END-IF
           IF IND-ADE-PRSTZ-INI-IND = -1
              MOVE HIGH-VALUES TO ADE-PRSTZ-INI-IND-NULL
           END-IF
           IF IND-ADE-FL-COINC-ASSTO = -1
              MOVE HIGH-VALUES TO ADE-FL-COINC-ASSTO-NULL
           END-IF
           IF IND-ADE-IB-DFLT = -1
              MOVE HIGH-VALUES TO ADE-IB-DFLT-NULL
           END-IF
           IF IND-ADE-MOD-CALC = -1
              MOVE HIGH-VALUES TO ADE-MOD-CALC-NULL
           END-IF
           IF IND-ADE-TP-FNT-CNBTVA = -1
              MOVE HIGH-VALUES TO ADE-TP-FNT-CNBTVA-NULL
           END-IF
           IF IND-ADE-IMP-AZ = -1
              MOVE HIGH-VALUES TO ADE-IMP-AZ-NULL
           END-IF
           IF IND-ADE-IMP-ADER = -1
              MOVE HIGH-VALUES TO ADE-IMP-ADER-NULL
           END-IF
           IF IND-ADE-IMP-TFR = -1
              MOVE HIGH-VALUES TO ADE-IMP-TFR-NULL
           END-IF
           IF IND-ADE-IMP-VOLO = -1
              MOVE HIGH-VALUES TO ADE-IMP-VOLO-NULL
           END-IF
           IF IND-ADE-PC-AZ = -1
              MOVE HIGH-VALUES TO ADE-PC-AZ-NULL
           END-IF
           IF IND-ADE-PC-ADER = -1
              MOVE HIGH-VALUES TO ADE-PC-ADER-NULL
           END-IF
           IF IND-ADE-PC-TFR = -1
              MOVE HIGH-VALUES TO ADE-PC-TFR-NULL
           END-IF
           IF IND-ADE-PC-VOLO = -1
              MOVE HIGH-VALUES TO ADE-PC-VOLO-NULL
           END-IF
           IF IND-ADE-DT-NOVA-RGM-FISC = -1
              MOVE HIGH-VALUES TO ADE-DT-NOVA-RGM-FISC-NULL
           END-IF
           IF IND-ADE-FL-ATTIV = -1
              MOVE HIGH-VALUES TO ADE-FL-ATTIV-NULL
           END-IF
           IF IND-ADE-IMP-REC-RIT-VIS = -1
              MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-VIS-NULL
           END-IF
           IF IND-ADE-IMP-REC-RIT-ACC = -1
              MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-ACC-NULL
           END-IF
           IF IND-ADE-FL-VARZ-STAT-TBGC = -1
              MOVE HIGH-VALUES TO ADE-FL-VARZ-STAT-TBGC-NULL
           END-IF
           IF IND-ADE-FL-PROVZA-MIGRAZ = -1
              MOVE HIGH-VALUES TO ADE-FL-PROVZA-MIGRAZ-NULL
           END-IF
           IF IND-ADE-IMPB-VIS-DA-REC = -1
              MOVE HIGH-VALUES TO ADE-IMPB-VIS-DA-REC-NULL
           END-IF
           IF IND-ADE-DT-DECOR-PREST-BAN = -1
              MOVE HIGH-VALUES TO ADE-DT-DECOR-PREST-BAN-NULL
           END-IF
           IF IND-ADE-DT-EFF-VARZ-STAT-T = -1
              MOVE HIGH-VALUES TO ADE-DT-EFF-VARZ-STAT-T-NULL
           END-IF
           IF IND-ADE-CUM-CNBT-CAP = -1
              MOVE HIGH-VALUES TO ADE-CUM-CNBT-CAP-NULL
           END-IF
           IF IND-ADE-IMP-GAR-CNBT = -1
              MOVE HIGH-VALUES TO ADE-IMP-GAR-CNBT-NULL
           END-IF
           IF IND-ADE-DT-ULT-CONS-CNBT = -1
              MOVE HIGH-VALUES TO ADE-DT-ULT-CONS-CNBT-NULL
           END-IF
           IF IND-ADE-IDEN-ISC-FND = -1
              MOVE HIGH-VALUES TO ADE-IDEN-ISC-FND-NULL
           END-IF
           IF IND-ADE-NUM-RAT-PIAN = -1
              MOVE HIGH-VALUES TO ADE-NUM-RAT-PIAN-NULL
           END-IF
           IF IND-ADE-DT-PRESC = -1
              MOVE HIGH-VALUES TO ADE-DT-PRESC-NULL
           END-IF.
           IF IND-ADE-CONCS-PREST = -1
              MOVE HIGH-VALUES TO ADE-CONCS-PREST-NULL
           END-IF.




           IF IND-POL-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO POL-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-POL-IB-OGG = -1
              MOVE HIGH-VALUES TO POL-IB-OGG-NULL
           END-IF
           IF IND-POL-DT-PROP = -1
              MOVE HIGH-VALUES TO POL-DT-PROP-NULL
           END-IF
           IF IND-POL-DUR-AA = -1
              MOVE HIGH-VALUES TO POL-DUR-AA-NULL
           END-IF
           IF IND-POL-DUR-MM = -1
              MOVE HIGH-VALUES TO POL-DUR-MM-NULL
           END-IF
           IF IND-POL-DT-SCAD = -1
              MOVE HIGH-VALUES TO POL-DT-SCAD-NULL
           END-IF
           IF IND-POL-COD-CONV = -1
              MOVE HIGH-VALUES TO POL-COD-CONV-NULL
           END-IF
           IF IND-POL-COD-RAMO = -1
              MOVE HIGH-VALUES TO POL-COD-RAMO-NULL
           END-IF
           IF IND-POL-DT-INI-VLDT-CONV = -1
              MOVE HIGH-VALUES TO POL-DT-INI-VLDT-CONV-NULL
           END-IF
           IF IND-POL-DT-APPLZ-CONV = -1
              MOVE HIGH-VALUES TO POL-DT-APPLZ-CONV-NULL
           END-IF
           IF IND-POL-TP-RGM-FISC = -1
              MOVE HIGH-VALUES TO POL-TP-RGM-FISC-NULL
           END-IF
           IF IND-POL-FL-ESTAS = -1
              MOVE HIGH-VALUES TO POL-FL-ESTAS-NULL
           END-IF
           IF IND-POL-FL-RSH-COMUN = -1
              MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-NULL
           END-IF
           IF IND-POL-FL-RSH-COMUN-COND = -1
              MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-COND-NULL
           END-IF
           IF IND-POL-FL-COP-FINANZ = -1
              MOVE HIGH-VALUES TO POL-FL-COP-FINANZ-NULL
           END-IF
           IF IND-POL-TP-APPLZ-DIR = -1
              MOVE HIGH-VALUES TO POL-TP-APPLZ-DIR-NULL
           END-IF
           IF IND-POL-SPE-MED = -1
              MOVE HIGH-VALUES TO POL-SPE-MED-NULL
           END-IF
           IF IND-POL-DIR-EMIS = -1
              MOVE HIGH-VALUES TO POL-DIR-EMIS-NULL
           END-IF
           IF IND-POL-DIR-1O-VERS = -1
              MOVE HIGH-VALUES TO POL-DIR-1O-VERS-NULL
           END-IF
           IF IND-POL-DIR-VERS-AGG = -1
              MOVE HIGH-VALUES TO POL-DIR-VERS-AGG-NULL
           END-IF
           IF IND-POL-COD-DVS = -1
              MOVE HIGH-VALUES TO POL-COD-DVS-NULL
           END-IF
           IF IND-POL-FL-FNT-AZ = -1
              MOVE HIGH-VALUES TO POL-FL-FNT-AZ-NULL
           END-IF
           IF IND-POL-FL-FNT-ADER = -1
              MOVE HIGH-VALUES TO POL-FL-FNT-ADER-NULL
           END-IF
           IF IND-POL-FL-FNT-TFR = -1
              MOVE HIGH-VALUES TO POL-FL-FNT-TFR-NULL
           END-IF
           IF IND-POL-FL-FNT-VOLO = -1
              MOVE HIGH-VALUES TO POL-FL-FNT-VOLO-NULL
           END-IF
           IF IND-POL-TP-OPZ-A-SCAD = -1
              MOVE HIGH-VALUES TO POL-TP-OPZ-A-SCAD-NULL
           END-IF
           IF IND-POL-AA-DIFF-PROR-DFLT = -1
              MOVE HIGH-VALUES TO POL-AA-DIFF-PROR-DFLT-NULL
           END-IF
           IF IND-POL-FL-VER-PROD = -1
              MOVE HIGH-VALUES TO POL-FL-VER-PROD-NULL
           END-IF
           IF IND-POL-DUR-GG = -1
              MOVE HIGH-VALUES TO POL-DUR-GG-NULL
           END-IF
           IF IND-POL-DIR-QUIET = -1
              MOVE HIGH-VALUES TO POL-DIR-QUIET-NULL
           END-IF
           IF IND-POL-TP-PTF-ESTNO = -1
              MOVE HIGH-VALUES TO POL-TP-PTF-ESTNO-NULL
           END-IF
           IF IND-POL-FL-CUM-PRE-CNTR = -1
              MOVE HIGH-VALUES TO POL-FL-CUM-PRE-CNTR-NULL
           END-IF
           IF IND-POL-FL-AMMB-MOVI = -1
              MOVE HIGH-VALUES TO POL-FL-AMMB-MOVI-NULL
           END-IF
           IF IND-POL-CONV-GECO = -1
              MOVE HIGH-VALUES TO POL-CONV-GECO-NULL
           END-IF
           IF IND-POL-FL-SCUDO-FISC = -1
              MOVE HIGH-VALUES TO POL-FL-SCUDO-FISC-NULL
           END-IF
           IF IND-POL-FL-TRASFE = -1
              MOVE HIGH-VALUES TO POL-FL-TRASFE-NULL
           END-IF
           IF IND-POL-FL-TFR-STRC = -1
              MOVE HIGH-VALUES TO POL-FL-TFR-STRC-NULL
           END-IF
           IF IND-POL-DT-PRESC = -1
              MOVE HIGH-VALUES TO POL-DT-PRESC-NULL
           END-IF
           IF IND-POL-COD-CONV-AGG = -1
              MOVE HIGH-VALUES TO POL-COD-CONV-AGG-NULL
           END-IF
           IF IND-POL-SUBCAT-PROD = -1
              MOVE HIGH-VALUES TO POL-SUBCAT-PROD-NULL
           END-IF.
           IF IND-POL-FL-QUEST-ADEGZ-ASS = -1
              MOVE HIGH-VALUES TO POL-FL-QUEST-ADEGZ-ASS-NULL
           END-IF
           IF IND-POL-COD-TPA = -1
              MOVE HIGH-VALUES TO POL-COD-TPA-NULL
           END-IF
           IF IND-POL-ID-ACC-COMM = -1
              MOVE HIGH-VALUES TO POL-ID-ACC-COMM-NULL
           END-IF
           IF IND-POL-FL-POLI-CPI-PR = -1
              MOVE HIGH-VALUES TO POL-FL-POLI-CPI-PR-NULL
           END-IF
           IF IND-POL-FL-POLI-BUNDLING = -1
              MOVE HIGH-VALUES TO POL-FL-POLI-BUNDLING-NULL
           END-IF
           IF IND-POL-IND-POLI-PRIN-COLL = -1
              MOVE HIGH-VALUES TO POL-IND-POLI-PRIN-COLL-NULL
           END-IF
           IF IND-POL-FL-VND-BUNDLE = -1
              MOVE HIGH-VALUES TO POL-FL-VND-BUNDLE-NULL
           END-IF.
           IF IND-POL-IB-BS = -1
              MOVE HIGH-VALUES TO POL-IB-BS-NULL
           END-IF.
           IF IND-POL-FL-POLI-IFP = -1
              MOVE HIGH-VALUES TO POL-FL-POLI-IFP-NULL
           END-IF.

           IF IND-STB-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO STB-ID-MOVI-CHIU-NULL
           END-IF.

       Z100-EX.
           EXIT.

      ******************************************************************

       Z150-VALORIZZA-DATA-SERVICES.

           MOVE IDSV0003-OPERAZIONE     TO POL-DS-OPER-SQL.

           MOVE IDSV0003-USER-NAME      TO POL-DS-UTENTE.

       Z150-EX.
           EXIT.

      ******************************************************************

       Z200-SET-INDICATORI-NULL.


           IF ADE-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-ADE-ID-MOVI-CHIU
           END-IF
           IF ADE-IB-PREV-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IB-PREV
           ELSE
              MOVE 0 TO IND-ADE-IB-PREV
           END-IF
           IF ADE-IB-OGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IB-OGG
           ELSE
              MOVE 0 TO IND-ADE-IB-OGG
           END-IF
           IF ADE-DT-DECOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DT-DECOR
           ELSE
              MOVE 0 TO IND-ADE-DT-DECOR
           END-IF
           IF ADE-DT-SCAD-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DT-SCAD
           ELSE
              MOVE 0 TO IND-ADE-DT-SCAD
           END-IF
           IF ADE-ETA-A-SCAD-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-ETA-A-SCAD
           ELSE
              MOVE 0 TO IND-ADE-ETA-A-SCAD
           END-IF
           IF ADE-DUR-AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DUR-AA
           ELSE
              MOVE 0 TO IND-ADE-DUR-AA
           END-IF
           IF ADE-DUR-MM-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DUR-MM
           ELSE
              MOVE 0 TO IND-ADE-DUR-MM
           END-IF
           IF ADE-DUR-GG-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DUR-GG
           ELSE
              MOVE 0 TO IND-ADE-DUR-GG
           END-IF
           IF ADE-TP-RIAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-TP-RIAT
           ELSE
              MOVE 0 TO IND-ADE-TP-RIAT
           END-IF
           IF ADE-TP-IAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-TP-IAS
           ELSE
              MOVE 0 TO IND-ADE-TP-IAS
           END-IF
           IF ADE-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DT-VARZ-TP-IAS
           ELSE
              MOVE 0 TO IND-ADE-DT-VARZ-TP-IAS
           END-IF
           IF ADE-PRE-NET-IND-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-PRE-NET-IND
           ELSE
              MOVE 0 TO IND-ADE-PRE-NET-IND
           END-IF
           IF ADE-PRE-LRD-IND-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-PRE-LRD-IND
           ELSE
              MOVE 0 TO IND-ADE-PRE-LRD-IND
           END-IF
           IF ADE-RAT-LRD-IND-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-RAT-LRD-IND
           ELSE
              MOVE 0 TO IND-ADE-RAT-LRD-IND
           END-IF
           IF ADE-PRSTZ-INI-IND-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-PRSTZ-INI-IND
           ELSE
              MOVE 0 TO IND-ADE-PRSTZ-INI-IND
           END-IF
           IF ADE-FL-COINC-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-FL-COINC-ASSTO
           ELSE
              MOVE 0 TO IND-ADE-FL-COINC-ASSTO
           END-IF
           IF ADE-IB-DFLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IB-DFLT
           ELSE
              MOVE 0 TO IND-ADE-IB-DFLT
           END-IF
           IF ADE-MOD-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-MOD-CALC
           ELSE
              MOVE 0 TO IND-ADE-MOD-CALC
           END-IF
           IF ADE-TP-FNT-CNBTVA-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-TP-FNT-CNBTVA
           ELSE
              MOVE 0 TO IND-ADE-TP-FNT-CNBTVA
           END-IF
           IF ADE-IMP-AZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IMP-AZ
           ELSE
              MOVE 0 TO IND-ADE-IMP-AZ
           END-IF
           IF ADE-IMP-ADER-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IMP-ADER
           ELSE
              MOVE 0 TO IND-ADE-IMP-ADER
           END-IF
           IF ADE-IMP-TFR-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IMP-TFR
           ELSE
              MOVE 0 TO IND-ADE-IMP-TFR
           END-IF
           IF ADE-IMP-VOLO-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IMP-VOLO
           ELSE
              MOVE 0 TO IND-ADE-IMP-VOLO
           END-IF
           IF ADE-PC-AZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-PC-AZ
           ELSE
              MOVE 0 TO IND-ADE-PC-AZ
           END-IF
           IF ADE-PC-ADER-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-PC-ADER
           ELSE
              MOVE 0 TO IND-ADE-PC-ADER
           END-IF
           IF ADE-PC-TFR-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-PC-TFR
           ELSE
              MOVE 0 TO IND-ADE-PC-TFR
           END-IF
           IF ADE-PC-VOLO-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-PC-VOLO
           ELSE
              MOVE 0 TO IND-ADE-PC-VOLO
           END-IF
           IF ADE-DT-NOVA-RGM-FISC-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DT-NOVA-RGM-FISC
           ELSE
              MOVE 0 TO IND-ADE-DT-NOVA-RGM-FISC
           END-IF
           IF ADE-FL-ATTIV-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-FL-ATTIV
           ELSE
              MOVE 0 TO IND-ADE-FL-ATTIV
           END-IF
           IF ADE-IMP-REC-RIT-VIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IMP-REC-RIT-VIS
           ELSE
              MOVE 0 TO IND-ADE-IMP-REC-RIT-VIS
           END-IF
           IF ADE-IMP-REC-RIT-ACC-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IMP-REC-RIT-ACC
           ELSE
              MOVE 0 TO IND-ADE-IMP-REC-RIT-ACC
           END-IF
           IF ADE-FL-VARZ-STAT-TBGC-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-FL-VARZ-STAT-TBGC
           ELSE
              MOVE 0 TO IND-ADE-FL-VARZ-STAT-TBGC
           END-IF
           IF ADE-FL-PROVZA-MIGRAZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-FL-PROVZA-MIGRAZ
           ELSE
              MOVE 0 TO IND-ADE-FL-PROVZA-MIGRAZ
           END-IF
           IF ADE-IMPB-VIS-DA-REC-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IMPB-VIS-DA-REC
           ELSE
              MOVE 0 TO IND-ADE-IMPB-VIS-DA-REC
           END-IF
           IF ADE-DT-DECOR-PREST-BAN-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DT-DECOR-PREST-BAN
           ELSE
              MOVE 0 TO IND-ADE-DT-DECOR-PREST-BAN
           END-IF
           IF ADE-DT-EFF-VARZ-STAT-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DT-EFF-VARZ-STAT-T
           ELSE
              MOVE 0 TO IND-ADE-DT-EFF-VARZ-STAT-T
           END-IF
           IF ADE-CUM-CNBT-CAP-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-CUM-CNBT-CAP
           ELSE
              MOVE 0 TO IND-ADE-CUM-CNBT-CAP
           END-IF
           IF ADE-IMP-GAR-CNBT-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IMP-GAR-CNBT
           ELSE
              MOVE 0 TO IND-ADE-IMP-GAR-CNBT
           END-IF
           IF ADE-DT-ULT-CONS-CNBT-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DT-ULT-CONS-CNBT
           ELSE
              MOVE 0 TO IND-ADE-DT-ULT-CONS-CNBT
           END-IF
           IF ADE-IDEN-ISC-FND-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IDEN-ISC-FND
           ELSE
              MOVE 0 TO IND-ADE-IDEN-ISC-FND
           END-IF
           IF ADE-NUM-RAT-PIAN-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-NUM-RAT-PIAN
           ELSE
              MOVE 0 TO IND-ADE-NUM-RAT-PIAN
           END-IF
           IF ADE-DT-PRESC-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DT-PRESC
           ELSE
              MOVE 0 TO IND-ADE-DT-PRESC
           END-IF.
           IF ADE-CONCS-PREST-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-CONCS-PREST
           ELSE
              MOVE 0 TO IND-ADE-CONCS-PREST
           END-IF.



           IF POL-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-POL-ID-MOVI-CHIU
           END-IF
           IF POL-IB-OGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-IB-OGG
           ELSE
              MOVE 0 TO IND-POL-IB-OGG
           END-IF
           IF POL-DT-PROP-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DT-PROP
           ELSE
              MOVE 0 TO IND-POL-DT-PROP
           END-IF
           IF POL-DUR-AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DUR-AA
           ELSE
              MOVE 0 TO IND-POL-DUR-AA
           END-IF
           IF POL-DUR-MM-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DUR-MM
           ELSE
              MOVE 0 TO IND-POL-DUR-MM
           END-IF
           IF POL-DT-SCAD-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DT-SCAD
           ELSE
              MOVE 0 TO IND-POL-DT-SCAD
           END-IF
           IF POL-COD-CONV-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-COD-CONV
           ELSE
              MOVE 0 TO IND-POL-COD-CONV
           END-IF
           IF POL-COD-RAMO-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-COD-RAMO
           ELSE
              MOVE 0 TO IND-POL-COD-RAMO
           END-IF
           IF POL-DT-INI-VLDT-CONV-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DT-INI-VLDT-CONV
           ELSE
              MOVE 0 TO IND-POL-DT-INI-VLDT-CONV
           END-IF
           IF POL-DT-APPLZ-CONV-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DT-APPLZ-CONV
           ELSE
              MOVE 0 TO IND-POL-DT-APPLZ-CONV
           END-IF
           IF POL-TP-RGM-FISC-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-TP-RGM-FISC
           ELSE
              MOVE 0 TO IND-POL-TP-RGM-FISC
           END-IF
           IF POL-FL-ESTAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-ESTAS
           ELSE
              MOVE 0 TO IND-POL-FL-ESTAS
           END-IF
           IF POL-FL-RSH-COMUN-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-RSH-COMUN
           ELSE
              MOVE 0 TO IND-POL-FL-RSH-COMUN
           END-IF
           IF POL-FL-RSH-COMUN-COND-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-RSH-COMUN-COND
           ELSE
              MOVE 0 TO IND-POL-FL-RSH-COMUN-COND
           END-IF
           IF POL-FL-COP-FINANZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-COP-FINANZ
           ELSE
              MOVE 0 TO IND-POL-FL-COP-FINANZ
           END-IF
           IF POL-TP-APPLZ-DIR-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-TP-APPLZ-DIR
           ELSE
              MOVE 0 TO IND-POL-TP-APPLZ-DIR
           END-IF
           IF POL-SPE-MED-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-SPE-MED
           ELSE
              MOVE 0 TO IND-POL-SPE-MED
           END-IF
           IF POL-DIR-EMIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DIR-EMIS
           ELSE
              MOVE 0 TO IND-POL-DIR-EMIS
           END-IF
           IF POL-DIR-1O-VERS-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DIR-1O-VERS
           ELSE
              MOVE 0 TO IND-POL-DIR-1O-VERS
           END-IF
           IF POL-DIR-VERS-AGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DIR-VERS-AGG
           ELSE
              MOVE 0 TO IND-POL-DIR-VERS-AGG
           END-IF
           IF POL-COD-DVS-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-COD-DVS
           ELSE
              MOVE 0 TO IND-POL-COD-DVS
           END-IF
           IF POL-FL-FNT-AZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-FNT-AZ
           ELSE
              MOVE 0 TO IND-POL-FL-FNT-AZ
           END-IF
           IF POL-FL-FNT-ADER-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-FNT-ADER
           ELSE
              MOVE 0 TO IND-POL-FL-FNT-ADER
           END-IF
           IF POL-FL-FNT-TFR-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-FNT-TFR
           ELSE
              MOVE 0 TO IND-POL-FL-FNT-TFR
           END-IF
           IF POL-FL-FNT-VOLO-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-FNT-VOLO
           ELSE
              MOVE 0 TO IND-POL-FL-FNT-VOLO
           END-IF
           IF POL-TP-OPZ-A-SCAD-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-TP-OPZ-A-SCAD
           ELSE
              MOVE 0 TO IND-POL-TP-OPZ-A-SCAD
           END-IF
           IF POL-AA-DIFF-PROR-DFLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-AA-DIFF-PROR-DFLT
           ELSE
              MOVE 0 TO IND-POL-AA-DIFF-PROR-DFLT
           END-IF
           IF POL-FL-VER-PROD-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-VER-PROD
           ELSE
              MOVE 0 TO IND-POL-FL-VER-PROD
           END-IF
           IF POL-DUR-GG-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DUR-GG
           ELSE
              MOVE 0 TO IND-POL-DUR-GG
           END-IF
           IF POL-DIR-QUIET-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DIR-QUIET
           ELSE
              MOVE 0 TO IND-POL-DIR-QUIET
           END-IF
           IF POL-TP-PTF-ESTNO-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-TP-PTF-ESTNO
           ELSE
              MOVE 0 TO IND-POL-TP-PTF-ESTNO
           END-IF
           IF POL-FL-CUM-PRE-CNTR-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-CUM-PRE-CNTR
           ELSE
              MOVE 0 TO IND-POL-FL-CUM-PRE-CNTR
           END-IF
           IF POL-FL-AMMB-MOVI-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-AMMB-MOVI
           ELSE
              MOVE 0 TO IND-POL-FL-AMMB-MOVI
           END-IF
           IF POL-CONV-GECO-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-CONV-GECO
           ELSE
              MOVE 0 TO IND-POL-CONV-GECO
           END-IF
           IF POL-FL-SCUDO-FISC-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-SCUDO-FISC
           ELSE
              MOVE 0 TO IND-POL-FL-SCUDO-FISC
           END-IF
           IF POL-FL-TRASFE-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-TRASFE
           ELSE
              MOVE 0 TO IND-POL-FL-TRASFE
           END-IF
           IF POL-FL-TFR-STRC-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-TFR-STRC
           ELSE
              MOVE 0 TO IND-POL-FL-TFR-STRC
           END-IF
           IF POL-DT-PRESC-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DT-PRESC
           ELSE
              MOVE 0 TO IND-POL-DT-PRESC
           END-IF
           IF POL-COD-CONV-AGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-COD-CONV-AGG
           ELSE
              MOVE 0 TO IND-POL-COD-CONV-AGG
           END-IF
           IF POL-SUBCAT-PROD-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-SUBCAT-PROD
           ELSE
              MOVE 0 TO IND-POL-SUBCAT-PROD
           END-IF.
           IF POL-FL-QUEST-ADEGZ-ASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-QUEST-ADEGZ-ASS
           ELSE
              MOVE 0 TO IND-POL-FL-QUEST-ADEGZ-ASS
           END-IF
           IF POL-COD-TPA-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-COD-TPA
           ELSE
              MOVE 0 TO IND-POL-COD-TPA
           END-IF
           IF POL-ID-ACC-COMM-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-ID-ACC-COMM
           ELSE
              MOVE 0 TO IND-POL-ID-ACC-COMM
           END-IF
           IF POL-FL-POLI-CPI-PR-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-POLI-CPI-PR
           ELSE
              MOVE 0 TO IND-POL-FL-POLI-CPI-PR
           END-IF
           IF POL-FL-POLI-BUNDLING-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-POLI-BUNDLING
           ELSE
              MOVE 0 TO IND-POL-FL-POLI-BUNDLING
           END-IF
           IF POL-IND-POLI-PRIN-COLL-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-IND-POLI-PRIN-COLL
           ELSE
              MOVE 0 TO IND-POL-IND-POLI-PRIN-COLL
           END-IF
           IF POL-FL-VND-BUNDLE-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-VND-BUNDLE
           ELSE
              MOVE 0 TO IND-POL-FL-VND-BUNDLE
           END-IF.
           IF POL-IB-BS-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-IB-BS
           ELSE
              MOVE 0 TO IND-POL-IB-BS
           END-IF.
           IF POL-FL-POLI-IFP-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-POLI-IFP
           ELSE
              MOVE 0 TO IND-POL-FL-POLI-IFP
           END-IF.

           IF STB-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-STB-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-STB-ID-MOVI-CHIU
           END-IF.

       Z200-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.

           MOVE ADE-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO ADE-DT-INI-EFF-DB
           MOVE ADE-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO ADE-DT-END-EFF-DB
           IF IND-ADE-DT-DECOR = 0
               MOVE ADE-DT-DECOR TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-DECOR-DB
           END-IF
           IF IND-ADE-DT-SCAD = 0
               MOVE ADE-DT-SCAD TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-SCAD-DB
           END-IF
           IF IND-ADE-DT-VARZ-TP-IAS = 0
               MOVE ADE-DT-VARZ-TP-IAS TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-VARZ-TP-IAS-DB
           END-IF
           IF IND-ADE-DT-NOVA-RGM-FISC = 0
               MOVE ADE-DT-NOVA-RGM-FISC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-NOVA-RGM-FISC-DB
           END-IF
           IF IND-ADE-DT-DECOR-PREST-BAN = 0
               MOVE ADE-DT-DECOR-PREST-BAN TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-DECOR-PREST-BAN-DB
           END-IF
           IF IND-ADE-DT-EFF-VARZ-STAT-T = 0
               MOVE ADE-DT-EFF-VARZ-STAT-T TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-EFF-VARZ-STAT-T-DB
           END-IF
           IF IND-ADE-DT-ULT-CONS-CNBT = 0
               MOVE ADE-DT-ULT-CONS-CNBT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-ULT-CONS-CNBT-DB
           END-IF
           IF IND-ADE-DT-PRESC = 0
               MOVE ADE-DT-PRESC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-PRESC-DB
           END-IF.



           IF IND-POL-DT-PROP = 0
               MOVE POL-DT-PROP TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-PROP-DB
           END-IF
           MOVE POL-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-INI-EFF-DB
           MOVE POL-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-END-EFF-DB
           MOVE POL-DT-DECOR TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-DECOR-DB
           MOVE POL-DT-EMIS TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-EMIS-DB
           IF IND-POL-DT-SCAD = 0
               MOVE POL-DT-SCAD TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-SCAD-DB
           END-IF
           MOVE POL-DT-INI-VLDT-PROD TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-INI-VLDT-PROD-DB
           IF IND-POL-DT-INI-VLDT-CONV = 0
               MOVE POL-DT-INI-VLDT-CONV TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-INI-VLDT-CONV-DB
           END-IF
           IF IND-POL-DT-APPLZ-CONV = 0
               MOVE POL-DT-APPLZ-CONV TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-APPLZ-CONV-DB
           END-IF
           IF IND-POL-DT-PRESC = 0
               MOVE POL-DT-PRESC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-PRESC-DB
           END-IF.



           MOVE STB-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO STB-DT-INI-EFF-DB
           MOVE STB-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO STB-DT-END-EFF-DB.


       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE ADE-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO ADE-DT-INI-EFF
           MOVE ADE-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO ADE-DT-END-EFF
           IF IND-ADE-DT-DECOR = 0
               MOVE ADE-DT-DECOR-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-DECOR
           END-IF
           IF IND-ADE-DT-SCAD = 0
               MOVE ADE-DT-SCAD-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-SCAD
           END-IF
           IF IND-ADE-DT-VARZ-TP-IAS = 0
               MOVE ADE-DT-VARZ-TP-IAS-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-VARZ-TP-IAS
           END-IF
           IF IND-ADE-DT-NOVA-RGM-FISC = 0
               MOVE ADE-DT-NOVA-RGM-FISC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-NOVA-RGM-FISC
           END-IF
           IF IND-ADE-DT-DECOR-PREST-BAN = 0
               MOVE ADE-DT-DECOR-PREST-BAN-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-DECOR-PREST-BAN
           END-IF
           IF IND-ADE-DT-EFF-VARZ-STAT-T = 0
               MOVE ADE-DT-EFF-VARZ-STAT-T-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-EFF-VARZ-STAT-T
           END-IF
           IF IND-ADE-DT-ULT-CONS-CNBT = 0
               MOVE ADE-DT-ULT-CONS-CNBT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-ULT-CONS-CNBT
           END-IF
           IF IND-ADE-DT-PRESC = 0
               MOVE ADE-DT-PRESC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-PRESC
           END-IF.


           IF IND-POL-DT-PROP = 0
               MOVE POL-DT-PROP-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-PROP
           END-IF
           MOVE POL-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-INI-EFF
           MOVE POL-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-END-EFF
           MOVE POL-DT-DECOR-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-DECOR
           MOVE POL-DT-EMIS-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-EMIS
           IF IND-POL-DT-SCAD = 0
               MOVE POL-DT-SCAD-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-SCAD
           END-IF
           MOVE POL-DT-INI-VLDT-PROD-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-INI-VLDT-PROD
           IF IND-POL-DT-INI-VLDT-CONV = 0
               MOVE POL-DT-INI-VLDT-CONV-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-INI-VLDT-CONV
           END-IF
           IF IND-POL-DT-APPLZ-CONV = 0
               MOVE POL-DT-APPLZ-CONV-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-APPLZ-CONV
           END-IF
           IF IND-POL-DT-PRESC = 0
               MOVE POL-DT-PRESC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-PRESC
           END-IF.


           MOVE STB-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO STB-DT-INI-EFF
           MOVE STB-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO STB-DT-END-EFF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.

       Z960-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

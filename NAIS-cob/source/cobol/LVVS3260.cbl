      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS3260.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2017.
       DATE-COMPILED.
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS3260'.
       01  PGM-IDBSE060                     PIC X(008) VALUE 'IDBSE060'.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01 WK-VARIABILI.
          03 WK-CAP-PROT                       PIC S9(11)V9(7).
      *----------------------------------------------------------------*
      *    COPY GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
      *---------------------------------------------------------------*
      * AREA HOST VARIABLES
      *---------------------------------------------------------------*
      *--> DCLGEN ESTENSIONE POLIZZA
           COPY IDBVE061.
      *----------------------------------------------------------------*
      *--> AREA ADESIONE
      *----------------------------------------------------------------*
       01 AREA-IO-POLI.
          03 DPOL-AREA-POLI.
             04 DPOL-ELE-ADES-MAX       PIC S9(04) COMP.
             04 DPOL-TAB-ADES.
             COPY LCCVPOL1              REPLACING ==(SF)== BY ==DPOL==.
      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.
      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    COPY DISPATHCER
      *----------------------------------------------------------------*
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
      *--> COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      *--> COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.
      *----------------------------------------------------------------*
      *      CAMPI DI ESITO, AREE DI SERVIZIO                          *
      *----------------------------------------------------------------*
       01 AREA-IDSV0001.
           COPY IDSV0001.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP VALUE 0.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS3260.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS3260.
      *----------------------------------------------------------------*
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.
      *
           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000
      *
           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.
      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT
                                             WK-VARIABILI.

           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.
      *
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
      * --     LETTURA MOVI PER VERIFCA PRESENZA SWITCH DI LINEA
               PERFORM A000-LEGGI-E06
                  THRU A000-LEGGI-E06-EX

               MOVE WK-CAP-PROT
                 TO IVVC0213-VAL-IMP-O
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA ESTENSIONE POLIZZA
      *----------------------------------------------------------------*
       A000-LEGGI-E06.
           INITIALIZE EST-POLI.
           MOVE DPOL-ID-POLI             TO E06-ID-POLI
           MOVE SPACES                   TO IDSV0003-BUFFER-WHERE-COND.

           SET IDSV0003-TRATT-X-COMPETENZA TO TRUE.
           SET IDSV0003-ID-PADRE           TO TRUE.
           SET IDSV0003-SELECT             TO TRUE.

           SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.

           CALL PGM-IDBSE060  USING  IDSV0003 EST-POLI
           ON EXCEPTION
              MOVE PGM-IDBSE060
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL IDBSE060 ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              IF E06-CPT-PROTETTO-NULL NOT = HIGH-VALUE
                                         AND LOW-VALUE AND SPACES
                 MOVE E06-CPT-PROTETTO
                   TO WK-CAP-PROT
              ELSE
                 MOVE ZEROES
                   TO WK-CAP-PROT
              END-IF
           ELSE
51238       IF  IDSV0003-NOT-FOUND
51238        IF DPOL-FL-POLI-IFP = 'G'
51238           SET IDSV0003-FIELD-NOT-VALUED TO TRUE
51238        ELSE
51238         SET IDSV0003-INVALID-OPER     TO TRUE
51238         MOVE PGM-IDBSE060             TO IDSV0003-COD-SERVIZIO-BE
51238
51238         MOVE   'MANCA LA VARIABILE CAPPROTETTO'
51238           TO  IDSV0003-DESCRIZ-ERR-DB2
51238
51238        END-IF
51238       ELSE
              SET IDSV0003-INVALID-OPER     TO TRUE
              MOVE PGM-IDBSE060             TO IDSV0003-COD-SERVIZIO-BE

              STRING 'CHIAMATA IDBSE060 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                     DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
51238       END-IF
           END-IF.

       A000-LEGGI-E06-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-POLI
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DPOL-AREA-POLI
           END-IF.
      *
       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORE
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.
      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.

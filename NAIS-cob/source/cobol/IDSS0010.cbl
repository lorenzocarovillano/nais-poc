       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDSS0010 IS INITIAL.
      ***************************************************************
      *                    PGM :  IDSS0010                          *
      *                   COPY :  IDSI0011 IDSO0011                 *
      ***************************************************************
       AUTHOR.  ATS.
      ***************************************************************
       ENVIRONMENT DIVISION.
      ***************************************************************
       CONFIGURATION SECTION.
      ***************************************************************
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
      ***************************************************************
       DATA DIVISION.
      ***************************************************************

      ***************************************************************
       WORKING-STORAGE SECTION.

      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WS-VARIABILI.
           03 WS-NOME-PROG                 PIC X(08).
      *----------------------------------------------------------------*
      *    COPY DECODIFICA TABELLA/NOME PROGRAMMA
      *----------------------------------------------------------------*
           COPY IABV0901.

      *----------------------------------------------------------------*
      *    SWITCH
      *----------------------------------------------------------------*
       01 SW-PGM-TROVATO             PIC X.
          88 PGM-TROVATO             VALUE 'S'.
          88 PGM-NO-TROVATO          VALUE 'N'.

           EXEC SQL INCLUDE SQLCA END-EXEC.

       01 WS-TS-SYSTEM-DB           PIC X(26).
      *----------------------------------------------------------------*
      *    CAMPI X ROUTINES DI GESTIONE DISPLAY IDSP8888
      *----------------------------------------------------------------*
           COPY IDSV8888.

           COPY IDSV0003.

      **************************************************************
       LINKAGE SECTION.
      **************************************************************
      *----------------------------------------------------------------*
      *    PUNTATORI
      *----------------------------------------------------------------*
      * 01 DISPATCHER-ADDRESS.
      *     05  WS-ADDRESS-DIS USAGE POINTER.

      ***************************************************************
      *    COPY INPUT
      ***************************************************************
       01 IN-IDSS0010.
           COPY IDSI0011.
      ***************************************************************
      *    COPY OUTPUT
      ***************************************************************
       01 OUT-IDSS0010.
           COPY IDSO0011.

      **************************************************************
       PROCEDURE DIVISION USING IN-IDSS0010  OUT-IDSS0010.

      *************************************************************

       MAIN  SECTION.

      *    SET ADDRESS OF IN-OUT-IDSS0010   TO WS-ADDRESS-DIS

           SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
           SET  IDSV8888-SONDA-S9           TO TRUE
           MOVE IDSI0011-MODALITA-ESECUTIVA
                TO IDSV8888-MODALITA-ESECUTIVA
           MOVE IDSI0011-USER-NAME          TO IDSV8888-USER-NAME
           MOVE IDSI0011-CODICE-STR-DATO     TO IDSV8888-NOME-PGM
           MOVE 'Data Dispatcher'           TO IDSV8888-DESC-PGM
           SET  IDSV8888-INIZIO             TO TRUE
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM A000-INIZIALIZZA-OUTPUT

           PERFORM A005-CONTROLLO-INPUT

           IF IDSO0011-SUCCESSFUL-RC
              PERFORM S2000-CERCA-NOME
                 THRU EX-S2000

              IF PGM-TROVATO
                 PERFORM S5000-VALOR-AREA
                    THRU EX-S5000
                 PERFORM S3000-CALL-PGM
                    THRU EX-S3000
              END-IF

           END-IF.

           SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
           SET  IDSV8888-SONDA-S9           TO TRUE
           MOVE IDSI0011-MODALITA-ESECUTIVA
                TO IDSV8888-MODALITA-ESECUTIVA
           MOVE IDSI0011-USER-NAME          TO IDSV8888-USER-NAME
           MOVE IDSI0011-CODICE-STR-DATO     TO IDSV8888-NOME-PGM
           MOVE 'Data Dispatcher'           TO IDSV8888-DESC-PGM
           SET  IDSV8888-FINE               TO TRUE
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM 4000-FINE.

      ***************************************************************
       A000-INIZIALIZZA-OUTPUT SECTION.
      *
           INITIALIZE WS-NOME-PROG.

           SET IDSO0011-SUCCESSFUL-RC  TO TRUE
           SET IDSO0011-SUCCESSFUL-SQL TO TRUE

           MOVE SPACES                TO IDSO0011-DESCRIZ-ERR-DB2.
      *
       A000-EX.
           EXIT.
      *************************************************************
      *                 CONTROLLA I CAMPI IN INPUT                *
      *************************************************************
       A005-CONTROLLO-INPUT    SECTION.
      *
           IF IDSI0011-CODICE-COMPAGNIA-ANIA = 0
              SET IDSO0011-COD-COMP-NOT-VALID TO TRUE
           END-IF.

           IF IDSI0011-OPERAZIONE      = SPACES
              SET IDSO0011-OPER-NOT-V TO TRUE
           END-IF.

           IF IDSI0011-CODICE-STR-DATO = SPACES
              SET IDSO0011-STR-DATO-NOT-VALID TO TRUE
           END-IF.

      *
       A005-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    RICERCA E DECODIFICA NOME TABELLA CON NOME PROGRAMMA
      *----------------------------------------------------------------*
       S2000-CERCA-NOME SECTION.

           SET PGM-NO-TROVATO TO TRUE.
           IF IDSI0011-CODICE-STR-DATO(1:4) = 'LDBS' OR 'IDES'
               MOVE IDSI0011-CODICE-STR-DATO        TO WS-NOME-PROG
               SET  PGM-TROVATO                     TO TRUE
           ELSE
               SEARCH ALL IABV0901-ELE-TAB-CALL-PGM
                     AT END
                           PERFORM S4000-SCRIVI-ERRORE
                              THRU EX-S4000
                     WHEN IABV0901-COD-STR-DATO(IABV0901-INDEX) =
                          IDSI0011-CODICE-STR-DATO
                          MOVE IABV0901-NOME-PGM(IABV0901-INDEX)
                            TO WS-NOME-PROG
                          SET PGM-TROVATO           TO TRUE
               END-SEARCH
           END-IF.

       EX-S2000.
           EXIT.
      *----------------------------------------------------------------*
      *   CHIAMATA AL PROGRAMMA CERCATO SULLA COPY
      *----------------------------------------------------------------*
       S3000-CALL-PGM SECTION.

           CALL WS-NOME-PROG           USING IDSV0003
                                             IDSI0011-BUFFER-DATI.

           MOVE IDSV0003-SQLCODE          TO IDSO0011-SQLCODE
                                             IDSO0011-SQLCODE-SIGNED.

           MOVE IDSV0003-RETURN-CODE      TO IDSO0011-RETURN-CODE.
           MOVE IDSV0003-DESCRIZ-ERR-DB2  TO IDSO0011-DESCRIZ-ERR-DB2.
           MOVE IDSV0003-CAMPI-ESITO      TO IDSO0011-CAMPI-ESITO.

       EX-S3000.
           EXIT.
      *----------------------------------------------------------------*
      *    ERRORE PER NON AVER TROVATO PROGRAMMA SU COPY
      *----------------------------------------------------------------*
       S4000-SCRIVI-ERRORE.

           MOVE 'SERVIZIO NON TROVATO'     TO IDSV0003-DESCRIZ-ERR-DB2.
           SET IDSV0003-SERV-NOT-F-ON-MSS  TO TRUE.
      *    MOVE IDSV0003-SQLCODE           TO IDSO0011-SQLCODE
      *                                       IDSO0011-SQLCODE-SIGNED.

           MOVE IDSV0003-RETURN-CODE       TO IDSO0011-RETURN-CODE.
           MOVE IDSV0003-DESCRIZ-ERR-DB2   TO IDSO0011-DESCRIZ-ERR-DB2.
           MOVE IDSV0003-CAMPI-ESITO       TO IDSO0011-CAMPI-ESITO.


       EX-S4000.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA
      *----------------------------------------------------------------*
       S5000-VALOR-AREA.

           SET IDSV0003-SUCCESSFUL-RC  TO TRUE
           SET IDSV0003-SUCCESSFUL-SQL TO TRUE

           INITIALIZE   IDSV0003-TIPOLOGIA-OPERAZIONE
                        IDSV0003-CAMPI-ESITO

           MOVE IDSI0011-MODALITA-ESECUTIVA
                         TO IDSV0003-MODALITA-ESECUTIVA
           MOVE IDSI0011-CODICE-COMPAGNIA-ANIA
                         TO IDSV0003-CODICE-COMPAGNIA-ANIA
           MOVE IDSI0011-COD-MAIN-BATCH
                         TO IDSV0003-COD-MAIN-BATCH
           MOVE IDSI0011-TIPO-MOVIMENTO
                         TO IDSV0003-TIPO-MOVIMENTO
           MOVE IDSI0011-SESSIONE
                         TO IDSV0003-SESSIONE
           MOVE IDSI0011-USER-NAME
                         TO IDSV0003-USER-NAME
           MOVE IDSI0011-DATA-INIZIO-EFFETTO
                         TO IDSV0003-DATA-INIZIO-EFFETTO
           MOVE IDSI0011-DATA-FINE-EFFETTO
                         TO IDSV0003-DATA-FINE-EFFETTO
           MOVE IDSI0011-DATA-COMPETENZA
                         TO IDSV0003-DATA-COMPETENZA
           MOVE IDSI0011-DATA-COMP-AGG-STOR
                         TO IDSV0003-DATA-COMP-AGG-STOR
           MOVE IDSI0011-TRATTAMENTO-STORICITA
                         TO IDSV0003-TRATTAMENTO-STORICITA
           MOVE IDSI0011-FORMATO-DATA-DB
                         TO IDSV0003-FORMATO-DATA-DB
           MOVE IDSI0011-ID-MOVI-ANNULLATO
                         TO IDSV0003-ID-MOVI-ANNULLATO
           MOVE IDSI0011-IDENTITA-CHIAMANTE
                         TO IDSV0003-IDENTITA-CHIAMANTE
           MOVE IDSI0011-LIVELLO-OPERAZIONE
                         TO IDSV0003-LIVELLO-OPERAZIONE
           MOVE IDSI0011-OPERAZIONE
                         TO IDSV0003-OPERAZIONE
           MOVE IDSI0011-FLAG-CODA-TS
                         TO IDSV0003-FLAG-CODA-TS
           MOVE IDSI0011-BUFFER-WHERE-COND
                         TO IDSV0003-BUFFER-WHERE-COND.

       EX-S5000.
           EXIT.
      *******************************************************
      *               GESTIONE CONDIZIONE ERRORE
      *********************************************************
       4000-FINE SECTION.
      *
           GOBACK.
      *
       4000-EX-FINE.
           EXIT.

       4010-TIMESTAMP.
           EXEC SQL
                 VALUES CURRENT TIMESTAMP
                   INTO :WS-TS-SYSTEM-DB
           END-EXEC.

       4010-EX-FINE.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI GESTIONE DISPLAY
      *----------------------------------------------------------------*
           COPY IDSP8889.

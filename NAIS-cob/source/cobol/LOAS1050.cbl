      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA VER. 1.0                          **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LOAS1050.
       AUTHOR.             AISS.
       DATE-WRITTEN.       2010.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *    PROGRAMMA ..... LOAS1050
      *    TIPOLOGIA...... GESTIONE MATR_ELAB_BATCH
      *    PROCESSO....... OPERAZIONI AUTOMATICA
      *    FUNZIONE....... TRASVERSALI
      *    DESCRIZIONE.... AGGIORNAMENTO DATA ELABORAZIONE
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01 WK-PGM                            PIC X(008) VALUE 'LOAS1050'.

      *----------------------------------------------------------------*
      *    VARIABILI DI WORKING
      *----------------------------------------------------------------*
       01 WK-VARIABILI.
          03 WK-TABELLA                     PIC X(20) VALUE SPACES.

      *----------------------------------------------------------------*
      *    AREE TABELLE
      *----------------------------------------------------------------*
       01  AREA-TABELLE.
           03 WL71-AREA-MATR-ELAB.
              04 WL71-ELE-MATR-ELAB-MAX  PIC 9(04) COMP.
              04 WL71-TAB-L71.
                 COPY LCCVL711           REPLACING ==(SF)== BY ==WL71==.

      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01 IX-INDICI.
          03 IX-TAB-L71                  PIC S9(04) COMP.

      *----------------------------------------------------------------*
      *    AREE MODULI CHIAMATI
      *----------------------------------------------------------------*
      *---- AREA PER ESTRAZIONE SEQUENCE

       01 AREA-IO-LCCS0090.
          COPY LCCC0090                  REPLACING ==(SF)== BY ==S090==.

       01 LCCS0090                       PIC X(008) VALUE 'LCCS0090'.

      *----------------------------------------------------------------*
      *    COPY VARIBILI GESTIONE TIPOLOGICHE                          *
      *----------------------------------------------------------------*
           COPY LCCC0006.
           COPY LCCVXMV0.
      *----------------------------------------------------------------*
      *    COPY DELLE TABELLE DB2
      *----------------------------------------------------------------*
           COPY IDBVL711.
      *----------------------------------------------------------------*
      *    COPY DISPATCHER
      *----------------------------------------------------------------*
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
           COPY IDSI0011.
           COPY IDSO0011.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.

       01  AREA-LOAS1050.
           COPY LOAV1051    REPLACING ==(SF)== BY ==S1050==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                AREA-LOAS1050.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *    OPERAZIONI INIZIALI                                         *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           PERFORM INIZIA-TOT-L71
              THRU INIZIA-TOT-L71-EX.

       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           PERFORM S2100-LEGGI-MATR-ELAB
              THRU EX-S2100

           IF IDSV0001-ESITO-OK
              MOVE IX-TAB-L71
                TO WL71-ELE-MATR-ELAB-MAX

              PERFORM AGGIORNA-MATR-ELAB-BATCH
                 THRU AGGIORNA-MATR-ELAB-BATCH-EX
           END-IF.

       EX-S1000.
           EXIT.
      *----------------------------------------------------------------
      *     LETTURA MATR_ELAB_BATCH
      *----------------------------------------------------------------
       S2100-LEGGI-MATR-ELAB.

           INITIALIZE                       MATR-ELAB-BATCH.

           MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-SELECT           TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.

           MOVE 'LDBS6590'               TO IDSI0011-CODICE-STR-DATO.
           MOVE SPACES                   TO IDSI0011-BUFFER-DATI.

           MOVE S1050-COD-RAMO           TO L71-COD-RAMO.
           MOVE S1050-FORMA-ASS          TO L71-TP-FRM-ASSVA.
           MOVE S1050-TP-MOVI            TO L71-TP-MOVI.
           MOVE MATR-ELAB-BATCH          TO IDSI0011-BUFFER-DATI.

           MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.

           SET  IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL  TO TRUE.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

              IF IDSO0011-SUCCESSFUL-RC
                 EVALUATE TRUE
                     WHEN IDSO0011-SUCCESSFUL-SQL
      *-->           OPERAZIONE ESEGUITA CORRETTAMENTE

                        MOVE IDSO0011-BUFFER-DATI
                          TO MATR-ELAB-BATCH

                        MOVE 1           TO IX-TAB-L71

                        PERFORM VALORIZZA-OUTPUT-L71
                           THRU VALORIZZA-OUTPUT-L71-EX

                        MOVE S1050-DT-ULT-ELABORAZIONE
                          TO WL71-DT-ULT-ELAB

                        SET  WL71-ST-MOD TO TRUE

                     WHEN IDSO0011-NOT-FOUND
      *--->          CHIAVE NON TROVATA

                        MOVE 1                      TO IX-TAB-L71
                        SET WL71-ST-ADD             TO TRUE

                        MOVE S1050-COD-RAMO         TO WL71-COD-RAMO
                        MOVE S1050-FORMA-ASS        TO WL71-TP-FRM-ASSVA
                        MOVE S1050-TP-MOVI          TO WL71-TP-MOVI
                        MOVE IDSV0001-COD-COMPAGNIA-ANIA
                          TO WL71-COD-COMP-ANIA
                        MOVE S1050-DT-ULT-ELABORAZIONE
                          TO WL71-DT-ULT-ELAB

                     WHEN OTHER
      *--->          ERRORE DI ACCESSO AL DB
                          MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                          MOVE 'LDBS6590'    TO IEAI9901-LABEL-ERR
                          MOVE '005016'      TO IEAI9901-COD-ERRORE
                          STRING 'MATR-ELAB-BATCH'    ';'
                                 IDSO0011-RETURN-CODE ';'
                                 IDSO0011-SQLCODE
                          DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                          END-STRING
                          PERFORM S0300-RICERCA-GRAVITA-ERRORE
                             THRU EX-S0300
                 END-EVALUATE
              ELSE
      *-->       GESTIONE ERRORE DISPATCHER
                 MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'LDBS6590'    TO IEAI9901-LABEL-ERR
                 MOVE '005016'      TO IEAI9901-COD-ERRORE
                 STRING 'MATR-ELAB-BATCH'     ';'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                 DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF.

       EX-S2100.
           EXIT.

      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

              GOBACK.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      *    COPY PROCEDURE (COMPONENTI COMUNI)
      *----------------------------------------------------------------*
           COPY LCCP0001              REPLACING ==(SF)== BY ==WL71==.
           COPY LCCP0002              REPLACING ==(SF)== BY ==WL71==.
      *----------------------------------------------------------------*
      *    COPY PROCEDURE LETTURA PORTAFOGLIO
      *----------------------------------------------------------------*
      *     COPY LCCVL712              REPLACING ==(SF)== BY ==WL71==.
           COPY LCCVL713              REPLACING ==(SF)== BY ==WL71==.
           COPY LCCVL714              REPLACING ==(SF)== BY ==WL71==.
      *----------------------------------------------------------------*
      *    COPY PROCEDURE AGGIORNAMENTO PORTAFOGLIO
      *----------------------------------------------------------------*
           COPY LCCVL715              REPLACING ==(SF)== BY ==WL71==.
           COPY LCCVL716              REPLACING ==(SF)== BY ==WL71==.
      *----------------------------------------------------------------*
      * ROUTINES GESTIONE ERRORI                                       *
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.
      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.

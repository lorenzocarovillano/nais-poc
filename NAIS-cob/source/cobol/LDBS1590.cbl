       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LDBS1590 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  12 LUG 2010.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2     PIC X(300)  VALUE SPACES.

       77 WS-BUFFER-TABLE     PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ      PIC 9(009)  VALUE ZEROES.

           EXEC SQL INCLUDE IDSV0010 END-EXEC.

      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDISO0 END-EXEC.
           EXEC SQL INCLUDE IDBDTIT0 END-EXEC.
           EXEC SQL INCLUDE IDBVISO2 END-EXEC.
           EXEC SQL INCLUDE IDBVTIT2 END-EXEC.
           EXEC SQL INCLUDE IDBVISO3 END-EXEC.
           EXEC SQL INCLUDE IDBVTIT3 END-EXEC.
      *
           EXEC SQL INCLUDE IDBVTIT1 END-EXEC.
           EXEC SQL INCLUDE IDBVISO1 END-EXEC.
      *
      *
      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE LDBV1591 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 LDBV1591.


           PERFORM A000-INIZIO              THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-WHERE-CONDITION
                       PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-WHERE-CONDITION
                         PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-WHERE-CONDITION
                           PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                      SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.


           GOBACK.

       A000-INIZIO.
           MOVE 'LDBS1590'              TO   IDSV0003-COD-SERVIZIO-BE.
           MOVE 'LDBV1591' TO   IDSV0003-NOME-TABELLA.

           MOVE '00'                      TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                    TO   IDSV0003-SQLCODE
                                               IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
                                               IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.

       A200-ELABORA-WC-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.

       B200-ELABORA-WC-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B200-EX.
           EXIT.

       C200-ELABORA-WC-NST.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       C200-EX.
           EXIT.
      *----
      *----  gestione WC Effetto
      *----
       A205-DECLARE-CURSOR-WC-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.


           SET IDSV0003-INVALID-OPER TO TRUE.
       A205-EX.
           EXIT.

       A210-SELECT-WC-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           EXEC SQL
            SELECT MAX(DT_INI_PER)
            INTO  :ISO-DT-INI-PER-DB
                  :IND-ISO-DT-INI-PER
                FROM IMPST_SOST
               WHERE ID_OGG = :LDBV1591-ID-ADES
                 AND TP_OGG = 'AD'
                 AND DT_INI_PER <=
                     :WS-DATA-INIZIO-EFFETTO-DB
                 AND DT_INI_EFF <=
                     :WS-DT-INFINITO-1
                 AND DT_END_EFF >
                     :WS-DT-INFINITO-1
                 AND DS_TS_INI_CPTZ <=
                     :WS-TS-INFINITO-1
                 AND DS_TS_END_CPTZ >
                     :WS-TS-INFINITO-1
                 AND COD_COMP_ANIA =
                     :IDSV0003-CODICE-COMPAGNIA-ANIA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

              IF IND-ISO-DT-INI-PER = -1
                 PERFORM Z900-CONVERTI-N-TO-X  THRU Z900-EX
              END-IF

              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
           END-IF.

       A210-EX.
           EXIT.

       A260-OPEN-CURSOR-WC-EFF.
           PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A260-EX.
           EXIT.

       A270-CLOSE-CURSOR-WC-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A270-EX.
           EXIT.

       A280-FETCH-FIRST-WC-EFF.
           PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
           END-IF.
       A280-EX.
           EXIT.

       A290-FETCH-NEXT-WC-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A290-EX.
           EXIT.
      *----
      *----  gestione WC Competenza
      *----
       B205-DECLARE-CURSOR-WC-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B205-EX.
           EXIT.

       B210-SELECT-WC-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           EXEC SQL
            SELECT MAX(DT_INI_PER)
            INTO  :ISO-DT-INI-PER-DB
                  :IND-ISO-DT-INI-PER
                FROM IMPST_SOST
               WHERE ID_OGG = :LDBV1591-ID-ADES
                 AND TP_OGG = 'AD'
                 AND DT_INI_PER <=
                     :WS-DATA-INIZIO-EFFETTO-DB
                 AND DT_INI_EFF <=
                     :WS-DT-INFINITO-1
                 AND DT_END_EFF >
                     :WS-DT-INFINITO-1
                 AND DS_TS_INI_CPTZ <=
                     :WS-TS-INFINITO-1
                 AND DS_TS_END_CPTZ >
                     :WS-TS-INFINITO-1
                 AND COD_COMP_ANIA =
                     :IDSV0003-CODICE-COMPAGNIA-ANIA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

              IF IND-ISO-DT-INI-PER = -1
                 PERFORM Z900-CONVERTI-N-TO-X  THRU Z900-EX
              END-IF

              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX

           END-IF.

       B210-EX.
           EXIT.

       B260-OPEN-CURSOR-WC-CPZ.
           PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B260-EX.
           EXIT.

       B270-CLOSE-CURSOR-WC-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B270-EX.
           EXIT.

       B280-FETCH-FIRST-WC-CPZ.
           PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
           END-IF.
       B280-EX.
           EXIT.

       B290-FETCH-NEXT-WC-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B290-EX.
           EXIT.
      *----
      *----  gestione WC Senza Storicit—
      *----
       C205-DECLARE-CURSOR-WC-NST.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       C205-EX.
           EXIT.

       C210-SELECT-WC-NST.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       C210-EX.
           EXIT.

       C260-OPEN-CURSOR-WC-NST.
           PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       C260-EX.
           EXIT.

       C270-CLOSE-CURSOR-WC-NST.
           SET IDSV0003-INVALID-OPER TO TRUE.
       C270-EX.
           EXIT.

       C280-FETCH-FIRST-WC-NST.
           PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
           END-IF.
       C280-EX.
           EXIT.

       C290-FETCH-NEXT-WC-NST.
           SET IDSV0003-INVALID-OPER TO TRUE.
       C290-EX.
           EXIT.
      *----
      *----  utilità comuni a tutti i livelli operazione
      *----
       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-ISO-ID-OGG = -1
              MOVE HIGH-VALUES TO ISO-ID-OGG-NULL
           END-IF
           IF IND-ISO-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO ISO-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-ISO-DT-INI-PER = -1
              MOVE HIGH-VALUES TO ISO-DT-INI-PER-NULL
           END-IF
           IF IND-ISO-DT-END-PER = -1
              MOVE HIGH-VALUES TO ISO-DT-END-PER-NULL
           END-IF
           IF IND-ISO-IMPST-SOST = -1
              MOVE HIGH-VALUES TO ISO-IMPST-SOST-NULL
           END-IF
           IF IND-ISO-IMPB-IS = -1
              MOVE HIGH-VALUES TO ISO-IMPB-IS-NULL
           END-IF
           IF IND-ISO-ALQ-IS = -1
              MOVE HIGH-VALUES TO ISO-ALQ-IS-NULL
           END-IF
           IF IND-ISO-COD-TRB = -1
              MOVE HIGH-VALUES TO ISO-COD-TRB-NULL
           END-IF
           IF IND-ISO-PRSTZ-LRD-ANTE-IS = -1
              MOVE HIGH-VALUES TO ISO-PRSTZ-LRD-ANTE-IS-NULL
           END-IF
           IF IND-ISO-RIS-MAT-NET-PREC = -1
              MOVE HIGH-VALUES TO ISO-RIS-MAT-NET-PREC-NULL
           END-IF
           IF IND-ISO-RIS-MAT-ANTE-TAX = -1
              MOVE HIGH-VALUES TO ISO-RIS-MAT-ANTE-TAX-NULL
           END-IF
           IF IND-ISO-RIS-MAT-POST-TAX = -1
              MOVE HIGH-VALUES TO ISO-RIS-MAT-POST-TAX-NULL
           END-IF
           IF IND-ISO-PRSTZ-NET = -1
              MOVE HIGH-VALUES TO ISO-PRSTZ-NET-NULL
           END-IF
           IF IND-ISO-PRSTZ-PREC = -1
              MOVE HIGH-VALUES TO ISO-PRSTZ-PREC-NULL
           END-IF
           IF IND-ISO-CUM-PRE-VERS = -1
              MOVE HIGH-VALUES TO ISO-CUM-PRE-VERS-NULL
           END-IF.

       Z100-EX.
           EXIT.

      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE LDBV1591-DT-DECOR  TO WS-DATE-N.
           PERFORM Z700-DT-N-TO-X THRU Z700-EX.
           MOVE WS-DATE-X      TO ISO-DT-INI-PER-DB.

       Z900-EX.
           EXIT.

      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE ISO-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO ISO-DT-INI-EFF
           MOVE ISO-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO ISO-DT-END-EFF
           IF IND-ISO-DT-INI-PER = 0
               MOVE ISO-DT-INI-PER-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ISO-DT-INI-PER
           END-IF
           IF IND-ISO-DT-END-PER = 0
               MOVE ISO-DT-END-PER-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ISO-DT-END-PER
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----
      *----  prevede statements AD HOC PRE Query
      *----
       Z970-CODICE-ADHOC-PRE.

           CONTINUE.
       Z970-EX.
           EXIT.
      *----
      *----  prevede statements AD HOC POST Query
      *----
       Z980-CODICE-ADHOC-POST.

              EXEC SQL
               SELECT VALUE (SUM(TOT_PRE_SOLO_RSH) , 0)
                 INTO :LDBV1591-IMP-TOT
               FROM TIT_CONT
                WHERE ID_OGG = :LDBV1591-ID-OGG
                  AND TP_OGG = :LDBV1591-TP-OGG
                  AND DT_INI_COP BETWEEN :ISO-DT-INI-PER-DB
                  AND :WS-DATA-INIZIO-EFFETTO-DB
                  AND COD_COMP_ANIA =
                      :IDSV0003-CODICE-COMPAGNIA-ANIA
                  AND DT_INI_EFF <=
                      :WS-DATA-INIZIO-EFFETTO-DB
                  AND DT_END_EFF >
                      :WS-DATA-INIZIO-EFFETTO-DB
                  AND DS_TS_INI_CPTZ <=
                      :WS-TS-COMPETENZA
                  AND DS_TS_END_CPTZ >
                      :WS-TS-COMPETENZA
              END-EXEC.
           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z980-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

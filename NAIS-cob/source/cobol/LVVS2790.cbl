      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS2790.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2013.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS2790
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... NUOVA FISCALITA'
      *                  CALCOLO DELLA VARIABILE CED_III
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS2790'.
       01  LDBS7480                         PIC X(008) VALUE 'LDBS7480'.
       01  IDBSLQU0                         PIC X(008) VALUE 'IDBSLQU0'.
       01  IDBSGRZ0                         PIC X(008) VALUE 'IDBSGRZ0'.
       01  IDBSTGA0                         PIC X(008) VALUE 'IDBSTGA0'.
       01  LDBS1560                         PIC X(008) VALUE 'LDBS1560'.
       01  LDBS6040                         PIC X(008) VALUE 'LDBS6040'.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  IX-TAB-LQU                      PIC 9(02) VALUE 0.
       01  WK-SOMMA                        PIC S9(12)V9(3) COMP-3.
       01  WK-DATA                         PIC 9(08).
       01  WK-DATA-SEPARATA REDEFINES WK-DATA.
           03 WK-ANNO                  PIC 9(04).
           03 WK-MESE                  PIC 9(02).
           03 WK-GG                    PIC 9(02).

       01 WK-DATA-INIZIO.
           03 WK-ANNO-INI              PIC 9(04).
           03 WK-MESE-INI              PIC 9(02).
           03 WK-GG-INI                PIC 9(02).

       01 WK-DATA-INIZIO-D             REDEFINES
             WK-DATA-INIZIO             PIC S9(8)V COMP-3.

       01 WK-DATA-FINE.
           03 WK-ANNO-FINE              PIC 9(04).
           03 WK-MESE-FINE              PIC 9(02).
           03 WK-GG-FINE                PIC 9(02).

       01 WK-DATA-FINE-D             REDEFINES
             WK-DATA-FINE               PIC S9(8)V COMP-3.
      *---------------------------------------------------------------*
      *  VARIABILI DI WORKING
      *---------------------------------------------------------------*
       01 WK-VARIABILI.

           03 FLAG-FINE-TDL                       PIC X(1).
              88 FINE-TDL-SI                      VALUE 'S'.
              88 FINE-TDL-NO                      VALUE 'N'.

           03 FLAG-FINE-LQU                       PIC X(1).
              88 FINE-LQU-SI                      VALUE 'S'.
              88 FINE-LQU-NO                      VALUE 'N'.

           03 FLAG-RAMO-III                          PIC X(1).
              88 RAMO-III-SI                         VALUE 'S'.
              88 RAMO-III-NO                         VALUE 'N'.

           03 FLAG-INDEX                          PIC X(1).
              88 INDEX-SI                         VALUE 'S'.
              88 INDEX-NO                         VALUE 'N'.

      *---------------------------------------------------------------*
      *  LISTA MOVIMENTI IMPLEMENTATI LCCC0006
      *---------------------------------------------------------------*
           COPY LCCC0006.
      *---------------------------------------------------------------*
      *  COPY DB2
      *---------------------------------------------------------------*
           COPY IDBVLQU1.
           COPY IDBVTLI1.
           COPY IDBVTGA1.
           COPY IDBVGRZ1.
           COPY IDBVMOV1.
      *---------------------------------------------------------------*
      *  LISTA TABELLE E TIPOLOGICHE
      *---------------------------------------------------------------*
10819      COPY LCCVXMVZ.
10819      COPY LCCVXMV2.
10819      COPY LCCVXMV3.
10819      COPY LCCVXMV5.
10819      COPY LCCVXMV6.
           COPY LCCVLQUZ.
      *----------------------------------------------------------------*
      *--> AREA LIQUIDAZIONE
      *----------------------------------------------------------------*
      *01 AREA-IO-LIQUID.
      *   03 WLQU-AREA-LIQ.
      *      04 WLQU-ELE-LIQ-MAX        PIC S9(04) COMP.
      *         COPY LCCVLQUB REPLACING   ==(SF)==  BY ==WLQU==.
      *      COPY LCCVLQU1              REPLACING ==(SF)== BY ==WLQU==.
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-POLI.
          03 DPOL-AREA-POLI.
             04 DPOL-ELE-POLI-MAX        PIC S9(04) COMP.
             04 DPOL-TAB-POLI.
             COPY LCCVPOL1              REPLACING ==(SF)== BY ==DPOL==.
      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIABILI PER LA GESTIONE ERRORI                     *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
           03 IX-TAB-ISO                     PIC S9(04) COMP.

      *----------------------------------------------------------------*
      *    FLAG
      *----------------------------------------------------------------*
       01 FLAG-CUR-MOV                       PIC X(01).
           88 INIT-CUR-MOV                   VALUE 'S'.
           88 FINE-CUR-MOV                   VALUE 'N'.

       01 FLAG-ULTIMA-LETTURA               PIC X(01).
          88 SI-ULTIMA-LETTURA              VALUE 'S'.
          88 NO-ULTIMA-LETTURA              VALUE 'N'.

       01 FLAG-FINE-LETTURA-P58               PIC X(01).
          88 FINE-LETT-P58-SI               VALUE 'S'.
          88 FINE-LETT-P58-NO               VALUE 'N'.

       01 FLAG-COMUN-TROV                    PIC X(01).
           88 COMUN-TROV-SI                  VALUE 'S'.
           88 COMUN-TROV-NO                  VALUE 'N'.

       01  WK-VAR-MOVI-COMUN.
           05 WK-ID-MOVI-COMUN             PIC S9(09) COMP-3.
           05 WK-DATA-EFF-PREC             PIC S9(08) COMP-3.
           05 WK-DATA-CPTZ-PREC            PIC S9(18) COMP-3.
           05 WK-DATA-EFF-RIP              PIC S9(08) COMP-3.
           05 WK-DATA-CPTZ-RIP             PIC S9(18) COMP-3.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS2790.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS2790.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

           SET COMUN-TROV-NO                 TO TRUE.

      *      PERFORM VARYING IX-TAB-LQU FROM 1 BY 1
      *              UNTIL IX-TAB-LQU > WK-LQU-MAX-B
      *      PERFORM INIZIA-TOT-LQU
      *         THRU INIZIA-TOT-LQU-EX
      *    END-PERFORM.
       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE AREA-IO-POLI.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE


           PERFORM VERIFICA-MOVIMENTO
              THRU VERIFICA-MOVIMENTO-EX

      *--> LETTURA DELLA POLIZZA PER REC DATA DECORRENZA
           IF  IDSV0003-SUCCESSFUL-RC
      *    AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1110-LETTURA-LIQ
                  THRU S1110-EX
           END-IF

           MOVE 0   TO WK-SOMMA.

      *    PERFORM VARYING IX-TAB-LQU FROM 1 BY 1
      *      UNTIL IX-TAB-LQU > WLQU-ELE-LIQ-MAX
      *        OR IDSV0003-INVALID-OPER
      *        IF  WLQU-TP-LIQ(IX-TAB-LQU)  = 'CE'
      *            PERFORM S1111-LETTURA-TRANCHE-LIQ
      *               THRU S1111-EX
      *        END-IF
      *
      *    END-PERFORM.

       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-POLI
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DPOL-AREA-POLI
           END-IF.


       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   LETTURA DELLA POLIZZA
      *----------------------------------------------------------------*
       S1110-LETTURA-LIQ.

           INITIALIZE LIQ
      *               IX-TAB-LQU
      *               WLQU-ELE-LIQ-MAX.

           SET FINE-LQU-NO           TO TRUE.
           MOVE IVVC0213-ID-ADESIONE TO LQU-ID-OGG.
           MOVE 'AD'                 TO LQU-TP-OGG.

      *  --> Tipo operazione
           SET IDSV0003-FETCH-FIRST       TO TRUE.
      *  --> Tipo livello
           SET IDSV0003-ID-OGGETTO        TO TRUE.
           SET IDSV0003-TRATT-X-COMPETENZA TO TRUE.
      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC     TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL    TO TRUE.

           PERFORM UNTIL FINE-LQU-SI

           CALL IDBSLQU0    USING IDSV0003 LIQ

           ON EXCEPTION
              MOVE IDBSLQU0               TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL-IDBSLQU0 ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER   TO TRUE
           END-CALL

           IF IDSV0003-SUCCESSFUL-RC

              EVALUATE TRUE

                 WHEN IDSV0003-NOT-FOUND
      *-->    NESSUN DATO IN TABELLA
                          SET FINE-LQU-SI             TO TRUE
                          SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                        IF IDSV0003-FETCH-FIRST
                           MOVE WK-PGM
                             TO IDSV0003-COD-SERVIZIO-BE
                           STRING 'ERRORE - NON TROVATE LIQUIDAZIONI'
                                   IDSV0003-RETURN-CODE ';'
                                   IDSV0003-SQLCODE
                                   DELIMITED BY SIZE INTO
                                   IDSV0003-DESCRIZ-ERR-DB2
                           END-STRING
                        END-IF
                     WHEN IDSV0003-SUCCESSFUL-SQL
      *                   ADD 1       TO IX-TAB-LQU
      *                   PERFORM VALORIZZA-OUTPUT-LQU
      *                      THRU VALORIZZA-OUTPUT-LQU-EX
      *                   MOVE IX-TAB-LQU TO WLQU-ELE-LIQ-MAX
                          IF  LQU-TP-LIQ  = 'CE'
                              PERFORM S1111-LETTURA-TRANCHE-LIQ
                                 THRU S1111-EX
                          END-IF
                          SET IDSV0003-FETCH-NEXT  TO TRUE
                     WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                          SET IDSV0003-INVALID-OPER  TO TRUE
                          MOVE WK-PGM
                            TO IDSV0003-COD-SERVIZIO-BE
                          STRING 'ERRORE LETTURA TABELLA LIQUIDAZ ;'
                                 IDSV0003-RETURN-CODE ';'
                                 IDSV0003-SQLCODE
                                 DELIMITED BY SIZE INTO
                                 IDSV0003-DESCRIZ-ERR-DB2
                          END-STRING
                          SET FINE-LQU-SI                TO TRUE

                 END-EVALUATE

39322      ELSE
39322 *--->   ERRORE DI ACCESSO AL DB
39322            SET IDSV0003-INVALID-OPER  TO TRUE
39322            MOVE WK-PGM
39322                      TO IDSV0003-COD-SERVIZIO-BE
39322            STRING 'ERRORE LETTURA TABELLA LIQUIDAZ ;'
39322                   IDSV0003-RETURN-CODE ';'
39322                   IDSV0003-SQLCODE
39322                   DELIMITED BY SIZE INTO
39322                   IDSV0003-DESCRIZ-ERR-DB2
39322            END-STRING
39322            SET FINE-LQU-SI                TO TRUE
           END-IF

           END-PERFORM.
      *    IF WLQU-ELE-LIQ-MAX = 0
      *       AND IDSV0003-SUCCESSFUL-SQL
      *       MOVE WK-PGM
      *         TO IDSV0003-COD-SERVIZIO-BE
      *       STRING 'ERRORE - NON TROVATE LIQUIDAZIONI'
      *               IDSV0003-RETURN-CODE ';'
      *               IDSV0003-SQLCODE
      *               DELIMITED BY SIZE INTO
      *               IDSV0003-DESCRIZ-ERR-DB2
      *       END-STRING
      *    END-IF.

       S1110-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1111-LETTURA-TRANCHE-LIQ.

      *--> TIPO OPERAZIONE
      *    SET IDSV0003-FETCH-FIRST       TO TRUE.

      *    SET FINE-TDL-NO                TO TRUE.

      *    SET INDEX-NO                   TO TRUE.

      *    SET RAMO-III-NO                TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
      *    SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
      *    SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.

      *    PERFORM UNTIL FINE-TDL-SI

      *       PERFORM INIZIA-TRANCHE-LIQ
      *          THRU INIZIA-TRANCHE-LIQ-EX


      *       CALL LDBS1560 USING IDSV0003 TRCH-LIQ
      *       ON EXCEPTION
      *          MOVE LDBS7480
      *            TO IDSV0003-COD-SERVIZIO-BE
      *          MOVE 'ERRORE CALL LDBS7480 FETCH'
      *            TO IDSV0003-DESCRIZ-ERR-DB2
      *          SET IDSV0003-INVALID-OPER TO TRUE
      *       END-CALL

      *       IF IDSV0003-SUCCESSFUL-RC

      *          EVALUATE TRUE

      *              WHEN IDSV0003-NOT-FOUND
      *-->    NESSUN DATO IN TABELLA
      *                   SET FINE-TDL-SI             TO TRUE
      *                   SET IDSV0003-SUCCESSFUL-SQL TO TRUE
      *              WHEN IDSV0003-SUCCESSFUL-SQL
      *                   PERFORM RECUPERA-TRCH-DI-GAR
      *                      THRU RECUPERA-TRCH-DI-GAR-EX
      *
      *                   SET IDSV0003-FETCH-NEXT  TO TRUE
      *                   PERFORM INIZIA-TRANCHE-LIQ
      *                      THRU INIZIA-TRANCHE-LIQ-EX
      *
      *              WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
      *                   SET IDSV0003-INVALID-OPER  TO TRUE
      *                   MOVE WK-PGM
      *                     TO IDSV0003-COD-SERVIZIO-BE
      *                   STRING 'ERRORE LETT TAB TRANCHE DI LIQ ;'
      *                          IDSV0003-RETURN-CODE ';'
      *                          IDSV0003-SQLCODE
      *                          DELIMITED BY SIZE INTO
      *                          IDSV0003-DESCRIZ-ERR-DB2
      *                   END-STRING
      *                   SET FINE-TDL-SI                TO TRUE
      *          END-EVALUATE
      *       ELSE
      *          SET IDSV0003-INVALID-OPER  TO TRUE
      *          MOVE WK-PGM
      *            TO IDSV0003-COD-SERVIZIO-BE
      *          STRING 'ERRORE LETT TAB TRANCHE DI LIQ ;'
      *                  IDSV0003-RETURN-CODE ';'
      *                  IDSV0003-SQLCODE
      *                  DELIMITED BY SIZE INTO
      *                  IDSV0003-DESCRIZ-ERR-DB2
      *          END-STRING
      *          SET FINE-TDL-SI                TO TRUE
      *       END-IF
      *
      *    END-PERFORM.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1150-CALCOLO-ANNO
                  THRU S1150-CALCOLO-ANNO-EX
           END-IF.

      *--> PERFORM DI CALCOLO DELL'IMPORTO
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
      *    AND RAMO-III-SI
               PERFORM S1200-CALCOLO-IMPORTO
                  THRU S1200-CALCOLO-IMPORTO-EX
           END-IF.


       S1111-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
      *INIZIA-TRANCHE-LIQ.
      *
      *       INITIALIZE TRCH-LIQ
      *
      *       MOVE WLQU-ID-LIQ(IX-TAB-LQU)
      *         TO TLI-ID-LIQ
      *
      *       MOVE TRCH-LIQ          TO IDSV0003-BUFFER-WHERE-COND
      *
      *       SET IDSV0003-TRATT-X-COMPETENZA TO TRUE

      *--> LIVELLO OPERAZIONE
      *       SET IDSV0003-WHERE-CONDITION TO TRUE.


      *INIZIA-TRANCHE-LIQ-EX.
      *    EXIT.

      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       RECUPERA-TRCH-DI-GAR.


      *--> TIPO OPERAZIONE
           SET IDSV0003-SELECT       TO TRUE.

      *--> TIPO OPERAZIONE
           SET IDSV0003-ID       TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.

              INITIALIZE TRCH-DI-GAR

              MOVE TLI-ID-TRCH-DI-GAR
                TO TGA-ID-TRCH-DI-GAR

              MOVE TRCH-DI-GAR          TO IDSV0003-BUFFER-WHERE-COND

              SET IDSV0003-TRATT-X-COMPETENZA TO TRUE

              CALL IDBSTGA0 USING IDSV0003 TRCH-DI-GAR
              ON EXCEPTION
                 MOVE IDBSTGA0
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'ERRORE CALL IDBSTGA0      '
                   TO IDSV0003-DESCRIZ-ERR-DB2
                 SET IDSV0003-INVALID-OPER TO TRUE
              END-CALL

              IF IDSV0003-SUCCESSFUL-RC

                 EVALUATE TRUE

                     WHEN IDSV0003-SUCCESSFUL-SQL

                          PERFORM S1120-VERIFICA-INDEX
                              THRU S1120-EX
                     WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                          SET IDSV0003-INVALID-OPER  TO TRUE
                          MOVE WK-PGM
                            TO IDSV0003-COD-SERVIZIO-BE
                          STRING 'ERRORE LETTURA TABELLA IDBSTGA0 ;'
                                 IDSV0003-RETURN-CODE ';'
                                 IDSV0003-SQLCODE
                                 DELIMITED BY SIZE INTO
                                 IDSV0003-DESCRIZ-ERR-DB2
                          END-STRING

                 END-EVALUATE

              END-IF.


       RECUPERA-TRCH-DI-GAR-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   VERIFICA SE INDEX LINKED
      *----------------------------------------------------------------*
       S1120-VERIFICA-INDEX.

      *--> TIPO OPERAZIONE
           SET IDSV0003-ID                TO TRUE.

      *--> TIPO OPERAZIONE
           SET IDSV0003-SELECT            TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.
           SET IDSV0003-TRATT-X-COMPETENZA TO TRUE


              INITIALIZE GAR

              MOVE TGA-ID-GAR
                TO GRZ-ID-GAR

              MOVE GAR          TO IDSV0003-BUFFER-WHERE-COND

              CALL IDBSGRZ0 USING IDSV0003 GAR
              ON EXCEPTION
                 MOVE IDBSGRZ0
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'ERRORE CALL IDBSGRZ0 FETCH'
                   TO IDSV0003-DESCRIZ-ERR-DB2
                 SET IDSV0003-INVALID-OPER TO TRUE
              END-CALL

              IF IDSV0003-SUCCESSFUL-RC

                 EVALUATE TRUE


                     WHEN IDSV0003-SUCCESSFUL-SQL

                          IF GRZ-TP-INVST =  8
                             SET INDEX-SI TO TRUE
                          END-IF

                          IF GRZ-RAMO-BILA = '3'
                             SET RAMO-III-SI TO TRUE
                          END-IF

                     WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                          SET IDSV0003-INVALID-OPER  TO TRUE
                          MOVE WK-PGM
                            TO IDSV0003-COD-SERVIZIO-BE
                          STRING 'ERRORE LETTURA TABELLA GARANZIA ;'
                                 IDSV0003-RETURN-CODE ';'
                                 IDSV0003-SQLCODE
                                 DELIMITED BY SIZE INTO
                                 IDSV0003-DESCRIZ-ERR-DB2
                          END-STRING

                 END-EVALUATE

              END-IF.

       S1120-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CALCOLO ANNO
      *----------------------------------------------------------------*
       S1150-CALCOLO-ANNO.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
             TO WK-DATA-FINE

      *    IF INDEX-NO
      *       MOVE 20120101
      *         TO WK-DATA-INIZIO-D
      *    ELSE
              MOVE DPOL-DT-DECOR
                TO WK-DATA-INIZIO.
      *    END-IF.

       S1150-CALCOLO-ANNO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CALCOLO IMPORTO
      *----------------------------------------------------------------*
       S1200-CALCOLO-IMPORTO.

           IF LQU-TP-LIQ  = 'CE'
              AND LQU-DT-INI-EFF >= WK-DATA-INIZIO-D
              AND LQU-DT-INI-EFF <= WK-DATA-FINE-D

      *---> LA SOMMATORIA AVVIENE SOLO PER I NON TASSATI
      *---> CIOE' SE TOT-IMP-IS T NULLO O PARI A 0
NF2014*       IF WLQU-TOT-IMP-IS-NULL(IX-TAB-LQU)
NF2014*              EQUAL HIGH-VALUES OR  LOW-VALUES
NF2014*                    OR  SPACES
NF2014*            IF WLQU-TOT-IMP-LRD-LIQTO-NULL(IX-TAB-LQU)
NF2014*                   EQUAL HIGH-VALUES OR  LOW-VALUES
NF2014*                         OR  SPACES
NF2014*                   CONTINUE
NF2014*            ELSE
NF2014*               COMPUTE WK-SOMMA = WK-SOMMA +
NF2014*                       WLQU-TOT-IMP-LRD-LIQTO(IX-TAB-LQU)
NF2014*            END-IF
NF2014*       ELSE
NF2014*          IF WLQU-TOT-IMP-IS(IX-TAB-LQU) EQUAL ZERO
NF2014*            IF WLQU-TOT-IMP-LRD-LIQTO-NULL(IX-TAB-LQU)
NF2014*                   EQUAL HIGH-VALUES OR  LOW-VALUES
NF2014*                         OR  SPACES
NF2014*                   CONTINUE
NF2014*            ELSE
NF2014*               COMPUTE WK-SOMMA = WK-SOMMA +
NF2014*                       WLQU-TOT-IMP-LRD-LIQTO(IX-TAB-LQU)
NF2014*            END-IF
NF2014*          ELSE
NF2014*            CONTINUE
NF2014*          END-IF

NF2014        IF (LQU-TOT-IMP-IS-NULL EQUAL
NF2014                           HIGH-VALUE OR LOW-VALUE OR SPACES) OR
NF2014            LQU-TOT-IMP-IS EQUAL ZEROES

NF2014            IF (LQU-IMPST-SOST-1382011-NULL EQUAL
NF2014                           HIGH-VALUE OR LOW-VALUE OR SPACES) OR
NF2014                LQU-IMPST-SOST-1382011 EQUAL ZEROES

NF2014                IF (LQU-IMPST-SOST-662014-NULL EQUAL
NF2014                           HIGH-VALUE OR LOW-VALUE OR SPACES) OR
NF2014                   LQU-IMPST-SOST-662014 EQUAL ZEROES

NF2014                   IF LQU-TOT-IMP-LRD-LIQTO-NULL
NF2014                          EQUAL HIGH-VALUES OR  LOW-VALUES
NF2014                                            OR  SPACES
NF2014                      CONTINUE
NF2014                   ELSE
NF2014                      COMPUTE WK-SOMMA = WK-SOMMA +
NF2014                              LQU-TOT-IMP-LRD-LIQTO
NF2014                   END-IF

NF2014                END-IF

NF2014            END-IF

NF2014        END-IF

           END-IF.

       S1200-CALCOLO-IMPORTO-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VERIFICA MOVIMENTO
      *----------------------------------------------------------------*
       VERIFICA-MOVIMENTO.

      *--> SE CI TROVIAMO IN FASE DI LIQUIDAZIONE DEVO RECUPERARE LE
      *--> IMMAGINI IN FASE DI COMUNICAZIONE
NEW        MOVE IVVC0213-TIPO-MOVI-ORIG
NEW          TO WS-MOVIMENTO

NEW        IF LIQUI-RISTOT-IND OR LIQUI-RISPAR-POLIND OR
NEW           LIQUI-SCAPOL OR LIQUI-SININD OR LIQUI-RECIND OR
10819         LIQUI-RPP-TAKE-PROFIT
10819X     OR LIQUI-RISTOT-INCAPIENZA
10819X     OR LIQUI-RPP-REDDITO-PROGR
10819      OR LIQUI-RPP-BENEFICIO-CONTR
FNZS2      OR LIQUI-RISTOT-INCAP

      *--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
NEW           PERFORM RECUP-MOVI-COMUN
NEW              THRU RECUP-MOVI-COMUN-EX

              IF COMUN-TROV-SI
                 MOVE IDSV0003-DATA-COMPETENZA
                   TO WK-DATA-CPTZ-RIP

                 MOVE IDSV0003-DATA-INIZIO-EFFETTO
                   TO WK-DATA-EFF-RIP

                 MOVE WK-DATA-CPTZ-PREC
                   TO IDSV0003-DATA-COMPETENZA

                 MOVE WK-DATA-EFF-PREC
                   TO IDSV0003-DATA-INIZIO-EFFETTO
                      IDSV0003-DATA-FINE-EFFETTO
              END-IF
           END-IF.

       VERIFICA-MOVIMENTO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    Recupero il movimento di comunicazione
      *----------------------------------------------------------------*
       RECUP-MOVI-COMUN.

           SET INIT-CUR-MOV               TO TRUE
           SET COMUN-TROV-NO              TO TRUE

           INITIALIZE MOVI

      *--> TIPO OPERAZIONE
           SET IDSV0003-FETCH-FIRST       TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
           SET  NO-ULTIMA-LETTURA         TO TRUE

      *
           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR FINE-CUR-MOV
                      OR COMUN-TROV-SI

              INITIALIZE MOVI
      *
              MOVE IVVC0213-ID-POLIZZA    TO MOV-ID-OGG
              MOVE 'PO'                   TO MOV-TP-OGG

              SET IDSV0003-TRATT-SENZA-STOR   TO TRUE

      *--> LIVELLO OPERAZIONE
              SET IDSV0003-WHERE-CONDITION TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
              SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
              SET  IDSV0003-SUCCESSFUL-SQL TO TRUE

              CALL  LDBS6040   USING IDSV0003 MOVI
                ON EXCEPTION
                   MOVE LDBS6040          TO IDSV0003-COD-SERVIZIO-BE
                   MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                     TO IDSV0003-DESCRIZ-ERR-DB2
                   SET IDSV0003-INVALID-OPER   TO TRUE
              END-CALL


                IF IDSV0003-SUCCESSFUL-RC

                    EVALUATE TRUE
                        WHEN IDSV0003-NOT-FOUND
      *-->    NESSUN DATO IN TABELLA
      *-->     LIQUIDAZIONE CONTESTUALE - UTILIZZO L'AREA DEL
      *-->     BOLLO IN INPUT
                          SET FINE-CUR-MOV TO TRUE

                        WHEN IDSV0003-SUCCESSFUL-SQL
      *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
      *                   MOVE IDSV0003-BUFFER-DATI TO MOVI
      *      TROVO IL MOVIMENTO DI COMUNICAZIONE RISCATTO PARZIALE
                          MOVE MOV-TP-MOVI      TO WS-MOVIMENTO
                          IF COMUN-RISPAR-IND
                          OR RISTO-INDIVI
                          OR VARIA-OPZION
                          OR SINIS-INDIVI
                          OR RECES-INDIVI
10819                     OR RPP-TAKE-PROFIT
10819X                    OR COMUN-RISTOT-INCAPIENZA
10819X                    OR RPP-REDDITO-PROGRAMMATO
10819                     OR RPP-BENEFICIO-CONTR
FNZS2                     OR COMUN-RISTOT-INCAP
                             MOVE MOV-ID-MOVI    TO WK-ID-MOVI-COMUN
                             SET SI-ULTIMA-LETTURA  TO TRUE
                             MOVE MOV-DT-EFF     TO WK-DATA-EFF-PREC
                             COMPUTE WK-DATA-CPTZ-PREC =  MOV-DS-TS-CPTZ
                                                       - 1
                             SET COMUN-TROV-SI   TO TRUE
                             PERFORM CLOSE-MOVI
                                THRU CLOSE-MOVI-EX
                             SET FINE-CUR-MOV   TO TRUE
                          ELSE
                             SET IDSV0003-FETCH-NEXT   TO TRUE
                          END-IF
                        WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                             MOVE LDBS6040
                               TO IDSV0003-COD-SERVIZIO-BE
                             MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                               TO IDSV0003-DESCRIZ-ERR-DB2
                             SET IDSV0003-INVALID-OPER   TO TRUE
                    END-EVALUATE
                 ELSE
      *--> GESTIRE ERRORE
                    MOVE LDBS6040
                      TO IDSV0003-COD-SERVIZIO-BE
                    MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                      TO IDSV0003-DESCRIZ-ERR-DB2
                    SET IDSV0003-INVALID-OPER   TO TRUE
                 END-IF
           END-PERFORM.
           MOVE IDSV0003-TIPO-MOVIMENTO TO WS-MOVIMENTO.

       RECUP-MOVI-COMUN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CHIUDO IL CURSORE SULLA TABELLA MOVIMENTO LDBS6040
      *----------------------------------------------------------------*
       CLOSE-MOVI.

           SET IDSV0003-CLOSE-CURSOR            TO TRUE.

           CALL LDBS6040 USING IDSV0003 MOVI
           ON EXCEPTION
              MOVE LDBS6040
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CALL LDBS6040 CLOSE'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER TO TRUE
           END-CALL.

           IF NOT IDSV0003-SUCCESSFUL-RC
           OR NOT IDSV0003-SUCCESSFUL-SQL
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CLOSE CURSORE LDBS6040'
                TO IDSV0003-DESCRIZ-ERR-DB2
               SET IDSV0003-INVALID-OPER
                TO TRUE
           END-IF.
       CLOSE-MOVI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           MOVE SPACES                     TO IVVC0213-VAL-STR-O.
           MOVE 0                          TO IVVC0213-VAL-PERC-O.
           MOVE WK-SOMMA                   TO IVVC0213-VAL-IMP-O.

           IF COMUN-TROV-SI
              MOVE WK-DATA-CPTZ-RIP
                TO IDSV0003-DATA-COMPETENZA

              MOVE WK-DATA-EFF-RIP
                TO IDSV0003-DATA-INIZIO-EFFETTO
                   IDSV0003-DATA-FINE-EFFETTO
           END-IF
      *
           GOBACK.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      *   COPY LCCV
      *----------------------------------------------------------------*
      *    COPY LCCVLQU3  REPLACING ==(SF)== BY ==WLQU==.
      *    COPY LCCVLQU4  REPLACING ==(SF)== BY ==WLQU==.



       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LDBS1350 IS INITIAL.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN.  .
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         :
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

      *------------------------------------------------------------
      * COMODO
      *------------------------------------------------------------

       77 DESCRIZ-ERR-DB2         PIC X(300)  VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


       01 WK-ID-ADES-DA           PIC S9(9)V     COMP-3.
       01 WK-ID-ADES-A            PIC S9(9)V     COMP-3.

       01 WK-ID-GAR-DA            PIC S9(9)V     COMP-3.
       01 WK-ID-GAR-A             PIC S9(9)V     COMP-3.



      *------------------------------------------------------------
      * FLAGS
      *------------------------------------------------------------

       01 FLAG-STATO              PIC X(01).
          88 NOT-STATO            VALUE 'N'.
          88 STATO                VALUE 'S'.

       01 FLAG-CAUSALE            PIC X(01).
          88 NOT-CAUSALE          VALUE 'N'.
          88 CAUSALE              VALUE 'S'.



      *------------------------------------------------------------
      * COSTANTI
      *------------------------------------------------------------

       01 NEGAZIONE               PIC X(03) VALUE 'NOT'.

      *------------------------------------------------------------
      * COPY
      *------------------------------------------------------------


           EXEC SQL INCLUDE IDSV0010 END-EXEC.

           EXEC SQL INCLUDE LDBV1351 END-EXEC.


      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDGRZ0 END-EXEC.
           EXEC SQL INCLUDE IDBVGRZ2 END-EXEC.
           EXEC SQL INCLUDE IDBVGRZ3 END-EXEC.

           EXEC SQL INCLUDE IDBDSTB0 END-EXEC.
           EXEC SQL INCLUDE IDBVSTB1 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVGRZ1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 GAR.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE ALSO TRUE
                    WHEN STATO ALSO CAUSALE
                         PERFORM A400-ELABORA-EFF       THRU A400-EX
                    WHEN NOT-STATO ALSO CAUSALE
                         PERFORM C400-ELABORA-EFF-NS    THRU C400-EX
                    WHEN STATO ALSO NOT-CAUSALE
                         PERFORM E400-ELABORA-EFF-NC    THRU E400-EX
                    WHEN NOT-STATO ALSO NOT-CAUSALE
                         PERFORM G400-ELABORA-EFF-NS-NC THRU G400-EX
                  END-EVALUATE
              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                 EVALUATE TRUE ALSO TRUE
                    WHEN STATO ALSO CAUSALE
                         PERFORM B400-ELABORA-CPZ       THRU B400-EX
                    WHEN NOT-STATO ALSO CAUSALE
                         PERFORM D400-ELABORA-CPZ-NS    THRU D400-EX
                    WHEN STATO ALSO NOT-CAUSALE
                         PERFORM F400-ELABORA-CPZ-NC    THRU F400-EX
                    WHEN NOT-STATO ALSO NOT-CAUSALE
                         PERFORM H400-ELABORA-CPZ-NS-NC THRU H400-EX
                  END-EVALUATE

                ELSE

                  SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

               END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'LDBS1350'               TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'GAR-STB'                TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           MOVE 000000000                TO WK-ID-ADES-DA
                                            WK-ID-GAR-DA
           MOVE 999999999                TO WK-ID-ADES-A
                                            WK-ID-GAR-A

           SET STATO                     TO TRUE
           SET CAUSALE                   TO TRUE

           MOVE IDSV0003-BUFFER-WHERE-COND TO LDBV1351

           PERFORM V010-VERIFICA-WHERE-COND THRU V010-EX

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.


       V010-VERIFICA-WHERE-COND.


           IF GRZ-ID-ADES IS NUMERIC AND
              GRZ-ID-ADES NOT = ZERO
              MOVE GRZ-ID-ADES      TO WK-ID-ADES-DA
                                       WK-ID-ADES-A
           END-IF.

           IF GRZ-ID-GAR IS NUMERIC AND
              GRZ-ID-GAR NOT = ZERO
              MOVE GRZ-ID-GAR       TO WK-ID-GAR-DA
                                       WK-ID-GAR-A
           END-IF.

           PERFORM V020-VERIF-OPERATORI-LOGICI THRU V020-EX.


       V010-EX.
           EXIT.

       V020-VERIF-OPERATORI-LOGICI.

           IF LDBV1351-OPER-LOG-STAT-BUS = NEGAZIONE
              SET NOT-STATO               TO TRUE
           END-IF


           IF LDBV1351-OPER-LOG-CAUS = NEGAZIONE
              SET NOT-CAUSALE             TO TRUE
           END-IF.


       V020-EX.
           EXIT.


       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.

       A400-ELABORA-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-WHERE-CONDITION
                 PERFORM A450-WHERE-CONDITION-EFF THRU A450-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-EFF     THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-EFF    THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-EFF     THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-EFF      THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER        TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.

       B400-ELABORA-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-WHERE-CONDITION
                 PERFORM B450-WHERE-CONDITION-CPZ THRU B450-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-CPZ     THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-CPZ    THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-CPZ     THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-CPZ      THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER        TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       C400-ELABORA-EFF-NS.
           EVALUATE TRUE
              WHEN IDSV0003-WHERE-CONDITION
                 PERFORM C450-WHERE-CONDITION-EFF-NS THRU C450-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM C460-OPEN-CURSOR-EFF-NS     THRU C460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM C470-CLOSE-CURSOR-EFF-NS    THRU C470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM C480-FETCH-FIRST-EFF-NS     THRU C480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM C490-FETCH-NEXT-EFF-NS      THRU C490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER        TO TRUE
           END-EVALUATE.
       C400-EX.
           EXIT.

       D400-ELABORA-CPZ-NS.
           EVALUATE TRUE
              WHEN IDSV0003-WHERE-CONDITION
                 PERFORM D450-WHERE-CONDITION-CPZ-NS THRU D450-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM D460-OPEN-CURSOR-CPZ-NS     THRU D460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM D470-CLOSE-CURSOR-CPZ-NS    THRU D470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM D480-FETCH-FIRST-CPZ-NS     THRU D480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM D490-FETCH-NEXT-CPZ-NS      THRU D490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER        TO TRUE
           END-EVALUATE.
       D400-EX.
           EXIT.

       E400-ELABORA-EFF-NC.
           EVALUATE TRUE
              WHEN IDSV0003-WHERE-CONDITION
                 PERFORM E450-WHERE-CONDITION-EFF-NC THRU E450-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM E460-OPEN-CURSOR-EFF-NC     THRU E460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM E470-CLOSE-CURSOR-EFF-NC    THRU E470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM E480-FETCH-FIRST-EFF-NC     THRU E480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM E490-FETCH-NEXT-EFF-NC      THRU E490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER        TO TRUE
           END-EVALUATE.
       E400-EX.
           EXIT.

       F400-ELABORA-CPZ-NC.
           EVALUATE TRUE
              WHEN IDSV0003-WHERE-CONDITION
                 PERFORM F450-WHERE-CONDITION-CPZ-NC THRU F450-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM F460-OPEN-CURSOR-CPZ-NC     THRU F460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM F470-CLOSE-CURSOR-CPZ-NC    THRU F470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM F480-FETCH-FIRST-CPZ-NC     THRU F480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM F490-FETCH-NEXT-CPZ-NC      THRU F490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER        TO TRUE
           END-EVALUATE.
       F400-EX.
           EXIT.

       G400-ELABORA-EFF-NS-NC.
           EVALUATE TRUE
              WHEN IDSV0003-WHERE-CONDITION
                 PERFORM G450-WHERE-CONDITION-EFF-NS-NC THRU G450-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM G460-OPEN-CURSOR-EFF-NS-NC     THRU G460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM G470-CLOSE-CURSOR-EFF-NS-NC    THRU G470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM G480-FETCH-FIRST-EFF-NS-NC     THRU G480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM G490-FETCH-NEXT-EFF-NS-NC      THRU G490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER        TO TRUE
           END-EVALUATE.
       G400-EX.
           EXIT.

       H400-ELABORA-CPZ-NS-NC.
           EVALUATE TRUE
              WHEN IDSV0003-WHERE-CONDITION
                 PERFORM H450-WHERE-CONDITION-CPZ-NS-NC THRU H450-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM H460-OPEN-CURSOR-CPZ-NS-NC     THRU H460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM H470-CLOSE-CURSOR-CPZ-NS-NC    THRU H470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM H480-FETCH-FIRST-CPZ-NS-NC     THRU H480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM H490-FETCH-NEXT-CPZ-NS-NC      THRU H490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER        TO TRUE
           END-EVALUATE.
       H400-EX.
           EXIT.

      *----
      *----  gestione Effetto STATO / CAUSALE
      *----
       A405-DECLARE-CURSOR-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-EFF-GRZ CURSOR FOR
              SELECT
                 A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.IB_OGG
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.COD_SEZ
                ,A.COD_TARI
                ,A.RAMO_BILA
                ,A.DT_INI_VAL_TAR
                ,A.ID_1O_ASSTO
                ,A.ID_2O_ASSTO
                ,A.ID_3O_ASSTO
                ,A.TP_GAR
                ,A.TP_RSH
                ,A.TP_INVST
                ,A.MOD_PAG_GARCOL
                ,A.TP_PER_PRE
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.TP_EMIS_PUR
                ,A.ETA_A_SCAD
                ,A.TP_CALC_PRE_PRSTZ
                ,A.TP_PRE
                ,A.TP_DUR
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.NUM_AA_PAG_PRE
                ,A.AA_PAG_PRE_UNI
                ,A.MM_PAG_PRE_UNI
                ,A.FRAZ_INI_EROG_REN
                ,A.MM_1O_RAT
                ,A.PC_1O_RAT
                ,A.TP_PRSTZ_ASSTA
                ,A.DT_END_CARZ
                ,A.PC_RIP_PRE
                ,A.COD_FND
                ,A.AA_REN_CER
                ,A.PC_REVRSB
                ,A.TP_PC_RIP
                ,A.PC_OPZ
                ,A.TP_IAS
                ,A.TP_STAB
                ,A.TP_ADEG_PRE
                ,A.DT_VARZ_TP_IAS
                ,A.FRAZ_DECR_CPT
                ,A.COD_TRAT_RIASS
                ,A.TP_DT_EMIS_RIASS
                ,A.TP_CESS_RIASS
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.AA_STAB
                ,A.TS_STAB_LIMITATA
                ,A.DT_PRESC
                ,A.RSH_INVST
                ,A.TP_RAMO_BILA
              FROM GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI       = :GRZ-ID-POLI
                    AND A.ID_ADES BETWEEN
                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
                    AND A.ID_GAR BETWEEN
                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.ID_MOVI_CHIU IS NULL

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.ID_MOVI_CHIU IS NULL

                    AND B.TP_OGG      = 'GA'
                    AND B.ID_OGG      = A.ID_GAR
                    AND B.TP_STAT_BUS IN (
                                         :LDBV1351-TP-STAT-BUS-01,
                                         :LDBV1351-TP-STAT-BUS-02,
                                         :LDBV1351-TP-STAT-BUS-03,
                                         :LDBV1351-TP-STAT-BUS-04,
                                         :LDBV1351-TP-STAT-BUS-05,
                                         :LDBV1351-TP-STAT-BUS-06,
                                         :LDBV1351-TP-STAT-BUS-07
                                         )
                    AND B.TP_CAUS     IN (
                                         :LDBV1351-TP-CAUS-01,
                                         :LDBV1351-TP-CAUS-02,
                                         :LDBV1351-TP-CAUS-03,
                                         :LDBV1351-TP-CAUS-04,
                                         :LDBV1351-TP-CAUS-05,
                                         :LDBV1351-TP-CAUS-06,
                                         :LDBV1351-TP-CAUS-07
                                         )
              ORDER BY A.ID_GAR ASC
           END-EXEC.
           CONTINUE.
       A405-EX.
           EXIT.

       A450-WHERE-CONDITION-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
             SELECT
                 A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.IB_OGG
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.COD_SEZ
                ,A.COD_TARI
                ,A.RAMO_BILA
                ,A.DT_INI_VAL_TAR
                ,A.ID_1O_ASSTO
                ,A.ID_2O_ASSTO
                ,A.ID_3O_ASSTO
                ,A.TP_GAR
                ,A.TP_RSH
                ,A.TP_INVST
                ,A.MOD_PAG_GARCOL
                ,A.TP_PER_PRE
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.TP_EMIS_PUR
                ,A.ETA_A_SCAD
                ,A.TP_CALC_PRE_PRSTZ
                ,A.TP_PRE
                ,A.TP_DUR
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.NUM_AA_PAG_PRE
                ,A.AA_PAG_PRE_UNI
                ,A.MM_PAG_PRE_UNI
                ,A.FRAZ_INI_EROG_REN
                ,A.MM_1O_RAT
                ,A.PC_1O_RAT
                ,A.TP_PRSTZ_ASSTA
                ,A.DT_END_CARZ
                ,A.PC_RIP_PRE
                ,A.COD_FND
                ,A.AA_REN_CER
                ,A.PC_REVRSB
                ,A.TP_PC_RIP
                ,A.PC_OPZ
                ,A.TP_IAS
                ,A.TP_STAB
                ,A.TP_ADEG_PRE
                ,A.DT_VARZ_TP_IAS
                ,A.FRAZ_DECR_CPT
                ,A.COD_TRAT_RIASS
                ,A.TP_DT_EMIS_RIASS
                ,A.TP_CESS_RIASS
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.AA_STAB
                ,A.TS_STAB_LIMITATA
                ,A.DT_PRESC
                ,A.RSH_INVST
                ,A.TP_RAMO_BILA
             INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
              FROM GAR A, STAT_OGG_BUS B
              WHERE     A.COD_TARI = :GRZ-COD-TARI
                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.ID_MOVI_CHIU IS NULL

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.ID_MOVI_CHIU IS NULL

                    AND B.TP_OGG      = 'GA'
                    AND B.ID_OGG      = A.ID_GAR
                    AND B.TP_STAT_BUS IN (
                                         :LDBV1351-TP-STAT-BUS-01,
                                         :LDBV1351-TP-STAT-BUS-02,
                                         :LDBV1351-TP-STAT-BUS-03,
                                         :LDBV1351-TP-STAT-BUS-04,
                                         :LDBV1351-TP-STAT-BUS-05,
                                         :LDBV1351-TP-STAT-BUS-06,
                                         :LDBV1351-TP-STAT-BUS-07
                                         )

                    AND B.TP_CAUS     IN (
                                         :LDBV1351-TP-CAUS-01,
                                         :LDBV1351-TP-CAUS-02,
                                         :LDBV1351-TP-CAUS-03,
                                         :LDBV1351-TP-CAUS-04,
                                         :LDBV1351-TP-CAUS-05,
                                         :LDBV1351-TP-CAUS-06,
                                         :LDBV1351-TP-CAUS-07
                                         )

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A450-EX.
           EXIT.

       A460-OPEN-CURSOR-EFF.

           PERFORM A405-DECLARE-CURSOR-EFF THRU A405-EX.

           EXEC SQL
                OPEN C-EFF-GRZ
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-EFF.
           EXEC SQL
                CLOSE C-EFF-GRZ
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-EFF.
           PERFORM A460-OPEN-CURSOR-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-EFF.
           EXEC SQL
                FETCH C-EFF-GRZ
           INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A470-CLOSE-CURSOR-EFF THRU A470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A490-EX.
           EXIT.


      *----
      *----  gestione Competenza STATO / CAUSALE
      *----
       B405-DECLARE-CURSOR-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-CPZ-GRZ CURSOR FOR
              SELECT
                 A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.IB_OGG
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.COD_SEZ
                ,A.COD_TARI
                ,A.RAMO_BILA
                ,A.DT_INI_VAL_TAR
                ,A.ID_1O_ASSTO
                ,A.ID_2O_ASSTO
                ,A.ID_3O_ASSTO
                ,A.TP_GAR
                ,A.TP_RSH
                ,A.TP_INVST
                ,A.MOD_PAG_GARCOL
                ,A.TP_PER_PRE
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.TP_EMIS_PUR
                ,A.ETA_A_SCAD
                ,A.TP_CALC_PRE_PRSTZ
                ,A.TP_PRE
                ,A.TP_DUR
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.NUM_AA_PAG_PRE
                ,A.AA_PAG_PRE_UNI
                ,A.MM_PAG_PRE_UNI
                ,A.FRAZ_INI_EROG_REN
                ,A.MM_1O_RAT
                ,A.PC_1O_RAT
                ,A.TP_PRSTZ_ASSTA
                ,A.DT_END_CARZ
                ,A.PC_RIP_PRE
                ,A.COD_FND
                ,A.AA_REN_CER
                ,A.PC_REVRSB
                ,A.TP_PC_RIP
                ,A.PC_OPZ
                ,A.TP_IAS
                ,A.TP_STAB
                ,A.TP_ADEG_PRE
                ,A.DT_VARZ_TP_IAS
                ,A.FRAZ_DECR_CPT
                ,A.COD_TRAT_RIASS
                ,A.TP_DT_EMIS_RIASS
                ,A.TP_CESS_RIASS
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.AA_STAB
                ,A.TS_STAB_LIMITATA
                ,A.DT_PRESC
                ,A.RSH_INVST
                ,A.TP_RAMO_BILA
              FROM GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :GRZ-ID-POLI
                    AND A.ID_ADES BETWEEN
                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
                    AND A.ID_GAR BETWEEN
                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND A.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND A.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND B.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.TP_OGG      = 'GA'
                    AND B.ID_OGG      = A.ID_GAR
                    AND B.TP_STAT_BUS IN (
                                         :LDBV1351-TP-STAT-BUS-01,
                                         :LDBV1351-TP-STAT-BUS-02,
                                         :LDBV1351-TP-STAT-BUS-03,
                                         :LDBV1351-TP-STAT-BUS-04,
                                         :LDBV1351-TP-STAT-BUS-05,
                                         :LDBV1351-TP-STAT-BUS-06,
                                         :LDBV1351-TP-STAT-BUS-07
                                         )
                    AND B.TP_CAUS     IN (
                                         :LDBV1351-TP-CAUS-01,
                                         :LDBV1351-TP-CAUS-02,
                                         :LDBV1351-TP-CAUS-03,
                                         :LDBV1351-TP-CAUS-04,
                                         :LDBV1351-TP-CAUS-05,
                                         :LDBV1351-TP-CAUS-06,
                                         :LDBV1351-TP-CAUS-07
                                         )
              ORDER BY A.ID_GAR ASC
           END-EXEC.
           CONTINUE.
       B405-EX.
           EXIT.


       B450-WHERE-CONDITION-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
             SELECT
                 A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.IB_OGG
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.COD_SEZ
                ,A.COD_TARI
                ,A.RAMO_BILA
                ,A.DT_INI_VAL_TAR
                ,A.ID_1O_ASSTO
                ,A.ID_2O_ASSTO
                ,A.ID_3O_ASSTO
                ,A.TP_GAR
                ,A.TP_RSH
                ,A.TP_INVST
                ,A.MOD_PAG_GARCOL
                ,A.TP_PER_PRE
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.TP_EMIS_PUR
                ,A.ETA_A_SCAD
                ,A.TP_CALC_PRE_PRSTZ
                ,A.TP_PRE
                ,A.TP_DUR
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.NUM_AA_PAG_PRE
                ,A.AA_PAG_PRE_UNI
                ,A.MM_PAG_PRE_UNI
                ,A.FRAZ_INI_EROG_REN
                ,A.MM_1O_RAT
                ,A.PC_1O_RAT
                ,A.TP_PRSTZ_ASSTA
                ,A.DT_END_CARZ
                ,A.PC_RIP_PRE
                ,A.COD_FND
                ,A.AA_REN_CER
                ,A.PC_REVRSB
                ,A.TP_PC_RIP
                ,A.PC_OPZ
                ,A.TP_IAS
                ,A.TP_STAB
                ,A.TP_ADEG_PRE
                ,A.DT_VARZ_TP_IAS
                ,A.FRAZ_DECR_CPT
                ,A.COD_TRAT_RIASS
                ,A.TP_DT_EMIS_RIASS
                ,A.TP_CESS_RIASS
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.AA_STAB
                ,A.TS_STAB_LIMITATA
                ,A.DT_PRESC
                ,A.RSH_INVST
                ,A.TP_RAMO_BILA
             INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
                ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
              FROM GAR A, STAT_OGG_BUS B
              WHERE     A.COD_TARI = :GRZ-COD-TARI

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND A.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND A.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND B.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.TP_OGG      = 'GA'
                    AND B.ID_OGG      = A.ID_GAR
                    AND B.TP_STAT_BUS IN (
                                         :LDBV1351-TP-STAT-BUS-01,
                                         :LDBV1351-TP-STAT-BUS-02,
                                         :LDBV1351-TP-STAT-BUS-03,
                                         :LDBV1351-TP-STAT-BUS-04,
                                         :LDBV1351-TP-STAT-BUS-05,
                                         :LDBV1351-TP-STAT-BUS-06,
                                         :LDBV1351-TP-STAT-BUS-07
                                         )
                    AND B.TP_CAUS     IN (
                                         :LDBV1351-TP-CAUS-01,
                                         :LDBV1351-TP-CAUS-02,
                                         :LDBV1351-TP-CAUS-03,
                                         :LDBV1351-TP-CAUS-04,
                                         :LDBV1351-TP-CAUS-05,
                                         :LDBV1351-TP-CAUS-06,
                                         :LDBV1351-TP-CAUS-07
                                         )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B450-EX.
           EXIT.

       B460-OPEN-CURSOR-CPZ.

           PERFORM B405-DECLARE-CURSOR-CPZ THRU B405-EX.

           EXEC SQL
                OPEN C-CPZ-GRZ
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-CPZ.
           EXEC SQL
                CLOSE C-CPZ-GRZ
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-CPZ.
           PERFORM B460-OPEN-CURSOR-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-CPZ.
           EXEC SQL
                FETCH C-CPZ-GRZ
           INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B470-CLOSE-CURSOR-CPZ THRU B470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B490-EX.
           EXIT.

      *----
      *----  gestione Effetto NOT STATO / CAUS
      *----
       C405-DECLARE-CURSOR-EFF-NS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-EFF-GRZ-NS CURSOR FOR
              SELECT
                 A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.IB_OGG
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.COD_SEZ
                ,A.COD_TARI
                ,A.RAMO_BILA
                ,A.DT_INI_VAL_TAR
                ,A.ID_1O_ASSTO
                ,A.ID_2O_ASSTO
                ,A.ID_3O_ASSTO
                ,A.TP_GAR
                ,A.TP_RSH
                ,A.TP_INVST
                ,A.MOD_PAG_GARCOL
                ,A.TP_PER_PRE
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.TP_EMIS_PUR
                ,A.ETA_A_SCAD
                ,A.TP_CALC_PRE_PRSTZ
                ,A.TP_PRE
                ,A.TP_DUR
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.NUM_AA_PAG_PRE
                ,A.AA_PAG_PRE_UNI
                ,A.MM_PAG_PRE_UNI
                ,A.FRAZ_INI_EROG_REN
                ,A.MM_1O_RAT
                ,A.PC_1O_RAT
                ,A.TP_PRSTZ_ASSTA
                ,A.DT_END_CARZ
                ,A.PC_RIP_PRE
                ,A.COD_FND
                ,A.AA_REN_CER
                ,A.PC_REVRSB
                ,A.TP_PC_RIP
                ,A.PC_OPZ
                ,A.TP_IAS
                ,A.TP_STAB
                ,A.TP_ADEG_PRE
                ,A.DT_VARZ_TP_IAS
                ,A.FRAZ_DECR_CPT
                ,A.COD_TRAT_RIASS
                ,A.TP_DT_EMIS_RIASS
                ,A.TP_CESS_RIASS
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.AA_STAB
                ,A.TS_STAB_LIMITATA
                ,A.DT_PRESC
                ,A.RSH_INVST
                ,A.TP_RAMO_BILA
              FROM GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI       = :GRZ-ID-POLI
                    AND A.ID_ADES BETWEEN
                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
                    AND A.ID_GAR BETWEEN
                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.ID_MOVI_CHIU IS NULL

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.ID_MOVI_CHIU IS NULL


                    AND B.TP_OGG      = 'GA'
                    AND B.ID_OGG      = A.ID_GAR
                    AND
                    NOT B.TP_STAT_BUS IN (
                                         :LDBV1351-TP-STAT-BUS-01,
                                         :LDBV1351-TP-STAT-BUS-02,
                                         :LDBV1351-TP-STAT-BUS-03,
                                         :LDBV1351-TP-STAT-BUS-04,
                                         :LDBV1351-TP-STAT-BUS-05,
                                         :LDBV1351-TP-STAT-BUS-06,
                                         :LDBV1351-TP-STAT-BUS-07
                                         )
                    AND B.TP_CAUS     IN (
                                         :LDBV1351-TP-CAUS-01,
                                         :LDBV1351-TP-CAUS-02,
                                         :LDBV1351-TP-CAUS-03,
                                         :LDBV1351-TP-CAUS-04,
                                         :LDBV1351-TP-CAUS-05,
                                         :LDBV1351-TP-CAUS-06,
                                         :LDBV1351-TP-CAUS-07
                                         )
              ORDER BY A.ID_GAR ASC
           END-EXEC.
           CONTINUE.
       C405-EX.
           EXIT.

       C450-WHERE-CONDITION-EFF-NS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
             SELECT
                 A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.IB_OGG
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.COD_SEZ
                ,A.COD_TARI
                ,A.RAMO_BILA
                ,A.DT_INI_VAL_TAR
                ,A.ID_1O_ASSTO
                ,A.ID_2O_ASSTO
                ,A.ID_3O_ASSTO
                ,A.TP_GAR
                ,A.TP_RSH
                ,A.TP_INVST
                ,A.MOD_PAG_GARCOL
                ,A.TP_PER_PRE
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.TP_EMIS_PUR
                ,A.ETA_A_SCAD
                ,A.TP_CALC_PRE_PRSTZ
                ,A.TP_PRE
                ,A.TP_DUR
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.NUM_AA_PAG_PRE
                ,A.AA_PAG_PRE_UNI
                ,A.MM_PAG_PRE_UNI
                ,A.FRAZ_INI_EROG_REN
                ,A.MM_1O_RAT
                ,A.PC_1O_RAT
                ,A.TP_PRSTZ_ASSTA
                ,A.DT_END_CARZ
                ,A.PC_RIP_PRE
                ,A.COD_FND
                ,A.AA_REN_CER
                ,A.PC_REVRSB
                ,A.TP_PC_RIP
                ,A.PC_OPZ
                ,A.TP_IAS
                ,A.TP_STAB
                ,A.TP_ADEG_PRE
                ,A.DT_VARZ_TP_IAS
                ,A.FRAZ_DECR_CPT
                ,A.COD_TRAT_RIASS
                ,A.TP_DT_EMIS_RIASS
                ,A.TP_CESS_RIASS
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.AA_STAB
                ,A.TS_STAB_LIMITATA
                ,A.DT_PRESC
                ,A.RSH_INVST
                ,A.TP_RAMO_BILA
             INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
              FROM GAR A, STAT_OGG_BUS B
              WHERE     A.COD_TARI = :GRZ-COD-TARI
                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.ID_MOVI_CHIU IS NULL

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.ID_MOVI_CHIU IS NULL

                    AND B.TP_OGG      = 'GA'
                    AND B.ID_OGG      = A.ID_GAR
                    AND
                    NOT B.TP_STAT_BUS IN (
                                         :LDBV1351-TP-STAT-BUS-01,
                                         :LDBV1351-TP-STAT-BUS-02,
                                         :LDBV1351-TP-STAT-BUS-03,
                                         :LDBV1351-TP-STAT-BUS-04,
                                         :LDBV1351-TP-STAT-BUS-05,
                                         :LDBV1351-TP-STAT-BUS-06,
                                         :LDBV1351-TP-STAT-BUS-07
                                         )
                    AND B.TP_CAUS     IN (
                                         :LDBV1351-TP-CAUS-01,
                                         :LDBV1351-TP-CAUS-02,
                                         :LDBV1351-TP-CAUS-03,
                                         :LDBV1351-TP-CAUS-04,
                                         :LDBV1351-TP-CAUS-05,
                                         :LDBV1351-TP-CAUS-06,
                                         :LDBV1351-TP-CAUS-07
                                         )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       C450-EX.
           EXIT.

       C460-OPEN-CURSOR-EFF-NS.

           PERFORM C405-DECLARE-CURSOR-EFF-NS THRU C405-EX.

           EXEC SQL
                OPEN C-EFF-GRZ-NS
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       C460-EX.
           EXIT.

       C470-CLOSE-CURSOR-EFF-NS.
           EXEC SQL
                CLOSE C-EFF-GRZ-NS
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       C470-EX.
           EXIT.

       C480-FETCH-FIRST-EFF-NS.
           PERFORM C460-OPEN-CURSOR-EFF-NS    THRU C460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM C490-FETCH-NEXT-EFF-NS THRU C490-EX
           END-IF.
       C480-EX.
           EXIT.

       C490-FETCH-NEXT-EFF-NS.
           EXEC SQL
                FETCH C-EFF-GRZ-NS
           INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM C470-CLOSE-CURSOR-EFF-NS THRU C470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       C490-EX.
           EXIT.



      *----
      *----  gestione Competenza NOT STATO / CAUSALE
      *----
       D405-DECLARE-CURSOR-CPZ-NS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-CPZ-GRZ-NS CURSOR FOR
              SELECT
                 A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.IB_OGG
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.COD_SEZ
                ,A.COD_TARI
                ,A.RAMO_BILA
                ,A.DT_INI_VAL_TAR
                ,A.ID_1O_ASSTO
                ,A.ID_2O_ASSTO
                ,A.ID_3O_ASSTO
                ,A.TP_GAR
                ,A.TP_RSH
                ,A.TP_INVST
                ,A.MOD_PAG_GARCOL
                ,A.TP_PER_PRE
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.TP_EMIS_PUR
                ,A.ETA_A_SCAD
                ,A.TP_CALC_PRE_PRSTZ
                ,A.TP_PRE
                ,A.TP_DUR
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.NUM_AA_PAG_PRE
                ,A.AA_PAG_PRE_UNI
                ,A.MM_PAG_PRE_UNI
                ,A.FRAZ_INI_EROG_REN
                ,A.MM_1O_RAT
                ,A.PC_1O_RAT
                ,A.TP_PRSTZ_ASSTA
                ,A.DT_END_CARZ
                ,A.PC_RIP_PRE
                ,A.COD_FND
                ,A.AA_REN_CER
                ,A.PC_REVRSB
                ,A.TP_PC_RIP
                ,A.PC_OPZ
                ,A.TP_IAS
                ,A.TP_STAB
                ,A.TP_ADEG_PRE
                ,A.DT_VARZ_TP_IAS
                ,A.FRAZ_DECR_CPT
                ,A.COD_TRAT_RIASS
                ,A.TP_DT_EMIS_RIASS
                ,A.TP_CESS_RIASS
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.AA_STAB
                ,A.TS_STAB_LIMITATA
                ,A.DT_PRESC
                ,A.RSH_INVST
                ,A.TP_RAMO_BILA
              FROM GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :GRZ-ID-POLI
                    AND A.ID_ADES BETWEEN
                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
                    AND A.ID_GAR BETWEEN
                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A
                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND A.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND A.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND B.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.TP_OGG      = 'GA'
                    AND B.ID_OGG      = A.ID_GAR
                    AND
                    NOT B.TP_STAT_BUS IN (
                                         :LDBV1351-TP-STAT-BUS-01,
                                         :LDBV1351-TP-STAT-BUS-02,
                                         :LDBV1351-TP-STAT-BUS-03,
                                         :LDBV1351-TP-STAT-BUS-04,
                                         :LDBV1351-TP-STAT-BUS-05,
                                         :LDBV1351-TP-STAT-BUS-06,
                                         :LDBV1351-TP-STAT-BUS-07
                                         )
                    AND B.TP_CAUS     IN (
                                         :LDBV1351-TP-CAUS-01,
                                         :LDBV1351-TP-CAUS-02,
                                         :LDBV1351-TP-CAUS-03,
                                         :LDBV1351-TP-CAUS-04,
                                         :LDBV1351-TP-CAUS-05,
                                         :LDBV1351-TP-CAUS-06,
                                         :LDBV1351-TP-CAUS-07
                                         )
              ORDER BY A.ID_GAR ASC
           END-EXEC.
           CONTINUE.
       D405-EX.
           EXIT.


       D450-WHERE-CONDITION-CPZ-NS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
             SELECT
                 A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.IB_OGG
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.COD_SEZ
                ,A.COD_TARI
                ,A.RAMO_BILA
                ,A.DT_INI_VAL_TAR
                ,A.ID_1O_ASSTO
                ,A.ID_2O_ASSTO
                ,A.ID_3O_ASSTO
                ,A.TP_GAR
                ,A.TP_RSH
                ,A.TP_INVST
                ,A.MOD_PAG_GARCOL
                ,A.TP_PER_PRE
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.TP_EMIS_PUR
                ,A.ETA_A_SCAD
                ,A.TP_CALC_PRE_PRSTZ
                ,A.TP_PRE
                ,A.TP_DUR
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.NUM_AA_PAG_PRE
                ,A.AA_PAG_PRE_UNI
                ,A.MM_PAG_PRE_UNI
                ,A.FRAZ_INI_EROG_REN
                ,A.MM_1O_RAT
                ,A.PC_1O_RAT
                ,A.TP_PRSTZ_ASSTA
                ,A.DT_END_CARZ
                ,A.PC_RIP_PRE
                ,A.COD_FND
                ,A.AA_REN_CER
                ,A.PC_REVRSB
                ,A.TP_PC_RIP
                ,A.PC_OPZ
                ,A.TP_IAS
                ,A.TP_STAB
                ,A.TP_ADEG_PRE
                ,A.DT_VARZ_TP_IAS
                ,A.FRAZ_DECR_CPT
                ,A.COD_TRAT_RIASS
                ,A.TP_DT_EMIS_RIASS
                ,A.TP_CESS_RIASS
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.AA_STAB
                ,A.TS_STAB_LIMITATA
                ,A.DT_PRESC
                ,A.RSH_INVST
                ,A.TP_RAMO_BILA
             INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
              FROM GAR A, STAT_OGG_BUS B
              WHERE     A.COD_TARI = :GRZ-COD-TARI
                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND A.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND A.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND B.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.TP_OGG      = 'GA'
                    AND B.ID_OGG      = A.ID_GAR
                    AND
                    NOT B.TP_STAT_BUS IN (
                                         :LDBV1351-TP-STAT-BUS-01,
                                         :LDBV1351-TP-STAT-BUS-02,
                                         :LDBV1351-TP-STAT-BUS-03,
                                         :LDBV1351-TP-STAT-BUS-04,
                                         :LDBV1351-TP-STAT-BUS-05,
                                         :LDBV1351-TP-STAT-BUS-06,
                                         :LDBV1351-TP-STAT-BUS-07
                                         )
                    AND B.TP_CAUS     IN (
                                         :LDBV1351-TP-CAUS-01,
                                         :LDBV1351-TP-CAUS-02,
                                         :LDBV1351-TP-CAUS-03,
                                         :LDBV1351-TP-CAUS-04,
                                         :LDBV1351-TP-CAUS-05,
                                         :LDBV1351-TP-CAUS-06,
                                         :LDBV1351-TP-CAUS-07
                                         )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       D450-EX.
           EXIT.

       D460-OPEN-CURSOR-CPZ-NS.

           PERFORM D405-DECLARE-CURSOR-CPZ-NS THRU D405-EX.

           EXEC SQL
                OPEN C-CPZ-GRZ-NS
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       D460-EX.
           EXIT.

       D470-CLOSE-CURSOR-CPZ-NS.
           EXEC SQL
                CLOSE C-CPZ-GRZ-NS
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       D470-EX.
           EXIT.

       D480-FETCH-FIRST-CPZ-NS.
           PERFORM D460-OPEN-CURSOR-CPZ-NS    THRU D460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM D490-FETCH-NEXT-CPZ-NS THRU D490-EX
           END-IF.
       D480-EX.
           EXIT.

       D490-FETCH-NEXT-CPZ-NS.
           EXEC SQL
                FETCH C-CPZ-GRZ-NS
           INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM D470-CLOSE-CURSOR-CPZ-NS THRU D470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       D490-EX.
           EXIT.


      *----
      *----  gestione Effetto STATO / CAUS NOT
      *----
       E405-DECLARE-CURSOR-EFF-NC.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-EFF-GRZ-NC CURSOR FOR
              SELECT
                 A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.IB_OGG
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.COD_SEZ
                ,A.COD_TARI
                ,A.RAMO_BILA
                ,A.DT_INI_VAL_TAR
                ,A.ID_1O_ASSTO
                ,A.ID_2O_ASSTO
                ,A.ID_3O_ASSTO
                ,A.TP_GAR
                ,A.TP_RSH
                ,A.TP_INVST
                ,A.MOD_PAG_GARCOL
                ,A.TP_PER_PRE
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.TP_EMIS_PUR
                ,A.ETA_A_SCAD
                ,A.TP_CALC_PRE_PRSTZ
                ,A.TP_PRE
                ,A.TP_DUR
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.NUM_AA_PAG_PRE
                ,A.AA_PAG_PRE_UNI
                ,A.MM_PAG_PRE_UNI
                ,A.FRAZ_INI_EROG_REN
                ,A.MM_1O_RAT
                ,A.PC_1O_RAT
                ,A.TP_PRSTZ_ASSTA
                ,A.DT_END_CARZ
                ,A.PC_RIP_PRE
                ,A.COD_FND
                ,A.AA_REN_CER
                ,A.PC_REVRSB
                ,A.TP_PC_RIP
                ,A.PC_OPZ
                ,A.TP_IAS
                ,A.TP_STAB
                ,A.TP_ADEG_PRE
                ,A.DT_VARZ_TP_IAS
                ,A.FRAZ_DECR_CPT
                ,A.COD_TRAT_RIASS
                ,A.TP_DT_EMIS_RIASS
                ,A.TP_CESS_RIASS
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.AA_STAB
                ,A.TS_STAB_LIMITATA
                ,A.DT_PRESC
                ,A.RSH_INVST
                ,A.TP_RAMO_BILA
              FROM GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI       = :GRZ-ID-POLI
                    AND A.ID_ADES BETWEEN
                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
                    AND A.ID_GAR BETWEEN
                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A
                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.ID_MOVI_CHIU IS NULL

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.ID_MOVI_CHIU IS NULL

                    AND B.TP_OGG      = 'GA'
                    AND B.ID_OGG      = A.ID_GAR
                    AND B.TP_STAT_BUS IN (
                                         :LDBV1351-TP-STAT-BUS-01,
                                         :LDBV1351-TP-STAT-BUS-02,
                                         :LDBV1351-TP-STAT-BUS-03,
                                         :LDBV1351-TP-STAT-BUS-04,
                                         :LDBV1351-TP-STAT-BUS-05,
                                         :LDBV1351-TP-STAT-BUS-06,
                                         :LDBV1351-TP-STAT-BUS-07
                                         )
                    AND
                    NOT B.TP_CAUS     IN (
                                         :LDBV1351-TP-CAUS-01,
                                         :LDBV1351-TP-CAUS-02,
                                         :LDBV1351-TP-CAUS-03,
                                         :LDBV1351-TP-CAUS-04,
                                         :LDBV1351-TP-CAUS-05,
                                         :LDBV1351-TP-CAUS-06,
                                         :LDBV1351-TP-CAUS-07
                                         )
              ORDER BY A.ID_GAR ASC
           END-EXEC.
           CONTINUE.
       E405-EX.
           EXIT.

       E450-WHERE-CONDITION-EFF-NC.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
             SELECT
                 A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.IB_OGG
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.COD_SEZ
                ,A.COD_TARI
                ,A.RAMO_BILA
                ,A.DT_INI_VAL_TAR
                ,A.ID_1O_ASSTO
                ,A.ID_2O_ASSTO
                ,A.ID_3O_ASSTO
                ,A.TP_GAR
                ,A.TP_RSH
                ,A.TP_INVST
                ,A.MOD_PAG_GARCOL
                ,A.TP_PER_PRE
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.TP_EMIS_PUR
                ,A.ETA_A_SCAD
                ,A.TP_CALC_PRE_PRSTZ
                ,A.TP_PRE
                ,A.TP_DUR
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.NUM_AA_PAG_PRE
                ,A.AA_PAG_PRE_UNI
                ,A.MM_PAG_PRE_UNI
                ,A.FRAZ_INI_EROG_REN
                ,A.MM_1O_RAT
                ,A.PC_1O_RAT
                ,A.TP_PRSTZ_ASSTA
                ,A.DT_END_CARZ
                ,A.PC_RIP_PRE
                ,A.COD_FND
                ,A.AA_REN_CER
                ,A.PC_REVRSB
                ,A.TP_PC_RIP
                ,A.PC_OPZ
                ,A.TP_IAS
                ,A.TP_STAB
                ,A.TP_ADEG_PRE
                ,A.DT_VARZ_TP_IAS
                ,A.FRAZ_DECR_CPT
                ,A.COD_TRAT_RIASS
                ,A.TP_DT_EMIS_RIASS
                ,A.TP_CESS_RIASS
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.AA_STAB
                ,A.TS_STAB_LIMITATA
                ,A.DT_PRESC
                ,A.RSH_INVST
                ,A.TP_RAMO_BILA
             INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
              FROM GAR A, STAT_OGG_BUS B
              WHERE     A.COD_TARI = :GRZ-COD-TARI
                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.ID_MOVI_CHIU IS NULL

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.ID_MOVI_CHIU IS NULL

                    AND B.TP_OGG      = 'GA'
                    AND B.ID_OGG      = A.ID_GAR
                    AND B.TP_STAT_BUS IN (
                                         :LDBV1351-TP-STAT-BUS-01,
                                         :LDBV1351-TP-STAT-BUS-02,
                                         :LDBV1351-TP-STAT-BUS-03,
                                         :LDBV1351-TP-STAT-BUS-04,
                                         :LDBV1351-TP-STAT-BUS-05,
                                         :LDBV1351-TP-STAT-BUS-06,
                                         :LDBV1351-TP-STAT-BUS-07
                                         )
                    AND
                    NOT B.TP_CAUS     IN (
                                         :LDBV1351-TP-CAUS-01,
                                         :LDBV1351-TP-CAUS-02,
                                         :LDBV1351-TP-CAUS-03,
                                         :LDBV1351-TP-CAUS-04,
                                         :LDBV1351-TP-CAUS-05,
                                         :LDBV1351-TP-CAUS-06,
                                         :LDBV1351-TP-CAUS-07
                                         )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       E450-EX.
           EXIT.

       E460-OPEN-CURSOR-EFF-NC.

           PERFORM E405-DECLARE-CURSOR-EFF-NC THRU E405-EX.

           EXEC SQL
                OPEN C-EFF-GRZ-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       E460-EX.
           EXIT.

       E470-CLOSE-CURSOR-EFF-NC.
           EXEC SQL
                CLOSE C-EFF-GRZ-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       E470-EX.
           EXIT.

       E480-FETCH-FIRST-EFF-NC.
           PERFORM E460-OPEN-CURSOR-EFF-NC    THRU E460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM E490-FETCH-NEXT-EFF-NC THRU E490-EX
           END-IF.
       E480-EX.
           EXIT.

       E490-FETCH-NEXT-EFF-NC.
           EXEC SQL
                FETCH C-EFF-GRZ-NC
           INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM E470-CLOSE-CURSOR-EFF-NC THRU E470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       E490-EX.
           EXIT.



      *----
      *----  gestione Competenza STATO / NOT CAUSALE
      *----
       F405-DECLARE-CURSOR-CPZ-NC.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-CPZ-GRZ-NC CURSOR FOR
              SELECT
                 A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.IB_OGG
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.COD_SEZ
                ,A.COD_TARI
                ,A.RAMO_BILA
                ,A.DT_INI_VAL_TAR
                ,A.ID_1O_ASSTO
                ,A.ID_2O_ASSTO
                ,A.ID_3O_ASSTO
                ,A.TP_GAR
                ,A.TP_RSH
                ,A.TP_INVST
                ,A.MOD_PAG_GARCOL
                ,A.TP_PER_PRE
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.TP_EMIS_PUR
                ,A.ETA_A_SCAD
                ,A.TP_CALC_PRE_PRSTZ
                ,A.TP_PRE
                ,A.TP_DUR
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.NUM_AA_PAG_PRE
                ,A.AA_PAG_PRE_UNI
                ,A.MM_PAG_PRE_UNI
                ,A.FRAZ_INI_EROG_REN
                ,A.MM_1O_RAT
                ,A.PC_1O_RAT
                ,A.TP_PRSTZ_ASSTA
                ,A.DT_END_CARZ
                ,A.PC_RIP_PRE
                ,A.COD_FND
                ,A.AA_REN_CER
                ,A.PC_REVRSB
                ,A.TP_PC_RIP
                ,A.PC_OPZ
                ,A.TP_IAS
                ,A.TP_STAB
                ,A.TP_ADEG_PRE
                ,A.DT_VARZ_TP_IAS
                ,A.FRAZ_DECR_CPT
                ,A.COD_TRAT_RIASS
                ,A.TP_DT_EMIS_RIASS
                ,A.TP_CESS_RIASS
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.AA_STAB
                ,A.TS_STAB_LIMITATA
                ,A.DT_PRESC
                ,A.RSH_INVST
                ,A.TP_RAMO_BILA
              FROM GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :GRZ-ID-POLI
                    AND A.ID_ADES BETWEEN
                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
                    AND A.ID_GAR BETWEEN
                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A
                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND A.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND A.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND B.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.TP_OGG      = 'GA'
                    AND B.ID_OGG      = A.ID_GAR
                    AND B.TP_STAT_BUS IN (
                                         :LDBV1351-TP-STAT-BUS-01,
                                         :LDBV1351-TP-STAT-BUS-02,
                                         :LDBV1351-TP-STAT-BUS-03,
                                         :LDBV1351-TP-STAT-BUS-04,
                                         :LDBV1351-TP-STAT-BUS-05,
                                         :LDBV1351-TP-STAT-BUS-06,
                                         :LDBV1351-TP-STAT-BUS-07
                                         )
                    AND
                    NOT B.TP_CAUS     IN (
                                         :LDBV1351-TP-CAUS-01,
                                         :LDBV1351-TP-CAUS-02,
                                         :LDBV1351-TP-CAUS-03,
                                         :LDBV1351-TP-CAUS-04,
                                         :LDBV1351-TP-CAUS-05,
                                         :LDBV1351-TP-CAUS-06,
                                         :LDBV1351-TP-CAUS-07
                                         )
              ORDER BY A.ID_GAR ASC
           END-EXEC.
           CONTINUE.
       F405-EX.
           EXIT.


       F450-WHERE-CONDITION-CPZ-NC.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
             SELECT
                 A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.IB_OGG
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.COD_SEZ
                ,A.COD_TARI
                ,A.RAMO_BILA
                ,A.DT_INI_VAL_TAR
                ,A.ID_1O_ASSTO
                ,A.ID_2O_ASSTO
                ,A.ID_3O_ASSTO
                ,A.TP_GAR
                ,A.TP_RSH
                ,A.TP_INVST
                ,A.MOD_PAG_GARCOL
                ,A.TP_PER_PRE
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.TP_EMIS_PUR
                ,A.ETA_A_SCAD
                ,A.TP_CALC_PRE_PRSTZ
                ,A.TP_PRE
                ,A.TP_DUR
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.NUM_AA_PAG_PRE
                ,A.AA_PAG_PRE_UNI
                ,A.MM_PAG_PRE_UNI
                ,A.FRAZ_INI_EROG_REN
                ,A.MM_1O_RAT
                ,A.PC_1O_RAT
                ,A.TP_PRSTZ_ASSTA
                ,A.DT_END_CARZ
                ,A.PC_RIP_PRE
                ,A.COD_FND
                ,A.AA_REN_CER
                ,A.PC_REVRSB
                ,A.TP_PC_RIP
                ,A.PC_OPZ
                ,A.TP_IAS
                ,A.TP_STAB
                ,A.TP_ADEG_PRE
                ,A.DT_VARZ_TP_IAS
                ,A.FRAZ_DECR_CPT
                ,A.COD_TRAT_RIASS
                ,A.TP_DT_EMIS_RIASS
                ,A.TP_CESS_RIASS
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.AA_STAB
                ,A.TS_STAB_LIMITATA
                ,A.DT_PRESC
                ,A.RSH_INVST
                ,A.TP_RAMO_BILA
             INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
              FROM GAR A, STAT_OGG_BUS B
              WHERE     A.COD_TARI = :GRZ-COD-TARI
                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND A.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND A.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND B.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.TP_OGG      = 'GA'
                    AND B.ID_OGG      = A.ID_GAR
                    AND B.TP_STAT_BUS IN (
                                         :LDBV1351-TP-STAT-BUS-01,
                                         :LDBV1351-TP-STAT-BUS-02,
                                         :LDBV1351-TP-STAT-BUS-03,
                                         :LDBV1351-TP-STAT-BUS-04,
                                         :LDBV1351-TP-STAT-BUS-05,
                                         :LDBV1351-TP-STAT-BUS-06,
                                         :LDBV1351-TP-STAT-BUS-07
                                         )
                    AND
                    NOT B.TP_CAUS     IN (
                                         :LDBV1351-TP-CAUS-01,
                                         :LDBV1351-TP-CAUS-02,
                                         :LDBV1351-TP-CAUS-03,
                                         :LDBV1351-TP-CAUS-04,
                                         :LDBV1351-TP-CAUS-05,
                                         :LDBV1351-TP-CAUS-06,
                                         :LDBV1351-TP-CAUS-07
                                         )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       F450-EX.
           EXIT.

       F460-OPEN-CURSOR-CPZ-NC.

           PERFORM F405-DECLARE-CURSOR-CPZ-NC THRU F405-EX.

           EXEC SQL
                OPEN C-CPZ-GRZ-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       F460-EX.
           EXIT.

       F470-CLOSE-CURSOR-CPZ-NC.
           EXEC SQL
                CLOSE C-CPZ-GRZ-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       F470-EX.
           EXIT.

       F480-FETCH-FIRST-CPZ-NC.
           PERFORM F460-OPEN-CURSOR-CPZ-NC    THRU F460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM F490-FETCH-NEXT-CPZ-NC THRU F490-EX
           END-IF.
       F480-EX.
           EXIT.

       F490-FETCH-NEXT-CPZ-NC.
           EXEC SQL
                FETCH C-CPZ-GRZ-NC
           INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM F470-CLOSE-CURSOR-CPZ-NC THRU F470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       F490-EX.
           EXIT.


      *----
      *----  gestione Effetto NOT STATO / CAUS NOT
      *----
       G405-DECLARE-CURSOR-EFF-NS-NC.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-EFF-GRZ-NS-NC CURSOR FOR
              SELECT
                 A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.IB_OGG
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.COD_SEZ
                ,A.COD_TARI
                ,A.RAMO_BILA
                ,A.DT_INI_VAL_TAR
                ,A.ID_1O_ASSTO
                ,A.ID_2O_ASSTO
                ,A.ID_3O_ASSTO
                ,A.TP_GAR
                ,A.TP_RSH
                ,A.TP_INVST
                ,A.MOD_PAG_GARCOL
                ,A.TP_PER_PRE
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.TP_EMIS_PUR
                ,A.ETA_A_SCAD
                ,A.TP_CALC_PRE_PRSTZ
                ,A.TP_PRE
                ,A.TP_DUR
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.NUM_AA_PAG_PRE
                ,A.AA_PAG_PRE_UNI
                ,A.MM_PAG_PRE_UNI
                ,A.FRAZ_INI_EROG_REN
                ,A.MM_1O_RAT
                ,A.PC_1O_RAT
                ,A.TP_PRSTZ_ASSTA
                ,A.DT_END_CARZ
                ,A.PC_RIP_PRE
                ,A.COD_FND
                ,A.AA_REN_CER
                ,A.PC_REVRSB
                ,A.TP_PC_RIP
                ,A.PC_OPZ
                ,A.TP_IAS
                ,A.TP_STAB
                ,A.TP_ADEG_PRE
                ,A.DT_VARZ_TP_IAS
                ,A.FRAZ_DECR_CPT
                ,A.COD_TRAT_RIASS
                ,A.TP_DT_EMIS_RIASS
                ,A.TP_CESS_RIASS
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.AA_STAB
                ,A.TS_STAB_LIMITATA
                ,A.DT_PRESC
                ,A.RSH_INVST
                ,A.TP_RAMO_BILA
              FROM GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI       = :GRZ-ID-POLI
                    AND A.ID_ADES BETWEEN
                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
                    AND A.ID_GAR BETWEEN
                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.ID_MOVI_CHIU IS NULL

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.ID_MOVI_CHIU IS NULL

                    AND B.TP_OGG      = 'GA'
                    AND B.ID_OGG      = A.ID_GAR
                    AND
                    NOT B.TP_STAT_BUS IN (
                                         :LDBV1351-TP-STAT-BUS-01,
                                         :LDBV1351-TP-STAT-BUS-02,
                                         :LDBV1351-TP-STAT-BUS-03,
                                         :LDBV1351-TP-STAT-BUS-04,
                                         :LDBV1351-TP-STAT-BUS-05,
                                         :LDBV1351-TP-STAT-BUS-06,
                                         :LDBV1351-TP-STAT-BUS-07
                                         )
                    AND
                    NOT B.TP_CAUS     IN (
                                         :LDBV1351-TP-CAUS-01,
                                         :LDBV1351-TP-CAUS-02,
                                         :LDBV1351-TP-CAUS-03,
                                         :LDBV1351-TP-CAUS-04,
                                         :LDBV1351-TP-CAUS-05,
                                         :LDBV1351-TP-CAUS-06,
                                         :LDBV1351-TP-CAUS-07
                                         )
              ORDER BY A.ID_GAR ASC
           END-EXEC.
           CONTINUE.
       G405-EX.
           EXIT.

       G450-WHERE-CONDITION-EFF-NS-NC.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
             SELECT
                 A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.IB_OGG
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.COD_SEZ
                ,A.COD_TARI
                ,A.RAMO_BILA
                ,A.DT_INI_VAL_TAR
                ,A.ID_1O_ASSTO
                ,A.ID_2O_ASSTO
                ,A.ID_3O_ASSTO
                ,A.TP_GAR
                ,A.TP_RSH
                ,A.TP_INVST
                ,A.MOD_PAG_GARCOL
                ,A.TP_PER_PRE
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.TP_EMIS_PUR
                ,A.ETA_A_SCAD
                ,A.TP_CALC_PRE_PRSTZ
                ,A.TP_PRE
                ,A.TP_DUR
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.NUM_AA_PAG_PRE
                ,A.AA_PAG_PRE_UNI
                ,A.MM_PAG_PRE_UNI
                ,A.FRAZ_INI_EROG_REN
                ,A.MM_1O_RAT
                ,A.PC_1O_RAT
                ,A.TP_PRSTZ_ASSTA
                ,A.DT_END_CARZ
                ,A.PC_RIP_PRE
                ,A.COD_FND
                ,A.AA_REN_CER
                ,A.PC_REVRSB
                ,A.TP_PC_RIP
                ,A.PC_OPZ
                ,A.TP_IAS
                ,A.TP_STAB
                ,A.TP_ADEG_PRE
                ,A.DT_VARZ_TP_IAS
                ,A.FRAZ_DECR_CPT
                ,A.COD_TRAT_RIASS
                ,A.TP_DT_EMIS_RIASS
                ,A.TP_CESS_RIASS
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.AA_STAB
                ,A.TS_STAB_LIMITATA
                ,A.DT_PRESC
                ,A.RSH_INVST
                ,A.TP_RAMO_BILA
             INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
              FROM GAR A, STAT_OGG_BUS B
              WHERE     A.COD_TARI = :GRZ-COD-TARI
                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.ID_MOVI_CHIU IS NULL

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.ID_MOVI_CHIU IS NULL

                    AND B.TP_OGG      = 'GA'
                    AND B.ID_OGG      = A.ID_GAR
                    AND
                    NOT B.TP_STAT_BUS IN (
                                         :LDBV1351-TP-STAT-BUS-01,
                                         :LDBV1351-TP-STAT-BUS-02,
                                         :LDBV1351-TP-STAT-BUS-03,
                                         :LDBV1351-TP-STAT-BUS-04,
                                         :LDBV1351-TP-STAT-BUS-05,
                                         :LDBV1351-TP-STAT-BUS-06,
                                         :LDBV1351-TP-STAT-BUS-07
                                         )
                    AND
                    NOT B.TP_CAUS     IN (
                                         :LDBV1351-TP-CAUS-01,
                                         :LDBV1351-TP-CAUS-02,
                                         :LDBV1351-TP-CAUS-03,
                                         :LDBV1351-TP-CAUS-04,
                                         :LDBV1351-TP-CAUS-05,
                                         :LDBV1351-TP-CAUS-06,
                                         :LDBV1351-TP-CAUS-07
                                         )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       G450-EX.
           EXIT.

       G460-OPEN-CURSOR-EFF-NS-NC.

           PERFORM G405-DECLARE-CURSOR-EFF-NS-NC THRU G405-EX.

           EXEC SQL
                OPEN C-EFF-GRZ-NS-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       G460-EX.
           EXIT.

       G470-CLOSE-CURSOR-EFF-NS-NC.
           EXEC SQL
                CLOSE C-EFF-GRZ-NS-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       G470-EX.
           EXIT.

       G480-FETCH-FIRST-EFF-NS-NC.
           PERFORM G460-OPEN-CURSOR-EFF-NS-NC    THRU G460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM G490-FETCH-NEXT-EFF-NS-NC THRU G490-EX
           END-IF.
       G480-EX.
           EXIT.

       G490-FETCH-NEXT-EFF-NS-NC.
           EXEC SQL
                FETCH C-EFF-GRZ-NS-NC
           INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM G470-CLOSE-CURSOR-EFF-NS-NC THRU G470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       G490-EX.
           EXIT.



      *----
      *----  gestione Competenza NOT STATO / NOT CAUSALE
      *----
       H405-DECLARE-CURSOR-CPZ-NS-NC.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-CPZ-GRZ-NS-NC CURSOR FOR
              SELECT
                 A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.IB_OGG
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.COD_SEZ
                ,A.COD_TARI
                ,A.RAMO_BILA
                ,A.DT_INI_VAL_TAR
                ,A.ID_1O_ASSTO
                ,A.ID_2O_ASSTO
                ,A.ID_3O_ASSTO
                ,A.TP_GAR
                ,A.TP_RSH
                ,A.TP_INVST
                ,A.MOD_PAG_GARCOL
                ,A.TP_PER_PRE
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.TP_EMIS_PUR
                ,A.ETA_A_SCAD
                ,A.TP_CALC_PRE_PRSTZ
                ,A.TP_PRE
                ,A.TP_DUR
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.NUM_AA_PAG_PRE
                ,A.AA_PAG_PRE_UNI
                ,A.MM_PAG_PRE_UNI
                ,A.FRAZ_INI_EROG_REN
                ,A.MM_1O_RAT
                ,A.PC_1O_RAT
                ,A.TP_PRSTZ_ASSTA
                ,A.DT_END_CARZ
                ,A.PC_RIP_PRE
                ,A.COD_FND
                ,A.AA_REN_CER
                ,A.PC_REVRSB
                ,A.TP_PC_RIP
                ,A.PC_OPZ
                ,A.TP_IAS
                ,A.TP_STAB
                ,A.TP_ADEG_PRE
                ,A.DT_VARZ_TP_IAS
                ,A.FRAZ_DECR_CPT
                ,A.COD_TRAT_RIASS
                ,A.TP_DT_EMIS_RIASS
                ,A.TP_CESS_RIASS
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.AA_STAB
                ,A.TS_STAB_LIMITATA
                ,A.DT_PRESC
                ,A.RSH_INVST
                ,A.TP_RAMO_BILA
              FROM GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :GRZ-ID-POLI
                    AND A.ID_ADES BETWEEN
                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
                    AND A.ID_GAR BETWEEN
                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND A.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND A.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND B.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.TP_OGG      = 'GA'
                    AND B.ID_OGG      = A.ID_GAR
                    AND
                    NOT B.TP_STAT_BUS IN (
                                         :LDBV1351-TP-STAT-BUS-01,
                                         :LDBV1351-TP-STAT-BUS-02,
                                         :LDBV1351-TP-STAT-BUS-03,
                                         :LDBV1351-TP-STAT-BUS-04,
                                         :LDBV1351-TP-STAT-BUS-05,
                                         :LDBV1351-TP-STAT-BUS-06,
                                         :LDBV1351-TP-STAT-BUS-07
                                         )
                    AND
                    NOT B.TP_CAUS     IN (
                                         :LDBV1351-TP-CAUS-01,
                                         :LDBV1351-TP-CAUS-02,
                                         :LDBV1351-TP-CAUS-03,
                                         :LDBV1351-TP-CAUS-04,
                                         :LDBV1351-TP-CAUS-05,
                                         :LDBV1351-TP-CAUS-06,
                                         :LDBV1351-TP-CAUS-07
                                         )
              ORDER BY A.ID_GAR ASC
           END-EXEC.
           CONTINUE.
       H405-EX.
           EXIT.


       H450-WHERE-CONDITION-CPZ-NS-NC.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
             SELECT
                 A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.IB_OGG
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.COD_SEZ
                ,A.COD_TARI
                ,A.RAMO_BILA
                ,A.DT_INI_VAL_TAR
                ,A.ID_1O_ASSTO
                ,A.ID_2O_ASSTO
                ,A.ID_3O_ASSTO
                ,A.TP_GAR
                ,A.TP_RSH
                ,A.TP_INVST
                ,A.MOD_PAG_GARCOL
                ,A.TP_PER_PRE
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.TP_EMIS_PUR
                ,A.ETA_A_SCAD
                ,A.TP_CALC_PRE_PRSTZ
                ,A.TP_PRE
                ,A.TP_DUR
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.NUM_AA_PAG_PRE
                ,A.AA_PAG_PRE_UNI
                ,A.MM_PAG_PRE_UNI
                ,A.FRAZ_INI_EROG_REN
                ,A.MM_1O_RAT
                ,A.PC_1O_RAT
                ,A.TP_PRSTZ_ASSTA
                ,A.DT_END_CARZ
                ,A.PC_RIP_PRE
                ,A.COD_FND
                ,A.AA_REN_CER
                ,A.PC_REVRSB
                ,A.TP_PC_RIP
                ,A.PC_OPZ
                ,A.TP_IAS
                ,A.TP_STAB
                ,A.TP_ADEG_PRE
                ,A.DT_VARZ_TP_IAS
                ,A.FRAZ_DECR_CPT
                ,A.COD_TRAT_RIASS
                ,A.TP_DT_EMIS_RIASS
                ,A.TP_CESS_RIASS
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.AA_STAB
                ,A.TS_STAB_LIMITATA
                ,A.DT_PRESC
                ,A.RSH_INVST
                ,A.TP_RAMO_BILA
             INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
              FROM GAR A, STAT_OGG_BUS B
              WHERE     A.COD_TARI = :GRZ-COD-TARI
                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND A.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND A.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND B.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.TP_OGG      = 'GA'
                    AND B.ID_OGG      = A.ID_GAR
                    AND
                    NOT B.TP_STAT_BUS IN (
                                         :LDBV1351-TP-STAT-BUS-01,
                                         :LDBV1351-TP-STAT-BUS-02,
                                         :LDBV1351-TP-STAT-BUS-03,
                                         :LDBV1351-TP-STAT-BUS-04,
                                         :LDBV1351-TP-STAT-BUS-05,
                                         :LDBV1351-TP-STAT-BUS-06,
                                         :LDBV1351-TP-STAT-BUS-07
                                         )
                    AND
                    NOT B.TP_CAUS     IN (
                                         :LDBV1351-TP-CAUS-01,
                                         :LDBV1351-TP-CAUS-02,
                                         :LDBV1351-TP-CAUS-03,
                                         :LDBV1351-TP-CAUS-04,
                                         :LDBV1351-TP-CAUS-05,
                                         :LDBV1351-TP-CAUS-06,
                                         :LDBV1351-TP-CAUS-07
                                         )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       H450-EX.
           EXIT.

       H460-OPEN-CURSOR-CPZ-NS-NC.

           PERFORM H405-DECLARE-CURSOR-CPZ-NS-NC THRU H405-EX.

           EXEC SQL
                OPEN C-CPZ-GRZ-NS-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       H460-EX.
           EXIT.

       H470-CLOSE-CURSOR-CPZ-NS-NC.
           EXEC SQL
                CLOSE C-CPZ-GRZ-NS-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       H470-EX.
           EXIT.

       H480-FETCH-FIRST-CPZ-NS-NC.
           PERFORM H460-OPEN-CURSOR-CPZ-NS-NC    THRU H460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM H490-FETCH-NEXT-CPZ-NS-NC THRU H490-EX
           END-IF.
       H480-EX.
           EXIT.

       H490-FETCH-NEXT-CPZ-NS-NC.
           EXEC SQL
                FETCH C-CPZ-GRZ-NS-NC
           INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM H470-CLOSE-CURSOR-CPZ-NS-NC THRU H470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       H490-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-GRZ-ID-ADES = -1
              MOVE HIGH-VALUES TO GRZ-ID-ADES-NULL
           END-IF
           IF IND-GRZ-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO GRZ-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-GRZ-IB-OGG = -1
              MOVE HIGH-VALUES TO GRZ-IB-OGG-NULL
           END-IF
           IF IND-GRZ-DT-DECOR = -1
              MOVE HIGH-VALUES TO GRZ-DT-DECOR-NULL
           END-IF
           IF IND-GRZ-DT-SCAD = -1
              MOVE HIGH-VALUES TO GRZ-DT-SCAD-NULL
           END-IF
           IF IND-GRZ-COD-SEZ = -1
              MOVE HIGH-VALUES TO GRZ-COD-SEZ-NULL
           END-IF
           IF IND-GRZ-RAMO-BILA = -1
              MOVE HIGH-VALUES TO GRZ-RAMO-BILA-NULL
           END-IF
           IF IND-GRZ-DT-INI-VAL-TAR = -1
              MOVE HIGH-VALUES TO GRZ-DT-INI-VAL-TAR-NULL
           END-IF
           IF IND-GRZ-ID-1O-ASSTO = -1
              MOVE HIGH-VALUES TO GRZ-ID-1O-ASSTO-NULL
           END-IF
           IF IND-GRZ-ID-2O-ASSTO = -1
              MOVE HIGH-VALUES TO GRZ-ID-2O-ASSTO-NULL
           END-IF
           IF IND-GRZ-ID-3O-ASSTO = -1
              MOVE HIGH-VALUES TO GRZ-ID-3O-ASSTO-NULL
           END-IF
           IF IND-GRZ-TP-GAR = -1
              MOVE HIGH-VALUES TO GRZ-TP-GAR-NULL
           END-IF
           IF IND-GRZ-TP-RSH = -1
              MOVE HIGH-VALUES TO GRZ-TP-RSH-NULL
           END-IF
           IF IND-GRZ-TP-INVST = -1
              MOVE HIGH-VALUES TO GRZ-TP-INVST-NULL
           END-IF
           IF IND-GRZ-MOD-PAG-GARCOL = -1
              MOVE HIGH-VALUES TO GRZ-MOD-PAG-GARCOL-NULL
           END-IF
           IF IND-GRZ-TP-PER-PRE = -1
              MOVE HIGH-VALUES TO GRZ-TP-PER-PRE-NULL
           END-IF
           IF IND-GRZ-ETA-AA-1O-ASSTO = -1
              MOVE HIGH-VALUES TO GRZ-ETA-AA-1O-ASSTO-NULL
           END-IF
           IF IND-GRZ-ETA-MM-1O-ASSTO = -1
              MOVE HIGH-VALUES TO GRZ-ETA-MM-1O-ASSTO-NULL
           END-IF
           IF IND-GRZ-ETA-AA-2O-ASSTO = -1
              MOVE HIGH-VALUES TO GRZ-ETA-AA-2O-ASSTO-NULL
           END-IF
           IF IND-GRZ-ETA-MM-2O-ASSTO = -1
              MOVE HIGH-VALUES TO GRZ-ETA-MM-2O-ASSTO-NULL
           END-IF
           IF IND-GRZ-ETA-AA-3O-ASSTO = -1
              MOVE HIGH-VALUES TO GRZ-ETA-AA-3O-ASSTO-NULL
           END-IF
           IF IND-GRZ-ETA-MM-3O-ASSTO = -1
              MOVE HIGH-VALUES TO GRZ-ETA-MM-3O-ASSTO-NULL
           END-IF
           IF IND-GRZ-TP-EMIS-PUR = -1
              MOVE HIGH-VALUES TO GRZ-TP-EMIS-PUR-NULL
           END-IF
           IF IND-GRZ-ETA-A-SCAD = -1
              MOVE HIGH-VALUES TO GRZ-ETA-A-SCAD-NULL
           END-IF
           IF IND-GRZ-TP-CALC-PRE-PRSTZ = -1
              MOVE HIGH-VALUES TO GRZ-TP-CALC-PRE-PRSTZ-NULL
           END-IF
           IF IND-GRZ-TP-PRE = -1
              MOVE HIGH-VALUES TO GRZ-TP-PRE-NULL
           END-IF
           IF IND-GRZ-TP-DUR = -1
              MOVE HIGH-VALUES TO GRZ-TP-DUR-NULL
           END-IF
           IF IND-GRZ-DUR-AA = -1
              MOVE HIGH-VALUES TO GRZ-DUR-AA-NULL
           END-IF
           IF IND-GRZ-DUR-MM = -1
              MOVE HIGH-VALUES TO GRZ-DUR-MM-NULL
           END-IF
           IF IND-GRZ-DUR-GG = -1
              MOVE HIGH-VALUES TO GRZ-DUR-GG-NULL
           END-IF
           IF IND-GRZ-NUM-AA-PAG-PRE = -1
              MOVE HIGH-VALUES TO GRZ-NUM-AA-PAG-PRE-NULL
           END-IF
           IF IND-GRZ-AA-PAG-PRE-UNI = -1
              MOVE HIGH-VALUES TO GRZ-AA-PAG-PRE-UNI-NULL
           END-IF
           IF IND-GRZ-MM-PAG-PRE-UNI = -1
              MOVE HIGH-VALUES TO GRZ-MM-PAG-PRE-UNI-NULL
           END-IF
           IF IND-GRZ-FRAZ-INI-EROG-REN = -1
              MOVE HIGH-VALUES TO GRZ-FRAZ-INI-EROG-REN-NULL
           END-IF
           IF IND-GRZ-MM-1O-RAT = -1
              MOVE HIGH-VALUES TO GRZ-MM-1O-RAT-NULL
           END-IF
           IF IND-GRZ-PC-1O-RAT = -1
              MOVE HIGH-VALUES TO GRZ-PC-1O-RAT-NULL
           END-IF
           IF IND-GRZ-TP-PRSTZ-ASSTA = -1
              MOVE HIGH-VALUES TO GRZ-TP-PRSTZ-ASSTA-NULL
           END-IF
           IF IND-GRZ-DT-END-CARZ = -1
              MOVE HIGH-VALUES TO GRZ-DT-END-CARZ-NULL
           END-IF
           IF IND-GRZ-PC-RIP-PRE = -1
              MOVE HIGH-VALUES TO GRZ-PC-RIP-PRE-NULL
           END-IF
           IF IND-GRZ-COD-FND = -1
              MOVE HIGH-VALUES TO GRZ-COD-FND-NULL
           END-IF
           IF IND-GRZ-AA-REN-CER = -1
              MOVE HIGH-VALUES TO GRZ-AA-REN-CER-NULL
           END-IF
           IF IND-GRZ-PC-REVRSB = -1
              MOVE HIGH-VALUES TO GRZ-PC-REVRSB-NULL
           END-IF
           IF IND-GRZ-TP-PC-RIP = -1
              MOVE HIGH-VALUES TO GRZ-TP-PC-RIP-NULL
           END-IF
           IF IND-GRZ-PC-OPZ = -1
              MOVE HIGH-VALUES TO GRZ-PC-OPZ-NULL
           END-IF
           IF IND-GRZ-TP-IAS = -1
              MOVE HIGH-VALUES TO GRZ-TP-IAS-NULL
           END-IF
           IF IND-GRZ-TP-STAB = -1
              MOVE HIGH-VALUES TO GRZ-TP-STAB-NULL
           END-IF
           IF IND-GRZ-TP-ADEG-PRE = -1
              MOVE HIGH-VALUES TO GRZ-TP-ADEG-PRE-NULL
           END-IF
           IF IND-GRZ-DT-VARZ-TP-IAS = -1
              MOVE HIGH-VALUES TO GRZ-DT-VARZ-TP-IAS-NULL
           END-IF
           IF IND-GRZ-FRAZ-DECR-CPT = -1
              MOVE HIGH-VALUES TO GRZ-FRAZ-DECR-CPT-NULL
           END-IF
           IF IND-GRZ-COD-TRAT-RIASS = -1
              MOVE HIGH-VALUES TO GRZ-COD-TRAT-RIASS-NULL
           END-IF
           IF IND-GRZ-TP-DT-EMIS-RIASS = -1
              MOVE HIGH-VALUES TO GRZ-TP-DT-EMIS-RIASS-NULL
           END-IF
           IF IND-GRZ-TP-CESS-RIASS = -1
              MOVE HIGH-VALUES TO GRZ-TP-CESS-RIASS-NULL
           END-IF
           IF IND-GRZ-AA-STAB = -1
              MOVE HIGH-VALUES TO GRZ-AA-STAB-NULL
           END-IF
           IF IND-GRZ-TS-STAB-LIMITATA = -1
              MOVE HIGH-VALUES TO GRZ-TS-STAB-LIMITATA-NULL
           END-IF
           IF IND-GRZ-DT-PRESC = -1
              MOVE HIGH-VALUES TO GRZ-DT-PRESC-NULL
           END-IF.
           IF IND-GRZ-RSH-INVST = -1
              MOVE HIGH-VALUES TO GRZ-RSH-INVST-NULL
           END-IF.
       Z100-EX.
           EXIT.

      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.

           MOVE GRZ-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO GRZ-DT-INI-EFF
           MOVE GRZ-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO GRZ-DT-END-EFF
           IF IND-GRZ-DT-DECOR = 0
               MOVE GRZ-DT-DECOR-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO GRZ-DT-DECOR
           END-IF
           IF IND-GRZ-DT-SCAD = 0
               MOVE GRZ-DT-SCAD-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO GRZ-DT-SCAD
           END-IF
           IF IND-GRZ-DT-INI-VAL-TAR = 0
               MOVE GRZ-DT-INI-VAL-TAR-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO GRZ-DT-INI-VAL-TAR
           END-IF
           IF IND-GRZ-DT-END-CARZ = 0
               MOVE GRZ-DT-END-CARZ-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO GRZ-DT-END-CARZ
           END-IF
           IF IND-GRZ-DT-VARZ-TP-IAS = 0
               MOVE GRZ-DT-VARZ-TP-IAS-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO GRZ-DT-VARZ-TP-IAS
           END-IF
           IF IND-GRZ-DT-PRESC = 0
               MOVE GRZ-DT-PRESC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO GRZ-DT-PRESC
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.


           CONTINUE.

       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.



       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LDBSC820 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  11 DIC 2017.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2     PIC X(300)  VALUE SPACES.

       77 WS-BUFFER-TABLE     PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ      PIC 9(009)  VALUE ZEROES.

           EXEC SQL INCLUDE IDSV0010 END-EXEC.

      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDADE0 END-EXEC.
           EXEC SQL INCLUDE IDBDPOL0 END-EXEC.
           EXEC SQL INCLUDE IDBDSTB0 END-EXEC.
           EXEC SQL INCLUDE IDBVADE2 END-EXEC.
           EXEC SQL INCLUDE IDBVPOL2 END-EXEC.
           EXEC SQL INCLUDE IDBVSTB2 END-EXEC.
           EXEC SQL INCLUDE IDBVADE3 END-EXEC.
           EXEC SQL INCLUDE IDBVPOL3 END-EXEC.
           EXEC SQL INCLUDE IDBVSTB3 END-EXEC.
      *
           EXEC SQL INCLUDE IDBVADE1 END-EXEC.
           EXEC SQL INCLUDE IDBVPOL1 END-EXEC.
           EXEC SQL INCLUDE IDBVSTB1 END-EXEC.
      *
      *
      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE LDBVC821 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 LDBVC821.


           PERFORM A000-INIZIO              THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-WHERE-CONDITION
                       PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-WHERE-CONDITION
                         PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-WHERE-CONDITION
                           PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                      SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.


           GOBACK.

       A000-INIZIO.
           MOVE 'LDBSC820'              TO   IDSV0003-COD-SERVIZIO-BE.
           MOVE 'LDBVC821' TO   IDSV0003-NOME-TABELLA.

           MOVE '00'                      TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                    TO   IDSV0003-SQLCODE
                                               IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
                                               IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.

       A200-ELABORA-WC-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.

       B200-ELABORA-WC-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B200-EX.
           EXIT.

       C200-ELABORA-WC-NST.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       C200-EX.
           EXIT.
      *----
      *----  gestione WC Effetto
      *----
       A205-DECLARE-CURSOR-WC-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.


           SET IDSV0003-INVALID-OPER TO TRUE.
       A205-EX.
           EXIT.

       A210-SELECT-WC-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           EXEC SQL
             SELECT
                     A.ID_ADES
                    ,A.ID_POLI
                    ,A.ID_MOVI_CRZ
                    ,A.ID_MOVI_CHIU
                    ,A.DT_INI_EFF
                    ,A.DT_END_EFF
                    ,A.IB_PREV
                    ,A.IB_OGG
                    ,A.COD_COMP_ANIA
                    ,A.DT_DECOR
                    ,A.DT_SCAD
                    ,A.ETA_A_SCAD
                    ,A.DUR_AA
                    ,A.DUR_MM
                    ,A.DUR_GG
                    ,A.TP_RGM_FISC
                    ,A.TP_RIAT
                    ,A.TP_MOD_PAG_TIT
                    ,A.TP_IAS
                    ,A.DT_VARZ_TP_IAS
                    ,A.PRE_NET_IND
                    ,A.PRE_LRD_IND
                    ,A.RAT_LRD_IND
                    ,A.PRSTZ_INI_IND
                    ,A.FL_COINC_ASSTO
                    ,A.IB_DFLT
                    ,A.MOD_CALC
                    ,A.TP_FNT_CNBTVA
                    ,A.IMP_AZ
                    ,A.IMP_ADER
                    ,A.IMP_TFR
                    ,A.IMP_VOLO
                    ,A.PC_AZ
                    ,A.PC_ADER
                    ,A.PC_TFR
                    ,A.PC_VOLO
                    ,A.DT_NOVA_RGM_FISC
                    ,A.FL_ATTIV
                    ,A.IMP_REC_RIT_VIS
                    ,A.IMP_REC_RIT_ACC
                    ,A.FL_VARZ_STAT_TBGC
                    ,A.FL_PROVZA_MIGRAZ
                    ,A.IMPB_VIS_DA_REC
                    ,A.DT_DECOR_PREST_BAN
                    ,A.DT_EFF_VARZ_STAT_T
                    ,A.DS_RIGA
                    ,A.DS_OPER_SQL
                    ,A.DS_VER
                    ,A.DS_TS_INI_CPTZ
                    ,A.DS_TS_END_CPTZ
                    ,A.DS_UTENTE
                    ,A.DS_STATO_ELAB
                    ,A.CUM_CNBT_CAP
                    ,A.IMP_GAR_CNBT
                    ,A.DT_ULT_CONS_CNBT
                    ,A.IDEN_ISC_FND
                    ,A.NUM_RAT_PIAN
                    ,A.DT_PRESC
                    ,A.CONCS_PREST
                    ,B.ID_POLI
                    ,B.ID_MOVI_CRZ
                    ,B.ID_MOVI_CHIU
                    ,B.IB_OGG
                    ,B.IB_PROP
                    ,B.DT_PROP
                    ,B.DT_INI_EFF
                    ,B.DT_END_EFF
                    ,B.COD_COMP_ANIA
                    ,B.DT_DECOR
                    ,B.DT_EMIS
                    ,B.TP_POLI
                    ,B.DUR_AA
                    ,B.DUR_MM
                    ,B.DT_SCAD
                    ,B.COD_PROD
                    ,B.DT_INI_VLDT_PROD
                    ,B.COD_CONV
                    ,B.COD_RAMO
                    ,B.DT_INI_VLDT_CONV
                    ,B.DT_APPLZ_CONV
                    ,B.TP_FRM_ASSVA
                    ,B.TP_RGM_FISC
                    ,B.FL_ESTAS
                    ,B.FL_RSH_COMUN
                    ,B.FL_RSH_COMUN_COND
                    ,B.TP_LIV_GENZ_TIT
                    ,B.FL_COP_FINANZ
                    ,B.TP_APPLZ_DIR
                    ,B.SPE_MED
                    ,B.DIR_EMIS
                    ,B.DIR_1O_VERS
                    ,B.DIR_VERS_AGG
                    ,B.COD_DVS
                    ,B.FL_FNT_AZ
                    ,B.FL_FNT_ADER
                    ,B.FL_FNT_TFR
                    ,B.FL_FNT_VOLO
                    ,B.TP_OPZ_A_SCAD
                    ,B.AA_DIFF_PROR_DFLT
                    ,B.FL_VER_PROD
                    ,B.DUR_GG
                    ,B.DIR_QUIET
                    ,B.TP_PTF_ESTNO
                    ,B.FL_CUM_PRE_CNTR
                    ,B.FL_AMMB_MOVI
                    ,B.CONV_GECO
                    ,B.DS_RIGA
                    ,B.DS_OPER_SQL
                    ,B.DS_VER
                    ,B.DS_TS_INI_CPTZ
                    ,B.DS_TS_END_CPTZ
                    ,B.DS_UTENTE
                    ,B.DS_STATO_ELAB
                    ,B.FL_SCUDO_FISC
                    ,B.FL_TRASFE
                    ,B.FL_TFR_STRC
                    ,B.DT_PRESC
                    ,B.COD_CONV_AGG
                    ,B.SUBCAT_PROD
                    ,B.FL_QUEST_ADEGZ_ASS
                    ,B.COD_TPA
                    ,B.ID_ACC_COMM
                    ,B.FL_POLI_CPI_PR
                    ,B.FL_POLI_BUNDLING
                    ,B.IND_POLI_PRIN_COLL
                    ,B.FL_VND_BUNDLE
                    ,B.IB_BS
                    ,B.FL_POLI_IFP
                    ,C.ID_STAT_OGG_BUS
                    ,C.ID_OGG
                    ,C.TP_OGG
                    ,C.ID_MOVI_CRZ
                    ,C.ID_MOVI_CHIU
                    ,C.DT_INI_EFF
                    ,C.DT_END_EFF
                    ,C.COD_COMP_ANIA
                    ,C.TP_STAT_BUS
                    ,C.TP_CAUS
                    ,C.DS_RIGA
                    ,C.DS_OPER_SQL
                    ,C.DS_VER
                    ,C.DS_TS_INI_CPTZ
                    ,C.DS_TS_END_CPTZ
                    ,C.DS_UTENTE
                    ,C.DS_STATO_ELAB
             INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
               ,
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
               ,
                :STB-ID-STAT-OGG-BUS
               ,:STB-ID-OGG
               ,:STB-TP-OGG
               ,:STB-ID-MOVI-CRZ
               ,:STB-ID-MOVI-CHIU
                :IND-STB-ID-MOVI-CHIU
               ,:STB-DT-INI-EFF-DB
               ,:STB-DT-END-EFF-DB
               ,:STB-COD-COMP-ANIA
               ,:STB-TP-STAT-BUS
               ,:STB-TP-CAUS
               ,:STB-DS-RIGA
               ,:STB-DS-OPER-SQL
               ,:STB-DS-VER
               ,:STB-DS-TS-INI-CPTZ
               ,:STB-DS-TS-END-CPTZ
               ,:STB-DS-UTENTE
               ,:STB-DS-STATO-ELAB
             FROM ADES A,
                  POLI B,
                  STAT_OGG_BUS C
             WHERE  B.TP_FRM_ASSVA   IN (:LC821-TP-FRM-ASSVA1,
                                         :LC821-TP-FRM-ASSVA2)
                    AND A.ID_POLI         =   B.ID_POLI
                    AND A.ID_POLI         = :LC821-ID-POLI
                    AND A.ID_ADES         =   C.ID_OGG
                    AND C.TP_OGG          =  'AD'
                    AND C.TP_STAT_BUS     =  'VI'
                    AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
                    AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.ID_MOVI_CHIU IS NULL
                    AND B.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.ID_MOVI_CHIU IS NULL
                    AND C.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND C.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND C.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND C.ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX

           END-IF.
       A210-EX.
           EXIT.

       A260-OPEN-CURSOR-WC-EFF.
           PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A260-EX.
           EXIT.

       A270-CLOSE-CURSOR-WC-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A270-EX.
           EXIT.

       A280-FETCH-FIRST-WC-EFF.
           PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
           END-IF.
       A280-EX.
           EXIT.

       A290-FETCH-NEXT-WC-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A290-EX.
           EXIT.
      *----
      *----  gestione WC Competenza
      *----
       B205-DECLARE-CURSOR-WC-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B205-EX.
           EXIT.

       B210-SELECT-WC-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           EXEC SQL
             SELECT
                     A.ID_ADES
                    ,A.ID_POLI
                    ,A.ID_MOVI_CRZ
                    ,A.ID_MOVI_CHIU
                    ,A.DT_INI_EFF
                    ,A.DT_END_EFF
                    ,A.IB_PREV
                    ,A.IB_OGG
                    ,A.COD_COMP_ANIA
                    ,A.DT_DECOR
                    ,A.DT_SCAD
                    ,A.ETA_A_SCAD
                    ,A.DUR_AA
                    ,A.DUR_MM
                    ,A.DUR_GG
                    ,A.TP_RGM_FISC
                    ,A.TP_RIAT
                    ,A.TP_MOD_PAG_TIT
                    ,A.TP_IAS
                    ,A.DT_VARZ_TP_IAS
                    ,A.PRE_NET_IND
                    ,A.PRE_LRD_IND
                    ,A.RAT_LRD_IND
                    ,A.PRSTZ_INI_IND
                    ,A.FL_COINC_ASSTO
                    ,A.IB_DFLT
                    ,A.MOD_CALC
                    ,A.TP_FNT_CNBTVA
                    ,A.IMP_AZ
                    ,A.IMP_ADER
                    ,A.IMP_TFR
                    ,A.IMP_VOLO
                    ,A.PC_AZ
                    ,A.PC_ADER
                    ,A.PC_TFR
                    ,A.PC_VOLO
                    ,A.DT_NOVA_RGM_FISC
                    ,A.FL_ATTIV
                    ,A.IMP_REC_RIT_VIS
                    ,A.IMP_REC_RIT_ACC
                    ,A.FL_VARZ_STAT_TBGC
                    ,A.FL_PROVZA_MIGRAZ
                    ,A.IMPB_VIS_DA_REC
                    ,A.DT_DECOR_PREST_BAN
                    ,A.DT_EFF_VARZ_STAT_T
                    ,A.DS_RIGA
                    ,A.DS_OPER_SQL
                    ,A.DS_VER
                    ,A.DS_TS_INI_CPTZ
                    ,A.DS_TS_END_CPTZ
                    ,A.DS_UTENTE
                    ,A.DS_STATO_ELAB
                    ,A.CUM_CNBT_CAP
                    ,A.IMP_GAR_CNBT
                    ,A.DT_ULT_CONS_CNBT
                    ,A.IDEN_ISC_FND
                    ,A.NUM_RAT_PIAN
                    ,A.DT_PRESC
                    ,A.CONCS_PREST
                    ,B.ID_POLI
                    ,B.ID_MOVI_CRZ
                    ,B.ID_MOVI_CHIU
                    ,B.IB_OGG
                    ,B.IB_PROP
                    ,B.DT_PROP
                    ,B.DT_INI_EFF
                    ,B.DT_END_EFF
                    ,B.COD_COMP_ANIA
                    ,B.DT_DECOR
                    ,B.DT_EMIS
                    ,B.TP_POLI
                    ,B.DUR_AA
                    ,B.DUR_MM
                    ,B.DT_SCAD
                    ,B.COD_PROD
                    ,B.DT_INI_VLDT_PROD
                    ,B.COD_CONV
                    ,B.COD_RAMO
                    ,B.DT_INI_VLDT_CONV
                    ,B.DT_APPLZ_CONV
                    ,B.TP_FRM_ASSVA
                    ,B.TP_RGM_FISC
                    ,B.FL_ESTAS
                    ,B.FL_RSH_COMUN
                    ,B.FL_RSH_COMUN_COND
                    ,B.TP_LIV_GENZ_TIT
                    ,B.FL_COP_FINANZ
                    ,B.TP_APPLZ_DIR
                    ,B.SPE_MED
                    ,B.DIR_EMIS
                    ,B.DIR_1O_VERS
                    ,B.DIR_VERS_AGG
                    ,B.COD_DVS
                    ,B.FL_FNT_AZ
                    ,B.FL_FNT_ADER
                    ,B.FL_FNT_TFR
                    ,B.FL_FNT_VOLO
                    ,B.TP_OPZ_A_SCAD
                    ,B.AA_DIFF_PROR_DFLT
                    ,B.FL_VER_PROD
                    ,B.DUR_GG
                    ,B.DIR_QUIET
                    ,B.TP_PTF_ESTNO
                    ,B.FL_CUM_PRE_CNTR
                    ,B.FL_AMMB_MOVI
                    ,B.CONV_GECO
                    ,B.DS_RIGA
                    ,B.DS_OPER_SQL
                    ,B.DS_VER
                    ,B.DS_TS_INI_CPTZ
                    ,B.DS_TS_END_CPTZ
                    ,B.DS_UTENTE
                    ,B.DS_STATO_ELAB
                    ,B.FL_SCUDO_FISC
                    ,B.FL_TRASFE
                    ,B.FL_TFR_STRC
                    ,B.DT_PRESC
                    ,B.COD_CONV_AGG
                    ,B.SUBCAT_PROD
                    ,B.FL_QUEST_ADEGZ_ASS
                    ,B.COD_TPA
                    ,B.ID_ACC_COMM
                    ,B.FL_POLI_CPI_PR
                    ,B.FL_POLI_BUNDLING
                    ,B.IND_POLI_PRIN_COLL
                    ,B.FL_VND_BUNDLE
                    ,B.IB_BS
                    ,B.FL_POLI_IFP
                    ,C.ID_STAT_OGG_BUS
                    ,C.ID_OGG
                    ,C.TP_OGG
                    ,C.ID_MOVI_CRZ
                    ,C.ID_MOVI_CHIU
                    ,C.DT_INI_EFF
                    ,C.DT_END_EFF
                    ,C.COD_COMP_ANIA
                    ,C.TP_STAT_BUS
                    ,C.TP_CAUS
                    ,C.DS_RIGA
                    ,C.DS_OPER_SQL
                    ,C.DS_VER
                    ,C.DS_TS_INI_CPTZ
                    ,C.DS_TS_END_CPTZ
                    ,C.DS_UTENTE
                    ,C.DS_STATO_ELAB
             INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
               ,
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
               ,
                :STB-ID-STAT-OGG-BUS
               ,:STB-ID-OGG
               ,:STB-TP-OGG
               ,:STB-ID-MOVI-CRZ
               ,:STB-ID-MOVI-CHIU
                :IND-STB-ID-MOVI-CHIU
               ,:STB-DT-INI-EFF-DB
               ,:STB-DT-END-EFF-DB
               ,:STB-COD-COMP-ANIA
               ,:STB-TP-STAT-BUS
               ,:STB-TP-CAUS
               ,:STB-DS-RIGA
               ,:STB-DS-OPER-SQL
               ,:STB-DS-VER
               ,:STB-DS-TS-INI-CPTZ
               ,:STB-DS-TS-END-CPTZ
               ,:STB-DS-UTENTE
               ,:STB-DS-STATO-ELAB
             FROM ADES A,
                  POLI B,
                  STAT_OGG_BUS C
             WHERE  B.TP_FRM_ASSVA   IN (:LC821-TP-FRM-ASSVA1,
                                         :LC821-TP-FRM-ASSVA2)
                    AND A.ID_POLI         =   B.ID_POLI
                    AND A.ID_POLI         = :LC821-ID-POLI
                    AND A.ID_ADES         =   C.ID_OGG
                    AND C.TP_OGG          =  'AD'
                    AND C.TP_STAT_BUS     =  'VI'
                    AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
                    AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND A.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
                    AND B.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND B.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
                    AND C.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND C.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND C.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND C.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND C.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX

           END-IF.
       B210-EX.
           EXIT.

       B260-OPEN-CURSOR-WC-CPZ.
           PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B260-EX.
           EXIT.

       B270-CLOSE-CURSOR-WC-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B270-EX.
           EXIT.

       B280-FETCH-FIRST-WC-CPZ.
           PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
           END-IF.
       B280-EX.
           EXIT.

       B290-FETCH-NEXT-WC-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B290-EX.
           EXIT.
      *----
      *----  gestione WC Senza Storicit`
      *----
       C205-DECLARE-CURSOR-WC-NST.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       C205-EX.
           EXIT.

       C210-SELECT-WC-NST.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       C210-EX.
           EXIT.

       C260-OPEN-CURSOR-WC-NST.
           PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       C260-EX.
           EXIT.

       C270-CLOSE-CURSOR-WC-NST.
           SET IDSV0003-INVALID-OPER TO TRUE.
       C270-EX.
           EXIT.

       C280-FETCH-FIRST-WC-NST.
           PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
           END-IF.
       C280-EX.
           EXIT.

       C290-FETCH-NEXT-WC-NST.
           SET IDSV0003-INVALID-OPER TO TRUE.
       C290-EX.
           EXIT.
      *----
      *----  utilit` comuni a tutti i livelli operazione
      *----
       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-ADE-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO ADE-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-ADE-IB-PREV = -1
              MOVE HIGH-VALUES TO ADE-IB-PREV-NULL
           END-IF
           IF IND-ADE-IB-OGG = -1
              MOVE HIGH-VALUES TO ADE-IB-OGG-NULL
           END-IF
           IF IND-ADE-DT-DECOR = -1
              MOVE HIGH-VALUES TO ADE-DT-DECOR-NULL
           END-IF
           IF IND-ADE-DT-SCAD = -1
              MOVE HIGH-VALUES TO ADE-DT-SCAD-NULL
           END-IF
           IF IND-ADE-ETA-A-SCAD = -1
              MOVE HIGH-VALUES TO ADE-ETA-A-SCAD-NULL
           END-IF
           IF IND-ADE-DUR-AA = -1
              MOVE HIGH-VALUES TO ADE-DUR-AA-NULL
           END-IF
           IF IND-ADE-DUR-MM = -1
              MOVE HIGH-VALUES TO ADE-DUR-MM-NULL
           END-IF
           IF IND-ADE-DUR-GG = -1
              MOVE HIGH-VALUES TO ADE-DUR-GG-NULL
           END-IF
           IF IND-ADE-TP-RIAT = -1
              MOVE HIGH-VALUES TO ADE-TP-RIAT-NULL
           END-IF
           IF IND-ADE-TP-IAS = -1
              MOVE HIGH-VALUES TO ADE-TP-IAS-NULL
           END-IF
           IF IND-ADE-DT-VARZ-TP-IAS = -1
              MOVE HIGH-VALUES TO ADE-DT-VARZ-TP-IAS-NULL
           END-IF
           IF IND-ADE-PRE-NET-IND = -1
              MOVE HIGH-VALUES TO ADE-PRE-NET-IND-NULL
           END-IF
           IF IND-ADE-PRE-LRD-IND = -1
              MOVE HIGH-VALUES TO ADE-PRE-LRD-IND-NULL
           END-IF
           IF IND-ADE-RAT-LRD-IND = -1
              MOVE HIGH-VALUES TO ADE-RAT-LRD-IND-NULL
           END-IF
           IF IND-ADE-PRSTZ-INI-IND = -1
              MOVE HIGH-VALUES TO ADE-PRSTZ-INI-IND-NULL
           END-IF
           IF IND-ADE-FL-COINC-ASSTO = -1
              MOVE HIGH-VALUES TO ADE-FL-COINC-ASSTO-NULL
           END-IF
           IF IND-ADE-IB-DFLT = -1
              MOVE HIGH-VALUES TO ADE-IB-DFLT-NULL
           END-IF
           IF IND-ADE-MOD-CALC = -1
              MOVE HIGH-VALUES TO ADE-MOD-CALC-NULL
           END-IF
           IF IND-ADE-TP-FNT-CNBTVA = -1
              MOVE HIGH-VALUES TO ADE-TP-FNT-CNBTVA-NULL
           END-IF
           IF IND-ADE-IMP-AZ = -1
              MOVE HIGH-VALUES TO ADE-IMP-AZ-NULL
           END-IF
           IF IND-ADE-IMP-ADER = -1
              MOVE HIGH-VALUES TO ADE-IMP-ADER-NULL
           END-IF
           IF IND-ADE-IMP-TFR = -1
              MOVE HIGH-VALUES TO ADE-IMP-TFR-NULL
           END-IF
           IF IND-ADE-IMP-VOLO = -1
              MOVE HIGH-VALUES TO ADE-IMP-VOLO-NULL
           END-IF
           IF IND-ADE-PC-AZ = -1
              MOVE HIGH-VALUES TO ADE-PC-AZ-NULL
           END-IF
           IF IND-ADE-PC-ADER = -1
              MOVE HIGH-VALUES TO ADE-PC-ADER-NULL
           END-IF
           IF IND-ADE-PC-TFR = -1
              MOVE HIGH-VALUES TO ADE-PC-TFR-NULL
           END-IF
           IF IND-ADE-PC-VOLO = -1
              MOVE HIGH-VALUES TO ADE-PC-VOLO-NULL
           END-IF
           IF IND-ADE-DT-NOVA-RGM-FISC = -1
              MOVE HIGH-VALUES TO ADE-DT-NOVA-RGM-FISC-NULL
           END-IF
           IF IND-ADE-FL-ATTIV = -1
              MOVE HIGH-VALUES TO ADE-FL-ATTIV-NULL
           END-IF
           IF IND-ADE-IMP-REC-RIT-VIS = -1
              MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-VIS-NULL
           END-IF
           IF IND-ADE-IMP-REC-RIT-ACC = -1
              MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-ACC-NULL
           END-IF
           IF IND-ADE-FL-VARZ-STAT-TBGC = -1
              MOVE HIGH-VALUES TO ADE-FL-VARZ-STAT-TBGC-NULL
           END-IF
           IF IND-ADE-FL-PROVZA-MIGRAZ = -1
              MOVE HIGH-VALUES TO ADE-FL-PROVZA-MIGRAZ-NULL
           END-IF
           IF IND-ADE-IMPB-VIS-DA-REC = -1
              MOVE HIGH-VALUES TO ADE-IMPB-VIS-DA-REC-NULL
           END-IF
           IF IND-ADE-DT-DECOR-PREST-BAN = -1
              MOVE HIGH-VALUES TO ADE-DT-DECOR-PREST-BAN-NULL
           END-IF
           IF IND-ADE-DT-EFF-VARZ-STAT-T = -1
              MOVE HIGH-VALUES TO ADE-DT-EFF-VARZ-STAT-T-NULL
           END-IF
           IF IND-ADE-CUM-CNBT-CAP = -1
              MOVE HIGH-VALUES TO ADE-CUM-CNBT-CAP-NULL
           END-IF
           IF IND-ADE-IMP-GAR-CNBT = -1
              MOVE HIGH-VALUES TO ADE-IMP-GAR-CNBT-NULL
           END-IF
           IF IND-ADE-DT-ULT-CONS-CNBT = -1
              MOVE HIGH-VALUES TO ADE-DT-ULT-CONS-CNBT-NULL
           END-IF
           IF IND-ADE-IDEN-ISC-FND = -1
              MOVE HIGH-VALUES TO ADE-IDEN-ISC-FND-NULL
           END-IF
           IF IND-ADE-NUM-RAT-PIAN = -1
              MOVE HIGH-VALUES TO ADE-NUM-RAT-PIAN-NULL
           END-IF
           IF IND-ADE-DT-PRESC = -1
              MOVE HIGH-VALUES TO ADE-DT-PRESC-NULL
           END-IF
           IF IND-ADE-CONCS-PREST = -1
              MOVE HIGH-VALUES TO ADE-CONCS-PREST-NULL
           END-IF
           IF IND-POL-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO POL-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-POL-IB-OGG = -1
              MOVE HIGH-VALUES TO POL-IB-OGG-NULL
           END-IF
           IF IND-POL-DT-PROP = -1
              MOVE HIGH-VALUES TO POL-DT-PROP-NULL
           END-IF
           IF IND-POL-DUR-AA = -1
              MOVE HIGH-VALUES TO POL-DUR-AA-NULL
           END-IF
           IF IND-POL-DUR-MM = -1
              MOVE HIGH-VALUES TO POL-DUR-MM-NULL
           END-IF
           IF IND-POL-DT-SCAD = -1
              MOVE HIGH-VALUES TO POL-DT-SCAD-NULL
           END-IF
           IF IND-POL-COD-CONV = -1
              MOVE HIGH-VALUES TO POL-COD-CONV-NULL
           END-IF
           IF IND-POL-COD-RAMO = -1
              MOVE HIGH-VALUES TO POL-COD-RAMO-NULL
           END-IF
           IF IND-POL-DT-INI-VLDT-CONV = -1
              MOVE HIGH-VALUES TO POL-DT-INI-VLDT-CONV-NULL
           END-IF
           IF IND-POL-DT-APPLZ-CONV = -1
              MOVE HIGH-VALUES TO POL-DT-APPLZ-CONV-NULL
           END-IF
           IF IND-POL-TP-RGM-FISC = -1
              MOVE HIGH-VALUES TO POL-TP-RGM-FISC-NULL
           END-IF
           IF IND-POL-FL-ESTAS = -1
              MOVE HIGH-VALUES TO POL-FL-ESTAS-NULL
           END-IF
           IF IND-POL-FL-RSH-COMUN = -1
              MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-NULL
           END-IF
           IF IND-POL-FL-RSH-COMUN-COND = -1
              MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-COND-NULL
           END-IF
           IF IND-POL-FL-COP-FINANZ = -1
              MOVE HIGH-VALUES TO POL-FL-COP-FINANZ-NULL
           END-IF
           IF IND-POL-TP-APPLZ-DIR = -1
              MOVE HIGH-VALUES TO POL-TP-APPLZ-DIR-NULL
           END-IF
           IF IND-POL-SPE-MED = -1
              MOVE HIGH-VALUES TO POL-SPE-MED-NULL
           END-IF
           IF IND-POL-DIR-EMIS = -1
              MOVE HIGH-VALUES TO POL-DIR-EMIS-NULL
           END-IF
           IF IND-POL-DIR-1O-VERS = -1
              MOVE HIGH-VALUES TO POL-DIR-1O-VERS-NULL
           END-IF
           IF IND-POL-DIR-VERS-AGG = -1
              MOVE HIGH-VALUES TO POL-DIR-VERS-AGG-NULL
           END-IF
           IF IND-POL-COD-DVS = -1
              MOVE HIGH-VALUES TO POL-COD-DVS-NULL
           END-IF
           IF IND-POL-FL-FNT-AZ = -1
              MOVE HIGH-VALUES TO POL-FL-FNT-AZ-NULL
           END-IF
           IF IND-POL-FL-FNT-ADER = -1
              MOVE HIGH-VALUES TO POL-FL-FNT-ADER-NULL
           END-IF
           IF IND-POL-FL-FNT-TFR = -1
              MOVE HIGH-VALUES TO POL-FL-FNT-TFR-NULL
           END-IF
           IF IND-POL-FL-FNT-VOLO = -1
              MOVE HIGH-VALUES TO POL-FL-FNT-VOLO-NULL
           END-IF
           IF IND-POL-TP-OPZ-A-SCAD = -1
              MOVE HIGH-VALUES TO POL-TP-OPZ-A-SCAD-NULL
           END-IF
           IF IND-POL-AA-DIFF-PROR-DFLT = -1
              MOVE HIGH-VALUES TO POL-AA-DIFF-PROR-DFLT-NULL
           END-IF
           IF IND-POL-FL-VER-PROD = -1
              MOVE HIGH-VALUES TO POL-FL-VER-PROD-NULL
           END-IF
           IF IND-POL-DUR-GG = -1
              MOVE HIGH-VALUES TO POL-DUR-GG-NULL
           END-IF
           IF IND-POL-DIR-QUIET = -1
              MOVE HIGH-VALUES TO POL-DIR-QUIET-NULL
           END-IF
           IF IND-POL-TP-PTF-ESTNO = -1
              MOVE HIGH-VALUES TO POL-TP-PTF-ESTNO-NULL
           END-IF
           IF IND-POL-FL-CUM-PRE-CNTR = -1
              MOVE HIGH-VALUES TO POL-FL-CUM-PRE-CNTR-NULL
           END-IF
           IF IND-POL-FL-AMMB-MOVI = -1
              MOVE HIGH-VALUES TO POL-FL-AMMB-MOVI-NULL
           END-IF
           IF IND-POL-CONV-GECO = -1
              MOVE HIGH-VALUES TO POL-CONV-GECO-NULL
           END-IF
           IF IND-POL-FL-SCUDO-FISC = -1
              MOVE HIGH-VALUES TO POL-FL-SCUDO-FISC-NULL
           END-IF
           IF IND-POL-FL-TRASFE = -1
              MOVE HIGH-VALUES TO POL-FL-TRASFE-NULL
           END-IF
           IF IND-POL-FL-TFR-STRC = -1
              MOVE HIGH-VALUES TO POL-FL-TFR-STRC-NULL
           END-IF
           IF IND-POL-DT-PRESC = -1
              MOVE HIGH-VALUES TO POL-DT-PRESC-NULL
           END-IF
           IF IND-POL-COD-CONV-AGG = -1
              MOVE HIGH-VALUES TO POL-COD-CONV-AGG-NULL
           END-IF
           IF IND-POL-SUBCAT-PROD = -1
              MOVE HIGH-VALUES TO POL-SUBCAT-PROD-NULL
           END-IF
           IF IND-POL-FL-QUEST-ADEGZ-ASS = -1
              MOVE HIGH-VALUES TO POL-FL-QUEST-ADEGZ-ASS-NULL
           END-IF
           IF IND-POL-COD-TPA = -1
              MOVE HIGH-VALUES TO POL-COD-TPA-NULL
           END-IF
           IF IND-POL-ID-ACC-COMM = -1
              MOVE HIGH-VALUES TO POL-ID-ACC-COMM-NULL
           END-IF
           IF IND-POL-FL-POLI-CPI-PR = -1
              MOVE HIGH-VALUES TO POL-FL-POLI-CPI-PR-NULL
           END-IF
           IF IND-POL-FL-POLI-BUNDLING = -1
              MOVE HIGH-VALUES TO POL-FL-POLI-BUNDLING-NULL
           END-IF
           IF IND-POL-IND-POLI-PRIN-COLL = -1
              MOVE HIGH-VALUES TO POL-IND-POLI-PRIN-COLL-NULL
           END-IF
           IF IND-POL-FL-VND-BUNDLE = -1
              MOVE HIGH-VALUES TO POL-FL-VND-BUNDLE-NULL
           END-IF
           IF IND-POL-IB-BS = -1
              MOVE HIGH-VALUES TO POL-IB-BS-NULL
           END-IF
           IF IND-POL-FL-POLI-IFP = -1
              MOVE HIGH-VALUES TO POL-FL-POLI-IFP-NULL
           END-IF
           IF IND-STB-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO STB-ID-MOVI-CHIU-NULL
           END-IF.

       Z100-EX.
           EXIT.

      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE ADE-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO ADE-DT-INI-EFF-DB
           MOVE ADE-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO ADE-DT-END-EFF-DB
           IF IND-ADE-DT-DECOR = 0
               MOVE ADE-DT-DECOR TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-DECOR-DB
           END-IF
           IF IND-ADE-DT-SCAD = 0
               MOVE ADE-DT-SCAD TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-SCAD-DB
           END-IF
           IF IND-ADE-DT-VARZ-TP-IAS = 0
               MOVE ADE-DT-VARZ-TP-IAS TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-VARZ-TP-IAS-DB
           END-IF
           IF IND-ADE-DT-NOVA-RGM-FISC = 0
               MOVE ADE-DT-NOVA-RGM-FISC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-NOVA-RGM-FISC-DB
           END-IF
           IF IND-ADE-DT-DECOR-PREST-BAN = 0
               MOVE ADE-DT-DECOR-PREST-BAN TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-DECOR-PREST-BAN-DB
           END-IF
           IF IND-ADE-DT-EFF-VARZ-STAT-T = 0
               MOVE ADE-DT-EFF-VARZ-STAT-T TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-EFF-VARZ-STAT-T-DB
           END-IF
           IF IND-ADE-DT-ULT-CONS-CNBT = 0
               MOVE ADE-DT-ULT-CONS-CNBT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-ULT-CONS-CNBT-DB
           END-IF
           IF IND-ADE-DT-PRESC = 0
               MOVE ADE-DT-PRESC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-PRESC-DB
           END-IF
           IF IND-POL-DT-PROP = 0
               MOVE POL-DT-PROP TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-PROP-DB
           END-IF
           MOVE POL-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-INI-EFF-DB
           MOVE POL-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-END-EFF-DB
           MOVE POL-DT-DECOR TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-DECOR-DB
           MOVE POL-DT-EMIS TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-EMIS-DB
           IF IND-POL-DT-SCAD = 0
               MOVE POL-DT-SCAD TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-SCAD-DB
           END-IF
           MOVE POL-DT-INI-VLDT-PROD TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-INI-VLDT-PROD-DB
           IF IND-POL-DT-INI-VLDT-CONV = 0
               MOVE POL-DT-INI-VLDT-CONV TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-INI-VLDT-CONV-DB
           END-IF
           IF IND-POL-DT-APPLZ-CONV = 0
               MOVE POL-DT-APPLZ-CONV TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-APPLZ-CONV-DB
           END-IF
           IF IND-POL-DT-PRESC = 0
               MOVE POL-DT-PRESC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-PRESC-DB
           END-IF
           MOVE STB-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO STB-DT-INI-EFF-DB
           MOVE STB-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO STB-DT-END-EFF-DB.

       Z900-EX.
           EXIT.

      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE ADE-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO ADE-DT-INI-EFF
           MOVE ADE-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO ADE-DT-END-EFF
           IF IND-ADE-DT-DECOR = 0
               MOVE ADE-DT-DECOR-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-DECOR
           END-IF
           IF IND-ADE-DT-SCAD = 0
               MOVE ADE-DT-SCAD-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-SCAD
           END-IF
           IF IND-ADE-DT-VARZ-TP-IAS = 0
               MOVE ADE-DT-VARZ-TP-IAS-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-VARZ-TP-IAS
           END-IF
           IF IND-ADE-DT-NOVA-RGM-FISC = 0
               MOVE ADE-DT-NOVA-RGM-FISC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-NOVA-RGM-FISC
           END-IF
           IF IND-ADE-DT-DECOR-PREST-BAN = 0
               MOVE ADE-DT-DECOR-PREST-BAN-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-DECOR-PREST-BAN
           END-IF
           IF IND-ADE-DT-EFF-VARZ-STAT-T = 0
               MOVE ADE-DT-EFF-VARZ-STAT-T-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-EFF-VARZ-STAT-T
           END-IF
           IF IND-ADE-DT-ULT-CONS-CNBT = 0
               MOVE ADE-DT-ULT-CONS-CNBT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-ULT-CONS-CNBT
           END-IF
           IF IND-ADE-DT-PRESC = 0
               MOVE ADE-DT-PRESC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-PRESC
           END-IF
           IF IND-POL-DT-PROP = 0
               MOVE POL-DT-PROP-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-PROP
           END-IF
           MOVE POL-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-INI-EFF
           MOVE POL-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-END-EFF
           MOVE POL-DT-DECOR-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-DECOR
           MOVE POL-DT-EMIS-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-EMIS
           IF IND-POL-DT-SCAD = 0
               MOVE POL-DT-SCAD-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-SCAD
           END-IF
           MOVE POL-DT-INI-VLDT-PROD-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-INI-VLDT-PROD
           IF IND-POL-DT-INI-VLDT-CONV = 0
               MOVE POL-DT-INI-VLDT-CONV-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-INI-VLDT-CONV
           END-IF
           IF IND-POL-DT-APPLZ-CONV = 0
               MOVE POL-DT-APPLZ-CONV-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-APPLZ-CONV
           END-IF
           IF IND-POL-DT-PRESC = 0
               MOVE POL-DT-PRESC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-PRESC
           END-IF
           MOVE STB-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO STB-DT-INI-EFF
           MOVE STB-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO STB-DT-END-EFF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----
      *----  prevede statements AD HOC PRE Query
      *----
       Z970-CODICE-ADHOC-PRE.

           CONTINUE.
       Z970-EX.
           EXIT.
      *----
      *----  prevede statements AD HOC POST Query
      *----
       Z980-CODICE-ADHOC-POST.

           MOVE ADES          TO  LDBVC821-ADES
           MOVE POLI          TO  LDBVC821-POLI
           MOVE STAT-OGG-BUS  TO
                LDBVC821-STAT-OGG-BUS.

       Z980-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

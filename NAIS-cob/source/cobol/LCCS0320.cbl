      *  ============================================================= *
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      *  ============================================================= *
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LCCS0320.
       AUTHOR.             AISS.
       DATE-WRITTEN.       MAGGIO 2008.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LCCS0320
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CALCOLO DATA RICORRENZA SUCCESSIVA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.

       DATA DIVISION.
      *
      *
       WORKING-STORAGE SECTION.

       01 WK-PGM                            PIC X(8) VALUE 'LCCS0320'.
      * --> GESTIONE DATE
       01  LCCS0003                         PIC X(8) VALUE 'LCCS0003'.
      *  --> MODULO PER CALCOLO DATA FINE MESE
       01  LCCS0029                         PIC X(8) VALUE 'LCCS0029'.
       01  LDBS2680                         PIC X(8) VALUE 'LDBS2680'.
47897  01  LDBSG350                         PIC X(8) VALUE 'LDBSG350'.
      *
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
      *
       01 WK-INDICI.
          05 IX-TAB-PMO                     PIC 9(04) VALUE ZEROES.
          05 IX-TAB-GRZ                     PIC 9(04) VALUE ZEROES.
          05 IX-TAB-TGA                     PIC 9(04) VALUE ZEROES.
      *
      *----------------------------------------------------------------*
      *     COPY DELLE TABELLE DB2 PER IL DISPATCHER DATI
      *----------------------------------------------------------------*
      *
      *  --> PARAMETRO MOVIMENTO
           COPY IDBVPMO1.
      *  --> WHERE CONDITION PARAMETRO MOVIMENTO
           COPY LDBV2681.
47897      COPY LDBVG351.
      *  --> COPY STAT-OGG-BUS
           COPY IDBVSTB1.
      *
      *----------------------------------------------------------------*
      *    COPY DISPATCHER
      *----------------------------------------------------------------*
      *
       01  IDSV0012.
           COPY IDSV0012.
      *
       01  DISPATCHER-VARIABLES.
      ** COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      ** COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.
      *
      *----------------------------------------------------------------*
      *      COPY VARIABILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
      *
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IEAV9903.
           COPY IDSV0002.
      *
      *----------------------------------------------------------------*
      *    AREA IO SERVIZIO CALCOLO DATA FINE MESE
      *----------------------------------------------------------------*
      *
       01 AREA-IO-LCCS0029.
          COPY LCCC0029                  REPLACING ==(SF)== BY ==S029==.
      *
      *----------------------------------------------------------------*
      *    AREA IO SERVIZIO CALCOLI SULLE DATE
      *----------------------------------------------------------------*
      *
           COPY LCCC0003.
      *----------------------------------------------------------------*
      *--  Interfaccia modulo LDBS1130 - Estrazione Parametro Oggetto
      *----------------------------------------------------------------*
           COPY LDBV1131.
      *
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
      *
      * --> Area di appoggio per i messaggi di errore
      *
       01 WK-LABEL-ERR                      PIC X(30) VALUE SPACES.
      *
       01  IN-RCODE                       PIC 9(2).
       01  WK-DT-CALCOLATA                PIC 9(8) VALUE ZEROES.
35581  01  WK-DT-LIMITE                   PIC 9(8) VALUE ZEROES.
       01  WK-APPO-DT-RICOR-SUCC          PIC 9(8) VALUE ZEROES.
      *
       01  WK-APPO-DT-NUM                 PIC 9(8) VALUE ZEROES.
      *
       01  WK-DT-DEC.
           03 WK-DT-DEC-AAAA              PIC 9(4) VALUE ZEROES.
           03 WK-DT-DEC-MM                PIC 9(2) VALUE ZEROES.
           03 WK-DT-DEC-GG                PIC 9(2) VALUE ZEROES.
      *
       01  WK-DT-CALC.
           03 WK-DT-CALC-AAAA             PIC 9(4) VALUE ZEROES.
           03 WK-DT-CALC-MM               PIC 9(2) VALUE ZEROES.
           03 WK-DT-CALC-GG               PIC 9(2) VALUE ZEROES.
      *
      * --> Gestione opzioni
       01  WK-STR-OPZIONE.
           03 WK-COD-OPZIONE              PIC X(02) VALUE SPACES.
           03 FILLER                      PIC X(10) VALUE SPACES.

       01  WK-TP-OPZIONI                  PIC X(02).
           88 WK-NESSUNA-OPZIONE                 VALUE '00'.
           88 WK-RENDITA-VITALIZIA               VALUE '01'.
           88 WK-RENDITA-CERTA                   VALUE '02'.
           88 WK-RENDITA-REVERSIBILE             VALUE '03'.
           88 WK-RENDITA-TEMPORANEA              VALUE '04'.
           88 WK-DIFFERIMENTO                    VALUE '05'.
           88 WK-PROROGA                         VALUE '06'.
           88 WK-CAPITALE                        VALUE '07'.
      *
      * --> Ricerca Occorrenza Parametro Movimento
       01 WK-PMO-TROVATO                 PIC X.
          88 WK-PMO-TROV-SI                VALUE 'S'.
          88 WK-PMO-TROV-NO                VALUE 'N'.
      * --> Ricerca Occorrenza Parametro OGGETTO
       01  WK-RICERCA-PERIODADEG             PIC X(001) VALUE 'N'.
           88 WK-TROVATO-PERIODADEG                     VALUE 'S'.
           88 WK-NON-TROVATO-PERIODADEG                 VALUE 'N'.

       01  WK-FRAZ-DECR-CPT                  PIC S9(5)V COMP-3 VALUE 0.

46761  01 WK-FRQ-MOVI                    PIC S9(5)V     COMP-3.
46761  01 WK-FRQ-MOVI-NULL REDEFINES WK-FRQ-MOVI PIC X(3).
46761  01 WK-DT-RICOR-SUCC               PIC S9(8)V COMP-3.
46761  01 WK-DT-RICOR-SUCC-NULL REDEFINES WK-DT-RICOR-SUCC PIC X(5).

46761  01 PMO-TROV                         PIC X(01).
46761     88 PMO-TROV-SI                  VALUE 'S'.
46761     88 PMO-TROV-NO                  VALUE 'N'.
      *
      * COPY PER TESTARE IL TIPO DATO DI RITORNO DAI SERVIZI.
      *
           COPY LCCC0006.
           COPY LCCVXMV0.
           COPY LCCVXOG0.
           COPY LCCVXFA0.
           COPY LCCVXCA0.
           COPY LCCVPMOZ.

      * COPY PER INCLUDERE LA TABELLA PARAM_OGG
           COPY IDBVPOG1.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
      *
       01 AREA-IDSV0001.
            COPY IDSV0001.
      *
      *-->  AREA PARAMETRO MOVIMENTO
       01 WPMO-AREA-PMO.
          04 WPMO-ELE-PMO-MAX            PIC S9(04) COMP.
                COPY LCCVPMOB REPLACING   ==(SF)==  BY ==WPMO==.
              COPY LCCVPMO1              REPLACING ==(SF)== BY ==WPMO==.
      *-->  AREA POLIZZA
       01 WPOL-AREA-POLIZZA.
          04 WPOL-ELE-POLI-MAX           PIC S9(04) COMP.
          04 WPOL-TAB-POL.
              COPY LCCVPOL1              REPLACING ==(SF)== BY ==WPOL==.
      *-->  AREA ADESIONE
       01 WADE-AREA-ADESIONE.
          04 WADE-ELE-ADE-MAX            PIC S9(004) COMP.
          04 WADE-TAB-ADE                OCCURS 1.
              COPY LCCVADE1              REPLACING ==(SF)== BY ==WADE==.
      *-->  AREA GARANZIA
       01 WGRZ-AREA-GARANZIA.
          04 WGRZ-ELE-GRZ-MAX            PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGRZ==.
              COPY LCCVGRZ1              REPLACING ==(SF)== BY ==WGRZ==.
      *-->  AREA TRANCHE DI GARANZIA
       01 WTGA-AREA-TGA.
          04 WTGA-ELE-TGA-MAX            PIC S9(04) COMP.
                COPY LCCVTGAC REPLACING   ==(SF)==  BY ==WTGA==.
              COPY LCCVTGA1              REPLACING ==(SF)== BY ==WTGA==.
      *
       01 WCOM-AREA-LCCC0320.
          COPY LCCC0320                  REPLACING ==(SF)== BY ==WCOM==.
      *----------------------------------------------------------------*
      *     P R O C E D U R E    D I V I S I O N
      *----------------------------------------------------------------*
      *
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WPMO-AREA-PMO
                                WPOL-AREA-POLIZZA
                                WADE-AREA-ADESIONE
                                WGRZ-AREA-GARANZIA
                                WTGA-AREA-TGA
                                WCOM-AREA-LCCC0320.
      *
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.
      *
           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.
      *
           PERFORM S9000-OPERAZ-FINALI
              THRU EX-S9000.
      *
      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI
      *----------------------------------------------------------------*
      *
       S0000-OPERAZIONI-INIZIALI.
      *
           MOVE WPOL-TP-FRM-ASSVA
             TO WS-TP-FRM-ASSVA.
      *
       EX-S0000.
           EXIT.
      *
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
      *
       S1000-ELABORAZIONE.
      *
           EVALUATE IDSV0001-TIPO-MOVIMENTO
      *
              WHEN 6101
              WHEN 6002
              WHEN 6003
              WHEN 6102
      *
                  PERFORM GESTINONE-PMO
                      THRU GESTINONE-PMO-EX
      *
              WHEN 6006
      *
                  PERFORM GESTIONE-RIVAL
                     THRU GESTIONE-RIVAL-EX
      *
              WHEN 6009
      *
                  PERFORM GESTIONE-SCADE
                     THRU GESTIONE-SCADE-EX
      *
              WHEN 6013
              WHEN 6016
      *
                  PERFORM GESTIONE-BONUS
                     THRU GESTIONE-BONUS-EX
      *
      *
              WHEN OTHER
      *
                   CONTINUE
      *
           END-EVALUATE.
      *
       EX-S1000.
           EXIT.
      *
      *----------------------------------------------------------------*
      * CALCOLO DATA RICORRENZA SUCCESSIVA (QUIETANZAMENTO)
      *----------------------------------------------------------------*
      *
       GESTINONE-PMO.
      *
46761      MOVE IDSV0001-TIPO-MOVIMENTO TO WS-MOVIMENTO
46761      IF GENER-TRAINC
46761       PERFORM S10100-6003-AGGIUNG-FRAZ
46761          THRU S10100-6003-AGGIUNG-FRAZ-EX
46761      ELSE
           PERFORM S10100-6101-AGGIUNG-FRAZ
               THRU S10100-6101-AGGIUNG-FRAZ-EX
46761      END-IF.
      *
46761 *    MOVE IDSV0001-TIPO-MOVIMENTO TO WS-MOVIMENTO
      *
           IF IDSV0001-ESITO-OK
           AND NOT GENER-TRAINC
      *
              PERFORM S10200-6101-VALIDA-DATA
                 THRU S10200-6101-VALIDA-DATA-EX
      *
           END-IF.
      *
           PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
             UNTIL IX-TAB-PMO > WPMO-ELE-PMO-MAX
                OR IDSV0001-ESITO-KO
      *
               IF NOT WPMO-ST-DEL(IX-TAB-PMO)
      *
                  MOVE WPMO-TP-MOVI(IX-TAB-PMO)
                    TO WS-MOVIMENTO
      *
                  IF GENER-TRANCH OR MOVIM-QUIETA OR
                     MOVIM-QUIETA-INT-PREST
                  OR GENER-TRAINC
      *
                     PERFORM S10300-6101-CICLO-PMO-GRZ
                        THRU S10300-6101-CICLO-PMO-GRZ-EX
      *
                  END-IF
      *
34024          ELSE
                  MOVE WPMO-TP-MOVI(IX-TAB-PMO)
                    TO WS-MOVIMENTO
                  IF GENER-TRAINC
                     PERFORM S10500-6101-VERIF-VAR-FRAZ
                        THRU S10500-6101-VERIF-VAR-FRAZ-EX
                  END-IF
               END-IF
      *
           END-PERFORM.
      *
       GESTINONE-PMO-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     AGGIUNGO UN FRAZIONAMENTO ALLA RICORRENZA ATTUALE
      *----------------------------------------------------------------*
      *
       S10100-6101-AGGIUNG-FRAZ.
      *
      * --> A2K-DELTA -> Delta Da Sommare
      *
           EVALUATE WPMO-FRQ-MOVI(1)
      * --> Mensile
               WHEN 12
      *
                 MOVE 1
                   TO A2K-DELTA
      *
      * --> Bimestrale
               WHEN 6
      *
                 MOVE 2
                   TO A2K-DELTA
      *
      * --> Trimestrale
               WHEN 4
      *
                 MOVE 3
                   TO A2K-DELTA
      *
      * --> Quadrimestrale
               WHEN 3
      *
                 MOVE 4
                   TO A2K-DELTA
      *
      * --> Semestrale
               WHEN 2
      *
                 MOVE 6
                   TO A2K-DELTA
      *
      * --> Annuale
               WHEN 1
      *
                 MOVE 12
                   TO A2K-DELTA
      *
           END-EVALUATE.
      *
      * --> A2K-TDELTA -> Tipo Delta: Mesi(M) o Anni(A)
      *
           IF WCOM-ACCORPATE-SI
              COMPUTE A2K-DELTA = A2K-DELTA * WCOM-NUM-RATE-ACCORPATE
           END-IF.

           MOVE 'M'                     TO A2K-TDELTA.
      *
      * --> A2K-INAMG -> Data Di Partenza Alla Quale Sommare Il Delta
      *
           MOVE WPMO-DT-RICOR-SUCC(1)
             TO A2K-INAMG.
      *
      * --> A2K-FUNZ -> Tipo Funzione: Somma Delta
      *
           MOVE '02'
             TO A2K-FUNZ.
      *
      * --> A2K-INFO -> Tipo Formato Data: AAAAMMGG
      *
           MOVE '03'
             TO A2K-INFO.
      *
      * --> A2K-INICON -> Giorno Inizio Conteggio (Stesso Giorno)
      *
           MOVE '0'
             TO A2K-INICON.
      *
      * --> A2K-FISLAV -> Tipo Giorni Da Sommare (Fissi)
      *
           MOVE '0'
             TO A2K-FISLAV.
      *
           PERFORM CALL-LCCS0003
              THRU CALL-LCCS0003-EX
35581      IF  IDSV0001-ESITO-OK
35581          MOVE A2K-OUAMG
35581           TO WK-DT-CALCOLATA
35581      END-IF.

       S10100-6101-AGGIUNG-FRAZ-EX.
           EXIT.
      *---------------------------------------------------------------- *
      *     AGGIUNGO UN FRAZIONAMENTO ALLA RICORRENZA ATTUALE
      *     PER GETRA DA INCASSO
      *----------------------------------------------------------------*
      *
46761  S10100-6003-AGGIUNG-FRAZ.
      *
           MOVE ZERO            TO WK-FRQ-MOVI
                                   WK-DT-RICOR-SUCC
           SET PMO-TROV-NO      TO TRUE
           PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
             UNTIL IX-TAB-PMO > WK-PMO-MAX-B
                OR PMO-TROV-SI
             IF  WPMO-TP-MOVI(IX-TAB-PMO) = 6003
             AND NOT WPMO-ST-DEL(IX-TAB-PMO)
                 IF WPMO-FRQ-MOVI-NULL(IX-TAB-PMO) NOT =
                    HIGH-VALUES
                    MOVE WPMO-FRQ-MOVI(IX-TAB-PMO)
                      TO WK-FRQ-MOVI
                 END-IF
                 IF WPMO-DT-RICOR-SUCC-NULL(IX-TAB-PMO) NOT =
                    HIGH-VALUES
                    MOVE WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                      TO WK-DT-RICOR-SUCC
                 END-IF
                 SET PMO-TROV-SI      TO TRUE
             END-IF
           END-PERFORM
           IF PMO-TROV-NO
              PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
                UNTIL IX-TAB-PMO > WK-PMO-MAX-B
                   OR PMO-TROV-SI
                IF  WPMO-TP-MOVI(IX-TAB-PMO) = 6003
                AND WPMO-ST-DEL(IX-TAB-PMO)
                    IF WPMO-FRQ-MOVI-NULL(IX-TAB-PMO) NOT =
                       HIGH-VALUES
                       MOVE WPMO-FRQ-MOVI(IX-TAB-PMO)
                         TO WK-FRQ-MOVI
                    END-IF
                    IF WPMO-DT-RICOR-SUCC-NULL(IX-TAB-PMO) NOT =
                       HIGH-VALUES
                       MOVE WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                         TO WK-DT-RICOR-SUCC
                    END-IF
                    SET PMO-TROV-SI      TO TRUE
                END-IF
              END-PERFORM
           END-IF

      * --> A2K-DELTA -> Delta Da Sommare
      *
           EVALUATE WK-FRQ-MOVI
      * --> Mensile
               WHEN 12
      *
                 MOVE 1
                   TO A2K-DELTA
      *
      * --> Bimestrale
               WHEN 6
      *
                 MOVE 2
                   TO A2K-DELTA
      *
      * --> Trimestrale
               WHEN 4
      *
                 MOVE 3
                   TO A2K-DELTA
      *
      * --> Quadrimestrale
               WHEN 3
      *
                 MOVE 4
                   TO A2K-DELTA
      *
      * --> Semestrale
               WHEN 2
      *
                 MOVE 6
                   TO A2K-DELTA
      *
      * --> Annuale
               WHEN 1
      *
                 MOVE 12
                   TO A2K-DELTA
      *
           END-EVALUATE.
      *
      * --> A2K-TDELTA -> Tipo Delta: Mesi(M) o Anni(A)
      *
           IF WCOM-ACCORPATE-SI
              COMPUTE A2K-DELTA = A2K-DELTA * WCOM-NUM-RATE-ACCORPATE
           END-IF.

           MOVE 'M'                     TO A2K-TDELTA.
      *
      * --> A2K-INAMG -> Data Di Partenza Alla Quale Sommare Il Delta
      *
           MOVE WK-DT-RICOR-SUCC
             TO A2K-INAMG.
      *
      * --> A2K-FUNZ -> Tipo Funzione: Somma Delta
      *
           MOVE '02'
             TO A2K-FUNZ.
      *
      * --> A2K-INFO -> Tipo Formato Data: AAAAMMGG
      *
           MOVE '03'
             TO A2K-INFO.
      *
      * --> A2K-INICON -> Giorno Inizio Conteggio (Stesso Giorno)
      *
           MOVE '0'
             TO A2K-INICON.
      *
      * --> A2K-FISLAV -> Tipo Giorni Da Sommare (Fissi)
      *
           MOVE '0'
             TO A2K-FISLAV.
      *
           PERFORM CALL-LCCS0003
              THRU CALL-LCCS0003-EX
35581      IF  IDSV0001-ESITO-OK
35581          MOVE A2K-OUAMG
35581           TO WK-DT-CALCOLATA
35581      END-IF.

       S10100-6003-AGGIUNG-FRAZ-EX.
           EXIT.
      *---------------------------------------------------------------- *
      *     AGGIUNGO UN FRAZIONAMENTO ALLA RICORRENZA ATTUALE
      *----------------------------------------------------------------*
      *
       S10150-6101-CALCOLA-LIM.
      *
      * --> A2K-DELTA -> Delta Da Sommare
      *
           IF WGRZ-NUM-AA-PAG-PRE-NULL(IX-TAB-GRZ)
            NOT =  HIGH-VALUE
             MOVE WGRZ-NUM-AA-PAG-PRE(IX-TAB-GRZ)   TO A2K-DELTA
           END-IF

      *
           MOVE 'A'                     TO A2K-TDELTA.
      *
      * --> A2K-INAMG -> Data Di Partenza Alla Quale Sommare Il Delta
      *
           MOVE WGRZ-DT-DECOR(IX-TAB-GRZ)
             TO A2K-INAMG.
      *
      * --> A2K-FUNZ -> Tipo Funzione: Somma Delta
      *
           MOVE '02'
             TO A2K-FUNZ.
      *
      * --> A2K-INFO -> Tipo Formato Data: AAAAMMGG
      *
           MOVE '03'
             TO A2K-INFO.
      *
      * --> A2K-INICON -> Giorno Inizio Conteggio (Stesso Giorno)
      *
           MOVE '0'
             TO A2K-INICON.
      *
      * --> A2K-FISLAV -> Tipo Giorni Da Sommare (Fissi)
      *
           MOVE '0'
             TO A2K-FISLAV.
      *
           PERFORM CALL-LCCS0003
              THRU CALL-LCCS0003-EX
35581      IF IDSV0001-ESITO-OK
35581          MOVE A2K-OUAMG
35581           TO WK-DT-LIMITE
35581      END-IF.
      *
       S10150-6101-CALCOLA-LIM-EX.
           EXIT.
      **
      *----------------------------------------------------------------*
      *     CHIAMATA SERVIZIO CALCOLI SU DATE - LCCS0003 -
      *----------------------------------------------------------------*
      *
       CALL-LCCS0003.
      *
           MOVE 'CALL-LCCS0003'
             TO WK-LABEL-ERR.
      *
           CALL LCCS0003         USING IO-A2K-LCCC0003
                                       IN-RCODE
      *
           ON EXCEPTION
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'ERRORE CALL SERVIZIO LCCS0003'
                TO CALL-DESC
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
      *
           END-CALL.

           IF IN-RCODE  = ZEROES
35581          CONTINUE
35581 *        MOVE A2K-OUAMG
35581 *         TO WK-DT-CALCOLATA
      *
           ELSE
      *
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'ERRORE CALL LCCS0003'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
       CALL-LCCS0003-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     ALLINEAMENTO AREA PMO CON QUELLA DI GARANZIA
      *----------------------------------------------------------------*
      *
       S10300-6101-CICLO-PMO-GRZ.
      *
           PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
             UNTIL IX-TAB-GRZ > WGRZ-ELE-GRZ-MAX
                OR IDSV0001-ESITO-KO
      *
               IF WGRZ-ID-GAR(IX-TAB-GRZ) =
                  WPMO-ID-OGG(IX-TAB-PMO)

      *
35581            INITIALIZE WK-DT-LIMITE
                  IF WGRZ-NUM-AA-PAG-PRE-NULL(IX-TAB-GRZ)
                                              NOT = HIGH-VALUE
                    PERFORM S10150-6101-CALCOLA-LIM
35581               THRU S10150-6101-CALCOLA-LIM-EX
                  END-IF

35581            IF IDSV0001-ESITO-OK
                  PERFORM S10400-6101-VERIFICA-DATA
                     THRU S10400-6101-VERIFICA-DATA-EX
35581            END-IF
      *
               END-IF
      *
           END-PERFORM.
      *
       S10300-6101-CICLO-PMO-GRZ-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VALIDAZIONE DELLA DATA IN FUNZIONE DELLA DECORRENZA DI TRANCHE
      *----------------------------------------------------------------*
      *
       S10200-6101-VALIDA-DATA.
      *
           IF INDIVIDUALE
      *
              MOVE WPOL-DT-DECOR
                TO WK-APPO-DT-NUM
      *
           ELSE
      *
              IF WADE-DT-DECOR-NULL(1) NOT = HIGH-VALUES
      *
                 MOVE WADE-DT-DECOR(1)
                   TO WK-APPO-DT-NUM
      *
              ELSE
      *
                 MOVE WPOL-DT-DECOR
                   TO WK-APPO-DT-NUM
      *
              END-IF
      *
           END-IF.
      *
           MOVE WK-APPO-DT-NUM
             TO WK-DT-DEC.
      *
           MOVE WK-DT-CALCOLATA
             TO WK-APPO-DT-NUM.
           MOVE WK-APPO-DT-NUM
             TO WK-DT-CALC.
      *
           IF WK-DT-DEC-GG NOT = WK-DT-CALC-GG
      *
              MOVE WK-DT-DEC-GG
                TO WK-DT-CALC-GG
              MOVE WK-DT-CALC
                TO S029-DT-CALC
      *
              PERFORM S10210-6101-VALID-FIN-MESE
                 THRU S10210-6101-VALID-FIN-MESE-EX
      *
           END-IF.
      *
       S10200-6101-VALIDA-DATA-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *   VALIDAZIONE FINE MESE
      *----------------------------------------------------------------*
      *
       S10210-6101-VALID-FIN-MESE.
      *
           CALL LCCS0029         USING AREA-IDSV0001
                                       AREA-IO-LCCS0029
           ON EXCEPTION
      *
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'ERRORE CHIAMATA LCCS0029'
                   TO CALL-DESC
                 MOVE WK-LABEL-ERR
                   TO IEAI9901-LABEL-ERR
                 PERFORM S0290-ERRORE-DI-SISTEMA
                    THRU EX-S0290
      *
           END-CALL.
      *
           IF IDSV0001-ESITO-OK
      *
F26564        IF WK-DT-CALC > S029-DT-CALC
                 MOVE S029-DT-CALC
                   TO WK-DT-CALCOLATA
F26564        ELSE
F26564           MOVE WK-DT-CALC
F26564             TO WK-DT-CALCOLATA
F26564        END-IF
      *
           END-IF.
      *
       S10210-6101-VALID-FIN-MESE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     VERIFICHE SULLA DATA CALCOLATA
      *----------------------------------------------------------------*
      *
       S10400-6101-VERIFICA-DATA.
      *
           IF WGRZ-DT-SCAD-NULL(IX-TAB-GRZ) NOT = HIGH-VALUES
      *
35581 *       IF WK-DT-CALCOLATA > WGRZ-DT-SCAD(IX-TAB-GRZ)
35581 *       IF  (WK-DT-CALCOLATA >=  WGRZ-DT-SCAD(IX-TAB-GRZ)
35581 *         OR  ( WK-DT-CALCOLATA >= WK-DT-LIMITE
      *         AND WK-DT-LIMITE > 0))
39333         IF  (WK-DT-CALCOLATA >  WGRZ-DT-SCAD(IX-TAB-GRZ)
39333           OR  ( WK-DT-CALCOLATA > WK-DT-LIMITE
                AND WK-DT-LIMITE > 0))

      *
39333 *          SET WPMO-ST-DEL(IX-TAB-PMO)
39333 *            TO TRUE
39333            SET WPMO-ST-INV(IX-TAB-PMO)
39333              TO TRUE
      *
              ELSE
      *

                 PERFORM S10500-6101-VERIF-VAR-FRAZ
                    THRU S10500-6101-VERIF-VAR-FRAZ-EX
      *
              END-IF
      *
           ELSE
39333         IF   (WK-DT-CALCOLATA >  WK-DT-LIMITE
39333           AND WK-DT-LIMITE > 0)

35581 *
39333 *          SET WPMO-ST-DEL(IX-TAB-PMO)
39333 *            TO TRUE
39333            SET WPMO-ST-INV(IX-TAB-PMO)
39333              TO TRUE

35581         ELSE
      *
              PERFORM S10500-6101-VERIF-VAR-FRAZ
                 THRU S10500-6101-VERIF-VAR-FRAZ-EX
      *
35581         END-IF
           END-IF.
      *
       S10400-6101-VERIFICA-DATA-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     VERIFICA ESISTENZA PMO CON STESSA DATA CALCOLATA
      *----------------------------------------------------------------*
      *
       S10500-6101-VERIF-VAR-FRAZ.
      *
           PERFORM S10600-6101-IMPOSTA-PMO
              THRU S10600-6101-IMPOSTA-PMO-EX.
      *
           PERFORM S10700-6101-LEGGI-PMO
              THRU S10700-6101-LEGGI-PMO-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              IF WK-PMO-TROV-SI
34024          IF  WK-DT-CALCOLATA <= PMO-DT-RICOR-SUCC
      *
                 IF WPMO-ID-PARAM-MOVI(IX-TAB-PMO) =
                    PMO-ID-PARAM-MOVI
      *
                    SET WPMO-ST-INV(IX-TAB-PMO)
                      TO TRUE
      *
                 ELSE
      *
16660 *                   SET WPMO-ST-DEL(IX-TAB-PMO)
16660                     SET WPMO-ST-INV(IX-TAB-PMO)
                      TO TRUE
      *
                 END-IF
      *

               ELSE
      *
34024           IF NOT WPMO-ST-DEL(IX-TAB-PMO)
                 SET WPMO-ST-MOD(IX-TAB-PMO)
                   TO TRUE
34024           END-IF
      *
                 MOVE WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                   TO WPMO-DT-RICOR-PREC(IX-TAB-PMO)
      *
                 MOVE WK-DT-CALCOLATA
                   TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
      *
               END-IF

34024         ELSE
      *
34024            SET WPMO-ST-ADD(IX-TAB-PMO)
                   TO TRUE
      *
34024            MOVE HIGH-VALUES
34024              TO WPMO-DT-RICOR-PREC-NULL(IX-TAB-PMO)
      *
34024            MOVE WK-DT-CALCOLATA
34024              TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
      *
34024         END-IF
      *
           END-IF.
      *
       S10500-6101-VERIF-VAR-FRAZ-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  VALORIZZAZIONI PER LETTURA PARAMETRO MOVIMENTO
      *----------------------------------------------------------------*
      *
       S10600-6101-IMPOSTA-PMO.
      *
           SET WK-PMO-TROV-NO
             TO TRUE.
      *
           INITIALIZE PARAM-MOVI.
      *
           MOVE WPMO-ID-OGG(IX-TAB-PMO)
47897 *      TO LDBV2681-ID-OGG.
47897        TO LDBVG351-ID-OGG.
      *
           SET GARANZIA
             TO TRUE
           MOVE WS-TP-OGG
47897 *      TO LDBV2681-TP-OGG.
47897        TO LDBVG351-TP-OGG
      *
           MOVE IDSV0001-TIPO-MOVIMENTO
47897        TO LDBVG351-TP-MOVI.
47897 *      TO LDBV2681-TP-MOVI-01
47897 *         LDBV2681-TP-MOVI-02
47897 *         LDBV2681-TP-MOVI-03
47897 *         LDBV2681-TP-MOVI-04
47897 *         LDBV2681-TP-MOVI-05
47897 *         LDBV2681-TP-MOVI-06.
      *
      *  --> La data effetto viene sempre valorizzata
      *
           MOVE WK-DT-CALCOLATA
             TO IDSI0011-DATA-INIZIO-EFFETTO.
      *
           MOVE 999912314023595959
             TO IDSI0011-DATA-COMPETENZA.
      *
           MOVE ZEROES
             TO IDSI0011-DATA-FINE-EFFETTO.
      *
           SET IDSI0011-TRATT-DEFAULT
             TO TRUE.
      *
      *  --> Nome tabella fisica db
      *
47897 *    MOVE LDBS2680
47897      MOVE LDBSG350
             TO IDSI0011-CODICE-STR-DATO.
      *
      *  --> Dclgen tabella
      *
47897 *    MOVE LDBV2681
47897      MOVE LDBVG351
             TO IDSI0011-BUFFER-WHERE-COND.
           MOVE SPACES
              TO IDSI0011-BUFFER-DATI.
      *
      *  --> Tipo operazione
      *
           SET IDSI0011-SELECT
             TO TRUE.
           SET IDSI0011-TRATT-X-EFFETTO
             TO TRUE.
           SET IDSI0011-WHERE-CONDITION
             TO TRUE.
      *
      *  --> Modalita di accesso
      *
           SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
      *
       S10600-6101-IMPOSTA-PMO-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  LETTURA DELLA TABELLA PARAMETRO MOVIMENTO
      *----------------------------------------------------------------*
      *
       S10700-6101-LEGGI-PMO.
      *
           MOVE 'S10700-6101-LEGGI-PMO'
             TO WK-LABEL-ERR.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
           IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
      *  --> Chiave non trovata
      *
                       CONTINUE
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
                       MOVE IDSO0011-BUFFER-DATI
                         TO PARAM-MOVI
      *
                       SET WK-PMO-TROV-SI
                         TO TRUE
      *
                  WHEN OTHER
      *
      *  --> Errore Lettura
      *
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL-ERR
                         TO IEAI9901-LABEL-ERR
                       MOVE '005015'
                         TO IEAI9901-COD-ERRORE
                       STRING 'ERRORE LETTURA PARAM MOVI ' ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                       DELIMITED BY SIZE
                       INTO IEAI9901-PARAMETRI-ERR
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
      *
           ELSE
      *
      *  --> Errore Dispatcher
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              MOVE 'ERRORE DISPATCHER LETTURA PARAM MOVI'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
       S10700-6101-LEGGI-PMO-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  LETTURA DELLA TABELLA PARAMETRO MOVIMENTO
      *----------------------------------------------------------------*
      *
       GESTIONE-RIVAL.
      *
           MOVE 'GESTIONE-RIVAL'
             TO WK-LABEL-ERR.
      *
           PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
             UNTIL IX-TAB-PMO > WPMO-ELE-PMO-MAX
      *
               PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
                  UNTIL IX-TAB-GRZ > WGRZ-ELE-GRZ-MAX
      *
                    IF WPMO-ID-OGG(IX-TAB-PMO) =
                       WGRZ-ID-GAR(IX-TAB-GRZ)
      *
                       PERFORM S6006-CALC-PROX-RIC
                          THRU S6006-CALC-PROX-RIC-EX
      *
                    END-IF
      *
               END-PERFORM
      *
           END-PERFORM.
      *
       GESTIONE-RIVAL-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *            CALCOLA LA DATA DI PROSSIMA RICORRENZA              *
      *----------------------------------------------------------------*
      *
       S6006-CALC-PROX-RIC.
      *
           MOVE 'S6006-CALC-PROX-RIC'
             TO WK-LABEL-ERR.
      *
           SET WPMO-ST-MOD(IX-TAB-PMO)
             TO TRUE.
      *
           MOVE WGRZ-ID-GAR(IX-TAB-GRZ)  TO   LDBV1131-ID-OGG
           MOVE 'GA'                      TO   LDBV1131-TP-OGG
           MOVE 'PERIODADEG'              TO   LDBV1131-COD-PARAM
           PERFORM S1820-LETTURA-POG      THRU EX-S1820
      *
           MOVE WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
             TO WPMO-DT-RICOR-PREC(IX-TAB-PMO).
      *
      * A SECONDA DELLA PRESENZA DEL PARAMETRO PERIODADEG BISOGNERu
      * AGGIUNGERE UN ANNO OPPURE UN PERIODO IN MESI ALLA DATA
      * DECORRENZA SUCCESSIVA
      *
           PERFORM S6006-AGGIUNGI-ANNO
              THRU S6006-AGGIUNGI-ANNO-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              MOVE A2K-OUAMG
                TO WK-APPO-DT-RICOR-SUCC
      *
      * --> Se la data di scadenza h minore della prossima ricorrenza
      * --> calcolata, allora la data di prossima ricorrenza h proprio
      * --> quella di scadenza
      *
              IF WGRZ-DT-SCAD-NULL(IX-TAB-GRZ) NOT = HIGH-VALUES
      *
                 IF WGRZ-DT-SCAD(IX-TAB-GRZ) <
                    WK-APPO-DT-RICOR-SUCC
      *
NEW                 PERFORM VERIFICA-STATO-POL
NEW                    THRU VERIFICA-STATO-POL-EX
      *
                    IF IDSV0001-ESITO-OK AND (
48913 *                DIFFERITA
48913 *             OR PROROGATA
48913                  PROROGATA
                    OR ATTESA-COMUNIC-PAG-REN
                    OR ATTESA-LIQ-RENDITA
12193               OR SINISTRO-RENDITA-DA-EROGARE
                    OR ATTESA-LIQ-REN-PRIMO-ASSIC
                    OR ATTESA-COM-PAG-REN-PRIMO-ASS
                    OR RENDITA-EROG-REVISIONARIO
                    OR RENDITA-EROGAZIONE )
                    AND STB-TP-STAT-BUS = 'VI'

NEW                    MOVE WK-APPO-DT-RICOR-SUCC
NEW                      TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)

      *
                    ELSE

NEW                    MOVE WGRZ-DT-SCAD(IX-TAB-GRZ)
NEW                      TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
      *
      *
                    END-IF
      *
                 ELSE
      *
                    MOVE WK-APPO-DT-RICOR-SUCC
                      TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
      *
                 END-IF
      *
28762            IF WPMO-DT-RICOR-SUCC(IX-TAB-PMO) =
28762               WPMO-DT-RICOR-PREC(IX-TAB-PMO)
28762               SET WPMO-ST-DEL(IX-TAB-PMO) TO TRUE
28762            END-IF
      *
              ELSE
      *
                 MOVE WK-APPO-DT-RICOR-SUCC
                   TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
      *
              END-IF
      *
           END-IF.
      *
       S6006-CALC-PROX-RIC-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  RICORRENZA SUCCESSIVA LETTA IN INPUT INCREMENTATA DI UN ANNO  *
      *----------------------------------------------------------------*
      *
       S6006-AGGIUNGI-ANNO.
      *
           MOVE ZEROES
             TO WK-APPO-DT-RICOR-SUCC.
      *
      * --> A2K-INAMG -> Data Di Partenza Alla Quale Sommare Il Delta
      *
           MOVE WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
             TO A2K-INAMG.
      *
      * --> A2K-TDELTA -> Tipo Delta: Anni(A)
      *
           IF WK-NON-TROVATO-PERIODADEG
              MOVE 'A'
                TO A2K-TDELTA
           ELSE
              MOVE 'M'
                TO A2K-TDELTA
           END-IF
      *
      * --> A2K-DELTA  -> Delta Da Sommare
      *
           IF WK-NON-TROVATO-PERIODADEG
              MOVE 1
                TO A2K-DELTA
           ELSE
              IF WGRZ-FRAZ-DECR-CPT-NULL(IX-TAB-GRZ) = HIGH-VALUE
                 MOVE 1 TO WGRZ-FRAZ-DECR-CPT(IX-TAB-GRZ)
              END-IF

              EVALUATE WGRZ-FRAZ-DECR-CPT(IX-TAB-GRZ)
              WHEN 1
                   MOVE 12 TO A2K-TDELTA
              WHEN 2
                   MOVE 6  TO A2K-TDELTA
              WHEN 3
                   MOVE 4  TO A2K-TDELTA
              WHEN 4
                   MOVE 3  TO A2K-TDELTA
              WHEN 6
                   MOVE 2  TO A2K-TDELTA
              WHEN OTHER
                  MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S6006-AGGIUNGI-ANNO' TO IEAI9901-LABEL-ERR
                  MOVE '005016'              TO IEAI9901-COD-ERRORE
                  STRING 'FRAZIONAMENTO DECRESCENZA CAPITALE ERRATO' ';'
                       IDSO0011-RETURN-CODE ';'
                       IDSO0011-SQLCODE
                       DELIMITED BY   SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE THRU EX-S0300
              END-EVALUATE
           END-IF
      *
      * --> A2K-FUNZ -> Tipo Funzione: Somma Delta
      *
           MOVE '02'
             TO A2K-FUNZ.
      *
      * --> A2K-INFO -> Tipo Formato Data: AAAAMMGG
      *
           MOVE '03'
             TO A2K-INFO.
      *
      * --> A2K-INICON -> Giorno Inizio Conteggio (Stesso Giorno)
      *
           MOVE '0'
             TO A2K-INICON.
      *
      * --> A2K-FISLAV -> Tipo Giorni Da Sommare (Fissi)
      *
           MOVE '0'
             TO A2K-FISLAV.
      *
           PERFORM CALL-LCCS0003
              THRU CALL-LCCS0003-EX.
      *
       S6006-AGGIUNGI-ANNO-EX.
           EXIT.
      *
MIKE  *----------------------------------------------------------------*
MIKE  *   VERIFICA STATO POLIZZA
MIKE  *----------------------------------------------------------------*
MIKE   VERIFICA-STATO-POL.

           INITIALIZE IDSI0011-AREA.

           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZERO                     TO IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-TRATT-DEFAULT    TO TRUE
           SET IDSI0011-SELECT           TO TRUE
           SET IDSI0011-ID-OGGETTO       TO TRUE

           MOVE 'PO'                     TO STB-TP-OGG
           MOVE WPOL-ID-POLI             TO STB-ID-OGG

           MOVE SPACES                   TO IDSI0011-BUFFER-DATI
           MOVE STAT-OGG-BUS             TO IDSI0011-BUFFER-DATI

           MOVE 'STAT-OGG-BUS'           TO IDSI0011-CODICE-STR-DATO

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE

                  WHEN IDSO0011-SUCCESSFUL-SQL
                     MOVE IDSO0011-BUFFER-DATI TO STAT-OGG-BUS

                     MOVE STB-TP-CAUS TO WS-TP-CAUS

                  WHEN OTHER
      *
      *  --> Errore Lettura
      *
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL-ERR
                         TO IEAI9901-LABEL-ERR
                       MOVE '005015'
                         TO IEAI9901-COD-ERRORE
                       STRING 'ERRORE LETTURA STAT-OGG-BUS'';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                       DELIMITED BY SIZE
                       INTO IEAI9901-PARAMETRI-ERR
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'VERIFICA-STATO-POL'    TO IEAI9901-LABEL-ERR
              MOVE '005016'                TO IEAI9901-COD-ERRORE
              STRING 'STAT-OGG-BUS'        ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY   SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE THRU EX-S0300
           END-IF.

MIKE   VERIFICA-STATO-POL-EX.
MIKE       EXIT.
      *----------------------------------------------------------------*
      *   LETTURA PARAMETRO OGGETTO
      *----------------------------------------------------------------*
       S1820-LETTURA-POG.

           INITIALIZE IDSI0011-AREA.

           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZERO                     TO IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-TRATT-DEFAULT    TO TRUE
           SET IDSI0011-SELECT           TO TRUE
           SET IDSI0011-WHERE-CONDITION  TO TRUE

           MOVE SPACES                   TO IDSI0011-BUFFER-DATI
           MOVE PARAM-OGG                TO IDSI0011-BUFFER-DATI

           MOVE 'LDBS1130'               TO IDSI0011-CODICE-STR-DATO
           MOVE  AREA-LDBV1131           TO IDSI0011-BUFFER-WHERE-COND.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                     SET WK-NON-TROVATO-PERIODADEG TO TRUE

                  WHEN IDSO0011-SUCCESSFUL-SQL
                     SET WK-TROVATO-PERIODADEG  TO TRUE
                     MOVE IDSO0011-BUFFER-DATI TO PARAM-OGG
              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1820-LETTURA-POG'     TO IEAI9901-LABEL-ERR
              MOVE '005016'                TO IEAI9901-COD-ERRORE
              STRING 'PARAM-OGG  '        ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY   SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE THRU EX-S0300
           END-IF.

       EX-S1820.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE SPOSTAMENTO DATA PER OPZIONI A SCADENZA
      *----------------------------------------------------------------*
       GESTIONE-SCADE.

           PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
             UNTIL IX-TAB-PMO > WPMO-ELE-PMO-MAX
      *
             IF WPMO-TP-OGG(IX-TAB-PMO) = 'AD'
                IF WPMO-TP-OPZ-NULL(IX-TAB-PMO) = HIGH-VALUES
                   CONTINUE
                ELSE
                   MOVE WPMO-TP-OPZ(IX-TAB-PMO)
                     TO WK-STR-OPZIONE
                   MOVE WK-COD-OPZIONE
                     TO WK-TP-OPZIONI
                END-IF
      *
                IF WK-DIFFERIMENTO
                OR WK-PROROGA
                   IF WADE-DT-SCAD-NULL(1) = HIGH-VALUES
                      CONTINUE
                   ELSE
                      SET WPMO-ST-MOD(IX-TAB-PMO)
                       TO TRUE
                      MOVE WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                        TO WPMO-DT-RICOR-PREC(IX-TAB-PMO)
                      MOVE WADE-DT-SCAD(1)
                        TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                      IF WK-PROROGA
                       IF WPMO-TOT-AA-GIA-PROR-NULL(IX-TAB-PMO) =
                                                       HIGH-VALUES
                          MOVE WPMO-DUR-AA(IX-TAB-PMO)
                            TO WPMO-TOT-AA-GIA-PROR(IX-TAB-PMO)
                       ELSE
                        COMPUTE WPMO-TOT-AA-GIA-PROR(IX-TAB-PMO) =
                                WPMO-TOT-AA-GIA-PROR(IX-TAB-PMO) +
                                WPMO-DUR-AA(IX-TAB-PMO)
                       END-IF
                      END-IF
                      MOVE HIGH-VALUES TO
                                 WPMO-DUR-AA-NULL(IX-TAB-PMO)
                                 WPMO-TP-OPZ-NULL(IX-TAB-PMO)
                                 WPMO-DUR-MM-NULL(IX-TAB-PMO)

                    END-IF
                 END-IF
             END-IF

           END-PERFORM.

       GESTIONE-SCADE-EX.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE SPOSTAMENTO DATA PER BONUS RICORRENTE E FEDELTA'
      *----------------------------------------------------------------*
       GESTIONE-BONUS.

16244      PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
16244        UNTIL IX-TAB-PMO > WPMO-ELE-PMO-MAX
      *
             SET WPMO-ST-MOD(IX-TAB-PMO) TO TRUE
             MOVE WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
               TO WPMO-DT-RICOR-PREC(IX-TAB-PMO)
             MOVE WCOM-DATA-RICOR-SUCC
               TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
      *
16244      END-PERFORM.

       GESTIONE-BONUS-EX.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE SPOSTAMENTO DATA PER ATTUAZIONE STRATEGIA
      *----------------------------------------------------------------*
       GESTIONE-ATT-STRA.

             IF WCOM-GESTIONE-ANNIVERSARIA AND
                WCOM-ANNUALE
                PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
                  UNTIL IX-TAB-PMO > WPMO-ELE-PMO-MAX
                     PERFORM S6210-AGGIUNGI-ANNO
                        THRU S6210-AGGIUNGI-ANNO-EX
                     IF IDSV0001-ESITO-OK
35581                   MOVE A2K-OUAMG
35581                    TO  WK-DT-CALCOLATA
                           WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
VERIF *            MOVE WK-DT-CALCOLATA TO S029-DT-CALC
VERIF *            PERFORM S10210-6101-VALID-FIN-MESE
VERIF *               THRU S10210-6101-VALID-FIN-MESE-EX

                     END-IF
                END-PERFORM
             END-IF.

       GESTIONE-ATT-STRA-EX.
            EXIT.
      *--------------------------------------------------------------
      * LA ROUTINE AGGIUNGE UN ANNO ALLA RICORRENZA E CONTROLLA IL
      * IL FINE MESE
      *--------------------------------------------------------------
       S6210-AGGIUNGI-ANNO.
      *
           MOVE ZEROES
             TO WK-APPO-DT-RICOR-SUCC.
      *
      * --> A2K-INAMG -> Data Di Partenza Alla Quale Sommare Il Delta
      *
           MOVE WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
             TO A2K-INAMG.
      *
      * --> A2K-TDELTA -> Tipo Delta: Anni(A)
              MOVE 'A'
                TO A2K-TDELTA
      *
      * --> A2K-DELTA  -> Delta Da Sommare
      *
              MOVE 1
                TO A2K-DELTA
      *
      * --> A2K-FUNZ -> Tipo Funzione: Somma Delta
      *
           MOVE '02'
             TO A2K-FUNZ.
      *
      * --> A2K-INFO -> Tipo Formato Data: AAAAMMGG
      *
           MOVE '03'
             TO A2K-INFO.
      *
      * --> A2K-INICON -> Giorno Inizio Conteggio (Stesso Giorno)
      *
           MOVE '0'
             TO A2K-INICON.
      *
      * --> A2K-FISLAV -> Tipo Giorni Da Sommare (Fissi)
      *
           MOVE '0'
             TO A2K-FISLAV.
      *
           PERFORM CALL-LCCS0003
              THRU CALL-LCCS0003-EX.
      *
       S6210-AGGIUNGI-ANNO-EX.
           EXIT.
      *

      * ============================================================== *
      * -->           O P E R Z I O N I   F I N A L I              <-- *
      * ============================================================== *
      *
       S9000-OPERAZ-FINALI.
      *
            GOBACK.
      *
       EX-S9000.
           EXIT.
      *
      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORI
      *----------------------------------------------------------------*
      *
           COPY IERP9901.
           COPY IERP9902.
      *
      *----------------------------------------------------------------*
      *    ROUTINES DISPATCHER
      *----------------------------------------------------------------*
      *
           COPY IDSP0012.
      *



      ******************************************************************
      **                                                        ********
      **    PORTAFOGLIO VITA ITALIA  VER 1.0                    ********
      **                                                        ********
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         IDSS0160.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2009.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *    PROGRAMMA ..... IDSS0160
      *    TIPOLOGIA...... SERVIZIO TRASVERSALE
      *    PROCESSO....... XXX
      *    FUNZIONE....... XXX
      *    DESCRIZIONE.... ESTRAZIONE PROPOSTA OGGETTO
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                          PIC X(008) VALUE 'IDSS0160'.

      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WS-VARIABILI.
           03 WS-TABELLA                   PIC X(30).
           03 WS-VALORE                    PIC X(50).
           03 WS-VALORE-ALL                PIC X(50).
           03 WS-VALORE-P                  PIC X(50).
           03 WS-KEY-OGGETTO               PIC X(40).
           03 WS-VALORE-NUM                PIC 9(18).
           03 WS-LUNGHEZZA-VALORE          PIC 9(05).
           03 WS-LUNGHEZZA-CAMPO           PIC 9(05).
           03 WS-POSIZIONE-VAL             PIC 9(05).
           03 WS-POSIZIONE-IB              PIC 9(05).
           03 WS-POSIZIONE-P               PIC 9(05).
           03 WS-POSIZIONE-APP             PIC 9(05).
           03 WS-LUNGHEZZA-P               PIC 9(05).
           03 WS-QTA-FLAG-K                PIC S9(04) COMP.
           03 WS-QTA-FLAG-P                PIC S9(04) COMP.
           03 WS-QTA-FLAG-J                PIC S9(04) COMP.
           03 WKS-AREA-TABB-APPO           PIC X(2500).
           03 WS-ID-NUM                    PIC 9(009).
           03 WS-LUNGHEZZA-VALORE-PR       PIC 9(05).
EG         03 WS-POSIZ-INI                 PIC 9(05).
      *
       01 WS-VALORE-APP                    PIC 9(18).
       01 WS-VALORE-STR  REDEFINES WS-VALORE-APP  PIC  X(018).
      *
       01 WS-TAB-APPO.
          05 TAB-APPO-1               OCCURS 50 TIMES.
              10 CARAT-T              PIC  X(001).
      *
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-TABB                      PIC S9(04) COMP.
           03 IX-AP-TABB                   PIC S9(04) COMP.
           03 IX-DATO                      PIC S9(04) COMP.
           03 IX-AP-DATO                   PIC S9(04) COMP.
           03 IX-IDSS0020                  PIC S9(04) COMP.
           03 IND-STR                      PIC S9(04) COMP.
           03 IX-TAB-CNO                   PIC S9(04) COMP.
           03 IX-TAB-RRE                   PIC S9(04) COMP.
           03 IX-TAB-VAL                   PIC S9(04) COMP.
           03 IX-WKS-TAB                   PIC S9(04) COMP.

      *----------------------------------------------------------------*
      *    FLAG
      *----------------------------------------------------------------*
       01  WS-FLAG-FINE-VALORE             PIC X(01).
           88 WS-SI-FINE-VALORE                       VALUE 'S'.
           88 WS-NO-FINE-VALORE                       VALUE 'N'.

       01  WK-FIND-CAMPO                   PIC X(01).
           88 WK-CAMPO-FIND-SI                        VALUE 'S'.
           88 WK-CAMPO-FIND-NO                        VALUE 'N'.

       01  WS-LETTURA                      PIC X(01).
           88 WS-PRIMA-LETTURA                        VALUE 'P'.
           88 WS-SECONDA-LETTURA                      VALUE 'S'.

       01  WKS-TROVATO-TABB                PIC X(01).
           88 WKS-TROVATO-TABB-SI                     VALUE 'S'.
           88 WKS-TROVATO-TABB-NO                     VALUE 'N'.

      *----------------------------------------------------------------*
      *    AREA TABELLE DI APPOGGIO
      *----------------------------------------------------------------*
       01  AREA-TAB-APPOGGIO.
      *--> AREA STRUTTURA TABELLE
           03 WKS-STRUTTURA-TAB.
              05 WKS-ELE-MAX-TABB                PIC S9(04) COMP-3.
              05 WKS-TAB-STR-DATO.
                 06 WKS-AREA-TABB                OCCURS 100.
                    07 WKS-NOME-TABB             PIC  X(30).
                    07 WKS-ELE-MAX-DATO          PIC S9(04) COMP-3.
                    07 WKS-ELEMENTS-STR-DATO     OCCURS 1000.
                       10 WKS-CODICE-DATO        PIC  X(30).
                       10 WKS-TIPO-DATO          PIC  X(02).
                       10 WKS-INI-POSI           PIC  9(05) COMP.
                       10 WKS-LUN-DATO           PIC  9(05) COMP.
                       10 WKS-LUNGHEZZA-DATO-NOM PIC  9(05) COMP.
                       10 WKS-PRECISIONE-DATO    PIC  9(02) COMP.
              05 WKS-TAB-STR-DATO-R REDEFINES WKS-TAB-STR-DATO.
                 09 FILLER                    PIC X(46033).
                 09 WKS-RESTO-TAB-STR-DATO    PIC X(4557267).
      *
       01  WKS-APPO-IDSI0011                PIC X(30462) VALUE SPACES.
      *----------------------------------------------------------------*
      *    COPY VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.

      *----------------------------------------------------------------*
      *    CAMPI X ROUTINES DI GESTIONE DISPLAY IDSP8888
      *----------------------------------------------------------------*
           COPY IDSV8888.


      *--> Area per modulo conversione stringa
       01  AREA-CALL-IWFS0050.
           COPY IWFI0051.
           COPY IWFO0051.

      *----------------------------------------------------------------*
      *    COPY PER TRASCODIFICA CAMPI COMP-3
      *----------------------------------------------------------------*
       01 AREA-IDSV0141.
           COPY IDSV0141.
      ******************************************************************
      *    COPY DISPATCHER
      ******************************************************************
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
      ** COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      ** COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.

      *----------------------------------------------------------------*
      *    COPY DELLE TABELLE DB2
      *----------------------------------------------------------------*
           COPY IDBVCNO1.
           COPY IDBVNOG1.
           COPY IDBVD031.
           COPY IDBVRRE1.
      *----------------------------------------------------------------*
      *    COPY ALIAS TABELLE
      *----------------------------------------------------------------*
       01  ALIAS-TABELLE.
           COPY IVVC0218                REPLACING ==(SF)== BY ==C161==.

      *----------------------------------------------------------------
      *    MODULI CHIAMATI
      *----------------------------------------------------------------
      *--> RECUPERA STRUTTURA TABELLA
       01  IDSS0020                         PIC X(008) VALUE 'IDSS0020'.
       01  IDSS0140                         PIC X(008) VALUE 'IDSS0140'.
       01  IWFS0050                         PIC X(008) VALUE 'IWFS0050'.

      *----------------------------------------------------------------*
      *    AREA IO RECUPERA STRUTTURA TABELLA IDSS0020
      *----------------------------------------------------------------*
       01  AREA-CALL.
           COPY IDSV0044.

       01  INPUT-IDSS0020.
           COPY IDSI0021.

       01  OUTPUT-IDSS0020.
           COPY IDSO0021.
      *----------------------------------------------------------------*
      *    COMMAREA SERVIZIO
      *----------------------------------------------------------------*
      *--  AREA POLIZZA
       01  WPOL-AREA-POLIZZA.
           04 WPOL-ELE-POLI-MAX          PIC S9(04) COMP.
           04 WPOL-TAB-POLI.
              COPY LCCVPOL1              REPLACING ==(SF)== BY ==WPOL==.

      *--  AREA ADESIONE
       01  WADE-AREA-ADESIONE.
           04 WADE-ELE-ADES-MAX          PIC S9(004) COMP.
           04 WADE-TAB-ADES.
              COPY LCCVADE1              REPLACING ==(SF)== BY ==WADE==.

      *--> AREA RAPPORTO RETE
       01  WRRE-AREA-RAPP-RETE.
           04 WRRE-ELE-RAPP-RETE-MAX     PIC S9(04) COMP.
           04 WRRE-TAB-RAPP-RETE         OCCURS 20.
              COPY LCCVRRE1              REPLACING ==(SF)== BY ==WRRE==.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.
      *--  AREA DI PAGINA
       01  WPAG-AREA-IO.
           COPY IDSV0161                 REPLACING ==(SF)== BY ==WPAG==.

      *---------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WPAG-AREA-IO.
      *---------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *    OPERAZIONI INIZIALI
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'IDSS0020'                     TO IDSV8888-NOME-PGM
           MOVE 'Initialize area di work'      TO IDSV8888-DESC-PGM
           SET  IDSV8888-INIZIO                TO TRUE
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           INITIALIZE                      WS-VARIABILI
                                           IX-INDICI
ALEX  *                                    AREA-TAB-APPOGGIO
                                           WKS-ELE-MAX-TABB
                                           WKS-NOME-TABB(1)
                                           WKS-ELE-MAX-DATO(1)
                                           PROGR-NUM-OGG.

           PERFORM VARYING IX-WKS-TAB FROM 1 BY 1
                   UNTIL   IX-WKS-TAB  >  1000
                   INITIALIZE   WKS-ELEMENTS-STR-DATO(1,IX-WKS-TAB)
           END-PERFORM.

           MOVE  WKS-TAB-STR-DATO  TO  WKS-RESTO-TAB-STR-DATO.

      *--> VALORIZZA TAB INPUT
           PERFORM A0010-VALORIZZA-DCLGEN  THRU A0010-EX
                   VARYING IND-STR FROM 1 BY 1
                     UNTIL IND-STR > WPAG-ELE-INFO-MAX OR
                           WPAG-TAB-ALIAS(IND-STR) =
                      SPACES OR LOW-VALUE OR HIGH-VALUE.
      *

           PERFORM INIZIA-TOT-D03
              THRU INIZIA-TOT-D03-EX.

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'IDSS0020'                     TO IDSV8888-NOME-PGM
           MOVE 'Initialize area di work'      TO IDSV8888-DESC-PGM
           SET  IDSV8888-FINE                  TO TRUE
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM S0005-CTRL-DATI-INPUT
              THRU EX-S0005.

       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTROLLO DATI INPUT
      *----------------------------------------------------------------*
       S0005-CTRL-DATI-INPUT.

      *--> CODICE COMPAGNIA OBBLIGATORIO
           IF IDSV0001-COD-COMPAGNIA-ANIA = ZEROES
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0005-CTRL-DATI-INPUT'
                TO IEAI9901-LABEL-ERR
              MOVE '005026'
                TO IEAI9901-COD-ERRORE
              MOVE 'COD-COMP-ANIA'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

      *--> TIPO FORMA ASSICURATIVA OBBLIGATORIO
           IF WPOL-TP-FRM-ASSVA = SPACES OR HIGH-VALUES
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0005-CTRL-DATI-INPUT'
                TO IEAI9901-LABEL-ERR
              MOVE '005026'
                TO IEAI9901-COD-ERRORE
              MOVE 'TP-FRM-ASSVA'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

      *--> CODICE OGGETTO OBBLIGATORIO
           IF WPAG-COD-OGGETTO = SPACES OR HIGH-VALUES
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0005-CTRL-DATI-INPUT'
                TO IEAI9901-LABEL-ERR
              MOVE '005026'
                TO IEAI9901-COD-ERRORE
              MOVE 'COD-OGGETTO'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

      *--> NEL CASO DI NUMERAZIONE MANUALE DEVE ESSERE VALORIZZATO IL
      *--> CAMPO IB-OGGETTO-I
           IF WPAG-MANUALE
              IF WPAG-IB-OGGETTO-I = SPACES OR HIGH-VALUE
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S0005-CTRL-DATI-INPUT'
                   TO IEAI9901-LABEL-ERR
                 MOVE '005026'
                   TO IEAI9901-COD-ERRORE
                 MOVE 'IB-OGGETTO-I'
                   TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

      *--> NEL CASO DI NUMERAZIONE AUTOMATICA IL CAMPO IB-OGGETTO-I
      *--> DEVE ESSERE VALORIZZATO A SPACES
           IF WPAG-AUTOMATICA
              IF WPAG-IB-OGGETTO-I NOT = SPACES
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S0005-CTRL-DATI-INPUT'
                   TO IEAI9901-LABEL-ERR
                 MOVE '005008'
                   TO IEAI9901-COD-ERRORE
                 MOVE 'IB-OGGETTO-I'
                   TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S0005.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.
      *
           MOVE 1                          TO WS-POSIZIONE-IB.
           SET WS-PRIMA-LETTURA            TO TRUE.
      *
      *--> LETTURA DELLA TABELLA COMP-NUM-OGG PER COMPORRE L'IB-OGGETTO
      *
           PERFORM S1100-LEGGI-COMP-NUM-OGG
              THRU EX-S1100.
      *
           IF WS-SECONDA-LETTURA
           AND IDSV0001-ESITO-OK
              PERFORM S1100-LEGGI-COMP-NUM-OGG
                 THRU EX-S1100
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA DELLA TABELLA COMP-NUM-OGG PER COMPORRE L'IB-OGGETTO
      *----------------------------------------------------------------*
       S1100-LEGGI-COMP-NUM-OGG.
      *
           PERFORM S1150-PREPARA-CNO            THRU EX-S1150.
      *
           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC  OR
                         NOT IDSO0011-SUCCESSFUL-SQL
      *
             PERFORM CALL-DISPATCHER
                THRU CALL-DISPATCHER-EX

             IF IDSO0011-SUCCESSFUL-RC
                EVALUATE TRUE

                    WHEN IDSO0011-SUCCESSFUL-SQL
      *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                         PERFORM S1101-LETTURA-CNO-OK
                            THRU EX-S1101
      *
                    WHEN IDSO0011-NOT-FOUND
      *--->         CHIAVE NON TROVATA SU TABELLA $
                         PERFORM S1102-LETTURA-CNO-100
                            THRU EX-S1102

                    WHEN OTHER
      *-->          ERRORE DI ACCESSO AL DB
                         PERFORM S9001-ERRORE-COMUNE
                            THRU EX-S9001
                END-EVALUATE
             ELSE
      *-->      ERRORE DISPATCHER
                PERFORM S9001-ERRORE-COMUNE      THRU EX-S9001
             END-IF
           END-PERFORM.

       EX-S1100.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE DELL'OUTPUT DELLA TABELLA CON LETTURA PROGR-OGG
      *----------------------------------------------------------------*
       S1150-PREPARA-CNO.
      *
           MOVE 'COMP-NUM-OGG'           TO WS-TABELLA.
      *--> DATA EFFETTO
           MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.
      *--> TIPO OPERAZIONE
           SET IDSI0011-FETCH-FIRST      TO TRUE.

      *--> INIZIALIZZAZIONE FLAG ESITO
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

      *--> TABELLA STORICA
           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.

      *--> MODALITA DI ACCESSO
           SET IDSI0011-WHERE-CONDITION  TO TRUE.

      *--> NOME TABELLA FISICA DB
           MOVE 'LDBS4870'               TO IDSI0011-CODICE-STR-DATO.

      *--> VALORIZZA DCLGEN TABELLA
           INITIALIZE                       COMP-NUM-OGG.
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
                                         TO CNO-COD-COMPAGNIA-ANIA.
           IF WS-SECONDA-LETTURA
              MOVE 'EN'                  TO CNO-FORMA-ASSICURATIVA
           ELSE
              MOVE WPOL-TP-FRM-ASSVA     TO CNO-FORMA-ASSICURATIVA
           END-IF.
           MOVE WPAG-COD-OGGETTO         TO CNO-COD-OGGETTO.

      *--> DCLGEN TABELLA
           MOVE COMP-NUM-OGG             TO IDSI0011-BUFFER-DATI.
           MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
      *
       EX-S1150.
           EXIT.
      *----------------------------------------------------------------*
      *   GESTIONE LETTURA OK
      *----------------------------------------------------------------*
       S1101-LETTURA-CNO-OK.
      *
           MOVE IDSO0011-BUFFER-DATI       TO COMP-NUM-OGG.
           MOVE IDSI0011-AREA              TO WKS-APPO-IDSI0011.
           ADD 1                           TO IX-TAB-CNO.

           IF CNO-FLAG-KEY-ULT-PROGR = 'P'
              PERFORM S110B-GEST-OUTPUT-CNO-P
                 THRU EX-S110B
           ELSE
              PERFORM S110A-GEST-OUTPUT-CNO
                THRU EX-S110A
           END-IF.
      *
           MOVE WKS-APPO-IDSI0011          TO IDSI0011-AREA.
           SET IDSI0011-FETCH-NEXT         TO TRUE.
      *
       EX-S1101.
           EXIT.
      *----------------------------------------------------------------*
      *   GESTIONE LETTURA NOT FOUND
      *----------------------------------------------------------------*
       S1102-LETTURA-CNO-100.
      *
           IF IDSI0011-FETCH-FIRST
              IF WS-PRIMA-LETTURA
                 SET WS-SECONDA-LETTURA             TO TRUE
              ELSE
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S1100-LEGGI-COMP-NUM-OGG'
                   TO IEAI9901-LABEL-ERR
                 MOVE '005056'
                   TO IEAI9901-COD-ERRORE
                 MOVE 'COMP-NUM-OGG'
                   TO IEAI9901-PARAMETRI-ERR
      *
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           ELSE
              IF IX-TAB-CNO = 1
              OR WS-QTA-FLAG-P GREATER 1
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S1100-LEGGI-COMP-NUM-OGG'
                   TO IEAI9901-LABEL-ERR
                 IF IX-TAB-CNO = 1
                    MOVE '001114'
                      TO IEAI9901-COD-ERRORE
                    MOVE 'UNA SOLA OCCORR TROVATA SU COMP-NUM-OGG'
                      TO IEAI9901-PARAMETRI-ERR
                 END-IF
                 IF WS-QTA-FLAG-P GREATER 1
                    MOVE '001114'
                      TO IEAI9901-COD-ERRORE
                    MOVE 'PIU OCCORR TROV SU COMP-NUM-OGG FLAG = P'
                      TO IEAI9901-PARAMETRI-ERR
                 END-IF
      *
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
              IF IDSV0001-ESITO-OK
                 IF WS-QTA-FLAG-K GREATER ZERO OR
                    WS-QTA-FLAG-J GREATER ZERO
                    PERFORM S1116-GEST-PROGR-NUM-OGG
                       THRU EX-S1116
                 ELSE
                    PERFORM S1115-GEST-NUM-OGG
                       THRU EX-S1115
                 END-IF
              END-IF
           END-IF.
      *
           SET IDSO0011-NOT-FOUND           TO TRUE.
      *
       EX-S1102.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE DELL'OUTPUT DELLA TABELLA CON FLAG = K OPPURE J
      *----------------------------------------------------------------*
       S110A-GEST-OUTPUT-CNO.
      *
           IF CNO-FLAG-KEY-ULT-PROGR = 'K'
              ADD 1                      TO WS-QTA-FLAG-K
           END-IF.
      *
           IF CNO-FLAG-KEY-ULT-PROGR = 'J'
              ADD 1                      TO WS-QTA-FLAG-J
           END-IF.

           IF CNO-VALORE-DEFAULT-NULL = HIGH-VALUE
      *-->
              IF CNO-COD-DATO-NULL NOT = HIGH-VALUE AND
                 CNO-COD-STR-DATO-NULL = HIGH-VALUE
                 PERFORM S110C-VALORIZZA-DATO-STATICO
                    THRU EX-S110C
              ELSE
                 PERFORM S2000-VERIFICA-DATO        THRU EX-S2000
      *-->
                 IF IDSV0001-ESITO-OK
                    PERFORM S3000-VALORIZZA-DATO    THRU EX-S3000
                 END-IF
              END-IF
           ELSE
              MOVE CNO-VALORE-DEFAULT             TO WS-VALORE
      *-->
              IF CNO-LUNGHEZZA-DATO-NULL = HIGH-VALUE
      *-->       CALCOLA LUNGHEZZA
                 PERFORM S4000-CALCOLA-LUNGHEZZA  THRU EX-S4000
              ELSE
                 MOVE CNO-LUNGHEZZA-DATO      TO WS-LUNGHEZZA-VALORE
              END-IF
           END-IF.

      *--> CONCATENA IB OGGETTO
           IF IDSV0001-ESITO-OK

              IF CNO-FLAG-KEY-ULT-PROGR NOT = 'J'
                 PERFORM S5000-CONCATENA-IB-OGG   THRU EX-S5000
              END-IF

              IF CNO-FLAG-KEY-ULT-PROGR = 'K' OR 'J'
                 PERFORM S5001-CONCATENA-KEY      THRU EX-S5001
              END-IF

              COMPUTE WS-POSIZIONE-IB = WS-POSIZIONE-IB +
                                        WS-LUNGHEZZA-VALORE

           END-IF.
      *
           IF CNO-FLAG-KEY-ULT-PROGR NOT = 'J'
              ADD WS-LUNGHEZZA-VALORE TO WS-LUNGHEZZA-VALORE-PR
           END-IF.

       EX-S110A.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE DELL'OUTPUT DELLA TABELLA CON FLAG = P
      *----------------------------------------------------------------*
       S110B-GEST-OUTPUT-CNO-P.
      *
            ADD 1                           TO WS-QTA-FLAG-P.
      *
            IF CNO-VALORE-DEFAULT-NULL = HIGH-VALUE
               IF CNO-LUNGHEZZA-DATO-NULL = HIGH-VALUE
                  MOVE ZERO                 TO WS-LUNGHEZZA-P
               ELSE
                  MOVE CNO-LUNGHEZZA-DATO   TO WS-LUNGHEZZA-P
               END-IF
           ELSE
               MOVE CNO-VALORE-DEFAULT      TO WS-VALORE-P
      *-->
               IF CNO-LUNGHEZZA-DATO-NULL = HIGH-VALUE
      *-->        CALCOLA LUNGHEZZA
                  PERFORM S4000-CALCOLA-LUNGHEZZA  THRU EX-S4000
                  MOVE WS-LUNGHEZZA-VALORE     TO WS-LUNGHEZZA-P
               ELSE
                  MOVE CNO-LUNGHEZZA-DATO      TO WS-LUNGHEZZA-P
               END-IF
           END-IF.
      *
           IF WPAG-IB-OGGETTO = SPACES OR HIGH-VALUE
              MOVE 1                           TO WS-POSIZIONE-P
           ELSE
              MOVE WS-POSIZIONE-IB             TO WS-POSIZIONE-P
           END-IF.
      *
           COMPUTE WS-POSIZIONE-IB = WS-POSIZIONE-IB + WS-LUNGHEZZA-P.
      *
       EX-S110B.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA IL DATO IN MANIERA STATICA PRENDENDOLO DAL
      *    CONTESTO E/O DALL' INTERFACCIA
      *----------------------------------------------------------------*
       S110C-VALORIZZA-DATO-STATICO.

      * TIPO NUMERAZIONE
           IF CNO-COD-DATO = 'TIPO-NUMERAZIONE'
              MOVE WPAG-TIPO-NUMERAZIONE TO WS-VALORE
              IF CNO-LUNGHEZZA-DATO-NULL = HIGH-VALUE
                 PERFORM S4000-CALCOLA-LUNGHEZZA
                    THRU EX-S4000
              ELSE
                 MOVE CNO-LUNGHEZZA-DATO
                   TO WS-LUNGHEZZA-VALORE
              END-IF
           END-IF.

       EX-S110C.
           EXIT.

      *----------------------------------------------------------------*
      *    GESTIONE DELL'OUTPUT DELLA TABELLA CON LETTURA NUM-OGG
      *----------------------------------------------------------------*
       S1115-GEST-NUM-OGG.
      *--> AGGIORNA IB OGGETTO
           IF WS-VALORE-P NOT = SPACES
              MOVE WS-VALORE-P              TO WS-VALORE-APP
              PERFORM S6000-AGGIORNA-IB-OGG
                 THRU EX-S6000
           ELSE
      *--> LETTURA DELLA TABELLA NUM-OGG PER ESTRARRE IL SEQUENCE
              PERFORM S115A-LEGGI-NUM-OGG   THRU EX-S115A
      *
              IF IDSV0001-ESITO-OK
                 PERFORM S6000-AGGIORNA-IB-OGG
                    THRU EX-S6000
              END-IF
      *
           END-IF.

       EX-S1115.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA DELLA TABELLA NUM-OGG PER ESTRARRE IL SEQUENCE
      *----------------------------------------------------------------*
       S115A-LEGGI-NUM-OGG.
      *
           PERFORM S115B-PREPARA-NOG          THRU EX-S115B.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI
                         TO NUM-OGG

      *-->             GESTIONE DELL'OUTPUT DELLA TABELLA
                       PERFORM S115C-GEST-OUTPUT-NOG
                          THRU EX-S115C

                  WHEN IDSO0011-NOT-FOUND
      *--->       CHIAVE NON TROVATA SU TABELLA $
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S1111-LEGGI-NUM-OGG'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005056'
                         TO IEAI9901-COD-ERRORE
                       MOVE 'NUM-OGG'
                         TO IEAI9901-PARAMETRI-ERR
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300

                  WHEN OTHER
      *-->        ERRORE DI ACCESSO AL DB
                       PERFORM S9001-ERRORE-COMUNE
                          THRU EX-S9001
              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              PERFORM S9001-ERRORE-COMUNE        THRU EX-S9001
           END-IF.

       EX-S115A.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE DELL'OUTPUT DELLA TABELLA
      *----------------------------------------------------------------*
       S115B-PREPARA-NOG.
      *
           MOVE 'NUM-OGG'                TO WS-TABELLA.
      *--> DATA EFFETTO
           MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

      *--> INIZIALIZZAZIONE FLAG ESITO
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

      *--> TABELLA STORICA
           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.

      *--> MODALITA DI ACCESSO
           SET IDSI0011-WHERE-CONDITION  TO TRUE.

      *--> TIPO OPERAZIONE
           SET IDSI0011-FETCH-FIRST      TO TRUE.

      *--> NOME TABELLA FISICA DB
           MOVE 'LDBS4880'               TO IDSI0011-CODICE-STR-DATO.

      *--> VALORIZZA DCLGEN TABELLA
           INITIALIZE                       NUM-OGG.
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
                                         TO NOG-COD-COMPAGNIA-ANIA.
           IF WS-SECONDA-LETTURA
              MOVE 'EN'                  TO NOG-FORMA-ASSICURATIVA
           ELSE
              MOVE WPOL-TP-FRM-ASSVA     TO NOG-FORMA-ASSICURATIVA
           END-IF
      *
           MOVE WPAG-COD-OGGETTO         TO NOG-COD-OGGETTO.
           MOVE 'PO'                     TO NOG-TIPO-OGGETTO.

      *--> DCLGEN TABELLA
           MOVE NUM-OGG                  TO IDSI0011-BUFFER-DATI.
           MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
      *
       EX-S115B.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE DELL'OUTPUT DELLA TABELLA
      *----------------------------------------------------------------*
       S115C-GEST-OUTPUT-NOG.
      *
           MOVE NOG-ULT-PROGR                   TO WS-VALORE-APP.

       EX-S115C.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE PROGR OGGETTO
      *----------------------------------------------------------------*
       S1116-GEST-PROGR-NUM-OGG.
      *
           IF WS-VALORE-P NOT = SPACES
              MOVE WS-VALORE-P               TO WS-VALORE-APP
              PERFORM S6000-AGGIORNA-IB-OGG
                 THRU EX-S6000
           ELSE
      *--> LETTURA PREVENTIVA DELLA TABELLA PROGR-NUM-OGG PER
      *--> CONTROLLARE IL RANGE DI PROGRESSIVI PRESENTI
              PERFORM S116E-LEGGI-PROGR-OGG
                 THRU EX-S116E

              IF IDSV0001-ESITO-OK
                 IF WPAG-MANUALE
                    PERFORM S1117-CONVERTI-CHAR
                       THRU EX-S1117
                    IF IDSV0001-ESITO-OK
                       MOVE IWFO0051-CAMPO-OUTPUT-DEFI  TO WS-ID-NUM
                       IF D03-PROGR-INIZIALE-NULL = HIGH-VALUE AND
                          D03-PROGR-FINALE-NULL = HIGH-VALUE
                          MOVE WK-PGM
                            TO IEAI9901-COD-SERVIZIO-BE
                          MOVE 'S1116-GEST-PROGR-NUM-OGG'
                            TO IEAI9901-LABEL-ERR
                          MOVE '005166'
                            TO IEAI9901-COD-ERRORE
                          MOVE 'RANGE DEI PROGRESSIVI NON VALORIZZATO'
                            TO IEAI9901-PARAMETRI-ERR
                          PERFORM S0300-RICERCA-GRAVITA-ERRORE
                             THRU EX-S0300
                       ELSE
                          IF WS-ID-NUM >= D03-PROGR-INIZIALE AND
                             WS-ID-NUM <= D03-PROGR-FINALE
                             MOVE WS-ID-NUM             TO WS-VALORE-APP
                             PERFORM S6000-AGGIORNA-IB-OGG
                                THRU EX-S6000
                          ELSE
                             MOVE WK-PGM
                               TO IEAI9901-COD-SERVIZIO-BE
                             MOVE 'S1116-GEST-PROGR-NUM-OGG'
                               TO IEAI9901-LABEL-ERR
                             MOVE '005166'
                               TO IEAI9901-COD-ERRORE
                             MOVE 'PROGRESSIVO NON COMPRESO NEL RANGE'
                               TO IEAI9901-PARAMETRI-ERR
                             PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                THRU EX-S0300
                          END-IF
                       END-IF
                    END-IF
                 ELSE
                    IF D03-PROGR-INIZIALE-NULL = HIGH-VALUE AND
                       D03-PROGR-FINALE-NULL = HIGH-VALUE
                       CONTINUE
                    ELSE
                       ADD 1 TO D03-ULT-PROGR
                       IF D03-ULT-PROGR >= D03-PROGR-INIZIALE AND
                          D03-ULT-PROGR <= D03-PROGR-FINALE
                          CONTINUE
                       ELSE
                          MOVE WK-PGM
                            TO IEAI9901-COD-SERVIZIO-BE
                          MOVE 'S1116-GEST-PROGR-NUM-OGG'
                            TO IEAI9901-LABEL-ERR
                          MOVE '005166'
                            TO IEAI9901-COD-ERRORE
                          MOVE 'PROGRESSIVO NON COMPRESO NEL RANGE'
                            TO IEAI9901-PARAMETRI-ERR
                          PERFORM S0300-RICERCA-GRAVITA-ERRORE
                             THRU EX-S0300
                       END-IF
                    END-IF
                    IF IDSV0001-ESITO-OK
      *--> LETTURA DELLA TABELLA PROGR-NUM-OGG PER ESTRARRE IL SEQUENCE
                       PERFORM S116A-LEGGI-PROGR-OGG-UPD
                          THRU EX-S116A
                       IF IDSV0001-ESITO-OK
                          PERFORM S6000-AGGIORNA-IB-OGG
                             THRU EX-S6000
                       END-IF
                    END-IF
                 END-IF
              END-IF
           END-IF.
      *
       EX-S1116.
           EXIT.

      *----------------------------------------------------------------*
      *    CONVERTI VALORE DA STRINGA IN NUMERICO
      *----------------------------------------------------------------*
       S1117-CONVERTI-CHAR.

           MOVE HIGH-VALUE          TO AREA-CALL-IWFS0050.

           ADD 1                    TO WS-LUNGHEZZA-VALORE-PR.
           MOVE WPAG-IB-OGGETTO-I(WS-LUNGHEZZA-VALORE-PR:WS-LUNGHEZZA-P)
             TO IWFI0051-ARRAY-STRINGA-INPUT.

           CALL IWFS0050            USING AREA-CALL-IWFS0050.

           MOVE IWFO0051-ESITO      TO IDSV0001-ESITO.
           MOVE IWFO0051-LOG-ERRORE TO IDSV0001-LOG-ERRORE.

       EX-S1117.
           EXIT.

      *----------------------------------------------------------------*
      *    LEGGE PROGR OGGETTO E POI AGGIORNA L' ULTIMO PROGRESSIVO
      *----------------------------------------------------------------*
       S116A-LEGGI-PROGR-OGG-UPD.
      *
           PERFORM S116B-PREPARA-D03          THRU EX-S116B.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI
                         TO PROGR-NUM-OGG

      *-->             GESTIONE DELL'OUTPUT DELLA TABELLA
                       PERFORM S116C-GEST-OUTPUT-D03
                          THRU EX-S116C

                  WHEN IDSO0011-NOT-FOUND
      *--->       CHIAVE NON TROVATA SU TABELLA $
                       MOVE '1'                 TO WS-VALORE
                       MOVE 1                   TO D03-ULT-PROGR
                                                   WS-VALORE-APP
                       PERFORM S116D-INSERT-D03
                          THRU EX-S116D
      *
                  WHEN OTHER
      *-->        ERRORE DI ACCESSO AL DB
                       PERFORM S9001-ERRORE-COMUNE
                          THRU EX-S9001
              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              PERFORM S9001-ERRORE-COMUNE        THRU EX-S9001
           END-IF.
      *
       EX-S116A.
           EXIT.
      *----------------------------------------------------------------*
      *   PEREPARA AREA LDBS
      *----------------------------------------------------------------*
       S116B-PREPARA-D03.
      *
           MOVE 'PROGR-NUM-OGG'          TO WS-TABELLA.
      *--> DATA EFFETTO
           MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.
      *--> TIPO OPERAZIONE
           SET IDSI0011-FETCH-FIRST      TO TRUE.

      *--> INIZIALIZZAZIONE FLAG ESITO
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

      *--> TABELLA STORICA
           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.

      *--> MODALITA DI ACCESSO
           SET IDSI0011-WHERE-CONDITION  TO TRUE.

      *--> NOME TABELLA FISICA DB
           MOVE 'LDBS4900'               TO IDSI0011-CODICE-STR-DATO.

      *--> VALORIZZA DCLGEN TABELLA
           INITIALIZE                       COMP-NUM-OGG.
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
                                         TO D03-COD-COMPAGNIA-ANIA.
           IF WS-SECONDA-LETTURA
              MOVE 'EN'                  TO D03-FORMA-ASSICURATIVA
           ELSE
              MOVE WPOL-TP-FRM-ASSVA     TO D03-FORMA-ASSICURATIVA
           END-IF.
      *
           MOVE WPAG-COD-OGGETTO         TO D03-COD-OGGETTO.
           MOVE WS-KEY-OGGETTO           TO D03-KEY-BUSINESS.

      *--> DCLGEN TABELLA
           MOVE PROGR-NUM-OGG            TO IDSI0011-BUFFER-DATI.
           MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
      *
       EX-S116B.
           EXIT.
      *----------------------------------------------------------------*
      *   GESTIONE PROGR-NUM-OGG
      *----------------------------------------------------------------*
       S116C-GEST-OUTPUT-D03.
      *
           MOVE D03-ULT-PROGR          TO WS-VALORE-APP.
      *
       EX-S116C.
           EXIT.
      *----------------------------------------------------------------*
      *   GESTIONE INSERT PROGR-NUM-OGG
      *----------------------------------------------------------------*
       S116D-INSERT-D03.
      *
           MOVE 'PROGR-NUM-OGG'          TO WS-TABELLA.
      *--> DATA EFFETTO
           MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.
      *--> TIPO OPERAZIONE
           SET IDSI0011-INSERT           TO TRUE.

      *--> INIZIALIZZAZIONE FLAG ESITO
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

      *--> TABELLA STORICA
           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.

      *--> MODALITA DI ACCESSO
           SET IDSI0011-PRIMARY-KEY      TO TRUE.

      *--> NOME TABELLA FISICA DB
           MOVE 'PROGR-NUM-OGG'          TO IDSI0011-CODICE-STR-DATO.

      *--> DCLGEN TABELLA
           MOVE PROGR-NUM-OGG            TO IDSI0011-BUFFER-DATI.
           MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                  WHEN IDSO0011-NOT-FOUND
                     CONTINUE
                  WHEN OTHER
      *-->        ERRORE DI ACCESSO AL DB
                       PERFORM S9001-ERRORE-COMUNE
                          THRU EX-S9001
              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              PERFORM S9001-ERRORE-COMUNE        THRU EX-S9001
           END-IF.
      *
       EX-S116D.
           EXIT.

      *----------------------------------------------------------------*
      *    LEGGURA PROGR OGGETTO
      *----------------------------------------------------------------*
       S116E-LEGGI-PROGR-OGG.

           MOVE 'PROGR-NUM-OGG'          TO WS-TABELLA.
      *--> DATA EFFETTO
           MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.
      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT           TO TRUE.

      *--> INIZIALIZZAZIONE FLAG ESITO
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

      *--> TABELLA STORICA
           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.

      *--> MODALITA DI ACCESSO
           SET IDSI0011-PRIMARY-KEY      TO TRUE.

      *--> NOME TABELLA FISICA DB
           MOVE 'PROGR-NUM-OGG'          TO IDSI0011-CODICE-STR-DATO.

      *--> VALORIZZA DCLGEN TABELLA

           MOVE IDSV0001-COD-COMPAGNIA-ANIA
                                         TO D03-COD-COMPAGNIA-ANIA.
           IF WS-SECONDA-LETTURA
              MOVE 'EN'                  TO D03-FORMA-ASSICURATIVA
           ELSE
              MOVE WPOL-TP-FRM-ASSVA     TO D03-FORMA-ASSICURATIVA
           END-IF.
      *
           MOVE WPAG-COD-OGGETTO         TO D03-COD-OGGETTO.
           MOVE WS-KEY-OGGETTO           TO D03-KEY-BUSINESS.

      *--> DCLGEN TABELLA
           MOVE PROGR-NUM-OGG            TO IDSI0011-BUFFER-DATI.
           MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI
                         TO PROGR-NUM-OGG

                  WHEN IDSO0011-NOT-FOUND
      *--->       CHIAVE NON TROVATA SU TABELLA $
                       CONTINUE
      *
                  WHEN OTHER
      *-->        ERRORE DI ACCESSO AL DB
                       PERFORM S9001-ERRORE-COMUNE
                          THRU EX-S9001

              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              PERFORM S9001-ERRORE-COMUNE        THRU EX-S9001
           END-IF.

       EX-S116E.
           EXIT.

      *----------------------------------------------------------------*
      *    CALCOLO STRUTTURA TABELLA SE NON E' GIA' STATA
      *    PRECEDENTEMENTE CARICATA
      *----------------------------------------------------------------*
       S2000-VERIFICA-DATO.

           SET WKS-TROVATO-TABB-NO           TO TRUE.
           SET WK-CAMPO-FIND-NO              TO TRUE.
           MOVE 1                            TO IX-AP-TABB.

           PERFORM VARYING IX-TABB FROM 1 BY 1
             UNTIL IX-TABB > WKS-ELE-MAX-TABB
               OR WKS-TROVATO-TABB-SI

               IF CNO-COD-STR-DATO = WKS-NOME-TABB(IX-TABB)
                  SET WKS-TROVATO-TABB-SI    TO TRUE
      *-->        MI SALVO L'INDICE PER CAPIRE QUAL'E' LA TABELLA DA
      *-->        UTILIZZARE PER REPERIRE IL DATO
                  MOVE IX-TABB               TO IX-AP-TABB
               END-IF
           END-PERFORM.

           IF WKS-TROVATO-TABB-SI
              PERFORM VARYING IX-DATO FROM 1 BY 1
               UNTIL IX-DATO > WKS-ELE-MAX-DATO(IX-AP-TABB)
                  OR WK-CAMPO-FIND-SI
                  IF WKS-CODICE-DATO(IX-AP-TABB, IX-DATO) =
                     CNO-COD-DATO

                     SET WK-CAMPO-FIND-SI         TO TRUE
                     MOVE IX-DATO                 TO IX-AP-DATO
                  END-IF

              END-PERFORM
           END-IF.

      *--> SE LA STRUTTURA NON E' GIA' SALVATA IN CACHE LA RECUPERO
      *--> INVOCANDO IL SERVIZIO IDSS0020
           IF WKS-TROVATO-TABB-NO
           OR WK-CAMPO-FIND-NO
      *-->    CHIAMATA RECUPERA STRUTTURA TABELLA IDSS0020
              PERFORM S2100-CALL-IDSS0020
                 THRU EX-S2100
           END-IF.

       EX-S2000.
           EXIT.
      *----------------------------------------------------------------*
      *    CHIAMATA RECUPERA STRUTTURA TABELLA IDSS0020
      *----------------------------------------------------------------*
       S2100-CALL-IDSS0020.

           MOVE SPACES                TO INPUT-IDSS0020.
           SET IDSI0021-INITIALIZE-SI TO TRUE.
           SET IDSI0021-ON-LINE       TO TRUE.
           SET IDSI0021-PTF-NEWLIFE   TO TRUE.
           MOVE 9999                  TO IDSI0021-TIPO-MOVIMENTO.
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
                                      TO IDSI0021-CODICE-COMPAGNIA-ANIA.
           MOVE CNO-COD-STR-DATO      TO IDSI0021-CODICE-STR-DATO.
           MOVE SPACES                TO AREA-CALL.
           MOVE INPUT-IDSS0020        TO AREA-CALL.

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'IDSS0020'                     TO IDSV8888-NOME-PGM
           MOVE 'Serv recup strutt tab. '      TO IDSV8888-DESC-PGM
           SET  IDSV8888-INIZIO                TO TRUE
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           CALL IDSS0020              USING AREA-CALL
           ON EXCEPTION
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'STRUTTURA TABELLA'
                TO CALL-DESC
              MOVE 'S2100-CALL-IDSS0020'
               TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'IDSS0020'                     TO IDSV8888-NOME-PGM
           MOVE 'Serv recup strutt tab. '      TO IDSV8888-DESC-PGM
           SET  IDSV8888-FINE                  TO TRUE
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

ALEX  *     MOVE SPACES                TO OUTPUT-IDSS0020.

           INITIALIZE  IDSO0021-LIMITE-STR-MAX
                       IDSO0021-NUM-ELEM
                       IDSO0021-AREA-ERRORI
                       IDSO0021-ELEMENTS-STR-DATO(1).

           MOVE IDSO0021-STRUTTURA-DATO  TO IDSO0021-RESTO-TAB.

           MOVE AREA-CALL             TO OUTPUT-IDSS0020.

           IF IDSO0021-SUCCESSFUL-RC
      *-->    OPERAZIONE ESEGUITA CORRETTAMENTE
              PERFORM S2110-VALORIZZA-AREAT
                 THRU EX-S2110
           ELSE
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              STRING 'CALL IDSS0020 (' CNO-COD-STR-DATO ') - '
              'RC: ' IDSO0021-RETURN-CODE ' - '
              'SQLCODE: ' IDSO0021-SQLCODE
              'DESC: ' IDSO0021-DESCRIZ-ERR-DB2
              DELIMITED BY SIZE
              INTO CALL-DESC
              MOVE 'S2100-CALL-IDSS0020'
               TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-IF.

       EX-S2100.
           EXIT.
      *----------------------------------------------------------------*
      *  VALORIZZAZIONI AREA TABELLA                                   *
      *----------------------------------------------------------------*
       S2110-VALORIZZA-AREAT.

           SET WK-CAMPO-FIND-NO        TO TRUE.
           ADD 1                       TO WKS-ELE-MAX-TABB.
           MOVE WKS-ELE-MAX-TABB       TO IX-TABB
                                          IX-AP-TABB.
           MOVE 0                      TO IX-DATO
                                          IX-AP-DATO.

           MOVE CNO-COD-STR-DATO       TO WKS-NOME-TABB(IX-AP-TABB).
           PERFORM VARYING IX-IDSS0020 FROM 1 BY 1
              UNTIL IX-IDSS0020 > 1000
                 OR IDSO0021-CODICE-DATO(IX-IDSS0020)
                 = HIGH-VALUES OR SPACES

                 ADD 1                 TO WKS-ELE-MAX-DATO(IX-AP-TABB)
                 ADD 1                 TO IX-DATO

                 MOVE IDSO0021-CODICE-DATO(IX-IDSS0020)
                   TO WKS-CODICE-DATO(IX-AP-TABB, IX-DATO)
                 MOVE IDSO0021-TIPO-DATO(IX-IDSS0020)
                   TO WKS-TIPO-DATO(IX-AP-TABB, IX-DATO)
                 MOVE IDSO0021-INIZIO-POSIZIONE(IX-IDSS0020)
                   TO WKS-INI-POSI(IX-AP-TABB, IX-DATO)
                 MOVE IDSO0021-LUNGHEZZA-DATO(IX-IDSS0020)
                   TO WKS-LUN-DATO(IX-AP-TABB, IX-DATO)
                 MOVE IDSO0021-LUNGHEZZA-DATO-NOM(IX-IDSS0020)
                   TO WKS-LUNGHEZZA-DATO-NOM(IX-AP-TABB, IX-DATO)
                 MOVE IDSO0021-PRECISIONE-DATO(IX-IDSS0020)
                   TO WKS-PRECISIONE-DATO(IX-AP-TABB, IX-DATO)

           END-PERFORM.

           PERFORM VARYING IX-DATO FROM 1 BY 1
             UNTIL IX-DATO > WKS-ELE-MAX-DATO(IX-AP-TABB)
                OR WK-CAMPO-FIND-SI
                IF WKS-CODICE-DATO(IX-AP-TABB, IX-DATO) =
                   CNO-COD-DATO

                   SET WK-CAMPO-FIND-SI         TO TRUE
                   MOVE IX-DATO                 TO IX-AP-DATO
                END-IF

           END-PERFORM.

       EX-S2110.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
       S3000-VALORIZZA-DATO.

           PERFORM S3100-SWITCH-DCLGEN-AREA-APPO
              THRU EX-S3100.

           IF WKS-TIPO-DATO(IX-AP-TABB, IX-AP-DATO) = 'C3'
      *-->    TRASCODIFICA CAMPO COMP-3
              SET IDSV0141-MITT-COMP-3  TO TRUE
              MOVE WKS-LUNGHEZZA-DATO-NOM(IX-AP-TABB, IX-AP-DATO)
                TO IDSV0141-LUNGHEZZA-DATO-MITT
              MOVE WKS-LUNGHEZZA-DATO-NOM(IX-AP-TABB, IX-AP-DATO)
                TO IDSV0141-LUNGHEZZA-DATO-STR
              MOVE WKS-PRECISIONE-DATO(IX-AP-TABB, IX-DATO)
                TO IDSV0141-PRECISIONE-DATO-MITT

              MOVE WKS-AREA-TABB-APPO(WKS-INI-POSI(IX-AP-TABB,
                                                   IX-AP-DATO):
                                      WKS-LUN-DATO(IX-AP-TABB,
                                                   IX-AP-DATO))
                TO IDSV0141-CAMPO-MITT

              PERFORM Y9999-CHIAMA-IDSS0100   THRU Y9999-EX
      *
              IF IDSV0001-ESITO-OK
                 MOVE IDSV0141-CAMPO-DEST     TO WS-VALORE-APP
                 COMPUTE WS-POSIZIONE-APP = (18 -
                 WKS-LUNGHEZZA-DATO-NOM(IX-AP-TABB, IX-AP-DATO)) + 1
                 MOVE WS-VALORE-STR(WS-POSIZIONE-APP:
                        WKS-LUNGHEZZA-DATO-NOM(IX-AP-TABB, IX-AP-DATO))
                   TO WS-VALORE
              END-IF
      *
           ELSE
      *
              MOVE WKS-AREA-TABB-APPO
                  (WKS-INI-POSI(IX-AP-TABB, IX-AP-DATO):
                   WKS-LUN-DATO(IX-AP-TABB, IX-AP-DATO))
                TO WS-VALORE
           END-IF.

           IF CNO-LUNGHEZZA-DATO-NULL = HIGH-VALUE
              MOVE WKS-LUN-DATO(IX-AP-TABB, IX-AP-DATO)
                TO WS-LUNGHEZZA-VALORE
           ELSE
              MOVE CNO-LUNGHEZZA-DATO
                TO WS-LUNGHEZZA-VALORE
           END-IF.
      *
       EX-S3000.
           EXIT.
      *----------------------------------------------------------------*
      *    SWITCH DCLGEN/AREA APPOGGIO
      *----------------------------------------------------------------*
       S3100-SWITCH-DCLGEN-AREA-APPO.

           MOVE SPACES                     TO WKS-AREA-TABB-APPO.

           EVALUATE CNO-COD-STR-DATO

      *-->     POLIZZA
               WHEN 'POLI'
MIGCOL         WHEN 'POLI-COLL'
                    MOVE WPOL-DATI
                      TO WKS-AREA-TABB-APPO

      *-->     ADESIONE
               WHEN 'ADES'
MIGCOL         WHEN 'ADES-COLL'
                    MOVE WADE-DATI
                      TO WKS-AREA-TABB-APPO

      *-->     RAPPORTO RETE
               WHEN 'RAPP-RETE'
                    PERFORM S7000-GESTIONE-RAPP-RETE
                       THRU EX-S7000

           END-EVALUATE.

       EX-S3100.
           EXIT.
      *----------------------------------------------------------------*
      *    CALCOLA LUNGHEZZA
      *----------------------------------------------------------------*
       S4000-CALCOLA-LUNGHEZZA.

           IF WS-VALORE-P NOT = SPACES
              MOVE LENGTH OF WS-VALORE-P      TO WS-LUNGHEZZA-CAMPO
           ELSE
              MOVE LENGTH OF WS-VALORE        TO WS-LUNGHEZZA-CAMPO
           END-IF.

           SET  WS-NO-FINE-VALORE          TO TRUE.

           PERFORM VARYING WS-POSIZIONE-VAL FROM 1 BY 1
             UNTIL WS-POSIZIONE-VAL > WS-LUNGHEZZA-CAMPO
                OR WS-SI-FINE-VALORE

               IF WS-VALORE(WS-POSIZIONE-VAL:1) = SPACES OR HIGH-VALUE
                                                  OR LOW-VALUE
                  SET  WS-SI-FINE-VALORE          TO TRUE

                  COMPUTE WS-LUNGHEZZA-VALORE =
                          WS-POSIZIONE-VAL - 1
               END-IF
           END-PERFORM.

       EX-S4000.
           EXIT.
      *----------------------------------------------------------------*
      *    CONCATENA IB OGGETTO
      *----------------------------------------------------------------*
       S5000-CONCATENA-IB-OGG.

           IF (CNO-VALORE-DEFAULT-NULL    = HIGH-VALUE)
           AND NOT (CNO-COD-DATO-NULL NOT = HIGH-VALUE AND
                    CNO-COD-STR-DATO-NULL = HIGH-VALUE)
           AND (WKS-TIPO-DATO(IX-AP-TABB, IX-AP-DATO) = 'C3')

              COMPUTE WS-POSIZ-INI =
                 WKS-LUNGHEZZA-DATO-NOM(IX-AP-TABB, IX-AP-DATO) -
                 WS-LUNGHEZZA-VALORE + 1

              MOVE WS-VALORE(WS-POSIZ-INI:WS-LUNGHEZZA-VALORE)
                TO WPAG-IB-OGGETTO(WS-POSIZIONE-IB:WS-LUNGHEZZA-VALORE)
           ELSE
              MOVE WS-VALORE(1:WS-LUNGHEZZA-VALORE)
                TO WPAG-IB-OGGETTO(WS-POSIZIONE-IB:WS-LUNGHEZZA-VALORE)
           END-IF.

      *    MOVE WS-VALORE(1:WS-LUNGHEZZA-VALORE)
      *      TO WPAG-IB-OGGETTO(WS-POSIZIONE-IB:WS-LUNGHEZZA-VALORE).
      *
       EX-S5000.
           EXIT.
      *----------------------------------------------------------------*
      *    CONCATENA IB OGGETTO
      *----------------------------------------------------------------*
       S5001-CONCATENA-KEY.
      *
           IF (CNO-VALORE-DEFAULT-NULL    = HIGH-VALUE)
           AND NOT (CNO-COD-DATO-NULL NOT = HIGH-VALUE AND
                    CNO-COD-STR-DATO-NULL = HIGH-VALUE)
           AND (WKS-TIPO-DATO(IX-AP-TABB, IX-AP-DATO) = 'C3')

              COMPUTE WS-POSIZ-INI =
                 WKS-LUNGHEZZA-DATO-NOM(IX-AP-TABB, IX-AP-DATO) -
                 WS-LUNGHEZZA-VALORE + 1

              IF WS-QTA-FLAG-P = 0
                 MOVE WS-VALORE(WS-POSIZ-INI:WS-LUNGHEZZA-VALORE)
                 TO WS-KEY-OGGETTO(WS-POSIZIONE-IB:WS-LUNGHEZZA-VALORE)
              ELSE
                 MOVE WS-VALORE(WS-POSIZ-INI:WS-LUNGHEZZA-VALORE)
                 TO WS-KEY-OGGETTO(WS-POSIZIONE-P:WS-LUNGHEZZA-VALORE)
              END-IF
           ELSE
              IF WS-QTA-FLAG-P = 0
                 MOVE WS-VALORE(1:WS-LUNGHEZZA-VALORE)
                 TO WS-KEY-OGGETTO(WS-POSIZIONE-IB:WS-LUNGHEZZA-VALORE)
              ELSE
                 MOVE WS-VALORE(1:WS-LUNGHEZZA-VALORE)
                 TO WS-KEY-OGGETTO(WS-POSIZIONE-P:WS-LUNGHEZZA-VALORE)
              END-IF
           END-IF.

      *    IF WS-QTA-FLAG-P = 0
      *       MOVE WS-VALORE(1:WS-LUNGHEZZA-VALORE)
      *         TO WS-KEY-OGGETTO(WS-POSIZIONE-IB:WS-LUNGHEZZA-VALORE)
      *    ELSE
      *       MOVE WS-VALORE(1:WS-LUNGHEZZA-VALORE)
      *         TO WS-KEY-OGGETTO(WS-POSIZIONE-P:WS-LUNGHEZZA-VALORE)
      *    END-IF.
      *
       EX-S5001.
           EXIT.
      *----------------------------------------------------------------*
      *    AGGIORNA IB OGGETTO
      *----------------------------------------------------------------*
       S6000-AGGIORNA-IB-OGG.
      *
           COMPUTE WS-POSIZIONE-APP = (18 - WS-LUNGHEZZA-P) + 1.
      *
           MOVE WS-VALORE-STR(WS-POSIZIONE-APP:WS-LUNGHEZZA-P)
             TO WPAG-IB-OGGETTO(WS-POSIZIONE-P:WS-LUNGHEZZA-P).
      *
       EX-S6000.
           EXIT.

      *----------------------------------------------------------------*
      *    GESTIONE RAPPORTO RETE
      *----------------------------------------------------------------*
       S7000-GESTIONE-RAPP-RETE.

      *    SE L'AREA E' PRESENTE IN CONTESTO
           IF WRRE-ELE-RAPP-RETE-MAX > ZEROES

      *       PRENDO I DATI DA CONTESTO
              PERFORM VARYING IX-TAB-RRE FROM 1 BY 1
                        UNTIL IX-TAB-RRE > WRRE-ELE-RAPP-RETE-MAX
                  IF  (NOT WRRE-ST-DEL(IX-TAB-RRE))
                  AND WRRE-ID-OGG(IX-TAB-RRE)  = WPOL-ID-POLI
                  AND WRRE-TP-OGG(IX-TAB-RRE)  = 'PO'
                  AND WRRE-TP-RETE(IX-TAB-RRE) = 'GE'
                      MOVE WRRE-DATI(IX-TAB-RRE)
                        TO WKS-AREA-TABB-APPO
                  END-IF
              END-PERFORM

      *    SE NON E' PRESENTE IN CONTESTO LA LEGGO DA DB
           ELSE

              PERFORM S7100-LETTURA-RAPP-RETE
                 THRU EX-S7100

              IF IDSV0001-ESITO-OK
                 MOVE WRRE-DATI(IX-TAB-RRE)
                   TO WKS-AREA-TABB-APPO
              END-IF

           END-IF.

       EX-S7000.
           EXIT.

      *----------------------------------------------------------------*
      *    LETTURA RAPPORTO RETE
      *----------------------------------------------------------------*
       S7100-LETTURA-RAPP-RETE.

      *    LETTURA PER ID-OGGETTO E TIPO-OGGETTO = POLIZZA
      *    E TIPO-RETE = GESTIONE
           MOVE WPOL-ID-POLI             TO RRE-ID-OGG
           MOVE 'PO'                     TO RRE-TP-OGG
           MOVE 'GE'                     TO RRE-TP-RETE

           MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA

      *--> LIVELLO OPERAZIONE
           SET IDSI0011-TRATT-X-COMPETENZA
            TO TRUE.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT            TO TRUE.
           SET IDSI0011-WHERE-CONDITION   TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL   TO TRUE.

           MOVE SPACES                    TO IDSI0011-BUFFER-WHERE-COND.
           MOVE RAPP-RETE                 TO IDSI0011-BUFFER-DATI
           MOVE 'LDBS4240'                TO IDSI0011-CODICE-STR-DATO

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC

              EVALUATE TRUE

      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                  WHEN IDSO0011-SUCCESSFUL-SQL

                       MOVE IDSO0011-BUFFER-DATI
                         TO RAPP-RETE

                       MOVE 1
                         TO IX-TAB-RRE

                       PERFORM VALORIZZA-OUTPUT-RRE
                          THRU VALORIZZA-OUTPUT-RRE-EX

                       MOVE IX-TAB-RRE
                         TO WRRE-ELE-RAPP-RETE-MAX

      *--->       ERRORE DI ACCESSO AL DB
                  WHEN OTHER
                       MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S7100-LETTURA-RAPP-RETE'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'      TO IEAI9901-COD-ERRORE
                       STRING 'LDBS4240;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE
                              INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300

              END-EVALUATE

           ELSE

      *-->    ERRORE DISPATCHER
              MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S7100-LETTURA-RAPP-RETE'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'           TO IEAI9901-COD-ERRORE
              STRING 'LDBS4240;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300

           END-IF.

       EX-S7100.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DELLA COPY IVVC0214
      *----------------------------------------------------------------*
       A0010-VALORIZZA-DCLGEN.

           IF WPAG-TAB-ALIAS(IND-STR) =
              C161-ALIAS-ADES
              MOVE WPAG-BUFFER-DATI
                  (WPAG-POSIZ-INI(IND-STR) :
                   WPAG-LUNGHEZZA(IND-STR))
                TO WADE-AREA-ADESIONE
           END-IF.

           IF WPAG-TAB-ALIAS(IND-STR) =
              C161-ALIAS-POLI
              MOVE WPAG-BUFFER-DATI
                  (WPAG-POSIZ-INI(IND-STR) :
                   WPAG-LUNGHEZZA(IND-STR))
                TO WPOL-AREA-POLIZZA
           END-IF.

           IF WPAG-TAB-ALIAS(IND-STR) =
              C161-ALIAS-RAPP-RETE
              MOVE WPAG-BUFFER-DATI
                  (WPAG-POSIZ-INI(IND-STR) :
                   WPAG-LUNGHEZZA(IND-STR))
                TO WRRE-AREA-RAPP-RETE
           END-IF.
      *
       A0010-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CHIAMATA MODULO PER FORMATTAZIONE CAMPI COMP
      *----------------------------------------------------------------*
       Y9999-CHIAMA-IDSS0100.
      *
           CALL IDSS0140              USING AREA-IDSV0141
             ON EXCEPTION
                MOVE WK-PGM
                  TO IEAI9901-COD-SERVIZIO-BE
                MOVE 'Y9999-CHIAMA-IDSS0100'
                  TO IEAI9901-LABEL-ERR
                MOVE '001114'
                  TO IEAI9901-COD-ERRORE
                MOVE 'ERRORE CALL IDSS0140'
                  TO IEAI9901-PARAMETRI-ERR
                PERFORM S0300-RICERCA-GRAVITA-ERRORE
                   THRU EX-S0300
           END-CALL.
      *
           IF IDSV0141-UNSUCCESSFUL-RC
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'Y9999-CHIAMA-IDSS0100'
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'ERRORE CHIAMATA MODULO IDSS0140'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       Y9999-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ERRORE COMUNE
      *----------------------------------------------------------------*
       S9001-ERRORE-COMUNE.
      *
           MOVE WK-PGM
             TO IEAI9901-COD-SERVIZIO-BE.
           MOVE 'S1111-LEGGI-NUM-OGG'
             TO IEAI9901-LABEL-ERR.
           MOVE '005016'
             TO IEAI9901-COD-ERRORE.
      *
           STRING WS-TABELLA            ';'
                  IDSO0011-RETURN-CODE ';'
                  IDSO0011-SQLCODE
                  DELIMITED BY SIZE
                  INTO IEAI9901-PARAMETRI-ERR
           END-STRING.
      *
           PERFORM S0300-RICERCA-GRAVITA-ERRORE
              THRU EX-S0300.
      *
       EX-S9001.
           EXIT.
      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      *    COPY VALORIZZA OUTPUT
      *----------------------------------------------------------------*
           COPY LCCVRRE3                 REPLACING ==(SF)== BY ==WRRE==.
      *----------------------------------------------------------------*
      *    ROUTINE INIZIALIZZAZIONE AREE
      *----------------------------------------------------------------*
           COPY LCCVD034                  REPLACING ==(SF)== BY ==D03==.
      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORE
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.
      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.
      *----------------------------------------------------------------*
      *    ROUTINES DI GESTIONE DISPLAY
      *----------------------------------------------------------------*
           COPY IDSP8888.

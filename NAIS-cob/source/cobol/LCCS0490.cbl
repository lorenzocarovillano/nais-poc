       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LCCS0490.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                      *
      * F A S E         : SERVIZIO CALCOLO RATEO                       *
      *                                                                *
      *----------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77  WK-PGM                         PIC X(08)  VALUE 'LCCS0490'.

      *----------------------------------------------------------------*
      * PGM CHIAMATI
      *----------------------------------------------------------------*
       77  PGM-LCCS0010                   PIC X(08)  VALUE 'LCCS0010'.
       77  PGM-LCCS0062                   PIC X(08)  VALUE 'LCCS0062'.
      *--  Routine date
       77  LCCS0003                       PIC X(08)  VALUE 'LCCS0003'.

      *----------------------------------------------------------------*
      * AREA HOST VARIABLES
      *----------------------------------------------------------------*
           COPY IDBVPCO1.
           COPY IDBVTIT1.
           COPY IDBVDTC1.

      *---------------------------------------------------------------*
      * COPY ELE-MAX
      *---------------------------------------------------------------*
           COPY LCCVTITZ.
           COPY LCCVDTCZ.

      *----------------------------------------------------------------*
      * COMODO
      *----------------------------------------------------------------*
       01  WK-LABEL                       PIC X(30).
       01  WK-DT-DECOR-GRZ                PIC 9(08).
       01  WK-FRAZIONAMENTO               PIC 9(05).
       01  WK-DT-INFERIORE                PIC 9(08).
       01  WK-DT-SUPERIORE                PIC 9(08).
       01  WK-DT-COMPETENZA               PIC 9(08).
       01  WK-DT-DECORRENZA               PIC 9(08).
       01  WK-DT-ULT-TIT-INC              PIC 9(08).
       01  WK-TIPO-RICORRENZA             PIC X(01).

      *-- AREA ROUTINE PER IL CALCOLO
       01  FORMATO                        PIC X(01).
       01  DATA-INFERIORE.
           05 AAAA-INF                    PIC 9(04).
           05 MM-INF                      PIC 9(02).
           05 GG-INF                      PIC 9(02).
       01  DATA-SUPERIORE.
           05 AAAA-SUP                    PIC 9(04).
           05 MM-SUP                      PIC 9(02).
           05 GG-SUP                      PIC 9(02).
       01  GG-DIFF                        PIC 9(05).
       01  CODICE-RITORNO                 PIC X(01).
       01  IN-RCODE                       PIC 9(2).

      *----------------------------------------------------------------*
      * COSTANTI
      *----------------------------------------------------------------*
       77  WK-RECUPERO-PROVV              PIC X(02) VALUE 'RP'.
       77  WK-PREMIO                      PIC X(02) VALUE 'PR'.

      *---------------------------------------------------------------*
      * COPY ELE-MAX
      *---------------------------------------------------------------*
           COPY LCCVGRZZ.

      *----------------------------------------------------------------*
      * FLAGS
      *----------------------------------------------------------------*
       01 GARANZIA-BASE                   PIC X(01).
          88 GARANZIA-BASE-OK             VALUE 'S'.
          88 GARANZIA-BASE-KO             VALUE 'N'.

       01 GARANZIA-ACC                    PIC X(01).
          88 GARANZIA-ACC-OK              VALUE 'S'.
          88 GARANZIA-ACC-KO              VALUE 'N'.

       01 RICERCA                         PIC X(01).
          88 TROVATO                      VALUE 'S'.
          88 NON-TROVATO                  VALUE 'N'.

      *----------------------------------------------------------------*
      * INDICI
      *----------------------------------------------------------------*
       01 IX-INDICI.
          03 IX-TAB-GRZ                   PIC S9(04) COMP-3.
          03 IX-TAB-RATE                  PIC S9(04) COMP-3.
          03 IX-TAB-TIT                   PIC S9(04) COMP-3.
          03 IX-TAB-DTC                   PIC S9(04) COMP-3.

      *----------------------------------------------------------------*
      *    COPY GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
           COPY IDSV0003.
           COPY IDSV0010.

      *----------------------------------------------------------------*
      *    INTERFACCIA SERVIZIO CALCOLA RATE DI POLIZZA
      *----------------------------------------------------------------*
       01  AREA-LCCC0062.
           COPY LCCC0062.

      *----------------------------------------------------------------*
      *    TIPOLOGICHE
      *----------------------------------------------------------------*
           COPY LCCC0006.
           COPY LCCVXST0.
           COPY LCCVXOG0.
           COPY LCCVXTT0.
           COPY LCCVXPT0.

      *----------------------------------------------------------------*
      *  Interfaccia modulo LDBS0290 - Estrattore Titoli Contabili
      *----------------------------------------------------------------*
           COPY LDBV0291.

      *----------------------------------------------------------------*
      *    DATE E TIMESTAMP
      *----------------------------------------------------------------*
           COPY IDSV0015.

      *----------------------------------------------------------------*
      *    INTERFACCIA ROUTINE CALCOLO DATA
      *----------------------------------------------------------------*
           COPY LCCC0003.

      *--  TITOLO CONTABILE
       01 WTIT-AREA-TITOLO.
          04 WTIT-ELE-TIT-MAX              PIC S9(004) COMP.
                COPY LCCVTITA REPLACING   ==(SF)==  BY ==WTIT==.
             COPY LCCVTIT1                 REPLACING ==(SF)==
                                                  BY ==WTIT==.
      *--  DETTAGLIO TITOLO CONTABILE
       01 WDTC-AREA-DETT-TITOLO.
          04 WDTC-ELE-DETT-TIT-MAX         PIC S9(004) COMP.
                COPY LCCVDTCA REPLACING   ==(SF)==  BY ==WDTC==.
             COPY LCCVDTC1                 REPLACING ==(SF)==
                                                  BY ==WDTC==.

      *----------------------------------------------------------------*
      *    COPY DISPATHCER
      *----------------------------------------------------------------*
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
      *--> COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      *--> COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.

      *-----------------------------------------------------------------
      *  LINKAGE
      *-----------------------------------------------------------------
       LINKAGE SECTION.

      *-----------------------------------------------------------------
      *  CAMPI DI ESITO, AREE DI SERVIZIO
      *-----------------------------------------------------------------
       01 AREA-IDSV0001.
           COPY IDSV0001.

      *--- AREA COMUNE.
       01 WCOM-AREA-STATI.
           COPY LCCC0001              REPLACING ==(SF)== BY ==WCOM==.

      ******************************************************************
      *   INPUT BUSINESS SERVICES
      ******************************************************************
           COPY LCCC0490.

      *-----------------------------------------------------------------
      *  PROCEDURE
      *-----------------------------------------------------------------
       PROCEDURE DIVISION             USING AREA-IDSV0001
                                            WCOM-AREA-STATI
                                            LCCC0490.

           PERFORM A000-INIZIO              THRU A000-EX.

           IF IDSV0001-ESITO-OK
              PERFORM A100-ELABORA          THRU A100-EX
           END-IF.

           GOBACK.

      *-----------------------------------------------------------------
      *  OPERAZIONI INIZIALI
      *-----------------------------------------------------------------
       A000-INIZIO.

      *--> COPY PER L'INIZIALIZZAZIONE AREA ERRORI
      *--> DELL'AREA CONTESTO.
           COPY IERP0001.

      *--  TITOLO CONTABILE
           PERFORM VARYING IX-TAB-TIT FROM 1 BY 1
             UNTIL IX-TAB-TIT > WK-TIT-MAX-A
             PERFORM INIZIA-TOT-TIT THRU INIZIA-TOT-TIT-EX
           END-PERFORM.

           MOVE ZEROES                        TO WTIT-ELE-TIT-MAX.

      *--  DETTAGLIO TITOLO CONTABILE
           PERFORM VARYING IX-TAB-DTC FROM 1 BY 1
             UNTIL IX-TAB-DTC > WK-DTC-MAX-A
             PERFORM INIZIA-TOT-DTC THRU INIZIA-TOT-DTC-EX
           END-PERFORM.

           MOVE ZEROES                        TO WDTC-ELE-DETT-TIT-MAX.

           INITIALIZE LCCC0490-AREA-OUTPUT
                      IX-INDICI.

           SET LCCC0490-NO-TIT-EMESSI         TO TRUE.
           SET LCCC0490-SI-PRESENZA-RATEO     TO TRUE.

           MOVE IDSV0001-DATA-COMPETENZA(1:8) TO WK-DT-COMPETENZA.
           SET GARANZIA-ACC-KO                TO TRUE.
           SET GARANZIA-BASE-KO               TO TRUE.

      *--> CONTROLLI SUI CAMPI DI INPUT
           PERFORM A010-CONTROLLI-INPUT
              THRU A010-EX.

      *--> LETTURA PARAMETRO COMPAGNIA
           IF IDSV0001-ESITO-OK
              PERFORM A020-LETTURA-PCO
                 THRU A020-EX
           END-IF.

       A000-EX.
           EXIT.

      *-----------------------------------------------------------------
      *  CONTROLLI DATI INPUT
      *-----------------------------------------------------------------
       A010-CONTROLLI-INPUT.

           IF LCCC0490-ID-POLI IS NUMERIC AND
              LCCC0490-ID-POLI > ZERO
              CONTINUE
           ELSE
              MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
              MOVE '005166'   TO IEAI9901-COD-ERRORE
              STRING 'IDENTIFICATIVO POLIZZA = '
                     LCCC0490-ID-POLI
                     ' NON CONSENTITO'
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

           IF IDSV0001-ESITO-OK
              IF LCCC0490-DT-DECOR-POLI IS NUMERIC AND
                 LCCC0490-DT-DECOR-POLI > ZERO
                 CONTINUE
              ELSE
                 MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                 MOVE '005166'   TO IEAI9901-COD-ERRORE
                 STRING 'DATA DECORRENZA POLIZZA = '
                        LCCC0490-DT-DECOR-POLI
                        ' NON CONSENTITA'
                        DELIMITED BY SIZE
                        INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

           PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
             UNTIL IX-TAB-GRZ > WK-GRZ-MAX-B
                OR IDSV0001-ESITO-KO

             IF LCCC0490-COD-TARI(IX-TAB-GRZ) =
                SPACES OR HIGH-VALUE OR LOW-VALUE
                MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                MOVE '005166'   TO IEAI9901-COD-ERRORE
                STRING 'CODICE TARIFFA = '
                       LCCC0490-COD-TARI(IX-TAB-GRZ)
                       ' NON CONSENTITO'
                       DELIMITED BY SIZE
                       INTO IEAI9901-PARAMETRI-ERR
                END-STRING
                PERFORM S0300-RICERCA-GRAVITA-ERRORE
                   THRU EX-S0300
             END-IF

             IF IDSV0001-ESITO-OK
                IF LCCC0490-TP-GAR(IX-TAB-GRZ) IS NUMERIC AND
                   LCCC0490-TP-GAR(IX-TAB-GRZ) > ZERO
                   CONTINUE
                ELSE
                   MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                   MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                   MOVE '005166'   TO IEAI9901-COD-ERRORE
                   STRING 'TIPO GARANZIA = '
                          LCCC0490-TP-GAR(IX-TAB-GRZ)
                          ' NON CONSENTITO'
                          DELIMITED BY SIZE
                          INTO IEAI9901-PARAMETRI-ERR
                   END-STRING
                   PERFORM S0300-RICERCA-GRAVITA-ERRORE
                      THRU EX-S0300
                END-IF
             END-IF

             IF IDSV0001-ESITO-OK
                IF LCCC0490-FRAZIONAMENTO(IX-TAB-GRZ) IS NUMERIC
                   CONTINUE
                ELSE
                   MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                   MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                   MOVE '005166'   TO IEAI9901-COD-ERRORE
                   STRING 'FRAZIONAMENTO = '
                          LCCC0490-FRAZIONAMENTO(IX-TAB-GRZ)
                          ' NON CONSENTITO'
                          DELIMITED BY SIZE
                          INTO IEAI9901-PARAMETRI-ERR
                   END-STRING
                   PERFORM S0300-RICERCA-GRAVITA-ERRORE
                      THRU EX-S0300
                END-IF
             END-IF

             IF IDSV0001-ESITO-OK
                IF LCCC0490-TP-PER-PRE(IX-TAB-GRZ) =
                   SPACES OR HIGH-VALUE OR LOW-VALUE
                   MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                   MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                   MOVE '005166'   TO IEAI9901-COD-ERRORE
                   STRING 'TIPO PERIODO PREMIO = '
                          LCCC0490-TP-PER-PRE(IX-TAB-GRZ)
                          ' NON CONSENTITO'
                          DELIMITED BY SIZE
                          INTO IEAI9901-PARAMETRI-ERR
                   END-STRING
                   PERFORM S0300-RICERCA-GRAVITA-ERRORE
                      THRU EX-S0300
                END-IF
             END-IF

             IF IDSV0001-ESITO-OK
                IF LCCC0490-DT-DECOR(IX-TAB-GRZ) IS NUMERIC AND
                   LCCC0490-DT-DECOR(IX-TAB-GRZ) > ZERO
                   CONTINUE
                ELSE
                   MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                   MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                   MOVE '005166'   TO IEAI9901-COD-ERRORE
                   STRING 'DATA DECORRENZA = '
                          LCCC0490-DT-DECOR(IX-TAB-GRZ)
                          ' NON CONSENTITA'
                          DELIMITED BY SIZE
                          INTO IEAI9901-PARAMETRI-ERR
                   END-STRING
                   PERFORM S0300-RICERCA-GRAVITA-ERRORE
                      THRU EX-S0300
                END-IF
             END-IF

             IF IDSV0001-ESITO-OK
                IF LCCC0490-TIPO-RICORRENZA(IX-TAB-GRZ) =
                   SPACES OR HIGH-VALUE OR LOW-VALUE
                   MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                   MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                   MOVE '005166'   TO IEAI9901-COD-ERRORE
                   STRING 'TIPOLOGIA RICORRENZA = '
                          LCCC0490-TIPO-RICORRENZA(IX-TAB-GRZ)
                          ' NON VALORIZZATA'
                          DELIMITED BY SIZE
                          INTO IEAI9901-PARAMETRI-ERR
                   END-STRING
                   PERFORM S0300-RICERCA-GRAVITA-ERRORE
                      THRU EX-S0300
                ELSE
                   IF LCCC0490-TIPO-RICORRENZA(IX-TAB-GRZ) NOT = 'Q' AND
                      LCCC0490-TIPO-RICORRENZA(IX-TAB-GRZ) NOT = 'G'
                      MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                      MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                      MOVE '005166'   TO IEAI9901-COD-ERRORE
                      STRING 'TIPOLOGIA RICORRENZA = '
                          LCCC0490-TIPO-RICORRENZA(IX-TAB-GRZ)
                          ' NON CONSENTITA'
                          DELIMITED BY SIZE
                          INTO IEAI9901-PARAMETRI-ERR
                      END-STRING
                      PERFORM S0300-RICERCA-GRAVITA-ERRORE
                         THRU EX-S0300
                   END-IF
                END-IF
             END-IF

           END-PERFORM.

       A010-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    LETTURA PARAMETRO COMPAGNIA
      *----------------------------------------------------------------*
       A020-LETTURA-PCO.

           MOVE HIGH-VALUE                  TO PARAM-COMP.

           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA TO PCO-COD-COMP-ANIA.

           MOVE 'PARAM-COMP'                TO IDSI0011-CODICE-STR-DATO.
           MOVE PARAM-COMP                  TO IDSI0011-BUFFER-DATI.

           SET IDSI0011-SELECT              TO TRUE.
           SET IDSI0011-PRIMARY-KEY         TO TRUE.
           SET IDSI0011-TRATT-SENZA-STOR    TO TRUE.

           PERFORM CALL-DISPATCHER          THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI TO PARAM-COMP
                  WHEN OTHER
                       MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                       MOVE '005016'   TO IEAI9901-COD-ERRORE
                       STRING IDSI0011-CODICE-STR-DATO ';'
                              IDSO0011-RETURN-CODE     ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE
                              INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *--> GESTIRE ERRORE
              MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL         TO IEAI9901-LABEL-ERR
              MOVE '005016'         TO IEAI9901-COD-ERRORE
              STRING IDSI0011-CODICE-STR-DATO ';'
                     IDSO0011-RETURN-CODE     ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       A020-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       A100-ELABORA.

           PERFORM A110-VERIFICA-GARANZIE
              THRU A110-EX
           VARYING IX-TAB-GRZ FROM 1 BY 1
             UNTIL IX-TAB-GRZ > WK-GRZ-MAX-B
                OR IDSV0001-ESITO-KO.

           IF IDSV0001-ESITO-OK AND
              GARANZIA-BASE-OK  AND
              GARANZIA-ACC-OK
              PERFORM A120-ULT-TIT-INCAS
                 THRU A120-EX
           END-IF.

       A100-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VERIFICA GARANZIE
      *----------------------------------------------------------------*
       A110-VERIFICA-GARANZIE.

           MOVE LCCC0490-TP-GAR(IX-TAB-GRZ) TO ACT-TP-GARANZIA.

           IF (LCCC0490-FRAZIONAMENTO(IX-TAB-GRZ) > 0 AND
               TP-GAR-COMPLEM)
               SET GARANZIA-ACC-OK TO TRUE
               MOVE LCCC0490-DT-DECOR(IX-TAB-GRZ)
                 TO WK-DT-DECOR-GRZ
               MOVE LCCC0490-FRAZIONAMENTO(IX-TAB-GRZ)
                 TO WK-FRAZIONAMENTO
               MOVE LCCC0490-TIPO-RICORRENZA(IX-TAB-GRZ)
                 TO WK-TIPO-RICORRENZA
           END-IF.

           IF TP-GAR-BASE AND
             (LCCC0490-TP-PER-PRE(IX-TAB-GRZ) = 'U' OR
              LCCC0490-FRAZIONAMENTO(IX-TAB-GRZ) > 0)
              SET GARANZIA-BASE-OK TO TRUE
           END-IF.

       A110-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    LETTURA ULTIMO TITOLO CONTABILE INCASSATO
      *----------------------------------------------------------------*
       A120-ULT-TIT-INCAS.

           INITIALIZE IDSI0011-BUFFER-DATI
                      TIT-CONT.

           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
           SET IDSI0011-SELECT           TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.
           SET IDSI0011-TRATT-DEFAULT    TO TRUE.

           MOVE LCCC0490-ID-POLI         TO TIT-ID-OGG.
           SET POLIZZA                   TO TRUE.
           MOVE WS-TP-OGG                TO TIT-TP-OGG.
           SET INCASSATO                 TO TRUE.
           MOVE WS-TP-STAT-TIT           TO TIT-TP-STAT-TIT.
           SET TT-PREMIO                 TO TRUE.
           MOVE WS-TP-TIT                TO TIT-TP-TIT.

           MOVE WS-DT-INFINITO-1-N       TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO.

           MOVE WS-TS-INFINITO-1-N       TO IDSI0011-DATA-COMPETENZA.

           MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
           MOVE 'LDBS5980'               TO IDSI0011-CODICE-STR-DATO.
           MOVE TIT-CONT                 TO IDSI0011-BUFFER-DATI.

           PERFORM CALL-DISPATCHER       THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI
                         TO TIT-CONT
                       MOVE TIT-DT-END-COP
                         TO WK-DT-ULT-TIT-INC
                       IF WK-DT-DECOR-GRZ < TIT-DT-END-COP
                          MOVE WK-DT-DECOR-GRZ TO WK-DT-INFERIORE
                          MOVE TIT-DT-END-COP  TO WK-DT-SUPERIORE
                                                  LCCC0490-DT-FINE-COP
                          PERFORM A121-CALCOLO-DIFF-DATE
                             THRU A121-EX
                       ELSE
                          MOVE WK-DT-DECOR-GRZ TO WK-DT-INFERIORE
      *-->        LETTURA DI EVENTUALI TITOLI EMESSI
                          PERFORM A124-PREPARA-AREA-TIT
                             THRU A124-EX
                          PERFORM A125-ESTRAZIONE-TIT
                             THRU A125-EX
                          IF IDSV0001-ESITO-OK
                             PERFORM A126-PREPARA-AREA-DTC
                                THRU A126-EX
                             PERFORM A127-ESTRAZIONE-DTC
                                THRU A127-EX
                          END-IF
                       END-IF

                  WHEN IDSO0011-NOT-FOUND
      *-->        NESSUN DATO IN TABELLA
                       MOVE LCCC0490-DT-DECOR-POLI  TO WK-DT-DECORRENZA
                       PERFORM A122-CALL-LCCS0062
                          THRU A122-EX
                       PERFORM A123-SCORRI-RATE
                          THRU A123-EX
                       IF TROVATO
                          MOVE WK-DT-DECOR-GRZ
                            TO WK-DT-INFERIORE
                          MOVE LCCC0490-DT-FINE-COP
                            TO WK-DT-SUPERIORE
                          PERFORM A121-CALCOLO-DIFF-DATE
                             THRU A121-EX
                       END-IF

                  WHEN OTHER
      *--->       ERRORE DI ACCESSO AL DB
                       MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                       MOVE '005016'   TO IEAI9901-COD-ERRORE
                       STRING IDSI0011-CODICE-STR-DATO ';'
                              IDSO0011-RETURN-CODE     ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE
                              INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300

              END-EVALUATE

           ELSE

      *--> GESTIRE ERRORE
              MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL         TO IEAI9901-LABEL-ERR
              MOVE '005016'         TO IEAI9901-COD-ERRORE
              STRING IDSI0011-CODICE-STR-DATO ';'
                     IDSO0011-RETURN-CODE     ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300

           END-IF.

       A120-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CALCOLO DELLA DIFFERENZA IN GIORNI TRA DUE DATE
      *----------------------------------------------------------------*
       A121-CALCOLO-DIFF-DATE.

           MOVE 'A'                       TO FORMATO.
           MOVE WK-DT-INFERIORE           TO DATA-INFERIORE.
           MOVE WK-DT-SUPERIORE           TO DATA-SUPERIORE.

           CALL PGM-LCCS0010 USING FORMATO,
                                   DATA-INFERIORE,
                                   DATA-SUPERIORE,
                                   GG-DIFF,
                                   CODICE-RITORNO
           ON EXCEPTION
              MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'CALCOLO DIFFERENZA GIORNI TRA DUE DATE'
                                          TO CALL-DESC
              MOVE WK-LABEL               TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

           IF CODICE-RITORNO = ZERO
              MOVE GG-DIFF                TO LCCC0490-RATEO
           ELSE
              MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL               TO IEAI9901-LABEL-ERR
              MOVE '005016'               TO IEAI9901-COD-ERRORE
              MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       A121-EX.
           EXIT.

      *----------------------------------------------------------------*
      *  ROUTINE PER IL CALCOLO DELLE RATE SUCCESSIVE
      *----------------------------------------------------------------*
       A122-CALL-LCCS0062.

           INITIALIZE AREA-LCCC0062.

           MOVE WK-DT-DECORRENZA           TO LCCC0062-DT-DECORRENZA.
           MOVE WK-DT-COMPETENZA           TO LCCC0062-DT-COMPETENZA.

           IF WK-TIPO-RICORRENZA = 'Q'
              MOVE PCO-DT-ULT-QTZO-IN      TO LCCC0062-DT-ULT-QTZO
              MOVE ZEROES                  TO LCCC0062-DT-ULTGZ-TRCH
           ELSE
              MOVE PCO-DT-ULTGZ-TRCH-E-IN  TO LCCC0062-DT-ULTGZ-TRCH
              MOVE ZEROES                  TO LCCC0062-DT-ULT-QTZO
           END-IF.

           MOVE WK-FRAZIONAMENTO           TO LCCC0062-FRAZIONAMENTO.

           CALL PGM-LCCS0062 USING  AREA-IDSV0001
                                    WCOM-AREA-STATI
                                    AREA-LCCC0062
           ON EXCEPTION
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SERVIZIO CALCOLO RATE'   TO CALL-DESC
              MOVE WK-LABEL                  TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

       A122-EX.
           EXIT.

      *----------------------------------------------------------------*
      *  SCORRI RATE
      *----------------------------------------------------------------*
       A123-SCORRI-RATE.

           SET NON-TROVATO                TO TRUE.

           PERFORM VARYING IX-TAB-RATE FROM 1 BY 1
             UNTIL IX-TAB-RATE > LCCC0062-TOT-NUM-RATE
                OR TROVATO

             IF WK-DT-DECOR-GRZ >= LCCC0062-DT-INF(IX-TAB-RATE) AND
                WK-DT-DECOR-GRZ <= LCCC0062-DT-SUP(IX-TAB-RATE)
                SET TROVATO               TO TRUE
                MOVE LCCC0062-DT-SUP(IX-TAB-RATE)
                  TO LCCC0490-DT-FINE-COP
             END-IF

           END-PERFORM.

       A123-EX.
           EXIT.

      *----------------------------------------------------------------*
      *  PREPARA AREA TITOLI PER ESTRARRE I TITOLI EMESSI              *
      *----------------------------------------------------------------*
       A124-PREPARA-AREA-TIT.

           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-FETCH-FIRST      TO TRUE.
           SET IDSI0011-TRATT-X-COMPETENZA TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.

           SET  EMESSO                   TO TRUE.
           MOVE WS-TP-STAT-TIT           TO TIT-TP-STAT-TIT.

           MOVE LCCC0490-ID-POLI         TO TIT-ID-OGG.
           SET  POLIZZA                  TO TRUE.
           MOVE WS-TP-OGG                TO TIT-TP-OGG.

           MOVE WK-RECUPERO-PROVV        TO TIT-TP-PRE-TIT.
           MOVE WK-PREMIO                TO TIT-TP-TIT.

           MOVE 'LDBS0880'               TO IDSI0011-CODICE-STR-DATO.
           MOVE  SPACES                  TO IDSI0011-BUFFER-WHERE-COND.

           PERFORM A128-PREP-CALOLO-DT
              THRU A128-EX.

           PERFORM A129-CALOLO-DT
              THRU A129-EX.

           IF IDSV0001-ESITO-OK
              MOVE A2K-OUAMG             TO TIT-DT-INI-COP
              MOVE SPACES                TO IDSI0011-BUFFER-DATI
              MOVE TIT-CONT              TO IDSI0011-BUFFER-DATI
           END-IF.

       A124-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ESTRAZIONE DEI TITOLI CONTABILI
      *----------------------------------------------------------------*
       A125-ESTRAZIONE-TIT.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSO0011-SUCCESSFUL-SQL
                      OR NOT IDSV0001-ESITO-OK

                 PERFORM CALL-DISPATCHER
                    THRU CALL-DISPATCHER-EX

                 IF IDSO0011-SUCCESSFUL-RC
                   EVALUATE TRUE

      *--> NON TROVATA
                       WHEN IDSO0011-NOT-FOUND
                          IF IDSI0011-FETCH-FIRST
                             IF WK-DT-DECOR-GRZ > WK-DT-ULT-TIT-INC
                                SET LCCC0490-NO-PRESENZA-RATEO TO TRUE
                             ELSE
                                IF WK-DT-DECOR-GRZ = WK-DT-ULT-TIT-INC
                                   MOVE WK-DT-ULT-TIT-INC
                                     TO LCCC0490-DT-FINE-COP
                                END-IF
                             END-IF
                          END-IF

                       WHEN IDSO0011-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                          MOVE IDSO0011-BUFFER-DATI TO TIT-CONT
                          IF IDSI0011-FETCH-FIRST
                             MOVE TIT-DT-END-COP    TO WK-DT-SUPERIORE
                                  LCCC0490-DT-FINE-COP
                             PERFORM A121-CALCOLO-DIFF-DATE
                                THRU A121-EX
                          END-IF
                          ADD 1                     TO IX-TAB-TIT
                          PERFORM VALORIZZA-OUTPUT-TIT
                             THRU VALORIZZA-OUTPUT-TIT-EX
                          ADD 1                     TO WTIT-ELE-TIT-MAX
                          IF WTIT-ELE-TIT-MAX > 1
                             SET LCCC0490-SI-TIT-EMESSI TO TRUE
                          END-IF

                          SET IDSI0011-FETCH-NEXT   TO TRUE

                      WHEN OTHER
      *--> ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM
                             TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'A125-ESTRAZIONE-TIT'
                             TO IEAI9901-LABEL-ERR
                           MOVE '005016'
                             TO IEAI9901-COD-ERRORE
                           STRING 'TABELLA TITOLO CONTABILE'     ';'
                                  IDSO0011-RETURN-CODE   ';'
                                  IDSO0011-SQLCODE
                           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                           END-STRING
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300

                   END-EVALUATE

                 ELSE

      *--> GESTIRE ERRORE DISPATCHER
                    MOVE WK-PGM
                      TO IEAI9901-COD-SERVIZIO-BE
                    MOVE 'A125-ESTRAZIONE-TIT'
                      TO IEAI9901-LABEL-ERR
                    MOVE '005016'
                      TO IEAI9901-COD-ERRORE
                    STRING 'TABELLA TITOLO CONTABILE'     ';'
                           IDSO0011-RETURN-CODE ';'
                           IDSO0011-SQLCODE
                    DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    END-STRING
                    PERFORM S0300-RICERCA-GRAVITA-ERRORE
                       THRU EX-S0300

                 END-IF

           END-PERFORM.

       A125-EX.
           EXIT.

      *----------------------------------------------------------------*
      *  PREPARA AREA DETTAGLIO TITOLI PER ESTRARRE I DETTAGLI TITOLI  *
      *  ASSOCIATI AI TITOLI EMESSI ESTRATTI IN PRECEDENZA             *
      *----------------------------------------------------------------*
       A126-PREPARA-AREA-DTC.

           MOVE WS-DT-INFINITO-1-N       TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO.

           MOVE WS-TS-INFINITO-1-N          TO IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-TRATT-X-COMPETENZA  TO TRUE.

       A126-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ESTRAZIONE DETTAGLI TITOLO CONTABILE
      *----------------------------------------------------------------*
       A127-ESTRAZIONE-DTC.

           PERFORM VARYING IX-TAB-TIT FROM 1 BY 1
           UNTIL IX-TAB-TIT > WTIT-ELE-TIT-MAX

              SET IDSO0011-SUCCESSFUL-SQL       TO TRUE
              SET IDSO0011-SUCCESSFUL-RC        TO TRUE
              SET IDSI0011-FETCH-FIRST          TO TRUE

      *--> MODALITA DI ACCESSO
              SET IDSI0011-ID-PADRE             TO TRUE

              MOVE WTIT-ID-TIT-CONT(IX-TAB-TIT) TO DTC-ID-TIT-CONT

      *--> NOME TABELLA FISICA DB
              MOVE 'DETT-TIT-CONT'        TO IDSI0011-CODICE-STR-DATO

      *--> DCLGEN TABELLA
              MOVE DETT-TIT-CONT          TO IDSI0011-BUFFER-DATI

              PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                         OR NOT IDSO0011-SUCCESSFUL-SQL
                         OR NOT IDSV0001-ESITO-OK

                 PERFORM CALL-DISPATCHER
                    THRU CALL-DISPATCHER-EX

                       IF IDSO0011-SUCCESSFUL-RC
                         EVALUATE TRUE
      *--> NON TROVATA
                             WHEN IDSO0011-NOT-FOUND
                              IF IDSI0011-FETCH-FIRST
                                 MOVE WK-PGM
                                   TO IEAI9901-COD-SERVIZIO-BE
                                 MOVE 'A127-ESTRAZIONE-DTC'
                                   TO IEAI9901-LABEL-ERR
                                 MOVE '005016'
                                   TO IEAI9901-COD-ERRORE
                                 STRING 'DETT-TIT-CONT'      ';'
                                        IDSO0011-RETURN-CODE ';'
                                        IDSO0011-SQLCODE
                                        DELIMITED BY SIZE INTO
                                        IEAI9901-PARAMETRI-ERR
                                 END-STRING
                                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                    THRU EX-S0300
                              END-IF

      *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                             WHEN IDSO0011-SUCCESSFUL-SQL
                              MOVE IDSO0011-BUFFER-DATI
                                TO DETT-TIT-CONT
                              ADD 1
                                TO IX-TAB-DTC
                              PERFORM VALORIZZA-OUTPUT-DTC
                                 THRU VALORIZZA-OUTPUT-DTC-EX
                              ADD 1
                                TO WDTC-ELE-DETT-TIT-MAX
                              SET IDSI0011-FETCH-NEXT   TO TRUE

      *--> ERRORE DI ACCESSO AL DB
                             WHEN OTHER
                              MOVE WK-PGM
                                TO IEAI9901-COD-SERVIZIO-BE
                              MOVE 'A127-ESTRAZIONE-DTC'
                                TO IEAI9901-LABEL-ERR
                              MOVE '005016'
                                TO IEAI9901-COD-ERRORE
                              STRING 'TABELLA DETT TITOLO CONTABILE' ';'
                                     IDSO0011-RETURN-CODE            ';'
                                     IDSO0011-SQLCODE
                                     DELIMITED BY SIZE INTO
                                     IEAI9901-PARAMETRI-ERR
                              END-STRING
                              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300

                      END-EVALUATE

                    ELSE

      *--> GESTIRE ERRORE DISPATCHER
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'A127-ESTRAZIONE-DTC'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING 'DETT-TIT-CONT'      ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300

                    END-IF

              END-PERFORM

           END-PERFORM.

       A127-EX.
           EXIT.

      *----------------------------------------------------------------*
      * VIENE SOTTRATTO UN GIORNO ALLA DATA EFFETTO                    *
      *----------------------------------------------------------------*
       A128-PREP-CALOLO-DT.

           INITIALIZE IO-A2K-LCCC0003.
           MOVE '03'                      TO A2K-FUNZ.
           MOVE '03'                      TO A2K-INFO.
           MOVE 1                         TO A2K-DELTA.
           MOVE IDSV0001-DATA-EFFETTO     TO A2K-INDATA.
           MOVE '0'                       TO A2K-FISLAV
                                             A2K-INICON.

       A128-EX.
           EXIT.

      *----------------------------------------------------------------*
      * RICHIAMO ROUTINE CALCOLO DATA                                  *
      *----------------------------------------------------------------*
       A129-CALOLO-DT.

           CALL LCCS0003 USING IO-A2K-LCCC0003 IN-RCODE
           ON EXCEPTION
              MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SERVIZIO OPERAZIONI SULLE DATE'
                                          TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

           IF IN-RCODE NOT = '00'
              MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'A129-CALOLO-DT'       TO IEAI9901-LABEL-ERR
              MOVE '005016'               TO IEAI9901-COD-ERRORE
              MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       A129-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORE
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.

      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.

      *----------------------------------------------------------------*
      *    COPY LETTURA TABELLE
      *----------------------------------------------------------------*
           COPY LCCVTIT3              REPLACING ==(SF)== BY ==WTIT==.
           COPY LCCVTIT4              REPLACING ==(SF)== BY ==WTIT==.
           COPY LCCVDTC3              REPLACING ==(SF)== BY ==WDTC==.
           COPY LCCVDTC4              REPLACING ==(SF)== BY ==WDTC==.

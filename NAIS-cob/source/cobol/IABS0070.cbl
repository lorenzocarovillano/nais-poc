       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IABS0070 IS INITIAL.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE
      * F A S E         : GESTIONE TABELLA BTC_BATCH_STATE
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

           EXEC SQL INCLUDE IDSV0010 END-EXEC.


      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDBBS0 END-EXEC.
           EXEC SQL INCLUDE IDBVBBS2 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IABV0002 END-EXEC.

           EXEC SQL INCLUDE IDBVBBS1 END-EXEC.

      *****************************************************************
       PROCEDURE DIVISION USING IDSV0003 IABV0002 BTC-BATCH-STATE.

           PERFORM A000-INIZIO              THRU A000-EX.

           PERFORM A300-ELABORA             THRU A300-EX

           GOBACK.

      ******************************************************************
       A000-INIZIO.

           MOVE 'IABS0070'              TO   IDSV0003-COD-SERVIZIO-BE.
           MOVE 'BTC_BATCH_STATE'       TO   IDSV0003-NOME-TABELLA.

           MOVE '00'                    TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                  TO   IDSV0003-SQLCODE
                                             IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                  TO   IDSV0003-DESCRIZ-ERR-DB2
                                             IDSV0003-KEY-TABELLA.

----->     PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.


       A000-EX.
           EXIT.
      ******************************************************************
       A100-CHECK-RETURN-CODE.

           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE
           END-IF.

       A100-EX.
           EXIT.
      ******************************************************************
       A300-ELABORA.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT                 THRU A310-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A360-OPEN-CURSOR            THRU A360-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR           THRU A370-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST            THRU A380-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT             THRU A390-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.

       A300-EX.
           EXIT.
      ******************************************************************
       A305-DECLARE-CURSOR.

           EXEC SQL
                DECLARE CUR-BBS CURSOR WITH HOLD FOR

              SELECT
                 COD_BATCH_STATE
                ,DES
                ,FLAG_TO_EXECUTE

              FROM BTC_BATCH_STATE

             ORDER BY COD_BATCH_STATE

           END-EXEC.

       A305-EX.
           EXIT.
      ******************************************************************
       A310-SELECT.

           EXEC SQL
             SELECT
                 COD_BATCH_STATE
                ,DES
                ,FLAG_TO_EXECUTE
             INTO
                  :BBS-COD-BATCH-STATE
                 ,:BBS-DES
                  :IND-BBS-DES
                 ,:BBS-FLAG-TO-EXECUTE
                  :IND-BBS-FLAG-TO-EXECUTE

             FROM  BTC_BATCH_STATE
             WHERE COD_BATCH_STATE = :IABV0002-STATE-CURRENT

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.

       A310-EX.
           EXIT.
      ******************************************************************
       A360-OPEN-CURSOR.

           PERFORM A305-DECLARE-CURSOR THRU A305-EX.

           EXEC SQL
                OPEN CUR-BBS
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A360-EX.
           EXIT.
      ******************************************************************
       A370-CLOSE-CURSOR.

           EXEC SQL
                CLOSE CUR-BBS
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A370-EX.
           EXIT.
      ******************************************************************
       A380-FETCH-FIRST.

           PERFORM A360-OPEN-CURSOR    THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT THRU A390-EX
           END-IF.

       A380-EX.
           EXIT.
      ******************************************************************
       A390-FETCH-NEXT.

           EXEC SQL
                FETCH CUR-BBS
           INTO
                  :BBS-COD-BATCH-STATE
                 ,:BBS-DES
                  :IND-BBS-DES
                 ,:BBS-FLAG-TO-EXECUTE
                  :IND-BBS-FLAG-TO-EXECUTE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX


           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       A390-EX.
           EXIT.
      ******************************************************************
       Z100-SET-COLONNE-NULL.

           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-BBS-DES = -1
              MOVE HIGH-VALUES TO BBS-DES
           END-IF
           IF IND-BBS-FLAG-TO-EXECUTE = -1
              MOVE HIGH-VALUES TO BBS-FLAG-TO-EXECUTE-NULL
           END-IF.

       Z100-EX.
           EXIT.
      ******************************************************************
       Z200-SET-INDICATORI-NULL.


           IF BBS-DES = HIGH-VALUES
              MOVE -1 TO IND-BBS-DES
           ELSE
              MOVE 0 TO IND-BBS-DES
           END-IF
           IF BBS-FLAG-TO-EXECUTE-NULL = HIGH-VALUES
              MOVE -1 TO IND-BBS-FLAG-TO-EXECUTE
           ELSE
              MOVE 0 TO IND-BBS-FLAG-TO-EXECUTE
           END-IF.

       Z200-EX.
           EXIT.
       Z900-CONVERTI-N-TO-X.

           CONTINUE.
       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.

           CONTINUE.
       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.


           CONTINUE.

       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
----->     EXEC SQL INCLUDE IDSP0003 END-EXEC.

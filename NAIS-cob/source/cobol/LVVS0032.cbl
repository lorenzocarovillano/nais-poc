      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0032.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0026
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... MODULO DI CALCOLO
      *  DESCRIZIONE.... Ricava dalla POLIZZA una data
      *                  in formato AAA,MM
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

      *----------------------------------------------------------------*
      *    PROGRAMMI
      *----------------------------------------------------------------*
       01  WK-PGM                        PIC X(008) VALUE 'LVVS0032'.
       01  PGM-LVVS0000                  PIC X(008) VALUE 'LVVS0000'.

       01  WK-DATA-INPUT.
           03 WK-AAAA-INPUT                PIC 9(04).
           03 WK-MM-INPUT                  PIC 9(02).
           03 WK-GG-INPUT                  PIC 9(02).

       01  IX-INDICI.
           03 IND-STR                    PIC S9(04) COMP.
           03 IND-ADE                    PIC S9(04) COMP.

       01 ALIAS-POLI                     PIC X(3) VALUE 'POL'.

       01 FLAG-STRUTTURA-TROVATA         PIC X(01).
          88 STRUTTURA-TROVATA-SI        VALUE 'S'.
          88 STRUTTURA-TROVATA-NO        VALUE 'N'.

       01 AREA-BUSINESS.
      *--  AREA POLIZZA
           03 DADE-AREA-ADES.
              04 DADE-ELE-ADES-MAX       PIC S9(04) COMP.
              04 DADE-TAB-ADES.
              COPY LCCVADE1              REPLACING ==(SF)== BY ==DADE==.

       01  INPUT-LVVS0000.
           COPY LVVC0000        REPLACING ==(SF)== BY ==LVVC0000==.
       01  ALIAS-TABELLE.
           COPY IVVC0218        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0032.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0032.
      *----------------------------------------------------------------*

           PERFORM L000-OPERAZIONI-INIZIALI
              THRU L000-EX.

           PERFORM L100-ELABORAZIONE
              THRU L100-EX.

           PERFORM L900-OPERAZIONI-FINALI
              THRU L900-EX.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       L000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IVVC0213-TAB-OUTPUT
                                             WK-DATA-INPUT.
      *
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
           SET STRUTTURA-TROVATA-NO          TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE      TO IVVC0213-TAB-OUTPUT.

       L000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       L100-ELABORAZIONE.
      *

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DELLA COPY IVVC0214

           PERFORM L500-VALORIZZA-DCLGEN THRU L500-EX
                   VARYING IND-STR FROM 1 BY 1
                     UNTIL IND-STR > IVVC0213-ELE-INFO-MAX OR
                           IVVC0213-TAB-ALIAS(IND-STR) =
                      SPACES OR LOW-VALUE OR HIGH-VALUE

           IF STRUTTURA-TROVATA-SI
              PERFORM L700-CALL-LVVS0000         THRU L700-EX
           ELSE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING 'STRUTTURA NON TROVATA'
                     ' : '
                     ALIAS-POLI
                     DELIMITED BY SIZE INTO
                     IDSV0003-DESCRIZ-ERR-DB2
               END-STRING

                SET IDSV0003-FIELD-NOT-VALUED    TO TRUE
           END-IF.
      *
       L100-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DELLA COPY IVVC0214
      *----------------------------------------------------------------*
       L500-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IND-STR) = IVVC0213-ALIAS-ADES
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IND-STR) :
                   IVVC0213-LUNGHEZZA(IND-STR))
                TO DADE-AREA-ADES
                SET STRUTTURA-TROVATA-SI TO TRUE
           END-IF.

       L500-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPARA CALL LVVS0000
      *----------------------------------------------------------------*
       L600-PREPARA-CALL.

      *--> COPY LVVC0000
           INITIALIZE INPUT-LVVS0000.

           SET LVVC0000-AAA-V-MM         TO TRUE.

           IF DADE-DUR-AA IS NUMERIC
              MOVE DADE-DUR-AA            TO WK-AAAA-INPUT
           ELSE
              MOVE ZEROES                 TO WK-AAAA-INPUT
           END-IF.

           IF DADE-DUR-MM IS NUMERIC
              MOVE DADE-DUR-MM            TO WK-MM-INPUT
           ELSE
              MOVE ZEROES                 TO WK-MM-INPUT
           END-IF.

           IF DADE-DUR-GG IS NUMERIC
              MOVE DADE-DUR-GG            TO WK-GG-INPUT
           ELSE
              MOVE ZEROES                 TO WK-GG-INPUT
           END-IF.

           MOVE WK-DATA-INPUT             TO LVVC0000-DATA-INPUT-1.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET IDSV0003-SUCCESSFUL-RC     TO TRUE
           SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.

       L600-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALL LDBS1650
      *----------------------------------------------------------------*
       L700-CALL-LVVS0000.
      *
           PERFORM L600-PREPARA-CALL  THRU L600-EX

           CALL PGM-LVVS0000  USING  IDSV0003 INPUT-LVVS0000
      *
           ON EXCEPTION
              MOVE PGM-LVVS0000
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL LVVS0000 ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE LVVC0000-DATA-OUTPUT    TO IVVC0213-VAL-IMP-O
           ELSE
              MOVE PGM-LVVS0000            TO IDSV0003-COD-SERVIZIO-BE

              STRING 'ERRORE ELABORAZIONE LVVS0000'
                     IDSV0003-RETURN-CODE
                     DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING

           END-IF.
      *
       L700-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       L900-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       L900-EX.
           EXIT.


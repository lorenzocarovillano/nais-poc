      *  ============================================================= *
      *                                                                *
      *        PORTAFOGLIO VITA ITALIA  VER 1.0                        *
      *                                                                *
      *  ============================================================= *
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LOAS0800.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2009.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *     PROGRAMMA ..... LOAS0800                                   *
      *     TIPOLOGIA...... OPERAZIONI AUTOMATICHE                     *
      *     PROCESSO....... CALCOLO MANAGEMENT FEE                     *
      *     FUNZIONE....... BATCH                                      *
      *     DESCRIZIONE.... SERVIZIO DI CALCOLO MANAGEMENT FEE         *
      *     PAGINA WEB..... N.A.                                       *
      *----------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
      *
       DATA DIVISION.
      *
       WORKING-STORAGE SECTION.
      *
      *----------------------------------------------------------------*
      *     COPY DISPATCHER
      *----------------------------------------------------------------*
      *
       01  IDSV0012.
           COPY IDSV0012.
      *
       01  DISPATCHER-VARIABLES.
      *  COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      *  COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.

      *----------------------------------------------------------------*
      *    CAMPI X ROUTINES DI GESTIONE DISPLAY IDSP8888
      *----------------------------------------------------------------*
           COPY IDSV8888.

      *
           COPY IDSV0003.
      *
      *----------------------------------------------------------------*
      *     COPY DELLE TABELLE DB2 PER IL DISPATCHER DATI
      *----------------------------------------------------------------*
      *
      *  --> PARAMETRO OGGETTO
           COPY IDBVPOG1.
      *  --> Where condition ad hoc per lettura Parametro Oggetto
           COPY LDBV1131.
      *
      *----------------------------------------------------------------*
      * COSTANTI
      *----------------------------------------------------------------*
      *
       77 WK-PGM                           PIC X(08) VALUE 'LOAS0800'.
      *
      *----------------------------------------------------------------*
      *  INDICI DI SCORRIMENTO
      *----------------------------------------------------------------*
      *
       01  IX-INDICI.
      *
           03 IX-TAB-PMO                   PIC S9(04) COMP.
           03 IX-TAB-GRZ                   PIC S9(04) COMP.
           03 IX-TAB-TGA                   PIC S9(04) COMP.
           03 IX-TAB-POG                   PIC S9(04) COMP.
      *
           03 IX-AREA-ISPC0211             PIC S9(04) COMP.
           03 IX-COMP-ISPC0211             PIC S9(04) COMP.
           03 IX-AREA-SCHEDA-P             PIC S9(04) COMP.
           03 IX-AREA-SCHEDA-T             PIC S9(04) COMP.
           03 IX-TAB-VAR-P                 PIC S9(04) COMP.
           03 IX-TAB-VAR-T                 PIC S9(04) COMP.
           03 IX-AREA-SCHEDA               PIC S9(04) COMP.
           03 IX-TAB-VAR                   PIC S9(04) COMP.
           03 IX-TAB-ERR                   PIC S9(04) COMP.
           03 IX-FRAZ                      PIC S9(04) COMP.
           03 IX-TAB-ISPS0211              PIC S9(04) COMP.
      *
      *----------------------------------------------------------------*
      *       COPY VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
      *
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IEAV9903.
           COPY IDSV0002.
      *
      * --> Area di appoggio per i messaggi di errore
      *
       01 WK-GESTIONE-MSG-ERR.
          03 WK-COD-ERR                 PIC X(06) VALUE ZEROES.
          03 WK-LABEL-ERR               PIC X(30) VALUE SPACES.
          03 WK-STRING                  PIC X(85) VALUE SPACES.
      *
      *----------------------------------------------------------------*
      *     FLAGS X GESTIONE DEBUG
      *----------------------------------------------------------------*
      *
       01 TEST-I             PIC S9(4) VALUE 0.
       01 TEST-J             PIC S9(4) VALUE 0.
       01 WK-DATA            PIC 9(8)  VALUE 0.
       01 WK-ID              PIC 9(9)  VALUE 0.
      *
      *----------------------------------------------------------------*
      *    AREA IO SERVIZIO CALCOLI SULLE DATE
      *----------------------------------------------------------------*
      *
      *  --> MODULO PER CALCOLI SU DATE
       01  LCCS0003                         PIC X(8) VALUE 'LCCS0003'.
          COPY LCCC0003.
      *
       01 IN-RCODE                       PIC 9(2).
       01 WK-DATA-CALCOLATA              PIC 9(8) VALUE ZEROES.
      *
      * --> Area di accept della data dalla variabile di sistema DATE
      *
       01 WK-CURRENT-DATE               PIC 9(08) VALUE ZEROES.
      *
      *----------------------------------------------------------------*
      *    AREA IO SERVIZIO CALCOLO DATA FINE MESE
      *----------------------------------------------------------------*
      *
      *  --> MODULO PER CALCOLO DATA FINE MESE
       01 LCCS0029                       PIC X(8) VALUE 'LCCS0029'.
      *
       01 AREA-IO-LCCS0029.
          COPY LCCC0029                  REPLACING ==(SF)== BY ==S029==.
      *
      *----------------------------------------------------------------*
      *     VARIABILI PER GESTIONE MANAGEMENT FEE
      *----------------------------------------------------------------*
      *
      * --> Indicatore del calcolo del Management Fee
      *
       01 WK-MANFEE                     PIC X(01).
          88 WK-MANFEE-YES                VALUE 'Y'.
          88 WK-MANFEE-NO                 VALUE 'N'.
      *
      * --> Area di appoggio per gestione Management Fee
      *
       01 WK-GG-INC                     PIC 9(05).
       01 WK-PERMANFEE                  PIC 9(05).
       01 WK-MESIDIFFMFEE               PIC 9(05).
       01 WK-DECADELMFEE                PIC 9(05).
       01 WK-NUM-ADD-FRAZ               PIC 9(02) VALUE ZEROES.
       01 WK-TOT-MANFEE                 PIC S9(12)V9(3) COMP-3.
       01 WK-DT-RICOR-SUCC              PIC 9(08) VALUE ZEROES.
       01 WK-DT-CALCOLATA               PIC 9(08) VALUE ZEROES.
       01 WK-DT-ULT-EROG-MF             PIC 9(08) VALUE ZEROES.
      *
       01 WK-APPO-DT.
          03 WK-APPO-AAAA               PIC 9(04).
          03 WK-APPO-MM                 PIC 9(02).
          03 WK-APPO-GG                 PIC 9(02).
      *
      *
      *  --> MODULO LDBS1130
       01 LDBS1130                         PIC X(8) VALUE 'LDBS1130'.
      *
      *----------------------------------------------------------------*
      *                 AREA PER SERVIZIO DI PRODOTTO
      *    "CALCOLI NOTEVOLI E CONTROLLI E DI LIQUIDAZIONE"
      *----------------------------------------------------------------*
       01  ISPS0211                         PIC X(08) VALUE 'ISPS0211'.
      *----------------------------------------------------------------*
      *    AREA PER SERVIZIO DI PRODOTTO CALCOLI E CONTROLLI ISPS0211
      *----------------------------------------------------------------*
       01 AREA-IO-ISPC0211.
          COPY ISPC0211.

          COPY ISPC000Z.
      *
      *----------------------------------------------------------------*
      *    COPY PER LETTURA MATRICE MOVIMENTO
      *----------------------------------------------------------------*
      *
           COPY LCCV0021.
      *
      *----------------------------------------------------------------*
      *  AREE TIPOLOGICHE DI PORTAFOGLIO
      *----------------------------------------------------------------*
      *
          COPY LCCC0006.
          COPY LCCVXTH0.
          COPY LCCVXOG0.
          COPY LCCVXMV0.
          COPY LCCVXDA0.

      * COPY ELE MAX TAB PTF
          COPY LCCVPOGZ.
      *----------------------------------------------------------------*
      * TABELLA DI APPOGGIO PER CARICAMENTO PARAMETRO OGGETTO
      *
      * --  AREA PARAMETRO OGGETTO
       01 WPOG-AREA-PARAM-OGG.
          04 WPOG-ELE-PARAM-OGG-MAX      PIC S9(04) COMP.
                COPY LCCVPOGA REPLACING   ==(SF)==  BY ==WPOG==.
          COPY LCCVPOG1                  REPLACING ==(SF)== BY ==WPOG==.

      *
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
      *        L I N K A G E   S E C T I O N
      *----------------------------------------------------------------*
      *
       LINKAGE SECTION.
      *
      *----------------------------------------------------------------*
      *   AREA-IDSV0001
      *----------------------------------------------------------------*
      *
       01  AREA-IDSV0001.
           COPY IDSV0001.
      *
      * -- Area parametro movimento
       01 WPMO-AREA-PARAM-MOVI.
          04 WPMO-ELE-PARAM-MOV-MAX      PIC S9(04) COMP.
                COPY LCCVPMOA REPLACING   ==(SF)==  BY ==WPMO==.
          COPY LCCVPMO1                  REPLACING ==(SF)== BY ==WPMO==.
      *
      * -- Area Movimento
       01 WMOV-AREA-MOVIMENTO.
          03 WMOV-ELE-MOVI-MAX           PIC S9(04) COMP.
          03 WMOV-TAB-MOV.
          COPY LCCVMOV1                  REPLACING ==(SF)== BY ==WMOV==.
      *
      * -- Area polizza
       01 WPOL-AREA-POLIZZA.
          03 WPOL-ELE-POLI-MAX           PIC S9(04) COMP.
          03 WPOL-TAB-POL.
          COPY LCCVPOL1                  REPLACING ==(SF)== BY ==WPOL==.
      *
      * -- Area Adesione
       01 WADE-AREA-ADESIONE.
          03 WADE-ELE-ADES-MAX           PIC S9(04) COMP.
          03 WADE-TAB-ADE                OCCURS 1.
          COPY LCCVADE1                  REPLACING ==(SF)== BY ==WADE==.
      *
      * -- Area Garanzia
       01 WGRZ-AREA-GARANZIA.
          04 WGRZ-ELE-GAR-MAX           PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGRZ==.
          COPY LCCVGRZ1                 REPLACING ==(SF)== BY ==WGRZ==.
      *
      * -- Area Tranche Di Garanzia
       01 WTGA-AREA-TRANCHE.
          04 WTGA-ELE-TRAN-MAX          PIC S9(04) COMP.
                COPY LCCVTGAC REPLACING   ==(SF)==  BY ==WTGA==.
            COPY LCCVTGA1               REPLACING ==(SF)== BY ==WTGA==.
      *
      *  -- Area Scheda Per Servizi Di Prodotto
       01 WSKD-AREA-SCHEDA.
          COPY IVVC0216                 REPLACING ==(SF)== BY ==WSKD==.
      *
      *  -- Area Infrastrutturale
       01 WCOM-IO-STATI.
           03 WCOM-AREA-STATI.
              COPY LCCC0001              REPLACING ==(SF)== BY ==WCOM==.
           03 WCOM-AREA-LCCC0261.
              COPY LCCC0261              REPLACING ==(SF)== BY ==WCOM==.
      *
       01 W660-AREA-PAG.
       COPY LOAC0660                     REPLACING ==(SF)== BY ==W660==.
      *
       01 W800-AREA-PAG.
       COPY LOAC0800                     REPLACING ==(SF)== BY ==W800==.
      *
      *  ====================  M A I N    L I N E  =================== *
      *
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WCOM-IO-STATI
                                WPMO-AREA-PARAM-MOVI
                                WMOV-AREA-MOVIMENTO
                                WPOL-AREA-POLIZZA
                                WADE-AREA-ADESIONE
                                WGRZ-AREA-GARANZIA
                                WTGA-AREA-TRANCHE
                                WSKD-AREA-SCHEDA
                                W660-AREA-PAG
                                W800-AREA-PAG.
      *
           PERFORM S00000-OPERAZ-INIZIALI
              THRU S00000-OPERAZ-INIZIALI-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S10000-ELABORAZIONE
                 THRU S10000-ELABORAZIONE-EX
      *
           END-IF.
      *
           PERFORM S90000-OPERAZ-FINALI
              THRU S90000-OPERAZ-FINALI-EX.
      *
           GOBACK.
      *
      * ============================================================== *
      * -->           O P E R Z I O N I   I N I Z I A L I          <-- *
      * ============================================================== *
      *
       S00000-OPERAZ-INIZIALI.
      *
           MOVE 'S00000-OPERAZ-INIZIALI'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.

           PERFORM S00100-INIZIALIZZA-WORK
              THRU S00100-INIZIALIZZA-WORK-EX.

           PERFORM S00200-INIZIA-AREE-TAB
              THRU S00200-INIZIA-AREE-TAB-EX.
      *
           PERFORM S00300-CTRL-INPUT
              THRU S00300-CTRL-INPUT-EX.
      *
       S00000-OPERAZ-INIZIALI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * INIZIALIZZAZIONE AREE WS
      *----------------------------------------------------------------*
      *
       S00100-INIZIALIZZA-WORK.
      *
           MOVE 'S00100-INIZIALIZZA-WORK'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           INITIALIZE IX-INDICI.
      *
           ACCEPT WK-CURRENT-DATE
             FROM DATE YYYYMMDD.
      *
       S00100-INIZIALIZZA-WORK-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * INIZIALIZZAZIONE AREE TABELLE
      *----------------------------------------------------------------*
      *
       S00200-INIZIA-AREE-TAB.
      *
           MOVE 'S00200-INIZIA-AREE-TAB'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
      * --> Inizializzazione Area Parametro Oggetto
      *
           PERFORM INIZIA-TOT-POG
              THRU INIZIA-TOT-POG-EX
           VARYING IX-TAB-POG FROM 1 BY 1
             UNTIL IX-TAB-POG > WK-POG-MAX-A.
      *
           MOVE ZEROES
             TO WPOG-ELE-PARAM-OGG-MAX
                IX-TAB-POG.
      *
       S00200-INIZIA-AREE-TAB-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * CONTROLLO DATI DI INPUT
      *----------------------------------------------------------------*
      *
       S00300-CTRL-INPUT.
      *
           MOVE 'S00300-CTRL-INPUT'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           MOVE IDSV0001-TIPO-MOVIMENTO
             TO WS-MOVIMENTO.
      *
           IF IDSV0001-TIPO-MOVIMENTO NOT = 6006 AND
                                      NOT = 6024 AND
                                      NOT = 6025 AND
                                      NOT = 6026
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'TIPO MOVIMENTO ERRATO'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
           IF WMOV-ELE-MOVI-MAX = 0
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'OCCORRENZA DI MOVIMENTO NON VALORIZZATA'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
           IF WPOL-ELE-POLI-MAX = 0
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'OCCORRENZA DI POLIZZA NON VALORIZZATA'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
           IF WADE-ELE-ADES-MAX = 0
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'OCCORRENZA DI ADESIONE NON VALORIZZATA'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
           IF WGRZ-ELE-GAR-MAX = 0
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'NESSUNA OCCORRENZA DI GARANZIA ELABORABILE'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
           IF WTGA-ELE-TRAN-MAX = 0
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'NESSUNA OCCORRENZA DI TRANCHE ELABORABILE'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.

           IF WSKD-ELE-LIVELLO-MAX-P = 0
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'AREA VARIABILI PROD NON VALORIZZATA'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
           IF WSKD-ELE-LIVELLO-MAX-T = 0
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'AREA VARIABILI GAR NON VALORIZZATA'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
           IF NOT W800-CON-CALCOLO   AND
              NOT W800-SENZA-CALCOLO
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'FLAG CHIAMATA SERVIZIO NON VALORIZAZTO'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
           IF W660-MANFEE-CONT-SI
      *
              IF W660-PERMANFEE NOT = 1 AND
                                NOT = 2 AND
                                NOT = 3 AND
                                NOT = 4 AND
                                NOT = 5 AND
                                NOT = 12
      *
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL-ERR
                   TO IEAI9901-LABEL-ERR
                 MOVE '001114'
                   TO IEAI9901-COD-ERRORE
                 MOVE 'VALORE PERMANFEE NON PREVISTO NEL DOMINIO'
                   TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
      *
              END-IF
      *
           END-IF.
      *
       S00300-CTRL-INPUT-EX.
           EXIT.
      *
      * ============================================================== *
      * -->               E L A B O R A Z I O N E                  <-- *
      * ============================================================== *
      *
       S10000-ELABORAZIONE.
      *
           MOVE 'S10000-ELABORAZIONE'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           IF W800-CON-CALCOLO
      *
              PERFORM S11000-GESTIONE-ISPS0211
                 THRU S11000-GESTIONE-ISPS0211-EX
      *
           END-IF.
      *
           IF IDSV0001-ESITO-OK
      *
              IF W800-CON-CALCOLO  AND
                 WK-MANFEE-NO
      *
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL-ERR
                   TO IEAI9901-LABEL-ERR
                 MOVE '001114'
                   TO IEAI9901-COD-ERRORE
                 MOVE 'IMPOSSIBILE EFETTUARE CALCOLO MF'
                   TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
      *
              ELSE
      *
                 PERFORM S12000-GESTIONE-MANFEE
                    THRU S12000-GESTIONE-MANFEE-EX
      *
              END-IF
      *
           END-IF.
      *
       S10000-ELABORAZIONE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * GESTIONE CHIAMATA AL SERVIZIO CALC. NOTEVOLI E CONTR. DI LIQ.
      *----------------------------------------------------------------*
      *
       S11000-GESTIONE-ISPS0211.
      *
           MOVE 'S11000-GESTIONE-ISPS0211'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           SET WK-MANFEE-NO
             TO TRUE.
      *
           PERFORM S11100-PREPARA-ISPS0211
              THRU S11100-PREPARA-ISPS0211-EX.
      *
           PERFORM S11200-CALL-ISPS0211
              THRU S11200-CALL-ISPS0211-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S11300-SCORRI-OUT-ISPS0211
                 THRU S11300-SCORRI-OUT-ISPS0211-EX
              VARYING IX-AREA-ISPC0211 FROM 1 BY 1
                UNTIL IX-AREA-ISPC0211 >
                      ISPC0211-ELE-MAX-SCHEDA-T
                   OR IDSV0001-ESITO-KO
      *
           END-IF.
      *
       S11000-GESTIONE-ISPS0211-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VALORIZZAZIONE DELL'INPUT DEL SERVIZIO ISPS0211
      *----------------------------------------------------------------*
      *
       S11100-PREPARA-ISPS0211.
      *
           MOVE 'S11100-PREPARA-ISPS0211'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LOAS0800'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE 'ISPC0211'                 TO IDSV8888-DESC-PGM
           STRING 'Iniz.tab. work ISPC0211'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
      *
           INITIALIZE ISPC0211-DATI-INPUT
                      ISPC0211-AREA-ERRORI
                      ISPC0211-TAB-SCHEDA-P(1)
                      ISPC0211-TAB-SCHEDA-T(1)

           MOVE ISPC0211-TAB-VAL-P   TO  ISPC0211-RESTO-TAB-SCHEDA-P.
           MOVE ISPC0211-TAB-VAL-T   TO  ISPC0211-RESTO-TAB-SCHEDA-T.
      *
           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LOAS0800'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE 'ISPC0211'                 TO IDSV8888-DESC-PGM
           STRING 'Iniz.tab. work ISPC0211'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           MOVE IDSV0001-COD-COMPAGNIA-ANIA
             TO ISPC0211-COD-COMPAGNIA.
      *
           MOVE WPOL-COD-PROD
             TO ISPC0211-COD-PRODOTTO.

           MOVE WSKD-DEE                  TO ISPC0211-DEE

           IF WPOL-IB-OGG-NULL = HIGH-VALUES
              MOVE SPACES
                TO ISPC0211-NUM-POLIZZA
           ELSE
              MOVE WPOL-IB-OGG
                TO ISPC0211-NUM-POLIZZA
           END-IF.
      *
           IF WPOL-COD-CONV-NULL = HIGH-VALUES
      *
              MOVE SPACES
                TO ISPC0211-COD-CONVENZIONE
      *
           ELSE
      *
              MOVE WPOL-COD-CONV
                TO ISPC0211-COD-CONVENZIONE
      *
           END-IF.
      *
           IF WPOL-DT-INI-VLDT-CONV-NULL = HIGH-VALUE
      *
              MOVE SPACES
                TO ISPC0211-DATA-INIZ-VALID-CONV
      *
           ELSE
      *
              MOVE WPOL-DT-INI-VLDT-CONV
                TO ISPC0211-DATA-INIZ-VALID-CONV
      *
           END-IF.
      *
           MOVE WCOM-DT-ULT-VERS-PROD
             TO ISPC0211-DATA-RIFERIMENTO.
      *
           MOVE WPOL-DT-DECOR
             TO ISPC0211-DATA-DECORR-POLIZZA.
      *
           MOVE WCOM-COD-LIV-AUT-PROFIL
             TO ISPC0211-LIVELLO-UTENTE.
      *
           MOVE IDSV0001-SESSIONE
             TO ISPC0211-SESSION-ID.
      *
           MOVE 'N'
             TO ISPC0211-FLG-REC-PROV.
      *
           MOVE IDSV0001-TIPO-MOVIMENTO
             TO ISPC0211-FUNZIONALITA.
      *
      * --> Valorizzazione occurs area input servizio calcoli e
      * --> controlli con l'output del valorizzatore variabili
      *
           MOVE WSKD-ELE-LIVELLO-MAX-P
             TO ISPC0211-ELE-MAX-SCHEDA-P.

           MOVE WSKD-ELE-LIVELLO-MAX-T
             TO ISPC0211-ELE-MAX-SCHEDA-T.
      *
           PERFORM VAL-SCHEDE-ISPC0211
              THRU VAL-SCHEDE-ISPC0211-EX.
      *
       S11100-PREPARA-ISPS0211-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * CHIAMATA AL SERVIZIO - ISPS0211 -
      *----------------------------------------------------------------*
      *
       S11200-CALL-ISPS0211.
      *
           MOVE 'S11200-CALL-ISPS0211'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           CALL ISPS0211          USING AREA-IDSV0001
                                        WCOM-AREA-STATI
                                        AREA-IO-ISPC0211

           ON EXCEPTION
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'ERRORE CHIAMATA ISPS0211'
                TO CALL-DESC
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
      *
           END-CALL.
      *
       S11200-CALL-ISPS0211-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     GESTIONE DELL'OUTPUT DEL SERVIZIO DI PRODOTTO ISPS0211     *
      *----------------------------------------------------------------*
      *
       S11300-SCORRI-OUT-ISPS0211.
      *
           MOVE 'S11300-SCORRI-OUT-ISPS0211'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           IF ISPC0211-TIPO-LIVELLO-T(IX-AREA-ISPC0211) = 'G' AND
              ISPC0211-IDENT-LIVELLO-T(IX-AREA-ISPC0211) >  0  AND
              ISPC0211-FLG-LIQ-T(IX-AREA-ISPC0211) = 'NO'
      *
              PERFORM S11310-ALLINEA-AREA-TRANCHE
                 THRU S11310-ALLINEA-AREA-TRANCHE-EX
      *
           END-IF.
      *
       S11300-SCORRI-OUT-ISPS0211-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * GESTIONE DELL'OUTPUT DEL SERVIZIO ISPS0211
      *----------------------------------------------------------------*
      *
       S11310-ALLINEA-AREA-TRANCHE.
      *
           MOVE 'S11310-ALLINEA-AREA-TRANCHE'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
      * --> Gestione Tranche Di Granzia
      *
           PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
             UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
                OR IDSV0001-ESITO-KO
      *
               IF WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) =
                  ISPC0211-IDENT-LIVELLO-T(IX-AREA-ISPC0211)
      *
                  PERFORM S11320-GESTIONE-LIV-GAR
                     THRU S11320-GESTIONE-LIV-GAR-EX
      *
               END-IF
      *
           END-PERFORM.
      *
       S11310-ALLINEA-AREA-TRANCHE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *         GESTIONE DEL LIVELLO DI GARANZIA                       *
      *----------------------------------------------------------------*
      *
       S11320-GESTIONE-LIV-GAR.
      *
           MOVE 'S11320-GESTIONE-LIV-GAR'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           PERFORM S11330-CONTR-LIV-GAR
              THRU S11330-CONTR-LIV-GAR-EX
           VARYING IX-COMP-ISPC0211 FROM 1 BY 1
             UNTIL IX-COMP-ISPC0211 >
                   ISPC0211-NUM-COMPON-MAX-ELE-T(IX-AREA-ISPC0211)
                OR IDSV0001-ESITO-KO.
      *
       S11320-GESTIONE-LIV-GAR-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *         TEST SUI COMPONENTI DELLA TABELLA TGA
      *----------------------------------------------------------------*
      *
       S11330-CONTR-LIV-GAR.
      *
           MOVE 'S11330-CONTR-LIV-GAR'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              MOVE ISPC0211-TIPO-DATO-T
                   (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                TO WS-TP-DATO
      *
              IF ISPC0211-CODICE-VARIABILE-T
                (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'MANFEE'
      *
                 EVALUATE TRUE
      *
                   WHEN TD-IMPORTO
      *
                     IF ISPC0211-VALORE-IMP-T
                       (IX-AREA-ISPC0211, IX-COMP-ISPC0211) > 0
      *
                        IF IDSV0001-TIPO-MOVIMENTO = 6024
      *
                           MOVE ISPC0211-VALORE-IMP-T
                               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                             TO WTGA-MANFEE-RICOR(IX-TAB-TGA)
      *
                           MOVE 'RI'
                             TO WTGA-TP-MANFEE-APPL(IX-TAB-TGA)
      *
                        ELSE
      *
                           MOVE ISPC0211-VALORE-IMP-T
                               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                             TO WTGA-MANFEE-ANTIC(IX-TAB-TGA)
      *
                           IF IDSV0001-TIPO-MOVIMENTO = 6025
      *
                              MOVE 'C1'
                                TO WTGA-TP-MANFEE-APPL(IX-TAB-TGA)
      *
                           ELSE
      *
                              MOVE 'C2'
                                TO WTGA-TP-MANFEE-APPL(IX-TAB-TGA)
      *
                           END-IF
      *
                        END-IF
      *
                        SET  WTGA-ST-MOD(IX-TAB-TGA)
                          TO TRUE
      *
                        SET WK-MANFEE-YES
                          TO TRUE
      *
                     END-IF
      *
                   WHEN OTHER
      *
                     MOVE WK-PGM
                       TO IEAI9901-COD-SERVIZIO-BE
                     MOVE WK-LABEL-ERR
                       TO IEAI9901-LABEL-ERR
                     MOVE '001114'
                       TO IEAI9901-COD-ERRORE
                     MOVE 'ERRORE TIPO COMP. MANFEE ISPS0211'
                       TO IEAI9901-PARAMETRI-ERR
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
      *
                 END-EVALUATE
      *
              END-IF
      *
           END-IF.
      *
       S11330-CONTR-LIV-GAR-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *               GESTIONE DEL MANAGEMENT FEE                      *
      *----------------------------------------------------------------*
      *
       S12000-GESTIONE-MANFEE.
      *
           MOVE 'S12000-GESTIONE-MANFEE'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              IF W660-MANFEE-CONT-SI
      *
                 MOVE W660-PERMANFEE
                   TO WK-PERMANFEE
      *
                 MOVE W660-MESIDIFFMFEE
                   TO WK-MESIDIFFMFEE
      *
                 MOVE W660-DECADELMFEE
                   TO WK-DECADELMFEE
      *
              ELSE
      *
                 PERFORM S12100-PARAMETRI-MANFEE
                    THRU S12100-PARAMETRI-MANFEE-EX
      *
              END-IF
      *
           END-IF.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S12200-CALCOLA-DATE-MANFEE
                 THRU S12200-CALCOLA-DATE-MANFEE-EX
      *
           END-IF.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S12300-CALCOLA-RATA-MANFEE
                 THRU S12300-CALCOLA-RATA-MANFEE-EX
      *
           END-IF.
      *
       S12000-GESTIONE-MANFEE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * ACQUISIZIONE DEL PARAMETRO PERMANFEE
      *----------------------------------------------------------------*
      *
       S12100-PARAMETRI-MANFEE.
      *
           MOVE 'S12100-PARAMETRI-MANFEE'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           IF IDSV0001-TIPO-MOVIMENTO = 6006 OR 6024
      *
              PERFORM S12110-CERCA-PERMANFEE
                 THRU S12110-CERCA-PERMANFEE-EX
      *
           END-IF.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S12120-CERCA-MESIDIFFMFEE
                 THRU S12120-CERCA-MESIDIFFMFEE-EX
      *
           END-IF.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S12130-CERCA-DECADELMFEE
                 THRU S12130-CERCA-DECADELMFEE-EX
      *
           END-IF.
      *
       S12100-PARAMETRI-MANFEE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * ACQUISIZIONE DEL PARAMETRO PERMANFEE
      *----------------------------------------------------------------*
      *
       S12110-CERCA-PERMANFEE.
      *
           MOVE 'S12110-CERCA-PERMANFEE'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           MOVE 'PERMANFEE'
             TO LDBV1131-COD-PARAM.
      *
           PERFORM S12190-IMPOSTA-PARAM-OGG
              THRU S12190-IMPOSTA-PARAM-OGG-EX.
      *
           PERFORM S12199-LEGGI-PARAM-OGG
              THRU S12199-LEGGI-PARAM-OGG-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              IF WPOG-VAL-NUM-NULL(IX-TAB-POG) NOT = HIGH-VALUES
      *
                 MOVE WPOG-VAL-NUM(IX-TAB-POG)
                   TO WK-PERMANFEE
      *
              ELSE
      *
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL-ERR
                   TO IEAI9901-LABEL-ERR
                 MOVE '001114'
                   TO IEAI9901-COD-ERRORE
                 MOVE 'PARAMETRO PERMANFEE NON REPERITO'
                   TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
      *
              END-IF
      *
           END-IF.
      *
       S12110-CERCA-PERMANFEE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * ACQUISIZIONE DEL PARAMETRO MESIDIFFMFEE
      *----------------------------------------------------------------*
      *
       S12120-CERCA-MESIDIFFMFEE.
      *
           MOVE 'S12120-CERCA-MESIDIFFMFEE'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           MOVE 'MESIDIFFMFEE'
             TO LDBV1131-COD-PARAM.
      *
           PERFORM S12190-IMPOSTA-PARAM-OGG
              THRU S12190-IMPOSTA-PARAM-OGG-EX.
      *
           PERFORM S12199-LEGGI-PARAM-OGG
              THRU S12199-LEGGI-PARAM-OGG-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              IF WPOG-VAL-NUM-NULL(IX-TAB-POG) NOT = HIGH-VALUES
      *
                 MOVE WPOG-VAL-NUM(IX-TAB-POG)
                   TO WK-MESIDIFFMFEE
      *
              ELSE
      *
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL-ERR
                   TO IEAI9901-LABEL-ERR
                 MOVE '001114'
                   TO IEAI9901-COD-ERRORE
                 MOVE 'PARAMETRO MESIDIFFMFEE NON REPERITO'
                   TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
      *
              END-IF
      *
           END-IF.
      *
       S12120-CERCA-MESIDIFFMFEE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * ACQUISIZIONE DEL PARAMETRO DECADELMFEE
      *----------------------------------------------------------------*
      *
       S12130-CERCA-DECADELMFEE.
      *
           MOVE 'S12130-CERCA-DECADELMFEE'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           MOVE 'DECADELMFEE'
             TO LDBV1131-COD-PARAM.
      *
           PERFORM S12190-IMPOSTA-PARAM-OGG
              THRU S12190-IMPOSTA-PARAM-OGG-EX.
      *
           PERFORM S12199-LEGGI-PARAM-OGG
              THRU S12199-LEGGI-PARAM-OGG-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              IF WPOG-VAL-NUM-NULL(IX-TAB-POG) NOT = HIGH-VALUES
      *
                 MOVE WPOG-VAL-NUM(IX-TAB-POG)
                   TO WK-DECADELMFEE
      *
              ELSE
      *
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL-ERR
                   TO IEAI9901-LABEL-ERR
                 MOVE '001114'
                   TO IEAI9901-COD-ERRORE
                 MOVE 'PARAMETRO DECADELMFEE NON REPERITO'
                   TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
      *
              END-IF
      *
           END-IF.
      *
       S12130-CERCA-DECADELMFEE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VALORIZZAZIONI PER LA LETTURA CON WHERE CONDITION AD HOC
      *----------------------------------------------------------------*
      *
       S12190-IMPOSTA-PARAM-OGG.
      *
           MOVE 'S12190-IMPOSTA-PARAM-OGG'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           MOVE WPOL-ID-POLI
             TO LDBV1131-ID-OGG.
           MOVE 'PO'
             TO LDBV1131-TP-OGG.
      *
      *  --> La data effetto viene sempre valorizzata
      *
           MOVE ZERO
             TO IDSI0011-DATA-INIZIO-EFFETTO
                IDSI0011-DATA-FINE-EFFETTO
                IDSI0011-DATA-COMPETENZA.
      *
           SET IDSI0011-TRATT-DEFAULT
             TO TRUE.
      *
      *  --> Nome tabella fisica db
      *
           MOVE LDBS1130
             TO IDSI0011-CODICE-STR-DATO.
      *
      *  --> Dclgen tabella
      *
           MOVE AREA-LDBV1131
             TO IDSI0011-BUFFER-WHERE-COND.
           MOVE SPACES
             TO IDSI0011-BUFFER-DATI.
      *
      *  --> Tipo operazione
      *
           SET IDSI0011-SELECT
             TO TRUE.
           SET IDSI0011-TRATT-DEFAULT
             TO TRUE.
           SET IDSI0011-WHERE-CONDITION
             TO TRUE.
      *
      *  --> Modalita di accesso
      *
           SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
      *
       S12190-IMPOSTA-PARAM-OGG-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * LETTURA DELLA TABELLA PARAMETRO OGGETTO
      *----------------------------------------------------------------*
      *
       S12199-LEGGI-PARAM-OGG.
      *
           MOVE 'S12199-LEGGI-PARAM-OGG'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX
      *
           IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
      *  --> Chiave non trovata
      *
                       EVALUATE LDBV1131-COD-PARAM
      *
                         WHEN 'PERMANFEE'
      *
                           MOVE 'PERMANFEE NON TROVATO SU PARAM_OGG'
                             TO IEAI9901-PARAMETRI-ERR
      *
                         WHEN 'MESIDIFFMFEE'
      *
                           MOVE 'MESIDIFFMFEE NON TROVATO SU PARAM_OGG'
                             TO IEAI9901-PARAMETRI-ERR
      *
                       END-EVALUATE
      *
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL-ERR
                         TO IEAI9901-LABEL-ERR
                       MOVE '005069'
                         TO IEAI9901-COD-ERRORE
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
      *  --> Operazione eseguita correttamente
      *
                       MOVE WPOG-ELE-PARAM-OGG-MAX
                         TO IX-TAB-POG
                       ADD 1
                         TO IX-TAB-POG
      *
                       IF IX-TAB-POG >  WK-POG-MAX-A
      *
                          MOVE WK-PGM
                            TO IEAI9901-COD-SERVIZIO-BE
                          MOVE WK-LABEL-ERR
                            TO IEAI9901-LABEL-ERR
                          MOVE '005069'
                            TO IEAI9901-COD-ERRORE
                          MOVE 'OVERFLOW CARICAMENTO TABELLA PARAM-OGG'
                            TO IEAI9901-PARAMETRI-ERR
                          PERFORM S0300-RICERCA-GRAVITA-ERRORE
                             THRU EX-S0300
      *
                       ELSE
      *
                          MOVE IX-TAB-POG
                            TO WPOG-ELE-PARAM-OGG-MAX
                          MOVE IDSO0011-BUFFER-DATI
                            TO PARAM-OGG
                          PERFORM VALORIZZA-OUTPUT-POG
                             THRU VALORIZZA-OUTPUT-POG-EX
                          SET WPOG-ST-INV(IX-TAB-POG)
                           TO TRUE
      *
                       END-IF
      *
                  WHEN OTHER
      *
      *  --> Errore di accesso al db
      *
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL-ERR
                         TO IEAI9901-LABEL-ERR
                       MOVE '005069'
                         TO IEAI9901-COD-ERRORE
                       MOVE 'ERRORE LETTURA PARAMETRO OGGETTO'
                         TO IEAI9901-PARAMETRI-ERR
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
      *
           ELSE
      *
      *  --> Errore dispatcher
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              MOVE 'ERRORE DISPATCHER LETTURA PARAMETRO OGGETTO'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
       S12199-LEGGI-PARAM-OGG-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * CALCOLO DELLE DATE DI RICORRENZA SUCCESSIVA E ULTIMA EROGAZIONE
      *----------------------------------------------------------------*
      *
       S12200-CALCOLA-DATE-MANFEE.
      *
           MOVE 'S12200-CALCOLA-DATE-MANFEE'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           MOVE ZEROES
             TO WK-DT-CALCOLATA.
      *
      * --> Vengono Aggiunti Gli Eventuali Mesi Di Differimento
      *
           PERFORM S12210-AGGIUNGI-DIFFMANFEE
              THRU S12210-AGGIUNGI-DIFFMANFEE-EX
      *
      * --> Viene Determinato Il Giorno Di Ricorrenza In Funzione
      * --> Della Decade Prevista
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S12292-VALUTA-DECADE
                 THRU S12292-VALUTA-DECADE-EX
      *
           END-IF.
      *
      * --> Alla Data Effetto Sono State Aggiunti I Mesi Di
      * --> Differimento Ed Il Giorno H Stato Calcolato In Base
      * --> Alla Decade Di Riferimento, il risultato h la
      * --> Data Ricorrenza Di Liquidazione
      *
           IF IDSV0001-ESITO-OK
      *
              MOVE WK-DT-CALCOLATA
                TO WK-DT-RICOR-SUCC
      *
           END-IF.
      *
           IF IDSV0001-ESITO-OK
      *
      * --> Se Il Parametro Permanfee H Uguale A 1
      * --> Il Management Fee Viene Erogato In Un Unica Rata Quindi
      * --> La Data Di Ricorrenza Successiva H Uguale Alla Data
      * --> Di Ultima Erogazione Management Fee
      * --> In Caso Contrario Il Parametro Permanfee Rappresenta Il
      * --> Numero Di Rate Da Erogare
      *
              IF WK-PERMANFEE = 1  OR
                 IDSV0001-TIPO-MOVIMENTO = 6025 OR 6026
      *
                 MOVE WK-DT-RICOR-SUCC
                   TO WK-DT-ULT-EROG-MF
      *
              ELSE
      *
                 PERFORM S12220-CALC-ULT-EROG-MF
                    THRU S12220-CALC-ULT-EROG-MF-EX
      *
              END-IF
      *
           END-IF.
      *
       S12200-CALCOLA-DATE-MANFEE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * AGGIUNGE I MESI DI DIFFERIMENTO ALLA RICORRENZA SUCCESSIVA
      *----------------------------------------------------------------*
      *
       S12210-AGGIUNGI-DIFFMANFEE.
      *
           MOVE 'S12210-AGGIUNGI-DIFFMANFEE'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
      * --> A2K-INAMG -> Data Di Partenza Alla Quale Sommare Il Delta
      *
           MOVE IDSV0001-DATA-EFFETTO
             TO A2K-INAMG.
      *
      * --> A2K-TDELTA -> Tipo Delta: Mesi(M) Anni(A)
      *
           MOVE 'M'
             TO A2K-TDELTA.
      *
      * --> A2K-DELTA  -> Delta Da Sommare
      *
           MOVE WK-MESIDIFFMFEE
             TO A2K-DELTA.
      *
      * --> A2K-FUNZ -> Tipo Funzione: Somma Delta
      *
           MOVE '02'
             TO A2K-FUNZ.
      *
      * --> A2K-INFO -> Tipo Formato Data: AAAAMMGG
      *
           MOVE '03'
             TO A2K-INFO.
      *
      * --> A2K-INICON -> Giorno Inizio Conteggio (Stesso Giorno)
      *
           MOVE '0'
             TO A2K-INICON.
      *
      * --> A2K-FISLAV -> Tipo Giorni Da Sommare (Fissi)
      *
           MOVE '0'
             TO A2K-FISLAV.
      *
           PERFORM S12291-CHIAMA-LCCS0003
              THRU S12291-CHIAMA-LCCS0003-EX.
      *
       S12210-AGGIUNGI-DIFFMANFEE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * CALCOLA DATA ULTIMA EROGAZIONE MANAGEMENT FEE
      *----------------------------------------------------------------*
      *
       S12220-CALC-ULT-EROG-MF.
      *
           MOVE 'S12220-CALC-ULT-EROG-MF'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
      * --> A2K-INAMG -> Data Di Partenza Alla Quale Sommare Il Delta
      *
           MOVE WK-DT-CALCOLATA
             TO A2K-INAMG.
      *
      * --> A2K-TDELTA -> Tipo Delta: Mesi(M) Anni(A)
      *
           MOVE 'M'
             TO A2K-TDELTA.
      *
      * --> A2K-DELTA  -> Delta Da Sommare
      *
           MOVE 1
             TO A2K-DELTA.
      *
      * --> A2K-FUNZ -> Tipo Funzione: Somma Delta
      *
           MOVE '02'
             TO A2K-FUNZ.
      *
      * --> A2K-INFO -> Tipo Formato Data: AAAAMMGG
      *
           MOVE '03'
             TO A2K-INFO.
      *
      * --> A2K-INICON -> Giorno Inizio Conteggio (Stesso Giorno)
      *
           MOVE '0'
             TO A2K-INICON.
      *
      * --> A2K-FISLAV -> Tipo Giorni Da Sommare (Fissi)
      *
           MOVE '0'
             TO A2K-FISLAV.
      *
           PERFORM S12230-AGGIUNGI-FRAZ
              THRU S12230-AGGIUNGI-FRAZ-EX
           VARYING IX-FRAZ FROM 1 BY 1
             UNTIL IX-FRAZ NOT < WK-PERMANFEE
                OR IDSV0001-ESITO-KO.
      *
       S12220-CALC-ULT-EROG-MF-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * AGGIUNGE TANTI FRAZIONAMENTI QUANTE SONO LE RATE
      *----------------------------------------------------------------*
      *
       S12230-AGGIUNGI-FRAZ.
      *
           MOVE 'S12230-AGGIUNGI-FRAZ'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           PERFORM S12291-CHIAMA-LCCS0003
              THRU S12291-CHIAMA-LCCS0003-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S12292-VALUTA-DECADE
                 THRU S12292-VALUTA-DECADE-EX
      *
           END-IF.
      *
           IF IDSV0001-ESITO-OK
      *
              MOVE WK-DT-CALCOLATA
                TO A2K-INAMG
                   WK-DT-ULT-EROG-MF
      *
           END-IF.
      *
       S12230-AGGIUNGI-FRAZ-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * CALCOLO DEL VALORE DI MANAGEMENT FEE DA RIPORATRE SULLA PMO
      *----------------------------------------------------------------*
      *
       S12300-CALCOLA-RATA-MANFEE.
      *
           MOVE 'S12300-CALCOLA-RATA-MANFEE'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
                UNTIL IX-TAB-GRZ > WGRZ-ELE-GAR-MAX
      *
                  PERFORM S12310-SOMMA-MANFEE-TGA
                     THRU S12310-SOMMA-MANFEE-TGA-EX
      *
                  PERFORM S12320-CREA-PMO-MANFEE
                     THRU S12320-CREA-PMO-MANFEE-EX
      *
              END-PERFORM
      *
           END-IF.
      *
       S12300-CALCOLA-RATA-MANFEE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * SOMMATORIA DELL'IMPORTO DI MANFFE DELLE TRANCHE (PER OGNI GAR)
      *----------------------------------------------------------------*
      *
       S12310-SOMMA-MANFEE-TGA.
      *
           MOVE 'S12310-SOMMA-MANFEE-TGA'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           MOVE ZEROES
             TO WK-TOT-MANFEE.
      *
      * --> Per Ogni Garanzia Vengono Sommati I Valori Di Management
      * --> Fee Di Ogni Tranche Tenedo In Considerazione La Tipologia
      * --> Viene Abbattuto Il Valore Delle Tranche Negative E
      * --> Sommato Per Le Altre Tipologie
      *
           PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
             UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
      *
                IF WGRZ-ID-GAR(IX-TAB-GRZ) =
                   WTGA-ID-GAR(IX-TAB-TGA)
      *
                   IF WTGA-ST-MOD(IX-TAB-TGA)
      *
                      IF WTGA-MANFEE-RICOR-NULL(IX-TAB-TGA)
                         NOT = HIGH-VALUES
      *
                         MOVE WTGA-TP-TRCH(IX-TAB-TGA)
                           TO WS-TP-TRCH
      *
                         IF TP-TRCH-NEG-PRCOS       OR
                            TP-TRCH-NEG-RIS-PAR     OR
                            TP-TRCH-NEG-RIS-PRO     OR
                            TP-TRCH-NEG-IMPST-SOST  OR
                            TP-TRCH-NEG-DA-DIS
      *
                            COMPUTE WK-TOT-MANFEE =
                                    WK-TOT-MANFEE -
                                    WTGA-MANFEE-RICOR(IX-TAB-TGA)
      *
                         ELSE
      *
                            COMPUTE WK-TOT-MANFEE =
                                    WK-TOT-MANFEE +
                                    WTGA-MANFEE-RICOR(IX-TAB-TGA)
      *
                         END-IF
      *
                      END-IF
      *
                   END-IF
      *
                END-IF
      *
           END-PERFORM.
      *
       S12310-SOMMA-MANFEE-TGA-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *         VALORIZZAZIONE DELLA PMO PER IL MANFEE                 *
      *----------------------------------------------------------------*
      *
       S12320-CREA-PMO-MANFEE.
      *
           MOVE 'S12320-CREA-PMO-MANFEE'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           MOVE WPMO-ELE-PARAM-MOV-MAX
             TO IX-TAB-PMO.
           ADD 1
             TO IX-TAB-PMO.
           MOVE IX-TAB-PMO
             TO WPMO-ELE-PARAM-MOV-MAX.
      *
           SET WPMO-ST-ADD(IX-TAB-PMO)
             TO TRUE.
      *
           MOVE IX-TAB-PMO
             TO WPMO-ID-PARAM-MOVI(IX-TAB-PMO).
      *
           MOVE WGRZ-ID-GAR(IX-TAB-GRZ)
             TO WPMO-ID-OGG(IX-TAB-PMO).
      *
           SET GARANZIA
             TO TRUE
           MOVE WS-TP-OGG
             TO WPMO-TP-OGG(IX-TAB-PMO).
      *
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
             TO WPMO-COD-COMP-ANIA(IX-TAB-PMO).
      *
           EVALUATE IDSV0001-TIPO-MOVIMENTO
      *
               WHEN 6006
                  MOVE 6023
                    TO WPMO-TP-MOVI(IX-TAB-PMO)
      *
               WHEN 6024
                  MOVE 6028
                    TO WPMO-TP-MOVI(IX-TAB-PMO)
      *
               WHEN 6025
                  MOVE 6029
                    TO WPMO-TP-MOVI(IX-TAB-PMO)
      *
               WHEN 6026
                  MOVE 6030
                    TO WPMO-TP-MOVI(IX-TAB-PMO)
      *
           END-EVALUATE.
      *
           MOVE WK-MESIDIFFMFEE
             TO WPMO-MM-DIFF(IX-TAB-PMO).
      *
           MOVE WADE-ID-ADES(1)
             TO WPMO-ID-ADES(IX-TAB-PMO).
      *
           MOVE WPOL-ID-POLI
             TO WPMO-ID-POLI(IX-TAB-PMO).
      *
           MOVE WPOL-TP-FRM-ASSVA
             TO WPMO-TP-FRM-ASSVA(IX-TAB-PMO).
      *
           IF IDSV0001-TIPO-MOVIMENTO = 6006 OR 6024
      *
              COMPUTE WPMO-IMP-RAT-MANFEE(IX-TAB-PMO) =
                     (WK-TOT-MANFEE / WK-PERMANFEE)
      *
              MOVE WK-PERMANFEE
                TO WPMO-FRQ-MOVI(IX-TAB-PMO)
      *
           ELSE
      *
              MOVE WK-TOT-MANFEE
                TO WPMO-IMP-RAT-MANFEE(IX-TAB-PMO)
      *
           END-IF.
      *
           MOVE WK-DT-RICOR-SUCC
             TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO).
      *
           MOVE WK-DT-ULT-EROG-MF
             TO WPMO-DT-ULT-EROG-MANFEE(IX-TAB-PMO).
      *
22584      MOVE WPOL-COD-RAMO
22584        TO WPMO-COD-RAMO(IX-TAB-PMO).
      *
       S12320-CREA-PMO-MANFEE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * CALL AL MODULO LCCS0003
      *----------------------------------------------------------------*
      *
       S12291-CHIAMA-LCCS0003.
      *
           MOVE 'S12291-CHIAMA-LCCS0003'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           CALL LCCS0003         USING IO-A2K-LCCC0003
                                       IN-RCODE
           ON EXCEPTION
      *
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'ERRORE CHIAMATA LCCS0003'
                   TO CALL-DESC
                 MOVE WK-LABEL-ERR
                   TO IEAI9901-LABEL-ERR
                 PERFORM S0290-ERRORE-DI-SISTEMA
                    THRU EX-S0290
      *
           END-CALL.
      *
           IF IN-RCODE  = '00'
      *
              MOVE A2K-OUAMG
                TO A2K-INAMG
                   WK-DT-CALCOLATA
      *
           ELSE
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'ERRORE CALCOLO DATA MF'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
       S12291-CHIAMA-LCCS0003-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VALUTAZIONE DELLA DECADE INDICATA DAL PARAMETRO
      *----------------------------------------------------------------*
      *
       S12292-VALUTA-DECADE.
      *
           MOVE 'S12292-VALUTA-DECADE'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           MOVE WK-DT-CALCOLATA
             TO WK-APPO-DT
      *
           EVALUATE WK-DECADELMFEE
      *
               WHEN 1
      *
                  MOVE 10
                    TO WK-APPO-GG
                  MOVE WK-APPO-DT
                    TO WK-DT-CALCOLATA
      *
               WHEN 2
      *
                  MOVE 20
                    TO WK-APPO-GG
                  MOVE WK-APPO-DT
                    TO WK-DT-CALCOLATA
      *
               WHEN 3
      *
                  MOVE WK-APPO-DT
                    TO S029-DT-CALC
      *
                  PERFORM S12293-GEST-FINE-MESE
                     THRU S12293-GEST-FINE-MESE-EX
      *
               WHEN OTHER
      *
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE WK-LABEL-ERR
                    TO IEAI9901-LABEL-ERR
                  MOVE '001114'
                    TO IEAI9901-COD-ERRORE
                  MOVE 'VALORE PARAMETRO DECADELMFEE NON AMMESSO'
                    TO IEAI9901-PARAMETRI-ERR
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
      *
           END-EVALUATE.
      *
       S12292-VALUTA-DECADE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * CERCA L'ULTIMO GIORNO DEL MESE IN ESAME
      *----------------------------------------------------------------*
      *
       S12293-GEST-FINE-MESE.
      *
           MOVE 'S12293-GEST-FINE-MESE'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           CALL LCCS0029         USING AREA-IDSV0001
                                       AREA-IO-LCCS0029
           ON EXCEPTION
      *
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'ERRORE CHIAMATA LCCS0003'
                   TO CALL-DESC
                 MOVE WK-LABEL-ERR
                   TO IEAI9901-LABEL-ERR
                 PERFORM S0290-ERRORE-DI-SISTEMA
                    THRU EX-S0290
      *
           END-CALL.
      *
           IF IDSV0001-ESITO-OK
      *
              MOVE S029-DT-CALC
                TO WK-DT-CALCOLATA
      *
           END-IF.
      *
       S12293-GEST-FINE-MESE-EX.
           EXIT.
      *
      * ============================================================== *
      * -->           O P E R Z I O N I   F I N A L I              <-- *
      * ============================================================== *
      *
       S90000-OPERAZ-FINALI.
      *
           MOVE 'S90000-OPERAZ-FINALI'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
       S90000-OPERAZ-FINALI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     ROUTINES CALL DISPATCHER                                   *
      *----------------------------------------------------------------*
      *
           COPY IDSP0012.
      *
      *----------------------------------------------------------------*
      *    COPY PER LETTURA MATRICE MOVIMENTO
      *----------------------------------------------------------------*
      *
           COPY LCCP0021.
      *
      *----------------------------------------------------------------*
      *  ROUTINES GESTIONE ERRORI                                      *
      *----------------------------------------------------------------*
      *
           COPY IERP9901.
           COPY IERP9902.
           COPY IERP9903.
      *
      *----------------------------------------------------------------*
      *  COPY DI VALORIZZAZIONE OUTPUT LETTURA DB
      *----------------------------------------------------------------*
      *
      *  --> PARAMETRO OGGETTO
           COPY LCCVPOG3                REPLACING ==(SF)== BY ==WPOG==.
      *
      *----------------------------------------------------------------*
      *    COPY INIZIALIZZAZIONE TABELLE
      *----------------------------------------------------------------*
      *
      *  --> PARAMETRO OGGETTO
           COPY LCCVPOG4                REPLACING ==(SF)== BY ==WPOG==.
      *
      *----------------------------------------------------------------*
      * DIPLAY LABEL
      *----------------------------------------------------------------*
      *
       DISPLAY-LABEL.
      *
           IF (IDSV0001-DEBUG-BASSO OR IDSV0001-DEBUG-ESASPERATO) AND
           NOT IDSV0001-ON-LINE
      *
      *       DISPLAY WK-LABEL-ERR
              CONTINUE
      *
           END-IF.
      *
       DISPLAY-LABEL-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * DIPLAY ESITO PRODOTTO
      *----------------------------------------------------------------*
      *
      *DISPLAY-ESITO-PROD.
      *
      *    IF IDSV0001-DEBUG-ESASPERATO  AND  NOT IDSV0001-ON-LINE
      *
      *      DISPLAY 'ESITO PRODOTTO : ' ISPC0211-ESITO
      *      DISPLAY 'NUMERO ELEMENTI: ' ISPC0211-ERR-NUM-ELE
      *
      *      PERFORM VARYING TEST-I FROM 1 BY 1
      *        UNTIL TEST-I > ISPC0211-ERR-NUM-ELE
      *
      *         DISPLAY 'DESCRIZIONE ERR: '
      *               ISPC0211-DESCRIZIONE-ERR(TEST-I)
      *
      *      END-PERFORM
      *
      *    END-IF.
      *
      *DISPLAY-ESITO-PROD-EX.
      *    EXIT.
      *
      *----------------------------------------------------------------*
      * DIPLAY COMPONENTI OUTPUT DI PRODOTTO
      *----------------------------------------------------------------*
      *
      *DISPLAY-COMPONENTE.
      *
      *    IF (IDSV0001-DEBUG-MEDIO OR IDSV0001-DEBUG-ESASPERATO) AND
      *    NOT IDSV0001-ON-LINE
      *
      *        DISPLAY ' TIPO-LIVELLO : '
      *                  ISPC0211-TIPO-LIVELLO(IX-AREA-ISPC0211)
      *
      *        DISPLAY ' CODICE-LIVELLO: '
      *                  ISPC0211-CODICE-LIVELLO(IX-AREA-ISPC0211)
      *
      *        MOVE ISPC0211-IDENT-LIVELLO(IX-AREA-ISPC0211)
      *          TO WK-ID
      *        DISPLAY ' ID-LIVELLO :'   WK-ID
      *
      *        MOVE ISPC0211-TIPO-DATO
      *            (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
      *          TO WS-TP-DATO
      *        DISPLAY 'TIPO-DATO: ' WS-TP-DATO
      *
      *        DISPLAY 'COMPONENTE: '
      *                 ISPC0211-CODICE-VARIABILE
      *                (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
      *
      *        EVALUATE TRUE
      *
      *          WHEN TD-IMPORTO
      *
      *             DISPLAY ' IMPORTO : '
      *                       ISPC0211-VALORE-IMP
      *                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
      *
      *          WHEN TD-PERC-CENTESIMI
      *
      *             DISPLAY ' PERCENTUALE : '
      *                       ISPC0211-VALORE-PERC
      *                      (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
      *
      *          WHEN OTHER
      *
      *             DISPLAY '? ' WS-TP-DATO
      *
      *         END-EVALUATE
      *
      *    END-IF.
      *
      *DISPLAY-COMPONENTE-EX.
      *    EXIT.

      *----------------------------------------------------------------*
      *    ROUTINES DI GESTIONE DISPLAY
      *----------------------------------------------------------------*
           COPY IDSP8888.
      *----------------------------------------------------------------*
      *    COPY PER VALORIZZAZIONE SCHEDE SERVIZIO ISPS0211
      *----------------------------------------------------------------*
           COPY ISPP0211.

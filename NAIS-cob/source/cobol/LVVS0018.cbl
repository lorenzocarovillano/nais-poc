      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0018.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0018
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0018'.
       01  WK-LVVS0000                      PIC X(008) VALUE 'LVVS0000'.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-DATA-OUTPUT                  PIC 9(04)V9(07).
       01  WK-DATA-OUTPUT-V REDEFINES WK-DATA-OUTPUT.
           03 WK-NUMERICI                  PIC 9(04).
           03 WK-DECIMALI                  PIC 9(07).

       01 WK-DATA-X-12.
          03 WK-X-AA                          PIC X(04).
          03 WK-VIROGLA                       PIC X(01) VALUE ','.
          03 WK-X-GG                          PIC X(07).



      *----------------------------------------------------------------*
      *    COPY DI INPUT PER LA CHIAMATA AL LVVS0000
      *----------------------------------------------------------------*
       01  INPUT-LVVS0000.
           COPY LVVC0000        REPLACING ==(SF)== BY ==LVVC0000==.
      *---------------------------------------------------------------*
      *  LISTA MOVIMENTI IMPLEMENTATI LCCC0006
      *---------------------------------------------------------------*
           COPY LCCC0006.

      *----------------------------------------------------------------*
      *--> AREE IMPUT
      *----------------------------------------------------------------*
       01 AREA-IO-LQU.
          03 DLQU-AREA-LIQUIDAZIONI.
             04 DLQU-ELE-LQU-MAX       PIC S9(04) COMP.
                COPY LCCVLQUB REPLACING   ==(SF)==  BY ==DLQU==.
             COPY LCCVLQU1             REPLACING ==(SF)== BY ==DLQU==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
           03 IX-TAB-LQU                     PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0018.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0018.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
      *     SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
      *     SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE AREA-IO-LQU
                      WK-DATA-OUTPUT
                      WK-DATA-X-12.
           INITIALIZE INPUT-LVVS0000.


      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1100-VALORIZZA-DCLGEN
                  THRU S1100-VALORIZZA-DCLGEN-EX
               VARYING IX-DCLGEN FROM 1 BY 1
                 UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                    OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                       SPACES OR LOW-VALUE OR HIGH-VALUE
           END-IF.

      *--> PERFORM DI CONTROLLI SUI I CAMPI CARICATI NELLE
      *--> DCLGEN DI WORKING
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1200-CONTROLLO-DATI
                  THRU S1200-CONTROLLO-DATI-EX
           ELSE
             IF IDSV0003-NOT-FOUND
                MOVE IVVC0213-DATA-EFFETTO
                  TO LVVC0000-DATA-INPUT-1
                PERFORM S1250-CONVERTI-DATA
                   THRU S1250-CONVERTI-DATA-EX
             END-IF
           END-IF.

       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-LIQUIDAZ
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DLQU-AREA-LIQUIDAZIONI
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO DATI DCLGEN
      *----------------------------------------------------------------*
       S1200-CONTROLLO-DATI.
           PERFORM VARYING IX-TAB-LQU FROM 1 BY 1
             UNTIL IX-TAB-LQU > DLQU-ELE-LQU-MAX

                IF DLQU-DT-DEN(IX-TAB-LQU) NOT NUMERIC
                   MOVE IVVC0213-DATA-EFFETTO
                     TO DLQU-DT-DEN(IX-TAB-LQU)
                ELSE
                   IF DLQU-DT-DEN(IX-TAB-LQU) = 0
                      MOVE IVVC0213-DATA-EFFETTO
                        TO DLQU-DT-DEN(IX-TAB-LQU)
                   END-IF
                END-IF

           END-PERFORM.

           PERFORM VARYING IX-TAB-LQU FROM 1 BY 1
             UNTIL IX-TAB-LQU > DLQU-ELE-LQU-MAX

                IF  DLQU-ID-OGG(IX-TAB-LQU) = IVVC0213-ID-ADESIONE
                AND DLQU-TP-OGG (IX-TAB-LQU) = 'AD'
                    MOVE DLQU-DT-DEN(IX-TAB-LQU)
                      TO LVVC0000-DATA-INPUT-1
                END-IF
           END-PERFORM.

           PERFORM S1250-CONVERTI-DATA THRU S1250-CONVERTI-DATA-EX.

       S1200-CONTROLLO-DATI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CHIAMATA AL SERVIZIO DI CALCOLO DATA
      *----------------------------------------------------------------*
       S1250-CONVERTI-DATA.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET IDSV0003-SUCCESSFUL-RC     TO TRUE
           SET IDSV0003-SUCCESSFUL-SQL    TO TRUE
      *
      *--> CALL MODULO PER RECUPERO DATA
           CALL WK-LVVS0000 USING       IDSV0003
                                       INPUT-LVVS0000.

           IF IDSV0003-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSV0003-SUCCESSFUL-SQL
      *-->             OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE LVVC0000-DATA-OUTPUT
                         TO IVVC0213-VAL-IMP-O

                  WHEN OTHER
      *--->            ERRORE DI ACCESSO AL DB
                       SET IDSV0003-INVALID-OPER TO TRUE
                       MOVE WK-PGM
                         TO IDSV0003-COD-SERVIZIO-BE
                       STRING IDSV0003-RETURN-CODE  ';'
                              IDSV0003-SQLCODE
                              DELIMITED BY SIZE
                              INTO IDSV0003-DESCRIZ-ERR-DB2
                       END-STRING

              END-EVALUATE
           ELSE
      *-->    GESTIONE ERRORE DISPATCHER
              SET IDSV0003-INVALID-OPER TO TRUE

              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING IDSV0003-RETURN-CODE  ';'
                     IDSV0003-SQLCODE
                     DELIMITED BY SIZE
                     INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           END-IF.

       S1250-CONVERTI-DATA-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           MOVE SPACES                     TO IVVC0213-VAL-STR-O.
           MOVE 0                          TO IVVC0213-VAL-PERC-O.
      *
           GOBACK.

       EX-S9000.
           EXIT.

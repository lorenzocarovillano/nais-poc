      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0109.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0109
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0109'.
      *--> ROUTINE CALCOLO ETA' ASSICURATO
       77  LCCS1750                         PIC X(08)  VALUE 'LCCS1750'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-DATA-OUTPUT                  PIC 9(04)V9(07).
       01  WK-DATA-OUTPUT-V REDEFINES WK-DATA-OUTPUT.
           03 WK-NUMERICI                  PIC 9(04).
           03 WK-DECIMALI                  PIC 9(07).

       01 WK-DT-APPOGGIO                   PIC 9(08) VALUE ZEROES.

       01 WK-RESTO                         PIC 9(002) VALUE ZERO.
       01 WK-INTERO                        PIC 9(005) VALUE ZERO.

      *-- AREA ROUTINE PER IL CALCOLO
       01  FORMATO                 PIC X(01).
       01  DATA-INFERIORE.
           05 AAAA-INF             PIC 9(04).
           05 MM-INF               PIC 9(02).
           05 GG-INF               PIC 9(02).
       01  DATA-SUPERIORE.
           05 AAAA-SUP             PIC 9(04).
           05 MM-SUP               PIC 9(02).
           05 GG-SUP               PIC 9(02).
       01  GG-DIFF                 PIC 9(05).
       01  GG-DIFF-1               PIC 9(05).
       01  CODICE-RITORNO          PIC X(01).

       01  NUM-GIORNI              PIC S9(9) COMP-3.
ALEX   01  TAB-MESI.
           05  FILLER         PIC X(12) VALUE 'GENNAIO   31'.
           05  FILLER         PIC X(12) VALUE 'FEBBRAIO  28'.
           05  FILLER         PIC X(12) VALUE 'MARZO     31'.
           05  FILLER         PIC X(12) VALUE 'APRILE    30'.
           05  FILLER         PIC X(12) VALUE 'MAGGIO    31'.
           05  FILLER         PIC X(12) VALUE 'GIUGNO    30'.
           05  FILLER         PIC X(12) VALUE 'LUGLIO    31'.
           05  FILLER         PIC X(12) VALUE 'AGOSTO    31'.
           05  FILLER         PIC X(12) VALUE 'SETTEMBRE 30'.
           05  FILLER         PIC X(12) VALUE 'OTTOBRE   31'.
           05  FILLER         PIC X(12) VALUE 'NOVEMBRE  30'.
           05  FILLER         PIC X(12) VALUE 'DICEMBRE  31'.
       01  TAB-MESI-R REDEFINES TAB-MESI.
           05  ELE-TAB-R   OCCURS 12.
               10  MESE             PIC X(10).
               10  GIORNO-FINE-MESE PIC 9(02).
      *---------------------------------------------------------------*
      *  COPY PER CHIAMATA IDBSTIT0
      *---------------------------------------------------------------*
           COPY IDBVTIT1.
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-RAN.
          03 DRAN-AREA-RAN.
             04 DRAN-ELE-RAN-MAX        PIC S9(04) COMP.
                COPY LCCVRANA REPLACING   ==(SF)==  BY ==DRAN==.
             COPY LCCVRAN1              REPLACING ==(SF)== BY ==DRAN==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
           03 IX-TAB-RAN                     PIC S9(04) COMP.

      *----------------------------------------------------------------*
      *    COPY ROUTINE DEL CALCOLO DELL'ETA'
      *----------------------------------------------------------------*
36829      COPY LCCC1751.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0109.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0109.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE AREA-IO-RAN
                      WK-DATA-OUTPUT.
      *
      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1200-CONTRLLO-DATI         THRU S1200-EX
           END-IF.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1260-CALCOLA-DIFF          THRU S1260-EX
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-RAPP-ANAG
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DRAN-AREA-RAN
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA VDA
      *----------------------------------------------------------------*
       S1200-CONTRLLO-DATI.
      *
           IF DRAN-DT-NASC(IVVC0213-IX-TABB) NOT NUMERIC
              SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'DATA DI NASCITA 1  ASS NON VALORIZZATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
           ELSE
              IF DRAN-DT-NASC(IVVC0213-IX-TABB) = ZEROES
                 SET  IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 MOVE WK-PGM
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'DATA DI NASCITA 1  ASS NON VALORIZZATA'
                   TO IDSV0003-DESCRIZ-ERR-DB2
              END-IF
           END-IF.
      *
       S1200-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA DATA 1
      *----------------------------------------------------------------*
       S1260-CALCOLA-DIFF.
      *
           MOVE  DRAN-DT-NASC(IVVC0213-IX-TABB)
                                            TO LCCC1751-DATA-INFERIORE.
           MOVE  IVVC0213-DT-DECOR          TO LCCC1751-DATA-SUPERIORE.

           CALL LCCS1750 USING LCCC1751
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CHIAMATA LCCS1751'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
      *
           IF LCCC1751-RETURN-CODE = SPACE
              IF LCCC1751-ETA-MM-ASSICURATO GREATER 5
                 ADD 1                     TO LCCC1751-ETA-AA-ASSICURATO
              END-IF
              MOVE LCCC1751-ETA-AA-ASSICURATO TO IVVC0213-VAL-IMP-O
           ELSE
              MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
              MOVE LCCC1751-DESCRIZ-ERR   TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
           END-IF.
      *
      *
       S1260-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.


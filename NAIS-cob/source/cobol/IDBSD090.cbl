       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSD090 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  10 MAG 2010.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDD090 END-EXEC.
           EXEC SQL INCLUDE IDBVD092 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVD091 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 PARAM-INFR-APPL.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSD090'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'PARAM_INFR_APPL' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                COD_COMP_ANIA
                ,AMBIENTE
                ,PIATTAFORMA
                ,TP_COM_COBOL_JAVA
                ,MQ_TP_UTILIZZO_API
                ,MQ_QUEUE_MANAGER
                ,MQ_CODA_PUT
                ,MQ_CODA_GET
                ,MQ_OPZ_PERSISTENZA
                ,MQ_OPZ_WAIT
                ,MQ_OPZ_SYNCPOINT
                ,MQ_ATTESA_RISPOSTA
                ,MQ_TEMPO_ATTESA_1
                ,MQ_TEMPO_ATTESA_2
                ,MQ_TEMPO_EXPIRY
                ,CSOCKET_IP_ADDRESS
                ,CSOCKET_PORT_NUM
                ,FL_COMPRESSORE_C
             INTO
                :D09-COD-COMP-ANIA
               ,:D09-AMBIENTE
               ,:D09-PIATTAFORMA
               ,:D09-TP-COM-COBOL-JAVA
               ,:D09-MQ-TP-UTILIZZO-API
                :IND-D09-MQ-TP-UTILIZZO-API
               ,:D09-MQ-QUEUE-MANAGER
                :IND-D09-MQ-QUEUE-MANAGER
               ,:D09-MQ-CODA-PUT
                :IND-D09-MQ-CODA-PUT
               ,:D09-MQ-CODA-GET
                :IND-D09-MQ-CODA-GET
               ,:D09-MQ-OPZ-PERSISTENZA
                :IND-D09-MQ-OPZ-PERSISTENZA
               ,:D09-MQ-OPZ-WAIT
                :IND-D09-MQ-OPZ-WAIT
               ,:D09-MQ-OPZ-SYNCPOINT
                :IND-D09-MQ-OPZ-SYNCPOINT
               ,:D09-MQ-ATTESA-RISPOSTA
                :IND-D09-MQ-ATTESA-RISPOSTA
               ,:D09-MQ-TEMPO-ATTESA-1
                :IND-D09-MQ-TEMPO-ATTESA-1
               ,:D09-MQ-TEMPO-ATTESA-2
                :IND-D09-MQ-TEMPO-ATTESA-2
               ,:D09-MQ-TEMPO-EXPIRY
                :IND-D09-MQ-TEMPO-EXPIRY
               ,:D09-CSOCKET-IP-ADDRESS
                :IND-D09-CSOCKET-IP-ADDRESS
               ,:D09-CSOCKET-PORT-NUM
                :IND-D09-CSOCKET-PORT-NUM
               ,:D09-FL-COMPRESSORE-C
                :IND-D09-FL-COMPRESSORE-C
             FROM PARAM_INFR_APPL
             WHERE     COD_COMP_ANIA = :D09-COD-COMP-ANIA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO PARAM_INFR_APPL
                     (
                        COD_COMP_ANIA
                       ,AMBIENTE
                       ,PIATTAFORMA
                       ,TP_COM_COBOL_JAVA
                       ,MQ_TP_UTILIZZO_API
                       ,MQ_QUEUE_MANAGER
                       ,MQ_CODA_PUT
                       ,MQ_CODA_GET
                       ,MQ_OPZ_PERSISTENZA
                       ,MQ_OPZ_WAIT
                       ,MQ_OPZ_SYNCPOINT
                       ,MQ_ATTESA_RISPOSTA
                       ,MQ_TEMPO_ATTESA_1
                       ,MQ_TEMPO_ATTESA_2
                       ,MQ_TEMPO_EXPIRY
                       ,CSOCKET_IP_ADDRESS
                       ,CSOCKET_PORT_NUM
                       ,FL_COMPRESSORE_C
                     )
                 VALUES
                     (
                       :D09-COD-COMP-ANIA
                       ,:D09-AMBIENTE
                       ,:D09-PIATTAFORMA
                       ,:D09-TP-COM-COBOL-JAVA
                       ,:D09-MQ-TP-UTILIZZO-API
                        :IND-D09-MQ-TP-UTILIZZO-API
                       ,:D09-MQ-QUEUE-MANAGER
                        :IND-D09-MQ-QUEUE-MANAGER
                       ,:D09-MQ-CODA-PUT
                        :IND-D09-MQ-CODA-PUT
                       ,:D09-MQ-CODA-GET
                        :IND-D09-MQ-CODA-GET
                       ,:D09-MQ-OPZ-PERSISTENZA
                        :IND-D09-MQ-OPZ-PERSISTENZA
                       ,:D09-MQ-OPZ-WAIT
                        :IND-D09-MQ-OPZ-WAIT
                       ,:D09-MQ-OPZ-SYNCPOINT
                        :IND-D09-MQ-OPZ-SYNCPOINT
                       ,:D09-MQ-ATTESA-RISPOSTA
                        :IND-D09-MQ-ATTESA-RISPOSTA
                       ,:D09-MQ-TEMPO-ATTESA-1
                        :IND-D09-MQ-TEMPO-ATTESA-1
                       ,:D09-MQ-TEMPO-ATTESA-2
                        :IND-D09-MQ-TEMPO-ATTESA-2
                       ,:D09-MQ-TEMPO-EXPIRY
                        :IND-D09-MQ-TEMPO-EXPIRY
                       ,:D09-CSOCKET-IP-ADDRESS
                        :IND-D09-CSOCKET-IP-ADDRESS
                       ,:D09-CSOCKET-PORT-NUM
                        :IND-D09-CSOCKET-PORT-NUM
                       ,:D09-FL-COMPRESSORE-C
                        :IND-D09-FL-COMPRESSORE-C
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE PARAM_INFR_APPL SET

                   COD_COMP_ANIA          =
                :D09-COD-COMP-ANIA
                  ,AMBIENTE               =
                :D09-AMBIENTE
                  ,PIATTAFORMA            =
                :D09-PIATTAFORMA
                  ,TP_COM_COBOL_JAVA      =
                :D09-TP-COM-COBOL-JAVA
                  ,MQ_TP_UTILIZZO_API     =
                :D09-MQ-TP-UTILIZZO-API
                                       :IND-D09-MQ-TP-UTILIZZO-API
                  ,MQ_QUEUE_MANAGER       =
                :D09-MQ-QUEUE-MANAGER
                                       :IND-D09-MQ-QUEUE-MANAGER
                  ,MQ_CODA_PUT            =
                :D09-MQ-CODA-PUT
                                       :IND-D09-MQ-CODA-PUT
                  ,MQ_CODA_GET            =
                :D09-MQ-CODA-GET
                                       :IND-D09-MQ-CODA-GET
                  ,MQ_OPZ_PERSISTENZA     =
                :D09-MQ-OPZ-PERSISTENZA
                                       :IND-D09-MQ-OPZ-PERSISTENZA
                  ,MQ_OPZ_WAIT            =
                :D09-MQ-OPZ-WAIT
                                       :IND-D09-MQ-OPZ-WAIT
                  ,MQ_OPZ_SYNCPOINT       =
                :D09-MQ-OPZ-SYNCPOINT
                                       :IND-D09-MQ-OPZ-SYNCPOINT
                  ,MQ_ATTESA_RISPOSTA     =
                :D09-MQ-ATTESA-RISPOSTA
                                       :IND-D09-MQ-ATTESA-RISPOSTA
                  ,MQ_TEMPO_ATTESA_1      =
                :D09-MQ-TEMPO-ATTESA-1
                                       :IND-D09-MQ-TEMPO-ATTESA-1
                  ,MQ_TEMPO_ATTESA_2      =
                :D09-MQ-TEMPO-ATTESA-2
                                       :IND-D09-MQ-TEMPO-ATTESA-2
                  ,MQ_TEMPO_EXPIRY        =
                :D09-MQ-TEMPO-EXPIRY
                                       :IND-D09-MQ-TEMPO-EXPIRY
                  ,CSOCKET_IP_ADDRESS     =
                :D09-CSOCKET-IP-ADDRESS
                                       :IND-D09-CSOCKET-IP-ADDRESS
                  ,CSOCKET_PORT_NUM       =
                :D09-CSOCKET-PORT-NUM
                                       :IND-D09-CSOCKET-PORT-NUM
                  ,FL_COMPRESSORE_C       =
                :D09-FL-COMPRESSORE-C
                                       :IND-D09-FL-COMPRESSORE-C
                WHERE     COD_COMP_ANIA = :D09-COD-COMP-ANIA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM PARAM_INFR_APPL
                WHERE     COD_COMP_ANIA = :D09-COD-COMP-ANIA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-D09-MQ-TP-UTILIZZO-API = -1
              MOVE HIGH-VALUES TO D09-MQ-TP-UTILIZZO-API-NULL
           END-IF
           IF IND-D09-MQ-QUEUE-MANAGER = -1
              MOVE HIGH-VALUES TO D09-MQ-QUEUE-MANAGER-NULL
           END-IF
           IF IND-D09-MQ-CODA-PUT = -1
              MOVE HIGH-VALUES TO D09-MQ-CODA-PUT-NULL
           END-IF
           IF IND-D09-MQ-CODA-GET = -1
              MOVE HIGH-VALUES TO D09-MQ-CODA-GET-NULL
           END-IF
           IF IND-D09-MQ-OPZ-PERSISTENZA = -1
              MOVE HIGH-VALUES TO D09-MQ-OPZ-PERSISTENZA-NULL
           END-IF
           IF IND-D09-MQ-OPZ-WAIT = -1
              MOVE HIGH-VALUES TO D09-MQ-OPZ-WAIT-NULL
           END-IF
           IF IND-D09-MQ-OPZ-SYNCPOINT = -1
              MOVE HIGH-VALUES TO D09-MQ-OPZ-SYNCPOINT-NULL
           END-IF
           IF IND-D09-MQ-ATTESA-RISPOSTA = -1
              MOVE HIGH-VALUES TO D09-MQ-ATTESA-RISPOSTA-NULL
           END-IF
           IF IND-D09-MQ-TEMPO-ATTESA-1 = -1
              MOVE HIGH-VALUES TO D09-MQ-TEMPO-ATTESA-1-NULL
           END-IF
           IF IND-D09-MQ-TEMPO-ATTESA-2 = -1
              MOVE HIGH-VALUES TO D09-MQ-TEMPO-ATTESA-2-NULL
           END-IF
           IF IND-D09-MQ-TEMPO-EXPIRY = -1
              MOVE HIGH-VALUES TO D09-MQ-TEMPO-EXPIRY-NULL
           END-IF
           IF IND-D09-CSOCKET-IP-ADDRESS = -1
              MOVE HIGH-VALUES TO D09-CSOCKET-IP-ADDRESS-NULL
           END-IF
           IF IND-D09-CSOCKET-PORT-NUM = -1
              MOVE HIGH-VALUES TO D09-CSOCKET-PORT-NUM-NULL
           END-IF
           IF IND-D09-FL-COMPRESSORE-C = -1
              MOVE HIGH-VALUES TO D09-FL-COMPRESSORE-C-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.

           CONTINUE.
       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.

           CONTINUE.
       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF D09-MQ-TP-UTILIZZO-API-NULL = HIGH-VALUES
              MOVE -1 TO IND-D09-MQ-TP-UTILIZZO-API
           ELSE
              MOVE 0 TO IND-D09-MQ-TP-UTILIZZO-API
           END-IF
           IF D09-MQ-QUEUE-MANAGER-NULL = HIGH-VALUES
              MOVE -1 TO IND-D09-MQ-QUEUE-MANAGER
           ELSE
              MOVE 0 TO IND-D09-MQ-QUEUE-MANAGER
           END-IF
           IF D09-MQ-CODA-PUT-NULL = HIGH-VALUES
              MOVE -1 TO IND-D09-MQ-CODA-PUT
           ELSE
              MOVE 0 TO IND-D09-MQ-CODA-PUT
           END-IF
           IF D09-MQ-CODA-GET-NULL = HIGH-VALUES
              MOVE -1 TO IND-D09-MQ-CODA-GET
           ELSE
              MOVE 0 TO IND-D09-MQ-CODA-GET
           END-IF
           IF D09-MQ-OPZ-PERSISTENZA-NULL = HIGH-VALUES
              MOVE -1 TO IND-D09-MQ-OPZ-PERSISTENZA
           ELSE
              MOVE 0 TO IND-D09-MQ-OPZ-PERSISTENZA
           END-IF
           IF D09-MQ-OPZ-WAIT-NULL = HIGH-VALUES
              MOVE -1 TO IND-D09-MQ-OPZ-WAIT
           ELSE
              MOVE 0 TO IND-D09-MQ-OPZ-WAIT
           END-IF
           IF D09-MQ-OPZ-SYNCPOINT-NULL = HIGH-VALUES
              MOVE -1 TO IND-D09-MQ-OPZ-SYNCPOINT
           ELSE
              MOVE 0 TO IND-D09-MQ-OPZ-SYNCPOINT
           END-IF
           IF D09-MQ-ATTESA-RISPOSTA-NULL = HIGH-VALUES
              MOVE -1 TO IND-D09-MQ-ATTESA-RISPOSTA
           ELSE
              MOVE 0 TO IND-D09-MQ-ATTESA-RISPOSTA
           END-IF
           IF D09-MQ-TEMPO-ATTESA-1-NULL = HIGH-VALUES
              MOVE -1 TO IND-D09-MQ-TEMPO-ATTESA-1
           ELSE
              MOVE 0 TO IND-D09-MQ-TEMPO-ATTESA-1
           END-IF
           IF D09-MQ-TEMPO-ATTESA-2-NULL = HIGH-VALUES
              MOVE -1 TO IND-D09-MQ-TEMPO-ATTESA-2
           ELSE
              MOVE 0 TO IND-D09-MQ-TEMPO-ATTESA-2
           END-IF
           IF D09-MQ-TEMPO-EXPIRY-NULL = HIGH-VALUES
              MOVE -1 TO IND-D09-MQ-TEMPO-EXPIRY
           ELSE
              MOVE 0 TO IND-D09-MQ-TEMPO-EXPIRY
           END-IF
           IF D09-CSOCKET-IP-ADDRESS-NULL = HIGH-VALUES
              MOVE -1 TO IND-D09-CSOCKET-IP-ADDRESS
           ELSE
              MOVE 0 TO IND-D09-CSOCKET-IP-ADDRESS
           END-IF
           IF D09-CSOCKET-PORT-NUM-NULL = HIGH-VALUES
              MOVE -1 TO IND-D09-CSOCKET-PORT-NUM
           ELSE
              MOVE 0 TO IND-D09-CSOCKET-PORT-NUM
           END-IF
           IF D09-FL-COMPRESSORE-C-NULL = HIGH-VALUES
              MOVE -1 TO IND-D09-FL-COMPRESSORE-C
           ELSE
              MOVE 0 TO IND-D09-FL-COMPRESSORE-C
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.

           CONTINUE.
       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.
           CONTINUE.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.
           CONTINUE.
       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.
           CONTINUE.
       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.

           CONTINUE.
       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.

           CONTINUE.
       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA VER. 1.0                          **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LOAS0350.
       AUTHOR.             AISS.
       DATE-WRITTEN.       01/2008.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *    PROGRAMMA ..... LOAS0350
      *    TIPOLOGIA...... GESTIONE PARAMETRO COMPAGNIA
      *    PROCESSO....... OPERAZIONI AUTOMATICA
      *    FUNZIONE....... TRASVERSALI
      *    DESCRIZIONE.... AGGIORNAMENTO DATA ELABORAZIONE
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *   COSTANTI
      *----------------------------------------------------------------*
       01 WK-PGM                            PIC X(008) VALUE 'LOAS0350'.

      *----------------------------------------------------------------*
      *    VARIABILI DI WORKING
      *----------------------------------------------------------------*
       01 WK-VARIABILI.
          03 WK-TABELLA                     PIC X(20) VALUE SPACES.

       01 WS-FORMA1                         PIC X(02).
       01 WS-FORMA2                         PIC X(02).

       01 IX-TAB-PCO                        PIC S9(04) COMP VALUE 0.

      *--> GESTIONE FLAG PER OVERFLOW
       01  WCOM-FLAG-OVERFLOW               PIC X(002).
           88 WCOM-OVERFLOW-YES              VALUE 'SI'.
           88 WCOM-OVERFLOW-NO               VALUE 'NO'.
      * ----------------------------------------------------------------
      *    AREE TABELLE
      * ----------------------------------------------------------------
       01  AREA-TABELLE.
           03 WPCO-AREA-PARAM-COMP.
              04 WPCO-ELE-PAR-COMP-MAX   PIC 9(04) COMP.
              04 WPCO-TAB-PCO.
                 COPY LCCVPCO1           REPLACING ==(SF)== BY ==WPCO==.
      * ----------------------------------------------------------------
      *    AREE MODULI CHIAMATI
      * ----------------------------------------------------------------
      * ---- AREA PER ESTRAZIONE SEQUENCE

       01 AREA-IO-LCCS0090.
          COPY LCCC0090                  REPLACING ==(SF)== BY ==S090==.

       01 LCCS0090                       PIC X(008) VALUE 'LCCS0090'.

      *----------------------------------------------------------------*
      *    COPY VARIBILI GESTIONE TIPOLOGICHE                          *
      *----------------------------------------------------------------*
           COPY LCCC0006.
11257 *    COPY LCCVXMV0.
11257      COPY LCCVXMVZ.
11257      COPY LCCVXMV6.
10819      COPY LCCVXMV2.
      *----------------------------------------------------------------*
      *    COPY DELLE TABELLE DB2
      *----------------------------------------------------------------*
           COPY IDBVPCO1.
      *----------------------------------------------------------------*
      *    COPY DISPATCHER
      *----------------------------------------------------------------*
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
           COPY IDSI0011.
           COPY IDSO0011.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.

      *---------------------------------------------------------------*
       LINKAGE SECTION.
      *---------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.

       01 AREA-LOAS0350.
          COPY LOAV0351    REPLACING ==(SF)== BY ==S350==.


      *---------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                AREA-LOAS0350.
      *---------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *    OPERAZIONI INIZIALI                                         *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE WK-VARIABILI.

           PERFORM INIZIA-TOT-PCO
              THRU INIZIA-TOT-PCO-EX.


           EVALUATE S350-FORMA-ASS
             WHEN 'IN'
                MOVE 'IN'           TO WS-FORMA1
                MOVE 'IN'           TO WS-FORMA2
             WHEN 'CO'
                MOVE 'CO'           TO WS-FORMA1
                MOVE 'CO'           TO WS-FORMA2
             WHEN OTHER
                MOVE 'IN'           TO WS-FORMA1
                MOVE 'CO'           TO WS-FORMA2
           END-EVALUATE.

           MOVE IDSV0001-TIPO-MOVIMENTO   TO WS-MOVIMENTO.

       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           PERFORM S2100-LEGGI-PAR-COMP
              THRU EX-S2100

           IF IDSV0001-ESITO-OK

              EVALUATE TRUE
      *-->      RINNOVO TARIFFE ACCESSORIE 6001
                WHEN RITAR-ACCES
                   PERFORM S1100-GEST-6001
                      THRU EX-S1100
      *-->      GENERAZIONE RISCATTI PARZIALI PROGRAMMATI
                WHEN GENER-RISPAR
                   PERFORM S1200-GEST-6005
                     THRU  EX-S1200
      *-->      ADEGUAMENTO PREMIO PRESTAZIONE 6006
                WHEN ADPRE-PRESTA
                   PERFORM S1300-GEST-6006
                      THRU EX-S1300
      *-->      GENERAZIONE CEDOLA 6007
                WHEN GENER-CEDOLA
                   PERFORM S1400-GEST-6007
                      THRU EX-S1400
      *-->      RINNOVO CONTRATTO COLLETTIVO 6008
                WHEN RICON-COLLET
                   PERFORM S1500-GEST-6008
                      THRU EX-S1500
      *-->      SCADENZA - DIFFERIMENTO - PROROGA 6009
                WHEN VARIA-OPZION
                   PERFORM S1600-GEST-6009
                      THRU EX-S1600
      *-->      CALCOLO IMPOSTA SOSTITUTIVA 6010
                WHEN CALC-IMPOSTA-SOSTIT
                   PERFORM S1700-GEST-6010
                      THRU EX-S1700
      *-->      CALCOLO BONUS FEDELTA' 6013
                WHEN BONUS-FEDE
      *-->      CALCOLO BONUS RICORRENTE 6016
                WHEN BONUS-RICOR
                   PERFORM S1800-GEST-BONUS
                      THRU EX-S1800
      *-->      SIGNIFICATIVITA DEL RISCHIO 6014
                WHEN SIGNI-RISCH
                   PERFORM S1900-GEST-6014
                      THRU EX-S1900
      *-->      ATTUAZIONE STRATEGIA DI INVESTIMENTO 6015
                WHEN ATTUA-STINV
                   PERFORM S2000-GEST-6015
                      THRU EX-S2000
      *-->      PRELIEVO COSTI GARANZIA ACCESSORIA 6017
                WHEN DETERM-BONUS
                   PERFORM S2200-GEST-6017
                      THRU EX-S2200
      *-->      QUIETANZAMENTO 6101 6102
                WHEN MOVIM-QUIETA
                WHEN MOVIM-QUIETA-INT-PREST
                   PERFORM S2300-GEST-6101
                      THRU EX-S2300
      *-->      GENERAZIONE TRANCHE STANDARD 6002
                WHEN GENER-TRANCH
                   PERFORM S2400-GEST-6002
                      THRU EX-S2400
11257 *-->      RINNOVO TACITO 6050
11257           WHEN RINN-TAC-KM2009
11257              PERFORM S2500-GEST-6050
11257                 THRU EX-S2500
10819 *-->      PASSO DOPO PASSO  2321
10819           WHEN SW-PASSO-PASSO
10819              PERFORM S2600-GEST-2321
10819                 THRU EX-S2600
10819 *-->      PASSO REDDITO PROGRAMMATO 2316
10819           WHEN RPP-REDDITO-PROGRAMMATO
10819              PERFORM S2700-GEST-2316
10819                 THRU EX-S2700
10819 *-->      PASSO REDDITO PROGRAMMATO 2318
10819           WHEN RPP-TAKE-PROFIT
10819              PERFORM S2800-GEST-2318
10819                 THRU EX-S2800

15784 *-->      ESTRAZIONE-DECESSI-COMUNICATI 6063
15784           WHEN ESTRAZIONE-DECESSI-COMUNICATI
15784              PERFORM S2900-GEST-6063
15784                 THRU EX-S2900
15410 *-->      ESTRAZIONE-CALCOLO-COSTI 6064
15410           WHEN ESTRAZIONE-CALCOLO-COSTI
15410              PERFORM S3000-GEST-6064
15410                 THRU EX-S3000
15410           WHEN OTHER
15410              CONTINUE
15410      END-EVALUATE.

           IF IDSV0001-ESITO-OK
              PERFORM AGGIORNA-PARAM-COMP
                 THRU AGGIORNA-PARAM-COMP-EX
           END-IF.

       EX-S1000.
           EXIT.
      *----------------------------------------------------------------
      *     LETTURA PARAMETRO COMPAGNIA
      *----------------------------------------------------------------
       S2100-LEGGI-PAR-COMP.

           MOVE HIGH-VALUE               TO PARAM-COMP.

           MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

           INITIALIZE IDSI0011-BUFFER-DATI.

           MOVE 'PARAM-COMP'                TO WK-TABELLA.
           SET  IDSV0001-ESITO-OK           TO TRUE.
           SET  IDSI0011-SELECT             TO TRUE.
           SET  IDSI0011-PRIMARY-KEY        TO TRUE.
           SET  IDSI0011-TRATT-SENZA-STOR   TO TRUE.

           MOVE WK-TABELLA                  TO IDSI0011-CODICE-STR-DATO.
           MOVE IDSV0001-COD-COMPAGNIA-ANIA TO PCO-COD-COMP-ANIA.
           MOVE PARAM-COMP                  TO IDSI0011-BUFFER-DATI.

           PERFORM LETTURA-PCO              THRU LETTURA-PCO-EX.

       EX-S2100.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE MOVIMENTO 6001
      *----------------------------------------------------------------*
       S1100-GEST-6001.

           IF WS-FORMA1 = 'IN'
              MOVE S350-DT-ULT-ELABORAZIONE  TO WPCO-DT-ULT-RINN-GARAC
              SET  WPCO-ST-MOD               TO TRUE
           END-IF.

       EX-S1100.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE MOVIMENTO 6005
      *----------------------------------------------------------------*
       S1200-GEST-6005.

           IF WS-FORMA1 = 'IN'
              MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTELRISCPAR-PR
              SET  WPCO-ST-MOD              TO TRUE
           END-IF.

       EX-S1200.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE MOVIMENTO 6006
      *----------------------------------------------------------------*
       S1300-GEST-6006.

           IF WS-FORMA1 = 'IN'
              MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULT-RIVAL-IN
              SET  WPCO-ST-MOD              TO TRUE
           END-IF.

           IF WS-FORMA2 = 'CO'
             MOVE S350-DT-ULT-ELABORAZIONE  TO WPCO-DT-ULT-RIVAL-CL
             SET  WPCO-ST-MOD               TO TRUE
           END-IF.

       EX-S1300.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE MOVIMENTO 6007
      *----------------------------------------------------------------*
       S1400-GEST-6007.

           IF WS-FORMA1 = 'IN'
              MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTGZ-CED
              SET  WPCO-ST-MOD              TO TRUE
           END-IF.

           IF WS-FORMA2 = 'CO'
              MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTGZ-CED-COLL
              SET  WPCO-ST-MOD              TO TRUE
           END-IF.


       EX-S1400.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE MOVIMENTO 6008
      *----------------------------------------------------------------*
       S1500-GEST-6008.

           IF WS-FORMA2 = 'CO'
               MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULT-RINN-COLL
               SET  WPCO-ST-MOD              TO TRUE
           END-IF.

       EX-S1500.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE MOVIMENTO 6009
      *----------------------------------------------------------------*
       S1600-GEST-6009.

           EVALUATE S350-FL-TP-OPZ-SCAD

              WHEN 'N'
                IF WS-FORMA1 = 'IN'
                   MOVE S350-DT-ULT-ELABORAZIONE
                     TO WPCO-DT-ULTSC-ELAB-IN
                   SET  WPCO-ST-MOD     TO TRUE
                END-IF
                IF WS-FORMA2 = 'CO'
                   MOVE S350-DT-ULT-ELABORAZIONE
                     TO WPCO-DT-ULTSC-ELAB-CL
                   SET  WPCO-ST-MOD     TO TRUE
                END-IF

              WHEN 'S'
                IF WS-FORMA1 = 'IN'
                  MOVE S350-DT-ULT-ELABORAZIONE
                    TO WPCO-DT-ULTSC-OPZ-IN
                  SET  WPCO-ST-MOD     TO TRUE
                END-IF
                IF WS-FORMA2 = 'CO'
                  MOVE S350-DT-ULT-ELABORAZIONE
                    TO WPCO-DT-ULTSC-OPZ-CL
                  SET  WPCO-ST-MOD     TO TRUE
                END-IF

              WHEN OTHER
                CONTINUE

           END-EVALUATE.

       EX-S1600.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE MOVIMENTO 6010
      *----------------------------------------------------------------*
       S1700-GEST-6010.

           IF WS-FORMA1 = 'IN'
              MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTC-IS-IN
              SET  WPCO-ST-MOD              TO TRUE
           END-IF.

           IF WS-FORMA2 = 'CO'
              MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTC-IS-CL
              SET  WPCO-ST-MOD              TO TRUE
           END-IF.

       EX-S1700.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE MOVIMENTO 6013
      *----------------------------------------------------------------*
       S1800-GEST-BONUS.

           IF S350-FL-BONUS-FEDELTA  = 'S'
              IF WS-FORMA1 = 'IN'
               MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTC-BNSFDT-IN
               SET  WPCO-ST-MOD              TO TRUE
              END-IF
              IF WS-FORMA2 = 'CO'
               MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTC-BNSFDT-CL
               SET  WPCO-ST-MOD              TO TRUE
              END-IF
           END-IF.
           IF S350-FL-BONUS-RICORRENTE  = 'S'
              IF WS-FORMA1 = 'IN'
               MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTC-BNSRIC-IN
               SET  WPCO-ST-MOD              TO TRUE
              END-IF
              IF WS-FORMA2 = 'CO'
               MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTC-BNSRIC-CL
               SET  WPCO-ST-MOD              TO TRUE
              END-IF
           END-IF.

       EX-S1800.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE MOVIMENTO 6014
      *----------------------------------------------------------------*
       S1900-GEST-6014.

           CONTINUE.

       EX-S1900.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE MOVIMENTO 6015
      *----------------------------------------------------------------*
       S2000-GEST-6015.

           CONTINUE.

       EX-S2000.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE MOVIMENTO 6017
      *----------------------------------------------------------------*
       S2200-GEST-6017.

           MOVE S350-DT-ULT-ELABORAZIONE
             TO WPCO-DT-ULT-ELAB-PRLCOS

           SET  WPCO-ST-MOD TO TRUE.

       EX-S2200.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE MOVIMENTO 6101
      *----------------------------------------------------------------*
       S2300-GEST-6101.

           IF WS-FORMA1 = 'IN'
              MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULT-QTZO-IN
              SET  WPCO-ST-MOD              TO TRUE
           END-IF.

           IF WS-FORMA2 = 'CO'
              MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULT-QTZO-CL
              SET  WPCO-ST-MOD              TO TRUE
           END-IF.

       EX-S2300.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE MOVIMENTO 6002
      *----------------------------------------------------------------*
       S2400-GEST-6002.

           IF WS-FORMA1 = 'IN'
              MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTGZ-TRCH-E-IN
              SET  WPCO-ST-MOD              TO TRUE
           END-IF.

           IF WS-FORMA2 = 'CO'
              MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTGZ-TRCH-E-CL
              SET  WPCO-ST-MOD              TO TRUE
           END-IF.

       EX-S2400.
           EXIT.
11257 *----------------------------------------------------------------*
11257 * GESTIONE MOVIMENTO 6050
11257 *----------------------------------------------------------------*
11257  S2500-GEST-6050.

11257      MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULT-RINN-TAC
11257      SET  WPCO-ST-MOD              TO TRUE.

11257  EX-S2500.
11257      EXIT.

10819 *----------------------------------------------------------------*
10819 * GESTIONE MOVIMENTO 2321
10819 *----------------------------------------------------------------*
10819  S2600-GEST-2321.

10819      MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULT-ELAB-PASPAS
10819      SET  WPCO-ST-MOD              TO TRUE.

10819  EX-S2600.
10819      EXIT.

10819 *----------------------------------------------------------------*
10819 * GESTIONE MOVIMENTO 2316
10819 *----------------------------------------------------------------*
10819  S2700-GEST-2316.

10819      MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULT-ELAB-REDPRO
10819      SET  WPCO-ST-MOD              TO TRUE.

10819  EX-S2700.
10819      EXIT.
10819 *----------------------------------------------------------------*
10819 * GESTIONE MOVIMENTO 2318
10819 *----------------------------------------------------------------*
10819  S2800-GEST-2318.

10819      MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULT-ELAB-TAKE-P.
10819      SET  WPCO-ST-MOD              TO TRUE.

10819  EX-S2800.
10819      EXIT.

15784 *----------------------------------------------------------------*
15784 * GESTIONE MOVIMENTO 6063
15784 *----------------------------------------------------------------*
15784  S2900-GEST-6063.

           MOVE S350-DT-ULT-ELABORAZIONE  TO WPCO-DT-ULT-ESTR-DEC-CO
           SET  WPCO-ST-MOD               TO TRUE.

15784  EX-S2900.
15784      EXIT.

15410 *----------------------------------------------------------------*
15410 * GESTIONE MOVIMENTO 6064
15410 *----------------------------------------------------------------*
       S3000-GEST-6064.
      *
           IF S350-TP-ELAB-COSTI = 'AT'
              MOVE S350-DT-ULT-ELABORAZIONE  TO WPCO-DT-ULT-ELAB-COS-AT
           ELSE
              MOVE S350-DT-ULT-ELABORAZIONE  TO WPCO-DT-ULT-ELAB-COS-ST
           END-IF.
      *
           SET  WPCO-ST-MOD               TO TRUE.
      *
15410  EX-S3000.
15410      EXIT.
      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

              GOBACK.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      *    COPY PROCEDURE (COMPONENTI COMUNI)
      *----------------------------------------------------------------*
           COPY LCCP0001              REPLACING ==(SF)== BY ==WPCO==.
           COPY LCCP0002              REPLACING ==(SF)== BY ==WPCO==.
      *----------------------------------------------------------------*
      *    COPY PROCEDURE LETTURA PORTAFOGLIO
      *----------------------------------------------------------------*
           COPY LCCVPCO2              REPLACING ==(SF)== BY ==WPCO==.
           COPY LCCVPCO3              REPLACING ==(SF)== BY ==WPCO==.
           COPY LCCVPCO4              REPLACING ==(SF)== BY ==WPCO==.
      *----------------------------------------------------------------*
      *    COPY PROCEDURE AGGIORNAMENTO PORTAFOGLIO
      *----------------------------------------------------------------*
           COPY LCCVPCO5              REPLACING ==(SF)== BY ==WPCO==.
           COPY LCCVPCO6              REPLACING ==(SF)== BY ==WPCO==.
      *----------------------------------------------------------------*
      * ROUTINES GESTIONE ERRORI                                       *
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.
      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.

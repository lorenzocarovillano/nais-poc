       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSPMO0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  17 FEB 2015.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.

NEWP   01 WK-ID-OGG               PIC 9(009)  VALUE ZEROES.

           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDPMO0 END-EXEC.
           EXEC SQL INCLUDE IDBVPMO2 END-EXEC.
           EXEC SQL INCLUDE IDBVPMO3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVPMO1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 PARAM-MOVI.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSPMO0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'PARAM_MOVI' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_PARAM_MOVI
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_MOVI
                ,FRQ_MOVI
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,DT_RICOR_PREC
                ,DT_RICOR_SUCC
                ,PC_INTR_FRAZ
                ,IMP_BNS_DA_SCO_TOT
                ,IMP_BNS_DA_SCO
                ,PC_ANTIC_BNS
                ,TP_RINN_COLL
                ,TP_RIVAL_PRE
                ,TP_RIVAL_PRSTZ
                ,FL_EVID_RIVAL
                ,ULT_PC_PERD
                ,TOT_AA_GIA_PROR
                ,TP_OPZ
                ,AA_REN_CER
                ,PC_REVRSB
                ,IMP_RISC_PARZ_PRGT
                ,IMP_LRD_DI_RAT
                ,IB_OGG
                ,COS_ONER
                ,SPE_PC
                ,FL_ATTIV_GAR
                ,CAMBIO_VER_PROD
                ,MM_DIFF
                ,IMP_RAT_MANFEE
                ,DT_ULT_EROG_MANFEE
                ,TP_OGG_RIVAL
                ,SOM_ASSTA_GARAC
                ,PC_APPLZ_OPZ
                ,ID_ADES
                ,ID_POLI
                ,TP_FRM_ASSVA
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TP_ESTR_CNT
                ,COD_RAMO
                ,GEN_DA_SIN
                ,COD_TARI
                ,NUM_RAT_PAG_PRE
                ,PC_SERV_VAL
                ,ETA_AA_SOGL_BNFICR
             INTO
                :PMO-ID-PARAM-MOVI
               ,:PMO-ID-OGG
               ,:PMO-TP-OGG
               ,:PMO-ID-MOVI-CRZ
               ,:PMO-ID-MOVI-CHIU
                :IND-PMO-ID-MOVI-CHIU
               ,:PMO-DT-INI-EFF-DB
               ,:PMO-DT-END-EFF-DB
               ,:PMO-COD-COMP-ANIA
               ,:PMO-TP-MOVI
                :IND-PMO-TP-MOVI
               ,:PMO-FRQ-MOVI
                :IND-PMO-FRQ-MOVI
               ,:PMO-DUR-AA
                :IND-PMO-DUR-AA
               ,:PMO-DUR-MM
                :IND-PMO-DUR-MM
               ,:PMO-DUR-GG
                :IND-PMO-DUR-GG
               ,:PMO-DT-RICOR-PREC-DB
                :IND-PMO-DT-RICOR-PREC
               ,:PMO-DT-RICOR-SUCC-DB
                :IND-PMO-DT-RICOR-SUCC
               ,:PMO-PC-INTR-FRAZ
                :IND-PMO-PC-INTR-FRAZ
               ,:PMO-IMP-BNS-DA-SCO-TOT
                :IND-PMO-IMP-BNS-DA-SCO-TOT
               ,:PMO-IMP-BNS-DA-SCO
                :IND-PMO-IMP-BNS-DA-SCO
               ,:PMO-PC-ANTIC-BNS
                :IND-PMO-PC-ANTIC-BNS
               ,:PMO-TP-RINN-COLL
                :IND-PMO-TP-RINN-COLL
               ,:PMO-TP-RIVAL-PRE
                :IND-PMO-TP-RIVAL-PRE
               ,:PMO-TP-RIVAL-PRSTZ
                :IND-PMO-TP-RIVAL-PRSTZ
               ,:PMO-FL-EVID-RIVAL
                :IND-PMO-FL-EVID-RIVAL
               ,:PMO-ULT-PC-PERD
                :IND-PMO-ULT-PC-PERD
               ,:PMO-TOT-AA-GIA-PROR
                :IND-PMO-TOT-AA-GIA-PROR
               ,:PMO-TP-OPZ
                :IND-PMO-TP-OPZ
               ,:PMO-AA-REN-CER
                :IND-PMO-AA-REN-CER
               ,:PMO-PC-REVRSB
                :IND-PMO-PC-REVRSB
               ,:PMO-IMP-RISC-PARZ-PRGT
                :IND-PMO-IMP-RISC-PARZ-PRGT
               ,:PMO-IMP-LRD-DI-RAT
                :IND-PMO-IMP-LRD-DI-RAT
               ,:PMO-IB-OGG
                :IND-PMO-IB-OGG
               ,:PMO-COS-ONER
                :IND-PMO-COS-ONER
               ,:PMO-SPE-PC
                :IND-PMO-SPE-PC
               ,:PMO-FL-ATTIV-GAR
                :IND-PMO-FL-ATTIV-GAR
               ,:PMO-CAMBIO-VER-PROD
                :IND-PMO-CAMBIO-VER-PROD
               ,:PMO-MM-DIFF
                :IND-PMO-MM-DIFF
               ,:PMO-IMP-RAT-MANFEE
                :IND-PMO-IMP-RAT-MANFEE
               ,:PMO-DT-ULT-EROG-MANFEE-DB
                :IND-PMO-DT-ULT-EROG-MANFEE
               ,:PMO-TP-OGG-RIVAL
                :IND-PMO-TP-OGG-RIVAL
               ,:PMO-SOM-ASSTA-GARAC
                :IND-PMO-SOM-ASSTA-GARAC
               ,:PMO-PC-APPLZ-OPZ
                :IND-PMO-PC-APPLZ-OPZ
               ,:PMO-ID-ADES
                :IND-PMO-ID-ADES
               ,:PMO-ID-POLI
               ,:PMO-TP-FRM-ASSVA
               ,:PMO-DS-RIGA
               ,:PMO-DS-OPER-SQL
               ,:PMO-DS-VER
               ,:PMO-DS-TS-INI-CPTZ
               ,:PMO-DS-TS-END-CPTZ
               ,:PMO-DS-UTENTE
               ,:PMO-DS-STATO-ELAB
               ,:PMO-TP-ESTR-CNT
                :IND-PMO-TP-ESTR-CNT
               ,:PMO-COD-RAMO
                :IND-PMO-COD-RAMO
               ,:PMO-GEN-DA-SIN
                :IND-PMO-GEN-DA-SIN
               ,:PMO-COD-TARI
                :IND-PMO-COD-TARI
               ,:PMO-NUM-RAT-PAG-PRE
                :IND-PMO-NUM-RAT-PAG-PRE
               ,:PMO-PC-SERV-VAL
                :IND-PMO-PC-SERV-VAL
               ,:PMO-ETA-AA-SOGL-BNFICR
                :IND-PMO-ETA-AA-SOGL-BNFICR
             FROM PARAM_MOVI
             WHERE     DS_RIGA = :PMO-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO PARAM_MOVI
                     (
                        ID_PARAM_MOVI
                       ,ID_OGG
                       ,TP_OGG
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,TP_MOVI
                       ,FRQ_MOVI
                       ,DUR_AA
                       ,DUR_MM
                       ,DUR_GG
                       ,DT_RICOR_PREC
                       ,DT_RICOR_SUCC
                       ,PC_INTR_FRAZ
                       ,IMP_BNS_DA_SCO_TOT
                       ,IMP_BNS_DA_SCO
                       ,PC_ANTIC_BNS
                       ,TP_RINN_COLL
                       ,TP_RIVAL_PRE
                       ,TP_RIVAL_PRSTZ
                       ,FL_EVID_RIVAL
                       ,ULT_PC_PERD
                       ,TOT_AA_GIA_PROR
                       ,TP_OPZ
                       ,AA_REN_CER
                       ,PC_REVRSB
                       ,IMP_RISC_PARZ_PRGT
                       ,IMP_LRD_DI_RAT
                       ,IB_OGG
                       ,COS_ONER
                       ,SPE_PC
                       ,FL_ATTIV_GAR
                       ,CAMBIO_VER_PROD
                       ,MM_DIFF
                       ,IMP_RAT_MANFEE
                       ,DT_ULT_EROG_MANFEE
                       ,TP_OGG_RIVAL
                       ,SOM_ASSTA_GARAC
                       ,PC_APPLZ_OPZ
                       ,ID_ADES
                       ,ID_POLI
                       ,TP_FRM_ASSVA
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,TP_ESTR_CNT
                       ,COD_RAMO
                       ,GEN_DA_SIN
                       ,COD_TARI
                       ,NUM_RAT_PAG_PRE
                       ,PC_SERV_VAL
                       ,ETA_AA_SOGL_BNFICR
                     )
                 VALUES
                     (
                       :PMO-ID-PARAM-MOVI
                       ,:PMO-ID-OGG
                       ,:PMO-TP-OGG
                       ,:PMO-ID-MOVI-CRZ
                       ,:PMO-ID-MOVI-CHIU
                        :IND-PMO-ID-MOVI-CHIU
                       ,:PMO-DT-INI-EFF-DB
                       ,:PMO-DT-END-EFF-DB
                       ,:PMO-COD-COMP-ANIA
                       ,:PMO-TP-MOVI
                        :IND-PMO-TP-MOVI
                       ,:PMO-FRQ-MOVI
                        :IND-PMO-FRQ-MOVI
                       ,:PMO-DUR-AA
                        :IND-PMO-DUR-AA
                       ,:PMO-DUR-MM
                        :IND-PMO-DUR-MM
                       ,:PMO-DUR-GG
                        :IND-PMO-DUR-GG
                       ,:PMO-DT-RICOR-PREC-DB
                        :IND-PMO-DT-RICOR-PREC
                       ,:PMO-DT-RICOR-SUCC-DB
                        :IND-PMO-DT-RICOR-SUCC
                       ,:PMO-PC-INTR-FRAZ
                        :IND-PMO-PC-INTR-FRAZ
                       ,:PMO-IMP-BNS-DA-SCO-TOT
                        :IND-PMO-IMP-BNS-DA-SCO-TOT
                       ,:PMO-IMP-BNS-DA-SCO
                        :IND-PMO-IMP-BNS-DA-SCO
                       ,:PMO-PC-ANTIC-BNS
                        :IND-PMO-PC-ANTIC-BNS
                       ,:PMO-TP-RINN-COLL
                        :IND-PMO-TP-RINN-COLL
                       ,:PMO-TP-RIVAL-PRE
                        :IND-PMO-TP-RIVAL-PRE
                       ,:PMO-TP-RIVAL-PRSTZ
                        :IND-PMO-TP-RIVAL-PRSTZ
                       ,:PMO-FL-EVID-RIVAL
                        :IND-PMO-FL-EVID-RIVAL
                       ,:PMO-ULT-PC-PERD
                        :IND-PMO-ULT-PC-PERD
                       ,:PMO-TOT-AA-GIA-PROR
                        :IND-PMO-TOT-AA-GIA-PROR
                       ,:PMO-TP-OPZ
                        :IND-PMO-TP-OPZ
                       ,:PMO-AA-REN-CER
                        :IND-PMO-AA-REN-CER
                       ,:PMO-PC-REVRSB
                        :IND-PMO-PC-REVRSB
                       ,:PMO-IMP-RISC-PARZ-PRGT
                        :IND-PMO-IMP-RISC-PARZ-PRGT
                       ,:PMO-IMP-LRD-DI-RAT
                        :IND-PMO-IMP-LRD-DI-RAT
                       ,:PMO-IB-OGG
                        :IND-PMO-IB-OGG
                       ,:PMO-COS-ONER
                        :IND-PMO-COS-ONER
                       ,:PMO-SPE-PC
                        :IND-PMO-SPE-PC
                       ,:PMO-FL-ATTIV-GAR
                        :IND-PMO-FL-ATTIV-GAR
                       ,:PMO-CAMBIO-VER-PROD
                        :IND-PMO-CAMBIO-VER-PROD
                       ,:PMO-MM-DIFF
                        :IND-PMO-MM-DIFF
                       ,:PMO-IMP-RAT-MANFEE
                        :IND-PMO-IMP-RAT-MANFEE
                       ,:PMO-DT-ULT-EROG-MANFEE-DB
                        :IND-PMO-DT-ULT-EROG-MANFEE
                       ,:PMO-TP-OGG-RIVAL
                        :IND-PMO-TP-OGG-RIVAL
                       ,:PMO-SOM-ASSTA-GARAC
                        :IND-PMO-SOM-ASSTA-GARAC
                       ,:PMO-PC-APPLZ-OPZ
                        :IND-PMO-PC-APPLZ-OPZ
                       ,:PMO-ID-ADES
                        :IND-PMO-ID-ADES
                       ,:PMO-ID-POLI
                       ,:PMO-TP-FRM-ASSVA
                       ,:PMO-DS-RIGA
                       ,:PMO-DS-OPER-SQL
                       ,:PMO-DS-VER
                       ,:PMO-DS-TS-INI-CPTZ
                       ,:PMO-DS-TS-END-CPTZ
                       ,:PMO-DS-UTENTE
                       ,:PMO-DS-STATO-ELAB
                       ,:PMO-TP-ESTR-CNT
                        :IND-PMO-TP-ESTR-CNT
                       ,:PMO-COD-RAMO
                        :IND-PMO-COD-RAMO
                       ,:PMO-GEN-DA-SIN
                        :IND-PMO-GEN-DA-SIN
                       ,:PMO-COD-TARI
                        :IND-PMO-COD-TARI
                       ,:PMO-NUM-RAT-PAG-PRE
                        :IND-PMO-NUM-RAT-PAG-PRE
                       ,:PMO-PC-SERV-VAL
                        :IND-PMO-PC-SERV-VAL
                       ,:PMO-ETA-AA-SOGL-BNFICR
                        :IND-PMO-ETA-AA-SOGL-BNFICR
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE PARAM_MOVI SET

                   ID_PARAM_MOVI          =
                :PMO-ID-PARAM-MOVI
                  ,ID_OGG                 =
                :PMO-ID-OGG
                  ,TP_OGG                 =
                :PMO-TP-OGG
                  ,ID_MOVI_CRZ            =
                :PMO-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :PMO-ID-MOVI-CHIU
                                       :IND-PMO-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :PMO-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :PMO-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :PMO-COD-COMP-ANIA
                  ,TP_MOVI                =
                :PMO-TP-MOVI
                                       :IND-PMO-TP-MOVI
                  ,FRQ_MOVI               =
                :PMO-FRQ-MOVI
                                       :IND-PMO-FRQ-MOVI
                  ,DUR_AA                 =
                :PMO-DUR-AA
                                       :IND-PMO-DUR-AA
                  ,DUR_MM                 =
                :PMO-DUR-MM
                                       :IND-PMO-DUR-MM
                  ,DUR_GG                 =
                :PMO-DUR-GG
                                       :IND-PMO-DUR-GG
                  ,DT_RICOR_PREC          =
           :PMO-DT-RICOR-PREC-DB
                                       :IND-PMO-DT-RICOR-PREC
                  ,DT_RICOR_SUCC          =
           :PMO-DT-RICOR-SUCC-DB
                                       :IND-PMO-DT-RICOR-SUCC
                  ,PC_INTR_FRAZ           =
                :PMO-PC-INTR-FRAZ
                                       :IND-PMO-PC-INTR-FRAZ
                  ,IMP_BNS_DA_SCO_TOT     =
                :PMO-IMP-BNS-DA-SCO-TOT
                                       :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,IMP_BNS_DA_SCO         =
                :PMO-IMP-BNS-DA-SCO
                                       :IND-PMO-IMP-BNS-DA-SCO
                  ,PC_ANTIC_BNS           =
                :PMO-PC-ANTIC-BNS
                                       :IND-PMO-PC-ANTIC-BNS
                  ,TP_RINN_COLL           =
                :PMO-TP-RINN-COLL
                                       :IND-PMO-TP-RINN-COLL
                  ,TP_RIVAL_PRE           =
                :PMO-TP-RIVAL-PRE
                                       :IND-PMO-TP-RIVAL-PRE
                  ,TP_RIVAL_PRSTZ         =
                :PMO-TP-RIVAL-PRSTZ
                                       :IND-PMO-TP-RIVAL-PRSTZ
                  ,FL_EVID_RIVAL          =
                :PMO-FL-EVID-RIVAL
                                       :IND-PMO-FL-EVID-RIVAL
                  ,ULT_PC_PERD            =
                :PMO-ULT-PC-PERD
                                       :IND-PMO-ULT-PC-PERD
                  ,TOT_AA_GIA_PROR        =
                :PMO-TOT-AA-GIA-PROR
                                       :IND-PMO-TOT-AA-GIA-PROR
                  ,TP_OPZ                 =
                :PMO-TP-OPZ
                                       :IND-PMO-TP-OPZ
                  ,AA_REN_CER             =
                :PMO-AA-REN-CER
                                       :IND-PMO-AA-REN-CER
                  ,PC_REVRSB              =
                :PMO-PC-REVRSB
                                       :IND-PMO-PC-REVRSB
                  ,IMP_RISC_PARZ_PRGT     =
                :PMO-IMP-RISC-PARZ-PRGT
                                       :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,IMP_LRD_DI_RAT         =
                :PMO-IMP-LRD-DI-RAT
                                       :IND-PMO-IMP-LRD-DI-RAT
                  ,IB_OGG                 =
                :PMO-IB-OGG
                                       :IND-PMO-IB-OGG
                  ,COS_ONER               =
                :PMO-COS-ONER
                                       :IND-PMO-COS-ONER
                  ,SPE_PC                 =
                :PMO-SPE-PC
                                       :IND-PMO-SPE-PC
                  ,FL_ATTIV_GAR           =
                :PMO-FL-ATTIV-GAR
                                       :IND-PMO-FL-ATTIV-GAR
                  ,CAMBIO_VER_PROD        =
                :PMO-CAMBIO-VER-PROD
                                       :IND-PMO-CAMBIO-VER-PROD
                  ,MM_DIFF                =
                :PMO-MM-DIFF
                                       :IND-PMO-MM-DIFF
                  ,IMP_RAT_MANFEE         =
                :PMO-IMP-RAT-MANFEE
                                       :IND-PMO-IMP-RAT-MANFEE
                  ,DT_ULT_EROG_MANFEE     =
           :PMO-DT-ULT-EROG-MANFEE-DB
                                       :IND-PMO-DT-ULT-EROG-MANFEE
                  ,TP_OGG_RIVAL           =
                :PMO-TP-OGG-RIVAL
                                       :IND-PMO-TP-OGG-RIVAL
                  ,SOM_ASSTA_GARAC        =
                :PMO-SOM-ASSTA-GARAC
                                       :IND-PMO-SOM-ASSTA-GARAC
                  ,PC_APPLZ_OPZ           =
                :PMO-PC-APPLZ-OPZ
                                       :IND-PMO-PC-APPLZ-OPZ
                  ,ID_ADES                =
                :PMO-ID-ADES
                                       :IND-PMO-ID-ADES
                  ,ID_POLI                =
                :PMO-ID-POLI
                  ,TP_FRM_ASSVA           =
                :PMO-TP-FRM-ASSVA
                  ,DS_RIGA                =
                :PMO-DS-RIGA
                  ,DS_OPER_SQL            =
                :PMO-DS-OPER-SQL
                  ,DS_VER                 =
                :PMO-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :PMO-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :PMO-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :PMO-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :PMO-DS-STATO-ELAB
                  ,TP_ESTR_CNT            =
                :PMO-TP-ESTR-CNT
                                       :IND-PMO-TP-ESTR-CNT
                  ,COD_RAMO               =
                :PMO-COD-RAMO
                                       :IND-PMO-COD-RAMO
                  ,GEN_DA_SIN             =
                :PMO-GEN-DA-SIN
                                       :IND-PMO-GEN-DA-SIN
                  ,COD_TARI               =
                :PMO-COD-TARI
                                       :IND-PMO-COD-TARI
                  ,NUM_RAT_PAG_PRE        =
                :PMO-NUM-RAT-PAG-PRE
                                       :IND-PMO-NUM-RAT-PAG-PRE
                  ,PC_SERV_VAL            =
                :PMO-PC-SERV-VAL
                                       :IND-PMO-PC-SERV-VAL
                  ,ETA_AA_SOGL_BNFICR     =
                :PMO-ETA-AA-SOGL-BNFICR
                                       :IND-PMO-ETA-AA-SOGL-BNFICR
                WHERE     DS_RIGA = :PMO-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM PARAM_MOVI
                WHERE     DS_RIGA = :PMO-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-PMO CURSOR FOR
              SELECT
                     ID_PARAM_MOVI
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_MOVI
                    ,FRQ_MOVI
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,DT_RICOR_PREC
                    ,DT_RICOR_SUCC
                    ,PC_INTR_FRAZ
                    ,IMP_BNS_DA_SCO_TOT
                    ,IMP_BNS_DA_SCO
                    ,PC_ANTIC_BNS
                    ,TP_RINN_COLL
                    ,TP_RIVAL_PRE
                    ,TP_RIVAL_PRSTZ
                    ,FL_EVID_RIVAL
                    ,ULT_PC_PERD
                    ,TOT_AA_GIA_PROR
                    ,TP_OPZ
                    ,AA_REN_CER
                    ,PC_REVRSB
                    ,IMP_RISC_PARZ_PRGT
                    ,IMP_LRD_DI_RAT
                    ,IB_OGG
                    ,COS_ONER
                    ,SPE_PC
                    ,FL_ATTIV_GAR
                    ,CAMBIO_VER_PROD
                    ,MM_DIFF
                    ,IMP_RAT_MANFEE
                    ,DT_ULT_EROG_MANFEE
                    ,TP_OGG_RIVAL
                    ,SOM_ASSTA_GARAC
                    ,PC_APPLZ_OPZ
                    ,ID_ADES
                    ,ID_POLI
                    ,TP_FRM_ASSVA
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,TP_ESTR_CNT
                    ,COD_RAMO
                    ,GEN_DA_SIN
                    ,COD_TARI
                    ,NUM_RAT_PAG_PRE
                    ,PC_SERV_VAL
                    ,ETA_AA_SOGL_BNFICR
              FROM PARAM_MOVI
              WHERE     ID_PARAM_MOVI = :PMO-ID-PARAM-MOVI
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY COD_COMP_ANIA, ID_PARAM_MOVI,
                        DS_TS_END_CPTZ, DT_INI_EFF
      *       ORDER BY DT_INI_EFF ASC

           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_PARAM_MOVI
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_MOVI
                ,FRQ_MOVI
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,DT_RICOR_PREC
                ,DT_RICOR_SUCC
                ,PC_INTR_FRAZ
                ,IMP_BNS_DA_SCO_TOT
                ,IMP_BNS_DA_SCO
                ,PC_ANTIC_BNS
                ,TP_RINN_COLL
                ,TP_RIVAL_PRE
                ,TP_RIVAL_PRSTZ
                ,FL_EVID_RIVAL
                ,ULT_PC_PERD
                ,TOT_AA_GIA_PROR
                ,TP_OPZ
                ,AA_REN_CER
                ,PC_REVRSB
                ,IMP_RISC_PARZ_PRGT
                ,IMP_LRD_DI_RAT
                ,IB_OGG
                ,COS_ONER
                ,SPE_PC
                ,FL_ATTIV_GAR
                ,CAMBIO_VER_PROD
                ,MM_DIFF
                ,IMP_RAT_MANFEE
                ,DT_ULT_EROG_MANFEE
                ,TP_OGG_RIVAL
                ,SOM_ASSTA_GARAC
                ,PC_APPLZ_OPZ
                ,ID_ADES
                ,ID_POLI
                ,TP_FRM_ASSVA
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TP_ESTR_CNT
                ,COD_RAMO
                ,GEN_DA_SIN
                ,COD_TARI
                ,NUM_RAT_PAG_PRE
                ,PC_SERV_VAL
                ,ETA_AA_SOGL_BNFICR
             INTO
                :PMO-ID-PARAM-MOVI
               ,:PMO-ID-OGG
               ,:PMO-TP-OGG
               ,:PMO-ID-MOVI-CRZ
               ,:PMO-ID-MOVI-CHIU
                :IND-PMO-ID-MOVI-CHIU
               ,:PMO-DT-INI-EFF-DB
               ,:PMO-DT-END-EFF-DB
               ,:PMO-COD-COMP-ANIA
               ,:PMO-TP-MOVI
                :IND-PMO-TP-MOVI
               ,:PMO-FRQ-MOVI
                :IND-PMO-FRQ-MOVI
               ,:PMO-DUR-AA
                :IND-PMO-DUR-AA
               ,:PMO-DUR-MM
                :IND-PMO-DUR-MM
               ,:PMO-DUR-GG
                :IND-PMO-DUR-GG
               ,:PMO-DT-RICOR-PREC-DB
                :IND-PMO-DT-RICOR-PREC
               ,:PMO-DT-RICOR-SUCC-DB
                :IND-PMO-DT-RICOR-SUCC
               ,:PMO-PC-INTR-FRAZ
                :IND-PMO-PC-INTR-FRAZ
               ,:PMO-IMP-BNS-DA-SCO-TOT
                :IND-PMO-IMP-BNS-DA-SCO-TOT
               ,:PMO-IMP-BNS-DA-SCO
                :IND-PMO-IMP-BNS-DA-SCO
               ,:PMO-PC-ANTIC-BNS
                :IND-PMO-PC-ANTIC-BNS
               ,:PMO-TP-RINN-COLL
                :IND-PMO-TP-RINN-COLL
               ,:PMO-TP-RIVAL-PRE
                :IND-PMO-TP-RIVAL-PRE
               ,:PMO-TP-RIVAL-PRSTZ
                :IND-PMO-TP-RIVAL-PRSTZ
               ,:PMO-FL-EVID-RIVAL
                :IND-PMO-FL-EVID-RIVAL
               ,:PMO-ULT-PC-PERD
                :IND-PMO-ULT-PC-PERD
               ,:PMO-TOT-AA-GIA-PROR
                :IND-PMO-TOT-AA-GIA-PROR
               ,:PMO-TP-OPZ
                :IND-PMO-TP-OPZ
               ,:PMO-AA-REN-CER
                :IND-PMO-AA-REN-CER
               ,:PMO-PC-REVRSB
                :IND-PMO-PC-REVRSB
               ,:PMO-IMP-RISC-PARZ-PRGT
                :IND-PMO-IMP-RISC-PARZ-PRGT
               ,:PMO-IMP-LRD-DI-RAT
                :IND-PMO-IMP-LRD-DI-RAT
               ,:PMO-IB-OGG
                :IND-PMO-IB-OGG
               ,:PMO-COS-ONER
                :IND-PMO-COS-ONER
               ,:PMO-SPE-PC
                :IND-PMO-SPE-PC
               ,:PMO-FL-ATTIV-GAR
                :IND-PMO-FL-ATTIV-GAR
               ,:PMO-CAMBIO-VER-PROD
                :IND-PMO-CAMBIO-VER-PROD
               ,:PMO-MM-DIFF
                :IND-PMO-MM-DIFF
               ,:PMO-IMP-RAT-MANFEE
                :IND-PMO-IMP-RAT-MANFEE
               ,:PMO-DT-ULT-EROG-MANFEE-DB
                :IND-PMO-DT-ULT-EROG-MANFEE
               ,:PMO-TP-OGG-RIVAL
                :IND-PMO-TP-OGG-RIVAL
               ,:PMO-SOM-ASSTA-GARAC
                :IND-PMO-SOM-ASSTA-GARAC
               ,:PMO-PC-APPLZ-OPZ
                :IND-PMO-PC-APPLZ-OPZ
               ,:PMO-ID-ADES
                :IND-PMO-ID-ADES
               ,:PMO-ID-POLI
               ,:PMO-TP-FRM-ASSVA
               ,:PMO-DS-RIGA
               ,:PMO-DS-OPER-SQL
               ,:PMO-DS-VER
               ,:PMO-DS-TS-INI-CPTZ
               ,:PMO-DS-TS-END-CPTZ
               ,:PMO-DS-UTENTE
               ,:PMO-DS-STATO-ELAB
               ,:PMO-TP-ESTR-CNT
                :IND-PMO-TP-ESTR-CNT
               ,:PMO-COD-RAMO
                :IND-PMO-COD-RAMO
               ,:PMO-GEN-DA-SIN
                :IND-PMO-GEN-DA-SIN
               ,:PMO-COD-TARI
                :IND-PMO-COD-TARI
               ,:PMO-NUM-RAT-PAG-PRE
                :IND-PMO-NUM-RAT-PAG-PRE
               ,:PMO-PC-SERV-VAL
                :IND-PMO-PC-SERV-VAL
               ,:PMO-ETA-AA-SOGL-BNFICR
                :IND-PMO-ETA-AA-SOGL-BNFICR
             FROM PARAM_MOVI
             WHERE     ID_PARAM_MOVI = :PMO-ID-PARAM-MOVI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE PARAM_MOVI SET

                   ID_PARAM_MOVI          =
                :PMO-ID-PARAM-MOVI
                  ,ID_OGG                 =
                :PMO-ID-OGG
                  ,TP_OGG                 =
                :PMO-TP-OGG
                  ,ID_MOVI_CRZ            =
                :PMO-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :PMO-ID-MOVI-CHIU
                                       :IND-PMO-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :PMO-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :PMO-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :PMO-COD-COMP-ANIA
                  ,TP_MOVI                =
                :PMO-TP-MOVI
                                       :IND-PMO-TP-MOVI
                  ,FRQ_MOVI               =
                :PMO-FRQ-MOVI
                                       :IND-PMO-FRQ-MOVI
                  ,DUR_AA                 =
                :PMO-DUR-AA
                                       :IND-PMO-DUR-AA
                  ,DUR_MM                 =
                :PMO-DUR-MM
                                       :IND-PMO-DUR-MM
                  ,DUR_GG                 =
                :PMO-DUR-GG
                                       :IND-PMO-DUR-GG
                  ,DT_RICOR_PREC          =
           :PMO-DT-RICOR-PREC-DB
                                       :IND-PMO-DT-RICOR-PREC
                  ,DT_RICOR_SUCC          =
           :PMO-DT-RICOR-SUCC-DB
                                       :IND-PMO-DT-RICOR-SUCC
                  ,PC_INTR_FRAZ           =
                :PMO-PC-INTR-FRAZ
                                       :IND-PMO-PC-INTR-FRAZ
                  ,IMP_BNS_DA_SCO_TOT     =
                :PMO-IMP-BNS-DA-SCO-TOT
                                       :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,IMP_BNS_DA_SCO         =
                :PMO-IMP-BNS-DA-SCO
                                       :IND-PMO-IMP-BNS-DA-SCO
                  ,PC_ANTIC_BNS           =
                :PMO-PC-ANTIC-BNS
                                       :IND-PMO-PC-ANTIC-BNS
                  ,TP_RINN_COLL           =
                :PMO-TP-RINN-COLL
                                       :IND-PMO-TP-RINN-COLL
                  ,TP_RIVAL_PRE           =
                :PMO-TP-RIVAL-PRE
                                       :IND-PMO-TP-RIVAL-PRE
                  ,TP_RIVAL_PRSTZ         =
                :PMO-TP-RIVAL-PRSTZ
                                       :IND-PMO-TP-RIVAL-PRSTZ
                  ,FL_EVID_RIVAL          =
                :PMO-FL-EVID-RIVAL
                                       :IND-PMO-FL-EVID-RIVAL
                  ,ULT_PC_PERD            =
                :PMO-ULT-PC-PERD
                                       :IND-PMO-ULT-PC-PERD
                  ,TOT_AA_GIA_PROR        =
                :PMO-TOT-AA-GIA-PROR
                                       :IND-PMO-TOT-AA-GIA-PROR
                  ,TP_OPZ                 =
                :PMO-TP-OPZ
                                       :IND-PMO-TP-OPZ
                  ,AA_REN_CER             =
                :PMO-AA-REN-CER
                                       :IND-PMO-AA-REN-CER
                  ,PC_REVRSB              =
                :PMO-PC-REVRSB
                                       :IND-PMO-PC-REVRSB
                  ,IMP_RISC_PARZ_PRGT     =
                :PMO-IMP-RISC-PARZ-PRGT
                                       :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,IMP_LRD_DI_RAT         =
                :PMO-IMP-LRD-DI-RAT
                                       :IND-PMO-IMP-LRD-DI-RAT
                  ,IB_OGG                 =
                :PMO-IB-OGG
                                       :IND-PMO-IB-OGG
                  ,COS_ONER               =
                :PMO-COS-ONER
                                       :IND-PMO-COS-ONER
                  ,SPE_PC                 =
                :PMO-SPE-PC
                                       :IND-PMO-SPE-PC
                  ,FL_ATTIV_GAR           =
                :PMO-FL-ATTIV-GAR
                                       :IND-PMO-FL-ATTIV-GAR
                  ,CAMBIO_VER_PROD        =
                :PMO-CAMBIO-VER-PROD
                                       :IND-PMO-CAMBIO-VER-PROD
                  ,MM_DIFF                =
                :PMO-MM-DIFF
                                       :IND-PMO-MM-DIFF
                  ,IMP_RAT_MANFEE         =
                :PMO-IMP-RAT-MANFEE
                                       :IND-PMO-IMP-RAT-MANFEE
                  ,DT_ULT_EROG_MANFEE     =
           :PMO-DT-ULT-EROG-MANFEE-DB
                                       :IND-PMO-DT-ULT-EROG-MANFEE
                  ,TP_OGG_RIVAL           =
                :PMO-TP-OGG-RIVAL
                                       :IND-PMO-TP-OGG-RIVAL
                  ,SOM_ASSTA_GARAC        =
                :PMO-SOM-ASSTA-GARAC
                                       :IND-PMO-SOM-ASSTA-GARAC
                  ,PC_APPLZ_OPZ           =
                :PMO-PC-APPLZ-OPZ
                                       :IND-PMO-PC-APPLZ-OPZ
                  ,ID_ADES                =
                :PMO-ID-ADES
                                       :IND-PMO-ID-ADES
                  ,ID_POLI                =
                :PMO-ID-POLI
                  ,TP_FRM_ASSVA           =
                :PMO-TP-FRM-ASSVA
                  ,DS_RIGA                =
                :PMO-DS-RIGA
                  ,DS_OPER_SQL            =
                :PMO-DS-OPER-SQL
                  ,DS_VER                 =
                :PMO-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :PMO-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :PMO-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :PMO-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :PMO-DS-STATO-ELAB
                  ,TP_ESTR_CNT            =
                :PMO-TP-ESTR-CNT
                                       :IND-PMO-TP-ESTR-CNT
                  ,COD_RAMO               =
                :PMO-COD-RAMO
                                       :IND-PMO-COD-RAMO
                  ,GEN_DA_SIN             =
                :PMO-GEN-DA-SIN
                                       :IND-PMO-GEN-DA-SIN
                  ,COD_TARI               =
                :PMO-COD-TARI
                                       :IND-PMO-COD-TARI
                  ,NUM_RAT_PAG_PRE        =
                :PMO-NUM-RAT-PAG-PRE
                                       :IND-PMO-NUM-RAT-PAG-PRE
                  ,PC_SERV_VAL            =
                :PMO-PC-SERV-VAL
                                       :IND-PMO-PC-SERV-VAL
                  ,ETA_AA_SOGL_BNFICR     =
                :PMO-ETA-AA-SOGL-BNFICR
                                       :IND-PMO-ETA-AA-SOGL-BNFICR
                WHERE     DS_RIGA = :PMO-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-PMO
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-PMO
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-PMO
           INTO
                :PMO-ID-PARAM-MOVI
               ,:PMO-ID-OGG
               ,:PMO-TP-OGG
               ,:PMO-ID-MOVI-CRZ
               ,:PMO-ID-MOVI-CHIU
                :IND-PMO-ID-MOVI-CHIU
               ,:PMO-DT-INI-EFF-DB
               ,:PMO-DT-END-EFF-DB
               ,:PMO-COD-COMP-ANIA
               ,:PMO-TP-MOVI
                :IND-PMO-TP-MOVI
               ,:PMO-FRQ-MOVI
                :IND-PMO-FRQ-MOVI
               ,:PMO-DUR-AA
                :IND-PMO-DUR-AA
               ,:PMO-DUR-MM
                :IND-PMO-DUR-MM
               ,:PMO-DUR-GG
                :IND-PMO-DUR-GG
               ,:PMO-DT-RICOR-PREC-DB
                :IND-PMO-DT-RICOR-PREC
               ,:PMO-DT-RICOR-SUCC-DB
                :IND-PMO-DT-RICOR-SUCC
               ,:PMO-PC-INTR-FRAZ
                :IND-PMO-PC-INTR-FRAZ
               ,:PMO-IMP-BNS-DA-SCO-TOT
                :IND-PMO-IMP-BNS-DA-SCO-TOT
               ,:PMO-IMP-BNS-DA-SCO
                :IND-PMO-IMP-BNS-DA-SCO
               ,:PMO-PC-ANTIC-BNS
                :IND-PMO-PC-ANTIC-BNS
               ,:PMO-TP-RINN-COLL
                :IND-PMO-TP-RINN-COLL
               ,:PMO-TP-RIVAL-PRE
                :IND-PMO-TP-RIVAL-PRE
               ,:PMO-TP-RIVAL-PRSTZ
                :IND-PMO-TP-RIVAL-PRSTZ
               ,:PMO-FL-EVID-RIVAL
                :IND-PMO-FL-EVID-RIVAL
               ,:PMO-ULT-PC-PERD
                :IND-PMO-ULT-PC-PERD
               ,:PMO-TOT-AA-GIA-PROR
                :IND-PMO-TOT-AA-GIA-PROR
               ,:PMO-TP-OPZ
                :IND-PMO-TP-OPZ
               ,:PMO-AA-REN-CER
                :IND-PMO-AA-REN-CER
               ,:PMO-PC-REVRSB
                :IND-PMO-PC-REVRSB
               ,:PMO-IMP-RISC-PARZ-PRGT
                :IND-PMO-IMP-RISC-PARZ-PRGT
               ,:PMO-IMP-LRD-DI-RAT
                :IND-PMO-IMP-LRD-DI-RAT
               ,:PMO-IB-OGG
                :IND-PMO-IB-OGG
               ,:PMO-COS-ONER
                :IND-PMO-COS-ONER
               ,:PMO-SPE-PC
                :IND-PMO-SPE-PC
               ,:PMO-FL-ATTIV-GAR
                :IND-PMO-FL-ATTIV-GAR
               ,:PMO-CAMBIO-VER-PROD
                :IND-PMO-CAMBIO-VER-PROD
               ,:PMO-MM-DIFF
                :IND-PMO-MM-DIFF
               ,:PMO-IMP-RAT-MANFEE
                :IND-PMO-IMP-RAT-MANFEE
               ,:PMO-DT-ULT-EROG-MANFEE-DB
                :IND-PMO-DT-ULT-EROG-MANFEE
               ,:PMO-TP-OGG-RIVAL
                :IND-PMO-TP-OGG-RIVAL
               ,:PMO-SOM-ASSTA-GARAC
                :IND-PMO-SOM-ASSTA-GARAC
               ,:PMO-PC-APPLZ-OPZ
                :IND-PMO-PC-APPLZ-OPZ
               ,:PMO-ID-ADES
                :IND-PMO-ID-ADES
               ,:PMO-ID-POLI
               ,:PMO-TP-FRM-ASSVA
               ,:PMO-DS-RIGA
               ,:PMO-DS-OPER-SQL
               ,:PMO-DS-VER
               ,:PMO-DS-TS-INI-CPTZ
               ,:PMO-DS-TS-END-CPTZ
               ,:PMO-DS-UTENTE
               ,:PMO-DS-STATO-ELAB
               ,:PMO-TP-ESTR-CNT
                :IND-PMO-TP-ESTR-CNT
               ,:PMO-COD-RAMO
                :IND-PMO-COD-RAMO
               ,:PMO-GEN-DA-SIN
                :IND-PMO-GEN-DA-SIN
               ,:PMO-COD-TARI
                :IND-PMO-COD-TARI
               ,:PMO-NUM-RAT-PAG-PRE
                :IND-PMO-NUM-RAT-PAG-PRE
               ,:PMO-PC-SERV-VAL
                :IND-PMO-PC-SERV-VAL
               ,:PMO-ETA-AA-SOGL-BNFICR
                :IND-PMO-ETA-AA-SOGL-BNFICR
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IBO-EFF-PMO CURSOR FOR
              SELECT
                     ID_PARAM_MOVI
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_MOVI
                    ,FRQ_MOVI
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,DT_RICOR_PREC
                    ,DT_RICOR_SUCC
                    ,PC_INTR_FRAZ
                    ,IMP_BNS_DA_SCO_TOT
                    ,IMP_BNS_DA_SCO
                    ,PC_ANTIC_BNS
                    ,TP_RINN_COLL
                    ,TP_RIVAL_PRE
                    ,TP_RIVAL_PRSTZ
                    ,FL_EVID_RIVAL
                    ,ULT_PC_PERD
                    ,TOT_AA_GIA_PROR
                    ,TP_OPZ
                    ,AA_REN_CER
                    ,PC_REVRSB
                    ,IMP_RISC_PARZ_PRGT
                    ,IMP_LRD_DI_RAT
                    ,IB_OGG
                    ,COS_ONER
                    ,SPE_PC
                    ,FL_ATTIV_GAR
                    ,CAMBIO_VER_PROD
                    ,MM_DIFF
                    ,IMP_RAT_MANFEE
                    ,DT_ULT_EROG_MANFEE
                    ,TP_OGG_RIVAL
                    ,SOM_ASSTA_GARAC
                    ,PC_APPLZ_OPZ
                    ,ID_ADES
                    ,ID_POLI
                    ,TP_FRM_ASSVA
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,TP_ESTR_CNT
                    ,COD_RAMO
                    ,GEN_DA_SIN
                    ,COD_TARI
                    ,NUM_RAT_PAG_PRE
                    ,PC_SERV_VAL
                    ,ETA_AA_SOGL_BNFICR
              FROM PARAM_MOVI
              WHERE     IB_OGG = :PMO-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_PARAM_MOVI ASC

           END-EXEC.

       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_PARAM_MOVI
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_MOVI
                ,FRQ_MOVI
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,DT_RICOR_PREC
                ,DT_RICOR_SUCC
                ,PC_INTR_FRAZ
                ,IMP_BNS_DA_SCO_TOT
                ,IMP_BNS_DA_SCO
                ,PC_ANTIC_BNS
                ,TP_RINN_COLL
                ,TP_RIVAL_PRE
                ,TP_RIVAL_PRSTZ
                ,FL_EVID_RIVAL
                ,ULT_PC_PERD
                ,TOT_AA_GIA_PROR
                ,TP_OPZ
                ,AA_REN_CER
                ,PC_REVRSB
                ,IMP_RISC_PARZ_PRGT
                ,IMP_LRD_DI_RAT
                ,IB_OGG
                ,COS_ONER
                ,SPE_PC
                ,FL_ATTIV_GAR
                ,CAMBIO_VER_PROD
                ,MM_DIFF
                ,IMP_RAT_MANFEE
                ,DT_ULT_EROG_MANFEE
                ,TP_OGG_RIVAL
                ,SOM_ASSTA_GARAC
                ,PC_APPLZ_OPZ
                ,ID_ADES
                ,ID_POLI
                ,TP_FRM_ASSVA
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TP_ESTR_CNT
                ,COD_RAMO
                ,GEN_DA_SIN
                ,COD_TARI
                ,NUM_RAT_PAG_PRE
                ,PC_SERV_VAL
                ,ETA_AA_SOGL_BNFICR
             INTO
                :PMO-ID-PARAM-MOVI
               ,:PMO-ID-OGG
               ,:PMO-TP-OGG
               ,:PMO-ID-MOVI-CRZ
               ,:PMO-ID-MOVI-CHIU
                :IND-PMO-ID-MOVI-CHIU
               ,:PMO-DT-INI-EFF-DB
               ,:PMO-DT-END-EFF-DB
               ,:PMO-COD-COMP-ANIA
               ,:PMO-TP-MOVI
                :IND-PMO-TP-MOVI
               ,:PMO-FRQ-MOVI
                :IND-PMO-FRQ-MOVI
               ,:PMO-DUR-AA
                :IND-PMO-DUR-AA
               ,:PMO-DUR-MM
                :IND-PMO-DUR-MM
               ,:PMO-DUR-GG
                :IND-PMO-DUR-GG
               ,:PMO-DT-RICOR-PREC-DB
                :IND-PMO-DT-RICOR-PREC
               ,:PMO-DT-RICOR-SUCC-DB
                :IND-PMO-DT-RICOR-SUCC
               ,:PMO-PC-INTR-FRAZ
                :IND-PMO-PC-INTR-FRAZ
               ,:PMO-IMP-BNS-DA-SCO-TOT
                :IND-PMO-IMP-BNS-DA-SCO-TOT
               ,:PMO-IMP-BNS-DA-SCO
                :IND-PMO-IMP-BNS-DA-SCO
               ,:PMO-PC-ANTIC-BNS
                :IND-PMO-PC-ANTIC-BNS
               ,:PMO-TP-RINN-COLL
                :IND-PMO-TP-RINN-COLL
               ,:PMO-TP-RIVAL-PRE
                :IND-PMO-TP-RIVAL-PRE
               ,:PMO-TP-RIVAL-PRSTZ
                :IND-PMO-TP-RIVAL-PRSTZ
               ,:PMO-FL-EVID-RIVAL
                :IND-PMO-FL-EVID-RIVAL
               ,:PMO-ULT-PC-PERD
                :IND-PMO-ULT-PC-PERD
               ,:PMO-TOT-AA-GIA-PROR
                :IND-PMO-TOT-AA-GIA-PROR
               ,:PMO-TP-OPZ
                :IND-PMO-TP-OPZ
               ,:PMO-AA-REN-CER
                :IND-PMO-AA-REN-CER
               ,:PMO-PC-REVRSB
                :IND-PMO-PC-REVRSB
               ,:PMO-IMP-RISC-PARZ-PRGT
                :IND-PMO-IMP-RISC-PARZ-PRGT
               ,:PMO-IMP-LRD-DI-RAT
                :IND-PMO-IMP-LRD-DI-RAT
               ,:PMO-IB-OGG
                :IND-PMO-IB-OGG
               ,:PMO-COS-ONER
                :IND-PMO-COS-ONER
               ,:PMO-SPE-PC
                :IND-PMO-SPE-PC
               ,:PMO-FL-ATTIV-GAR
                :IND-PMO-FL-ATTIV-GAR
               ,:PMO-CAMBIO-VER-PROD
                :IND-PMO-CAMBIO-VER-PROD
               ,:PMO-MM-DIFF
                :IND-PMO-MM-DIFF
               ,:PMO-IMP-RAT-MANFEE
                :IND-PMO-IMP-RAT-MANFEE
               ,:PMO-DT-ULT-EROG-MANFEE-DB
                :IND-PMO-DT-ULT-EROG-MANFEE
               ,:PMO-TP-OGG-RIVAL
                :IND-PMO-TP-OGG-RIVAL
               ,:PMO-SOM-ASSTA-GARAC
                :IND-PMO-SOM-ASSTA-GARAC
               ,:PMO-PC-APPLZ-OPZ
                :IND-PMO-PC-APPLZ-OPZ
               ,:PMO-ID-ADES
                :IND-PMO-ID-ADES
               ,:PMO-ID-POLI
               ,:PMO-TP-FRM-ASSVA
               ,:PMO-DS-RIGA
               ,:PMO-DS-OPER-SQL
               ,:PMO-DS-VER
               ,:PMO-DS-TS-INI-CPTZ
               ,:PMO-DS-TS-END-CPTZ
               ,:PMO-DS-UTENTE
               ,:PMO-DS-STATO-ELAB
               ,:PMO-TP-ESTR-CNT
                :IND-PMO-TP-ESTR-CNT
               ,:PMO-COD-RAMO
                :IND-PMO-COD-RAMO
               ,:PMO-GEN-DA-SIN
                :IND-PMO-GEN-DA-SIN
               ,:PMO-COD-TARI
                :IND-PMO-COD-TARI
               ,:PMO-NUM-RAT-PAG-PRE
                :IND-PMO-NUM-RAT-PAG-PRE
               ,:PMO-PC-SERV-VAL
                :IND-PMO-PC-SERV-VAL
               ,:PMO-ETA-AA-SOGL-BNFICR
                :IND-PMO-ETA-AA-SOGL-BNFICR
             FROM PARAM_MOVI
             WHERE     IB_OGG = :PMO-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           EXEC SQL
                OPEN C-IBO-EFF-PMO
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           EXEC SQL
                CLOSE C-IBO-EFF-PMO
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           EXEC SQL
                FETCH C-IBO-EFF-PMO
           INTO
                :PMO-ID-PARAM-MOVI
               ,:PMO-ID-OGG
               ,:PMO-TP-OGG
               ,:PMO-ID-MOVI-CRZ
               ,:PMO-ID-MOVI-CHIU
                :IND-PMO-ID-MOVI-CHIU
               ,:PMO-DT-INI-EFF-DB
               ,:PMO-DT-END-EFF-DB
               ,:PMO-COD-COMP-ANIA
               ,:PMO-TP-MOVI
                :IND-PMO-TP-MOVI
               ,:PMO-FRQ-MOVI
                :IND-PMO-FRQ-MOVI
               ,:PMO-DUR-AA
                :IND-PMO-DUR-AA
               ,:PMO-DUR-MM
                :IND-PMO-DUR-MM
               ,:PMO-DUR-GG
                :IND-PMO-DUR-GG
               ,:PMO-DT-RICOR-PREC-DB
                :IND-PMO-DT-RICOR-PREC
               ,:PMO-DT-RICOR-SUCC-DB
                :IND-PMO-DT-RICOR-SUCC
               ,:PMO-PC-INTR-FRAZ
                :IND-PMO-PC-INTR-FRAZ
               ,:PMO-IMP-BNS-DA-SCO-TOT
                :IND-PMO-IMP-BNS-DA-SCO-TOT
               ,:PMO-IMP-BNS-DA-SCO
                :IND-PMO-IMP-BNS-DA-SCO
               ,:PMO-PC-ANTIC-BNS
                :IND-PMO-PC-ANTIC-BNS
               ,:PMO-TP-RINN-COLL
                :IND-PMO-TP-RINN-COLL
               ,:PMO-TP-RIVAL-PRE
                :IND-PMO-TP-RIVAL-PRE
               ,:PMO-TP-RIVAL-PRSTZ
                :IND-PMO-TP-RIVAL-PRSTZ
               ,:PMO-FL-EVID-RIVAL
                :IND-PMO-FL-EVID-RIVAL
               ,:PMO-ULT-PC-PERD
                :IND-PMO-ULT-PC-PERD
               ,:PMO-TOT-AA-GIA-PROR
                :IND-PMO-TOT-AA-GIA-PROR
               ,:PMO-TP-OPZ
                :IND-PMO-TP-OPZ
               ,:PMO-AA-REN-CER
                :IND-PMO-AA-REN-CER
               ,:PMO-PC-REVRSB
                :IND-PMO-PC-REVRSB
               ,:PMO-IMP-RISC-PARZ-PRGT
                :IND-PMO-IMP-RISC-PARZ-PRGT
               ,:PMO-IMP-LRD-DI-RAT
                :IND-PMO-IMP-LRD-DI-RAT
               ,:PMO-IB-OGG
                :IND-PMO-IB-OGG
               ,:PMO-COS-ONER
                :IND-PMO-COS-ONER
               ,:PMO-SPE-PC
                :IND-PMO-SPE-PC
               ,:PMO-FL-ATTIV-GAR
                :IND-PMO-FL-ATTIV-GAR
               ,:PMO-CAMBIO-VER-PROD
                :IND-PMO-CAMBIO-VER-PROD
               ,:PMO-MM-DIFF
                :IND-PMO-MM-DIFF
               ,:PMO-IMP-RAT-MANFEE
                :IND-PMO-IMP-RAT-MANFEE
               ,:PMO-DT-ULT-EROG-MANFEE-DB
                :IND-PMO-DT-ULT-EROG-MANFEE
               ,:PMO-TP-OGG-RIVAL
                :IND-PMO-TP-OGG-RIVAL
               ,:PMO-SOM-ASSTA-GARAC
                :IND-PMO-SOM-ASSTA-GARAC
               ,:PMO-PC-APPLZ-OPZ
                :IND-PMO-PC-APPLZ-OPZ
               ,:PMO-ID-ADES
                :IND-PMO-ID-ADES
               ,:PMO-ID-POLI
               ,:PMO-TP-FRM-ASSVA
               ,:PMO-DS-RIGA
               ,:PMO-DS-OPER-SQL
               ,:PMO-DS-VER
               ,:PMO-DS-TS-INI-CPTZ
               ,:PMO-DS-TS-END-CPTZ
               ,:PMO-DS-UTENTE
               ,:PMO-DS-STATO-ELAB
               ,:PMO-TP-ESTR-CNT
                :IND-PMO-TP-ESTR-CNT
               ,:PMO-COD-RAMO
                :IND-PMO-COD-RAMO
               ,:PMO-GEN-DA-SIN
                :IND-PMO-GEN-DA-SIN
               ,:PMO-COD-TARI
                :IND-PMO-COD-TARI
               ,:PMO-NUM-RAT-PAG-PRE
                :IND-PMO-NUM-RAT-PAG-PRE
               ,:PMO-PC-SERV-VAL
                :IND-PMO-PC-SERV-VAL
               ,:PMO-ETA-AA-SOGL-BNFICR
                :IND-PMO-ETA-AA-SOGL-BNFICR
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A570-CLOSE-CURSOR-IBO     THRU A570-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDO-EFF-PMO CURSOR FOR
              SELECT
                     ID_PARAM_MOVI
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_MOVI
                    ,FRQ_MOVI
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,DT_RICOR_PREC
                    ,DT_RICOR_SUCC
                    ,PC_INTR_FRAZ
                    ,IMP_BNS_DA_SCO_TOT
                    ,IMP_BNS_DA_SCO
                    ,PC_ANTIC_BNS
                    ,TP_RINN_COLL
                    ,TP_RIVAL_PRE
                    ,TP_RIVAL_PRSTZ
                    ,FL_EVID_RIVAL
                    ,ULT_PC_PERD
                    ,TOT_AA_GIA_PROR
                    ,TP_OPZ
                    ,AA_REN_CER
                    ,PC_REVRSB
                    ,IMP_RISC_PARZ_PRGT
                    ,IMP_LRD_DI_RAT
                    ,IB_OGG
                    ,COS_ONER
                    ,SPE_PC
                    ,FL_ATTIV_GAR
                    ,CAMBIO_VER_PROD
                    ,MM_DIFF
                    ,IMP_RAT_MANFEE
                    ,DT_ULT_EROG_MANFEE
                    ,TP_OGG_RIVAL
                    ,SOM_ASSTA_GARAC
                    ,PC_APPLZ_OPZ
                    ,ID_ADES
                    ,ID_POLI
                    ,TP_FRM_ASSVA
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,TP_ESTR_CNT
                    ,COD_RAMO
                    ,GEN_DA_SIN
                    ,COD_TARI
                    ,NUM_RAT_PAG_PRE
                    ,PC_SERV_VAL
                    ,ETA_AA_SOGL_BNFICR
              FROM PARAM_MOVI
              WHERE     ID_OGG = :PMO-ID-OGG
                    AND TP_OGG = :PMO-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_PARAM_MOVI ASC

           END-EXEC.

       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_PARAM_MOVI
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_MOVI
                ,FRQ_MOVI
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,DT_RICOR_PREC
                ,DT_RICOR_SUCC
                ,PC_INTR_FRAZ
                ,IMP_BNS_DA_SCO_TOT
                ,IMP_BNS_DA_SCO
                ,PC_ANTIC_BNS
                ,TP_RINN_COLL
                ,TP_RIVAL_PRE
                ,TP_RIVAL_PRSTZ
                ,FL_EVID_RIVAL
                ,ULT_PC_PERD
                ,TOT_AA_GIA_PROR
                ,TP_OPZ
                ,AA_REN_CER
                ,PC_REVRSB
                ,IMP_RISC_PARZ_PRGT
                ,IMP_LRD_DI_RAT
                ,IB_OGG
                ,COS_ONER
                ,SPE_PC
                ,FL_ATTIV_GAR
                ,CAMBIO_VER_PROD
                ,MM_DIFF
                ,IMP_RAT_MANFEE
                ,DT_ULT_EROG_MANFEE
                ,TP_OGG_RIVAL
                ,SOM_ASSTA_GARAC
                ,PC_APPLZ_OPZ
                ,ID_ADES
                ,ID_POLI
                ,TP_FRM_ASSVA
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TP_ESTR_CNT
                ,COD_RAMO
                ,GEN_DA_SIN
                ,COD_TARI
                ,NUM_RAT_PAG_PRE
                ,PC_SERV_VAL
                ,ETA_AA_SOGL_BNFICR
             INTO
                :PMO-ID-PARAM-MOVI
               ,:PMO-ID-OGG
               ,:PMO-TP-OGG
               ,:PMO-ID-MOVI-CRZ
               ,:PMO-ID-MOVI-CHIU
                :IND-PMO-ID-MOVI-CHIU
               ,:PMO-DT-INI-EFF-DB
               ,:PMO-DT-END-EFF-DB
               ,:PMO-COD-COMP-ANIA
               ,:PMO-TP-MOVI
                :IND-PMO-TP-MOVI
               ,:PMO-FRQ-MOVI
                :IND-PMO-FRQ-MOVI
               ,:PMO-DUR-AA
                :IND-PMO-DUR-AA
               ,:PMO-DUR-MM
                :IND-PMO-DUR-MM
               ,:PMO-DUR-GG
                :IND-PMO-DUR-GG
               ,:PMO-DT-RICOR-PREC-DB
                :IND-PMO-DT-RICOR-PREC
               ,:PMO-DT-RICOR-SUCC-DB
                :IND-PMO-DT-RICOR-SUCC
               ,:PMO-PC-INTR-FRAZ
                :IND-PMO-PC-INTR-FRAZ
               ,:PMO-IMP-BNS-DA-SCO-TOT
                :IND-PMO-IMP-BNS-DA-SCO-TOT
               ,:PMO-IMP-BNS-DA-SCO
                :IND-PMO-IMP-BNS-DA-SCO
               ,:PMO-PC-ANTIC-BNS
                :IND-PMO-PC-ANTIC-BNS
               ,:PMO-TP-RINN-COLL
                :IND-PMO-TP-RINN-COLL
               ,:PMO-TP-RIVAL-PRE
                :IND-PMO-TP-RIVAL-PRE
               ,:PMO-TP-RIVAL-PRSTZ
                :IND-PMO-TP-RIVAL-PRSTZ
               ,:PMO-FL-EVID-RIVAL
                :IND-PMO-FL-EVID-RIVAL
               ,:PMO-ULT-PC-PERD
                :IND-PMO-ULT-PC-PERD
               ,:PMO-TOT-AA-GIA-PROR
                :IND-PMO-TOT-AA-GIA-PROR
               ,:PMO-TP-OPZ
                :IND-PMO-TP-OPZ
               ,:PMO-AA-REN-CER
                :IND-PMO-AA-REN-CER
               ,:PMO-PC-REVRSB
                :IND-PMO-PC-REVRSB
               ,:PMO-IMP-RISC-PARZ-PRGT
                :IND-PMO-IMP-RISC-PARZ-PRGT
               ,:PMO-IMP-LRD-DI-RAT
                :IND-PMO-IMP-LRD-DI-RAT
               ,:PMO-IB-OGG
                :IND-PMO-IB-OGG
               ,:PMO-COS-ONER
                :IND-PMO-COS-ONER
               ,:PMO-SPE-PC
                :IND-PMO-SPE-PC
               ,:PMO-FL-ATTIV-GAR
                :IND-PMO-FL-ATTIV-GAR
               ,:PMO-CAMBIO-VER-PROD
                :IND-PMO-CAMBIO-VER-PROD
               ,:PMO-MM-DIFF
                :IND-PMO-MM-DIFF
               ,:PMO-IMP-RAT-MANFEE
                :IND-PMO-IMP-RAT-MANFEE
               ,:PMO-DT-ULT-EROG-MANFEE-DB
                :IND-PMO-DT-ULT-EROG-MANFEE
               ,:PMO-TP-OGG-RIVAL
                :IND-PMO-TP-OGG-RIVAL
               ,:PMO-SOM-ASSTA-GARAC
                :IND-PMO-SOM-ASSTA-GARAC
               ,:PMO-PC-APPLZ-OPZ
                :IND-PMO-PC-APPLZ-OPZ
               ,:PMO-ID-ADES
                :IND-PMO-ID-ADES
               ,:PMO-ID-POLI
               ,:PMO-TP-FRM-ASSVA
               ,:PMO-DS-RIGA
               ,:PMO-DS-OPER-SQL
               ,:PMO-DS-VER
               ,:PMO-DS-TS-INI-CPTZ
               ,:PMO-DS-TS-END-CPTZ
               ,:PMO-DS-UTENTE
               ,:PMO-DS-STATO-ELAB
               ,:PMO-TP-ESTR-CNT
                :IND-PMO-TP-ESTR-CNT
               ,:PMO-COD-RAMO
                :IND-PMO-COD-RAMO
               ,:PMO-GEN-DA-SIN
                :IND-PMO-GEN-DA-SIN
               ,:PMO-COD-TARI
                :IND-PMO-COD-TARI
               ,:PMO-NUM-RAT-PAG-PRE
                :IND-PMO-NUM-RAT-PAG-PRE
               ,:PMO-PC-SERV-VAL
                :IND-PMO-PC-SERV-VAL
               ,:PMO-ETA-AA-SOGL-BNFICR
                :IND-PMO-ETA-AA-SOGL-BNFICR
             FROM PARAM_MOVI
             WHERE     ID_OGG = :PMO-ID-OGG
                    AND TP_OGG = :PMO-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           EXEC SQL
                OPEN C-IDO-EFF-PMO
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           EXEC SQL
                CLOSE C-IDO-EFF-PMO
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           EXEC SQL
                FETCH C-IDO-EFF-PMO
           INTO
                :PMO-ID-PARAM-MOVI
               ,:PMO-ID-OGG
               ,:PMO-TP-OGG
               ,:PMO-ID-MOVI-CRZ
               ,:PMO-ID-MOVI-CHIU
                :IND-PMO-ID-MOVI-CHIU
               ,:PMO-DT-INI-EFF-DB
               ,:PMO-DT-END-EFF-DB
               ,:PMO-COD-COMP-ANIA
               ,:PMO-TP-MOVI
                :IND-PMO-TP-MOVI
               ,:PMO-FRQ-MOVI
                :IND-PMO-FRQ-MOVI
               ,:PMO-DUR-AA
                :IND-PMO-DUR-AA
               ,:PMO-DUR-MM
                :IND-PMO-DUR-MM
               ,:PMO-DUR-GG
                :IND-PMO-DUR-GG
               ,:PMO-DT-RICOR-PREC-DB
                :IND-PMO-DT-RICOR-PREC
               ,:PMO-DT-RICOR-SUCC-DB
                :IND-PMO-DT-RICOR-SUCC
               ,:PMO-PC-INTR-FRAZ
                :IND-PMO-PC-INTR-FRAZ
               ,:PMO-IMP-BNS-DA-SCO-TOT
                :IND-PMO-IMP-BNS-DA-SCO-TOT
               ,:PMO-IMP-BNS-DA-SCO
                :IND-PMO-IMP-BNS-DA-SCO
               ,:PMO-PC-ANTIC-BNS
                :IND-PMO-PC-ANTIC-BNS
               ,:PMO-TP-RINN-COLL
                :IND-PMO-TP-RINN-COLL
               ,:PMO-TP-RIVAL-PRE
                :IND-PMO-TP-RIVAL-PRE
               ,:PMO-TP-RIVAL-PRSTZ
                :IND-PMO-TP-RIVAL-PRSTZ
               ,:PMO-FL-EVID-RIVAL
                :IND-PMO-FL-EVID-RIVAL
               ,:PMO-ULT-PC-PERD
                :IND-PMO-ULT-PC-PERD
               ,:PMO-TOT-AA-GIA-PROR
                :IND-PMO-TOT-AA-GIA-PROR
               ,:PMO-TP-OPZ
                :IND-PMO-TP-OPZ
               ,:PMO-AA-REN-CER
                :IND-PMO-AA-REN-CER
               ,:PMO-PC-REVRSB
                :IND-PMO-PC-REVRSB
               ,:PMO-IMP-RISC-PARZ-PRGT
                :IND-PMO-IMP-RISC-PARZ-PRGT
               ,:PMO-IMP-LRD-DI-RAT
                :IND-PMO-IMP-LRD-DI-RAT
               ,:PMO-IB-OGG
                :IND-PMO-IB-OGG
               ,:PMO-COS-ONER
                :IND-PMO-COS-ONER
               ,:PMO-SPE-PC
                :IND-PMO-SPE-PC
               ,:PMO-FL-ATTIV-GAR
                :IND-PMO-FL-ATTIV-GAR
               ,:PMO-CAMBIO-VER-PROD
                :IND-PMO-CAMBIO-VER-PROD
               ,:PMO-MM-DIFF
                :IND-PMO-MM-DIFF
               ,:PMO-IMP-RAT-MANFEE
                :IND-PMO-IMP-RAT-MANFEE
               ,:PMO-DT-ULT-EROG-MANFEE-DB
                :IND-PMO-DT-ULT-EROG-MANFEE
               ,:PMO-TP-OGG-RIVAL
                :IND-PMO-TP-OGG-RIVAL
               ,:PMO-SOM-ASSTA-GARAC
                :IND-PMO-SOM-ASSTA-GARAC
               ,:PMO-PC-APPLZ-OPZ
                :IND-PMO-PC-APPLZ-OPZ
               ,:PMO-ID-ADES
                :IND-PMO-ID-ADES
               ,:PMO-ID-POLI
               ,:PMO-TP-FRM-ASSVA
               ,:PMO-DS-RIGA
               ,:PMO-DS-OPER-SQL
               ,:PMO-DS-VER
               ,:PMO-DS-TS-INI-CPTZ
               ,:PMO-DS-TS-END-CPTZ
               ,:PMO-DS-UTENTE
               ,:PMO-DS-STATO-ELAB
               ,:PMO-TP-ESTR-CNT
                :IND-PMO-TP-ESTR-CNT
               ,:PMO-COD-RAMO
                :IND-PMO-COD-RAMO
               ,:PMO-GEN-DA-SIN
                :IND-PMO-GEN-DA-SIN
               ,:PMO-COD-TARI
                :IND-PMO-COD-TARI
               ,:PMO-NUM-RAT-PAG-PRE
                :IND-PMO-NUM-RAT-PAG-PRE
               ,:PMO-PC-SERV-VAL
                :IND-PMO-PC-SERV-VAL
               ,:PMO-ETA-AA-SOGL-BNFICR
                :IND-PMO-ETA-AA-SOGL-BNFICR
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A770-CLOSE-CURSOR-IDO     THRU A770-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_PARAM_MOVI
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_MOVI
                ,FRQ_MOVI
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,DT_RICOR_PREC
                ,DT_RICOR_SUCC
                ,PC_INTR_FRAZ
                ,IMP_BNS_DA_SCO_TOT
                ,IMP_BNS_DA_SCO
                ,PC_ANTIC_BNS
                ,TP_RINN_COLL
                ,TP_RIVAL_PRE
                ,TP_RIVAL_PRSTZ
                ,FL_EVID_RIVAL
                ,ULT_PC_PERD
                ,TOT_AA_GIA_PROR
                ,TP_OPZ
                ,AA_REN_CER
                ,PC_REVRSB
                ,IMP_RISC_PARZ_PRGT
                ,IMP_LRD_DI_RAT
                ,IB_OGG
                ,COS_ONER
                ,SPE_PC
                ,FL_ATTIV_GAR
                ,CAMBIO_VER_PROD
                ,MM_DIFF
                ,IMP_RAT_MANFEE
                ,DT_ULT_EROG_MANFEE
                ,TP_OGG_RIVAL
                ,SOM_ASSTA_GARAC
                ,PC_APPLZ_OPZ
                ,ID_ADES
                ,ID_POLI
                ,TP_FRM_ASSVA
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TP_ESTR_CNT
                ,COD_RAMO
                ,GEN_DA_SIN
                ,COD_TARI
                ,NUM_RAT_PAG_PRE
                ,PC_SERV_VAL
                ,ETA_AA_SOGL_BNFICR
             INTO
                :PMO-ID-PARAM-MOVI
               ,:PMO-ID-OGG
               ,:PMO-TP-OGG
               ,:PMO-ID-MOVI-CRZ
               ,:PMO-ID-MOVI-CHIU
                :IND-PMO-ID-MOVI-CHIU
               ,:PMO-DT-INI-EFF-DB
               ,:PMO-DT-END-EFF-DB
               ,:PMO-COD-COMP-ANIA
               ,:PMO-TP-MOVI
                :IND-PMO-TP-MOVI
               ,:PMO-FRQ-MOVI
                :IND-PMO-FRQ-MOVI
               ,:PMO-DUR-AA
                :IND-PMO-DUR-AA
               ,:PMO-DUR-MM
                :IND-PMO-DUR-MM
               ,:PMO-DUR-GG
                :IND-PMO-DUR-GG
               ,:PMO-DT-RICOR-PREC-DB
                :IND-PMO-DT-RICOR-PREC
               ,:PMO-DT-RICOR-SUCC-DB
                :IND-PMO-DT-RICOR-SUCC
               ,:PMO-PC-INTR-FRAZ
                :IND-PMO-PC-INTR-FRAZ
               ,:PMO-IMP-BNS-DA-SCO-TOT
                :IND-PMO-IMP-BNS-DA-SCO-TOT
               ,:PMO-IMP-BNS-DA-SCO
                :IND-PMO-IMP-BNS-DA-SCO
               ,:PMO-PC-ANTIC-BNS
                :IND-PMO-PC-ANTIC-BNS
               ,:PMO-TP-RINN-COLL
                :IND-PMO-TP-RINN-COLL
               ,:PMO-TP-RIVAL-PRE
                :IND-PMO-TP-RIVAL-PRE
               ,:PMO-TP-RIVAL-PRSTZ
                :IND-PMO-TP-RIVAL-PRSTZ
               ,:PMO-FL-EVID-RIVAL
                :IND-PMO-FL-EVID-RIVAL
               ,:PMO-ULT-PC-PERD
                :IND-PMO-ULT-PC-PERD
               ,:PMO-TOT-AA-GIA-PROR
                :IND-PMO-TOT-AA-GIA-PROR
               ,:PMO-TP-OPZ
                :IND-PMO-TP-OPZ
               ,:PMO-AA-REN-CER
                :IND-PMO-AA-REN-CER
               ,:PMO-PC-REVRSB
                :IND-PMO-PC-REVRSB
               ,:PMO-IMP-RISC-PARZ-PRGT
                :IND-PMO-IMP-RISC-PARZ-PRGT
               ,:PMO-IMP-LRD-DI-RAT
                :IND-PMO-IMP-LRD-DI-RAT
               ,:PMO-IB-OGG
                :IND-PMO-IB-OGG
               ,:PMO-COS-ONER
                :IND-PMO-COS-ONER
               ,:PMO-SPE-PC
                :IND-PMO-SPE-PC
               ,:PMO-FL-ATTIV-GAR
                :IND-PMO-FL-ATTIV-GAR
               ,:PMO-CAMBIO-VER-PROD
                :IND-PMO-CAMBIO-VER-PROD
               ,:PMO-MM-DIFF
                :IND-PMO-MM-DIFF
               ,:PMO-IMP-RAT-MANFEE
                :IND-PMO-IMP-RAT-MANFEE
               ,:PMO-DT-ULT-EROG-MANFEE-DB
                :IND-PMO-DT-ULT-EROG-MANFEE
               ,:PMO-TP-OGG-RIVAL
                :IND-PMO-TP-OGG-RIVAL
               ,:PMO-SOM-ASSTA-GARAC
                :IND-PMO-SOM-ASSTA-GARAC
               ,:PMO-PC-APPLZ-OPZ
                :IND-PMO-PC-APPLZ-OPZ
               ,:PMO-ID-ADES
                :IND-PMO-ID-ADES
               ,:PMO-ID-POLI
               ,:PMO-TP-FRM-ASSVA
               ,:PMO-DS-RIGA
               ,:PMO-DS-OPER-SQL
               ,:PMO-DS-VER
               ,:PMO-DS-TS-INI-CPTZ
               ,:PMO-DS-TS-END-CPTZ
               ,:PMO-DS-UTENTE
               ,:PMO-DS-STATO-ELAB
               ,:PMO-TP-ESTR-CNT
                :IND-PMO-TP-ESTR-CNT
               ,:PMO-COD-RAMO
                :IND-PMO-COD-RAMO
               ,:PMO-GEN-DA-SIN
                :IND-PMO-GEN-DA-SIN
               ,:PMO-COD-TARI
                :IND-PMO-COD-TARI
               ,:PMO-NUM-RAT-PAG-PRE
                :IND-PMO-NUM-RAT-PAG-PRE
               ,:PMO-PC-SERV-VAL
                :IND-PMO-PC-SERV-VAL
               ,:PMO-ETA-AA-SOGL-BNFICR
                :IND-PMO-ETA-AA-SOGL-BNFICR
             FROM PARAM_MOVI
             WHERE     ID_PARAM_MOVI = :PMO-ID-PARAM-MOVI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IBO-CPZ-PMO CURSOR FOR
              SELECT
                     ID_PARAM_MOVI
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_MOVI
                    ,FRQ_MOVI
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,DT_RICOR_PREC
                    ,DT_RICOR_SUCC
                    ,PC_INTR_FRAZ
                    ,IMP_BNS_DA_SCO_TOT
                    ,IMP_BNS_DA_SCO
                    ,PC_ANTIC_BNS
                    ,TP_RINN_COLL
                    ,TP_RIVAL_PRE
                    ,TP_RIVAL_PRSTZ
                    ,FL_EVID_RIVAL
                    ,ULT_PC_PERD
                    ,TOT_AA_GIA_PROR
                    ,TP_OPZ
                    ,AA_REN_CER
                    ,PC_REVRSB
                    ,IMP_RISC_PARZ_PRGT
                    ,IMP_LRD_DI_RAT
                    ,IB_OGG
                    ,COS_ONER
                    ,SPE_PC
                    ,FL_ATTIV_GAR
                    ,CAMBIO_VER_PROD
                    ,MM_DIFF
                    ,IMP_RAT_MANFEE
                    ,DT_ULT_EROG_MANFEE
                    ,TP_OGG_RIVAL
                    ,SOM_ASSTA_GARAC
                    ,PC_APPLZ_OPZ
                    ,ID_ADES
                    ,ID_POLI
                    ,TP_FRM_ASSVA
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,TP_ESTR_CNT
                    ,COD_RAMO
                    ,GEN_DA_SIN
                    ,COD_TARI
                    ,NUM_RAT_PAG_PRE
                    ,PC_SERV_VAL
                    ,ETA_AA_SOGL_BNFICR
              FROM PARAM_MOVI
              WHERE     IB_OGG = :PMO-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_PARAM_MOVI ASC

           END-EXEC.

       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_PARAM_MOVI
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_MOVI
                ,FRQ_MOVI
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,DT_RICOR_PREC
                ,DT_RICOR_SUCC
                ,PC_INTR_FRAZ
                ,IMP_BNS_DA_SCO_TOT
                ,IMP_BNS_DA_SCO
                ,PC_ANTIC_BNS
                ,TP_RINN_COLL
                ,TP_RIVAL_PRE
                ,TP_RIVAL_PRSTZ
                ,FL_EVID_RIVAL
                ,ULT_PC_PERD
                ,TOT_AA_GIA_PROR
                ,TP_OPZ
                ,AA_REN_CER
                ,PC_REVRSB
                ,IMP_RISC_PARZ_PRGT
                ,IMP_LRD_DI_RAT
                ,IB_OGG
                ,COS_ONER
                ,SPE_PC
                ,FL_ATTIV_GAR
                ,CAMBIO_VER_PROD
                ,MM_DIFF
                ,IMP_RAT_MANFEE
                ,DT_ULT_EROG_MANFEE
                ,TP_OGG_RIVAL
                ,SOM_ASSTA_GARAC
                ,PC_APPLZ_OPZ
                ,ID_ADES
                ,ID_POLI
                ,TP_FRM_ASSVA
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TP_ESTR_CNT
                ,COD_RAMO
                ,GEN_DA_SIN
                ,COD_TARI
                ,NUM_RAT_PAG_PRE
                ,PC_SERV_VAL
                ,ETA_AA_SOGL_BNFICR
             INTO
                :PMO-ID-PARAM-MOVI
               ,:PMO-ID-OGG
               ,:PMO-TP-OGG
               ,:PMO-ID-MOVI-CRZ
               ,:PMO-ID-MOVI-CHIU
                :IND-PMO-ID-MOVI-CHIU
               ,:PMO-DT-INI-EFF-DB
               ,:PMO-DT-END-EFF-DB
               ,:PMO-COD-COMP-ANIA
               ,:PMO-TP-MOVI
                :IND-PMO-TP-MOVI
               ,:PMO-FRQ-MOVI
                :IND-PMO-FRQ-MOVI
               ,:PMO-DUR-AA
                :IND-PMO-DUR-AA
               ,:PMO-DUR-MM
                :IND-PMO-DUR-MM
               ,:PMO-DUR-GG
                :IND-PMO-DUR-GG
               ,:PMO-DT-RICOR-PREC-DB
                :IND-PMO-DT-RICOR-PREC
               ,:PMO-DT-RICOR-SUCC-DB
                :IND-PMO-DT-RICOR-SUCC
               ,:PMO-PC-INTR-FRAZ
                :IND-PMO-PC-INTR-FRAZ
               ,:PMO-IMP-BNS-DA-SCO-TOT
                :IND-PMO-IMP-BNS-DA-SCO-TOT
               ,:PMO-IMP-BNS-DA-SCO
                :IND-PMO-IMP-BNS-DA-SCO
               ,:PMO-PC-ANTIC-BNS
                :IND-PMO-PC-ANTIC-BNS
               ,:PMO-TP-RINN-COLL
                :IND-PMO-TP-RINN-COLL
               ,:PMO-TP-RIVAL-PRE
                :IND-PMO-TP-RIVAL-PRE
               ,:PMO-TP-RIVAL-PRSTZ
                :IND-PMO-TP-RIVAL-PRSTZ
               ,:PMO-FL-EVID-RIVAL
                :IND-PMO-FL-EVID-RIVAL
               ,:PMO-ULT-PC-PERD
                :IND-PMO-ULT-PC-PERD
               ,:PMO-TOT-AA-GIA-PROR
                :IND-PMO-TOT-AA-GIA-PROR
               ,:PMO-TP-OPZ
                :IND-PMO-TP-OPZ
               ,:PMO-AA-REN-CER
                :IND-PMO-AA-REN-CER
               ,:PMO-PC-REVRSB
                :IND-PMO-PC-REVRSB
               ,:PMO-IMP-RISC-PARZ-PRGT
                :IND-PMO-IMP-RISC-PARZ-PRGT
               ,:PMO-IMP-LRD-DI-RAT
                :IND-PMO-IMP-LRD-DI-RAT
               ,:PMO-IB-OGG
                :IND-PMO-IB-OGG
               ,:PMO-COS-ONER
                :IND-PMO-COS-ONER
               ,:PMO-SPE-PC
                :IND-PMO-SPE-PC
               ,:PMO-FL-ATTIV-GAR
                :IND-PMO-FL-ATTIV-GAR
               ,:PMO-CAMBIO-VER-PROD
                :IND-PMO-CAMBIO-VER-PROD
               ,:PMO-MM-DIFF
                :IND-PMO-MM-DIFF
               ,:PMO-IMP-RAT-MANFEE
                :IND-PMO-IMP-RAT-MANFEE
               ,:PMO-DT-ULT-EROG-MANFEE-DB
                :IND-PMO-DT-ULT-EROG-MANFEE
               ,:PMO-TP-OGG-RIVAL
                :IND-PMO-TP-OGG-RIVAL
               ,:PMO-SOM-ASSTA-GARAC
                :IND-PMO-SOM-ASSTA-GARAC
               ,:PMO-PC-APPLZ-OPZ
                :IND-PMO-PC-APPLZ-OPZ
               ,:PMO-ID-ADES
                :IND-PMO-ID-ADES
               ,:PMO-ID-POLI
               ,:PMO-TP-FRM-ASSVA
               ,:PMO-DS-RIGA
               ,:PMO-DS-OPER-SQL
               ,:PMO-DS-VER
               ,:PMO-DS-TS-INI-CPTZ
               ,:PMO-DS-TS-END-CPTZ
               ,:PMO-DS-UTENTE
               ,:PMO-DS-STATO-ELAB
               ,:PMO-TP-ESTR-CNT
                :IND-PMO-TP-ESTR-CNT
               ,:PMO-COD-RAMO
                :IND-PMO-COD-RAMO
               ,:PMO-GEN-DA-SIN
                :IND-PMO-GEN-DA-SIN
               ,:PMO-COD-TARI
                :IND-PMO-COD-TARI
               ,:PMO-NUM-RAT-PAG-PRE
                :IND-PMO-NUM-RAT-PAG-PRE
               ,:PMO-PC-SERV-VAL
                :IND-PMO-PC-SERV-VAL
               ,:PMO-ETA-AA-SOGL-BNFICR
                :IND-PMO-ETA-AA-SOGL-BNFICR
             FROM PARAM_MOVI
             WHERE     IB_OGG = :PMO-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           EXEC SQL
                OPEN C-IBO-CPZ-PMO
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           EXEC SQL
                CLOSE C-IBO-CPZ-PMO
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           EXEC SQL
                FETCH C-IBO-CPZ-PMO
           INTO
                :PMO-ID-PARAM-MOVI
               ,:PMO-ID-OGG
               ,:PMO-TP-OGG
               ,:PMO-ID-MOVI-CRZ
               ,:PMO-ID-MOVI-CHIU
                :IND-PMO-ID-MOVI-CHIU
               ,:PMO-DT-INI-EFF-DB
               ,:PMO-DT-END-EFF-DB
               ,:PMO-COD-COMP-ANIA
               ,:PMO-TP-MOVI
                :IND-PMO-TP-MOVI
               ,:PMO-FRQ-MOVI
                :IND-PMO-FRQ-MOVI
               ,:PMO-DUR-AA
                :IND-PMO-DUR-AA
               ,:PMO-DUR-MM
                :IND-PMO-DUR-MM
               ,:PMO-DUR-GG
                :IND-PMO-DUR-GG
               ,:PMO-DT-RICOR-PREC-DB
                :IND-PMO-DT-RICOR-PREC
               ,:PMO-DT-RICOR-SUCC-DB
                :IND-PMO-DT-RICOR-SUCC
               ,:PMO-PC-INTR-FRAZ
                :IND-PMO-PC-INTR-FRAZ
               ,:PMO-IMP-BNS-DA-SCO-TOT
                :IND-PMO-IMP-BNS-DA-SCO-TOT
               ,:PMO-IMP-BNS-DA-SCO
                :IND-PMO-IMP-BNS-DA-SCO
               ,:PMO-PC-ANTIC-BNS
                :IND-PMO-PC-ANTIC-BNS
               ,:PMO-TP-RINN-COLL
                :IND-PMO-TP-RINN-COLL
               ,:PMO-TP-RIVAL-PRE
                :IND-PMO-TP-RIVAL-PRE
               ,:PMO-TP-RIVAL-PRSTZ
                :IND-PMO-TP-RIVAL-PRSTZ
               ,:PMO-FL-EVID-RIVAL
                :IND-PMO-FL-EVID-RIVAL
               ,:PMO-ULT-PC-PERD
                :IND-PMO-ULT-PC-PERD
               ,:PMO-TOT-AA-GIA-PROR
                :IND-PMO-TOT-AA-GIA-PROR
               ,:PMO-TP-OPZ
                :IND-PMO-TP-OPZ
               ,:PMO-AA-REN-CER
                :IND-PMO-AA-REN-CER
               ,:PMO-PC-REVRSB
                :IND-PMO-PC-REVRSB
               ,:PMO-IMP-RISC-PARZ-PRGT
                :IND-PMO-IMP-RISC-PARZ-PRGT
               ,:PMO-IMP-LRD-DI-RAT
                :IND-PMO-IMP-LRD-DI-RAT
               ,:PMO-IB-OGG
                :IND-PMO-IB-OGG
               ,:PMO-COS-ONER
                :IND-PMO-COS-ONER
               ,:PMO-SPE-PC
                :IND-PMO-SPE-PC
               ,:PMO-FL-ATTIV-GAR
                :IND-PMO-FL-ATTIV-GAR
               ,:PMO-CAMBIO-VER-PROD
                :IND-PMO-CAMBIO-VER-PROD
               ,:PMO-MM-DIFF
                :IND-PMO-MM-DIFF
               ,:PMO-IMP-RAT-MANFEE
                :IND-PMO-IMP-RAT-MANFEE
               ,:PMO-DT-ULT-EROG-MANFEE-DB
                :IND-PMO-DT-ULT-EROG-MANFEE
               ,:PMO-TP-OGG-RIVAL
                :IND-PMO-TP-OGG-RIVAL
               ,:PMO-SOM-ASSTA-GARAC
                :IND-PMO-SOM-ASSTA-GARAC
               ,:PMO-PC-APPLZ-OPZ
                :IND-PMO-PC-APPLZ-OPZ
               ,:PMO-ID-ADES
                :IND-PMO-ID-ADES
               ,:PMO-ID-POLI
               ,:PMO-TP-FRM-ASSVA
               ,:PMO-DS-RIGA
               ,:PMO-DS-OPER-SQL
               ,:PMO-DS-VER
               ,:PMO-DS-TS-INI-CPTZ
               ,:PMO-DS-TS-END-CPTZ
               ,:PMO-DS-UTENTE
               ,:PMO-DS-STATO-ELAB
               ,:PMO-TP-ESTR-CNT
                :IND-PMO-TP-ESTR-CNT
               ,:PMO-COD-RAMO
                :IND-PMO-COD-RAMO
               ,:PMO-GEN-DA-SIN
                :IND-PMO-GEN-DA-SIN
               ,:PMO-COD-TARI
                :IND-PMO-COD-TARI
               ,:PMO-NUM-RAT-PAG-PRE
                :IND-PMO-NUM-RAT-PAG-PRE
               ,:PMO-PC-SERV-VAL
                :IND-PMO-PC-SERV-VAL
               ,:PMO-ETA-AA-SOGL-BNFICR
                :IND-PMO-ETA-AA-SOGL-BNFICR
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B570-CLOSE-CURSOR-IBO-CPZ     THRU B570-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDO-CPZ-PMO CURSOR FOR
              SELECT
                     ID_PARAM_MOVI
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_MOVI
                    ,FRQ_MOVI
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,DT_RICOR_PREC
                    ,DT_RICOR_SUCC
                    ,PC_INTR_FRAZ
                    ,IMP_BNS_DA_SCO_TOT
                    ,IMP_BNS_DA_SCO
                    ,PC_ANTIC_BNS
                    ,TP_RINN_COLL
                    ,TP_RIVAL_PRE
                    ,TP_RIVAL_PRSTZ
                    ,FL_EVID_RIVAL
                    ,ULT_PC_PERD
                    ,TOT_AA_GIA_PROR
                    ,TP_OPZ
                    ,AA_REN_CER
                    ,PC_REVRSB
                    ,IMP_RISC_PARZ_PRGT
                    ,IMP_LRD_DI_RAT
                    ,IB_OGG
                    ,COS_ONER
                    ,SPE_PC
                    ,FL_ATTIV_GAR
                    ,CAMBIO_VER_PROD
                    ,MM_DIFF
                    ,IMP_RAT_MANFEE
                    ,DT_ULT_EROG_MANFEE
                    ,TP_OGG_RIVAL
                    ,SOM_ASSTA_GARAC
                    ,PC_APPLZ_OPZ
                    ,ID_ADES
                    ,ID_POLI
                    ,TP_FRM_ASSVA
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,TP_ESTR_CNT
                    ,COD_RAMO
                    ,GEN_DA_SIN
                    ,COD_TARI
                    ,NUM_RAT_PAG_PRE
                    ,PC_SERV_VAL
                    ,ETA_AA_SOGL_BNFICR
              FROM PARAM_MOVI
              WHERE     ID_OGG = :PMO-ID-OGG
           AND TP_OGG = :PMO-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_PARAM_MOVI ASC

           END-EXEC.

       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_PARAM_MOVI
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_MOVI
                ,FRQ_MOVI
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,DT_RICOR_PREC
                ,DT_RICOR_SUCC
                ,PC_INTR_FRAZ
                ,IMP_BNS_DA_SCO_TOT
                ,IMP_BNS_DA_SCO
                ,PC_ANTIC_BNS
                ,TP_RINN_COLL
                ,TP_RIVAL_PRE
                ,TP_RIVAL_PRSTZ
                ,FL_EVID_RIVAL
                ,ULT_PC_PERD
                ,TOT_AA_GIA_PROR
                ,TP_OPZ
                ,AA_REN_CER
                ,PC_REVRSB
                ,IMP_RISC_PARZ_PRGT
                ,IMP_LRD_DI_RAT
                ,IB_OGG
                ,COS_ONER
                ,SPE_PC
                ,FL_ATTIV_GAR
                ,CAMBIO_VER_PROD
                ,MM_DIFF
                ,IMP_RAT_MANFEE
                ,DT_ULT_EROG_MANFEE
                ,TP_OGG_RIVAL
                ,SOM_ASSTA_GARAC
                ,PC_APPLZ_OPZ
                ,ID_ADES
                ,ID_POLI
                ,TP_FRM_ASSVA
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TP_ESTR_CNT
                ,COD_RAMO
                ,GEN_DA_SIN
                ,COD_TARI
                ,NUM_RAT_PAG_PRE
                ,PC_SERV_VAL
                ,ETA_AA_SOGL_BNFICR
             INTO
                :PMO-ID-PARAM-MOVI
               ,:PMO-ID-OGG
               ,:PMO-TP-OGG
               ,:PMO-ID-MOVI-CRZ
               ,:PMO-ID-MOVI-CHIU
                :IND-PMO-ID-MOVI-CHIU
               ,:PMO-DT-INI-EFF-DB
               ,:PMO-DT-END-EFF-DB
               ,:PMO-COD-COMP-ANIA
               ,:PMO-TP-MOVI
                :IND-PMO-TP-MOVI
               ,:PMO-FRQ-MOVI
                :IND-PMO-FRQ-MOVI
               ,:PMO-DUR-AA
                :IND-PMO-DUR-AA
               ,:PMO-DUR-MM
                :IND-PMO-DUR-MM
               ,:PMO-DUR-GG
                :IND-PMO-DUR-GG
               ,:PMO-DT-RICOR-PREC-DB
                :IND-PMO-DT-RICOR-PREC
               ,:PMO-DT-RICOR-SUCC-DB
                :IND-PMO-DT-RICOR-SUCC
               ,:PMO-PC-INTR-FRAZ
                :IND-PMO-PC-INTR-FRAZ
               ,:PMO-IMP-BNS-DA-SCO-TOT
                :IND-PMO-IMP-BNS-DA-SCO-TOT
               ,:PMO-IMP-BNS-DA-SCO
                :IND-PMO-IMP-BNS-DA-SCO
               ,:PMO-PC-ANTIC-BNS
                :IND-PMO-PC-ANTIC-BNS
               ,:PMO-TP-RINN-COLL
                :IND-PMO-TP-RINN-COLL
               ,:PMO-TP-RIVAL-PRE
                :IND-PMO-TP-RIVAL-PRE
               ,:PMO-TP-RIVAL-PRSTZ
                :IND-PMO-TP-RIVAL-PRSTZ
               ,:PMO-FL-EVID-RIVAL
                :IND-PMO-FL-EVID-RIVAL
               ,:PMO-ULT-PC-PERD
                :IND-PMO-ULT-PC-PERD
               ,:PMO-TOT-AA-GIA-PROR
                :IND-PMO-TOT-AA-GIA-PROR
               ,:PMO-TP-OPZ
                :IND-PMO-TP-OPZ
               ,:PMO-AA-REN-CER
                :IND-PMO-AA-REN-CER
               ,:PMO-PC-REVRSB
                :IND-PMO-PC-REVRSB
               ,:PMO-IMP-RISC-PARZ-PRGT
                :IND-PMO-IMP-RISC-PARZ-PRGT
               ,:PMO-IMP-LRD-DI-RAT
                :IND-PMO-IMP-LRD-DI-RAT
               ,:PMO-IB-OGG
                :IND-PMO-IB-OGG
               ,:PMO-COS-ONER
                :IND-PMO-COS-ONER
               ,:PMO-SPE-PC
                :IND-PMO-SPE-PC
               ,:PMO-FL-ATTIV-GAR
                :IND-PMO-FL-ATTIV-GAR
               ,:PMO-CAMBIO-VER-PROD
                :IND-PMO-CAMBIO-VER-PROD
               ,:PMO-MM-DIFF
                :IND-PMO-MM-DIFF
               ,:PMO-IMP-RAT-MANFEE
                :IND-PMO-IMP-RAT-MANFEE
               ,:PMO-DT-ULT-EROG-MANFEE-DB
                :IND-PMO-DT-ULT-EROG-MANFEE
               ,:PMO-TP-OGG-RIVAL
                :IND-PMO-TP-OGG-RIVAL
               ,:PMO-SOM-ASSTA-GARAC
                :IND-PMO-SOM-ASSTA-GARAC
               ,:PMO-PC-APPLZ-OPZ
                :IND-PMO-PC-APPLZ-OPZ
               ,:PMO-ID-ADES
                :IND-PMO-ID-ADES
               ,:PMO-ID-POLI
               ,:PMO-TP-FRM-ASSVA
               ,:PMO-DS-RIGA
               ,:PMO-DS-OPER-SQL
               ,:PMO-DS-VER
               ,:PMO-DS-TS-INI-CPTZ
               ,:PMO-DS-TS-END-CPTZ
               ,:PMO-DS-UTENTE
               ,:PMO-DS-STATO-ELAB
               ,:PMO-TP-ESTR-CNT
                :IND-PMO-TP-ESTR-CNT
               ,:PMO-COD-RAMO
                :IND-PMO-COD-RAMO
               ,:PMO-GEN-DA-SIN
                :IND-PMO-GEN-DA-SIN
               ,:PMO-COD-TARI
                :IND-PMO-COD-TARI
               ,:PMO-NUM-RAT-PAG-PRE
                :IND-PMO-NUM-RAT-PAG-PRE
               ,:PMO-PC-SERV-VAL
                :IND-PMO-PC-SERV-VAL
               ,:PMO-ETA-AA-SOGL-BNFICR
                :IND-PMO-ETA-AA-SOGL-BNFICR
             FROM PARAM_MOVI
             WHERE     ID_OGG = :PMO-ID-OGG
                    AND TP_OGG = :PMO-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           EXEC SQL
                OPEN C-IDO-CPZ-PMO
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           EXEC SQL
                CLOSE C-IDO-CPZ-PMO
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           EXEC SQL
                FETCH C-IDO-CPZ-PMO
           INTO
                :PMO-ID-PARAM-MOVI
               ,:PMO-ID-OGG
               ,:PMO-TP-OGG
               ,:PMO-ID-MOVI-CRZ
               ,:PMO-ID-MOVI-CHIU
                :IND-PMO-ID-MOVI-CHIU
               ,:PMO-DT-INI-EFF-DB
               ,:PMO-DT-END-EFF-DB
               ,:PMO-COD-COMP-ANIA
               ,:PMO-TP-MOVI
                :IND-PMO-TP-MOVI
               ,:PMO-FRQ-MOVI
                :IND-PMO-FRQ-MOVI
               ,:PMO-DUR-AA
                :IND-PMO-DUR-AA
               ,:PMO-DUR-MM
                :IND-PMO-DUR-MM
               ,:PMO-DUR-GG
                :IND-PMO-DUR-GG
               ,:PMO-DT-RICOR-PREC-DB
                :IND-PMO-DT-RICOR-PREC
               ,:PMO-DT-RICOR-SUCC-DB
                :IND-PMO-DT-RICOR-SUCC
               ,:PMO-PC-INTR-FRAZ
                :IND-PMO-PC-INTR-FRAZ
               ,:PMO-IMP-BNS-DA-SCO-TOT
                :IND-PMO-IMP-BNS-DA-SCO-TOT
               ,:PMO-IMP-BNS-DA-SCO
                :IND-PMO-IMP-BNS-DA-SCO
               ,:PMO-PC-ANTIC-BNS
                :IND-PMO-PC-ANTIC-BNS
               ,:PMO-TP-RINN-COLL
                :IND-PMO-TP-RINN-COLL
               ,:PMO-TP-RIVAL-PRE
                :IND-PMO-TP-RIVAL-PRE
               ,:PMO-TP-RIVAL-PRSTZ
                :IND-PMO-TP-RIVAL-PRSTZ
               ,:PMO-FL-EVID-RIVAL
                :IND-PMO-FL-EVID-RIVAL
               ,:PMO-ULT-PC-PERD
                :IND-PMO-ULT-PC-PERD
               ,:PMO-TOT-AA-GIA-PROR
                :IND-PMO-TOT-AA-GIA-PROR
               ,:PMO-TP-OPZ
                :IND-PMO-TP-OPZ
               ,:PMO-AA-REN-CER
                :IND-PMO-AA-REN-CER
               ,:PMO-PC-REVRSB
                :IND-PMO-PC-REVRSB
               ,:PMO-IMP-RISC-PARZ-PRGT
                :IND-PMO-IMP-RISC-PARZ-PRGT
               ,:PMO-IMP-LRD-DI-RAT
                :IND-PMO-IMP-LRD-DI-RAT
               ,:PMO-IB-OGG
                :IND-PMO-IB-OGG
               ,:PMO-COS-ONER
                :IND-PMO-COS-ONER
               ,:PMO-SPE-PC
                :IND-PMO-SPE-PC
               ,:PMO-FL-ATTIV-GAR
                :IND-PMO-FL-ATTIV-GAR
               ,:PMO-CAMBIO-VER-PROD
                :IND-PMO-CAMBIO-VER-PROD
               ,:PMO-MM-DIFF
                :IND-PMO-MM-DIFF
               ,:PMO-IMP-RAT-MANFEE
                :IND-PMO-IMP-RAT-MANFEE
               ,:PMO-DT-ULT-EROG-MANFEE-DB
                :IND-PMO-DT-ULT-EROG-MANFEE
               ,:PMO-TP-OGG-RIVAL
                :IND-PMO-TP-OGG-RIVAL
               ,:PMO-SOM-ASSTA-GARAC
                :IND-PMO-SOM-ASSTA-GARAC
               ,:PMO-PC-APPLZ-OPZ
                :IND-PMO-PC-APPLZ-OPZ
               ,:PMO-ID-ADES
                :IND-PMO-ID-ADES
               ,:PMO-ID-POLI
               ,:PMO-TP-FRM-ASSVA
               ,:PMO-DS-RIGA
               ,:PMO-DS-OPER-SQL
               ,:PMO-DS-VER
               ,:PMO-DS-TS-INI-CPTZ
               ,:PMO-DS-TS-END-CPTZ
               ,:PMO-DS-UTENTE
               ,:PMO-DS-STATO-ELAB
               ,:PMO-TP-ESTR-CNT
                :IND-PMO-TP-ESTR-CNT
               ,:PMO-COD-RAMO
                :IND-PMO-COD-RAMO
               ,:PMO-GEN-DA-SIN
                :IND-PMO-GEN-DA-SIN
               ,:PMO-COD-TARI
                :IND-PMO-COD-TARI
               ,:PMO-NUM-RAT-PAG-PRE
                :IND-PMO-NUM-RAT-PAG-PRE
               ,:PMO-PC-SERV-VAL
                :IND-PMO-PC-SERV-VAL
               ,:PMO-ETA-AA-SOGL-BNFICR
                :IND-PMO-ETA-AA-SOGL-BNFICR
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B770-CLOSE-CURSOR-IDO-CPZ     THRU B770-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-PMO-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO PMO-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-PMO-TP-MOVI = -1
              MOVE HIGH-VALUES TO PMO-TP-MOVI-NULL
           END-IF
           IF IND-PMO-FRQ-MOVI = -1
              MOVE HIGH-VALUES TO PMO-FRQ-MOVI-NULL
           END-IF
           IF IND-PMO-DUR-AA = -1
              MOVE HIGH-VALUES TO PMO-DUR-AA-NULL
           END-IF
           IF IND-PMO-DUR-MM = -1
              MOVE HIGH-VALUES TO PMO-DUR-MM-NULL
           END-IF
           IF IND-PMO-DUR-GG = -1
              MOVE HIGH-VALUES TO PMO-DUR-GG-NULL
           END-IF
           IF IND-PMO-DT-RICOR-PREC = -1
              MOVE HIGH-VALUES TO PMO-DT-RICOR-PREC-NULL
           END-IF
           IF IND-PMO-DT-RICOR-SUCC = -1
              MOVE HIGH-VALUES TO PMO-DT-RICOR-SUCC-NULL
           END-IF
           IF IND-PMO-PC-INTR-FRAZ = -1
              MOVE HIGH-VALUES TO PMO-PC-INTR-FRAZ-NULL
           END-IF
           IF IND-PMO-IMP-BNS-DA-SCO-TOT = -1
              MOVE HIGH-VALUES TO PMO-IMP-BNS-DA-SCO-TOT-NULL
           END-IF
           IF IND-PMO-IMP-BNS-DA-SCO = -1
              MOVE HIGH-VALUES TO PMO-IMP-BNS-DA-SCO-NULL
           END-IF
           IF IND-PMO-PC-ANTIC-BNS = -1
              MOVE HIGH-VALUES TO PMO-PC-ANTIC-BNS-NULL
           END-IF
           IF IND-PMO-TP-RINN-COLL = -1
              MOVE HIGH-VALUES TO PMO-TP-RINN-COLL-NULL
           END-IF
           IF IND-PMO-TP-RIVAL-PRE = -1
              MOVE HIGH-VALUES TO PMO-TP-RIVAL-PRE-NULL
           END-IF
           IF IND-PMO-TP-RIVAL-PRSTZ = -1
              MOVE HIGH-VALUES TO PMO-TP-RIVAL-PRSTZ-NULL
           END-IF
           IF IND-PMO-FL-EVID-RIVAL = -1
              MOVE HIGH-VALUES TO PMO-FL-EVID-RIVAL-NULL
           END-IF
           IF IND-PMO-ULT-PC-PERD = -1
              MOVE HIGH-VALUES TO PMO-ULT-PC-PERD-NULL
           END-IF
           IF IND-PMO-TOT-AA-GIA-PROR = -1
              MOVE HIGH-VALUES TO PMO-TOT-AA-GIA-PROR-NULL
           END-IF
           IF IND-PMO-TP-OPZ = -1
              MOVE HIGH-VALUES TO PMO-TP-OPZ-NULL
           END-IF
           IF IND-PMO-AA-REN-CER = -1
              MOVE HIGH-VALUES TO PMO-AA-REN-CER-NULL
           END-IF
           IF IND-PMO-PC-REVRSB = -1
              MOVE HIGH-VALUES TO PMO-PC-REVRSB-NULL
           END-IF
           IF IND-PMO-IMP-RISC-PARZ-PRGT = -1
              MOVE HIGH-VALUES TO PMO-IMP-RISC-PARZ-PRGT-NULL
           END-IF
           IF IND-PMO-IMP-LRD-DI-RAT = -1
              MOVE HIGH-VALUES TO PMO-IMP-LRD-DI-RAT-NULL
           END-IF
           IF IND-PMO-IB-OGG = -1
              MOVE HIGH-VALUES TO PMO-IB-OGG-NULL
           END-IF
           IF IND-PMO-COS-ONER = -1
              MOVE HIGH-VALUES TO PMO-COS-ONER-NULL
           END-IF
           IF IND-PMO-SPE-PC = -1
              MOVE HIGH-VALUES TO PMO-SPE-PC-NULL
           END-IF
           IF IND-PMO-FL-ATTIV-GAR = -1
              MOVE HIGH-VALUES TO PMO-FL-ATTIV-GAR-NULL
           END-IF
           IF IND-PMO-CAMBIO-VER-PROD = -1
              MOVE HIGH-VALUES TO PMO-CAMBIO-VER-PROD-NULL
           END-IF
           IF IND-PMO-MM-DIFF = -1
              MOVE HIGH-VALUES TO PMO-MM-DIFF-NULL
           END-IF
           IF IND-PMO-IMP-RAT-MANFEE = -1
              MOVE HIGH-VALUES TO PMO-IMP-RAT-MANFEE-NULL
           END-IF
           IF IND-PMO-DT-ULT-EROG-MANFEE = -1
              MOVE HIGH-VALUES TO PMO-DT-ULT-EROG-MANFEE-NULL
           END-IF
           IF IND-PMO-TP-OGG-RIVAL = -1
              MOVE HIGH-VALUES TO PMO-TP-OGG-RIVAL-NULL
           END-IF
           IF IND-PMO-SOM-ASSTA-GARAC = -1
              MOVE HIGH-VALUES TO PMO-SOM-ASSTA-GARAC-NULL
           END-IF
           IF IND-PMO-PC-APPLZ-OPZ = -1
              MOVE HIGH-VALUES TO PMO-PC-APPLZ-OPZ-NULL
           END-IF
           IF IND-PMO-ID-ADES = -1
              MOVE HIGH-VALUES TO PMO-ID-ADES-NULL
           END-IF
           IF IND-PMO-TP-ESTR-CNT = -1
              MOVE HIGH-VALUES TO PMO-TP-ESTR-CNT-NULL
           END-IF
           IF IND-PMO-COD-RAMO = -1
              MOVE HIGH-VALUES TO PMO-COD-RAMO-NULL
           END-IF
           IF IND-PMO-GEN-DA-SIN = -1
              MOVE HIGH-VALUES TO PMO-GEN-DA-SIN-NULL
           END-IF
           IF IND-PMO-COD-TARI = -1
              MOVE HIGH-VALUES TO PMO-COD-TARI-NULL
           END-IF
           IF IND-PMO-NUM-RAT-PAG-PRE = -1
              MOVE HIGH-VALUES TO PMO-NUM-RAT-PAG-PRE-NULL
           END-IF
           IF IND-PMO-PC-SERV-VAL = -1
              MOVE HIGH-VALUES TO PMO-PC-SERV-VAL-NULL
           END-IF
           IF IND-PMO-ETA-AA-SOGL-BNFICR = -1
              MOVE HIGH-VALUES TO PMO-ETA-AA-SOGL-BNFICR-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO PMO-DS-OPER-SQL
           MOVE 0                   TO PMO-DS-VER
NEWP       INITIALIZE WK-ID-OGG
NEWP       MOVE PMO-ID-POLI
NEWP         TO WK-ID-OGG
NEWP       MOVE WK-ID-OGG(8:2)
NEWP         TO PMO-DS-VER
NEWP       IF PMO-DS-VER = 00
NEWP          MOVE 100
NEWP            TO PMO-DS-VER
NEWP       END-IF
           MOVE IDSV0003-USER-NAME TO PMO-DS-UTENTE
           MOVE '1'                   TO PMO-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO PMO-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO PMO-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF PMO-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-PMO-ID-MOVI-CHIU
           END-IF
           IF PMO-TP-MOVI-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-MOVI
           ELSE
              MOVE 0 TO IND-PMO-TP-MOVI
           END-IF
           IF PMO-FRQ-MOVI-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-FRQ-MOVI
           ELSE
              MOVE 0 TO IND-PMO-FRQ-MOVI
           END-IF
           IF PMO-DUR-AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-DUR-AA
           ELSE
              MOVE 0 TO IND-PMO-DUR-AA
           END-IF
           IF PMO-DUR-MM-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-DUR-MM
           ELSE
              MOVE 0 TO IND-PMO-DUR-MM
           END-IF
           IF PMO-DUR-GG-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-DUR-GG
           ELSE
              MOVE 0 TO IND-PMO-DUR-GG
           END-IF
           IF PMO-DT-RICOR-PREC-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-DT-RICOR-PREC
           ELSE
              MOVE 0 TO IND-PMO-DT-RICOR-PREC
           END-IF
           IF PMO-DT-RICOR-SUCC-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-DT-RICOR-SUCC
           ELSE
              MOVE 0 TO IND-PMO-DT-RICOR-SUCC
           END-IF
           IF PMO-PC-INTR-FRAZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-PC-INTR-FRAZ
           ELSE
              MOVE 0 TO IND-PMO-PC-INTR-FRAZ
           END-IF
           IF PMO-IMP-BNS-DA-SCO-TOT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-IMP-BNS-DA-SCO-TOT
           ELSE
              MOVE 0 TO IND-PMO-IMP-BNS-DA-SCO-TOT
           END-IF
           IF PMO-IMP-BNS-DA-SCO-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-IMP-BNS-DA-SCO
           ELSE
              MOVE 0 TO IND-PMO-IMP-BNS-DA-SCO
           END-IF
           IF PMO-PC-ANTIC-BNS-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-PC-ANTIC-BNS
           ELSE
              MOVE 0 TO IND-PMO-PC-ANTIC-BNS
           END-IF
           IF PMO-TP-RINN-COLL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-RINN-COLL
           ELSE
              MOVE 0 TO IND-PMO-TP-RINN-COLL
           END-IF
           IF PMO-TP-RIVAL-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-RIVAL-PRE
           ELSE
              MOVE 0 TO IND-PMO-TP-RIVAL-PRE
           END-IF
           IF PMO-TP-RIVAL-PRSTZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-RIVAL-PRSTZ
           ELSE
              MOVE 0 TO IND-PMO-TP-RIVAL-PRSTZ
           END-IF
           IF PMO-FL-EVID-RIVAL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-FL-EVID-RIVAL
           ELSE
              MOVE 0 TO IND-PMO-FL-EVID-RIVAL
           END-IF
           IF PMO-ULT-PC-PERD-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-ULT-PC-PERD
           ELSE
              MOVE 0 TO IND-PMO-ULT-PC-PERD
           END-IF
           IF PMO-TOT-AA-GIA-PROR-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TOT-AA-GIA-PROR
           ELSE
              MOVE 0 TO IND-PMO-TOT-AA-GIA-PROR
           END-IF
           IF PMO-TP-OPZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-OPZ
           ELSE
              MOVE 0 TO IND-PMO-TP-OPZ
           END-IF
           IF PMO-AA-REN-CER-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-AA-REN-CER
           ELSE
              MOVE 0 TO IND-PMO-AA-REN-CER
           END-IF
           IF PMO-PC-REVRSB-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-PC-REVRSB
           ELSE
              MOVE 0 TO IND-PMO-PC-REVRSB
           END-IF
           IF PMO-IMP-RISC-PARZ-PRGT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-IMP-RISC-PARZ-PRGT
           ELSE
              MOVE 0 TO IND-PMO-IMP-RISC-PARZ-PRGT
           END-IF
           IF PMO-IMP-LRD-DI-RAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-IMP-LRD-DI-RAT
           ELSE
              MOVE 0 TO IND-PMO-IMP-LRD-DI-RAT
           END-IF
           IF PMO-IB-OGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-IB-OGG
           ELSE
              MOVE 0 TO IND-PMO-IB-OGG
           END-IF
           IF PMO-COS-ONER-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-COS-ONER
           ELSE
              MOVE 0 TO IND-PMO-COS-ONER
           END-IF
           IF PMO-SPE-PC-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-SPE-PC
           ELSE
              MOVE 0 TO IND-PMO-SPE-PC
           END-IF
           IF PMO-FL-ATTIV-GAR-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-FL-ATTIV-GAR
           ELSE
              MOVE 0 TO IND-PMO-FL-ATTIV-GAR
           END-IF
           IF PMO-CAMBIO-VER-PROD-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-CAMBIO-VER-PROD
           ELSE
              MOVE 0 TO IND-PMO-CAMBIO-VER-PROD
           END-IF
           IF PMO-MM-DIFF-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-MM-DIFF
           ELSE
              MOVE 0 TO IND-PMO-MM-DIFF
           END-IF
           IF PMO-IMP-RAT-MANFEE-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-IMP-RAT-MANFEE
           ELSE
              MOVE 0 TO IND-PMO-IMP-RAT-MANFEE
           END-IF
           IF PMO-DT-ULT-EROG-MANFEE-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-DT-ULT-EROG-MANFEE
           ELSE
              MOVE 0 TO IND-PMO-DT-ULT-EROG-MANFEE
           END-IF
           IF PMO-TP-OGG-RIVAL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-OGG-RIVAL
           ELSE
              MOVE 0 TO IND-PMO-TP-OGG-RIVAL
           END-IF
           IF PMO-SOM-ASSTA-GARAC-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-SOM-ASSTA-GARAC
           ELSE
              MOVE 0 TO IND-PMO-SOM-ASSTA-GARAC
           END-IF
           IF PMO-PC-APPLZ-OPZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-PC-APPLZ-OPZ
           ELSE
              MOVE 0 TO IND-PMO-PC-APPLZ-OPZ
           END-IF
           IF PMO-ID-ADES-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-ID-ADES
           ELSE
              MOVE 0 TO IND-PMO-ID-ADES
           END-IF
           IF PMO-TP-ESTR-CNT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-ESTR-CNT
           ELSE
              MOVE 0 TO IND-PMO-TP-ESTR-CNT
           END-IF
           IF PMO-COD-RAMO-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-COD-RAMO
           ELSE
              MOVE 0 TO IND-PMO-COD-RAMO
           END-IF
           IF PMO-GEN-DA-SIN-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-GEN-DA-SIN
           ELSE
              MOVE 0 TO IND-PMO-GEN-DA-SIN
           END-IF
           IF PMO-COD-TARI-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-COD-TARI
           ELSE
              MOVE 0 TO IND-PMO-COD-TARI
           END-IF
           IF PMO-NUM-RAT-PAG-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-NUM-RAT-PAG-PRE
           ELSE
              MOVE 0 TO IND-PMO-NUM-RAT-PAG-PRE
           END-IF
           IF PMO-PC-SERV-VAL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-PC-SERV-VAL
           ELSE
              MOVE 0 TO IND-PMO-PC-SERV-VAL
           END-IF
           IF PMO-ETA-AA-SOGL-BNFICR-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-ETA-AA-SOGL-BNFICR
           ELSE
              MOVE 0 TO IND-PMO-ETA-AA-SOGL-BNFICR
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : PMO-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE PARAM-MOVI TO WS-BUFFER-TABLE.

           MOVE PMO-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO PMO-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO PMO-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO PMO-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO PMO-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO PMO-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO PMO-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO PMO-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.

           MOVE PARAM-MOVI TO WS-BUFFER-TABLE.

           MOVE PMO-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.

       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO PARAM-MOVI.

           MOVE WS-ID-MOVI-CRZ  TO PMO-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO PMO-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO PMO-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO PMO-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO PMO-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO PMO-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO PMO-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE PMO-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO PMO-DT-INI-EFF-DB
           MOVE PMO-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO PMO-DT-END-EFF-DB
           IF IND-PMO-DT-RICOR-PREC = 0
               MOVE PMO-DT-RICOR-PREC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PMO-DT-RICOR-PREC-DB
           END-IF
           IF IND-PMO-DT-RICOR-SUCC = 0
               MOVE PMO-DT-RICOR-SUCC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PMO-DT-RICOR-SUCC-DB
           END-IF
           IF IND-PMO-DT-ULT-EROG-MANFEE = 0
               MOVE PMO-DT-ULT-EROG-MANFEE TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PMO-DT-ULT-EROG-MANFEE-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE PMO-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO PMO-DT-INI-EFF
           MOVE PMO-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO PMO-DT-END-EFF
           IF IND-PMO-DT-RICOR-PREC = 0
               MOVE PMO-DT-RICOR-PREC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PMO-DT-RICOR-PREC
           END-IF
           IF IND-PMO-DT-RICOR-SUCC = 0
               MOVE PMO-DT-RICOR-SUCC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PMO-DT-RICOR-SUCC
           END-IF
           IF IND-PMO-DT-ULT-EROG-MANFEE = 0
               MOVE PMO-DT-ULT-EROG-MANFEE-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PMO-DT-ULT-EROG-MANFEE
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0101.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0007
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0101'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
       01  WK-PRESTITO                      PIC X(02).
           88  PREST-ATTIVO              VALUE 'SI'.
           88  PREST-NON-ATTIVO          VALUE 'NO'.
       01  WK-APPO-DATA                     PIC 9(008) VALUE ZEROES.
       01  WK-IMPO-DIFF                     PIC S9(11)V9(07).
      *---------------------------------------------------------------*
      *  COPY PER CHIAMATA LDBS1780
      *---------------------------------------------------------------*
           COPY LDBVB441.
           COPY LDBVB471.
      *---------------------------------------------------------------*
10819      COPY LCCVXMVZ.
10819      COPY LCCVXMV2.
10819      COPY LCCVXMV5.
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-ADE.
          03 DADE-AREA-ADES.
             04 DADE-ELE-ADES-MAX        PIC S9(04) COMP.
             04 DADE-TAB-ADES.
             COPY LCCVADE1               REPLACING ==(SF)== BY ==DADE==.

      *----------------------------------------------------------------*
      *--> AREA TABELLE
      *----------------------------------------------------------------*
      *  TABELLA PRESTITO
         COPY IDBVPRE1.
      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
           03 IX-TAB-ADE                     PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0007.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0007.
      *----------------------------------------------------------------*
      *
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.
      *
           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000
      *
           GOBACK.
      *
      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.

           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           INITIALIZE LDBVB441.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE AREA-IO-ADE.

      *
      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1110-LEGGI-PRESTITO  THRU  S1110-EX
               IF  PREST-ATTIVO
                   PERFORM S1255-CALCOLA-DATA  THRU S1255-EX
                   IF IDSV0003-NOT-FOUND
      * SE NON TROVATO IMPOSTO IL VALORE DEGLI INTERESSI CALCOLATO ALLA
      * CONCESSIONE DEL PRESTITO, DIFFERENZA FRA CONCESSO E LIQUIDATO
                   AND PRE-IMP-PREST       IS NUMERIC
                   AND PRE-IMP-PREST-LIQTO IS NUMERIC
                      MOVE ZEROES TO WK-IMPO-DIFF
                      COMPUTE WK-IMPO-DIFF =
                              PRE-IMP-PREST - PRE-IMP-PREST-LIQTO
                      MOVE WK-IMPO-DIFF     TO IVVC0213-VAL-IMP-O
                   ELSE
                      CONTINUE
                   END-IF
               ELSE
                   MOVE  ZERO               TO IVVC0213-VAL-IMP-O
               END-IF
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-ADES
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DADE-AREA-ADES
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA DELLA TABELLA PRESTITI CON DATA DECORRENZA PRESTITO
      *    MINORE
      *----------------------------------------------------------------*
       S1110-LEGGI-PRESTITO.

           SET PREST-NON-ATTIVO       TO TRUE

           INITIALIZE                     PREST.
           SET  IDSV0003-TRATT-X-EFFETTO TO TRUE.
           SET  IDSV0003-WHERE-CONDITION  TO TRUE.
           SET  IDSV0003-SELECT        TO TRUE
           MOVE 'AD'                   TO PRE-TP-OGG
           MOVE DADE-ID-ADES           TO PRE-ID-OGG
           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                       TO PRE-COD-COMP-ANIA
           MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WK-APPO-DATA
           MOVE 99991230               TO IDSV0003-DATA-INIZIO-EFFETTO
      *
           MOVE 'LDBS6160'                     TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 PREST
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS6160 ERRORE CHIAMATA '
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
           MOVE  WK-APPO-DATA          TO IDSV0003-DATA-INIZIO-EFFETTO

           IF IDSV0003-SUCCESSFUL-RC
              EVALUATE TRUE
                WHEN IDSV0003-SUCCESSFUL-SQL
                   SET PREST-ATTIVO      TO  TRUE
                WHEN IDSV0003-NOT-FOUND
                   CONTINUE
                WHEN OTHER
                   SET IDSV0003-INVALID-OPER            TO TRUE
                   MOVE WK-CALL-PGM        TO IDSV0003-COD-SERVIZIO-BE
                   STRING 'CHIAMATA LDBS6160 ;'
                      IDSV0003-RETURN-CODE ';'
                      IDSV0003-SQLCODE
                      DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                   END-STRING
              END-EVALUATE
           ELSE
              SET IDSV0003-INVALID-OPER            TO TRUE
              MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
              STRING 'ERRORE CHIAMATA LDBS6160'   ';'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                   DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           END-IF.

       S1110-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA
      *----------------------------------------------------------------*
       S1255-CALCOLA-DATA.
      *
ALFR       SET IDSV0003-SELECT             TO TRUE
 "         SET IDSV0003-WHERE-CONDITION    TO TRUE
 "         SET IDSV0003-TRATT-X-COMPETENZA TO TRUE

           INITIALIZE LDBVB441.
      *TEST SOLO INTERESSI DI PRESTITO
      *    MOVE 'PR'                           TO LDBVB441-TP-TIT-01.
           MOVE SPACES                         TO LDBVB441-TP-TIT-01.
           MOVE 'IP'                           TO LDBVB441-TP-TIT-02.
           MOVE DADE-ID-POLI                   TO LDBVB441-ID-OGG.
           MOVE 'PO'                           TO LDBVB441-TP-OGG.

SBF        MOVE IVVC0213-TIPO-MOVI-ORIG        TO WS-MOVIMENTO
           IF LIQUI-RISPAR-POLIND OR
              LIQUI-RISPAR-ADE OR
              COMUN-RISPAR-TRA OR
              COMUN-RISPAR-IND OR
              LIQUI-RISPAR-TRA OR
              LIQUI-RISTOT-IND OR
              LIQUI-RISTOT-TRA OR
              LIQUI-TRASF-INDIVI OR
              SCANT-INDIVI OR
              LIQUI-SININD OR
              COLIQ-SCAIND OR
              LIQUI-RISPOL OR
              LIQUI-SCAPOL OR
              SWITCH-OUT OR
              LIQUI-RISPAR-RPA OR
              LIQUI-SINISTRO-TERM-FISSO-SCAD OR
10819         RPP-TAKE-PROFIT OR
10819         LIQUI-RPP-TAKE-PROFIT
10819X      OR LIQUI-RISTOT-INCAPIENZA
10819X      OR RPP-REDDITO-PROGRAMMATO
10819       OR LIQUI-RPP-BENEFICIO-CONTR
FNZS2       OR LIQUI-RISTOT-INCAP
              MOVE 'IN'                        TO LDBVB441-TP-STAT-TIT-1
              MOVE SPACES                      TO LDBVB441-TP-STAT-TIT-2
              MOVE SPACES                      TO LDBVB441-TP-STAT-TIT-3
           ELSE
              MOVE 'IN'                        TO LDBVB441-TP-STAT-TIT-1
              MOVE 'SB'                        TO LDBVB441-TP-STAT-TIT-2
              MOVE SPACES                      TO LDBVB441-TP-STAT-TIT-3
SBF        END-IF
      *
           MOVE 'LDBSB440'                     TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 LDBVB441
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBSB440 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM S1260-CALCOLA-IMPORTO      THRU S1260-EX
           ELSE
              IF IDSV0003-NOT-FOUND
              OR IDSV0003-SQLCODE = -305
      *TEST     SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                 CONTINUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF
      *
              MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA LDBSB440 ;'
                  IDSV0003-RETURN-CODE ';'
                  IDSV0003-SQLCODE
                  DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
      *
           END-IF.
      *
       S1255-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA
      *----------------------------------------------------------------*
       S1260-CALCOLA-IMPORTO.
      *
ALFR       SET IDSV0003-SELECT             TO TRUE
 "         SET IDSV0003-WHERE-CONDITION    TO TRUE
 "         SET IDSV0003-TRATT-X-COMPETENZA TO TRUE
      *
           INITIALIZE LDBVB471.
      *
      *TEST SOLO INTERESSI DI PRESTITO
      *    MOVE 'PR'                           TO LDBVB471-TP-TIT-01.
           MOVE SPACES                         TO LDBVB471-TP-TIT-01.
           MOVE 'IP'                           TO LDBVB471-TP-TIT-02.
           MOVE LDBVB441-DT-MAX                TO LDBVB471-DT-MAX.
           MOVE DADE-ID-POLI                   TO LDBVB471-ID-OGG.
           MOVE 'PO'                           TO LDBVB471-TP-OGG.

SBF        MOVE IVVC0213-TIPO-MOVI-ORIG        TO WS-MOVIMENTO
           IF LIQUI-RISPAR-POLIND OR
              LIQUI-RISPAR-ADE OR
              COMUN-RISPAR-TRA OR
              COMUN-RISPAR-IND OR
              LIQUI-RISPAR-TRA OR
              LIQUI-RISTOT-IND OR
              LIQUI-RISTOT-TRA OR
              LIQUI-TRASF-INDIVI OR
              SCANT-INDIVI OR
              LIQUI-SININD OR
              COLIQ-SCAIND OR
              LIQUI-RISPOL OR
              LIQUI-SCAPOL OR
              SWITCH-OUT OR
              LIQUI-RISPAR-RPA OR
              LIQUI-SINISTRO-TERM-FISSO-SCAD OR
10819         RPP-TAKE-PROFIT OR
10819         LIQUI-RPP-TAKE-PROFIT
10819X     OR LIQUI-RISTOT-INCAPIENZA
10819X     OR RPP-REDDITO-PROGRAMMATO
10819      OR LIQUI-RPP-BENEFICIO-CONTR
FNZS2      OR LIQUI-RISTOT-INCAP
              MOVE 'IN'                        TO LDBVB471-TP-STAT-TIT-1
              MOVE SPACES                      TO LDBVB471-TP-STAT-TIT-2
              MOVE SPACES                      TO LDBVB471-TP-STAT-TIT-3
           ELSE
              MOVE 'IN'                        TO LDBVB471-TP-STAT-TIT-1
              MOVE 'SB'                        TO LDBVB471-TP-STAT-TIT-2
              MOVE SPACES                      TO LDBVB471-TP-STAT-TIT-3
SBF        END-IF
      *
           MOVE 'LDBSB470'                     TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 LDBVB471
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBSB470 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
22812         IF LDBVB471-TOT-INT-PRE-NULL = HIGH-VALUE
22812            MOVE 0                          TO IVVC0213-VAL-IMP-O
              ELSE
                 MOVE LDBVB471-TOT-INT-PRE       TO IVVC0213-VAL-IMP-O
              END-IF
           ELSE
              IF IDSV0003-NOT-FOUND
              OR IDSV0003-SQLCODE = -305
      *TEST      SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                 CONTINUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF
      *
              MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA LDBSB470 ;'
                  IDSV0003-RETURN-CODE ';'
                  IDSV0003-SQLCODE
                  DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
      *
           END-IF.
      *
       S1260-EX.
           EXIT.


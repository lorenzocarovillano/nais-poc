      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0089.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0089
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0089'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-DATA-OUTPUT                  PIC 9(04)V9(07).
       01  WK-DATA-OUTPUT-V REDEFINES WK-DATA-OUTPUT.
           03 WK-NUMERICI                  PIC 9(04).
           03 WK-DECIMALI                  PIC 9(07).
       01  WK-DATA-INFERIORE               PIC 9(08).

       01 WK-DATA-X-12.
          03 WK-X-AA                          PIC X(04).
          03 WK-VIROGLA                       PIC X(01) VALUE ','.
          03 WK-X-GG                          PIC X(07).

       01  WK-DS-TS-CPTZ           PIC S9(18)     COMP-3.
      *-- AREA ROUTINE PER IL CALCOLO
       01  FORMATO                 PIC X(01).
       01  DATA-INFERIORE.
           05 AAAA-INF             PIC 9(04).
           05 MM-INF               PIC 9(02).
           05 GG-INF               PIC 9(02).
       01  DATA-SUPERIORE.
           05 AAAA-SUP             PIC 9(04).
           05 MM-SUP               PIC 9(02).
           05 GG-SUP               PIC 9(02).
       01  GG-DIFF                 PIC 9(05).
       01  CODICE-RITORNO          PIC X(01).

       01  WK-FIND-LETTO                     PIC X(001).
           88 WK-LETTO-SI                    VALUE 'S'.
           88 WK-LETTO-NO                    VALUE 'N'.

       01  FLAG-CUR-MOV                     PIC X(001).
           88 INIT-CUR-MOV                            VALUE 'S'.
           88 FINE-CUR-MOV                            VALUE 'N'.

       01  FLAG-MOV-TROV                    PIC X(01).
           88 MOV-TROV-SI                             VALUE 'S'.
           88 MOV-TROV-NO                             VALUE 'N'.

       01  WS-TIPO-MOV                          PIC X(01).
           88 LIQUIDAZIONE                            VALUE 'L'.
           88 COMUNICAZIONE                           VALUE 'C'.

       01 WS-TAB-MAX                            PIC 9(03) VALUE 50.

       01 WS-TAB-LIQUI-COMUN.
          05 FILLER                             PIC X(05) VALUE '05010'.
          05 FILLER                             PIC X(05) VALUE '03012'.
NEW       05 FILLER                             PIC X(05) VALUE '05006'.
NEW       05 FILLER                             PIC X(05) VALUE '05005'.
NEW       05 FILLER                             PIC X(05) VALUE '05002'.
NEW       05 FILLER                             PIC X(05) VALUE '03009'.
NEW       05 FILLER                             PIC X(05) VALUE '05018'.
NEW       05 FILLER                             PIC X(05) VALUE '03019'.
NEW       05 FILLER                             PIC X(05) VALUE '05026'.
NEW       05 FILLER                             PIC X(05) VALUE '06009'.
NEW       05 FILLER                             PIC X(05) VALUE '05015'.
NEW       05 FILLER                             PIC X(05) VALUE '03017'.
10819     05 FILLER                             PIC X(05) VALUE '02319'.
10819     05 FILLER                             PIC X(05) VALUE '02318'.
10819X    05 FILLER                             PIC X(05) VALUE '02325'.
10819X    05 FILLER                             PIC X(05) VALUE '02324'.
10819X    05 FILLER                             PIC X(05) VALUE '02317'.
10819X    05 FILLER                             PIC X(05) VALUE '02316'.
10819     05 FILLER                             PIC X(05) VALUE '02323'.
10819     05 FILLER                             PIC X(05) VALUE '02322'.
10819X    05 FILLER                             PIC X(400).
       01 WS-TAB-LIQUI-COMUN-R REDEFINES WS-TAB-LIQUI-COMUN.
          05 WS-EL-TAB-LIQUI-COMUN  OCCURS 50.
             10 WS-MOVI-LIQUID                  PIC 9(05).
             10 WS-MOVI-COMUN                   PIC 9(05).

       01 WS-TRCH-POS.
          05 WS-TP-TRCH-POS-1                    PIC X(02) VALUE '1'.
          05 WS-TP-TRCH-POS-2                    PIC X(02) VALUE '2'.
          05 WS-TP-TRCH-POS-3                    PIC X(02) VALUE '3'.
          05 WS-TP-TRCH-POS-4                    PIC X(02) VALUE '4'.
          05 WS-TP-TRCH-POS-5                    PIC X(02) VALUE '5'.
          05 WS-TP-TRCH-POS-6                    PIC X(02) VALUE '6'.
          05 WS-TP-TRCH-POS-7                    PIC X(02) VALUE '7'.
          05 WS-TP-TRCH-POS-8                    PIC X(02) VALUE '8'.
          05 WS-TP-TRCH-POS-16                   PIC X(02) VALUE '16'.
          05 WS-TP-TRCH-POS-17                   PIC X(02) VALUE '17'.

       01 WS-TRCH-NEG.
          05 WS-TP-TRCH-NEG-9                    PIC X(02) VALUE '9'.
          05 WS-TP-TRCH-NEG-10                   PIC X(02) VALUE '10'.
          05 WS-TP-TRCH-NEG-11                   PIC X(02) VALUE '11'.
          05 WS-TP-TRCH-NEG-13                   PIC X(02) VALUE '13'.
          05 WS-TP-TRCH-NEG-14                   PIC X(02) VALUE '14'.
          05 WS-TP-TRCH-NEG-15                   PIC X(02) VALUE '15'.
          05 FILLER                              PIC X(08) VALUE SPACES.

       01 WS-IMPB-VIS-END2000                    PIC S9(12)V9(3).

       01 WK-DATA-EFF-RIP                        PIC S9(08) COMP-3.
       01 WK-DATA-CPTZ-RIP                       PIC S9(18) COMP-3.
      *---------------------------------------------------------------
      *  COPY PER CHIAMATA LDBS2890
      *---------------------------------------------------------------
           COPY LDBV2891.

      *---------------------------------------------------------------
      *  COPY PER CHIAMATA LDBSE260
      *---------------------------------------------------------------
           COPY LDBVE261.
      *---------------------------------------------------------------
      *  COPY PER CHIAMATA LDBSE250
      *---------------------------------------------------------------
           COPY LDBVE251.

           COPY LCCVXMV0.
           COPY IDBVMOV1.
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-POL.
          03 DPOL-AREA-POLIZZA.
             04 DPOL-ELE-POLI-MAX       PIC S9(04) COMP.
             04 DPOL-TAB-POLI.
             COPY LCCVPOL1              REPLACING ==(SF)== BY ==DPOL==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
           03 IX-TAB-TGA                     PIC S9(04) COMP.
           03 IX-TAB                         PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0007.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0007.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

      *    INITIALIZE LDBV2891.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
             TO WK-DATA-EFF-RIP

           MOVE IDSV0003-DATA-COMPETENZA
             TO WK-DATA-CPTZ-RIP.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE WK-DATA-OUTPUT
                      WK-DATA-X-12.


      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
      *    PERFORM S1100-VALORIZZA-DCLGEN
      *       THRU S1100-VALORIZZA-DCLGEN-EX
      *    VARYING IX-DCLGEN FROM 1 BY 1
      *      UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
      *         OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
      *            SPACES OR LOW-VALUE OR HIGH-VALUE.

ROB        MOVE ZEROES    TO IVVC0213-VAL-IMP-O

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL

ROB   *        PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
      *           UNTIL IX-TAB-TGA > DTGA-ELE-TRCH-MAX
      *
      *           IF DTGA-IMPB-VIS-END2000-NULL(IX-TAB-TGA) =
      *                       HIGH-VALUE OR LOW-VALUE OR SPACES
      *              CONTINUE
      *           ELSE
      *              ADD DTGA-IMPB-VIS-END2000(IX-TAB-TGA)
      *               TO IVVC0213-VAL-IMP-O
      *           END-IF
      *
ROB   *        END-PERFORM


      *--> ROUTINE PER LA RICERCA DEL MOVIMENTO ORIGINARIO
      *
           SET  MOV-TROV-NO           TO TRUE

           IF  IVVC0213-TIPO-MOVI-ORIG IS NUMERIC
           AND IVVC0213-TIPO-MOVI-ORIG > ZERO
               PERFORM S1200-RICERCA-MOVI-ORIG
                  THRU S1200-RICERCA-MOVI-ORIG-EX
               VARYING IX-TAB    FROM 1 BY 1
                 UNTIL IX-TAB    > WS-TAB-MAX
                    OR WS-MOVI-LIQUID(IX-TAB) = 0
                    OR MOV-TROV-SI
               SUBTRACT  1  FROM  IX-TAB
           END-IF.

           IF MOV-TROV-SI
              IF LIQUIDAZIONE
                 MOVE WS-MOVI-LIQUID(IX-TAB)  TO WS-MOVIMENTO
      *--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
                 PERFORM RECUP-MOVI-COMUN
                    THRU RECUP-MOVI-COMUN-EX
              ELSE
                 MOVE WS-MOVI-COMUN(IX-TAB)     TO WS-MOVIMENTO
                 MOVE IDSV0003-DATA-COMPETENZA TO WK-DS-TS-CPTZ
ROB   *        MOVE IVVC0213-ID-POLIZZA        TO LDBV2891-ID-POL
ROB   *        PERFORM S1250-CALCOLA-DATA1     THRU S1250-EX
              END-IF
              IF  IDSV0003-SUCCESSFUL-RC
                  PERFORM S1303-LEGGI-TRCH-POS THRU S1303-EX
              END-IF

      *--> LETTURA TRCH NEGATIVE
              IF  IDSV0003-SUCCESSFUL-RC
              AND IDSV0003-SUCCESSFUL-SQL
                  PERFORM S1304-LEGGI-TRCH-NEG THRU S1304-EX
              END-IF

           ELSE
              MOVE ZERO                       TO IVVC0213-VAL-IMP-O
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-POLI
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DPOL-AREA-POLIZZA
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER LA RICERCA DEL MOVIMENTO ORIGINARIO
      *    NELLA TABELLA DEI MOVIMENTI GESTITI
      *----------------------------------------------------------------*
       S1200-RICERCA-MOVI-ORIG.

           IF WS-MOVI-LIQUID(IX-TAB) = IVVC0213-TIPO-MOVI-ORIG
              SET  LIQUIDAZIONE TO TRUE
              SET  MOV-TROV-SI  TO TRUE
           END-IF.

           IF WS-MOVI-COMUN(IX-TAB) = IVVC0213-TIPO-MOVI-ORIG
              SET  COMUNICAZIONE TO TRUE
              SET  MOV-TROV-SI  TO TRUE
           END-IF.

       S1200-RICERCA-MOVI-ORIG-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    Recupero il movimento di comunicazione
      *----------------------------------------------------------------*
       RECUP-MOVI-COMUN.

           SET IDSV0003-FETCH-FIRST           TO TRUE
           SET IDSV0003-SUCCESSFUL-RC         TO TRUE
           SET IDSV0003-SUCCESSFUL-SQL        TO TRUE

           SET INIT-CUR-MOV                   TO TRUE

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR FINE-CUR-MOV

              INITIALIZE MOVI

              MOVE IVVC0213-ID-POLIZZA            TO MOV-ID-OGG
              MOVE 'PO'                           TO MOV-TP-OGG
              MOVE IDSV0003-DATA-INIZIO-EFFETTO   TO MOV-DT-EFF

              SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE

      *--> LIVELLO OPERAZIONE
              SET IDSV0003-WHERE-CONDITION       TO TRUE
      *--> INIZIALIZZA CODICE DI RITORNO
              SET IDSV0003-SUCCESSFUL-RC         TO TRUE
              SET IDSV0003-SUCCESSFUL-SQL        TO TRUE

              MOVE 'LDBS8850'            TO WK-CALL-PGM
              CALL WK-CALL-PGM   USING  IDSV0003 MOVI

              ON EXCEPTION
                 MOVE 'LDBS8850'
                   TO IDSV0003-COD-SERVIZIO-BE
                MOVE 'CALL-LDBS6040 ERRORE CHIAMATA '
                   TO IDSV0003-DESCRIZ-ERR-DB2
                   SET IDSV0003-INVALID-OPER  TO TRUE
              END-CALL

              IF IDSV0003-SUCCESSFUL-RC
                EVALUATE TRUE

                    WHEN IDSV0003-NOT-FOUND
      *          COMUNICAZIONE E LIQUIDAZIONE CONTESTUALE
                      MOVE IDSV0003-DATA-COMPETENZA TO WK-DS-TS-CPTZ
                      SET FINE-CUR-MOV TO TRUE

                    WHEN IDSV0003-SUCCESSFUL-SQL
                      MOVE MOV-TP-MOVI      TO WS-MOVIMENTO
                      MOVE MOV-DS-TS-CPTZ   TO WK-DS-TS-CPTZ
                      IF WS-MOVI-COMUN(IX-TAB) = MOV-TP-MOVI
                      AND MOV-DT-EFF = IDSV0003-DATA-INIZIO-EFFETTO
                         SET FINE-CUR-MOV   TO TRUE
                      END-IF
                      IF FINE-CUR-MOV
                         PERFORM CLOSE-MOVI
                            THRU CLOSE-MOVI-EX
                      ELSE
                         SET IDSV0003-FETCH-NEXT   TO TRUE
                      END-IF

                    WHEN OTHER
                        SET IDSV0003-INVALID-OPER  TO TRUE
                        MOVE WK-CALL-PGM
                          TO IDSV0003-COD-SERVIZIO-BE
                        STRING 'ERRORE RECUP MOVI COMUN ;'
                               IDSV0003-RETURN-CODE ';'
                               IDSV0003-SQLCODE
                               DELIMITED BY SIZE INTO
                               IDSV0003-DESCRIZ-ERR-DB2
                        END-STRING

                END-EVALUATE
              ELSE
                MOVE WK-CALL-PGM      TO IDSV0003-COD-SERVIZIO-BE
                STRING 'CHIAMATA LDBS8850 ;'
                       IDSV0003-RETURN-CODE ';'
                       IDSV0003-SQLCODE
                   DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                END-STRING
                IF IDSV0003-NOT-FOUND
                OR IDSV0003-SQLCODE = -305
                   SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                ELSE
                   SET IDSV0003-INVALID-OPER            TO TRUE
                END-IF
              END-IF
           END-PERFORM.

       RECUP-MOVI-COMUN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CHIUDO IL CURSORE SULLA TABELLA MOVIMENTO LDBS6040
      *----------------------------------------------------------------*
       CLOSE-MOVI.

           SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE

           MOVE SPACES                     TO IDSV0003-BUFFER-WHERE-COND
      *
           SET IDSV0003-CLOSE-CURSOR       TO TRUE.
           SET IDSV0003-WHERE-CONDITION    TO TRUE.
      *
           SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.

           MOVE 'LDBS8850'            TO WK-CALL-PGM
           CALL WK-CALL-PGM    USING  IDSV0003 MOVI

           ON EXCEPTION
              MOVE 'LDBS6040'
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS6040 ERRORE CHIAMATA '
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL

           IF IDSV0003-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSV0003-SUCCESSFUL-SQL
                      CONTINUE
                  WHEN OTHER
                      MOVE WK-CALL-PGM       TO IDSV0003-COD-SERVIZIO-BE
                      STRING 'CHIAMATA LDBS6040 ;'
                             IDSV0003-RETURN-CODE ';'
                             IDSV0003-SQLCODE
                         DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                      END-STRING
                      IF IDSV0003-NOT-FOUND
                      OR IDSV0003-SQLCODE = -305
                         SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                      ELSE
                         SET IDSV0003-INVALID-OPER            TO TRUE
                      END-IF
              END-EVALUATE
           ELSE
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA LDBS6040 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
              OR IDSV0003-SQLCODE = -305
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF

           END-IF.

       CLOSE-MOVI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA DATA 1
      *----------------------------------------------------------------*
       S1250-CALCOLA-DATA1.
      *
           MOVE 'LDBS2890'                     TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 LDBV2891
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS2890 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE LDBV2891-IMPB-VIS-END2000      TO IVVC0213-VAL-IMP-O
           ELSE
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA LDBS2890 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF
           END-IF.
      *
       S1250-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1303-LEGGI-TRCH-POS.
      *
           SET IDSV0003-SELECT           TO TRUE
           SET IDSV0003-WHERE-CONDITION  TO TRUE
           SET IDSV0003-TRATT-X-COMPETENZA TO TRUE
      *
           INITIALIZE LDBVE261.
      *
           MOVE WK-DS-TS-CPTZ         TO IDSV0003-DATA-COMPETENZA
           MOVE IVVC0213-ID-ADESIONE     TO LDBVE261-ID-ADES.
           MOVE WS-TRCH-POS              TO LDBVE261-TP-TRCH.
           MOVE 'LDBSE260'               TO WK-CALL-PGM

           CALL WK-CALL-PGM  USING  IDSV0003 LDBVE261
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBSE260 ERRORE CHIAMATA '
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
      *
           IF IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
              IF LDBVE261-IMPB-VIS-END2000 IS NUMERIC
                 MOVE LDBVE261-IMPB-VIS-END2000
                   TO WS-IMPB-VIS-END2000
              END-IF
           ELSE
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA LDBSE260 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
              OR IDSV0003-SQLCODE = -305
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF
           END-IF.

       S1303-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1304-LEGGI-TRCH-NEG.
      *
           SET IDSV0003-SELECT           TO TRUE
           SET IDSV0003-WHERE-CONDITION  TO TRUE
           SET IDSV0003-TRATT-X-COMPETENZA TO TRUE
      *
           INITIALIZE LDBVE261.
      *
           MOVE WK-DS-TS-CPTZ         TO IDSV0003-DATA-COMPETENZA
           MOVE IVVC0213-ID-ADESIONE     TO LDBVE261-ID-ADES.
           MOVE WS-TRCH-NEG              TO LDBVE261-TP-TRCH.
           MOVE 'LDBSE260'               TO WK-CALL-PGM

           CALL WK-CALL-PGM  USING  IDSV0003 LDBVE261
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBSE260 ERRORE CHIAMATA '
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
      *
           IF IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
              IF LDBVE261-IMPB-VIS-END2000 IS NUMERIC
                 COMPUTE IVVC0213-VAL-IMP-O = WS-IMPB-VIS-END2000 -
                                              LDBVE261-IMPB-VIS-END2000
              END-IF
           ELSE
              IF IDSV0003-NOT-FOUND
              OR IDSV0003-SQLCODE = -305
                 SET IDSV0003-SUCCESSFUL-RC  TO TRUE
                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                 MOVE WS-IMPB-VIS-END2000    TO IVVC0213-VAL-IMP-O
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
                 MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
                 STRING 'CHIAMATA LDBSE260 ;'
                        IDSV0003-RETURN-CODE ';'
                        IDSV0003-SQLCODE
                    DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                 END-STRING
              END-IF
           END-IF.
      *
       S1304-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           MOVE WK-DATA-EFF-RIP
             TO IDSV0003-DATA-INIZIO-EFFETTO

           MOVE WK-DATA-CPTZ-RIP
             TO IDSV0003-DATA-COMPETENZA

           GOBACK.
      *
       EX-S9000.
           EXIT.


       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSPCO0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  13 NOV 2018.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDPCO0 END-EXEC.
           EXEC SQL INCLUDE IDBVPCO2 END-EXEC.
           EXEC SQL INCLUDE IDBVPCO3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVPCO1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 PARAM-COMP.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSPCO0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'PARAM_COMP' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                COD_COMP_ANIA
                ,COD_TRAT_CIRT
                ,LIM_VLTR
                ,TP_RAT_PERF
                ,TP_LIV_GENZ_TIT
                ,ARROT_PRE
                ,DT_CONT
                ,DT_ULT_RIVAL_IN
                ,DT_ULT_QTZO_IN
                ,DT_ULT_RICL_RIASS
                ,DT_ULT_TABUL_RIASS
                ,DT_ULT_BOLL_EMES
                ,DT_ULT_BOLL_STOR
                ,DT_ULT_BOLL_LIQ
                ,DT_ULT_BOLL_RIAT
                ,DT_ULTELRISCPAR_PR
                ,DT_ULTC_IS_IN
                ,DT_ULT_RICL_PRE
                ,DT_ULTC_MARSOL
                ,DT_ULTC_RB_IN
                ,PC_PROV_1AA_ACQ
                ,MOD_INTR_PREST
                ,GG_MAX_REC_PROV
                ,DT_ULTGZ_TRCH_E_IN
                ,DT_ULT_BOLL_SNDEN
                ,DT_ULT_BOLL_SNDNLQ
                ,DT_ULTSC_ELAB_IN
                ,DT_ULTSC_OPZ_IN
                ,DT_ULTC_BNSRIC_IN
                ,DT_ULTC_BNSFDT_IN
                ,DT_ULT_RINN_GARAC
                ,DT_ULTGZ_CED
                ,DT_ULT_ELAB_PRLCOS
                ,DT_ULT_RINN_COLL
                ,FL_RVC_PERF
                ,FL_RCS_POLI_NOPERF
                ,FL_GEST_PLUSV
                ,DT_ULT_RIVAL_CL
                ,DT_ULT_QTZO_CL
                ,DT_ULTC_BNSRIC_CL
                ,DT_ULTC_BNSFDT_CL
                ,DT_ULTC_IS_CL
                ,DT_ULTC_RB_CL
                ,DT_ULTGZ_TRCH_E_CL
                ,DT_ULTSC_ELAB_CL
                ,DT_ULTSC_OPZ_CL
                ,STST_X_REGIONE
                ,DT_ULTGZ_CED_COLL
                ,TP_MOD_RIVAL
                ,NUM_MM_CALC_MORA
                ,DT_ULT_EC_RIV_COLL
                ,DT_ULT_EC_RIV_IND
                ,DT_ULT_EC_IL_COLL
                ,DT_ULT_EC_IL_IND
                ,DT_ULT_EC_UL_COLL
                ,DT_ULT_EC_UL_IND
                ,AA_UTI
                ,CALC_RSH_COMUN
                ,FL_LIV_DEBUG
                ,DT_ULT_BOLL_PERF_C
                ,DT_ULT_BOLL_RSP_IN
                ,DT_ULT_BOLL_RSP_CL
                ,DT_ULT_BOLL_EMES_I
                ,DT_ULT_BOLL_STOR_I
                ,DT_ULT_BOLL_RIAT_I
                ,DT_ULT_BOLL_SD_I
                ,DT_ULT_BOLL_SDNL_I
                ,DT_ULT_BOLL_PERF_I
                ,DT_RICL_RIRIAS_COM
                ,DT_ULT_ELAB_AT92_C
                ,DT_ULT_ELAB_AT92_I
                ,DT_ULT_ELAB_AT93_C
                ,DT_ULT_ELAB_AT93_I
                ,DT_ULT_ELAB_SPE_IN
                ,DT_ULT_ELAB_PR_CON
                ,DT_ULT_BOLL_RP_CL
                ,DT_ULT_BOLL_RP_IN
                ,DT_ULT_BOLL_PRE_I
                ,DT_ULT_BOLL_PRE_C
                ,DT_ULTC_PILDI_MM_C
                ,DT_ULTC_PILDI_AA_C
                ,DT_ULTC_PILDI_MM_I
                ,DT_ULTC_PILDI_TR_I
                ,DT_ULTC_PILDI_AA_I
                ,DT_ULT_BOLL_QUIE_C
                ,DT_ULT_BOLL_QUIE_I
                ,DT_ULT_BOLL_COTR_I
                ,DT_ULT_BOLL_COTR_C
                ,DT_ULT_BOLL_CORI_C
                ,DT_ULT_BOLL_CORI_I
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TP_VALZZ_DT_VLT
                ,FL_FRAZ_PROV_ACQ
                ,DT_ULT_AGG_EROG_RE
                ,PC_RM_MARSOL
                ,PC_C_SUBRSH_MARSOL
                ,COD_COMP_ISVAP
                ,LM_RIS_CON_INT
                ,LM_C_SUBRSH_CON_IN
                ,PC_GAR_NORISK_MARS
                ,CRZ_1A_RAT_INTR_PR
                ,NUM_GG_ARR_INTR_PR
                ,FL_VISUAL_VINPG
                ,DT_ULT_ESTRAZ_FUG
                ,DT_ULT_ELAB_PR_AUT
                ,DT_ULT_ELAB_COMMEF
                ,DT_ULT_ELAB_LIQMEF
                ,COD_FISC_MEF
                ,IMP_ASS_SOCIALE
                ,MOD_COMNZ_INVST_SW
                ,DT_RIAT_RIASS_RSH
                ,DT_RIAT_RIASS_COMM
                ,GG_INTR_RIT_PAG
                ,DT_ULT_RINN_TAC
                ,DESC_COMP
                ,DT_ULT_EC_TCM_IND
                ,DT_ULT_EC_TCM_COLL
                ,DT_ULT_EC_MRM_IND
                ,DT_ULT_EC_MRM_COLL
                ,COD_COMP_LDAP
                ,PC_RID_IMP_1382011
                ,PC_RID_IMP_662014
                ,SOGL_AML_PRE_UNI
                ,SOGL_AML_PRE_PER
                ,COD_SOGG_FTZ_ASSTO
                ,DT_ULT_ELAB_REDPRO
                ,DT_ULT_ELAB_TAKE_P
                ,DT_ULT_ELAB_PASPAS
                ,SOGL_AML_PRE_SAV_R
                ,DT_ULT_ESTR_DEC_CO
                ,DT_ULT_ELAB_COS_AT
                ,FRQ_COSTI_ATT
                ,DT_ULT_ELAB_COS_ST
                ,FRQ_COSTI_STORNATI
                ,DT_ESTR_ASS_MIN70A
                ,DT_ESTR_ASS_MAG70A
             INTO
                :PCO-COD-COMP-ANIA
               ,:PCO-COD-TRAT-CIRT
                :IND-PCO-COD-TRAT-CIRT
               ,:PCO-LIM-VLTR
                :IND-PCO-LIM-VLTR
               ,:PCO-TP-RAT-PERF
                :IND-PCO-TP-RAT-PERF
               ,:PCO-TP-LIV-GENZ-TIT
               ,:PCO-ARROT-PRE
                :IND-PCO-ARROT-PRE
               ,:PCO-DT-CONT-DB
                :IND-PCO-DT-CONT
               ,:PCO-DT-ULT-RIVAL-IN-DB
                :IND-PCO-DT-ULT-RIVAL-IN
               ,:PCO-DT-ULT-QTZO-IN-DB
                :IND-PCO-DT-ULT-QTZO-IN
               ,:PCO-DT-ULT-RICL-RIASS-DB
                :IND-PCO-DT-ULT-RICL-RIASS
               ,:PCO-DT-ULT-TABUL-RIASS-DB
                :IND-PCO-DT-ULT-TABUL-RIASS
               ,:PCO-DT-ULT-BOLL-EMES-DB
                :IND-PCO-DT-ULT-BOLL-EMES
               ,:PCO-DT-ULT-BOLL-STOR-DB
                :IND-PCO-DT-ULT-BOLL-STOR
               ,:PCO-DT-ULT-BOLL-LIQ-DB
                :IND-PCO-DT-ULT-BOLL-LIQ
               ,:PCO-DT-ULT-BOLL-RIAT-DB
                :IND-PCO-DT-ULT-BOLL-RIAT
               ,:PCO-DT-ULTELRISCPAR-PR-DB
                :IND-PCO-DT-ULTELRISCPAR-PR
               ,:PCO-DT-ULTC-IS-IN-DB
                :IND-PCO-DT-ULTC-IS-IN
               ,:PCO-DT-ULT-RICL-PRE-DB
                :IND-PCO-DT-ULT-RICL-PRE
               ,:PCO-DT-ULTC-MARSOL-DB
                :IND-PCO-DT-ULTC-MARSOL
               ,:PCO-DT-ULTC-RB-IN-DB
                :IND-PCO-DT-ULTC-RB-IN
               ,:PCO-PC-PROV-1AA-ACQ
                :IND-PCO-PC-PROV-1AA-ACQ
               ,:PCO-MOD-INTR-PREST
                :IND-PCO-MOD-INTR-PREST
               ,:PCO-GG-MAX-REC-PROV
                :IND-PCO-GG-MAX-REC-PROV
               ,:PCO-DT-ULTGZ-TRCH-E-IN-DB
                :IND-PCO-DT-ULTGZ-TRCH-E-IN
               ,:PCO-DT-ULT-BOLL-SNDEN-DB
                :IND-PCO-DT-ULT-BOLL-SNDEN
               ,:PCO-DT-ULT-BOLL-SNDNLQ-DB
                :IND-PCO-DT-ULT-BOLL-SNDNLQ
               ,:PCO-DT-ULTSC-ELAB-IN-DB
                :IND-PCO-DT-ULTSC-ELAB-IN
               ,:PCO-DT-ULTSC-OPZ-IN-DB
                :IND-PCO-DT-ULTSC-OPZ-IN
               ,:PCO-DT-ULTC-BNSRIC-IN-DB
                :IND-PCO-DT-ULTC-BNSRIC-IN
               ,:PCO-DT-ULTC-BNSFDT-IN-DB
                :IND-PCO-DT-ULTC-BNSFDT-IN
               ,:PCO-DT-ULT-RINN-GARAC-DB
                :IND-PCO-DT-ULT-RINN-GARAC
               ,:PCO-DT-ULTGZ-CED-DB
                :IND-PCO-DT-ULTGZ-CED
               ,:PCO-DT-ULT-ELAB-PRLCOS-DB
                :IND-PCO-DT-ULT-ELAB-PRLCOS
               ,:PCO-DT-ULT-RINN-COLL-DB
                :IND-PCO-DT-ULT-RINN-COLL
               ,:PCO-FL-RVC-PERF
                :IND-PCO-FL-RVC-PERF
               ,:PCO-FL-RCS-POLI-NOPERF
                :IND-PCO-FL-RCS-POLI-NOPERF
               ,:PCO-FL-GEST-PLUSV
                :IND-PCO-FL-GEST-PLUSV
               ,:PCO-DT-ULT-RIVAL-CL-DB
                :IND-PCO-DT-ULT-RIVAL-CL
               ,:PCO-DT-ULT-QTZO-CL-DB
                :IND-PCO-DT-ULT-QTZO-CL
               ,:PCO-DT-ULTC-BNSRIC-CL-DB
                :IND-PCO-DT-ULTC-BNSRIC-CL
               ,:PCO-DT-ULTC-BNSFDT-CL-DB
                :IND-PCO-DT-ULTC-BNSFDT-CL
               ,:PCO-DT-ULTC-IS-CL-DB
                :IND-PCO-DT-ULTC-IS-CL
               ,:PCO-DT-ULTC-RB-CL-DB
                :IND-PCO-DT-ULTC-RB-CL
               ,:PCO-DT-ULTGZ-TRCH-E-CL-DB
                :IND-PCO-DT-ULTGZ-TRCH-E-CL
               ,:PCO-DT-ULTSC-ELAB-CL-DB
                :IND-PCO-DT-ULTSC-ELAB-CL
               ,:PCO-DT-ULTSC-OPZ-CL-DB
                :IND-PCO-DT-ULTSC-OPZ-CL
               ,:PCO-STST-X-REGIONE-DB
                :IND-PCO-STST-X-REGIONE
               ,:PCO-DT-ULTGZ-CED-COLL-DB
                :IND-PCO-DT-ULTGZ-CED-COLL
               ,:PCO-TP-MOD-RIVAL
               ,:PCO-NUM-MM-CALC-MORA
                :IND-PCO-NUM-MM-CALC-MORA
               ,:PCO-DT-ULT-EC-RIV-COLL-DB
                :IND-PCO-DT-ULT-EC-RIV-COLL
               ,:PCO-DT-ULT-EC-RIV-IND-DB
                :IND-PCO-DT-ULT-EC-RIV-IND
               ,:PCO-DT-ULT-EC-IL-COLL-DB
                :IND-PCO-DT-ULT-EC-IL-COLL
               ,:PCO-DT-ULT-EC-IL-IND-DB
                :IND-PCO-DT-ULT-EC-IL-IND
               ,:PCO-DT-ULT-EC-UL-COLL-DB
                :IND-PCO-DT-ULT-EC-UL-COLL
               ,:PCO-DT-ULT-EC-UL-IND-DB
                :IND-PCO-DT-ULT-EC-UL-IND
               ,:PCO-AA-UTI
                :IND-PCO-AA-UTI
               ,:PCO-CALC-RSH-COMUN
               ,:PCO-FL-LIV-DEBUG
               ,:PCO-DT-ULT-BOLL-PERF-C-DB
                :IND-PCO-DT-ULT-BOLL-PERF-C
               ,:PCO-DT-ULT-BOLL-RSP-IN-DB
                :IND-PCO-DT-ULT-BOLL-RSP-IN
               ,:PCO-DT-ULT-BOLL-RSP-CL-DB
                :IND-PCO-DT-ULT-BOLL-RSP-CL
               ,:PCO-DT-ULT-BOLL-EMES-I-DB
                :IND-PCO-DT-ULT-BOLL-EMES-I
               ,:PCO-DT-ULT-BOLL-STOR-I-DB
                :IND-PCO-DT-ULT-BOLL-STOR-I
               ,:PCO-DT-ULT-BOLL-RIAT-I-DB
                :IND-PCO-DT-ULT-BOLL-RIAT-I
               ,:PCO-DT-ULT-BOLL-SD-I-DB
                :IND-PCO-DT-ULT-BOLL-SD-I
               ,:PCO-DT-ULT-BOLL-SDNL-I-DB
                :IND-PCO-DT-ULT-BOLL-SDNL-I
               ,:PCO-DT-ULT-BOLL-PERF-I-DB
                :IND-PCO-DT-ULT-BOLL-PERF-I
               ,:PCO-DT-RICL-RIRIAS-COM-DB
                :IND-PCO-DT-RICL-RIRIAS-COM
               ,:PCO-DT-ULT-ELAB-AT92-C-DB
                :IND-PCO-DT-ULT-ELAB-AT92-C
               ,:PCO-DT-ULT-ELAB-AT92-I-DB
                :IND-PCO-DT-ULT-ELAB-AT92-I
               ,:PCO-DT-ULT-ELAB-AT93-C-DB
                :IND-PCO-DT-ULT-ELAB-AT93-C
               ,:PCO-DT-ULT-ELAB-AT93-I-DB
                :IND-PCO-DT-ULT-ELAB-AT93-I
               ,:PCO-DT-ULT-ELAB-SPE-IN-DB
                :IND-PCO-DT-ULT-ELAB-SPE-IN
               ,:PCO-DT-ULT-ELAB-PR-CON-DB
                :IND-PCO-DT-ULT-ELAB-PR-CON
               ,:PCO-DT-ULT-BOLL-RP-CL-DB
                :IND-PCO-DT-ULT-BOLL-RP-CL
               ,:PCO-DT-ULT-BOLL-RP-IN-DB
                :IND-PCO-DT-ULT-BOLL-RP-IN
               ,:PCO-DT-ULT-BOLL-PRE-I-DB
                :IND-PCO-DT-ULT-BOLL-PRE-I
               ,:PCO-DT-ULT-BOLL-PRE-C-DB
                :IND-PCO-DT-ULT-BOLL-PRE-C
               ,:PCO-DT-ULTC-PILDI-MM-C-DB
                :IND-PCO-DT-ULTC-PILDI-MM-C
               ,:PCO-DT-ULTC-PILDI-AA-C-DB
                :IND-PCO-DT-ULTC-PILDI-AA-C
               ,:PCO-DT-ULTC-PILDI-MM-I-DB
                :IND-PCO-DT-ULTC-PILDI-MM-I
               ,:PCO-DT-ULTC-PILDI-TR-I-DB
                :IND-PCO-DT-ULTC-PILDI-TR-I
               ,:PCO-DT-ULTC-PILDI-AA-I-DB
                :IND-PCO-DT-ULTC-PILDI-AA-I
               ,:PCO-DT-ULT-BOLL-QUIE-C-DB
                :IND-PCO-DT-ULT-BOLL-QUIE-C
               ,:PCO-DT-ULT-BOLL-QUIE-I-DB
                :IND-PCO-DT-ULT-BOLL-QUIE-I
               ,:PCO-DT-ULT-BOLL-COTR-I-DB
                :IND-PCO-DT-ULT-BOLL-COTR-I
               ,:PCO-DT-ULT-BOLL-COTR-C-DB
                :IND-PCO-DT-ULT-BOLL-COTR-C
               ,:PCO-DT-ULT-BOLL-CORI-C-DB
                :IND-PCO-DT-ULT-BOLL-CORI-C
               ,:PCO-DT-ULT-BOLL-CORI-I-DB
                :IND-PCO-DT-ULT-BOLL-CORI-I
               ,:PCO-DS-OPER-SQL
               ,:PCO-DS-VER
               ,:PCO-DS-TS-CPTZ
               ,:PCO-DS-UTENTE
               ,:PCO-DS-STATO-ELAB
               ,:PCO-TP-VALZZ-DT-VLT
                :IND-PCO-TP-VALZZ-DT-VLT
               ,:PCO-FL-FRAZ-PROV-ACQ
                :IND-PCO-FL-FRAZ-PROV-ACQ
               ,:PCO-DT-ULT-AGG-EROG-RE-DB
                :IND-PCO-DT-ULT-AGG-EROG-RE
               ,:PCO-PC-RM-MARSOL
                :IND-PCO-PC-RM-MARSOL
               ,:PCO-PC-C-SUBRSH-MARSOL
                :IND-PCO-PC-C-SUBRSH-MARSOL
               ,:PCO-COD-COMP-ISVAP
                :IND-PCO-COD-COMP-ISVAP
               ,:PCO-LM-RIS-CON-INT
                :IND-PCO-LM-RIS-CON-INT
               ,:PCO-LM-C-SUBRSH-CON-IN
                :IND-PCO-LM-C-SUBRSH-CON-IN
               ,:PCO-PC-GAR-NORISK-MARS
                :IND-PCO-PC-GAR-NORISK-MARS
               ,:PCO-CRZ-1A-RAT-INTR-PR
                :IND-PCO-CRZ-1A-RAT-INTR-PR
               ,:PCO-NUM-GG-ARR-INTR-PR
                :IND-PCO-NUM-GG-ARR-INTR-PR
               ,:PCO-FL-VISUAL-VINPG
                :IND-PCO-FL-VISUAL-VINPG
               ,:PCO-DT-ULT-ESTRAZ-FUG-DB
                :IND-PCO-DT-ULT-ESTRAZ-FUG
               ,:PCO-DT-ULT-ELAB-PR-AUT-DB
                :IND-PCO-DT-ULT-ELAB-PR-AUT
               ,:PCO-DT-ULT-ELAB-COMMEF-DB
                :IND-PCO-DT-ULT-ELAB-COMMEF
               ,:PCO-DT-ULT-ELAB-LIQMEF-DB
                :IND-PCO-DT-ULT-ELAB-LIQMEF
               ,:PCO-COD-FISC-MEF
                :IND-PCO-COD-FISC-MEF
               ,:PCO-IMP-ASS-SOCIALE
                :IND-PCO-IMP-ASS-SOCIALE
               ,:PCO-MOD-COMNZ-INVST-SW
                :IND-PCO-MOD-COMNZ-INVST-SW
               ,:PCO-DT-RIAT-RIASS-RSH-DB
                :IND-PCO-DT-RIAT-RIASS-RSH
               ,:PCO-DT-RIAT-RIASS-COMM-DB
                :IND-PCO-DT-RIAT-RIASS-COMM
               ,:PCO-GG-INTR-RIT-PAG
                :IND-PCO-GG-INTR-RIT-PAG
               ,:PCO-DT-ULT-RINN-TAC-DB
                :IND-PCO-DT-ULT-RINN-TAC
               ,:PCO-DESC-COMP-VCHAR
                :IND-PCO-DESC-COMP
               ,:PCO-DT-ULT-EC-TCM-IND-DB
                :IND-PCO-DT-ULT-EC-TCM-IND
               ,:PCO-DT-ULT-EC-TCM-COLL-DB
                :IND-PCO-DT-ULT-EC-TCM-COLL
               ,:PCO-DT-ULT-EC-MRM-IND-DB
                :IND-PCO-DT-ULT-EC-MRM-IND
               ,:PCO-DT-ULT-EC-MRM-COLL-DB
                :IND-PCO-DT-ULT-EC-MRM-COLL
               ,:PCO-COD-COMP-LDAP
                :IND-PCO-COD-COMP-LDAP
               ,:PCO-PC-RID-IMP-1382011
                :IND-PCO-PC-RID-IMP-1382011
               ,:PCO-PC-RID-IMP-662014
                :IND-PCO-PC-RID-IMP-662014
               ,:PCO-SOGL-AML-PRE-UNI
                :IND-PCO-SOGL-AML-PRE-UNI
               ,:PCO-SOGL-AML-PRE-PER
                :IND-PCO-SOGL-AML-PRE-PER
               ,:PCO-COD-SOGG-FTZ-ASSTO
                :IND-PCO-COD-SOGG-FTZ-ASSTO
               ,:PCO-DT-ULT-ELAB-REDPRO-DB
                :IND-PCO-DT-ULT-ELAB-REDPRO
               ,:PCO-DT-ULT-ELAB-TAKE-P-DB
                :IND-PCO-DT-ULT-ELAB-TAKE-P
               ,:PCO-DT-ULT-ELAB-PASPAS-DB
                :IND-PCO-DT-ULT-ELAB-PASPAS
               ,:PCO-SOGL-AML-PRE-SAV-R
                :IND-PCO-SOGL-AML-PRE-SAV-R
               ,:PCO-DT-ULT-ESTR-DEC-CO-DB
                :IND-PCO-DT-ULT-ESTR-DEC-CO
               ,:PCO-DT-ULT-ELAB-COS-AT-DB
                :IND-PCO-DT-ULT-ELAB-COS-AT
               ,:PCO-FRQ-COSTI-ATT
                :IND-PCO-FRQ-COSTI-ATT
               ,:PCO-DT-ULT-ELAB-COS-ST-DB
                :IND-PCO-DT-ULT-ELAB-COS-ST
               ,:PCO-FRQ-COSTI-STORNATI
                :IND-PCO-FRQ-COSTI-STORNATI
               ,:PCO-DT-ESTR-ASS-MIN70A-DB
                :IND-PCO-DT-ESTR-ASS-MIN70A
               ,:PCO-DT-ESTR-ASS-MAG70A-DB
                :IND-PCO-DT-ESTR-ASS-MAG70A
             FROM PARAM_COMP
             WHERE     COD_COMP_ANIA = :PCO-COD-COMP-ANIA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO PARAM_COMP
                     (
                        COD_COMP_ANIA
                       ,COD_TRAT_CIRT
                       ,LIM_VLTR
                       ,TP_RAT_PERF
                       ,TP_LIV_GENZ_TIT
                       ,ARROT_PRE
                       ,DT_CONT
                       ,DT_ULT_RIVAL_IN
                       ,DT_ULT_QTZO_IN
                       ,DT_ULT_RICL_RIASS
                       ,DT_ULT_TABUL_RIASS
                       ,DT_ULT_BOLL_EMES
                       ,DT_ULT_BOLL_STOR
                       ,DT_ULT_BOLL_LIQ
                       ,DT_ULT_BOLL_RIAT
                       ,DT_ULTELRISCPAR_PR
                       ,DT_ULTC_IS_IN
                       ,DT_ULT_RICL_PRE
                       ,DT_ULTC_MARSOL
                       ,DT_ULTC_RB_IN
                       ,PC_PROV_1AA_ACQ
                       ,MOD_INTR_PREST
                       ,GG_MAX_REC_PROV
                       ,DT_ULTGZ_TRCH_E_IN
                       ,DT_ULT_BOLL_SNDEN
                       ,DT_ULT_BOLL_SNDNLQ
                       ,DT_ULTSC_ELAB_IN
                       ,DT_ULTSC_OPZ_IN
                       ,DT_ULTC_BNSRIC_IN
                       ,DT_ULTC_BNSFDT_IN
                       ,DT_ULT_RINN_GARAC
                       ,DT_ULTGZ_CED
                       ,DT_ULT_ELAB_PRLCOS
                       ,DT_ULT_RINN_COLL
                       ,FL_RVC_PERF
                       ,FL_RCS_POLI_NOPERF
                       ,FL_GEST_PLUSV
                       ,DT_ULT_RIVAL_CL
                       ,DT_ULT_QTZO_CL
                       ,DT_ULTC_BNSRIC_CL
                       ,DT_ULTC_BNSFDT_CL
                       ,DT_ULTC_IS_CL
                       ,DT_ULTC_RB_CL
                       ,DT_ULTGZ_TRCH_E_CL
                       ,DT_ULTSC_ELAB_CL
                       ,DT_ULTSC_OPZ_CL
                       ,STST_X_REGIONE
                       ,DT_ULTGZ_CED_COLL
                       ,TP_MOD_RIVAL
                       ,NUM_MM_CALC_MORA
                       ,DT_ULT_EC_RIV_COLL
                       ,DT_ULT_EC_RIV_IND
                       ,DT_ULT_EC_IL_COLL
                       ,DT_ULT_EC_IL_IND
                       ,DT_ULT_EC_UL_COLL
                       ,DT_ULT_EC_UL_IND
                       ,AA_UTI
                       ,CALC_RSH_COMUN
                       ,FL_LIV_DEBUG
                       ,DT_ULT_BOLL_PERF_C
                       ,DT_ULT_BOLL_RSP_IN
                       ,DT_ULT_BOLL_RSP_CL
                       ,DT_ULT_BOLL_EMES_I
                       ,DT_ULT_BOLL_STOR_I
                       ,DT_ULT_BOLL_RIAT_I
                       ,DT_ULT_BOLL_SD_I
                       ,DT_ULT_BOLL_SDNL_I
                       ,DT_ULT_BOLL_PERF_I
                       ,DT_RICL_RIRIAS_COM
                       ,DT_ULT_ELAB_AT92_C
                       ,DT_ULT_ELAB_AT92_I
                       ,DT_ULT_ELAB_AT93_C
                       ,DT_ULT_ELAB_AT93_I
                       ,DT_ULT_ELAB_SPE_IN
                       ,DT_ULT_ELAB_PR_CON
                       ,DT_ULT_BOLL_RP_CL
                       ,DT_ULT_BOLL_RP_IN
                       ,DT_ULT_BOLL_PRE_I
                       ,DT_ULT_BOLL_PRE_C
                       ,DT_ULTC_PILDI_MM_C
                       ,DT_ULTC_PILDI_AA_C
                       ,DT_ULTC_PILDI_MM_I
                       ,DT_ULTC_PILDI_TR_I
                       ,DT_ULTC_PILDI_AA_I
                       ,DT_ULT_BOLL_QUIE_C
                       ,DT_ULT_BOLL_QUIE_I
                       ,DT_ULT_BOLL_COTR_I
                       ,DT_ULT_BOLL_COTR_C
                       ,DT_ULT_BOLL_CORI_C
                       ,DT_ULT_BOLL_CORI_I
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,TP_VALZZ_DT_VLT
                       ,FL_FRAZ_PROV_ACQ
                       ,DT_ULT_AGG_EROG_RE
                       ,PC_RM_MARSOL
                       ,PC_C_SUBRSH_MARSOL
                       ,COD_COMP_ISVAP
                       ,LM_RIS_CON_INT
                       ,LM_C_SUBRSH_CON_IN
                       ,PC_GAR_NORISK_MARS
                       ,CRZ_1A_RAT_INTR_PR
                       ,NUM_GG_ARR_INTR_PR
                       ,FL_VISUAL_VINPG
                       ,DT_ULT_ESTRAZ_FUG
                       ,DT_ULT_ELAB_PR_AUT
                       ,DT_ULT_ELAB_COMMEF
                       ,DT_ULT_ELAB_LIQMEF
                       ,COD_FISC_MEF
                       ,IMP_ASS_SOCIALE
                       ,MOD_COMNZ_INVST_SW
                       ,DT_RIAT_RIASS_RSH
                       ,DT_RIAT_RIASS_COMM
                       ,GG_INTR_RIT_PAG
                       ,DT_ULT_RINN_TAC
                       ,DESC_COMP
                       ,DT_ULT_EC_TCM_IND
                       ,DT_ULT_EC_TCM_COLL
                       ,DT_ULT_EC_MRM_IND
                       ,DT_ULT_EC_MRM_COLL
                       ,COD_COMP_LDAP
                       ,PC_RID_IMP_1382011
                       ,PC_RID_IMP_662014
                       ,SOGL_AML_PRE_UNI
                       ,SOGL_AML_PRE_PER
                       ,COD_SOGG_FTZ_ASSTO
                       ,DT_ULT_ELAB_REDPRO
                       ,DT_ULT_ELAB_TAKE_P
                       ,DT_ULT_ELAB_PASPAS
                       ,SOGL_AML_PRE_SAV_R
                       ,DT_ULT_ESTR_DEC_CO
                       ,DT_ULT_ELAB_COS_AT
                       ,FRQ_COSTI_ATT
                       ,DT_ULT_ELAB_COS_ST
                       ,FRQ_COSTI_STORNATI
                       ,DT_ESTR_ASS_MIN70A
                       ,DT_ESTR_ASS_MAG70A
                     )
                 VALUES
                     (
                       :PCO-COD-COMP-ANIA
                       ,:PCO-COD-TRAT-CIRT
                        :IND-PCO-COD-TRAT-CIRT
                       ,:PCO-LIM-VLTR
                        :IND-PCO-LIM-VLTR
                       ,:PCO-TP-RAT-PERF
                        :IND-PCO-TP-RAT-PERF
                       ,:PCO-TP-LIV-GENZ-TIT
                       ,:PCO-ARROT-PRE
                        :IND-PCO-ARROT-PRE
                       ,:PCO-DT-CONT-DB
                        :IND-PCO-DT-CONT
                       ,:PCO-DT-ULT-RIVAL-IN-DB
                        :IND-PCO-DT-ULT-RIVAL-IN
                       ,:PCO-DT-ULT-QTZO-IN-DB
                        :IND-PCO-DT-ULT-QTZO-IN
                       ,:PCO-DT-ULT-RICL-RIASS-DB
                        :IND-PCO-DT-ULT-RICL-RIASS
                       ,:PCO-DT-ULT-TABUL-RIASS-DB
                        :IND-PCO-DT-ULT-TABUL-RIASS
                       ,:PCO-DT-ULT-BOLL-EMES-DB
                        :IND-PCO-DT-ULT-BOLL-EMES
                       ,:PCO-DT-ULT-BOLL-STOR-DB
                        :IND-PCO-DT-ULT-BOLL-STOR
                       ,:PCO-DT-ULT-BOLL-LIQ-DB
                        :IND-PCO-DT-ULT-BOLL-LIQ
                       ,:PCO-DT-ULT-BOLL-RIAT-DB
                        :IND-PCO-DT-ULT-BOLL-RIAT
                       ,:PCO-DT-ULTELRISCPAR-PR-DB
                        :IND-PCO-DT-ULTELRISCPAR-PR
                       ,:PCO-DT-ULTC-IS-IN-DB
                        :IND-PCO-DT-ULTC-IS-IN
                       ,:PCO-DT-ULT-RICL-PRE-DB
                        :IND-PCO-DT-ULT-RICL-PRE
                       ,:PCO-DT-ULTC-MARSOL-DB
                        :IND-PCO-DT-ULTC-MARSOL
                       ,:PCO-DT-ULTC-RB-IN-DB
                        :IND-PCO-DT-ULTC-RB-IN
                       ,:PCO-PC-PROV-1AA-ACQ
                        :IND-PCO-PC-PROV-1AA-ACQ
                       ,:PCO-MOD-INTR-PREST
                        :IND-PCO-MOD-INTR-PREST
                       ,:PCO-GG-MAX-REC-PROV
                        :IND-PCO-GG-MAX-REC-PROV
                       ,:PCO-DT-ULTGZ-TRCH-E-IN-DB
                        :IND-PCO-DT-ULTGZ-TRCH-E-IN
                       ,:PCO-DT-ULT-BOLL-SNDEN-DB
                        :IND-PCO-DT-ULT-BOLL-SNDEN
                       ,:PCO-DT-ULT-BOLL-SNDNLQ-DB
                        :IND-PCO-DT-ULT-BOLL-SNDNLQ
                       ,:PCO-DT-ULTSC-ELAB-IN-DB
                        :IND-PCO-DT-ULTSC-ELAB-IN
                       ,:PCO-DT-ULTSC-OPZ-IN-DB
                        :IND-PCO-DT-ULTSC-OPZ-IN
                       ,:PCO-DT-ULTC-BNSRIC-IN-DB
                        :IND-PCO-DT-ULTC-BNSRIC-IN
                       ,:PCO-DT-ULTC-BNSFDT-IN-DB
                        :IND-PCO-DT-ULTC-BNSFDT-IN
                       ,:PCO-DT-ULT-RINN-GARAC-DB
                        :IND-PCO-DT-ULT-RINN-GARAC
                       ,:PCO-DT-ULTGZ-CED-DB
                        :IND-PCO-DT-ULTGZ-CED
                       ,:PCO-DT-ULT-ELAB-PRLCOS-DB
                        :IND-PCO-DT-ULT-ELAB-PRLCOS
                       ,:PCO-DT-ULT-RINN-COLL-DB
                        :IND-PCO-DT-ULT-RINN-COLL
                       ,:PCO-FL-RVC-PERF
                        :IND-PCO-FL-RVC-PERF
                       ,:PCO-FL-RCS-POLI-NOPERF
                        :IND-PCO-FL-RCS-POLI-NOPERF
                       ,:PCO-FL-GEST-PLUSV
                        :IND-PCO-FL-GEST-PLUSV
                       ,:PCO-DT-ULT-RIVAL-CL-DB
                        :IND-PCO-DT-ULT-RIVAL-CL
                       ,:PCO-DT-ULT-QTZO-CL-DB
                        :IND-PCO-DT-ULT-QTZO-CL
                       ,:PCO-DT-ULTC-BNSRIC-CL-DB
                        :IND-PCO-DT-ULTC-BNSRIC-CL
                       ,:PCO-DT-ULTC-BNSFDT-CL-DB
                        :IND-PCO-DT-ULTC-BNSFDT-CL
                       ,:PCO-DT-ULTC-IS-CL-DB
                        :IND-PCO-DT-ULTC-IS-CL
                       ,:PCO-DT-ULTC-RB-CL-DB
                        :IND-PCO-DT-ULTC-RB-CL
                       ,:PCO-DT-ULTGZ-TRCH-E-CL-DB
                        :IND-PCO-DT-ULTGZ-TRCH-E-CL
                       ,:PCO-DT-ULTSC-ELAB-CL-DB
                        :IND-PCO-DT-ULTSC-ELAB-CL
                       ,:PCO-DT-ULTSC-OPZ-CL-DB
                        :IND-PCO-DT-ULTSC-OPZ-CL
                       ,:PCO-STST-X-REGIONE-DB
                        :IND-PCO-STST-X-REGIONE
                       ,:PCO-DT-ULTGZ-CED-COLL-DB
                        :IND-PCO-DT-ULTGZ-CED-COLL
                       ,:PCO-TP-MOD-RIVAL
                       ,:PCO-NUM-MM-CALC-MORA
                        :IND-PCO-NUM-MM-CALC-MORA
                       ,:PCO-DT-ULT-EC-RIV-COLL-DB
                        :IND-PCO-DT-ULT-EC-RIV-COLL
                       ,:PCO-DT-ULT-EC-RIV-IND-DB
                        :IND-PCO-DT-ULT-EC-RIV-IND
                       ,:PCO-DT-ULT-EC-IL-COLL-DB
                        :IND-PCO-DT-ULT-EC-IL-COLL
                       ,:PCO-DT-ULT-EC-IL-IND-DB
                        :IND-PCO-DT-ULT-EC-IL-IND
                       ,:PCO-DT-ULT-EC-UL-COLL-DB
                        :IND-PCO-DT-ULT-EC-UL-COLL
                       ,:PCO-DT-ULT-EC-UL-IND-DB
                        :IND-PCO-DT-ULT-EC-UL-IND
                       ,:PCO-AA-UTI
                        :IND-PCO-AA-UTI
                       ,:PCO-CALC-RSH-COMUN
                       ,:PCO-FL-LIV-DEBUG
                       ,:PCO-DT-ULT-BOLL-PERF-C-DB
                        :IND-PCO-DT-ULT-BOLL-PERF-C
                       ,:PCO-DT-ULT-BOLL-RSP-IN-DB
                        :IND-PCO-DT-ULT-BOLL-RSP-IN
                       ,:PCO-DT-ULT-BOLL-RSP-CL-DB
                        :IND-PCO-DT-ULT-BOLL-RSP-CL
                       ,:PCO-DT-ULT-BOLL-EMES-I-DB
                        :IND-PCO-DT-ULT-BOLL-EMES-I
                       ,:PCO-DT-ULT-BOLL-STOR-I-DB
                        :IND-PCO-DT-ULT-BOLL-STOR-I
                       ,:PCO-DT-ULT-BOLL-RIAT-I-DB
                        :IND-PCO-DT-ULT-BOLL-RIAT-I
                       ,:PCO-DT-ULT-BOLL-SD-I-DB
                        :IND-PCO-DT-ULT-BOLL-SD-I
                       ,:PCO-DT-ULT-BOLL-SDNL-I-DB
                        :IND-PCO-DT-ULT-BOLL-SDNL-I
                       ,:PCO-DT-ULT-BOLL-PERF-I-DB
                        :IND-PCO-DT-ULT-BOLL-PERF-I
                       ,:PCO-DT-RICL-RIRIAS-COM-DB
                        :IND-PCO-DT-RICL-RIRIAS-COM
                       ,:PCO-DT-ULT-ELAB-AT92-C-DB
                        :IND-PCO-DT-ULT-ELAB-AT92-C
                       ,:PCO-DT-ULT-ELAB-AT92-I-DB
                        :IND-PCO-DT-ULT-ELAB-AT92-I
                       ,:PCO-DT-ULT-ELAB-AT93-C-DB
                        :IND-PCO-DT-ULT-ELAB-AT93-C
                       ,:PCO-DT-ULT-ELAB-AT93-I-DB
                        :IND-PCO-DT-ULT-ELAB-AT93-I
                       ,:PCO-DT-ULT-ELAB-SPE-IN-DB
                        :IND-PCO-DT-ULT-ELAB-SPE-IN
                       ,:PCO-DT-ULT-ELAB-PR-CON-DB
                        :IND-PCO-DT-ULT-ELAB-PR-CON
                       ,:PCO-DT-ULT-BOLL-RP-CL-DB
                        :IND-PCO-DT-ULT-BOLL-RP-CL
                       ,:PCO-DT-ULT-BOLL-RP-IN-DB
                        :IND-PCO-DT-ULT-BOLL-RP-IN
                       ,:PCO-DT-ULT-BOLL-PRE-I-DB
                        :IND-PCO-DT-ULT-BOLL-PRE-I
                       ,:PCO-DT-ULT-BOLL-PRE-C-DB
                        :IND-PCO-DT-ULT-BOLL-PRE-C
                       ,:PCO-DT-ULTC-PILDI-MM-C-DB
                        :IND-PCO-DT-ULTC-PILDI-MM-C
                       ,:PCO-DT-ULTC-PILDI-AA-C-DB
                        :IND-PCO-DT-ULTC-PILDI-AA-C
                       ,:PCO-DT-ULTC-PILDI-MM-I-DB
                        :IND-PCO-DT-ULTC-PILDI-MM-I
                       ,:PCO-DT-ULTC-PILDI-TR-I-DB
                        :IND-PCO-DT-ULTC-PILDI-TR-I
                       ,:PCO-DT-ULTC-PILDI-AA-I-DB
                        :IND-PCO-DT-ULTC-PILDI-AA-I
                       ,:PCO-DT-ULT-BOLL-QUIE-C-DB
                        :IND-PCO-DT-ULT-BOLL-QUIE-C
                       ,:PCO-DT-ULT-BOLL-QUIE-I-DB
                        :IND-PCO-DT-ULT-BOLL-QUIE-I
                       ,:PCO-DT-ULT-BOLL-COTR-I-DB
                        :IND-PCO-DT-ULT-BOLL-COTR-I
                       ,:PCO-DT-ULT-BOLL-COTR-C-DB
                        :IND-PCO-DT-ULT-BOLL-COTR-C
                       ,:PCO-DT-ULT-BOLL-CORI-C-DB
                        :IND-PCO-DT-ULT-BOLL-CORI-C
                       ,:PCO-DT-ULT-BOLL-CORI-I-DB
                        :IND-PCO-DT-ULT-BOLL-CORI-I
                       ,:PCO-DS-OPER-SQL
                       ,:PCO-DS-VER
                       ,:PCO-DS-TS-CPTZ
                       ,:PCO-DS-UTENTE
                       ,:PCO-DS-STATO-ELAB
                       ,:PCO-TP-VALZZ-DT-VLT
                        :IND-PCO-TP-VALZZ-DT-VLT
                       ,:PCO-FL-FRAZ-PROV-ACQ
                        :IND-PCO-FL-FRAZ-PROV-ACQ
                       ,:PCO-DT-ULT-AGG-EROG-RE-DB
                        :IND-PCO-DT-ULT-AGG-EROG-RE
                       ,:PCO-PC-RM-MARSOL
                        :IND-PCO-PC-RM-MARSOL
                       ,:PCO-PC-C-SUBRSH-MARSOL
                        :IND-PCO-PC-C-SUBRSH-MARSOL
                       ,:PCO-COD-COMP-ISVAP
                        :IND-PCO-COD-COMP-ISVAP
                       ,:PCO-LM-RIS-CON-INT
                        :IND-PCO-LM-RIS-CON-INT
                       ,:PCO-LM-C-SUBRSH-CON-IN
                        :IND-PCO-LM-C-SUBRSH-CON-IN
                       ,:PCO-PC-GAR-NORISK-MARS
                        :IND-PCO-PC-GAR-NORISK-MARS
                       ,:PCO-CRZ-1A-RAT-INTR-PR
                        :IND-PCO-CRZ-1A-RAT-INTR-PR
                       ,:PCO-NUM-GG-ARR-INTR-PR
                        :IND-PCO-NUM-GG-ARR-INTR-PR
                       ,:PCO-FL-VISUAL-VINPG
                        :IND-PCO-FL-VISUAL-VINPG
                       ,:PCO-DT-ULT-ESTRAZ-FUG-DB
                        :IND-PCO-DT-ULT-ESTRAZ-FUG
                       ,:PCO-DT-ULT-ELAB-PR-AUT-DB
                        :IND-PCO-DT-ULT-ELAB-PR-AUT
                       ,:PCO-DT-ULT-ELAB-COMMEF-DB
                        :IND-PCO-DT-ULT-ELAB-COMMEF
                       ,:PCO-DT-ULT-ELAB-LIQMEF-DB
                        :IND-PCO-DT-ULT-ELAB-LIQMEF
                       ,:PCO-COD-FISC-MEF
                        :IND-PCO-COD-FISC-MEF
                       ,:PCO-IMP-ASS-SOCIALE
                        :IND-PCO-IMP-ASS-SOCIALE
                       ,:PCO-MOD-COMNZ-INVST-SW
                        :IND-PCO-MOD-COMNZ-INVST-SW
                       ,:PCO-DT-RIAT-RIASS-RSH-DB
                        :IND-PCO-DT-RIAT-RIASS-RSH
                       ,:PCO-DT-RIAT-RIASS-COMM-DB
                        :IND-PCO-DT-RIAT-RIASS-COMM
                       ,:PCO-GG-INTR-RIT-PAG
                        :IND-PCO-GG-INTR-RIT-PAG
                       ,:PCO-DT-ULT-RINN-TAC-DB
                        :IND-PCO-DT-ULT-RINN-TAC
                       ,:PCO-DESC-COMP-VCHAR
                        :IND-PCO-DESC-COMP
                       ,:PCO-DT-ULT-EC-TCM-IND-DB
                        :IND-PCO-DT-ULT-EC-TCM-IND
                       ,:PCO-DT-ULT-EC-TCM-COLL-DB
                        :IND-PCO-DT-ULT-EC-TCM-COLL
                       ,:PCO-DT-ULT-EC-MRM-IND-DB
                        :IND-PCO-DT-ULT-EC-MRM-IND
                       ,:PCO-DT-ULT-EC-MRM-COLL-DB
                        :IND-PCO-DT-ULT-EC-MRM-COLL
                       ,:PCO-COD-COMP-LDAP
                        :IND-PCO-COD-COMP-LDAP
                       ,:PCO-PC-RID-IMP-1382011
                        :IND-PCO-PC-RID-IMP-1382011
                       ,:PCO-PC-RID-IMP-662014
                        :IND-PCO-PC-RID-IMP-662014
                       ,:PCO-SOGL-AML-PRE-UNI
                        :IND-PCO-SOGL-AML-PRE-UNI
                       ,:PCO-SOGL-AML-PRE-PER
                        :IND-PCO-SOGL-AML-PRE-PER
                       ,:PCO-COD-SOGG-FTZ-ASSTO
                        :IND-PCO-COD-SOGG-FTZ-ASSTO
                       ,:PCO-DT-ULT-ELAB-REDPRO-DB
                        :IND-PCO-DT-ULT-ELAB-REDPRO
                       ,:PCO-DT-ULT-ELAB-TAKE-P-DB
                        :IND-PCO-DT-ULT-ELAB-TAKE-P
                       ,:PCO-DT-ULT-ELAB-PASPAS-DB
                        :IND-PCO-DT-ULT-ELAB-PASPAS
                       ,:PCO-SOGL-AML-PRE-SAV-R
                        :IND-PCO-SOGL-AML-PRE-SAV-R
                       ,:PCO-DT-ULT-ESTR-DEC-CO-DB
                        :IND-PCO-DT-ULT-ESTR-DEC-CO
                       ,:PCO-DT-ULT-ELAB-COS-AT-DB
                        :IND-PCO-DT-ULT-ELAB-COS-AT
                       ,:PCO-FRQ-COSTI-ATT
                        :IND-PCO-FRQ-COSTI-ATT
                       ,:PCO-DT-ULT-ELAB-COS-ST-DB
                        :IND-PCO-DT-ULT-ELAB-COS-ST
                       ,:PCO-FRQ-COSTI-STORNATI
                        :IND-PCO-FRQ-COSTI-STORNATI
                       ,:PCO-DT-ESTR-ASS-MIN70A-DB
                        :IND-PCO-DT-ESTR-ASS-MIN70A
                       ,:PCO-DT-ESTR-ASS-MAG70A-DB
                        :IND-PCO-DT-ESTR-ASS-MAG70A
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE PARAM_COMP SET

                   COD_COMP_ANIA          =
                :PCO-COD-COMP-ANIA
                  ,COD_TRAT_CIRT          =
                :PCO-COD-TRAT-CIRT
                                       :IND-PCO-COD-TRAT-CIRT
                  ,LIM_VLTR               =
                :PCO-LIM-VLTR
                                       :IND-PCO-LIM-VLTR
                  ,TP_RAT_PERF            =
                :PCO-TP-RAT-PERF
                                       :IND-PCO-TP-RAT-PERF
                  ,TP_LIV_GENZ_TIT        =
                :PCO-TP-LIV-GENZ-TIT
                  ,ARROT_PRE              =
                :PCO-ARROT-PRE
                                       :IND-PCO-ARROT-PRE
                  ,DT_CONT                =
           :PCO-DT-CONT-DB
                                       :IND-PCO-DT-CONT
                  ,DT_ULT_RIVAL_IN        =
           :PCO-DT-ULT-RIVAL-IN-DB
                                       :IND-PCO-DT-ULT-RIVAL-IN
                  ,DT_ULT_QTZO_IN         =
           :PCO-DT-ULT-QTZO-IN-DB
                                       :IND-PCO-DT-ULT-QTZO-IN
                  ,DT_ULT_RICL_RIASS      =
           :PCO-DT-ULT-RICL-RIASS-DB
                                       :IND-PCO-DT-ULT-RICL-RIASS
                  ,DT_ULT_TABUL_RIASS     =
           :PCO-DT-ULT-TABUL-RIASS-DB
                                       :IND-PCO-DT-ULT-TABUL-RIASS
                  ,DT_ULT_BOLL_EMES       =
           :PCO-DT-ULT-BOLL-EMES-DB
                                       :IND-PCO-DT-ULT-BOLL-EMES
                  ,DT_ULT_BOLL_STOR       =
           :PCO-DT-ULT-BOLL-STOR-DB
                                       :IND-PCO-DT-ULT-BOLL-STOR
                  ,DT_ULT_BOLL_LIQ        =
           :PCO-DT-ULT-BOLL-LIQ-DB
                                       :IND-PCO-DT-ULT-BOLL-LIQ
                  ,DT_ULT_BOLL_RIAT       =
           :PCO-DT-ULT-BOLL-RIAT-DB
                                       :IND-PCO-DT-ULT-BOLL-RIAT
                  ,DT_ULTELRISCPAR_PR     =
           :PCO-DT-ULTELRISCPAR-PR-DB
                                       :IND-PCO-DT-ULTELRISCPAR-PR
                  ,DT_ULTC_IS_IN          =
           :PCO-DT-ULTC-IS-IN-DB
                                       :IND-PCO-DT-ULTC-IS-IN
                  ,DT_ULT_RICL_PRE        =
           :PCO-DT-ULT-RICL-PRE-DB
                                       :IND-PCO-DT-ULT-RICL-PRE
                  ,DT_ULTC_MARSOL         =
           :PCO-DT-ULTC-MARSOL-DB
                                       :IND-PCO-DT-ULTC-MARSOL
                  ,DT_ULTC_RB_IN          =
           :PCO-DT-ULTC-RB-IN-DB
                                       :IND-PCO-DT-ULTC-RB-IN
                  ,PC_PROV_1AA_ACQ        =
                :PCO-PC-PROV-1AA-ACQ
                                       :IND-PCO-PC-PROV-1AA-ACQ
                  ,MOD_INTR_PREST         =
                :PCO-MOD-INTR-PREST
                                       :IND-PCO-MOD-INTR-PREST
                  ,GG_MAX_REC_PROV        =
                :PCO-GG-MAX-REC-PROV
                                       :IND-PCO-GG-MAX-REC-PROV
                  ,DT_ULTGZ_TRCH_E_IN     =
           :PCO-DT-ULTGZ-TRCH-E-IN-DB
                                       :IND-PCO-DT-ULTGZ-TRCH-E-IN
                  ,DT_ULT_BOLL_SNDEN      =
           :PCO-DT-ULT-BOLL-SNDEN-DB
                                       :IND-PCO-DT-ULT-BOLL-SNDEN
                  ,DT_ULT_BOLL_SNDNLQ     =
           :PCO-DT-ULT-BOLL-SNDNLQ-DB
                                       :IND-PCO-DT-ULT-BOLL-SNDNLQ
                  ,DT_ULTSC_ELAB_IN       =
           :PCO-DT-ULTSC-ELAB-IN-DB
                                       :IND-PCO-DT-ULTSC-ELAB-IN
                  ,DT_ULTSC_OPZ_IN        =
           :PCO-DT-ULTSC-OPZ-IN-DB
                                       :IND-PCO-DT-ULTSC-OPZ-IN
                  ,DT_ULTC_BNSRIC_IN      =
           :PCO-DT-ULTC-BNSRIC-IN-DB
                                       :IND-PCO-DT-ULTC-BNSRIC-IN
                  ,DT_ULTC_BNSFDT_IN      =
           :PCO-DT-ULTC-BNSFDT-IN-DB
                                       :IND-PCO-DT-ULTC-BNSFDT-IN
                  ,DT_ULT_RINN_GARAC      =
           :PCO-DT-ULT-RINN-GARAC-DB
                                       :IND-PCO-DT-ULT-RINN-GARAC
                  ,DT_ULTGZ_CED           =
           :PCO-DT-ULTGZ-CED-DB
                                       :IND-PCO-DT-ULTGZ-CED
                  ,DT_ULT_ELAB_PRLCOS     =
           :PCO-DT-ULT-ELAB-PRLCOS-DB
                                       :IND-PCO-DT-ULT-ELAB-PRLCOS
                  ,DT_ULT_RINN_COLL       =
           :PCO-DT-ULT-RINN-COLL-DB
                                       :IND-PCO-DT-ULT-RINN-COLL
                  ,FL_RVC_PERF            =
                :PCO-FL-RVC-PERF
                                       :IND-PCO-FL-RVC-PERF
                  ,FL_RCS_POLI_NOPERF     =
                :PCO-FL-RCS-POLI-NOPERF
                                       :IND-PCO-FL-RCS-POLI-NOPERF
                  ,FL_GEST_PLUSV          =
                :PCO-FL-GEST-PLUSV
                                       :IND-PCO-FL-GEST-PLUSV
                  ,DT_ULT_RIVAL_CL        =
           :PCO-DT-ULT-RIVAL-CL-DB
                                       :IND-PCO-DT-ULT-RIVAL-CL
                  ,DT_ULT_QTZO_CL         =
           :PCO-DT-ULT-QTZO-CL-DB
                                       :IND-PCO-DT-ULT-QTZO-CL
                  ,DT_ULTC_BNSRIC_CL      =
           :PCO-DT-ULTC-BNSRIC-CL-DB
                                       :IND-PCO-DT-ULTC-BNSRIC-CL
                  ,DT_ULTC_BNSFDT_CL      =
           :PCO-DT-ULTC-BNSFDT-CL-DB
                                       :IND-PCO-DT-ULTC-BNSFDT-CL
                  ,DT_ULTC_IS_CL          =
           :PCO-DT-ULTC-IS-CL-DB
                                       :IND-PCO-DT-ULTC-IS-CL
                  ,DT_ULTC_RB_CL          =
           :PCO-DT-ULTC-RB-CL-DB
                                       :IND-PCO-DT-ULTC-RB-CL
                  ,DT_ULTGZ_TRCH_E_CL     =
           :PCO-DT-ULTGZ-TRCH-E-CL-DB
                                       :IND-PCO-DT-ULTGZ-TRCH-E-CL
                  ,DT_ULTSC_ELAB_CL       =
           :PCO-DT-ULTSC-ELAB-CL-DB
                                       :IND-PCO-DT-ULTSC-ELAB-CL
                  ,DT_ULTSC_OPZ_CL        =
           :PCO-DT-ULTSC-OPZ-CL-DB
                                       :IND-PCO-DT-ULTSC-OPZ-CL
                  ,STST_X_REGIONE         =
           :PCO-STST-X-REGIONE-DB
                                       :IND-PCO-STST-X-REGIONE
                  ,DT_ULTGZ_CED_COLL      =
           :PCO-DT-ULTGZ-CED-COLL-DB
                                       :IND-PCO-DT-ULTGZ-CED-COLL
                  ,TP_MOD_RIVAL           =
                :PCO-TP-MOD-RIVAL
                  ,NUM_MM_CALC_MORA       =
                :PCO-NUM-MM-CALC-MORA
                                       :IND-PCO-NUM-MM-CALC-MORA
                  ,DT_ULT_EC_RIV_COLL     =
           :PCO-DT-ULT-EC-RIV-COLL-DB
                                       :IND-PCO-DT-ULT-EC-RIV-COLL
                  ,DT_ULT_EC_RIV_IND      =
           :PCO-DT-ULT-EC-RIV-IND-DB
                                       :IND-PCO-DT-ULT-EC-RIV-IND
                  ,DT_ULT_EC_IL_COLL      =
           :PCO-DT-ULT-EC-IL-COLL-DB
                                       :IND-PCO-DT-ULT-EC-IL-COLL
                  ,DT_ULT_EC_IL_IND       =
           :PCO-DT-ULT-EC-IL-IND-DB
                                       :IND-PCO-DT-ULT-EC-IL-IND
                  ,DT_ULT_EC_UL_COLL      =
           :PCO-DT-ULT-EC-UL-COLL-DB
                                       :IND-PCO-DT-ULT-EC-UL-COLL
                  ,DT_ULT_EC_UL_IND       =
           :PCO-DT-ULT-EC-UL-IND-DB
                                       :IND-PCO-DT-ULT-EC-UL-IND
                  ,AA_UTI                 =
                :PCO-AA-UTI
                                       :IND-PCO-AA-UTI
                  ,CALC_RSH_COMUN         =
                :PCO-CALC-RSH-COMUN
                  ,FL_LIV_DEBUG           =
                :PCO-FL-LIV-DEBUG
                  ,DT_ULT_BOLL_PERF_C     =
           :PCO-DT-ULT-BOLL-PERF-C-DB
                                       :IND-PCO-DT-ULT-BOLL-PERF-C
                  ,DT_ULT_BOLL_RSP_IN     =
           :PCO-DT-ULT-BOLL-RSP-IN-DB
                                       :IND-PCO-DT-ULT-BOLL-RSP-IN
                  ,DT_ULT_BOLL_RSP_CL     =
           :PCO-DT-ULT-BOLL-RSP-CL-DB
                                       :IND-PCO-DT-ULT-BOLL-RSP-CL
                  ,DT_ULT_BOLL_EMES_I     =
           :PCO-DT-ULT-BOLL-EMES-I-DB
                                       :IND-PCO-DT-ULT-BOLL-EMES-I
                  ,DT_ULT_BOLL_STOR_I     =
           :PCO-DT-ULT-BOLL-STOR-I-DB
                                       :IND-PCO-DT-ULT-BOLL-STOR-I
                  ,DT_ULT_BOLL_RIAT_I     =
           :PCO-DT-ULT-BOLL-RIAT-I-DB
                                       :IND-PCO-DT-ULT-BOLL-RIAT-I
                  ,DT_ULT_BOLL_SD_I       =
           :PCO-DT-ULT-BOLL-SD-I-DB
                                       :IND-PCO-DT-ULT-BOLL-SD-I
                  ,DT_ULT_BOLL_SDNL_I     =
           :PCO-DT-ULT-BOLL-SDNL-I-DB
                                       :IND-PCO-DT-ULT-BOLL-SDNL-I
                  ,DT_ULT_BOLL_PERF_I     =
           :PCO-DT-ULT-BOLL-PERF-I-DB
                                       :IND-PCO-DT-ULT-BOLL-PERF-I
                  ,DT_RICL_RIRIAS_COM     =
           :PCO-DT-RICL-RIRIAS-COM-DB
                                       :IND-PCO-DT-RICL-RIRIAS-COM
                  ,DT_ULT_ELAB_AT92_C     =
           :PCO-DT-ULT-ELAB-AT92-C-DB
                                       :IND-PCO-DT-ULT-ELAB-AT92-C
                  ,DT_ULT_ELAB_AT92_I     =
           :PCO-DT-ULT-ELAB-AT92-I-DB
                                       :IND-PCO-DT-ULT-ELAB-AT92-I
                  ,DT_ULT_ELAB_AT93_C     =
           :PCO-DT-ULT-ELAB-AT93-C-DB
                                       :IND-PCO-DT-ULT-ELAB-AT93-C
                  ,DT_ULT_ELAB_AT93_I     =
           :PCO-DT-ULT-ELAB-AT93-I-DB
                                       :IND-PCO-DT-ULT-ELAB-AT93-I
                  ,DT_ULT_ELAB_SPE_IN     =
           :PCO-DT-ULT-ELAB-SPE-IN-DB
                                       :IND-PCO-DT-ULT-ELAB-SPE-IN
                  ,DT_ULT_ELAB_PR_CON     =
           :PCO-DT-ULT-ELAB-PR-CON-DB
                                       :IND-PCO-DT-ULT-ELAB-PR-CON
                  ,DT_ULT_BOLL_RP_CL      =
           :PCO-DT-ULT-BOLL-RP-CL-DB
                                       :IND-PCO-DT-ULT-BOLL-RP-CL
                  ,DT_ULT_BOLL_RP_IN      =
           :PCO-DT-ULT-BOLL-RP-IN-DB
                                       :IND-PCO-DT-ULT-BOLL-RP-IN
                  ,DT_ULT_BOLL_PRE_I      =
           :PCO-DT-ULT-BOLL-PRE-I-DB
                                       :IND-PCO-DT-ULT-BOLL-PRE-I
                  ,DT_ULT_BOLL_PRE_C      =
           :PCO-DT-ULT-BOLL-PRE-C-DB
                                       :IND-PCO-DT-ULT-BOLL-PRE-C
                  ,DT_ULTC_PILDI_MM_C     =
           :PCO-DT-ULTC-PILDI-MM-C-DB
                                       :IND-PCO-DT-ULTC-PILDI-MM-C
                  ,DT_ULTC_PILDI_AA_C     =
           :PCO-DT-ULTC-PILDI-AA-C-DB
                                       :IND-PCO-DT-ULTC-PILDI-AA-C
                  ,DT_ULTC_PILDI_MM_I     =
           :PCO-DT-ULTC-PILDI-MM-I-DB
                                       :IND-PCO-DT-ULTC-PILDI-MM-I
                  ,DT_ULTC_PILDI_TR_I     =
           :PCO-DT-ULTC-PILDI-TR-I-DB
                                       :IND-PCO-DT-ULTC-PILDI-TR-I
                  ,DT_ULTC_PILDI_AA_I     =
           :PCO-DT-ULTC-PILDI-AA-I-DB
                                       :IND-PCO-DT-ULTC-PILDI-AA-I
                  ,DT_ULT_BOLL_QUIE_C     =
           :PCO-DT-ULT-BOLL-QUIE-C-DB
                                       :IND-PCO-DT-ULT-BOLL-QUIE-C
                  ,DT_ULT_BOLL_QUIE_I     =
           :PCO-DT-ULT-BOLL-QUIE-I-DB
                                       :IND-PCO-DT-ULT-BOLL-QUIE-I
                  ,DT_ULT_BOLL_COTR_I     =
           :PCO-DT-ULT-BOLL-COTR-I-DB
                                       :IND-PCO-DT-ULT-BOLL-COTR-I
                  ,DT_ULT_BOLL_COTR_C     =
           :PCO-DT-ULT-BOLL-COTR-C-DB
                                       :IND-PCO-DT-ULT-BOLL-COTR-C
                  ,DT_ULT_BOLL_CORI_C     =
           :PCO-DT-ULT-BOLL-CORI-C-DB
                                       :IND-PCO-DT-ULT-BOLL-CORI-C
                  ,DT_ULT_BOLL_CORI_I     =
           :PCO-DT-ULT-BOLL-CORI-I-DB
                                       :IND-PCO-DT-ULT-BOLL-CORI-I
                  ,DS_OPER_SQL            =
                :PCO-DS-OPER-SQL
                  ,DS_VER                 =
                :PCO-DS-VER
                  ,DS_TS_CPTZ             =
                :PCO-DS-TS-CPTZ
                  ,DS_UTENTE              =
                :PCO-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :PCO-DS-STATO-ELAB
                  ,TP_VALZZ_DT_VLT        =
                :PCO-TP-VALZZ-DT-VLT
                                       :IND-PCO-TP-VALZZ-DT-VLT
                  ,FL_FRAZ_PROV_ACQ       =
                :PCO-FL-FRAZ-PROV-ACQ
                                       :IND-PCO-FL-FRAZ-PROV-ACQ
                  ,DT_ULT_AGG_EROG_RE     =
           :PCO-DT-ULT-AGG-EROG-RE-DB
                                       :IND-PCO-DT-ULT-AGG-EROG-RE
                  ,PC_RM_MARSOL           =
                :PCO-PC-RM-MARSOL
                                       :IND-PCO-PC-RM-MARSOL
                  ,PC_C_SUBRSH_MARSOL     =
                :PCO-PC-C-SUBRSH-MARSOL
                                       :IND-PCO-PC-C-SUBRSH-MARSOL
                  ,COD_COMP_ISVAP         =
                :PCO-COD-COMP-ISVAP
                                       :IND-PCO-COD-COMP-ISVAP
                  ,LM_RIS_CON_INT         =
                :PCO-LM-RIS-CON-INT
                                       :IND-PCO-LM-RIS-CON-INT
                  ,LM_C_SUBRSH_CON_IN     =
                :PCO-LM-C-SUBRSH-CON-IN
                                       :IND-PCO-LM-C-SUBRSH-CON-IN
                  ,PC_GAR_NORISK_MARS     =
                :PCO-PC-GAR-NORISK-MARS
                                       :IND-PCO-PC-GAR-NORISK-MARS
                  ,CRZ_1A_RAT_INTR_PR     =
                :PCO-CRZ-1A-RAT-INTR-PR
                                       :IND-PCO-CRZ-1A-RAT-INTR-PR
                  ,NUM_GG_ARR_INTR_PR     =
                :PCO-NUM-GG-ARR-INTR-PR
                                       :IND-PCO-NUM-GG-ARR-INTR-PR
                  ,FL_VISUAL_VINPG        =
                :PCO-FL-VISUAL-VINPG
                                       :IND-PCO-FL-VISUAL-VINPG
                  ,DT_ULT_ESTRAZ_FUG      =
           :PCO-DT-ULT-ESTRAZ-FUG-DB
                                       :IND-PCO-DT-ULT-ESTRAZ-FUG
                  ,DT_ULT_ELAB_PR_AUT     =
           :PCO-DT-ULT-ELAB-PR-AUT-DB
                                       :IND-PCO-DT-ULT-ELAB-PR-AUT
                  ,DT_ULT_ELAB_COMMEF     =
           :PCO-DT-ULT-ELAB-COMMEF-DB
                                       :IND-PCO-DT-ULT-ELAB-COMMEF
                  ,DT_ULT_ELAB_LIQMEF     =
           :PCO-DT-ULT-ELAB-LIQMEF-DB
                                       :IND-PCO-DT-ULT-ELAB-LIQMEF
                  ,COD_FISC_MEF           =
                :PCO-COD-FISC-MEF
                                       :IND-PCO-COD-FISC-MEF
                  ,IMP_ASS_SOCIALE        =
                :PCO-IMP-ASS-SOCIALE
                                       :IND-PCO-IMP-ASS-SOCIALE
                  ,MOD_COMNZ_INVST_SW     =
                :PCO-MOD-COMNZ-INVST-SW
                                       :IND-PCO-MOD-COMNZ-INVST-SW
                  ,DT_RIAT_RIASS_RSH      =
           :PCO-DT-RIAT-RIASS-RSH-DB
                                       :IND-PCO-DT-RIAT-RIASS-RSH
                  ,DT_RIAT_RIASS_COMM     =
           :PCO-DT-RIAT-RIASS-COMM-DB
                                       :IND-PCO-DT-RIAT-RIASS-COMM
                  ,GG_INTR_RIT_PAG        =
                :PCO-GG-INTR-RIT-PAG
                                       :IND-PCO-GG-INTR-RIT-PAG
                  ,DT_ULT_RINN_TAC        =
           :PCO-DT-ULT-RINN-TAC-DB
                                       :IND-PCO-DT-ULT-RINN-TAC
                  ,DESC_COMP              =
                :PCO-DESC-COMP-VCHAR
                                       :IND-PCO-DESC-COMP
                  ,DT_ULT_EC_TCM_IND      =
           :PCO-DT-ULT-EC-TCM-IND-DB
                                       :IND-PCO-DT-ULT-EC-TCM-IND
                  ,DT_ULT_EC_TCM_COLL     =
           :PCO-DT-ULT-EC-TCM-COLL-DB
                                       :IND-PCO-DT-ULT-EC-TCM-COLL
                  ,DT_ULT_EC_MRM_IND      =
           :PCO-DT-ULT-EC-MRM-IND-DB
                                       :IND-PCO-DT-ULT-EC-MRM-IND
                  ,DT_ULT_EC_MRM_COLL     =
           :PCO-DT-ULT-EC-MRM-COLL-DB
                                       :IND-PCO-DT-ULT-EC-MRM-COLL
                  ,COD_COMP_LDAP          =
                :PCO-COD-COMP-LDAP
                                       :IND-PCO-COD-COMP-LDAP
                  ,PC_RID_IMP_1382011     =
                :PCO-PC-RID-IMP-1382011
                                       :IND-PCO-PC-RID-IMP-1382011
                  ,PC_RID_IMP_662014      =
                :PCO-PC-RID-IMP-662014
                                       :IND-PCO-PC-RID-IMP-662014
                  ,SOGL_AML_PRE_UNI       =
                :PCO-SOGL-AML-PRE-UNI
                                       :IND-PCO-SOGL-AML-PRE-UNI
                  ,SOGL_AML_PRE_PER       =
                :PCO-SOGL-AML-PRE-PER
                                       :IND-PCO-SOGL-AML-PRE-PER
                  ,COD_SOGG_FTZ_ASSTO     =
                :PCO-COD-SOGG-FTZ-ASSTO
                                       :IND-PCO-COD-SOGG-FTZ-ASSTO
                  ,DT_ULT_ELAB_REDPRO     =
           :PCO-DT-ULT-ELAB-REDPRO-DB
                                       :IND-PCO-DT-ULT-ELAB-REDPRO
                  ,DT_ULT_ELAB_TAKE_P     =
           :PCO-DT-ULT-ELAB-TAKE-P-DB
                                       :IND-PCO-DT-ULT-ELAB-TAKE-P
                  ,DT_ULT_ELAB_PASPAS     =
           :PCO-DT-ULT-ELAB-PASPAS-DB
                                       :IND-PCO-DT-ULT-ELAB-PASPAS
                  ,SOGL_AML_PRE_SAV_R     =
                :PCO-SOGL-AML-PRE-SAV-R
                                       :IND-PCO-SOGL-AML-PRE-SAV-R
                  ,DT_ULT_ESTR_DEC_CO     =
           :PCO-DT-ULT-ESTR-DEC-CO-DB
                                       :IND-PCO-DT-ULT-ESTR-DEC-CO
                  ,DT_ULT_ELAB_COS_AT     =
           :PCO-DT-ULT-ELAB-COS-AT-DB
                                       :IND-PCO-DT-ULT-ELAB-COS-AT
                  ,FRQ_COSTI_ATT          =
                :PCO-FRQ-COSTI-ATT
                                       :IND-PCO-FRQ-COSTI-ATT
                  ,DT_ULT_ELAB_COS_ST     =
           :PCO-DT-ULT-ELAB-COS-ST-DB
                                       :IND-PCO-DT-ULT-ELAB-COS-ST
                  ,FRQ_COSTI_STORNATI     =
                :PCO-FRQ-COSTI-STORNATI
                                       :IND-PCO-FRQ-COSTI-STORNATI
                  ,DT_ESTR_ASS_MIN70A     =
           :PCO-DT-ESTR-ASS-MIN70A-DB
                                       :IND-PCO-DT-ESTR-ASS-MIN70A
                  ,DT_ESTR_ASS_MAG70A     =
           :PCO-DT-ESTR-ASS-MAG70A-DB
                                       :IND-PCO-DT-ESTR-ASS-MAG70A
                WHERE     COD_COMP_ANIA = :PCO-COD-COMP-ANIA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM PARAM_COMP
                WHERE     COD_COMP_ANIA = :PCO-COD-COMP-ANIA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-PCO-COD-TRAT-CIRT = -1
              MOVE HIGH-VALUES TO PCO-COD-TRAT-CIRT-NULL
           END-IF
           IF IND-PCO-LIM-VLTR = -1
              MOVE HIGH-VALUES TO PCO-LIM-VLTR-NULL
           END-IF
           IF IND-PCO-TP-RAT-PERF = -1
              MOVE HIGH-VALUES TO PCO-TP-RAT-PERF-NULL
           END-IF
           IF IND-PCO-ARROT-PRE = -1
              MOVE HIGH-VALUES TO PCO-ARROT-PRE-NULL
           END-IF
           IF IND-PCO-DT-CONT = -1
              MOVE HIGH-VALUES TO PCO-DT-CONT-NULL
           END-IF
           IF IND-PCO-DT-ULT-RIVAL-IN = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-RIVAL-IN-NULL
           END-IF
           IF IND-PCO-DT-ULT-QTZO-IN = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-QTZO-IN-NULL
           END-IF
           IF IND-PCO-DT-ULT-RICL-RIASS = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-RICL-RIASS-NULL
           END-IF
           IF IND-PCO-DT-ULT-TABUL-RIASS = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-TABUL-RIASS-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-EMES = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-EMES-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-STOR = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-STOR-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-LIQ = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-LIQ-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-RIAT = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-RIAT-NULL
           END-IF
           IF IND-PCO-DT-ULTELRISCPAR-PR = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTELRISCPAR-PR-NULL
           END-IF
           IF IND-PCO-DT-ULTC-IS-IN = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTC-IS-IN-NULL
           END-IF
           IF IND-PCO-DT-ULT-RICL-PRE = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-RICL-PRE-NULL
           END-IF
           IF IND-PCO-DT-ULTC-MARSOL = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTC-MARSOL-NULL
           END-IF
           IF IND-PCO-DT-ULTC-RB-IN = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTC-RB-IN-NULL
           END-IF
           IF IND-PCO-PC-PROV-1AA-ACQ = -1
              MOVE HIGH-VALUES TO PCO-PC-PROV-1AA-ACQ-NULL
           END-IF
           IF IND-PCO-MOD-INTR-PREST = -1
              MOVE HIGH-VALUES TO PCO-MOD-INTR-PREST-NULL
           END-IF
           IF IND-PCO-GG-MAX-REC-PROV = -1
              MOVE HIGH-VALUES TO PCO-GG-MAX-REC-PROV-NULL
           END-IF
           IF IND-PCO-DT-ULTGZ-TRCH-E-IN = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTGZ-TRCH-E-IN-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-SNDEN = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-SNDEN-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-SNDNLQ = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-SNDNLQ-NULL
           END-IF
           IF IND-PCO-DT-ULTSC-ELAB-IN = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTSC-ELAB-IN-NULL
           END-IF
           IF IND-PCO-DT-ULTSC-OPZ-IN = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTSC-OPZ-IN-NULL
           END-IF
           IF IND-PCO-DT-ULTC-BNSRIC-IN = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTC-BNSRIC-IN-NULL
           END-IF
           IF IND-PCO-DT-ULTC-BNSFDT-IN = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTC-BNSFDT-IN-NULL
           END-IF
           IF IND-PCO-DT-ULT-RINN-GARAC = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-RINN-GARAC-NULL
           END-IF
           IF IND-PCO-DT-ULTGZ-CED = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTGZ-CED-NULL
           END-IF
           IF IND-PCO-DT-ULT-ELAB-PRLCOS = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-PRLCOS-NULL
           END-IF
           IF IND-PCO-DT-ULT-RINN-COLL = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-RINN-COLL-NULL
           END-IF
           IF IND-PCO-FL-RVC-PERF = -1
              MOVE HIGH-VALUES TO PCO-FL-RVC-PERF-NULL
           END-IF
           IF IND-PCO-FL-RCS-POLI-NOPERF = -1
              MOVE HIGH-VALUES TO PCO-FL-RCS-POLI-NOPERF-NULL
           END-IF
           IF IND-PCO-FL-GEST-PLUSV = -1
              MOVE HIGH-VALUES TO PCO-FL-GEST-PLUSV-NULL
           END-IF
           IF IND-PCO-DT-ULT-RIVAL-CL = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-RIVAL-CL-NULL
           END-IF
           IF IND-PCO-DT-ULT-QTZO-CL = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-QTZO-CL-NULL
           END-IF
           IF IND-PCO-DT-ULTC-BNSRIC-CL = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTC-BNSRIC-CL-NULL
           END-IF
           IF IND-PCO-DT-ULTC-BNSFDT-CL = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTC-BNSFDT-CL-NULL
           END-IF
           IF IND-PCO-DT-ULTC-IS-CL = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTC-IS-CL-NULL
           END-IF
           IF IND-PCO-DT-ULTC-RB-CL = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTC-RB-CL-NULL
           END-IF
           IF IND-PCO-DT-ULTGZ-TRCH-E-CL = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTGZ-TRCH-E-CL-NULL
           END-IF
           IF IND-PCO-DT-ULTSC-ELAB-CL = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTSC-ELAB-CL-NULL
           END-IF
           IF IND-PCO-DT-ULTSC-OPZ-CL = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTSC-OPZ-CL-NULL
           END-IF
           IF IND-PCO-STST-X-REGIONE = -1
              MOVE HIGH-VALUES TO PCO-STST-X-REGIONE-NULL
           END-IF
           IF IND-PCO-DT-ULTGZ-CED-COLL = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTGZ-CED-COLL-NULL
           END-IF
           IF IND-PCO-NUM-MM-CALC-MORA = -1
              MOVE HIGH-VALUES TO PCO-NUM-MM-CALC-MORA-NULL
           END-IF
           IF IND-PCO-DT-ULT-EC-RIV-COLL = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-RIV-COLL-NULL
           END-IF
           IF IND-PCO-DT-ULT-EC-RIV-IND = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-RIV-IND-NULL
           END-IF
           IF IND-PCO-DT-ULT-EC-IL-COLL = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-IL-COLL-NULL
           END-IF
           IF IND-PCO-DT-ULT-EC-IL-IND = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-IL-IND-NULL
           END-IF
           IF IND-PCO-DT-ULT-EC-UL-COLL = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-UL-COLL-NULL
           END-IF
           IF IND-PCO-DT-ULT-EC-UL-IND = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-UL-IND-NULL
           END-IF
           IF IND-PCO-AA-UTI = -1
              MOVE HIGH-VALUES TO PCO-AA-UTI-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-PERF-C = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-PERF-C-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-RSP-IN = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-RSP-IN-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-RSP-CL = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-RSP-CL-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-EMES-I = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-EMES-I-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-STOR-I = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-STOR-I-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-RIAT-I = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-RIAT-I-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-SD-I = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-SD-I-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-SDNL-I = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-SDNL-I-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-PERF-I = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-PERF-I-NULL
           END-IF
           IF IND-PCO-DT-RICL-RIRIAS-COM = -1
              MOVE HIGH-VALUES TO PCO-DT-RICL-RIRIAS-COM-NULL
           END-IF
           IF IND-PCO-DT-ULT-ELAB-AT92-C = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-AT92-C-NULL
           END-IF
           IF IND-PCO-DT-ULT-ELAB-AT92-I = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-AT92-I-NULL
           END-IF
           IF IND-PCO-DT-ULT-ELAB-AT93-C = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-AT93-C-NULL
           END-IF
           IF IND-PCO-DT-ULT-ELAB-AT93-I = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-AT93-I-NULL
           END-IF
           IF IND-PCO-DT-ULT-ELAB-SPE-IN = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-SPE-IN-NULL
           END-IF
           IF IND-PCO-DT-ULT-ELAB-PR-CON = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-PR-CON-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-RP-CL = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-RP-CL-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-RP-IN = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-RP-IN-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-PRE-I = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-PRE-I-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-PRE-C = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-PRE-C-NULL
           END-IF
           IF IND-PCO-DT-ULTC-PILDI-MM-C = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTC-PILDI-MM-C-NULL
           END-IF
           IF IND-PCO-DT-ULTC-PILDI-AA-C = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTC-PILDI-AA-C-NULL
           END-IF
           IF IND-PCO-DT-ULTC-PILDI-MM-I = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTC-PILDI-MM-I-NULL
           END-IF
           IF IND-PCO-DT-ULTC-PILDI-TR-I = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTC-PILDI-TR-I-NULL
           END-IF
           IF IND-PCO-DT-ULTC-PILDI-AA-I = -1
              MOVE HIGH-VALUES TO PCO-DT-ULTC-PILDI-AA-I-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-QUIE-C = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-QUIE-C-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-QUIE-I = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-QUIE-I-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-COTR-I = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-COTR-I-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-COTR-C = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-COTR-C-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-CORI-C = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-CORI-C-NULL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-CORI-I = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-CORI-I-NULL
           END-IF
           IF IND-PCO-TP-VALZZ-DT-VLT = -1
              MOVE HIGH-VALUES TO PCO-TP-VALZZ-DT-VLT-NULL
           END-IF
           IF IND-PCO-FL-FRAZ-PROV-ACQ = -1
              MOVE HIGH-VALUES TO PCO-FL-FRAZ-PROV-ACQ-NULL
           END-IF
           IF IND-PCO-DT-ULT-AGG-EROG-RE = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-AGG-EROG-RE-NULL
           END-IF
           IF IND-PCO-PC-RM-MARSOL = -1
              MOVE HIGH-VALUES TO PCO-PC-RM-MARSOL-NULL
           END-IF
           IF IND-PCO-PC-C-SUBRSH-MARSOL = -1
              MOVE HIGH-VALUES TO PCO-PC-C-SUBRSH-MARSOL-NULL
           END-IF
           IF IND-PCO-COD-COMP-ISVAP = -1
              MOVE HIGH-VALUES TO PCO-COD-COMP-ISVAP-NULL
           END-IF
           IF IND-PCO-LM-RIS-CON-INT = -1
              MOVE HIGH-VALUES TO PCO-LM-RIS-CON-INT-NULL
           END-IF
           IF IND-PCO-LM-C-SUBRSH-CON-IN = -1
              MOVE HIGH-VALUES TO PCO-LM-C-SUBRSH-CON-IN-NULL
           END-IF
           IF IND-PCO-PC-GAR-NORISK-MARS = -1
              MOVE HIGH-VALUES TO PCO-PC-GAR-NORISK-MARS-NULL
           END-IF
           IF IND-PCO-CRZ-1A-RAT-INTR-PR = -1
              MOVE HIGH-VALUES TO PCO-CRZ-1A-RAT-INTR-PR-NULL
           END-IF
           IF IND-PCO-NUM-GG-ARR-INTR-PR = -1
              MOVE HIGH-VALUES TO PCO-NUM-GG-ARR-INTR-PR-NULL
           END-IF
           IF IND-PCO-FL-VISUAL-VINPG = -1
              MOVE HIGH-VALUES TO PCO-FL-VISUAL-VINPG-NULL
           END-IF
           IF IND-PCO-DT-ULT-ESTRAZ-FUG = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-ESTRAZ-FUG-NULL
           END-IF
           IF IND-PCO-DT-ULT-ELAB-PR-AUT = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-PR-AUT-NULL
           END-IF
           IF IND-PCO-DT-ULT-ELAB-COMMEF = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-COMMEF-NULL
           END-IF
           IF IND-PCO-DT-ULT-ELAB-LIQMEF = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-LIQMEF-NULL
           END-IF
           IF IND-PCO-COD-FISC-MEF = -1
              MOVE HIGH-VALUES TO PCO-COD-FISC-MEF-NULL
           END-IF
           IF IND-PCO-IMP-ASS-SOCIALE = -1
              MOVE HIGH-VALUES TO PCO-IMP-ASS-SOCIALE-NULL
           END-IF
           IF IND-PCO-MOD-COMNZ-INVST-SW = -1
              MOVE HIGH-VALUES TO PCO-MOD-COMNZ-INVST-SW-NULL
           END-IF
           IF IND-PCO-DT-RIAT-RIASS-RSH = -1
              MOVE HIGH-VALUES TO PCO-DT-RIAT-RIASS-RSH-NULL
           END-IF
           IF IND-PCO-DT-RIAT-RIASS-COMM = -1
              MOVE HIGH-VALUES TO PCO-DT-RIAT-RIASS-COMM-NULL
           END-IF
           IF IND-PCO-GG-INTR-RIT-PAG = -1
              MOVE HIGH-VALUES TO PCO-GG-INTR-RIT-PAG-NULL
           END-IF
           IF IND-PCO-DT-ULT-RINN-TAC = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-RINN-TAC-NULL
           END-IF
           IF IND-PCO-DESC-COMP = -1
              MOVE HIGH-VALUES TO PCO-DESC-COMP
           END-IF
           IF IND-PCO-DT-ULT-EC-TCM-IND = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-TCM-IND-NULL
           END-IF
           IF IND-PCO-DT-ULT-EC-TCM-COLL = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-TCM-COLL-NULL
           END-IF
           IF IND-PCO-DT-ULT-EC-MRM-IND = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-MRM-IND-NULL
           END-IF
           IF IND-PCO-DT-ULT-EC-MRM-COLL = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-MRM-COLL-NULL
           END-IF
           IF IND-PCO-COD-COMP-LDAP = -1
              MOVE HIGH-VALUES TO PCO-COD-COMP-LDAP-NULL
           END-IF
           IF IND-PCO-PC-RID-IMP-1382011 = -1
              MOVE HIGH-VALUES TO PCO-PC-RID-IMP-1382011-NULL
           END-IF
           IF IND-PCO-PC-RID-IMP-662014 = -1
              MOVE HIGH-VALUES TO PCO-PC-RID-IMP-662014-NULL
           END-IF
           IF IND-PCO-SOGL-AML-PRE-UNI = -1
              MOVE HIGH-VALUES TO PCO-SOGL-AML-PRE-UNI-NULL
           END-IF
           IF IND-PCO-SOGL-AML-PRE-PER = -1
              MOVE HIGH-VALUES TO PCO-SOGL-AML-PRE-PER-NULL
           END-IF
           IF IND-PCO-COD-SOGG-FTZ-ASSTO = -1
              MOVE HIGH-VALUES TO PCO-COD-SOGG-FTZ-ASSTO-NULL
           END-IF
           IF IND-PCO-DT-ULT-ELAB-REDPRO = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-REDPRO-NULL
           END-IF
           IF IND-PCO-DT-ULT-ELAB-TAKE-P = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-TAKE-P-NULL
           END-IF
           IF IND-PCO-DT-ULT-ELAB-PASPAS = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-PASPAS-NULL
           END-IF
           IF IND-PCO-SOGL-AML-PRE-SAV-R = -1
              MOVE HIGH-VALUES TO PCO-SOGL-AML-PRE-SAV-R-NULL
           END-IF
           IF IND-PCO-DT-ULT-ESTR-DEC-CO = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-ESTR-DEC-CO-NULL
           END-IF
           IF IND-PCO-DT-ULT-ELAB-COS-AT = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-COS-AT-NULL
           END-IF
           IF IND-PCO-FRQ-COSTI-ATT = -1
              MOVE HIGH-VALUES TO PCO-FRQ-COSTI-ATT-NULL
           END-IF
           IF IND-PCO-DT-ULT-ELAB-COS-ST = -1
              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-COS-ST-NULL
           END-IF
           IF IND-PCO-FRQ-COSTI-STORNATI = -1
              MOVE HIGH-VALUES TO PCO-FRQ-COSTI-STORNATI-NULL
           END-IF
           IF IND-PCO-DT-ESTR-ASS-MIN70A = -1
              MOVE HIGH-VALUES TO PCO-DT-ESTR-ASS-MIN70A-NULL
           END-IF
           IF IND-PCO-DT-ESTR-ASS-MAG70A = -1
              MOVE HIGH-VALUES TO PCO-DT-ESTR-ASS-MAG70A-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO PCO-DS-OPER-SQL
           MOVE 0                   TO PCO-DS-VER
           MOVE IDSV0003-USER-NAME TO PCO-DS-UTENTE
           MOVE '1'                   TO PCO-DS-STATO-ELAB
           MOVE WS-TS-COMPETENZA-AGG-STOR TO PCO-DS-TS-CPTZ.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO PCO-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO PCO-DS-UTENTE
           MOVE WS-TS-COMPETENZA-AGG-STOR TO PCO-DS-TS-CPTZ.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF PCO-COD-TRAT-CIRT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-COD-TRAT-CIRT
           ELSE
              MOVE 0 TO IND-PCO-COD-TRAT-CIRT
           END-IF
           IF PCO-LIM-VLTR-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-LIM-VLTR
           ELSE
              MOVE 0 TO IND-PCO-LIM-VLTR
           END-IF
           IF PCO-TP-RAT-PERF-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-TP-RAT-PERF
           ELSE
              MOVE 0 TO IND-PCO-TP-RAT-PERF
           END-IF
           IF PCO-ARROT-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-ARROT-PRE
           ELSE
              MOVE 0 TO IND-PCO-ARROT-PRE
           END-IF
           IF PCO-DT-CONT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-CONT
           ELSE
              MOVE 0 TO IND-PCO-DT-CONT
           END-IF
           IF PCO-DT-ULT-RIVAL-IN-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-RIVAL-IN
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-RIVAL-IN
           END-IF
           IF PCO-DT-ULT-QTZO-IN-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-QTZO-IN
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-QTZO-IN
           END-IF
           IF PCO-DT-ULT-RICL-RIASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-RICL-RIASS
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-RICL-RIASS
           END-IF
           IF PCO-DT-ULT-TABUL-RIASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-TABUL-RIASS
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-TABUL-RIASS
           END-IF
           IF PCO-DT-ULT-BOLL-EMES-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-EMES
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-EMES
           END-IF
           IF PCO-DT-ULT-BOLL-STOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-STOR
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-STOR
           END-IF
           IF PCO-DT-ULT-BOLL-LIQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-LIQ
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-LIQ
           END-IF
           IF PCO-DT-ULT-BOLL-RIAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-RIAT
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-RIAT
           END-IF
           IF PCO-DT-ULTELRISCPAR-PR-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTELRISCPAR-PR
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTELRISCPAR-PR
           END-IF
           IF PCO-DT-ULTC-IS-IN-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTC-IS-IN
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTC-IS-IN
           END-IF
           IF PCO-DT-ULT-RICL-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-RICL-PRE
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-RICL-PRE
           END-IF
           IF PCO-DT-ULTC-MARSOL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTC-MARSOL
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTC-MARSOL
           END-IF
           IF PCO-DT-ULTC-RB-IN-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTC-RB-IN
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTC-RB-IN
           END-IF
           IF PCO-PC-PROV-1AA-ACQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-PC-PROV-1AA-ACQ
           ELSE
              MOVE 0 TO IND-PCO-PC-PROV-1AA-ACQ
           END-IF
           IF PCO-MOD-INTR-PREST-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-MOD-INTR-PREST
           ELSE
              MOVE 0 TO IND-PCO-MOD-INTR-PREST
           END-IF
           IF PCO-GG-MAX-REC-PROV-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-GG-MAX-REC-PROV
           ELSE
              MOVE 0 TO IND-PCO-GG-MAX-REC-PROV
           END-IF
           IF PCO-DT-ULTGZ-TRCH-E-IN-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTGZ-TRCH-E-IN
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTGZ-TRCH-E-IN
           END-IF
           IF PCO-DT-ULT-BOLL-SNDEN-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-SNDEN
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-SNDEN
           END-IF
           IF PCO-DT-ULT-BOLL-SNDNLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-SNDNLQ
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-SNDNLQ
           END-IF
           IF PCO-DT-ULTSC-ELAB-IN-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTSC-ELAB-IN
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTSC-ELAB-IN
           END-IF
           IF PCO-DT-ULTSC-OPZ-IN-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTSC-OPZ-IN
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTSC-OPZ-IN
           END-IF
           IF PCO-DT-ULTC-BNSRIC-IN-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTC-BNSRIC-IN
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTC-BNSRIC-IN
           END-IF
           IF PCO-DT-ULTC-BNSFDT-IN-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTC-BNSFDT-IN
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTC-BNSFDT-IN
           END-IF
           IF PCO-DT-ULT-RINN-GARAC-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-RINN-GARAC
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-RINN-GARAC
           END-IF
           IF PCO-DT-ULTGZ-CED-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTGZ-CED
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTGZ-CED
           END-IF
           IF PCO-DT-ULT-ELAB-PRLCOS-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-ELAB-PRLCOS
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-ELAB-PRLCOS
           END-IF
           IF PCO-DT-ULT-RINN-COLL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-RINN-COLL
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-RINN-COLL
           END-IF
           IF PCO-FL-RVC-PERF-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-FL-RVC-PERF
           ELSE
              MOVE 0 TO IND-PCO-FL-RVC-PERF
           END-IF
           IF PCO-FL-RCS-POLI-NOPERF-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-FL-RCS-POLI-NOPERF
           ELSE
              MOVE 0 TO IND-PCO-FL-RCS-POLI-NOPERF
           END-IF
           IF PCO-FL-GEST-PLUSV-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-FL-GEST-PLUSV
           ELSE
              MOVE 0 TO IND-PCO-FL-GEST-PLUSV
           END-IF
           IF PCO-DT-ULT-RIVAL-CL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-RIVAL-CL
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-RIVAL-CL
           END-IF
           IF PCO-DT-ULT-QTZO-CL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-QTZO-CL
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-QTZO-CL
           END-IF
           IF PCO-DT-ULTC-BNSRIC-CL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTC-BNSRIC-CL
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTC-BNSRIC-CL
           END-IF
           IF PCO-DT-ULTC-BNSFDT-CL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTC-BNSFDT-CL
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTC-BNSFDT-CL
           END-IF
           IF PCO-DT-ULTC-IS-CL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTC-IS-CL
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTC-IS-CL
           END-IF
           IF PCO-DT-ULTC-RB-CL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTC-RB-CL
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTC-RB-CL
           END-IF
           IF PCO-DT-ULTGZ-TRCH-E-CL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTGZ-TRCH-E-CL
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTGZ-TRCH-E-CL
           END-IF
           IF PCO-DT-ULTSC-ELAB-CL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTSC-ELAB-CL
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTSC-ELAB-CL
           END-IF
           IF PCO-DT-ULTSC-OPZ-CL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTSC-OPZ-CL
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTSC-OPZ-CL
           END-IF
           IF PCO-STST-X-REGIONE-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-STST-X-REGIONE
           ELSE
              MOVE 0 TO IND-PCO-STST-X-REGIONE
           END-IF
           IF PCO-DT-ULTGZ-CED-COLL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTGZ-CED-COLL
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTGZ-CED-COLL
           END-IF
           IF PCO-NUM-MM-CALC-MORA-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-NUM-MM-CALC-MORA
           ELSE
              MOVE 0 TO IND-PCO-NUM-MM-CALC-MORA
           END-IF
           IF PCO-DT-ULT-EC-RIV-COLL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-EC-RIV-COLL
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-EC-RIV-COLL
           END-IF
           IF PCO-DT-ULT-EC-RIV-IND-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-EC-RIV-IND
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-EC-RIV-IND
           END-IF
           IF PCO-DT-ULT-EC-IL-COLL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-EC-IL-COLL
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-EC-IL-COLL
           END-IF
           IF PCO-DT-ULT-EC-IL-IND-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-EC-IL-IND
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-EC-IL-IND
           END-IF
           IF PCO-DT-ULT-EC-UL-COLL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-EC-UL-COLL
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-EC-UL-COLL
           END-IF
           IF PCO-DT-ULT-EC-UL-IND-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-EC-UL-IND
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-EC-UL-IND
           END-IF
           IF PCO-AA-UTI-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-AA-UTI
           ELSE
              MOVE 0 TO IND-PCO-AA-UTI
           END-IF
           IF PCO-DT-ULT-BOLL-PERF-C-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-PERF-C
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-PERF-C
           END-IF
           IF PCO-DT-ULT-BOLL-RSP-IN-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-RSP-IN
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-RSP-IN
           END-IF
           IF PCO-DT-ULT-BOLL-RSP-CL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-RSP-CL
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-RSP-CL
           END-IF
           IF PCO-DT-ULT-BOLL-EMES-I-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-EMES-I
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-EMES-I
           END-IF
           IF PCO-DT-ULT-BOLL-STOR-I-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-STOR-I
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-STOR-I
           END-IF
           IF PCO-DT-ULT-BOLL-RIAT-I-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-RIAT-I
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-RIAT-I
           END-IF
           IF PCO-DT-ULT-BOLL-SD-I-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-SD-I
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-SD-I
           END-IF
           IF PCO-DT-ULT-BOLL-SDNL-I-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-SDNL-I
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-SDNL-I
           END-IF
           IF PCO-DT-ULT-BOLL-PERF-I-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-PERF-I
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-PERF-I
           END-IF
           IF PCO-DT-RICL-RIRIAS-COM-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-RICL-RIRIAS-COM
           ELSE
              MOVE 0 TO IND-PCO-DT-RICL-RIRIAS-COM
           END-IF
           IF PCO-DT-ULT-ELAB-AT92-C-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-ELAB-AT92-C
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-ELAB-AT92-C
           END-IF
           IF PCO-DT-ULT-ELAB-AT92-I-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-ELAB-AT92-I
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-ELAB-AT92-I
           END-IF
           IF PCO-DT-ULT-ELAB-AT93-C-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-ELAB-AT93-C
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-ELAB-AT93-C
           END-IF
           IF PCO-DT-ULT-ELAB-AT93-I-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-ELAB-AT93-I
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-ELAB-AT93-I
           END-IF
           IF PCO-DT-ULT-ELAB-SPE-IN-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-ELAB-SPE-IN
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-ELAB-SPE-IN
           END-IF
           IF PCO-DT-ULT-ELAB-PR-CON-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-ELAB-PR-CON
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-ELAB-PR-CON
           END-IF
           IF PCO-DT-ULT-BOLL-RP-CL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-RP-CL
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-RP-CL
           END-IF
           IF PCO-DT-ULT-BOLL-RP-IN-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-RP-IN
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-RP-IN
           END-IF
           IF PCO-DT-ULT-BOLL-PRE-I-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-PRE-I
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-PRE-I
           END-IF
           IF PCO-DT-ULT-BOLL-PRE-C-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-PRE-C
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-PRE-C
           END-IF
           IF PCO-DT-ULTC-PILDI-MM-C-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTC-PILDI-MM-C
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTC-PILDI-MM-C
           END-IF
           IF PCO-DT-ULTC-PILDI-AA-C-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTC-PILDI-AA-C
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTC-PILDI-AA-C
           END-IF
           IF PCO-DT-ULTC-PILDI-MM-I-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTC-PILDI-MM-I
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTC-PILDI-MM-I
           END-IF
           IF PCO-DT-ULTC-PILDI-TR-I-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTC-PILDI-TR-I
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTC-PILDI-TR-I
           END-IF
           IF PCO-DT-ULTC-PILDI-AA-I-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULTC-PILDI-AA-I
           ELSE
              MOVE 0 TO IND-PCO-DT-ULTC-PILDI-AA-I
           END-IF
           IF PCO-DT-ULT-BOLL-QUIE-C-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-QUIE-C
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-QUIE-C
           END-IF
           IF PCO-DT-ULT-BOLL-QUIE-I-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-QUIE-I
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-QUIE-I
           END-IF
           IF PCO-DT-ULT-BOLL-COTR-I-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-COTR-I
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-COTR-I
           END-IF
           IF PCO-DT-ULT-BOLL-COTR-C-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-COTR-C
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-COTR-C
           END-IF
           IF PCO-DT-ULT-BOLL-CORI-C-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-CORI-C
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-CORI-C
           END-IF
           IF PCO-DT-ULT-BOLL-CORI-I-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-BOLL-CORI-I
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-BOLL-CORI-I
           END-IF
           IF PCO-TP-VALZZ-DT-VLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-TP-VALZZ-DT-VLT
           ELSE
              MOVE 0 TO IND-PCO-TP-VALZZ-DT-VLT
           END-IF
           IF PCO-FL-FRAZ-PROV-ACQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-FL-FRAZ-PROV-ACQ
           ELSE
              MOVE 0 TO IND-PCO-FL-FRAZ-PROV-ACQ
           END-IF
           IF PCO-DT-ULT-AGG-EROG-RE-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-AGG-EROG-RE
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-AGG-EROG-RE
           END-IF
           IF PCO-PC-RM-MARSOL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-PC-RM-MARSOL
           ELSE
              MOVE 0 TO IND-PCO-PC-RM-MARSOL
           END-IF
           IF PCO-PC-C-SUBRSH-MARSOL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-PC-C-SUBRSH-MARSOL
           ELSE
              MOVE 0 TO IND-PCO-PC-C-SUBRSH-MARSOL
           END-IF
           IF PCO-COD-COMP-ISVAP-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-COD-COMP-ISVAP
           ELSE
              MOVE 0 TO IND-PCO-COD-COMP-ISVAP
           END-IF
           IF PCO-LM-RIS-CON-INT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-LM-RIS-CON-INT
           ELSE
              MOVE 0 TO IND-PCO-LM-RIS-CON-INT
           END-IF
           IF PCO-LM-C-SUBRSH-CON-IN-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-LM-C-SUBRSH-CON-IN
           ELSE
              MOVE 0 TO IND-PCO-LM-C-SUBRSH-CON-IN
           END-IF
           IF PCO-PC-GAR-NORISK-MARS-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-PC-GAR-NORISK-MARS
           ELSE
              MOVE 0 TO IND-PCO-PC-GAR-NORISK-MARS
           END-IF
           IF PCO-CRZ-1A-RAT-INTR-PR-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-CRZ-1A-RAT-INTR-PR
           ELSE
              MOVE 0 TO IND-PCO-CRZ-1A-RAT-INTR-PR
           END-IF
           IF PCO-NUM-GG-ARR-INTR-PR-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-NUM-GG-ARR-INTR-PR
           ELSE
              MOVE 0 TO IND-PCO-NUM-GG-ARR-INTR-PR
           END-IF
           IF PCO-FL-VISUAL-VINPG-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-FL-VISUAL-VINPG
           ELSE
              MOVE 0 TO IND-PCO-FL-VISUAL-VINPG
           END-IF
           IF PCO-DT-ULT-ESTRAZ-FUG-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-ESTRAZ-FUG
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-ESTRAZ-FUG
           END-IF
           IF PCO-DT-ULT-ELAB-PR-AUT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-ELAB-PR-AUT
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-ELAB-PR-AUT
           END-IF
           IF PCO-DT-ULT-ELAB-COMMEF-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-ELAB-COMMEF
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-ELAB-COMMEF
           END-IF
           IF PCO-DT-ULT-ELAB-LIQMEF-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-ELAB-LIQMEF
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-ELAB-LIQMEF
           END-IF
           IF PCO-COD-FISC-MEF-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-COD-FISC-MEF
           ELSE
              MOVE 0 TO IND-PCO-COD-FISC-MEF
           END-IF
           IF PCO-IMP-ASS-SOCIALE-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-IMP-ASS-SOCIALE
           ELSE
              MOVE 0 TO IND-PCO-IMP-ASS-SOCIALE
           END-IF
           IF PCO-MOD-COMNZ-INVST-SW-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-MOD-COMNZ-INVST-SW
           ELSE
              MOVE 0 TO IND-PCO-MOD-COMNZ-INVST-SW
           END-IF
           IF PCO-DT-RIAT-RIASS-RSH-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-RIAT-RIASS-RSH
           ELSE
              MOVE 0 TO IND-PCO-DT-RIAT-RIASS-RSH
           END-IF
           IF PCO-DT-RIAT-RIASS-COMM-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-RIAT-RIASS-COMM
           ELSE
              MOVE 0 TO IND-PCO-DT-RIAT-RIASS-COMM
           END-IF
           IF PCO-GG-INTR-RIT-PAG-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-GG-INTR-RIT-PAG
           ELSE
              MOVE 0 TO IND-PCO-GG-INTR-RIT-PAG
           END-IF
           IF PCO-DT-ULT-RINN-TAC-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-RINN-TAC
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-RINN-TAC
           END-IF
           IF PCO-DESC-COMP = HIGH-VALUES
              MOVE -1 TO IND-PCO-DESC-COMP
           ELSE
              MOVE 0 TO IND-PCO-DESC-COMP
           END-IF
           IF PCO-DT-ULT-EC-TCM-IND-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-EC-TCM-IND
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-EC-TCM-IND
           END-IF
           IF PCO-DT-ULT-EC-TCM-COLL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-EC-TCM-COLL
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-EC-TCM-COLL
           END-IF
           IF PCO-DT-ULT-EC-MRM-IND-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-EC-MRM-IND
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-EC-MRM-IND
           END-IF
           IF PCO-DT-ULT-EC-MRM-COLL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-EC-MRM-COLL
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-EC-MRM-COLL
           END-IF
           IF PCO-COD-COMP-LDAP-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-COD-COMP-LDAP
           ELSE
              MOVE 0 TO IND-PCO-COD-COMP-LDAP
           END-IF
           IF PCO-PC-RID-IMP-1382011-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-PC-RID-IMP-1382011
           ELSE
              MOVE 0 TO IND-PCO-PC-RID-IMP-1382011
           END-IF
           IF PCO-PC-RID-IMP-662014-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-PC-RID-IMP-662014
           ELSE
              MOVE 0 TO IND-PCO-PC-RID-IMP-662014
           END-IF
           IF PCO-SOGL-AML-PRE-UNI-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-SOGL-AML-PRE-UNI
           ELSE
              MOVE 0 TO IND-PCO-SOGL-AML-PRE-UNI
           END-IF
           IF PCO-SOGL-AML-PRE-PER-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-SOGL-AML-PRE-PER
           ELSE
              MOVE 0 TO IND-PCO-SOGL-AML-PRE-PER
           END-IF
           IF PCO-COD-SOGG-FTZ-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-COD-SOGG-FTZ-ASSTO
           ELSE
              MOVE 0 TO IND-PCO-COD-SOGG-FTZ-ASSTO
           END-IF
           IF PCO-DT-ULT-ELAB-REDPRO-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-ELAB-REDPRO
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-ELAB-REDPRO
           END-IF
           IF PCO-DT-ULT-ELAB-TAKE-P-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-ELAB-TAKE-P
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-ELAB-TAKE-P
           END-IF
           IF PCO-DT-ULT-ELAB-PASPAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-ELAB-PASPAS
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-ELAB-PASPAS
           END-IF
           IF PCO-SOGL-AML-PRE-SAV-R-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-SOGL-AML-PRE-SAV-R
           ELSE
              MOVE 0 TO IND-PCO-SOGL-AML-PRE-SAV-R
           END-IF
           IF PCO-DT-ULT-ESTR-DEC-CO-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-ESTR-DEC-CO
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-ESTR-DEC-CO
           END-IF
           IF PCO-DT-ULT-ELAB-COS-AT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-ELAB-COS-AT
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-ELAB-COS-AT
           END-IF
           IF PCO-FRQ-COSTI-ATT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-FRQ-COSTI-ATT
           ELSE
              MOVE 0 TO IND-PCO-FRQ-COSTI-ATT
           END-IF
           IF PCO-DT-ULT-ELAB-COS-ST-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ULT-ELAB-COS-ST
           ELSE
              MOVE 0 TO IND-PCO-DT-ULT-ELAB-COS-ST
           END-IF
           IF PCO-FRQ-COSTI-STORNATI-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-FRQ-COSTI-STORNATI
           ELSE
              MOVE 0 TO IND-PCO-FRQ-COSTI-STORNATI
           END-IF
           IF PCO-DT-ESTR-ASS-MIN70A-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ESTR-ASS-MIN70A
           ELSE
              MOVE 0 TO IND-PCO-DT-ESTR-ASS-MIN70A
           END-IF
           IF PCO-DT-ESTR-ASS-MAG70A-NULL = HIGH-VALUES
              MOVE -1 TO IND-PCO-DT-ESTR-ASS-MAG70A
           ELSE
              MOVE 0 TO IND-PCO-DT-ESTR-ASS-MAG70A
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.

           CONTINUE.
       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.
           CONTINUE.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.
           CONTINUE.
       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.
           CONTINUE.
       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           IF IND-PCO-DT-CONT = 0
               MOVE PCO-DT-CONT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-CONT-DB
           END-IF
           IF IND-PCO-DT-ULT-RIVAL-IN = 0
               MOVE PCO-DT-ULT-RIVAL-IN TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-RIVAL-IN-DB
           END-IF
           IF IND-PCO-DT-ULT-QTZO-IN = 0
               MOVE PCO-DT-ULT-QTZO-IN TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-QTZO-IN-DB
           END-IF
           IF IND-PCO-DT-ULT-RICL-RIASS = 0
               MOVE PCO-DT-ULT-RICL-RIASS TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-RICL-RIASS-DB
           END-IF
           IF IND-PCO-DT-ULT-TABUL-RIASS = 0
               MOVE PCO-DT-ULT-TABUL-RIASS TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-TABUL-RIASS-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-EMES = 0
               MOVE PCO-DT-ULT-BOLL-EMES TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-EMES-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-STOR = 0
               MOVE PCO-DT-ULT-BOLL-STOR TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-STOR-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-LIQ = 0
               MOVE PCO-DT-ULT-BOLL-LIQ TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-LIQ-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-RIAT = 0
               MOVE PCO-DT-ULT-BOLL-RIAT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-RIAT-DB
           END-IF
           IF IND-PCO-DT-ULTELRISCPAR-PR = 0
               MOVE PCO-DT-ULTELRISCPAR-PR TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTELRISCPAR-PR-DB
           END-IF
           IF IND-PCO-DT-ULTC-IS-IN = 0
               MOVE PCO-DT-ULTC-IS-IN TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTC-IS-IN-DB
           END-IF
           IF IND-PCO-DT-ULT-RICL-PRE = 0
               MOVE PCO-DT-ULT-RICL-PRE TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-RICL-PRE-DB
           END-IF
           IF IND-PCO-DT-ULTC-MARSOL = 0
               MOVE PCO-DT-ULTC-MARSOL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTC-MARSOL-DB
           END-IF
           IF IND-PCO-DT-ULTC-RB-IN = 0
               MOVE PCO-DT-ULTC-RB-IN TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTC-RB-IN-DB
           END-IF
           IF IND-PCO-DT-ULTGZ-TRCH-E-IN = 0
               MOVE PCO-DT-ULTGZ-TRCH-E-IN TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTGZ-TRCH-E-IN-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-SNDEN = 0
               MOVE PCO-DT-ULT-BOLL-SNDEN TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-SNDEN-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-SNDNLQ = 0
               MOVE PCO-DT-ULT-BOLL-SNDNLQ TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-SNDNLQ-DB
           END-IF
           IF IND-PCO-DT-ULTSC-ELAB-IN = 0
               MOVE PCO-DT-ULTSC-ELAB-IN TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTSC-ELAB-IN-DB
           END-IF
           IF IND-PCO-DT-ULTSC-OPZ-IN = 0
               MOVE PCO-DT-ULTSC-OPZ-IN TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTSC-OPZ-IN-DB
           END-IF
           IF IND-PCO-DT-ULTC-BNSRIC-IN = 0
               MOVE PCO-DT-ULTC-BNSRIC-IN TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTC-BNSRIC-IN-DB
           END-IF
           IF IND-PCO-DT-ULTC-BNSFDT-IN = 0
               MOVE PCO-DT-ULTC-BNSFDT-IN TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTC-BNSFDT-IN-DB
           END-IF
           IF IND-PCO-DT-ULT-RINN-GARAC = 0
               MOVE PCO-DT-ULT-RINN-GARAC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-RINN-GARAC-DB
           END-IF
           IF IND-PCO-DT-ULTGZ-CED = 0
               MOVE PCO-DT-ULTGZ-CED TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTGZ-CED-DB
           END-IF
           IF IND-PCO-DT-ULT-ELAB-PRLCOS = 0
               MOVE PCO-DT-ULT-ELAB-PRLCOS TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-PRLCOS-DB
           END-IF
           IF IND-PCO-DT-ULT-RINN-COLL = 0
               MOVE PCO-DT-ULT-RINN-COLL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-RINN-COLL-DB
           END-IF
           IF IND-PCO-DT-ULT-RIVAL-CL = 0
               MOVE PCO-DT-ULT-RIVAL-CL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-RIVAL-CL-DB
           END-IF
           IF IND-PCO-DT-ULT-QTZO-CL = 0
               MOVE PCO-DT-ULT-QTZO-CL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-QTZO-CL-DB
           END-IF
           IF IND-PCO-DT-ULTC-BNSRIC-CL = 0
               MOVE PCO-DT-ULTC-BNSRIC-CL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTC-BNSRIC-CL-DB
           END-IF
           IF IND-PCO-DT-ULTC-BNSFDT-CL = 0
               MOVE PCO-DT-ULTC-BNSFDT-CL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTC-BNSFDT-CL-DB
           END-IF
           IF IND-PCO-DT-ULTC-IS-CL = 0
               MOVE PCO-DT-ULTC-IS-CL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTC-IS-CL-DB
           END-IF
           IF IND-PCO-DT-ULTC-RB-CL = 0
               MOVE PCO-DT-ULTC-RB-CL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTC-RB-CL-DB
           END-IF
           IF IND-PCO-DT-ULTGZ-TRCH-E-CL = 0
               MOVE PCO-DT-ULTGZ-TRCH-E-CL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTGZ-TRCH-E-CL-DB
           END-IF
           IF IND-PCO-DT-ULTSC-ELAB-CL = 0
               MOVE PCO-DT-ULTSC-ELAB-CL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTSC-ELAB-CL-DB
           END-IF
           IF IND-PCO-DT-ULTSC-OPZ-CL = 0
               MOVE PCO-DT-ULTSC-OPZ-CL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTSC-OPZ-CL-DB
           END-IF
           IF IND-PCO-STST-X-REGIONE = 0
               MOVE PCO-STST-X-REGIONE TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-STST-X-REGIONE-DB
           END-IF
           IF IND-PCO-DT-ULTGZ-CED-COLL = 0
               MOVE PCO-DT-ULTGZ-CED-COLL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTGZ-CED-COLL-DB
           END-IF
           IF IND-PCO-DT-ULT-EC-RIV-COLL = 0
               MOVE PCO-DT-ULT-EC-RIV-COLL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-RIV-COLL-DB
           END-IF
           IF IND-PCO-DT-ULT-EC-RIV-IND = 0
               MOVE PCO-DT-ULT-EC-RIV-IND TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-RIV-IND-DB
           END-IF
           IF IND-PCO-DT-ULT-EC-IL-COLL = 0
               MOVE PCO-DT-ULT-EC-IL-COLL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-IL-COLL-DB
           END-IF
           IF IND-PCO-DT-ULT-EC-IL-IND = 0
               MOVE PCO-DT-ULT-EC-IL-IND TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-IL-IND-DB
           END-IF
           IF IND-PCO-DT-ULT-EC-UL-COLL = 0
               MOVE PCO-DT-ULT-EC-UL-COLL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-UL-COLL-DB
           END-IF
           IF IND-PCO-DT-ULT-EC-UL-IND = 0
               MOVE PCO-DT-ULT-EC-UL-IND TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-UL-IND-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-PERF-C = 0
               MOVE PCO-DT-ULT-BOLL-PERF-C TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-PERF-C-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-RSP-IN = 0
               MOVE PCO-DT-ULT-BOLL-RSP-IN TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-RSP-IN-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-RSP-CL = 0
               MOVE PCO-DT-ULT-BOLL-RSP-CL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-RSP-CL-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-EMES-I = 0
               MOVE PCO-DT-ULT-BOLL-EMES-I TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-EMES-I-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-STOR-I = 0
               MOVE PCO-DT-ULT-BOLL-STOR-I TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-STOR-I-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-RIAT-I = 0
               MOVE PCO-DT-ULT-BOLL-RIAT-I TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-RIAT-I-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-SD-I = 0
               MOVE PCO-DT-ULT-BOLL-SD-I TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-SD-I-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-SDNL-I = 0
               MOVE PCO-DT-ULT-BOLL-SDNL-I TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-SDNL-I-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-PERF-I = 0
               MOVE PCO-DT-ULT-BOLL-PERF-I TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-PERF-I-DB
           END-IF
           IF IND-PCO-DT-RICL-RIRIAS-COM = 0
               MOVE PCO-DT-RICL-RIRIAS-COM TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-RICL-RIRIAS-COM-DB
           END-IF
           IF IND-PCO-DT-ULT-ELAB-AT92-C = 0
               MOVE PCO-DT-ULT-ELAB-AT92-C TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-AT92-C-DB
           END-IF
           IF IND-PCO-DT-ULT-ELAB-AT92-I = 0
               MOVE PCO-DT-ULT-ELAB-AT92-I TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-AT92-I-DB
           END-IF
           IF IND-PCO-DT-ULT-ELAB-AT93-C = 0
               MOVE PCO-DT-ULT-ELAB-AT93-C TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-AT93-C-DB
           END-IF
           IF IND-PCO-DT-ULT-ELAB-AT93-I = 0
               MOVE PCO-DT-ULT-ELAB-AT93-I TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-AT93-I-DB
           END-IF
           IF IND-PCO-DT-ULT-ELAB-SPE-IN = 0
               MOVE PCO-DT-ULT-ELAB-SPE-IN TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-SPE-IN-DB
           END-IF
           IF IND-PCO-DT-ULT-ELAB-PR-CON = 0
               MOVE PCO-DT-ULT-ELAB-PR-CON TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-PR-CON-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-RP-CL = 0
               MOVE PCO-DT-ULT-BOLL-RP-CL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-RP-CL-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-RP-IN = 0
               MOVE PCO-DT-ULT-BOLL-RP-IN TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-RP-IN-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-PRE-I = 0
               MOVE PCO-DT-ULT-BOLL-PRE-I TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-PRE-I-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-PRE-C = 0
               MOVE PCO-DT-ULT-BOLL-PRE-C TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-PRE-C-DB
           END-IF
           IF IND-PCO-DT-ULTC-PILDI-MM-C = 0
               MOVE PCO-DT-ULTC-PILDI-MM-C TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTC-PILDI-MM-C-DB
           END-IF
           IF IND-PCO-DT-ULTC-PILDI-AA-C = 0
               MOVE PCO-DT-ULTC-PILDI-AA-C TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTC-PILDI-AA-C-DB
           END-IF
           IF IND-PCO-DT-ULTC-PILDI-MM-I = 0
               MOVE PCO-DT-ULTC-PILDI-MM-I TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTC-PILDI-MM-I-DB
           END-IF
           IF IND-PCO-DT-ULTC-PILDI-TR-I = 0
               MOVE PCO-DT-ULTC-PILDI-TR-I TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTC-PILDI-TR-I-DB
           END-IF
           IF IND-PCO-DT-ULTC-PILDI-AA-I = 0
               MOVE PCO-DT-ULTC-PILDI-AA-I TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULTC-PILDI-AA-I-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-QUIE-C = 0
               MOVE PCO-DT-ULT-BOLL-QUIE-C TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-QUIE-C-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-QUIE-I = 0
               MOVE PCO-DT-ULT-BOLL-QUIE-I TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-QUIE-I-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-COTR-I = 0
               MOVE PCO-DT-ULT-BOLL-COTR-I TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-COTR-I-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-COTR-C = 0
               MOVE PCO-DT-ULT-BOLL-COTR-C TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-COTR-C-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-CORI-C = 0
               MOVE PCO-DT-ULT-BOLL-CORI-C TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-CORI-C-DB
           END-IF
           IF IND-PCO-DT-ULT-BOLL-CORI-I = 0
               MOVE PCO-DT-ULT-BOLL-CORI-I TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-CORI-I-DB
           END-IF
           IF IND-PCO-DT-ULT-AGG-EROG-RE = 0
               MOVE PCO-DT-ULT-AGG-EROG-RE TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-AGG-EROG-RE-DB
           END-IF
           IF IND-PCO-DT-ULT-ESTRAZ-FUG = 0
               MOVE PCO-DT-ULT-ESTRAZ-FUG TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-ESTRAZ-FUG-DB
           END-IF
           IF IND-PCO-DT-ULT-ELAB-PR-AUT = 0
               MOVE PCO-DT-ULT-ELAB-PR-AUT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-PR-AUT-DB
           END-IF
           IF IND-PCO-DT-ULT-ELAB-COMMEF = 0
               MOVE PCO-DT-ULT-ELAB-COMMEF TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-COMMEF-DB
           END-IF
           IF IND-PCO-DT-ULT-ELAB-LIQMEF = 0
               MOVE PCO-DT-ULT-ELAB-LIQMEF TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-LIQMEF-DB
           END-IF
           IF IND-PCO-DT-RIAT-RIASS-RSH = 0
               MOVE PCO-DT-RIAT-RIASS-RSH TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-RIAT-RIASS-RSH-DB
           END-IF
           IF IND-PCO-DT-RIAT-RIASS-COMM = 0
               MOVE PCO-DT-RIAT-RIASS-COMM TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-RIAT-RIASS-COMM-DB
           END-IF
           IF IND-PCO-DT-ULT-RINN-TAC = 0
               MOVE PCO-DT-ULT-RINN-TAC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-RINN-TAC-DB
           END-IF
           IF IND-PCO-DT-ULT-EC-TCM-IND = 0
               MOVE PCO-DT-ULT-EC-TCM-IND TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-TCM-IND-DB
           END-IF
           IF IND-PCO-DT-ULT-EC-TCM-COLL = 0
               MOVE PCO-DT-ULT-EC-TCM-COLL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-TCM-COLL-DB
           END-IF
           IF IND-PCO-DT-ULT-EC-MRM-IND = 0
               MOVE PCO-DT-ULT-EC-MRM-IND TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-MRM-IND-DB
           END-IF
           IF IND-PCO-DT-ULT-EC-MRM-COLL = 0
               MOVE PCO-DT-ULT-EC-MRM-COLL TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-MRM-COLL-DB
           END-IF
           IF IND-PCO-DT-ULT-ELAB-REDPRO = 0
               MOVE PCO-DT-ULT-ELAB-REDPRO TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-REDPRO-DB
           END-IF
           IF IND-PCO-DT-ULT-ELAB-TAKE-P = 0
               MOVE PCO-DT-ULT-ELAB-TAKE-P TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-TAKE-P-DB
           END-IF
           IF IND-PCO-DT-ULT-ELAB-PASPAS = 0
               MOVE PCO-DT-ULT-ELAB-PASPAS TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-PASPAS-DB
           END-IF
           IF IND-PCO-DT-ULT-ESTR-DEC-CO = 0
               MOVE PCO-DT-ULT-ESTR-DEC-CO TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-ESTR-DEC-CO-DB
           END-IF
           IF IND-PCO-DT-ULT-ELAB-COS-AT = 0
               MOVE PCO-DT-ULT-ELAB-COS-AT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-COS-AT-DB
           END-IF
           IF IND-PCO-DT-ULT-ELAB-COS-ST = 0
               MOVE PCO-DT-ULT-ELAB-COS-ST TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-COS-ST-DB
           END-IF
           IF IND-PCO-DT-ESTR-ASS-MIN70A = 0
               MOVE PCO-DT-ESTR-ASS-MIN70A TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ESTR-ASS-MIN70A-DB
           END-IF
           IF IND-PCO-DT-ESTR-ASS-MAG70A = 0
               MOVE PCO-DT-ESTR-ASS-MAG70A TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PCO-DT-ESTR-ASS-MAG70A-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           IF IND-PCO-DT-CONT = 0
               MOVE PCO-DT-CONT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-CONT
           END-IF
           IF IND-PCO-DT-ULT-RIVAL-IN = 0
               MOVE PCO-DT-ULT-RIVAL-IN-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-RIVAL-IN
           END-IF
           IF IND-PCO-DT-ULT-QTZO-IN = 0
               MOVE PCO-DT-ULT-QTZO-IN-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-QTZO-IN
           END-IF
           IF IND-PCO-DT-ULT-RICL-RIASS = 0
               MOVE PCO-DT-ULT-RICL-RIASS-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-RICL-RIASS
           END-IF
           IF IND-PCO-DT-ULT-TABUL-RIASS = 0
               MOVE PCO-DT-ULT-TABUL-RIASS-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-TABUL-RIASS
           END-IF
           IF IND-PCO-DT-ULT-BOLL-EMES = 0
               MOVE PCO-DT-ULT-BOLL-EMES-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-EMES
           END-IF
           IF IND-PCO-DT-ULT-BOLL-STOR = 0
               MOVE PCO-DT-ULT-BOLL-STOR-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-STOR
           END-IF
           IF IND-PCO-DT-ULT-BOLL-LIQ = 0
               MOVE PCO-DT-ULT-BOLL-LIQ-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-LIQ
           END-IF
           IF IND-PCO-DT-ULT-BOLL-RIAT = 0
               MOVE PCO-DT-ULT-BOLL-RIAT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-RIAT
           END-IF
           IF IND-PCO-DT-ULTELRISCPAR-PR = 0
               MOVE PCO-DT-ULTELRISCPAR-PR-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTELRISCPAR-PR
           END-IF
           IF IND-PCO-DT-ULTC-IS-IN = 0
               MOVE PCO-DT-ULTC-IS-IN-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTC-IS-IN
           END-IF
           IF IND-PCO-DT-ULT-RICL-PRE = 0
               MOVE PCO-DT-ULT-RICL-PRE-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-RICL-PRE
           END-IF
           IF IND-PCO-DT-ULTC-MARSOL = 0
               MOVE PCO-DT-ULTC-MARSOL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTC-MARSOL
           END-IF
           IF IND-PCO-DT-ULTC-RB-IN = 0
               MOVE PCO-DT-ULTC-RB-IN-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTC-RB-IN
           END-IF
           IF IND-PCO-DT-ULTGZ-TRCH-E-IN = 0
               MOVE PCO-DT-ULTGZ-TRCH-E-IN-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTGZ-TRCH-E-IN
           END-IF
           IF IND-PCO-DT-ULT-BOLL-SNDEN = 0
               MOVE PCO-DT-ULT-BOLL-SNDEN-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-SNDEN
           END-IF
           IF IND-PCO-DT-ULT-BOLL-SNDNLQ = 0
               MOVE PCO-DT-ULT-BOLL-SNDNLQ-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-SNDNLQ
           END-IF
           IF IND-PCO-DT-ULTSC-ELAB-IN = 0
               MOVE PCO-DT-ULTSC-ELAB-IN-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTSC-ELAB-IN
           END-IF
           IF IND-PCO-DT-ULTSC-OPZ-IN = 0
               MOVE PCO-DT-ULTSC-OPZ-IN-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTSC-OPZ-IN
           END-IF
           IF IND-PCO-DT-ULTC-BNSRIC-IN = 0
               MOVE PCO-DT-ULTC-BNSRIC-IN-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTC-BNSRIC-IN
           END-IF
           IF IND-PCO-DT-ULTC-BNSFDT-IN = 0
               MOVE PCO-DT-ULTC-BNSFDT-IN-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTC-BNSFDT-IN
           END-IF
           IF IND-PCO-DT-ULT-RINN-GARAC = 0
               MOVE PCO-DT-ULT-RINN-GARAC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-RINN-GARAC
           END-IF
           IF IND-PCO-DT-ULTGZ-CED = 0
               MOVE PCO-DT-ULTGZ-CED-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTGZ-CED
           END-IF
           IF IND-PCO-DT-ULT-ELAB-PRLCOS = 0
               MOVE PCO-DT-ULT-ELAB-PRLCOS-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-PRLCOS
           END-IF
           IF IND-PCO-DT-ULT-RINN-COLL = 0
               MOVE PCO-DT-ULT-RINN-COLL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-RINN-COLL
           END-IF
           IF IND-PCO-DT-ULT-RIVAL-CL = 0
               MOVE PCO-DT-ULT-RIVAL-CL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-RIVAL-CL
           END-IF
           IF IND-PCO-DT-ULT-QTZO-CL = 0
               MOVE PCO-DT-ULT-QTZO-CL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-QTZO-CL
           END-IF
           IF IND-PCO-DT-ULTC-BNSRIC-CL = 0
               MOVE PCO-DT-ULTC-BNSRIC-CL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTC-BNSRIC-CL
           END-IF
           IF IND-PCO-DT-ULTC-BNSFDT-CL = 0
               MOVE PCO-DT-ULTC-BNSFDT-CL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTC-BNSFDT-CL
           END-IF
           IF IND-PCO-DT-ULTC-IS-CL = 0
               MOVE PCO-DT-ULTC-IS-CL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTC-IS-CL
           END-IF
           IF IND-PCO-DT-ULTC-RB-CL = 0
               MOVE PCO-DT-ULTC-RB-CL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTC-RB-CL
           END-IF
           IF IND-PCO-DT-ULTGZ-TRCH-E-CL = 0
               MOVE PCO-DT-ULTGZ-TRCH-E-CL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTGZ-TRCH-E-CL
           END-IF
           IF IND-PCO-DT-ULTSC-ELAB-CL = 0
               MOVE PCO-DT-ULTSC-ELAB-CL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTSC-ELAB-CL
           END-IF
           IF IND-PCO-DT-ULTSC-OPZ-CL = 0
               MOVE PCO-DT-ULTSC-OPZ-CL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTSC-OPZ-CL
           END-IF
           IF IND-PCO-STST-X-REGIONE = 0
               MOVE PCO-STST-X-REGIONE-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-STST-X-REGIONE
           END-IF
           IF IND-PCO-DT-ULTGZ-CED-COLL = 0
               MOVE PCO-DT-ULTGZ-CED-COLL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTGZ-CED-COLL
           END-IF
           IF IND-PCO-DT-ULT-EC-RIV-COLL = 0
               MOVE PCO-DT-ULT-EC-RIV-COLL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-RIV-COLL
           END-IF
           IF IND-PCO-DT-ULT-EC-RIV-IND = 0
               MOVE PCO-DT-ULT-EC-RIV-IND-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-RIV-IND
           END-IF
           IF IND-PCO-DT-ULT-EC-IL-COLL = 0
               MOVE PCO-DT-ULT-EC-IL-COLL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-IL-COLL
           END-IF
           IF IND-PCO-DT-ULT-EC-IL-IND = 0
               MOVE PCO-DT-ULT-EC-IL-IND-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-IL-IND
           END-IF
           IF IND-PCO-DT-ULT-EC-UL-COLL = 0
               MOVE PCO-DT-ULT-EC-UL-COLL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-UL-COLL
           END-IF
           IF IND-PCO-DT-ULT-EC-UL-IND = 0
               MOVE PCO-DT-ULT-EC-UL-IND-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-UL-IND
           END-IF
           IF IND-PCO-DT-ULT-BOLL-PERF-C = 0
               MOVE PCO-DT-ULT-BOLL-PERF-C-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-PERF-C
           END-IF
           IF IND-PCO-DT-ULT-BOLL-RSP-IN = 0
               MOVE PCO-DT-ULT-BOLL-RSP-IN-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-RSP-IN
           END-IF
           IF IND-PCO-DT-ULT-BOLL-RSP-CL = 0
               MOVE PCO-DT-ULT-BOLL-RSP-CL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-RSP-CL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-EMES-I = 0
               MOVE PCO-DT-ULT-BOLL-EMES-I-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-EMES-I
           END-IF
           IF IND-PCO-DT-ULT-BOLL-STOR-I = 0
               MOVE PCO-DT-ULT-BOLL-STOR-I-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-STOR-I
           END-IF
           IF IND-PCO-DT-ULT-BOLL-RIAT-I = 0
               MOVE PCO-DT-ULT-BOLL-RIAT-I-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-RIAT-I
           END-IF
           IF IND-PCO-DT-ULT-BOLL-SD-I = 0
               MOVE PCO-DT-ULT-BOLL-SD-I-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-SD-I
           END-IF
           IF IND-PCO-DT-ULT-BOLL-SDNL-I = 0
               MOVE PCO-DT-ULT-BOLL-SDNL-I-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-SDNL-I
           END-IF
           IF IND-PCO-DT-ULT-BOLL-PERF-I = 0
               MOVE PCO-DT-ULT-BOLL-PERF-I-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-PERF-I
           END-IF
           IF IND-PCO-DT-RICL-RIRIAS-COM = 0
               MOVE PCO-DT-RICL-RIRIAS-COM-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-RICL-RIRIAS-COM
           END-IF
           IF IND-PCO-DT-ULT-ELAB-AT92-C = 0
               MOVE PCO-DT-ULT-ELAB-AT92-C-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-AT92-C
           END-IF
           IF IND-PCO-DT-ULT-ELAB-AT92-I = 0
               MOVE PCO-DT-ULT-ELAB-AT92-I-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-AT92-I
           END-IF
           IF IND-PCO-DT-ULT-ELAB-AT93-C = 0
               MOVE PCO-DT-ULT-ELAB-AT93-C-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-AT93-C
           END-IF
           IF IND-PCO-DT-ULT-ELAB-AT93-I = 0
               MOVE PCO-DT-ULT-ELAB-AT93-I-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-AT93-I
           END-IF
           IF IND-PCO-DT-ULT-ELAB-SPE-IN = 0
               MOVE PCO-DT-ULT-ELAB-SPE-IN-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-SPE-IN
           END-IF
           IF IND-PCO-DT-ULT-ELAB-PR-CON = 0
               MOVE PCO-DT-ULT-ELAB-PR-CON-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-PR-CON
           END-IF
           IF IND-PCO-DT-ULT-BOLL-RP-CL = 0
               MOVE PCO-DT-ULT-BOLL-RP-CL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-RP-CL
           END-IF
           IF IND-PCO-DT-ULT-BOLL-RP-IN = 0
               MOVE PCO-DT-ULT-BOLL-RP-IN-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-RP-IN
           END-IF
           IF IND-PCO-DT-ULT-BOLL-PRE-I = 0
               MOVE PCO-DT-ULT-BOLL-PRE-I-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-PRE-I
           END-IF
           IF IND-PCO-DT-ULT-BOLL-PRE-C = 0
               MOVE PCO-DT-ULT-BOLL-PRE-C-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-PRE-C
           END-IF
           IF IND-PCO-DT-ULTC-PILDI-MM-C = 0
               MOVE PCO-DT-ULTC-PILDI-MM-C-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTC-PILDI-MM-C
           END-IF
           IF IND-PCO-DT-ULTC-PILDI-AA-C = 0
               MOVE PCO-DT-ULTC-PILDI-AA-C-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTC-PILDI-AA-C
           END-IF
           IF IND-PCO-DT-ULTC-PILDI-MM-I = 0
               MOVE PCO-DT-ULTC-PILDI-MM-I-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTC-PILDI-MM-I
           END-IF
           IF IND-PCO-DT-ULTC-PILDI-TR-I = 0
               MOVE PCO-DT-ULTC-PILDI-TR-I-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTC-PILDI-TR-I
           END-IF
           IF IND-PCO-DT-ULTC-PILDI-AA-I = 0
               MOVE PCO-DT-ULTC-PILDI-AA-I-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULTC-PILDI-AA-I
           END-IF
           IF IND-PCO-DT-ULT-BOLL-QUIE-C = 0
               MOVE PCO-DT-ULT-BOLL-QUIE-C-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-QUIE-C
           END-IF
           IF IND-PCO-DT-ULT-BOLL-QUIE-I = 0
               MOVE PCO-DT-ULT-BOLL-QUIE-I-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-QUIE-I
           END-IF
           IF IND-PCO-DT-ULT-BOLL-COTR-I = 0
               MOVE PCO-DT-ULT-BOLL-COTR-I-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-COTR-I
           END-IF
           IF IND-PCO-DT-ULT-BOLL-COTR-C = 0
               MOVE PCO-DT-ULT-BOLL-COTR-C-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-COTR-C
           END-IF
           IF IND-PCO-DT-ULT-BOLL-CORI-C = 0
               MOVE PCO-DT-ULT-BOLL-CORI-C-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-CORI-C
           END-IF
           IF IND-PCO-DT-ULT-BOLL-CORI-I = 0
               MOVE PCO-DT-ULT-BOLL-CORI-I-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-CORI-I
           END-IF
           IF IND-PCO-DT-ULT-AGG-EROG-RE = 0
               MOVE PCO-DT-ULT-AGG-EROG-RE-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-AGG-EROG-RE
           END-IF
           IF IND-PCO-DT-ULT-ESTRAZ-FUG = 0
               MOVE PCO-DT-ULT-ESTRAZ-FUG-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-ESTRAZ-FUG
           END-IF
           IF IND-PCO-DT-ULT-ELAB-PR-AUT = 0
               MOVE PCO-DT-ULT-ELAB-PR-AUT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-PR-AUT
           END-IF
           IF IND-PCO-DT-ULT-ELAB-COMMEF = 0
               MOVE PCO-DT-ULT-ELAB-COMMEF-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-COMMEF
           END-IF
           IF IND-PCO-DT-ULT-ELAB-LIQMEF = 0
               MOVE PCO-DT-ULT-ELAB-LIQMEF-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-LIQMEF
           END-IF
           IF IND-PCO-DT-RIAT-RIASS-RSH = 0
               MOVE PCO-DT-RIAT-RIASS-RSH-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-RIAT-RIASS-RSH
           END-IF
           IF IND-PCO-DT-RIAT-RIASS-COMM = 0
               MOVE PCO-DT-RIAT-RIASS-COMM-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-RIAT-RIASS-COMM
           END-IF
           IF IND-PCO-DT-ULT-RINN-TAC = 0
               MOVE PCO-DT-ULT-RINN-TAC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-RINN-TAC
           END-IF
           IF IND-PCO-DT-ULT-EC-TCM-IND = 0
               MOVE PCO-DT-ULT-EC-TCM-IND-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-TCM-IND
           END-IF
           IF IND-PCO-DT-ULT-EC-TCM-COLL = 0
               MOVE PCO-DT-ULT-EC-TCM-COLL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-TCM-COLL
           END-IF
           IF IND-PCO-DT-ULT-EC-MRM-IND = 0
               MOVE PCO-DT-ULT-EC-MRM-IND-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-MRM-IND
           END-IF
           IF IND-PCO-DT-ULT-EC-MRM-COLL = 0
               MOVE PCO-DT-ULT-EC-MRM-COLL-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-MRM-COLL
           END-IF
           IF IND-PCO-DT-ULT-ELAB-REDPRO = 0
               MOVE PCO-DT-ULT-ELAB-REDPRO-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-REDPRO
           END-IF
           IF IND-PCO-DT-ULT-ELAB-TAKE-P = 0
               MOVE PCO-DT-ULT-ELAB-TAKE-P-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-TAKE-P
           END-IF
           IF IND-PCO-DT-ULT-ELAB-PASPAS = 0
               MOVE PCO-DT-ULT-ELAB-PASPAS-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-PASPAS
           END-IF
           IF IND-PCO-DT-ULT-ESTR-DEC-CO = 0
               MOVE PCO-DT-ULT-ESTR-DEC-CO-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-ESTR-DEC-CO
           END-IF
           IF IND-PCO-DT-ULT-ELAB-COS-AT = 0
               MOVE PCO-DT-ULT-ELAB-COS-AT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-COS-AT
           END-IF
           IF IND-PCO-DT-ULT-ELAB-COS-ST = 0
               MOVE PCO-DT-ULT-ELAB-COS-ST-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-COS-ST
           END-IF
           IF IND-PCO-DT-ESTR-ASS-MIN70A = 0
               MOVE PCO-DT-ESTR-ASS-MIN70A-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ESTR-ASS-MIN70A
           END-IF
           IF IND-PCO-DT-ESTR-ASS-MAG70A = 0
               MOVE PCO-DT-ESTR-ASS-MAG70A-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PCO-DT-ESTR-ASS-MAG70A
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

            CONTINUE.
ITRAT *     MOVE LENGTH OF PCO-DESC-COMP
ITRAT *                TO PCO-DESC-COMP-LEN.

       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

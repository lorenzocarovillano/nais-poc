       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LDBS5560 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  11 DIC 2017.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2     PIC X(300)  VALUE SPACES.

       77 WS-BUFFER-TABLE     PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ      PIC 9(009)  VALUE ZEROES.

           EXEC SQL INCLUDE IDSV0010 END-EXEC.

      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDPOL0 END-EXEC.
           EXEC SQL INCLUDE IDBVPOL2 END-EXEC.
           EXEC SQL INCLUDE IDBVPOL3 END-EXEC.
      *
      *
           EXEC SQL INCLUDE LDBV5561 END-EXEC.
      *
      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVPOL1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 POLI.

           MOVE IDSV0003-BUFFER-WHERE-COND TO LDBV5561.

           PERFORM A000-INIZIO              THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-WHERE-CONDITION
                       PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-WHERE-CONDITION
                         PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-WHERE-CONDITION
                           PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                      SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           MOVE LDBV5561 TO IDSV0003-BUFFER-WHERE-COND.

           GOBACK.

       A000-INIZIO.
           MOVE 'LDBS5560'              TO   IDSV0003-COD-SERVIZIO-BE.
           MOVE 'POLI' TO   IDSV0003-NOME-TABELLA.

           MOVE '00'                      TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                    TO   IDSV0003-SQLCODE
                                               IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
                                               IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.

       A200-ELABORA-WC-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.

       B200-ELABORA-WC-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B200-EX.
           EXIT.

       C200-ELABORA-WC-NST.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       C200-EX.
           EXIT.
      *----
      *----  gestione WC Effetto
      *----
       A205-DECLARE-CURSOR-WC-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.


           EXEC SQL
              DECLARE C-EFF CURSOR FOR
              SELECT
                     A.ID_POLI
                    ,A.ID_MOVI_CRZ
                    ,A.ID_MOVI_CHIU
                    ,A.IB_OGG
                    ,A.IB_PROP
                    ,A.DT_PROP
                    ,A.DT_INI_EFF
                    ,A.DT_END_EFF
                    ,A.COD_COMP_ANIA
                    ,A.DT_DECOR
                    ,A.DT_EMIS
                    ,A.TP_POLI
                    ,A.DUR_AA
                    ,A.DUR_MM
                    ,A.DT_SCAD
                    ,A.COD_PROD
                    ,A.DT_INI_VLDT_PROD
                    ,A.COD_CONV
                    ,A.COD_RAMO
                    ,A.DT_INI_VLDT_CONV
                    ,A.DT_APPLZ_CONV
                    ,A.TP_FRM_ASSVA
                    ,A.TP_RGM_FISC
                    ,A.FL_ESTAS
                    ,A.FL_RSH_COMUN
                    ,A.FL_RSH_COMUN_COND
                    ,A.TP_LIV_GENZ_TIT
                    ,A.FL_COP_FINANZ
                    ,A.TP_APPLZ_DIR
                    ,A.SPE_MED
                    ,A.DIR_EMIS
                    ,A.DIR_1O_VERS
                    ,A.DIR_VERS_AGG
                    ,A.COD_DVS
                    ,A.FL_FNT_AZ
                    ,A.FL_FNT_ADER
                    ,A.FL_FNT_TFR
                    ,A.FL_FNT_VOLO
                    ,A.TP_OPZ_A_SCAD
                    ,A.AA_DIFF_PROR_DFLT
                    ,A.FL_VER_PROD
                    ,A.DUR_GG
                    ,A.DIR_QUIET
                    ,A.TP_PTF_ESTNO
                    ,A.FL_CUM_PRE_CNTR
                    ,A.FL_AMMB_MOVI
                    ,A.CONV_GECO
                    ,A.DS_RIGA
                    ,A.DS_OPER_SQL
                    ,A.DS_VER
                    ,A.DS_TS_INI_CPTZ
                    ,A.DS_TS_END_CPTZ
                    ,A.DS_UTENTE
                    ,A.DS_STATO_ELAB
                    ,A.FL_SCUDO_FISC
                    ,A.FL_TRASFE
                    ,A.FL_TFR_STRC
                    ,A.DT_PRESC
                    ,A.COD_CONV_AGG
                    ,A.SUBCAT_PROD
                    ,A.FL_QUEST_ADEGZ_ASS
                    ,A.COD_TPA
                    ,A.ID_ACC_COMM
                    ,A.FL_POLI_CPI_PR
                    ,A.FL_POLI_BUNDLING
                    ,A.IND_POLI_PRIN_COLL
                    ,A.FL_VND_BUNDLE
                    ,A.IB_BS
                    ,A.FL_POLI_IFP
              FROM POLI A,
                   RAPP_ANA B
              WHERE   B.COD_SOGG   = :LDBV5561-COD-SOGG
                    AND B.ID_OGG       = A.ID_POLI
                    AND B.TP_OGG       = 'PO'
                    AND B.TP_RAPP_ANA  = 'CO'
                    AND A.TP_FRM_ASSVA = 'IN'
                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.ID_MOVI_CHIU IS NULL
                    AND B.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.ID_MOVI_CHIU IS NULL

           END-EXEC.

       A205-EX.
           EXIT.

       A210-SELECT-WC-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           EXEC SQL
             SELECT
                     A.ID_POLI
                    ,A.ID_MOVI_CRZ
                    ,A.ID_MOVI_CHIU
                    ,A.IB_OGG
                    ,A.IB_PROP
                    ,A.DT_PROP
                    ,A.DT_INI_EFF
                    ,A.DT_END_EFF
                    ,A.COD_COMP_ANIA
                    ,A.DT_DECOR
                    ,A.DT_EMIS
                    ,A.TP_POLI
                    ,A.DUR_AA
                    ,A.DUR_MM
                    ,A.DT_SCAD
                    ,A.COD_PROD
                    ,A.DT_INI_VLDT_PROD
                    ,A.COD_CONV
                    ,A.COD_RAMO
                    ,A.DT_INI_VLDT_CONV
                    ,A.DT_APPLZ_CONV
                    ,A.TP_FRM_ASSVA
                    ,A.TP_RGM_FISC
                    ,A.FL_ESTAS
                    ,A.FL_RSH_COMUN
                    ,A.FL_RSH_COMUN_COND
                    ,A.TP_LIV_GENZ_TIT
                    ,A.FL_COP_FINANZ
                    ,A.TP_APPLZ_DIR
                    ,A.SPE_MED
                    ,A.DIR_EMIS
                    ,A.DIR_1O_VERS
                    ,A.DIR_VERS_AGG
                    ,A.COD_DVS
                    ,A.FL_FNT_AZ
                    ,A.FL_FNT_ADER
                    ,A.FL_FNT_TFR
                    ,A.FL_FNT_VOLO
                    ,A.TP_OPZ_A_SCAD
                    ,A.AA_DIFF_PROR_DFLT
                    ,A.FL_VER_PROD
                    ,A.DUR_GG
                    ,A.DIR_QUIET
                    ,A.TP_PTF_ESTNO
                    ,A.FL_CUM_PRE_CNTR
                    ,A.FL_AMMB_MOVI
                    ,A.CONV_GECO
                    ,A.DS_RIGA
                    ,A.DS_OPER_SQL
                    ,A.DS_VER
                    ,A.DS_TS_INI_CPTZ
                    ,A.DS_TS_END_CPTZ
                    ,A.DS_UTENTE
                    ,A.DS_STATO_ELAB
                    ,A.FL_SCUDO_FISC
                    ,A.FL_TRASFE
                    ,A.FL_TFR_STRC
                    ,A.DT_PRESC
                    ,A.COD_CONV_AGG
                    ,A.SUBCAT_PROD
                    ,A.FL_QUEST_ADEGZ_ASS
                    ,A.COD_TPA
                    ,A.ID_ACC_COMM
                    ,A.FL_POLI_CPI_PR
                    ,A.FL_POLI_BUNDLING
                    ,A.IND_POLI_PRIN_COLL
                    ,A.FL_VND_BUNDLE
                    ,A.IB_BS
                    ,A.FL_POLI_IFP
             INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
             FROM POLI A,
                  RAPP_ANA B
             WHERE   B.COD_SOGG   = :LDBV5561-COD-SOGG
                    AND B.ID_OGG       = A.ID_POLI
                    AND B.TP_OGG       = 'PO'
                    AND B.TP_RAPP_ANA  = 'CO'
                    AND A.TP_FRM_ASSVA = 'IN'
                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.ID_MOVI_CHIU IS NULL
                    AND B.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX

           END-IF.
       A210-EX.
           EXIT.

       A260-OPEN-CURSOR-WC-EFF.
           PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.

           EXEC SQL
                OPEN C-EFF
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A260-EX.
           EXIT.

       A270-CLOSE-CURSOR-WC-EFF.
           EXEC SQL
                CLOSE C-EFF
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A270-EX.
           EXIT.

       A280-FETCH-FIRST-WC-EFF.
           PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
           END-IF.
       A280-EX.
           EXIT.

       A290-FETCH-NEXT-WC-EFF.
           EXEC SQL
                FETCH C-EFF
           INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A290-EX.
           EXIT.
      *----
      *----  gestione WC Competenza
      *----
       B205-DECLARE-CURSOR-WC-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           EXEC SQL
              DECLARE C-CPZ CURSOR FOR
              SELECT
                     A.ID_POLI
                    ,A.ID_MOVI_CRZ
                    ,A.ID_MOVI_CHIU
                    ,A.IB_OGG
                    ,A.IB_PROP
                    ,A.DT_PROP
                    ,A.DT_INI_EFF
                    ,A.DT_END_EFF
                    ,A.COD_COMP_ANIA
                    ,A.DT_DECOR
                    ,A.DT_EMIS
                    ,A.TP_POLI
                    ,A.DUR_AA
                    ,A.DUR_MM
                    ,A.DT_SCAD
                    ,A.COD_PROD
                    ,A.DT_INI_VLDT_PROD
                    ,A.COD_CONV
                    ,A.COD_RAMO
                    ,A.DT_INI_VLDT_CONV
                    ,A.DT_APPLZ_CONV
                    ,A.TP_FRM_ASSVA
                    ,A.TP_RGM_FISC
                    ,A.FL_ESTAS
                    ,A.FL_RSH_COMUN
                    ,A.FL_RSH_COMUN_COND
                    ,A.TP_LIV_GENZ_TIT
                    ,A.FL_COP_FINANZ
                    ,A.TP_APPLZ_DIR
                    ,A.SPE_MED
                    ,A.DIR_EMIS
                    ,A.DIR_1O_VERS
                    ,A.DIR_VERS_AGG
                    ,A.COD_DVS
                    ,A.FL_FNT_AZ
                    ,A.FL_FNT_ADER
                    ,A.FL_FNT_TFR
                    ,A.FL_FNT_VOLO
                    ,A.TP_OPZ_A_SCAD
                    ,A.AA_DIFF_PROR_DFLT
                    ,A.FL_VER_PROD
                    ,A.DUR_GG
                    ,A.DIR_QUIET
                    ,A.TP_PTF_ESTNO
                    ,A.FL_CUM_PRE_CNTR
                    ,A.FL_AMMB_MOVI
                    ,A.CONV_GECO
                    ,A.DS_RIGA
                    ,A.DS_OPER_SQL
                    ,A.DS_VER
                    ,A.DS_TS_INI_CPTZ
                    ,A.DS_TS_END_CPTZ
                    ,A.DS_UTENTE
                    ,A.DS_STATO_ELAB
                    ,A.FL_SCUDO_FISC
                    ,A.FL_TRASFE
                    ,A.FL_TFR_STRC
                    ,A.DT_PRESC
                    ,A.COD_CONV_AGG
                    ,A.SUBCAT_PROD
                    ,A.FL_QUEST_ADEGZ_ASS
                    ,A.COD_TPA
                    ,A.ID_ACC_COMM
                    ,A.FL_POLI_CPI_PR
                    ,A.FL_POLI_BUNDLING
                    ,A.IND_POLI_PRIN_COLL
                    ,A.FL_VND_BUNDLE
                    ,A.IB_BS
                    ,A.FL_POLI_IFP
              FROM POLI A,
                   RAPP_ANA B
              WHERE   B.COD_SOGG   = :LDBV5561-COD-SOGG
                        AND B.ID_OGG       = A.ID_POLI
                        AND B.TP_OGG       = 'PO'
                        AND B.TP_RAPP_ANA  = 'CO'
                        AND A.TP_FRM_ASSVA = 'IN'
                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND A.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
                    AND B.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND B.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

       B205-EX.
           EXIT.

       B210-SELECT-WC-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           EXEC SQL
             SELECT
                     A.ID_POLI
                    ,A.ID_MOVI_CRZ
                    ,A.ID_MOVI_CHIU
                    ,A.IB_OGG
                    ,A.IB_PROP
                    ,A.DT_PROP
                    ,A.DT_INI_EFF
                    ,A.DT_END_EFF
                    ,A.COD_COMP_ANIA
                    ,A.DT_DECOR
                    ,A.DT_EMIS
                    ,A.TP_POLI
                    ,A.DUR_AA
                    ,A.DUR_MM
                    ,A.DT_SCAD
                    ,A.COD_PROD
                    ,A.DT_INI_VLDT_PROD
                    ,A.COD_CONV
                    ,A.COD_RAMO
                    ,A.DT_INI_VLDT_CONV
                    ,A.DT_APPLZ_CONV
                    ,A.TP_FRM_ASSVA
                    ,A.TP_RGM_FISC
                    ,A.FL_ESTAS
                    ,A.FL_RSH_COMUN
                    ,A.FL_RSH_COMUN_COND
                    ,A.TP_LIV_GENZ_TIT
                    ,A.FL_COP_FINANZ
                    ,A.TP_APPLZ_DIR
                    ,A.SPE_MED
                    ,A.DIR_EMIS
                    ,A.DIR_1O_VERS
                    ,A.DIR_VERS_AGG
                    ,A.COD_DVS
                    ,A.FL_FNT_AZ
                    ,A.FL_FNT_ADER
                    ,A.FL_FNT_TFR
                    ,A.FL_FNT_VOLO
                    ,A.TP_OPZ_A_SCAD
                    ,A.AA_DIFF_PROR_DFLT
                    ,A.FL_VER_PROD
                    ,A.DUR_GG
                    ,A.DIR_QUIET
                    ,A.TP_PTF_ESTNO
                    ,A.FL_CUM_PRE_CNTR
                    ,A.FL_AMMB_MOVI
                    ,A.CONV_GECO
                    ,A.DS_RIGA
                    ,A.DS_OPER_SQL
                    ,A.DS_VER
                    ,A.DS_TS_INI_CPTZ
                    ,A.DS_TS_END_CPTZ
                    ,A.DS_UTENTE
                    ,A.DS_STATO_ELAB
                    ,A.FL_SCUDO_FISC
                    ,A.FL_TRASFE
                    ,A.FL_TFR_STRC
                    ,A.DT_PRESC
                    ,A.COD_CONV_AGG
                    ,A.SUBCAT_PROD
                    ,A.FL_QUEST_ADEGZ_ASS
                    ,A.COD_TPA
                    ,A.ID_ACC_COMM
                    ,A.FL_POLI_CPI_PR
                    ,A.FL_POLI_BUNDLING
                    ,A.IND_POLI_PRIN_COLL
                    ,A.FL_VND_BUNDLE
                    ,A.IB_BS
                    ,A.FL_POLI_IFP
             INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
             FROM POLI A,
                  RAPP_ANA B
             WHERE   B.COD_SOGG   = :LDBV5561-COD-SOGG
                    AND B.ID_OGG       = A.ID_POLI
                    AND B.TP_OGG       = 'PO'
                    AND B.TP_RAPP_ANA  = 'CO'
                    AND A.TP_FRM_ASSVA = 'IN'
                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND A.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
                    AND B.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND B.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX

           END-IF.
       B210-EX.
           EXIT.

       B260-OPEN-CURSOR-WC-CPZ.
           PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.

           EXEC SQL
                OPEN C-CPZ
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B260-EX.
           EXIT.

       B270-CLOSE-CURSOR-WC-CPZ.
           EXEC SQL
                CLOSE C-CPZ
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B270-EX.
           EXIT.

       B280-FETCH-FIRST-WC-CPZ.
           PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
           END-IF.
       B280-EX.
           EXIT.

       B290-FETCH-NEXT-WC-CPZ.
           EXEC SQL
                FETCH C-CPZ
           INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B290-EX.
           EXIT.
      *----
      *----  gestione WC Senza Storicit`
      *----
       C205-DECLARE-CURSOR-WC-NST.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       C205-EX.
           EXIT.

       C210-SELECT-WC-NST.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       C210-EX.
           EXIT.

       C260-OPEN-CURSOR-WC-NST.
           PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       C260-EX.
           EXIT.

       C270-CLOSE-CURSOR-WC-NST.
           SET IDSV0003-INVALID-OPER TO TRUE.
       C270-EX.
           EXIT.

       C280-FETCH-FIRST-WC-NST.
           PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
           END-IF.
       C280-EX.
           EXIT.

       C290-FETCH-NEXT-WC-NST.
           SET IDSV0003-INVALID-OPER TO TRUE.
       C290-EX.
           EXIT.
      *----
      *----  utilit` comuni a tutti i livelli operazione
      *----
       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-POL-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO POL-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-POL-IB-OGG = -1
              MOVE HIGH-VALUES TO POL-IB-OGG-NULL
           END-IF
           IF IND-POL-DT-PROP = -1
              MOVE HIGH-VALUES TO POL-DT-PROP-NULL
           END-IF
           IF IND-POL-DUR-AA = -1
              MOVE HIGH-VALUES TO POL-DUR-AA-NULL
           END-IF
           IF IND-POL-DUR-MM = -1
              MOVE HIGH-VALUES TO POL-DUR-MM-NULL
           END-IF
           IF IND-POL-DT-SCAD = -1
              MOVE HIGH-VALUES TO POL-DT-SCAD-NULL
           END-IF
           IF IND-POL-COD-CONV = -1
              MOVE HIGH-VALUES TO POL-COD-CONV-NULL
           END-IF
           IF IND-POL-COD-RAMO = -1
              MOVE HIGH-VALUES TO POL-COD-RAMO-NULL
           END-IF
           IF IND-POL-DT-INI-VLDT-CONV = -1
              MOVE HIGH-VALUES TO POL-DT-INI-VLDT-CONV-NULL
           END-IF
           IF IND-POL-DT-APPLZ-CONV = -1
              MOVE HIGH-VALUES TO POL-DT-APPLZ-CONV-NULL
           END-IF
           IF IND-POL-TP-RGM-FISC = -1
              MOVE HIGH-VALUES TO POL-TP-RGM-FISC-NULL
           END-IF
           IF IND-POL-FL-ESTAS = -1
              MOVE HIGH-VALUES TO POL-FL-ESTAS-NULL
           END-IF
           IF IND-POL-FL-RSH-COMUN = -1
              MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-NULL
           END-IF
           IF IND-POL-FL-RSH-COMUN-COND = -1
              MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-COND-NULL
           END-IF
           IF IND-POL-FL-COP-FINANZ = -1
              MOVE HIGH-VALUES TO POL-FL-COP-FINANZ-NULL
           END-IF
           IF IND-POL-TP-APPLZ-DIR = -1
              MOVE HIGH-VALUES TO POL-TP-APPLZ-DIR-NULL
           END-IF
           IF IND-POL-SPE-MED = -1
              MOVE HIGH-VALUES TO POL-SPE-MED-NULL
           END-IF
           IF IND-POL-DIR-EMIS = -1
              MOVE HIGH-VALUES TO POL-DIR-EMIS-NULL
           END-IF
           IF IND-POL-DIR-1O-VERS = -1
              MOVE HIGH-VALUES TO POL-DIR-1O-VERS-NULL
           END-IF
           IF IND-POL-DIR-VERS-AGG = -1
              MOVE HIGH-VALUES TO POL-DIR-VERS-AGG-NULL
           END-IF
           IF IND-POL-COD-DVS = -1
              MOVE HIGH-VALUES TO POL-COD-DVS-NULL
           END-IF
           IF IND-POL-FL-FNT-AZ = -1
              MOVE HIGH-VALUES TO POL-FL-FNT-AZ-NULL
           END-IF
           IF IND-POL-FL-FNT-ADER = -1
              MOVE HIGH-VALUES TO POL-FL-FNT-ADER-NULL
           END-IF
           IF IND-POL-FL-FNT-TFR = -1
              MOVE HIGH-VALUES TO POL-FL-FNT-TFR-NULL
           END-IF
           IF IND-POL-FL-FNT-VOLO = -1
              MOVE HIGH-VALUES TO POL-FL-FNT-VOLO-NULL
           END-IF
           IF IND-POL-TP-OPZ-A-SCAD = -1
              MOVE HIGH-VALUES TO POL-TP-OPZ-A-SCAD-NULL
           END-IF
           IF IND-POL-AA-DIFF-PROR-DFLT = -1
              MOVE HIGH-VALUES TO POL-AA-DIFF-PROR-DFLT-NULL
           END-IF
           IF IND-POL-FL-VER-PROD = -1
              MOVE HIGH-VALUES TO POL-FL-VER-PROD-NULL
           END-IF
           IF IND-POL-DUR-GG = -1
              MOVE HIGH-VALUES TO POL-DUR-GG-NULL
           END-IF
           IF IND-POL-DIR-QUIET = -1
              MOVE HIGH-VALUES TO POL-DIR-QUIET-NULL
           END-IF
           IF IND-POL-TP-PTF-ESTNO = -1
              MOVE HIGH-VALUES TO POL-TP-PTF-ESTNO-NULL
           END-IF
           IF IND-POL-FL-CUM-PRE-CNTR = -1
              MOVE HIGH-VALUES TO POL-FL-CUM-PRE-CNTR-NULL
           END-IF
           IF IND-POL-FL-AMMB-MOVI = -1
              MOVE HIGH-VALUES TO POL-FL-AMMB-MOVI-NULL
           END-IF
           IF IND-POL-CONV-GECO = -1
              MOVE HIGH-VALUES TO POL-CONV-GECO-NULL
           END-IF
           IF IND-POL-FL-SCUDO-FISC = -1
              MOVE HIGH-VALUES TO POL-FL-SCUDO-FISC-NULL
           END-IF
           IF IND-POL-FL-TRASFE = -1
              MOVE HIGH-VALUES TO POL-FL-TRASFE-NULL
           END-IF
           IF IND-POL-FL-TFR-STRC = -1
              MOVE HIGH-VALUES TO POL-FL-TFR-STRC-NULL
           END-IF
           IF IND-POL-DT-PRESC = -1
              MOVE HIGH-VALUES TO POL-DT-PRESC-NULL
           END-IF
           IF IND-POL-COD-CONV-AGG = -1
              MOVE HIGH-VALUES TO POL-COD-CONV-AGG-NULL
           END-IF
           IF IND-POL-SUBCAT-PROD = -1
              MOVE HIGH-VALUES TO POL-SUBCAT-PROD-NULL
           END-IF
           IF IND-POL-FL-QUEST-ADEGZ-ASS = -1
              MOVE HIGH-VALUES TO POL-FL-QUEST-ADEGZ-ASS-NULL
           END-IF
           IF IND-POL-COD-TPA = -1
              MOVE HIGH-VALUES TO POL-COD-TPA-NULL
           END-IF
           IF IND-POL-ID-ACC-COMM = -1
              MOVE HIGH-VALUES TO POL-ID-ACC-COMM-NULL
           END-IF
           IF IND-POL-FL-POLI-CPI-PR = -1
              MOVE HIGH-VALUES TO POL-FL-POLI-CPI-PR-NULL
           END-IF
           IF IND-POL-FL-POLI-BUNDLING = -1
              MOVE HIGH-VALUES TO POL-FL-POLI-BUNDLING-NULL
           END-IF
           IF IND-POL-IND-POLI-PRIN-COLL = -1
              MOVE HIGH-VALUES TO POL-IND-POLI-PRIN-COLL-NULL
           END-IF
           IF IND-POL-FL-VND-BUNDLE = -1
              MOVE HIGH-VALUES TO POL-FL-VND-BUNDLE-NULL
           END-IF
           IF IND-POL-IB-BS = -1
              MOVE HIGH-VALUES TO POL-IB-BS-NULL
           END-IF
           IF IND-POL-FL-POLI-IFP = -1
              MOVE HIGH-VALUES TO POL-FL-POLI-IFP-NULL
           END-IF.

       Z100-EX.
           EXIT.

      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           IF IND-POL-DT-PROP = 0
               MOVE POL-DT-PROP TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-PROP-DB
           END-IF
           MOVE POL-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-INI-EFF-DB
           MOVE POL-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-END-EFF-DB
           MOVE POL-DT-DECOR TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-DECOR-DB
           MOVE POL-DT-EMIS TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-EMIS-DB
           IF IND-POL-DT-SCAD = 0
               MOVE POL-DT-SCAD TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-SCAD-DB
           END-IF
           MOVE POL-DT-INI-VLDT-PROD TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-INI-VLDT-PROD-DB
           IF IND-POL-DT-INI-VLDT-CONV = 0
               MOVE POL-DT-INI-VLDT-CONV TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-INI-VLDT-CONV-DB
           END-IF
           IF IND-POL-DT-APPLZ-CONV = 0
               MOVE POL-DT-APPLZ-CONV TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-APPLZ-CONV-DB
           END-IF
           IF IND-POL-DT-PRESC = 0
               MOVE POL-DT-PRESC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-PRESC-DB
           END-IF.

       Z900-EX.
           EXIT.

      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           IF IND-POL-DT-PROP = 0
               MOVE POL-DT-PROP-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-PROP
           END-IF
           MOVE POL-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-INI-EFF
           MOVE POL-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-END-EFF
           MOVE POL-DT-DECOR-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-DECOR
           MOVE POL-DT-EMIS-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-EMIS
           IF IND-POL-DT-SCAD = 0
               MOVE POL-DT-SCAD-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-SCAD
           END-IF
           MOVE POL-DT-INI-VLDT-PROD-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-INI-VLDT-PROD
           IF IND-POL-DT-INI-VLDT-CONV = 0
               MOVE POL-DT-INI-VLDT-CONV-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-INI-VLDT-CONV
           END-IF
           IF IND-POL-DT-APPLZ-CONV = 0
               MOVE POL-DT-APPLZ-CONV-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-APPLZ-CONV
           END-IF
           IF IND-POL-DT-PRESC = 0
               MOVE POL-DT-PRESC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-PRESC
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----
      *----  prevede statements AD HOC PRE Query
      *----
       Z970-CODICE-ADHOC-PRE.

           CONTINUE.
       Z970-EX.
           EXIT.
      *----
      *----  prevede statements AD HOC POST Query
      *----
       Z980-CODICE-ADHOC-POST.

           CONTINUE.
       Z980-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

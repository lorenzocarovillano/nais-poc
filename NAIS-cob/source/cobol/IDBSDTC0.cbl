       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSDTC0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  28 NOV 2014.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDDTC0 END-EXEC.
           EXEC SQL INCLUDE IDBVDTC2 END-EXEC.
           EXEC SQL INCLUDE IDBVDTC3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVDTC1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 DETT-TIT-CONT.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSDTC0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'DETT_TIT_CONT' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_DETT_TIT_CONT
                ,ID_TIT_CONT
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_INI_COP
                ,DT_END_COP
                ,PRE_NET
                ,INTR_FRAZ
                ,INTR_MORA
                ,INTR_RETDT
                ,INTR_RIAT
                ,DIR
                ,SPE_MED
                ,TAX
                ,SOPR_SAN
                ,SOPR_SPO
                ,SOPR_TEC
                ,SOPR_PROF
                ,SOPR_ALT
                ,PRE_TOT
                ,PRE_PP_IAS
                ,PRE_SOLO_RSH
                ,CAR_ACQ
                ,CAR_GEST
                ,CAR_INC
                ,PROV_ACQ_1AA
                ,PROV_ACQ_2AA
                ,PROV_RICOR
                ,PROV_INC
                ,PROV_DA_REC
                ,COD_DVS
                ,FRQ_MOVI
                ,TP_RGM_FISC
                ,COD_TARI
                ,TP_STAT_TIT
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,MANFEE_ANTIC
                ,MANFEE_RICOR
                ,MANFEE_REC
                ,DT_ESI_TIT
                ,SPE_AGE
                ,CAR_IAS
                ,TOT_INTR_PREST
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,NUM_GG_RITARDO_PAG
                ,NUM_GG_RIVAL
                ,ACQ_EXP
                ,REMUN_ASS
                ,COMMIS_INTER
                ,CNBT_ANTIRAC
             INTO
                :DTC-ID-DETT-TIT-CONT
               ,:DTC-ID-TIT-CONT
               ,:DTC-ID-OGG
               ,:DTC-TP-OGG
               ,:DTC-ID-MOVI-CRZ
               ,:DTC-ID-MOVI-CHIU
                :IND-DTC-ID-MOVI-CHIU
               ,:DTC-DT-INI-EFF-DB
               ,:DTC-DT-END-EFF-DB
               ,:DTC-COD-COMP-ANIA
               ,:DTC-DT-INI-COP-DB
                :IND-DTC-DT-INI-COP
               ,:DTC-DT-END-COP-DB
                :IND-DTC-DT-END-COP
               ,:DTC-PRE-NET
                :IND-DTC-PRE-NET
               ,:DTC-INTR-FRAZ
                :IND-DTC-INTR-FRAZ
               ,:DTC-INTR-MORA
                :IND-DTC-INTR-MORA
               ,:DTC-INTR-RETDT
                :IND-DTC-INTR-RETDT
               ,:DTC-INTR-RIAT
                :IND-DTC-INTR-RIAT
               ,:DTC-DIR
                :IND-DTC-DIR
               ,:DTC-SPE-MED
                :IND-DTC-SPE-MED
               ,:DTC-TAX
                :IND-DTC-TAX
               ,:DTC-SOPR-SAN
                :IND-DTC-SOPR-SAN
               ,:DTC-SOPR-SPO
                :IND-DTC-SOPR-SPO
               ,:DTC-SOPR-TEC
                :IND-DTC-SOPR-TEC
               ,:DTC-SOPR-PROF
                :IND-DTC-SOPR-PROF
               ,:DTC-SOPR-ALT
                :IND-DTC-SOPR-ALT
               ,:DTC-PRE-TOT
                :IND-DTC-PRE-TOT
               ,:DTC-PRE-PP-IAS
                :IND-DTC-PRE-PP-IAS
               ,:DTC-PRE-SOLO-RSH
                :IND-DTC-PRE-SOLO-RSH
               ,:DTC-CAR-ACQ
                :IND-DTC-CAR-ACQ
               ,:DTC-CAR-GEST
                :IND-DTC-CAR-GEST
               ,:DTC-CAR-INC
                :IND-DTC-CAR-INC
               ,:DTC-PROV-ACQ-1AA
                :IND-DTC-PROV-ACQ-1AA
               ,:DTC-PROV-ACQ-2AA
                :IND-DTC-PROV-ACQ-2AA
               ,:DTC-PROV-RICOR
                :IND-DTC-PROV-RICOR
               ,:DTC-PROV-INC
                :IND-DTC-PROV-INC
               ,:DTC-PROV-DA-REC
                :IND-DTC-PROV-DA-REC
               ,:DTC-COD-DVS
                :IND-DTC-COD-DVS
               ,:DTC-FRQ-MOVI
                :IND-DTC-FRQ-MOVI
               ,:DTC-TP-RGM-FISC
               ,:DTC-COD-TARI
                :IND-DTC-COD-TARI
               ,:DTC-TP-STAT-TIT
               ,:DTC-IMP-AZ
                :IND-DTC-IMP-AZ
               ,:DTC-IMP-ADER
                :IND-DTC-IMP-ADER
               ,:DTC-IMP-TFR
                :IND-DTC-IMP-TFR
               ,:DTC-IMP-VOLO
                :IND-DTC-IMP-VOLO
               ,:DTC-MANFEE-ANTIC
                :IND-DTC-MANFEE-ANTIC
               ,:DTC-MANFEE-RICOR
                :IND-DTC-MANFEE-RICOR
               ,:DTC-MANFEE-REC
                :IND-DTC-MANFEE-REC
               ,:DTC-DT-ESI-TIT-DB
                :IND-DTC-DT-ESI-TIT
               ,:DTC-SPE-AGE
                :IND-DTC-SPE-AGE
               ,:DTC-CAR-IAS
                :IND-DTC-CAR-IAS
               ,:DTC-TOT-INTR-PREST
                :IND-DTC-TOT-INTR-PREST
               ,:DTC-DS-RIGA
               ,:DTC-DS-OPER-SQL
               ,:DTC-DS-VER
               ,:DTC-DS-TS-INI-CPTZ
               ,:DTC-DS-TS-END-CPTZ
               ,:DTC-DS-UTENTE
               ,:DTC-DS-STATO-ELAB
               ,:DTC-IMP-TRASFE
                :IND-DTC-IMP-TRASFE
               ,:DTC-IMP-TFR-STRC
                :IND-DTC-IMP-TFR-STRC
               ,:DTC-NUM-GG-RITARDO-PAG
                :IND-DTC-NUM-GG-RITARDO-PAG
               ,:DTC-NUM-GG-RIVAL
                :IND-DTC-NUM-GG-RIVAL
               ,:DTC-ACQ-EXP
                :IND-DTC-ACQ-EXP
               ,:DTC-REMUN-ASS
                :IND-DTC-REMUN-ASS
               ,:DTC-COMMIS-INTER
                :IND-DTC-COMMIS-INTER
               ,:DTC-CNBT-ANTIRAC
                :IND-DTC-CNBT-ANTIRAC
             FROM DETT_TIT_CONT
             WHERE     DS_RIGA = :DTC-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO DETT_TIT_CONT
                     (
                        ID_DETT_TIT_CONT
                       ,ID_TIT_CONT
                       ,ID_OGG
                       ,TP_OGG
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,DT_INI_COP
                       ,DT_END_COP
                       ,PRE_NET
                       ,INTR_FRAZ
                       ,INTR_MORA
                       ,INTR_RETDT
                       ,INTR_RIAT
                       ,DIR
                       ,SPE_MED
                       ,TAX
                       ,SOPR_SAN
                       ,SOPR_SPO
                       ,SOPR_TEC
                       ,SOPR_PROF
                       ,SOPR_ALT
                       ,PRE_TOT
                       ,PRE_PP_IAS
                       ,PRE_SOLO_RSH
                       ,CAR_ACQ
                       ,CAR_GEST
                       ,CAR_INC
                       ,PROV_ACQ_1AA
                       ,PROV_ACQ_2AA
                       ,PROV_RICOR
                       ,PROV_INC
                       ,PROV_DA_REC
                       ,COD_DVS
                       ,FRQ_MOVI
                       ,TP_RGM_FISC
                       ,COD_TARI
                       ,TP_STAT_TIT
                       ,IMP_AZ
                       ,IMP_ADER
                       ,IMP_TFR
                       ,IMP_VOLO
                       ,MANFEE_ANTIC
                       ,MANFEE_RICOR
                       ,MANFEE_REC
                       ,DT_ESI_TIT
                       ,SPE_AGE
                       ,CAR_IAS
                       ,TOT_INTR_PREST
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,IMP_TRASFE
                       ,IMP_TFR_STRC
                       ,NUM_GG_RITARDO_PAG
                       ,NUM_GG_RIVAL
                       ,ACQ_EXP
                       ,REMUN_ASS
                       ,COMMIS_INTER
                       ,CNBT_ANTIRAC
                     )
                 VALUES
                     (
                       :DTC-ID-DETT-TIT-CONT
                       ,:DTC-ID-TIT-CONT
                       ,:DTC-ID-OGG
                       ,:DTC-TP-OGG
                       ,:DTC-ID-MOVI-CRZ
                       ,:DTC-ID-MOVI-CHIU
                        :IND-DTC-ID-MOVI-CHIU
                       ,:DTC-DT-INI-EFF-DB
                       ,:DTC-DT-END-EFF-DB
                       ,:DTC-COD-COMP-ANIA
                       ,:DTC-DT-INI-COP-DB
                        :IND-DTC-DT-INI-COP
                       ,:DTC-DT-END-COP-DB
                        :IND-DTC-DT-END-COP
                       ,:DTC-PRE-NET
                        :IND-DTC-PRE-NET
                       ,:DTC-INTR-FRAZ
                        :IND-DTC-INTR-FRAZ
                       ,:DTC-INTR-MORA
                        :IND-DTC-INTR-MORA
                       ,:DTC-INTR-RETDT
                        :IND-DTC-INTR-RETDT
                       ,:DTC-INTR-RIAT
                        :IND-DTC-INTR-RIAT
                       ,:DTC-DIR
                        :IND-DTC-DIR
                       ,:DTC-SPE-MED
                        :IND-DTC-SPE-MED
                       ,:DTC-TAX
                        :IND-DTC-TAX
                       ,:DTC-SOPR-SAN
                        :IND-DTC-SOPR-SAN
                       ,:DTC-SOPR-SPO
                        :IND-DTC-SOPR-SPO
                       ,:DTC-SOPR-TEC
                        :IND-DTC-SOPR-TEC
                       ,:DTC-SOPR-PROF
                        :IND-DTC-SOPR-PROF
                       ,:DTC-SOPR-ALT
                        :IND-DTC-SOPR-ALT
                       ,:DTC-PRE-TOT
                        :IND-DTC-PRE-TOT
                       ,:DTC-PRE-PP-IAS
                        :IND-DTC-PRE-PP-IAS
                       ,:DTC-PRE-SOLO-RSH
                        :IND-DTC-PRE-SOLO-RSH
                       ,:DTC-CAR-ACQ
                        :IND-DTC-CAR-ACQ
                       ,:DTC-CAR-GEST
                        :IND-DTC-CAR-GEST
                       ,:DTC-CAR-INC
                        :IND-DTC-CAR-INC
                       ,:DTC-PROV-ACQ-1AA
                        :IND-DTC-PROV-ACQ-1AA
                       ,:DTC-PROV-ACQ-2AA
                        :IND-DTC-PROV-ACQ-2AA
                       ,:DTC-PROV-RICOR
                        :IND-DTC-PROV-RICOR
                       ,:DTC-PROV-INC
                        :IND-DTC-PROV-INC
                       ,:DTC-PROV-DA-REC
                        :IND-DTC-PROV-DA-REC
                       ,:DTC-COD-DVS
                        :IND-DTC-COD-DVS
                       ,:DTC-FRQ-MOVI
                        :IND-DTC-FRQ-MOVI
                       ,:DTC-TP-RGM-FISC
                       ,:DTC-COD-TARI
                        :IND-DTC-COD-TARI
                       ,:DTC-TP-STAT-TIT
                       ,:DTC-IMP-AZ
                        :IND-DTC-IMP-AZ
                       ,:DTC-IMP-ADER
                        :IND-DTC-IMP-ADER
                       ,:DTC-IMP-TFR
                        :IND-DTC-IMP-TFR
                       ,:DTC-IMP-VOLO
                        :IND-DTC-IMP-VOLO
                       ,:DTC-MANFEE-ANTIC
                        :IND-DTC-MANFEE-ANTIC
                       ,:DTC-MANFEE-RICOR
                        :IND-DTC-MANFEE-RICOR
                       ,:DTC-MANFEE-REC
                        :IND-DTC-MANFEE-REC
                       ,:DTC-DT-ESI-TIT-DB
                        :IND-DTC-DT-ESI-TIT
                       ,:DTC-SPE-AGE
                        :IND-DTC-SPE-AGE
                       ,:DTC-CAR-IAS
                        :IND-DTC-CAR-IAS
                       ,:DTC-TOT-INTR-PREST
                        :IND-DTC-TOT-INTR-PREST
                       ,:DTC-DS-RIGA
                       ,:DTC-DS-OPER-SQL
                       ,:DTC-DS-VER
                       ,:DTC-DS-TS-INI-CPTZ
                       ,:DTC-DS-TS-END-CPTZ
                       ,:DTC-DS-UTENTE
                       ,:DTC-DS-STATO-ELAB
                       ,:DTC-IMP-TRASFE
                        :IND-DTC-IMP-TRASFE
                       ,:DTC-IMP-TFR-STRC
                        :IND-DTC-IMP-TFR-STRC
                       ,:DTC-NUM-GG-RITARDO-PAG
                        :IND-DTC-NUM-GG-RITARDO-PAG
                       ,:DTC-NUM-GG-RIVAL
                        :IND-DTC-NUM-GG-RIVAL
                       ,:DTC-ACQ-EXP
                        :IND-DTC-ACQ-EXP
                       ,:DTC-REMUN-ASS
                        :IND-DTC-REMUN-ASS
                       ,:DTC-COMMIS-INTER
                        :IND-DTC-COMMIS-INTER
                       ,:DTC-CNBT-ANTIRAC
                        :IND-DTC-CNBT-ANTIRAC
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE DETT_TIT_CONT SET

                   ID_DETT_TIT_CONT       =
                :DTC-ID-DETT-TIT-CONT
                  ,ID_TIT_CONT            =
                :DTC-ID-TIT-CONT
                  ,ID_OGG                 =
                :DTC-ID-OGG
                  ,TP_OGG                 =
                :DTC-TP-OGG
                  ,ID_MOVI_CRZ            =
                :DTC-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :DTC-ID-MOVI-CHIU
                                       :IND-DTC-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :DTC-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :DTC-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :DTC-COD-COMP-ANIA
                  ,DT_INI_COP             =
           :DTC-DT-INI-COP-DB
                                       :IND-DTC-DT-INI-COP
                  ,DT_END_COP             =
           :DTC-DT-END-COP-DB
                                       :IND-DTC-DT-END-COP
                  ,PRE_NET                =
                :DTC-PRE-NET
                                       :IND-DTC-PRE-NET
                  ,INTR_FRAZ              =
                :DTC-INTR-FRAZ
                                       :IND-DTC-INTR-FRAZ
                  ,INTR_MORA              =
                :DTC-INTR-MORA
                                       :IND-DTC-INTR-MORA
                  ,INTR_RETDT             =
                :DTC-INTR-RETDT
                                       :IND-DTC-INTR-RETDT
                  ,INTR_RIAT              =
                :DTC-INTR-RIAT
                                       :IND-DTC-INTR-RIAT
                  ,DIR                    =
                :DTC-DIR
                                       :IND-DTC-DIR
                  ,SPE_MED                =
                :DTC-SPE-MED
                                       :IND-DTC-SPE-MED
                  ,TAX                    =
                :DTC-TAX
                                       :IND-DTC-TAX
                  ,SOPR_SAN               =
                :DTC-SOPR-SAN
                                       :IND-DTC-SOPR-SAN
                  ,SOPR_SPO               =
                :DTC-SOPR-SPO
                                       :IND-DTC-SOPR-SPO
                  ,SOPR_TEC               =
                :DTC-SOPR-TEC
                                       :IND-DTC-SOPR-TEC
                  ,SOPR_PROF              =
                :DTC-SOPR-PROF
                                       :IND-DTC-SOPR-PROF
                  ,SOPR_ALT               =
                :DTC-SOPR-ALT
                                       :IND-DTC-SOPR-ALT
                  ,PRE_TOT                =
                :DTC-PRE-TOT
                                       :IND-DTC-PRE-TOT
                  ,PRE_PP_IAS             =
                :DTC-PRE-PP-IAS
                                       :IND-DTC-PRE-PP-IAS
                  ,PRE_SOLO_RSH           =
                :DTC-PRE-SOLO-RSH
                                       :IND-DTC-PRE-SOLO-RSH
                  ,CAR_ACQ                =
                :DTC-CAR-ACQ
                                       :IND-DTC-CAR-ACQ
                  ,CAR_GEST               =
                :DTC-CAR-GEST
                                       :IND-DTC-CAR-GEST
                  ,CAR_INC                =
                :DTC-CAR-INC
                                       :IND-DTC-CAR-INC
                  ,PROV_ACQ_1AA           =
                :DTC-PROV-ACQ-1AA
                                       :IND-DTC-PROV-ACQ-1AA
                  ,PROV_ACQ_2AA           =
                :DTC-PROV-ACQ-2AA
                                       :IND-DTC-PROV-ACQ-2AA
                  ,PROV_RICOR             =
                :DTC-PROV-RICOR
                                       :IND-DTC-PROV-RICOR
                  ,PROV_INC               =
                :DTC-PROV-INC
                                       :IND-DTC-PROV-INC
                  ,PROV_DA_REC            =
                :DTC-PROV-DA-REC
                                       :IND-DTC-PROV-DA-REC
                  ,COD_DVS                =
                :DTC-COD-DVS
                                       :IND-DTC-COD-DVS
                  ,FRQ_MOVI               =
                :DTC-FRQ-MOVI
                                       :IND-DTC-FRQ-MOVI
                  ,TP_RGM_FISC            =
                :DTC-TP-RGM-FISC
                  ,COD_TARI               =
                :DTC-COD-TARI
                                       :IND-DTC-COD-TARI
                  ,TP_STAT_TIT            =
                :DTC-TP-STAT-TIT
                  ,IMP_AZ                 =
                :DTC-IMP-AZ
                                       :IND-DTC-IMP-AZ
                  ,IMP_ADER               =
                :DTC-IMP-ADER
                                       :IND-DTC-IMP-ADER
                  ,IMP_TFR                =
                :DTC-IMP-TFR
                                       :IND-DTC-IMP-TFR
                  ,IMP_VOLO               =
                :DTC-IMP-VOLO
                                       :IND-DTC-IMP-VOLO
                  ,MANFEE_ANTIC           =
                :DTC-MANFEE-ANTIC
                                       :IND-DTC-MANFEE-ANTIC
                  ,MANFEE_RICOR           =
                :DTC-MANFEE-RICOR
                                       :IND-DTC-MANFEE-RICOR
                  ,MANFEE_REC             =
                :DTC-MANFEE-REC
                                       :IND-DTC-MANFEE-REC
                  ,DT_ESI_TIT             =
           :DTC-DT-ESI-TIT-DB
                                       :IND-DTC-DT-ESI-TIT
                  ,SPE_AGE                =
                :DTC-SPE-AGE
                                       :IND-DTC-SPE-AGE
                  ,CAR_IAS                =
                :DTC-CAR-IAS
                                       :IND-DTC-CAR-IAS
                  ,TOT_INTR_PREST         =
                :DTC-TOT-INTR-PREST
                                       :IND-DTC-TOT-INTR-PREST
                  ,DS_RIGA                =
                :DTC-DS-RIGA
                  ,DS_OPER_SQL            =
                :DTC-DS-OPER-SQL
                  ,DS_VER                 =
                :DTC-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :DTC-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :DTC-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :DTC-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :DTC-DS-STATO-ELAB
                  ,IMP_TRASFE             =
                :DTC-IMP-TRASFE
                                       :IND-DTC-IMP-TRASFE
                  ,IMP_TFR_STRC           =
                :DTC-IMP-TFR-STRC
                                       :IND-DTC-IMP-TFR-STRC
                  ,NUM_GG_RITARDO_PAG     =
                :DTC-NUM-GG-RITARDO-PAG
                                       :IND-DTC-NUM-GG-RITARDO-PAG
                  ,NUM_GG_RIVAL           =
                :DTC-NUM-GG-RIVAL
                                       :IND-DTC-NUM-GG-RIVAL
                  ,ACQ_EXP                =
                :DTC-ACQ-EXP
                                       :IND-DTC-ACQ-EXP
                  ,REMUN_ASS              =
                :DTC-REMUN-ASS
                                       :IND-DTC-REMUN-ASS
                  ,COMMIS_INTER           =
                :DTC-COMMIS-INTER
                                       :IND-DTC-COMMIS-INTER
                  ,CNBT_ANTIRAC           =
                :DTC-CNBT-ANTIRAC
                                       :IND-DTC-CNBT-ANTIRAC
                WHERE     DS_RIGA = :DTC-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM DETT_TIT_CONT
                WHERE     DS_RIGA = :DTC-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-DTC CURSOR FOR
              SELECT
                     ID_DETT_TIT_CONT
                    ,ID_TIT_CONT
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,DT_INI_COP
                    ,DT_END_COP
                    ,PRE_NET
                    ,INTR_FRAZ
                    ,INTR_MORA
                    ,INTR_RETDT
                    ,INTR_RIAT
                    ,DIR
                    ,SPE_MED
                    ,TAX
                    ,SOPR_SAN
                    ,SOPR_SPO
                    ,SOPR_TEC
                    ,SOPR_PROF
                    ,SOPR_ALT
                    ,PRE_TOT
                    ,PRE_PP_IAS
                    ,PRE_SOLO_RSH
                    ,CAR_ACQ
                    ,CAR_GEST
                    ,CAR_INC
                    ,PROV_ACQ_1AA
                    ,PROV_ACQ_2AA
                    ,PROV_RICOR
                    ,PROV_INC
                    ,PROV_DA_REC
                    ,COD_DVS
                    ,FRQ_MOVI
                    ,TP_RGM_FISC
                    ,COD_TARI
                    ,TP_STAT_TIT
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,MANFEE_ANTIC
                    ,MANFEE_RICOR
                    ,MANFEE_REC
                    ,DT_ESI_TIT
                    ,SPE_AGE
                    ,CAR_IAS
                    ,TOT_INTR_PREST
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,IMP_TRASFE
                    ,IMP_TFR_STRC
                    ,NUM_GG_RITARDO_PAG
                    ,NUM_GG_RIVAL
                    ,ACQ_EXP
                    ,REMUN_ASS
                    ,COMMIS_INTER
                    ,CNBT_ANTIRAC
              FROM DETT_TIT_CONT
              WHERE     ID_DETT_TIT_CONT = :DTC-ID-DETT-TIT-CONT
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY DT_INI_EFF ASC

           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_DETT_TIT_CONT
                ,ID_TIT_CONT
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_INI_COP
                ,DT_END_COP
                ,PRE_NET
                ,INTR_FRAZ
                ,INTR_MORA
                ,INTR_RETDT
                ,INTR_RIAT
                ,DIR
                ,SPE_MED
                ,TAX
                ,SOPR_SAN
                ,SOPR_SPO
                ,SOPR_TEC
                ,SOPR_PROF
                ,SOPR_ALT
                ,PRE_TOT
                ,PRE_PP_IAS
                ,PRE_SOLO_RSH
                ,CAR_ACQ
                ,CAR_GEST
                ,CAR_INC
                ,PROV_ACQ_1AA
                ,PROV_ACQ_2AA
                ,PROV_RICOR
                ,PROV_INC
                ,PROV_DA_REC
                ,COD_DVS
                ,FRQ_MOVI
                ,TP_RGM_FISC
                ,COD_TARI
                ,TP_STAT_TIT
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,MANFEE_ANTIC
                ,MANFEE_RICOR
                ,MANFEE_REC
                ,DT_ESI_TIT
                ,SPE_AGE
                ,CAR_IAS
                ,TOT_INTR_PREST
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,NUM_GG_RITARDO_PAG
                ,NUM_GG_RIVAL
                ,ACQ_EXP
                ,REMUN_ASS
                ,COMMIS_INTER
                ,CNBT_ANTIRAC
             INTO
                :DTC-ID-DETT-TIT-CONT
               ,:DTC-ID-TIT-CONT
               ,:DTC-ID-OGG
               ,:DTC-TP-OGG
               ,:DTC-ID-MOVI-CRZ
               ,:DTC-ID-MOVI-CHIU
                :IND-DTC-ID-MOVI-CHIU
               ,:DTC-DT-INI-EFF-DB
               ,:DTC-DT-END-EFF-DB
               ,:DTC-COD-COMP-ANIA
               ,:DTC-DT-INI-COP-DB
                :IND-DTC-DT-INI-COP
               ,:DTC-DT-END-COP-DB
                :IND-DTC-DT-END-COP
               ,:DTC-PRE-NET
                :IND-DTC-PRE-NET
               ,:DTC-INTR-FRAZ
                :IND-DTC-INTR-FRAZ
               ,:DTC-INTR-MORA
                :IND-DTC-INTR-MORA
               ,:DTC-INTR-RETDT
                :IND-DTC-INTR-RETDT
               ,:DTC-INTR-RIAT
                :IND-DTC-INTR-RIAT
               ,:DTC-DIR
                :IND-DTC-DIR
               ,:DTC-SPE-MED
                :IND-DTC-SPE-MED
               ,:DTC-TAX
                :IND-DTC-TAX
               ,:DTC-SOPR-SAN
                :IND-DTC-SOPR-SAN
               ,:DTC-SOPR-SPO
                :IND-DTC-SOPR-SPO
               ,:DTC-SOPR-TEC
                :IND-DTC-SOPR-TEC
               ,:DTC-SOPR-PROF
                :IND-DTC-SOPR-PROF
               ,:DTC-SOPR-ALT
                :IND-DTC-SOPR-ALT
               ,:DTC-PRE-TOT
                :IND-DTC-PRE-TOT
               ,:DTC-PRE-PP-IAS
                :IND-DTC-PRE-PP-IAS
               ,:DTC-PRE-SOLO-RSH
                :IND-DTC-PRE-SOLO-RSH
               ,:DTC-CAR-ACQ
                :IND-DTC-CAR-ACQ
               ,:DTC-CAR-GEST
                :IND-DTC-CAR-GEST
               ,:DTC-CAR-INC
                :IND-DTC-CAR-INC
               ,:DTC-PROV-ACQ-1AA
                :IND-DTC-PROV-ACQ-1AA
               ,:DTC-PROV-ACQ-2AA
                :IND-DTC-PROV-ACQ-2AA
               ,:DTC-PROV-RICOR
                :IND-DTC-PROV-RICOR
               ,:DTC-PROV-INC
                :IND-DTC-PROV-INC
               ,:DTC-PROV-DA-REC
                :IND-DTC-PROV-DA-REC
               ,:DTC-COD-DVS
                :IND-DTC-COD-DVS
               ,:DTC-FRQ-MOVI
                :IND-DTC-FRQ-MOVI
               ,:DTC-TP-RGM-FISC
               ,:DTC-COD-TARI
                :IND-DTC-COD-TARI
               ,:DTC-TP-STAT-TIT
               ,:DTC-IMP-AZ
                :IND-DTC-IMP-AZ
               ,:DTC-IMP-ADER
                :IND-DTC-IMP-ADER
               ,:DTC-IMP-TFR
                :IND-DTC-IMP-TFR
               ,:DTC-IMP-VOLO
                :IND-DTC-IMP-VOLO
               ,:DTC-MANFEE-ANTIC
                :IND-DTC-MANFEE-ANTIC
               ,:DTC-MANFEE-RICOR
                :IND-DTC-MANFEE-RICOR
               ,:DTC-MANFEE-REC
                :IND-DTC-MANFEE-REC
               ,:DTC-DT-ESI-TIT-DB
                :IND-DTC-DT-ESI-TIT
               ,:DTC-SPE-AGE
                :IND-DTC-SPE-AGE
               ,:DTC-CAR-IAS
                :IND-DTC-CAR-IAS
               ,:DTC-TOT-INTR-PREST
                :IND-DTC-TOT-INTR-PREST
               ,:DTC-DS-RIGA
               ,:DTC-DS-OPER-SQL
               ,:DTC-DS-VER
               ,:DTC-DS-TS-INI-CPTZ
               ,:DTC-DS-TS-END-CPTZ
               ,:DTC-DS-UTENTE
               ,:DTC-DS-STATO-ELAB
               ,:DTC-IMP-TRASFE
                :IND-DTC-IMP-TRASFE
               ,:DTC-IMP-TFR-STRC
                :IND-DTC-IMP-TFR-STRC
               ,:DTC-NUM-GG-RITARDO-PAG
                :IND-DTC-NUM-GG-RITARDO-PAG
               ,:DTC-NUM-GG-RIVAL
                :IND-DTC-NUM-GG-RIVAL
               ,:DTC-ACQ-EXP
                :IND-DTC-ACQ-EXP
               ,:DTC-REMUN-ASS
                :IND-DTC-REMUN-ASS
               ,:DTC-COMMIS-INTER
                :IND-DTC-COMMIS-INTER
               ,:DTC-CNBT-ANTIRAC
                :IND-DTC-CNBT-ANTIRAC
             FROM DETT_TIT_CONT
             WHERE     ID_DETT_TIT_CONT = :DTC-ID-DETT-TIT-CONT
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE DETT_TIT_CONT SET

                   ID_DETT_TIT_CONT       =
                :DTC-ID-DETT-TIT-CONT
                  ,ID_TIT_CONT            =
                :DTC-ID-TIT-CONT
                  ,ID_OGG                 =
                :DTC-ID-OGG
                  ,TP_OGG                 =
                :DTC-TP-OGG
                  ,ID_MOVI_CRZ            =
                :DTC-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :DTC-ID-MOVI-CHIU
                                       :IND-DTC-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :DTC-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :DTC-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :DTC-COD-COMP-ANIA
                  ,DT_INI_COP             =
           :DTC-DT-INI-COP-DB
                                       :IND-DTC-DT-INI-COP
                  ,DT_END_COP             =
           :DTC-DT-END-COP-DB
                                       :IND-DTC-DT-END-COP
                  ,PRE_NET                =
                :DTC-PRE-NET
                                       :IND-DTC-PRE-NET
                  ,INTR_FRAZ              =
                :DTC-INTR-FRAZ
                                       :IND-DTC-INTR-FRAZ
                  ,INTR_MORA              =
                :DTC-INTR-MORA
                                       :IND-DTC-INTR-MORA
                  ,INTR_RETDT             =
                :DTC-INTR-RETDT
                                       :IND-DTC-INTR-RETDT
                  ,INTR_RIAT              =
                :DTC-INTR-RIAT
                                       :IND-DTC-INTR-RIAT
                  ,DIR                    =
                :DTC-DIR
                                       :IND-DTC-DIR
                  ,SPE_MED                =
                :DTC-SPE-MED
                                       :IND-DTC-SPE-MED
                  ,TAX                    =
                :DTC-TAX
                                       :IND-DTC-TAX
                  ,SOPR_SAN               =
                :DTC-SOPR-SAN
                                       :IND-DTC-SOPR-SAN
                  ,SOPR_SPO               =
                :DTC-SOPR-SPO
                                       :IND-DTC-SOPR-SPO
                  ,SOPR_TEC               =
                :DTC-SOPR-TEC
                                       :IND-DTC-SOPR-TEC
                  ,SOPR_PROF              =
                :DTC-SOPR-PROF
                                       :IND-DTC-SOPR-PROF
                  ,SOPR_ALT               =
                :DTC-SOPR-ALT
                                       :IND-DTC-SOPR-ALT
                  ,PRE_TOT                =
                :DTC-PRE-TOT
                                       :IND-DTC-PRE-TOT
                  ,PRE_PP_IAS             =
                :DTC-PRE-PP-IAS
                                       :IND-DTC-PRE-PP-IAS
                  ,PRE_SOLO_RSH           =
                :DTC-PRE-SOLO-RSH
                                       :IND-DTC-PRE-SOLO-RSH
                  ,CAR_ACQ                =
                :DTC-CAR-ACQ
                                       :IND-DTC-CAR-ACQ
                  ,CAR_GEST               =
                :DTC-CAR-GEST
                                       :IND-DTC-CAR-GEST
                  ,CAR_INC                =
                :DTC-CAR-INC
                                       :IND-DTC-CAR-INC
                  ,PROV_ACQ_1AA           =
                :DTC-PROV-ACQ-1AA
                                       :IND-DTC-PROV-ACQ-1AA
                  ,PROV_ACQ_2AA           =
                :DTC-PROV-ACQ-2AA
                                       :IND-DTC-PROV-ACQ-2AA
                  ,PROV_RICOR             =
                :DTC-PROV-RICOR
                                       :IND-DTC-PROV-RICOR
                  ,PROV_INC               =
                :DTC-PROV-INC
                                       :IND-DTC-PROV-INC
                  ,PROV_DA_REC            =
                :DTC-PROV-DA-REC
                                       :IND-DTC-PROV-DA-REC
                  ,COD_DVS                =
                :DTC-COD-DVS
                                       :IND-DTC-COD-DVS
                  ,FRQ_MOVI               =
                :DTC-FRQ-MOVI
                                       :IND-DTC-FRQ-MOVI
                  ,TP_RGM_FISC            =
                :DTC-TP-RGM-FISC
                  ,COD_TARI               =
                :DTC-COD-TARI
                                       :IND-DTC-COD-TARI
                  ,TP_STAT_TIT            =
                :DTC-TP-STAT-TIT
                  ,IMP_AZ                 =
                :DTC-IMP-AZ
                                       :IND-DTC-IMP-AZ
                  ,IMP_ADER               =
                :DTC-IMP-ADER
                                       :IND-DTC-IMP-ADER
                  ,IMP_TFR                =
                :DTC-IMP-TFR
                                       :IND-DTC-IMP-TFR
                  ,IMP_VOLO               =
                :DTC-IMP-VOLO
                                       :IND-DTC-IMP-VOLO
                  ,MANFEE_ANTIC           =
                :DTC-MANFEE-ANTIC
                                       :IND-DTC-MANFEE-ANTIC
                  ,MANFEE_RICOR           =
                :DTC-MANFEE-RICOR
                                       :IND-DTC-MANFEE-RICOR
                  ,MANFEE_REC             =
                :DTC-MANFEE-REC
                                       :IND-DTC-MANFEE-REC
                  ,DT_ESI_TIT             =
           :DTC-DT-ESI-TIT-DB
                                       :IND-DTC-DT-ESI-TIT
                  ,SPE_AGE                =
                :DTC-SPE-AGE
                                       :IND-DTC-SPE-AGE
                  ,CAR_IAS                =
                :DTC-CAR-IAS
                                       :IND-DTC-CAR-IAS
                  ,TOT_INTR_PREST         =
                :DTC-TOT-INTR-PREST
                                       :IND-DTC-TOT-INTR-PREST
                  ,DS_RIGA                =
                :DTC-DS-RIGA
                  ,DS_OPER_SQL            =
                :DTC-DS-OPER-SQL
                  ,DS_VER                 =
                :DTC-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :DTC-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :DTC-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :DTC-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :DTC-DS-STATO-ELAB
                  ,IMP_TRASFE             =
                :DTC-IMP-TRASFE
                                       :IND-DTC-IMP-TRASFE
                  ,IMP_TFR_STRC           =
                :DTC-IMP-TFR-STRC
                                       :IND-DTC-IMP-TFR-STRC
                  ,NUM_GG_RITARDO_PAG     =
                :DTC-NUM-GG-RITARDO-PAG
                                       :IND-DTC-NUM-GG-RITARDO-PAG
                  ,NUM_GG_RIVAL           =
                :DTC-NUM-GG-RIVAL
                                       :IND-DTC-NUM-GG-RIVAL
                  ,ACQ_EXP                =
                :DTC-ACQ-EXP
                                       :IND-DTC-ACQ-EXP
                  ,REMUN_ASS              =
                :DTC-REMUN-ASS
                                       :IND-DTC-REMUN-ASS
                  ,COMMIS_INTER           =
                :DTC-COMMIS-INTER
                                       :IND-DTC-COMMIS-INTER
                  ,CNBT_ANTIRAC           =
                :DTC-CNBT-ANTIRAC
                                       :IND-DTC-CNBT-ANTIRAC
                WHERE     DS_RIGA = :DTC-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-DTC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-DTC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-DTC
           INTO
                :DTC-ID-DETT-TIT-CONT
               ,:DTC-ID-TIT-CONT
               ,:DTC-ID-OGG
               ,:DTC-TP-OGG
               ,:DTC-ID-MOVI-CRZ
               ,:DTC-ID-MOVI-CHIU
                :IND-DTC-ID-MOVI-CHIU
               ,:DTC-DT-INI-EFF-DB
               ,:DTC-DT-END-EFF-DB
               ,:DTC-COD-COMP-ANIA
               ,:DTC-DT-INI-COP-DB
                :IND-DTC-DT-INI-COP
               ,:DTC-DT-END-COP-DB
                :IND-DTC-DT-END-COP
               ,:DTC-PRE-NET
                :IND-DTC-PRE-NET
               ,:DTC-INTR-FRAZ
                :IND-DTC-INTR-FRAZ
               ,:DTC-INTR-MORA
                :IND-DTC-INTR-MORA
               ,:DTC-INTR-RETDT
                :IND-DTC-INTR-RETDT
               ,:DTC-INTR-RIAT
                :IND-DTC-INTR-RIAT
               ,:DTC-DIR
                :IND-DTC-DIR
               ,:DTC-SPE-MED
                :IND-DTC-SPE-MED
               ,:DTC-TAX
                :IND-DTC-TAX
               ,:DTC-SOPR-SAN
                :IND-DTC-SOPR-SAN
               ,:DTC-SOPR-SPO
                :IND-DTC-SOPR-SPO
               ,:DTC-SOPR-TEC
                :IND-DTC-SOPR-TEC
               ,:DTC-SOPR-PROF
                :IND-DTC-SOPR-PROF
               ,:DTC-SOPR-ALT
                :IND-DTC-SOPR-ALT
               ,:DTC-PRE-TOT
                :IND-DTC-PRE-TOT
               ,:DTC-PRE-PP-IAS
                :IND-DTC-PRE-PP-IAS
               ,:DTC-PRE-SOLO-RSH
                :IND-DTC-PRE-SOLO-RSH
               ,:DTC-CAR-ACQ
                :IND-DTC-CAR-ACQ
               ,:DTC-CAR-GEST
                :IND-DTC-CAR-GEST
               ,:DTC-CAR-INC
                :IND-DTC-CAR-INC
               ,:DTC-PROV-ACQ-1AA
                :IND-DTC-PROV-ACQ-1AA
               ,:DTC-PROV-ACQ-2AA
                :IND-DTC-PROV-ACQ-2AA
               ,:DTC-PROV-RICOR
                :IND-DTC-PROV-RICOR
               ,:DTC-PROV-INC
                :IND-DTC-PROV-INC
               ,:DTC-PROV-DA-REC
                :IND-DTC-PROV-DA-REC
               ,:DTC-COD-DVS
                :IND-DTC-COD-DVS
               ,:DTC-FRQ-MOVI
                :IND-DTC-FRQ-MOVI
               ,:DTC-TP-RGM-FISC
               ,:DTC-COD-TARI
                :IND-DTC-COD-TARI
               ,:DTC-TP-STAT-TIT
               ,:DTC-IMP-AZ
                :IND-DTC-IMP-AZ
               ,:DTC-IMP-ADER
                :IND-DTC-IMP-ADER
               ,:DTC-IMP-TFR
                :IND-DTC-IMP-TFR
               ,:DTC-IMP-VOLO
                :IND-DTC-IMP-VOLO
               ,:DTC-MANFEE-ANTIC
                :IND-DTC-MANFEE-ANTIC
               ,:DTC-MANFEE-RICOR
                :IND-DTC-MANFEE-RICOR
               ,:DTC-MANFEE-REC
                :IND-DTC-MANFEE-REC
               ,:DTC-DT-ESI-TIT-DB
                :IND-DTC-DT-ESI-TIT
               ,:DTC-SPE-AGE
                :IND-DTC-SPE-AGE
               ,:DTC-CAR-IAS
                :IND-DTC-CAR-IAS
               ,:DTC-TOT-INTR-PREST
                :IND-DTC-TOT-INTR-PREST
               ,:DTC-DS-RIGA
               ,:DTC-DS-OPER-SQL
               ,:DTC-DS-VER
               ,:DTC-DS-TS-INI-CPTZ
               ,:DTC-DS-TS-END-CPTZ
               ,:DTC-DS-UTENTE
               ,:DTC-DS-STATO-ELAB
               ,:DTC-IMP-TRASFE
                :IND-DTC-IMP-TRASFE
               ,:DTC-IMP-TFR-STRC
                :IND-DTC-IMP-TFR-STRC
               ,:DTC-NUM-GG-RITARDO-PAG
                :IND-DTC-NUM-GG-RITARDO-PAG
               ,:DTC-NUM-GG-RIVAL
                :IND-DTC-NUM-GG-RIVAL
               ,:DTC-ACQ-EXP
                :IND-DTC-ACQ-EXP
               ,:DTC-REMUN-ASS
                :IND-DTC-REMUN-ASS
               ,:DTC-COMMIS-INTER
                :IND-DTC-COMMIS-INTER
               ,:DTC-CNBT-ANTIRAC
                :IND-DTC-CNBT-ANTIRAC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-EFF-DTC CURSOR FOR
              SELECT
                     ID_DETT_TIT_CONT
                    ,ID_TIT_CONT
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,DT_INI_COP
                    ,DT_END_COP
                    ,PRE_NET
                    ,INTR_FRAZ
                    ,INTR_MORA
                    ,INTR_RETDT
                    ,INTR_RIAT
                    ,DIR
                    ,SPE_MED
                    ,TAX
                    ,SOPR_SAN
                    ,SOPR_SPO
                    ,SOPR_TEC
                    ,SOPR_PROF
                    ,SOPR_ALT
                    ,PRE_TOT
                    ,PRE_PP_IAS
                    ,PRE_SOLO_RSH
                    ,CAR_ACQ
                    ,CAR_GEST
                    ,CAR_INC
                    ,PROV_ACQ_1AA
                    ,PROV_ACQ_2AA
                    ,PROV_RICOR
                    ,PROV_INC
                    ,PROV_DA_REC
                    ,COD_DVS
                    ,FRQ_MOVI
                    ,TP_RGM_FISC
                    ,COD_TARI
                    ,TP_STAT_TIT
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,MANFEE_ANTIC
                    ,MANFEE_RICOR
                    ,MANFEE_REC
                    ,DT_ESI_TIT
                    ,SPE_AGE
                    ,CAR_IAS
                    ,TOT_INTR_PREST
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,IMP_TRASFE
                    ,IMP_TFR_STRC
                    ,NUM_GG_RITARDO_PAG
                    ,NUM_GG_RIVAL
                    ,ACQ_EXP
                    ,REMUN_ASS
                    ,COMMIS_INTER
                    ,CNBT_ANTIRAC
              FROM DETT_TIT_CONT
              WHERE     ID_TIT_CONT = :DTC-ID-TIT-CONT
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_DETT_TIT_CONT ASC

           END-EXEC.

       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_DETT_TIT_CONT
                ,ID_TIT_CONT
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_INI_COP
                ,DT_END_COP
                ,PRE_NET
                ,INTR_FRAZ
                ,INTR_MORA
                ,INTR_RETDT
                ,INTR_RIAT
                ,DIR
                ,SPE_MED
                ,TAX
                ,SOPR_SAN
                ,SOPR_SPO
                ,SOPR_TEC
                ,SOPR_PROF
                ,SOPR_ALT
                ,PRE_TOT
                ,PRE_PP_IAS
                ,PRE_SOLO_RSH
                ,CAR_ACQ
                ,CAR_GEST
                ,CAR_INC
                ,PROV_ACQ_1AA
                ,PROV_ACQ_2AA
                ,PROV_RICOR
                ,PROV_INC
                ,PROV_DA_REC
                ,COD_DVS
                ,FRQ_MOVI
                ,TP_RGM_FISC
                ,COD_TARI
                ,TP_STAT_TIT
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,MANFEE_ANTIC
                ,MANFEE_RICOR
                ,MANFEE_REC
                ,DT_ESI_TIT
                ,SPE_AGE
                ,CAR_IAS
                ,TOT_INTR_PREST
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,NUM_GG_RITARDO_PAG
                ,NUM_GG_RIVAL
                ,ACQ_EXP
                ,REMUN_ASS
                ,COMMIS_INTER
                ,CNBT_ANTIRAC
             INTO
                :DTC-ID-DETT-TIT-CONT
               ,:DTC-ID-TIT-CONT
               ,:DTC-ID-OGG
               ,:DTC-TP-OGG
               ,:DTC-ID-MOVI-CRZ
               ,:DTC-ID-MOVI-CHIU
                :IND-DTC-ID-MOVI-CHIU
               ,:DTC-DT-INI-EFF-DB
               ,:DTC-DT-END-EFF-DB
               ,:DTC-COD-COMP-ANIA
               ,:DTC-DT-INI-COP-DB
                :IND-DTC-DT-INI-COP
               ,:DTC-DT-END-COP-DB
                :IND-DTC-DT-END-COP
               ,:DTC-PRE-NET
                :IND-DTC-PRE-NET
               ,:DTC-INTR-FRAZ
                :IND-DTC-INTR-FRAZ
               ,:DTC-INTR-MORA
                :IND-DTC-INTR-MORA
               ,:DTC-INTR-RETDT
                :IND-DTC-INTR-RETDT
               ,:DTC-INTR-RIAT
                :IND-DTC-INTR-RIAT
               ,:DTC-DIR
                :IND-DTC-DIR
               ,:DTC-SPE-MED
                :IND-DTC-SPE-MED
               ,:DTC-TAX
                :IND-DTC-TAX
               ,:DTC-SOPR-SAN
                :IND-DTC-SOPR-SAN
               ,:DTC-SOPR-SPO
                :IND-DTC-SOPR-SPO
               ,:DTC-SOPR-TEC
                :IND-DTC-SOPR-TEC
               ,:DTC-SOPR-PROF
                :IND-DTC-SOPR-PROF
               ,:DTC-SOPR-ALT
                :IND-DTC-SOPR-ALT
               ,:DTC-PRE-TOT
                :IND-DTC-PRE-TOT
               ,:DTC-PRE-PP-IAS
                :IND-DTC-PRE-PP-IAS
               ,:DTC-PRE-SOLO-RSH
                :IND-DTC-PRE-SOLO-RSH
               ,:DTC-CAR-ACQ
                :IND-DTC-CAR-ACQ
               ,:DTC-CAR-GEST
                :IND-DTC-CAR-GEST
               ,:DTC-CAR-INC
                :IND-DTC-CAR-INC
               ,:DTC-PROV-ACQ-1AA
                :IND-DTC-PROV-ACQ-1AA
               ,:DTC-PROV-ACQ-2AA
                :IND-DTC-PROV-ACQ-2AA
               ,:DTC-PROV-RICOR
                :IND-DTC-PROV-RICOR
               ,:DTC-PROV-INC
                :IND-DTC-PROV-INC
               ,:DTC-PROV-DA-REC
                :IND-DTC-PROV-DA-REC
               ,:DTC-COD-DVS
                :IND-DTC-COD-DVS
               ,:DTC-FRQ-MOVI
                :IND-DTC-FRQ-MOVI
               ,:DTC-TP-RGM-FISC
               ,:DTC-COD-TARI
                :IND-DTC-COD-TARI
               ,:DTC-TP-STAT-TIT
               ,:DTC-IMP-AZ
                :IND-DTC-IMP-AZ
               ,:DTC-IMP-ADER
                :IND-DTC-IMP-ADER
               ,:DTC-IMP-TFR
                :IND-DTC-IMP-TFR
               ,:DTC-IMP-VOLO
                :IND-DTC-IMP-VOLO
               ,:DTC-MANFEE-ANTIC
                :IND-DTC-MANFEE-ANTIC
               ,:DTC-MANFEE-RICOR
                :IND-DTC-MANFEE-RICOR
               ,:DTC-MANFEE-REC
                :IND-DTC-MANFEE-REC
               ,:DTC-DT-ESI-TIT-DB
                :IND-DTC-DT-ESI-TIT
               ,:DTC-SPE-AGE
                :IND-DTC-SPE-AGE
               ,:DTC-CAR-IAS
                :IND-DTC-CAR-IAS
               ,:DTC-TOT-INTR-PREST
                :IND-DTC-TOT-INTR-PREST
               ,:DTC-DS-RIGA
               ,:DTC-DS-OPER-SQL
               ,:DTC-DS-VER
               ,:DTC-DS-TS-INI-CPTZ
               ,:DTC-DS-TS-END-CPTZ
               ,:DTC-DS-UTENTE
               ,:DTC-DS-STATO-ELAB
               ,:DTC-IMP-TRASFE
                :IND-DTC-IMP-TRASFE
               ,:DTC-IMP-TFR-STRC
                :IND-DTC-IMP-TFR-STRC
               ,:DTC-NUM-GG-RITARDO-PAG
                :IND-DTC-NUM-GG-RITARDO-PAG
               ,:DTC-NUM-GG-RIVAL
                :IND-DTC-NUM-GG-RIVAL
               ,:DTC-ACQ-EXP
                :IND-DTC-ACQ-EXP
               ,:DTC-REMUN-ASS
                :IND-DTC-REMUN-ASS
               ,:DTC-COMMIS-INTER
                :IND-DTC-COMMIS-INTER
               ,:DTC-CNBT-ANTIRAC
                :IND-DTC-CNBT-ANTIRAC
             FROM DETT_TIT_CONT
             WHERE     ID_TIT_CONT = :DTC-ID-TIT-CONT
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           EXEC SQL
                OPEN C-IDP-EFF-DTC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           EXEC SQL
                CLOSE C-IDP-EFF-DTC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           EXEC SQL
                FETCH C-IDP-EFF-DTC
           INTO
                :DTC-ID-DETT-TIT-CONT
               ,:DTC-ID-TIT-CONT
               ,:DTC-ID-OGG
               ,:DTC-TP-OGG
               ,:DTC-ID-MOVI-CRZ
               ,:DTC-ID-MOVI-CHIU
                :IND-DTC-ID-MOVI-CHIU
               ,:DTC-DT-INI-EFF-DB
               ,:DTC-DT-END-EFF-DB
               ,:DTC-COD-COMP-ANIA
               ,:DTC-DT-INI-COP-DB
                :IND-DTC-DT-INI-COP
               ,:DTC-DT-END-COP-DB
                :IND-DTC-DT-END-COP
               ,:DTC-PRE-NET
                :IND-DTC-PRE-NET
               ,:DTC-INTR-FRAZ
                :IND-DTC-INTR-FRAZ
               ,:DTC-INTR-MORA
                :IND-DTC-INTR-MORA
               ,:DTC-INTR-RETDT
                :IND-DTC-INTR-RETDT
               ,:DTC-INTR-RIAT
                :IND-DTC-INTR-RIAT
               ,:DTC-DIR
                :IND-DTC-DIR
               ,:DTC-SPE-MED
                :IND-DTC-SPE-MED
               ,:DTC-TAX
                :IND-DTC-TAX
               ,:DTC-SOPR-SAN
                :IND-DTC-SOPR-SAN
               ,:DTC-SOPR-SPO
                :IND-DTC-SOPR-SPO
               ,:DTC-SOPR-TEC
                :IND-DTC-SOPR-TEC
               ,:DTC-SOPR-PROF
                :IND-DTC-SOPR-PROF
               ,:DTC-SOPR-ALT
                :IND-DTC-SOPR-ALT
               ,:DTC-PRE-TOT
                :IND-DTC-PRE-TOT
               ,:DTC-PRE-PP-IAS
                :IND-DTC-PRE-PP-IAS
               ,:DTC-PRE-SOLO-RSH
                :IND-DTC-PRE-SOLO-RSH
               ,:DTC-CAR-ACQ
                :IND-DTC-CAR-ACQ
               ,:DTC-CAR-GEST
                :IND-DTC-CAR-GEST
               ,:DTC-CAR-INC
                :IND-DTC-CAR-INC
               ,:DTC-PROV-ACQ-1AA
                :IND-DTC-PROV-ACQ-1AA
               ,:DTC-PROV-ACQ-2AA
                :IND-DTC-PROV-ACQ-2AA
               ,:DTC-PROV-RICOR
                :IND-DTC-PROV-RICOR
               ,:DTC-PROV-INC
                :IND-DTC-PROV-INC
               ,:DTC-PROV-DA-REC
                :IND-DTC-PROV-DA-REC
               ,:DTC-COD-DVS
                :IND-DTC-COD-DVS
               ,:DTC-FRQ-MOVI
                :IND-DTC-FRQ-MOVI
               ,:DTC-TP-RGM-FISC
               ,:DTC-COD-TARI
                :IND-DTC-COD-TARI
               ,:DTC-TP-STAT-TIT
               ,:DTC-IMP-AZ
                :IND-DTC-IMP-AZ
               ,:DTC-IMP-ADER
                :IND-DTC-IMP-ADER
               ,:DTC-IMP-TFR
                :IND-DTC-IMP-TFR
               ,:DTC-IMP-VOLO
                :IND-DTC-IMP-VOLO
               ,:DTC-MANFEE-ANTIC
                :IND-DTC-MANFEE-ANTIC
               ,:DTC-MANFEE-RICOR
                :IND-DTC-MANFEE-RICOR
               ,:DTC-MANFEE-REC
                :IND-DTC-MANFEE-REC
               ,:DTC-DT-ESI-TIT-DB
                :IND-DTC-DT-ESI-TIT
               ,:DTC-SPE-AGE
                :IND-DTC-SPE-AGE
               ,:DTC-CAR-IAS
                :IND-DTC-CAR-IAS
               ,:DTC-TOT-INTR-PREST
                :IND-DTC-TOT-INTR-PREST
               ,:DTC-DS-RIGA
               ,:DTC-DS-OPER-SQL
               ,:DTC-DS-VER
               ,:DTC-DS-TS-INI-CPTZ
               ,:DTC-DS-TS-END-CPTZ
               ,:DTC-DS-UTENTE
               ,:DTC-DS-STATO-ELAB
               ,:DTC-IMP-TRASFE
                :IND-DTC-IMP-TRASFE
               ,:DTC-IMP-TFR-STRC
                :IND-DTC-IMP-TFR-STRC
               ,:DTC-NUM-GG-RITARDO-PAG
                :IND-DTC-NUM-GG-RITARDO-PAG
               ,:DTC-NUM-GG-RIVAL
                :IND-DTC-NUM-GG-RIVAL
               ,:DTC-ACQ-EXP
                :IND-DTC-ACQ-EXP
               ,:DTC-REMUN-ASS
                :IND-DTC-REMUN-ASS
               ,:DTC-COMMIS-INTER
                :IND-DTC-COMMIS-INTER
               ,:DTC-CNBT-ANTIRAC
                :IND-DTC-CNBT-ANTIRAC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDO-EFF-DTC CURSOR FOR
              SELECT
                     ID_DETT_TIT_CONT
                    ,ID_TIT_CONT
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,DT_INI_COP
                    ,DT_END_COP
                    ,PRE_NET
                    ,INTR_FRAZ
                    ,INTR_MORA
                    ,INTR_RETDT
                    ,INTR_RIAT
                    ,DIR
                    ,SPE_MED
                    ,TAX
                    ,SOPR_SAN
                    ,SOPR_SPO
                    ,SOPR_TEC
                    ,SOPR_PROF
                    ,SOPR_ALT
                    ,PRE_TOT
                    ,PRE_PP_IAS
                    ,PRE_SOLO_RSH
                    ,CAR_ACQ
                    ,CAR_GEST
                    ,CAR_INC
                    ,PROV_ACQ_1AA
                    ,PROV_ACQ_2AA
                    ,PROV_RICOR
                    ,PROV_INC
                    ,PROV_DA_REC
                    ,COD_DVS
                    ,FRQ_MOVI
                    ,TP_RGM_FISC
                    ,COD_TARI
                    ,TP_STAT_TIT
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,MANFEE_ANTIC
                    ,MANFEE_RICOR
                    ,MANFEE_REC
                    ,DT_ESI_TIT
                    ,SPE_AGE
                    ,CAR_IAS
                    ,TOT_INTR_PREST
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,IMP_TRASFE
                    ,IMP_TFR_STRC
                    ,NUM_GG_RITARDO_PAG
                    ,NUM_GG_RIVAL
                    ,ACQ_EXP
                    ,REMUN_ASS
                    ,COMMIS_INTER
                    ,CNBT_ANTIRAC
              FROM DETT_TIT_CONT
              WHERE     ID_OGG = :DTC-ID-OGG
                    AND TP_OGG = :DTC-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_DETT_TIT_CONT ASC

           END-EXEC.

       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_DETT_TIT_CONT
                ,ID_TIT_CONT
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_INI_COP
                ,DT_END_COP
                ,PRE_NET
                ,INTR_FRAZ
                ,INTR_MORA
                ,INTR_RETDT
                ,INTR_RIAT
                ,DIR
                ,SPE_MED
                ,TAX
                ,SOPR_SAN
                ,SOPR_SPO
                ,SOPR_TEC
                ,SOPR_PROF
                ,SOPR_ALT
                ,PRE_TOT
                ,PRE_PP_IAS
                ,PRE_SOLO_RSH
                ,CAR_ACQ
                ,CAR_GEST
                ,CAR_INC
                ,PROV_ACQ_1AA
                ,PROV_ACQ_2AA
                ,PROV_RICOR
                ,PROV_INC
                ,PROV_DA_REC
                ,COD_DVS
                ,FRQ_MOVI
                ,TP_RGM_FISC
                ,COD_TARI
                ,TP_STAT_TIT
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,MANFEE_ANTIC
                ,MANFEE_RICOR
                ,MANFEE_REC
                ,DT_ESI_TIT
                ,SPE_AGE
                ,CAR_IAS
                ,TOT_INTR_PREST
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,NUM_GG_RITARDO_PAG
                ,NUM_GG_RIVAL
                ,ACQ_EXP
                ,REMUN_ASS
                ,COMMIS_INTER
                ,CNBT_ANTIRAC
             INTO
                :DTC-ID-DETT-TIT-CONT
               ,:DTC-ID-TIT-CONT
               ,:DTC-ID-OGG
               ,:DTC-TP-OGG
               ,:DTC-ID-MOVI-CRZ
               ,:DTC-ID-MOVI-CHIU
                :IND-DTC-ID-MOVI-CHIU
               ,:DTC-DT-INI-EFF-DB
               ,:DTC-DT-END-EFF-DB
               ,:DTC-COD-COMP-ANIA
               ,:DTC-DT-INI-COP-DB
                :IND-DTC-DT-INI-COP
               ,:DTC-DT-END-COP-DB
                :IND-DTC-DT-END-COP
               ,:DTC-PRE-NET
                :IND-DTC-PRE-NET
               ,:DTC-INTR-FRAZ
                :IND-DTC-INTR-FRAZ
               ,:DTC-INTR-MORA
                :IND-DTC-INTR-MORA
               ,:DTC-INTR-RETDT
                :IND-DTC-INTR-RETDT
               ,:DTC-INTR-RIAT
                :IND-DTC-INTR-RIAT
               ,:DTC-DIR
                :IND-DTC-DIR
               ,:DTC-SPE-MED
                :IND-DTC-SPE-MED
               ,:DTC-TAX
                :IND-DTC-TAX
               ,:DTC-SOPR-SAN
                :IND-DTC-SOPR-SAN
               ,:DTC-SOPR-SPO
                :IND-DTC-SOPR-SPO
               ,:DTC-SOPR-TEC
                :IND-DTC-SOPR-TEC
               ,:DTC-SOPR-PROF
                :IND-DTC-SOPR-PROF
               ,:DTC-SOPR-ALT
                :IND-DTC-SOPR-ALT
               ,:DTC-PRE-TOT
                :IND-DTC-PRE-TOT
               ,:DTC-PRE-PP-IAS
                :IND-DTC-PRE-PP-IAS
               ,:DTC-PRE-SOLO-RSH
                :IND-DTC-PRE-SOLO-RSH
               ,:DTC-CAR-ACQ
                :IND-DTC-CAR-ACQ
               ,:DTC-CAR-GEST
                :IND-DTC-CAR-GEST
               ,:DTC-CAR-INC
                :IND-DTC-CAR-INC
               ,:DTC-PROV-ACQ-1AA
                :IND-DTC-PROV-ACQ-1AA
               ,:DTC-PROV-ACQ-2AA
                :IND-DTC-PROV-ACQ-2AA
               ,:DTC-PROV-RICOR
                :IND-DTC-PROV-RICOR
               ,:DTC-PROV-INC
                :IND-DTC-PROV-INC
               ,:DTC-PROV-DA-REC
                :IND-DTC-PROV-DA-REC
               ,:DTC-COD-DVS
                :IND-DTC-COD-DVS
               ,:DTC-FRQ-MOVI
                :IND-DTC-FRQ-MOVI
               ,:DTC-TP-RGM-FISC
               ,:DTC-COD-TARI
                :IND-DTC-COD-TARI
               ,:DTC-TP-STAT-TIT
               ,:DTC-IMP-AZ
                :IND-DTC-IMP-AZ
               ,:DTC-IMP-ADER
                :IND-DTC-IMP-ADER
               ,:DTC-IMP-TFR
                :IND-DTC-IMP-TFR
               ,:DTC-IMP-VOLO
                :IND-DTC-IMP-VOLO
               ,:DTC-MANFEE-ANTIC
                :IND-DTC-MANFEE-ANTIC
               ,:DTC-MANFEE-RICOR
                :IND-DTC-MANFEE-RICOR
               ,:DTC-MANFEE-REC
                :IND-DTC-MANFEE-REC
               ,:DTC-DT-ESI-TIT-DB
                :IND-DTC-DT-ESI-TIT
               ,:DTC-SPE-AGE
                :IND-DTC-SPE-AGE
               ,:DTC-CAR-IAS
                :IND-DTC-CAR-IAS
               ,:DTC-TOT-INTR-PREST
                :IND-DTC-TOT-INTR-PREST
               ,:DTC-DS-RIGA
               ,:DTC-DS-OPER-SQL
               ,:DTC-DS-VER
               ,:DTC-DS-TS-INI-CPTZ
               ,:DTC-DS-TS-END-CPTZ
               ,:DTC-DS-UTENTE
               ,:DTC-DS-STATO-ELAB
               ,:DTC-IMP-TRASFE
                :IND-DTC-IMP-TRASFE
               ,:DTC-IMP-TFR-STRC
                :IND-DTC-IMP-TFR-STRC
               ,:DTC-NUM-GG-RITARDO-PAG
                :IND-DTC-NUM-GG-RITARDO-PAG
               ,:DTC-NUM-GG-RIVAL
                :IND-DTC-NUM-GG-RIVAL
               ,:DTC-ACQ-EXP
                :IND-DTC-ACQ-EXP
               ,:DTC-REMUN-ASS
                :IND-DTC-REMUN-ASS
               ,:DTC-COMMIS-INTER
                :IND-DTC-COMMIS-INTER
               ,:DTC-CNBT-ANTIRAC
                :IND-DTC-CNBT-ANTIRAC
             FROM DETT_TIT_CONT
             WHERE     ID_OGG = :DTC-ID-OGG
                    AND TP_OGG = :DTC-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           EXEC SQL
                OPEN C-IDO-EFF-DTC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           EXEC SQL
                CLOSE C-IDO-EFF-DTC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           EXEC SQL
                FETCH C-IDO-EFF-DTC
           INTO
                :DTC-ID-DETT-TIT-CONT
               ,:DTC-ID-TIT-CONT
               ,:DTC-ID-OGG
               ,:DTC-TP-OGG
               ,:DTC-ID-MOVI-CRZ
               ,:DTC-ID-MOVI-CHIU
                :IND-DTC-ID-MOVI-CHIU
               ,:DTC-DT-INI-EFF-DB
               ,:DTC-DT-END-EFF-DB
               ,:DTC-COD-COMP-ANIA
               ,:DTC-DT-INI-COP-DB
                :IND-DTC-DT-INI-COP
               ,:DTC-DT-END-COP-DB
                :IND-DTC-DT-END-COP
               ,:DTC-PRE-NET
                :IND-DTC-PRE-NET
               ,:DTC-INTR-FRAZ
                :IND-DTC-INTR-FRAZ
               ,:DTC-INTR-MORA
                :IND-DTC-INTR-MORA
               ,:DTC-INTR-RETDT
                :IND-DTC-INTR-RETDT
               ,:DTC-INTR-RIAT
                :IND-DTC-INTR-RIAT
               ,:DTC-DIR
                :IND-DTC-DIR
               ,:DTC-SPE-MED
                :IND-DTC-SPE-MED
               ,:DTC-TAX
                :IND-DTC-TAX
               ,:DTC-SOPR-SAN
                :IND-DTC-SOPR-SAN
               ,:DTC-SOPR-SPO
                :IND-DTC-SOPR-SPO
               ,:DTC-SOPR-TEC
                :IND-DTC-SOPR-TEC
               ,:DTC-SOPR-PROF
                :IND-DTC-SOPR-PROF
               ,:DTC-SOPR-ALT
                :IND-DTC-SOPR-ALT
               ,:DTC-PRE-TOT
                :IND-DTC-PRE-TOT
               ,:DTC-PRE-PP-IAS
                :IND-DTC-PRE-PP-IAS
               ,:DTC-PRE-SOLO-RSH
                :IND-DTC-PRE-SOLO-RSH
               ,:DTC-CAR-ACQ
                :IND-DTC-CAR-ACQ
               ,:DTC-CAR-GEST
                :IND-DTC-CAR-GEST
               ,:DTC-CAR-INC
                :IND-DTC-CAR-INC
               ,:DTC-PROV-ACQ-1AA
                :IND-DTC-PROV-ACQ-1AA
               ,:DTC-PROV-ACQ-2AA
                :IND-DTC-PROV-ACQ-2AA
               ,:DTC-PROV-RICOR
                :IND-DTC-PROV-RICOR
               ,:DTC-PROV-INC
                :IND-DTC-PROV-INC
               ,:DTC-PROV-DA-REC
                :IND-DTC-PROV-DA-REC
               ,:DTC-COD-DVS
                :IND-DTC-COD-DVS
               ,:DTC-FRQ-MOVI
                :IND-DTC-FRQ-MOVI
               ,:DTC-TP-RGM-FISC
               ,:DTC-COD-TARI
                :IND-DTC-COD-TARI
               ,:DTC-TP-STAT-TIT
               ,:DTC-IMP-AZ
                :IND-DTC-IMP-AZ
               ,:DTC-IMP-ADER
                :IND-DTC-IMP-ADER
               ,:DTC-IMP-TFR
                :IND-DTC-IMP-TFR
               ,:DTC-IMP-VOLO
                :IND-DTC-IMP-VOLO
               ,:DTC-MANFEE-ANTIC
                :IND-DTC-MANFEE-ANTIC
               ,:DTC-MANFEE-RICOR
                :IND-DTC-MANFEE-RICOR
               ,:DTC-MANFEE-REC
                :IND-DTC-MANFEE-REC
               ,:DTC-DT-ESI-TIT-DB
                :IND-DTC-DT-ESI-TIT
               ,:DTC-SPE-AGE
                :IND-DTC-SPE-AGE
               ,:DTC-CAR-IAS
                :IND-DTC-CAR-IAS
               ,:DTC-TOT-INTR-PREST
                :IND-DTC-TOT-INTR-PREST
               ,:DTC-DS-RIGA
               ,:DTC-DS-OPER-SQL
               ,:DTC-DS-VER
               ,:DTC-DS-TS-INI-CPTZ
               ,:DTC-DS-TS-END-CPTZ
               ,:DTC-DS-UTENTE
               ,:DTC-DS-STATO-ELAB
               ,:DTC-IMP-TRASFE
                :IND-DTC-IMP-TRASFE
               ,:DTC-IMP-TFR-STRC
                :IND-DTC-IMP-TFR-STRC
               ,:DTC-NUM-GG-RITARDO-PAG
                :IND-DTC-NUM-GG-RITARDO-PAG
               ,:DTC-NUM-GG-RIVAL
                :IND-DTC-NUM-GG-RIVAL
               ,:DTC-ACQ-EXP
                :IND-DTC-ACQ-EXP
               ,:DTC-REMUN-ASS
                :IND-DTC-REMUN-ASS
               ,:DTC-COMMIS-INTER
                :IND-DTC-COMMIS-INTER
               ,:DTC-CNBT-ANTIRAC
                :IND-DTC-CNBT-ANTIRAC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A770-CLOSE-CURSOR-IDO     THRU A770-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_DETT_TIT_CONT
                ,ID_TIT_CONT
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_INI_COP
                ,DT_END_COP
                ,PRE_NET
                ,INTR_FRAZ
                ,INTR_MORA
                ,INTR_RETDT
                ,INTR_RIAT
                ,DIR
                ,SPE_MED
                ,TAX
                ,SOPR_SAN
                ,SOPR_SPO
                ,SOPR_TEC
                ,SOPR_PROF
                ,SOPR_ALT
                ,PRE_TOT
                ,PRE_PP_IAS
                ,PRE_SOLO_RSH
                ,CAR_ACQ
                ,CAR_GEST
                ,CAR_INC
                ,PROV_ACQ_1AA
                ,PROV_ACQ_2AA
                ,PROV_RICOR
                ,PROV_INC
                ,PROV_DA_REC
                ,COD_DVS
                ,FRQ_MOVI
                ,TP_RGM_FISC
                ,COD_TARI
                ,TP_STAT_TIT
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,MANFEE_ANTIC
                ,MANFEE_RICOR
                ,MANFEE_REC
                ,DT_ESI_TIT
                ,SPE_AGE
                ,CAR_IAS
                ,TOT_INTR_PREST
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,NUM_GG_RITARDO_PAG
                ,NUM_GG_RIVAL
                ,ACQ_EXP
                ,REMUN_ASS
                ,COMMIS_INTER
                ,CNBT_ANTIRAC
             INTO
                :DTC-ID-DETT-TIT-CONT
               ,:DTC-ID-TIT-CONT
               ,:DTC-ID-OGG
               ,:DTC-TP-OGG
               ,:DTC-ID-MOVI-CRZ
               ,:DTC-ID-MOVI-CHIU
                :IND-DTC-ID-MOVI-CHIU
               ,:DTC-DT-INI-EFF-DB
               ,:DTC-DT-END-EFF-DB
               ,:DTC-COD-COMP-ANIA
               ,:DTC-DT-INI-COP-DB
                :IND-DTC-DT-INI-COP
               ,:DTC-DT-END-COP-DB
                :IND-DTC-DT-END-COP
               ,:DTC-PRE-NET
                :IND-DTC-PRE-NET
               ,:DTC-INTR-FRAZ
                :IND-DTC-INTR-FRAZ
               ,:DTC-INTR-MORA
                :IND-DTC-INTR-MORA
               ,:DTC-INTR-RETDT
                :IND-DTC-INTR-RETDT
               ,:DTC-INTR-RIAT
                :IND-DTC-INTR-RIAT
               ,:DTC-DIR
                :IND-DTC-DIR
               ,:DTC-SPE-MED
                :IND-DTC-SPE-MED
               ,:DTC-TAX
                :IND-DTC-TAX
               ,:DTC-SOPR-SAN
                :IND-DTC-SOPR-SAN
               ,:DTC-SOPR-SPO
                :IND-DTC-SOPR-SPO
               ,:DTC-SOPR-TEC
                :IND-DTC-SOPR-TEC
               ,:DTC-SOPR-PROF
                :IND-DTC-SOPR-PROF
               ,:DTC-SOPR-ALT
                :IND-DTC-SOPR-ALT
               ,:DTC-PRE-TOT
                :IND-DTC-PRE-TOT
               ,:DTC-PRE-PP-IAS
                :IND-DTC-PRE-PP-IAS
               ,:DTC-PRE-SOLO-RSH
                :IND-DTC-PRE-SOLO-RSH
               ,:DTC-CAR-ACQ
                :IND-DTC-CAR-ACQ
               ,:DTC-CAR-GEST
                :IND-DTC-CAR-GEST
               ,:DTC-CAR-INC
                :IND-DTC-CAR-INC
               ,:DTC-PROV-ACQ-1AA
                :IND-DTC-PROV-ACQ-1AA
               ,:DTC-PROV-ACQ-2AA
                :IND-DTC-PROV-ACQ-2AA
               ,:DTC-PROV-RICOR
                :IND-DTC-PROV-RICOR
               ,:DTC-PROV-INC
                :IND-DTC-PROV-INC
               ,:DTC-PROV-DA-REC
                :IND-DTC-PROV-DA-REC
               ,:DTC-COD-DVS
                :IND-DTC-COD-DVS
               ,:DTC-FRQ-MOVI
                :IND-DTC-FRQ-MOVI
               ,:DTC-TP-RGM-FISC
               ,:DTC-COD-TARI
                :IND-DTC-COD-TARI
               ,:DTC-TP-STAT-TIT
               ,:DTC-IMP-AZ
                :IND-DTC-IMP-AZ
               ,:DTC-IMP-ADER
                :IND-DTC-IMP-ADER
               ,:DTC-IMP-TFR
                :IND-DTC-IMP-TFR
               ,:DTC-IMP-VOLO
                :IND-DTC-IMP-VOLO
               ,:DTC-MANFEE-ANTIC
                :IND-DTC-MANFEE-ANTIC
               ,:DTC-MANFEE-RICOR
                :IND-DTC-MANFEE-RICOR
               ,:DTC-MANFEE-REC
                :IND-DTC-MANFEE-REC
               ,:DTC-DT-ESI-TIT-DB
                :IND-DTC-DT-ESI-TIT
               ,:DTC-SPE-AGE
                :IND-DTC-SPE-AGE
               ,:DTC-CAR-IAS
                :IND-DTC-CAR-IAS
               ,:DTC-TOT-INTR-PREST
                :IND-DTC-TOT-INTR-PREST
               ,:DTC-DS-RIGA
               ,:DTC-DS-OPER-SQL
               ,:DTC-DS-VER
               ,:DTC-DS-TS-INI-CPTZ
               ,:DTC-DS-TS-END-CPTZ
               ,:DTC-DS-UTENTE
               ,:DTC-DS-STATO-ELAB
               ,:DTC-IMP-TRASFE
                :IND-DTC-IMP-TRASFE
               ,:DTC-IMP-TFR-STRC
                :IND-DTC-IMP-TFR-STRC
               ,:DTC-NUM-GG-RITARDO-PAG
                :IND-DTC-NUM-GG-RITARDO-PAG
               ,:DTC-NUM-GG-RIVAL
                :IND-DTC-NUM-GG-RIVAL
               ,:DTC-ACQ-EXP
                :IND-DTC-ACQ-EXP
               ,:DTC-REMUN-ASS
                :IND-DTC-REMUN-ASS
               ,:DTC-COMMIS-INTER
                :IND-DTC-COMMIS-INTER
               ,:DTC-CNBT-ANTIRAC
                :IND-DTC-CNBT-ANTIRAC
             FROM DETT_TIT_CONT
             WHERE     ID_DETT_TIT_CONT = :DTC-ID-DETT-TIT-CONT
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-CPZ-DTC CURSOR FOR
              SELECT
                     ID_DETT_TIT_CONT
                    ,ID_TIT_CONT
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,DT_INI_COP
                    ,DT_END_COP
                    ,PRE_NET
                    ,INTR_FRAZ
                    ,INTR_MORA
                    ,INTR_RETDT
                    ,INTR_RIAT
                    ,DIR
                    ,SPE_MED
                    ,TAX
                    ,SOPR_SAN
                    ,SOPR_SPO
                    ,SOPR_TEC
                    ,SOPR_PROF
                    ,SOPR_ALT
                    ,PRE_TOT
                    ,PRE_PP_IAS
                    ,PRE_SOLO_RSH
                    ,CAR_ACQ
                    ,CAR_GEST
                    ,CAR_INC
                    ,PROV_ACQ_1AA
                    ,PROV_ACQ_2AA
                    ,PROV_RICOR
                    ,PROV_INC
                    ,PROV_DA_REC
                    ,COD_DVS
                    ,FRQ_MOVI
                    ,TP_RGM_FISC
                    ,COD_TARI
                    ,TP_STAT_TIT
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,MANFEE_ANTIC
                    ,MANFEE_RICOR
                    ,MANFEE_REC
                    ,DT_ESI_TIT
                    ,SPE_AGE
                    ,CAR_IAS
                    ,TOT_INTR_PREST
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,IMP_TRASFE
                    ,IMP_TFR_STRC
                    ,NUM_GG_RITARDO_PAG
                    ,NUM_GG_RIVAL
                    ,ACQ_EXP
                    ,REMUN_ASS
                    ,COMMIS_INTER
                    ,CNBT_ANTIRAC
              FROM DETT_TIT_CONT
              WHERE     ID_TIT_CONT = :DTC-ID-TIT-CONT
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_DETT_TIT_CONT ASC

           END-EXEC.

       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_DETT_TIT_CONT
                ,ID_TIT_CONT
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_INI_COP
                ,DT_END_COP
                ,PRE_NET
                ,INTR_FRAZ
                ,INTR_MORA
                ,INTR_RETDT
                ,INTR_RIAT
                ,DIR
                ,SPE_MED
                ,TAX
                ,SOPR_SAN
                ,SOPR_SPO
                ,SOPR_TEC
                ,SOPR_PROF
                ,SOPR_ALT
                ,PRE_TOT
                ,PRE_PP_IAS
                ,PRE_SOLO_RSH
                ,CAR_ACQ
                ,CAR_GEST
                ,CAR_INC
                ,PROV_ACQ_1AA
                ,PROV_ACQ_2AA
                ,PROV_RICOR
                ,PROV_INC
                ,PROV_DA_REC
                ,COD_DVS
                ,FRQ_MOVI
                ,TP_RGM_FISC
                ,COD_TARI
                ,TP_STAT_TIT
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,MANFEE_ANTIC
                ,MANFEE_RICOR
                ,MANFEE_REC
                ,DT_ESI_TIT
                ,SPE_AGE
                ,CAR_IAS
                ,TOT_INTR_PREST
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,NUM_GG_RITARDO_PAG
                ,NUM_GG_RIVAL
                ,ACQ_EXP
                ,REMUN_ASS
                ,COMMIS_INTER
                ,CNBT_ANTIRAC
             INTO
                :DTC-ID-DETT-TIT-CONT
               ,:DTC-ID-TIT-CONT
               ,:DTC-ID-OGG
               ,:DTC-TP-OGG
               ,:DTC-ID-MOVI-CRZ
               ,:DTC-ID-MOVI-CHIU
                :IND-DTC-ID-MOVI-CHIU
               ,:DTC-DT-INI-EFF-DB
               ,:DTC-DT-END-EFF-DB
               ,:DTC-COD-COMP-ANIA
               ,:DTC-DT-INI-COP-DB
                :IND-DTC-DT-INI-COP
               ,:DTC-DT-END-COP-DB
                :IND-DTC-DT-END-COP
               ,:DTC-PRE-NET
                :IND-DTC-PRE-NET
               ,:DTC-INTR-FRAZ
                :IND-DTC-INTR-FRAZ
               ,:DTC-INTR-MORA
                :IND-DTC-INTR-MORA
               ,:DTC-INTR-RETDT
                :IND-DTC-INTR-RETDT
               ,:DTC-INTR-RIAT
                :IND-DTC-INTR-RIAT
               ,:DTC-DIR
                :IND-DTC-DIR
               ,:DTC-SPE-MED
                :IND-DTC-SPE-MED
               ,:DTC-TAX
                :IND-DTC-TAX
               ,:DTC-SOPR-SAN
                :IND-DTC-SOPR-SAN
               ,:DTC-SOPR-SPO
                :IND-DTC-SOPR-SPO
               ,:DTC-SOPR-TEC
                :IND-DTC-SOPR-TEC
               ,:DTC-SOPR-PROF
                :IND-DTC-SOPR-PROF
               ,:DTC-SOPR-ALT
                :IND-DTC-SOPR-ALT
               ,:DTC-PRE-TOT
                :IND-DTC-PRE-TOT
               ,:DTC-PRE-PP-IAS
                :IND-DTC-PRE-PP-IAS
               ,:DTC-PRE-SOLO-RSH
                :IND-DTC-PRE-SOLO-RSH
               ,:DTC-CAR-ACQ
                :IND-DTC-CAR-ACQ
               ,:DTC-CAR-GEST
                :IND-DTC-CAR-GEST
               ,:DTC-CAR-INC
                :IND-DTC-CAR-INC
               ,:DTC-PROV-ACQ-1AA
                :IND-DTC-PROV-ACQ-1AA
               ,:DTC-PROV-ACQ-2AA
                :IND-DTC-PROV-ACQ-2AA
               ,:DTC-PROV-RICOR
                :IND-DTC-PROV-RICOR
               ,:DTC-PROV-INC
                :IND-DTC-PROV-INC
               ,:DTC-PROV-DA-REC
                :IND-DTC-PROV-DA-REC
               ,:DTC-COD-DVS
                :IND-DTC-COD-DVS
               ,:DTC-FRQ-MOVI
                :IND-DTC-FRQ-MOVI
               ,:DTC-TP-RGM-FISC
               ,:DTC-COD-TARI
                :IND-DTC-COD-TARI
               ,:DTC-TP-STAT-TIT
               ,:DTC-IMP-AZ
                :IND-DTC-IMP-AZ
               ,:DTC-IMP-ADER
                :IND-DTC-IMP-ADER
               ,:DTC-IMP-TFR
                :IND-DTC-IMP-TFR
               ,:DTC-IMP-VOLO
                :IND-DTC-IMP-VOLO
               ,:DTC-MANFEE-ANTIC
                :IND-DTC-MANFEE-ANTIC
               ,:DTC-MANFEE-RICOR
                :IND-DTC-MANFEE-RICOR
               ,:DTC-MANFEE-REC
                :IND-DTC-MANFEE-REC
               ,:DTC-DT-ESI-TIT-DB
                :IND-DTC-DT-ESI-TIT
               ,:DTC-SPE-AGE
                :IND-DTC-SPE-AGE
               ,:DTC-CAR-IAS
                :IND-DTC-CAR-IAS
               ,:DTC-TOT-INTR-PREST
                :IND-DTC-TOT-INTR-PREST
               ,:DTC-DS-RIGA
               ,:DTC-DS-OPER-SQL
               ,:DTC-DS-VER
               ,:DTC-DS-TS-INI-CPTZ
               ,:DTC-DS-TS-END-CPTZ
               ,:DTC-DS-UTENTE
               ,:DTC-DS-STATO-ELAB
               ,:DTC-IMP-TRASFE
                :IND-DTC-IMP-TRASFE
               ,:DTC-IMP-TFR-STRC
                :IND-DTC-IMP-TFR-STRC
               ,:DTC-NUM-GG-RITARDO-PAG
                :IND-DTC-NUM-GG-RITARDO-PAG
               ,:DTC-NUM-GG-RIVAL
                :IND-DTC-NUM-GG-RIVAL
               ,:DTC-ACQ-EXP
                :IND-DTC-ACQ-EXP
               ,:DTC-REMUN-ASS
                :IND-DTC-REMUN-ASS
               ,:DTC-COMMIS-INTER
                :IND-DTC-COMMIS-INTER
               ,:DTC-CNBT-ANTIRAC
                :IND-DTC-CNBT-ANTIRAC
             FROM DETT_TIT_CONT
             WHERE     ID_TIT_CONT = :DTC-ID-TIT-CONT
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           EXEC SQL
                OPEN C-IDP-CPZ-DTC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           EXEC SQL
                CLOSE C-IDP-CPZ-DTC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           EXEC SQL
                FETCH C-IDP-CPZ-DTC
           INTO
                :DTC-ID-DETT-TIT-CONT
               ,:DTC-ID-TIT-CONT
               ,:DTC-ID-OGG
               ,:DTC-TP-OGG
               ,:DTC-ID-MOVI-CRZ
               ,:DTC-ID-MOVI-CHIU
                :IND-DTC-ID-MOVI-CHIU
               ,:DTC-DT-INI-EFF-DB
               ,:DTC-DT-END-EFF-DB
               ,:DTC-COD-COMP-ANIA
               ,:DTC-DT-INI-COP-DB
                :IND-DTC-DT-INI-COP
               ,:DTC-DT-END-COP-DB
                :IND-DTC-DT-END-COP
               ,:DTC-PRE-NET
                :IND-DTC-PRE-NET
               ,:DTC-INTR-FRAZ
                :IND-DTC-INTR-FRAZ
               ,:DTC-INTR-MORA
                :IND-DTC-INTR-MORA
               ,:DTC-INTR-RETDT
                :IND-DTC-INTR-RETDT
               ,:DTC-INTR-RIAT
                :IND-DTC-INTR-RIAT
               ,:DTC-DIR
                :IND-DTC-DIR
               ,:DTC-SPE-MED
                :IND-DTC-SPE-MED
               ,:DTC-TAX
                :IND-DTC-TAX
               ,:DTC-SOPR-SAN
                :IND-DTC-SOPR-SAN
               ,:DTC-SOPR-SPO
                :IND-DTC-SOPR-SPO
               ,:DTC-SOPR-TEC
                :IND-DTC-SOPR-TEC
               ,:DTC-SOPR-PROF
                :IND-DTC-SOPR-PROF
               ,:DTC-SOPR-ALT
                :IND-DTC-SOPR-ALT
               ,:DTC-PRE-TOT
                :IND-DTC-PRE-TOT
               ,:DTC-PRE-PP-IAS
                :IND-DTC-PRE-PP-IAS
               ,:DTC-PRE-SOLO-RSH
                :IND-DTC-PRE-SOLO-RSH
               ,:DTC-CAR-ACQ
                :IND-DTC-CAR-ACQ
               ,:DTC-CAR-GEST
                :IND-DTC-CAR-GEST
               ,:DTC-CAR-INC
                :IND-DTC-CAR-INC
               ,:DTC-PROV-ACQ-1AA
                :IND-DTC-PROV-ACQ-1AA
               ,:DTC-PROV-ACQ-2AA
                :IND-DTC-PROV-ACQ-2AA
               ,:DTC-PROV-RICOR
                :IND-DTC-PROV-RICOR
               ,:DTC-PROV-INC
                :IND-DTC-PROV-INC
               ,:DTC-PROV-DA-REC
                :IND-DTC-PROV-DA-REC
               ,:DTC-COD-DVS
                :IND-DTC-COD-DVS
               ,:DTC-FRQ-MOVI
                :IND-DTC-FRQ-MOVI
               ,:DTC-TP-RGM-FISC
               ,:DTC-COD-TARI
                :IND-DTC-COD-TARI
               ,:DTC-TP-STAT-TIT
               ,:DTC-IMP-AZ
                :IND-DTC-IMP-AZ
               ,:DTC-IMP-ADER
                :IND-DTC-IMP-ADER
               ,:DTC-IMP-TFR
                :IND-DTC-IMP-TFR
               ,:DTC-IMP-VOLO
                :IND-DTC-IMP-VOLO
               ,:DTC-MANFEE-ANTIC
                :IND-DTC-MANFEE-ANTIC
               ,:DTC-MANFEE-RICOR
                :IND-DTC-MANFEE-RICOR
               ,:DTC-MANFEE-REC
                :IND-DTC-MANFEE-REC
               ,:DTC-DT-ESI-TIT-DB
                :IND-DTC-DT-ESI-TIT
               ,:DTC-SPE-AGE
                :IND-DTC-SPE-AGE
               ,:DTC-CAR-IAS
                :IND-DTC-CAR-IAS
               ,:DTC-TOT-INTR-PREST
                :IND-DTC-TOT-INTR-PREST
               ,:DTC-DS-RIGA
               ,:DTC-DS-OPER-SQL
               ,:DTC-DS-VER
               ,:DTC-DS-TS-INI-CPTZ
               ,:DTC-DS-TS-END-CPTZ
               ,:DTC-DS-UTENTE
               ,:DTC-DS-STATO-ELAB
               ,:DTC-IMP-TRASFE
                :IND-DTC-IMP-TRASFE
               ,:DTC-IMP-TFR-STRC
                :IND-DTC-IMP-TFR-STRC
               ,:DTC-NUM-GG-RITARDO-PAG
                :IND-DTC-NUM-GG-RITARDO-PAG
               ,:DTC-NUM-GG-RIVAL
                :IND-DTC-NUM-GG-RIVAL
               ,:DTC-ACQ-EXP
                :IND-DTC-ACQ-EXP
               ,:DTC-REMUN-ASS
                :IND-DTC-REMUN-ASS
               ,:DTC-COMMIS-INTER
                :IND-DTC-COMMIS-INTER
               ,:DTC-CNBT-ANTIRAC
                :IND-DTC-CNBT-ANTIRAC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDO-CPZ-DTC CURSOR FOR
              SELECT
                     ID_DETT_TIT_CONT
                    ,ID_TIT_CONT
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,DT_INI_COP
                    ,DT_END_COP
                    ,PRE_NET
                    ,INTR_FRAZ
                    ,INTR_MORA
                    ,INTR_RETDT
                    ,INTR_RIAT
                    ,DIR
                    ,SPE_MED
                    ,TAX
                    ,SOPR_SAN
                    ,SOPR_SPO
                    ,SOPR_TEC
                    ,SOPR_PROF
                    ,SOPR_ALT
                    ,PRE_TOT
                    ,PRE_PP_IAS
                    ,PRE_SOLO_RSH
                    ,CAR_ACQ
                    ,CAR_GEST
                    ,CAR_INC
                    ,PROV_ACQ_1AA
                    ,PROV_ACQ_2AA
                    ,PROV_RICOR
                    ,PROV_INC
                    ,PROV_DA_REC
                    ,COD_DVS
                    ,FRQ_MOVI
                    ,TP_RGM_FISC
                    ,COD_TARI
                    ,TP_STAT_TIT
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,MANFEE_ANTIC
                    ,MANFEE_RICOR
                    ,MANFEE_REC
                    ,DT_ESI_TIT
                    ,SPE_AGE
                    ,CAR_IAS
                    ,TOT_INTR_PREST
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,IMP_TRASFE
                    ,IMP_TFR_STRC
                    ,NUM_GG_RITARDO_PAG
                    ,NUM_GG_RIVAL
                    ,ACQ_EXP
                    ,REMUN_ASS
                    ,COMMIS_INTER
                    ,CNBT_ANTIRAC
              FROM DETT_TIT_CONT
              WHERE     ID_OGG = :DTC-ID-OGG
           AND TP_OGG = :DTC-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_DETT_TIT_CONT ASC

           END-EXEC.

       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_DETT_TIT_CONT
                ,ID_TIT_CONT
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_INI_COP
                ,DT_END_COP
                ,PRE_NET
                ,INTR_FRAZ
                ,INTR_MORA
                ,INTR_RETDT
                ,INTR_RIAT
                ,DIR
                ,SPE_MED
                ,TAX
                ,SOPR_SAN
                ,SOPR_SPO
                ,SOPR_TEC
                ,SOPR_PROF
                ,SOPR_ALT
                ,PRE_TOT
                ,PRE_PP_IAS
                ,PRE_SOLO_RSH
                ,CAR_ACQ
                ,CAR_GEST
                ,CAR_INC
                ,PROV_ACQ_1AA
                ,PROV_ACQ_2AA
                ,PROV_RICOR
                ,PROV_INC
                ,PROV_DA_REC
                ,COD_DVS
                ,FRQ_MOVI
                ,TP_RGM_FISC
                ,COD_TARI
                ,TP_STAT_TIT
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,MANFEE_ANTIC
                ,MANFEE_RICOR
                ,MANFEE_REC
                ,DT_ESI_TIT
                ,SPE_AGE
                ,CAR_IAS
                ,TOT_INTR_PREST
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,NUM_GG_RITARDO_PAG
                ,NUM_GG_RIVAL
                ,ACQ_EXP
                ,REMUN_ASS
                ,COMMIS_INTER
                ,CNBT_ANTIRAC
             INTO
                :DTC-ID-DETT-TIT-CONT
               ,:DTC-ID-TIT-CONT
               ,:DTC-ID-OGG
               ,:DTC-TP-OGG
               ,:DTC-ID-MOVI-CRZ
               ,:DTC-ID-MOVI-CHIU
                :IND-DTC-ID-MOVI-CHIU
               ,:DTC-DT-INI-EFF-DB
               ,:DTC-DT-END-EFF-DB
               ,:DTC-COD-COMP-ANIA
               ,:DTC-DT-INI-COP-DB
                :IND-DTC-DT-INI-COP
               ,:DTC-DT-END-COP-DB
                :IND-DTC-DT-END-COP
               ,:DTC-PRE-NET
                :IND-DTC-PRE-NET
               ,:DTC-INTR-FRAZ
                :IND-DTC-INTR-FRAZ
               ,:DTC-INTR-MORA
                :IND-DTC-INTR-MORA
               ,:DTC-INTR-RETDT
                :IND-DTC-INTR-RETDT
               ,:DTC-INTR-RIAT
                :IND-DTC-INTR-RIAT
               ,:DTC-DIR
                :IND-DTC-DIR
               ,:DTC-SPE-MED
                :IND-DTC-SPE-MED
               ,:DTC-TAX
                :IND-DTC-TAX
               ,:DTC-SOPR-SAN
                :IND-DTC-SOPR-SAN
               ,:DTC-SOPR-SPO
                :IND-DTC-SOPR-SPO
               ,:DTC-SOPR-TEC
                :IND-DTC-SOPR-TEC
               ,:DTC-SOPR-PROF
                :IND-DTC-SOPR-PROF
               ,:DTC-SOPR-ALT
                :IND-DTC-SOPR-ALT
               ,:DTC-PRE-TOT
                :IND-DTC-PRE-TOT
               ,:DTC-PRE-PP-IAS
                :IND-DTC-PRE-PP-IAS
               ,:DTC-PRE-SOLO-RSH
                :IND-DTC-PRE-SOLO-RSH
               ,:DTC-CAR-ACQ
                :IND-DTC-CAR-ACQ
               ,:DTC-CAR-GEST
                :IND-DTC-CAR-GEST
               ,:DTC-CAR-INC
                :IND-DTC-CAR-INC
               ,:DTC-PROV-ACQ-1AA
                :IND-DTC-PROV-ACQ-1AA
               ,:DTC-PROV-ACQ-2AA
                :IND-DTC-PROV-ACQ-2AA
               ,:DTC-PROV-RICOR
                :IND-DTC-PROV-RICOR
               ,:DTC-PROV-INC
                :IND-DTC-PROV-INC
               ,:DTC-PROV-DA-REC
                :IND-DTC-PROV-DA-REC
               ,:DTC-COD-DVS
                :IND-DTC-COD-DVS
               ,:DTC-FRQ-MOVI
                :IND-DTC-FRQ-MOVI
               ,:DTC-TP-RGM-FISC
               ,:DTC-COD-TARI
                :IND-DTC-COD-TARI
               ,:DTC-TP-STAT-TIT
               ,:DTC-IMP-AZ
                :IND-DTC-IMP-AZ
               ,:DTC-IMP-ADER
                :IND-DTC-IMP-ADER
               ,:DTC-IMP-TFR
                :IND-DTC-IMP-TFR
               ,:DTC-IMP-VOLO
                :IND-DTC-IMP-VOLO
               ,:DTC-MANFEE-ANTIC
                :IND-DTC-MANFEE-ANTIC
               ,:DTC-MANFEE-RICOR
                :IND-DTC-MANFEE-RICOR
               ,:DTC-MANFEE-REC
                :IND-DTC-MANFEE-REC
               ,:DTC-DT-ESI-TIT-DB
                :IND-DTC-DT-ESI-TIT
               ,:DTC-SPE-AGE
                :IND-DTC-SPE-AGE
               ,:DTC-CAR-IAS
                :IND-DTC-CAR-IAS
               ,:DTC-TOT-INTR-PREST
                :IND-DTC-TOT-INTR-PREST
               ,:DTC-DS-RIGA
               ,:DTC-DS-OPER-SQL
               ,:DTC-DS-VER
               ,:DTC-DS-TS-INI-CPTZ
               ,:DTC-DS-TS-END-CPTZ
               ,:DTC-DS-UTENTE
               ,:DTC-DS-STATO-ELAB
               ,:DTC-IMP-TRASFE
                :IND-DTC-IMP-TRASFE
               ,:DTC-IMP-TFR-STRC
                :IND-DTC-IMP-TFR-STRC
               ,:DTC-NUM-GG-RITARDO-PAG
                :IND-DTC-NUM-GG-RITARDO-PAG
               ,:DTC-NUM-GG-RIVAL
                :IND-DTC-NUM-GG-RIVAL
               ,:DTC-ACQ-EXP
                :IND-DTC-ACQ-EXP
               ,:DTC-REMUN-ASS
                :IND-DTC-REMUN-ASS
               ,:DTC-COMMIS-INTER
                :IND-DTC-COMMIS-INTER
               ,:DTC-CNBT-ANTIRAC
                :IND-DTC-CNBT-ANTIRAC
             FROM DETT_TIT_CONT
             WHERE     ID_OGG = :DTC-ID-OGG
                    AND TP_OGG = :DTC-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           EXEC SQL
                OPEN C-IDO-CPZ-DTC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           EXEC SQL
                CLOSE C-IDO-CPZ-DTC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           EXEC SQL
                FETCH C-IDO-CPZ-DTC
           INTO
                :DTC-ID-DETT-TIT-CONT
               ,:DTC-ID-TIT-CONT
               ,:DTC-ID-OGG
               ,:DTC-TP-OGG
               ,:DTC-ID-MOVI-CRZ
               ,:DTC-ID-MOVI-CHIU
                :IND-DTC-ID-MOVI-CHIU
               ,:DTC-DT-INI-EFF-DB
               ,:DTC-DT-END-EFF-DB
               ,:DTC-COD-COMP-ANIA
               ,:DTC-DT-INI-COP-DB
                :IND-DTC-DT-INI-COP
               ,:DTC-DT-END-COP-DB
                :IND-DTC-DT-END-COP
               ,:DTC-PRE-NET
                :IND-DTC-PRE-NET
               ,:DTC-INTR-FRAZ
                :IND-DTC-INTR-FRAZ
               ,:DTC-INTR-MORA
                :IND-DTC-INTR-MORA
               ,:DTC-INTR-RETDT
                :IND-DTC-INTR-RETDT
               ,:DTC-INTR-RIAT
                :IND-DTC-INTR-RIAT
               ,:DTC-DIR
                :IND-DTC-DIR
               ,:DTC-SPE-MED
                :IND-DTC-SPE-MED
               ,:DTC-TAX
                :IND-DTC-TAX
               ,:DTC-SOPR-SAN
                :IND-DTC-SOPR-SAN
               ,:DTC-SOPR-SPO
                :IND-DTC-SOPR-SPO
               ,:DTC-SOPR-TEC
                :IND-DTC-SOPR-TEC
               ,:DTC-SOPR-PROF
                :IND-DTC-SOPR-PROF
               ,:DTC-SOPR-ALT
                :IND-DTC-SOPR-ALT
               ,:DTC-PRE-TOT
                :IND-DTC-PRE-TOT
               ,:DTC-PRE-PP-IAS
                :IND-DTC-PRE-PP-IAS
               ,:DTC-PRE-SOLO-RSH
                :IND-DTC-PRE-SOLO-RSH
               ,:DTC-CAR-ACQ
                :IND-DTC-CAR-ACQ
               ,:DTC-CAR-GEST
                :IND-DTC-CAR-GEST
               ,:DTC-CAR-INC
                :IND-DTC-CAR-INC
               ,:DTC-PROV-ACQ-1AA
                :IND-DTC-PROV-ACQ-1AA
               ,:DTC-PROV-ACQ-2AA
                :IND-DTC-PROV-ACQ-2AA
               ,:DTC-PROV-RICOR
                :IND-DTC-PROV-RICOR
               ,:DTC-PROV-INC
                :IND-DTC-PROV-INC
               ,:DTC-PROV-DA-REC
                :IND-DTC-PROV-DA-REC
               ,:DTC-COD-DVS
                :IND-DTC-COD-DVS
               ,:DTC-FRQ-MOVI
                :IND-DTC-FRQ-MOVI
               ,:DTC-TP-RGM-FISC
               ,:DTC-COD-TARI
                :IND-DTC-COD-TARI
               ,:DTC-TP-STAT-TIT
               ,:DTC-IMP-AZ
                :IND-DTC-IMP-AZ
               ,:DTC-IMP-ADER
                :IND-DTC-IMP-ADER
               ,:DTC-IMP-TFR
                :IND-DTC-IMP-TFR
               ,:DTC-IMP-VOLO
                :IND-DTC-IMP-VOLO
               ,:DTC-MANFEE-ANTIC
                :IND-DTC-MANFEE-ANTIC
               ,:DTC-MANFEE-RICOR
                :IND-DTC-MANFEE-RICOR
               ,:DTC-MANFEE-REC
                :IND-DTC-MANFEE-REC
               ,:DTC-DT-ESI-TIT-DB
                :IND-DTC-DT-ESI-TIT
               ,:DTC-SPE-AGE
                :IND-DTC-SPE-AGE
               ,:DTC-CAR-IAS
                :IND-DTC-CAR-IAS
               ,:DTC-TOT-INTR-PREST
                :IND-DTC-TOT-INTR-PREST
               ,:DTC-DS-RIGA
               ,:DTC-DS-OPER-SQL
               ,:DTC-DS-VER
               ,:DTC-DS-TS-INI-CPTZ
               ,:DTC-DS-TS-END-CPTZ
               ,:DTC-DS-UTENTE
               ,:DTC-DS-STATO-ELAB
               ,:DTC-IMP-TRASFE
                :IND-DTC-IMP-TRASFE
               ,:DTC-IMP-TFR-STRC
                :IND-DTC-IMP-TFR-STRC
               ,:DTC-NUM-GG-RITARDO-PAG
                :IND-DTC-NUM-GG-RITARDO-PAG
               ,:DTC-NUM-GG-RIVAL
                :IND-DTC-NUM-GG-RIVAL
               ,:DTC-ACQ-EXP
                :IND-DTC-ACQ-EXP
               ,:DTC-REMUN-ASS
                :IND-DTC-REMUN-ASS
               ,:DTC-COMMIS-INTER
                :IND-DTC-COMMIS-INTER
               ,:DTC-CNBT-ANTIRAC
                :IND-DTC-CNBT-ANTIRAC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B770-CLOSE-CURSOR-IDO-CPZ     THRU B770-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-DTC-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO DTC-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-DTC-DT-INI-COP = -1
              MOVE HIGH-VALUES TO DTC-DT-INI-COP-NULL
           END-IF
           IF IND-DTC-DT-END-COP = -1
              MOVE HIGH-VALUES TO DTC-DT-END-COP-NULL
           END-IF
           IF IND-DTC-PRE-NET = -1
              MOVE HIGH-VALUES TO DTC-PRE-NET-NULL
           END-IF
           IF IND-DTC-INTR-FRAZ = -1
              MOVE HIGH-VALUES TO DTC-INTR-FRAZ-NULL
           END-IF
           IF IND-DTC-INTR-MORA = -1
              MOVE HIGH-VALUES TO DTC-INTR-MORA-NULL
           END-IF
           IF IND-DTC-INTR-RETDT = -1
              MOVE HIGH-VALUES TO DTC-INTR-RETDT-NULL
           END-IF
           IF IND-DTC-INTR-RIAT = -1
              MOVE HIGH-VALUES TO DTC-INTR-RIAT-NULL
           END-IF
           IF IND-DTC-DIR = -1
              MOVE HIGH-VALUES TO DTC-DIR-NULL
           END-IF
           IF IND-DTC-SPE-MED = -1
              MOVE HIGH-VALUES TO DTC-SPE-MED-NULL
           END-IF
           IF IND-DTC-TAX = -1
              MOVE HIGH-VALUES TO DTC-TAX-NULL
           END-IF
           IF IND-DTC-SOPR-SAN = -1
              MOVE HIGH-VALUES TO DTC-SOPR-SAN-NULL
           END-IF
           IF IND-DTC-SOPR-SPO = -1
              MOVE HIGH-VALUES TO DTC-SOPR-SPO-NULL
           END-IF
           IF IND-DTC-SOPR-TEC = -1
              MOVE HIGH-VALUES TO DTC-SOPR-TEC-NULL
           END-IF
           IF IND-DTC-SOPR-PROF = -1
              MOVE HIGH-VALUES TO DTC-SOPR-PROF-NULL
           END-IF
           IF IND-DTC-SOPR-ALT = -1
              MOVE HIGH-VALUES TO DTC-SOPR-ALT-NULL
           END-IF
           IF IND-DTC-PRE-TOT = -1
              MOVE HIGH-VALUES TO DTC-PRE-TOT-NULL
           END-IF
           IF IND-DTC-PRE-PP-IAS = -1
              MOVE HIGH-VALUES TO DTC-PRE-PP-IAS-NULL
           END-IF
           IF IND-DTC-PRE-SOLO-RSH = -1
              MOVE HIGH-VALUES TO DTC-PRE-SOLO-RSH-NULL
           END-IF
           IF IND-DTC-CAR-ACQ = -1
              MOVE HIGH-VALUES TO DTC-CAR-ACQ-NULL
           END-IF
           IF IND-DTC-CAR-GEST = -1
              MOVE HIGH-VALUES TO DTC-CAR-GEST-NULL
           END-IF
           IF IND-DTC-CAR-INC = -1
              MOVE HIGH-VALUES TO DTC-CAR-INC-NULL
           END-IF
           IF IND-DTC-PROV-ACQ-1AA = -1
              MOVE HIGH-VALUES TO DTC-PROV-ACQ-1AA-NULL
           END-IF
           IF IND-DTC-PROV-ACQ-2AA = -1
              MOVE HIGH-VALUES TO DTC-PROV-ACQ-2AA-NULL
           END-IF
           IF IND-DTC-PROV-RICOR = -1
              MOVE HIGH-VALUES TO DTC-PROV-RICOR-NULL
           END-IF
           IF IND-DTC-PROV-INC = -1
              MOVE HIGH-VALUES TO DTC-PROV-INC-NULL
           END-IF
           IF IND-DTC-PROV-DA-REC = -1
              MOVE HIGH-VALUES TO DTC-PROV-DA-REC-NULL
           END-IF
           IF IND-DTC-COD-DVS = -1
              MOVE HIGH-VALUES TO DTC-COD-DVS-NULL
           END-IF
           IF IND-DTC-FRQ-MOVI = -1
              MOVE HIGH-VALUES TO DTC-FRQ-MOVI-NULL
           END-IF
           IF IND-DTC-COD-TARI = -1
              MOVE HIGH-VALUES TO DTC-COD-TARI-NULL
           END-IF
           IF IND-DTC-IMP-AZ = -1
              MOVE HIGH-VALUES TO DTC-IMP-AZ-NULL
           END-IF
           IF IND-DTC-IMP-ADER = -1
              MOVE HIGH-VALUES TO DTC-IMP-ADER-NULL
           END-IF
           IF IND-DTC-IMP-TFR = -1
              MOVE HIGH-VALUES TO DTC-IMP-TFR-NULL
           END-IF
           IF IND-DTC-IMP-VOLO = -1
              MOVE HIGH-VALUES TO DTC-IMP-VOLO-NULL
           END-IF
           IF IND-DTC-MANFEE-ANTIC = -1
              MOVE HIGH-VALUES TO DTC-MANFEE-ANTIC-NULL
           END-IF
           IF IND-DTC-MANFEE-RICOR = -1
              MOVE HIGH-VALUES TO DTC-MANFEE-RICOR-NULL
           END-IF
           IF IND-DTC-MANFEE-REC = -1
              MOVE HIGH-VALUES TO DTC-MANFEE-REC-NULL
           END-IF
           IF IND-DTC-DT-ESI-TIT = -1
              MOVE HIGH-VALUES TO DTC-DT-ESI-TIT-NULL
           END-IF
           IF IND-DTC-SPE-AGE = -1
              MOVE HIGH-VALUES TO DTC-SPE-AGE-NULL
           END-IF
           IF IND-DTC-CAR-IAS = -1
              MOVE HIGH-VALUES TO DTC-CAR-IAS-NULL
           END-IF
           IF IND-DTC-TOT-INTR-PREST = -1
              MOVE HIGH-VALUES TO DTC-TOT-INTR-PREST-NULL
           END-IF
           IF IND-DTC-IMP-TRASFE = -1
              MOVE HIGH-VALUES TO DTC-IMP-TRASFE-NULL
           END-IF
           IF IND-DTC-IMP-TFR-STRC = -1
              MOVE HIGH-VALUES TO DTC-IMP-TFR-STRC-NULL
           END-IF
           IF IND-DTC-NUM-GG-RITARDO-PAG = -1
              MOVE HIGH-VALUES TO DTC-NUM-GG-RITARDO-PAG-NULL
           END-IF
           IF IND-DTC-NUM-GG-RIVAL = -1
              MOVE HIGH-VALUES TO DTC-NUM-GG-RIVAL-NULL
           END-IF
           IF IND-DTC-ACQ-EXP = -1
              MOVE HIGH-VALUES TO DTC-ACQ-EXP-NULL
           END-IF
           IF IND-DTC-REMUN-ASS = -1
              MOVE HIGH-VALUES TO DTC-REMUN-ASS-NULL
           END-IF
           IF IND-DTC-COMMIS-INTER = -1
              MOVE HIGH-VALUES TO DTC-COMMIS-INTER-NULL
           END-IF
           IF IND-DTC-CNBT-ANTIRAC = -1
              MOVE HIGH-VALUES TO DTC-CNBT-ANTIRAC-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO DTC-DS-OPER-SQL
           MOVE 0                   TO DTC-DS-VER
           MOVE IDSV0003-USER-NAME TO DTC-DS-UTENTE
           MOVE '1'                   TO DTC-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO DTC-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO DTC-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF DTC-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-DTC-ID-MOVI-CHIU
           END-IF
           IF DTC-DT-INI-COP-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-DT-INI-COP
           ELSE
              MOVE 0 TO IND-DTC-DT-INI-COP
           END-IF
           IF DTC-DT-END-COP-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-DT-END-COP
           ELSE
              MOVE 0 TO IND-DTC-DT-END-COP
           END-IF
           IF DTC-PRE-NET-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-PRE-NET
           ELSE
              MOVE 0 TO IND-DTC-PRE-NET
           END-IF
           IF DTC-INTR-FRAZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-INTR-FRAZ
           ELSE
              MOVE 0 TO IND-DTC-INTR-FRAZ
           END-IF
           IF DTC-INTR-MORA-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-INTR-MORA
           ELSE
              MOVE 0 TO IND-DTC-INTR-MORA
           END-IF
           IF DTC-INTR-RETDT-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-INTR-RETDT
           ELSE
              MOVE 0 TO IND-DTC-INTR-RETDT
           END-IF
           IF DTC-INTR-RIAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-INTR-RIAT
           ELSE
              MOVE 0 TO IND-DTC-INTR-RIAT
           END-IF
           IF DTC-DIR-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-DIR
           ELSE
              MOVE 0 TO IND-DTC-DIR
           END-IF
           IF DTC-SPE-MED-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-SPE-MED
           ELSE
              MOVE 0 TO IND-DTC-SPE-MED
           END-IF
           IF DTC-TAX-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-TAX
           ELSE
              MOVE 0 TO IND-DTC-TAX
           END-IF
           IF DTC-SOPR-SAN-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-SOPR-SAN
           ELSE
              MOVE 0 TO IND-DTC-SOPR-SAN
           END-IF
           IF DTC-SOPR-SPO-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-SOPR-SPO
           ELSE
              MOVE 0 TO IND-DTC-SOPR-SPO
           END-IF
           IF DTC-SOPR-TEC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-SOPR-TEC
           ELSE
              MOVE 0 TO IND-DTC-SOPR-TEC
           END-IF
           IF DTC-SOPR-PROF-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-SOPR-PROF
           ELSE
              MOVE 0 TO IND-DTC-SOPR-PROF
           END-IF
           IF DTC-SOPR-ALT-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-SOPR-ALT
           ELSE
              MOVE 0 TO IND-DTC-SOPR-ALT
           END-IF
           IF DTC-PRE-TOT-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-PRE-TOT
           ELSE
              MOVE 0 TO IND-DTC-PRE-TOT
           END-IF
           IF DTC-PRE-PP-IAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-PRE-PP-IAS
           ELSE
              MOVE 0 TO IND-DTC-PRE-PP-IAS
           END-IF
           IF DTC-PRE-SOLO-RSH-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-PRE-SOLO-RSH
           ELSE
              MOVE 0 TO IND-DTC-PRE-SOLO-RSH
           END-IF
           IF DTC-CAR-ACQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-CAR-ACQ
           ELSE
              MOVE 0 TO IND-DTC-CAR-ACQ
           END-IF
           IF DTC-CAR-GEST-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-CAR-GEST
           ELSE
              MOVE 0 TO IND-DTC-CAR-GEST
           END-IF
           IF DTC-CAR-INC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-CAR-INC
           ELSE
              MOVE 0 TO IND-DTC-CAR-INC
           END-IF
           IF DTC-PROV-ACQ-1AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-PROV-ACQ-1AA
           ELSE
              MOVE 0 TO IND-DTC-PROV-ACQ-1AA
           END-IF
           IF DTC-PROV-ACQ-2AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-PROV-ACQ-2AA
           ELSE
              MOVE 0 TO IND-DTC-PROV-ACQ-2AA
           END-IF
           IF DTC-PROV-RICOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-PROV-RICOR
           ELSE
              MOVE 0 TO IND-DTC-PROV-RICOR
           END-IF
           IF DTC-PROV-INC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-PROV-INC
           ELSE
              MOVE 0 TO IND-DTC-PROV-INC
           END-IF
           IF DTC-PROV-DA-REC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-PROV-DA-REC
           ELSE
              MOVE 0 TO IND-DTC-PROV-DA-REC
           END-IF
           IF DTC-COD-DVS-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-COD-DVS
           ELSE
              MOVE 0 TO IND-DTC-COD-DVS
           END-IF
           IF DTC-FRQ-MOVI-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-FRQ-MOVI
           ELSE
              MOVE 0 TO IND-DTC-FRQ-MOVI
           END-IF
           IF DTC-COD-TARI-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-COD-TARI
           ELSE
              MOVE 0 TO IND-DTC-COD-TARI
           END-IF
           IF DTC-IMP-AZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-IMP-AZ
           ELSE
              MOVE 0 TO IND-DTC-IMP-AZ
           END-IF
           IF DTC-IMP-ADER-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-IMP-ADER
           ELSE
              MOVE 0 TO IND-DTC-IMP-ADER
           END-IF
           IF DTC-IMP-TFR-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-IMP-TFR
           ELSE
              MOVE 0 TO IND-DTC-IMP-TFR
           END-IF
           IF DTC-IMP-VOLO-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-IMP-VOLO
           ELSE
              MOVE 0 TO IND-DTC-IMP-VOLO
           END-IF
           IF DTC-MANFEE-ANTIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-MANFEE-ANTIC
           ELSE
              MOVE 0 TO IND-DTC-MANFEE-ANTIC
           END-IF
           IF DTC-MANFEE-RICOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-MANFEE-RICOR
           ELSE
              MOVE 0 TO IND-DTC-MANFEE-RICOR
           END-IF
           IF DTC-MANFEE-REC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-MANFEE-REC
           ELSE
              MOVE 0 TO IND-DTC-MANFEE-REC
           END-IF
           IF DTC-DT-ESI-TIT-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-DT-ESI-TIT
           ELSE
              MOVE 0 TO IND-DTC-DT-ESI-TIT
           END-IF
           IF DTC-SPE-AGE-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-SPE-AGE
           ELSE
              MOVE 0 TO IND-DTC-SPE-AGE
           END-IF
           IF DTC-CAR-IAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-CAR-IAS
           ELSE
              MOVE 0 TO IND-DTC-CAR-IAS
           END-IF
           IF DTC-TOT-INTR-PREST-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-TOT-INTR-PREST
           ELSE
              MOVE 0 TO IND-DTC-TOT-INTR-PREST
           END-IF
           IF DTC-IMP-TRASFE-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-IMP-TRASFE
           ELSE
              MOVE 0 TO IND-DTC-IMP-TRASFE
           END-IF
           IF DTC-IMP-TFR-STRC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-IMP-TFR-STRC
           ELSE
              MOVE 0 TO IND-DTC-IMP-TFR-STRC
           END-IF
           IF DTC-NUM-GG-RITARDO-PAG-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-NUM-GG-RITARDO-PAG
           ELSE
              MOVE 0 TO IND-DTC-NUM-GG-RITARDO-PAG
           END-IF
           IF DTC-NUM-GG-RIVAL-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-NUM-GG-RIVAL
           ELSE
              MOVE 0 TO IND-DTC-NUM-GG-RIVAL
           END-IF
           IF DTC-ACQ-EXP-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-ACQ-EXP
           ELSE
              MOVE 0 TO IND-DTC-ACQ-EXP
           END-IF
           IF DTC-REMUN-ASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-REMUN-ASS
           ELSE
              MOVE 0 TO IND-DTC-REMUN-ASS
           END-IF
           IF DTC-COMMIS-INTER-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-COMMIS-INTER
           ELSE
              MOVE 0 TO IND-DTC-COMMIS-INTER
           END-IF
           IF DTC-CNBT-ANTIRAC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DTC-CNBT-ANTIRAC
           ELSE
              MOVE 0 TO IND-DTC-CNBT-ANTIRAC
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : DTC-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE DETT-TIT-CONT TO WS-BUFFER-TABLE.

           MOVE DTC-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO DTC-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO DTC-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO DTC-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO DTC-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO DTC-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO DTC-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO DTC-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.

           MOVE DETT-TIT-CONT TO WS-BUFFER-TABLE.

           MOVE DTC-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.

       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO DETT-TIT-CONT.

           MOVE WS-ID-MOVI-CRZ  TO DTC-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO DTC-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO DTC-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO DTC-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO DTC-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO DTC-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO DTC-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE DTC-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO DTC-DT-INI-EFF-DB
           MOVE DTC-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO DTC-DT-END-EFF-DB
           IF IND-DTC-DT-INI-COP = 0
               MOVE DTC-DT-INI-COP TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO DTC-DT-INI-COP-DB
           END-IF
           IF IND-DTC-DT-END-COP = 0
               MOVE DTC-DT-END-COP TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO DTC-DT-END-COP-DB
           END-IF
           IF IND-DTC-DT-ESI-TIT = 0
               MOVE DTC-DT-ESI-TIT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO DTC-DT-ESI-TIT-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE DTC-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO DTC-DT-INI-EFF
           MOVE DTC-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO DTC-DT-END-EFF
           IF IND-DTC-DT-INI-COP = 0
               MOVE DTC-DT-INI-COP-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO DTC-DT-INI-COP
           END-IF
           IF IND-DTC-DT-END-COP = 0
               MOVE DTC-DT-END-COP-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO DTC-DT-END-COP
           END-IF
           IF IND-DTC-DT-ESI-TIT = 0
               MOVE DTC-DT-ESI-TIT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO DTC-DT-ESI-TIT
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.
